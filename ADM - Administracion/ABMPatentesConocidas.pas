{-----------------------------------------------------------------------------
 File Name: ABMPatentesConocidas.pas
 Author: Eduardo Baeza O.
 Date Created: 28-feb-2013
 Language: ES-CH
 Firma       : PCL00200_EBA_20130228
 Description: ingresar, actualiza o elimina una patente a la cual se le eliguen ciertos estado

 Etiqueta       :   TASK_135_MGO_20170208
 Descripci�n    :   Se redise�a el formulario entero, a�adiendo Motivos de Seguimiento y pesta�a TAGs

-----------------------------------------------------------------------------}
unit ABMPatentesConocidas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, ADODB, DMConnection, Util, PeaProcsCN,
  DPSControls, VariantComboBox, Masks, ImgList, Grids, DBGrids, ComCtrls,
  DBClient, Provider, ToolWin, Variants;

type
  TFormABMPatentesConocidas = class(TForm)
    GroupB: TPanel;
    Label15: TLabel;
    Panel2: TPanel;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    RadioGroup1: TRadioGroup;
    chkForzarValidacionManual: TCheckBox;
    spInserta: TADOStoredProc;
    txtPatente: TEdit;
    spActualiza: TADOStoredProc;
    spElimina: TADOStoredProc;
    pgcCentral: TPageControl;
    tsPatentes: TTabSheet;
    dbgrdPatentes: TDBGrid;
    spPatentesConocidas_SELECT: TADOStoredProc;
    dtstprPatentes: TDataSetProvider;
    cdsPatentes: TClientDataSet;
    dsPatentes: TDataSource;
    tsTAGs: TTabSheet;
    lbl1: TLabel;
    vcbMotivoSeguimiento: TVariantComboBox;
    ilImagenes: TImageList;
    pnl1: TPanel;
    tlbBotonera: TToolBar;
    btnSalir1: TToolButton;
    btn1: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    dbgrdTAGs: TDBGrid;
    pnlTAGs: TPanel;
    lbl3: TLabel;
    vcbMotivoSeguimientoTAG: TVariantComboBox;
    spTAGsEnSeguimiento_SELECT: TADOStoredProc;
    dtstprTAGs: TDataSetProvider;
    cdsTAGs: TClientDataSet;
    dsTAGs: TDataSource;
    spInsertaTAG: TADOStoredProc;
    spEliminaTAG: TADOStoredProc;
    lbl2: TLabel;
    edtEtiqueta: TEdit;
    lbl4: TLabel;
    vcbConcesionaria: TVariantComboBox;
    chkMMIContactOperator: TCheckBox;
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure cdsPatentesAfterScroll(DataSet: TDataSet);
    procedure btnEliminarClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure pgcCentralChanging(Sender: TObject; var AllowChange: Boolean);
    procedure pgcCentralChange(Sender: TObject);
    procedure cdsTAGsAfterScroll(DataSet: TDataSet);
    procedure dbgrdTAGsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    Accion: Integer; // 0: Consulta; 1: Insertar; 2: Modificar
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
    function CargarPatentes: Boolean;   
    function CargarTAGs: Boolean;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormABMPatentesConocidas: TFormABMPatentesConocidas;

implementation

resourcestring
    MSG_DELETE_CAPTION		    = 'Eliminar';
    MSG_DELETE_QUESTION		    = '�Est� seguro de querer eliminar el filtro para patentes conocidas seleccionado?';
    MSG_DELETE_QUESTION_TAG	    = '�Desea quitar el TAG de Seguimento?';
    MSG_DELETE_ERROR		      = 'No se puede eliminar';
    MSG_DELETE_PROCESO        = 'Registro eliminado con �xito';

    MSG_ACTUALIZAR_CAPTION	  = 'Actualizar';
    MSG_ACTUALIZAR_ERROR  	  = 'Error no se pudo actualizar el registro';
    MSG_ACTUALIZA_PROCESO     = 'Registro actualizado con �xito';

    MSG_INSERTA_CAPTION       = 'Agregar';
    MSG_INSERTA_ERROR         = 'Error al agregar registro';
    MSG_INSERTA_PROCESO       = 'Registro agregado con �xito';

    MSG_DESCRIPCION           = 'Debe ingresar un filtro';
    MSG_OPCION                = 'Debe seleccionar una opci�n';
    MSG_PATENTE_ERROR         = 'S�lo puede ingresar letras, espacio, n�meros y los carcateres especiales # & *, (excepto la letra � y �) ';

    MSG_CAPTION_PROCESO       = 'Atenci�n';

{$R *.dfm}

procedure TFormABMPatentesConocidas.VolverCampos;
begin
    Accion := 0;
    
    btnSalir1.Enabled := True;
    btnAgregar.Enabled := True;
    if pgcCentral.ActivePage = tsPatentes then begin
        btnEditar.Enabled := (cdsPatentes.RecordCount > 0);
        btnEliminar.Enabled := (cdsPatentes.RecordCount > 0);
    end;
    if pgcCentral.ActivePage = tsTAGs then begin   
        btnEditar.Enabled := False;
        btnEliminar.Enabled := (cdsTAGs.RecordCount > 0);
    end;

    txtPatente.Clear;
    Notebook.PageIndex 	:= 0;
    groupb.Enabled      := False;
    pnlTAGs.Enabled     := False;
    txtPatente.Enabled	:= False;
    edtEtiqueta.Enabled := False;
    vcbMotivoSeguimiento.Enabled    := False;
    vcbMotivoSeguimientoTAG.Enabled := False;
    vcbConcesionaria.Enabled    := False;

    dbgrdPatentes.Enabled := True;
    dbgrdTAGs.Enabled   := True;
end;

function TFormABMPatentesConocidas.Inicializa: boolean;
resourcestring
    MSG_ERROR_MOTIVOS = 'Ha ocurrido un error al obtener los motivos de seguimiento';
    MSG_ERROR_CONCESIONARIAS = 'Ha ocurrido un error al obtener las concesionarias';
Var
	S: TSize;
    spMotivosSeguimiento_SELECT: TADOStoredProc;
  spObtenerConcesionarias: TADOStoredProc;
begin
    Result := True;
    Accion := 0;

    vcbMotivoSeguimiento.Clear;
    spMotivosSeguimiento_SELECT := TADOStoredProc.Create(Self);
    try
        try
            spMotivosSeguimiento_SELECT.Connection := DMConnections.BaseCOP;
            spMotivosSeguimiento_SELECT.ProcedureName := 'MotivosSeguimiento_SELECT';
            spMotivosSeguimiento_SELECT.CommandTimeout := 30;
            spMotivosSeguimiento_SELECT.Parameters.Refresh;
            spMotivosSeguimiento_SELECT.Open;

            if spMotivosSeguimiento_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spMotivosSeguimiento_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

            vcbMotivoSeguimiento.Items.Add('(ninguno)', 0);
            vcbMotivoSeguimientoTAG.Items.Add('(ninguno)', 0);
            while not spMotivosSeguimiento_SELECT.Eof do begin
                vcbMotivoSeguimiento.Items.Add(
                    spMotivosSeguimiento_SELECT.FieldByName('Descripcion').AsString,
                    spMotivosSeguimiento_SELECT.FieldByName('CodigoMotivo').AsInteger);

                vcbMotivoSeguimientoTAG.Items.Add(
                    spMotivosSeguimiento_SELECT.FieldByName('Descripcion').AsString,
                    spMotivosSeguimiento_SELECT.FieldByName('CodigoMotivo').AsInteger);

                spMotivosSeguimiento_SELECT.Next;
            end;
            vcbMotivoSeguimiento.ItemIndex := 0;   
            vcbMotivoSeguimientoTAG.ItemIndex := 0;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_MOTIVOS, e.Message, 'Error', MB_ICONERROR);
                Result := False;
                Exit;
            end;
        end;
    finally
        spMotivosSeguimiento_SELECT.Free;
    end;

    spObtenerConcesionarias := TADOStoredProc.Create(Self);
    try
        try
            spObtenerConcesionarias.Connection := DMConnections.BaseCAC;
            spObtenerConcesionarias.ProcedureName := 'ObtenerConcesionarias';
            spObtenerConcesionarias.CommandTimeout := 30;
            spObtenerConcesionarias.Parameters.Refresh;
            spObtenerConcesionarias.Parameters.ParamByName('@FiltraPorConcepto').Value := 0;
            spObtenerConcesionarias.Open;

            if spObtenerConcesionarias.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spObtenerConcesionarias.Parameters.ParamByName('@ErrorDescription').Value);

            vcbConcesionaria.Items.Add('(seleccione)', 0);
            while not spObtenerConcesionarias.Eof do begin
                vcbConcesionaria.Items.Add(
                    spObtenerConcesionarias.FieldByName('Descripcion').AsString,
                    spObtenerConcesionarias.FieldByName('CodigoConcesionaria').AsInteger);

                spObtenerConcesionarias.Next;
            end;
            vcbConcesionaria.ItemIndex := 0;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_CONCESIONARIAS, e.Message, 'Error', MB_ICONERROR);
                Result := False;
                Exit;
            end;
        end;
    finally
        spObtenerConcesionarias.Free;
    end;

    if Result then
        Result := CargarPatentes;    

    if Result then
        Result := CargarTAGs;

    VolverCampos;
end;

function TFormABMPatentesConocidas.CargarPatentes: Boolean;
begin
    Result := True;
    try
        try
            spPatentesConocidas_SELECT.Parameters.Refresh;
            spPatentesConocidas_SELECT.Open;

            if spPatentesConocidas_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spPatentesConocidas_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

            cdsPatentes.Data := dtstprPatentes.Data;
        except
            on e: Exception do begin
                MsgBoxErr('Error al obtener las Patentes', e.Message, 'Error', MB_ICONERROR);
                Result := False;
            end;
        end;
    finally
        spPatentesConocidas_SELECT.Close;
    end;
end;  

function TFormABMPatentesConocidas.CargarTAGs: Boolean;
begin
    Result := True;
    try
        try
            spTAGsEnSeguimiento_SELECT.Parameters.Refresh;
            spTAGsEnSeguimiento_SELECT.Open;

            if spTAGsEnSeguimiento_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spTAGsEnSeguimiento_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

            cdsTAGs.Data := dtstprTAGs.Data;
        except
            on e: Exception do begin
                MsgBoxErr('Error al obtener los TAGs', e.Message, 'Error', MB_ICONERROR);
                Result := False;
            end;
        end;
    finally
        spTAGsEnSeguimiento_SELECT.Close;
    end;
end;

procedure TFormABMPatentesConocidas.cdsPatentesAfterScroll(DataSet: TDataSet);
begin
    with cdsPatentes do begin
        txtPatente.Text := FieldByName('Patente').AsString;
        if FieldByName('Gold').AsBoolean = True then RadioGroup1.ItemIndex := 0;
        if FieldByName('CargaPeligrosa').AsBoolean = True then RadioGroup1.ItemIndex := 1;
        if FieldByName('EnSeguimiento').AsBoolean = True then RadioGroup1.ItemIndex := 2;
        if FieldByName('Fraudulenta').AsBoolean = true then RadioGroup1.ItemIndex := 3;
        chkForzarValidacionManual.Checked := FieldByName('ForzarValidacionManual').AsBoolean;
        vcbMotivoSeguimiento.Value := FieldByName('CodigoMotivo').AsInteger;
    end;
end;

procedure TFormABMPatentesConocidas.cdsTAGsAfterScroll(DataSet: TDataSet);
begin
    with cdsTAGs do begin
        edtEtiqueta.Text := FieldByName('NumeroSerie').AsString;
        vcbConcesionaria.Value := FieldByName('CodigoConcesionaria').AsInteger;
        chkMMIContactOperator.Checked := FieldByName('MMIContactOperator').AsBoolean;
        vcbMotivoSeguimientoTAG.Value := FieldByName('CodigoMotivo').AsInteger;
    end;
end;

procedure TFormABMPatentesConocidas.dbgrdTAGsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
    if cdsTAGs.FieldByName('FechaHoraBaja').AsString <> '' then begin
        dbgrdTAGs.Canvas.Font.Color := clRed;
        dbgrdTAGs.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
end;

procedure TFormABMPatentesConocidas.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormABMPatentesConocidas.btnEditarClick(Sender: TObject);
begin
    Accion := 2;
    if pgcCentral.ActivePage = tsPatentes then begin
        HabilitarCampos;
        txtPatente.Enabled := False;
        RadioGroup1.SetFocus;
    end;
end;

procedure TFormABMPatentesConocidas.btnEliminarClick(Sender: TObject);
begin
    try
        if pgcCentral.ActivePage = tsPatentes then begin
            If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO+MB_ICONQUESTION) = ID_NO then
                Exit;

            Screen.Cursor := crHourGlass;
            try
                with spElimina, Parameters do begin
                    refresh;
                    ParamByName('@Patente').Value := txtPatente.Text;
                    ParamByName('@Usuario').Value := UsuarioSistema;
                    ExecProc;
                    if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then begin
                        MsgBox(MSG_DELETE_PROCESO, MSG_CAPTION_PROCESO);
                    end else begin
                        raise exception.Create(Parameters.ParamByName('@ErrorDescription').Value);
                    end;
                end;
            except
                On E: Exception do
                    MsgBoxErr(MSG_DELETE_ERROR, E.Message, 'Error', MB_ICONERROR);
            end;
        end;

        if pgcCentral.ActivePage = tsTAGs then begin
            If MsgBox(MSG_DELETE_QUESTION_TAG, MSG_DELETE_CAPTION, MB_YESNO+MB_ICONQUESTION) = ID_NO then
                Exit;

            Screen.Cursor := crHourGlass;
            try
                with spEliminaTAG, Parameters do begin
                    Refresh;
                    ParamByName('@IdTAGEnSeguimiento').Value := cdsTAGs.FieldByName('IdTAGEnSeguimiento').Value;
                    ParamByName('@UsuarioBaja').Value := UsuarioSistema;
                    ExecProc;
                    if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then begin
                        MsgBox(MSG_DELETE_PROCESO, MSG_CAPTION_PROCESO);
                    end else begin
                        raise exception.Create(Parameters.ParamByName('@ErrorDescription').Value);
                    end;
                end;
            except
                On E: Exception do
                    MsgBoxErr(MSG_DELETE_ERROR, E.Message, 'Error', MB_ICONERROR);
            end;
        end;
    finally
        CargarPatentes;
        CargarTAGs;
        VolverCampos;
        Screen.Cursor := crDefault;
    end;
end;

procedure TFormABMPatentesConocidas.Limpiar_Campos;
begin
	txtPatente.Clear;
    RadioGroup1.ItemIndex := -1;
    chkForzarValidacionManual.Checked := false;
    vcbMotivoSeguimiento.ItemIndex := 0;

    edtEtiqueta.Clear;
    chkMMIContactOperator.Checked := False;
    vcbConcesionaria.ItemIndex := 0;
    vcbMotivoSeguimientoTAG.ItemIndex := 0;
end;

procedure TFormABMPatentesConocidas.pgcCentralChange(Sender: TObject);
begin
    VolverCampos;
end;

procedure TFormABMPatentesConocidas.pgcCentralChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
    AllowChange := (Accion = 0);
end;

procedure TFormABMPatentesConocidas.RadioGroup1Click(Sender: TObject);
begin
    vcbMotivoSeguimiento.Enabled := (RadioGroup1.ItemIndex = 2) and (Accion > 0);
end;

procedure TFormABMPatentesConocidas.BtnAceptarClick(Sender: TObject);
      function ValidaContenido: Boolean;
      var
          I: Integer;
          Mask: String;
      begin
          Result := true;

          Mask := '';
          for I := 0 to Length(txtPatente.Text) - 1 do
          begin
            Mask := Mask + '[a-n,o-z,A-N,O-Z,0-9,*,&,#]';
          end;

          if not MatchesMask(txtPatente.Text, Mask) then
              Result := False;
      end;
Var
  Msg_Caption: String;
  Msg_Error: String;
begin                                   
    if Accion = 1 then begin
        Msg_Caption := MSG_INSERTA_CAPTION;
    end else if Accion = 2 then begin
        Msg_Caption :=  MSG_ACTUALIZAR_CAPTION;
    end;

    if pgcCentral.ActivePage = tsPatentes then begin
        if not ValidateControlsCN([txtPatente, RadioGroup1, txtPatente],
                    [(trim(txtPatente.text) <> ''),
                    RadioGroup1.ItemIndex > -1,
                    ValidaContenido()],
                    Msg_Caption,
                    [MSG_DESCRIPCION, MSG_OPCION, MSG_PATENTE_ERROR], Self) then exit;

        Try
            Screen.Cursor := crHourGlass;
            Try
                if Accion = 1 then
                begin
                    Msg_Error := MSG_INSERTA_ERROR;
                    with spInserta, Parameters do begin
                        refresh;
                        ParamByName('@Patente').Value := txtPatente.Text;
                        ParamByName('@Gold').Value := (RadioGroup1.ItemIndex = 0);
                        ParamByName('@CargaPeligrosa').Value := (RadioGroup1.ItemIndex = 1);
                        ParamByName('@EnSeguimiento').Value := (RadioGroup1.ItemIndex = 2);
                        ParamByName('@Fraudulenta').Value := (RadioGroup1.ItemIndex = 3);
                        ParamByName('@ForzarValidacionManual').Value := chkForzarValidacionManual.Checked;
                        ParamByName('@CodigoMotivo').Value := IIf(RadioGroup1.ItemIndex = 2, vcbMotivoSeguimiento.Value, Null);

                        ParamByName('@Usuario').Value := UsuarioSistema;
                        ExecProc;

                        if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then begin
                            MsgBox(MSG_INSERTA_PROCESO, MSG_CAPTION_PROCESO);
                        end else begin
                            raise exception.Create(Parameters.ParamByName('@ErrorDescription').Value);
                        end;
                    end;
                end
                else if Accion = 2 then begin
                    Msg_Error := MSG_ACTUALIZAR_ERROR;
                    with spActualiza, Parameters do begin
                        refresh;
                        ParamByName('@Patente').Value := txtPatente.Text;
                        ParamByName('@Gold').Value := (RadioGroup1.ItemIndex = 0);
                        ParamByName('@CargaPeligrosa').Value := (RadioGroup1.ItemIndex = 1);
                        ParamByName('@EnSeguimiento').Value := (RadioGroup1.ItemIndex = 2);
                        ParamByName('@Fraudulenta').Value := (RadioGroup1.ItemIndex = 3);
                        ParamByName('@ForzarValidacionManual').Value := chkForzarValidacionManual.Checked;    
                        ParamByName('@CodigoMotivo').Value := IIf(RadioGroup1.ItemIndex = 2, vcbMotivoSeguimiento.Value, Null);
                        ParamByName('@Usuario').Value := UsuarioSistema;
                        ExecProc;

                        if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then begin
                            MsgBox(MSG_ACTUALIZA_PROCESO, MSG_CAPTION_PROCESO);
                        end else begin
                            raise exception.Create(Parameters.ParamByName('@ErrorDescription').Value);
                        end;
                    end;
                end;
            except
                On E: Exception do begin
                    MsgBoxErr(Msg_Error, E.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        Finally
            Screen.Cursor	:= crDefault;
        End;
    end;

    if pgcCentral.ActivePage = tsTAGs then begin
        if edtEtiqueta.Text = EmptyStr then begin
            MsgBoxBalloon('Debe ingresar una Etiqueta', Msg_Caption, MB_ICONSTOP, edtEtiqueta);
            Exit;
        end;

        if vcbConcesionaria.ItemIndex = 0 then begin
            MsgBoxBalloon('Debe seleccionar la Concesionaria del TAG', Msg_Caption, MB_ICONSTOP, vcbConcesionaria);
            Exit;
        end;

        try
            Screen.Cursor := crHourGlass;
            try
                if Accion = 1 then
                begin
                    Msg_Error := MSG_INSERTA_ERROR;
                    with spInsertaTAG, Parameters do begin
                        refresh;
                        ParamByName('@NumeroSerie').Value := edtEtiqueta.Text;
                        ParamByName('@CodigoConcesionaria').Value := vcbConcesionaria.Value;
                        ParamByName('@MMIContactOperator').Value := chkMMIContactOperator.Checked;
                        ParamByName('@CodigoMotivo').Value := vcbMotivoSeguimientoTAG.Value;
                        ParamByName('@UsuarioAlta').Value := UsuarioSistema;
                        ExecProc;

                        if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then begin
                            MsgBox(MSG_INSERTA_PROCESO, MSG_CAPTION_PROCESO);
                        end else begin
                            raise exception.Create(Parameters.ParamByName('@ErrorDescription').Value);
                        end;
                    end;
                end;
            except
                On E: Exception do begin
                    MsgBoxErr(Msg_Error, E.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        finally
            Screen.Cursor := crDefault;
        end;
    end;

    CargarPatentes;
    CargarTAGs;
    VolverCampos;
end;

procedure TFormABMPatentesConocidas.btnAgregarClick(Sender: TObject);
begin
    Accion := 1;                               
    Limpiar_Campos;
    HabilitarCampos;
    if pgcCentral.ActivePage = tsPatentes then
        txtPatente.SetFocus
    else if pgcCentral.ActivePage = tsTAGs then
        edtEtiqueta.SetFocus;
end;

procedure TFormABMPatentesConocidas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormABMPatentesConocidas.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormABMPatentesConocidas.HabilitarCampos;
begin
    Notebook.PageIndex 	:= 1;

    btnSalir1.Enabled   := False;
    btnAgregar.Enabled  := False;
    btnEditar.Enabled   := False;
    btnEliminar.Enabled := False;

    if pgcCentral.ActivePage = tsPatentes then begin
        groupb.Enabled     	:= True;
        txtPatente.Enabled	:= True;
        dbgrdPatentes.Enabled := False;
        vcbMotivoSeguimiento.Enabled := (RadioGroup1.ItemIndex = 2);
    end;

    if pgcCentral.ActivePage = tsTAGs then begin
        pnlTAGs.Enabled := True;
        edtEtiqueta.Enabled := True;
        vcbConcesionaria.Enabled := True;
        vcbMotivoSeguimientoTAG.Enabled := True;
        dbgrdTAGs.Enabled := False;
    end;
end;

end.
