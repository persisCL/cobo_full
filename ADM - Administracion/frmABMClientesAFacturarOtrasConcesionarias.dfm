object ABMClientesAFacturarOtrasConcesionariasForm: TABMClientesAFacturarOtrasConcesionariasForm
  Left = 0
  Top = 0
  Caption = 'ABMClientesAFacturarOtrasConcesionariasForm'
  ClientHeight = 430
  ClientWidth = 805
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlCampos: TPanel
    Left = 0
    Top = 240
    Width = 805
    Height = 151
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Label1: TLabel
      Left = 25
      Top = 47
      Width = 116
      Height = 13
      Caption = 'Numero Documento:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 56
      Top = 74
      Width = 85
      Height = 13
      Caption = 'Concesionaria:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 13
      Top = 101
      Width = 128
      Height = 13
      Caption = 'Fecha Inicio Vigencia:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 27
      Top = 128
      Width = 114
      Height = 13
      Caption = 'Fecha Fin Vigencia:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 97
      Top = 15
      Width = 44
      Height = 13
      Caption = 'Cliente:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCliente: TLabel
      Left = 152
      Top = 15
      Width = 43
      Height = 13
      Caption = 'lblCliente'
    end
    object vcbConcesionaria: TVariantComboBox
      Left = 147
      Top = 70
      Width = 230
      Height = 21
      Style = vcsDropDownList
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
    object peNumeroDocumento: TPickEdit
      Left = 147
      Top = 43
      Width = 177
      Height = 21
      Enabled = True
      TabOrder = 0
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object deFechaInicio: TDateEdit
      Left = 147
      Top = 97
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 2
      Date = -693594.000000000000000000
    end
    object deFechaFin: TDateEdit
      Left = 147
      Top = 124
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 3
      Date = -693594.000000000000000000
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 391
    Width = 805
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel1: TPanel
      Left = 483
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 126
        Top = 2
        Width = 197
        Height = 37
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object btnSalir: TButton
            Left = 116
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = btnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object btnAceptar: TButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = btnAceptarClick
          end
          object btnCancelar: TButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = btnCancelarClick
          end
        end
      end
    end
  end
  object dblClientesAFacturar: TDBListEx
    Left = 0
    Top = 73
    Width = 805
    Height = 167
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        Header.Caption = 'Cliente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NombreCliente'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Numero Documento'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroDocumento'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Concesionaria'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NombreConcesionaria'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha Inicio Vigencia'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaInicioVigencia'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha Fin Vigencia'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaFinVigencia'
      end>
    DataSource = dsClientes
    DragReorder = True
    ParentColor = False
    TabOrder = 2
    TabStop = True
  end
  object pnlToolbar: TPanel
    Left = 0
    Top = 0
    Width = 805
    Height = 73
    Align = alTop
    TabOrder = 3
    object btnExit: TSpeedButton
      Left = 31
      Top = 13
      Width = 23
      Height = 22
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00330000000000
        03333377777777777F333301111111110333337F333333337F33330111111111
        0333337F333333337F333301111111110333337F333333337F33330111111111
        0333337F333333337F333301111111110333337F333333337F33330111111111
        0333337F3333333F7F333301111111B10333337F333333737F33330111111111
        0333337F333333337F333301111111110333337F33FFFFF37F3333011EEEEE11
        0333337F377777F37F3333011EEEEE110333337F37FFF7F37F3333011EEEEE11
        0333337F377777337F333301111111110333337F333333337F33330111111111
        0333337FFFFFFFFF7F3333000000000003333377777777777333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnSalirClick
    end
    object btnInsertar: TSpeedButton
      Left = 76
      Top = 13
      Width = 23
      Height = 22
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnInsertarClick
    end
    object btnEliminar: TSpeedButton
      Left = 105
      Top = 13
      Width = 23
      Height = 22
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        300033FFFFFF3333377739999993333333333777777F3333333F399999933333
        3300377777733333337733333333333333003333333333333377333333333333
        3333333333333333333F333333333333330033333F33333333773333C3333333
        330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
        333333377F33333333FF3333C333333330003333733333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnEliminarClick
    end
    object btnModificar: TSpeedButton
      Left = 134
      Top = 13
      Width = 23
      Height = 22
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnModificarClick
    end
    object btnBuscar: TSpeedButton
      Left = 471
      Top = 13
      Width = 23
      Height = 22
      Hint = 'Buscar - Filtrar por Rut'
      Flat = True
      Glyph.Data = {
        42010000424D4201000000000000760000002800000011000000110000000100
        040000000000CC00000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDD0000000D00DDDDDDDDDDDDDD0000000D000DDDDDDDDDDDDD0000000DD00
        0DDDDDDDDDDDD0000000DDD000D800008DDDD0000000DDDD0000777700DDD000
        0000DDDDD08EE777780DD0000000DDDD807E77777708D0000000DDDD07E77777
        7770D0000000DDDD077777777770D0000000DDDD077777777E70D0000000DDDD
        077777777E70D0000000DDDD80777777EE08D0000000DDDDD08777EEE80DD000
        0000DDDDDD00777700DDD0000000DDDDDDD800008DDDD0000000DDDDDDDDDDDD
        DDDDD0000000}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnBuscarClick
    end
    object btnRefrescar: TSpeedButton
      Left = 696
      Top = 13
      Width = 23
      Height = 22
      Hint = 'Mostrar por cantidad de registros'
      Flat = True
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000E0000000E0000000100
        0400000000007000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DD00DDDDD4444DDDDD00DDD44444444DDD00DD444DDDD444DD00DD44DDDDDD44
        DD00D44DDDDDDDD44D00D44DDDDDDDD44D00D44DDDDDDDD44D00D44DDDDDDDD4
        4D00DD44DDDD4D44DD00DD44DDDD4444DD00DDDDDDDD444DDD00DDDDDDDD4444
        DD00DDDDDDDDDDDDDD00}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnRefrescarClick
    end
    object Label6: TLabel
      Left = 221
      Top = 18
      Width = 98
      Height = 13
      Caption = 'Numero Documento:'
    end
    object Label7: TLabel
      Left = 528
      Top = 16
      Width = 95
      Height = 13
      Caption = 'Cantidad Registros:'
    end
    object Label8: TLabel
      Left = 535
      Top = 47
      Width = 88
      Height = 13
      Caption = 'Existe un total de:'
    end
    object lblTotalRegistros: TLabel
      Left = 629
      Top = 47
      Width = 96
      Height = 13
      Caption = 'lblTotalRegistros'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object neRegistros: TNumericEdit
      Left = 629
      Top = 14
      Width = 61
      Height = 21
      TabOrder = 1
    end
    object peNumDocABuscar: TPickEdit
      Left = 320
      Top = 14
      Width = 145
      Height = 21
      Enabled = True
      TabOrder = 0
      EditorStyle = bteTextEdit
    end
  end
  object spObtenerConcesionarias: TADOStoredProc
    ProcedureName = 'ObtenerConcesionarias'
    Parameters = <>
    Left = 536
    Top = 128
  end
  object qryClientes: TADOQuery
    Parameters = <>
    Left = 496
    Top = 128
  end
  object dsClientes: TDataSource
    DataSet = qryClientes
    OnDataChange = dsClientesDataChange
    Left = 496
    Top = 168
  end
  object qryEjecutar: TADOQuery
    Parameters = <>
    Prepared = True
    Left = 672
    Top = 120
  end
end
