object FormConfiguracionNotificaciones: TFormConfiguracionNotificaciones
  Left = 192
  Top = 110
  Width = 601
  Height = 594
  Caption = 'Configuraci'#243'n de Notificaciones de Morosos'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 593
    Height = 33
    Habilitados = [btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 593
    Height = 294
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'86'#0'C'#243'digo               '
      #0'181'#0'Descripci'#243'n                                       ')
    HScrollBar = True
    RefreshTime = 100
    Table = ConfiguracionNotificaciones
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object Panel2: TPanel
    Left = 0
    Top = 528
    Width = 593
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Notebook: TNotebook
      Left = 396
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 327
    Width = 593
    Height = 201
    ActivePage = tab_General
    Align = alBottom
    TabIndex = 0
    TabOrder = 3
    object tab_General: TTabSheet
      Caption = 'General'
      Enabled = False
      DesignSize = (
        585
        173)
      object Label15: TLabel
        Left = 15
        Top = 12
        Width = 39
        Height = 13
        Caption = 'C'#243'digo: '
        FocusControl = txt_Codigo
      end
      object Label1: TLabel
        Left = 15
        Top = 38
        Width = 72
        Height = 13
        Caption = 'Descripci'#243'n:'
        FocusControl = txt_Descripcion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 15
        Top = 62
        Width = 30
        Height = 13
        Caption = 'Dias:'
        FocusControl = txt_Descripcion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 15
        Top = 87
        Width = 84
        Height = 13
        Caption = 'Monto m'#237'nimo:'
        FocusControl = txt_Descripcion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 15
        Top = 114
        Width = 111
        Height = 13
        Caption = 'Medio de contacto:'
        FocusControl = cb_MediosContacto
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_Codigo: TNumericEdit
        Left = 160
        Top = 8
        Width = 69
        Height = 21
        TabStop = False
        Color = clBtnFace
        MaxLength = 3
        ReadOnly = True
        TabOrder = 0
        Decimals = 0
      end
      object txt_Descripcion: TEdit
        Left = 160
        Top = 34
        Width = 394
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        Color = 16444382
        MaxLength = 255
        TabOrder = 1
      end
      object txt_Dias: TNumericEdit
        Left = 160
        Top = 60
        Width = 121
        Height = 21
        Color = 16444382
        MaxLength = 3
        TabOrder = 2
        Decimals = 0
      end
      object txt_MontoMinimo: TNumericEdit
        Left = 160
        Top = 85
        Width = 121
        Height = 21
        Color = 16444382
        MaxLength = 8
        TabOrder = 3
        Decimals = 0
      end
      object cb_MediosContacto: TComboBox
        Left = 160
        Top = 110
        Width = 145
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 4
      end
      object chk_Activado: TCheckBox
        Left = 13
        Top = 140
        Width = 160
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Activado:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
      end
    end
    object tab_TextoAEnviar: TTabSheet
      Caption = 'Texto a Enviar'
      Enabled = False
      ImageIndex = 2
      object txt_TextoAEnviar: TMemo
        Left = 0
        Top = 0
        Width = 585
        Height = 173
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 8000
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  object ConfiguracionNotificaciones: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'ConfiguracionNotificaciones'
    Left = 528
    Top = 62
  end
end
