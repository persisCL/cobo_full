unit ABMActividades;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB, 
  DPSControls;

type
  TFormActividades = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    Actividades: TADOTable;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    txt_Descripcion: TEdit;
    Panel1: TPanel;
    Notebook: TNotebook;
	txt_codigo: TNumericEdit;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
	{ Private declarations }
    procedure Limpiar_Campos;
  public
	{ Public declarations }
	Function Inicializa : boolean;
  end;

var
  FormActividades  : TFormActividades;

implementation

resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se puedo actualizar la Actividad';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Actividad';
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar la Actividad?';
    MSG_DELETE_ERROR		= 'No se puede eliminar la Actividad porque hay datos que dependen de ella.';
    MSG_DELETE_CAPTION 		= 'Eliminar Actividad';

{$R *.DFM}

function TFormActividades.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([Actividades]) then
    	Result := False
    else begin
    	Result := True;
       	Lista.Reload;
	end;
end;

procedure TFormActividades.Limpiar_Campos();
begin
	txt_Codigo.Clear;
	txt_Descripcion.Clear;
end;

function TFormActividades.ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
begin
	Result := True;
	Texto := Tabla.FieldByName('CodigoActividad').AsString + ' ' +
	  Tabla.FieldByName('Descripcion').AsString;
end;

procedure TFormActividades.BtSalirClick(Sender: TObject);
begin
	 Close;
end;

procedure TFormActividades.ListaInsert(Sender: TObject);
begin
	Lista.Estado       := Alta;
	Limpiar_Campos;
	GroupB.Enabled     := True;
	Lista.Enabled      := False;
	Notebook.PageIndex := 1;
	txt_Codigo.SetFocus;
end;

procedure TFormActividades.ListaEdit(Sender: TObject);
begin
	Lista.Estado       := Modi;
	GroupB.Enabled     := True;
	Lista.Enabled      := False;
	Notebook.PageIndex := 1;
	txt_Codigo.SetFocus;
end;

procedure TFormActividades.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION,  MB_YESNO or MB_ICONWARNING) = IDYES then begin
		try
			Actividades.Delete;
		except
			On E: EDataBaseError do begin
				Actividades.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		Lista.Reload;
	end;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormActividades.ListaClick(Sender: TObject);
begin
	 with Actividades do begin
		  txt_Codigo.Value     := FieldByName('CodigoActividad').AsInteger;
		  txt_Descripcion.text := Trim(FieldByName('Descripcion').AsString);
	 end;
end;

procedure TFormActividades.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos;
end;

procedure TFormActividades.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormActividades.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, FieldByName('CodigoActividad').AsString);
		TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormActividades.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

procedure TFormActividades.BtnAceptarClick(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	With Actividades do begin
		Try
			if Lista.Estado = Alta then Append else Edit;
			FieldByName('CodigoActividad').AsFloat  := txt_Codigo.Value;
			FieldByName('Descripcion').AsString := Trim(txt_Descripcion.Text);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Lista.Reload;
	Lista.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormActividades.BtnCancelarClick(Sender: TObject);
begin
	GroupB.Enabled     := False;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;

procedure TFormActividades.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormActividades.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

end.
