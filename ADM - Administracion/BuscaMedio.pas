unit BuscaMedio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, PeaTypes, Db, DBTables, UtilDB, DPSControls;

type
  TFrmBuscarMedioPago = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    txt_bandaiso: TEdit;
    txt_titular: TEdit;
    txt_patente: TEdit;
	Bevel1: TBevel;
    BuscarMediosPago: TStoredProc;
    Button1: TDPSButton;
    Button2: TDPSButton;
    procedure Button1Click(Sender: TObject);
  private
	{ Private declarations }
  public
	{ Public declarations }
	property Resultado: TStoredProc Read BuscarMediosPago;
  end;

var
  FrmBuscarMedioPago: TFrmBuscarMedioPago;

implementation

{$R *.DFM}

{ TFrmBuscarMedioPago }

procedure TFrmBuscarMedioPago.Button1Click(Sender: TObject);
begin
	BuscarMediosPago.Close;
	BuscarMediosPago.ParamByName('@BandaISO').AsString := txt_bandaiso.text;
	BuscarMediosPago.ParamByName('@NombreTitular').AsString := txt_titular.text;
	BuscarMediosPago.ParamByName('@Patente').AsString := txt_patente.text;
	if not Opentables([BuscarMediosPago]) then Exit;
	ModalResult := mrOk;
end;

end.
