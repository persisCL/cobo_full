object FormABMCombinacionLetrasPatente: TFormABMCombinacionLetrasPatente
  Left = 120
  Top = 165
  Caption = 'Mantenimiento de Combinaciones V'#225'lidas de Patente'
  ClientHeight = 420
  ClientWidth = 717
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GroupB: TPanel
    Left = 0
    Top = 249
    Width = 717
    Height = 132
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Label1: TLabel
      Left = 52
      Top = 83
      Width = 72
      Height = 13
      Caption = 'Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 47
      Top = 10
      Width = 77
      Height = 13
      Caption = 'Combinaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 18
      Top = 113
      Width = 106
      Height = 13
      Caption = 'Tipo Combinaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 183
      Top = 113
      Width = 137
      Height = 13
      Caption = 'Fecha de actualizaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object GBDigitoVerificador: TGroupBox
      Left = 15
      Top = 35
      Width = 410
      Height = 41
      Caption = 'D'#237'gitos verificadores '
      TabOrder = 1
      object Label5: TLabel
        Left = 87
        Top = 21
        Width = 27
        Height = 13
        Caption = 'DV 7:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 175
        Top = 21
        Width = 27
        Height = 13
        Caption = 'DV 6:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 255
        Top = 21
        Width = 27
        Height = 13
        Caption = 'DV 5:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object edtDv7: TEdit
        Left = 119
        Top = 17
        Width = 39
        Height = 21
        Hint = 'D'#237'gito verificador'
        Color = 16444382
        MaxLength = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnKeyPress = edtDv7KeyPress
      end
      object edtDv6: TEdit
        Left = 207
        Top = 17
        Width = 39
        Height = 21
        Hint = 'D'#237'gito verificador'
        Color = 16444382
        MaxLength = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnKeyPress = edtDv7KeyPress
      end
      object edtDv5: TEdit
        Left = 287
        Top = 17
        Width = 39
        Height = 21
        Hint = 'D'#237'gito verificador'
        Color = 16444382
        MaxLength = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnKeyPress = edtDv7KeyPress
      end
    end
    object deFecha: TDateEdit
      Left = 326
      Top = 109
      Width = 90
      Height = 21
      Hint = 'Fecha de actualizaci'#243'n'
      AutoSelect = False
      TabOrder = 4
      Date = -693594.000000000000000000
    end
    object edtCombinacion: TEdit
      Left = 134
      Top = 6
      Width = 39
      Height = 21
      Hint = 'Combinaci'#243'n de letra posible para la patente'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 4
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object edtDesc: TEdit
      Left = 134
      Top = 79
      Width = 283
      Height = 21
      Hint = 'Descripci'#243'n'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 50
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object edtTipo: TEdit
      Left = 134
      Top = 109
      Width = 39
      Height = 21
      Color = 16444382
      MaxLength = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 381
    Width = 717
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 520
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = 'Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Configurar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 0
    Width = 717
    Height = 41
    Align = alTop
    TabOrder = 2
    object btnTSalir: TSpeedButton
      Left = 16
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Salir'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00330000000000
        03333377777777777F333301111111110333337F333333337F33330111111111
        0333337F333333337F333301111111110333337F333333337F33330111111111
        0333337F333333337F333301111111110333337F333333337F33330111111111
        0333337F3333333F7F333301111111B10333337F333333737F33330111111111
        0333337F333333337F333301111111110333337F33FFFFF37F3333011EEEEE11
        0333337F377777F37F3333011EEEEE110333337F37FFF7F37F3333011EEEEE11
        0333337F377777337F333301111111110333337F333333337F33330111111111
        0333337FFFFFFFFF7F3333000000000003333377777777777333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnTSalirClick
    end
    object btnTInsertar: TSpeedButton
      Left = 47
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Agregar una combinaci'#243'n'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnTInsertarClick
    end
    object btnTEliminar: TSpeedButton
      Left = 79
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Eliminar una combinaci'#243'n'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        300033FFFFFF3333377739999993333333333777777F3333333F399999933333
        3300377777733333337733333333333333003333333333333377333333333333
        3333333333333333333F333333333333330033333F33333333773333C3333333
        330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
        333333377F33333333FF3333C333333330003333733333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnTEliminarClick
    end
    object btnTEditar: TSpeedButton
      Left = 110
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Modificar una combinaci'#243'n'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnTEditarClick
    end
    object btnTFiltrar: TSpeedButton
      Left = 142
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Filtrar por una combinaci'#243'n'
      Flat = True
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333300003333333333333333333300003333333333333373333300003333
        333333333770333300003333333333337700033300003333333333377000F033
        0000333337777777000F0333000033337700000000F033330000333770788870
        0F03333300003377078FF887003333330000337078F888887033333300003370
        8F88888880333333000033708F888888703333330000337088888FF870333333
        0000333778888FF7773333330000333307888877033333330000333330777770
        3333333300003333337000733333333300003333333333333333333300003333
        33333333333333330000}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnTFiltrarClick
    end
    object btnTDesFiltrar: TSpeedButton
      Left = 174
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Listar todas las combinaciones'
      Flat = True
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333300003333333333333333333300003333333330033003300300003333
        3333300330033003000033303333333333333333000033000333333333333333
        000030F000333333333333330000330F000333333333333300003330F0000000
        73333333000033330F0078887033333300003333300788FF8703333300003333
        30788888F877333300003333308888888F80333300003333307888888F803333
        000033333078FF8888803333000033333777FF88887733330000333333077888
        8703333300003333333077777033333300003333333370007333333300003333
        33333333333333330000}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnTDesFiltrarClick
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 41
    Width = 717
    Height = 208
    Align = alClient
    DataSource = dsCombinacion
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    OnKeyPress = DBGrid1KeyPress
  end
  object ObtenerCombinacionLetrasValidas: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    IndexFieldNames = 'CombinacionValida'
    TableName = 'CombinacionLetrasValidas'
    Left = 250
    Top = 6
    object ObtenerCombinacionLetrasValidasCombinacionValida: TStringField
      Alignment = taCenter
      DisplayWidth = 4
      FieldName = 'CombinacionValida'
      FixedChar = True
      Size = 4
    end
    object ObtenerCombinacionLetrasValidasD7: TWordField
      FieldName = 'D7'
    end
    object ObtenerCombinacionLetrasValidasD6: TWordField
      FieldName = 'D6'
    end
    object ObtenerCombinacionLetrasValidasD5: TWordField
      FieldName = 'D5'
    end
    object ObtenerCombinacionLetrasValidasDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object ObtenerCombinacionLetrasValidasTipoCombinacion: TStringField
      FieldName = 'TipoCombinacion'
      FixedChar = True
      Size = 1
    end
    object ObtenerCombinacionLetrasValidasFechaActualizacion: TDateTimeField
      FieldName = 'FechaActualizacion'
    end
  end
  object VerificarLetraPatenteNueva: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Letra'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'Select Top 1  * '
      'FROM LetrasDigitoVerificador WITH (NOLOCK) '
      'Where Letra = :Letra')
    Left = 300
    Top = 8
  end
  object tmerResetBuffer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = tmerResetBufferTimer
    Left = 440
    Top = 8
  end
  object dsCombinacion: TDataSource
    DataSet = ObtenerCombinacionLetrasValidas
    OnDataChange = dsCombinacionDataChange
    Left = 250
    Top = 40
  end
end
