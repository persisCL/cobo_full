{-----------------------------------------------------------------------------
 File Name: FrmTiposDayPass.pas
 Author:    flamas
 Date Created: 21/02/2005
 Language: ES-AR
 Description: Carga las Tarifas y Categor�as de los Pases Diarios
 -----------------------------------------------------------------------------
 Revision 1
 Author       : Nelson Droguett Sierra
 Date         : 18/02/2009
 Description  : en la llamada a spActualizarTarifaDayPass se agrego el parametro
                CodigoCategoria, ya que se caia al agregar una tarifa de pase
                diario pues no enviaba el CodigoCategoria que es un campo
                obligatorio en la tabla de TarifasDayPass.
                Se cambia el ListBox lb_Categorias por un ComboBox en que obligue
                a seleccionar UNA categoria de las existentes.
                La relacion CodigoTarifaDayPass/Categorias ya no se usa como 1aN
                --------------------------------------------------------------}

unit FrmTiposDayPass;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  Util, UtilProc, OleCtrls, DmiCtrls, ComCtrls, PeaProcs, DMConnection,
  ADODB, variants, DPSControls, PeaTypes, Validate, DateEdit, VariantComboBox,
  FormAgregarCategoriaDayPass, DBCtrls, DateUtils;

ResourceString
    MSG_ERROR_SELECCIONAR_ITEM  = 'Debe seleccionar un item de la lista.';
    MSG_NUEVO_PREFIJO_QUERY     = 'Nuevo Prefijo: ';
    MSG_NUEVO_PREFIJO_CAPTION   = 'Agregar Prefijo';
    MSG_FILTRO_TARJETAS         = 'Debe indicar la tarjeta de cr�dito filtro.';
    MSG_ERROR                   = 'El prefijo NO pude ser mayor a 10 Caracteres.';
type
  TFormTiposDayPass = class(TForm)
    AbmToolbar1: TAbmToolbar;
    gbTarifasDayPass: TPanel;
    Panel2: TPanel;
    txt_CodigoMaterial: TEdit;
    Label2: TLabel;
    Notebook: TNotebook;
    spActualizarTarifaDayPass: TADOStoredProc;
    TarifasDayPass: TADOTable;
    spEliminarCategoriasDayPass: TADOStoredProc;
    Label1: TLabel;
    Label6: TLabel;
    Label15: TLabel;
    txt_codigo: TNumericEdit;
    alTarifasDayPass: TAbmList;
    deFechaActivacion: TDateEdit;
    spObtenerCategoriasTarifaDayPass: TADOStoredProc;
    neImporte: TNumericEdit;
    Label3: TLabel;
    spAgregarCategoriaDayPass: TADOStoredProc;
    spEliminarTarifaDayPass: TADOStoredProc;
    Label4: TLabel;
    cbTipos: TVariantComboBox;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    dsObtenerCategorias: TDataSource;
    spObtenerCategorias: TADOStoredProc;
    dblucbCategorias: TDBLookupComboBox;
    spObtenerCategoriasDescComentario: TStringField;
    spObtenerCategoriasCategoria: TWordField;
    spObtenerCategoriasDescripcion: TStringField;
    spObtenerCategoriasComentarios: TWideMemoField;
    spObtenerUltimaFechaActivacionTarifaDayPass: TADOStoredProc;
    spObtenerUltimaFechaActivacionTarifaDayPassUltimaFechaActivacion: TDateTimeField;
    lblUltimaFechaActivacion: TLabel;
    procedure BtnCancelarClick(Sender: TObject);
    procedure alTarifasDayPassDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure alTarifasDayPassEdit(Sender: TObject);
    procedure alTarifasDayPassRefresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure alTarifasDayPassDelete(Sender: TObject);
    procedure alTarifasDayPassInsert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure alTarifasDayPassClick(Sender: TObject);
    procedure spObtenerCategoriasCalcFields(DataSet: TDataSet);
    //procedure btnAgregarCategoriaClick(Sender: TObject);
    //procedure btnEliminarCategoriaClick(Sender: TObject);
    //procedure lb_CategoriasClick(Sender: TObject);
  private
	{ Private declarations }
    CodigoTarifaDayPass: integer;
    procedure HabilitarCamposEdicion(habilitar : Boolean);
	procedure Limpiar_Campos;
    procedure Volver_Campos;
    procedure RefrescarCamposRegistroActual;
	function  ValidateDayPass : boolean;
  public
	{ Public declarations }
    function Inicializar(MDIChild: Boolean = True): Boolean;
  end;

var
  FormTiposDayPass: TFormTiposDayPass;

implementation
{$R *.DFM}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 21/02/2005
  Description: Inicializa el Formulario
  Parameters: MDIChild: Boolean = True
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormTiposDayPass.Inicializar(MDIChild: Boolean = True): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;

  spObtenerCategorias.Close;
  spObtenerCategorias.Open;

	// Agrega los Tipos de Pase Diario al Combo
    cbTipos.Items.Add(TPD_NORMAL_DESC, TPD_NORMAL);
    cbTipos.Items.Add(TPD_TARDIO_DESC, TPD_TARDIO);

	if not OpenTables([TarifasDayPass]) then exit;
	Volver_Campos;
    alTarifasDayPass.Reload;
   	Notebook.PageIndex := 0;
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: HabilitarCamposEdicion
  Author:    flamas
  Date Created: 21/02/2005
  Description: Habilita la Edici�n
  Parameters: habilitar: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.HabilitarCamposEdicion(habilitar: Boolean);
begin
    alTarifasDayPass.Enabled        := not Habilitar;
    gbTarifasDayPass.Enabled	    := Habilitar;
    Notebook.PageIndex 		        := ord(habilitar);
    {Revision 1 / 11-02-2009 / Nelson Droguett S.
     -------------------------------------------
     Cambia el lb_Categorias por dblucbCategorias, que permite seleccionar solo UNA.
     ademas se eliminan los botones que permitian agregar y eliminar categorias
     de la lista.
     lb_Categorias.Enabled           := habilitar;
     btnAgregarCategoria.Enabled     := habilitar;
     btnEliminarCategoria.Enabled    := habilitar;

     18-02-2009 / Nelson Droguett S.
     La fecha de activacion solo se puede ingresar en caso de alta.
    }
    dblucbCategorias.Enabled        := Habilitar;
    if (alTarifasDayPass.Estado=alta) then begin
      deFechaActivacion.Enabled:=True;
      lblUltimaFechaActivacion.Caption:='Ultima Fecha Activaci�n : ' + DateToStr(spObtenerUltimaFechaActivacionTarifaDayPass.FieldByName('UltimaFechaActivacion').AsDateTime);
      lblUltimaFechaActivacion.Visible:=True;
    end else begin
      lblUltimaFechaActivacion.Visible:=False;
      deFechaActivacion.Enabled:=False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Volver_Campos
  Author:    flamas
  Date Created: 21/02/2005
  Description: Vuelve los Campos a su estado Original y deshabilita la Edicion
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.Volver_Campos ;
begin
	alTarifasDayPass.Estado := Normal;
  {Revision 1 / 11-02-2009 / Nelson Droguett S.
     -------------------------------------------
     Se vuelven a cargar los campos y refrescar el registro, ya
     que al cancelar una insercion el panel de datos quedaba con
     los campos en blanco y no con los datos del registro actual.
  }
  alTarifasDayPass.Reload;
  RefrescarCamposRegistroActual;

  HabilitarCamposEdicion(False);
  {Revision 1 / 11-02-2009 / Nelson Droguett S.
     -------------------------------------------
     Ya no debe limpiar la lista de categorias pues no se usa.
    //lb_Categorias.Clear;
  }
	Notebook.PageIndex := 0;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnCancelarClick
  Author:    flamas
  Date Created: 21/02/2005
  Description: Cancela la Edici�n
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

{-----------------------------------------------------------------------------
  Function Name: alTarifasDayPassDrawItem
  Author:    flamas
  Date Created: 21/02/2005
  Description: Dibuja el registro en el Grid (si esta seleccionado lo marca)
  Parameters: Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.alTarifasDayPassDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas  do begin
        FillRect(Rect);
            if odSelected in State then
                Sender.Canvas.Font.Color := clWindow
            else
                Sender.Canvas.Font.Color := clWindowText;
        With Tabla do begin
            TextOut(Cols[0] + 1, Rect.Top, Trim(Fieldbyname('CodigoMaterial').AsString));
            if (Fieldbyname('TipoDayPass').AsString = TPD_NORMAL) then TextOut(Cols[1], Rect.Top, TPD_NORMAL_DESC)
            else TextOut(Cols[1], Rect.Top, TPD_TARDIO_DESC);
            TextOut(Cols[2], Rect.Top, Fieldbyname('FechaActivacion').AsString);
            TextOut(Cols[3], Rect.Top, Format('$ %9.0n', [Fieldbyname('Importe').AsFloat/100]));
            TextOut(Cols[4], Rect.Top, Fieldbyname('CodigoCategoria').AsString);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: alTarifasDayPassEdit
  Author:    flamas
  Date Created: 21/02/2005
  Description: Habilita la Edici�n de un Registro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.alTarifasDayPassEdit(Sender: TObject);
begin
    {Revision 1 / 18-02-2009 / Nelson Droguett S.
     -------------------------------------------
     Cambio primero el estado a "modi", para no habilitar la fecha cuando
     habilite los controles en la edicion
    }
    alTarifasDayPass.Estado     := modi;
    HabilitarCamposEdicion(True);
	  txt_CodigoMaterial.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: alTarifasDayPassRefresh
  Author:    flamas
  Date Created: 21/02/2005
  Description: REfresca el Valor de los Campos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.alTarifasDayPassRefresh(Sender: TObject);
begin
	if alTarifasDayPass.Empty then Limpiar_Campos;
end;

{-----------------------------------------------------------------------------
  Function Name: Limpiar_Campos
  Author:    flamas
  Date Created: 21/02/2005
  Description: Limpia el contenido de los campos de Edici�n
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.Limpiar_Campos;
begin
    txt_codigo.Clear;
	txt_CodigoMaterial.clear;
    cbTipos.Value := Null;
    deFechaActivacion.Clear;
    neImporte.Clear;
    {Revision 1 / 11-02-2009 / Nelson Droguett S.
     -------------------------------------------
     Ya no debe limpiar la lista de categorias pues no se usa.
    //lb_Categorias.Clear;
    }
end;

{-----------------------------------------------------------------------------
  Function Name: AbmToolbar1Close
  Author:    flamas
  Date Created: 21/02/2005
  Description: Cierra el Formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

{-----------------------------------------------------------------------------
  Function Name: alTarifasDayPassDelete
  Author:    flamas
  Date Created: 21/02/2005
  Description: Elimina un registro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.alTarifasDayPassDelete(Sender: TObject);
resourcestring
	MSG_ELIMINATE_DAY_PASS_CAPTION		 	= 'Eliminar la Tarifa de Pase Diario';
	MSG_ELIMINATE_DAY_PASS_ERROR            = 'No se puede eliminar la tarifa porque hay datos que dependen de ella.';
	MSG_ELIMINATE_DAY_PASS_QUESTION         = '�Seguro desea eliminar esta Tarifa de Pase Diario?';
	MSG_ELIMINATE_DAY_PASS_QUESTION_CAPTION = 'Confirmaci�n...';
begin
	Screen.Cursor := crHourGlass;
    try
        If MsgBox(MSG_ELIMINATE_DAY_PASS_QUESTION, MSG_ELIMINATE_DAY_PASS_QUESTION_CAPTION, MB_YESNO) = IDYES then begin
            try
             	spEliminarTarifaDayPass.Parameters.ParamByName('@CodigoTarifaDayPass').Value := CodigoTarifaDayPass;
              spEliminarTarifaDayPass.ExecProc;
              {Revision 1 / 18-02-2009 / Nelson Droguett S.
               -------------------------------------------
               Se cierra el SP, para que vuelva a determinar la ultima fecha.
              }
              spObtenerUltimaFechaActivacionTarifaDayPass.Close;
            except
                On E: exception do begin
                    {Revision 1 / 19-02-2009 / Nelson Droguett S.
                     -------------------------------------------
                     Se modifico el mensaje de error, para que no muestre el detalle
                     tecnico del error en la base de datos.
                    }
                    MsgBox(MSG_ELIMINATE_DAY_PASS_ERROR,MSG_ELIMINATE_DAY_PASS_QUESTION_CAPTION,MB_OK);
                    //MsgBoxErr(MSG_ELIMINATE_DAY_PASS_ERROR, E.message, MSG_ELIMINATE_DAY_PASS_CAPTION, MB_ICONSTOP);
                end;
            end;
        end;
        Volver_Campos;
    finally
	    Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: alTarifasDayPassInsert
  Author:    flamas
  Date Created: 21/02/2005
  Description: Inserta un registro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.alTarifasDayPassInsert(Sender: TObject);
begin

	Limpiar_Campos;
  {Revision 1 / 18-02-2009 / Nelson Droguett S.
   -------------------------------------------
   Cambio el estado antes de la habilitacion de campos, para
   que solo en el estado 'alta' se habilite la fecha de activacion
  }
  alTarifasDayPass.Estado := alta;
  HabilitarCamposEdicion(True);
	txt_CodigoMaterial.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    flamas
  Date Created: 21/02/2005
  Description: Libera la memoria al cerrar el Formulario
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author:    flamas
  Date Created: 21/02/2005
  Description: Cierra el Formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.BtnSalirClick(Sender: TObject);
begin
     close;
end;

{-----------------------------------------------------------------------------
  Function Name: alTarifasDayPassClick
  Author:    flamas
  Date Created: 21/02/2005
  Description: Muestra los datos de la Tarifa Seleccionada
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.alTarifasDayPassClick(Sender: TObject);
begin
	if (TarifasDayPass.FieldByName('CodigoTarifaDayPass').AsInteger = CodigoTarifaDayPass) then Exit;

	TarifasDayPass.DisableControls;
    RefrescarCamposRegistroActual;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAgregarCategoriaClick
  Author:    flamas
  Date Created: 21/02/2005
  Description: Agrega una categor�a
  Parameters: Sender: TObject
  Return Value: None

-----------------------------------------------------------------------------}
  {Revision 1 / 11-02-2009 / Nelson Droguett S.
  -------------------------------------------
  Este procedimiento se elimino, ya que pertenecia a un boton que
  se quito de la interfaz de usuario
  }
{
procedure TFormTiposDayPass.btnAgregarCategoriaClick(Sender: TObject);
Var
	f 		   : TFrmAgregarCategoriaDayPass;
    sCategoria : string;
begin
	try

		Application.CreateForm(TFrmAgregarCategoriaDayPass, f);
        if f.Inicializar then begin
        	if (f.ShowModal = mrOK) then begin
            	sCategoria := PADR(f.spObtenerCategorias.FieldByName('Descripcion').AsString, 200, ' ') +
                				f.spObtenerCategorias.FieldByName('Categoria').AsString;
                if ( lb_Categorias.Items.IndexOf(sCategoria) = -1) then lb_Categorias.Items.Add(sCategoria);
            end;
        end;
    finally
    	f.Release;
	end;
end;
}



{-----------------------------------------------------------------------------
  Function Name: btnEliminarCategoriaClick
  Author:    flamas
  Date Created: 21/02/2005
  Description: Elimina una categor�a
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
  {Revision 1 / 11-02-2009 / Nelson Droguett S.
  -------------------------------------------
  Este procedimiento se elimino, ya que pertenecia a un boton que
  se quito de la interfaz de usuario
  }
{
procedure TFormTiposDayPass.btnEliminarCategoriaClick(Sender: TObject);
begin
	if lb_Categorias.itemindex < 0 then begin
		MsgBox( MSG_ERROR_SELECCIONAR_ITEM, Caption, MB_ICONSTOP);
	end else begin
		lb_Categorias.items.delete(lb_Categorias.itemindex);
	end;
end;
}

{-----------------------------------------------------------------------------
  Function Name: RefrescarCamposRegistroActual
  Author:    flamas
  Date Created: 21/02/2005
  Description: Muestra el Contenido del Registro Actual
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.RefrescarCamposRegistroActual;
resourcestring
	MSG_ERROR_LOADING_CATEGORIES = 'Error obteniendo Categor�as';
begin
  {Revision 1 / 18-02-2009 / Nelson Droguett S.
   -------------------------------------------
   Se desactiva el boton de edicion, para las tarifas que no son
   del "ultimo grupo" (es decir, los que sean iguales a la ultima fecha)
   Se deja invisible el recordatorio de la ultima fecha pues solo
   se ve en modo inserci�n o 'alta'.
  }
  if Not(spObtenerUltimaFechaActivacionTarifaDayPass.Active) then begin
    spObtenerUltimaFechaActivacionTarifaDayPass.Open;
  end;
  if (TarifasDayPass.FieldByName('FechaActivacion').AsDateTime < spObtenerUltimaFechaActivacionTarifaDayPass.FieldByName('UltimaFechaActivacion').AsDateTime) then
    AbmToolbar1.Habilitados := [btSalir,btBuscar,btAlta,btBaja]
  else
    AbmToolbar1.Habilitados := [btSalir,btBuscar,btAlta,btBaja,btModi];
  lblUltimaFechaActivacion.Visible:=False;


	With TarifasDayPass do begin
		txt_codigo.Value        := FieldByName('CodigoTarifaDayPass').AsInteger;
		txt_CodigoMaterial.text	:= Trim(FieldByName('CodigoMaterial').AsString);
		cbTipos.Value			      := FieldByName('TipoDayPass').AsString;
		deFechaActivacion.Date	:= FieldByName('FechaActivacion').AsDateTime;
		neImporte.ValueInt		  := FieldByName('Importe').AsVariant div 100;
    	CodigoTarifaDayPass   	:= FieldByName('CodigoTarifaDayPass').AsInteger;
	    {Revision 1 / 11-02-2009 / Nelson Droguett S.
	     -------------------------------------------
	     Este c�digo se elimino, pues llenaba el listbox de las N categorias de una
	     tarifa de pase diario. La relacion ya no es 1 a N, asi que este listbox se
	     cambio por un combo donde solo se puede seleccionar 1 categoria.
	     try
	       // Categorias
	       spObtenerCategoriasTarifaDayPass.Close;
	       spObtenerCategoriasTarifaDayPass.Parameters.ParamByName('@CodigoTarifaDayPass').Value := CodigoTarifaDayPass;
	       spObtenerCategoriasTarifaDayPass.Open;
	       lb_Categorias.Clear;
	       While not spObtenerCategoriasTarifaDayPass.Eof do begin
	         lb_Categorias.Items.Add(PADR(spObtenerCategoriasTarifaDayPass.FieldByName('DescCategoria').AsString, 200, ' ') +
	                               spObtenerCategoriasTarifaDayPass.FieldByName('CodigoCategoria').AsString);
	         spObtenerCategoriasTarifaDayPass.Next;
	       end;
	       spObtenerCategoriasTarifaDayPass.Close;
	     except
	           on e: exception do begin
	               MsgBoxErr(MSG_ERROR_LOADING_CATEGORIES, E.message, Caption, MB_ICONERROR);
	             end;
	     end;
	    }
	    dblucbCategorias.KeyValue:= FieldByName('CodigoCategoria').AsInteger;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:    flamas
  Date Created: 21/02/2005
  Description: Acepta los cambios realizados
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTiposDayPass.BtnAceptarClick(Sender: TObject);
ResourceString
	MSG_UPDATE_CAPTION		    	= 'Actualizar tarifa pase diario';
    MSG_UPDATE_ERROR		    	= 'No se pudieron actualizar los datos del pase diario';
    MSG_CATEGORY_UPDATE_ERROR		= 'No se pudieron actualizar las categor�as del pase diario';
var
    FCodigoTarifa : integer;
    TodoOk: Boolean;
    i: Integer;
begin
	if not ValidateDayPass then Exit;

	Screen.Cursor := crHourGlass;
    TodoOk := False;
    try
    	DMConnections.BaseCAC.BeginTrans;
        try
            if alTarifasDayPass.Estado = Alta then FCodigoTarifa := -1
            else FCodigoTarifa := TarifasDayPass.FieldByName('CodigoTarifaDayPass').AsInteger;

            with spActualizarTarifaDayPass.Parameters do begin
                ParamByName('@CodigoTarifaDayPass').Value	:= iif(FCodigoTarifa = -1, null, FCodigoTarifa);
                ParamByName('@CodigoMaterial').Value	  	:= txt_CodigoMaterial.Text;
                ParamByName('@TipoDayPass').Value	  		:= cbTipos.Value;
                ParamByName('@FechaActivacion').Value		:= deFechaActivacion.Date;
                ParamByName('@Importe').Value				:= neImporte.ValueInt * 100;
                {Revision 1 / 11-02-2009 / Nelson Droguett S.
                 -------------------------------------------
                 Se caia al agregar una tarifa de pase diario pues no enviaba
                 el codigo de categoria que es un campo obligatorio
                }
                ParamByName('@CodigoCategoria').Value				:= dblucbCategorias.KeyValue;
            end;
            spActualizarTarifaDayPass.ExecProc;
            FCodigoTarifa := spActualizarTarifaDayPass.Parameters.ParamByName('@CodigoTarifaDayPass').Value;

            if alTarifasDayPass.Estado = Modi then begin
                // Para grabar la lista de categorias, primero borro los anteriores.
                spEliminarCategoriasDayPass.Parameters.ParamByName('@CodigoTarifaDayPass').Value := FCodigoTarifa;
                spEliminarCategoriasDayPass.ExecProc;
            end;

            { Revision 1 / 11-02-2009 / Nelson Droguett S.
              -------------------------------------------
              Esta grabacion de las categorias de una tarifa de pase diario
              ya no debe recorrer un listbox y grabar varias categorias para
              una tarifa. Ahora graba solo una eleccion que se saca de un ComboBox
              for i := 0 to lb_Categorias.items.count -1 do begin
                // Escribir uno por uno.
                try
                	spAgregarCategoriaDayPass.Parameters.ParamByName('@CodigoTarifaDayPass').Value := FCodigoTarifa;
                  spAgregarCategoriaDayPass.Parameters.ParamByName('@CodigoCategoria').Value    := StrToInt(Trim(Copy(lb_Categorias.Items[i], 200, 20)));
					        spAgregarCategoriaDayPass.ExecProc;
                except
                    On E: Exception do begin
                		MsgBoxErr( MSG_CATEGORY_UPDATE_ERROR, e.message, MSG_UPDATE_CAPTION, MB_ICONSTOP);
                    end;
                 end;
              end;
            }
            try
            	spAgregarCategoriaDayPass.Parameters.ParamByName('@CodigoTarifaDayPass').Value  := FCodigoTarifa;
              spAgregarCategoriaDayPass.Parameters.ParamByName('@CodigoCategoria').Value      := dblucbCategorias.KeyValue;
					    spAgregarCategoriaDayPass.ExecProc;
            except
                On E: Exception do begin
            		MsgBoxErr( MSG_CATEGORY_UPDATE_ERROR, e.message, MSG_UPDATE_CAPTION, MB_ICONSTOP);
                end;
            end;

            {Revision 1 / 18-02-2009 / Nelson Droguett S.
             -------------------------------------------
             Se cierra el SP, para que vuelva a determinar la ultima fecha.
            }
            spObtenerUltimaFechaActivacionTarifaDayPass.Close;

        	TodoOk := True;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_UPDATE_ERROR, e.message, MSG_UPDATE_CAPTION, MB_ICONSTOP);
            end;
        end;
    finally
    	if TodoOk then begin
        	DMConnections.BaseCAC.CommitTrans;
            Volver_Campos;
            Screen.Cursor := crDefault;
        end else begin
	        DMConnections.BaseCAC.RollbackTrans;
            Screen.Cursor := crDefault;
		end;
        alTarifasDayPass.Reload;
        RefrescarCamposRegistroActual;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidateDayPass
  Author:    flamas
  Date Created: 23/02/2005
  Description: Valida el Pase Diario a Ingresar
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFormTiposDayPass.ValidateDayPass : boolean;
resourcestring
  MSG_VALIDATE_CAPTION                  = 'Validar datos del Pase Diario';
  MSG_VALIDATE_MATERIAL_CODE            = 'Debe ingresar un C�digo de Material';
  MSG_VALIDATE_MATERIAL_CODE_DUPLICATED = 'El C�digo de Material %s ya est� ingresado';
  MSG_VALIDATE_ACTIVATION_DATE          = 'Debe ingresar la Fecha de Activaci�n';
  MSG_VALIDATE_TYPE                     = 'Debe ingresar el Tipo de Pase Diario';
  MSG_VALIDATE_AMOUNT                   = 'Debe ingresar un importe';
  MSG_VALIDATE_NONEGATIVE               = 'El valor del importe no puede ser negativo';
  MSG_VALIDATE_LASINPUT_DATE            = 'La fecha debe ser mayor o igual a la ultima ingresada';

begin
	result := False;

    // Valida el C�digo de Material
   	if (Trim(txt_CodigoMaterial.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDATE_MATERIAL_CODE, MSG_VALIDATE_CAPTION, MB_ICONSTOP, txt_CodigoMaterial);
		Exit;
    end;

    // Valida el C�digo de Material Duplicado
    if (alTarifasDayPass.Estado = Alta) and
    	(QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ExisteCodigoMaterialDayPass(''%s'', ''%s'')', [cbTipos.Value, txt_CodigoMaterial.Text])) = 'True') then begin
		MsgBoxBalloon(Format(MSG_VALIDATE_MATERIAL_CODE_DUPLICATED,[txt_CodigoMaterial.Text]),
        				MSG_VALIDATE_CAPTION, MB_ICONSTOP, txt_CodigoMaterial);
		Exit;
    end;

    // Valida el Tipo de Pase Diario
   	if (cbTipos.Value = Null) or (cbTipos.Value = Unassigned) then begin
		MsgBoxBalloon(MSG_VALIDATE_TYPE, MSG_VALIDATE_CAPTION, MB_ICONSTOP, cbTipos);
		Exit;
    end;

    {Revision 1 / 18-02-2009 / Nelson Droguett S.
     -------------------------------------------
     la fecha de activacion solo se valida cuando es posible
     editarla, es decir, solo en el modo "alta".
    }

    if deFechaActivacion.Enabled then begin
      // Valida la Fecha de Activaci�n
      if (deFechaActivacion.Date = nulldate) then begin
      MsgBoxBalloon(MSG_VALIDATE_ACTIVATION_DATE, MSG_VALIDATE_CAPTION, MB_ICONSTOP, deFechaActivacion);
      Exit;
      end;

      // Valida la Fecha de Activaci�n
      if (deFechaActivacion.Date < spObtenerUltimaFechaActivacionTarifaDayPass.FieldByName('UltimaFechaActivacion').AsDateTime) then begin
      MsgBoxBalloon(MSG_VALIDATE_LASINPUT_DATE, MSG_VALIDATE_CAPTION, MB_ICONSTOP, deFechaActivacion);
      Exit;
      end;
    end;


    // Valida el Importe
   	if (Trim(neImporte.Text) = '') then begin
		MsgBoxBalloon(MSG_VALIDATE_AMOUNT, MSG_VALIDATE_CAPTION, MB_ICONSTOP, neImporte);
		Exit;                        
    end;

    // Valida el Importe
   	if (neImporte.Value < 0) then begin
		MsgBoxBalloon(MSG_VALIDATE_NONEGATIVE, MSG_VALIDATE_CAPTION, MB_ICONSTOP, neImporte);
		Exit;
    end;



	result := True;
end;

procedure TFormTiposDayPass.spObtenerCategoriasCalcFields(DataSet: TDataSet);
begin
  DataSet.FieldByName('DescComentario').AsString:= StrLeft(DataSet.FieldByName('Descripcion').ASString,30)+'  |  '+StrLeft(DataSet.FieldByName('Comentarios').ASString,70);
end;

end.
