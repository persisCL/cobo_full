unit ABMMensajes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask, ComCtrls, PeaProcs, ADODB, DPSControls,
  DBCtrls;

type
  TFormABMMensajes = class(TForm)
    AbmToolbar1: TAbmToolbar;
    dbl_Mensaje: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    Panel1: TPanel;
    Notebook: TNotebook;
    txt_titulo: TEdit;
    txt_CodigoMensaje: TEdit;
    qry_Mensajes: TADOTable;
    Label2: TLabel;
    txt_comentario: TEdit;
    Label3: TLabel;
    qry_MensajesCodigoMensaje: TWordField;
    qry_MensajesTituloMensaje: TStringField;
    qry_MensajesDescripcion: TMemoField;
    qry_MensajesComentario: TStringField;
    txt_descripcion: TMemo;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure dbl_MensajeClick(Sender: TObject);
    procedure dbl_MensajeDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dbl_MensajeEdit(Sender: TObject);
    procedure dbl_MensajeRefresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos();
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormABMMensajes: TFormABMMensajes;

implementation
resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Mensaje';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Mensaje';
    MSG_TITULO              = 'Debe especificar un titulo.';
    MSG_DESCRIPCION         = 'Debe especificar una descripcion.';
{$R *.DFM}

function TFormABMMensajes.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([qry_Mensajes]) then
		Result := False
	else begin
       	Notebook.PageIndex := 0;
		Result := True;
		dbl_Mensaje.Reload;
	end;
end;

procedure TFormABMMensajes.BtnCancelarClick(Sender: TObject);
begin
   	dbl_Mensaje.Estado     := Normal;
	dbl_Mensaje.Enabled    := True;
	dbl_Mensaje.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled := False;
end;

procedure TFormABMMensajes.dbl_MensajeClick(Sender: TObject);
begin
	 with qry_Mensajes do begin
          txt_CodigoMensaje.Text:= FieldByName('CodigoMensaje').AsString;
		  txt_titulo.Text 		:= FieldByName('TituloMensaje').AsString;
		  txt_descripcion.Text 	:= FieldByName('Descripcion').AsString;
          txt_comentario.text 	:= FieldByName('Comentario').AsString;
     end;
end;

procedure TFormABMMensajes.dbl_MensajeDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to tabla.FieldCount-2 do // el ultimo campo no lo listo
       		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormABMMensajes.dbl_MensajeEdit(Sender: TObject);
begin
	dbl_Mensaje.Enabled     := False;
    dbl_Mensaje.Estado      := modi;
	Notebook.PageIndex      := 1;
    groupb.Enabled          := True;

    txt_titulo.setFocus;
end;

procedure TFormABMMensajes.dbl_MensajeRefresh(Sender: TObject);
begin
	 if dbl_Mensaje.Empty then Limpiar_Campos();
end;

procedure TFormABMMensajes.Limpiar_Campos();
begin
	txt_descripcion.Clear;
	txt_CodigoMensaje.Clear;
    txt_comentario.Clear;
    txt_titulo.Clear;
end;

procedure TFormABMMensajes.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormABMMensajes.BtnAceptarClick(Sender: TObject);
begin

    if not (ValidateControls([txt_titulo,txt_descripcion],
        [trim(txt_titulo.Text)<>'',trim(txt_descripcion.Text)<>''],
        MSG_ACTUALIZAR_CAPTION,
        [MSG_TITULO,MSG_DESCRIPCION]) ) then exit;

    Screen.Cursor := crHourGlass;
    try
        With qry_Mensajes do begin
            Try
                Edit;
                FieldByName('Descripcion').AsString  := txt_descripcion.Text;
                FieldByName('TituloMensaje').AsString  := txt_titulo.Text;
                FieldByName('Comentario').AsString  := txt_comentario.Text;
                Post;
            except
                On E: EDataBaseError do begin
                    Cancel;
                    MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                end;
            end;
        end;
        groupb.Enabled      := False;
        dbl_Mensaje.Estado  := Normal;
        dbl_Mensaje.Enabled := True;
        Notebook.PageIndex  := 0;
        dbl_Mensaje.Reload;
        dbl_Mensaje.SetFocus;
    finally
    	Screen.Cursor := crDefault;
    end;
end;

procedure TFormABMMensajes.FormShow(Sender: TObject);
begin
   	dbl_Mensaje.Reload;
    dbl_Mensaje.SetFocus;
end;

procedure TFormABMMensajes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormABMMensajes.BtnSalirClick(Sender: TObject);
begin
     close;
end;

end.
