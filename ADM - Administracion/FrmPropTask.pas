unit FrmPropTask;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, WorkflowComp, DmiCtrls, DB, ADODB, Util, UtilDB,
  peaprocs, DPSControls;

type
  TFormPropTarea = class(TForm)
    Label1: TLabel;
	Edit1: TEdit;
    GroupBox1: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    Label2: TLabel;
    cb_Area: TComboBox;
    Label3: TLabel;
    NumericEdit1: TNumericEdit;
    Areas: TADOQuery;
    cb_TiposProcesos: TComboBox;
    Label4: TLabel;
    Button1: TDPSButton;
    Button2: TDPSButton;
  private
    function GetDescripcion: AnsiString;
	procedure SetDescripcion(const Value: AnsiString);
	function GetTaskType: TTaskType;
	procedure SetTaskType(const Value: TTaskType);
    function GetMaxDur: Integer;
    procedure SetMaxDur(const Value: Integer);
    function GetArea: Integer;
    procedure SetArea(const Value: Integer);
    function GetProceso: Integer;
    procedure SetProceso(const Value: Integer);

	{ Private declarations }
  public
	{ Public declarations }
	function Inicializa: Boolean;
	property Descripcion: AnsiString Read GetDescripcion Write SetDescripcion;
	property TaskType: TTaskType Read GetTaskType Write SetTaskType;
	property DuracionMaxima: Integer Read GetMaxDur Write SetMaxDur;
	property Area: Integer Read GetArea Write SetArea;
    property Proceso: integer Read GetProceso Write SetProceso;
  end;

var
  FormPropTarea: TFormPropTarea;

implementation

uses DMConnection;

{$R *.dfm}

{ TFormPropTarea }

function TFormPropTarea.GetArea: Integer;
begin
    Result := IVal(StrRight(cb_Area.Text, 10));
end;

function TFormPropTarea.GetDescripcion: AnsiString;
begin
	Result := Edit1.Text;
end;

function TFormPropTarea.GetMaxDur: Integer;
begin
	Result := Round(NumericEdit1.Value * 60);
end;

function TFormPropTarea.GetTaskType: TTaskType;
begin
	if RadioButton2.Checked then result := ttOptional
	else if RadioButton3.Checked then result := ttResult
	else Result := ttNormal;
end;

function TFormPropTarea.GetProceso: Integer;
begin
	Result := Ival(StrRight(cb_TiposProcesos.Text, 10));
end;

function TFormPropTarea.Inicializa: Boolean;
begin
	Result := OpenTables([Areas]);
	if not Result then Exit;
	cb_Area.Clear;
	While not Areas.Eof do begin
		cb_Area.Items.Add(Areas.FieldByName('Descripcion').AsString +
		  Space(200) + Areas.FieldByName('CodigoArea').AsString);
		Areas.Next;
	end;
	Areas.Close;
    CargarTiposProcesos(DmConnections.BaseCAC, cb_TiposProcesos, 0);
end;

procedure TFormPropTarea.SetArea(const Value: Integer);
Var
	i: Integer;
begin
	for i := 0 to cb_Area.Items.Count - 1 do begin
		if IVal(StrRight(cb_Area.Items[i], 10)) = Value then begin
			cb_Area.ItemIndex := i;
			Exit;
		end;
	end;
	cb_Area.ItemIndex := -1;
end;

procedure TFormPropTarea.SetDescripcion(const Value: AnsiString);
begin
	Edit1.Text := Value;
end;

procedure TFormPropTarea.SetMaxDur(const Value: Integer);
begin
	NumericEdit1.Value := Value / 60;
end;

procedure TFormPropTarea.SetTaskType(const Value: TTaskType);
begin
	Case Value of
		ttOptional: radiobutton2.Checked := true;
		ttResult: radiobutton3.Checked := true;
		else radiobutton1.Checked := true;
	end;
end;

procedure TFormPropTarea.SetProceso(const Value: Integer);
begin
	cb_TiposProcesos.ItemIndex := Value;
end;

end.
