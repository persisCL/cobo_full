{********************************** File Header ********************************
File Name   : ABMMaestroCalles.pas
Author      : dcalani
Date Created: 19/02/2004
Language    : ES-AR
Description : Administra el Alta, Baja y modificacion de datos de las calles
              y sus Tramos.
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Date		: 15-Ene-2004
Author		: Castro, Ra�l <rcastro@dpsautomation.com>
Description : En caso de ser inicializado con un c�digo de Regi�n definido, se
				deshabilita el combo correspondiente a la Regi�n
*******************************************************************************}

unit ABMMaestroCalles;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, Util, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, ADODB, BuscaTab,
  Variants, DMConnection, DPSControls, Peatypes, ListBoxEx, DBListEx,
  Provider, DBClient,RStrings,ABMAlias;

type

  TFormCalles = class(TForm)
    ATCalle: TAbmToolbar;
    pnl_BotonesGeneral: TPanel;
    Notebook: TNotebook;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    dsTramos: TDataSource;
    cdsTramos: TClientDataSet;
    dspTramos: TDataSetProvider;
    ObtenerCalles: TADOStoredProc;
    ActualizarCalle: TADOStoredProc;
    qry_ActualizarTramo: TADOQuery;
    qry_Actualizar: TADOQuery;
    ActualizarTramo: TADOStoredProc;
    EliminarCalle: TADOStoredProc;
    PageControl: TPageControl;
    tsGeneral: TTabSheet;
    tsTramos: TTabSheet;
    dblTramos: TDBListEx;
    Label2: TLabel;
    txt_descripcion: TEdit;
    btnInsertar: TDPSButton;
    btnEditar: TDPSButton;
    btnBorrar: TDPSButton;
    tsAlias: TTabSheet;
    dbl_Alias: TDBListEx;
    btn_InsertarAlia: TDPSButton;
    btn_EditarAlia: TDPSButton;
    btn_BorrarAlia: TDPSButton;
    Dbl_Calles: TAbmList;
    Panel1: TPanel;
    Label1: TLabel;
    cb_Region: TComboBox;
    ObtenerTramosCalle: TADOStoredProc;
    EliminarTramosCalle: TADOStoredProc;
    cbComuna: TComboBox;
    Label4: TLabel;
    cdsTramosCodigoSegmento: TIntegerField;
    cdsTramosCodigoPais: TStringField;
    cdsTramosCodigoRegion: TStringField;
    cdsTramosCodigoComuna: TStringField;
    cdsTramosCodigoCalle: TIntegerField;
    cdsTramosCAD: TStringField;
    cdsTramosNumeroDesde: TIntegerField;
    cdsTramosNumeroHasta: TIntegerField;
    cdsTramosCodigoPostalPar: TStringField;
    cdsTramosCodigoPostalImpar: TStringField;
    cdsTramosLatitud: TFloatField;
    cdsTramosLongitud: TFloatField;
    cdsTramosCodigoSegmentoInterfase: TIntegerField;
    cds_Alias: TClientDataSet;
    ds_Alias: TDataSource;
    ObtenerAliasCalles: TADOStoredProc;
    dsp_Alias: TDataSetProvider;
    cds_AliasCodigoPais: TStringField;
    cds_AliasCodigoRegion: TStringField;
    cds_AliasCodigoComuna: TStringField;
    cds_AliasCodigoCalle: TIntegerField;
    cds_AliasAlias: TStringField;
    ActualizarAliasCalles: TADOStoredProc;
    procedure Dbl_CallesDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure Dbl_CallesEdit(Sender: TObject);
    procedure Dbl_CallesRefresh(Sender: TObject);
    procedure ATCalleClose(Sender: TObject);
    procedure Dbl_CallesDelete(Sender: TObject);
    procedure Dbl_CallesInsert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    function Dbl_CallesProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure ALTramosDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    function ALTramosProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure BtnCancelarClick(Sender: TObject);
    procedure btnInsertarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnBorrarClick(Sender: TObject);
    procedure Dbl_CallesClick(Sender: TObject);
    procedure cb_RegionChange(Sender: TObject);
    procedure cdsTramosAfterOpen(DataSet: TDataSet);
    procedure dblTramosDblClick(Sender: TObject);
    procedure cbComunaChange(Sender: TObject);
    procedure dbl_AliasDblClick(Sender: TObject);
    procedure btn_InsertarAliaClick(Sender: TObject);
    procedure btn_EditarAliaClick(Sender: TObject);
    procedure cds_AliasAfterOpen(DataSet: TDataSet);
    procedure btn_BorrarAliaClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoPais: String;
    function DarRegion:AnsiString;
    function DarComuna: AnsiString;
    procedure HabilitarCamposEdicion(habilitar : Boolean);
	procedure Limpiar_Campos;
	procedure Volver_Campos;
    function ValidarNumerosyClaves(NumeroDesde,NumeroHasta:Integer;CodigoRegion,CodigoComuna:String;Exceptuar:Integer=-1):Boolean;
    function AliasCargado(Alia:String):Boolean;
  public
    { Public declarations }
    function Inicializa(MDIChild: Boolean;
                        CodigoPais: AnsiString = PAIS_CHILE;
                        CodigoRegion: AnsiString = REGION_SANTIAGO;
                        CodigoComuna: AnsiString = '';
                        CodigoCiudad: AnsiString = ''): Boolean;
  end;

var
  FormCalles: TFormCalles;

implementation

uses ABMCallesTramos;
resourcestring
    MSG_DELETE_QUESTION		  = '�Est� seguro de querer eliminar la Calle seleccionada?';
    MSG_DELETE_TRAMO_QUESTION = '�Est� seguro de querer eliminar el Tramo seleccionado?';
    MSG_DELETE_TRAMO_CAPTION  = 'Eliminar Tramo';
    MSG_DELETE_ERROR		  = 'No se puede eliminar la calle. Verifique que la calle no se este referenciada.';
    MSG_DELETE_CAPTION	      = 'Eliminar Calle';
    MSG_ACTUALIZAR_ERROR      = 'No se pudieron actualizar los datos de la Calle seleccionada.';
    MSG_ACTUALIZAR_TRAMO_ERROR 	= 'No se pudieron actualizar los datos del tramo seleccionada.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Calle';
    MSG_ACTUALIZAR_TRAMO_CAPTION	= 'Actualizar Tramo';
    MSG_INSERT_TRAMO_ERROR = 'No se pudo insertar el tramo.';
    MSG_DESCRIPCION         = 'Debe ingresar el descripci�n';
    MSG_TIPOCALLE           = 'Debe ingresar el tipo de calle';
    MSG_RANGOINCORRECTO     = 'El rango de la numeraci�n es incorrecto';
    MSG_CAPTION_TRAMOS= 'Edici�n de Tramos.';
    MSG_CLAVES_TRAMOS       = 'El pais o la region o la comuna o la ciudad ya estan asignadod a otro tramo.';
    MSG_SIN_TRAMO = 'Debe especificar por lo menos un tramo.';


{$R *.DFM}

function TFormCalles.Inicializa(MDIChild: Boolean;
                                CodigoPais: AnsiString = PAIS_CHILE;
                                CodigoRegion: AnsiString = REGION_SANTIAGO;
                                CodigoComuna: AnsiString = '';
                                CodigoCiudad: AnsiString = ''): Boolean;
Var
	S: TSize;
begin
    try
        if MDIChild then begin
            S := GetFormClientSize(Application.MainForm);
            SetBounds(0, 0, S.cx, S.cy);
        end else begin
            FormStyle := fsNormal;
            Visible := False;
        end;

        FCodigoPais:=CodigoPais;
        CargarRegiones(DMConnections.BaseCAC, cb_Region, CodigoPais, CodigoRegion);
        cb_Region.OnChange(cb_Region);
        cdsTramos.Close;
        cds_Alias.Close;
        cdsTramos.CreateDataSet;
        cds_Alias.CreateDataSet;
        Volver_Campos;
        PageControl.ActivePageIndex := 0;
      	Dbl_Calles.Reload;
        Result := True;
    except
        result := False;
    end;
end;

procedure TFormCalles.Dbl_CallesDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        TextOut(Cols[1] + 1, Rect.Top,FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormCalles.Dbl_CallesEdit(Sender: TObject);
begin
    HabilitarCamposEdicion(True);
    txt_Descripcion.setFocus;
end;

procedure TFormCalles.Dbl_CallesRefresh(Sender: TObject);
begin
	if Dbl_Calles.Empty then Limpiar_Campos;
end;


procedure TFormCalles.ATCalleClose(Sender: TObject);
begin
     close;
end;

procedure TFormCalles.Dbl_CallesDelete(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        if MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
            IDYES then begin
            try
                with EliminarCalle do begin
                    Parameters.ParamByName('@CodigoPais').Value := FCOdigoPais;
                    Parameters.ParamByName('@CodigoRegion').Value := DarRegion;
                    Parameters.ParamByName('@CodigoComuna').Value := DarComuna;
                    Parameters.ParamByName('@CodigoCalle').Value := dbl_calles.Table.FieldByName('CodigoCalle').AsInteger;
                    ExecProc;
                    close;
                end;
            Except
                On E: Exception do begin
                    MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
                end;
            end;
        end;
    finally
        Volver_Campos;
        Dbl_Calles.Reload;
        Dbl_Calles.Estado:=Normal;
        Dbl_Calles.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

procedure TFormCalles.Dbl_CallesInsert(Sender: TObject);
begin
    HabilitarCamposEdicion(True);
    Limpiar_Campos;
    ObtenerTramosCalle.close;
    txt_Descripcion.setFocus;
end;

procedure TFormCalles.BtnAceptarClick(Sender: TObject);
var
    CodigoCalle: Integer;
begin

    if not ValidateControls(
        [txt_descripcion],[
        trim(txt_descripcion.text) <> ''],
        MSG_ACTUALIZAR_CAPTION,
        [MSG_DESCRIPCION]) then exit;

 	Screen.Cursor := crHourGlass;
    Try
        Try
            DMConnections.BaseCAC.BeginTrans;
            with ActualizarCalle do begin // actualizo maestrocalles
                close;
                if Dbl_Calles.Estado=Alta then
                    Parameters.ParamByName('@CodigoCalle').Value := -1
                else
                    Parameters.ParamByName('@CodigoCalle').Value := ObtenerCalles.FieldbyName('CodigoCalle').Value;
                Parameters.ParamByName('@CodigoPais').Value   := PAIS_CHILE;
                Parameters.ParamByName('@CodigoRegion').Value := DarRegion;
                Parameters.ParamByName('@CodigoComuna').Value := DarComuna;
                Parameters.ParamByName('@Descripcion').Value  := trim(txt_descripcion.Text);
                Parameters.ParamByName('@CallePropia').Value  := 1;

                ExecProc;
                CodigoCalle:=Parameters.ParamByName('@CodigoCalle').Value;
            end;

            // Eliminamos los tramos de la calle
            with EliminarTramosCalle do begin
                Parameters.ParamByName('@CodigoPais').Value :=  FCodigoPais;
                Parameters.ParamByName('@CodigoRegion').Value := DarRegion;
                Parameters.ParamByName('@CodigoComuna').Value := DarComuna;
                Parameters.ParamByName('@CodigoCalle').Value := CodigoCalle;
                execProc;
            end;

            // MaetsroCallesTramos

            //elimino los cargados
            QueryExecute(DMConnections.BaseCAC,format('EXEC EliminarAliasCalle ''%s'',''%s'',''%s'',%d',[FCodigoPais,DarRegion,DarComuna,CodigoCalle]));

            // inserto los nuevos
            with cdsTramos do begin
                First;
                while not(eof) do begin
                    ActualizarTramo.Close;
                    with ActualizarTramo.Parameters do begin
                        ParamByName('@CodigoPais').Value:=FCodigoPais;
                        ParamByName('@CodigoRegion').Value:=DarRegion;
                        ParamByName('@CodigoComuna').Value:=DarComuna;
                        ParamByName('@CodigoCalle').Value:=CodigoCalle;
                        ParamByName('@CAD').Value:=cdsTramos.FieldByName('CAD').Value;
                        ParamByName('@NumeroDesde').Value:=cdsTramos.FieldByName('NumeroDesde').Value;
                        ParamByName('@NumeroHasta').Value:=cdsTramos.FieldByName('NumeroHasta').Value;
                        ParamByName('@CodigoPostalPar').Value:=cdsTramos.FieldByName('CodigoPostalPar').Value;
                        ParamByName('@CodigoPostalImpar').Value:=cdsTramos.FieldByName('CodigoPostalImpar').Value;
                        ParamByName('@Latitud').Value:=cdsTramos.FieldByName('Latitud').Value;
                        ParamByName('@Longitud').Value:=cdsTramos.FieldByName('Longitud').Value;
                    end;

                    ActualizarTramo.ExecProc;
                    next;
                end;
            end;
            //Alias
            with cds_Alias do begin
                First;
                while not(eof) do begin
                    ActualizarAliasCalles.Close;
                    with ActualizarAliasCalles.Parameters do begin
                        ParamByName('@CodigoPais').Value:=FCodigoPais;
                        ParamByName('@CodigoRegion').Value:=DarRegion;
                        ParamByName('@CodigoComuna').Value:=DarComuna;
                        ParamByName('@CodigoCalle').Value:=CodigoCalle;
                        ParamByName('@Alias').Value:=cds_Alias.FieldByName('Alias').Value;
                    end;

                    ActualizarAliasCalles.ExecProc;
                    next;
                end;

            end;

            DMConnections.BaseCAC.CommitTrans;

        except
                On E: Exception do begin
                    DMConnections.BaseCAC.RollbackTrans;
                    MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                end;
        end;
    finally
        HabilitarCamposEdicion(False); // deshabilitos los combos
        Dbl_Calles.Reload;
        Dbl_Calles.Estado:=Normal;
        Dbl_Calles.SetFocus;
        Screen.Cursor  := crDefault;
    end;
end;

procedure TFormCalles.FormShow(Sender: TObject);
begin
   	Dbl_Calles.Reload;
end;

procedure TFormCalles.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormCalles.BtnSalirClick(Sender: TObject);
begin
     close;
end;

function TFormCalles.Dbl_CallesProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	 Result := True;
end;

procedure TFormCalles.ALTramosDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);

        TextOut(Cols[0] + 1, Rect.Top,FieldByName('CodigoPais').AsString);
        TextOut(Cols[1] + 1, Rect.Top,FieldByName('NumeroDesde').AsString);
        TextOut(Cols[2] + 1, Rect.Top,FieldByName('NumeroHasta').AsString);
        TextOut(Cols[3] + 1, Rect.Top,FieldByName('Region').AsString);
        TextOut(Cols[4] + 1, Rect.Top,FieldByName('Comuna').AsString);
        TextOut(Cols[5] + 1, Rect.Top,FieldByName('Ciudad').AsString);

    end;
end;

function TFormCalles.ALTramosProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
    with Tabla do begin
        Texto:=fieldbyname('Descripcion').asstring;
    end;
	Result := True;
end;

procedure TFormCalles.BtnCancelarClick(Sender: TObject);
begin
    HabilitarCamposEdicion(False);
    Limpiar_Campos;
    Dbl_Calles.Estado:=Normal;
    Dbl_Calles.setfocus;
end;

procedure TFormCalles.btnInsertarClick(Sender: TObject);
var
    f:TFormTramosCalles;
    fin:Boolean;
begin
    Application.CreateForm(TFormTramosCalles,f);
    try
        fin:=false;
        cdsTramos.Last;
        if f.Inicializar(0, 0, '0', '', '',0,0) then begin
            while not(fin) and (f.showmodal=mrOk) do begin
                if ValidarNumerosyClaves(f.NumeroDesde, f.NumeroHasta,
                  cdsTramos.fieldbyname('CodigoRegion').AsString,
                  cdsTramos.fieldbyname('CodigoComuna').AsString) then begin
                    try
                        with cdsTramos do begin
                            Append;
                            FieldbyName('NumeroDesde').Asinteger:=f.NumeroDesde;
                            FieldbyName('NumeroHasta').Asinteger:=f.NumeroHasta;
                            FieldbyName('CAD').asString:=f.CAD;
                            FieldbyName('CodigoPostalPar').AsString:= f.CodigoPostalPar;
                            FieldbyName('CodigoPostalImPar').AsString:= f.CodigoPostalImpar;
                            if (f.Latitud=0) and (f.Longitud=0) then begin
                                FieldbyName('Latitud').Clear;
                                FieldbyName('Longitud').Clear;
                            end else begin
                                FieldbyName('Latitud').AsFloat:= f.Latitud;
                                FieldbyName('Longitud').AsFloat:= f.Longitud;
                            end;
                            post;
                        end;
                        fin:=True;
                    except
                        On E: Exception do begin
                            MsgBoxErr(MSG_INSERT_TRAMO_ERROR,e.message, MSG_ACTUALIZAR_TRAMO_CAPTION, MB_ICONSTOP);
                        end;
                    end;
                end;
            end;
        end;
    finally
        f.Release;
        dblTramos.SetFocus;
        cdsTramos.First;
    end;
end;

procedure TFormCalles.btnEditarClick(Sender: TObject);
var
    f:TFormTramosCalles;
    fin:Boolean;
begin
    Application.CreateForm(TFormTramosCalles,f);
    try
        fin:=false;
        if f.Inicializar(cdsTramos.fieldbyname('NumeroDesde').AsInteger,
          cdsTramos.fieldbyname('NumeroHasta').AsInteger,
          cdsTramos.fieldbyname('CAD').AsString,
          cdsTramos.fieldbyname('CodigoPostalPar').AsString,
          cdsTramos.fieldbyname('CodigoPostalImpar').AsString,
          cdsTramos.fieldbyname('Latitud').AsFloat,
          cdsTramos.fieldbyname('Longitud').AsFloat) then begin
            while not(fin) and (f.showmodal=mrOk) do begin
                if ValidarNumerosyClaves(f.NumeroDesde,f.NumeroHasta,
                  cdsTramos.fieldbyname('CodigoRegion').AsString,
                  cdsTramos.fieldbyname('CodigoComuna').AsString,
                  cdsTramos.RecNo) then begin
                    try
                        with cdsTramos do begin
                            edit;
                            FieldbyName('NumeroDesde').Asinteger:=f.NumeroDesde;
                            FieldbyName('NumeroHasta').Asinteger:=f.NumeroHasta;
                            FieldbyName('CAD').AsString := f.CAD;
                            FieldbyName('CodigoPostalPar').AsString := f.CodigoPostalPar;
                            FieldbyName('CodigoPostalImpar').AsString :=f.CodigoPostalImpar;
                            FieldbyName('Latitud').AsFloat:= f.Latitud;
                            FieldbyName('Longitud').AsFloat:= f.Longitud;
                            post;
                        end;
                        fin:=True;
                    except
                        On E: Exception do begin
                            MsgBoxErr(MSG_ACTUALIZAR_TRAMO_ERROR,e.message, MSG_ACTUALIZAR_TRAMO_CAPTION, MB_ICONSTOP);                        end;
                    end;
                end;
            end;
        end;
    finally
        f.Release;
        dblTramos.SetFocus;
    end;
end;

procedure TFormCalles.btnBorrarClick(Sender: TObject);
begin
    if MsgBox(MSG_DELETE_TRAMO_QUESTION, MSG_DELETE_TRAMO_CAPTION, MB_YESNO + MB_ICONQUESTION) =
        IDYES then begin
            cdsTramos.Delete;
            dblTramos.SetFocus;
    end;
end;

procedure TFormCalles.Dbl_CallesClick(Sender: TObject);
begin

    txt_descripcion.Text:=trim(ObtenerCalles.fieldbyname('Descripcion').AsString);
    with ObtenerTramosCalle do begin
        close;
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoPais').Value := FCodigoPais;
        Parameters.ParamByName('@CodigoRegion').Value := DarRegion;
        Parameters.ParamByName('@CodigoComuna').Value := DarComuna;
        Parameters.ParamByName('@CodigoCalle').Value := ObtenerCalles.FieldByName('CodigoCalle').Value;
    end;
    with ObtenerAliasCalles do begin
        close;
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoPais').Value:=FCodigoPais;
        Parameters.ParamByName('@CodigoRegion').value:=DarRegion;
        Parameters.ParamByName('@CodigoComuna').value:= DarComuna;
        Parameters.ParamByName('@CodigoCalle').value:= ObtenerCalles.FieldByName('CodigoCalle').Value;
    end;


    if not OpenTables([ObtenerTramosCalle,ObtenerTramosCalle]) then exit;
    cdsTramos.Data := dspTramos.Data;
    cds_Alias.Data := dsp_Alias.Data;
end;

procedure TFormCalles.cb_RegionChange(Sender: TObject);
begin
    CargarComunas(DMConnections.BaseCAC, cbComuna, FCodigoPais, DarRegion);
    cbComuna.OnChange(cbComuna);
end;

function TFormCalles.DarRegion:AnsiString;
begin
    result:=Trim(StrRight(cb_Region.Text, 3));
end;

function TFormCalles.DarComuna:AnsiString;
begin
    result:=Trim(StrRight(cbComuna.Text,3));
end;


{******************************** Function Header ******************************
Function Name: TFormCalles.HabilitarCamposEdicion
Author       : dcalani
Date Created : 02/03/2004
Description  : Habilita los combos para edicion o no.
Parameters   : habilitar : Boolean
Return Value : None
*******************************************************************************}
procedure TFormCalles.HabilitarCamposEdicion(habilitar : Boolean);
begin
    txt_descripcion.Enabled :=habilitar;
    btnInsertar.Enabled     :=habilitar;
    btnEditar.Enabled       :=(habilitar) and (cdsTramos.active) and (dblTramos.Enabled)and (cdsTramos.RecordCount>0);
    btnBorrar.Enabled       :=btnEditar.Enabled;
    btn_InsertarAlia.Enabled:=habilitar;
    btn_EditarAlia.Enabled  :=(habilitar) and (cds_Alias.active) and (dbl_Alias.Enabled) and (cds_Alias.RecordCount>0);
    btn_BorrarAlia.Enabled  :=btn_EditarAlia.Enabled;
    Dbl_Calles.Enabled      :=not(habilitar);
    cb_Region.Enabled       :=not(habilitar);
    cbComuna.Enabled        :=not(habilitar);
    ATCalle.Enabled         :=not(habilitar);
    Notebook.PageIndex      :=ord(habilitar);
    PageControl.ActivePageIndex := 0;
end;

procedure TFormCalles.Limpiar_Campos;
begin
    txt_descripcion.Clear;
    if cdsTramos.Active then cdsTramos.EmptyDataSet;
    if cds_Alias.Active then cds_Alias.EmptyDataSet;
end;

procedure TFormCalles.Volver_Campos;
begin
    HabilitarCamposEdicion(false);
    Limpiar_Campos;
    Dbl_Calles.SetFocus;
end;

{******************************** Function Header ******************************
Function Name: TFormCalles.ValidarNumerosyClaves
Author       : dcalani
Date Created : 02/03/2004
Description  : Valida que las calves de un tramo no esten ya cargada
                Tambiewn valida que la numeracion no se superponga con otra cargada
Parameters   : NumeroDesde,NumeroHasta:Integer;CodigoRegion,CodigoComuna,CodigoCiudad:String;Exceptuar:Integer=-1
Return Value : Boolean
*******************************************************************************}
function TFormCalles.ValidarNumerosyClaves(NumeroDesde,NumeroHasta:Integer;CodigoRegion,CodigoComuna:String;Exceptuar:Integer=-1):Boolean;
var
    book:TBookmark;
begin
    result:=true;
    with cdsTramos do begin
       book:=GetBookmark;
        First;
        while not (Eof) do begin
            if RecNo<>Exceptuar then begin
                if ((FieldByName('NumeroDesde').AsInteger<=NumeroDesde) and (NumeroDesde<=FieldByName('NumeroHasta').AsInteger)) or
                ((FieldByName('NumeroDesde').AsInteger<=NumeroDesde) and (NumeroHasta<=FieldByName('NumeroHasta').AsInteger)) or
                ((NumeroDesde<=FieldByName('NumeroDesde').AsInteger) and (FieldByName('NumeroHasta').AsInteger<=NumeroHasta)) or
                ((NumeroDesde<=FieldByName('NumeroDesde').AsInteger) and (NumeroHasta<=FieldByName('NumeroHasta').AsInteger) and (FieldByName('NumeroDesde').AsInteger<=NumeroHasta) )
                then begin
                    MsgBox(MSG_RANGOINCORRECTO, MSG_CAPTION_TRAMOS, MB_ICONSTOP);
                    result:=False;
                    GotoBookmark(book);
                    exit;
                end;
          (*
                if ((Trim(FieldByName('CodigoRegion').AsString)=CodigoRegion) and
                  (Trim(FieldByName('CodigoComuna').AsString)=CodigoComuna)) then begin
                    MsgBox(MSG_CLAVES_TRAMOS, MSG_CAPTION_TRAMOS, MB_ICONSTOP);
                    result:=False;
                    GotoBookmark(book);
                    exit;
                end;
          *)
            end;
            next;
        end;
        GotoBookmark(book);
    end;
end;

procedure TFormCalles.cdsTramosAfterOpen(DataSet: TDataSet);
begin
    btnEditar.Enabled := ((dbl_calles.Estado = Modi) or (dbl_calles.Estado = Alta)) and cdsTramos.active and (cdsTramos.RecordCount > 0);
    btnBorrar.Enabled := btnEditar.Enabled;
end;

procedure TFormCalles.dblTramosDblClick(Sender: TObject);
begin
    if btnEditar.Enabled then btnEditar.Click;
end;

procedure TFormCalles.cbComunaChange(Sender: TObject);
begin
    with ObtenerCalles do  begin
        close;
        Parameters.ParamByName('@CodigoPais').Value:=FCodigoPais;
        Parameters.ParamByName('@CodigoRegion').Value:=DarRegion;
        Parameters.ParamByName('@CodigoComuna').Value:=DarComuna;
    end;
    if not OpenTables([ObtenerCalles]) then exit;

    if cdsTramos.Active then begin
        cdsTramos.EmptyDataSet;
        cdsTramos.AfterOpen(cdsTramos); // para que actualiza los botones
    end;

    Dbl_Calles.Reload;
    Dbl_Calles.SetFocus;
end;

procedure TFormCalles.dbl_AliasDblClick(Sender: TObject);
begin
    if btn_EditarAlia.Enabled then btn_EditarAlia.Click;
end;


function TFormCalles.AliasCargado(Alia:String):Boolean;
var
    pos:TBookmark;
begin
    result:=False;
    with cds_Alias do begin
        pos:=GetBookmark;
        first;
        while not(eof) do begin
            if trim(FieldbyName('Alias').AsString)=Trim(Alia) then
                result:=True;
            next;
        end;
        GotoBookmark(pos);
    end;
end;
procedure TFormCalles.btn_InsertarAliaClick(Sender: TObject);
var
    f:TFormABMAlias;
begin
    Application.CreateForm(TFormABMAlias,f);
    if (f.Inicializa()) and (f.ShowModal=mrOk) then begin
        if not(AliasCargado(f.Alias)) then begin
            with cds_Alias do begin
                append;
                FieldbyName('Alias').Value:=f.Alias;
                Post;
            end;
        end else begin
            MsgBoxBalloon(format(MSG_ERROR_DUPLICADO_EL,[STR_ALIAS]),format(MSG_CAPTION_VALIDAR,[STR_ALIAS]),MB_ICONSTOP,dbl_Alias);
        end;
    end;
    f.Release;
end;

procedure TFormCalles.btn_EditarAliaClick(Sender: TObject);
var
    f:TFormABMAlias;
begin
    Application.CreateForm(TFormABMAlias,f);
    if (f.Inicializa(cds_Alias.FieldbyName('Alias').AsString)) and (f.ShowModal=mrOk) then begin
        if not(AliasCargado(f.Alias)) then begin
            with cds_Alias do begin
                Edit;
                FieldbyName('Alias').Value:=f.Alias;
                Post;
            end;
        end else begin
            MsgBoxBalloon(format(MSG_ERROR_DUPLICADO_EL,[STR_ALIAS]),format(MSG_CAPTION_VALIDAR,[STR_ALIAS]),MB_ICONSTOP,dbl_Alias);
        end;
    end;
    f.Release;
end;

procedure TFormCalles.cds_AliasAfterOpen(DataSet: TDataSet);
begin
    btn_EditarAlia.Enabled := ((dbl_calles.Estado = Modi) or (dbl_calles.Estado = Alta)) and cds_Alias.active and (cds_Alias.RecordCount > 0);
    btn_BorrarAlia.Enabled := btn_EditarAlia.Enabled;
end;

procedure TFormCalles.btn_BorrarAliaClick(Sender: TObject);
begin
    if MsgBox(format(MSG_QUESTION_BAJA,[STR_ALIAS]), format(MSG_CAPTION_ELIMINAR,[STR_ALIAS]), MB_YESNO + MB_ICONQUESTION) = IDYES then cds_Alias.Delete;
    dbl_Alias.SetFocus;
end;

end.



