{-----------------------------------------------------------------------------
 File Name: FrmDatosUsuario
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura

Revision : 2
Date: 13/05/2016
Author: JLO
Description:  Importado desde VNE y sacado todo el codigo en duro.

Etiqueta	:	20160623 MGO
Descripción	:	Se agrega configuración de alertas

-----------------------------------------------------------------------------}
unit FrmDatosUsuario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, StdCtrls, ExtCtrls, UtilProc, UtilDB, ADODB,
  PeaProcs, PeaTypes,Util, Variants, DMConnection, DPSControls, BuscaTab, DmiCtrls,
  Mask, DBCtrls, VariantComboBox, Grids, DBGrids, DBClient;	// 20160623 MGO

type
  TFormDatosUsuario = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    txt_codigousuario: TEdit;
    Bevel1: TBevel;
    lb_gruposusuario: TListBox;
    Label5: TLabel;
    lb_grupos: TListBox;
    Label6: TLabel;
    Bevel2: TBevel;
    RegistrarUsuarioSistema: TADOStoredProc;
    cb_Deshabilitado: TCheckBox;
    cb_CambioPass: TCheckBox;
    gb_CambiarContrasena: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    txt_password: TEdit;
    txt_password2: TEdit;
    cb_CambiarContrasena: TCheckBox;
    RegistrarUsuarioSistemaNuevo: TADOStoredProc;
    btn_agregar: TButton;
    btn_quitar: TButton;
    btn_ok: TButton;
    btn_cancel: TButton;
    Label7: TLabel;
    edtApellido: TEdit;
    edtApellidoMaterno: TEdit;
    edtNombre: TEdit;
    Label8: TLabel;
    edtEmail: TEdit;
    lbl1: TLabel;
    spUsuario: TADOStoredProc;
    spGrupos: TADOStoredProc;
    spUsuariosSistemas_1: TADOStoredProc;
    spDELGruposUsuarios: TADOStoredProc;
    spINSGruposUsuarios: TADOStoredProc;
    // INICIO : 20160623 MGO
    Bevel3: TBevel;
    Label9: TLabel;
    dbgrdAlertas: TDBGrid;
    dsAlertas: TDataSource;
    cdsAlertas: TClientDataSet;
    spADM_UsuariosSistemasEnvioAlarmas_SELECT: TADOStoredProc;
    spADM_UsuariosSistemasEnvioAlarmas_INSERT: TADOStoredProc;
    spADM_UsuariosSistemasEnvioAlarmas_DELETE: TADOStoredProc;
	// FIN : 20160623 MGO
	procedure btn_quitarClick(Sender: TObject);
    procedure btn_agregarClick(Sender: TObject);
    procedure btn_okClick(Sender: TObject);
    procedure txt_codigousuarioChange(Sender: TObject);
    procedure lb_gruposusuarioDblClick(Sender: TObject);
    procedure cb_CambiarContrasenaClick(Sender: TObject);
    // INICIO : 20160623 MGO
    procedure dbgrdAlertasCellClick(Column: TColumn);
    procedure dbgrdAlertasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgrdAlertasColEnter(Sender: TObject);
    procedure dbgrdAlertasColExit(Sender: TObject);
    function ObtenerConfiguracionAlertas(CodigoUsuario: String): Boolean;
    procedure dbgrdAlertasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
	// FIN : 20160623 MGO
  private
    { Private declarations }
    FCodigoUsuario: AnsiString;
	// INICIO : 20160623 MGO
    GridOriginalOptions: TDBGridOptions;
    CantidadRegistros: Integer;
    Posicion: TBookmark;
	// FIN : 20160623 MGO
  public
    { Public declarations }
    function Inicializa(CodigoUsuario: AnsiString): Boolean;
  end;

var
  FormDatosUsuario: TFormDatosUsuario;

implementation

resourcestring
  MSG_VALIDAR_CAPTION  = 'Datos de Usuario';
  MSG_ERROR_PASSWORD   = 'Debe ingresar una password.';
  MSG_ERROR_CONTRASENIA= 'La contraseña no coincide con la repetición.';
  MSG_ERROR_USUARIO    = 'El codigo de usuario indicado ya existe.';
  MSG_ERROR_INSERT_USUARIO    = 'Error cargando nuevo usuario.';
  MSG_ERROR_ACTUALIZANDO_USUARIO = 'Error actualizando datos del usuario.';
  MSG_USUARIO_TODOS_DELETE  = 'Este Grupo es obligatorio en todos los usuarios';

{$R *.DFM}

{ TFormDatosUsuario }

function TFormDatosUsuario.Inicializa(CodigoUsuario: AnsiString): Boolean;
begin
    //JLO 20160513 INICIO
    Result := False;
    FCodigoUsuario := CodigoUsuario;
    spGrupos.Parameters.ParamByName('@CodigoUsuario').Value := CodigoUsuario;
    spUsuario.Parameters.ParamByName('@CodigoUsuario').Value := CodigoUsuario;
    if not OpenTables([spGrupos, spUsuario]) then Exit;
    // Datos del Usuario
    if CodigoUsuario <> '' then begin
        txt_CodigoUsuario.Text    := FCodigoUsuario;
        txt_CodigoUsuario.Color   := clBtnFace;
        txt_CodigoUsuario.Enabled := False;
        //JLO 20160526 INICIO
        edtApellido.Text		  := TRIM(spUsuario.FieldByName('Apellido').AsString);
        edtApellidoMaterno.Text   := TRIM(spUsuario.FieldByName('ApellidoMaterno').AsString);
        edtNombre.Text			  := TRIM(spUsuario.FieldByName('Nombre').AsString);
        edtEmail.Text             := TRIM(spUsuario.FieldByName('Email').AsString);
        //JLO 20160526 TERMINO
        ActiveControl             := cb_CambiarContrasena;
        cb_Deshabilitado.Checked  := spUsuario.FieldByName('Deshabilitado').AsBoolean;
        cb_CambioPass.Checked     := spUsuario.FieldByName('PedirCambiarPassword').AsBoolean;
        cb_CambioPass.Enabled     := True;

    end else begin
        cb_CambiarContrasena.Checked := True;
        cb_CambiarContrasena.OnClick(Self);
        cb_CambiarContrasena.Enabled := False;
    end;

    // Grupos
    While not spGrupos.eof do begin
      if spGrupos.FieldByName('Existe').AsInteger = 1
        then lb_gruposusuario.Items.Add(spGrupos.FieldByName('CodigoGrupo').AsString)
        else begin
            if (trim(spGrupos.FieldByName('CodigoGrupo').AsString) <> USUARIO_TODOS) then
                lb_grupos.Items.Add(spGrupos.FieldByName('CodigoGrupo').AsString)
            else
                lb_gruposusuario.Items.Add(spGrupos.FieldByName('CodigoGrupo').AsString);
        end;

      spGrupos.Next;
    end;

    ObtenerConfiguracionAlertas(CodigoUsuario);     // 20160623 MGO     

    // Listo
    spGrupos.Close;
    spUsuario.Close;
    // JLO 20160513
    Result := True
end;

// INICIO : 20160623 MGO
function TFormDatosUsuario.ObtenerConfiguracionAlertas(CodigoUsuario: String): Boolean;
resourcestring
    MSG_ERROR_SELECT = 'Error al obtener la configuración de alertas';
var
    I: Integer;
    ErrorDescription,
    Descripcion: String;
begin
    try
        with spADM_UsuariosSistemasEnvioAlarmas_SELECT do begin
            Parameters.Refresh;
            Parameters.ParamByName('@Usuario').Value := CodigoUsuario;
            Open;

            if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin
                ErrorDescription := Parameters.ParamByName('@ErrorDescription').Value;
                Application.MessageBox(PChar(ErrorDescription), 'Error', MB_ICONERROR);
                Exit;
            end;

            CantidadRegistros := RecordCount;

            cdsAlertas.Active := False;
            for I := 0 to cdsAlertas.Fields.Count - 1 do
                cdsAlertas.Fields[i].ReadOnly := False;

            try
                cdsAlertas.EmptyDataSet;
            except
            end;

            cdsAlertas.CreateDataSet;
            cdsAlertas.Active   := True;
            cdsAlertas.ReadOnly := False;

            First;
            while not Eof do begin
                cdsAlertas.Append;

                Descripcion := FieldByName('Descripcion').Value;
                if (FieldByName('CodigoSistema').AsInteger > 0) and (FieldByName('CodigoSistemaModulo').AsInteger > 0) then
                    Descripcion := '    ' + Descripcion;

                cdsAlertas.FieldByName('CodigoSistema').Value := FieldByName('CodigoSistema').Value;
                cdsAlertas.FieldByName('CodigoSistemaModulo').Value := FieldByName('CodigoSistemaModulo').Value;
                cdsAlertas.FieldByName('EnvioEventos').Value := FieldByName('EnvioEventos').Value;
                cdsAlertas.FieldByName('EnvioAlertas').Value := FieldByName('EnvioAlertas').Value;
                cdsAlertas.FieldByName('Descripcion').Value := Descripcion;

                cdsAlertas.Post;
                Next;
            end;

            for I := 0 to cdsAlertas.Fields.Count - 1 do
                if cdsAlertas.Fields[I].DataType <> ftBoolean then
                    cdsAlertas.Fields[i].ReadOnly := True;

            Close;
            cdsAlertas.First;
        end;

        Result := True;
    except
        on E : Exception do begin
          MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
          Result := False;
        end;
    end;
end;
// FIN : 20160623 MGO

procedure TFormDatosUsuario.btn_quitarClick(Sender: TObject);
Var
  i: Integer;
begin

  i := 0;
  while i < lb_gruposusuario.Items.Count do
    if lb_gruposusuario.Selected[i] then begin
      if (Trim(lb_gruposusuario.items[i]) = USUARIO_TODOS) then begin
        MsgBox(MSG_USUARIO_TODOS_DELETE, self.Caption, MB_ICONSTOP);
        Inc(i);
        continue;
      end;
      lb_grupos.Items.Add(lb_gruposusuario.items[i]);
      lb_gruposusuario.Items.Delete(i);
	end
    else Inc(i)
end;

procedure TFormDatosUsuario.btn_agregarClick(Sender: TObject);
Var
  i: Integer;
begin
  i := 0;
  while i < lb_grupos.Items.Count do
    if lb_grupos.Selected[i] then begin
      lb_gruposusuario.Items.Add(lb_grupos.items[i]);
      lb_grupos.Items.Delete(i);
    end
    else Inc(i)
end;

{-----------------------------------------------------------------------------
  Function Name: btn_okClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormDatosUsuario.btn_okClick(Sender: TObject);
// Ini Sub-funcion ...........................
    function ValidarContrasena: Boolean;
    begin
    	Result := ValidateControls ([txt_password, txt_password2],
	    					   [Trim(txt_Password.Text) <> '',
		    				   trim(txt_password.text) = trim(txt_password2.text)],
			    			   MSG_VALIDAR_CAPTION,
				    		   [MSG_ERROR_PASSWORD, MSG_ERROR_CONTRASENIA]);
    end;
// Fin Sub-funcion ...........................
Var
	i: Integer;
    todoOk, finalizo : Boolean;
begin
    if cb_CambiarContrasena.Checked then begin
        if not ValidarContrasena then Exit;
    end;

    todoOk := False;

    try
        // Nuevo Usuario
    	if FCodigoUsuario = '' then begin
            //JLO 20160513 INICIO
            spUsuariosSistemas_1.Parameters.ParamByName('@CodigoUsuario').Value :=  Trim(txt_codigousuario.text);
            spUsuariosSistemas_1.ExecProc;

            if spUsuariosSistemas_1.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                MsgBox(MSG_ERROR_USUARIO, MSG_VALIDAR_CAPTION, MB_ICONSTOP);
	    		Exit;
            end;


            { //JLO
	    	if QueryGetValue(qry_Usuario.Connection,
   			  'SELECT TOP 1 * FROM UsuariosSistemas  WITH (NOLOCK) WHERE CodigoUsuario = ' +
    		  '''' + Trim(txt_codigousuario.text) + '''') <> ''	then begin
		    	MsgBox(MSG_ERROR_USUARIO, MSG_VALIDAR_CAPTION, MB_ICONSTOP);
	    		Exit;
    		end;
            }

            try
                if not DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.BeginTrans;

                with RegistrarUsuarioSistemaNuevo do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoUsuario').Value  		:= Trim(txt_CodigoUsuario.Text);
                    Parameters.ParamByName('@Apellido').Value  				:= Trim(edtApellido.Text);
                    Parameters.ParamByName('@ApellidoMaterno').Value  		:= Trim(edtApellidoMaterno.Text);
                    Parameters.ParamByName('@Nombre').Value  				:= Trim(edtNombre.Text);
                    Parameters.ParamByName('@Password').Value       		:= Trim(txt_password.text);
                    Parameters.ParamByName('@Deshabilitado').Value  		:= cb_deshabilitado.Checked;
                    Parameters.ParamByName('@PedirCambiarPassword').Value	:= cb_CambioPass.Checked;
                    Parameters.ParamByName('@UsuarioSistema').Value  		:= UsuarioSistema;
                    Parameters.ParamByName('@Email').Value                  := Trim(edtEmail.Text);
                    ExecProc;
                    Close;
                end;

            except
                on e: Exception do begin
                	if DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.RollbackTrans;
                    MsgBoxErr(MSG_ERROR_INSERT_USUARIO, e.Message, MSG_VALIDAR_CAPTION, MB_ICONSTOP);
                    Exit;
                end;
            end;
            FCodigoUsuario := txt_codigousuario.text;
        end
    	else begin
            try
                if not DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.BeginTrans;

                with RegistrarUsuarioSistema do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoUsuario').Value := Trim(FCodigoUsuario);
                    Parameters.ParamByName('@Apellido').Value  				:= Trim(edtApellido.Text);
                    Parameters.ParamByName('@ApellidoMaterno').Value  		:= Trim(edtApellidoMaterno.Text);
                    Parameters.ParamByName('@Nombre').Value  				:= Trim(edtNombre.Text);
                    if cb_CambiarContrasena.Checked then begin
                    	Parameters.ParamByName('@Password').Value := Trim(txt_password.text);
                    end;
                    Parameters.ParamByName('@Deshabilitado').Value 			:= cb_deshabilitado.Checked;
                    Parameters.ParamByName('@PedirCambiarPassword').Value	:= cb_CambioPass.Checked;
                    Parameters.ParamByName('@UsuarioSistema').Value  		:= UsuarioSistema;
                    Parameters.ParamByName('@Email').Value                  := Trim(edtEmail.Text);
                    ExecProc;
                    Close;
                end;

            except
                on E: Exception do begin
                	if DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.RollbackTrans;
                    MsgBoxErr(MSG_ERROR_ACTUALIZANDO_USUARIO, e.Message, MSG_VALIDAR_CAPTION, MB_ICONSTOP);
                    Exit;
                end;
            end;
        end;

    	// Grabamos los grupos
        try
            if not DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.BeginTrans;

            //JLO 20160513
            spDELGruposUsuarios.Parameters.ParamByName('@CodigoUsuario').Value :=  FCodigoUsuario;
            spDELGruposUsuarios.ExecProc;


            { //JLO 20160513
            QueryExecute (qry_Usuario.Connection, 'DELETE FROM GruposUsuario ' +
    		  'WHERE CodigoUsuario = ''' + FCodigoUsuario + '''');
            }
        except
            on E: Exception do begin
              	if DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.RollbackTrans;
                MsgBoxErr(MSG_ERROR_ACTUALIZANDO_USUARIO, e.Message, MSG_VALIDAR_CAPTION, MB_ICONSTOP);
    	    	Exit;
            end;
        end;

        Finalizo := true;
        if not DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.BeginTrans;

    	for I := 0 to lb_gruposusuario.items.count - 1 do begin
            try

                //JLO 20160513
                spINSGruposUsuarios.Parameters.ParamByName('@CodigoUsuario').Value :=  FCodigoUsuario;
                spINSGruposUsuarios.Parameters.ParamByName('@CodigoGrupo').Value :=  lb_gruposusuario.items[I];
                spINSGruposUsuarios.ExecProc;


                { //JLO 20160513
                QueryExecute (qry_Usuario.Connection, 'INSERT INTO GruposUsuario ' +
			  '(CodigoUsuario, CodigoGrupo) VALUES (''' + FCodigoUsuario +
			  ''', ''' + lb_gruposusuario.items[I] + ''')');
              }

            except
                on E: Exception do begin
	              	if DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.RollbackTrans;
                    MsgBoxErr(MSG_ERROR_ACTUALIZANDO_USUARIO, '', MSG_VALIDAR_CAPTION, MB_ICONSTOP);
                    finalizo := false;
                    break;
                end;
            end;
        end;
        if not Finalizo then Exit;

        // INICIO : 20160623 MGO
        // Limpia la configuración de alertas
        try
            if not DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.BeginTrans;

            spADM_UsuariosSistemasEnvioAlarmas_DELETE.Parameters.Refresh;
            spADM_UsuariosSistemasEnvioAlarmas_DELETE.Parameters.ParamByName('@Usuario').Value := FCodigoUsuario;
            spADM_UsuariosSistemasEnvioAlarmas_DELETE.Parameters.ParamByName('@ErrorDescription').Value := Null;
            spADM_UsuariosSistemasEnvioAlarmas_DELETE.ExecProc;
        except
            on E: Exception do begin
              	if DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.RollbackTrans;
                MsgBoxErr(MSG_ERROR_ACTUALIZANDO_USUARIO, e.Message, MSG_VALIDAR_CAPTION, MB_ICONSTOP);
    	    	Exit;
            end;
        end;
        
        // Guarda la configuración de alertas
        if not DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.BeginTrans;
        cdsAlertas.First;
        while not cdsAlertas.Eof do begin
        
            if not cdsAlertas.FieldByName('EnvioEventos').AsBoolean and not cdsAlertas.FieldByName('EnvioAlertas').AsBoolean then begin
                cdsAlertas.Next;
                Continue;
            end;

            try
                with spADM_UsuariosSistemasEnvioAlarmas_INSERT do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@Usuario').Value := FCodigoUsuario;
                    Parameters.ParamByName('@Sistema').Value := cdsAlertas.FieldByName('CodigoSistema').AsInteger;
                    Parameters.ParamByName('@SistemaModulo').Value := cdsAlertas.FieldByName('CodigoSistemaModulo').AsInteger;
                    Parameters.ParamByName('@EnvioEventos').Value := cdsAlertas.FieldByName('EnvioEventos').AsBoolean;
                    Parameters.ParamByName('@EnvioAlertas').Value := cdsAlertas.FieldByName('EnvioAlertas').AsBoolean;   
                    Parameters.ParamByName('@ErrorDescription').Value := Null;
                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin
                        MsgBoxErr(MSG_ERROR_ACTUALIZANDO_USUARIO, Parameters.ParamByName('@ErrorDescripcion').Value, MSG_VALIDAR_CAPTION, MB_ICONSTOP);
                        Finalizo := False;
                        Break;
                    end;
                end;
            except
                on E: Exception do begin
                    if DMConnections.BaseBO_Master.InTransaction then DMConnections.BaseBO_Master.RollbackTrans;
                    MsgBoxErr(MSG_ERROR_ACTUALIZANDO_USUARIO, e.Message, MSG_VALIDAR_CAPTION, MB_ICONSTOP);
                    Finalizo := False;
                    Break;
                end;
            end;
            cdsAlertas.Next;
        end;
        if not Finalizo then Exit;
        // FIN : 20160623 MGO

        ModalResult := mrOk;
        todoOk := true;
    finally
      	if DMConnections.BaseBO_Master.InTransaction then begin
	        if TodoOk then DMConnections.BaseBO_Master.CommitTrans
    	    else DMConnections.BaseBO_Master.RollbackTrans;
        end;
    end;
end;

procedure TFormDatosUsuario.txt_codigousuarioChange(Sender: TObject);
begin
	btn_ok.enabled := (Trim(txt_Codigousuario.text) <> '')
end;

procedure TFormDatosUsuario.lb_gruposusuarioDblClick(Sender: TObject);
begin
	btn_quitar.click;
end;

procedure TFormDatosUsuario.cb_CambiarContrasenaClick(Sender: TObject);
begin
    gb_CambiarContrasena.Enabled := cb_CambiarContrasena.Checked;
    if gb_CambiarContrasena.Enabled then begin
        txt_password.Color := clWindow;
        txt_password2.Color := clWindow;
    end else begin
        txt_password.Color := clBtnFace;
        txt_password2.Color := clBtnFace;
    end;
end;

// INICIO : 20160623 MGO
procedure TFormDatosUsuario.dbgrdAlertasCellClick(Column: TColumn);
begin
    if Column.Field.DataType = ftBoolean then begin
        Column.Grid.DataSource.DataSet.Edit;
        Column.Field.Value := not Column.Field.AsBoolean; 
        Column.Grid.DataSource.DataSet.Post;
    end;
end;

procedure TFormDatosUsuario.dbgrdAlertasColEnter(Sender: TObject);
begin
    if dbgrdAlertas.SelectedField.DataType = ftBoolean then begin
        GridOriginalOptions := dbgrdAlertas.Options;
        dbgrdAlertas.Options := dbgrdAlertas.Options - [dgEditing];
    end;
end;

procedure TFormDatosUsuario.dbgrdAlertasColExit(Sender: TObject);
begin
    if dbgrdAlertas.SelectedField.DataType = ftBoolean then
        dbgrdAlertas.Options := GridOriginalOptions;
end;

procedure TFormDatosUsuario.dbgrdAlertasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
   CtrlState: array[Boolean] of integer = (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED) ;
begin
    if Column.Field.DataType = ftBoolean then begin
        dbgrdAlertas.Canvas.FillRect(Rect);
        if VarIsNull(Column.Field.Value) then
            DrawFrameControl(dbgrdAlertas.Canvas.Handle,Rect, DFC_BUTTON, DFCS_BUTTONCHECK or DFCS_INACTIVE)
        else
            DrawFrameControl(dbgrdAlertas.Canvas.Handle,Rect, DFC_BUTTON, CtrlState[Column.Field.AsBoolean]);
    end;
end;

procedure TFormDatosUsuario.dbgrdAlertasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (dbgrdAlertas.SelectedField.DataType = ftBoolean) and (key = VK_SPACE) then begin
        dbgrdAlertas.DataSource.DataSet.Edit;
        dbgrdAlertas.SelectedField.Value:= not dbgrdAlertas.SelectedField.AsBoolean;
        dbgrdAlertas.DataSource.DataSet.Post;
    end;
end;
// FIN : 20160623 MGO

end.
