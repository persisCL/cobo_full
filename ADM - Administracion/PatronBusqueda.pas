unit PatronBusqueda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBClient;

type
  TfrmPatronBusqueda = class(TForm)
    edBuscar: TEdit;
    btnBuscar: TButton;
    lbl1: TLabel;
    procedure btnBuscarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPatronBusqueda: TfrmPatronBusqueda;

implementation

{$R *.dfm}

procedure TfrmPatronBusqueda.btnBuscarClick(Sender: TObject);

begin
  with TCLientDataSet(TForm(Owner).FindComponent('CDS')) do begin
     Filtered := False;
    if edBuscar.Text <> '' then begin
      Filter   := 'UPPER(Descripcion) LIKE ''%' + UpperCase(edBuscar.Text) + '%''';
      Filtered := True;
    end;
  end;

   Close;

  end;


procedure TfrmPatronBusqueda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
