unit CargarDemoras;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Db, DBTables, Grids, Util, UtilDB, UtilProc, ADODB, 
  DMConnection, DPSControls;

type
    TDemora = packed record
    	PCIni: Integer;
        PCFin: Integer;
        TipoH: String;
    end;

type
  TFormDemoras = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    btn_Cancel: TDPSButton;
    btn_Aceptar: TDPSButton;
    Grid: TStringGrid;
    ObtenerCombinacionesPuntosCobro: TADOStoredProc;
    GrabarDemoras: TADOStoredProc;
    Panel3: TPanel;
    Label3: TLabel;
    cb_concesionaria: TComboBox;
    Concesionarias: TADOTable;
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_CancelClick(Sender: TObject);
    procedure GridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure cb_concesionariaChange(Sender: TObject);
  private
    { Private declarations }
    FPorticos: Array[1..200] of TDemora;
    function CargarGrilla: Boolean;
  public
    { Public declarations }
    function Inicializa: Boolean;
  end;
resourcestring
    MSG_GRABAR = 'Grabar';
    GRABO_OK = 'Demoras grabadas con Exito!';
    GRABO_ERROR = 'Error al Grabar demoras: ';
    NO_HAY_DATOS = 'No hay datos para grabar para la concesionaria.';
    MSG_ERROR = 'Error';

var
  FormDemoras: TFormDemoras;

implementation

{$R *.DFM}

function TFormDemoras.Inicializa: Boolean;
begin
    Result := False;
    try
        Grid.Cells[0, 0] := '  Punto Cobro Inicial';
        Grid.Cells[1, 0] := '  Punto Cobro Final';
        Grid.Cells[2, 0] := '  Tipo Horario';
		Grid.Cells[3, 0] := ' Demora M�xima (min)';
        Grid.Row := 1;
		Grid.Col := 3;

		if not OpenTables([Concesionarias]) then Exit;
		cb_concesionaria.Clear;
		Concesionarias.First;
		while not Concesionarias.EOF do begin
            cb_concesionaria.Items.Add(
            PadR(Concesionarias.FieldByName('Descripcion').AsString, 200, ' ') +
            Istr(Concesionarias.FieldByName('CodigoConcesionaria').AsInteger, 10)
            );
			Concesionarias.Next;
		end;
		cb_concesionaria.ItemIndex := 0;
		Concesionarias.Close;

		cb_concesionaria.OnChange(nil);
		Result := True;
	except
		on e: Exception do begin
			MsgBoxErr(MSG_ERROR, e.message, MSG_ERROR, MB_ICONSTOP);
			Concesionarias.Close
		end;
	end;
end;

function TFormDemoras.CargarGrilla: Boolean;
var
	i, j, cant: Integer;
begin
    Result := True;
    i := 1;
    FillChar(FPorticos, SizeOf(FPorticos), 0);
	Grid.RowCount := ObtenerCombinacionesPuntosCobro.RecordCount + 1;
	ObtenerCombinacionesPuntosCobro.First;
    While not ObtenerCombinacionesPuntosCobro.Eof do begin
    	Grid.Cells[0, i] := PadL(ObtenerCombinacionesPuntosCobro.FieldByName('PuntoCobroInicial').AsString, 3, ' ') +
          ' - ' + Copy(ObtenerCombinacionesPuntosCobro.FieldByName('DescriInicial').AsString, 0, 30);
        Grid.Cells[1, i] := PadL(ObtenerCombinacionesPuntosCobro.FieldByName('PuntoCobroFinal').AsString, 3, ' ') +
          ' - ' + Copy(ObtenerCombinacionesPuntosCobro.FieldByName('DescriFinal').AsString, 0, 20);
        Grid.Cells[2, i] := ' ' + Copy(ObtenerCombinacionesPuntosCobro.FieldByName('DescriTipoHorario').AsString, 0, 20);
        Grid.Cells[3, i] := PadL(ObtenerCombinacionesPuntosCobro.FieldByName('MaxTiempo').AsString, 8, ' ');
        //
        FPorticos[i].PCIni := IVal(Grid.Cells[0, i]);
        FPorticos[i].PCFin := IVal(Grid.Cells[1, i]);
        FPorticos[i].TipoH := ObtenerCombinacionesPuntosCobro.FieldByName('TipoHorario').AsString;
        // Avanzar
        ObtenerCombinacionesPuntosCobro.Next;
        Inc(i);
        cant := i;
        for j := cant to cant + 1 do begin
      		Grid.Cells[0, i] := ' ';
            Grid.Cells[1, i] := ' ';
            Grid.Cells[2, i] := ' ' + Copy(ObtenerCombinacionesPuntosCobro.FieldByName('DescriTipoHorario').AsString, 0, 20);
            Grid.Cells[3, i] := PadL(ObtenerCombinacionesPuntosCobro.FieldByName('MaxTiempo').AsString, 8, ' ');
            //
            FPorticos[i].PCIni := FPorticos[i-1].PCIni;
        	FPorticos[i].PCFin := FPorticos[i-1].PCFin;
        	FPorticos[i].TipoH := ObtenerCombinacionesPuntosCobro.FieldByName('TipoHorario').AsString;
            // Avanzar
            Inc(i);
            ObtenerCombinacionesPuntosCobro.Next;
        end;
    end;
    ObtenerCombinacionesPuntosCobro.Close;
end;

procedure TFormDemoras.btn_AceptarClick(Sender: TObject);
var
	i: Integer;
begin
    // Grabar la Tabla de Demoras
    // FALTA: que genere solo las posibilidades en base a la secuencia de p�rticos.
    // POR AHORAAAAAAAAAAAAAAAAAA, CARGA TODO

    i := 1;
    try
		while FPorticos[i].PCIni > 0 do begin
            GrabarDemoras.Parameters.ParamByName('@CodigoConcesionaria').Value := Ival(StrRight(cb_Concesionaria.Text, 10));
			GrabarDemoras.Parameters.ParamByName('@PuntoCobroInicial').Value := FPorticos[i].PCIni;
			GrabarDemoras.Parameters.ParamByName('@PuntoCobroFinal').Value   := FPorticos[i].PCFin;
			GrabarDemoras.Parameters.ParamByName('@TipoHorario').Value       := FPorticos[i].TipoH;
			GrabarDemoras.Parameters.ParamByName('@MaxTiempo').Value		 := IVal(Grid.Cells[3, i]);;
			//
			GrabarDemoras.ExecProc;
			Inc(i);
		end;
        if i > 1 then
		    MsgBox(GRABO_OK, MSG_GRABAR, MB_ICONINFORMATION)
        else
            MsgBox(NO_HAY_DATOS, MSG_GRABAR, MB_ICONWARNING);

	except
		on e: Exception do begin
        	MsgBoxErr(GRABO_ERROR, e.message, MSG_ERROR, MB_ICONSTOP);
            GrabarDemoras.Close;
        end;
    end;
end;

procedure TFormDemoras.btn_CancelClick(Sender: TObject);
begin
    ObtenerCombinacionesPuntosCobro.Close;
	Close;
end;

procedure TFormDemoras.GridSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var
	i: Integer;
begin
    for i := 1 to Grid.RowCount do begin
        Grid.Cells[3, i] := PadL(Grid.Cells[3, i], 8, ' '); 
    end;
end;

procedure TFormDemoras.GridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
	if (Key = Ord('.')) or (Key = Ord(',')) then Key := 0;
end;

procedure TFormDemoras.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if (ActiveControl = Grid) then begin
		case Key of
			'0'..'9', #8:
				;
			#13:
				btn_aceptar.Click;
			#27:
				btn_cancel.Click;
			'.', ',':
				Key := #0;
			else
				Key := #0;
		end;
	end;
end;

procedure TFormDemoras.cb_concesionariaChange(Sender: TObject);
begin
	try
		ObtenerCombinacionesPuntosCobro.Parameters.ParamByName('@CodigoConcesionaria').Value :=
		  Ival(StrRight(cb_Concesionaria.Text, 10));
		if not OpenTables([ObtenerCombinacionesPuntosCobro]) then Exit;
		if not CargarGrilla then Raise Exception.Create('ObtenerCombinacionesPuntosCobro');
	except
		on e: Exception do begin
			MsgBox('Error al Cargar datos. ' + e.Message);
			ObtenerCombinacionesPuntosCobro.Close
		end;
	end;
end;

end.
