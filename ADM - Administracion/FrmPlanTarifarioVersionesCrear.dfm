object FormPlanTarifarioVersionesCrear: TFormPlanTarifarioVersionesCrear
  Left = 0
  Top = 0
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'Versi'#243'n Plan Tarifario - Crear'
  ClientHeight = 167
  ClientWidth = 399
  Color = clBtnFace
  Constraints.MaxHeight = 205
  Constraints.MaxWidth = 415
  Constraints.MinWidth = 415
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlConcesionaria: TPanel
    Left = 0
    Top = 0
    Width = 399
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object lblConsecionaria: TLabel
      Left = 56
      Top = 12
      Width = 71
      Height = 13
      Caption = 'Consecionaria:'
    end
    object cbbConcesionaria: TVariantComboBox
      Left = 133
      Top = 10
      Width = 210
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
  end
  object pnlGenerar: TPanel
    Left = 0
    Top = 33
    Width = 399
    Height = 59
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object lblCategoriaClases: TLabel
      Left = 47
      Top = 6
      Width = 80
      Height = 13
      Caption = 'Clase Categor'#237'a:'
    end
    object lblFechaActivacion: TLabel
      Left = 42
      Top = 34
      Width = 85
      Height = 13
      Caption = 'Fecha Activaci'#243'n:'
    end
    object cbbClaseCategorias: TVariantComboBox
      Left = 133
      Top = 4
      Width = 210
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbbClaseCategoriasChange
      Items = <>
    end
    object edtFechaActivacion: TDateEdit
      Left = 133
      Top = 31
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
  end
  object pnlPlantilla: TPanel
    Left = 0
    Top = 92
    Width = 399
    Height = 34
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object lblEsquema: TLabel
      Left = 87
      Top = 9
      Width = 40
      Height = 13
      Caption = 'Plantilla:'
    end
    object cbbEsquema: TVariantComboBox
      Left = 133
      Top = 6
      Width = 210
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 126
    Width = 399
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object btnGenerar: TButton
      Left = 238
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Guardar'
      TabOrder = 0
      OnClick = btnGenerarClick
    end
    object btnCancelar: TButton
      Left = 319
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
end
