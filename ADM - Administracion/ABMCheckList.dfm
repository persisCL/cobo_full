object FrmCheckList: TFrmCheckList
  Left = 179
  Top = 149
  Caption = 'Mantenimiento de Lista de Chequeo'
  ClientHeight = 339
  ClientWidth = 528
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 528
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object dblChecklist: TAbmList
    Left = 0
    Top = 33
    Width = 528
    Height = 188
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'134'#0'Codigo                               '
      
        #0'223'#0'Descripci'#243'n                                                ' +
        '     ')
    HScrollBar = True
    RefreshTime = 100
    Table = tblChecklist
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dblChecklistClick
    OnDrawItem = dblChecklistDrawItem
    OnRefresh = dblChecklistRefresh
    OnInsert = dblChecklistInsert
    OnDelete = dblChecklistDelete
    OnEdit = dblChecklistEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 221
    Width = 528
    Height = 79
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    DesignSize = (
      528
      79)
    object Label15: TLabel
      Left = 13
      Top = 14
      Width = 48
      Height = 13
      Caption = '&C'#243'digo: '
      FocusControl = txtCodigo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 13
      Top = 44
      Width = 56
      Height = 13
      Caption = '&Descripci'#243'n'
      FocusControl = txtdescripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtCodigo: TEdit
      Left = 96
      Top = 10
      Width = 29
      Height = 21
      Color = 16444382
      Enabled = False
      MaxLength = 3
      ReadOnly = True
      TabOrder = 0
    end
    object txtdescripcion: TEdit
      Left = 96
      Top = 38
      Width = 415
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      MaxLength = 100
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 300
    Width = 528
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Panel1: TPanel
      Left = 206
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 125
        Top = 0
        Width = 197
        Height = 39
        Align = alRight
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object BtnSalir: TDPSButton
            Left = 111
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TDPSButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TDPSButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
    end
  end
  object tblChecklist: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'MaestroCheckList'
    Left = 418
    Top = 86
    object tblChecklistCodigoCheckList: TIntegerField
      FieldName = 'CodigoCheckList'
    end
    object tblChecklistDescripcion: TStringField
      FieldName = 'Descripcion'
      FixedChar = True
      Size = 100
    end
  end
  object qryMaxCheckList: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      '  ISNULL(MAX(CodigoChecklist),0) AS CodigoChecklist'
      'FROM'
      '  MaestroCheckList WITH (NOLOCK) ')
    Left = 383
    Top = 87
  end
  object qryVerificarCheckList: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoCheckList'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 *'
      'FROM ConvenioCheckList  WITH (NOLOCK) '
      'WHERE ConvenioCheckList.CodigoCheckList = :CodigoCheckList ')
    Left = 344
    Top = 88
  end
end
