object frmABMClientesNQ: TfrmABMClientesNQ
  Left = 215
  Top = 166
  Width = 766
  Height = 570
  Caption = 'Mantenimiento de ClientesNQ'
  Color = clBtnFace
  Constraints.MaxHeight = 570
  Constraints.MaxWidth = 770
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupB: TPanel
    Left = 0
    Top = 264
    Width = 758
    Height = 233
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object lblCodigo: TLabel
      Left = 9
      Top = 11
      Width = 44
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCodigoPostal: TLabel
      Left = 511
      Top = 197
      Width = 25
      Height = 13
      Caption = 'C.P:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNumero: TLabel
      Left = 511
      Top = 175
      Width = 25
      Height = 13
      Caption = 'Nro:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCalle: TLabel
      Left = 9
      Top = 176
      Width = 33
      Height = 13
      Caption = 'Calle:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRegion: TLabel
      Left = 9
      Top = 150
      Width = 45
      Height = 13
      Caption = 'Regi'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDetalle: TLabel
      Left = 9
      Top = 197
      Width = 45
      Height = 13
      Caption = 'Detalle:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblPersoneria: TLabel
      Left = 9
      Top = 45
      Width = 67
      Height = 13
      Caption = 'Personer'#237'a:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDocumento: TLabel
      Left = 9
      Top = 70
      Width = 31
      Height = 13
      Caption = 'RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRazonSocial: TLabel
      Left = 372
      Top = 70
      Width = 80
      Height = 13
      Caption = 'Raz'#243'n Social:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblApellidoMaterno: TLabel
      Left = 372
      Top = 94
      Width = 100
      Height = 13
      Caption = 'Apellido Materno:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblApellido: TLabel
      Left = 9
      Top = 94
      Width = 50
      Height = 13
      Caption = 'Apellido:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 9
      Top = 120
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblSexo: TLabel
      Left = 372
      Top = 120
      Width = 33
      Height = 13
      Caption = 'Sexo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblComuna: TLabel
      Left = 372
      Top = 150
      Width = 50
      Height = 13
      Caption = 'Comuna:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtCodigo: TNumericEdit
      Left = 88
      Top = 8
      Width = 105
      Height = 21
      Color = 16444382
      Enabled = False
      TabOrder = 0
      Decimals = 0
    end
    object txtCodigoPostal: TEdit
      Left = 563
      Top = 193
      Width = 76
      Height = 21
      Hint = 'C'#243'digo Postal'
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 13
      Text = '1234567890'
    end
    object txtNumero: TEdit
      Left = 563
      Top = 170
      Width = 130
      Height = 21
      Hint = 'N'#250'mero'
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
    end
    object cbRegion: TComboBox
      Left = 59
      Top = 146
      Width = 293
      Height = 21
      Hint = 'Regi'#243'n'
      Style = csDropDownList
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnChange = cbRegionChange
    end
    object txtDetalle: TEdit
      Left = 59
      Top = 193
      Width = 444
      Height = 21
      Hint = 'Detalle'
      CharCase = ecUpperCase
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 100
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 12
    end
    object cbPersoneria: TComboBox
      Left = 88
      Top = 41
      Width = 117
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbPersoneriaChange
    end
    object txtDocumento: TEdit
      Left = 88
      Top = 66
      Width = 117
      Height = 21
      Hint = 'N'#250'mero de documento'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 9
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object txtRazonSocial: TEdit
      Left = 473
      Top = 66
      Width = 277
      Height = 21
      Hint = 'Raz'#243'n Social'
      CharCase = ecUpperCase
      Color = 16444382
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
    object txtApellidoMaterno: TEdit
      Left = 473
      Top = 90
      Width = 278
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 30
      TabOrder = 5
    end
    object txtApellido: TEdit
      Left = 88
      Top = 90
      Width = 278
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 30
      TabOrder = 4
    end
    object txtNombre: TEdit
      Left = 88
      Top = 116
      Width = 278
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 30
      TabOrder = 6
    end
    object cbSexo: TComboBox
      Left = 473
      Top = 116
      Width = 117
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 7
    end
    object cbComuna: TComboBox
      Left = 473
      Top = 146
      Width = 251
      Height = 21
      Hint = 'Comuna'
      Style = csDropDownList
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
      OnChange = cbComunaChange
    end
    object txtCalle: TEdit
      Left = 59
      Top = 170
      Width = 444
      Height = 21
      Hint = 'Detalle'
      CharCase = ecUpperCase
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 100
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 497
    Width = 758
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 561
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = Salir
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 758
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = Salir
  end
  object lbClientesNQ: TAbmList
    Left = 0
    Top = 33
    Width = 758
    Height = 231
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'74'#0'C'#243'digo           '
      
        #0'204'#0'Apellido                                                   ' +
        '  '
      #0'183'#0'Nombre                                              ')
    HScrollBar = True
    RefreshTime = 100
    Table = tblClientesNQ
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = lbClientesNQClick
    OnProcess = lbClientesNQProcess
    OnDrawItem = lbClientesNQDrawItem
    OnRefresh = lbClientesNQRefresh
    OnInsert = lbClientesNQInsert
    OnDelete = lbClientesNQDelete
    OnEdit = lbClientesNQEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object tblClientesNQ: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'ClientesNQ'
    Left = 364
    Top = 81
  end
  object spActualizarClientesNQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarClienteNQ;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoClienteNQ'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Calle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DetalleDomicilio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end>
    Left = 364
    Top = 136
  end
  object dsClientesNQ: TDataSource
    DataSet = tblClientesNQ
    Left = 462
    Top = 80
  end
  object EliminarClienteNQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarClienteNQ;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoClienteNQ'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Resultado'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 464
    Top = 136
  end
end
