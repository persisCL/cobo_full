object FormRegistroDeOperacionesFiltrar: TFormRegistroDeOperacionesFiltrar
  Left = 303
  Top = 218
  BorderStyle = bsDialog
  Caption = 'Filtrar'
  ClientHeight = 336
  ClientWidth = 373
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnAceptar: TButton
    Left = 206
    Top = 304
    Width = 75
    Height = 25
    Hint = 'Aceptar'
    Caption = '&Aceptar'
    Default = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 286
    Top = 304
    Width = 75
    Height = 25
    Hint = 'Cancelar'
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
  end
  object pnlFiltrar: TPanel
    Left = 4
    Top = 7
    Width = 365
    Height = 290
    BevelOuter = bvLowered
    TabOrder = 0
    object labCategoria: TLabel
      Left = 16
      Top = 16
      Width = 50
      Height = 13
      Caption = 'Categor'#237'a:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object labAccion: TLabel
      Left = 16
      Top = 96
      Width = 36
      Height = 13
      Caption = 'Acci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object labModulo: TLabel
      Left = 16
      Top = 56
      Width = 38
      Height = 13
      Caption = 'M'#243'dulo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object labSeveridad: TLabel
      Left = 16
      Top = 176
      Width = 51
      Height = 13
      Caption = 'Severidad:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object labUsuario: TLabel
      Left = 16
      Top = 216
      Width = 39
      Height = 13
      Caption = 'Usuario:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object labResultado: TLabel
      Left = 16
      Top = 256
      Width = 51
      Height = 13
      Caption = 'Resultado:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object labFechaDesde: TLabel
      Left = 16
      Top = 136
      Width = 67
      Height = 13
      Caption = 'Fecha Desde:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object labFechaHasta: TLabel
      Left = 208
      Top = 136
      Width = 31
      Height = 13
      Caption = 'Hasta:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object cbModulos: TVariantComboBox
      Left = 104
      Top = 56
      Width = 249
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
    object cbCategorias: TVariantComboBox
      Left = 104
      Top = 16
      Width = 249
      Height = 19
      Style = vcsOwnerDrawVariable
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbCategoriasChange
      Items = <>
    end
    object cbAcciones: TVariantComboBox
      Left = 104
      Top = 96
      Width = 249
      Height = 19
      Style = vcsOwnerDrawVariable
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
    object cbSeveridades: TVariantComboBox
      Left = 104
      Top = 176
      Width = 249
      Height = 19
      Style = vcsOwnerDrawVariable
      ItemHeight = 13
      TabOrder = 5
      Items = <
        item
          Caption = 'Todos'
          Value = 0
        end
        item
          Caption = 'Baja'
          Value = 1
        end
        item
          Caption = 'Normal'
          Value = 2
        end
        item
          Caption = 'Media'
          Value = 3
        end
        item
          Caption = 'Alta'
          Value = 4
        end
        item
          Caption = 'Extrema'
          Value = 5
        end>
    end
    object cbUsuarios: TVariantComboBox
      Left = 104
      Top = 216
      Width = 249
      Height = 19
      Style = vcsOwnerDrawVariable
      ItemHeight = 13
      TabOrder = 6
      Items = <>
    end
    object cbResultados: TVariantComboBox
      Left = 104
      Top = 256
      Width = 249
      Height = 19
      Style = vcsOwnerDrawVariable
      ItemHeight = 13
      TabOrder = 7
      Items = <
        item
          Caption = 'Todos'
          Value = 0
        end
        item
          Caption = 'Operaci'#243'n Fallida'
          Value = 1
        end
        item
          Caption = 'Operaci'#243'n Exitosa'
          Value = 2
        end>
    end
    object DateEditFechaDesde: TDateEdit
      Left = 104
      Top = 136
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 3
      Date = -693594
    end
    object DateEditFechaHasta: TDateEdit
      Left = 264
      Top = 136
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 4
      Date = -693594
    end
  end
  object btnRestaurar: TButton
    Left = 14
    Top = 304
    Width = 75
    Height = 25
    Hint = 'Restaurar'
    Caption = '&Restaurar'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = btnRestaurarClick
  end
  object ObtenerCategoriasModulos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCategoriasModulos;1'
    Parameters = <>
    Left = 8
    Top = 304
  end
  object ObtenerModulos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerModulos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@CodigoCategoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 40
    Top = 304
  end
  object ObtenerAcciones: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ObtenerAcciones;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 304
  end
  object ObtenerUsuariosSistemas: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ObtenerUsuariosSistemas;1'
    Parameters = <>
    Left = 104
    Top = 304
  end
end
