unit ABMCMovimientos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  ADODB, DPSControls;

type
  TFormCMovimientos = class(TForm)
    AbmToolbar1: TAbmToolbar;
    Lista: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    Panel1: TPanel;
    Notebook: TNotebook;
    Descripcion: TEdit;
    movimientos: TADOTable;
    CodigoConcepto: TNumericEdit;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure ListaClick(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure ListaEdit(Sender: TObject);
    procedure ListaRefresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDelete(Sender: TObject);
    procedure ListaInsert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos();
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormCMovimientos: TFormCMovimientos;

implementation
resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar este Concepto de Movimiento?';
    MSG_DELETE_ERROR		= 'No se puede eliminar este Concepto de Movimiento porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Concepto de Movimiento';
    MSG_ACTUALIZAR_ERROR	= 'No se pudo actualizar el Concepto de Movimiento.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Concepto de Movimiento';
    MSG_CONCEPTO_MOVIMIENTO	= 'Concepto de Movimiento';
	MSG_YES					= 'Si';

{$R *.DFM}

function TFormCMovimientos.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Notebook.PageIndex := 0;
	if not OpenTables([Movimientos]) then
		Result := False
	else begin
		Result := True;
		Lista.Reload;
	end;
end;

procedure TFormCMovimientos.BtnCancelarClick(Sender: TObject);
begin
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
    CodigoConcepto.Color := Descripcion.Color;
end;

procedure TFormCMovimientos.ListaClick(Sender: TObject);
begin
	with Movimientos do begin
		codigoConcepto.Value	:= FieldByName('CodigoConcepto').AsInteger;
		Descripcion.text 	    := Trim(FieldByName('Descripcion').AsString);
        {
		if FieldByName('Interno').AsBoolean then begin
			Lista.Access := Lista.Access - [accModi];
			AbmToolbar1.Habilitados := AbmToolbar1.Habilitados - [btModi];
		end else begin
			Lista.Access := Lista.Access + [accModi];
			AbmToolbar1.Habilitados := AbmToolbar1.Habilitados + [btModi];
		end;
        }
	end;
end;

procedure TFormCMovimientos.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 2 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);

        if tabla.FieldbyName('interno').asBoolean then begin
            //font.Color := clRed;
     		TextOut(Cols[2], Rect.Top, MSG_YES);
           end
        else
           begin
            //font.Color := clblack;
     		TextOut(Cols[2], Rect.Top, 'No');
           end;
        sender.Canvas.font.Color := clblack;        
	end;
end;

procedure TFormCMovimientos.ListaEdit(Sender: TObject);
begin
	Lista.Enabled    := False;
	Lista.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;

    CodigoConcepto.Color := clBtnFace;
    CodigoConcepto.ReadOnly := true;
    Descripcion.SetFocus;
    {
    if (Movimientos.FieldByName('Interno').AsBoolean) then
    begin
        CodigoConcepto.Color := clBtnFace;
        CodigoConcepto.ReadOnly := true;
        Descripcion.SetFocus;
    end
    else
    begin
        CodigoConcepto.Color := Descripcion.Color;
        CodigoConcepto.ReadOnly := false;
        codigoConcepto.setFocus;
    end;
    }
end;

procedure TFormCMovimientos.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos();
end;

procedure TFormCMovimientos.Limpiar_Campos();
begin
	CodigoConcepto.Clear;
	Descripcion.Clear;
end;

procedure TFormCMovimientos.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormCMovimientos.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			Movimientos.Delete;
		Except
			On E: Exception do begin
				Movimientos.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		Lista.Reload;
	end;
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormCMovimientos.ListaInsert(Sender: TObject);
begin
    groupb.Enabled     	:= True;
	Limpiar_Campos;
	Lista.Enabled    	:= False;
	Notebook.PageIndex	:= 1;
    CodigoConcepto.Color := Descripcion.Color;
    CodigoConcepto.ReadOnly := false;
	CodigoConcepto.SetFocus;
end;

procedure TFormCMovimientos.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_FALTA_CODIGO = 'Debe ingresar el c�digo';
    MSG_FALTA_DESCRIPCION = 'Debe ingresar la descripci�n';
    MSG_VALIDAR_CODIGO = 'El c�digo de concepto no puede ser menor a 100 (cien)';

begin
    if not ValidateControls(
        [CodigoConcepto, Descripcion],
        [Trim(CodigoConcepto.text) <> '',
        Trim(Descripcion.text) <> ''],
        MSG_CONCEPTO_MOVIMIENTO,
        [MSG_FALTA_CODIGO, MSG_FALTA_DESCRIPCION]) then begin
        CodigoConcepto.Color := Descripcion.Color;
        exit;
    end;

    if Lista.Estado = Alta then begin
        if Trunc(CodigoConcepto.Value) < 100 then begin
            MsgBox( MSG_VALIDAR_CODIGO, MSG_CONCEPTO_MOVIMIENTO, MB_ICONSTOP);
            CodigoConcepto.SetFocus;
            Exit;
        end;
    end;

 	Screen.Cursor := crHourGlass;
	With movimientos do begin
		Try
			if Lista.Estado = Alta then begin
                Append;
                //Los que se dan de alta no son interno
                FieldByName('Interno').AsBoolean  := False;
                FieldByName('EsAjuste').AsBoolean  := False;
            end
            else begin
                Edit;
                // En la modificaci�n no se altera el valor de Interno y EsAjuste
            end;
			FieldByName('CodigoConcepto').text := CodigoConcepto.Text;
			FieldByName('Descripcion').AsString  := Descripcion.text;
            Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
    groupb.Enabled     := False;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	Notebook.PageIndex := 0;
	Lista.Reload;
	Lista.SetFocus;
    CodigoConcepto.Color := Descripcion.Color;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormCMovimientos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormCMovimientos.BtnSalirClick(Sender: TObject);
begin
     close;
end;

end.
