object FormVehiculos: TFormVehiculos
  Left = 131
  Top = 132
  Width = 720
  Height = 490
  Caption = 'Mantenimiento de Veh'#237'culos'
  Color = clBtnFace
  Constraints.MinHeight = 420
  Constraints.MinWidth = 720
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 712
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 712
    Height = 281
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'152'#0'Marca                                      '
      #0'142'#0'Modelo                                 '
      #0'48'#0'A'#241'o       '
      #0'67'#0'Largo (cm)   '
      #0'71'#0'Ancho (cm)   '
      #0'61'#0'Alto (cm)    '
      #0'58'#0'Tolerancia')
    HScrollBar = True
    RefreshTime = 100
    Table = Vehiculos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 314
    Width = 712
    Height = 103
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label20: TLabel
      Left = 11
      Top = 48
      Width = 46
      Height = 13
      Caption = 'Modelo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 11
      Top = 21
      Width = 40
      Height = 13
      Caption = 'Marca:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 322
      Top = 73
      Width = 65
      Height = 13
      Caption = '&Tolerancia:'
      FocusControl = MedidaTolerancia
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 11
      Top = 73
      Width = 27
      Height = 13
      Caption = '&A'#241'o:'
      FocusControl = anio
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblAnioHasta: TLabel
      Left = 181
      Top = 73
      Width = 62
      Height = 13
      Caption = '&A'#241'o hasta:'
      FocusControl = AnioHasta
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object cb_modelo: TComboBox
      Left = 64
      Top = 42
      Width = 246
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
    end
    object GroupDimensiones: TGroupBox
      Left = 318
      Top = 11
      Width = 382
      Height = 52
      Caption = 'Dimensiones (en cent'#237'metros)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      object Label1: TLabel
        Left = 31
        Top = 25
        Width = 37
        Height = 13
        Caption = '&Largo:'
        FocusControl = largo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 149
        Top = 25
        Width = 41
        Height = 13
        Caption = '&Ancho:'
        FocusControl = ancho
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 277
        Top = 25
        Width = 27
        Height = 13
        Caption = '&Alto:'
        FocusControl = alto
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object largo: TNumericEdit
        Left = 70
        Top = 19
        Width = 65
        Height = 21
        Color = 16444382
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnExit = largoExit
        Decimals = 0
      end
      object ancho: TNumericEdit
        Left = 191
        Top = 19
        Width = 65
        Height = 21
        Color = 16444382
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnExit = anchoExit
        Decimals = 0
      end
      object alto: TNumericEdit
        Left = 306
        Top = 19
        Width = 65
        Height = 21
        Color = 16444382
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnExit = altoExit
        Decimals = 0
      end
    end
    object MedidaTolerancia: TNumericEdit
      Left = 388
      Top = 67
      Width = 66
      Height = 21
      Color = 16444382
      MaxLength = 6
      TabOrder = 5
      Decimals = 0
    end
    object CB_unidadTolerancia: TComboBox
      Left = 457
      Top = 67
      Width = 119
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 6
      Items.Strings = (
        'Ascendente'
        'Descendente')
    end
    object anio: TNumericEdit
      Left = 64
      Top = 67
      Width = 59
      Height = 21
      Color = 16444382
      MaxLength = 4
      TabOrder = 2
      OnExit = anioExit
      Decimals = 0
    end
    object cb_marca: TVariantComboBox
      Left = 64
      Top = 16
      Width = 246
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      OnChange = cb_marcaChange
      Items = <>
    end
    object AnioHasta: TNumericEdit
      Left = 248
      Top = 67
      Width = 59
      Height = 21
      Color = 16444382
      MaxLength = 4
      TabOrder = 3
      Visible = False
      OnExit = AnioHastaExit
      Decimals = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 417
    Width = 712
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 515
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object BuscaTablaVehiculos: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    HelpContext = 0
    Dataset = Vehiculos
    OnProcess = BuscaTablaVehiculosProcess
    Left = 212
    Top = 96
  end
  object Vehiculos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      '    V.*,'#9
      '    VMarca.Descripcion as DescMarca,'
      '    VModelo.Descripcion as DescModelo'
      'FROM  Vehiculos V,'
      'VehiculosMarcas VMarca,'
      'VehiculosModelos VModelo'
      'WHERE V.Marca = VMarca.CodigoMarca'
      '  and V.Marca = VModelo.CodigoMarca'
      '  and V.Modelo = VModelo.CodigoModelo')
    Left = 182
    Top = 96
  end
  object dsVehiculos: TDataSource
    Left = 152
    Top = 96
  end
  object ActualizarVehiculos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarVehiculos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Marca'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Modelo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Anio'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Largo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 18
        Value = Null
      end
      item
        Name = '@Ancho'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 18
        Value = Null
      end
      item
        Name = '@Alto'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 18
        Value = Null
      end
      item
        Name = '@UnidadTolerancia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@MedidaTolerancia'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end>
    Left = 152
    Top = 126
  end
  object EliminarVehiculo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarVehiculo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Marca'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Modelo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Anio'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 182
    Top = 126
  end
end
