unit ABMTSMCs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList,
  ToolWin, ComCtrls, UtilProc, Util, DBClient, DBCtrls, Mask, Provider, StrUtils,
  XMLIntf, XMLDoc, UtilDB, PeaProcs, VariantComboBox;                                                    // TASK_152_MGO_20170314

                   
resourcestring
	MSG_ERROR_UPDATE = 'Error Actualizando Registro';
 	MSG_ERROR_INSERT = 'Error Agregando Registro';
  	MSG_ERROR_DELETE = 'Error Eliminando Registro';
   	MSG_ERROR_SELECT = 'Error Consultando Registros';
    HINT_SALIR       = 'Salir del ABM';
    HINT_AGREGAR     = 'Agregar TSMC';
    HINT_ELIMINAR    = 'Eliminar TSMC';
    HINT_EDITAR      = 'Editar TSMC';
    ANSW_ELIMINAR    = 'Est� seguro de eliminar este TSMC? - ';
    ANSW_SALIR       = 'Confirma que desea salir?';

    PARAMS_IGNORE    = '@UsuarioCreacion, @FechaHoraCreacion, @UsuarioModificacion, @FechaHoraModificacion, @DomainID, @TSMCID, @ID_CategoriasClases, @CategoriasClasesDesc';     // TASK_152_MGO_20170314
    FIELDS_TINYINT   = 'TSMCID, NroLCUTRecuperacion';
    FIELDS_SMALLNT   = 'DomainID, CodigoVersionPBL';

    PROCEDURE_SELECT = 'ADM_TSMCs_SELECT';
    PROCEDURE_INSERT = 'ADM_TSMCs_INSERT';
    PROCEDURE_UPDATE = 'ADM_TSMCs_UPDATE';
    PROCEDURE_DELETE = 'ADM_TSMCs_DELETE';

    CAPTION_FORM     = 'ABM de TSMCs';

type
  TfrmABMTSMCs = class(TForm)
    procSelect: TADOStoredProc;
    dsGrid: TDataSource;
    Grid: TDBGrid;
    pnlSuperior: TPanel;
    pnlControles: TPanel;
    pnlInferior: TPanel;
    Botonera: TToolBar;
    Imagenes: TImageList;
    btnSalir: TToolButton;
    ImageList1: TImageList;
    ToolButton2: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    ToolButton6: TToolButton;
    btnImprimir: TToolButton;
    ToolButton8: TToolButton;
    btnBuscar: TToolButton;
    btnGuardar: TButton;
    btnCancelar: TButton;
    procInsert: TADOStoredProc;
    procDelete: TADOStoredProc;
    procUpdate: TADOStoredProc;
    ADO: TADOConnection;
    qryDomain: TADOQuery;
    pg01: TPageControl;
    tabConexion: TTabSheet;
    tabGeneral: TTabSheet;
    cbDomain: TDBLookupComboBox;
    edDescripcion: TDBEdit;
    edTSMCID: TDBEdit;
    edIP: TDBEdit;
    dbPort1: TDBEdit;
    edPort2: TDBEdit;
    edPort3: TDBEdit;
    dsDomain: TDataSource;
    edFTP: TDBEdit;
    edFTPUser: TDBEdit;
    edFTPPassword: TDBEdit;
    edFTPPort: TDBEdit;
    dspTSMC: TDataSetProvider;
    procSelectDomainID: TSmallintField;
    procSelectTSMCID: TWordField;
    procSelectDescripcion: TStringField;
    procSelectDireccionIP: TStringField;
    procSelectPort1: TIntegerField;
    procSelectPort2: TIntegerField;
    procSelectPort3: TIntegerField;
    procSelectUserID: TStringField;
    procSelectFTPServer: TStringField;
    procSelectFTPUser: TStringField;
    procSelectFTPPassword: TStringField;
    procSelectFTPPort: TIntegerField;
    procSelectImagePath: TStringField;
    procSelectNroLCUTRecuperacion: TWordField;
    procSelectActionListCargados: TIntegerField;
    procSelectUsuarioCreacion: TStringField;
    procSelectFechaHoraCreacion: TDateTimeField;
    procSelectUsuarioModificacion: TStringField;
    procSelectFechaHoraModificacion: TDateTimeField;
    procSelectCodigoVersionPBL: TSmallintField;
    qryPBL: TADOQuery;
    dsPBL: TDataSource;
    cbPBL: TDBLookupComboBox;
    lbl01: TLabel;
    lbl02: TLabel;
    lbl03: TLabel;
    lbl04: TLabel;
    lbl05: TLabel;
    lbl06: TLabel;
    lbl07: TLabel;
    lbl08: TLabel;
    lbl09: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl14: TLabel;
    strngfldSelectCategoriasClasesDesc: TStringField;
    procSelectID_CategoriasClases: TIntegerField;
    lbl1: TLabel;
    qryCategoriasClases: TADOQuery;
    vcbCategoriasClases: TVariantComboBox;
    cds: TClientDataSet;
    procedure FormCreate(Sender: TObject);
    procedure HabilitaBotones(Botones : string);
    function TraeRegistros : Boolean;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    function Inicializar(MDIChild: Boolean): Boolean;
    procedure PonerFocoEnPrimerControl;
    procedure ValidarTextoDeControl(Sender: TField; const Text: string);
    function GetCamposPK : string;
    procedure HabilitarDeshabilitarControles(Estado : Boolean);
    procedure CDSAfterScroll(DataSet: TDataSet);
    procedure vcbCategoriasClasesChange(Sender: TObject);
  private
    { Private declarations }
    CantidadRegistros : integer;
    Posicion          : TBookmark;
    Accion            : Integer; // 1=agregar   2=eliminar  3=editar
    Retorno           : Integer;
    MensajeError      : string;
    OldDomainID,
    OldTSMCID         : integer;
    FIELDS_PK         : string;
  public
    { Public declarations }
  published

  end;

var
  frmABMTSMCs: TfrmABMTSMCs;

implementation

uses DMConnection;

{$R *.dfm}

procedure TfrmABMTSMCs.HabilitarDeshabilitarControles(Estado : Boolean);
var
  i : Integer;
begin
   // INICIO : TASK_152_MGO_20170314
   for i := 0 to ComponentCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox, TVariantComboBox, TDBCheckBox', Components[i].ClassName) then begin

      if Components[i].ClassName = 'TDBEdit' then
        TDBEdit(Components[i]).Enabled := Estado;

      if Components[i].ClassName = 'TDBLookupComboBox' then
        TDBLookupComboBox(Components[i]).Enabled := Estado;

      if Components[i].ClassName = 'TVariantComboBox' then
        TVariantComboBox(Components[i]).Enabled := Estado;

      if Components[i].ClassName = 'TDBCheckBox' then
        TDBCheckBox(Components[i]).Enabled := Estado;

    end;
    // FIN : TASK_152_MGO_20170314
end;


function TfrmABMTSMCs.GetCamposPK : string;
var
  qry           : TADOQuery;
  XML, Salida   : string;
  DocXML        : IXMLDocument;
  Nodo          : IXMLNode;
begin

  qry := TADOQuery.Create(nil);
  qry.Connection := DMConnections.BaseBO_Master;
  qry.SQL.Text :=   'SELECT                                                                                                          ' +
                    '   CAST(                                                                                                        ' +
                    '       (SELECT COLUMN_NAME AS Nombre                                                                            ' +
                    '           FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A                                                          ' +
                    '               LEFT JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B ON A.CONSTRAINT_NAME = B.CONSTRAINT_NAME  ' +
                    '           where A.TABLE_SCHEMA + ''.'' + A.TABLE_NAME = ''dbo.TSMCs'' and CONSTRAINT_TYPE = ''PRIMARY KEY'' ' +
                    '       FOR XML PATH(''Campo''), ROOT(''Campos''), TYPE)                                                         ' +
                    '       AS VARCHAR(8000)) AS Campos                                                                              ' ;
  qry.Open;
  XML := qry.FieldByName('Campos').AsString;
  qry.Close;
  FreeAndNil(qry);

  DocXML          := TXMLDocument.Create(nil);
  DocXML.XML.Text := XML;
  DocXML.Active   := True;

  Nodo := DocXML.DocumentElement.ChildNodes.FindNode('Campo');

  Salida := EmptyStr;

  repeat
    if Nodo <> nil then begin
      Salida := Salida  + Nodo.ChildNodes.Nodes[0].NodeValue + ', ';
      Nodo:= Nodo.NextSibling;
    end;
  until Nodo = nil;

  DocXML.Active   := False;
  FreeAndNil(Nodo);

  Result          := Salida;

end;


procedure TfrmABMTSMCs.ValidarTextoDeControl(Sender: TField; const Text: string);
var
  Texto, NombreCampo    : string;
  Valida                : Boolean;
  ValorCampo            : LongInt;
begin

  Valida := True;

  if TField(Sender).ClassName = 'TSmallintField'  then begin

    NombreCampo := TField(Sender).FieldName;
    Texto       := Text;

    try
      ValorCampo := StrToInt(Texto);
    except
      TField(Sender).Value  := TField(Sender).OldValue;
      raise Exception.Create('Error en valor del campo, reintente');
      Exit;
    end;

    if ContainsText(FIELDS_TINYINT, NombreCampo) then
      if not ((ValorCampo >= 0) and (ValorCampo <= 255)) then
          Valida := False;

    if ContainsText(FIELDS_SMALLNT, NombreCampo) then
       if not ((ValorCampo >= -32768) and (ValorCampo <= 32767)) then
          Valida := False;

    if not Valida then
      TField(Sender).Value := TField(Sender).OldValue
    else
     TField(Sender).Value := ValorCampo;


  end;

  if not Valida then
    raise Exception.Create('Error en valor del campo, reintente')
  else
    TField(Sender).Value := Text;


end;

procedure TfrmABMTSMCs.vcbCategoriasClasesChange(Sender: TObject);
begin
    CDS.FieldByName('ID_CategoriasClases').Value := vcbCategoriasClases.Value;
end;

function TfrmABMTSMCs.Inicializar(MDIChild: Boolean): Boolean;
Var
	S: TSize;
    i: Integer; // TASK_152_MGO_20170314
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
    Position := poMainFormCenter;

    // INICIO : TASK_152_MGO_20170314
    qryCategoriasClases.Open;
    vcbCategoriasClases.Items.Clear;
    vcbCategoriasClases.Items.Add('(Seleccione)', 0);
    while not qryCategoriasClases.Eof do begin
        vcbCategoriasClases.Items.Add(
            qryCategoriasClases.FieldByName('Descripcion').AsString,
            qryCategoriasClases.FieldByName('ID_CategoriasClases').AsInteger);
        qryCategoriasClases.Next;
    end;
    vcbCategoriasClases.ItemIndex := 0;
    qryCategoriasClases.Close;

    TraeRegistros;

    for i := 0 to CDS.Fields.Count - 1 do
        CDS.Fields[i].OnSetText := ValidarTextoDeControl;

    if CantidadRegistros > 0 then
        HabilitaBotones('111100001')
    else
        HabilitaBotones('110000001');
    // FIN : TASK_152_MGO_20170314

	Result := True;
end;


function TfrmABMTSMCs.TraeRegistros;
var
  Resultado : Boolean;
  i         : integer;
begin

  try
    qryDomain.Close;
    qryPBL.Close;
    qryDomain.Open;
    qryPBL.Open;

    procSelect.Parameters.Refresh;
    procSelect.Open;
    Retorno := procSelect.Parameters.ParamByName('@RETURN_VALUE').Value;

    if Retorno <> 0 then begin
      MensajeError := procSelect.Parameters.ParamByName('@ErrorDescription').Value;
      Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
    end;

    CantidadRegistros := procSelect.RecordCount;
    CDS.Data := dspTSMC.Data;   // TASK_152_MGO_20170314
    CDS.First;                  // TASK_152_MGO_20170314
    //CDS.Close;                // TASK_152_MGO_20170314

    for i := 0 to CDS.Fields.Count - 1 do
      CDS.Fields[i].ReadOnly := False;

    //CDS.Open;                 // TASK_152_MGO_20170314
    procSelect.Close;
    CDS.ReadOnly := True;

    try
      CDS.GotoBookmark(Posicion);
    except
    end;

    Resultado := True;
  except
    on E : Exception do begin
      MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
      Resultado := False;
    end;
  end;
  
   HabilitarDeshabilitarControles(False);

  Result := Resultado;
end;

procedure TfrmABMTSMCs.btnAgregarClick(Sender: TObject);
var				// TASK_152_MGO_20170314
    I: Integer;	// TASK_152_MGO_20170314
begin
  HabilitaBotones('000000110');
  try
    Posicion := CDS.GetBookmark;
  except
  end;
  CDS.ReadOnly := False;
  CDS.Append;
  CDS.Post;
  CDS.Edit;

  pg01.ActivePage := tabGeneral;

  HabilitarDeshabilitarControles(True);
  PonerFocoEnPrimerControl;

  Accion := 1;
end;

procedure TfrmABMTSMCs.btnCancelarClick(Sender: TObject);
begin
  CDS.Cancel;
  TraeRegistros;

  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');

end;

procedure TfrmABMTSMCs.PonerFocoEnPrimerControl;
var
  i, ControlIndex, TabOrderMenor : Integer;
begin

  ControlIndex  := 99;
  TabOrderMenor := 99;

  for i := 0 to pg01.ActivePage.ControlCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox',TWinControl(pg01.ActivePage.Controls[i]).ClassType.ClassName) then
      if TWinControl(pg01.ActivePage.Controls[i]).TabOrder < TabOrderMenor then begin
        TabOrderMenor := TWinControl(pg01.ActivePage.Controls[i]).TabOrder;
        ControlIndex  := i;
      end;

  if ControlIndex < 99 then
    TWinControl(pg01.ActivePage.Controls[ControlIndex]).SetFocus;

end;

procedure TfrmABMTSMCs.btnEditarClick(Sender: TObject);
var
  i : Integer;
begin

  HabilitaBotones('000000110');
  HabilitarDeshabilitarControles(True);

  Posicion      := CDS.GetBookmark;
  OldDomainID   := CDS.FieldByName('DomainID').Value;
  OldTSMCID     := CDS.FieldByName('TSMCID').Value;
  CDS.ReadOnly  := False;

  for i := 0 to CDS.Fields.Count - 1 do
    if ContainsStr(FIELDS_PK, CDS.Fields[i].FieldName + ',') then
      CDS.Fields[i].ReadOnly := True;

  { INICIO : TASK_152_MGO_20170314
  for i := 0 to ComponentCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox, TDBCheckBox', Components[i].ClassName) then begin

      if Components[i].ClassName = 'TDBEdit' then
        if ContainsText(FIELDS_PK, TDBEdit(Components[i]).DataField) then
          TDBEdit(Components[i]).ReadOnly := True;

      if Components[i].ClassName = 'TDBLookupComboBox' then
        if ContainsText(FIELDS_PK, TDBLookupComboBox(Components[i]).DataField) then
          TDBLookupComboBox(Components[i]).ReadOnly := True;

      if Components[i].ClassName = 'TDBCheckBox' then
        if ContainsText(FIELDS_PK, TDBCheckBox(Components[i]).DataField) then
          TDBCheckBox(Components[i]).ReadOnly := True;

    end;
  }
    cbDomain.Enabled := False;
    edTSMCID.Enabled := False;
  // FIN : TASK_152_MGO_20170314

  CDS.Edit;

  //PonerFocoEnPrimerControl;   // TASK_152_MGO_20170314

  Accion := 3;
end;

procedure TfrmABMTSMCs.btnEliminarClick(Sender: TObject);
begin

  HabilitaBotones('000000000');

  try
    Posicion := CDS.GetBookmark;
  except
  end;

  if Application.MessageBox(PChar(ANSW_ELIMINAR + CDS.FieldByName('Descripcion').AsString),'Pregunta',MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    with procDelete do begin
      Parameters.Refresh;
      Parameters.ParamByName('@DomainID').Value               := CDS.FieldByName('DomainID').Value;
      Parameters.ParamByName('@TSMCID').Value                 := CDS.FieldByName('TSMCID').Value;

      try
        ExecProc;
        Retorno := procDelete.Parameters.ParamByName('@RETURN_VALUE').Value;
        if Retorno <> 0 then begin
          MensajeError := procDelete.Parameters.ParamByName('@ErrorDescription').Value;
          MsgBoxErr(MSG_ERROR_DELETE, MensajeError, Caption, MB_ICONERROR);
        end;
      except
        on E : Exception do begin
          MsgBoxErr(MSG_ERROR_DELETE, E.Message, Caption, MB_ICONERROR);
        end;
      end;
  end;

  TraeRegistros;

  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
end;

procedure TfrmABMTSMCs.btnGuardarClick(Sender: TObject);
// INICIO : TASK_152_MGO_20170314
resourcestring
    MSG_ERROR_VERSION_PBL = 'Debe seleccionar una Versi�n PBL';
    MSG_ERROR_IP = 'Debe ingresar una Direcci�n IP';
    MSG_ERROR_PUERTO = 'Debe ingresar un puerto';
    MSG_ERROR_FTP_SERVER = 'Debe ingresar un Servidor FTP';
    MSG_ERROR_FTP_USER = 'Debe ingresar el nombre de usuario para el FTP';
    MSG_ERROR_FTP_PASS = 'Debe ingresar la contrase�a para el FTP';
    MSG_ERROR_FTP_PORT = 'Debe ingresar el puerto del FTP';
    MSG_ERROR_DOMAIN = 'Debe seleccionar un Domain';
    MSG_ERROR_TSMC = 'Debe ingresar un ID de TSMC';
    MSG_ERROR_DESCRIPCION = 'Debe ingresar una descripci�n';
    MSG_ERROR_CLASE_CAT = 'Debe seleccionar una Clase de Categor�a';
var
    CATEGORIA_CLASE_URBANA,
// FIN : TASK_152_MGO_20170314
    i : integer;
begin
    CDS.Post;

    // INICIO : TASK_152_MGO_20170314
    if not ValidateControls([cbDomain, edTSMCID, vcbCategoriasClases, edDescripcion],
                            [CDS.FieldByName('DomainID').AsInteger > 0, edTSMCID.Text <> '', vcbCategoriasClases.ItemIndex > 0, edDescripcion.Text <> ''],
                            'Error', [MSG_ERROR_DOMAIN, MSG_ERROR_TSMC, MSG_ERROR_CLASE_CAT, MSG_ERROR_DESCRIPCION]) then begin
        cds.Edit;
        Exit;
    end;
             
    CATEGORIA_CLASE_URBANA := QueryGetValueInt(DMConnections.BaseCOP, 'SELECT dbo.CONST_CODIGO_CATEGORIA_CLASE_URBANA()', 30);

    if vcbCategoriasClases.Value = CATEGORIA_CLASE_URBANA then begin
        if not ValidateControls([cbPBL, edIP, dbPort1, edPort2, edPort3, edFTP, edFTPUser, edFTPPassword, edFTPPort],
                                [CDS.FieldByName('CodigoVersionPBL').AsInteger > 0, edIP.Text <> '', dbPort1.Text <> '', edPort2.Text <> '',
                                edPort3.Text <> '', edFTP.Text <> '', edFTPUser.Text <> '', edFTPPassword.Text <> '', edFTPPort.Text <> ''],
                                'Error',[MSG_ERROR_VERSION_PBL, MSG_ERROR_IP, MSG_ERROR_PUERTO, MSG_ERROR_PUERTO, MSG_ERROR_PUERTO,
                                MSG_ERROR_FTP_SERVER, MSG_ERROR_FTP_USER, MSG_ERROR_FTP_PASS, MSG_ERROR_FTP_PORT]) then begin
            CDS.Edit;
            Exit;
        end;
    end;
    // FIN : TASK_152_MGO_20170314

    if Accion = 1 then begin
        with procInsert do begin
            Parameters.Refresh;

            for i:=0 to  CDS.Fields.Count - 1 do
               if NOT ContainsStr(PARAMS_IGNORE, '@' + CDS.Fields[i].FieldName) then
                 Parameters.ParamByName('@' + CDS.Fields[i].FieldName).Value := CDS.Fields[i].Value;

            Parameters.ParamByName('@UsuarioCreacion').Value        := UsuarioSistema;
            Parameters.ParamByName('@DomainID').Value               := CDS.FieldByName('DomainID').Value;
            Parameters.ParamByName('@TSMCID').Value                 := CDS.FieldByName('TSMCID').Value;
            Parameters.ParamByName('@ID_CategoriasClases').Value    := vcbCategoriasClases.Value;   // TASK_152_MGO_20170314

            try
                ExecProc;
                Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                if Retorno <> 0 then begin
                    MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
                    MsgBoxErr(MSG_ERROR_INSERT, MensajeError, Caption, MB_ICONERROR);
                end;
            except
                on E : Exception do begin
                    MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
                end;
            end;

        end;
    end;

    if Accion = 3 then begin

        with procUpdate do begin
            Parameters.Refresh;

            for i:=0 to  CDS.Fields.Count - 1 do
                if NOT ContainsStr(PARAMS_IGNORE, '@' + CDS.Fields[i].FieldName) then
                    Parameters.ParamByName('@' + CDS.Fields[i].FieldName).Value := CDS.Fields[i].Value;

            Parameters.ParamByName('@UsuarioModificacion').Value  := UsuarioSistema;
            Parameters.ParamByName('@DomainID').Value             := OldDomainID;
            Parameters.ParamByName('@TSMCID').Value               := OldTSMCID;
            Parameters.ParamByName('@DomainIDNew').Value          := CDS.FieldByName('DomainID').Value;
            Parameters.ParamByName('@TSMCIDNew').Value            := CDS.FieldByName('TSMCID').Value;  
            Parameters.ParamByName('@ID_CategoriasClases').Value  := vcbCategoriasClases.Value;   // TASK_152_MGO_20170314

            try
                ExecProc;
                Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                if Retorno <> 0 then begin
                    MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
                    MsgBoxErr(MSG_ERROR_UPDATE, MensajeError, Caption, MB_ICONERROR);
                end;
            except
                on E : Exception do begin
                    MsgBoxErr(MSG_ERROR_UPDATE, E.Message, Caption, MB_ICONERROR);
                end;
            end;

        end;
    end;

    TraeRegistros;
    if CantidadRegistros > 0 then
        HabilitaBotones('111100001')
    else
        HabilitaBotones('110000001');
end;

procedure TfrmABMTSMCs.btnSalirClick(Sender: TObject);
begin
    Close;
end;

// INICIO : TASK_152_MGO_20170314
procedure TfrmABMTSMCs.CDSAfterScroll(DataSet: TDataSet);
begin
    vcbCategoriasClases.Value := CDS.FieldByName('ID_CategoriasClases').AsInteger;
end;
// FIN : TASK_152_MGO_20170314

procedure TfrmABMTSMCs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
{ INICIO : TASK_152_MGO_20170314
  if Application.MessageBox(PChar(ANSW_SALIR),'Pregunta', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    Action := caFree
  else
    Action := caNone;
}
    Action := caFree;
// FIN : TASK_152_MGO_20170314
end;

procedure TfrmABMTSMCs.FormCreate(Sender: TObject);
var
  i         : Integer;
  SP_Aux    : TADOStoredProc;
begin

  Caption           := CAPTION_FORM;
  btnSalir.Hint     := HINT_SALIR;
  btnAgregar.Hint   := HINT_AGREGAR;
  btnEliminar.Hint  := HINT_ELIMINAR;
  btnEditar.Hint    := HINT_EDITAR;

  procSelect.ProcedureName := PROCEDURE_SELECT;
  procInsert.ProcedureName := PROCEDURE_INSERT;
  procUpdate.ProcedureName := PROCEDURE_UPDATE;
  procDelete.ProcedureName := PROCEDURE_DELETE;

  procSelect.Connection := DMConnections.BaseBO_Master;
  procInsert.Connection := DMConnections.BaseBO_Master;
  procUpdate.Connection := DMConnections.BaseBO_Master;
  procDelete.Connection := DMConnections.BaseBO_Master;

  qryPBL.Connection     := DMConnections.BaseCOP;
  qryDomain.Connection  := DMConnections.BaseCOP;

  pg01.ActivePage := tabGeneral;

  FIELDS_PK := GetCamposPK;

  { INICIO : TASK_152_MGO_20170314
  TraeRegistros;

  for i := 0 to CDS.Fields.Count - 1 do
    CDS.Fields[i].OnSetText := ValidarTextoDeControl;

  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
  } // FIN : TASK_152_MGO_20170314
end;

procedure TfrmABMTSMCs.HabilitaBotones(Botones : string);
begin
  btnSalir.Enabled      := Botones[1] = '1';
  btnAgregar.Enabled    := Botones[2] = '1';
  btnEliminar.Enabled   := Botones[3] = '1';
  btnEditar.Enabled     := Botones[4] = '1';
  btnImprimir.Enabled   := Botones[5] = '1';
  btnBuscar.Enabled     := Botones[6] = '1';
  btnGuardar.Enabled    := Botones[7] = '1';
  btnCancelar.Enabled   := Botones[8] = '1';
  Grid.Enabled          := Botones[9] = '1';
end;

end.



