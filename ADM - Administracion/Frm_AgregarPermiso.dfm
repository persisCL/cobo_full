object FormAgregarPermiso: TFormAgregarPermiso
  Left = 266
  Top = 155
  BorderStyle = bsDialog
  Caption = 'Agregar Usuarios / Grupos'
  ClientHeight = 306
  ClientWidth = 536
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object img_usuario: TImage
    Left = 8
    Top = 280
    Width = 17
    Height = 17
    AutoSize = True
    Picture.Data = {
      07544269746D617042010000424D420100000000000076000000280000001100
      0000110000000100040000000000CC000000C40E0000C40E0000100000000000
      0000000000000000800000800000008080008000000080008000808000008080
      8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
      FF009999999999999999900000009999EEE00EE00EE9900000009990EE0EE00E
      0E0990000000999900EE08F0009990000000999990008F809999900000009999
      9908F000099990000000999990008F8F80999000000099999000F8F800999000
      0000999900008F8F80999000000099900008F8F8F80990000000999000000F8C
      80999000000099900000F8F0099990000000999000078F8F0999900000009990
      000007F099999000000099990000000009999000000099999000000099999000
      0000999999999999999990000000}
    Visible = False
  end
  object img_grupo: TImage
    Left = 32
    Top = 280
    Width = 21
    Height = 17
    AutoSize = True
    Picture.Data = {
      07544269746D617042010000424D420100000000000076000000280000001500
      0000110000000100040000000000CC000000C40E0000C40E0000100000000000
      0000000000000000800000800000008080008000000080008000808000008080
      8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
      FF00990000000000009999999000990888888888809999999000990FFFFFFFFF
      F099999990009990000000000DDD09909000999088888888DDF0E70990009990
      8CCCCCBB0F0070999000999084ECC3B0F00070099000999084ECBB0F70077770
      900099908444BB0FFFF07770900099908888BB0FFF807777000099990000BB0F
      FFFF0700900099999990BBB0F7007770900099999990BBB0FFF0070090009999
      99990BBB0000000090009999999990BBBBB00009900099999999990000099999
      9000999999999999999999999000}
    Visible = False
  end
  object Bevel1: TBevel
    Left = 0
    Top = 272
    Width = 534
    Height = 5
    Shape = bsTopLine
  end
  object rb_conpermiso: TRadioButton
    Left = 5
    Top = 232
    Width = 220
    Height = 17
    Caption = 'Permitir el acceso a la funci'#243'n indicada'
    Checked = True
    TabOrder = 2
    TabStop = True
  end
  object RadioButton2: TRadioButton
    Left = 5
    Top = 252
    Width = 220
    Height = 17
    Caption = 'Denegar el acceso a la funci'#243'n indicada'
    TabOrder = 3
  end
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 513
    Height = 217
    BevelOuter = bvNone
    BorderStyle = bsSingle
    TabOrder = 4
    object Shape1: TShape
      Left = 0
      Top = 18
      Width = 509
      Height = 1
      Align = alTop
    end
    object lb_usuarios: TListBox
      Left = 0
      Top = 19
      Width = 509
      Height = 194
      Style = lbOwnerDrawFixed
      Align = alClient
      BorderStyle = bsNone
      Ctl3D = False
      ItemHeight = 17
      MultiSelect = True
      ParentCtl3D = False
      TabOrder = 0
      OnDblClick = lb_usuariosDblClick
      OnDrawItem = lb_usuariosDrawItem
      OnKeyPress = lb_usuariosKeyPress
    end
    object Header1: THeader
      Left = 0
      Top = 0
      Width = 509
      Height = 18
      Align = alTop
      AllowResize = False
      BorderStyle = bsNone
      Sections.Sections = (
        #0'24'#0
        #0'150'#0'Grupo / Usuario'
        #0'45'#0'Nombre')
      TabOrder = 1
    end
  end
  object btn_Aceptar: TButton
    Left = 378
    Top = 279
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    TabOrder = 0
    OnClick = btn_AceptarClick
  end
  object Button2: TButton
    Left = 459
    Top = 279
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 1
  end
  object ObtenerUsuariosGruposSinFuncion: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ObtenerUsuariosGruposSinFuncion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@Funcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end>
    Left = 248
    Top = 232
  end
end
