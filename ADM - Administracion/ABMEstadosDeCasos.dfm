object FABMEstadosDeCasos: TFABMEstadosDeCasos
  Left = 107
  Top = 114
  Caption = 'Mantenimiento de Estados de Casos'
  ClientHeight = 567
  ClientWidth = 809
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupB: TPanel
    Left = 0
    Top = 428
    Width = 809
    Height = 100
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Lcodigo: TLabel
      Left = 11
      Top = 8
      Width = 44
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Ldescripcion: TLabel
      Left = 11
      Top = 68
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Ltipo: TLabel
      Left = 11
      Top = 38
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtCodigo: TNumericEdit
      Left = 86
      Top = 8
      Width = 105
      Height = 21
      Color = 16444382
      Enabled = False
      TabOrder = 0
    end
    object txtDescripcion: TEdit
      Left = 86
      Top = 68
      Width = 388
      Height = 21
      Color = 16444382
      TabOrder = 2
    end
    object txtNombre: TEdit
      Left = 86
      Top = 38
      Width = 203
      Height = 21
      Color = 16444382
      TabOrder = 1
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 528
    Width = 809
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 612
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 809
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbarClose
  end
  object ListaEstados: TAbmList
    Left = 0
    Top = 33
    Width = 809
    Height = 395
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'59'#0'C'#243'digo'
      #0'178'#0'Nombre'
      #0'64'#0'Descripci'#243'n')
    HScrollBar = True
    RefreshTime = 100
    Table = tblEstadosDeCasos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaEstadosClick
    OnProcess = ListaEstadosProcess
    OnDrawItem = ListaEstadosDrawItem
    OnRefresh = ListaEstadosRefresh
    OnInsert = ListaEstadosInsert
    OnDelete = ListaEstadosDelete
    OnEdit = ListaEstadosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object tblEstadosDeCasos: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'EstadosDeCasos'
    Left = 92
    Top = 89
  end
  object spActualizarEstadosDeCasos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEstadosDeCasos'
    Parameters = <>
    Left = 244
    Top = 104
  end
  object dsEstadosDeCasos: TDataSource
    DataSet = tblEstadosDeCasos
    Left = 94
    Top = 144
  end
  object spEliminarEstadosDeCasos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarEstadosDeCasos'
    Parameters = <>
    Left = 240
    Top = 160
  end
end
