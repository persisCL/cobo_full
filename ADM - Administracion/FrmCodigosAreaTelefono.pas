{-----------------------------------------------------------------------------
 File Name: FrmCodigosAreaTelefono
 Author:
 Date Created:
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

Revision : 2
Date: 02/08/2010
Author: pdominguez
Description: SS 903 - Error al modificar medio contacto celular
    - Se modificaron los siguientes Procedimientos / Funciones:
        dblCodigosAreaClick,
        dblCodigosAreaDrawItem,
        Limpiar_Campos,
        BtnAceptarClick
-----------------------------------------------------------------------------}
unit FrmCodigosAreaTelefono;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls,RStrings, VariantComboBox;

type
  TFormCodigosAreaTelefono = class(TForm)
    AbmToolbar: TAbmToolbar;
    dblCodigosArea: TAbmList;
    GroupB: TPanel;
    lblDescripcion: TLabel;
    lblCodigoArea: TLabel;
    Panel2: TPanel;
    txtDescripcion: TEdit;
    CodigosAreaTelefono: TADOTable;
    txtCodigoArea: TNumericEdit;
    Notebook: TNotebook;
    txt_RangoDesde: TNumericEdit;
    Label1: TLabel;
    txt_RangoHasta: TNumericEdit;
    Label2: TLabel;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    vcbTiposTelefono: TVariantComboBox;
    lblTipo: TLabel;
    txt_CodigoPais: TNumericEdit;
    lblCodigoPais: TLabel;
    procedure BtnCancelarClick(Sender: TObject);
    procedure dblCodigosAreaClick(Sender: TObject);
    procedure dblCodigosAreaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dblCodigosAreaEdit(Sender: TObject);
    procedure dblCodigosAreaRefresh(Sender: TObject);
    procedure AbmToolbarClose(Sender: TObject);
    procedure dblCodigosAreaDelete(Sender: TObject);
    procedure dblCodigosAreaInsert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializar: boolean;

  end;

var
  FormCodigosAreaTelefono: TFormCodigosAreaTelefono;

implementation

{$R *.DFM}

procedure TFormCodigosAreaTelefono.VolverCampos;
begin
	dblCodigosArea.Estado     	:= Normal;
	dblCodigosArea.Enabled    	:= True;
	dblCodigosArea.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
    txtCodigoArea.Enabled	    := True;
end;

function TFormCodigosAreaTelefono.Inicializar: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([CodigosAreaTelefono]) then
		Result := False
	else begin
		Result := True;
		dblCodigosArea.Reload;
	end;
   	Notebook.PageIndex := 0;
end;

procedure TFormCodigosAreaTelefono.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

{
    Revision: 2
    Author  : pdomniguez
    Date    : 02/08/2010
    Description: SS 903 - Error al modificar medio contacto celular
        - Se a�ade la actualizaci�n de los datos del Combo de Tipos
        de L�nea.
        - Se a�ade el C�digo Pa�s.
}
procedure TFormCodigosAreaTelefono.dblCodigosAreaClick(Sender: TObject);
begin
	with CodigosAreaTelefono do begin
		txtCodigoArea.Value	:= FieldByName('CodigoArea').AsInteger;
		txtDescripcion.text	:= FieldByName('Descripcion').AsString;
        txt_RangoDesde.Value:= FieldByName('RangoDesde').AsInteger;
        txt_RangoHasta.Value:= FieldByName('RangoHasta').AsInteger;
        txt_CodigoPais.Value:= FieldByName('CodigoTelefonoPais').AsInteger; // Rev. 2 (SS 903)
        vcbTiposTelefono.ItemIndex := vcbTiposTelefono.Items.IndexOfValue(FieldByName('TipoLinea').AsString); // Rev. 2 (SS 903)
	end;
end;

{
    Revision: 2
    Author  : pdomniguez
    Date    : 02/08/2010
    Description: SS 903 - Error al modificar medio contacto celular
        - Se a�ade la actualizaci�n del Tipo de L�nea y C�digo Pa�s en la Rejilla de datos.
}
procedure TFormCodigosAreaTelefono.dblCodigosAreaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('CodigoArea').AsString);
        TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldByName('Descripcion').AsString));
      	TextOut(Cols[2], Rect.Top, iif(not(Tabla.FieldByName('RangoDesde').IsNull) and Tabla.FieldByName('RangoDesde').Value<=0,'',Trim(Tabla.FieldByName('RangoDesde').AsString)));
        TextOut(Cols[3], Rect.Top, iif(not(Tabla.FieldByName('RangoHasta').IsNull) and Tabla.FieldByName('RangoHasta').Value<=0,'',Trim(Tabla.FieldByName('RangoHasta').AsString)));
        TextOut(Cols[4], Rect.Top, iif(FieldByName('CodigoTelefonoPais').IsNull, '', FieldByName('CodigoTelefonoPais').AsString)); // Rev. 2 (SS 903)
        TextOut(Cols[5], Rect.Top, iif(Tabla.FieldByName('TipoLinea').AsString = 'C','Celular', 'L�nea Fija')); // Rev. 2 (SS 903)
	end;
end;

procedure TFormCodigosAreaTelefono.dblCodigosAreaEdit(Sender: TObject);
begin
	HabilitarCampos;
    dblCodigosArea.Estado := modi;
end;

procedure TFormCodigosAreaTelefono.dblCodigosAreaInsert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCampos;
    dblCodigosArea.Estado := Alta;
end;

procedure TFormCodigosAreaTelefono.dblCodigosAreaRefresh(Sender: TObject);
begin
	if dblCodigosArea.Empty then Limpiar_Campos;
end;

{
    Revision: 2
    Author  : pdomniguez
    Date    : 02/08/2010
    Description: SS 903 - Error al modificar medio contacto celular
        - Se a�ade la inicializaci�n del Tipo de L�nea.
        - Se a�ade la inicializaci�n del C�digo Pa�s.
}
procedure TFormCodigosAreaTelefono.Limpiar_Campos;
begin
	txtCodigoArea.Clear;
	txtDescripcion.Clear;
    txt_RangoDesde.Clear;
    txt_RangoHasta.Clear;
    txt_CodigoPais.Clear; // Rev. 2 (SS 903)
    vcbTiposTelefono.ItemIndex := vcbTiposTelefono.Items.IndexOfValue('F'); // Rev. 2 (SS 903)
end;

procedure TFormCodigosAreaTelefono.AbmToolbarClose(Sender: TObject);
begin
    close;
end;

procedure TFormCodigosAreaTelefono.dblCodigosAreaDelete(Sender: TObject);
resourcestring
    MSG_ERROR_RELATED_RECORDS = 'No se puede eliminar el C�digo de Area porque existen datos relacionados';
begin
	Screen.Cursor := crHourGlass;
    try
        If MsgBox(format(MSG_QUESTION_BAJA,[FLD_CODIGO_AREA]), format(MSG_CAPTION_GESTION,[FLD_CODIGO_AREA]), MB_YESNO + MB_ICONQUESTION) =
            IDYES then begin
            try
                CodigosAreaTelefono.Delete;
            Except
                On E: Exception do begin
                    CodigosAreaTelefono.Cancel;
                    MsgBoxErr(MSG_ERROR_RELATED_RECORDS, e.message, format(MSG_CAPTION_GESTION,[FLD_CODIGO_AREA]), MB_ICONSTOP);
                end;
            end;
            dblCodigosArea.Reload;
        end;
        VolverCampos;
    finally
	    Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

  Revision: 2
    Author  : pdomniguez
    Date    : 02/08/2010
    Description: SS 903 - Error al modificar medio contacto celular
        - Se a�ade la grabaci�n del Tipo de L�nea y C�digo Pa�s.
-----------------------------------------------------------------------------}
procedure TFormCodigosAreaTelefono.BtnAceptarClick(Sender: TObject);
var
    ExisteCodigoArea : Boolean;
begin
    if not ValidateControls([txtCodigoArea],
                [trim(txtCodigoArea.text) <> ''],
                format(MSG_CAPTION_ACTUALIZAR,[FLD_CODIGO_AREA]),
                [format(MSG_VALIDAR_DEBE_EL,[FLD_CODIGO_AREA])]) then exit;

    if txt_RangoDesde.Value>txt_RangoHasta.Value then begin
        MsgBoxBalloon(MSG_VALIDAR_RANGO_NUMERICO,format(MSG_CAPTION_ACTUALIZAR,[FLD_CODIGO_AREA]),MB_ICONSTOP,txt_RangoDesde);
        exit;
    end;

   ExisteCodigoArea := (QueryGetValue(DMConnections.BaseCAC,
                            'SELECT CodigoArea FROM CodigosAreaTelefono  WITH (NOLOCK) ' +
                            'WHERE CodigoArea = ' + trim(txtCodigoArea.text)) <> '') and ((dblCodigosArea.Estado = Alta) or
                            ((dblCodigosArea.Estado = Modi) and (CodigosAreaTelefono.FieldByName('CodigoArea').AsInteger <> txtCodigoArea.Value)));


    if ExisteCodigoArea then begin
        MsgBoxBalloon(format(MSG_ERROR_EXISTE_EL,['C�digo de Area ' + txtCodigoArea.text]), format(MSG_CAPTION_ACTUALIZAR,[FLD_CODIGO_AREA]), MB_ICONSTOP, txtCodigoArea);
       Exit;
    end;
 	Screen.Cursor := crHourGlass;
	With CodigosAreaTelefono do begin
		Try
			if dblCodigosArea.Estado = Alta then begin Append;
			end else Edit;
            FieldByName('CodigoArea').value		:= txtCodigoArea.value;
			FieldByName('Descripcion').AsString	:= Trim(txtDescripcion.text);
            // Rev. 2 (SS 903)
            if txt_RangoDesde.ValueInt > 0 then
                FieldByName('RangoDesde').AsInteger	:= txt_RangoDesde.ValueInt
            else FieldByName('RangoDesde').Clear;
            if txt_RangoHasta.ValueInt > 0 then
                FieldByName('RangoHasta').AsInteger	:= txt_RangoHasta.ValueInt
            else FieldByName('RangoHasta').Clear;
            if txt_CodigoPais.ValueInt > 0 then
                FieldByName('CodigoTelefonoPais').AsInteger	:= txt_CodigoPais.ValueInt
            else FieldByName('CodigoTelefonoPais').Clear;
            FieldByName('TipoLinea').AsString   := vcbTiposTelefono.Value;
            // Fin Rev. 2 (SS 903)
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(format(MSG_ERROR_ACTUALIZAR,[FLD_CODIGO_AREA]), e.message, format(MSG_CAPTION_ACTUALIZAR,[FLD_CODIGO_AREA]), MB_ICONSTOP);
                Screen.Cursor      := crDefault;
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFormCodigosAreaTelefono.FormShow(Sender: TObject);
begin
   	dblCodigosArea.Reload;
end;

procedure TFormCodigosAreaTelefono.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormCodigosAreaTelefono.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormCodigosAreaTelefono.HabilitarCampos;
begin
	dblCodigosArea.Enabled := False;
	Notebook.PageIndex 	   := 1;
    groupb.Enabled     	   := True;
    txtCodigoArea.SetFocus;
end;

end.
