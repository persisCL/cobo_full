
{********************************** Unit Header ********************************
File Name : ABMTramosSerbanc.pas
Author : ndonadio                            
Date Created: 02/09/2005
Language : ES-AR
Description :  ABM de mantenimiento de tarifas y tramos de tarifacion de gestiones
              de cobranza de Serbanc

Etiqueta	:	20160610 MGO
Descripci�n	:	Se corrige espaciado.
				Se reemplazan SPs de Serbanc por Cobranza.
				Se cambia t�tulo de formulario.
*******************************************************************************}
unit ABMTramosSerbanc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilDB, DMConnection, DB, Validate, DateEdit, StdCtrls,
  DmiCtrls, ExtCtrls, DbList, Abm_obj, ADODB, PeaProcs,Util, UtilProc, ComCtrls, DBClient, Grids,
  DBGrids, Provider, fDlgTramosCobranza, ListBoxEx, DBListEx, PeaTypes;


type
  TfrmABMTramosGastosSerbanc = class(TForm)
    pnlButtons: TPanel;
    Notebook1: TNotebook;
    btnSalir: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    AbmToolbar1: TAbmToolbar;
    AbmList1: TAbmList;
    tblTarifas: TADOTable;
	{ INICIO : 20160610 MGO
    spObtenerTarifaVigenteSerbanc: TADOStoredProc;
	}
    spObtenerTarifaGastosCobranzaVigente: TADOStoredProc;
    // FIN : 20160610 MGO
    pnlData: TPanel;
    pnlTarifa: TPanel;
    dateActivacionTarifa: TDateEdit;
    Label5: TLabel;
    lblEstadoTarifa: TLabel;
    cdsTramos: TClientDataSet;
	// FIN : 20160610 MGO
    spBorrarTramos: TADOStoredProc;
    spAgregarTramo: TADOStoredProc;
    dsTramos: TDataSource;
    btnAgregarTramo: TButton;
    btnEditarTramo: TButton;
    btnEliminarTramo: TButton;
    dspTramos: TDataSetProvider;
	{ INICIO : 20160610 MGO
    spActualizarTarifaSerbanc: TADOStoredProc;
	}
    spActualizarTarifaGastosCobranza: TADOStoredProc;
    spAgregarTarifaGastosCobranza: TADOStoredProc;
    lblCodigoTarifa: TLabel;       
    txtCodigoTarifa: TNumericEdit;
	// FIN : 20160610 MGO
    dblTramos: TDBListEx;
    spObtenerTramosTarifaGastosCobranza: TADOStoredProc;
    procedure dblTramosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
      State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnEliminarTramoClick(Sender: TObject);
    procedure btnEditarTramoClick(Sender: TObject);
    procedure btnAgregarTramoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure AbmList1Insert(Sender: TObject);
    procedure AbmList1Edit(Sender: TObject);
    procedure AbmList1Delete(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure AbmList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect;
      State: TOwnerDrawState; Cols: TColPositions);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AbmList1Click(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
  private
    { Private declarations }
    FInicializando: boolean;
    FFechaActual,
    FFechaTarifaVigente: TDateTime;
    FCodigoTarifaVigente: Integer;
    procedure CambiarAModo(Modo: integer);
    procedure BorrarTramos( CodigoTarifa: Integer);
    procedure HabilitarBotonesTramos(Habilitar: Boolean);
    function ObtenerTarifaVigente(var Error: AnsiString): Boolean;
    function CargarTramos( CodigoTarifa: Integer; var Error: AnsiString):Boolean;
    function BuscarMayorMonto(Campo: string): TBookmark;         //TASK_115_JMA_20170316
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

const
    CONSULTA  = 0; // indica que la ventana esta en modo normal
    EDICION = 1; // indica que la ventana esta en modo edicion
var
  frmABMTramosGastosSerbanc: TfrmABMTramosGastosSerbanc;

implementation

{$R *.dfm}

{ TForm1 }

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 02/09/2005
Description :   Inicializa el form.
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TfrmABMTramosGastosSerbanc.Inicializar: Boolean;
resourcestring
    ERROR_MSG = 'Error de Inicializaci�n';
Var
    Error: AnsiString;
	S: TSize;
begin
    FInicializando := True;
    Result := False;
    try
        //centro el form
        CenterForm(Self);
        //lo escalo al area cliente
        S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
        if not ObtenerTarifaVigente(Error) then begin
             MsgBoxErr(ERROR_MSG, Error, Caption, MB_ICONSTOP) ;
             Exit;
        end;

        try
            tblTarifas.Open;
            // Cambiamos a modo consulta a mano
            // para evitar la llamada al reload y el onClick (que estaria dehabilitado)
            // y se lo llama mas abajo.... en el finally
            abmList1.Enabled := True;
            Notebook1.ActivePage := 'Salir';

            abmList1.Reload;
            Result := True;
        except
            on e:exception do begin
                MsgBoxErr(ERROR_MSG,e.Message,'ERROR',MB_ICONSTOP) ;
            end;
        end;
    finally
         FInicializando := False;
         abmList1click(self);
    end;

end;

{******************************** Function Header ******************************
Function Name: AbmList1Click
Author : ndonadio
Date Created : 02/09/2005
Description :   Maneja la respuesta al evento OnClick en el ABMList
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmABMTramosGastosSerbanc.AbmList1Click(Sender: TObject);
resourcestring
    STATE_CURRENT   = 'VIGENTE';
    STATE_INACTIVE  = 'Inactiva';
    STATE_FUTURE    = 'Pendiente';
    ERROR_LOADING_DATA = 'Error cargando los datos para la tarifa seleccionada';
var
    Error: AnsiString;
begin
    // Cargar datos Tarifa
    txtCodigoTarifa.ValueInt := tblTarifas.FieldByName('CodigoTarifa').AsInteger;    // 20160610 MGO
    dateActivacionTarifa.Date := tblTarifas.FieldByName('FechaActivacion').asDateTime;
    if tblTarifas.FieldByName('FechaActivacion').asDateTime = FFechaTarifaVigente then begin
        lblEstadoTarifa.Caption := STATE_CURRENT;
        abmList1.Access := [accAlta] ;
        abmToolbar1.Habilitados := [btAlta];
    end
    else if tblTarifas.FieldByName('FechaActivacion').asDateTime < FFechaTarifaVigente then begin
            lblEstadoTarifa.Caption := STATE_INACTIVE;
            abmList1.Access := [accAlta] ;
            abmToolbar1.Habilitados := [btAlta];
        end
        else begin
            lblEstadoTarifa.Caption := STATE_FUTURE;
            abmList1.Access := [accAlta, accBaja, accModi] ;
            abmToolbar1.Habilitados := [btAlta, btBaja, btModi];
        end;
    // Cargar Datos Tramos
    { INICIO : 20160610 MGO
    if not CargarTramos(tblTarifas.FieldByName('CodigoTarifaSerbanc').AsInteger, Error) then begin
    }
    if not CargarTramos(tblTarifas.FieldByName('CodigoTarifa').AsInteger, Error) then begin
    // FIN : 20160610 MGO
        MsgBoxErr(ERROR_LOADING_DATA, Error, Caption, MB_ICONERROR);
        Close;
    end;
end;

{******************************** Function Header ******************************
Function Name: CambiarAModo
Author : ndonadio
Date Created : 02/09/2005
Description :  Pone el form en modo CONSULTA o EDICION segun el valor pasado
Parameters : Modo: integer
Return Value : None
*******************************************************************************}
procedure TfrmABMTramosGastosSerbanc.CambiarAModo(Modo: integer);
begin
    if (Modo = CONSULTA) then begin
        // inhabilita edit boxes
        lblEstadoTarifa.Visible := True;
        dateActivacionTarifa.Enabled := False;
        pnlTarifa.Enabled := False;
        abmList1.Enabled := True;
        HabilitarBotonesTramos(False);
        Notebook1.ActivePage := 'Salir';
        abmList1.Estado := Normal;
        abmList1.Reload;
        abmList1Click(Self);
        abmToolbar1.Refresh;
        
    end
    else begin
        // Estoy en modo edicion. La ABMList esta deshabilitada. Solo puedo trabajar sobre
        // los Edit y los botones Aceptar y Cancelar.

        lblEstadoTarifa.Visible := False;
        dateActivacionTarifa.Enabled := True;
        HabilitarBotonesTramos(True);
        pnlTarifa.Enabled := True;
        abmToolbar1.Habilitados := [];
        abmList1.Access := [] ;
        abmList1.Enabled := False;
        Notebook1.ActivePage := 'Editing';
    end;
end;

procedure TfrmABMTramosGastosSerbanc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmABMTramosGastosSerbanc.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := NOT ( False );
end;

{******************************** Function Header ******************************
Function Name: ObtenerTarifaVigente
Author : ndonadio
Date Created : 02/09/2005
Description :   Obtiene Codigo y Fecha de la tarifa vigente, mas la fecha actual
                segun el servidro de db.
Parameters :    None
Return Value :  Integer
*******************************************************************************}
function TfrmABMTramosGastosSerbanc.ObtenerTarifaVigente(var Error: AnsiString): Boolean;
begin
    Result := False;
    try
        { INICIO : 20160610 MGO
        spObtenerTarifaVigenteSerbanc.Open;
        if spObtenerTarifaVigenteSerbanc.IsEmpty then begin
            FFechaActual := QueryGetValueDateTime(DMCOnnections.BaseCAC, 'SELECT dbo.PrincipioDia(GetDate())');
            FFechaTarifaVigente := FFechaActual;
//            FCodigoTarifaVigente := NULL;
        end
        else begin
            FFechaActual            := spObtenerTarifaVigenteSerbanc.FieldBYName('FechaActual').AsDateTime; ;
            FFechaTarifaVigente     := spObtenerTarifaVigenteSerbanc.FieldBYName('FechaActivacion').AsDateTime;
            FCodigoTarifaVigente    := spObtenerTarifaVigenteSerbanc.FieldBYName('CodigoTarifaSerbanc').AsInteger;
        end;
        spObtenerTarifaVigenteSerbanc.Close;
        }
        with spObtenerTarifaGastosCobranzaVigente do begin
            Open;

            if IsEmpty then begin
                FFechaActual := QueryGetValueDateTime(DMCOnnections.BaseCAC, 'SELECT dbo.PrincipioDia(GetDate())');
                FFechaTarifaVigente := FFechaActual;
            end else begin
                FFechaActual            := FieldBYName('FechaActual').AsDateTime; ;
                FFechaTarifaVigente     := FieldBYName('FechaActivacion').AsDateTime;
                FCodigoTarifaVigente    := FieldBYName('CodigoTarifa').AsInteger;
            end;
            
            Close;
        end;
        // FIN : 20160610 MGO
        Result := True;
    except
        on e: Exception do begin
            Error := e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: AbmList1DrawItem
Author : ndonadio
Date Created : 02/09/2005
Description :    Camia el color de las entradas en lalista de tarifas de acuerdo a la vigencia
Parameters : Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
Return Value : None
*******************************************************************************}
procedure TfrmABMTramosGastosSerbanc.AbmList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect;
  State: TOwnerDrawState; Cols: TColPositions);
begin
    // Decido como mostrar los datos en la tabla...
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        if  (Tabla.FieldByName('FechaActivacion').AsDateTime = FFechaTarifaVigente) then begin
            Font.Style := [];
            Font.Color := clRed;
        end;
        if  (Tabla.FieldByName('FechaActivacion').AsDateTime > FFechaTarifaVigente) then begin
            Font.Style := [];
            Font.Color := clWindowText;
        end;
        if  (Tabla.FieldByName('FechaActivacion').AsDateTime < FFechaTarifaVigente) then begin
            Font.Style := [];
            Font.Color := clGrayText;
        end;
        { INICIO : 20160610 MGO
      	TextOut(Cols[0], Rect.Top, FormatDateTime('dd/mm/yyyy',  Tabla.FieldByName('FechaActivacion').AsDateTime ));
        TextOut(Cols[1], Rect.Top, FormatDateTime('dd/mm/yyyy HH:nn:ss',  Tabla.FieldByName('FechaActualizacion').AsDateTime));
        }
        TextOut(Cols[0], Rect.Top, IntToStr(Tabla.FieldByName('CodigoTarifa').AsInteger));
        TextOut(Cols[1], Rect.Top, FormatDateTime('dd/mm/yyyy',  Tabla.FieldByName('FechaActivacion').AsDateTime ));
        TextOut(Cols[2], Rect.Top, FormatDateTime('dd/mm/yyyy HH:nn:ss',  Tabla.FieldByName('FechaActualizacion').AsDateTime));
        TextOut(Cols[3], Rect.Top, Tabla.FieldByName('Usuario').AsString);
        // FIN : 20160610 MGO
	end;

end;

{******************************** Function Header ******************************
Function Name: CargarTramos
Author : ndonadio
Date Created : 02/09/2005
Description :   Carga los tramos en el cds. Si no hay tramos, crea un DS vacio.
Parameters : CodigoTarifa: Integer; var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmABMTramosGastosSerbanc.CargarTramos(CodigoTarifa: Integer;
  var Error: AnsiString): Boolean;
begin
    Result := False;
    //Carga los tramos para una tarifa seleccionada...
    try
        { INICIO : 20160610 MGO
        spObtenerTramosTarifaSerbanc.Parameters.ParamByName('@CodigoTarifaSerbanc').Value := CodigoTarifa;
        spObtenerTramosTarifaSerbanc.Open;
        }
        spObtenerTramosTarifaGastosCobranza.Parameters.Refresh;
        spObtenerTramosTarifaGastosCobranza.Parameters.ParamByName('@CodigoTarifa').Value := CodigoTarifa;
        spObtenerTramosTarifaGastosCobranza.Open;
        // FIN : 20160610 MGO
    except
        on e:Exception do begin
            Error := e.Message;
            Exit;
        end;
    end;
    try
        if not cdsTramos.Active  then cdsTramos.Open;
        cdsTramos.EmptyDataSet;
        { INICIO : 20160610 MGO
        while  not spObtenerTramosTarifaSerbanc.Eof do begin
            cdsTramos.Append;
            cdsTramos.FieldByName('MontoInicial').Value := spObtenerTramosTarifaSerbanc.FieldByName('MontoInicial').asInteger;
            cdsTramos.FieldByName('PorcentajeGasto').Value := spObtenerTramosTarifaSerbanc.FieldByName('PorcentajeGasto').AsFloat;
            cdsTramos.Post;
            spObtenerTramosTarifaSerbanc.Next;
        end;
        }
        while  not spObtenerTramosTarifaGastosCobranza.Eof do begin
            cdsTramos.Append;
            cdsTramos.FieldByName('MontoInicial').Value := spObtenerTramosTarifaGastosCobranza.FieldByName('MontoInicial').asInteger;
            //INICIO		: 20160909 CFU
            cdsTramos.FieldByName('MontoFinal').Value	:= spObtenerTramosTarifaGastosCobranza.FieldByName('MontoFinal').asInteger;
            cdsTramos.FieldByName('MontoTramo').Value	:= spObtenerTramosTarifaGastosCobranza.FieldByName('MontoTramo').asInteger;
            //TERMINO	: 20160909 CFU
            cdsTramos.FieldByName('PorcentajeGasto').Value := spObtenerTramosTarifaGastosCobranza.FieldByName('PorcentajeGasto').AsFloat;
            cdsTramos.Post;
            spObtenerTramosTarifaGastosCobranza.Next;
        end;
        // FIN : 20160610 MGO
        Result := True;
    finally
        { INICIO : 20160610 MGO
        spObtenerTramosTarifaSerbanc.Close;
        }
        spObtenerTramosTarifaGastosCobranza.Close;
        // FIN : 20160610 MGO
        dblTramos.Refresh;
        dblTramos.Repaint;
    end;
end;

procedure TfrmABMTramosGastosSerbanc.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMTramosGastosSerbanc.AbmList1Delete(Sender: TObject);
resourcestring
    ERROR_DELETE = 'Error al intentar eliminar el registro';
    MSG_DELETE =   'Desea eliminar la Tarifa vigente a partir del %s ';
    TITLE_DELETE = 'Confirme Eliminaci�n';
begin
    try
        try
            // Solicito al usuario confirmacion de lo que quiere borrar
            if (MsgBox(Format(MSG_DELETE,[tblTarifas.FieldByName('FechaActivacion').asString ]),TITLE_DELETE,MB_ICONQUESTION+MB_YESNO) = mrYes) then begin
                { INICIO : 20160610 MGO
                BorrarTramos(tblTarifas.FieldByName('CodigoTarifaSerbanc').AsInteger);
                }
                BorrarTramos(tblTarifas.FieldByName('CodigoTarifa').AsInteger);
                // FIN : 20160610 MGO
                tblTarifas.DeleteRecords(arCurrent);
                abmList1.Repaint;
                abmList1Click(Sender);
            end;
        except
            // Se produjo un error!
            on e:exception do begin
                MsgBoxErr(ERROR_DELETE,e.Message,'ERROR',MB_ICONSTOP);
            end;
        end;
    finally
        // Cambio a modo consulta (por las dudas)
        CambiarAModo(CONSULTA);
    end;
end;

{******************************** Function Header ******************************
Function Name: BorrarTramos
Author : ndonadio
Date Created : 02/09/2005
Description :   Borra los tramos de la tarifa...
Parameters : CodigoTarifa: Integer
Return Value : None
*******************************************************************************}
procedure TfrmABMTramosGastosSerbanc.BorrarTramos(CodigoTarifa: Integer);
begin
    // Borra los tramos de la tarifa
    { INICIO : 20160610 MGO
    spBorrarTramos.Parameters.ParamByName('@CodigoTarifaSerbanc').Value := CodigoTarifa;
    }
    spBorrarTramos.Parameters.Refresh;
    spBorrarTramos.Parameters.ParamByName('@CodigoTarifa').Value := CodigoTarifa;
    // FIN : 20160610 MGO
    spBorrarTramos.ExecProc;
end;

procedure TfrmABMTramosGastosSerbanc.AbmList1Edit(Sender: TObject);
begin
// Entro en Modo Edicion
   CambiarAModo(EDICION);
end;

procedure TfrmABMTramosGastosSerbanc.AbmList1Insert(Sender: TObject);
begin
    txtCodigoTarifa.ValueInt := 0;  // 20160610 MGO
    dateActivacionTarifa.Date := NowBase(DMConnections.BaseCAC)+1;
    cdsTramos.EmptyDataSet;
    CambiarAModo(EDICION);
end;

// INICIO : 20160610 MGO
procedure TfrmABMTramosGastosSerbanc.AbmToolbar1Close(Sender: TObject);
begin
    Close;
end;
// FIN : 20160610 MGO

{******************************** Function Header ******************************
Function Name: btnAceptarClick
Author : ndonadio
Date Created : 02/09/2005
Description :   Acepte las modificaciones en una tarifa y en sus tramos...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmABMTramosGastosSerbanc.btnAceptarClick(Sender: TObject);
resourcestring
    ERROR_VALIDATING_DATE   = 'La fecha de Activacion de una Tarifa debe ser mayor que la fecha actual.';
    ERROR_INSERTING_RECORD  = 'Error insertando la tarifa actual';
    ERROR_MODIFYING_RECORD  = 'Error modificando registro.';
    ERROR_LOADING_CURRENT_VALUES= 'Error al cargar la tarifa actual.';
    ERROR_CANT_ADD_SAME_DATE    = 'No se puede agregar otra tarifa para la misma fecha.';
    ERROR_INSERT_FAILS          = 'La operacion de inserci�n ha fallado.';
    ERROR_DATA_IS_EMPTY_ALTA    = 'No se puede guardar una tarifa sin tramos cargados.';
    ERROR_DATA_IS_EMPTY_MODI    = 'No se han definido Tramos de Cobranza. Si desea eliminar la tarifa, ' + CRLF + 'Cancele la edici�n y seleccione la opci�n borrar de la Barra de ABM.';
var
    Error           : AnsiString;
    Error_Sin_Tramos: AnsiString;
    CodigoTarifa    : Integer;  // 20160610 MGO
begin
    if abmlist1.Estado = MODI then Error_Sin_Tramos := ERROR_DATA_IS_EMPTY_MODI
    else  Error_Sin_Tramos := ERROR_DATA_IS_EMPTY_ALTA;
    if not ValidateControls([dateActivacionTarifa,dblTramos],
        [(dateActivacionTarifa.Date > FFechaActual), (NOT cdsTramos.IsEmpty) ],
            Caption,[ERROR_VALIDATING_DATE, Error_Sin_Tramos]) then Exit;

{INICIO: TASK_115_JMA_20170316
try

    // Guardo la tarifa y sus datos
    if ABMList1.Estado = Alta then begin
        try
            { INICIO : 20160610 MGO
            DMConnections.BaseCAC.BeginTrans;
            // Estoy insertando un nuevo registro
            spActualizarTarifaSerbanc.Close;
            spActualizarTarifaSerbanc.Parameters.Refresh;
            spActualizarTarifaSerbanc.Parameters.ParamByName('@FechaActivacion').Value := dateActivacionTarifa.Date;
            spActualizarTarifaSerbanc.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
            spActualizarTarifaSerbanc.Parameters.ParamByName('@CodigoTarifaSerbanc').Value := NULL;
            spActualizarTarifaSerbanc.ExecProc;

            if spActualizarTarifaSerbanc.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin
                if spActualizarTarifaSerbanc.Parameters.ParamByName('@RETURN_VALUE').Value = -1 then begin
                        DMConnections.BaseCAC.RollbackTrans;
                        MsgBox(ERROR_CANT_ADD_SAME_DATE, Caption, MB_ICONWARNING);
                        Exit;
                end
                else    raise Exception.Create(ERROR_INSERT_FAILS);
            end;
            // Agrego los tramos
            if not cdsTramos.IsEmpty then begin
                cdsTramos.DisableControls;
                cdsTramos.First;
                while not cdsTramos.Eof do begin
                    spAgregarTramo.Parameters.ParamByName('@CodigoTarifaSerbanc').Value := spActualizarTarifaSerbanc.Parameters.ParamByName('@RETURN_VALUE').Value ;
                    spAgregarTramo.Parameters.ParamByName('@MontoInicial').Value := cdsTramos.FieldByName('MontoInicial').AsInteger;
                    spAgregarTramo.Parameters.ParamByName('@PorcentajeGasto').Value := cdsTramos.FieldByName('PorcentajeGasto').AsFloat;
                    spAgregarTramo.ExecProc;
                    cdsTramos.Next;
                end;
                cdsTramos.EnableControls;
            end;
            DMConnections.BaseCAC.CommitTrans;
                //
                DMConnections.BaseCAC.BeginTrans;
                // Estoy insertando un nuevo registro
                with spActualizarTarifaGastosCobranza do begin
                    Close;
                    Parameters.Refresh;
                    Parameters.ParamByName('@FechaActivacion').Value := dateActivacionTarifa.Date;
                    Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                    Parameters.ParamByName('@CodigoTarifa').Value := Null;
                    Parameters.ParamByName('@ErrorDescription').Value := Null;
                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin
                        if Parameters.ParamByName('@RETURN_VALUE').Value = -2 then begin
                            DMConnections.BaseCAC.RollbackTrans;
                            MsgBox(ERROR_CANT_ADD_SAME_DATE, Caption, MB_ICONWARNING);
                            Exit;
                        end else
                            raise Exception.Create(ERROR_INSERT_FAILS);
                    end;

                    CodigoTarifa := Parameters.ParamByName('@CodigoTarifa').Value;
                end;

                // Agrego los tramos
                if not cdsTramos.IsEmpty then begin
                    cdsTramos.DisableControls;
                    cdsTramos.First;
                    while not cdsTramos.Eof do begin
                        spAgregarTramo.Parameters.Refresh;
                        spAgregarTramo.Parameters.ParamByName('@CodigoTarifa').Value := CodigoTarifa;
                        spAgregarTramo.Parameters.ParamByName('@MontoInicial').Value := cdsTramos.FieldByName('MontoInicial').AsInteger;
                        //INICIO		: 20160909 CFU
                        spAgregarTramo.Parameters.ParamByName('@MontoFinal').Value := cdsTramos.FieldByName('MontoFinal').AsInteger;
                        spAgregarTramo.Parameters.ParamByName('@MontoTramo').Value := cdsTramos.FieldByName('MontoTramo').AsInteger;
                        //TERMINO		: 20160909 CFU
                        spAgregarTramo.Parameters.ParamByName('@PorcentajeGasto').Value := cdsTramos.FieldByName('PorcentajeGasto').AsFloat;
                        spAgregarTramo.Parameters.ParamByName('@ErrorDescription').Value := Null;
                        spAgregarTramo.ExecProc; 

                        if spAgregarTramo.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin
                            DMConnections.BaseCAC.RollbackTrans;
                            MsgBox(spAgregarTramo.Parameters.ParamByName('@ErrorDescription').Value, Caption, MB_ICONWARNING);
                            Exit;
                        end;

                        cdsTramos.Next;
                    end;
                    cdsTramos.EnableControls;
                end;
                DMConnections.BaseCAC.CommitTrans;
                // FIN : 20160610 MGO
        except
            on e:exception do begin
                if DMConnections.BaseCAC.InTRansaction then  DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(ERROR_INSERTING_RECORD, e.Message, Caption, MB_ICONERROR);
            end;
            end;
        end
        else begin
            try
                { INICIO : 20160610 MGO
                DMConnections.BaseCAC.BeginTrans;
                // Estoy insertando un nuevo registro
                spActualizarTarifaSerbanc.Close;
                spActualizarTarifaSerbanc.Parameters.Refresh;
                spActualizarTarifaSerbanc.Parameters.ParamByName('@FechaActivacion').Value := dateActivacionTarifa.Date;
                spActualizarTarifaSerbanc.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                spActualizarTarifaSerbanc.Parameters.ParamByName('@CodigoTarifaSerbanc').Value := tblTarifas.FieldByName('CodigotarifaSerbanc').asInteger;
                spActualizarTarifaSerbanc.ExecProc;

                if spActualizarTarifaSerbanc.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin
                    if spActualizarTarifaSerbanc.Parameters.ParamByName('@RETURN_VALUE').Value = -1 then begin
                            DMConnections.BaseCAC.RollbackTrans;
                            MsgBox(ERROR_CANT_ADD_SAME_DATE, Caption, MB_ICONWARNING);
                            Exit;
                    end
                    else    raise Exception.Create(ERROR_INSERT_FAILS);
                end;
                // Borro los tramos y agrego los nuevos...
                spBorrarTramos.Parameters.ParamByName('@CodigoTarifaSerbanc').Value := spActualizarTarifaSerbanc.Parameters.ParamByName('@CodigoTarifaSerbanc').Value;
                spBorrarTramos.ExecProc;
                // Agrego los tramos
                if not cdsTramos.IsEmpty then begin
                    cdsTramos.DisableControls;
                    cdsTramos.First;
                    while not cdsTramos.Eof do begin
                        spAgregarTramo.Parameters.ParamByName('@CodigoTarifaSerbanc').Value := spActualizarTarifaSerbanc.Parameters.ParamByName('@CodigoTarifaSerbanc').Value;
                        spAgregarTramo.Parameters.ParamByName('@MontoInicial').Value := cdsTramos.FieldByName('MontoInicial').AsInteger;
                        spAgregarTramo.Parameters.ParamByName('@PorcentajeGasto').Value := cdsTramos.FieldByName('PorcentajeGasto').AsFloat;
                        spAgregarTramo.ExecProc;
                        cdsTramos.Next;
                    end;
                    cdsTramos.EnableControls;
                end;
                DMConnections.BaseCAC.CommitTrans;
                //
                DMConnections.BaseCAC.BeginTrans;
                // estoy modificando un registro
                CodigoTarifa := txtCodigoTarifa.ValueInt;

                with spActualizarTarifaGastosCobranza do begin
                    Close;
                    Parameters.Refresh;
                    Parameters.ParamByName('@FechaActivacion').Value := dateActivacionTarifa.Date;
                    Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                    Parameters.ParamByName('@CodigoTarifa').Value := CodigoTarifa;
                    Parameters.ParamByName('@ErrorDescription').Value := Null;
                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin
                        if Parameters.ParamByName('@RETURN_VALUE').Value = -2 then begin
                            DMConnections.BaseCAC.RollbackTrans;
                            MsgBox(ERROR_CANT_ADD_SAME_DATE, Caption, MB_ICONWARNING);
                            Exit;
                        end else
                            raise Exception.Create(ERROR_MODIFYING_RECORD);
                    end;
                end;

                // Borro los tramos y agrego los nuevos...
                spBorrarTramos.Parameters.Refresh;
                spBorrarTramos.Parameters.ParamByName('@CodigoTarifa').Value := CodigoTarifa;
                spBorrarTramos.ExecProc;

                // Agrego los tramos
                if not cdsTramos.IsEmpty then begin
                    cdsTramos.DisableControls;
                    cdsTramos.First;
                    while not cdsTramos.Eof do begin
                        spAgregarTramo.Parameters.Refresh;
                        spAgregarTramo.Parameters.ParamByName('@CodigoTarifa').Value := CodigoTarifa;
                        spAgregarTramo.Parameters.ParamByName('@MontoInicial').Value := cdsTramos.FieldByName('MontoInicial').AsInteger;
                        spAgregarTramo.Parameters.ParamByName('@PorcentajeGasto').Value := cdsTramos.FieldByName('PorcentajeGasto').AsFloat;
                        spAgregarTramo.ExecProc;

                        if spAgregarTramo.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin
                            DMConnections.BaseCAC.RollbackTrans;
                            MsgBox(spAgregarTramo.Parameters.ParamByName('@ErrorDescription').Value, Caption, MB_ICONWARNING);
                            Exit;
                        end;
                        
                        cdsTramos.Next;
                    end;
                    cdsTramos.EnableControls;
                end;
                DMConnections.BaseCAC.CommitTrans;
                // FIN : 20160610 MGO
            except
                on e:exception do begin
                    if DMConnections.BaseCAC.InTransaction then  DMConnections.BaseCAC.RollbackTrans;
                    MsgBoxErr(ERROR_INSERTING_RECORD, e.Message, Caption, MB_ICONERROR);
                end;
            end;
        end;
    finally
        if not ObtenerTarifaVigente(Error) then begin
            MSgBoxErr(ERROR_LOADING_CURRENT_VALUES, Error, Caption, MB_ICONERROR);
            Close;
        end;
        CambiarAModo(CONSULTA);
        dblTramos.Refresh;
        dblTramos.Repaint;
    end;
}

    //VALIDACION DE TRAMOS COMPLETOS
    cdsTramos.GotoBookmark(BuscarMayorMonto('MontoInicial'));
    if cdsTramos.FieldByName('MontoFinal').Value <> 0 then
    begin
        cdsTramos.GotoBookmark(BuscarMayorMonto('MontoInicial'));
        MsgBox('El �ltimo tramo debe tener Monto Final igual a 0');   
        Exit;
    end;

    try

    // Guardo la tarifa y sus datos
        try
            DMConnections.BaseCAC.BeginTrans;
            CodigoTarifa := 0;
            if ABMList1.Estado <> Alta then
                CodigoTarifa := txtCodigoTarifa.ValueInt;

            // Estoy insertando un nuevo registro
            with spActualizarTarifaGastosCobranza do begin
                Close;
                Parameters.Refresh;
                Parameters.ParamByName('@FechaActivacion').Value := dateActivacionTarifa.Date;
                Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                Parameters.ParamByName('@CodigoTarifa').Value := CodigoTarifa;
                Parameters.ParamByName('@ErrorDescription').Value := Null;
                ExecProc;

                if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin
                    if Parameters.ParamByName('@RETURN_VALUE').Value = -2 then begin
                        DMConnections.BaseCAC.RollbackTrans;
                        MsgBox(ERROR_CANT_ADD_SAME_DATE, Caption, MB_ICONWARNING);
                        Exit;
                    end else
                    begin
                        if ABMList1.Estado = Alta then
                            raise Exception.Create(ERROR_INSERT_FAILS)
                        else
                            raise Exception.Create(ERROR_MODIFYING_RECORD);
                    end;
                end;

                CodigoTarifa := Parameters.ParamByName('@CodigoTarifa').Value;
            end;

            if ABMList1.Estado <> Alta then // Borro los tramos y agrego los nuevos...
            begin

                spBorrarTramos.Parameters.Refresh;
                spBorrarTramos.Parameters.ParamByName('@CodigoTarifa').Value := CodigoTarifa;
                spBorrarTramos.ExecProc;
            end;

            // Agrego los tramos
            if not cdsTramos.IsEmpty then begin
                cdsTramos.DisableControls;
                cdsTramos.First;
                while not cdsTramos.Eof do begin
                    spAgregarTramo.Parameters.Refresh;
                    spAgregarTramo.Parameters.ParamByName('@CodigoTarifa').Value := CodigoTarifa;
                    spAgregarTramo.Parameters.ParamByName('@MontoInicial').Value := cdsTramos.FieldByName('MontoInicial').AsInteger;
                    spAgregarTramo.Parameters.ParamByName('@MontoFinal').Value := cdsTramos.FieldByName('MontoFinal').AsInteger;
                    spAgregarTramo.Parameters.ParamByName('@MontoTramo').Value := cdsTramos.FieldByName('MontoTramo').AsInteger;
                    spAgregarTramo.Parameters.ParamByName('@PorcentajeGasto').Value := cdsTramos.FieldByName('PorcentajeGasto').AsFloat;
                    spAgregarTramo.Parameters.ParamByName('@ErrorDescription').Value := Null;
                    spAgregarTramo.ExecProc; 

                    if spAgregarTramo.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin
                        DMConnections.BaseCAC.RollbackTrans;
                        MsgBox(spAgregarTramo.Parameters.ParamByName('@ErrorDescription').Value, Caption, MB_ICONWARNING);
                        Exit;
                    end;

                    cdsTramos.Next;
                end;
                cdsTramos.EnableControls;
            end;
            DMConnections.BaseCAC.CommitTrans;
        except
            on e:exception do begin
                if DMConnections.BaseCAC.InTRansaction then  DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(ERROR_INSERTING_RECORD, e.Message, Caption, MB_ICONERROR);
            end;
            end;

    finally
        if not ObtenerTarifaVigente(Error) then begin
            MSgBoxErr(ERROR_LOADING_CURRENT_VALUES, Error, Caption, MB_ICONERROR);
            Close;
        end;
        CambiarAModo(CONSULTA);
        dblTramos.Refresh;
        dblTramos.Repaint;
    end;

{TERMINO: TASK_115_JMA_20170316}
end;

procedure TfrmABMTramosGastosSerbanc.btnCancelarClick(Sender: TObject);
begin
    CambiarAModo(CONSULTA);
    dblTramos.Refresh;
    dblTramos.Repaint;
end;

procedure TfrmABMTramosGastosSerbanc.btnAgregarTramoClick(Sender: TObject);
resourcestring
    { INICIO : 20160610 MGO
    ERROR_INSERT    = 'Error al insertar tramo';
    }
    {INICIO		: 20160909 CFU
    ERROR_INSERT    = 'El monto inicial ya existe';
    // FIN : 20160610 MGO
    }
    ERROR_INSERT    = 'El tramo o monto inicial ya existe';                //TASK_115_JMA_20170316
    //TERMINO	: 20160909 CFU
var
    f: TfrmEdicionTramosCobranza;
    montoInicio, montoFin, montoTramo: Integer;         //TASK_115_JMA_20170316
    volver : Boolean;                       //TASK_115_JMA_20170316
begin
    Application.CreateForm(TfrmEdicionTramosCobranza,f);
    try
    	{INICIO		: 20160909 CFU
        if f.Inicializar(0,0) then begin
        }
{INICIO: TASK_115_JMA_20170316
        if f.Inicializar(0,0,0,0) then begin
}
        montoInicio := 0;
        montoFin := 0;
        montoTramo := 0;
        volver := True;
        if cdsTramos.RecordCount > 0 then
        begin
            try
                cdsTramos.DisableControls;
                cdsTramos.First;
                while not cdsTramos.Eof do
                begin
                    if cdsTramos.FieldByName('MontoFinal').Value > montoInicio then
                    begin
                        montoInicio := cdsTramos.FieldByName('MontoFinal').Value;
                        //montoFin := cdsTramos.FieldByName('MontoInicial').Value;
                    end;
                    cdsTramos.Next;
                end;
                montoInicio := montoInicio + 1;
                montoFin := montoInicio + 1;
                montoTramo := montoFin - montoInicio +1;
            finally
                cdsTramos.EnableControls;
            end;
        end;
        if f.Inicializar(montoInicio,montoFin,montoTramo,0, True) then begin
{TERMINO: TASK_115_JMA_20170316}
        //TERMINO	: 20160909 CFU
            while volver do                                              //TASK_115_JMA_20170316
            begin                                                        //TASK_115_JMA_20170316
                volver:= False;                                          //TASK_115_JMA_20170316
                if f.ShowModal = mrOK then begin
                    // Agrego el tramo...
                    cdsTramos.Insert;
                    cdsTramos.FieldByName('MontoInicial').Value := StrToInt(f.txtInicioTramo.Text);
                    //INICIO		: 20160909 CFU
                    cdsTramos.FieldByName('MontoFinal').Value := StrToInt(f.txtFinTramo.Text);
                    cdsTramos.FieldByName('MontoTramo').Value := StrToInt(f.txtMontoTramo.Text);
                    //TERMINO	: 20160909 CFU
                    cdsTramos.FieldByName('PorcentajeGasto').Value := StrToFloat(f.txtPorcentajeTramo.Text);
                    try
                        cdsTramos.Post;                       
                    except
                        on e:exception do begin
                            cdsTramos.Cancel;
                            { INICIO : 20160610 MGO
                            MsgBoxErr(ERROR_INSERT, e.Message, Caption, MB_ICONERROR);
                            }
                            MsgBox(ERROR_INSERT, Caption, MB_ICONEXCLAMATION+MB_OK);
                            // FIN : 20160610 MGO
                            volver:= True;                                    //TASK_115_JMA_20170316
                        end;
                    end;
                end
            end;
        end;
    finally
        f.Release;
		//dblTramos.Refresh;						//TASK_115_JMA_20170316
        //dblTramos.Repaint;						//TASK_115_JMA_20170316
        dblTramos.DataSource.DataSet := cdsTramos;	//TASK_115_JMA_20170316
        HabilitarBotonesTramos(True);
    end;
end;

procedure TfrmABMTramosGastosSerbanc.btnEditarTramoClick(Sender: TObject);
resourcestring
    { INICIO : 20160610 MGO
    ERROR_EDITING    = 'Error al editar tramo';
    }
    ERROR_EDITING    = 'El monto inicial ya existe';
    // FIN : 20160610 MGO
var
    f: TfrmEdicionTramosCobranza;
    volver, EsUltimo: Boolean;                //TASK_115_JMA_20170316
    bm: TBookmark;                  //TASK_115_JMA_20170316
begin
    Application.CreateForm(TfrmEdicionTramosCobranza,f);
    try
    	{INICIO		: 20160909 CFU
        if f.Inicializar(cdsTramos.FieldByName('MontoInicial').asInteger,cdsTramos.FieldByName('PorcentajeGasto').asFloat) then begin
        }
{INICIO: TASK_115_JMA_20170316}
        bm := cdstramos.GetBookmark;
        EsUltimo := cdsTramos.CompareBookmarks(bm, BuscarMayorMonto('MontoFinal')) = 0; //TASK_115_JMA_20170316}
        if f.Inicializar(cdsTramos.FieldByName('MontoInicial').asInteger,
        				 cdsTramos.FieldByName('MontoFinal').asInteger,
                         cdsTramos.FieldByName('MontoTramo').asInteger,
        				 cdsTramos.FieldByName('PorcentajeGasto').asFloat,
                         EsUltimo) then begin
        //TERMINO	: 20160909 CFU
{INICIO: TASK_115_JMA_20170316}
            volver := True;
            while volver do
            begin
                volver := False;
                if f.ShowModal = mrOK then begin
                
                    //Validaci�n de superposici�n
                    if cdsTramos.RecordCount > 0 then 
                    begin
                        try
                            cdsTramos.DisableControls;
                            cdsTramos.First;
                            while not cdsTramos.Eof do
                            begin
                                if (cdsTramos.CompareBookmarks(bm, cdsTramos.GetBookmark) <> 0) then
                                begin
                                    if ((StrToInt(f.txtFinTramo.Text) >= cdsTramos.FieldByName('MontoInicial').Value)
                                        and
                                        (StrToInt(f.txtFinTramo.Text) <= cdsTramos.FieldByName('MontoFinal').Value))
                                        or
                                        ((StrToInt(f.txtInicioTramo.Text) < cdsTramos.FieldByName('MontoFinal').Value)
                                        and
                                        (StrToInt(f.txtFinTramo.Text) > cdsTramos.FieldByName('MontoFinal').Value))
                                          then
                                    begin
                                        MsgBox('El monto fin no puede estar entre los tramos existentes');
                                        volver := True;
                                        break;
                                    end;
                                end;
                                cdsTramos.Next;
                            end;
                        finally
                            cdsTramos.EnableConstraints;
                        end;
                    end;

                    // Agrego el tramo
                    if not volver then
                    begin
{TERMINO: TASK_115_JMA_20170316}
                        try
                            cdsTramos.GotoBookmark(bm);
                            cdsTramos.Edit;
                            cdsTramos.FieldByName('MontoInicial').Value := StrToInt(f.txtInicioTramo.Text);
                            //INICIO		: 20160909 CFU
                             cdsTramos.FieldByName('MontoFinal').Value	:= StrToInt(f.txtFinTramo.Text);
                             cdsTramos.FieldByName('MontoTramo').Value	:= StrToInt(f.txtMontoTramo.Text);
                            //TERMINO	: 20160909 CFU
                            cdsTramos.FieldByName('PorcentajeGasto').Value := StrToFloat(f.txtPorcentajeTramo.Text);
                            cdsTramos.Post;
                        except
                            on e:exception do begin
                                cdsTramos.Cancel;
                                { INICIO : 20160610 MGO
                                MsgBoxErr(ERROR_EDITING, e.Message, Caption, MB_ICONERROR);
                                }
                                MsgBox(ERROR_EDITING, Caption, MB_ICONEXCLAMATION+MB_OK);
                                // FIN : 20160610 MGO
                                volver := True;                        //TASK_115_JMA_20170316
                            end;
                        end;                                           //TASK_115_JMA_20170316
                    end
                end;                                               //TASK_115_JMA_20170316
            end;
        end;
    finally
        f.Release;
		//dblTramos.Refresh;						//TASK_115_JMA_20170316
        //dblTramos.Repaint;						//TASK_115_JMA_20170316
        dblTramos.DataSource.DataSet := cdsTramos;	//TASK_115_JMA_20170316
        HabilitarBotonesTramos(True);
        if bm <> nil then                            //TASK_115_JMA_20170316
            cdsTramos.FreeBookmark(bm);              //TASK_115_JMA_20170316
    end;
end;

procedure TfrmABMTramosGastosSerbanc.btnEliminarTramoClick(Sender: TObject);
begin
{INICIO: TASK_115_JMA_20170316
    cdsTramos.Delete
    dblTramos.Refresh;
}
    if cdsTramos.CompareBookmarks(cdsTramos.GetBookmark, BuscarMayorMonto('MontoInicial')) = 0 then begin
        cdsTramos.Delete;
        dblTramos.Refresh;
    end
    else
        MsgBox('Solo se puede eliminar el ultimo tramo');
{TERMINO: TASK_115_JMA_20170316}
    HabilitarBotonesTramos(True);
end;

procedure TfrmABMTramosGastosSerbanc.dblTramosDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: string;
  var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column.Index = 0 then begin
    // es el monto... formateo a entero "UF"
        if TRIM(Text) <> '' then
            Text := cdsTramos.FieldByName('MontoInicial').asString + ' UF'
    end
    //INICIO		: 20160909 CFU
    else if Column.Index = 1 then begin
    // es el monto... formateo a entero "UF"
        if TRIM(Text) <> '' then
            Text := cdsTramos.FieldByName('MontoFinal').asString + ' UF'
    end
    else if Column.Index = 2 then begin
    // es el monto... formateo a entero "UF"
        if TRIM(Text) <> '' then
            Text := cdsTramos.FieldByName('MontoTramo').asString + ' UF'
    end
    //TERMINO	: 20160909 CFU
    else begin
    // es el porcentaje... formateo como procentaje
        if TRIM(Text) <> '' then
            Text := FormatFloat( FORMATO_PORCENTAJE, cdsTramos.FieldByName('PorcentajeGasto').asFloat) + ' %';
    end;
end;

{******************************** Function Header ******************************
Function Name: HabilitarBotonesTramos
Author : ndonadio
Date Created : 13/09/2005
Description :  Habilita los botones de tramos o no segun el parametro y
                el estado actual del recordset...
Parameters : Habilitar: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmABMTramosGastosSerbanc.HabilitarBotonesTramos(Habilitar: Boolean);
begin
    btnAgregarTramo.Enabled     := Habilitar    // Le estoy pidiendo que habilite...
                                AND (cdsTramos.Active)                  // el RS est� activo
                                AND (dateActivacionTarifa.Enabled );    // se est� editando;

    btnEditarTramo.Enabled      := Habilitar    // Le estoy pidiendo que habilite...
                                AND (cdsTramos.Active)                  // el RS est� activo
                                AND (NOT cdsTramos.IsEmpty)             // el RS no est� vacio...
                                AND (dateActivacionTarifa.Enabled );    // se est� editando;

    btnEliminarTramo.Enabled    := Habilitar    // Le estoy pidiendo que habilite...
                                AND (cdsTramos.Active)                  // el RS est� activo
                                AND (NOT cdsTramos.IsEmpty)             // el RS no est� vacio...
                                AND (dateActivacionTarifa.Enabled );    // se est� editando

end;

{INICIO: TASK_115_JMA_20170316}
function TfrmABMTramosGastosSerbanc.BuscarMayorMonto(Campo: string): TBookmark;
var
    mayor : Integer;
    bm: TBookmark;
begin
     Result := nil;
     mayor := 0;
     cdsTramos.DisableControls;
     bm := cdsTramos.GetBookmark;
     try
        cdsTramos.First;
        while not cdsTramos.Eof do
        begin
            if cdsTramos.FieldByName(Campo).Value > mayor then
            begin
                mayor := cdsTramos.FieldByName(Campo).Value;
                Result := cdsTramos.GetBookmark;
            end;
            cdsTramos.Next;
        end;
     finally
         cdsTramos.EnableControls;
         if cdsTramos.BookmarkValid(bm) then
         begin
             cdsTramos.GotoBookmark(bm);
             cdsTramos.FreeBookmark(bm);
         end;
     end;
end;
{TERMINO: TASK_115_JMA_20170316}

end.
