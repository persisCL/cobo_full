object FrmFuentesOrigenContacto: TFrmFuentesOrigenContacto
  Left = 280
  Top = 162
  Width = 600
  Height = 403
  Caption = 'Mantenimiento de Fuentes de Contacto'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 208
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'86'#0'C'#243'digo               '
      #0'181'#0'Descripci'#243'n                                       '
      #0'84'#0'Orden a Mostrar')
    HScrollBar = True
    RefreshTime = 100
    Table = FuentesOrigenContacto
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 241
    Width = 592
    Height = 89
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 15
      Top = 38
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txt_Descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 15
      Top = 12
      Width = 39
      Height = 13
      Caption = '&C'#243'digo: '
      FocusControl = txt_CodigoFuenteOrigenContacto
    end
    object Label2: TLabel
      Left = 15
      Top = 65
      Width = 96
      Height = 13
      Caption = '&Orden a Mostrar:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Descripcion: TEdit
      Left = 160
      Top = 34
      Width = 305
      Height = 21
      Color = 16444382
      MaxLength = 40
      TabOrder = 1
    end
    object txt_CodigoFuenteOrigenContacto: TNumericEdit
      Left = 160
      Top = 8
      Width = 69
      Height = 21
      TabStop = False
      Color = clBtnFace
      Enabled = False
      MaxLength = 3
      ReadOnly = True
      TabOrder = 0
      Decimals = 0
    end
    object txt_OrdenMostrar: TNumericEdit
      Left = 160
      Top = 60
      Width = 70
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 2
      Decimals = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 330
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 395
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object FuentesOrigenContacto: TADOTable
    Connection = DMConnections.BaseCAC
    IndexFieldNames = 'descripcion'
    TableName = 'FuentesOrigenContacto'
    Left = 242
    Top = 110
    object FuentesOrigenContactoCodigoFuenteOrigenContacto: TWordField
      Alignment = taCenter
      FieldName = 'CodigoFuenteOrigenContacto'
    end
    object FuentesOrigenContactoDescripcion: TStringField
      FieldName = 'Descripcion'
      FixedChar = True
      Size = 50
    end
    object FuentesOrigenContactoOrdenMostrar: TWordField
      Alignment = taCenter
      FieldName = 'OrdenMostrar'
    end
  end
end
