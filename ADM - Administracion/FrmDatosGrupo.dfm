object FormDatosGrupo: TFormDatosGrupo
  Left = 236
  Top = 198
  BorderStyle = bsDialog
  Caption = 'Datos del Grupo'
  ClientHeight = 96
  ClientWidth = 374
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 13
    Top = 15
    Width = 32
    Height = 13
    Caption = 'Grupo:'
  end
  object Label2: TLabel
    Left = 13
    Top = 39
    Width = 59
    Height = 13
    Caption = 'Descripci'#243'n:'
  end
  object txt_codigogrupo: TEdit
    Left = 85
    Top = 12
    Width = 145
    Height = 21
    MaxLength = 20
    TabOrder = 0
    OnChange = txt_codigogrupoChange
  end
  object txt_descripcion: TEdit
    Left = 85
    Top = 36
    Width = 281
    Height = 21
    MaxLength = 60
    TabOrder = 1
  end
  object btn_ok: TButton
    Left = 215
    Top = 68
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    Enabled = False
    TabOrder = 2
    OnClick = btn_okClick
  end
  object Button2: TButton
    Left = 295
    Top = 68
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 3
  end
  object spGruposSistemas: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_GruposSistemas_SELECT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGrupo'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 8
    Top = 64
  end
  object spUsuariosSistemas_1: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_GruposSistemas_SELECT_2'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGrupo'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 40
    Top = 64
  end
  object spINSGruposSistemas: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_GruposSistemas_INSERT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGrupo'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 72
    Top = 64
  end
  object spUPDUsuariosSistemas: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_GruposSistemas_UPDATE'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGrupo'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Descripcion'
        DataType = ftString
        Precision = 60
        Value = Null
      end>
    Left = 104
    Top = 64
  end
end
