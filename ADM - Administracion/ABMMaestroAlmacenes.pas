{********************************** File Header ********************************
File Name   : ABMMaestroAlmacenes.pas
Author      : dgonzales
Date Created: 10/06/2004
Language    : ES-AR
Description : ABM para Maestro almacenes.
*******************************************************************************
Historial de Revisiones
*******************************************************************************
Date		: 16/06/2004
Author		: Chiappero, Conrado <cchiappero@dpsautomation.com>
Description : Administraci�n de Operaciones de Envio y Revisi�n para el almacen

Revision :2
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura


*******************************************************************************}



unit ABMMaestroAlmacenes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls,
  PeaProcs, Peatypes, validate, Dateedit, RStrings, ADODB, DPSControls,
  FreDomicilio, VariantComboBox, Util, Variants, CheckLst;
type
  TFormABMMaestroAlmacenes = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    Panel2: TPanel;
    Notebook: TNotebook;
    ObtenerMaestroAlmacenes: TADOStoredProc;
    ActualizarMaestroAlmacenes: TADOStoredProc;
    EliminarMaestroAlmacen: TADOStoredProc;
    ActualizarDomicilio: TADOStoredProc;
    pgMaestroAlmacenes: TPageControl;
    tsAlmacenes: TTabSheet;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    txt_Descripcion: TEdit;
    txt_CodigoAlmacen: TNumericEdit;
    cbBodega: TComboBox;
    cbUsuarioSupervisor: TVariantComboBox;
    cbSoloNominado: TCheckBox;
    GBDomicilios: TGroupBox;
    freDomicilio: TFrameDomicilio;
    tsOperacionesDestino: TTabSheet;
    tsOperacionesOrigen: TTabSheet;
    chkOperacionesRecepcion: TCheckListBox;
    chkOperacionesEnvio: TCheckListBox;
    ObtenerOperacionesOrigen: TADOStoredProc;
    ObtenerOperacionesDestino: TADOStoredProc;
    ObtenerOperacionesTags: TADOStoredProc;
    ActualizarOperacionesOrigen: TADOStoredProc;
    ActualizarOperacionesDestino: TADOStoredProc;
    ValidarAlmacenNoAsignado: TADOStoredProc;
    AlmacenValidoSinOperacion: TADOQuery;
    ObtenerSaldosTAGsAlmacen: TADOStoredProc;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
	procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    function DBList1Process(Tabla: TDataSet; var Texto: String): Boolean;
    procedure chkOperacionesRecepcionClickCheck(Sender: TObject);
    procedure chkOperacionesEnvioClickCheck(Sender: TObject);
  private
    { Private declarations }
    EstadoAnteriorNominado: boolean;
    Domicilio: TDatosDomicilio;
    procedure HabilitarCamposEdicion(habilitar : Boolean);
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
    procedure CargarOperacionesOrigen(CodigoAlmacen : integer);
    procedure CargarOperacionesDestino(CodigoAlmacen : integer);
    procedure CargarOperaciones(chklist : TCheckListBox);
    procedure GuardarOperacionesOrigen(CodigoAlmacen : integer);
    procedure GuardarOperacionesDestino(CodigoAlmacen : integer);
    procedure ValidarAlmacenesAsignados(CodigoAlmacen : integer; ValidoAlmacenProveedor, ValidoAlmacenRecepcion, ValidoVenta, ValidoComodato : boolean);
    function GetChecked(chklist : TCheckListBox): string;
    procedure CheckOperaciones(chkOperaciones : TCheckListBox);
  public
    { Public declarations }
	function Inicializa(Caption: TCaption; MDIChild: Boolean): Boolean;
  end;

var
  FormABMMaestroAlmacenes: TFormABMMaestroAlmacenes;

implementation

uses DMConnection;
resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Almac�n seleccionado?';
	MSG_DELETE_ERROR		= 'No se pudo eliminar el Almac�n seleccionado.';
    MSG_DELETE_CAPTION		= 'Eliminar Almac�n';
    MSG_ACTUALIZAR_ERROR    = 'No se pudieron actualizar los datos del Almac�n.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Almac�n';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n';
    MSG_ERROR_REFERENCIADO  = 'Existen movimientos para el Almac�n seleccionado por lo que no se podr� borrar.';
{$R *.DFM}

function TFormABMMaestroAlmacenes.Inicializa(Caption: TCaption; MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
	if not OpenTables([ObtenerMaestroAlmacenes]) then exit;
    CargarBodegas(DMConnections.BaseCAC, cbBodega,0, True);
    CargarUsuariosSistemas(DMConnections.BaseCAC, cbUsuarioSupervisor,'',True);
	Notebook.PageIndex := 0;
	DBList1.Reload;
    HabilitarCamposEdicion(false);
    freDomicilio.Inicializa;
    freDomicilio.Limpiar;
	Result := True;
end;

procedure TFormABMMaestroAlmacenes.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFormABMMaestroAlmacenes.DBList1Click(Sender: TObject);
begin
    pgMaestroAlmacenes.ActivePageIndex := 0;

	with (Sender AS TDbList).Table do begin
        txt_CodigoAlmacen.Value:= ObtenerMaestroAlmacenes.fieldbyname('CodigoAlmacen').AsInteger;
        txt_descripcion.Text:= trim(ObtenerMaestroAlmacenes.fieldbyname('Descripcion').AsString);
        if ObtenerMaestroAlmacenes.fieldbyname('DescripcionBodega').IsNull then
            cbBodega.ItemIndex := 0
        else
            CargarBodegas(DMConnections.BaseCAC,cbBodega, ObtenerMaestroAlmacenes.fieldbyname('CodigoBodega').AsInteger, True);
        if ObtenerMaestroAlmacenes.fieldbyname('CodigoUsuarioSupervisor').IsNull then
            cbUsuarioSupervisor.ItemIndex := 0
        else
            MarcarItemComboVariant(cbUsuarioSupervisor,
                                ObtenerMaestroAlmacenes.Fieldbyname('CodigoUsuarioSupervisor').AsString, false);
        if ObtenerMaestroAlmacenes.Fieldbyname('SoloNominado').IsNull then cbSoloNominado.Checked := False
        else cbSoloNominado.Checked := ObtenerMaestroAlmacenes.Fieldbyname('SoloNominado').AsBoolean;

        if (ObtenerMaestroAlmacenes.FieldByName('CodigoDomicilio').IsNull) then begin
            freDomicilio.Limpiar;
        end
        else begin
            with Domicilio do begin
                CodigoDomicilio := FieldByName('CodigoDomicilio').AsInteger;
                CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
                CodigoSegmento := iif(FieldByName('CodigoSegmento').IsNull, -1, FieldByName('CodigoSegmento').AsInteger);
                NumeroCalleSTR := FieldByName('Numero').AsString;
                NumeroCalle := FormatearNumeroCalle(FieldByName('Numero').AsString);
                Detalle := FieldByName('Detalle').AsString;
                CodigoPais := FieldByName('CodigoPais').AsString;
                CodigoRegion := FieldByName('CodigoRegion').AsString;
                CodigoComuna := FieldByName('CodigoComuna').AsString;
                CalleDesnormalizada := iif(FieldByName('CodigoCalle').IsNull, FieldByName('CalleDesnormalizada').AsString,'');
                //DescripcionCiudad := iif(FieldByName('DescripcionCiudad').IsNull, '', Trim(FieldByName('DescripcionCiudad').AsString));
                CodigoPostal := FieldByName('CodigoPostal').AsString;
            end;
            FreDomicilio.CargarDatosDomicilio(Domicilio);
        end;
        if not FreDomicilio.Enabled then
            FreDomicilio.cb_Comunas.Enabled := false;
        CargarOperacionesOrigen(ObtenerMaestroAlmacenes.fieldbyname('CodigoAlmacen').AsInteger);
        CargarOperacionesDestino(ObtenerMaestroAlmacenes.fieldbyname('CodigoAlmacen').AsInteger);

    end;
end;

procedure TFormABMMaestroAlmacenes.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        TextOut(Cols[0], Rect.Top, FieldByName('CodigoAlmacen').AsString);
        TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
        TextOut(Cols[2], Rect.Top, iif(Tabla.FieldByName('SoloNominado').AsBoolean,MSG_SI,MSG_NO));
        TextOut(Cols[3], Rect.Top, FieldByName('DescripcionBodega').AsString);
        TextOut(Cols[4], Rect.Top, FieldByName('DescripcionUsuarioSupervisor').AsString);
	end;
end;

procedure TFormABMMaestroAlmacenes.DBList1Edit(Sender: TObject);
begin
    EstadoAnteriorNominado := cbSoloNominado.Checked;
    pgMaestroAlmacenes.ActivePageIndex := 0;
    HabilitarCamposEdicion(True);
    DBList1Click(Sender);
    txt_Descripcion.setFocus;
end;

procedure TFormABMMaestroAlmacenes.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormABMMaestroAlmacenes.Limpiar_Campos();
begin
    pgMaestroAlmacenes.ActivePageIndex := 0;
    txt_CodigoAlmacen.Clear;
	txt_Descripcion.Clear;
    cbSoloNominado.Checked := false;
    cbBodega.ItemIndex := 0;
    cbUsuarioSupervisor.ItemIndex := 0;
    freDomicilio.Inicializa;
    freDomicilio.Limpiar;
    CargarOperaciones(chkOperacionesRecepcion);
    CargarOperaciones(chkOperacionesEnvio);
end;

procedure TFormABMMaestroAlmacenes.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormABMMaestroAlmacenes.DBList1Delete(Sender: TObject);
resourcestring
	MSG_EXISTEN_PUNTOS_ENTREGAN = 'No es posible eliminar el alm�cen porque existen puntos de entrega asociados.';
	MSG_EXISTE_SALDO = 'No es posible eliminar el alm�cen porque tiene un saldo.';
	MSG_EXISTEN_MOVIMIENTOS = 'No es posible eliminar el alm�cen porque existen movimientos de stock asociados.';
	MSG_EXISTEN_REG_HISTORICO = 'No es posible eliminar el alm�cen porque existen registros en el hist�rico de cambios de ubicaci�n.';
	MSG_EXISTEN_TRANSPORTISTA = 'No es posible eliminar el alm�cen est� asociado a transportistas.';
	MSG_DELETE_ERROR = 'No se puede eliminar el Almac�n seleccionado.';

begin
	if ObtenerMaestroAlmacenes.FieldbyName('CodigoAlmacen').Value <= 5 then begin
		MsgBox(MSG_DELETE_ERROR, MSG_DELETE_CAPTION, MB_OK + MB_ICONERROR);

		DbList1.Estado     := Normal;
		DbList1.Enabled    := True;
		Notebook.PageIndex := 0;

		Exit
	end;

	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        try
            with EliminarMaestroAlmacen do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoAlmacen').Value := ObtenerMaestroAlmacenes.FieldbyName('CodigoAlmacen').Value;
                Parameters.ParamByName('@Resultado').Value := 0;
                ExecProc;
                if Parameters.ParamByName('@Resultado').Value = 1 then
                    MsgBox(MSG_EXISTEN_PUNTOS_ENTREGAN, MSG_DELETE_CAPTION, MB_ICONSTOP)
                else if Parameters.ParamByName('@Resultado').Value = 2 then
                    MsgBox(MSG_EXISTE_SALDO, MSG_DELETE_CAPTION, MB_ICONSTOP)
                else if Parameters.ParamByName('@Resultado').Value = 3 then
                    MsgBox(MSG_EXISTEN_MOVIMIENTOS, MSG_DELETE_CAPTION, MB_ICONSTOP)
                else if Parameters.ParamByName('@Resultado').Value = 4 then
                    MsgBox(MSG_EXISTEN_REG_HISTORICO, MSG_DELETE_CAPTION, MB_ICONSTOP)
                else if Parameters.ParamByName('@Resultado').Value = 5 then
                    MsgBox(MSG_EXISTEN_TRANSPORTISTA, MSG_DELETE_CAPTION, MB_ICONSTOP);

                close;
            end;
        Except
            On E: Exception do begin
                MsgBox(MSG_ERROR_REFERENCIADO, MSG_DELETE_CAPTION, MB_ICONSTOP);
            end;
        end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormABMMaestroAlmacenes.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    HabilitarCamposEdicion(True);

	txt_Descripcion.SetFocus;
end;

procedure TFormABMMaestroAlmacenes.BtnAceptarClick(Sender: TObject);
var
    sp:TADOStoredProc;
    CodigoDomicilio: integer;
    CodigoAlmacen : integer;
begin
    if not ValidateControls([txt_Descripcion, cbBodega],
                [trim(txt_Descripcion.text) <> '',
                cbBodega.itemindex > 0],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_DESCRIPCION,
                 MSG_VALIDAR_BODEGA]) then exit;

    //Verifico que quiere nominarlo no tenga Stock
    if (DBList1.Estado=modi) and (not EstadoAnteriorNominado) and (cbSoloNominado.Checked) then begin
        ObtenerSaldosTAGsAlmacen.Close;
        ObtenerSaldosTAGsAlmacen.Parameters.ParamByName('@CodigoAlmacen').Value := ObtenerMaestroAlmacenes.FieldbyName('CodigoAlmacen').Value;
        ObtenerSaldosTAGsAlmacen.Open;
        ObtenerSaldosTAGsAlmacen.First;
        while not ObtenerSaldosTAGsAlmacen.Eof do begin
            if (ObtenerSaldosTAGsAlmacen.FieldByName('Saldo').AsInteger > 0) then begin
                MsgBox('No se puede nominar este almac�n mientras tenga stock.',self.Caption,MB_ICONSTOP);
                Exit;
            end;
            ObtenerSaldosTAGsAlmacen.Next;
        end;
    end;
    if (DBList1.Estado=modi) and (EstadoAnteriorNominado) and (not cbSoloNominado.Checked) and
	   (MsgBox('�Est� seguro de cambiar este almac�n a no nominado?', self.caption, MB_YESNO + MB_ICONQUESTION) <> IDYES) then begin
        Exit; 
    end;

    if not (FreDomicilio.ValidarDomicilio) then Exit;
//    CodigoDomicilio := -1;
 	Screen.Cursor := crHourGlass;
    sp:=TADOStoredProc.Create(nil);
    try
        try
            With DbList1.Table do begin
                DMConnections.BaseCAC.BeginTrans;
                sp.Connection:=DMConnections.BaseCAC;
                sp.ProcedureName:='ActualizarDomicilio';
                with sp.Parameters do begin
                    Refresh;
                    ParamByName('@CodigoTipoDomicilio').Value:=TIPO_DOMICILIO_COMERCIAL;
                    ParamByName('@CodigoPais').Value:=PAIS_CHILE;
                    ParamByName('@CodigoRegion').Value:=freDomicilio.Region;
                    ParamByName('@CodigoComuna').Value:=freDomicilio.Comuna;
                    ParamByName('@CodigoCalle').Value:= iif(freDomicilio.EsCalleNormalizada, freDomicilio.CodigoCalle, null);
                    ParamByName('@CodigoSegmento').Value := iif(freDomicilio.EsCalleNormalizada, freDomicilio.CodigoSegmento, null);
                    ParamByName('@CalleDesnormalizada').Value:=iif(not (freDomicilio.EsCalleNormalizada),
                                                                freDomicilio.DescripcionCalle, null);
                    ParamByName('@DescripcionCiudad').Value := Null;
                    ParamByName('@Numero').Value:= freDomicilio.Numero ;
                    ParamByName('@Piso').Value:=null;
                    ParamByName('@Depto').Value:=null;
                    ParamByName('@CodigoPostal').Value:= freDomicilio.CodigoPostal;
                    ParamByName('@Detalle').Value:= freDomicilio.Detalle;
                    ParamByName('@CodigoDomicilio').Value:= iif(freDomicilio.CodigoDomicilio < 0, null, freDomicilio.CodigoDomicilio);
                end;
                sp.ExecProc;
                CodigoDomicilio := sp.Parameters.ParamByName('@CodigoDomicilio').Value;
                sp.close;
                with ActualizarMaestroAlmacenes do begin // actualizo Almacen
                    close;
                    Parameters.Refresh;
                    if DBList1.Estado=Alta then
                        Parameters.ParamByName('@CodigoAlmacen').Value := -1
                    else
                        Parameters.ParamByName('@CodigoAlmacen').Value := ObtenerMaestroAlmacenes.FieldbyName('CodigoAlmacen').Value;
                    Parameters.ParamByName('@Descripcion').Value  := Trim(txt_descripcion.Text);
                    Parameters.ParamByName('@CodigoBodega').Value  := IIf((cbBodega.ItemIndex < 1), NULL, StrRight(cbBodega.items[cbBodega.ItemIndex], 10));
                    Parameters.ParamByName('@SoloNominado').Value  := cbSoloNominado.Checked;
                    Parameters.ParamByName('@CodigoDomicilio').Value := IIf(CodigoDomicilio < 0, null, CodigoDomicilio);
                    Parameters.ParamByName('@CodigoUsuarioSupervisor').Value := iif(cbUsuarioSupervisor.ItemIndex < 1, null, cbUsuarioSupervisor.value);
                    ExecProc;
                    CodigoAlmacen:=Parameters.ParamByName('@CodigoAlmacen').Value;
                end;

                GuardarOperacionesOrigen(CodigoAlmacen);
                GuardarOperacionesDestino(CodigoAlmacen);

                DMConnections.BaseCAC.CommitTrans;
            end;
        except
            On E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
            end;
        end;
    finally
        HabilitarCamposEdicion(False); // deshabilitos los combos
        DBList1.Reload;
        DBList1.Estado:=Normal;
        DBList1.SetFocus;
        Screen.Cursor  := crDefault;
        sp.Free;
    end;
	Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormABMMaestroAlmacenes.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormABMMaestroAlmacenes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormABMMaestroAlmacenes.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormABMMaestroAlmacenes.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
//    DBList1.Reload;
	DbList1.SetFocus;
    HabilitarCamposEdicion(false);
	Notebook.PageIndex := 0;
    pgMaestroAlmacenes.ActivePageIndex := 0;
end;

function TFormABMMaestroAlmacenes.DBList1Process(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Texto := Tabla.FieldByName('CodigoAlmacen').AsString + ' ' +
	  Tabla.FieldByName('Descripcion').AsString;
	Result := True;
end;

procedure TFormABMMaestroAlmacenes.HabilitarCamposEdicion(habilitar: Boolean);
begin
	DbList1.Enabled := not(habilitar);
    AbmToolbar1.Enabled := not(habilitar);
	Notebook.PageIndex := ord(habilitar);

    groupb.Enabled := habilitar;
    EnableControlsInContainer(GroupB, habilitar);
    txt_Descripcion.Enabled := habilitar;
    chkOperacionesRecepcion.Enabled := habilitar;
    chkOperacionesEnvio.Enabled := habilitar;
    (*
    groupb.Enabled := habilitar;
    txt_descripcion.Enabled := habilitar;
    cbSoloNominado.Enabled := habilitar;
    cbBodega.Enabled := habilitar;
    cbUsuarioSupervisor.Enabled := habilitar;
    GBDomicilios.Enabled := habilitar;
    *)
end;

procedure TFormABMMaestroAlmacenes.CargarOperacionesDestino(
  CodigoAlmacen: integer);
var
 index : integer;
begin
  try
    CargarOperaciones(chkOperacionesRecepcion);
    with ObtenerOperacionesDestino do
    begin
        Parameters.ParamByName('@CodigoAlmacen').Value := CodigoAlmacen;
        Open;
        while not eof do begin
            index := chkOperacionesRecepcion.Items.IndexOf(FieldByName('Operacion').AsString);
            if index >= 0 then chkOperacionesRecepcion.Checked[index] := true;
            next;
        end;
    end;
  finally
        ObtenerOperacionesDestino.Close;
  end;
end;

procedure TFormABMMaestroAlmacenes.CargarOperacionesOrigen(
  CodigoAlmacen: integer);
var
 index : integer;
begin
 try
    CargarOperaciones(chkOperacionesEnvio);
    with ObtenerOperacionesOrigen do
    begin
        Parameters.ParamByName('@CodigoAlmacen').Value := CodigoAlmacen;
        Open;


        while not eof do begin
            index := chkOperacionesEnvio.Items.IndexOf(FieldByName('Operacion').AsString);
            if index >= 0 then chkOperacionesEnvio.Checked[index] := true;
            next;
        end;
    end;
 finally
     ObtenerOperacionesOrigen.Close;
 end;
end;




procedure TFormABMMaestroAlmacenes.CargarOperaciones(chklist: TCheckListBox);
begin
    chklist.Items.Clear;

    
    if ObtenerOperacionesTags.Active then
       ObtenerOperacionesTags.First
    else
       ObtenerOperacionesTags.Open;

    while not ObtenerOperacionesTags.eof do
    begin
          chkList.AddItem(ObtenerOperacionesTags.FieldByName('Descripcion').AsString,
                        TObject(ObtenerOperacionesTags.FieldByName('CodigoOperacion').AsInteger));
          ObtenerOperacionesTags.next;
    end;

end;

procedure TFormABMMaestroAlmacenes.GuardarOperacionesDestino(
  CodigoAlmacen: integer);
var i : integer;
begin
    QueryExecute(DMConnections.BaseCAC, 'Delete From AlmacenesDestinoOperacion Where CodigoAlmacen = ' + IntToStr(CodigoAlmacen));
    for i := 0 to chkOperacionesRecepcion.Items.Count - 1 do
    begin
        if chkOperacionesRecepcion.Checked[i] then
        begin
           with ActualizarOperacionesDestino do
           begin
                 Parameters.ParamByName('@CodigoAlmacen').Value := CodigoAlmacen;
                 Parameters.ParamByName('@CodigoOperacion').Value := integer(chkOperacionesRecepcion.Items.Objects[i]);
                 ExecProc;
           end;
        end;
    end;
end;

procedure TFormABMMaestroAlmacenes.GuardarOperacionesOrigen(
  CodigoAlmacen: integer);
var i : integer;
begin
    QueryExecute(DMConnections.BaseCAC, 'Delete From AlmacenesOrigenOperacion Where CodigoAlmacen = ' + IntToStr(CodigoAlmacen));
    for i := 0 to chkOperacionesEnvio.Items.Count - 1 do
    begin
        if chkOperacionesEnvio.Checked[i] then
        begin
           with ActualizarOperacionesOrigen do
           begin
                 Parameters.ParamByName('@CodigoAlmacen').Value := CodigoAlmacen;
                 Parameters.ParamByName('@CodigoOperacion').Value := integer(chkOperacionesEnvio.Items.Objects[i]);
                 ExecProc;
           end;
        end;
    end;
end;

procedure TFormABMMaestroAlmacenes.chkOperacionesRecepcionClickCheck(
  Sender: TObject);
begin
    CheckOperaciones(chkOperacionesRecepcion);
end;

procedure TFormABMMaestroAlmacenes.chkOperacionesEnvioClickCheck(
  Sender: TObject);
begin
    CheckOperaciones(chkOperacionesEnvio);
end;


procedure TFormABMMaestroAlmacenes.ValidarAlmacenesAsignados(
  CodigoAlmacen: integer; ValidoAlmacenProveedor, ValidoAlmacenRecepcion,
  ValidoVenta, ValidoComodato: boolean);
begin
   with ValidarAlmacenNoAsignado do
   try
        Parameters.ParamByName('@CodigoAlmacen').Value := CodigoAlmacen;
        Parameters.ParamByName('@ValidoAlmacenProveedor').Value := ValidoAlmacenProveedor;
        Parameters.ParamByName('@ValidoAlmacenRecepcion').Value := ValidoAlmacenRecepcion;
        Parameters.ParamByName('@ValidoVenta').Value := ValidoVenta;
        Parameters.ParamByName('@ValidoComodato').Value := ValidoComodato;
        ExecProc;
   finally
        close;
   end;


end;

function TFormABMMaestroAlmacenes.GetChecked(chklist: TCheckListBox): string;
var
 i: integer;
begin
        Result := '';
        for i := 0 to chklist.Items.Count - 1 do
          if chklist.Checked[i] then
           if result = '' then result := inttostr(integer(chklist.Items.Objects[i]))
           else result := result + ',' + inttostr(integer(chklist.Items.Objects[i]));

end;



{-----------------------------------------------------------------------------
  Function Name: CheckOperaciones
  Author:
  Date Created:  /  /
  Description:
  Parameters: chkOperaciones : TCheckListBox
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormABMMaestroAlmacenes.CheckOperaciones(chkOperaciones : TCheckListBox);
resourcestring
 ERROR_DESMARCAR = 'No se puede desmarcar la operaci�n.';
var
 where_in : string;

begin
 try
     try
          if DBList1.Estado=modi then
           if not chkOperaciones.Checked[chkOperaciones.ItemIndex] then
            with AlmacenValidoSinOperacion do
            begin
                SQL.Text :=
                '  select CASE WHEN sum(CAST(ValidoAlmacenProveedor AS INT)) > 0 then 1 ' +
        	    '  ELSE  0 END as ValidoAlmacenProveedor, ' +
        	    '  CASE WHEN sum(CAST(ValidoAlmacenRecepcion AS INT)) > 0 then 1 ' +
	            '  ELSE  0 END as ValidoAlmacenRecepcion, ' +
    	        '  CASE WHEN sum(CAST(ValidoDestinoVenta AS INT)) > 0 then 1 ' +
                '  ELSE  0 END as ValidoDestinoVenta, ' +
	            '  CASE WHEN sum(CAST(ValidoDestinoComodato AS INT)) > 0 then 1 ' +
	            '  ELSE  0   END as ValidoDestinoComodato ' +
	            '  from OperacionesTags OT WITH (NOLOCK) ';

                where_in := GetChecked(chkOperacionesRecepcion);
                if where_in <> '' then
                  SQL.Text := SQL.Text + ' where OT.CodigoOperacion in (' + where_in + ')'
                else
                  SQL.Text := SQL.Text + ' where OT.CodigoOperacion is NULL';

                Open;
                if RecordCount > 0 then
                  ValidarAlmacenesAsignados(ObtenerMaestroAlmacenes.FieldbyName('CodigoAlmacen').Value,
                                            FieldByName('ValidoAlmacenProveedor').AsInteger = 1,
                                            FieldByName('ValidoAlmacenRecepcion').AsInteger = 1,
                                            FieldByName('ValidoDestinoVenta').AsInteger = 1,
                                            FieldByName('ValidoDestinoComodato').AsInteger = 1);
            end;

     finally
           AlmacenValidoSinOperacion.Close;
     end;
 except
    on E: Exception do begin
     chkOperaciones.Checked[chkOperaciones.ItemIndex] := not chkOperaciones.Checked[chkOperaciones.ItemIndex];
     MsgBoxBalloon(ERROR_DESMARCAR + #13#10 + E.Message,  MSG_ACTUALIZAR_CAPTION,MB_ICONSTOP,chkOperaciones);
    end;
 end;
end;


end.
