unit SelTran;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, StdCtrls, validate, Dateedit, Util, UtilDb, UtilProc,
  DPSControls;

type
  TFormSeleccionTransitos = class(TForm)
    btn_Aceptar: TDPSButton;
    btn_Cancelar: TDPSButton;
    cb_PuntosDeCobro: TComboBox;
    cb_Categorias: TComboBox;
    dt_FechaDesde: TDateEdit;
    dt_FechaHasta: TDateEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    qry_PuntosDeCobro: TQuery;
    Query2: TQuery;
    procedure ValidarFecha(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Function Inicializa: Boolean;
  end;

var
  FormSeleccionTransitos: TFormSeleccionTransitos;

implementation

{$R *.DFM}

{ TForm2 }

function TFormSeleccionTransitos.Inicializa: Boolean;
resourcestring
	MSG_TODOS = 'Todos';
begin
	Result := False;
    if not OpenTables([qry_PuntosDeCobro]) then Exit;
    btn_Aceptar.Enabled := False;
	cb_PuntosDeCobro.Items.Clear;
    cb_PuntosDeCobro.Items.Add(PadR(MSG_TODOS, 200, ' ')  + Istr(0));
    cb_PuntosDeCobro.ItemIndex := 0;
	While not qry_PuntosDeCobro.Eof do begin
    	cb_PuntosDeCobro.Items.Add(
		  PadR(qry_PuntosDeCobro.FieldByName('Descripcion').AsString, 200, ' ') +
		  Istr(qry_PuntosDeCobro.FieldByName('NumeroPuntoCobro').AsInteger, 10));
		qry_PuntosDeCobro.Next;
	end;
	Result := True;
end;

procedure TFormSeleccionTransitos.ValidarFecha(Sender: TObject);
begin
	btn_Aceptar.Enabled :=
	  IsValidDate(dt_FechaDesde.Text) and
	  IsValidDate(dt_FechaHasta.Text) and
      (dt_FechaDesde.Date <= dt_FechaHasta.Date);
end;

end.
