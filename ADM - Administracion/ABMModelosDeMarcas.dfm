object FormModelosDeMarcas: TFormModelosDeMarcas
  Left = 134
  Top = 155
  Width = 720
  Height = 436
  Caption = 'Mantenimiento de Modelos de Veh'#237'culos'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 712
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 712
    Height = 215
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'140'#0'Marca                                  '
      #0'58'#0'Modelo     '
      
        #0'232'#0'Descripci'#243'n                                                ' +
        '        '
      #0'128'#0'Tipo de veh'#237'culo             ')
    HScrollBar = True
    RefreshTime = 100
    Table = Modelos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 248
    Width = 712
    Height = 115
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 10
      Top = 62
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txt_Descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblModelo: TLabel
      Left = 10
      Top = 38
      Width = 46
      Height = 13
      Caption = 'Modelo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 10
      Top = 89
      Width = 102
      Height = 13
      Caption = '&Tipo de veh'#237'culo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMarca: TLabel
      Left = 11
      Top = 13
      Width = 40
      Height = 13
      Caption = '&Marca:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Descripcion: TEdit
      Left = 116
      Top = 58
      Width = 297
      Height = 21
      Color = 16444382
      MaxLength = 30
      TabOrder = 2
    end
    object txt_CodigoModelo: TNumericEdit
      Left = 116
      Top = 34
      Width = 75
      Height = 21
      TabStop = False
      Color = clBtnFace
      MaxLength = 3
      ReadOnly = True
      TabOrder = 1
      Decimals = 0
    end
    object cb_TipoVehiculo: TVariantComboBox
      Left = 116
      Top = 84
      Width = 297
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 3
      Items = <>
    end
    object cb_marca: TVariantComboBox
      Left = 116
      Top = 7
      Width = 297
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 363
    Width = 712
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 515
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Modelos: TADOTable
    Connection = DMConnections.BaseCAC
    Filtered = True
    TableName = 'VehiculosModelos'
    Left = 180
    Top = 84
  end
  object qry_MaxModelo: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoMarca'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAX(CodigoModelo) as CodigoModelo from VehiculosModelos  ' +
        'WITH (NOLOCK) '
      'where codigoMarca = :CodigoMarca')
    Left = 215
    Top = 84
  end
  object qry_Marcas: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'select * from VehiculosMarcas  WITH (NOLOCK) ')
    Left = 216
    Top = 120
  end
  object BuscaTablaMarcas: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    HelpContext = 0
    Dataset = qry_Marcas
    Left = 256
    Top = 121
  end
end
