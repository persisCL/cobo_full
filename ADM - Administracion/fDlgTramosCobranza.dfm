object frmEdicionTramosCobranza: TfrmEdicionTramosCobranza
  Left = 521
  Top = 196
  BorderStyle = bsDialog
  Caption = 'Tramo de Cobranza'
  ClientHeight = 197
  ClientWidth = 250
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    250
    197)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 17
    Width = 58
    Height = 13
    Caption = 'Monto inicial'
  end
  object Label4: TLabel
    Left = 193
    Top = 17
    Width = 13
    Height = 13
    Caption = 'UF'
  end
  object Label2: TLabel
    Left = 7
    Top = 112
    Width = 96
    Height = 13
    Caption = 'Porcentaje a Aplicar'
  end
  object Label3: TLabel
    Left = 189
    Top = 112
    Width = 17
    Height = 13
    Caption = ' % '
  end
  object Bevel1: TBevel
    Left = -1
    Top = 0
    Width = 273
    Height = 159
  end
  object Label5: TLabel
    Left = 8
    Top = 48
    Width = 53
    Height = 13
    Caption = 'Monto final'
  end
  object Label6: TLabel
    Left = 8
    Top = 80
    Width = 61
    Height = 13
    Caption = 'Monto tramo'
  end
  object btnAceptarTramo: TButton
    Left = 90
    Top = 164
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Aceptar'
    ModalResult = 1
    TabOrder = 0
    OnClick = btnAceptarTramoClick
  end
  object btnCancelarTramo: TButton
    Left = 171
    Top = 165
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancelar'
    ModalResult = 2
    TabOrder = 1
  end
  object txtInicioTramo: TEdit
    Left = 109
    Top = 13
    Width = 73
    Height = 21
    Enabled = False
    TabOrder = 2
    Text = 'txtInicioTramo'
  end
  object txtPorcentajeTramo: TEdit
    Left = 109
    Top = 109
    Width = 74
    Height = 21
    TabOrder = 5
    Text = 'txtPorcentajeTramo'
  end
  object txtFinTramo: TEdit
    Left = 109
    Top = 45
    Width = 73
    Height = 21
    TabOrder = 3
    Text = 'txtFinTramo'
    OnKeyPress = txtFinTramoKeyPress
    OnKeyUp = txtFinTramoKeyUp
  end
  object txtMontoTramo: TEdit
    Left = 109
    Top = 77
    Width = 73
    Height = 21
    Enabled = False
    TabOrder = 4
    Text = 'txtMontoTramo'
  end
end
