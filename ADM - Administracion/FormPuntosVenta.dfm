object FormPuntosDeVenta: TFormPuntosDeVenta
  Left = 395
  Top = 158
  BorderStyle = bsDialog
  Caption = 'Punto de Venta'
  ClientHeight = 199
  ClientWidth = 494
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    494
    199)
  PixelsPerInch = 96
  TextHeight = 13
  object GBDetalle: TGroupBox
    Left = 8
    Top = 12
    Width = 481
    Height = 141
    Caption = 'Configuraci'#243'n del punto de venta'
    TabOrder = 0
    object Label5: TLabel
      Left = 16
      Top = 56
      Width = 72
      Height = 13
      Caption = 'Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 32
      Width = 44
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 82
      Width = 55
      Height = 13
      Caption = 'POSNET:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Descripcion: TEdit
      Left = 98
      Top = 53
      Width = 204
      Height = 21
      Hint = 'Descripci'#243'n de este Punto de Venta'
      CharCase = ecUpperCase
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object txt_codigo: TEdit
      Left = 98
      Top = 29
      Width = 103
      Height = 21
      Color = clBtnFace
      Enabled = False
      MaxLength = 60
      TabOrder = 1
    end
    object btePos: TBuscaTabEdit
      Left = 98
      Top = 80
      Width = 284
      Height = 21
      Hint = 'Terminal POS (TransBank) asignado a este Punto de Venta'
      Enabled = True
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      Decimals = 0
      EditorStyle = bteTextEdit
      BuscaTabla = btPOS
    end
    object btnDesasignar: TButton
      Left = 393
      Top = 78
      Width = 75
      Height = 25
      Caption = '&Desasignar'
      TabOrder = 3
      OnClick = btnDesasignarClick
    end
  end
  object BtnAceptar: TButton
    Left = 322
    Top = 167
    Width = 79
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = BtnAceptarClick
  end
  object BtnCancelar: TButton
    Left = 409
    Top = 167
    Width = 79
    Height = 26
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    TabOrder = 2
    OnClick = BtnCancelarClick
  end
  object btPOS: TBuscaTabla
    Caption = 'Seleccione un Terminal POS (TransBank)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = tbPOS
    OnProcess = btPOSProcess
    OnSelect = btPOSSelect
    Left = 8
    Top = 160
  end
  object tbPOS: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Filter = 'Asignado = 0'
    Filtered = True
    TableName = 'POS'
    Left = 40
    Top = 160
  end
end
