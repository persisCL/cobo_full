object AbmTiposClientesForm: TAbmTiposClientesForm
  Left = 191
  Top = 177
  Caption = 'Mantenimiento de Tipos de Cliente'
  ClientHeight = 417
  ClientWidth = 730
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 730
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object pnlAbm: TPanel
    Left = 0
    Top = 316
    Width = 730
    Height = 62
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object Label8: TLabel
      Left = 5
      Top = 9
      Width = 58
      Height = 13
      Caption = 'Sem'#225'foro:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 5
      Top = 33
      Width = 72
      Height = 13
      Caption = 'Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 183
      Top = 9
      Width = 34
      Height = 13
      Caption = 'Color:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Led1: TLed
      Left = 319
      Top = 10
      Width = 21
      Height = 17
      Blinking = False
      Diameter = 15
      Enabled = True
      ColorOn = clRed
      ColorOff = clBtnFace
    end
    object txtDescripcion: TEdit
      Left = 83
      Top = 33
      Width = 230
      Height = 21
      Hint = 'Descripci'#243'n'
      Color = 16444382
      MaxLength = 40
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = txtDescripcionChange
      OnKeyPress = txtDescripcionKeyPress
    end
    object txtColor: TEdit
      Left = 223
      Top = 6
      Width = 90
      Height = 21
      Hint = 'Color'
      TabStop = False
      CharCase = ecUpperCase
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 6
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      OnChange = txtTipoClienteChange
    end
    object btnColor: TBitBtn
      Left = 338
      Top = 6
      Width = 79
      Height = 21
      Hint = 'Seleccionar Color'
      Caption = 'Cambiar Color'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnColorClick
    end
    object BtnDetalleMensajes: TBitBtn
      Left = 444
      Top = 6
      Width = 73
      Height = 41
      Caption = 'Detalle Mensajes'
      TabOrder = 3
      WordWrap = True
      OnClick = BtnDetalleMensajesClick
    end
    object cboSemaforo: TComboBox
      Left = 74
      Top = 4
      Width = 49
      Height = 21
      Hint = 'Seleccione un Semaforo'
      Style = csDropDownList
      ItemHeight = 13
      Sorted = True
      TabOrder = 4
      Items.Strings = (
        '1'
        '2'
        '3')
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 378
    Width = 730
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 533
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        ExplicitWidth = 0
        ExplicitHeight = 0
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 26
          Top = 6
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 730
    Height = 283
    TabStop = True
    TabOrder = 0
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'50'#0'C'#243'digo   '
      #0'59'#0'Sem'#225'foro  '
      
        #0'433'#0'Descripci'#243'n                                                ' +
        '                                                                ' +
        '           '
      #0'50'#0'Color      ')
    Table = spLista
    Style = lbOwnerDrawFixed
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object spLista: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'TiposClienteObtenerListaCompleta'
    Parameters = <>
    Left = 480
    Top = 80
  end
  object cdColores: TColorDialog
    Left = 480
    Top = 144
  end
  object spObtenerTiposDeClientesMensaje: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTiposDeClientesMensaje'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoClienteMensaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@semaforo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 216
    Top = 152
  end
end
