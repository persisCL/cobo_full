{-----------------------------------------------------------------------------
 Unit Name: ABMEstadosDeCasos
 Author:  CFU
 Description: ABM de Estados de Casos
-----------------------------------------------------------------------------}
unit ABMEstadosDeCasos;

interface

uses
  //ABM Estados de Casos
  DMConnection,
  UtilProc,
  Util,
  RStrings,
  utildb,
  PeaTypes,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants;

type
  TFABMEstadosDeCasos = class(TForm)
	GroupB: TPanel;
    PAbajo: TPanel;
	Notebook: TNotebook;
    txtCodigo: TNumericEdit;
    AbmToolbar: TAbmToolbar;
    Lcodigo: TLabel;
    Ldescripcion: TLabel;
    txtDescripcion: TEdit;
    tblEstadosDeCasos: TADOTable;
    dsEstadosDeCasos: TDataSource;
    spActualizarEstadosDeCasos: TADOStoredProc;
    spEliminarEstadosDeCasos: TADOStoredProc;
    ListaEstados: TAbmList;
    Ltipo: TLabel;
    txtNombre: TEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure FormShow(Sender: TObject);
   	procedure ListaEstadosRefresh(Sender: TObject);
   	function  ListaEstadosProcess(Tabla: TDataSet; var Texto: String): Boolean;
   	procedure ListaEstadosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaEstadosClick(Sender: TObject);
	procedure ListaEstadosInsert(Sender: TObject);
	procedure ListaEstadosEdit(Sender: TObject);
	procedure ListaEstadosDelete(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure AbmToolbarClose(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FABMEstadosDeCasos: TFABMEstadosDeCasos;

resourcestring
	STR_MAESTRO_Estados	= 'Maestro de Estados de Casos';


implementation

{$R *.DFM}

function TFABMEstadosDeCasos.Inicializa:Boolean;
Var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblEstadosDeCasos]) then exit;
	Notebook.PageIndex := 0;
	ListaEstados.Reload;
	Result := True;
end;

procedure TFABMEstadosDeCasos.LimpiarCampos;
begin
	txtCodigo.Clear;
	txtDescripcion.Clear;
    txtNombre.Clear;
end;

procedure TFABMEstadosDeCasos.FormShow(Sender: TObject);
begin
	ListaEstados.Reload;
end;

procedure TFABMEstadosDeCasos.ListaEstadosRefresh(Sender: TObject);
begin
	 if ListaEstados.Empty then LimpiarCampos;
end;

procedure TFABMEstadosDeCasos.Volver_Campos;
begin
	ListaEstados.Estado := Normal;
	ListaEstados.Enabled:= True;

	ActiveControl       := ListaEstados;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;
	ListaEstados.Reload;
end;

procedure TFABMEstadosDeCasos.ListaEstadosDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoEstadoDeCaso').AsInteger, 10));
        TextOut(Cols[1], Rect.Top, Tabla.FieldbyName('NombreEstadoDeCaso').AsString);
		TextOut(Cols[2], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TFABMEstadosDeCasos.ListaEstadosClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   txtCodigo.Value		:= FieldByName('CodigoEstadoDeCaso').AsInteger;
       txtNombre.Text	    := FieldByname('NombreEstadoDeCaso').AsString;
	   txtDescripcion.Text	:= FieldByname('Descripcion').AsString;
	end;
end;

function TFABMEstadosDeCasos.ListaEstadosProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
	Result := True;
end;

procedure TFABMEstadosDeCasos.ListaEstadosInsert(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled          	:= True;
	ListaEstados.Enabled 		:= False;
	Notebook.PageIndex      	:= 1;
	ActiveControl           	:= txtNombre;
end;

procedure TFABMEstadosDeCasos.ListaEstadosEdit(Sender: TObject);
begin
	ListaEstados.Enabled:= False;
	ListaEstados.Estado := modi;

	Notebook.PageIndex := 1;
	groupb.Enabled     := True;

	ActiveControl:= txtDescripcion
end;

procedure TFABMEstadosDeCasos.ListaEstadosDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_Estados]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with spEliminarEstadosDeCasos do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoEstadoDeCaso').Value := tblEstadosDeCasos.FieldbyName('CodigoEstadoDeCaso').Value;
                ExecProc;
            end;
		Except
			On E: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_Estados]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_Estados]), MB_ICONSTOP);
			end
		end
	end;

	ListaEstados.Reload;
	ListaEstados.Estado := Normal;
	ListaEstados.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFABMEstadosDeCasos.BtnAceptarClick(Sender: TObject);

begin
	if not ValidateControls([txtDescripcion],
	  [(Trim(txtDescripcion.Text) <> '')],
	  Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_Estados]),
	  [Format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION])]) then begin
		Exit;
	end;

	with ListaEstados do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarEstadosDeCasos, Parameters do begin
                    refresh;
					ParamByName('@CodigoEstadoDeCaso').Value	:= txtCodigo.ValueInt;
                    ParamByName('@Nombre').Value 				:= txtNombre.Text;
					ParamByName('@Descripcion').Value 			:= Trim(txtDescripcion.Text);
                    ParamByName('@Usuario').Value				:= UsuarioSistema;
					ExecProc;
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_Estados]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_Estados]), MB_ICONSTOP);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

procedure TFABMEstadosDeCasos.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFABMEstadosDeCasos.AbmToolbarClose(Sender: TObject);
begin
	 close;
end;

procedure TFABMEstadosDeCasos.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TFABMEstadosDeCasos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;

end.

