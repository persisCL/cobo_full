{-----------------------------------------------------------------------------
 Unit Name: FrmDescripcionesPuntosCobro
 Author:    ggomez
 Purpose: Form para modificar las descripciones de los Puntos de Cobro.
    Si la Trim(Descripci�n) es vac�a, no se permite la modificaci�n.
    El ToolBar tiene deshabilitados los botones Agregar y Eliminar.
    El TAbmList tiene deshabilitados los Access accAlta y accBaja.

 History:

 Revision 1
 Author: mbecerra
 Date: 29-Julio-2010
 Description:		(Ref. Fase 2)
                Se agregan todos los campos de la tabla PuntosCobro, ya
                que actualmente s�lo se pod�a modificar la Descripci�n.

                Adicionalmente se agregan los campos CodigoConcesionaria y
                Numero punto Cobro Interno de la Concesionaria.

                Se documentan las funciones pues ninguna estaba documentada

                Nota: se habilita el bot�n Insertar, para agregar nuevos
                puntos de cobro, pero no se habilita el bot�n eliminar,
                ya que los puntos de cobro no se pueden eliminar.

 Revision   : 2
 Author     : Nelson Droguett Sierra
 Date       : 19-Enero-2011
 Description: (Ref.Fase2 SF010)
              campo Juzgado no puede ser obligatorio
              Todos los campos que son obligatorios, mostrarlos en la parte superior, arriba en el ABM.
Firma       : SF010-NDR-20110119

Autor       :   CQuezadaI
Fecha       :   17 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se agrega Exception ya que s�lo se consideraba EDataBaseError.
                Se agrega funci�n que permite identificar si un numero es TinyInt
                Se agrega toda la l�gica para EsItaliano ya que no estaba contemplada.
-----------------------------------------------------------------------------}
unit FrmDescripcionesPuntosCobro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, DbList, Abm_obj, DB, ADODB,
  DMConnection, RStrings, UtilProc, StrUtils, UtilDB, Util, PeaTypes,  PeaProcs,
  VariantComboBox, DmiCtrls;

type
  TFDescripcionesPuntosCobro = class(TForm)
    AbmToolbar: TAbmToolbar;
    lb_PuntosCobro: TAbmList;
    pnl_Datos: TPanel;
    lbl_Descripcion: TLabel;
    edtDescripcion: TEdit;
    Panel2: TPanel;
    Notebook: TNotebook;
    tbl_PuntosCobro: TADOTable;
    spActualizarPuntoCobro: TADOStoredProc;
    ds_PuntosCobro: TDataSource;
    btn_Salir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    neNumeroPuntoCobro: TNumericEdit;
    neDomain: TNumericEdit;
    neTSMCID: TNumericEdit;
    neEjeVial: TNumericEdit;
    edtSentido: TEdit;
    neProgresivaMetros: TNumericEdit;
    neTSID: TNumericEdit;
    edtPrefijoImagen: TEdit;
    nePuntoCobroAnterior: TNumericEdit;
    neCarriles: TNumericEdit;
    vcbJuzgado: TVariantComboBox;
    nePCConcesionaria: TNumericEdit;
    neKms: TNumericEdit;
    chkSentido: TCheckBox;
    vcbConcesionarias: TVariantComboBox;
    spObtenerConcesionarias: TADOStoredProc;
    spObtenerJuzgados: TADOStoredProc;
    neNroPtoCobroCC: TNumericEdit;
    cbEsItaliano: TVariantComboBox;
    lblEsItaliano: TLabel;
    procedure AbmToolbarClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure lb_PuntosCobroClick(Sender: TObject);
    procedure lb_PuntosCobroDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure lb_PuntosCobroEdit(Sender: TObject);
    procedure lb_PuntosCobroRefresh(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
    procedure lb_PuntosCobroInsert(Sender: TObject);
  private
    { Private declarations }
    procedure LimpiarCampos;
    procedure VolverCampos;
  public
    { Public declarations }
	function Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
  end;

var
  FDescripcionesPuntosCobro: TFDescripcionesPuntosCobro;

resourcestring
	STR_MAESTRO_PUNTOSCOBRO = 'Maestro de Puntos de Cobro';

implementation


{$R *.dfm}

{-------------------------------------------------------------------
            AbmToolbarClose

Author:
Date:
Description:	(mbecerra)
            	Cierra la ventana del ABM de Puntos de Cobro
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.AbmToolbarClose(Sender: TObject);
begin
    Close;
end;

{-------------------------------------------------------------------
            FormShow

Author:
Date:
Description:	(mbecerra)
            	Se actualiza la grilla
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.FormShow(Sender: TObject);
begin
    lb_PuntosCobro.Reload;
end;

{-------------------------------------------------------------------
            FormClose

Author:
Date:
Description:	(mbecerra)
                Cierra el formulario del ABM de PuntosCobro
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Action := caFree;
end;

{-------------------------------------------------------------------
            BtnCancelarClick

Author:
Date:
Description:	(mbecerra)
                Cancela la edici�n o la Inserci�n 
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.BtnCancelarClick(Sender: TObject);
begin
    VolverCampos;
end;

{-------------------------------------------------------------------
            BtnAceptarClick

Author: mbecerra
Date: 30-Julio-2010
Description:	(Ref. Fase 2)
                Grabar la modificaci�n o la inserci�n del Punto de Cobro
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.BtnAceptarClick(Sender: TObject);
    function EsTinyInt(pNumeroValidar : Extended) : Boolean;                        // SS_1147_CQU_20140714
    var                                                                             // SS_1147_CQU_20140714
        Resultado : Boolean;                                                        // SS_1147_CQU_20140714
    begin                                                                           // SS_1147_CQU_20140714
        Resultado := False;                                                         // SS_1147_CQU_20140714
        if (pNumeroValidar > 0) and (pNumeroValidar <= 255) then Resultado := True; // SS_1147_CQU_20140714
        Result := Resultado;                                                        // SS_1147_CQU_20140714
    end;                                                                            // SS_1147_CQU_20140714

resourcestring
    CAPTION_TITULO		= 'Puntos de Cobro';
    MSG_FALTA_NUMERO	= 'No ha ingresado del N�mero de Punto de Cobro';
	MSG_FALTA_DOMAIN	= 'No ha ingresado el DomainID';
    MSG_FALTA_TSMCID	= 'No ha ingresado el TSMCID';
    MSG_FALTA_TSID		= 'No ha ingresado el TSID';
    MSG_FALTA_DESC		= 'No ha ingresado la Descripci�n';
    MSG_FALTA_EJE		= 'Falta el Eje Vial';
    MSG_FALTA_SENTIDO   = 'Por favor indique el Sentido';
    MSG_FALTA_METROS	= 'No ha ingresado Progresiva metros';
    MSG_FALTA_PC		= 'Falta ingresar C�digo PC Concesionaria';
    MSG_FALTA_KMS		= 'No ha indicado la cantidad de Kil�metros';
    //Rev.2 / 19-Enero-2011 / Nelson Droguett Sierra ----------------------------------------
    //MSG_FALTA_JUZGADO	= 'No ha indicado el Juzgado';                      //SF010-NDR-20110119
    MSG_FALTA_CONCES	= 'No ha indicado al Concesionaria a la que pertence el Punto de cobro';
    MSG_FALTA_PCINTERNO = 'No ha ingresado el N�mero de Punto de Cobro de la concesionaria';
    MSG_NO_ES_TINYINT   = 'El valor ingresado para %s no es un valor permitido';    // SS_1147_CQU_20140714
begin
    if not ValidateControls(	[	neNumeroPuntoCobro,
    								neDomain,
    								neTSMCID,
    								neTSID,
									edtDescripcion,
        							neEjeVial,
    								edtSentido,
    								neProgresivaMetros,
    								nePCConcesionaria,
    								neKms,
                    //Rev.2 / 19-Enero-2011 / Nelson Droguett Sierra
    								//vcbJuzgado,             //SF010-NDR-20110119
    								vcbConcesionarias,
                                    neNroPtoCobroCC,
                                    neNumeroPuntoCobro,     // SS_1147_CQU_20140714
                                    neDomain,               // SS_1147_CQU_20140714
    								neTSMCID,               // SS_1147_CQU_20140714
    								neTSID,                 // SS_1147_CQU_20140714
                                    neEjeVial,              // SS_1147_CQU_20140714
                                    nePuntoCobroAnterior,   // SS_1147_CQU_20140714
                                    neCarriles,             // SS_1147_CQU_20140714
                                    nePCConcesionaria       // SS_1147_CQU_20140714
                            	],
                                [   neNumeroPuntoCobro.Value > 0,
    								neDomain.Value > 0,
    								neTSMCID.Value > 0,
    								neTSID.Value > 0,
									Trim(edtDescripcion.Text) <> '',
        							neEjeVial.Value > 0,
    								edtSentido.Text <> '',
    								neProgresivaMetros.Value > 0,
    								nePCConcesionaria.Value > 0,
    								neKms.Value > 0,
                    //Rev.2 / 19-Enero-2011 / Nelson Droguett Sierra
    								//vcbJuzgado.ItemIndex >= 0,    //SF010-NDR-20110119
    								vcbConcesionarias.ItemIndex >= 0,
                                    neNroPtoCobroCC.Value > 0,
                                    EsTinyInt(neNumeroPuntoCobro.Value) = True,     // SS_1147_CQU_20140714
                                    EsTinyInt(neDomain.Value) = True,               // SS_1147_CQU_20140714
    								EsTinyInt(neTSMCID.Value) = True,               // SS_1147_CQU_20140714
    								EsTinyInt(neTSID.Value) = True,                 // SS_1147_CQU_20140714
                                    EsTinyInt(neEjeVial.Value) = True,              // SS_1147_CQU_20140714
                                    EsTinyInt(nePuntoCobroAnterior.Value) = True,   // SS_1147_CQU_20140714
                                    EsTinyInt(neCarriles.Value) = True,             // SS_1147_CQU_20140714
                                    EsTinyInt(nePCConcesionaria.Value) = True       // SS_1147_CQU_20140714
                                ],
                                CAPTION_TITULO,
                                [	MSG_FALTA_NUMERO,
                                	MSG_FALTA_DOMAIN,
                                    MSG_FALTA_TSMCID,
                                    MSG_FALTA_TSID,
                                    MSG_FALTA_DESC,
                                    MSG_FALTA_EJE,
                                    MSG_FALTA_SENTIDO,
                                    MSG_FALTA_METROS,
                                    MSG_FALTA_PC,
                                    MSG_FALTA_KMS,
                                    //Rev.2 / 19-Enero-2011 / Nelson Droguett Sierra
                                    //MSG_FALTA_JUZGADO,    //SF010-NDR-20110119
                                    MSG_FALTA_CONCES,
                                    MSG_FALTA_PCINTERNO,
                                    Format(MSG_NO_ES_TINYINT, ['Nro. Punto de Cobro']),     // SS_1147_CQU_20140714
                                    Format(MSG_NO_ES_TINYINT, ['Domain ID']),               // SS_1147_CQU_20140714
                                    Format(MSG_NO_ES_TINYINT, ['TSMCID']),                  // SS_1147_CQU_20140714
                                    Format(MSG_NO_ES_TINYINT, ['TSID']),                    // SS_1147_CQU_20140714
                                    Format(MSG_NO_ES_TINYINT, ['C�digo Eje Vial']),         // SS_1147_CQU_20140714
                                    Format(MSG_NO_ES_TINYINT, ['Punto de Cobro Anterior']), // SS_1147_CQU_20140714
                                    Format(MSG_NO_ES_TINYINT, ['Cant. Carriles']),          // SS_1147_CQU_20140714
                                    Format(MSG_NO_ES_TINYINT, ['C�digo Concesionaria'])     // SS_1147_CQU_20140714
                                ] ) then  Exit;

    with lb_PuntosCobro do begin
    	Screen.Cursor := crHourGlass;
        try
        	with spActualizarPuntoCobro do begin
            	Parameters.ParamByName('@NumeroPuntoCobro').Value               := neNumeroPuntoCobro.Value;
                Parameters.ParamByName('@DomainID').Value                       := neDomain.Value;
                Parameters.ParamByName('@TSMCID').Value                         := neTSMCID.Value;
                Parameters.ParamByName('@Descripcion').Value                    := edtDescripcion.Text;
                Parameters.ParamByName('@CodigoEjeVial').Value                  := neEjeVial.Value;
                Parameters.ParamByName('@Sentido').Value                        := Copy(Trim(edtSentido.Text),1,1);
                Parameters.ParamByName('@ProgresivaMetros').Value               := neProgresivaMetros.Value;
                Parameters.ParamByName('@TSID').Value                           := neTSID.Value;
                Parameters.ParamByName('@PrefijoImagen').Value                  := edtPrefijoImagen.Text;
                Parameters.ParamByName('@PuntoCobroAnterior').Value             := nePuntoCobroAnterior.Value;
                Parameters.ParamByName('@CantidadCarriles').Value               := neCarriles.Value;
                Parameters.ParamByName('@UltimoPorticoSentido').Value           := chkSentido.Checked;
                Parameters.ParamByName('@CodigoJuzgado').Value                  := vcbJuzgado.Value;
                Parameters.ParamByName('@CodigoPCConcesionaria').Value          := nePCConcesionaria.Value;
                Parameters.ParamByName('@Kms').Value                            := neKms.Value;
                Parameters.ParamByName('@CodigoConcesionaria').Value            := vcbConcesionarias.Value;
                Parameters.ParamByName('@NumeroPuntoCobroConcesionaria').Value  := neNroPtoCobroCC.Value;
                Parameters.ParamByName('@Usuario').Value                        := UsuarioSistema;
                Parameters.ParamByName('@EsItaliano').Value                     := cbEsItaliano.Value;  // SS_1147_CQU_20140714
                ExecProc;
            end;
        except on E: EDataBaseError do begin
        		MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR, [STR_MAESTRO_PUNTOSCOBRO]),
                			E.Message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_PUNTOSCOBRO]),
                            MB_ICONSTOP);
        	end;
            on E: Exception do begin                                                                // SS_1147_CQU_20140714
        		MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR, [STR_MAESTRO_PUNTOSCOBRO]),                  // SS_1147_CQU_20140714
                			E.Message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_PUNTOSCOBRO]),    // SS_1147_CQU_20140714
                            MB_ICONSTOP);                                                           // SS_1147_CQU_20140714
            end;                                                                                    // SS_1147_CQU_20140714
        end;

        VolverCampos;
        Screen.Cursor:= crDefault;
        Reload;
    end; // with
end;

{-------------------------------------------------------------------
            lb_PuntosCobroClick

Author:
Date:
Description:	(mbecerra)
                Muestra los datos a sociados al registro seleccionado en la
                grilla
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.lb_PuntosCobroClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
    	neNumeroPuntoCobro.Value	:= FieldByName('NumeroPuntoCobro').AsInteger;
    	neDomain.Value 				:= FieldByName('DomainID').AsInteger;
    	neTSMCID.Value 				:= FieldByName('TSMCID').AsInteger;
    	neTSID.Value 				:= FieldByName('TSID').AsInteger;
		edtDescripcion.Text	    	:= Trim(FieldByname('Descripcion').AsString);
        neEjeVial.Value				:= FieldByName('CodigoEjeVial').AsInteger;
    	nePuntoCobroAnterior.Value 	:= FieldByName('PuntoCobroAnterior').AsInteger;
    	edtSentido.Text				:= FieldByName('Sentido').AsString;
    	neCarriles.Value			:= FieldByName('CantidadCarriles').AsInteger;
    	neProgresivaMetros.Value	:= FieldByName('ProgresivaMetros').AsFloat;
    	nePCConcesionaria.Value		:= FieldByName('CodigoPCConcesionaria').AsInteger;
    	edtPrefijoImagen.Text		:= FieldByName('PrefijoImagen').AsString;
    	neKms.Value					:= FieldByName('Kms').AsFloat;
    	vcbJuzgado.ItemIndex		:= vcbJuzgado.Items.IndexOfValue(FieldByName('CodigoJuzgado').AsInteger);
    	chkSentido.Checked			:= FieldByName('UltimoPorticoSentido').AsBoolean;
    	vcbConcesionarias.ItemIndex	:= vcbConcesionarias.Items.IndexOfValue(FieldByName('CodigoConcesionaria').AsInteger);
    	neNroPtoCobroCC.Value       := FieldByName('NumeroPuntoCobroConcesionaria').AsInteger;
        cbEsItaliano.Value          := FieldByName('EsItaliano').AsBoolean; // SS_1147_CQU_20140714
	end;
end;

{-------------------------------------------------------------------
            lb_PuntosCobroDrawItem

Author:
Date:
Description:	(mbecerra)
                Da formato a la visualizaci�n los datos a sociados
                al registro seleccionado en la grilla
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.lb_PuntosCobroDrawItem(
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
	with Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(FieldbyName('NumeroPuntoCobro').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Istr(FieldbyName('DomainID').AsInteger, 10));
		TextOut(Cols[2], Rect.Top, Istr(FieldbyName('TSMCID').AsInteger, 10));
		TextOut(Cols[3], Rect.Top, Trim(FieldbyName('Descripcion').AsString));
		TextOut(Cols[4], Rect.Top, Istr(FieldbyName('CodigoEjeVial').AsInteger, 10));
		TextOut(Cols[5], Rect.Top, Trim(FieldbyName('Sentido').AsString));
		TextOut(Cols[6], Rect.Top, Rstr(FieldbyName('ProgresivaMetros').AsFloat, 10, 1));
		TextOut(Cols[7], Rect.Top, Istr(FieldbyName('TSID').AsInteger, 10));
    //Rev.2 / 19-Enero-2011 / Nelson Droguett Sierra-----------------------------------------------
		//TextOut(Cols[8], Rect.Top, Trim(FieldbyName('PrefijoImagen').AsString));                                                                             //SF010-NDR-20110119
		//TextOut(Cols[9], Rect.Top, Istr(FieldbyName('PuntoCobroAnterior').AsInteger, 10));                                                                   //SF010-NDR-20110119
		//TextOut(Cols[10], Rect.Top, Istr(FieldbyName('CantidadCarriles').AsInteger, 10));                                                                    //SF010-NDR-20110119
		//TextOut(Cols[11], Rect.Top, iif(Fieldbyname('UltimoPorticoSentido').AsBoolean, MSG_SI, MSG_NO));                                                     //SF010-NDR-20110119
    //TextOut(Cols[12], Rect.Top, Istr(FieldbyName('CodigoJuzgado').AsInteger, 10));                                                                       //SF010-NDR-20110119
		TextOut(Cols[8], Rect.Top, Istr(FieldbyName('CodigoPCConcesionaria').AsInteger, 10));                                                                  //SF010-NDR-20110119
		TextOut(Cols[9], Rect.Top, FieldbyName('Kms').AsString);                                                                                               //SF010-NDR-20110119
		TextOut(Cols[10], Rect.Top, vcbConcesionarias.Items[vcbConcesionarias.Items.IndexOfValue(FieldByName('CodigoConcesionaria').AsInteger)].Caption);      //SF010-NDR-20110119
    //FinRev.12-------------------------------------------------------------------------------------
        TextOut(Cols[11], Rect.Top, FieldByName('NumeroPuntoCobroConcesionaria').AsString);
	end;
end;

{-------------------------------------------------------------------
            lb_PuntosCobroEdit

Author:
Date:
Description:	(mbecerra)
                Permite la Edici�n de los datos del punto de cobro
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.lb_PuntosCobroEdit(Sender: TObject);
begin
	lb_PuntosCobro.Enabled  := False;
    lb_PuntosCobro.Estado   := Modi;
	Notebook.PageIndex      := 1;
    pnl_Datos.Enabled       := True;
    neNumeroPuntoCobro.SetFocus;
end;

{-------------------------------------------------------------------
            lb_PuntosCobroInsert

Author:
Date:
Description:	(mbecerra)
                Limpia los campos y permite la inserci�n de un nuevo
                Punto de Cobro
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.lb_PuntosCobroInsert(Sender: TObject);
begin
    LimpiarCampos();
    lb_PuntosCobro.Enabled := False;
    lb_PuntosCobro.Estado := Alta;
    Notebook.PageIndex := 1;
    pnl_Datos.Enabled := True;
    neNumeroPuntoCobro.SetFocus;
end;

{-------------------------------------------------------------------
            lb_PuntosCobroRefresh

Author:
Date:
Description:	(mbecerra)
                Limpia los campos
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.lb_PuntosCobroRefresh(
  Sender: TObject);
begin
    if lb_PuntosCobro.Empty then LimpiarCampos;
end;

{-------------------------------------------------------------------
            btn_SalirClick

Author:
Date:
Description:	(mbecerra)
                Cierra la ventana de ABM de Puntos de Cobro
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

{-------------------------------------------------------------------
            Inicializar

Author:
Date:
Description:	(mbecerra)
                Inicializa la ventana de ABM de Puntos de Cobro
--------------------------------------------------------------------}
function TFDescripcionesPuntosCobro.Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
resourcestring
    SQL_TIPO_CONCE_AUTOP	= 'SELECT dbo.CONST_TIPO_CONCESIONARIA_AUTOPISTA()';
var
	S: TSize;
    TipoConcesionaria : integer;
begin
	Result := False;

	if MDIChild then begin
        FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	Notebook.PageIndex := 0;

   	CenterForm(Self);
    Caption := AnsiReplaceStr(txtCaption, '&', '');

	if not OpenTables([tbl_PuntosCobro]) then Exit;

    //Cargar los juzgados
    vcbJuzgado.Clear;
    with spObtenerJuzgados do begin
        Close;
        Open;
        while not Eof do begin
            vcbJuzgado.Items.Add(	FieldByName('Descripcion').AsString,
                                	FieldByName('CodigoJuzgado').AsInteger );
            Next;
        end;

        Close;
    end;
    
    //Cargar  las concesionarias de tipo Autopista
    TipoConcesionaria := QueryGetValueInt(DMConnections.BaseCAC, SQL_TIPO_CONCE_AUTOP);
    vcbConcesionarias.Clear;
    with spObtenerConcesionarias do begin
    	Close;
        Parameters.ParamByName('@CodigoTipoConcesionaria').Value := TipoConcesionaria;
        Open;
        while not Eof do begin
            vcbConcesionarias.Items.Add(	FieldByName('Descripcion').AsString,
                                        	FieldByName('CodigoConcesionaria').AsInteger );
            Next;
        end;
        
        Close;
    end;

    // Limpiar controles de ingreso de datos.
    LimpiarCampos;

    KeyPreview := True;
	lb_PuntosCobro.Reload;
	Result := True;
end;

{-------------------------------------------------------------------
            LimpiarCampos

Author:
Date:
Description:	(mbecerra)
                Limpia los campos relacionados con el ingreso de datos 
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.LimpiarCampos;
begin
    neNumeroPuntoCobro.Value := 0;
    neDomain.Value := 0;
    neTSMCID.Value := 0;
    neTSID.Value := 0;
    edtDescripcion.Clear;
    neEjeVial.Value := 0;
    nePuntoCobroAnterior.Value := 0;
    edtSentido.Clear;
    neCarriles.Value := 0;
    neProgresivaMetros.Value := 0;
    nePCConcesionaria.Value := 0;
    edtPrefijoImagen.Clear;
    neKms.Value := 0;
    vcbJuzgado.ItemIndex := -1;
    chkSentido.Checked := False;
    vcbConcesionarias.ItemIndex := -1;
    neNroPtoCobroCC.Value := 0;
    cbEsItaliano.ItemIndex := 1;    // SS_1147_CQU_20140714
end;

{-------------------------------------------------------------------
            VolverCampos

Author:
Date:
Description:	(mbecerra)
                Devuelve el valor de los campos al valor original
                del registro seleccionado
--------------------------------------------------------------------}
procedure TFDescripcionesPuntosCobro.VolverCampos;
begin
	lb_PuntosCobro.Estado	:= Normal;
	lb_PuntosCobro.Enabled	:= True;
	ActiveControl       	:= lb_PuntosCobro;
	Notebook.PageIndex  	:= 0;
	pnl_Datos.Enabled		:= False;
	lb_PuntosCobro.Reload;
end;

end.
