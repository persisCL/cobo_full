{********************************** File Header ********************************
File Name   : ABMPreguntaSolicitud.pas
Author      : dcalani
Date Created: 10/03/2004
Language    : ES-AR
Description : Se encarga de la seleccion de FuentesSolicitud y de la probabilidad
            que tiene una pregutna con este tipo de fuente. 
*******************************************************************************}

unit ABMPreguntaSolicitud;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, DPSControls, ExtCtrls,PeaProcs,DMConnection,Util,utildb;

type
  TFormPreguntaSolicitud = class(TForm)
    pnl_BotonesGeneral: TPanel;
    GroupBox1: TGroupBox;
    cb_FuenteSolicitud: TComboBox;
    Label1: TLabel;
    txt_Proba: TNumericEdit;
    Label2: TLabel;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
     function GetCodigoFuenteSolicitud:Integer;
     function GetProbabilidad:Integer;
     function GetFuenteSolicitud:String;
  public
    { Public declarations }
     function inicializar(CodigofuenteSolicitud:Integer=0;Probabilidad:Integer=50):Boolean;
     property CodigoFuenteSolicitud:Integer read GetCodigoFuenteSolicitud;
     property Probabilidad:Integer read GetProbabilidad;
     property FuenteSolicitud:String read GetFuenteSolicitud;
  end;

var
  FormPreguntaSolicitud: TFormPreguntaSolicitud;

implementation

{$R *.dfm}
resourcestring
    MSG_CAPTION_FUENTE = 'Fuentes de Solicitud x Pregunta.';
    MSG_ERROR_PROBA = 'La probabilidad debe estar entre 0 y 100.';

function TFormPreguntaSolicitud.inicializar(CodigofuenteSolicitud:Integer=0;Probabilidad:Integer=50):Boolean;
begin
    result := false;
    try
        CargarFuentesSolicitud(DMConnections.BaseCAC,cb_FuenteSolicitud,CodigofuenteSolicitud);
        txt_Proba.Value:=Probabilidad;
        result := True;
    except
    end;
end;

procedure TFormPreguntaSolicitud.BtnCancelarClick(Sender: TObject);
begin
    close;
end;

procedure TFormPreguntaSolicitud.BtnAceptarClick(Sender: TObject);
begin
    if not(ValidateControls([txt_Proba],[(txt_Proba.value>=0) and (txt_Proba.value<=100)],MSG_CAPTION_FUENTE,[MSG_ERROR_PROBA])) then exit;

    ModalResult:=mrOk;
end;

procedure TFormPreguntaSolicitud.FormShow(Sender: TObject);
begin
    cb_FuenteSolicitud.SetFocus;
end;

function TFormPreguntaSolicitud.GetCodigoFuenteSolicitud:Integer;
begin
    result:=StrToInt(StrRight(cb_FuenteSolicitud.Text,10));
end;


function TFormPreguntaSolicitud.GetProbabilidad:Integer;
begin
    result:=txt_Proba.ValueInt;
end;

function TFormPreguntaSolicitud.GetFuenteSolicitud:String;
begin
    result:=Trim(StrLeft(cb_FuenteSolicitud.Text,50));
end;

end.
