unit FrmPlanTarifarioGaps;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ListBoxEx, DBListEx, DB;

type
  TfrmPlanesTarifariosGaps = class(TForm)
    dblGaps: TDBListEx;
    btnAceptar: TButton;
    lbl1: TLabel;
    procedure btnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure LiberarObjetos();
  public
    { Public declarations }
    function Inicializar(Datos: TDataSet):Boolean;
  end;

var
  frmPlanesTarifariosGaps: TfrmPlanesTarifariosGaps;

implementation

{$R *.dfm}



function TfrmPlanesTarifariosGaps.Inicializar(Datos: TDataSet):Boolean;
begin
    LiberarObjetos();
    dblGaps.DataSource := TDataSource.Create(nil);
    dblGaps.DataSource.DataSet := Datos;
end;
      
procedure TfrmPlanesTarifariosGaps.btnAceptarClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmPlanesTarifariosGaps.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     LiberarObjetos();
end;

procedure TfrmPlanesTarifariosGaps.LiberarObjetos();
begin
    if Assigned(dblGaps.DataSource) then
    begin
       if Assigned(dblGaps.DataSource.DataSet) then
        dblGaps.DataSource.Dataset.Free;

       dblGaps.DataSource.Free;
    end;
end;

end.
