object FormABMAlias: TFormABMAlias
  Left = 270
  Top = 250
  BorderStyle = bsDialog
  Caption = 'Gestion de Alias'
  ClientHeight = 80
  ClientWidth = 386
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 17
    Width = 32
    Height = 13
    Caption = 'Alias:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object pnl_BotonesGeneral: TPanel
    Left = 0
    Top = 41
    Width = 386
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object BtnCancelar: TButton
      Left = 295
      Top = 7
      Width = 79
      Height = 26
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 0
      OnClick = BtnCancelarClick
    end
    object BtnAceptar: TButton
      Left = 208
      Top = 7
      Width = 79
      Height = 26
      Caption = '&Aceptar'
      Default = True
      TabOrder = 1
      OnClick = BtnAceptarClick
    end
  end
  object txt_Alias: TMaskEdit
    Left = 51
    Top = 12
    Width = 325
    Height = 21
    Color = 16444382
    MaxLength = 100
    TabOrder = 1
    Text = 'txt_Alias'
  end
end
