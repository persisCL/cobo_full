unit ABMCiudades;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, Util, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, ADODB, BuscaTab,
  Variants, DMConnection, DPSControls, Peatypes, CheckLst;

type
  TFormCiudades = class(TForm)
    AbmToolbar1: TAbmToolbar;
    dblCiudades: TAbmList;
    GroupB: TPanel;
    Panel2: TPanel;
    txt_descripcion: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    txt_CodigoCiudad: TEdit;
    Notebook: TNotebook;
    Label8: TLabel;
    Label9: TLabel;
    BuscaTablaPaises: TBuscaTabla;
    BuscaTablaRegiones: TBuscaTabla;
    qry_Paises: TADOQuery;
    qry_Regiones: TADOQuery;
    Ciudades: TADOTable;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    cb_pais: TComboBox;
    cb_regiones: TComboBox;
    clbComunas: TCheckListBox;
    Label1: TLabel;
    ObtenerComunasCiudad: TADOStoredProc;
    EliminarCiudad: TADOStoredProc;
    AgregarCiudadComuna: TADOStoredProc;
    EliminarCiudadComuna: TADOStoredProc;
    procedure BtnCancelarClick(Sender: TObject);
    procedure dblCiudadesClick(Sender: TObject);
    procedure dblCiudadesDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dblCiudadesEdit(Sender: TObject);
    procedure dblCiudadesRefresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure dblCiudadesDelete(Sender: TObject);
    procedure dblCiudadesInsert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    function dblCiudadesProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure cb_paisChange(Sender: TObject);
    procedure cb_regionesChange(Sender: TObject);
  private
    { Private declarations }

	procedure Limpiar_Campos;
    procedure Volver_Campos;
    procedure CargarComunasCiudad(CodigoPais: AnsiString = ''; CodigoRegion: AnsiString = ''; CodigoCiudad: AnsiString = '');
    function  ChequeoMasDeUnaComuna: boolean;
  public
    { Public declarations }
    function Inicializa(MDIChild: Boolean; CodigoPais: AnsiString = ''; CodigoRegion: AnsiString = ''; CodigoCiudad: AnsiString = ''): Boolean;
  end;

var
  FormCiudades: TFormCiudades;

implementation
resourcestring
    MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar la Comuna seleccionada?';
    MSG_DELETE_ERROR		= 'No se pudo eliminar la Comuna seleccionada.';
    MSG_DELETE_CAPTION		= 'Eliminar Comuna';
    MSG_ACTUALIZAR_ERROR 	= 'No se pudieron actualizar los datos de la Comuna seleccionada.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Comuna';
    MSG_PAIS                = 'Debe seleccionar el pa�s';
    MSG_REGION              = 'Debe seleccionar la regi�n';
    MSG_CODIGO              = 'Debe ingresar el c�digo';
    MSG_DESCRIPCION         = 'Debe ingresar el descripci�n';
    MSG_ACTUALIZAR_CIUDAD_COMUNA_ERROR = 'No se pudo Actualizar la Ciudad-Comuna';

{$R *.DFM}

function TFormCiudades.Inicializa(MDIChild: Boolean; CodigoPais: AnsiString = '';
    CodigoRegion: AnsiString = ''; CodigoCiudad: AnsiString = ''): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
   	if (CodigoPais <> '') or (CodigoRegion <> '') then begin
		Ciudades.Filtered := True;
        if (CodigoPais <> '') then Ciudades.Filter := 'CodigoPais=''' + CodigoPais+ '''';
        if (CodigoPais <> '') and (CodigoRegion <> '') then Ciudades.Filter := Ciudades.Filter + ' and CodigoRegion = ''' + CodigoRegion+ ''''
        else if (CodigoRegion <> '') then Ciudades.Filter := 'CodigoRegion = ''' + CodigoRegion+ '''';
    end;


	if not OpenTables([qry_Paises, Ciudades]) then Exit;

    if CodigoPais = '' then CodigoPais := PAIS_CHILE;
    CargarPaises(DMConnections.BaseCAC,cb_pais, CodigoPais);
    CargarRegiones(DMConnections.BaseCAC,cb_regiones,CodigoPais,'');
    CargarComunasCiudad( Trim(Ciudades.FieldByName('CodigoPais').AsString), Trim(Ciudades.FieldByName('CodigoRegion').AsString), Trim(Ciudades.FieldByName('CodigoCiudad').AsString));
	Result := True;
	Volver_Campos;
end;

procedure TFormCiudades.Volver_Campos;
begin
	Notebook.PageIndex          := 0;
    groupb.Enabled              := False;
    cb_Pais.enabled             := True;
    cb_Regiones.enabled         := True;
    txt_CodigoCiudad.Enabled    := True;
  	dblCiudades.Estado          := Normal;
	dblCiudades.Enabled         := True;
	dblCiudades.Reload;
	dblCiudades.SetFocus;
	Screen.Cursor 	        := crDefault;
end;

procedure TFormCiudades.CargarComunasCiudad(CodigoPais: AnsiString = ''; CodigoRegion: AnsiString = ''; CodigoCiudad: AnsiString = '');
var
    i: integer;
begin
    clbComunas.Clear;
    with ObtenerComunasCiudad.Parameters do begin
        ParamByName('@CodigoPais').Value      := Trim(CodigoPais);
        ParamByName('@CodigoRegion').Value    := Trim(CodigoRegion);
        ParamByName('@CodigoCiudad').Value    := iif( Trim(CodigoCiudad) = '', null, Trim(CodigoCiudad));
    end;
    if not OpenTables([ObtenerComunasCiudad]) then exit;

    i:= -1;
    While not ObtenerComunasCiudad.Eof do begin
        inc(i);
        clbComunas.Items.Add(
            Trim(ObtenerComunasCiudad.FieldByName('Descripcion').AsString) + space(200) +
            Trim(ObtenerComunasCiudad.FieldByName('CodigoComuna').AsString));
        clbComunas.Checked[i] := (ObtenerComunasCiudad.FieldByName('Pertenece').AsInteger = 1);
        ObtenerComunasCiudad.Next;
    end;
    ObtenerComunasCiudad.Close
end;

procedure TFormCiudades.BtnCancelarClick(Sender: TObject);
begin
    Volver_Campos;
end;

procedure TFormCiudades.dblCiudadesClick(Sender: TObject);
begin
	With dblCiudades do begin
        CargarPaises(DMConnections.BaseCAC, cb_pais, Ciudades.FieldByName('CodigoPais').AsString);
        CargarRegiones(DMConnections.BaseCAC,cb_regiones,Ciudades.FieldByName('CodigoPais').AsString, Ciudades.FieldByName('CodigoRegion').AsString);
        txt_CodigoCiudad.Text 	:= Ciudades.FieldByName('CodigoCiudad').AsString;
        txt_Descripcion.Text 	:= Ciudades.FieldByName('Descripcion').AsString;
        CargarComunasCiudad(Ciudades.FieldByName('CodigoPais').AsString, Ciudades.FieldByName('CodigoRegion').AsString, Ciudades.FieldByName('CodigoCiudad').AsString);
    end;
end;

procedure TFormCiudades.dblCiudadesDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0] + 1, Rect.Top, QueryGetValue( DMConnections.BaseCAC, 'SELECT Descripcion FROM Paises WHERE CodigoPais = ''' + Tabla.FieldByName('CodigoPais').AsString + ''''));
		TextOut(Cols[1], Rect.Top, QueryGetValue( DMConnections.BaseCAC, 'SELECT Descripcion FROM Regiones WHERE CodigoPais = ''' +	 Tabla.FieldByName('CodigoPais').AsString + ''' AND CodigoRegion = ''' + Tabla.FieldByName('CodigoRegion').AsString + ''''));
		TextOut(Cols[2], Rect.Top, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormCiudades.dblCiudadesEdit(Sender: TObject);
begin
	dblCiudades.Enabled         := False;
    dblCiudades.Estado          := modi;
	Notebook.PageIndex          := 1;
    groupb.Enabled              := True;
    cb_Pais.enabled             := False;
    cb_Regiones.enabled         := False;
    txt_CodigoCiudad.enabled    := False;
    txt_descripcion.setFocus;
end;

procedure TFormCiudades.dblCiudadesRefresh(Sender: TObject);
begin
	 if dblCiudades.Empty then Limpiar_Campos;
end;

procedure TFormCiudades.Limpiar_Campos();
begin
	txt_CodigoCiudad.Clear;
  	txt_Descripcion.Clear;
    CargarPaises(DMConnections.BaseCAC,cb_pais,PAIS_CHILE);
    CargarRegiones(DMConnections.BaseCAC,cb_regiones,PAIS_CHILE, REGION_SANTIAGO);
    CargarComunasCiudad(PAIS_CHILE, REGION_SANTIAGO, '');
end;

procedure TFormCiudades.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormCiudades.dblCiudadesDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_QUESTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			With EliminarCiudad.Parameters do begin
                ParamByName('@CodigoPais').Value := Trim(Ciudades.FieldByName('CodigoPais').AsString);
                ParamByName('@CodigoRegion').Value := Trim(Ciudades.FieldByName('CodigoRegion').AsString);
                ParamByName('@CodigoCiudad').Value := Trim(Ciudades.FieldByName('CodigoCiudad').AsString);
            end;
    		EliminarCiudad.ExecProc;
			EliminarCiudad.Close;
		Except
			On E: Exception do begin
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_QUESTION, MB_ICONSTOP);
				EliminarCiudad.Close;
			end;
		end;
		dblCiudades.Reload;
	end;
	Volver_Campos;
end;

procedure TFormCiudades.dblCiudadesInsert(Sender: TObject);
begin
    groupb.Enabled      := True;
	Limpiar_Campos;
	dblCiudades.Enabled := False;
	Notebook.PageIndex  := 1;
	txt_CodigoCiudad.SetFocus;
    clbComunas.Clear;
    CargarPaises(DMConnections.BaseCAC,cb_pais, PAIS_CHILE);
    CargarRegiones(DMConnections.BaseCAC,cb_regiones, PAIS_CHILE, REGION_SANTIAGO);
    cb_regiones.OnChange(nil);
end;

procedure TFormCiudades.BtnAceptarClick(Sender: TObject);
resourcestring
    ERROR_BORRAR_PARTE1 = 'Los datos se grabaron correctamente, pero ';
    ERROR_BORRAR_PARTE2 = 'no se pudieron eliminar ';
    ERROR_BORRAR_PARTE3 = ' comunas porque tienen calles asociadas';
    MSG_SOLO_UNA_COMUNA = 'Las ciudades distintas de Santiago solo pueden pertenecer a una Comuna.';
var
    TodoOk : Boolean;
    i: integer;
    CadenaNoBorro,
    Pais, Region, Ciudad: AnsiString;
    IntentoBorrar: boolean;
    NoPudoBorrar: smallint;
begin
    if not ValidateControls(
        [cb_pais,
        cb_regiones,
        txt_CodigoCiudad,
        txt_descripcion],
        [Trim(cb_pais.text) <> '',
        Trim(cb_regiones.text) <> '',
        trim(txt_CodigoCiudad.text) <> '',
        trim(txt_descripcion.text) <> ''],
        MSG_ACTUALIZAR_CAPTION,
        [MSG_PAIS,
        MSG_REGION,
        MSG_CODIGO,
        MSG_DESCRIPCION]) then exit;

    Pais := Trim(StrRight(cb_pais.Text, 20));
    Region := Trim(StrRight(cb_regiones.Text, 20));
    Ciudad := Trim(txt_codigoCiudad.Text);

    if (Region <> REGION_SANTIAGO) and (ChequeoMasDeUnaComuna) then begin
        //tiene que haber chequeado solo una comuna
        MsgBox(MSG_SOLO_UNA_COMUNA, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
        exit;
    end;

 	Screen.Cursor := crHourGlass;

    TodoOk := False;
    IntentoBorrar := false;
    NoPudoBorrar := 0;
    CadenaNoBorro := '';

    DMConnections.BaseCAC.BeginTrans;
    try
        //Primero grabo la ciudad
        With Ciudades do begin
            Try
                if dblCiudades.Estado = Alta then Append
                    else Edit;
                FieldByName('CodigoPais').AsString  	:= Pais;
                FieldByName('CodigoRegion').AsString  	:= Region;
                FieldByName('CodigoCiudad').AsString 	:= Ciudad;
                FieldByName('Descripcion').AsString  	:= Trim(txt_descripcion.text);
				FieldByName('DescripcionInterfase').AsString  	:= Trim(txt_descripcion.text);
				Post;
            except
                On E: Exception do begin
                    Cancel;
                    MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                    DMConnections.BaseCAC.RollbackTrans;
                    Exit;
                end;
            end;
        end;
        {AHORA GRABO LAS COMUNAS:
            si esta chequeada
                Si no existe la relacion ciudad comuna
                    Alta
                sino, nada!
            si no esta chequeada
                Si existe la relacion ciudad comuna
                    baja
                sino, nada!
            fin!}
        //Ahora grabo las Comunas
        for i:= 0 to clbComunas.Items.Count -1 do begin
            if clbComunas.Checked[i] then begin
                if not ExisteCiudadComuna (Pais, Region, TRim(StrRight(Trim(clbComunas.Items[i]), 3)), Ciudad) then begin
                    //Alta
                    try
                        with AgregarCiudadComuna.Parameters do begin
                            ParamByName('@CodigoPais').Value    := Pais;
                            ParamByName('@CodigoRegion').Value  := Region;
                            ParamByName('@CodigoComuna').Value  := Trim(StrRight(Trim(clbComunas.Items[i]), 3));
                            ParamByName('@CodigoCiudad').Value  := Ciudad;
                        end;
                        AgregarCiudadComuna.ExecProc;
                        AgregarCiudadComuna.Close;
                    except
                        On E: Exception do begin
                            AgregarCiudadComuna.Close;
                            //MsgBoxErr(MSG_ACTUALIZAR_CIUDAD_COMUNA_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                            //exit;
                        end;
                    end;
                end;
            end
            else begin
                if ExisteCiudadComuna (Pais, Region, TRim(StrRight(Trim(clbComunas.Items[i]), 3)), Ciudad) then begin
                    //Tengo que intentar borrarlas por si estaba marcada y la desmarcan..
                    try
                        IntentoBorrar := true;
                        with EliminarCiudadComuna.Parameters do begin
                            ParamByName('@CodigoPais').Value    := Pais;
                            ParamByName('@CodigoRegion').Value  := Region;
                            ParamByName('@CodigoComuna').Value  := TRim(StrRight(Trim(clbComunas.Items[i]), 3));
                            ParamByName('@CodigoCiudad').Value  := Ciudad;
                        end;
                        EliminarCiudadComuna.ExecProc;
                        EliminarCiudadComuna.Close;
                    except
                        On E: Exception do begin
                            EliminarCiudadComuna.Close;
                            Inc(NoPudoBorrar);
                            CadenaNoBorro := IIf(CadenaNoBorro = '', Trim(Copy(clbComunas.Items[i],1,30)),
                                                CadenaNoBorro + ', ' + Trim(Copy(clbComunas.Items[i],1,30)));
                        end;
                    end;
                end;
            end;
        end;
        TodoOk := True;
    finally
        if TodoOk then begin
            DMConnections.BaseCAC.CommitTrans;
            if (IntentoBorrar) and (NoPudoBorrar > 0) then begin
                MsgBox(ERROR_BORRAR_PARTE1 + #13#10 +
                       ERROR_BORRAR_PARTE2 + IntToStr(NoPudoBorrar) + ERROR_BORRAR_PARTE3 + #13#10 +
                          '(' + CadenaNoBorro + ')', MSG_ACTUALIZAR_CAPTION, MB_ICONWARNING);

            end;
        end
        else DMConnections.BaseCAC.RollbackTrans;
    end;
    Volver_Campos;
end;

procedure TFormCiudades.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormCiudades.BtnSalirClick(Sender: TObject);
begin
     close;
end;

function TFormCiudades.dblCiudadesProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	 Result := True;
	 //Texto :=  PadR(Trim(Tabla.FieldByName('CodigoDocumento').AsString), 4, ' ' ) + ' '+ Tabla.FieldByName('Descripcion').AsString;
end;

procedure TFormCiudades.cb_paisChange(Sender: TObject);
begin
    CargarRegiones(DMConnections.BaseCAC,cb_regiones,Trim(StrRight(cb_pais.Text, 20)),'');
	CargarComunasCiudad( StrRight(cb_Pais.Text, 3), StrRight(cb_Regiones.Text, 3), Trim(txt_CodigoCiudad.Text));
end;

procedure TFormCiudades.cb_regionesChange(Sender: TObject);
begin
    CargarComunasCiudad( StrRight(cb_Pais.Text, 3), StrRight(cb_Regiones.Text, 3), Trim(txt_CodigoCiudad.Text));
end;

function TFormCiudades.ChequeoMasDeUnaComuna: boolean;
var
    i, cantidad: smallint;
begin
    cantidad := 0;
    for i:= 0 to clbComunas.Items.Count -1 do begin
        if clbComunas.Checked[i] then
            Inc(cantidad);
    end;
    result := cantidad > 1;
end;

end.
