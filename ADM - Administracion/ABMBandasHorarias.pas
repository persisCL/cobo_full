unit ABMBandasHorarias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ComCtrls, ToolWin, ImgList, ExtCtrls, StdCtrls,
  Validate, TimeEdit, VariantComboBox, DB, ADODB, Provider, DBClient, DMConnection,
  UtilDB, UtilProc, Util;

type
  TBandaHoraria = record
      CodigoBandaHoraria: Integer;
      CodigoDiaTipo: string;
      //Temporada: string;      // TASK_155_MGO_20170317
      HoraDesde: TDateTime;
      HoraHasta: TDateTime;
  end;
  TfrmABMBandasHorarias = class(TForm)
    pnlSuperior: TPanel;
    dbgrdEsquemas: TDBGrid;
    ilImagenes: TImageList;
    pnl1: TPanel;
    tlbBotonera: TToolBar;
    btnSalir: TToolButton;
    btn1: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    btn2: TToolButton;
    btnImprimir: TToolButton;
    btn3: TToolButton;
    btnBuscar: TToolButton;
    pnlMedio: TPanel;
    pnl2: TPanel;
    tlb1: TToolBar;
    btnAgregarHorario: TToolButton;
    btnEliminarHorario: TToolButton;
    btnEditarHorario: TToolButton;
    pnl3: TPanel;
    dbgrdBandasHorarias: TDBGrid;
    pnl4: TPanel;
    pnlInferior: TPanel;
    lbl1: TLabel;
    lbl2: TLabel;
    edtCodigoEsquema: TEdit;
    edtDescripcion: TEdit;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    //lbl7: TLabel;	// TASK_155_MGO_20170317
    lbl8: TLabel;
    vcbTipoDia: TVariantComboBox;
    //vcbTemporada: TVariantComboBox;	// TASK_155_MGO_20170317
    vcbTarifa: TVariantComboBox;
    tedtDesde: TTimeEdit;
    tedtHasta: TTimeEdit;
    dsEsquemas: TDataSource;
    dsBandasHorarias: TDataSource;
    cdsEsquemas: TClientDataSet;
    cdsBandasHorarias: TClientDataSet;
    dtstprEsquemas: TDataSetProvider;
    dtstprBandasHorarias: TDataSetProvider;
    spADM_Esquemas_SELECT: TADOStoredProc;
    spADM_BandasHorarias_SELECT: TADOStoredProc;
    nb1: TNotebook;
    btnSalir1: TButton;
    btnCancelar: TButton;
    btnGuardar: TButton;
    intgrfldADM_Esquemas_SELECTCodigoEsquema: TIntegerField;
    strngfldADM_Esquemas_SELECTDescripcion: TStringField;
    strngfldADM_Esquemas_SELECTUsuarioCreador: TStringField;
    dtmfldADM_Esquemas_SELECTFechaCreacion: TDateTimeField;
    strngfldADM_Esquemas_SELECTUsuarioModificador: TStringField;
    dtmfldADM_Esquemas_SELECTFechaModificacion: TDateTimeField;
    blnfldADM_Esquemas_SELECTCompleto: TBooleanField;
    spADM_Esquemas_INSERT: TADOStoredProc;
    spADM_Esquemas_UPDATE: TADOStoredProc;
    spADM_Esquemas_DELETE: TADOStoredProc;
    spADM_BandasHorarias_UPDATE: TADOStoredProc;
    spADM_BandasHorarias_INSERT: TADOStoredProc;
    spADM_BandasHorarias_DELETE: TADOStoredProc;
    strngfldADM_BandasHorarias_SELECTTipoDiaDescripcion: TStringField;
    //strngfldADM_BandasHorarias_SELECTTemporada: TStringField;		// TASK_155_MGO_20170317
    strngfldADM_BandasHorarias_SELECTTarifa: TStringField;
    strngfldADM_BandasHorarias_SELECTUsuarioCreador: TStringField;
    dtmfldADM_BandasHorarias_SELECTFechaCreacion: TDateTimeField;
    strngfldADM_BandasHorarias_SELECTUsuarioModificador: TStringField;
    dtmfldADM_BandasHorarias_SELECTFechaModificacion: TDateTimeField;
    intgrfldADM_BandasHorarias_SELECTCodigoBandaHoraria: TIntegerField;
    intgrfldADM_BandasHorarias_SELECTCodigoEsquema: TIntegerField;
    strngfldADM_BandasHorarias_SELECTCodigoDiaTipo: TStringField;
    strngfldADM_BandasHorarias_SELECTCodigoTarifaTipo: TStringField;
    btnAceptar: TButton;
    strngfldADM_BandasHorarias_SELECTHoraDesde: TStringField;
    strngfldADM_BandasHorarias_SELECTHoraHasta: TStringField;
    btnCancelarHorario: TButton;
    strngfldADM_Esquemas_SELECTCompletoDesc: TStringField;
    lbl9: TLabel;
	// INICIO : TASK_155_MGO_20170317
    lbl7: TLabel;
    vcbCategoriaClase: TVariantComboBox;
    procADM_Esquemas_SELECTId_CategoriasClases: TIntegerField;
    strngfldADM_Esquemas_SELECTCategoriasClasesDesc: TStringField;
	// FIN : TASK_155_MGO_20170317
    procedure btnSalir1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure cdsEsquemasAfterScroll(DataSet: TDataSet);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure cdsBandasHorariasAfterScroll(DataSet: TDataSet);
    procedure btnAgregarHorarioClick(Sender: TObject);
    procedure btnEditarHorarioClick(Sender: TObject);
    procedure btnEliminarHorarioClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure vcbTipoDiaChange(Sender: TObject);
    //procedure vcbTemporadaChange(Sender: TObject);                            // TASK_155_MGO_20170317
    procedure btnCancelarHorarioClick(Sender: TObject);
    procedure dbgrdEsquemasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    Accion: Integer; // 0: Consultar; 1: Insertar; 2: Actualizar
    FCodigoEsquema,
    FCodigoBandaHoraria: Integer;
    FGuardandoDatos: Boolean;
    FBandasHorariasEliminadas: array of TBandaHoraria;
    FBandasHorarias: array of TBandaHoraria;

    //function ObtenerIndiceBandaHoraria(CodigoBandaHoraria: Integer; CodigoDiaTipo: string;    // TASK_155_MGO_20170317
    //    Temporada: string; HoraDesde: TDateTime; HoraHasta: TDateTime): Integer;              // TASK_155_MGO_20170317
    function ObtenerIndiceBandaHoraria(CodigoBandaHoraria: Integer; CodigoDiaTipo: string;      // TASK_155_MGO_20170317
       HoraDesde: TDateTime; HoraHasta: TDateTime): Integer;                                    // TASK_155_MGO_20170317
    //function ObtenerHoraHastaUltimaBandaHoraria(CodigoDiaTipo: string; Temporada: string): TDateTime;     // TASK_155_MGO_20170317
    function ObtenerHoraHastaUltimaBandaHoraria(CodigoDiaTipo: string): TDateTime;                          // TASK_155_MGO_20170317
    function ObtenerHoraDesdeNueva: TDateTime; 
    function GuardarCambiosHorario: Boolean;
    procedure EliminarItemBandaHoraria(const Index: Integer);
    procedure Limpiar_Campos(SoloHorario: Boolean = False);
    procedure CargarBandasHorarias;                 
    procedure HabilitarControlesEsquema(Estado : Boolean);
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

resourcestring
  QRY_EXISTE_PDC = 'SELECT dbo.EsquemaExisteEnPuntosCobro(%d)';

var
  frmABMBandasHorarias: TfrmABMBandasHorarias;

implementation

{$R *.dfm}

function TfrmABMBandasHorarias.Inicializar: Boolean;
var
  sp: TADOStoredProc;
begin
    Result := True;

    Accion := 0;

    vcbTipoDia.Clear;
    vcbTarifa.Clear;
    //vcbTemporada.Clear;       // TASK_155_MGO_20170317
    vcbCategoriaClase.Clear;    // TASK_155_MGO_20170317
    sp := TADOStoredProc.Create(Self);
    try
        try
            // Se cargan los tipos de d�a
            sp.Connection := DMConnections.BaseBO_Rating;
            sp.ProcedureName := 'PlanTarifarioDiasTipos_Obtener';
            sp.CommandTimeout := 30;
            sp.Parameters.Refresh;
            sp.Open;
            vcbTipoDia.Items.Add('(seleccione)','');
            while not sp.Eof do begin
                vcbTipoDia.Items.Add(
                    sp.FieldByName('Descripcion').AsString,
                    sp.FieldByName('CodigoDiaTipo').AsString);
                sp.Next;
            end;
            sp.Close;
            vcbTipoDia.ItemIndex := 0;

            // Se cargan los tipos de Tarifa
            sp.ProcedureName := 'PlanTarifarioTarifasTipos_Obtener';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@MostrarEnBandasHorarias').Value := 1;   // TASK_155_MGO_20170317
            sp.Open;
            vcbTarifa.Items.Add('(seleccione)','');
            while not sp.Eof do begin
                vcbTarifa.Items.Add(
                    sp.FieldByName('Descripcion').AsString,
                    sp.FieldByName('CodigoTarifaTipo').AsString);
                sp.Next;
            end;
            sp.Close;
            vcbTarifa.ItemIndex := 0;

            { INICIO : TASK_155_MGO_20170317
            vcbTemporada.Items.Add('(seleccione)','');
            vcbTemporada.Items.Add('Normal', 'N');
            vcbTemporada.Items.Add('Alta', 'A');
            vcbTemporada.ItemIndex := 0;
            }
            sp.ProcedureName := 'CategoriaClases_Obtener';
            sp.Parameters.Refresh;
            sp.Open;
            vcbCategoriaClase.Items.Add('(seleccione)','');
            while not sp.Eof do begin
                vcbCategoriaClase.Items.Add(
                    sp.FieldByName('Descripcion').AsString,
                    sp.FieldByName('ID_CategoriasClases').AsInteger);
                sp.Next;
            end;
            sp.Close;
            vcbCategoriaClase.ItemIndex := 0;
            // FIN : TASK_155_MGO_20170317
            
            Limpiar_Campos(False);

            //Se obtienen los Esquemas de horarios
            spADM_Esquemas_SELECT.Close;
            spADM_Esquemas_SELECT.Parameters.Refresh;
            spADM_Esquemas_SELECT.Open;

            if spADM_Esquemas_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spADM_Esquemas_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

            cdsEsquemas.Data := dtstprEsquemas.Data;
            cdsEsquemas.First;

            HabilitarControlesEsquema(False);
        except
            on e: Exception do begin
                MsgBoxErr('Error inicializando el formulario', e.Message, 'Error', MB_ICONERROR);
                Result := False;
            end;
        end;
    finally
        sp.Free;
    end;
end;

function TfrmABMBandasHorarias.ObtenerIndiceBandaHoraria(CodigoBandaHoraria: Integer; CodigoDiaTipo: string;
    //Temporada: string; HoraDesde: TDateTime; HoraHasta: TDateTime): Integer;      // TASK_155_MGO_20170317
    HoraDesde: TDateTime; HoraHasta: TDateTime): Integer;                           // TASK_155_MGO_20170317
var
    I: Integer;
begin       
    Result := -1;
    if CodigoBandaHoraria > 0 then begin
        for I := Low(FBandasHorarias) to High(FBandasHorarias) do begin
            if (FBandasHorarias[I].CodigoBandaHoraria = CodigoBandaHoraria) then
            begin
                Result := I;
                break;
            end;
        end;
    end else begin
        for I := Low(FBandasHorarias) to High(FBandasHorarias) do begin
            if (FBandasHorarias[I].CodigoDiaTipo = CodigoDiaTipo) and
               //(FBandasHorarias[I].Temporada = Temporada) and                 // TASK_155_MGO_20170317
               (FBandasHorarias[I].HoraDesde = HoraDesde) and
               (FBandasHorarias[I].HoraHasta = HoraHasta) then
            begin
                Result := I;
                break;
            end;
        end;
    end;
end;

{ INICIO : TASK_155_MGO_20170317
procedure TfrmABMBandasHorarias.vcbTemporadaChange(Sender: TObject);
begin
    tedtDesde.Time := ObtenerHoraDesdeNueva;
end;
} // FIN : TASK_155_MGO_20170317

procedure TfrmABMBandasHorarias.vcbTipoDiaChange(Sender: TObject);
begin
    tedtDesde.Time := ObtenerHoraDesdeNueva;    
end;

function TfrmABMBandasHorarias.ObtenerHoraDesdeNueva: TDateTime;
resourcestring
    //MSG_BANDA_COMPLETA = 'Ya se ha completado toda la Banda Horaria para el Tipo de D�a y Temporada seleccionados';      // TASK_155_MGO_20170317
    MSG_BANDA_COMPLETA = 'Ya se ha completado toda la Banda Horaria para el Tipo de D�a seleccionado';                     // TASK_155_MGO_20170317
var
    UltimaHoraHasta: TDateTime;
    myHour, myMin, mySec, myMilli : Word;
begin
    Result := tedtDesde.Time;

    if FCodigoBandaHoraria > -1 then Exit;

    //if (vcbTipoDia.Value = EmptyStr) or (vcbTemporada.Value = EmptyStr) then Exit;                    // TASK_155_MGO_20170317

    //UltimaHoraHasta := ObtenerHoraHastaUltimaBandaHoraria(vcbTipoDia.Value, vcbTemporada.Value);      // TASK_155_MGO_20170317
    UltimaHoraHasta := ObtenerHoraHastaUltimaBandaHoraria(vcbTipoDia.Value);                            // TASK_155_MGO_20170317
    if UltimaHoraHasta > 0 then
        UltimaHoraHasta := UltimaHoraHasta + (1/1440);

    if UltimaHoraHasta = 1 then begin
        UltimaHoraHasta := 0;
        MsgBox(MSG_BANDA_COMPLETA, 'Aviso', MB_ICONINFORMATION);
        btnAceptar.Enabled := False;
        btnAceptar.Default := False;
        btnGuardar.Default := True;
        btnCancelarHorario.Enabled := False;
    end else begin
        btnAceptar.Enabled := True;
        btnAceptar.Default := True;
        btnGuardar.Default := False;
        btnCancelarHorario.Enabled := True;
    end;

    Result := UltimaHoraHasta;
end;

//function TfrmABMBandasHorarias.ObtenerHoraHastaUltimaBandaHoraria(CodigoDiaTipo: string; Temporada: string): TDateTime;   // TASK_155_MGO_20170317
function TfrmABMBandasHorarias.ObtenerHoraHastaUltimaBandaHoraria(CodigoDiaTipo: string): TDateTime;                        // TASK_155_MGO_20170317
var
    I: Integer;
    MaxHoraHasta: TDateTime;
begin
    MaxHoraHasta := 0;
    for I := Low(FBandasHorarias) to High(FBandasHorarias) do begin
        //if (FBandasHorarias[I].CodigoDiaTipo = CodigoDiaTipo) and             // TASK_155_MGO_20170317
        //   (FBandasHorarias[I].Temporada = Temporada) then                    // TASK_155_MGO_20170317
        if (FBandasHorarias[I].CodigoDiaTipo = CodigoDiaTipo) then              // TASK_155_MGO_20170317
        begin
            if FBandasHorarias[I].HoraHasta > MaxHoraHasta then
                MaxHoraHasta := FBandasHorarias[I].HoraHasta;
        end;
    end;

    Result := MaxHoraHasta;
end;

procedure TfrmABMBandasHorarias.EliminarItemBandaHoraria(const Index: Integer);
var
    ALength: Integer;
    i: Integer;
begin
    if Index = -1 then Exit;

    ALength := Length(FBandasHorarias);
    Assert(ALength > 0);
    Assert(Index < ALength);
    for i := Index + 1 to ALength - 1 do
        FBandasHorarias[i - 1] := FBandasHorarias[i];
    SetLength(FBandasHorarias, ALength - 1);
end;

procedure TfrmABMBandasHorarias.HabilitarControlesEsquema(Estado: Boolean);
begin
    btnSalir.Enabled      := not Estado;
    btnAgregar.Enabled    := not Estado;
    btnEliminar.Enabled   := (not Estado) and (cdsEsquemas.RecordCount > 0);
    btnEditar.Enabled     := (not Estado) and (cdsEsquemas.RecordCount > 0);
    dbgrdEsquemas.Enabled := not Estado;

    edtDescripcion.Enabled := Estado;
    vcbCategoriaClase.Enabled := Estado;    // TASK_155_MGO_20170317

    btnAgregarHorario.Enabled := Estado;
    btnEditarHorario.Enabled := Estado and (cdsBandasHorarias.RecordCount > 0);
    btnEliminarHorario.Enabled := Estado and (cdsBandasHorarias.RecordCount > 0);
    dbgrdBandasHorarias.Enabled := Estado;

    vcbTipoDia.Enabled := False;
    tedtDesde.Enabled := False;   
    tedtHasta.Enabled := False;
    //vcbTemporada.Enabled := False;        // TASK_155_MGO_20170317
    vcbTarifa.Enabled := False;
    btnAceptar.Enabled := False;
    btnAceptar.Default := False;
    btnGuardar.Default := True;
    btnCancelarHorario.Enabled := False;

    btnGuardar.Enabled := Estado;
    btnCancelar.Enabled := Estado;

    if Estado then
        nb1.ActivePage := 'pgAltaModi'
    else
        nb1.ActivePage := 'pgSalir';
end;

procedure TfrmABMBandasHorarias.Limpiar_Campos(SoloHorario: Boolean = False);
begin
    if not SoloHorario then begin
        edtCodigoEsquema.Clear;
        edtDescripcion.Clear;
        vcbCategoriaClase.ItemIndex := 0;   // TASK_155_MGO_20170317
    end else
        HabilitarControlesEsquema(True);
        
    vcbTarifa.ItemIndex := 0;
    vcbTipoDia.ItemIndex := 0;
    //vcbTemporada.ItemIndex := 0;          // TASK_155_MGO_20170317
    tedtDesde.Time := 0;
    tedtHasta.Time := 0;
end;

procedure TfrmABMBandasHorarias.btnAceptarClick(Sender: TObject);
begin
    GuardarCambiosHorario;
end;

function TfrmABMBandasHorarias.GuardarCambiosHorario: Boolean;
resourcestring
    MSG_ERROR_BANDA = 'Error guardando los cambios de la Banda Horaria';
var
    I: Integer;
begin
    Result := True;

    if vcbTipoDia.Value = EmptyStr then begin
        MsgBoxBalloon('Debe seleccionar el Tipo de D�a', 'Error', MB_ICONSTOP, vcbTipoDia);
        Result := False;
        Exit
    end;

    { INICIO : TASK_155_MGO_20170317
    if vcbTemporada .Value = EmptyStr then begin
        MsgBoxBalloon('Debe seleccionar la Temporada', 'Error', MB_ICONSTOP, vcbTemporada);
        Result := False;
        Exit
    end;
    } // FIN : TASK_155_MGO_20170317

    if not (tedtHasta.Time > tedtDesde.Time) then begin
        MsgBoxBalloon('La hora Hasta debe ser mayor a la hora Desde', 'Error', MB_ICONSTOP, tedtHasta);  
        Result := False;
        Exit
    end;
    
    if vcbTarifa.Value = EmptyStr then begin
        MsgBoxBalloon('Debe seleccionar la Tarifa', 'Error', MB_ICONSTOP, vcbTarifa);  
        Result := False;
        Exit
    end;

    try
        FGuardandoDatos := True;
        cdsBandasHorarias.DisableControls;

        try
            if FCodigoBandaHoraria > -1 then begin
                cdsBandasHorarias.Edit;
                cdsBandasHorarias.FieldByName('HoraHasta').Value := tedtHasta.Text;
                cdsBandasHorarias.FieldByName('CodigoTarifaTipo').Value := vcbTarifa.Value;
                cdsBandasHorarias.FieldByName('TipoTarifaDescripcion').Value := vcbTarifa.Text;
                cdsBandasHorarias.Post;

                //I := ObtenerIndiceBandaHoraria(FCodigoBandaHoraria, EmptyStr, EmptyStr, 0, 0);                            // TASK_155_MGO_20170317
                I := ObtenerIndiceBandaHoraria(FCodigoBandaHoraria, vcbTipoDia.Value, tedtDesde.Time, tedtHasta.Time);      // TASK_155_MGO_20170317
                FBandasHorarias[I].HoraHasta := tedtHasta.Time;
            end else begin
                cdsBandasHorarias.Append;
                cdsBandasHorarias.FieldByName('CodigoBandaHoraria').Value := 0;
                cdsBandasHorarias.FieldByName('CodigoEsquema').Value := FCodigoEsquema;
                cdsBandasHorarias.FieldByName('CodigoDiaTipo').Value := vcbTipoDia.Value;
                cdsBandasHorarias.FieldByName('TipoDiaDescripcion').Value := vcbTipoDia.Text;
                cdsBandasHorarias.FieldByName('HoraDesde').Value := tedtDesde.Text;
                cdsBandasHorarias.FieldByName('HoraHasta').Value := tedtHasta.Text;
                //cdsBandasHorarias.FieldByName('Temporada').Value := vcbTemporada.Value;       // TASK_155_MGO_20170317
                cdsBandasHorarias.FieldByName('CodigoTarifaTipo').Value := vcbTarifa.Value;
                cdsBandasHorarias.FieldByName('TipoTarifaDescripcion').Value := vcbTarifa.Text;
                cdsBandasHorarias.Post;

                SetLength(FBandasHorarias, Length(FBandasHorarias) + 1);
                I := High(FBandasHorarias);
                FBandasHorarias[I].CodigoBandaHoraria := 0;
                FBandasHorarias[I].CodigoDiaTipo := vcbTipoDia.Value;
                //FBandasHorarias[I].Temporada := vcbTemporada.Value;           // TASK_155_MGO_20170317
                FBandasHorarias[I].HoraDesde := tedtDesde.Time;
                FBandasHorarias[I].HoraHasta := tedtHasta.Time;

                FCodigoBandaHoraria := 0;                                       // TASK_155_MGO_20170317
            end;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_BANDA, e.Message, 'Error', MB_ICONERROR);
                Result := False;
            end;
        end;
    finally
        FGuardandoDatos := False;
        cdsBandasHorarias.EnableControls;
        HabilitarControlesEsquema(True);
    end;
end;

procedure TfrmABMBandasHorarias.btnAgregarClick(Sender: TObject);
begin
    Accion := 1;
    Limpiar_Campos(False);
    HabilitarControlesEsquema(True);
    edtDescripcion.SetFocus;
    FCodigoEsquema := 0;
    CargarBandasHorarias;
end;

procedure TfrmABMBandasHorarias.btnAgregarHorarioClick(Sender: TObject);
begin
    Limpiar_Campos(True);
    FCodigoBandaHoraria := -1;

    dbgrdBandasHorarias.Enabled := False;
    btnAceptar.Enabled := True;
    btnAceptar.Default := True;
    btnGuardar.Default := False;
    btnCancelarHorario.Enabled := True;
    btnEliminarHorario.Enabled := False;
    btnEditarHorario.Enabled := False;
    btnAgregarHorario.Enabled := False;

    vcbTipoDia.Enabled := True;
    //vcbTemporada.Enabled := True;         // TASK_155_MGO_20170317
    vcbTarifa.Enabled := True;
    tedtHasta.Enabled := True;
    vcbTipoDia.SetFocus;
end;

procedure TfrmABMBandasHorarias.btnCancelarClick(Sender: TObject);
resourcestring
    MSG_CONFIRM = 'Los cambios realizados sobre el Esquema y sus Bandas Horarias se perder�n. �Desea continuar?.';
    MSG_TITLE = 'Confirmaci�n';
begin
    if MsgBox(MSG_CONFIRM, MSG_TITLE, MB_YESNO+MB_ICONWARNING) = ID_NO then
        Exit;

    if FCodigoEsquema = 0 then
        cdsEsquemas.First;
    
    Accion := 0;
    HabilitarControlesEsquema(False);
    CargarBandasHorarias;
end;

procedure TfrmABMBandasHorarias.btnEditarClick(Sender: TObject);
begin
    if FCodigoEsquema = 0 then Exit;

    Accion := 2;
    HabilitarControlesEsquema(True);   
    edtDescripcion.SetFocus;
end;

procedure TfrmABMBandasHorarias.btnEditarHorarioClick(Sender: TObject);
var
    UltimaHoraHasta: TDateTime;
begin
    dbgrdBandasHorarias.Enabled := False;
    btnAceptar.Enabled := True;
    btnAceptar.Default := True;
    btnGuardar.Default := False;
    btnCancelarHorario.Enabled := True;
    btnEliminarHorario.Enabled := False;
    btnEditarHorario.Enabled := False;
    btnAgregarHorario.Enabled := False;
    
    vcbTarifa.Enabled := True;
    vcbTarifa.SetFocus;
    
    UltimaHoraHasta := ObtenerHoraHastaUltimaBandaHoraria(
                            cdsBandasHorarias.FieldByName('CodigoDiaTipo').AsString);       // TASK_155_MGO_20170317
                            //cdsBandasHorarias.FieldByName('Temporada').AsString);         // TASK_155_MGO_20170317
    tedtHasta.Enabled := (tedtHasta.Time = UltimaHoraHasta);
end;

procedure TfrmABMBandasHorarias.btnEliminarClick(Sender: TObject);
resourcestring
    MSG_CONFIRM = '�Desea eliminar el Esquema Horario seleccionado?';
    MSG_TITLE = 'Confirmaci�n';
    MSG_ERROR = 'Error eliminando el Esquema';
    MSG_EXISTE_PDC = 'No es posible eliminar el Esquema porque est� asignado a un Punto de Cobro';
begin
    if MsgBox(MSG_CONFIRM, MSG_TITLE, MB_YESNO+MB_ICONQUESTION) = ID_NO then
        Exit;

    if QueryGetValueInt(DMConnections.BaseBO_Rating, Format(QRY_EXISTE_PDC, [FCodigoEsquema]), 30) = 1 then begin
        MsgBox(MSG_EXISTE_PDC, 'Error', MB_ICONSTOP);
        Exit;
    end;

    try
        spADM_Esquemas_DELETE.Parameters.Refresh;
        spADM_Esquemas_DELETE.Parameters.ParamByName('@CodigoEsquema').Value := FCodigoEsquema;
        spADM_Esquemas_DELETE.ExecProc;

        if spADM_Esquemas_DELETE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
            raise Exception.Create(spADM_Esquemas_DELETE.Parameters.ParamByName('@ErrorDescription').Value);
    except
        on e: Exception do
            MsgBoxErr(MSG_ERROR, e.Message, 'Error', MB_ICONERROR);
    end;

    cdsEsquemas.Delete;
    HabilitarControlesEsquema(False);
end;

procedure TfrmABMBandasHorarias.btnSalir1Click(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMBandasHorarias.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMBandasHorarias.cdsBandasHorariasAfterScroll(DataSet: TDataSet);
begin
    if FGuardandoDatos or (cdsBandasHorarias.RecordCount = 0) then Exit;

    FCodigoBandaHoraria := cdsBandasHorarias.FieldByName('CodigoBandaHoraria').AsInteger;
    vcbTipoDia.Value := cdsBandasHorarias.FieldByName('CodigoDiaTipo').AsString;
    if cdsBandasHorarias.FieldByName('HoraDesde').AsString <> EmptyStr then
        tedtDesde.Time := cdsBandasHorarias.FieldByName('HoraDesde').AsDateTime;
    if cdsBandasHorarias.FieldByName('HoraHasta').AsString <> EmptyStr then
        tedtHasta.Time := cdsBandasHorarias.FieldByName('HoraHasta').AsDateTime;
    //vcbTemporada.Value := cdsBandasHorarias.FieldByName('Temporada').AsString;        // TASK_155_MGO_20170317
    vcbTarifa.Value := cdsBandasHorarias.FieldByName('CodigoTarifaTipo').AsString;
end;

procedure TfrmABMBandasHorarias.cdsEsquemasAfterScroll(DataSet: TDataSet);
begin
    if FGuardandoDatos then Exit;    

    FCodigoEsquema          := cdsEsquemas.FieldByName('CodigoEsquema').AsInteger;
    edtCodigoEsquema.Text   := cdsEsquemas.FieldByName('CodigoEsquema').AsString;
    edtDescripcion.Text     := cdsEsquemas.FieldByName('Descripcion').AsString;
    vcbCategoriaClase.Value := cdsEsquemas.FieldByName('ID_CategoriasClases').AsInteger;    // TASK_155_MGO_20170317
    CargarBandasHorarias;
end;

procedure TfrmABMBandasHorarias.dbgrdEsquemasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
    if not cdsEsquemas.FieldByName('Completo').AsBoolean then begin
        dbgrdEsquemas.Canvas.Font.Color := clRed;
        dbgrdEsquemas.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
end;

procedure TfrmABMBandasHorarias.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmABMBandasHorarias.CargarBandasHorarias;
resourcestring
    MSG_ERROR = 'Error obteniendo Bandas Horarias';
begin
    SetLength(FBandasHorarias, 0);
    SetLength(FBandasHorariasEliminadas, 0);

    try
        try
            spADM_BandasHorarias_SELECT.Parameters.Refresh;
            spADM_BandasHorarias_SELECT.Parameters.ParamByName('@CodigoEsquema').Value := FCodigoEsquema;
            spADM_BandasHorarias_SELECT.Open;

            if spADM_BandasHorarias_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spADM_BandasHorarias_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

            cdsBandasHorarias.Data := dtstprBandasHorarias.Data;
            cdsBandasHorarias.First;

            spADM_BandasHorarias_SELECT.First;
            while not spADM_BandasHorarias_SELECT.Eof do begin
                SetLength(FBandasHorarias, Length(FBandasHorarias) + 1);
                FBandasHorarias[High(FBandasHorarias)].CodigoBandaHoraria := spADM_BandasHorarias_SELECT.FieldByName('CodigoBandaHoraria').AsInteger;
                FBandasHorarias[High(FBandasHorarias)].CodigoDiaTipo := spADM_BandasHorarias_SELECT.FieldByName('CodigoDiaTipo').AsString;
                //FBandasHorarias[High(FBandasHorarias)].Temporada := spADM_BandasHorarias_SELECT.FieldByName('Temporada').AsString;        // TASK_155_MGO_20170317
                FBandasHorarias[High(FBandasHorarias)].HoraDesde := spADM_BandasHorarias_SELECT.FieldByName('HoraDesde').AsDateTime;
                FBandasHorarias[High(FBandasHorarias)].HoraHasta := spADM_BandasHorarias_SELECT.FieldByName('HoraHasta').AsDateTime;

                spADM_BandasHorarias_SELECT.Next;
            end;
        except
            on e: Exception do
                MsgBoxErr(MSG_ERROR, e.Message, 'Error', MB_ICONERROR);
        end;
    finally
        spADM_BandasHorarias_SELECT.Close;
    end;
end;

procedure TfrmABMBandasHorarias.btnEliminarHorarioClick(Sender: TObject);
resourcestring
    //MSG_NO_ULTIMA = 'La Banda Horaria debe ser la �ltima de su Tipo de D�a y Temporada para poder ser eliminada';       // TASK_155_MGO_20170317
    MSG_NO_ULTIMA = 'La Banda Horaria debe ser la �ltima de su Tipo de D�a para poder ser eliminada';                     // TASK_155_MGO_20170317
    MSG_CONFIRM = '�Desea eliminar la Banda Horaria seleccionada?';
    MSG_TITLE = 'Confirmaci�n';
var
    UltimaHoraHasta: TDateTime;
begin
    UltimaHoraHasta := ObtenerHoraHastaUltimaBandaHoraria(
                            cdsBandasHorarias.FieldByName('CodigoDiaTipo').AsString);       // TASK_155_MGO_20170317
                            //cdsBandasHorarias.FieldByName('Temporada').AsString);         // TASK_155_MGO_20170317

    if cdsBandasHorarias.FieldByName('HoraHasta').AsDateTime <> UltimaHoraHasta then begin
        MsgBox(MSG_NO_ULTIMA, 'Error', MB_ICONINFORMATION);
        Exit;
    end;

    if MsgBox(MSG_CONFIRM, MSG_TITLE, MB_YESNO+MB_ICONQUESTION) = ID_NO then
        Exit;

    if FCodigoBandaHoraria > 0 then begin
        SetLength(FBandasHorariasEliminadas, Length(FBandasHorariasEliminadas) + 1);
        FBandasHorariasEliminadas[High(FBandasHorariasEliminadas)].CodigoBandaHoraria := FCodigoBandaHoraria;
    end;

    EliminarItemBandaHoraria(
        ObtenerIndiceBandaHoraria(
            FCodigoBandaHoraria,
            cdsBandasHorarias.FieldByName('CodigoDiaTipo').AsString,
            //cdsBandasHorarias.FieldByName('Temporada').AsString,      // TASK_155_MGO_20170317
            cdsBandasHorarias.FieldByName('HoraDesde').AsDateTime,
            cdsBandasHorarias.FieldByName('HoraHasta').AsDateTime));

    cdsBandasHorarias.Delete;
    HabilitarControlesEsquema(True);
end;

procedure TfrmABMBandasHorarias.btnGuardarClick(Sender: TObject);
resourcestring
    MSG_ERROR_INSERT = 'Ha ocurrido un error al insertar el nuevo Esquema';
    MSG_ERROR_UPDATE = 'Ha ocurrido un error al actualizar el Esquema';
    MSG_ERROR_BANDA_INSERT = 'Ha ocurrido un error al insertar la Banda Horaria %d';
    MSG_ERROR_BANDA_UPDATE = 'Ha ocurrido un error al actualizar la Banda Horaria %d';
    MSG_ERROR_BANDA_DELETE = 'Ha ocurrido un error al eliminar las Bandas Horarias descartadas';
    MSG_EXISTE_PDC = 'El Esquema no puede guardarse incompleto ya que se encuentra asignado a un Punto de Cobro';
    MSG_FALTA_DESC = 'Debe ingresar una descripci�n';
    MSG_FALTA_CLASE = 'Debe seleccionar la clase de categor�a';                 // TASK_155_MGO_20170317
function EsquemaEstaCompleto: Boolean;
    var
      I, J: Integer;
      UltimaHoraHasta: TDateTime;
    begin
        Result := True;

        for I := 1 to vcbTipoDia.Items.Count - 1 do begin
            //for J := 1 to vcbTemporada.Items.Count - 1 do begin               // TASK_155_MGO_20170317
            //UltimaHoraHasta := ObtenerHoraHastaUltimaBandaHoraria(vcbTipoDia.Items[I].Value, vcbTemporada.Items[J].Value);    // TASK_155_MGO_20170317
            UltimaHoraHasta := ObtenerHoraHastaUltimaBandaHoraria(vcbTipoDia.Items[I].Value);                                   // TASK_155_MGO_20170317

            if UltimaHoraHasta > 0 then
                UltimaHoraHasta := UltimaHoraHasta + (1/1440);

            if UltimaHoraHasta < 1 then begin
                Result := False;
                Break;
            end;
            //end;                                                              // TASK_155_MGO_20170317
            if not Result then Break;            
        end;
    end;
var
    I: Integer;
    Completo: Boolean;
begin
    if edtDescripcion.Text = EmptyStr then begin
        MsgBoxBalloon(MSG_FALTA_DESC, 'Error', MB_ICONSTOP, edtDescripcion);
        Exit;
    end;

    // INICIO : TASK_155_MGO_20170317
    if vcbCategoriaClase.ItemIndex <= 0 then begin
        MsgBoxBalloon(MSG_FALTA_CLASE, 'Error', MB_ICONSTOP, vcbCategoriaClase);
        Exit;
    end;
    // FIN : TASK_155_MGO_20170317

    if btnAceptar.Enabled then
        if not GuardarCambiosHorario then Exit;    

    try
        FGuardandoDatos := True;
        cdsEsquemas.DisableControls;
        cdsBandasHorarias.DisableControls;
        Completo := EsquemaEstaCompleto;

        if not Completo and (QueryGetValueInt(DMConnections.BaseBO_Rating, Format(QRY_EXISTE_PDC, [FCodigoEsquema]), 30) = 1) then begin
            MsgBox(MSG_EXISTE_PDC, 'Error', MB_ICONSTOP);
            Exit;
        end;

        // si es alta, se guardan los campos del Esquema
        if Accion = 1 then begin
            try
                spADM_Esquemas_INSERT.Parameters.Refresh;
                spADM_Esquemas_INSERT.Parameters.ParamByName('@CodigoEsquema').Value := Null;
                spADM_Esquemas_INSERT.Parameters.ParamByName('@Completo').Value := Completo;
                spADM_Esquemas_INSERT.Parameters.ParamByName('@Descripcion').Value := edtDescripcion.Text;   
                spADM_Esquemas_INSERT.Parameters.ParamByName('@ID_CategoriasClases').Value := vcbCategoriaClase.Value;      // TASK_155_MGO_20170317
                spADM_Esquemas_INSERT.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                spADM_Esquemas_INSERT.ExecProc;

                if spADM_Esquemas_INSERT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                    raise Exception.Create(spADM_Esquemas_INSERT.Parameters.ParamByName('@ErrorDescription').Value);

                FCodigoEsquema := spADM_Esquemas_INSERT.Parameters.ParamByName('@CodigoEsquema').Value;
                edtCodigoEsquema.Text := IntToStr(FCodigoEsquema);

                cdsEsquemas.Append;
                cdsEsquemas.FieldByName('CodigoEsquema').Value := FCodigoEsquema;
                cdsEsquemas.FieldByName('Completo').Value := Completo;
                cdsEsquemas.FieldByName('CompletoDesc').Value := IIf(Completo, 'SI', 'NO');
                cdsEsquemas.FieldByName('Descripcion').Value := edtDescripcion.Text;             
                cdsEsquemas.FieldByName('ID_CategoriasClases').Value := vcbCategoriaClase.Value;        // TASK_155_MGO_20170317
                cdsEsquemas.FieldByName('UsuarioCreacion').Value := UsuarioSistema;
                cdsEsquemas.FieldByName('FechaHoraCreacion').Value := Now;
                cdsEsquemas.Post;
            except
                on e: Exception do begin
                    MsgBoxErr(MSG_ERROR_INSERT, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        end;

        // si es modificaci�n, se actualizan los campos del Esquema
        if Accion = 2 then begin
            try
                spADM_Esquemas_UPDATE.Parameters.Refresh;
                spADM_Esquemas_UPDATE.Parameters.ParamByName('@CodigoEsquema').Value := FCodigoEsquema;
                spADM_Esquemas_UPDATE.Parameters.ParamByName('@Completo').Value := Completo;
                spADM_Esquemas_UPDATE.Parameters.ParamByName('@Descripcion').Value := edtDescripcion.Text;
                spADM_Esquemas_UPDATE.Parameters.ParamByName('@ID_CategoriasClases').Value := vcbCategoriaClase.Value;      // TASK_155_MGO_20170317
                spADM_Esquemas_UPDATE.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                spADM_Esquemas_UPDATE.ExecProc;

                if spADM_Esquemas_UPDATE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                    raise Exception.Create(spADM_Esquemas_UPDATE.Parameters.ParamByName('@ErrorDescription').Value);

                cdsEsquemas.Edit;
                cdsEsquemas.FieldByName('Completo').Value := Completo;                 
                cdsEsquemas.FieldByName('CompletoDesc').Value := IIf(Completo, 'SI', 'NO');
                cdsEsquemas.FieldByName('Descripcion').Value := edtDescripcion.Text;        
                cdsEsquemas.FieldByName('ID_CategoriasClases').Value := vcbCategoriaClase.Value;        // TASK_155_MGO_20170317
                cdsEsquemas.FieldByName('UsuarioModificacion').Value := UsuarioSistema;
                cdsEsquemas.FieldByName('FechaHoraModificacion').Value := Now;
                cdsEsquemas.Post;
            except
                on e: Exception do begin
                    MsgBoxErr(MSG_ERROR_UPDATE, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        end;

        // si hubo Bandas Horarias eliminadas, se dan de baja en la base
        try
            for I := 0 to High(FBandasHorariasEliminadas) do begin
                spADM_BandasHorarias_DELETE.Parameters.Refresh;
                spADM_BandasHorarias_DELETE.Parameters.ParamByName('@CodigoBandaHoraria').Value := FBandasHorariasEliminadas[I].CodigoBandaHoraria;
                spADM_BandasHorarias_DELETE.ExecProc;

                if spADM_BandasHorarias_DELETE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                    raise Exception.Create(spADM_BandasHorarias_DELETE.Parameters.ParamByName('@ErrorDescription').Value);
            end;
        except
             on e: Exception do begin
                MsgBoxErr(MSG_ERROR_BANDA_DELETE, e.Message, 'Error', MB_ICONERROR);
                Exit;
            end;
        end;

        cdsBandasHorarias.First;
        // se recorre el DataSet de Bandas Horarias
        while not cdsBandasHorarias.Eof do begin
            // si CodigoBandaHorarias es 0, es un alta
            if cdsBandasHorarias.FieldByName('CodigoBandaHoraria').AsInteger = 0 then begin
                try
                    spADM_BandasHorarias_INSERT.Parameters.Refresh;
                    spADM_BandasHorarias_INSERT.Parameters.ParamByName('@CodigoEsquema').Value := FCodigoEsquema;
                    spADM_BandasHorarias_INSERT.Parameters.ParamByName('@CodigoDiaTipo').Value := cdsBandasHorarias.FieldByName('CodigoDiaTipo').AsString;
                    spADM_BandasHorarias_INSERT.Parameters.ParamByName('@HoraDesde').Value := cdsBandasHorarias.FieldByName('HoraDesde').AsDateTime;
                    spADM_BandasHorarias_INSERT.Parameters.ParamByName('@HoraHasta').Value := cdsBandasHorarias.FieldByName('HoraHasta').AsDateTime;
                    //spADM_BandasHorarias_INSERT.Parameters.ParamByName('@Temporada').Value := cdsBandasHorarias.FieldByName('Temporada').AsString;        // TASK_155_MGO_20170317
                    spADM_BandasHorarias_INSERT.Parameters.ParamByName('@CodigoTarifaTipo').Value := cdsBandasHorarias.FieldByName('CodigoTarifaTipo').AsString;
                    spADM_BandasHorarias_INSERT.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                    spADM_BandasHorarias_INSERT.ExecProc;

                    if spADM_BandasHorarias_INSERT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                        raise Exception.Create(spADM_BandasHorarias_INSERT.Parameters.ParamByName('@ErrorDescription').Value);
                except
                    on e: Exception do begin
                        MsgBoxErr(Format(MSG_ERROR_BANDA_INSERT, [cdsBandasHorarias.RecNo]), e.Message, 'Error', MB_ICONERROR);
                        Exit;
                    end;
                end;
            end else begin
            // si no, es una modificaci�n
                try
                    spADM_BandasHorarias_UPDATE.Parameters.Refresh;                                               
                    spADM_BandasHorarias_UPDATE.Parameters.ParamByName('@CodigoBandaHoraria').Value := cdsBandasHorarias.FieldByName('CodigoBandaHoraria').AsInteger;
                    spADM_BandasHorarias_UPDATE.Parameters.ParamByName('@CodigoDiaTipo').Value := cdsBandasHorarias.FieldByName('CodigoDiaTipo').AsString;
                    spADM_BandasHorarias_UPDATE.Parameters.ParamByName('@HoraDesde').Value := cdsBandasHorarias.FieldByName('HoraDesde').AsDateTime;
                    spADM_BandasHorarias_UPDATE.Parameters.ParamByName('@HoraHasta').Value := cdsBandasHorarias.FieldByName('HoraHasta').AsDateTime;
                    //spADM_BandasHorarias_UPDATE.Parameters.ParamByName('@Temporada').Value := cdsBandasHorarias.FieldByName('Temporada').AsString;        // TASK_155_MGO_20170317
                    spADM_BandasHorarias_UPDATE.Parameters.ParamByName('@CodigoTarifaTipo').Value := cdsBandasHorarias.FieldByName('CodigoTarifaTipo').AsString;
                    spADM_BandasHorarias_UPDATE.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                    spADM_BandasHorarias_UPDATE.ExecProc;

                    if spADM_BandasHorarias_UPDATE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                        raise Exception.Create(spADM_BandasHorarias_UPDATE.Parameters.ParamByName('@ErrorDescription').Value);
                except
                    on e: Exception do begin
                        MsgBoxErr(Format(MSG_ERROR_BANDA_UPDATE, [cdsBandasHorarias.RecNo]), e.Message, 'Error', MB_ICONERROR);
                        Exit;
                    end;
                end;
            end;
            cdsBandasHorarias.Next;
        end;
    finally
        FGuardandoDatos := False;
        cdsEsquemas.EnableControls;
        cdsBandasHorarias.EnableControls;
    end;

    Accion := 0;
    HabilitarControlesEsquema(False);
    CargarBandasHorarias;
end;

procedure TfrmABMBandasHorarias.btnCancelarHorarioClick(Sender: TObject);
begin
    HabilitarControlesEsquema(True);
end;

end.
