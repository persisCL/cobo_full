{-----------------------------------------------------------------------------
 Unit Name: UmbralOCRConfig
 Author:    gcasais
 Purpose: Configurar el umbral del OCR
 History:


  Revision 1:
  Date: 20/02/2009
  Author: mpiazza
  Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
    los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}


unit UmbralOCRConfig;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, DmiCtrls, DMConnection, UtilDB, UtilProc, Util;

type
  TfrmOCR = class(TForm)
    btn_Aceptar: TButton;
    Button2: TButton;
    Bevel1: TBevel;
    Label1: TLabel;
    neConf: TNumericEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button2Click(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
  private
    FValue: Integer;
  public
    Function Inicializar: Boolean;
  end;

var
  frmOCR: TfrmOCR;

implementation


{$R *.dfm}

{ TfrmOCR }

{-----------------------------------------------------------------------------
  Procedure: TfrmOCR.Inicializar
  Author:    gcasais
  Date:      06-Oct-2004
  Arguments: None
  Result:    Boolean

	Revision 1:
	Date: 20/02/2009
	Author: mpiazza
	Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
		los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}
function TfrmOCR.Inicializar: Boolean;
ResourceString
    MSG_INIT_ERROR = 'Error de inicializaci�n';
    MSG_ERROR = 'Error';
begin
    Result := False;
    try
        FValue := QueryGetValueInt(DMConnections.BaseCAC,
         'SELECT VALOR FROM PARAMETROSGENERALES  WITH (NOLOCK) WHERE CLASEPARAMETRO =' +
         '''' +  'V' + '''' + ' AND ' +
         'NOMBRE = ' + '''' + 'UmbralConfiabilidadOCR' + '''');
         neConf.ValueInt := FValue;
         ActiveControl := neConf;
         Result := True;
    except
        on e: Exception do Begin
            MsgBoxErr(MSG_INIT_ERROR, e.message, MSG_ERROR, 0);
        end;
    end;
end;

procedure TfrmOCR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmOCR.Button2Click(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmOCR.btn_AceptarClick
  Author:    gcasais
  Date:      06-Oct-2004
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TfrmOCR.btn_AceptarClick(Sender: TObject);
ResourceString
    MSG_THERESHOLD_ERROR = 'El valor del Umbral no est� en el rango permitido' + CRLF + 'posible entre 0 - 100 por ciento';
    MSG_ERROR = 'Error';
    MSG_CONFIRMATION1 = '� Desea establecer ';
    MSG_CONFIRMATION2 = ' por ciento como nuevo umbral' + CRLF + 'de confiabilidad para el OCR ?';
    MSG_CONFIRMATION3 = 'Confirmar Nuevo Umbral';
    MSG_UPDATE_ERROR =  'Error al actualizar';
begin
    if FValue = neConf.ValueInt then begin
        Close;
        Exit;
    end;
    if not (neConf.ValueInt in [0..100]) then begin
        MsgBox(MSG_THERESHOLD_ERROR, MSG_ERROR, MB_ICONERROR);
        neConf.ValueInt := FValue;
        Exit;
    end;
    if MsgBox(MSG_CONFIRMATION1 + IntToStr(neConf.ValueInt) +MSG_CONFIRMATION2,
        MSG_CONFIRMATION3, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        try
            QueryExecute(DMConnections.BaseCAC,
                ' UPDATE PARAMETROSGENERALES SET ' +
                ' VALOR = ' + IntToStr(neConf.ValueInt) + Space(1) +
                ' WHERE CLASEPARAMETRO = ' + '''' + 'V' + '''' +
                ' AND NOMBRE = ' + '''' + 'UmbralConfiabilidadOCR' + '''');
            Close;
        except
            on e: exception do begin
                MsgBoxErr(MSG_UPDATE_ERROR, e.Message, MSG_ERROR, 0);
            end;
        end;
    end else begin
        neConf.ValueInt := FValue;
    end;
end;

end.
