object FAbmContextMarks: TFAbmContextMarks
  Left = 155
  Top = 109
  Caption = 'Mantenimiento de ContextMarks'
  ClientHeight = 509
  ClientWidth = 738
  Color = clBtnFace
  Constraints.MinHeight = 480
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 738
    Height = 33
    Habilitados = [btModi, btSalir]
    OnClose = AbmToolbarClose
  end
  object lb_ContextMarks: TAbmList
    Left = 0
    Top = 33
    Width = 738
    Height = 344
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'74'#0'ContextMark  '
      #0'85'#0'Issuer Identifier  '
      #0'95'#0'TypeOfContract    '
      #0'85'#0'ContextVersion  '
      #0'154'#0'Proveedor Tag                         '
      #0'144'#0'Concesionaria                       ')
    HScrollBar = True
    RefreshTime = 100
    Table = SPObtenerContextMarks
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = lb_ContextMarksClick
    OnDrawItem = lb_ContextMarksDrawItem
    OnRefresh = lb_ContextMarksRefresh
    OnEdit = lb_ContextMarksEdit
    Access = [accModi]
    Estado = Normal
    ToolBar = AbmToolbar
    ExplicitTop = 27
  end
  object pnl_Datos: TPanel
    Left = 0
    Top = 377
    Width = 738
    Height = 93
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    ExplicitTop = 378
    object lbl_proveedortag: TLabel
      Left = 11
      Top = 35
      Width = 89
      Height = 13
      Caption = '&Proveedor Tag:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_Concesionaria: TLabel
      Left = 11
      Top = 60
      Width = 85
      Height = 13
      Caption = '&Concesionaria:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CbConcesionaria: TVariantComboBox
      Left = 106
      Top = 59
      Width = 414
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
    object CBProveedorTag: TVariantComboBox
      Left = 106
      Top = 30
      Width = 414
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 470
    Width = 738
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 541
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btn_Salir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btn_SalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object ds_ContextMark: TDataSource
    DataSet = SPObtenerContextMarks
    Left = 8
    Top = 60
  end
  object SPObtenerContextMarks: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerContextMarks;1'
    Parameters = <>
    Left = 40
    Top = 64
  end
  object SPActualizarContextMark: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarContextMark;1'
    Parameters = <>
    Left = 40
    Top = 104
  end
end
