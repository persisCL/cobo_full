object ProveedoresDayPassForm: TProveedoresDayPassForm
  Left = 0
  Top = 0
  Caption = 'ProveedoresDayPassForm'
  ClientHeight = 556
  ClientWidth = 785
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBotones: TPanel
    Left = 0
    Top = 0
    Width = 785
    Height = 41
    Align = alTop
    TabOrder = 0
    object btnTSalir: TSpeedButton
      Left = 16
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Salir'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00330000000000
        03333377777777777F333301111111110333337F333333337F33330111111111
        0333337F333333337F333301111111110333337F333333337F33330111111111
        0333337F333333337F333301111111110333337F333333337F33330111111111
        0333337F3333333F7F333301111111B10333337F333333737F33330111111111
        0333337F333333337F333301111111110333337F33FFFFF37F3333011EEEEE11
        0333337F377777F37F3333011EEEEE110333337F37FFF7F37F3333011EEEEE11
        0333337F377777337F333301111111110333337F333333337F33330111111111
        0333337FFFFFFFFF7F3333000000000003333377777777777333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnTSalirClick
    end
    object btnTInsertar: TSpeedButton
      Left = 47
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Agregar un Proveedor DayPass'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnTInsertarClick
    end
    object btnTEliminar: TSpeedButton
      Left = 79
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Eliminar un Proveedor DayPass'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        300033FFFFFF3333377739999993333333333777777F3333333F399999933333
        3300377777733333337733333333333333003333333333333377333333333333
        3333333333333333333F333333333333330033333F33333333773333C3333333
        330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
        333333377F33333333FF3333C333333330003333733333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnTEliminarClick
    end
    object btnTEditar: TSpeedButton
      Left = 110
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Modificar un Proveedor DayPass'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnTEditarClick
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 41
    Width = 785
    Height = 271
    Align = alClient
    DataSource = dsProveedores
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
  end
  object PAbajo: TPanel
    Left = 0
    Top = 512
    Width = 785
    Height = 44
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Notebook: TNotebook
      Left = 588
      Top = 0
      Width = 197
      Height = 44
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object GroupB: TPanel
    Left = 0
    Top = 312
    Width = 785
    Height = 200
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 3
    object lblCodigo: TLabel
      Left = 45
      Top = 10
      Width = 44
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCodigoPostal: TLabel
      Left = 519
      Top = 96
      Width = 25
      Height = 13
      Caption = 'C.P:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNumero: TLabel
      Left = 391
      Top = 96
      Width = 25
      Height = 13
      Caption = 'Nro:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCalle: TLabel
      Left = 56
      Top = 96
      Width = 33
      Height = 13
      Caption = 'Calle:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRegion: TLabel
      Left = 44
      Top = 162
      Width = 45
      Height = 13
      Caption = 'Regi'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDetalle: TLabel
      Left = 44
      Top = 129
      Width = 45
      Height = 13
      Caption = 'Detalle:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDocumento: TLabel
      Left = 58
      Top = 69
      Width = 31
      Height = 13
      Caption = 'RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblComuna: TLabel
      Left = 391
      Top = 159
      Width = 50
      Height = 13
      Caption = 'Comuna:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 17
      Top = 39
      Width = 72
      Height = 13
      Caption = 'Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 388
      Top = 69
      Width = 28
      Height = 13
      Caption = 'Giro:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 391
      Top = 129
      Width = 67
      Height = 13
      Caption = 'Personer'#237'a:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtCodigo: TNumericEdit
      Left = 95
      Top = 6
      Width = 105
      Height = 21
      Hint = 'Codigo Proveedor DayPass'
      Color = 16444382
      Enabled = False
      MaxLength = 0
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object edtCodigoPostal: TEdit
      Left = 549
      Top = 92
      Width = 70
      Height = 21
      Hint = 'C'#243'digo Postal'
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
    end
    object edtNumero: TEdit
      Left = 421
      Top = 92
      Width = 70
      Height = 21
      Hint = 'N'#250'mero del Domicilio'
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
    end
    object cbRegion: TComboBox
      Left = 95
      Top = 158
      Width = 278
      Height = 21
      Hint = 'Regi'#243'n'
      Style = csDropDownList
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
      OnChange = cbRegionChange
    end
    object edtDetalle: TEdit
      Left = 95
      Top = 125
      Width = 278
      Height = 21
      Hint = 'Detalles de la direcci'#243'n'
      CharCase = ecUpperCase
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 100
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
    end
    object edtDocumento: TEdit
      Left = 95
      Top = 65
      Width = 117
      Height = 21
      Hint = 'N'#250'mero de documento'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 9
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object cbComuna: TComboBox
      Left = 447
      Top = 155
      Width = 278
      Height = 21
      Hint = 'Comuna'
      Style = csDropDownList
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
    end
    object edtCalle: TEdit
      Left = 95
      Top = 92
      Width = 278
      Height = 21
      Hint = 'Calle del Domicilio'
      CharCase = ecUpperCase
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 100
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
    end
    object edtDescripcion: TEdit
      Left = 95
      Top = 35
      Width = 278
      Height = 21
      Hint = 'Descripci'#243'n o Nombre del Proveedor DayPass'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 50
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object edtGiro: TEdit
      Left = 422
      Top = 65
      Width = 198
      Height = 21
      Hint = 'Giro del Proveedor DayPass'
      CharCase = ecUpperCase
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 100
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
    object rbtnJuridica: TRadioButton
      Left = 488
      Top = 127
      Width = 84
      Height = 17
      Caption = 'Jur'#237'dica'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
    end
    object rbtnNatural: TRadioButton
      Left = 578
      Top = 127
      Width = 87
      Height = 17
      Caption = 'Natural'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 9
    end
  end
  object tblProveedoresDayPass: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    IndexFieldNames = 'CodigoProveedorDayPass'
    TableName = 'ProveedoresDayPass'
    Left = 250
    Top = 6
  end
  object dsProveedores: TDataSource
    DataSet = tblProveedoresDayPass
    OnDataChange = dsProveedoresDataChange
    Left = 250
    Top = 40
  end
end
