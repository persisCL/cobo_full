{-----------------------------------------------------------------------------
 Unit Name: ABMDeclaracionesConvenios
 Author:    ggomez
 Purpose:   Form para hacer Modificaciones sobre los registros de la tabla
            DeclaracionesConvenios.

 History:
-----------------------------------------------------------------------------}

unit ABMDeclaracionesConvenios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, DbList, Abm_obj, StdCtrls, DmiCtrls, DPSControls,
  ExtCtrls,
  UtilProc, StrUtils, UtilDB, PeaProcs, RStrings;

type
  TFABMDeclaracionesConvenios = class(TForm)
    AbmToolbar: TAbmToolbar;
    Panel2: TPanel;
    Notebook: TNotebook;
    pnl_Datos: TPanel;
    lbl_Texto: TLabel;
    txt_Texto: TEdit;
    lb_DeclaracionesConvenios: TAbmList;
    dsDeclaracionesConvenios: TDataSource;
    tblDeclaracionesConvenios: TADOTable;
    tblDeclaracionesConveniosTipoMensaje: TStringField;
    tblDeclaracionesConveniosTexto: TStringField;
    cbTipoMensaje: TComboBox;
    Label1: TLabel;
    btn_Salir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure AbmToolbarClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure lb_DeclaracionesConveniosClick(Sender: TObject);
    procedure lb_DeclaracionesConveniosDrawItem(Sender: TDBList;
      Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
      Cols: TColPositions);
    procedure lb_DeclaracionesConveniosEdit(Sender: TObject);
    procedure lb_DeclaracionesConveniosRefresh(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
    procedure lb_DeclaracionesConveniosInsert(Sender: TObject);
    procedure lb_DeclaracionesConveniosDelete(Sender: TObject);
  private
    { Private declarations }
    procedure LimpiarCampos;
    procedure VolverCampos;
  public
    { Public declarations }
	function Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
  end;

var
  FABMDeclaracionesConvenios: TFABMDeclaracionesConvenios;

resourcestring
	STR_MAESTRO_DECLARACIONESCONVENIOS = 'Maestro de Declaraciones de Convenios';

implementation

uses DMConnection;

const sTiposMensaje = 'AMPE';

{$R *.dfm}

function TFABMDeclaracionesConvenios.Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
Var
	S: TSize;
begin
	Result := False;

	if MDIChild then begin
        FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	Notebook.PageIndex := 0;

   	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

	if not OpenTables([tblDeclaracionesConvenios]) then Exit;

    // Limpiar controles de ingreso de datos.
    LimpiarCampos;

    KeyPreview := True;
	lb_DeclaracionesConvenios.Reload;
   	Notebook.PageIndex := 0;
	Result := True;
end;

procedure TFABMDeclaracionesConvenios.AbmToolbarClose(Sender: TObject);
begin
    Close;
end;

procedure TFABMDeclaracionesConvenios.LimpiarCampos;
begin
    txt_Texto.Clear;
    cbTipoMensaje.ItemIndex := -1;
end;

procedure TFABMDeclaracionesConvenios.VolverCampos;
begin
	lb_DeclaracionesConvenios.Estado  := Normal;
	lb_DeclaracionesConvenios.Enabled := True;
	ActiveControl                     := lb_DeclaracionesConvenios;
	Notebook.PageIndex                := 0;
	pnl_Datos.Enabled                 := False;
	lb_DeclaracionesConvenios.Reload;
end;

procedure TFABMDeclaracionesConvenios.FormShow(Sender: TObject);
begin
    lb_DeclaracionesConvenios.Reload;
end;

procedure TFABMDeclaracionesConvenios.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Action := caFree;
end;

procedure TFABMDeclaracionesConvenios.BtnCancelarClick(Sender: TObject);
begin
    VolverCampos;
end;

procedure TFABMDeclaracionesConvenios.BtnAceptarClick(Sender: TObject);
begin
	with lb_DeclaracionesConvenios do begin
		Screen.Cursor := crHourGlass;
		try
			try
                if (Estado = Alta) then
                    tblDeclaracionesConvenios.Append
                else
                    tblDeclaracionesConvenios.Edit;
                tblDeclaracionesConvenios.FieldByName('TipoMensaje').Value := sTiposMensaje[cbTipoMensaje.ItemIndex + 1];
                tblDeclaracionesConvenios.FieldByName('Texto').Value       := Trim(txt_Texto.Text);
                tblDeclaracionesConvenios.Post;
			except
				on E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,
                        [STR_MAESTRO_DECLARACIONESCONVENIOS]), e.message,
                        Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_DECLARACIONESCONVENIOS]),
                        MB_ICONSTOP);
				end;
			end;
		finally
            VolverCampos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

procedure TFABMDeclaracionesConvenios.lb_DeclaracionesConveniosClick(
  Sender: TObject);
begin
	with (Sender as TDbList).Table do begin
        cbTipoMensaje.ItemIndex := Pos(FieldByname('TipoMensaje').AsString, sTiposMensaje) - 1;
	    txt_Texto.Text          := FieldByname('Texto').AsString;
	end;
end;

procedure TFABMDeclaracionesConvenios.lb_DeclaracionesConveniosDrawItem(
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
resourcestring
    MSG_TIPO_ALTA          = 'Alta';
    MSG_TIPO_MODIFICACION  = 'Modificación';
    MSG_TIPO_PERDIDA       = 'Pérdida';
    MSG_TIPO_ESTADO_ACTUAL = 'Estado Actual';
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);

        case Tabla.FieldByName('TipoMensaje').AsString[1] of
            'A' : TextOut(Cols[0], Rect.Top, MSG_TIPO_ALTA);
            'M' : TextOut(Cols[0], Rect.Top, MSG_TIPO_MODIFICACION);
            'P' : TextOut(Cols[0], Rect.Top, MSG_TIPO_PERDIDA);
            'E' : TextOut(Cols[0], Rect.Top, MSG_TIPO_ESTADO_ACTUAL);
        end;
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Texto').AsString));
	end;
end;

procedure TFABMDeclaracionesConvenios.lb_DeclaracionesConveniosEdit(
  Sender: TObject);
begin
	lb_DeclaracionesConvenios.Enabled := False;
    lb_DeclaracionesConvenios.Estado  := Modi;
	Notebook.PageIndex                := 1;
    pnl_Datos.Enabled                 := True;
    txt_Texto.Enabled                 := True;
    ActiveControl                     := cbTipoMensaje;
end;

procedure TFABMDeclaracionesConvenios.lb_DeclaracionesConveniosRefresh(
  Sender: TObject);
begin
    if lb_DeclaracionesConvenios.Empty then LimpiarCampos;
end;

procedure TFABMDeclaracionesConvenios.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFABMDeclaracionesConvenios.lb_DeclaracionesConveniosInsert(
  Sender: TObject);
begin
    LimpiarCampos;
	lb_DeclaracionesConvenios.Enabled := False;
    lb_DeclaracionesConvenios.Estado  := Alta;
	Notebook.PageIndex                := 1;
    pnl_Datos.Enabled                 := True;
    txt_Texto.Enabled                 := True;
    ActiveControl                     := cbTipoMensaje;
end;

procedure TFABMDeclaracionesConvenios.lb_DeclaracionesConveniosDelete(
  Sender: TObject);
resourcestring
    MSG_DELETE_DECLARACION     = '¿Confirma eliminar la declaración de convenio seleccionada?';
    CAPTION_DELETE_DECLARACION = 'Eliminar Declaración de Convenio';
    MSG_DELETE_ERROR           = 'Se produjo un error al eliminar la declaración de convenio seleccionada';
begin
	Screen.Cursor := crHourGlass;
    try
        If MsgBox(MSG_DELETE_DECLARACION, CAPTION_DELETE_DECLARACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            try
                (Sender AS TDbList).Table.Delete;
            Except
                On E: Exception do begin
                    (Sender AS TDbList).Table.Cancel;
                    MsgBoxErr(MSG_DELETE_ERROR, E.Message, CAPTION_DELETE_DECLARACION, MB_ICONSTOP);
                    Raise;
                end;
            end;
            lb_DeclaracionesConvenios.Reload;
        end;
        lb_DeclaracionesConvenios.Estado  := Normal;
        lb_DeclaracionesConvenios.Enabled := True;
        Notebook.PageIndex := 0;
    finally
	    Screen.Cursor := crDefault;
    end;
end;

end.
