{********************************** File Header ********************************
File Name : FrmDetalleMensajesTipoCliente.pas
Author : mpiazza
Date Created: 20/01/2009
Language : ES-AR
Description : Este formulario recibe un arreglo de detalles de mensajes
para realizar tareas de ABM segun un codigotipocliente, despues regresa el mismo
arreglo con modificaciones para ser aplicado en la base de datos


Revision 1
Author : jjofre
Date Created: 13/08/2010
Description: (SS 690 Semaforos)
        -Se agregan/Modifican los siguientes procedures/funciones/Objetos
            TAuxGrid = class(TStringGrid);
            btnBorrarClick
            btnEditarClick
            EditarDatos
            lbMensajeTipo
            lbMensajeTipoClienteDblClick

Etiqueta    : 20160614 MGO
Descripci�n : Se muestra fecha hora actual al ingresar nuevo registro
              (Se usa fecha de la m�quina y no la base de datos ya que es un
              registro temporal, y dicha fecha no se termina guardando)
*******************************************************************************}

unit FrmDetalleMensajesTipoCliente;


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PeaTypes, DB, ADODB, DmiCtrls, Buttons, DbList, Abm_obj, StdCtrls,
  VariantComboBox, ExtCtrls, Grids , Util, UtilProc, Mask;

type

  TRegistroDetalleMensajeTipoCliente = class(TObject)
  private

  public
      CodigoTipoClienteMensaje  : Integer   ;
      FechaHora                 : TDateTime ;
      CodigoTipoCliente         : Integer   ;
      Semaforo                  : Integer   ;
      CodigoSistema             : Integer   ;
      Descripcion               : string    ;
      DescripcionSistema        : string    ;
      //los siguientes datos son de contol,
      //para posterior proceso de los datos
      ClaveTimeStamp            : integer   ;
      modificado                : boolean   ;
      Nuevo                     : boolean   ;
      borrado                   : boolean   ;
      constructor Create();
  end;

  TAuxGrid = class(TStringGrid);

  TDetalleMensajesTipoClienteForm = class(TForm)
    pnlBotones: TPanel;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    pnlDetalleMensajeTipoCliente: TPanel;
    lblCodigoTipoMedioPago: TLabel;
    lblDescripcion: TLabel;
    lblCodigoConceptoPago: TLabel;
    txtCodigoTipoClienteMensaje: TNumericEdit;
    txtDescripcion: TEdit;
    cbCodigoSistema: TVariantComboBox;
    txtCodigoTipoCliente: TNumericEdit;
    Label1: TLabel;
    Label2: TLabel;
    lbMensajeTipoCliente: TStringGrid;
    cdColores: TColorDialog;
    qrySistemas: TADOQuery;
    btnNuevo: TButton;
    btnBorrar: TButton;
    btnEditar: TButton;
    txtSemaforo: TMaskEdit;
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnCancelarClick(Sender: TObject);
    procedure lbMensajeTipoClienteDblClick(Sender: TObject);
    procedure txtDescripcionChange(Sender: TObject);
    procedure cbCodigoSistemaSelect(Sender: TObject);
    //procedure txtColorChange(Sender: TObject);
    procedure tbTipoMedioPagoClose(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnNuevoClick(Sender: TObject);
    procedure btnBorrarClick(Sender: TObject);
  private
    { Private declarations }
    fDatos              : tlist;
    fCodigoTipoCliente  : integer;
    fSemaforo           : integer;
    RegistroActivo      : TRegistroDetalleMensajeTipoCliente;
    fModificado         : boolean ;
    function InsertarRegistro():boolean;
    //function ModificarRegistro():boolean;
    Function LlenarGrilla(Datos : tlist): boolean;
    function ValidadSistema(CodigoSistema:Integer; CodigoTipoClienteMensaje: integer; ClaveTimeStamp :integer): boolean;
  public
    function Inicializar(MDIChild: Boolean; CodigoTipoCliente : integer; Semaforo: integer; Datos : tlist): Boolean;
    procedure EditarDatos(TipoAbm: TAbmState);
    procedure LimpiarCampos;
    procedure CargaRegistroActivo(Registro : TRegistroDetalleMensajeTipoCliente);
    procedure FinEditarDatos(Actualizar: boolean);
    //function Inicializar(CodigoTipoCliente : integer; Datos: TDetalleMensajeTipoCliente): Boolean;

    { Public declarations }
  end;

var
  DetalleMensajesTipoClienteForm: TDetalleMensajesTipoClienteForm;

implementation

{$R *.dfm}

{ TDetalleMensajesTipoClienteForm }

{-----------------------------------------------------------------------------
  Procedure: BtnAceptarClick
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Guarda el registro actual en una lista y valida los datos
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_WRONGKEY = 'Solo se puede crear un Registro por C�digo de Sistema'; // SS_690_20100901
    MSG_CAPTION = 'Error de Validaci�n';
begin

  if not (RegistroActivo = nil) then
  begin
    if ValidadSistema(cbCodigoSistema.Value, RegistroActivo.CodigoTipoClienteMensaje, RegistroActivo.ClaveTimeStamp ) then begin
        RegistroActivo.CodigoTipoClienteMensaje :=  txtCodigoTipoClienteMensaje.ValueInt;
        RegistroActivo.CodigoTipoCliente := txtCodigoTipoCliente.ValueInt;
        RegistroActivo.Descripcion := txtDescripcion.Text;
        RegistroActivo.Semaforo := strtoint(txtSemaforo.Text);
        RegistroActivo.CodigoSistema := cbCodigoSistema.Value;
        RegistroActivo.DescripcionSistema := cbCodigoSistema.text;
        RegistroActivo.FechaHora := Now;    // 20160614 MGO
        RegistroActivo.modificado := true;
        if RegistroActivo.Nuevo then begin
            RegistroActivo.Nuevo := false ;
            fdatos.Add(RegistroActivo)
        end;
        FinEditarDatos(True);
        fModificado := true;
    end else begin
        MsgBox(MSG_WRONGKEY, MSG_CAPTION, MB_ICONSTOP);
    end;
  end;
end;
{-----------------------------------------------------------------------------
  Procedure: BtnCancelarClick
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Cancela la operacion descartando los cambios
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.BtnCancelarClick(Sender: TObject);
begin
    FinEditarDatos(false);
end;
{-----------------------------------------------------------------------------
  Procedure: btnNuevoClick
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Deja en edicion un nuevo registro
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.btnNuevoClick(Sender: TObject);
begin
  EditarDatos(Alta);
end;
{-----------------------------------------------------------------------------
  Procedure: BtnSalirClick
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Deja en edicion un nuevo registro
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.BtnSalirClick(Sender: TObject);
begin
  Close;
end;

{-----------------------------------------------------------------------------
  Procedure: btnBorrarClick
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Deja en edicion un nuevo registro
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.btnBorrarClick(Sender: TObject);
var
    Indice: integer;
begin
    Indice := lbMensajeTipoCliente.Row;

    if (fDatos.Count > 0) and (Indice > 0) then begin

        TRegistroDetalleMensajeTipoCliente( lbMensajeTipoCliente.Objects[0, Indice]).borrado:= true;
        
        if lbMensajeTipoCliente.RowCount > 1 then begin
            TAuxGrid(lbMensajeTipoCliente).DeleteRow(Indice);
        end;

        if lbMensajeTipoCliente.RowCount = 1 then begin
            lbMensajeTipoCliente.ColCount := 6;
            lbMensajeTipoCliente.RowCount := 2;
            lbMensajeTipoCliente.Cells[0 , 0] := 'C�digo Mensaje';
            lbMensajeTipoCliente.Cells[1 , 0] := 'Fecha/Hora';
            lbMensajeTipoCliente.Cells[2 , 0] := 'Tipo Cliente';
            lbMensajeTipoCliente.Cells[3 , 0] := 'Sem�foro';
            lbMensajeTipoCliente.Cells[4 , 0] := 'Sistema';
            lbMensajeTipoCliente.Cells[5 , 0] := 'Descripci�n';
            lbMensajeTipoCliente.FixedRows := 1;
            lbMensajeTipoCliente.Rows[1].Clear;
            btnBorrar.Enabled:= False;
            btnEditar.Enabled:= False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: btnEditarClick
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Prepara para editar el registro marcado en la grilla
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.btnEditarClick(Sender: TObject);
begin
    if fdatos.count > 0 then EditarDatos(Modi)
end;

{-----------------------------------------------------------------------------
  Procedure: CargaRegistroActivo
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Registro: TRegistroDetalleMensajeTipoCliente
  Result: N/A
  Purpose: Toma el registro activo y lo carga en el frame de edicion
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.CargaRegistroActivo(
  Registro: TRegistroDetalleMensajeTipoCliente);
begin
    RegistroActivo := Registro;
    txtCodigoTipoClienteMensaje.ValueInt := RegistroActivo.CodigoTipoClienteMensaje ;
    txtSemaforo.Text := inttostr(RegistroActivo.Semaforo ) ;
    txtDescripcion.Text :=  RegistroActivo.Descripcion;
    txtCodigoTipoCliente.ValueInt := RegistroActivo.CodigoTipoCliente;
    cbCodigoSistema.Value := RegistroActivo.CodigoSistema;
end;

{-----------------------------------------------------------------------------
  Procedure: cbCodigoSistemaSelect
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Evento del comboque habilita el el boton aceptar
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.cbCodigoSistemaSelect(
  Sender: TObject);
begin
  if not BtnAceptar.Enabled  then BtnAceptar.Enabled := true;

end;

{-----------------------------------------------------------------------------
  Procedure: EditarDatos
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: TipoAbm: TAbmState
  Result: N/A
  Purpose: prepara el frame de edicion con los datos respectivos
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.EditarDatos(TipoAbm: TAbmState);
var
Indice: integer;
begin
    lbMensajeTipoCliente.Enabled     := False;
    BtnCancelar.Enabled := true;
    BtnCancelar.Visible  := true;
    case TipoAbm of
      Alta:
        begin
            btnBorrar.Enabled:= True;
            btnEditar.Enabled:= True; 
            LimpiarCampos;
            RegistroActivo := TRegistroDetalleMensajeTipoCliente.create();
            RegistroActivo.Nuevo := true;
        end;
      Modi:
        begin
          Indice := lbMensajeTipoCliente.Row;
          CargaRegistroActivo( TRegistroDetalleMensajeTipoCliente( lbMensajeTipoCliente.Objects[0, Indice]));
        end;
    end;

    BtnAceptar.Enabled := false;
    Notebook.PageIndex  := 1;
    pnlDetalleMensajeTipoCliente.Visible      := True;
    txtDescripcion.SetFocus;
end;

{-----------------------------------------------------------------------------
  Procedure: FinEditarDatos
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Actualizar: boolean
  Result: N/A
  Purpose: Cuando termina la edicion, prepara los controles y el frame para
  que se pueda seguir con la edicion o salida de otros registros
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.FinEditarDatos(Actualizar: boolean);
var RegistroActual: Pointer;

begin
    pnlDetalleMensajeTipoCliente.Visible     := False;
    Notebook.PageIndex := 0;
    Screen.Cursor 	   := crDefault;
    if Actualizar then begin
        LlenarGrilla(fdatos);
    end;
    lbMensajeTipoCliente.Enabled    := True;
    lbMensajeTipoCliente.SetFocus;
end;

{-----------------------------------------------------------------------------
  Procedure: FormClose
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject; var Action: TCloseAction
  Result: N/A
  Purpose: Responde al evento cerrar el formulacio
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//     action := caFree;
end;

{-----------------------------------------------------------------------------
  Procedure: Inicializar
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: MDIChild: Boolean;CodigoTipoCliente : integer; Datos : tlist
  Result: Boolean
  Purpose: Inicializa todos los controles del formulario y carga la grilla
  con datos
-----------------------------------------------------------------------------}
function TDetalleMensajesTipoClienteForm.Inicializar(MDIChild: Boolean; CodigoTipoCliente : integer; Semaforo: integer; Datos : tlist): Boolean;
Var
  CantidadSistemas  : integer;
  i                 : Integer;
  S                 : TSize;
begin
  fModificado := false;
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		//SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
  fDatos := Datos;
  fCodigoTipoCliente := CodigoTipoCliente;
  fsemaforo := Semaforo;
  LlenarGrilla( fDatos);
  qrySistemas.Active := True;
  CantidadSistemas := qrySistemas.RecordCount;
  qrySistemas.First;
  cbCodigoSistema.Items.Clear;
//  cbCodigoSistema.Items.Add('SELECCIONAR',0);
  for i := 0 to CantidadSistemas - 1 do begin
    cbCodigoSistema.Items.Add(qrySistemas.FieldByName('Descripcion').AsString, qrySistemas.FieldByName('CodigoSistema').AsInteger);
    qrySistemas.Next;
  end;
  qrySistemas.Active := False;
	Result := True;
 	Notebook.PageIndex := 0;

end;

{-----------------------------------------------------------------------------
  Procedure: InsertarRegistro
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: N/A
  Result: Boolean
  Purpose: Inserta un registro en la la lista y a la grilla
-----------------------------------------------------------------------------}
function TDetalleMensajesTipoClienteForm.InsertarRegistro(): boolean;
var
indice : integer;
begin
    lbMensajeTipoCliente.RowCount := lbMensajeTipoCliente.RowCount + 1 ;
    fDatos.add(registroActivo);
    lbMensajeTipoCliente.Objects[0,indice + 1] :=  registroActivo;
    indice := lbMensajeTipoCliente.RowCount;
    with TRegistroDetalleMensajeTipoCliente(registroActivo ) do
    begin
        lbMensajeTipoCliente.Cells[0 ,indice] := inttostr(CodigoTipoClienteMensaje);
        lbMensajeTipoCliente.Cells[1 ,indice] := datetimetostr(FechaHora);
        lbMensajeTipoCliente.Cells[2 ,indice] := inttostr(CodigoTipoCliente);
        lbMensajeTipoCliente.Cells[3 ,indice] := inttostr(Semaforo);
        lbMensajeTipoCliente.Cells[4 ,indice] := inttostr(CodigoSistema);
        lbMensajeTipoCliente.Cells[5 ,indice] := Descripcion;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: lbMensajeTipoClienteDblClick
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Evento DblClick de la grilla para editar un registro
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.lbMensajeTipoClienteDblClick(
  Sender: TObject);
begin
    if (fDatos.Count > 0) and (lbMensajeTipoCliente.Row > 1) then EditarDatos(Modi)
end;

{-----------------------------------------------------------------------------
  Procedure: LimpiarCampos
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Inicializa los controles para dar de alta un nuevo registro
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.LimpiarCampos;
begin
    txtCodigoTipoClienteMensaje.ValueInt := -1;
    txtCodigoTipoCliente.ValueInt := fCodigoTipoCliente;
    txtDescripcion.Text := '' ;
    cbCodigoSistema.ItemIndex :=0;
    txtSemaforo.Text := inttostr(fSemaforo );
end;

{-----------------------------------------------------------------------------
  Procedure: LlenarGrilla
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Datos : tlist
  Result: boolean
  Purpose: Llena la grilla con los datos correspondientes
-----------------------------------------------------------------------------}
function TDetalleMensajesTipoClienteForm.LlenarGrilla(Datos : tlist): boolean;
var
CantidadRegistros : integer;
i                 : integer;
Indice            : integer;
Registros          : TRegistroDetalleMensajeTipoCliente;
begin
  lbMensajeTipoCliente.Visible := false ;
  CantidadRegistros := Datos.Count ;
  lbMensajeTipoCliente.ColCount := 6;
  lbMensajeTipoCliente.RowCount :=2;


  //genero encabezados de la grilla
  lbMensajeTipoCliente.Cells[0 , 0] := 'C�digo Mensaje';
  lbMensajeTipoCliente.Cells[1 , 0] := 'Fecha/Hora';
  lbMensajeTipoCliente.Cells[2 , 0] := 'Tipo Cliente';
  lbMensajeTipoCliente.Cells[3 , 0] := 'Sem�foro';
  lbMensajeTipoCliente.Cells[4 , 0] := 'Sistema';
  lbMensajeTipoCliente.Cells[5 , 0] := 'Descripci�n';
  lbMensajeTipoCliente.FixedRows := 1;
  i := 0;
  for Indice := 0 to CantidadRegistros  - 1 do begin
    with TRegistroDetalleMensajeTipoCliente(Datos.Items[indice] ) do begin
      if not borrado then begin
        if i > 0  then begin
            lbMensajeTipoCliente.RowCount := lbMensajeTipoCliente.RowCount +1;
        end;
        lbMensajeTipoCliente.Objects[0,i + 1] :=  Datos.Items[i];
        lbMensajeTipoCliente.Cells[0 ,i + 1] := inttostr(CodigoTipoClienteMensaje);
        lbMensajeTipoCliente.Cells[1 ,i + 1] := datetimetostr(FechaHora);
        lbMensajeTipoCliente.Cells[2 ,i + 1] := inttostr(CodigoTipoCliente);
        lbMensajeTipoCliente.Cells[3 ,i + 1] := inttostr(Semaforo);
        lbMensajeTipoCliente.Cells[4 ,i + 1] := DescripcionSistema;
        lbMensajeTipoCliente.Cells[5 ,i + 1] := Descripcion;
        inc(i);
      end else begin
        //beep;
      end;
    end;
  end;
  lbMensajeTipoCliente.Visible := true;
end;


{-----------------------------------------------------------------------------
  Procedure: tbTipoMedioPagoClose
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Cierra el formulario
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.tbTipoMedioPagoClose(Sender: TObject);
begin
  close;
end;

{-----------------------------------------------------------------------------
  Procedure: txtDescripcionChange
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Evento change del txtDescripcion habilita el el boton aceptar
-----------------------------------------------------------------------------}
procedure TDetalleMensajesTipoClienteForm.txtDescripcionChange(Sender: TObject);
begin
  if not BtnAceptar.Enabled  then BtnAceptar.Enabled := true;
end;

function TDetalleMensajesTipoClienteForm.ValidadSistema(
  CodigoSistema: Integer; CodigoTipoClienteMensaje: integer; ClaveTimeStamp :integer): boolean;
var
i                   : integer;
RegistroEncontrado  : boolean ;
begin
    RegistroEncontrado := true ;
    For i := 0 to fdatos.Count -1 Do Begin
      if not (TRegistroDetalleMensajeTipoCliente(fDatos.Items[i] ).borrado) then begin
          if (CodigoTipoClienteMensaje = -1) then begin
              If (TRegistroDetalleMensajeTipoCliente(fDatos.Items[i] ).CodigoSistema = CodigoSistema) and (TRegistroDetalleMensajeTipoCliente(fDatos.Items[i] ).ClaveTimeStamp <> ClaveTimeStamp) then begin
                RegistroEncontrado := false;
                Break;
              end;
          end else If (TRegistroDetalleMensajeTipoCliente(fDatos.Items[i] ).CodigoSistema = CodigoSistema) and (TRegistroDetalleMensajeTipoCliente(fDatos.Items[i] ).CodigoTipoClienteMensaje <> CodigoTipoClienteMensaje) then begin
              RegistroEncontrado := false;
              Break;
          end;
      end;
    End;
    result := RegistroEncontrado ;
end;

{ TRegistroDetalleMensajeTipoCliente }
{-----------------------------------------------------------------------------
  Procedure: Create;
  Author:    mpiazza
  Date:      21/01/2009
  Arguments: N/A
  Result: N/A
  Purpose: Create del objeto DetalleMensajetipoCliente donde inicializo
  variables y timestamp para posterior validacion de si existen registros con
  TipoCliente/ sistemas repetido. El TimeStamp se utiliza para los registros
  nuevos que aun no disponen de clave primaria ya que seran dados de alta
  en una sola transaccion en el formulario de frmabmtipocliente
-----------------------------------------------------------------------------}
constructor TRegistroDetalleMensajeTipoCliente.Create;
var  Hour, Min, Sec, MSec : Word;
begin
      //Cuidado, es una funcion timestamp para uso interno
      DecodeTime(now, Hour, Min, Sec, MSec);
      ClaveTimeStamp            := (10000 * Hour ) * ( 1000 * Min )+ (100* sec) + MSec;
      //**************************************************
      CodigoTipoClienteMensaje  := -1;
      //FechaHora                 := NowBase(DMConnections.BaseCAC);
      CodigoTipoCliente         := -1;
      Semaforo                  := -1;
      CodigoSistema             :=-1;
      Descripcion               := '';
      DescripcionSistema        := '';
      modificado                := false;
      Nuevo                     := false;
      borrado                   := false;
end;

end.
