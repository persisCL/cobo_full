unit FrmWorkload;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DmiCtrls, ComCtrls, StdCtrls, DB, ADODB, UtilDB, Util,
  UtilProc;

type
  TFormWorkload = class(TForm)
	hg_CallCenter: THistogram;
    pnl_CallCenter: TPanel;
	pnl_BackOffice: TPanel;
	hg_Validacion: THistogram;
	pnl_Validacion: TPanel;
	hg_BackOffice: THistogram;
	Label1: TLabel;
	Label2: TLabel;
	Label3: TLabel;
    ObtenerWorkload: TADOStoredProc;
    Timer: TTimer;
    qnt_callcenter: TPanel;
    qnt_backoffice: TPanel;
    qnt_validacion: TPanel;
    lbl_CC_Valor: TLabel;
    lbl_BO_Valor: TLabel;
    lbl_VA_Valor: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    lbl_CC_Max: TLabel;
    lbl_BO_Max: TLabel;
    lbl_VA_Max: TLabel;
    lbl_CC_Dev: TLabel;
    lbl_BO_Dev: TLabel;
    lbl_VA_Dev: TLabel;
    bv_separador: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    lbl_CC_Status: TLabel;
    lbl_BO_Status: TLabel;
    lbl_VA_Status: TLabel;
	procedure FormResize(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure FormCreate(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private
	{ Private declarations }
	function Recargar: Boolean;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FormWorkload: TFormWorkload;

implementation

uses DMConnection;

{$R *.dfm}

procedure TFormWorkload.FormResize(Sender: TObject);
Var
	DeltaY: Integer;
begin
	DeltaY := Height - 478;
	hg_CallCenter.Height := 100 + (DeltaY div 3);
	hg_BackOffice.Height := 100 + (DeltaY div 3);
	hg_Validacion.Height := 100 + (DeltaY div 3);
	pnl_BackOffice.Top := hg_CallCenter.Top + hg_CallCenter.Height;
	hg_BackOffice.Top := pnl_BackOffice.Top + pnl_BackOffice.Height;
	pnl_Validacion.Top := hg_BackOffice.Top + hg_BackOffice.Height;
	hg_Validacion.Top := pnl_Validacion.Top + pnl_Validacion.Height;
	qnt_callcenter.top := hg_CallCenter.Top;
	qnt_backoffice.top := hg_BackOffice.Top;
	qnt_validacion.Top := hg_Validacion.Top;
end;

procedure TFormWorkload.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormWorkload.FormCreate(Sender: TObject);
Var
	i: Integer;
begin
	Width := 582;
	Height := 478;
	For i := 0 to ControlCount - 1 do begin
		if Controls[i] is THistogram then Controls[i].Anchors := [akLeft, akRight, akTop];
		if (Controls[i] = pnl_Validacion) or (Controls[i] = pnl_BackOffice)
		  or (Controls[i] = pnl_CallCenter) then Controls[i].Anchors := [akLeft, akRight, akTop];
		if Controls[i] is TLabel then Controls[i].Anchors := [akLeft, akBottom];
		if Controls[i] is TBevel then Controls[i].Anchors := [akLeft, akBottom];
	end;
	bv_separador.Anchors := [akLeft, akRight, akBottom];
end;

function TFormWorkload.Inicializa: Boolean;
begin
	Result := Recargar;
(*	hg_CallCenter.Count := 2;
	hg_BackOffice.Count := 2;
	hg_Validacion.Count := 2;
	hg_CallCenter.VarColor[1] := clRed;
	hg_BackOffice.VarColor[1] := clRed;
	hg_Validacion.VarColor[1] := clRed;
	hg_CallCenter.VarColor[2] := clWhite;
	hg_BackOffice.VarColor[2] := clWhite;
	hg_Validacion.VarColor[2] := clWhite;*)
	if Result then Timer.Enabled := True;
end;

procedure TFormWorkload.TimerTimer(Sender: TObject);
begin
	Timer.Enabled := False;
	if Recargar then Timer.Interval := 1000 else Timer.Interval := 60000;
	Timer.Enabled := True;
end;

function TFormWorkload.Recargar: Boolean;
Const
	LIM_MIN = -10;
	LIM_MAX = +10;
Var
	CCVal, BOVal, VAVal: Integer;
	CCLim, BOLim, VALim: Integer;
	CCDev, BODev, VADev: Double;
	CCPorc, BOPorc, VAPorc: Integer;
begin
	try
		ObtenerWorkload.ExecProc;
		// C�lculos
		CCVal := ObtenerWorkload.Parameters.ParamByName('@DataCallCenter').Value;
		BOVal := ObtenerWorkload.Parameters.ParamByName('@DataBackOffice').Value;
		VAVal := ObtenerWorkload.Parameters.ParamByName('@DataValidacion').Value;
		CCLim := ObtenerWorkload.Parameters.ParamByName('@MaxCallCenter').Value;
		BOLim := ObtenerWorkload.Parameters.ParamByName('@MaxBackOffice').Value;
		VALim := ObtenerWorkload.Parameters.ParamByName('@MaxValidacion').Value;
		CCDev := (CCVal - CCLim) / CCLim * 100;
		BODev := (BOVal - BOLim) / BOLim * 100;
		VADev := (VAVal - VALim) / VALim * 100;
		CCPorc := Round(CCVal / CCLim / 3 * 100);
		BOPorc := Round(BOVal / BOLim / 3 * 100);
		VAPorc := Round(VAVal / VALim / 3 * 100);
		// Actualizamos los datos en pantalla
		hg_CallCenter.AddValues([iif(CCPorc > 100, 100, CCPorc), 33]);
		hg_BackOffice.AddValues([iif(BOPorc > 100, 100, BOPorc), 33]);
		hg_Validacion.AddValues([iif(VAPorc > 100, 100, VAPorc), 33]);
		qnt_callcenter.Caption := IntToStr(CCVal);
		qnt_backoffice.Caption := IntToStr(BOVal);
		qnt_validacion.Caption := IntToStr(VAVal);
		lbl_CC_Valor.Caption   := qnt_callcenter.Caption;
		lbl_BO_Valor.Caption   := qnt_backoffice.Caption;
		lbl_VA_Valor.Caption   := qnt_validacion.Caption;
		lbl_CC_Max.Caption     := IntToStr(CCLim);
		lbl_BO_Max.Caption     := IntToStr(BOLim);
		lbl_VA_Max.Caption     := IntToStr(VALim);
		lbl_CC_Dev.Caption     := IntToStr(Round(CCDev)) + '%';
		lbl_BO_Dev.Caption     := IntToStr(Round(BODev)) + '%';
		lbl_VA_Dev.Caption     := IntToStr(Round(VADev)) + '%';
		lbl_CC_Status.Caption  := iif(CCDev <= LIM_MIN, 'Normal', iif(CCDev <= LIM_MAX, 'Alerta', 'Cr�tica'));
		lbl_BO_Status.Caption  := iif(BODev <= LIM_MIN, 'Normal', iif(BODev <= LIM_MAX, 'Alerta', 'Cr�tica'));
		lbl_VA_Status.Caption  := iif(VADev <= LIM_MIN, 'Normal', iif(VADev <= LIM_MAX, 'Alerta', 'Cr�tica'));
		Result := True;
	except
		on e: exception do begin
			MsgBox(e.Message, 'Error', MB_ICONSTOP);
			Result := False;
		end;
	end;
end;

end.
