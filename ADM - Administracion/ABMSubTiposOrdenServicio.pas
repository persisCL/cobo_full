{
Revision	:1
Author		: Nelson Droguett Sierra
Date 		: 09-Julio-2010
Description	: Se cambian los mensajes "Tipo Orden" por "Subtipo Orden"
			Se agrega un parameters.refresh en la llamada al stored que borra.
}

unit ABMSubTiposOrdenServicio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, AbmModelos, ADODB, DPSControls, Peatypes, VariantComboBox;

type
  TFormABMSubTiposOrdenServicio = class(TForm)
    GroupB: TPanel;
    Label2: TLabel;
    txt_descripcion: TMemo;
    Panel2: TPanel;
    Panel1: TPanel;
    Notebook: TNotebook;
    DBList1: TAbmList;
    btn_Mensajes: TSpeedButton;
    Panel3: TPanel;
    AbmToolbar1: TAbmToolbar;
    ObtenerSubTiposOrdenServicio: TADOStoredProc;
    InsertarSubTiposOrdenServicio: TADOStoredProc;
    BorrarSubTiposOrdenServicio: TADOStoredProc;
    ModificarSubTiposOrdenServicio: TADOStoredProc;
    Label1: TLabel;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    cbTiposOrdenServicio: TVariantComboBox;
    procedure AbmToolbar1Close(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect;
      State: TOwnerDrawState; Cols: TColPositions);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AltaSubTiposOrdenServicio(Descripcion:String;TipoOrdenServicio:Integer);
    procedure BajaSubTiposOrdenServicio(ID:Integer);
    procedure ModificacionSubTiposOrdenServicio(SubTipoOrdenServicio,TipoOrdenServicio:Integer;Descripcion:String);
    procedure Limpiar_Campos();
    function Inicializar(MDIChild: Boolean): Boolean;
    procedure Volver_Campos;
  end;

var
  FormABMSubTiposOrdenServicio: TFormABMSubTiposOrdenServicio;

implementation

uses DMConnection;

resourcestring
	//Rev.1 / 09-Julio-2010 / Nelson Droguett Sierra----------------------------------
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Subtipo de Orden de Servicio';
    MSG_TEXTO               = 'Debe ingresar el Texto';
{$R *.dfm}

procedure TFormABMSubTiposOrdenServicio.AbmToolbar1Close(Sender: TObject);
begin
     Close;
end;

function TFormABMSubTiposOrdenServicio.Inicializar(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
	if not OpenTables([ObtenerSubTiposOrdenServicio]) then exit;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	Result := True;
    CargarComboTiposOrdenServicio(DMConnections.BaseCAC, cbTiposOrdenServicio,True);
end;

procedure TFormABMSubTiposOrdenServicio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action:=caFree;
end;

procedure TFormABMSubTiposOrdenServicio.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

procedure TFormABMSubTiposOrdenServicio.Limpiar_Campos();
begin
    txt_descripcion.Clear;
    cbTiposOrdenServicio.ItemIndex:=0;
end;

procedure TFormABMSubTiposOrdenServicio.BtnCancelarClick(Sender: TObject);
begin
    Volver_Campos;
end;



procedure TFormABMSubTiposOrdenServicio.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then
     begin
     	Limpiar_Campos();
     end
end;

procedure TFormABMSubTiposOrdenServicio.AltaSubTiposOrdenServicio(Descripcion:String;TipoOrdenServicio:Integer);
begin
   with InsertarSubTiposOrdenServicio.Parameters do
   begin
    	Refresh;
        ParamByName('@Descripcion').Value   	:= Descripcion;
        ParamByName('@UsuarioCreacion').Value   := UsuarioSistema;
        if TipoOrdenServicio>0 then
	       ParamByName('@TipoOrdenServicio').Value	:= TipoOrdenServicio;
   end;
   InsertarSubTiposOrdenServicio.ExecProc;
end;


procedure TFormABMSubTiposOrdenServicio.ModificacionSubTiposOrdenServicio(SubTipoOrdenServicio,TipoOrdenServicio:Integer;Descripcion:String);
begin
   with ModificarSubTiposOrdenServicio.Parameters do
   begin
    	Refresh;
        ParamByName('@SubTipoOrdenServicio').Value	:= SubTipoOrdenServicio;
        ParamByName('@Descripcion').Value			:= Descripcion;
        ParamByName('@UsuarioModificacion').Value   := UsuarioSistema;
        if TipoOrdenServicio>0 then
          ParamByName('@TipoOrdenServicio').Value		:= TipoOrdenServicio;
   end;
   ModificarSubTiposOrdenServicio.ExecProc;
end;

procedure TFormABMSubTiposOrdenServicio.BajaSubTiposOrdenServicio(ID:Integer);
begin
	//Rev.1 / 09-Julio-2010 / Nelson Droguett Sierra----------------------------------
   BorrarSubTiposOrdenServicio.Parameters.Refresh;
   BorrarSubTiposOrdenServicio.Parameters.ParamByName('@Id').Value:=Id;
   BorrarSubTiposOrdenServicio.ExecProc;
end;

procedure TFormABMSubTiposOrdenServicio.BtnAceptarClick(Sender: TObject);
resourcestring
	//Rev.1 / 09-Julio-2010 / Nelson Droguett Sierra----------------------------------
    MSG_ACTUALIZARTpOrdSrv = 'No se pudieron actualizar los datos del Subtipo de Orden de Servicio seleccionado.';
    CAPTION_ACTUALIZARTpOrdSrv = 'Actualizar Subtipo de Orden de Servicio';
begin
    if not ValidateControls([txt_descripcion],
                [trim(txt_descripcion.text) <> ''],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_TEXTO]) then exit;

 	Screen.Cursor := crHourGlass;
	With DbList1.Table do begin
		Try
			if DbList1.Estado = Alta then begin
                AltaSubTiposOrdenServicio( Trim(txt_descripcion.Lines.Text),
                                           cbTiposOrdenServicio.Value);
			end else begin
                ModificacionSubTiposOrdenServicio( FieldByName('SubTipoOrdenServicio').asInteger,
                                                   cbTiposOrdenServicio.Value,
                                                   Trim(txt_descripcion.Lines.Text)
                                                  );
            end;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(MSG_ACTUALIZARTpOrdSrv, E.message, CAPTION_ACTUALIZARTpOrdSrv, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormABMSubTiposOrdenServicio.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormABMSubTiposOrdenServicio.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
    DbList1.Estado     := Alta;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
	txt_descripcion.SetFocus;
end;

procedure TFormABMSubTiposOrdenServicio.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    		:= False;
    dblist1.Estado     		:= modi;
	Notebook.PageIndex 		:= 1;
    groupb.Enabled     		:= True;
    txt_descripcion.setFocus;
end;

procedure TFormABMSubTiposOrdenServicio.DBList1Delete(Sender: TObject);
resourcestring
	//Rev.1 / 09-Julio-2010 / Nelson Droguett Sierra----------------------------------
    MSG_ELIMINARTpOrdSrv = '�Est� seguro de querer eliminar el Subtipo de Orden de Servico seleccionado?';
    MSG_ERRORELIMINARTpOrdSrv = 'No se pudo eliminar el Subtipo de Orden de Servico seleccionado?.';
    CAPTION_ELIMINARTpOrdSrv = 'Eliminar Subtipo de Orden de Servico';
var
    primerRg:Boolean;
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_ELIMINARTpOrdSrv, CAPTION_ELIMINARTpOrdSrv, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
            primerRg:=ObtenerSubTiposOrdenServicio.Bof;
            BajaSubTiposOrdenServicio(ObtenerSubTiposOrdenServicio.FieldbyName('SubTipoOrdenServicio').Value);
            if primerRg then
                begin
                    ObtenerSubTiposOrdenServicio.Close;
                    ObtenerSubTiposOrdenServicio.Open;
                end
                else
                    ObtenerSubTiposOrdenServicio.MoveBy(-1);

		Except
			On E: Exception do begin
				MsgBoxErr(MSG_ERRORELIMINARTpOrdSrv, e.message, CAPTION_ELIMINARTpOrdSrv, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormABMSubTiposOrdenServicio.DBList1Click(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
        txt_descripcion.text		:= FieldByName('Descripcion').AsString;
        if FieldByName('TipoOrdenServicio').IsNull  then
          cbTiposOrdenServicio.Value	:= -1
        else
          cbTiposOrdenServicio.Value	:= FieldByName('TipoOrdenServicio').AsInteger;
	end;
end;

procedure TFormABMSubTiposOrdenServicio.DBList1DrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do
    begin
		FillRect(Rect);
        TextOut(Cols[0], Rect.Top, FieldbyName('SubTipoOrdenServicio').AsString);
		TextOut(Cols[1], Rect.Top, FieldbyName('Descripcion').AsString);
        TextOut(Cols[2], Rect.Top, FieldbyName('TipoOrdenServicio').AsString);
		TextOut(Cols[3], Rect.Top, FieldbyName('TOSDescripcion').AsString);
	end;
end;

end.
