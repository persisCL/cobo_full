object frmInterfaseGEO: TfrmInterfaseGEO
  Left = 122
  Top = 135
  BorderStyle = bsToolWindow
  Caption = 'Interfase a GEO'
  ClientHeight = 523
  ClientWidth = 778
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object DPSPageControl1: TDPSPageControl
    Left = 0
    Top = 0
    Width = 778
    Height = 490
    ActivePage = TabSheet2
    Align = alTop
    OwnerDraw = True
    TabIndex = 1
    TabOrder = 0
    PageColor = clBtnFace
    Color = clBtnFace
    object TabSheet1: TTabSheet
      Caption = 'Equivalencias'
      object Label1: TLabel
        Left = 3
        Top = 3
        Width = 52
        Height = 13
        Caption = 'Comunas'
        FocusControl = ed_EquivComunas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 387
        Top = 3
        Width = 53
        Height = 13
        Caption = 'Ciudades'
        FocusControl = ed_EquivCiudades
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ed_EquivComunas: TValueListEditor
        Left = 3
        Top = 18
        Width = 380
        Height = 440
        FixedCols = 1
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing, goAlwaysShowEditor, goThumbTracking]
        TabOrder = 0
        TitleCaptions.Strings = (
          'Sistema'
          'Interfase')
        ColWidths = (
          48
          326)
      end
      object ed_EquivCiudades: TValueListEditor
        Left = 387
        Top = 18
        Width = 380
        Height = 440
        FixedCols = 1
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing, goAlwaysShowEditor, goThumbTracking]
        TabOrder = 1
        TitleCaptions.Strings = (
          'Sistema'
          'Interfase')
        ColWidths = (
          48
          326)
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Importaci'#243'n'
      ImageIndex = 1
      object Log: TMemo
        Left = 0
        Top = 0
        Width = 770
        Height = 424
        Align = alTop
        ReadOnly = True
        TabOrder = 0
      end
      object ListaDatos: TValueListEditor
        Left = 15
        Top = 9
        Width = 199
        Height = 73
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing, goAlwaysShowEditor, goThumbTracking]
        TabOrder = 1
        TitleCaptions.Strings = (
          'Campo'
          'Valor')
        Visible = False
        ColWidths = (
          89
          104)
      end
      object btn_Procesar: TButton
        Left = 691
        Top = 432
        Width = 75
        Height = 25
        Caption = 'Procesar'
        TabOrder = 2
        OnClick = btn_ProcesarClick
      end
    end
  end
  object btn_Salir: TButton
    Left = 697
    Top = 495
    Width = 75
    Height = 25
    Caption = 'Salir'
    TabOrder = 1
    OnClick = btn_SalirClick
  end
  object qry_Comunas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Descrip'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = 'vitacura'
      end>
    SQL.Strings = (
      'select * from comunas'
      'where DescripcionInterfase = :Descrip')
    Left = 252
    Top = 33
  end
  object qry_TiposCalle: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Descrip'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = 'vitacura'
      end>
    SQL.Strings = (
      'select CodigoTipoCalle, Descripcion from TiposCalle'
      'where upper(Descripcion) = :Descrip')
    Left = 219
    Top = 33
  end
  object qry_Ciudades: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Descrip'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = 'vitacura'
      end
      item
        Name = 'CodigoPais'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'CodigoRegion'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      'select * from Ciudades'
      'where '
      '  DescripcionInterfase = :Descrip and'
      '  CodigoPais = :CodigoPais and'
      '  CodigoRegion = :CodigoRegion')
    Left = 282
    Top = 33
  end
  object ActualizarCallesPorInterfase: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarCallesPorInterfase;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@NumeroDesde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroHasta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoCalle'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoOriginalGEO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 312
    Top = 33
  end
  object qry_ListaComunas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from Comunas order by CodigoComuna')
    Left = 12
    Top = 492
  end
  object qry_ListaCiudades: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from Ciudades order by CodigoCiudad')
    Left = 42
    Top = 492
  end
  object ActualizarComunasCiudadesInterfase: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarComunasCiudadesInterfase;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
      end
      item
        Name = '@DescripcionComunaInterfase'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end
      item
        Name = '@CodigoCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
      end
      item
        Name = '@DescripcionCiudadInterfase'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end>
    Left = 342
    Top = 33
  end
end
