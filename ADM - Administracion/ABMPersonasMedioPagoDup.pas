
unit ABMPersonasMedioPagoDup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb, PeaTypes,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls, ListBoxEx, DBListEx, Variants,
  RStrings, StrUtils;

type
  TFABMPersonasMedioPagoDup = class(TForm)
	GroupB: TPanel;
	Panel2: TPanel;
	Notebook: TNotebook;
    tblPersonasMPDup: TADOTable;
	Panel1: TPanel;
    AbmToolbar1: TAbmToolbar;
	Label2: TLabel;
    Label3: TLabel;
	Label10: TLabel;
    Lista: TAbmList;
    dsPersonasMPDup: TDataSource;
    txtDocumento: TEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
	procedure BtnCancelarClick(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaEdit(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
	function ListaProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure txtDocumentoKeyPress(Sender: TObject; var Key: Char);
  private
	{ Private declarations }
    FRUTOriginal: AnsiString;
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FABMPersonasMedioPagoDup: TFABMPersonasMedioPagoDup;

implementation

{$R *.DFM}

resourcestring
    STR_PERSONAS_MPD = 'Personas Medio Pago Duplicado';

function TFABMPersonasMedioPagoDup.Inicializa:Boolean;
Var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblPersonasMPDup]) then exit;
	Notebook.PageIndex := 0;
    FRUTOriginal := '';
	Lista.Reload;
	Result := True;
end;

procedure TFABMPersonasMedioPagoDup.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFABMPersonasMedioPagoDup.ListaClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do
	   txtDocumento.Text := FieldByName('NumeroDocumento').AsString;
end;

procedure TFABMPersonasMedioPagoDup.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Trim(Tabla.FieldbyName('NumeroDocumento').AsString));
	end;
end;

procedure TFABMPersonasMedioPagoDup.ListaEdit(Sender: TObject);
begin
	Lista.Enabled:= False;
	Lista.Estado := modi;

	Notebook.PageIndex := 1;
	groupb.Enabled     := True;
    txtDocumento.Text := TRIM(txtDocumento.Text);
    FRUTOriginal := txtDocumento.Text;

	ActiveControl := txtDocumento;
end;

procedure TFABMPersonasMedioPagoDup.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then LimpiarCampos;
end;

procedure TFABMPersonasMedioPagoDup.LimpiarCampos;
begin
	txtDocumento.Clear;
end;

procedure TFABMPersonasMedioPagoDup.AbmToolbar1Close(Sender: TObject);
begin
	 close;
end;

procedure TFABMPersonasMedioPagoDup.ListaDelete(Sender: TObject);
resourcestring
  MSG_DELETE_CAPTION = 'Eliminar Documento';
begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_PERSONAS_MPD]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            tblPersonasMPDup.Delete;
            tblPersonasMPDup.Refresh;
		Except
			On E: Exception do begin
				MsgBox(Format(MSG_ERROR_ELIMINAR,[STR_PERSONAS_MPD]), Format(MSG_CAPTION_ELIMINAR,[STR_PERSONAS_MPD]), MB_ICONSTOP);
			end
		end
	end;

	Lista.Reload;
	Lista.Estado  := Normal;
	Lista.Enabled := True;
	Notebook.PageIndex := 0;
	Screen.Cursor := crDefault;
end;

procedure TFABMPersonasMedioPagoDup.ListaInsert(Sender: TObject);
begin
	LimpiarCampos;
    FRUTOriginal        := '';
    groupb.Enabled      := True;
	Lista.Enabled       := False;
	Notebook.PageIndex  := 1;
	ActiveControl       := txtDocumento;
end;

procedure TFABMPersonasMedioPagoDup.BtnAceptarClick(Sender: TObject);

    function DocumentoRepetido: Boolean;
    begin
        with tblPersonasMPDup do begin
            DisableControls;
            Result :=  Locate('NumeroDocumento', Trim(txtDocumento.Text), []);
            if ( (TRIM(FieldByName('NumeroDocumento').AsString) = FRUTOriginal) AND
                  Result ) then Result := False;
            EnableControls;
        end;
    end;

resourcestring
    STR_NRO_DOC = 'Documento repetido';
    MSG_ERROR = 'Se produjo un error al insertar el registro';
    MSG_ERROR_DUP = 'El documento ya fu� dado de alta';
    MSG_ERROR_DIGITO = 'El d�gito verificador no es v�lido';
begin
    txtDocumento.Text := TRIM(txtDocumento.Text)  ;

	if not ValidateControls([txtDocumento,txtDocumento, txtDocumento],
	  [(Trim(txtDocumento.Text) <> ''),
      ValidarRUT(DMConnections.BaseCAC, txtDocumento.Text),
      (NOT DocumentoRepetido)],
	  Format(MSG_CAPTION_ACTUALIZAR,[STR_PERSONAS_MPD]),
	  [Format(MSG_VALIDAR_DEBE_EL,[STR_NRO_DOC]),MSG_ERROR_DIGITO,MSG_ERROR_DUP]) then begin
		Exit;
	end;

	with Lista do begin
		Screen.Cursor := crHourGlass;
        txtDocumento.Text := PadL(txtDocumento.Text, 9, '0');
		try
			try
                if Lista.Estado = Alta then tblPersonasMPDup.Append else tblPersonasMPDup.Edit;
                tblPersonasMPDup.FieldByName('NumeroDocumento').AsString := Trim(txtDocumento.Text);
                tblPersonasMPDup.Post;
			except
				On E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_PERSONAS_MPD]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_PERSONAS_MPD]), MB_ICONSTOP);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

procedure TFABMPersonasMedioPagoDup.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

procedure TFABMPersonasMedioPagoDup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;

procedure TFABMPersonasMedioPagoDup.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TFABMPersonasMedioPagoDup.Volver_Campos;
begin
	Lista.Estado := Normal;
	Lista.Enabled:= True;

	ActiveControl       := Lista;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;
	Lista.Reload;
end;

function TFABMPersonasMedioPagoDup.ListaProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Result := True;
end;

procedure TFABMPersonasMedioPagoDup.txtDocumentoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if NOT (Key in [char(VK_BACK), char(VK_TAB), char(VK_CLEAR), char(VK_RETURN), Char(VK_DELETE), '0'..'9','k','K'] )then Key := #0;
end;

end.

