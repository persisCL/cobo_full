object ABMTarifasInfractoresForm: TABMTarifasInfractoresForm
  Left = 0
  Top = 0
  Caption = 'Mantenimiento de Tarifas Infractores'
  ClientHeight = 362
  ClientWidth = 646
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object tbTarifasInfractores: TAbmToolbar
    Left = 0
    Top = 0
    Width = 646
    Height = 35
    Habilitados = [btAlta, btBaja, btSalir, btBuscar]
    OnClose = tbTarifasInfractoresClose
  end
  object lbTarifasInfractores: TAbmList
    Left = 0
    Top = 35
    Width = 646
    Height = 154
    TabStop = True
    TabOrder = 1
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'177'#0'Fecha Activacion'
      #0'255'#0'Categor'#237'a'
      #0'167'#0'Importe'
      #0'8'#0)
    HScrollBar = True
    RefreshTime = 300
    Table = SpTarifasInfractores
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = lbTarifasInfractoresClick
    OnDrawItem = lbTarifasInfractoresDrawItem
    OnRefresh = lbTarifasInfractoresRefresh
    OnInsert = lbTarifasInfractoresInsert
    OnDelete = lbTarifasInfractoresDelete
    Access = [accAlta, accBaja]
    Estado = Normal
    ToolBar = tbTarifasInfractores
  end
  object pnlTarifasInfractores: TPanel
    Left = 0
    Top = 189
    Width = 646
    Height = 132
    Align = alBottom
    TabOrder = 2
    object lblFechaActivacion: TLabel
      Left = 56
      Top = 19
      Width = 115
      Height = 13
      Caption = 'Fecha de Activaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCategoria: TLabel
      Left = 56
      Top = 46
      Width = 58
      Height = 13
      Caption = 'Categor'#237'a:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblImporte: TLabel
      Left = 56
      Top = 74
      Width = 50
      Height = 13
      Caption = 'Importe:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtImporte: TEdit
      Left = 217
      Top = 71
      Width = 186
      Height = 21
      Color = 16444382
      MaxLength = 7
      TabOrder = 2
      OnKeyPress = txtImporteKeyPress
    end
    object cbCategoria: TVariantComboBox
      Left = 217
      Top = 44
      Width = 186
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
    object deFechaActivacion: TDateEdit
      Left = 217
      Top = 17
      Width = 186
      Height = 21
      AutoSelect = False
      Color = 16444382
      TabOrder = 0
      Date = -693594.000000000000000000
    end
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 321
    Width = 646
    Height = 41
    Align = alBottom
    TabOrder = 3
    object Notebook: TNotebook
      Left = 448
      Top = 1
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 76
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 26
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 183
    Top = 4
    Width = 314
    Height = 28
    BevelOuter = bvNone
    Caption = 'Panel1'
    TabOrder = 4
    object Label1: TLabel
      Left = 11
      Top = 7
      Width = 71
      Height = 13
      Caption = 'Concesionaria:'
    end
    object vcbConcesionaria: TVariantComboBox
      Left = 87
      Top = 4
      Width = 215
      Height = 21
      Style = vcsDropDownList
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 0
      OnChange = vcbConcesionariaChange
      Items = <>
    end
  end
  object qryCategorias: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT Categoria, Descripcion'
      'FROM Categorias (NOLOCK)')
    Left = 168
    Top = 80
  end
  object SpTarifasInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTarifasInfractores;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 120
    Top = 80
  end
end
