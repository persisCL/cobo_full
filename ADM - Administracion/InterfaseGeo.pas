{********************************** File Header ********************************
File Name   : InterfaseGeo
Author      : Castro, Ra�l <rcastro@dpsautomation.com>
Date Created: 02-FEB-2004
Language    : ES-AR
Description : Procesa e incorpora la informaci�n de calles provista por GEOInfo
				a la base de datos
*******************************************************************************}

unit InterfaseGeo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, Grids, ValEdit, ComCtrls, DPSPageControl, UtilProc;
  
type
  TfrmInterfaseGEO = class(TForm)
	qry_Comunas: TADOQuery;
	qry_TiposCalle: TADOQuery;
	qry_Ciudades: TADOQuery;
	ActualizarCallesPorInterfase: TADOStoredProc;
    DPSPageControl1: TDPSPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Log: TMemo;
    ListaDatos: TValueListEditor;
    btn_Procesar: TButton;
    ed_EquivComunas: TValueListEditor;
    ed_EquivCiudades: TValueListEditor;
    qry_ListaComunas: TADOQuery;
	qry_ListaCiudades: TADOQuery;
	Label1: TLabel;
	Label2: TLabel;
	btn_Salir: TButton;
	ActualizarComunasCiudadesInterfase: TADOStoredProc;
	procedure btn_SalirClick(Sender: TObject);
	procedure ActualizarComunasYCiudadesInterfase;
	procedure btn_ProcesarClick(Sender: TObject);
	procedure Iniciar;
	procedure FormCreate(Sender: TObject);
	procedure FormDestroy(Sender: TObject);
  private
	{ Private declarations }
	CantErrores: Integer;
  public
	{ Public declarations }
  end;

var
  frmInterfaseGEO: TfrmInterfaseGEO;

implementation

uses DMConnection;

{$R *.dfm}

procedure TfrmInterfaseGEO.btn_SalirClick(Sender: TObject);
begin
	Close
end;

procedure TfrmInterfaseGEO.ActualizarComunasYCiudadesInterfase;
var
	i : integer;
begin
	for i := 1 To ed_EquivComunas.RowCount-1 do
		With ActualizarComunasCiudadesInterfase, Parameters Do Begin
			ParamByName ('@CodigoComuna').Value 				:= ed_EquivComunas.Cells [0, i];
			ParamByName ('@DescripcionComunaInterfase').Value 	:= ed_EquivComunas.Cells [1, i];

			ExecProc;
			Close
		end;

	for i := 1 To ed_EquivCiudades.RowCount-1 do
		With ActualizarComunasCiudadesInterfase, Parameters Do Begin
			ParamByName ('@CodigoCiudad').Value 				:= ed_EquivCiudades.Cells [0, i];
			ParamByName ('@DescripcionCiudadInterfase').Value 	:= ed_EquivCiudades.Cells [1, i];

			ExecProc;
			Close
		end
end;


procedure TfrmInterfaseGEO.btn_ProcesarClick(Sender: TObject);
resourcestring
  MSG_ACTUALIZAR_ERROR   = 'Error al actualizar datos de la Calle.';
  MSG_ACTUALIZAR_CAPTION = 'Actualizar Maestro de Calles';

	procedure Error (Linea: Integer; TxtLinea, TxtError: String; var CErrores: Integer);
	begin
		Inc (CErrores);
		If CErrores = 1 then begin
			Log.Lines.Add ('');
			Log.Lines.Add ('La l�nea ' + IntToStr (Linea) + ' contiene errores; ha sido Ignorada.');
			Log.Lines.Add (TxtLinea);
		end;

		Log.Lines.Add ('  Error: ' + TxtError);
	end;

	procedure VerificarEntrada (LineaProc: Integer; TextoLinea: String; CantItems : Integer);
	var
		DesdeNro,
		HastaNro,
		i : integer;
	begin
		CantErrores := 0;

		If CantItems < ListaDatos.RowCount then
			Error (LineaProc, TextoLinea, 'Cantidad de datos insuficientes.' + inttostr (CantItems) + ' items de ' + inttostr (ListaDatos.RowCount) + ' requeridos.', CantErrores)
		else begin
			// Analizo que cada campo no sea vac�o
			for i := 1 To ListaDatos.RowCount-1 do begin
				If ListaDatos.Cells [1, i] = '' then
					Error (LineaProc, TextoLinea, 'Campo ' + ListaDatos.Cells [0, i] +  ' vac�o.', CantErrores);
			end;

			// Hay errores, salgo
			If CantErrores <> 0 then Exit;

			// Analizo puntualmente los datos
			// Comuna
			ListaDatos.FindRow ('Comuna', i);
			with qry_Comunas, Parameters do begin
				Close;
				ParambyName ('Descrip').Value := UpperCase (ListaDatos.Cells [1,i]);
				Open;

				If FieldByName ('CodigoComuna').AsString = '' Then
					Error (LineaProc, TextoLinea, 'Comuna inexistente: ' + ListaDatos.Cells [1,i], CantErrores)
			end;

			// Ciudad
			ListaDatos.FindRow ('Ciudad', i);
			with qry_Ciudades, Parameters do begin
				Close;
				ParambyName ('Descrip').Value := UpperCase (ListaDatos.Cells [1,i]);
				ParambyName ('CodigoPais').Value := qry_Comunas.FieldByName ('CodigoPais').AsString;
				ParambyName ('CodigoRegion').Value := qry_Comunas.FieldByName ('CodigoRegion').AsString;;

				Open;

				If FieldByName ('CodigoCiudad').AsString = '' Then
					Error (LineaProc, TextoLinea, 'Ciudad inexistente en la Comuna indicada: ' + ListaDatos.Cells [1,i], CantErrores)
			end;

			// DesdeNro & HastaNro
			ListaDatos.FindRow ('DesdeNro', i);
			DesdeNro := StrToInt (ListaDatos.Cells [1,i]);

			ListaDatos.FindRow ('HastaNro', i);
			HastaNro := StrToInt (ListaDatos.Cells [1,i]);

			If (DesdeNro = 0) Or (HastaNro = 0) Or (DesdeNro > HastaNro) Then
				Error (LineaProc, TextoLinea, 'Valores en campos DesdeNro y/o HastaNro incorrectos: DesdeNro= ' + IntToStr (DesdeNro) + ', HastaNro= ' + IntToStr (HastaNro), CantErrores);

			// TipoCalle
			ListaDatos.FindRow ('TipoCalle', i);
			with qry_TiposCalle, Parameters do begin
				Close;
				ParambyName ('Descrip').Value := UpperCase (ListaDatos.Cells [1,i]);
				Open;

				If FieldByName ('CodigoTipoCalle').AsInteger = 0 Then
					Error (LineaProc, TextoLinea, 'Tipo de Calle inexistente: ' + ListaDatos.Cells [1,i], CantErrores)
			End;

			// Hay errores, salgo
			If CantErrores <> 0 then Exit;

			// Los datos fueron correctos, ingreso sus datos en la BD
			Screen.Cursor := crHourGlass;
			try
			  With ActualizarCallesPorInterfase, Parameters do begin
				ParamByName('@CodigoCalle').Value	:= 0;
				ListaDatos.FindRow ('Descripcion', i);
				ParamByName('@Descripcion').Value	:= ListaDatos.Cells [1,i];
				ParamByName('@NumeroDesde').Value	:= DesdeNro;
				ParamByName('@NumeroHasta').Value	:= HastaNro;
				ParamByName('@CodigoTipoCalle').Value:= qry_TiposCalle.FieldByName ('CodigoTipoCalle').AsInteger;
				ParambyName('@CodigoPais').Value 	:= qry_Comunas.FieldByName ('CodigoPais').AsString;
				ParambyName('@CodigoRegion').Value 	:= qry_Comunas.FieldByName ('CodigoRegion').AsString;;
				ParamByName('@CodigoComuna').Value	:= qry_Comunas.FieldByName ('CodigoComuna').AsString;;
				ParamByName('@CodigoCiudad').Value	:= qry_Ciudades.FieldByName ('CodigoCiudad').AsString;;
				ListaDatos.FindRow ('CodigoGEO', i);
				ParamByName('@CodigoOriginalGEO').Value:= ListaDatos.Cells [1,i]
			  end;

			  ActualizarCallesPorInterfase.ExecProc;
			  ActualizarCallesPorInterfase.Close

			 except

			  On E: exception do begin
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);

				ActualizarCallesPorInterfase.Close
			  end;
			end;
			Screen.Cursor := crDefault;

			qry_Comunas.Close;
			qry_Ciudades.Close;
			qry_TiposCalle.Close
		end
	end;

var
	F: TextFile;
	FormatoLinea,
	LineaTexto,
	LineaLeida,
	Token: String;
	Linea,
	PosTkn,
	Items,
	IniDelim,
	FinDelim: Integer;
begin
	ActualizarComunasYCiudadesInterfase;

	Log.Lines.Clear;
	Log.Lines.Add ('Proceso Iniciado: ' + DateTimeToStr (Now));
	Log.Lines.Add ('');

	{$I-}
	AssignFile(F, 'CamposGEO.txt');
	Reset(F);
	{$I-}

	If IOResult <> 0 then
		Log.Lines.Add ('Error: El archivo de campos no existe.')
	else begin
		// Genero Lista de Campos
		FormatoLinea := '';

		Readln(F, LineaTexto);			// L�nea de info
		while Not EoF (F) Do Begin
			Readln(F, LineaTexto);
			LineaTexto := Trim (LineaTexto);

			If (LineaTexto <> '') Then begin
				If (Not ListaDatos.FindRow (LineaTexto, PosTkn)) Then ListaDatos.InsertRow(LineaTexto, '', True);

				FormatoLinea := FormatoLinea + '<' + LineaTexto + '>'
			End
		End;
		CloseFile(F);

		Log.Lines.Add ('Formato de l�nea: ' + FormatoLinea);

		{$I-}
		AssignFile(F, 'DatosGEO.txt');
		Reset(F);
		{$I-}

		If IOResult <> 0 then
			Log.Lines.Add ('Error: El archivo de datos no existe.')
		else begin
			// Proceso los datos del GEO
			Linea := 0;
			while Not EoF (F) Do Begin
				Readln(F, LineaTexto);
				LineaLeida := LineaTexto;
				Inc (Linea);

				// Busca delimitador de inicio
				IniDelim := Pos ('<', LineaTexto);

				Items := 1;
				While (Items < ListaDatos.RowCount) And (IniDelim <> 0) Do Begin
					// consume hasta delimitador de inicio
					Delete (LineaTexto, 1, IniDelim);

					// Busca delimitador de finalizaci�n
					FinDelim := Pos ('>', LineaTexto);

					If FinDelim <> 0 Then Begin
						// extrae token
						Token := Copy (LineaTexto, 1, FinDelim - 1);

						ListaDatos.Cells[2, Items] := Token;

						// consume hasta delimitador de finalizaci�n
						Delete (LineaTexto, 1, FinDelim)
					End;

					// Busca delimitador de inicio
					IniDelim := Pos ('<', LineaTexto);
					Inc (Items);
				End;

				// Termin� la l�nea, verifica los datos
				VerificarEntrada (Linea, LineaLeida, Items)
			End;
			CloseFile(F)
		end
	end;

	Log.Lines.Add ('');
	Log.Lines.Add ('Proceso Terminado: ' + DateTimeToStr (Now));
	Log.Lines.Add ('Se gener� el archivo InterfaseGEO.log');
	Log.Lines.SaveToFile ('InterfaseGEO.log')
end;

procedure TfrmInterfaseGEO.Iniciar;
var
  Texto : String;
begin
	With qry_ListaComunas Do begin
		Open;
		While Not EoF Do Begin
			Texto := UpperCase (Trim (FieldByName ('DescripcionInterfase').AsString));

			If (Texto = '') Then
				ed_EquivComunas.InsertRow(FieldByName ('CodigoComuna').AsString, UpperCase (Trim (FieldByName ('Descripcion').AsString)), True)
			else
				ed_EquivComunas.InsertRow(FieldByName ('CodigoComuna').AsString, Texto, True);

			Next
		End;
		Close
	End;

	With qry_ListaCiudades Do begin
		Open;
		While Not EoF Do Begin
			Texto := UpperCase (Trim (FieldByName ('DescripcionInterfase').AsString));

			If (Texto = '') Then
				ed_EquivCiudades.InsertRow(FieldByName ('CodigoCiudad').AsString, UpperCase (Trim (FieldByName ('Descripcion').AsString)), True)
			else
				ed_EquivCiudades.InsertRow(FieldByName ('CodigoCiudad').AsString, Texto, True);

			Next
		End;
		Close
	End;

	DPSPageControl1.ActivePageIndex := 0
end;

procedure TfrmInterfaseGEO.FormCreate(Sender: TObject);
begin
	Iniciar;
end;

procedure TfrmInterfaseGEO.FormDestroy(Sender: TObject);
begin
	ActualizarComunasYCiudadesInterfase
end;

end.

