{-----------------------------------------------------------------------------
 Unit Name: ABMBodegas
 Author:
 Description: ABM de Bodegas
-----------------------------------------------------------------------------}
unit ABMBodegas;

interface

uses
  //ABM Bodegas
  DMConnection,
  UtilProc,
  Util,
  RStrings,
  utildb,
  PeaTypes,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants;

type
  TFABMBodegas = class(TForm)
	GroupB: TPanel;
    PAbajo: TPanel;
	Notebook: TNotebook;
    tblBodegas: TADOTable;
    txtCodigo: TNumericEdit;
    AbmToolbar: TAbmToolbar;
    spActualizarBodegas: TADOStoredProc;
    Lcodigo: TLabel;
    Ldescripcion: TLabel;
    ListaBodegas: TAbmList;
    dsBodegas: TDataSource;
    txtDescripcion: TEdit;
    EliminarBodega: TADOStoredProc;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure FormShow(Sender: TObject);
   	procedure ListaBodegasRefresh(Sender: TObject);
   	function  ListaBodegasProcess(Tabla: TDataSet; var Texto: String): Boolean;
   	procedure ListaBodegasDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaBodegasClick(Sender: TObject);
	procedure ListaBodegasInsert(Sender: TObject);
	procedure ListaBodegasEdit(Sender: TObject);
	procedure ListaBodegasDelete(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure AbmToolbarClose(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FABMBodegas: TFABMBodegas;

resourcestring
	STR_MAESTRO_BODEGAS	= 'Maestro de Bodegas';


implementation

{$R *.DFM}

{-----------------------------------------------------------------------------
  Function Name: Inicializa
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFABMBodegas.Inicializa:Boolean;
Var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblBodegas]) then exit;
	Notebook.PageIndex := 0;
	ListaBodegas.Reload;
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarCampos
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.LimpiarCampos;
begin
	txtCodigo.Clear;
	txtDescripcion.Clear;
end;


{-----------------------------------------------------------------------------
  Function Name: FormShow
  Author:
  Date Created:
  Description: Actualizo la grilla
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.FormShow(Sender: TObject);
begin
	ListaBodegas.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaBodegasRefresh
  Author:
  Date Created:
  Description: lipio los campos si la tabla esta vacia
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.ListaBodegasRefresh(Sender: TObject);
begin
	 if ListaBodegas.Empty then LimpiarCampos;
end;

{-----------------------------------------------------------------------------
  Function Name: Volver_Campos
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.Volver_Campos;
begin
	ListaBodegas.Estado := Normal;
	ListaBodegas.Enabled:= True;

	ActiveControl       := ListaBodegas;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;
	ListaBodegas.Reload;
end;


{-----------------------------------------------------------------------------
  Function Name: ListaBodegasDrawItem
  Author:
  Date Created:
  Description: Muestro los Registros
  Parameters: Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.ListaBodegasDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoBodega').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaBodegasClick
  Author:
  Date Created:
  Description: Selecciono un campo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.ListaBodegasClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   txtCodigo.Value		:= FieldByName('CodigoBodega').AsInteger;
	   txtDescripcion.Text	:= FieldByname('Descripcion').AsString;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaBodegasProcess
  Author:
  Date Created:
  Description:
  Parameters: Tabla: TDataSet;var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFABMBodegas.ListaBodegasProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaBodegasInsert
  Author:
  Date Created:
  Description: limpio los campos para que ingrese un nuevo registro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.ListaBodegasInsert(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled          	:= True;
	ListaBodegas.Enabled := False;
	Notebook.PageIndex      	:= 1;
	ActiveControl           	:= txtDescripcion;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaBodegasEdit
  Author:
  Date Created:
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.ListaBodegasEdit(Sender: TObject);
begin
	ListaBodegas.Enabled:= False;
	ListaBodegas.Estado := modi;

	Notebook.PageIndex := 1;
	groupb.Enabled     := True;

	ActiveControl:= txtDescripcion
end;

{-----------------------------------------------------------------------------
  Function Name: ListaBodegasDelete
  Author:
  Date Created:
  Description: elimino un item
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.ListaBodegasDelete(Sender: TObject);
resourcestring
  MSG_EXISTEN_ALMACEN = 'Existen Almacen asociados a esta bodega.';
  MSG_DELETE_CAPTION		   = 'Eliminar Bodega.';
begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_BODEGAS]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with EliminarBodega do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoBodega').Value := tblBodegas.FieldbyName('CodigoBodega').Value;
                Parameters.ParamByName('@Resultado').Value := 0;
                ExecProc;
                if Parameters.ParamByName('@Resultado').Value = 1 then
                    MsgBox(MSG_EXISTEN_ALMACEN, MSG_DELETE_CAPTION, MB_ICONSTOP);
                close;
            end;
		Except
			On E: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_BODEGAS]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_BODEGAS]), MB_ICONSTOP);
			end
		end
	end;

	ListaBodegas.Reload;
	ListaBodegas.Estado := Normal;
	ListaBodegas.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:    lgisuk
  Date Created: 19/04/2005
  Description: Acepto los cambios
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.BtnAceptarClick(Sender: TObject);

begin
	if not ValidateControls([txtDescripcion],
	  [(Trim(txtDescripcion.Text) <> '')],
	  Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_BODEGAS]),
	  [Format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION])]) then begin
		Exit;
	end;

	with ListaBodegas do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarBodegas, Parameters do begin
					ParamByName('@CodigoBodega').Value := txtCodigo.ValueInt;
					ParamByName('@Descripcion').Value := Trim(txtDescripcion.Text);

					ExecProc;
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_BODEGAS]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_BODEGAS]), MB_ICONSTOP);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnCancelarClick
  Author:
  Date Created:
  Description: permito cancelar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

{-----------------------------------------------------------------------------
  Function Name: AbmToolbar1Close
  Author:
  Date Created:
  Description: salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.AbmToolbarClose(Sender: TObject);
begin
	 close;
end;


{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author:
  Date Created:
  Description:  salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:
  Date Created:
  Description: lo libero de memoria
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMBodegas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;




end.

