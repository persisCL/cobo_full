object FormDemoras: TFormDemoras
  Left = 186
  Top = 163
  BorderStyle = bsDialog
  Caption = 'Demoras M'#225'ximas entre Puntos de Cobro'
  ClientHeight = 450
  ClientWidth = 662
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 417
    Width = 662
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object btn_Cancel: TDPSButton
      Left = 586
      Top = 7
      Width = 69
      Height = 22
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 0
      OnClick = btn_CancelClick
    end
    object btn_Aceptar: TDPSButton
      Left = 504
      Top = 7
      Width = 69
      Height = 22
      Caption = '&Aceptar'
      Default = True
      ModalResult = 1
      TabOrder = 1
      OnClick = btn_AceptarClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = -31
    Width = 662
    Height = 448
    Align = alBottom
    TabOrder = 1
    object Grid: TStringGrid
      Left = 1
      Top = 1
      Width = 660
      Height = 446
      Align = alClient
      ColCount = 4
      DefaultColWidth = 160
      DefaultRowHeight = 20
      FixedCols = 3
      RowCount = 50
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goRowSizing, goColSizing, goRowMoving, goColMoving, goEditing]
      TabOrder = 0
      OnKeyDown = GridKeyDown
      OnSelectCell = GridSelectCell
      ColWidths = (
        160
        160
        160
        155)
      RowHeights = (
        23
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20)
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 662
    Height = 36
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    object Label3: TLabel
      Left = 13
      Top = 11
      Width = 70
      Height = 13
      Caption = 'Concesionaria:'
    end
    object cb_concesionaria: TComboBox
      Left = 89
      Top = 7
      Width = 192
      Height = 21
      Style = csDropDownList
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      OnChange = cb_concesionariaChange
      Items.Strings = (
        '1 - Costanera Norte'
        '2 - Autopista Central')
    end
  end
  object ObtenerCombinacionesPuntosCobro: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCombinacionesPuntosCobro'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 520
    Top = 5
  end
  object GrabarDemoras: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GrabarDemoras'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PuntoCobroInicial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PuntoCobroFinal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoHorario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@MaxTiempo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 552
    Top = 5
  end
  object Concesionarias: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'Concesionarias'
    Left = 592
    Top = 5
  end
end
