object FormFuentesSolicitud: TFormFuentesSolicitud
  Left = 254
  Top = 186
  Width = 600
  Height = 452
  Caption = 'Mantenimiento de Fuentes de Solicitud'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 207
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'86'#0'C'#243'digo               '
      #0'187'#0'Descripci'#243'n                                         '
      #0'91'#0'Cant de Pregunta'
      #0'47'#0'Activo   '
      #0'86'#0'Nombre Dominio')
    HScrollBar = True
    RefreshTime = 100
    Table = FuentesSolicitud
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 240
    Width = 592
    Height = 139
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 15
      Top = 36
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txt_Descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 15
      Top = 12
      Width = 39
      Height = 13
      Caption = '&C'#243'digo: '
      FocusControl = txt_CodigoFuenteSolicitud
    end
    object Label3: TLabel
      Left = 15
      Top = 112
      Width = 174
      Height = 13
      Caption = 'C&antidad de Preguntas en Encuesta:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 15
      Top = 86
      Width = 81
      Height = 13
      Caption = 'Nombre D&ominio:'
      FocusControl = txt_dominio
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 15
      Top = 62
      Width = 75
      Height = 13
      Caption = 'Tipo de F&uente:'
      FocusControl = txt_dominio
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txt_Descripcion: TEdit
      Left = 106
      Top = 32
      Width = 271
      Height = 21
      Hint = 'Descripci'#243'n de la fuente'
      Color = 16444382
      MaxLength = 40
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object txt_CodigoFuenteSolicitud: TNumericEdit
      Left = 106
      Top = 8
      Width = 69
      Height = 21
      TabStop = False
      Color = clBtnFace
      MaxLength = 3
      ReadOnly = True
      TabOrder = 0
      Decimals = 0
    end
    object txt_CantPregunta: TNumericEdit
      Left = 200
      Top = 107
      Width = 69
      Height = 21
      Hint = 'Cantidad de preguntas a realizar al final de la solicitud'
      MaxLength = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Decimals = 0
    end
    object cb_Desactivada: TCheckBox
      Left = 384
      Top = 112
      Width = 129
      Height = 17
      Caption = 'Fuente Desactivada'
      TabOrder = 5
    end
    object txt_dominio: TEdit
      Left = 106
      Top = 82
      Width = 271
      Height = 21
      Hint = 'Nombre de dominio (para tipo de fuente WEB).'
      MaxLength = 50
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
    object cbTipoFuenteSolicitud: TVariantComboBox
      Left = 106
      Top = 57
      Width = 151
      Height = 21
      Hint = 'Tipo de fuente'
      Style = vcsDropDownList
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 379
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Label5: TLabel
      Left = 35
      Top = 13
      Width = 63
      Height = 13
      Caption = 'Desactivado.'
    end
    object Notebook: TNotebook
      Left = 395
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
    object Panel1: TPanel
      Left = 16
      Top = 12
      Width = 12
      Height = 14
      BevelOuter = bvNone
      Color = clRed
      TabOrder = 1
    end
  end
  object FuentesSolicitud: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'FuentesSolicitud'
    Left = 242
    Top = 110
  end
end
