object frmABMMotivosTransitosNoFacturables: TfrmABMMotivosTransitosNoFacturables
  Left = 252
  Top = 222
  Width = 638
  Height = 480
  Caption = 'frmABMMotivosTransitosNoFacturables'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolBar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 630
    Height = 33
    Habilitados = [btModi, btSalir]
    OnClose = AbmToolBarClose
  end
  object pnl_Bottom: TPanel
    Left = 0
    Top = 407
    Width = 630
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object lbl_HintDeshabilitado: TLabel
      Left = 35
      Top = 12
      Width = 150
      Height = 13
      Caption = ' No Editable. Valor Param'#233'trico.'
    end
    object nb_Botones: TNotebook
      Left = 433
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btn_Salir: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btn_SalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object btn_Aceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 25
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btn_AceptarClick
        end
        object btn_Cancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = btn_CancelarClick
        end
      end
    end
    object pnl_HintDeshabilitado: TPanel
      Left = 16
      Top = 12
      Width = 12
      Height = 13
      BevelOuter = bvNone
      Color = clGray
      TabOrder = 1
    end
  end
  object dbl_Motivos: TAbmList
    Left = 0
    Top = 33
    Width = 630
    Height = 311
    TabStop = True
    TabOrder = 2
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'86'#0'C'#243'digo               '
      #0'187'#0'Descripci'#243'n                                         ')
    HScrollBar = True
    RefreshTime = 100
    Table = tbl_MotivosTransitoNoFacturable
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dbl_MotivosClick
    OnDrawItem = dbl_MotivosDrawItem
    OnInsert = dbl_MotivosInsert
    OnDelete = dbl_MotivosDelete
    OnEdit = dbl_MotivosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolBar
  end
  object pnl_Datos: TPanel
    Left = 0
    Top = 344
    Width = 630
    Height = 63
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 3
    object Ldescripcion: TLabel
      Left = 16
      Top = 36
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_CodigoMotivo: TLabel
      Left = 16
      Top = 12
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object txt_Descripcion: TEdit
      Left = 92
      Top = 32
      Width = 388
      Height = 21
      Color = 16444382
      MaxLength = 50
      TabOrder = 1
    end
    object txt_CodigoMotivo: TNumericEdit
      Left = 92
      Top = 8
      Width = 105
      Height = 21
      TabStop = False
      Color = 16444382
      MaxLength = 3
      TabOrder = 0
      Decimals = 0
    end
  end
  object ds_MotivosTransitoNoFacturable: TDataSource
    DataSet = tbl_MotivosTransitoNoFacturable
    Left = 76
    Top = 104
  end
  object tbl_MotivosTransitoNoFacturable: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'MotivosTransitoNoFacturable'
    Left = 76
    Top = 60
  end
end
