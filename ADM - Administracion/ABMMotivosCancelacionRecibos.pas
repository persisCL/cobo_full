unit ABMMotivosCancelacionRecibos;

interface

{******************************************************************************
Revision    : 1
Author      : FSandi
Date Created: 03-10-2007
Description : Se agregan dos nuevas columnas a la tabla MotivosAnulacion, para determinar
            si estos motivos se incluyen en la pantalla Anulacion de Recibos, en la pantalla
            Anulacion de Cheques Protestados, o en ambas.

Etiqueta    :   20160613 MGO
Descripci�n :   Se valida que la descripci�n ingresada no exista.
                Se mantiene el campo de c�digo con color de deshabilitado.
*******************************************************************************}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, DPSControls, ExtCtrls, DmiCtrls, DbList,
  Abm_obj, UtilProc, UtilDB, PeaProcs;

type
  TFormABMMotivosCancelacionRecibos = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    txt_Descripcion: TEdit;
    txt_CodigoMotivoAnulacion: TNumericEdit;
    Panel2: TPanel;
    Notebook: TNotebook;
    MotivosAnulacion: TADOTable;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    ChkIncluirCheque: TCheckBox;
    ChkIncluirRecibo: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos();
  public
    { Public declarations }
    function Inicializa: boolean;
  end;

var
  FormABMMotivosCancelacionRecibos: TFormABMMotivosCancelacionRecibos;

implementation

uses
  DMConnection;

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar este Motivo de Anulaci�n de Recibo?';
    MSG_DELETE_ERROR		= 'No se puede eliminar este Motivo de Anulaci�n de Recibo porque hay datos que dependen de el.';
    MSG_DELETE_CAPTION 		= 'Eliminar Motivo de Anulaci�n de Recibo';
    MSG_ACTUALIZAR_ERROR	= 'No se pudo actualizar el Motivo de Anulaci�n de Recibo';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Motivo de Anulaci�n de Recibo';

{$R *.dfm}

{ TFormABMMotivosCancelacionRecibos }

procedure TFormABMMotivosCancelacionRecibos.Limpiar_Campos;
begin
	txt_CodigoMotivoAnulacion.Clear;
	txt_Descripcion.Clear;
    ChkIncluirCheque.Checked := False; //Revision 1
    ChkIncluirRecibo.Checked := False; //Revision 1
end;

function TFormABMMotivosCancelacionRecibos.Inicializa: boolean;
var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([MotivosAnulacion]) then
		Result := False
	else begin
		Result := True;
		DbList1.Reload;
	end;
   	Notebook.PageIndex := 0;
end;

procedure TFormABMMotivosCancelacionRecibos.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormABMMotivosCancelacionRecibos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFormABMMotivosCancelacionRecibos.AbmToolbar1Close(
  Sender: TObject);
begin
    Close;
end;

procedure TFormABMMotivosCancelacionRecibos.DBList1Click(Sender: TObject);
begin
	with MotivosAnulacion do begin
		txt_CodigoMotivoAnulacion.Value	:= FieldByName('CodigoMotivoAnulacion').AsInteger;
		txt_Descripcion.text			:= FieldByName('Descripcion').AsString;
        ChkIncluirRecibo.Checked        := FieldByName('IncluirEnAnulacionRecibo').AsBoolean;          //Revision 1
        ChkIncluirCheque.Checked        := FieldByName('IncluirEnAnulacionChequeProtestado').AsBoolean;//Revision 1
	end;
end;

procedure TFormABMMotivosCancelacionRecibos.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	if MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			MotivosAnulacion.Delete;
		except
			On E: Exception do begin
				MotivosAnulacion.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormABMMotivosCancelacionRecibos.DBList1DrawItem(
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
var
    I: Integer;
begin
	with Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 1 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormABMMotivosCancelacionRecibos.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    DBList1.Estado     := modi;
	Notebook.PageIndex := 1;
    GroupB.Enabled     := True;
    txt_CodigoMotivoAnulacion.Enabled	:= False;
	//txt_CodigoMotivoAnulacion.Color	:= clBtnFace;   // 20160613 MGO
    txt_Descripcion.setFocus;
end;

procedure TFormABMMotivosCancelacionRecibos.DBList1Insert(Sender: TObject);
begin
    GroupB.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    txt_CodigoMotivoAnulacion.Enabled	:= False;
	//txt_CodigoMotivoAnulacion.Color	:= clBtnFace;   // 20160613 MGO
	txt_Descripcion.SetFocus;
end;

procedure TFormABMMotivosCancelacionRecibos.DBList1Refresh(
  Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormABMMotivosCancelacionRecibos.BtnCancelarClick(
  Sender: TObject);
begin
   	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    GroupB.Enabled     := False;
end;

procedure TFormABMMotivosCancelacionRecibos.BtnAceptarClick(
  Sender: TObject);
resourcestring
    MSG_DESCRIPCION = 'Falta completar la descripci�n';
// INICIO : 20160613 MGO
    MSG_DESCRIPCION_EXISTE = 'La descripci�n ingresada ya existe';
var
    ValidarDesc: Boolean;
// FIN : 20160613 MGO
begin
    if not ValidateControls(
        [txt_Descripcion],
        [Trim(txt_Descripcion.Text) <> ''],
        MSG_ACTUALIZAR_CAPTION,
        [MSG_DESCRIPCION]) then exit;

 	Screen.Cursor := crHourGlass;
	with MotivosAnulacion do begin
		try
            { INICIO : 20160613 MGO
            if DBList1.Estado = Alta then Append
			else Edit;

            FieldByName('Descripcion').AsString := Trim(txt_Descripcion.text);
            FieldByName('IncluirEnAnulacionRecibo').AsBoolean := ChkIncluirRecibo.Checked;          //Revision 1
            FieldByName('IncluirEnAnulacionChequeProtestado').AsBoolean := ChkIncluirCheque.Checked;//Revision 1
			Post;
            }
            // Si es un alta o se modific� la descripci�n, la valido
            ValidarDesc := (DBList1.Estado = Alta) or
                            (Trim(txt_Descripcion.Text) <> FieldByName('Descripcion').AsString);

            if ValidarDesc and MotivosAnulacion.Locate('Descripcion', Trim(txt_Descripcion.Text), [loCaseInsensitive]) then begin
                // Si valido la descripci�n y la misma est� en el listado, devuelve error
                MsgBox(MSG_DESCRIPCION_EXISTE, MSG_ACTUALIZAR_CAPTION, MB_ICONEXCLAMATION+MB_OK);
                Cancel;
            end else begin
                // Si no, guarda el registro
                if DBList1.Estado = Alta then Append
			    else Edit;

                FieldByName('Descripcion').AsString := Trim(txt_Descripcion.text);
                FieldByName('IncluirEnAnulacionRecibo').AsBoolean := ChkIncluirRecibo.Checked;
                FieldByName('IncluirEnAnulacionChequeProtestado').AsBoolean := ChkIncluirCheque.Checked;
                Post;
            end;
            // FIN : 20160613 MGO
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
    GroupB.Enabled := False;
  	DBList1.Estado := Normal;
	DBList1.Enabled := True;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	DBList1.SetFocus;
    txt_CodigoMotivoAnulacion.Enabled:= True;
	//txt_CodigoMotivoAnulacion.Color	:= clWindow;    // 20160613 MGO
	Screen.Cursor := crDefault;
end;

procedure TFormABMMotivosCancelacionRecibos.BtnSalirClick(Sender: TObject);
begin
     Close;
end;

end.
