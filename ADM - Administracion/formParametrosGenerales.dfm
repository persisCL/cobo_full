object frmParametrosGenerales: TfrmParametrosGenerales
  Left = 245
  Top = 177
  Width = 600
  Height = 481
  Caption = 'Configuraci'#243'n General Solicitud'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 249
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'204'#0'Nombre                                                     '
      #0'137'#0'Valor                                   '
      #0'119'#0'Tipo                              '
      
        #0'225'#0'Valor Default                                              ' +
        '      ')
    HScrollBar = True
    RefreshTime = 100
    Table = QryParametrosSolicitud
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 282
    Width = 592
    Height = 133
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    DesignSize = (
      592
      133)
    object Label1: TLabel
      Left = 19
      Top = 43
      Width = 59
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 19
      Top = 13
      Width = 43
      Height = 13
      Caption = 'Nombre: '
    end
    object Label2: TLabel
      Left = 19
      Top = 105
      Width = 27
      Height = 13
      Caption = '&Valor:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 19
      Top = 74
      Width = 62
      Height = 13
      Caption = '&Valor default:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object eDescripcion: TEdit
      Left = 109
      Top = 37
      Width = 464
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = clBtnFace
      Enabled = False
      MaxLength = 255
      TabOrder = 0
    end
    object nbValores: TNotebook
      Left = 99
      Top = 66
      Width = 481
      Height = 60
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 1
      object TPage
        Left = 0
        Top = 0
        Caption = 'Texto'
        DesignSize = (
          481
          60)
        object eValor: TEdit
          Left = 10
          Top = 35
          Width = 464
          Height = 21
          Hint = 'Ingrese el valor del par'#225'metro'
          Anchors = [akLeft, akTop, akRight]
          MaxLength = 255
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object eValorDefault: TEdit
          Left = 10
          Top = 4
          Width = 464
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Color = clBtnFace
          Enabled = False
          TabOrder = 1
        end
      end
      object TPage
        Left = 0
        Top = 0
        Caption = 'Entero'
        object neValor: TNumericEdit
          Left = 10
          Top = 35
          Width = 121
          Height = 21
          TabOrder = 0
          Decimals = 0
        end
        object neValorDefault: TNumericEdit
          Left = 10
          Top = 4
          Width = 121
          Height = 21
          Color = clBtnFace
          Enabled = False
          TabOrder = 1
          Decimals = 0
        end
      end
      object TPage
        Left = 0
        Top = 0
        Caption = 'Numerico'
        object neNumericoValorDefault: TNumericEdit
          Left = 10
          Top = 4
          Width = 121
          Height = 21
          Color = clBtnFace
          Enabled = False
          TabOrder = 0
          Decimals = 0
        end
        object neNumericoValor: TNumericEdit
          Left = 10
          Top = 35
          Width = 121
          Height = 21
          TabOrder = 1
          Decimals = 2
        end
      end
      object TPage
        Left = 0
        Top = 0
        Caption = 'Fecha'
        object deValor: TDateEdit
          Left = 10
          Top = 35
          Width = 103
          Height = 21
          AutoSelect = False
          TabOrder = 0
          Date = -693594
        end
        object deValorDefault: TDateEdit
          Left = 10
          Top = 4
          Width = 103
          Height = 21
          AutoSelect = False
          Color = clBtnFace
          Enabled = False
          TabOrder = 1
          Date = -693594
        end
      end
    end
    object eNombre: TEdit
      Left = 109
      Top = 8
      Width = 464
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = clBtnFace
      Enabled = False
      MaxLength = 255
      TabOrder = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 415
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 224
      Top = 0
      Width = 368
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 278
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 192
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 1
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 279
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 2
          OnClick = BtnCancelarClick
        end
        object btnDefault: TDPSButton
          Left = 106
          Top = 7
          Width = 79
          Height = 26
          Hint = 'Asignar Valor default'
          Caption = '&Default'
          TabOrder = 0
          OnClick = btnDefaultClick
        end
      end
    end
  end
  object QryParametrosSolicitud: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'ClaseParametro'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT * FROM ParametrosGenerales WHERE ClaseParametro = :ClaseP' +
        'arametro')
    Left = 253
    Top = 92
  end
  object ActualizarParametroGeneral: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarParametroGeneral'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ValorParametro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 288
    Top = 88
  end
end
