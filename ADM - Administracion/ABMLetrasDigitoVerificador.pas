unit ABMLetrasDigitoVerificador;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls,RStrings, Validate, DateEdit;

type
  TFormABMLetrasDigitoVerificador = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    ObtenerLetrasDigitoVerificador: TADOTable;
    Notebook: TNotebook;
    txt_Letra: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    FechaActualizacion: TDateEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    ObtenerLetrasDigitoVerificadorLetra: TStringField;
    ObtenerLetrasDigitoVerificadorDigitoVerificador: TStringField;
    txt_DigitoVerificador: TNumericEdit;
    ObtenerLetrasDigitoVerificadorFechaActualizacion: TDateTimeField;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure txt_DigitoVerificadorKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

  
resourcestring
    FLD_LETRAS_DIGITO_VERIFICADOR = 'Letra Digito Verificador';

var
  FormABMLetrasDigitoVerificador: TFormABMLetrasDigitoVerificador;

implementation

{$R *.DFM}

procedure TFormABMLetrasDigitoVerificador.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
end;

function TFormABMLetrasDigitoVerificador.Inicializa: boolean;
begin
    CenterForm(Self);
	if not OpenTables([ObtenerLetrasDigitoVerificador]) then
		Result := False
	else begin
    	Notebook.PageIndex := 0;
		Result := True;
        VolverCampos;
		DbList1.Reload;
	end;
end;

procedure TFormABMLetrasDigitoVerificador.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormABMLetrasDigitoVerificador.DBList1Click(Sender: TObject);
begin
	with ObtenerLetrasDigitoVerificador do begin
      	Txt_Letra.Text := FieldByName('Letra').AsString;
        Txt_DigitoVerificador.Value := STRTOINT(FieldByName('DigitoVerificador').AsString);
        FechaActualizacion.Date := FieldByName('FechaActualizacion').AsDateTime;
	end;
end;

procedure TFormABMLetrasDigitoVerificador.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('Letra').AsString);
        TextOut(Cols[1], Rect.Top, Tabla.FieldByName('DigitoVerificador').AsString);
        //FIRMA			: 201160613 CFU
        //DESCRIPCON	: Se formatea la salida de la fecha del registro para mostrar en formato 'dd-mm-yyyy HH:nn:ss'
        //TextOut(Cols[2], Rect.Top, Tabla.FieldByName('FechaActualizacion').AsString);
        TextOut(Cols[2], Rect.Top, FormatDateTime('dd-mm-yyyy', Tabla.FieldByName('FechaActualizacion').AsDateTime));
	end;
end;

procedure TFormABMLetrasDigitoVerificador.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado     := modi;
end;

procedure TFormABMLetrasDigitoVerificador.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCampos;
    dblist1.Estado     := Alta;
end;

procedure TFormABMLetrasDigitoVerificador.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormABMLetrasDigitoVerificador.Limpiar_Campos;
begin
	Txt_Letra.Clear;
	Txt_DigitoVerificador.Clear;
end;

procedure TFormABMLetrasDigitoVerificador.AbmToolbar1Close(Sender: TObject);
begin
    close;
end;

procedure TFormABMLetrasDigitoVerificador.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(format(MSG_QUESTION_BAJA_ESTA,[FLD_LETRAS_DIGITO_VERIFICADOR]), format(MSG_CAPTION_ELIMINAR,[FLD_LETRAS_DIGITO_VERIFICADOR]), MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			ObtenerLetrasDigitoVerificador.Delete;
		Except
			On E: Exception do begin
				ObtenerLetrasDigitoVerificador.Cancel;
				MsgBoxErr(format(MSG_ERROR_DAR_BAJA_ESTA,[FLD_LETRAS_DIGITO_VERIFICADOR]), e.message, format(MSG_CAPTION_GESTION,[FLD_LETRAS_DIGITO_VERIFICADOR]), MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	VolverCampos;
	Screen.Cursor      := crDefault;
end;

procedure TFormABMLetrasDigitoVerificador.BtnAceptarClick(Sender: TObject);
    function CombinacionCargada(Combinacion:String;Posicion:Integer=-1):Boolean;
    var
        pos:TBookmark;
    begin
        result:=False;
        with ObtenerLetrasDigitoVerificador do begin
            pos:=GetBookmark;
            First;
            while not(eof) and (result=False) do begin
                if (Posicion<>RecNo) and (uppercase(FieldByName('Letra').AsString)=uppercase(Combinacion)) then
                        Result:=True;
                next;
            end;
            GotoBookmark(pos);
        end;
    end;

begin
    Txt_Letra.Text:=uppercase(Txt_Letra.Text);
    if not ValidateControls([Txt_Letra,
                    txt_DigitoVerificador],
                [trim(Txt_Letra.Text) <> '',
                //txt_DigitoVerificador.value>=0],                                          // SS_1147_CQU_20140508
                ((txt_DigitoVerificador.value>=0) AND (txt_DigitoVerificador.Text <> ''))], // SS_1147_CQU_20140508
                format(MSG_CAPTION_GESTION,[FLD_LETRAS_DIGITO_VERIFICADOR]),
                [format(MSG_VALIDAR_DEBE_LA,[FLD_LETRAS_DIGITO_VERIFICADOR]),
                format(MSG_VALIDAR_DEBE_EL,[FLD_DIGITO]),
                format(MSG_VALIDAR_DEBE_EL,[FLD_DIGITO])]) then exit;

    if CombinacionCargada(trim(Txt_Letra.Text),iif(DbList1.Estado = Alta,-1,ObtenerLetrasDigitoVerificador.RecNo)) then begin
        MsgBoxBalloon(format(MSG_ERROR_DUPLICADO_EL,[FLD_LETRAS_DIGITO_VERIFICADOR]),format(MSG_CAPTION_GESTION,[FLD_LETRAS_DIGITO_VERIFICADOR]),MB_ICONSTOP,Txt_Letra);
        Exit;
    end;

 	Screen.Cursor := crHourGlass;
	With ObtenerLetrasDigitoVerificador do begin
		Try
			if DbList1.Estado = Alta then Append
			else Edit;

      	    FieldByName('Letra').Value:=Txt_Letra.Text;
            FieldByName('DigitoVerificador').Value:= StrToInt(txt_DigitoVerificador.text);
            FieldByName('FechaActualizacion').Value := FechaActualizacion.Date;
			Post;
		except
			//On E: EDataBaseError do begin // SS_1147_CQU_20140508
            On E: Exception do begin        // SS_1147_CQU_20140508
				Cancel;
				MsgBoxErr(format(MSG_ERROR_ACTUALIZAR,[FLD_LETRAS_DIGITO_VERIFICADOR]), E.message, format(MSG_CAPTION_GESTION,[FLD_LETRAS_DIGITO_VERIFICADOR]), MB_ICONSTOP);
                Screen.Cursor	:= crDefault;
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFormABMLetrasDigitoVerificador.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormABMLetrasDigitoVerificador.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormABMLetrasDigitoVerificador.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormABMLetrasDigitoVerificador.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    Txt_Letra.SetFocus;
end;

procedure TFormABMLetrasDigitoVerificador.txt_DigitoVerificadorKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9', Char(VK_BACK)]) then
        Key := #0;
end;

end.
