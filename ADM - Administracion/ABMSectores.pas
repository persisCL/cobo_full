unit ABMSectores;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls;

type
  TFormSectores = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    txt_CodigoSector: TNumericEdit;
    cb_EjesViales: TComboBox;
    Qry_EjesViales: TADOQuery;
    Label2: TLabel;
    Notebook: TNotebook;
    Sectores: TADOTable;
    cbConcesionarias: TComboBox;
    Label3: TLabel;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure cbConcesionariasChange(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
    procedure CargarEjesDelConcesionario(codigoConcesionario: integer);
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormSectores: TFormSectores;

implementation
resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar este Sector?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Sector porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Sector';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Sector.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Sector';
{$R *.DFM}

function TFormSectores.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Result := False;
    // Cargamos las concesionarias
    CargarConcesionarias(DMConnections.BaseCAC, cbConcesionarias, 0);
    if not Opentables([Sectores]) then Exit;
    Notebook.PageIndex := 0;
	DbList1.Reload;
   	Result := True;
end;

procedure TFormSectores.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormSectores.DBList1Click(Sender: TObject);
var
	i: integer;
begin
	with (Sender AS TDbList).Table do begin
        cb_EjesViales.Clear;
        //cb_EjesViales.ItemIndex := -1;
        cb_EjesViales.Items.Add(Trim(QueryGetValue(Sectores.Connection,
            'SELECT Descripcion FROM EjesViales WHERE CodigoEjeVial = '
            + FieldByName('CodigoEjeVial').AsString)));
        cb_EjesViales.ItemIndex := 0;

        // Buscar la concesionaria
        for i := 0 to cbConcesionarias.Items.Count - 1 do begin
        	if Ival(strRight(cbConcesionarias.Items[i], 20)) =  FieldByName('CodigoConcesionaria').AsInteger then begin
            	cbConcesionarias.ItemIndex := i;
            	Break;
            end;
        end;
		txt_CodigoSector.Value := FieldByName('Sector').AsInteger;
		txt_Descripcion.text   := FieldByName('Descripcion').AsString;
	end;
end;

procedure TFormSectores.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top,
          Trim(QueryGetValue(Sectores.Connection,
            'SELECT Descripcion FROM Concesionarias WHERE CodigoConcesionaria = '
            + FieldByName('CodigoConcesionaria').AsString)));
		TextOut(Cols[1], Rect.Top, Tabla.FieldbyName('Sector').AsString);
		TextOut(Cols[2], Rect.Top, Tabla.FieldbyName('Descripcion').AsString);
		TextOut(Cols[3], Rect.Top,
          Trim(QueryGetValue(Sectores.Connection,
            'SELECT Descripcion FROM EjesViales WHERE CodigoEjeVial = '
            + FieldByName('CodigoEjeVial').AsString)));
	end;
end;

procedure TFormSectores.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    dblist1.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;
    CargarEjesDelConcesionario(Ival(StrRight(cbConcesionarias.Text, 20)));
	cb_EjesViales.SetFocus;
end;

procedure TFormSectores.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormSectores.Limpiar_Campos();
begin
	txt_codigoSector.Clear;
	txt_Descripcion.Clear;
    cb_EjesViales.ItemIndex := 0;
end;

procedure TFormSectores.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormSectores.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			(Sender AS TDbList).Table.Delete;
		except
        	on E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormSectores.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    //Marco el primer concesionario y traigo los sectores del mismo
    cbConcesionarias.ItemIndex := 0;
    CargarEjesDelConcesionario(Ival(StrRight(cbConcesionarias.Text, 20)));
	cbConcesionarias.SetFocus;
end;

procedure TFormSectores.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
	With DbList1.Table do begin
		Try
			if DbList1.Estado = Alta then Append
			else Edit;
   			FieldByName('CodigoConcesionaria').AsInteger	:= Ival(StrRight(cbConcesionarias.Text, 20));
			FieldByName('CodigoEjeVial').AsInteger	:= Ival(Copy(cb_EjesViales.Text, 201, 10));
			FieldByName('Sector').AsInteger			:= Trunc(txt_codigoSector.value);
            if Trim(txt_Descripcion.text) = '' then
            	FieldByName('Descripcion').Clear
            else
				FieldByName('Descripcion').AsString	 := Trim(txt_Descripcion.text);
			Post;
		except
			On E: Exception do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormSectores.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormSectores.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormSectores.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormSectores.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

procedure TFormSectores.cbConcesionariasChange(Sender: TObject);
begin
    if Sender = ActiveControl then begin
        CargarEjesDelConcesionario(Ival(StrRight(cbConcesionarias.Text, 20)));
    end;
end;

procedure TFormSectores.CargarEjesDelConcesionario(
  codigoConcesionario: integer);
var
    trajoAlguno: boolean;
begin
    trajoAlguno := false;
    Qry_EjesViales.Parameters.ParamByName('CodigoConcesionaria').Value := Ival(StrRight(cbConcesionarias.Text, 20));
    if not OpenTables([Qry_EjesViales]) then exit;
    // Cargamos en el Combo Todos los Ejes Viales.
    cb_EjesViales.Clear;
    While not Qry_EjesViales.Eof do begin
        trajoAlguno := true;
        cb_EjesViales.Items.Add(
        PadR(Qry_EjesViales.FieldByName('Descripcion').AsString, 200, ' ') +
        Istr(Qry_EjesViales.FieldByName('CodigoEjeVial').AsInteger, 10));
        Qry_EjesViales.Next;
    end;
    if trajoAlguno then cb_EjesViales.ItemIndex := 0; //marco el primero
    Qry_EjesViales.Close;
end;

end.
