unit ABMConceptosMovimientosLibrosContables;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls,RStrings, Validate, DateEdit, VariantComboBox;

type
  TFormABMConceptosMovimientosLibrosContables = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    CbColumnas: TVariantComboBox;
    CbLibroContable: TVariantComboBox;
    Label2: TLabel;
    QryLibrosContables: TADOQuery;
    QryConceptosMovimientos: TADOQuery;
    ConceptosMovimientoLibrosContablesIdLibroContable: TIntegerField;
    ConceptosMovimientoLibrosContablesIdColumnaLibroContable: TIntegerField;
    ConceptosMovimientoLibrosContablesCodigoConcepto: TWordField;
    ConceptosMovimientoLibrosContablesUsuarioModificacion: TStringField;
    ConceptosMovimientoLibrosContablesFechaModificacion: TDateTimeField;
    CbConceptos: TVariantComboBox;
    Label3: TLabel;
    QryColumnas: TADOQuery;
    ConceptosMovimientoLibrosContables: TADOTable;
    ConceptosMovimientoLibrosContablesUsuarioCreacion: TStringField;
    ConceptosMovimientoLibrosContablesFechaCreacion: TDateTimeField;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure CbLibroContableChange(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
    procedure BuscarTextoenCombo(Combo: TVariantComboBox; Valor: Variant);
    procedure LlenarDatoscombo(Combo: TVariantComboBox; Datos: TAdoQuery; CampoCaption,CampoValor: String);
  public
    { Public declarations }
    function Inicializa: boolean;

  end;


resourcestring
    FLD_TIPO_CONCEPTO = 'Concepto Movimiento';
    FLD_TIPO_CONCEPTO_RELACION = 'Relaci�n Concepto Movimiento - Libro Contable';

var
  FormABMConceptosMovimientosLibrosContables: TFormABMConceptosMovimientosLibrosContables;

implementation

{$R *.DFM}

procedure TFormABMConceptosMovimientosLibrosContables.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
end;

function TFormABMConceptosMovimientosLibrosContables.Inicializa: boolean;
begin
    CenterForm(Self);
	if not OpenTables([ConceptosMovimientoLibrosContables]) then
		Result := False
	else begin
    	Notebook.PageIndex := 0;
		Result := True;
        VolverCampos;
		DbList1.Reload;
        LlenarDatoscombo(CbLibroContable, QryLibrosContables, 'Descripcion','IdLibroContable');
        LlenarDatoscombo(CbConceptos, QryConceptosMovimientos, 'Descripcion','CodigoConcepto');
        CbColumnas.Items.Clear;
    end;
end;

procedure TFormABMConceptosMovimientosLibrosContables.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormABMConceptosMovimientosLibrosContables.DBList1Click(Sender: TObject);
begin
    BuscarTextoenCombo(CbConceptos, ConceptosMovimientoLibrosContables.FieldByName('CodigoConcepto').AsVariant);
    BuscarTextoenCombo(CbLibrocontable, ConceptosMovimientoLibrosContables.FieldByName('IdLibroContable').AsVariant);
    CbColumnas.Items.Clear;
    QryColumnas.Parameters.ParamByName('CodigoLibro').Value:=CbLibroContable.Value;
    LlenarDatoscombo(CbColumnas, QryColumnas, 'Descripcion','IdColumnaLibroContable');
    BuscarTextoenCombo(CbColumnas, ConceptosMovimientoLibrosContables.FieldByName('IdColumnaLibroContable').AsVariant);
end;

procedure TFormABMConceptosMovimientosLibrosContables.DBList1Delete(
  Sender: TObject);
resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar la Relaci�n Concepto - Libro Contable?';
    MSG_DELETE_ERROR		= 'No se puede eliminar la relaci�n Concepto - Libro Contable porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Relaci�n Concepto - Libro Contable';
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) =
		IDYES then begin
		try
			ConceptosMovimientoLibrosContables.Delete;
		Except
			On E: EDataBaseError do begin
				ConceptosMovimientoLibrosContables.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DBList1.Reload;
	end;
	DBList1.Estado       := Normal;
	DBList1.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormABMConceptosMovimientosLibrosContables.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        TextOut(Cols[0], Rect.Top, Tabla.FieldByName('IdLibroContable').AsString);
        TextOut(Cols[1], Rect.Top, Tabla.FieldByName('IdColumnaLibroContable').AsString);
        TextOut(Cols[2], Rect.Top, Tabla.FieldByName('CodigoConcepto').AsString);
	end;
end;

procedure TFormABMConceptosMovimientosLibrosContables.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado     := modi;
end;

procedure TFormABMConceptosMovimientosLibrosContables.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCampos;
    dblist1.Estado     := Alta;
end;

procedure TFormABMConceptosMovimientosLibrosContables.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormABMConceptosMovimientosLibrosContables.Limpiar_Campos;
begin
    CbConceptos.ItemIndex := -1;
    CbLibroContable.ItemIndex := -1;
    CbColumnas.ItemIndex := -1;
end;

procedure TFormABMConceptosMovimientosLibrosContables.LlenarDatoscombo(
  Combo: TVariantComboBox; Datos: TAdoQuery; CampoCaption,CampoValor: String);
var
    i, cont:integer;
begin
    Datos.Open;
    cont := Datos.RecordCount;
    Combo.Clear;
    Datos.First;
    for i := 0 to cont - 1 do begin
        Combo.Items.Add(Datos.FieldByName(CampoCaption).AsString, Datos.FieldByName(CampoValor).AsVariant);
        Datos.Next;
    end;
    Datos.Close;
end;

procedure TFormABMConceptosMovimientosLibrosContables.AbmToolbar1Close(Sender: TObject);
begin
    close;
end;

procedure TFormABMConceptosMovimientosLibrosContables.BtnAceptarClick(Sender: TObject);
    function ConceptoCargado(Concepto, Libro:Integer;Posicion:Integer=-1):Boolean;
    var
        pos:TBookmark;
    begin
        result:=False;
        with ConceptosMovimientoLibrosContables do begin
            pos:=GetBookmark;
            First;
            while not(eof) and (result=False) do begin
                if ((Posicion<>RecNo) and (FieldByName('CodigoConcepto').AsInteger = Concepto)
                and (FieldByName('IDLibroContable').AsInteger = Libro)) then
                        Result:=True;
                Next;
            end;
            GotoBookmark(pos);
        end;
    end;

begin
    if trim(CbLibroContable.Text) = EmptyStr then exit;
    if trim(CbColumnas.Text) = EmptyStr then exit;
    if trim(CbConceptos.Text) = EmptyStr then exit;

    if ConceptoCargado(CbConceptos.Value,CbLibroContable.Value,iif(DbList1.Estado = Alta,-1,ConceptosMovimientoLibrosContables.RecNo)) then begin
        MsgBoxBalloon(format(MSG_ERROR_DUPLICADO_EL,[FLD_TIPO_CONCEPTO]),format(MSG_CAPTION_GESTION,[FLD_TIPO_CONCEPTO_RELACION]),MB_ICONSTOP,CbConceptos);
        Exit;
    end;

 	Screen.Cursor := crHourGlass;
	With ConceptosMovimientoLibrosContables do begin
		Try
			if DbList1.Estado = Alta then Append
			else Edit;

            FieldByName('IdLibroContable').Value:= CbLibroContable.Value;
            FieldByName('IdColumnaLibroContable').Value:= CbColumnas.Value;
            FieldByName('CodigoConcepto').Value:= CbConceptos.Value;
            FieldByName('UsuarioModificacion').Value:= UsuarioSistema;
            FieldByName('FechaModificacion').Value:= NowBase(DMConnections.BaseCAC);
            if DbList1.Estado = Alta then begin
                FieldByName('UsuarioCreacion').Value:= UsuarioSistema;
                FieldByName('FechaCreacion').Value:= NowBase(DMConnections.BaseCAC);
            end;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(format(MSG_ERROR_ACTUALIZAR,[FLD_TIPO_CONCEPTO_RELACION]), E.message, format(MSG_CAPTION_GESTION,[FLD_TIPO_CONCEPTO_RELACION]), MB_ICONSTOP);
                Screen.Cursor	:= crDefault;
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFormABMConceptosMovimientosLibrosContables.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormABMConceptosMovimientosLibrosContables.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormABMConceptosMovimientosLibrosContables.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormABMConceptosMovimientosLibrosContables.BuscarTextoenCombo(Combo: TVariantComboBox; Valor: Variant);
var
    i:integer;
begin
    for i := 0 to Combo.Items.Count - 1 do begin
        if Combo.Items.Items[i].Value = Valor then begin
            Combo.ItemIndex := i;
            Break;
        end;
    end;
end;


procedure TFormABMConceptosMovimientosLibrosContables.CbLibroContableChange(
  Sender: TObject);
begin
    CbColumnas.Items.Clear;
    QryColumnas.Parameters.ParamByName('CodigoLibro').Value:=CbLibroContable.Value;
    LlenarDatoscombo(CbColumnas, QryColumnas, 'Descripcion','IdColumnaLibroContable');
end;

procedure TFormABMConceptosMovimientosLibrosContables.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    CbLibroContable.SetFocus;
end;

end.
