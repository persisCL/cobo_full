object FormLugaresVenta: TFormLugaresVenta
  Left = 107
  Top = 152
  Width = 812
  Height = 595
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Administraci'#243'n de Lugares de Venta'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlInferior: TPanel
    Left = 0
    Top = 523
    Width = 804
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Label5: TLabel
      Left = 39
      Top = 13
      Width = 48
      Height = 13
      Caption = 'Eliminado.'
    end
    object Notebook: TNotebook
      Left = 607
      Top = 0
      Width = 197
      Height = 38
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 112
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 27
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 107
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
    object Panel3: TPanel
      Left = 20
      Top = 12
      Width = 12
      Height = 14
      BevelOuter = bvNone
      Color = clRed
      TabOrder = 1
    end
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 804
    Height = 35
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object alLugaresDeVenta: TAbmList
    Left = 0
    Top = 35
    Width = 804
    Height = 237
    TabStop = True
    TabOrder = 2
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = 12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      
        #0'231'#0'Nombre de Fantas'#237'a                                         ' +
        ' '
      
        #0'218'#0'Raz'#243'n Social                                               ' +
        '  '
      #0'99'#0'Operador Log'#237'stico')
    HScrollBar = True
    Table = tblLugaresDeVenta
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = alLugaresDeVentaClick
    OnDrawItem = alLugaresDeVentaDrawItem
    OnRefresh = alLugaresDeVentaRefresh
    OnInsert = alLugaresDeVentaInsert
    OnDelete = alLugaresDeVentaDelete
    OnEdit = alLugaresDeVentaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object pcDatosPersonal: TPageControl
    Left = 0
    Top = 272
    Width = 804
    Height = 251
    ActivePage = tsCliente
    Align = alBottom
    TabIndex = 0
    TabOrder = 3
    object tsCliente: TTabSheet
      Caption = 'Identificaci'#243'n'
      object pnlDatosPersonal: TPanel
        Left = 0
        Top = 0
        Width = 796
        Height = 223
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object lblRazonSocial: TLabel
          Left = 9
          Top = 68
          Width = 66
          Height = 13
          Caption = 'Raz'#243'n Social:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lblNombreDeFantasia: TLabel
          Left = 9
          Top = 17
          Width = 120
          Height = 13
          Caption = 'Nombre de Fantas'#237'a:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TipoNumeroDocumento: TLabel
          Left = 9
          Top = 93
          Width = 75
          Height = 13
          Caption = 'Tipo/Nro. Doc.:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lblContacto: TLabel
          Left = 421
          Top = 17
          Width = 46
          Height = 13
          Caption = 'Contacto:'
        end
        object Label11: TLabel
          Left = 421
          Top = 43
          Width = 82
          Height = 13
          Caption = 'Situaci'#243'n de IVA:'
        end
        object Label1: TLabel
          Left = 8
          Top = 43
          Width = 94
          Height = 13
          Caption = 'Operador Log'#237'stico:'
        end
        object txtRazonSocial: TEdit
          Left = 132
          Top = 63
          Width = 278
          Height = 21
          MaxLength = 30
          TabOrder = 2
        end
        object txtNombreDeFantasia: TEdit
          Left = 132
          Top = 13
          Width = 278
          Height = 21
          Color = 16444382
          MaxLength = 30
          TabOrder = 0
        end
        object mcTipoNumeroDocumento: TMaskCombo
          Left = 132
          Top = 88
          Width = 277
          Height = 21
          OnChange = mcTipoNumeroDocumentoChange
          Items = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Color = clWindow
          ComboWidth = 145
          ItemIndex = -1
          OmitCharacterRequired = False
          TabOrder = 3
        end
        object txtContacto: TEdit
          Left = 524
          Top = 13
          Width = 278
          Height = 21
          MaxLength = 30
          TabOrder = 4
        end
        object cbSituacionIVA: TComboBox
          Left = 524
          Top = 38
          Width = 278
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 5
        end
        object bteOperadoresLogisticos: TBuscaTabEdit
          Left = 132
          Top = 38
          Width = 278
          Height = 21
          Color = 16444382
          Enabled = True
          ReadOnly = True
          TabOrder = 1
          OnEnter = IrAlInicio
          OnExit = IrAlInicio
          Decimals = 0
          EditorStyle = bteTextEdit
          BuscaTabla = btOperadoresLogisticos
        end
        object chkActivo: TCheckBox
          Left = 421
          Top = 68
          Width = 136
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Activo:'
          TabOrder = 6
        end
      end
    end
    object tsDomicilio: TTabSheet
      Caption = 'Domicilio'
      ImageIndex = 1
      object pnlDomicilio: TPanel
        Left = 0
        Top = 0
        Width = 796
        Height = 223
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          796
          223)
        object dblDomicilios: TDBListEx
          Left = 16
          Top = 12
          Width = 762
          Height = 173
          Anchors = [akLeft, akTop, akRight]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 95
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriTipoOrigenDato'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 200
              Header.Caption = 'Ubicaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Arial'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriCiudad'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 200
              Header.Caption = 'Direcci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Arial'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DireccionCompleta'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 200
              Header.Caption = 'Detalle'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Detalle'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'Domicilio Entrega'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
            end>
          DataSource = dsDomicilios
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnDrawText = dblDomiciliosDrawText
        end
        object btnAgregarDomicilio: TDPSButton
          Left = 555
          Top = 192
          Anchors = [akTop, akRight]
          Caption = '&Agregar'
          TabOrder = 1
          OnClick = Agregar
        end
        object btnEditarDomicilio: TDPSButton
          Left = 630
          Top = 192
          Anchors = [akTop, akRight]
          Caption = '&Editar'
          TabOrder = 2
          OnClick = btnEditarDomicilioClick
        end
        object btnEliminarDomicilio: TDPSButton
          Left = 705
          Top = 192
          Anchors = [akTop, akRight]
          Caption = 'E&liminar'
          TabOrder = 3
          OnClick = btnEliminarDomicilioClick
        end
      end
    end
    object tsContacto: TTabSheet
      Caption = 'Contacto'
      ImageIndex = 2
      object pnlMediosContacto: TPanel
        Left = 0
        Top = 0
        Width = 796
        Height = 223
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          796
          223)
        object dblMediosComunicacion: TDBListEx
          Left = 16
          Top = 12
          Width = 762
          Height = 173
          Anchors = [akLeft, akTop, akRight]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriTipoOrigenDato'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Medio de Contacto'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriMedioContacto'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 255
              Header.Caption = 'Detalle'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Detalle'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 80
              Header.Caption = 'Hora desde'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'HorarioDesde'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 80
              Header.Caption = 'Hora hasta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'HorarioHasta'
            end>
          DataSource = dsMediosComunicacion
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnDrawText = dblMediosComunicacionDrawText
        end
        object btnAgregarMedioComunicacion: TDPSButton
          Left = 555
          Top = 192
          Anchors = [akTop, akRight]
          Caption = '&Agregar'
          TabOrder = 1
          OnClick = btnAgregarMedioComunicacionClick
        end
        object btnEditarMedioComunicacion: TDPSButton
          Left = 630
          Top = 192
          Anchors = [akTop, akRight]
          Caption = '&Editar'
          TabOrder = 2
          OnClick = btnEditarMedioComunicacionClick
        end
        object btnEliminarMedioComunicacion: TDPSButton
          Left = 705
          Top = 192
          Anchors = [akTop, akRight]
          Caption = 'E&liminar'
          TabOrder = 3
          OnClick = btnEliminarMedioComunicacionClick
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 170
    Top = 0
    Width = 71
    Height = 35
    BevelOuter = bvNone
    TabOrder = 4
    object btnPuntosVenta: TSpeedButton
      Left = 4
      Top = 5
      Width = 26
      Height = 25
      Hint = 'Mantenimiento de Puntos de Venta'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000333300
        0000377777F3337777770FFFF099990FFFF07FFFF7FFFF7FFFF7000000999900
        00007777777777777777307703399330770337FF7F37F337FF7F300003399330
        000337777337F337777333333339933333333FFFFFF7F33FFFFF000000399300
        0000777777F7F37777770FFFF099990FFFF07FFFF7F7FF7FFFF7000000999900
        00007777777777777777307703399330770337FF7F37F337FF7F300003399330
        0003377773F7FFF77773333330000003333333333777777F3333333330FFFF03
        3333333337FFFF7F333333333000000333333333377777733333333333077033
        33333333337FF7F3333333333300003333333333337777333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnPuntosVentaClick
    end
  end
  object tblLugaresDeVenta: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'VW_LugaresDeVenta'
    Left = 284
    Top = 176
  end
  object ObtenerMediosComunicacionLugarDeVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMediosComunicacionLugarDeVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 84
    Top = 72
  end
  object ActualizarDatosLugarDeVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosLugarDeVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@RazonSocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@NombreDeFantasia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Contacto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoSituacionIVA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoDomicilioEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperadorLogistico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 256
    Top = 116
  end
  object QueryTemp: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 512
    Top = 148
  end
  object dsMediosComunicacion: TDataSource
    DataSet = cdsMediosComunicacion
    Left = 112
    Top = 72
  end
  object cdsMediosComunicacion: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoTipoOrigenDato'
        DataType = ftSmallint
      end
      item
        Name = 'CodigoMedioContacto'
        DataType = ftSmallint
      end
      item
        Name = 'DescriMedioContacto'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TipoMedio'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DescriTipoOrigenDato'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Valor'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'CodigoDomicilio'
        DataType = ftInteger
      end
      item
        Name = 'DescriDomicilio'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Detalle'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'HorarioDesde'
        DataType = ftDateTime
      end
      item
        Name = 'HorarioHasta'
        DataType = ftDateTime
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Activo'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoDomicilioRecNo'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'cdsMediosComunicacionIndex1'
        Fields = 'CodigoMedioContacto; CodigoTipoOrigenDato'
        Options = [ixUnique]
      end>
    IndexName = 'cdsMediosComunicacionIndex1'
    Params = <>
    ProviderName = 'dspMediosComunicacion'
    StoreDefs = True
    Left = 140
    Top = 72
  end
  object dspMediosComunicacion: TDataSetProvider
    DataSet = ObtenerMediosComunicacionLugarDeVenta
    Constraints = True
    Left = 168
    Top = 72
  end
  object EliminarLugarDeVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarLugarDeVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 284
    Top = 116
  end
  object qryOperadoresLogisticos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      '0 AS CodigoOperadorLogistico,'
      #39'(Todos)'#39' AS RazonSocial'
      ''
      'UNION'
      ''
      'SELECT '
      'CodigoPersona AS CodigoOperadorLogistico,'
      'Apellido AS RazonSocial'
      'FROM'
      'VW_OperadoresLogisticos')
    Left = 408
    Top = 164
  end
  object btOperadoresLogisticos: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = qryOperadoresLogisticos
    OnProcess = btOperadoresLogisticosProcess
    OnSelect = btOperadoresLogisticosSelect
    Left = 436
    Top = 164
  end
  object ObtenerDomiciliosLugarDeVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomiciliosLugarDeVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 80
    Top = 152
  end
  object dsDomicilios: TDataSource
    DataSet = cdsDomicilios
    Left = 108
    Top = 152
  end
  object cdsDomicilios: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoDomicilio'
        DataType = ftInteger
      end
      item
        Name = 'CodigoAlmacen'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTipoOrigenDato'
        DataType = ftSmallint
      end
      item
        Name = 'DescriTipoOrigenDato'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoCalle'
        DataType = ftInteger
      end
      item
        Name = 'Numero'
        DataType = ftInteger
      end
      item
        Name = 'Piso'
        DataType = ftSmallint
      end
      item
        Name = 'Dpto'
        Attributes = [faFixed]
        DataType = ftString
        Size = 4
      end
      item
        Name = 'CalleDesnormalizada'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Detalle'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoPostal'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoTipoEdificacion'
        DataType = ftSmallint
      end
      item
        Name = 'DescriTipoEdificacion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 15
      end
      item
        Name = 'DomicilioEntrega'
        DataType = ftBoolean
      end
      item
        Name = 'CalleRelacionadaUno'
        DataType = ftInteger
      end
      item
        Name = 'NumeroCalleRelacionadaUno'
        DataType = ftInteger
      end
      item
        Name = 'CalleRelacionadaDos'
        DataType = ftInteger
      end
      item
        Name = 'NumeroCalleRelacionadaDos'
        DataType = ftInteger
      end
      item
        Name = 'Normalizado'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTipoCalle'
        DataType = ftSmallint
      end
      item
        Name = 'DescriCalle'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoPais'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriPais'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoRegion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriRegion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoComuna'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriComuna'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoCiudad'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriCiudad'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DireccionCompleta'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'DomicilioCompleto'
        DataType = ftString
        Size = 200
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'dspDomicilios'
    StoreDefs = True
    Left = 136
    Top = 152
  end
  object dspDomicilios: TDataSetProvider
    DataSet = ObtenerDomiciliosLugarDeVenta
    Constraints = True
    Left = 168
    Top = 152
  end
  object ActualizarMedioComunicacionLugarDeVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioComunicacionLugaresDeVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@HorarioDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 452
    Top = 76
  end
  object ObtenerDatosLugarDeVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosLugaresDeVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 481
    Top = 76
  end
  object EliminarDomiciliosLugarDeVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarDomiciliosLugarDeVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ListaDomicilios'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 588
    Top = 136
  end
  object EliminarTodosDomiciliosLugarDeVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTodosDomiciliosLugarDeVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 616
    Top = 136
  end
  object ActualizarDomicilioLugarDeVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioLugarDeVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoEdificacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Depto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 658
    Top = 140
  end
  object ActualizarDomicilioEntregaLugarDeVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioEntregaLugarDeVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilioEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 686
    Top = 140
  end
  object ActualizarDomicilioRelacionadoLugarDeVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioRelacionadoLugarDeVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 714
    Top = 140
  end
end
