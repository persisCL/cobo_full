unit ABMIPC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcsCN, validate,
  Dateedit, Util, ADODB,DPSControls, Provider, DBClient, ListBoxEx, DBListEx,
  VariantComboBox, RStrings, ImgList, ToolWin, Variants, PeaProcs, IPC, Grids,                        //TASK_105_JMA_20170302
  DBGrids;                                                                                              //TASK_105_JMA_20170302

type
  TFormIPC = class(TForm)
    pnlEdicion: TPanel;
    pnlBotones: TPanel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    ilImgActiva: TImageList;
    tlbBotonera: TToolBar;
    btnSalir: TToolButton;
    btnAgregar: TToolButton;
    btn1: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    btn2: TToolButton;
    btnImprimir: TToolButton;
    btn3: TToolButton;
    btnBuscar: TToolButton;
    dbgrdIPC: TDBGrid;

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    //procedure dblIPCClick(Sender: TObject);                   //TASK_105_JMA_20170302
    procedure btnAceptarClick(Sender: TObject);
    procedure dbgrdIPCDrawColumnCell(Sender: TObject; const Rect: TRect;	//TASK_112_JMA_20170309
      DataCol: Integer; Column: TColumn; State: TGridDrawState);			//TASK_112_JMA_20170309
    //procedure btnAgregarClick(Sender: TObject);               //TASK_112_JMA_20170309
  private
    { Private declarations }
    objIPC : TIPC;            //TASK_105_JMA_20170302
    FEditando : Boolean;                                      //TASK_112_JMA_20170309
    FAnioSeleccionado: Integer;                               //TASK_112_JMA_20170309
    FAnioInicial : Integer;                                   //TASK_112_JMA_20170309

    //procedure LimpiarCampos();                              //TASK_112_JMA_20170309
    procedure PermitirEdicion(Activar : boolean);
    procedure CargarIPC();
    function Validar():Boolean;
    procedure IPC_AfterEdit(Sender: TDataSet);                 //TASK_112_JMA_20170309
    //procedure IPC_AfterScroll(DataSet: TDataSet);            //TASK_112_JMA_20170309
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormIPC: TFormIPC;
//  spObtenerIPC, spActualizarIPC: TADOStoredProc; //TASK_105_JMA_20170302
  //accion, ultimo, primero: integer;              //TASK_105_JMA_20170302 //TASK_112_JMA_20170309
  
implementation

Uses
  DMConnection;

resourcestring
  MSG_CAPTION_VALIDAR                = 'Validar datos ingresados';    
  MSG_ANIO_VACIO                     = 'Ingrese A�o';
  MSG_ANIO_INVALIDO                   = 'Ingrese un valor entero para ANIO';
  MSG_TBP_VACIO                      = 'Ingrese Valor de TBP';
  MSG_TBP_INVALIDO                   = 'Ingrese un valor entero para TBP';
  MSG_TBFP_VACIO                     = 'Ingrese Valor de TBFP';
  MSG_TBFP_INVALIDO                   = 'Ingrese un valor entero para TBFP';
  MSG_TBI_VACIO                     = 'Ingrese Valor de TBI';                  //TASK_105_JMA_20170302
  MSG_TBI_INVALIDO                   = 'Ingrese un valor entero para TBI';     //TASK_105_JMA_20170302
  MSG_VALOR_VACIO                    = 'Ingrese Valor de IPC';
  MSG_VALOR_INVALIDO                   = 'Ingrese un valor entero para VALOR';

  MSG_TITULO_ERROR                   = 'Error de ejecuci�n';

  MSG_AGREGAR_CAPTION   	   	     = 'Agregar IPC';
  MSG_AGREGAR_OK                     = 'IPC agregado con �xito';
  MSG_AGREGAR_ERROR                  = 'Error al agregar IPC';

  MSG_ACTUALIZAR_CAPTION   		     = 'Actualizar IPC';
  MSG_ACTUALIZAR_OK                  = 'IPC actualizado con �xito';
  MSG_ACTUALIZAR_ERROR               = 'Error al actualizar IPC';

  MSG_CONTINUAR_ACTUALIZACION        = '�Confirma actualizar los valores?';			//TASK_105_JMA_20170302		//TASK_112_JMA_20170309
  MSG_CONTINUAR_ACTUALIZACION2        = '�ATENCI�N! Este valor de IPC ha sido utilizado para calcular transitos y tarifas �Desea continuar?';

{$R *.DFM}

{$REGION 'TASK_112_JMA_20170309'}
{INICIO: TASK_112_JMA_20170309
function TFormIPC.Inicializa: boolean;
begin
    objIPC := TIPC.Create;

    DecimalSeparator := '.';
    accion := 0;

    try
        ultimo := objIPC.ObtenerUltimoAnio;
        primero := objIPC.ObtenerPrimerAnio;
    Except
        on e: Exception do
        begin
            MsgBoxErr('Error obteniendo ultimo a�o', e.Message, 'Error', MB_ICONERROR);
            Close;
        end;
    end;

    PermitirEdicion(false);

    CargarIPC();
    dblIPC.SetFocus;
                
    Result := True;
end;


procedure TFormIPC.CargarIPC();
begin
    //INICIO: TASK_105_JMA_20170302
    try

        if not Assigned(spObtenerIPC) then
        begin
            spObtenerIPC := TADOStoredProc.Create(nil);
            spObtenerIPC.Connection := DMConnections.BaseCAC;
            spObtenerIPC.ProcedureName := 'IPC_SELECT';
            spObtenerIPC.Parameters.Refresh();
        end;

        dsIPC.DataSet := spObtenerIPC;
        if spObtenerIPC.Active  then
           spObtenerIPC.Close();
        spObtenerIPC.Open();

    except
        on e: Exception do
        begin
            FreeAndNil(spObtenerIPC);
        end;
    end;
    //
    try
        if Assigned(dblIPC.DataSource) then
            dblIPC.DataSource.Free;
        dblIPC.DataSource := TDataSource.Create(nil);

        dblIPC.DataSource.DataSet := objIPC.Obtener();		//TASK_109_JMA_20170215
        objIPC.ClientDataSet.AfterScroll := IPC_AfterScroll;
        IPC_AfterScroll(objIPC.ClientDataSet);

        if objIPC.ClientDataSet.RecordCount = 0  then
            btnAgregar.Enabled := True;

    except
        on e: Exception do
        begin
            MsgBoxErr('Error cargando IPCs', e.Message, 'Error de Carga', MB_ICONSTOP);
        end;
    end;
    //TERMINO: TASK_105_JMA_20170302

end;
//INICIO: TASK_105_JMA_20170302
procedure TFormIPC.dblIPCClick(Sender: TObject);
begin
    if Assigned(spObtenerIPC) and (spObtenerIPC.RecordCount > 0) then begin
        txtAnio.Text := dblIPC.DataSource.DataSet.FieldByName('Anio').Value;
        txtTBP.Text := dblIPC.DataSource.DataSet.FieldByName('TBP').Value;
        txtTBFP.text := dblIPC.DataSource.DataSet.FieldByName('TBFP').Value;
        txtValor.text := iif(dblIPC.DataSource.DataSet.FieldByName('Valor').Value = Null, '', dblIPC.DataSource.DataSet.FieldByName('Valor').Value);
    end;
end;
//
procedure TFormIPC.IPC_AfterScroll(DataSet: TDataSet);
begin
    LimpiarCampos;      
    if Assigned(objIPC.ClientDataSet) and (objIPC.ClientDataSet.RecordCount > 0) then begin
        txtAnio.Text := FloatToStr(objIPC.Anio);
        txtTBP.Text := FloatToStr(objIPC.TBP);
        txtTBFP.text := FloatToStr(objIPC.TBFP);
        txtTBI.text := FloatToStr(objIPC.TBI);
        txtValor.text := IIf(objIPC.Valor = null, '', objIPC.Valor);
    end;
end;
//TERMINO: TASK_105_JMA_20170302

procedure TFormIPC.btnAgregarClick(Sender: TObject);
begin
    accion := 1;
    PermitirEdicion(true);
    txtAnio.SetFocus;
end;

procedure TFormIPC.btnEditarClick(Sender: TObject);
begin
    //if Assigned(spObtenerIPC) and (spObtenerIPC.RecordCount > 0) then //TASK_105_JMA_20170302
    if Assigned(objIPC.ClientDataSet) and (objIPC.ClientDataSet.RecordCount > 0)  then     //TASK_105_JMA_20170302
    begin
        //dblIPCClick(Sender);                          //TASK_105_JMA_20170302
        IPC_AfterScroll(objIPC.ClientDataSet);          //TASK_105_JMA_20170302
        accion := 2;
        PermitirEdicion(True);
        txtValor.SetFocus;
    end;
end;

procedure TFormIPC.PermitirEdicion(Activar: Boolean);
var
    i: integer;
    anioSeleccionado:Integer;
begin
    pnlBotones.Enabled := not Activar;
    dblIPC.Enabled := not Activar;

    btnAceptar.Enabled := Activar;
    btnCancelar.Enabled := Activar;
                                            
//INICIO: TASK_105_JMA_20170302
    txtAnio.Enabled := True;
    txtTBP.Enabled := True;
    txtTBFP.Enabled := True;
    txtValor.Enabled := True;
    
    if Activar then
    begin
        if accion = 1 then //Agregar
        begin
            LimpiarCampos();
            if ultimo <> 0 then     //Existen Registros
            begin
                spObtenerIPC.First;
                for I := 0 to spObtenerIPC.RecordCount - 1 do
                begin
                    if spObtenerIPC.FieldByName('Anio').value = ultimo then
                        break;
                    spObtenerIPC.Next;
                end;

                if spObtenerIPC.FieldByName('Anio').value <> ultimo then
                begin
                   MsgBox('El ultimo valor del IPC fue modificado, recargue la ventana');
                   Exit;
                end;
                txtAnio.Enabled := False;
                txtTBP.Enabled := False;
                txtTBFP.Enabled := False;
                txtAnio.Text := spObtenerIPC.FieldByName('Anio').Value;
                txtTBP.Text := spObtenerIPC.FieldByName('TBP').Value;
                txtTBFP.Text := spObtenerIPC.FieldByName('TBFP').Value;
            end;
        end
        else if accion = 2 then //Editar
        begin
            txtAnio.Enabled := False;
        end;
    end
    else
        LimpiarCampos();
//
    txtAnio.Enabled := False;
    txtTBP.Enabled := False;
    txtTBFP.Enabled := False;
    txtTBI.Enabled := False;
    txtValor.Enabled := False;

    if Activar then
    begin
        anioSeleccionado := 0;
        if objIPC.ClientDataSet.RecordCount > 0 then
            anioSeleccionado := objIPC.Anio;

        if (ultimo = 0) OR (primero = anioSeleccionado) then     //Primer registro
        begin
            txtAnio.Enabled := True;
            txtTBP.Enabled := True;
            txtTBFP.Enabled := True;
            txtTBI.Enabled := True;
            txtValor.Enabled := True;
        end
        else   //Existen Registros
        begin
            if accion = 1 then //Agregar
            begin
                LimpiarCampos();

                //Esto lo hago para que seleccion el ultimo a�o asi este en otro registro actualmente.
                objIPC.ClientDataSet.First;
                for I := 0 to objIPC.ClientDataSet.RecordCount - 1 do
                begin
                    if objIPC.Anio = ultimo then
                        break;
                    objIPC.ClientDataSet.Next;
                end;

                if objIPC.Anio <> ultimo then
                begin
                   MsgBox('El ultimo valor del IPC fue modificado, recargue la ventana');
                   Exit;
                end;

                txtValor.Enabled := True;

                txtAnio.Text := FloatToStr(objIPC.Anio);
                txtTBP.Text := FloatToStr(objIPC.TBP);
                txtTBFP.Text := FloatToStr(objIPC.TBFP);
                txtTBI.Text := FloatToStr(objIPC.TBI);
            end
            else
                txtValor.Enabled := True;
        end;
    end
    else
        IPC_AfterScroll(objIPC.ClientDataSet);     //TASK_105_JMA_20170302
//TERMINO: TASK_105_JMA_20170302

end;

procedure TFormIPC.LimpiarCampos();
begin
	txtAnio.Clear();
    txtTBP.Clear();
    txtTBFP.Clear();
    txtTBI.Clear();          //TASK_105_JMA_20170302
    txtValor.Clear();
end;

procedure TFormIPC.btnAceptarClick(Sender: TObject);
var
    IdIPC: Integer;
    m: string;                           //TASK_109_JMA_20170215
begin
//INICIO: TASK_105_JMA_20170302
    try
        try      
       
            if not Validar() then
            Exit;

            if not Assigned(spActualizarIPC) then
            begin
                spActualizarIPC := TADOStoredProc.Create(nil);
                spActualizarIPC.Connection := DMConnections.BaseCAC;        
            end;
    
            if (accion = 1) then  //Agregar
                spActualizarIPC.ProcedureName := 'IPC_Agregar'
            else if (accion = 2) then //Modificar
            begin
                if (spObtenerIPC.FieldByName('Anio').Value <> ultimo) then
                   if MsgBox(MSG_CONTINUAR_ACTUALIZACION, MSG_ACTUALIZAR_CAPTION, MB_YESNO+MB_ICONQUESTION) = ID_NO then
                   begin
                        Exit;
                   end;

                if QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.IPC_EnUso(' + IntToStr(spObtenerIPC.FieldByName('Anio').Value) + ')') > 0 then
                   if MsgBox(MSG_CONTINUAR_ACTUALIZACION2, MSG_ACTUALIZAR_CAPTION, MB_YESNO+MB_ICONQUESTION) = ID_NO then
                   begin
                        Exit;
                   end;

                spActualizarIPC.ProcedureName := 'IPC_Modificar';
            end;
            
            btnAceptar.Enabled := False;
            btnCancelar.Enabled := False;
            Screen.Cursor := crHourGlass;
            
            spActualizarIPC.Parameters.Refresh();   
            spActualizarIPC.Parameters.ParamByName('@Anio').Value := txtAnio.Text;
            spActualizarIPC.Parameters.ParamByName('@TBP').Value := StrToFloat(txtTBP.Text) * 100;
            spActualizarIPC.Parameters.ParamByName('@TBFP').Value := StrToFloat(txtTBFP.Text) * 100;
            spActualizarIPC.Parameters.ParamByName('@Valor').Value := StrToFloat(StringReplace(txtValor.Text,',','.',[rfReplaceAll])) * 100;
            spActualizarIPC.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
            spActualizarIPC.ExecProc;
        
            if spActualizarIPC.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
            begin
                MsgBoxErr(MSG_ACTUALIZAR_ERROR,
                            spActualizarIPC.Parameters.ParamByName('@ErrorDescription').Value,
                            MSG_TITULO_ERROR,
                            MB_ICONSTOP);
                btnAceptar.Enabled := True;
                btnCancelar.Enabled := True;            
            end
            else
            begin
                CargarIPC();
                PermitirEdicion(false);
                if accion = 1 then
                begin
                    ultimo := ultimo + 1;
                    MsgBox(MSG_AGREGAR_OK, MSG_AGREGAR_CAPTION, MB_OK);
                end
                else
                    MsgBox(MSG_ACTUALIZAR_OK, MSG_ACTUALIZAR_CAPTION, MB_OK);   
            end;              
        except
            on e: exception do
            begin
                btnAceptar.Enabled := True;
                btnCancelar.Enabled := True;
                MsgBoxErr(MSG_TITULO_ERROR, e.Message, MSG_TITULO_ERROR, MB_ICONSTOP);
            end;
        end;    
    finally
        FreeAndNil(spActualizarIPC);
        Screen.Cursor := crArrow;        
    end;
//
    try
        try      

            if not Validar() then
                Exit;

            if (accion = 2) then //Modificar
            begin
                if (objIPC.Anio <> ultimo) AND (MsgBox(MSG_CONTINUAR_ACTUALIZACION, MSG_ACTUALIZAR_CAPTION, MB_YESNO+MB_ICONQUESTION) = ID_NO) then
                    Exit;

                if (objIPC.TieneTraficos() > 0) AND (MsgBox(MSG_CONTINUAR_ACTUALIZACION2, MSG_ACTUALIZAR_CAPTION, MB_YESNO+MB_ICONQUESTION) = ID_NO) then
                    Exit;
            end;

            btnAceptar.Enabled := False;
            btnCancelar.Enabled := False;
            Screen.Cursor := crHourGlass;

            IdIPC := 0;
            if ultimo <> 0  then
                IdIPC := objIPC.IdIPC;

            if objIPC.Agregar(IdIPC, txtAnio.Text, txtTBP.Text, txtTBFP.Text, txtTBI.Text, txtValor.Text, UsuarioSistema) then
            begin
                PermitirEdicion(false);
                ultimo := objIPC.ObtenerUltimoAnio;
                primero := objIPC.ObtenerPrimerAnio;
                m:= objIPC.ClientDataSet.Bookmark;             //TASK_109_JMA_20170215
                CargarIPC();
                objIPC.ClientDataSet.Bookmark := m;             //TASK_109_JMA_20170215
                if accion = 1 then
                    MsgBox(MSG_AGREGAR_OK, MSG_AGREGAR_CAPTION, MB_OK)
                else
                    MsgBox(MSG_ACTUALIZAR_OK, MSG_ACTUALIZAR_CAPTION, MB_OK);
            end;

        except
            on e: exception do
            begin
                btnAceptar.Enabled := True;
                btnCancelar.Enabled := True;
                MsgBoxErr(MSG_TITULO_ERROR, e.Message, MSG_TITULO_ERROR, MB_ICONSTOP);
            end;
        end;    
    finally
        Screen.Cursor := crArrow;        
    end;
//TERMINO: TASK_105_JMA_20170302
end;

function TFormIPC.Validar():Boolean;
var 
    mensaje: string;
begin
    Result:= True;
    mensaje:= '';
//INICIO: TASK_105_JMA_20170302
    if TRIM(txtValor.Text) = '' then
        mensaje := MSG_VALOR_VACIO

    else if TRIM(txtTBP.Text) = '' then
        mensaje := MSG_TBP_VACIO

    else if TRIM(txtTBFP.Text) = '' then
        mensaje := MSG_TBFP_VACIO

    else if (accion = 2) and not IsInteger(txtTBP.Text) then
        mensaje := MSG_TBP_INVALIDO

    else if (accion = 2) and not IsInteger(txtTBFP.Text) then
        mensaje := MSG_TBFP_INVALIDO

    else if (accion = 1) and (ultimo = 0) then //Agregar
    begin
        if TRIM(txtAnio.Text) = '' then
            mensaje := MSG_ANIO_VACIO
        else if not IsInteger(txtAnio.Text) then
            mensaje := MSG_ANIO_INVALIDO;
    end;
 //

    if (TRIM(txtValor.Text) <> '') and not IsFloat(TRIM(txtValor.Text)) then
        mensaje := MSG_VALOR_INVALIDO;

    if ultimo = 0 then
    begin
        if TRIM(txtTBP.Text) = '' then
            mensaje := MSG_TBP_VACIO
        else if not IsFloat(txtTBP.Text) then
            mensaje := MSG_TBP_INVALIDO
        else if TRIM(txtTBFP.Text) = '' then
            mensaje := MSG_TBFP_VACIO
        else if not IsFloat(txtTBFP.Text) then
            mensaje := MSG_TBFP_INVALIDO
        else if TRIM(txtTBI.Text) = '' then
            mensaje := MSG_TBI_VACIO
        else if not IsFloat(txtTBI.Text) then
            mensaje := MSG_TBI_INVALIDO;
    end;

//TERMINO: TASK_105_JMA_20170302
    if mensaje <> '' then
    begin
        MsgBox(mensaje, MSG_CAPTION_VALIDAR, MB_ICONSTOP);
        Result := False;
    end;
end;

procedure TFormIPC.btnCancelarClick(Sender: TObject);
begin
    accion := 0;
    PermitirEdicion(False);
end;                              

procedure TFormIPC.btnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormIPC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//INICIO: TASK_105_JMA_20170302
    FreeAndNil(spObtenerIPC);
    FreeAndNil(spActualizarIPC);
//
    dblIPC.DataSource.Free;
    objIPC.Free;
//TERMINO: TASK_105_JMA_20170302
    Action := caFree;
end;
}
{$ENDREGION}


function TFormIPC.Inicializa: boolean;
begin
    objIPC := TIPC.Create;

    DecimalSeparator := '.';
    FEditando := False;
    CargarIPC();
    FAnioInicial :=  objIPC.AnioInicial();
    Result := True;
end;

procedure TFormIPC.CargarIPC();
var
    i: integer;
begin

    try
        if Assigned(dbgrdIPC.DataSource) then
            dbgrdIPC.DataSource.Free;
        dbgrdIPC.DataSource := TDataSource.Create(nil);
        objIPC.Obtener();

        for I := 0 to objIPC.ClientDataSet.Fields.Count - 1 do
            objIPC.ClientDataSet.Fields[i].ReadOnly := True;

        dbgrdIPC.DataSource.DataSet := objIPC.ClientDataSet;
        for I := 0 to dbgrdIPC.Columns.Count - 1 do
        begin
             if dbgrdIPC.Columns[i].Title.Caption = 'Anio' then
             begin
                dbgrdIPC.Columns[i].Title.Caption := 'A�o';
                dbgrdIPC.Columns[i].Font.Color := clGrayText;
             end;                  
             dbgrdIPC.Columns[i].Width := 100;
        end;
        objIPC.ClientDataSet.AfterEdit := IPC_AfterEdit;

    except
        on e: Exception do
        begin
            MsgBoxErr('Error cargando IPCs', e.Message, 'Error de Carga', MB_ICONSTOP);
        end;
    end;

end;

procedure TFormIPC.dbgrdIPCDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
    if (Column.Title.Caption <> 'A�o') then
            TFloatField(Column.Field).DisplayFormat  := '##0.00';
    if FAnioSeleccionado <> 0 then
    begin      
        if (Column.Title.Caption <> 'A�o') and (objIPC.Anio = FAnioSeleccionado) then
            dbgrdIPC.Canvas.Font.Style := [fsBold]
        else
            dbgrdIPC.Canvas.Font.Style := [];
        dbgrdIPC.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
end;

procedure TFormIPC.IPC_AfterEdit(Sender: TDataSet);
begin
    if objIPC.Anio <> FAnioSeleccionado then
       objIPC.ClientDataSet.cancel;
end;

procedure TFormIPC.btnEditarClick(Sender: TObject);
begin

    if Assigned(objIPC.ClientDataSet) and (objIPC.ClientDataSet.RecordCount > 0)  then
    begin
        if FAnioInicial = objIPC.Anio then
            MsgBox('El primer a�o no se puede modificar')
        else
            PermitirEdicion(True);
    end;
end;

procedure TFormIPC.PermitirEdicion(Activar: Boolean);
var
    i: integer;
begin
    btnAceptar.Enabled := Activar;
    btnCancelar.Enabled := Activar;
    pnlBotones.Enabled := not activar;

    FAnioSeleccionado := 0;
    if Activar then
        FAnioSeleccionado := objIPC.Anio;

    for i := 0 to objIPC.ClientDataSet.Fields.Count - 1 do
    begin
         if (objIPC.ClientDataSet.Fields[i].FieldName <> 'Anio') then
         begin
             objIPC.ClientDataSet.Fields[i].ReadOnly := not Activar;
         end;
    end;
end;

procedure TFormIPC.btnAceptarClick(Sender: TObject);
begin

    try
        try

            if not Validar() then
                Exit;

            if MsgBox(MSG_CONTINUAR_ACTUALIZACION, MSG_ACTUALIZAR_CAPTION, MB_YESNO+MB_ICONQUESTION) = ID_NO then
                Exit;

            if (objIPC.TieneTraficos() > 0) AND (MsgBox(MSG_CONTINUAR_ACTUALIZACION2, MSG_ACTUALIZAR_CAPTION, MB_YESNO+MB_ICONQUESTION) = ID_NO) then
                Exit;                             

            btnAceptar.Enabled := False;
            btnCancelar.Enabled := False;
            Screen.Cursor := crHourGlass;
            //OJO Validar ya lo deja en el registro modificado
            if objIPC.modificar(objipc.Anio, objIPC.ClientDataSet, UsuarioSistema) then
            begin
                PermitirEdicion(false);
                CargarIPC;
                MsgBox('Valores actualizados', 'Actualizaci�n', MB_OK);
            end;

        except
            on e: exception do
            begin
                btnAceptar.Enabled := True;
                btnCancelar.Enabled := True;
                MsgBoxErr(MSG_TITULO_ERROR, e.Message, MSG_TITULO_ERROR, MB_ICONSTOP);
            end;
        end;    
    finally
        Screen.Cursor := crArrow;
    end;

end;

function TFormIPC.Validar():Boolean;
var
    i: integer;
    marca : TBookmark;
begin
    Result:= True;

    marca := objIPC.BuscarEnClientDataSet('Anio', IntToStr(FAnioSeleccionado));
    try
        objipc.ClientDataSet.GotoBookmark(marca);
        for I := 0 to objIPC.ClientDataSet.Fields.Count - 1 do
        begin
            if not ValidateControls([dbgrdIPC], [VarToStr(objIPC.ClientDataSet.Fields[i].Value) <> ''], 'Validar', ['No deje valores vacios']) then
            begin
               Result := false;
               Exit;
            end;
        end;
    finally
        if objIPC.ClientDataSet.BookmarkValid(marca) then
            objIPC.ClientDataSet.FreeBookmark(marca);
    end;

end;

procedure TFormIPC.btnCancelarClick(Sender: TObject);
begin
    PermitirEdicion(False);
    CargarIPC;
end;

procedure TFormIPC.btnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormIPC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    FreeAndNil(objIPC);
    if Assigned(dbgrdIPC.DataSource) then
        dbgrdIPC.DataSource.Free;
    Action := caFree;
end;
{TERMINO: TASK_112_JMA_20170309}

end.
