object FormDocs: TFormDocs
  Left = 233
  Top = 185
  Width = 600
  Height = 400
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Tipos de Documentos'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 334
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 270
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 126
        Top = 2
        Width = 197
        Height = 37
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object BtnSalir: TDPSButton
            Left = 116
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TDPSButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TDPSButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
    end
  end
  object Lista: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 189
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'47'#0'C'#243'digo  '
      #0'64'#0'Descripci'#243'n')
    HScrollBar = True
    RefreshTime = 10
    Table = Documentos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaClick
    OnProcess = ListaProcess
    OnDrawItem = ListaDrawItem
    OnRefresh = ListaRefresh
    OnInsert = ListaInsert
    OnDelete = ListaDelete
    OnEdit = ListaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar, btImprimir]
    OnClose = AbmToolbar1Close
  end
  object GroupB: TPanel
    Left = 0
    Top = 222
    Width = 592
    Height = 112
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 3
    object Label1: TLabel
      Left = 9
      Top = 40
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txt_Descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 9
      Top = 13
      Width = 48
      Height = 13
      Caption = '&C'#243'digo: '
      FocusControl = txt_Documento
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 9
      Top = 86
      Width = 44
      Height = 13
      Caption = 'M'#225'scara:'
    end
    object Label3: TLabel
      Left = 229
      Top = 86
      Width = 116
      Height = 13
      Caption = 'Caracter para completar:'
    end
    object txt_Descripcion: TEdit
      Left = 100
      Top = 36
      Width = 277
      Height = 21
      Color = 16444382
      MaxLength = 60
      TabOrder = 1
    end
    object txt_Documento: TEdit
      Left = 100
      Top = 9
      Width = 55
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 4
      TabOrder = 0
    end
    object chk_default: TCheckBox
      Left = 9
      Top = 61
      Width = 104
      Height = 17
      Alignment = taLeftJustify
      BiDiMode = bdLeftToRight
      Caption = 'Default:'
      ParentBiDiMode = False
      TabOrder = 2
    end
    object txtMascara: TEdit
      Left = 100
      Top = 82
      Width = 121
      Height = 21
      MaxLength = 20
      TabOrder = 4
    end
    object txtCaracter: TEdit
      Left = 348
      Top = 82
      Width = 29
      Height = 21
      MaxLength = 1
      TabOrder = 5
    end
    object chkDigitoVerificador: TCheckBox
      Left = 128
      Top = 61
      Width = 113
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Digito Verificador:'
      TabOrder = 3
    end
  end
  object Documentos: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'TiposDocumento'
    Left = 316
    Top = 75
  end
end
