object FABMTiposDeCasos: TFABMTiposDeCasos
  Left = 107
  Top = 114
  Caption = 'Mantenimiento de Tipos de Casos'
  ClientHeight = 567
  ClientWidth = 809
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupB: TPanel
    Left = 0
    Top = 428
    Width = 809
    Height = 100
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Lcodigo: TLabel
      Left = 10
      Top = 14
      Width = 44
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Ldescripcion: TLabel
      Left = 275
      Top = 14
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Ltipo: TLabel
      Left = 10
      Top = 44
      Width = 55
      Height = 13
      Caption = 'Prioridad:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 275
      Top = 44
      Width = 109
      Height = 13
      Caption = 'Tiempo Resoluci'#243'n'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 448
      Top = 44
      Width = 26
      Height = 13
      Caption = 'd'#237'as'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 536
      Top = 44
      Width = 32
      Height = 13
      Caption = 'horas'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 634
      Top = 44
      Width = 44
      Height = 13
      Caption = 'minutos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtCodigo: TNumericEdit
      Left = 86
      Top = 10
      Width = 105
      Height = 21
      Color = 16444382
      Enabled = False
      TabOrder = 0
    end
    object txtDescripcion: TEdit
      Left = 392
      Top = 10
      Width = 347
      Height = 21
      Color = 16444382
      TabOrder = 1
    end
    object cbPrioridad: TVariantComboBox
      Left = 86
      Top = 40
      Width = 145
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Items = <
        item
          Caption = 'Muy Alta'
          Value = '1'
        end
        item
          Caption = 'Alta'
          Value = '2'
        end
        item
          Caption = 'Media'
          Value = '3'
        end
        item
          Caption = 'Baja'
          Value = '4'
        end>
    end
    object seDias: TSpinEdit
      Left = 392
      Top = 39
      Width = 50
      Height = 22
      Color = 16444382
      MaxValue = 0
      MinValue = 0
      TabOrder = 3
      Value = 0
    end
    object seHoras: TSpinEdit
      Left = 480
      Top = 39
      Width = 50
      Height = 22
      Color = 16444382
      MaxLength = 23
      MaxValue = 0
      MinValue = 0
      TabOrder = 4
      Value = 0
    end
    object seMinutos: TSpinEdit
      Left = 576
      Top = 39
      Width = 50
      Height = 22
      Color = 16444382
      MaxValue = 59
      MinValue = 0
      TabOrder = 5
      Value = 0
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 528
    Width = 809
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 612
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        ExplicitWidth = 0
        ExplicitHeight = 0
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 809
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbarClose
  end
  object ListaTiposDeCasos: TAbmList
    Left = 0
    Top = 33
    Width = 809
    Height = 395
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'65'#0'C'#243'digo           '
      
        #0'414'#0'Descripci'#243'n                                                ' +
        '                                                  '
      #0'127'#0'Prioridad'
      #0'145'#0'Tiempo Resoluci'#243'n (Minutos)')
    HScrollBar = True
    RefreshTime = 100
    Table = tblTiposDeCasos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaTiposDeCasosClick
    OnProcess = ListaTiposDeCasosProcess
    OnDrawItem = ListaTiposDeCasosDrawItem
    OnRefresh = ListaTiposDeCasosRefresh
    OnInsert = ListaTiposDeCasosInsert
    OnDelete = ListaTiposDeCasosDelete
    OnEdit = ListaTiposDeCasosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object tblTiposDeCasos: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'TiposOrdenServicio'
    Left = 84
    Top = 81
  end
  object spActualizarTiposOrdenServicio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTiposOrdenServicio'
    Parameters = <>
    Left = 244
    Top = 80
  end
  object dsTiposDeCasos: TDataSource
    DataSet = tblTiposDeCasos
    Left = 86
    Top = 128
  end
  object spEliminarTiposOrdenServicio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTiposOrdenServicio'
    Parameters = <>
    Left = 240
    Top = 136
  end
end
