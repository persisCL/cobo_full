{********************************** File Header *********************************
File Name   : frmABMTipoMedioPago
Author      : nefernandez
Date Created: 20/09/2007
Language    : ES-AR
Description : Realiza la administraci�n de los tipos de medios de pagos.

Revision : 1
    Author : pdominguez
    Date   : 17/06/2009
    Description : SS 809
            - Se a�adieron los objetos:
                cdsFormasPagoAsociadas: TClientDataSet
                cdsFormasPagoNoAsociadas: TClientDataSet
                dsFormasPagoAsociadas: TDataSource
                dsFormasPagoNoAsociadas: TDataSource
                dbleFormasPagoAsociadas: TDBListEx
                dbleFormasPagoNoAsociadas: TDBListEx
            - Se modificaron / a�adieron los siguientes procedimientos / funciones:
                BtnAceptarClick
                btnAgregarFormasPagoCanalClick
                btnQuitarFormasPagoCanalClick
                dbleFormasPagoAsociadasDblClick
                dbleFormasPagoAsociadasDrawText                    
                HabilitarCamposEdicion
                FormasPagoDisponibles
                RefrescarCamposRegistroActual
                Limpiar_Campos

Etiqueta    : 20160503 MGO
Description : Se optimiza la carga del combo de conceptos
********************************************************************************}
unit frmABMTipoMedioPago;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilProc, ExtCtrls, DbList, Abm_obj, DMConnection, DB, ADODB,
  StdCtrls, DmiCtrls, UtilDB, VariantComboBox, Util, CheckLst, DBClient, ListBoxEx, DBListEx, ImgList;

type
  TABMTipoMedioPagoForm = class(TForm)
    tbTipoMedioPago: TAbmToolbar;
    lbTipoMedioPago: TAbmList;
    pnlTipoMedioPago: TPanel;
    pnlBotones: TPanel;
    TipoMedioPago: TADOTable;
    lblCodigoTipoMedioPago: TLabel;
    lblDescripcion: TLabel;
    lblMedioPagoAutomatico: TLabel;
    lblCodigoConceptoPago: TLabel;
    lblFuncion: TLabel;
    lblCodigoDeServicio: TLabel;
    lblCodigoFormaPago: TLabel;
    txtCodigoTipoMedioPago: TNumericEdit;
    txtDescripcion: TEdit;
    txtFuncion: TEdit;
    txtCodigoDeServicio: TEdit;
    cbCodigoConceptoPago: TVariantComboBox;
    qryConceptosMovimiento: TADOQuery;
    qryFormasPagoCanal: TADOQuery;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    spEliminarTipoMedioPago: TADOStoredProc;
    spActualizarTipoMedioPago: TADOStoredProc;
    qryTemp: TADOQuery;
    btnAgregarFormasPagoCanal: TButton;
    btnQuitarFormasPagoCanal: TButton;
    qryFormasPago: TADOQuery;
    lblFormasPago: TLabel;
    cbMedioPagoAutomatico: TVariantComboBox;
    dbleFormasPagoAsociadas: TDBListEx;
    dbleFormasPagoNoAsociadas: TDBListEx;
    cdsFormasPagoAsociadas: TClientDataSet;
    cdsFormasPagoNoAsociadas: TClientDataSet;
    dsFormasPagoAsociadas: TDataSource;
    dsFormasPagoNoAsociadas: TDataSource;
    cdsFormasPagoAsociadasCodigo: TSmallintField;
    cdsFormasPagoAsociadasDescripcion: TStringField;
    cdsFormasPagoNoAsociadasCodigo: TSmallintField;
    cdsFormasPagoNoAsociadasDescripcion: TStringField;
    cdsFormasPagoAsociadasAceptaPagoParcial: TBooleanField;
    Img_Tilde: TImageList;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbTipoMedioPagoDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure RefrescarCamposRegistroActual;
	procedure Limpiar_Campos;
    procedure lbTipoMedioPagoClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure tbTipoMedioPagoClose(Sender: TObject);
    procedure lbTipoMedioPagoInsert(Sender: TObject);
    procedure lbTipoMedioPagoDelete(Sender: TObject);
    procedure lbTipoMedioPagoEdit(Sender: TObject);
    procedure lbTipoMedioPagoRefresh(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure btnQuitarFormasPagoCanalClick(Sender: TObject);
    procedure btnAgregarFormasPagoCanalClick(Sender: TObject);
    procedure dbleFormasPagoAsociadasDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure dbleFormasPagoAsociadasDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitarCamposEdicion(Habilitar : Boolean);
    procedure PantallaModoNavegacion;
    procedure FormasPagoDisponibles(CodigoTipoMedioPago: Integer);
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

var
  ABMTipoMedioPagoForm: TABMTipoMedioPagoForm;

implementation

{$R *.dfm}

ResourceString
	MSG_CAPTION_VALIDAR_FP      		= 'Validar Formas de Pago de Tipo Medio de Pago';
  	MSG_ERROR_SELECCIONAR_ITEM_CFP		= 'Debe seleccionar un item de la lista de las Formas de Pago asignadas';
  	MSG_ERROR_SELECCIONAR_ITEM_FP		= 'Debe seleccionar un item de la lista de las Formas de Pago disponibles';

{******************************** Function Header ******************************
Function Name: BtnAceptarClick
Author: nefernandez
Date Created: 20/09/2007
Description: Ejecuta la inserci�n o actualizaci�n del tipo de medio pago
Parameters: Sender: TObject
Return Value: None

Revision : 1
    Author : pdominguez
    Date   : 17/06/2009
    Description : SS 809
            - Se cambia el objeto lbcodigoFormaPago: TListBox, por el objeto
            cdsFormasPagoAsociadas: TClientDataSet.
            - se a�ade la grabaci�n del campo AceptaPagoParcial en la tabla
            FormasPagoCanal.
*******************************************************************************}
procedure TABMTipoMedioPagoForm.BtnAceptarClick(Sender: TObject);
ResourceString
	MSG_ACTUALIZAR_CAPTION		    = 'Actualizar tipo de medio de pago';
    MSG_ACTUALIZAR_ERROR		    = 'No se pudieron actualizar los datos del tipo de medio de pago';
    MSG_VALIDAR_CAPTION 		    = 'Validar datos del tipo de medio de pago';
    MSG_VALIDAR_TARJETA             = 'Debe ingresar el tipo de medio de pago';
	MSG_VALIDAR_DESCRIPCION 	    = 'Debe ingresar una descripci�n';
    MSG_VALIDAR_PAGO_AUTOMATICO     = 'Debe indicar si el tipo de medio de pago es autom�tico';
    MSG_ACTUALIZAR_CAPTION_FPAGO	= 'Actualizar Formas de Pago ';
    MSG_ERROR_CONCEPTO_PAGO         = 'Debe seleccionar un valor de concepto de pago';
var
    CodigoTipoMedioPago: Integer;
    TodoOk: Boolean;
    i: Integer;
begin
   	if (Trim(txtDescripcion.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDAR_DESCRIPCION, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtDescripcion);
		Exit;
    end;

   	if (cbCodigoConceptoPago.ItemIndex = 0) then begin
		MsgBoxBalloon( MSG_ERROR_CONCEPTO_PAGO, MSG_VALIDAR_CAPTION, MB_ICONSTOP, cbCodigoConceptoPago);
		Exit;
    end;

	Screen.Cursor := crHourGlass;
    TodoOk := False;
    try
    	DMConnections.BaseCAC.BeginTrans;
        try
            if lbTipoMedioPago.Estado = Alta then begin
                CodigoTipoMedioPago := -1;
            end else begin
                CodigoTipoMedioPago := TipoMedioPago.FieldByName('CodigoTipoMedioPago').AsInteger;
            end;

            spActualizarTipoMedioPago.Parameters.ParamByName('@CodigoTipoMedioPago').Value := iif(CodigoTipoMedioPago = -1, null, CodigoTipoMedioPago);;
            spActualizarTipoMedioPago.Parameters.ParamByName('@Descripcion').Value := iif(Trim(txtDescripcion.Text)= '', null, Trim(txtDescripcion.Text));
            spActualizarTipoMedioPago.Parameters.ParamByName('@MedioPagoAutomatico').Value  := cbMedioPagoAutomatico.Value;
            spActualizarTipoMedioPago.Parameters.ParamByName('@CodigoConceptoPago').Value  := cbCodigoConceptoPago.value;
            spActualizarTipoMedioPago.Parameters.ParamByName('@Funcion').Value  := iif(Trim(txtFuncion.Text)= '', null, Trim(txtFuncion.Text));
            spActualizarTipoMedioPago.Parameters.ParamByName('@CodigoDeServicio').Value  := iif(Trim(txtCodigoDeServicio.Text)= '', null, Trim(txtCodigoDeServicio.Text));

            spActualizarTipoMedioPago.ExecProc;
            CodigoTipoMedioPago := spActualizarTipoMedioPago.Parameters.ParamByName('@CodigoTipoMedioPago').Value;

            if lbTipoMedioPago.Estado = Modi then begin
                try
                   qryTemp.close;
                   qryTemp.SQL.clear;
                   qryTemp.SQL.add('DELETE FROM FormasPagoCanal WHERE CodigoTipoMedioPago = ' + IntToStr(CodigoTipoMedioPago));
                   qryTemp.ExecSQL;
                except
                    on E: EDataBaseError do begin
                        raise Exception.Create(MSG_ACTUALIZAR_CAPTION_FPAGO + #13#10 + E.message);
                    end;
                end;
            end;
            // Revisi�n 1 (SS 809)
            cdsFormasPagoAsociadas.First;
            cdsFormasPagoAsociadas.DisableControls;

            While Not cdsFormasPagoAsociadas.Eof do begin
                try
                    qryTemp.close;
                    qryTemp.SQL.clear;
                    qryTemp.SQL.Add('INSERT INTO FormasPagoCanal (CodigoTipoMedioPago, CodigoFormaPago, AceptaPagoParcial)'); // SS 809
                    qryTemp.SQL.Add('VALUES ( ' + IntToStr(CodigoTipoMedioPago));
                    qryTemp.SQL.Add(', ' + Trim(cdsFormasPagoAsociadasCodigo.AsString));
                    qryTemp.SQL.Add(', ' + iif(cdsFormasPagoAsociadasAceptaPagoParcial.AsBoolean,'1','0') +  ')'); // SS 809
                    qryTemp.ExecSQL;
                except
                    On E: Exception do begin
                        raise Exception.Create(MSG_ACTUALIZAR_CAPTION_FPAGO + #13#10 +
                                                E.message);
                    end;
                 end;
                cdsFormasPagoAsociadas.Next;
            end;

            cdsFormasPagoAsociadas.EnableControls;
            // Fin Revisi�n 1 (SS 809)
            
        TodoOk := True;

        except
            On E: Exception do begin
                MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                Exit;
            end;
        end;
    finally
    	if TodoOk then begin
        	DMConnections.BaseCAC.CommitTrans;
            PantallaModoNavegacion;
            Screen.Cursor := crDefault;
        end else begin
	        DMConnections.BaseCAC.RollbackTrans;
            Screen.Cursor := crDefault;
		end;
        lbTipoMedioPago.Reload;
        RefrescarCamposRegistroActual;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnAgregarFormasPagoCanalClick
Author: nefernandez
Date Created: 20/09/2007
Description: Agrega Formas de Pago al tipo de medio pago en edici�n
Parameters: Sender: TObject
Return Value: None

Revision : 1
    Author : pdominguez
    Date   : 17/06/2009
    Description : SS 809
            - Se cambian los objetos lbcodigoFormasPago, lbFormasPago: TListBox,
            por los objetos cdsFormasPagoAsociadas, cdsFormasPagoNoAsociadas: TClientDataSet.
*******************************************************************************}
procedure TABMTipoMedioPagoForm.btnAgregarFormasPagoCanalClick(Sender: TObject);
begin
	if cdsFormasPagoNoAsociadas.IsEmpty then begin
		MsgBox( MSG_ERROR_SELECCIONAR_ITEM_FP, MSG_CAPTION_VALIDAR_FP, MB_ICONSTOP);
	end else begin
        cdsFormasPagoAsociadas.AppendRecord(
            [cdsFormasPagoNoAsociadasCodigo.AsInteger,
             cdsFormasPagoNoAsociadasDescripcion.AsString,
             False]);
        cdsFormasPagoNoAsociadas.Delete;
	end;
end;

{******************************** Function Header ******************************
Function Name: BtnCancelarClick
Author: nefernandez
Date Created: 20/09/2007
Description: Cancela la edici�n del tipo de medio pago llamando al procedimiento PantallaModoNavegacion
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTipoMedioPagoForm.BtnCancelarClick(Sender: TObject);
begin
    PantallaModoNavegacion;
end;

{******************************** Function Header ******************************
Function Name: btnQuitarFormasPagoCanalClick
Author: nefernandez
Date Created: 20/09/2007
Description: Quita Formas de Pago al tipo de medio pago en edici�n
Parameters: Sender: TObject
Return Value: None

Revision : 1
    Author : pdominguez
    Date   : 17/06/2009
    Description : SS 809
            - Se cambian los objetos lbcodigoFormasPago, lbFormasPago: TListBox,
            por los objetos cdsFormasPagoAsociadas, cdsFormasPagoNoAsociadas: TClientDataSet.
*******************************************************************************}
procedure TABMTipoMedioPagoForm.btnQuitarFormasPagoCanalClick(Sender: TObject);
begin
	if cdsFormasPagoAsociadas.IsEmpty then begin
		MsgBox( MSG_ERROR_SELECCIONAR_ITEM_CFP, MSG_CAPTION_VALIDAR_FP, MB_ICONSTOP);
	end else begin
        cdsFormasPagoNoAsociadas.AppendRecord(
            [cdsFormasPagoAsociadasCodigo.AsInteger,
             cdsFormasPagoAsociadasDescripcion.AsString]);
        cdsFormasPagoAsociadas.Delete;
	end;
end;

{******************************** Function Header ******************************
Function Name: BtnSalirClick
Author: nefernandez
Date Created: 20/09/2007
Description: Llama al metodo para cerrar el formulario
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTipoMedioPagoForm.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Procedure Header *****************************
Procedure Name: dbleFormasPagoAsociadasDblClick
Author : pdominguez
Date Created : 17/06/2009
Parameters : Sender: TObject
Description : SS 809
        - Se cambia el estado del campo AceptaPagoParcial, al efectuar doble Click
        sobre la rejilla de datos.
*******************************************************************************}
procedure TABMTipoMedioPagoForm.dbleFormasPagoAsociadasDblClick(Sender: TObject);
begin
    cdsFormasPagoAsociadas.Edit;
    cdsFormasPagoAsociadasAceptaPagoParcial.AsBoolean := not cdsFormasPagoAsociadasAceptaPagoParcial.AsBoolean;
    cdsFormasPagoAsociadas.Post; 
end;

{******************************** Procedure Header *****************************
Procedure Name: dbleFormasPagoAsociadasDrawText
Author : pdominguez
Date Created : 17/06/2009
Parameters : Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
var DefaultDraw: Boolean
Description : SS 809
        - Se dibuja en la rejilla de datos el s�mbolo de "check" si el valor
        del campo AceptaPagoParcial es Verdadero.
*******************************************************************************}
procedure TABMTipoMedioPagoForm.dbleFormasPagoAsociadasDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'AceptaPagoParcial') then begin
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;
            try
                if (cdsFormasPagoAsociadas.FieldByName('AceptaPagoParcial').AsBoolean ) then
                    Img_Tilde.GetBitmap(0, Bmp)
                else
                    Img_Tilde.GetBitmap(1, Bmp);

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author: nefernandez
Date Created: 20/09/2007
Description: Cierra el formulario
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTipoMedioPagoForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

{******************************** Function Header ******************************
Function Name: HabilitarCamposEdicion
Author: nefernandez
Date Created: 20/09/2007
Description: Habilita/Deshabilita los campos para edici�n de un registro
Parameters: Habilitar: Boolean
Return Value: None

Revision : 1
    Author : pdominguez
    Date   : 17/06/2009
    Description : SS 809
            - Se cambia el objeto lbFormasPago: TListBox, por el objeto
            cdsFormasPagoNoAsociadas: TClientDataSet.
*******************************************************************************}
procedure TABMTipoMedioPagoForm.HabilitarCamposEdicion(Habilitar: Boolean);
begin
    lbTipoMedioPago.Enabled := not Habilitar;
	pnlTipoMedioPago.Enabled := Habilitar;
    if Habilitar then begin
        btnAgregarFormasPagoCanal.Visible := True;
        btnQuitarFormasPagoCanal.Visible := True;
        lblFormasPago.Visible := True;
        dbleFormasPagoNoAsociadas.Visible := True;
    end
    else begin
        btnAgregarFormasPagoCanal.Visible := False;
        btnQuitarFormasPagoCanal.Visible := False;
        lblFormasPago.Visible := False;
        dbleFormasPagoNoAsociadas.Visible := False;
    end;
	Notebook.PageIndex := ord(Habilitar);
end;

{******************************** Function Header ******************************
Function Name: PantallaModoNavegacion
Author: nefernandez
Date Created: 20/09/2007
Description: Se establece el formulario en modo de navegaci�n de registros
Parameters: None
Return Value: None
*******************************************************************************}
procedure TABMTipoMedioPagoForm.PantallaModoNavegacion;
begin
	lbTipoMedioPago.Estado     := Normal;
    HabilitarCamposEdicion(False);
	Notebook.PageIndex      := 0;
end;

{******************************** Function Header ******************************
Function Name: FormasPagoDisponibles
Author: nefernandez
Date Created: 20/09/2007
Description: Presenta las formas de pagos que se le pueden asignar a un tipo de
medio de pago en edici�n
Parameters: CodigoTipoMedioPago: Integer
Return Value: None

Revision : 1
    Author : pdominguez
    Date   : 17/06/2009
    Description : SS 809
            - Se cambia el objeto lbFormasPago: TListBox, por el objeto
            cdsFormasPagoNoAsociadas: TClientDataSet.
*******************************************************************************}
procedure TABMTipoMedioPagoForm.FormasPagoDisponibles(CodigoTipoMedioPago: Integer);
begin
    qryFormasPago.Close;
    qryFormasPago.Parameters.ParamByName('CodigoTipoMedioPago').Value :=  CodigoTipoMedioPago;
    qryFormasPago.Open;

    cdsFormasPagoNoAsociadas.EmptyDataSet;
    cdsFormasPagoNoAsociadas.DisableControls;

    while not qryFormasPago.Eof do begin
        cdsFormasPagoNoAsociadas.AppendRecord(
            [qryFormasPago.FieldByName('CodigoFormaPago').AsInteger,
             qryFormasPago.FieldByName('Descripcion').AsString]);
        qryFormasPago.Next;
    end;
    qryFormasPago.Close;

    cdsFormasPagoNoAsociadas.EnableControls;
    cdsFormasPagoNoAsociadas.First;
end;

{******************************** Function Header ******************************
Function Name: RefrescarCamposRegistroActual
Author: nefernandez
Date Created: 20/09/2007
Description: Por cada tipo de medio de pago seleccionado (grilla), se muestra
el valor de todos los campos en el panel de detalle
Parameters: CodigoTipoMedioPago: Integer
Return Value: None

Revision : 1
    Author : pdominguez
    Date   : 17/06/2009
    Description : SS 809
            - Se cambia el objeto lbCodigoFormasPago: TListBox, por el objeto
            cdsFormasPagoAsociadas: TClientDataSet.
*******************************************************************************}
procedure TABMTipoMedioPagoForm.RefrescarCamposRegistroActual;
var
    i: Integer;
begin

	with TipoMedioPago do begin
        txtCodigoTipoMedioPago.Value := FieldByName('CodigoTipoMedioPago').AsInteger;
		txtDescripcion.Text := Trim(FieldByName('Descripcion').AsString);
		txtFuncion.Text := Trim(FieldByName('Funcion').AsString);
		txtCodigoDeServicio.Text := Trim(FieldByName('CodigoDeServicio').AsString);

        if (FieldByName('MedioPagoAutomatico').AsBoolean) then
            cbMedioPagoAutomatico.ItemIndex := 1
        else
            cbMedioPagoAutomatico.ItemIndex := 0;

        cbCodigoConceptoPago.ItemIndex := 0;
        { INICIO : 20160503 MGO
        for i := 0 to cbCodigoConceptoPago.Items.Count - 1 do begin
            if FieldByName('CodigoConceptoPago').AsInteger = cbCodigoConceptoPago.Items.Items[i].Value then break;
            cbCodigoConceptoPago.ItemIndex :=  cbCodigoConceptoPago.ItemIndex + 1;
        end;
        }
        cbCodigoConceptoPago.ItemIndex := cbCodigoConceptoPago.Items.IndexOfValue(FieldByName('CodigoConceptoPago').AsInteger);
        // FIN : 20160503 MGO

		// FormasPagoCanal: Asociaci�n entre TipoMedioPago y FormasPago
		qryFormasPagoCanal.Close;
		qryFormasPagoCanal.Parameters.ParamByName('CodigoTipoMedioPago').Value :=  FieldByName('CodigoTipoMedioPago').AsInteger;
		qryFormasPagoCanal.Open;
        // Revisi�n 1 (SS 809)
        cdsFormasPagoAsociadas.DisableControls;
        cdsFormasPagoAsociadas.EmptyDataSet;

		while not qryFormasPagoCanal.Eof do begin
            cdsFormasPagoAsociadas.AppendRecord(
                [qryFormasPagoCanal.FieldByName('CodigoFormaPago').AsInteger,
                 qryFormasPagoCanal.FieldByName('Descripcion').AsString,
                 qryFormasPagoCanal.FieldByName('AceptaPagoParcial').AsBoolean]);
			qryFormasPagoCanal.Next;
		end;
		qryFormasPagoCanal.Close;

        cdsFormasPagoAsociadas.EnableControls;
        cdsFormasPagoAsociadas.First;
        // Fin Revisi�n 1 (SS 809)

        FormasPagoDisponibles(FieldByName('CodigoTipoMedioPago').AsInteger);
	end;
end;

{******************************** Function Header ******************************
Function Name: Limpiar_Campos
Author: nefernandez
Date Created: 20/09/2007
Description: Vac�a todos los campos del panel de detalle
Parameters: None
Return Value: None

Revision : 1
    Author : pdominguez
    Date   : 17/06/2009
    Description : SS 809
            - Se a�ade la inicializaci�n de los objetos cdsFormasPagoAsociadas y
            cdsFormasPagoNoAsociadas.
*******************************************************************************}
procedure TABMTipoMedioPagoForm.Limpiar_Campos;
begin
    txtCodigoTipoMedioPago.Clear;
	txtDescripcion.clear;
    txtFuncion.Clear;
    txtCodigoDeServicio.Clear;
    cdsFormasPagoAsociadas.EmptyDataSet; // SS 809
    cdsFormasPagoNoAsociadas.EmptyDataSet; // SS 809
    cbMedioPagoAutomatico.ItemIndex := 0;
    cbCodigoConceptoPago.ItemIndex := 0;

    // Se obtienen todas las formas de pagos disponibles
    FormasPagoDisponibles(-1);
end;

{******************************** Function Header ******************************
Function Name: tbTipoMedioPagoClose
Author: nefernandez
Date Created: 20/09/2007
Description: Llama al m�todo de cierre del formulario
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTipoMedioPagoForm.tbTipoMedioPagoClose(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author: nefernandez
Date Created: 20/09/2007
Description: Proceso inicial que se ejecuta la crear el formulario. Presenta los
registros de TipoMedioPago en la grilla
Parameters: None
Return Value: Boolean
*******************************************************************************}
function TABMTipoMedioPagoForm.Inicializar: Boolean;
resourcestring
    ERROR_MSG = 'Error de Inicializacion';
var
	S: TSize;
    i, CantidadConceptos: Integer;
begin
    Result := False;
    FormStyle := fsMDIChild;
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);
    CenterForm(Self);
    try

    	if not OpenTables([TipoMedioPago]) then exit;
        TipoMedioPago.Sort := 'CodigoTipoMedioPago ASC';
    	PantallaModoNavegacion;
        lbTipoMedioPago.Reload;
   	    Notebook.PageIndex := 0;

        qryConceptosMovimiento.Active := True;
        CantidadConceptos := qryConceptosMovimiento.RecordCount;
        qryConceptosMovimiento.First;
        cbCodigoConceptoPago.Items.Clear;
        cbCodigoConceptoPago.Items.Add('SELECCIONAR',0);
        for i := 0 to CantidadConceptos - 1 do begin
            cbCodigoConceptoPago.Items.Add(qryConceptosMovimiento.FieldByName('Descripcion').AsString, qryConceptosMovimiento.FieldByName('CodigoConcepto').AsInteger);
            qryConceptosMovimiento.Next;
        end;
        qryConceptosMovimiento.Active := False;

        cbMedioPagoAutomatico.Items.Clear;
        cbMedioPagoAutomatico.Items.Add('No', 0);
        cbMedioPagoAutomatico.Items.Add('Si', 1);

        Result := True;
    except
        On E: exception do begin
            exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: lbTipoMedioPagoClick
Author: nefernandez
Date Created: 20/09/2007
Description: Proceso que se ejecuta al seleccionar un registro de la grilla.
Pone en modo de edici�n y recupera todos los campos del registro seleccionado en
el panel de detalle
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTipoMedioPagoForm.lbTipoMedioPagoClick(Sender: TObject);
begin
    TipoMedioPago.DisableControls;
    RefrescarCamposRegistroActual;
end;

{******************************** Function Header ******************************
Function Name: lbTipoMedioPagoDelete
Author: nefernandez
Date Created: 20/09/2007
Description: Proceso para eliminar el registro seleccionado en la grilla
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTipoMedioPagoForm.lbTipoMedioPagoDelete(Sender: TObject);
resourcestring
	MSG_ELIMINAR_TIPO_MP_CAPTION            = 'Eliminar Tipo de Medio de Pago';
	MSG_ELIMINAR_TIPO_MP_ERROR              = 'No se puede eliminar este Tipo de Medio de Pago porque hay datos que dependen de �l.';
	MSG_ELIMINAR_TIPO_MP_QUESTION           = '�Est� seguro de querer eliminar este Tipo de Medio de Pago?';
	MSG_ELIMINAR_TIPO_MP_QUESTION_CAPTION   = 'Confirmaci�n...';
begin

	Screen.Cursor := crHourGlass;
    try
        If MsgBox(MSG_ELIMINAR_TIPO_MP_QUESTION, MSG_ELIMINAR_TIPO_MP_CAPTION, MB_YESNO) = IDYES then begin
            try
                spEliminarTipoMedioPago.Parameters.ParamByName('@CodigoTipoMedioPago').Value := TipoMedioPago.FieldByName('CodigoTipoMedioPago').AsInteger;
                spEliminarTipoMedioPago.ExecProc;
            except
                On E: exception do begin
                    MsgBoxErr(MSG_ELIMINAR_TIPO_MP_ERROR, E.message, MSG_ELIMINAR_TIPO_MP_CAPTION, MB_ICONSTOP);
                end;
            end;
        end;
        PantallaModoNavegacion;
    finally
	    Screen.Cursor := crDefault;
    end;
end;

{******************************** Function Header ******************************
Function Name: lbTipoMedioPagoDrawItem
Author: nefernandez
Date Created: 20/09/2007
Description: Coloca el valor de las columnas en la grilla
Parameters: Sender: TDBList, Tabla: TDataSet, Rect: TRect, State: TOwnerDrawState, Cols: TColPositions
Return Value: None
*******************************************************************************}
procedure TABMTipoMedioPagoForm.lbTipoMedioPagoDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var
    ObtenerDescripcionConceptoPago: String;
begin
	with Sender.Canvas  do begin
        FillRect(Rect);

        with Tabla do begin
            TextOut(Cols[0], Rect.Top, Trim(Fieldbyname('Descripcion').AsString));
            TextOut(Cols[1], Rect.Top, IIF(Fieldbyname('MedioPagoAutomatico').AsBoolean, 'Si', 'No'));
            ObtenerDescripcionConceptoPago := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerDescripcionConceptoMovimiento(' + QuotedStr(Trim(Fieldbyname('CodigoConceptoPago').AsString)) + ')', 10));
            TextOut(Cols[2], Rect.Top, ObtenerDescripcionConceptoPago);
            TextOut(Cols[3], Rect.Top, Trim(Fieldbyname('CodigoDeServicio').AsString));
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: lbTipoMedioPagoEdit
Author: nefernandez
Date Created: 20/09/2007
Description: Pone la pantalla en modo de edici�n (del registro seleccionado)
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTipoMedioPagoForm.lbTipoMedioPagoEdit(Sender: TObject);
begin
    HabilitarCamposEdicion(True);
    lbTipoMedioPago.Estado := modi;
	txtDescripcion.SetFocus;
end;

{******************************** Function Header ******************************
Function Name: lbTipoMedioPagoInsert
Author: nefernandez
Date Created: 20/09/2007
Description: Pone la pantalla en modo de inserci�n de un nuevo registro
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTipoMedioPagoForm.lbTipoMedioPagoInsert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCamposEdicion(True);

    lbTipoMedioPago.Estado := alta;
	txtDescripcion.SetFocus;
end;

{******************************** Function Header ******************************
Function Name: lbTipoMedioPagoRefresh
Author: nefernandez
Date Created: 20/09/2007
Description: Proceso de refresh de la grilla. Si no hay registros en la grilla,
se vac�a los campos del panel de detalle
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTipoMedioPagoForm.lbTipoMedioPagoRefresh(Sender: TObject);
begin
	if lbTipoMedioPago.Empty then Limpiar_Campos;
end;

end.
