{-----------------------------------------------------------------------------
 Unit Name: frmAbmTiposClientes
 Author:    lcanteros
 Date:      01-Abr-2008
 Purpose:   Administra los datos de tipos de clientes en la base de datos
 History:

Revision 1:
Date: 20-01-2009
Author: mpiazza
Description: Se agrega boton para acceder a detalle mensajes por tipo cliente (ss-777)

Revision 2:
Date: 13-08-2010
Author: jjofre
Description: (SS 690 Semaforos)
        -Se agregan/Modifican los siguientes procedures/funciones/Objetos
            cboSemaforo: TComboBox;
            EditarDatos
            LimpiarCampos
            Modificar
            ModificarDetalleMensajes
            ObtenerTiposDeClientesMensaje
            btnAceptarClick
            btnDetalleMensajesClick


Revision 3:
Date: 03-09-2010
Author: jjofre
Description: (SS 690 Semaforos)  Comentario de Linea // REV 3.  SS_690
        Se modifica la funcion
            BtnDetalleMensajesClick
            FinEditarDatos
            LimpiarCampos

Etiqueta    : 20160614 MGO
Descripci�n : Se env�a CodigoTipoCliente al detalle de mensajes

-----------------------------------------------------------------------------}
unit frmAbmTiposClientes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, Util, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, ADODB, BuscaTab,
  Variants, DMConnection, DPSControls, Peatypes, VariantComboBox, ListBoxEx,
  DBListEx, FrmDetalleMensajesTipoCliente;

type
  TAbmTiposClientesForm = class(TForm)
    AbmToolbar1: TAbmToolbar;
    pnlAbm: TPanel;
    Panel2: TPanel;
    Notebook: TNotebook;
    Label8: TLabel;
    Label9: TLabel;
    spLista: TADOStoredProc;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    DBList1: TAbmList;
    txtDescripcion: TEdit;
    Label1: TLabel;
    cdColores: TColorDialog;
    txtColor: TEdit;
    Led1: TLed;
    btnColor: TBitBtn;
    BtnDetalleMensajes: TBitBtn;
    spObtenerTiposDeClientesMensaje: TADOStoredProc;
    cboSemaforo: TComboBox;  //REV. 2
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure txtTipoClienteChange(Sender: TObject);
    procedure txtDescripcionChange(Sender: TObject);
    procedure btnColorClick(Sender: TObject);
    procedure txtTipoClienteKeyPress(Sender: TObject; var Key: Char);
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure txtDescripcionKeyPress(Sender: TObject; var Key: Char);
    procedure BtnDetalleMensajesClick(Sender: TObject);
  private
	{ Private declarations }
    fMeensajeTipoClienteLista : tlist;
   	procedure LimpiarCampos;
    function DesactivarTipoCliente(): boolean;
    function Modificar(Estado: TAbmState): boolean;
    function ModificarDetalleMensajes(Estado: TAbmState; TipoClienteCodigo: integer): boolean;
    procedure ActualizarLista();
    procedure EditarDatos(TipoAbm: TAbmState);
    procedure FinEditarDatos(Actualizar: boolean);
    procedure CambiarColorLed(NuevoColor: TColor);
    Procedure ObtenerTiposDeClientesMensaje( data : tlist);
  public
    //TASK_002_AUN_20170309-CU.COBO.ADM.COB.103
    function Inicializar(MDIChild, MostrarDetalleMensajes: Boolean): Boolean;
  end;

var
  //TASK_002_AUN_20170309-CU.COBO.ADM.COB.103
  FCodigoTipoCliente: Integer;  // 20160614 MGO

implementation

Const
    SP_MODIF = 'TiposClienteActualizarDatos';
    SP_MODIFDETMENSAJES = 'Actualizar_TiposDeClientesMensaje';
    SP_ELIMINAR = 'TiposClienteEliminar';
    SP_ELIMINARDETMENSAJES = 'BorrarTiposDeClientesMensaje';
    BOTON_DEFAULT = '2';

resourcestring
    MSG_ACTUALIZAR_ERROR 	= 'No se pudieron actualizar los datos del tipo seleccionado.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Tipo Cliente';
    MSG_ERROR_ELIMINAR_TIPO = 'Error al eliminar tipo';
    MSG_ERROR_VACIO = 'Por favor complete el campo ';
    MSG_ERROR_AL_VALIDAR_TIPO_CLIENTE = 'Se a producido un errror al validar el tipo'+ crlf+ 'Por favor intente nuevamente';
    MSG_CONFIRMA_ELIMINAR_TIPO_CLIENTE = 'Confirma eliminar tipo de cliente?';
    CAPTION_TIPO_CLIENTE = 'Sem�foro';
    MSG_TIPO_CLIENTE_ACTIVO = 'El tipo de cliente y color ya existe';
    CAPTION_INFO = 'Informaci�n';
    TXT_TIPO_CLIENTE = 'Sem�foro';
    TXT_DESCRIPCION = 'Descripci�n';
    TXT_COLOR = 'Color';
    CAPTION_COMPLETAR = 'Completar';
    MSG_SMALLINT = 'El numero debe ser menor o igual a 32767';
    MSG_NO_SE_PUEDE_ELIMINAR = 'No se puede eliminar el codigo seleccionado';
    MSG_EXISTEN_PERSONAS = 'Hay registros relacionados con el codigo de cliente';
    TXT_DESCRIPCION_DEFAULT = 'No Clasificado';

{$R *.DFM}

{-----------------------------------------------------------------------------
  Procedure: Inicializar
  Author:    lcanteros
  Date:      29-Feb-2008
  Arguments: MDIChild: Boolean
  Result:    Boolean
  Purpose:   Inicializa el formulario
  //TASK_002_AUN_20170309-CU.COBO.ADM.COB.103
  Revisi�n: Agregu� el par�metro MostrarDetalleMensajes
-----------------------------------------------------------------------------}
function TAbmTiposClientesForm.Inicializar(MDIChild, MostrarDetalleMensajes: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
	if not OpenTables([spLista]) then Exit;
    //TASK_002_AUN_20170309-CU.COBO.ADM.COB.103
    BtnDetalleMensajes.Visible := MostrarDetalleMensajes;
	Result := True;
	DbList1.Reload;
  	Notebook.PageIndex := 0;
end;
{-----------------------------------------------------------------------------
  Procedure: DBList1Delete
  Author:    lcanteros
  Date:      11-Mar-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Marca como eliminado el registro seleccionado
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.DBList1Delete(Sender: TObject);
begin
    if MessageBox(self.Handle, Pchar(MSG_CONFIRMA_ELIMINAR_TIPO_CLIENTE), Pchar(CAPTION_TIPO_CLIENTE), MB_YESNO or MB_ICONQUESTION) = idyes then DesactivarTipoCliente;
    DBList1.Estado := Normal;
    ActualizarLista;
end;
{-----------------------------------------------------------------------------
  Procedure: DBList1DrawItem
  Author:    lcanteros
  Date:      11-Mar-2008
  Arguments: Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Result:    None
  Purpose:   Dibuja las filas de la lista
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
Var
    CeldaColor: TRect;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Trim(FieldByName('CodigoTipoCliente').AsString ));
		TextOut(Cols[1], Rect.Top, Trim(FieldByName('Semaforo').AsString));
		TextOut(Cols[2], Rect.Top, Trim(FieldByName('Descripcion').AsString));
        //TextOut(Cols[3], Rect.Top, Trim(FieldByName('Color').AsString));
        CeldaColor.Left := Cols[3];
        CeldaColor.Top := Rect.Top;
        CeldaColor.Right :=  Cols[3] + 15;
        CeldaColor.Bottom := Rect.Top + 15;
        Brush.Color := HexToInt(FieldByName('Color').AsString);
        Ellipse(CeldaColor);
	end;
end;
{-----------------------------------------------------------------------------
  Procedure: DBList1Edit
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Comienzo de edicion de datos
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.DBList1Edit(Sender: TObject);
resourcestring
    MSG_ERROR_EDITAR = 'Error al editar datos';
    CAPTION_ERROR = 'Error';
begin
    try
        EditarDatos(Modi);
    except
        on ex: Exception do begin
            MsgBoxErr(MSG_ERROR_EDITAR, '', CAPTION_ERROR, 0);
        end;
    end;
end;
{-----------------------------------------------------------------------------
  Procedure: DBList1Refresh
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Refresca lo datos de la lista
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then begin
        LimpiarCampos
	 end;
end;
{-----------------------------------------------------------------------------
  Procedure: DesactivarTipoCliente
  Author:    lcanteros
  Date:      11-Mar-2008
  Arguments: None
  Result:    boolean
  Purpose:   Desactiva el registro seleccionado
-----------------------------------------------------------------------------}
function TAbmTiposClientesForm.DesactivarTipoCliente: boolean;
var spDesactivar: TADOStoredProc;
    CursorActual: TCursor;
begin
    Result := false;
    spDesactivar := TADOStoredProc.Create(Self);
    CursorActual := Screen.Cursor;
    Screen.Cursor := crHourGlass;
    try
      with spDesactivar, Parameters do begin
        Connection := DMConnections.BaseCAC;
        ProcedureName := SP_ELIMINAR;
        Refresh;
        ParamByName('@CodigoTipoCliente').Value := spLista.FieldByName('CodigoTipoCliente').AsInteger;
        ParamByName('@Usuario').Value := UsuarioSistema;
        ExecProc;
      end;
      if spDesactivar.Parameters.ParamByName('@RETURN_VALUE').Value = 1 then
        MsgBoxErr(MSG_NO_SE_PUEDE_ELIMINAR, MSG_EXISTEN_PERSONAS, CAPTION_INFO, 0);

      Result := true;
    finally
        spDesactivar.Close;
        spDesactivar.Free;
        Screen.Cursor := CursorActual;
    end;
    if not DBList1.Table.Bof then
        DBList1.Table.Prior
    else
        DBList1.Table.First;

end;
{-----------------------------------------------------------------------------
  Procedure: EditarDatos
  Author:    lcanteros
  Date:      29-Feb-2008
  Arguments: TipoAbm: TAbmState
  Result:    None
  Purpose:   Ingresa o modifica datos
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.EditarDatos(TipoAbm: TAbmState);
begin
    DbList1.Enabled     := False;
    case TipoAbm of
      Alta:
        begin
            cboSemaforo.Enabled:= True;
            DBList1.Estado      := Alta;
            LimpiarCampos;
            if spLista.IsEmpty then txtDescripcion.Text := TXT_DESCRIPCION_DEFAULT;
            Led1.ColorOn := clWindow;
            FCodigoTipoCliente := 0;    // 20160614 MGO
        end;
      Modi:
        begin
            DBList1.Estado      := modi;
            cboSemaforo.ItemIndex := StrToInt(spLista.FieldByName('Semaforo').AsWideString) -1;
            cboSemaforo.Enabled := False;
            txtDescripcion.Text := spLista.FieldByName('Descripcion').AsWideString;
            txtColor.Text := spLista.FieldByName('Color').AsWideString;
            CambiarColorLed(HexToInt(spLista.FieldByName('Color').AsWideString));
            FCodigoTipoCliente := spLista.FieldByName('CodigoTipoCliente').AsInteger;   // 20160614 MGO
        end;
    end;
    BtnAceptar.Enabled := false;
    Notebook.PageIndex  := 1;
    pnlAbm.Visible      := True;

end;
{-----------------------------------------------------------------------------
  Procedure: LimpiarCampos
  Author:    lcanteros
  Date:      29-Feb-2008
  Arguments:
  Result:    None
  Purpose:  Vacia los controles
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.LimpiarCampos();
begin
    cboSemaforo.ClearSelection;  // REV 3.  SS_690
    txtDescripcion.Clear;
    txtColor.Clear;
end;
{-----------------------------------------------------------------------------
  Procedure: Modificar
  Author:    lcanteros
  Date:      11-Mar-2008
  Arguments: Estado: TAbmState
  Result:    boolean
  Purpose:   Modifica datos del registro
-----------------------------------------------------------------------------}
function TAbmTiposClientesForm.Modificar(Estado: TAbmState) : boolean;
var
  spModificar : TADOStoredProc;
  Clave       : integer;
  iTansaccion : Integer; //Numero de transaccion
    //Retorno: integer;
begin
    Result := False;
    spModificar := TADOStoredProc.Create(Self);
    try
        try
            iTansaccion := DMConnections.BaseCAC.BeginTrans;
            spModificar.Connection := DMConnections.BaseCAC;
            if Estado = Alta then begin
                with spModificar , Parameters do begin
                    ProcedureName := SP_MODIF;
                    Refresh;
                    ParamByName('@TipoOperacion').Value := 1; // inserta
                    ParamByName('@CodigoTipoCliente').Value := 0;
                    ParamByName('@Semaforo').Value := StrToInt(Trim(cboSemaforo.Text));
                    ParamByName('@Descripcion').Value := Trim(txtDescripcion.Text);
                    ParamByName('@Color').Value := Trim(txtColor.Text);
                    ParamByName('@Usuario').Value := UsuarioSistema;
                end;
            end else begin
                with spModificar , Parameters do begin
                    ProcedureName := SP_MODIF;
                    Refresh;
                    ParamByName('@TipoOperacion').Value := 2; // actualiza
                    ParamByName('@CodigoTipoCliente').Value := spLista.FieldByName('CodigoTipoCliente').AsInteger;
                    ParamByName('@Semaforo').Value := StrToInt(Trim(cboSemaforo.Text));
                    ParamByName('@Descripcion').Value := Trim(txtDescripcion.Text);
                    ParamByName('@Color').Value := Trim(txtColor.Text);
                    ParamByName('@Usuario').Value := UsuarioSistema;
                end;
            end;
            spModificar.ExecProc;
            clave := spModificar.Parameters.ParamByName('@CodigoTipoCliente').Value;

            ModificarDetalleMensajes(Estado, clave); //aqui modifico o borro detalles de mensajes segun tipo de usuario
            //Retorno := spModificar.Parameters.ParamByName('@RETURN_VALUE').Value;
            //if Retorno = 1 then MsgBox(MSG_TIPO_CLIENTE_ACTIVO, CAPTION_INFO);
            Result := True;

            DMConnections.BaseCAC.CommitTrans;
        except
            On E: EDataBaseError do begin
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                DMConnections.BaseCAC.RollbackTrans;
			end;
        end;
    finally
        spModificar.Close;
        spModificar.Free;
    end;
    fMeensajeTipoClienteLista.Free;
    fMeensajeTipoClienteLista := nil;
end;
{-----------------------------------------------------------------------------
  Procedure: ModificarDetalleMensajes
  Author:    mpiazza
  Date:      22/01/2009
  Arguments: Estado: TAbmState
  Result:    boolean
  Purpose:   Modificar y dar de alta registros de TiposDeClientesMensaje
  provenientes del formulario frmdetallemensajestipocliente
-----------------------------------------------------------------------------}
function TAbmTiposClientesForm.ModificarDetalleMensajes(
  Estado: TAbmState; TipoClienteCodigo: integer): boolean;
var
    spModificarDetalleMensaje : TADOStoredProc;
    Indice                    : integer;
    ItemMensaje               : TRegistroDetalleMensajeTipoCliente;
begin
    Result := False;
    if fMeensajeTipoClienteLista <> nil then begin
        spModificarDetalleMensaje := TADOStoredProc.Create(Self);
        try
            try
                spModificarDetalleMensaje.Connection := DMConnections.BaseCAC;
                if (Estado = modi) or (Estado = Alta) then begin
                    for Indice := 0 to fMeensajeTipoClienteLista.Count - 1 do begin
                        ItemMensaje := TRegistroDetalleMensajeTipoCliente(fMeensajeTipoClienteLista.Items[Indice]);
                        with spModificarDetalleMensaje , Parameters do begin
                            if ItemMensaje.borrado then begin
                                if ItemMensaje.CodigoTipoClienteMensaje > -1 then begin
                                    ProcedureName := SP_ELIMINARDETMENSAJES;
                                    Refresh;
                                    ParamByName('@CodigoTipoClienteMensaje').Value := ItemMensaje.CodigoTipoClienteMensaje;
                                    spModificarDetalleMensaje.Close;
                                    spModificarDetalleMensaje.ExecProc;
                                end;
                            end else begin
                                if ItemMensaje.modificado then begin
                                    ProcedureName := SP_MODIFDETMENSAJES;
                                    Refresh;
                                    ParamByName('@CodigoTipoClienteMensaje').Value := ItemMensaje.CodigoTipoClienteMensaje;
                                    ParamByName('@FechaHora').Value := nowbase(  DMConnections.BaseCAC);
                                    ParamByName('@CodigoTipoCliente').Value := TipoClienteCodigo;
                                    ParamByName('@Semaforo').Value := ItemMensaje.Semaforo;
                                    ParamByName('@CodigoSistema').Value := ItemMensaje.CodigoSistema;
                                    ParamByName('@Descripcion').Value := ItemMensaje.Descripcion;
                                    ParamByName('@Usuario').Value := UsuarioSistema;
                                    spModificarDetalleMensaje.Close;
                                    spModificarDetalleMensaje.ExecProc;
                                end;
                            end;
                        end;
                        TRegistroDetalleMensajeTipoCliente(fMeensajeTipoClienteLista.Items[Indice]).Free;
                    end;
                end;
                Result := True;
            except
                On E: EDataBaseError do begin
                    MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                end;
            end;
        finally
            spModificarDetalleMensaje.Close;
            spModificarDetalleMensaje.Free;
        end;
    end else begin
        Result := True;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: ObtenerTiposDeClientesMensaje
  Author:    mpiazza
  Date:      21-01-2009
  Arguments: Sender: TObject
  Result:    None
  Purpose: Obtiene los tipodeclientesmensajes y devuelve un array de registros
-----------------------------------------------------------------------------}
Procedure TAbmTiposClientesForm.ObtenerTiposDeClientesMensaje( data : TList);
var
    CantidadMensajes  : integer;
    i                 : Integer;
    Registro          : TRegistroDetalleMensajeTipoCliente;
begin

    with spObtenerTiposDeClientesMensaje, Parameters do begin
        Connection := DMConnections.BaseCAC;
        Refresh;
        ParamByName('@CodigoTipoCliente').Value := spLista.FieldByName('CodigoTipoCliente').AsInteger;
        ParamByName('@Semaforo').Value := spLista.FieldByName('Semaforo').AsInteger;
        active := true;
        Requery();
        CantidadMensajes := RecordCount;
        for i := 0 to CantidadMensajes - 1 do begin
            registro := TRegistroDetalleMensajeTipoCliente.Create;
            with registro  do begin
                CodigoTipoClienteMensaje := FieldByName('CodigoTipoClienteMensaje').AsInteger;
                FechaHora                := FieldByName('FechaHora').AsDateTime ;
                CodigoTipoCliente        := FieldByName('CodigoTipoCliente').asinteger;
                Semaforo                 := FieldByName('Semaforo').asinteger;
                CodigoSistema            := FieldByName('CodigoSistema').asinteger;
                Descripcion              := FieldByName('Descripcion').AsString;
                DescripcionSistema       := FieldByName('DescripcionSistema').AsString;
                modificado               := false;
                borrado                  := false;
            end;
            data.Add( registro ) ;
            MoveBy(1);
        end;
        active := false;
    end;
end;


{-----------------------------------------------------------------------------
  Procedure: pnlColorClick
  Author:    lcanteros
  Date:      11-Mar-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.btnColorClick(Sender: TObject);
begin
    if cdColores.Execute then begin
        txtColor.Text := IntToHex(cdColores.Color, 8);
        CambiarColorLed(cdColores.Color);
        BtnAceptar.Enabled := true;
    end;
end;
{-----------------------------------------------------------------------------
  Procedure: txtDescripcionChange
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Habilita el boton aceptar si se producen cambios en los datos
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.txtDescripcionChange(Sender: TObject);
begin
    if not BtnAceptar.Enabled  then BtnAceptar.Enabled := true;
end;
{-----------------------------------------------------------------------------
  Procedure: txtDescripcionKeyPress
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject; var Key: Char
  Result:    None
  Purpose: Solo acepta caracteres alfanumericos y  espacios en blanco
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.txtDescripcionKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not( Key  in ['0'..'9', 'A'..'Z', 'a'..'z', ' ', #8]) then
        Key := #0;
end;
{-----------------------------------------------------------------------------
  Procedure: txtTipoClienteChange
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Habilita el boton aceptar si se producen cambios en los datos
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.txtTipoClienteChange(Sender: TObject);
begin
    if not BtnAceptar.Enabled  then BtnAceptar.Enabled := true;
end;
{-----------------------------------------------------------------------------
  Procedure: txtTipoClienteKeyPress
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject; var Key: Char
  Result:    None
  Purpose:   Acepta solo numeros
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.txtTipoClienteKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (Key in  ['0'..'9', #8]) then
        Key := #0;
end;
{-----------------------------------------------------------------------------
  Procedure: AbmToolbar1Close
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Accion del boton cerrar de la toolbar se cierra el formulario
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;
{-----------------------------------------------------------------------------
  Procedure: DBList1Insert
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Comienza la insercion de datos
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.DBList1Insert(Sender: TObject);
begin
    EditarDatos(Alta);
end;
{-----------------------------------------------------------------------------
  Procedure: ActualizarLista
  Author:    lcanteros
  Date:      28-Feb-2008
  Arguments: None
  Result:    refresca los datos de la lista
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.ActualizarLista;
begin
    spLista.Close;
    spLista.Open;
end;
{-----------------------------------------------------------------------------
  Procedure: BtnAceptarClick
  Author:    lcanteros
  Date:      28-Feb-2008
  Arguments: Sender: TObject
  Result:    aplica los cambios seleccionados
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_CONFIRM_CHANGE = 'Confirma aceptar datos ?';
 begin
    if not ValidateControls(
        [cboSemaforo ,
        txtDescripcion,
        btnColor],
        [Trim(cboSemaforo.Text) <> EmptyStr,
        trim(txtDescripcion.Text) <> EmptyStr,
        Trim(txtColor.Text) <> EmptyStr],
        CAPTION_COMPLETAR,
        [MSG_ERROR_VACIO + TXT_TIPO_CLIENTE,
        MSG_ERROR_VACIO + TXT_DESCRIPCION,
        MSG_ERROR_VACIO + TXT_COLOR]) then exit;

        if Modificar(DBList1.Estado) then begin
            Screen.Cursor := crHourGlass;
            FinEditarDatos(True);
        end;

end;
{-----------------------------------------------------------------------------
  Procedure: FormShow
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Recarga la lista de datos al mostrar el formulario
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;
{-----------------------------------------------------------------------------
  Procedure: FinEditarDatos
  Author:    lcanteros
  Date:      28-Feb-2008
  Arguments: Actualizar: boolean
  Result:    finaliza proceso de edicion
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.FinEditarDatos(Actualizar: boolean);
var RegistroActual: Pointer;

begin
    pnlAbm.Visible     := False;
    DbList1.Estado     := Normal;
  fMeensajeTipoClienteLista.Free;
  fMeensajeTipoClienteLista := nil;
	Notebook.PageIndex := 0;
	Screen.Cursor 	   := crDefault;
    if Actualizar then begin
        RegistroActual := DBList1.Table.GetBookmark;
        ActualizarLista;
        DBList1.Reload;
        if not DBList1.Table.IsEmpty then
            if DBList1.Table.BookmarkValid(RegistroActual) then DBList1.Table.GotoBookmark(RegistroActual);
        cboSemaforo.ClearSelection;  // REV 3.  SS_690

    end;
    DbList1.Enabled    := True;
    DbList1.SetFocus;

end;
{-----------------------------------------------------------------------------
  Procedure: FormClose
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject; var Action: TCloseAction
  Result:    None
  Purpose:   libera la instancia del form al cerrar
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;
{-----------------------------------------------------------------------------
  Procedure: BtnDetalleMensajesClick
  Author:    mpiazza
  Date:      20/01/2009
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Llama al formulario Detalle de mensajes por tipo cliente
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.BtnDetalleMensajesClick(Sender: TObject);
var
	f: TDetalleMensajesTipoClienteForm;
begin
    if DBList1.Estado = modi then begin
        if fMeensajeTipoClienteLista = nil then begin
            fMeensajeTipoClienteLista := tlist.create();
            ObtenerTiposDeClientesMensaje(fMeensajeTipoClienteLista);
        end;
    end else begin
        fMeensajeTipoClienteLista := nil;
        fMeensajeTipoClienteLista := tlist.create();
    end;

    // SS_690_20100901
    try
        f := TDetalleMensajesTipoClienteForm.Create(Self);
        { INICIO : 20160614 MGO
		if f.Inicializar(True, 0 , StrToInt(cboSemaforo.Text), fMeensajeTipoClienteLista) then
        }
        if f.Inicializar(True, FCodigoTipoCliente, StrToInt(cboSemaforo.Text), fMeensajeTipoClienteLista) then
        // FIN : 20160614 MGO
            f.ShowModal;
    finally
        if assigned(f) then FreeAndNil(f);
        if not BtnAceptar.Enabled  then BtnAceptar.Enabled := true;
    end;

    {
	if FindFormOrCreate(TDetalleMensajesTipoClienteForm, f) then
		f.ShowModal
	else begin
		if not f.Inicializar(True, spLista.FieldByName('CodigoTipoCliente').AsInteger, spLista.FieldByName('Semaforo').AsInteger, fMeensajeTipoClienteLista) then f.Release;
        if not BtnAceptar.Enabled  then BtnAceptar.Enabled := true;
	end;
    }
    // Fin SS_690_20100901
end;

{-----------------------------------------------------------------------------
  Procedure: BtnSalirClick
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Cierra el formulario
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.BtnSalirClick(Sender: TObject);
begin
     Close;
end;
{-----------------------------------------------------------------------------
  Procedure: CambiarColorLed
  Author:    lcanteros
  Date:      11-Mar-2008
  Arguments: NuevoColor: TColor
  Result:    None
  Purpose:   Aplica el color al control led
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.CambiarColorLed(NuevoColor: TColor);
begin
    Led1.ColorOn := NuevoColor;
end;
{-----------------------------------------------------------------------------
  Procedure: BtnCancelarClick
  Author:    lcanteros
  Date:      01-Abr-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Cancela edicion de datos
-----------------------------------------------------------------------------}
procedure TAbmTiposClientesForm.BtnCancelarClick(Sender: TObject);
begin

    FinEditarDatos(false);
end;

end.
