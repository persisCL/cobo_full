unit FrmGestionMensajes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx, DB, ADODB;

type
  TFormGestionMensajes = class(TForm)
    Panel2: TPanel;
    Notebook: TNotebook;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    DBList: TDBListEx;
    ds_Mensaje: TDataSource;
    pnl_Top: TGroupBox;
    Label1: TLabel;
    cb_Mensaje: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    cb_Sistema: TComboBox;
    cb_Formaulario: TComboBox;
    vw_mensajesayuda: TADOTable;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializa(MDIChild: Boolean): Boolean;
  end;

var
  FormGestionMensajes: TFormGestionMensajes;

implementation

uses DMConnection;

{$R *.dfm}

procedure TFormGestionMensajes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormGestionMensajes.BtnSalirClick(Sender: TObject);
begin
    close;
end;

function TFormGestionMensajes.Inicializa(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
    
	Notebook.PageIndex := 0;
	Result := True;
end;

end.
