unit frmColores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, ActnMan, ActnColorMaps, StdCtrls, CategoryButtons,
  DB, ADODB, Grids, DBGrids, ComCtrls;

type
  TColoresForm = class(TForm)
    MainMenu: TMainMenu;
    Opcion11: TMenuItem;
    Opcion111: TMenuItem;
    Opcion121: TMenuItem;
    Opcion131: TMenuItem;
    Opcion21: TMenuItem;
    Opcion211: TMenuItem;
    Opcion221: TMenuItem;
    Opcion231: TMenuItem;
    Opcion2311: TMenuItem;
    Opcion2321: TMenuItem;
    Opcion2331: TMenuItem;
    qryEsquemasColores: TADOQuery;
    qryEsquemasColoresIdColor: TLargeintField;
    qryEsquemasColoresDescripcion: TStringField;
    qryEsquemasColoresColorNormal: TStringField;
    qryEsquemasColoresColorTexto: TStringField;
    qryEsquemasColoresColorSeleccionado: TStringField;
    qryEsquemasColoresColorTextoSeleccionado: TStringField;
    DBGEsquemasColores: TDBGrid;
    dsEsquemasColores: TDataSource;
    pnlPrincipal: TPanel;
    pnlAbajo: TPanel;
    pnlEjemplos: TPanel;
    Splitter1: TSplitter;
    pnlEsquemasColores: TPanel;
    Panel1: TPanel;
    btnGrabar: TButton;
    pnlEjemploIngreso: TPanel;
    PCEjemplo: TPageControl;
    TSEjemplo1: TTabSheet;
    TSEjemplo2: TTabSheet;
    lblTexto: TLabel;
    EEjemplo: TEdit;
    lblCombo: TLabel;
    CBEjemplo: TComboBox;
    btnBoton: TButton;
    qryFijarEsquema: TADOQuery;
    LargeintField1: TLargeintField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    qryLimpiarEsquemas: TADOQuery;
    LargeintField2: TLargeintField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    procedure DrawMenuItem(Sender: TObject; ACanvas: TCanvas; ARect: TRect;
      Selected: Boolean);
    procedure FormShow(Sender: TObject);
    procedure DBGEsquemasColoresDblClick(Sender: TObject);
    procedure PCEjemploDrawTab(Control: TCustomTabControl; TabIndex: Integer;
      const Rect: TRect; Active: Boolean);
    procedure btnGrabarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FColorMenu, FColorMenuSel, FColorText, FColorTextSel:TColor;
    function CambiarEventoMenu(Menu: TMenu): Boolean;

  end;

var
  ColoresForm: TColoresForm;

implementation

uses DMConnection;
{$R *.dfm}


procedure TColoresForm.btnGrabarClick(Sender: TObject);
begin
  qryLimpiarEsquemas.ExecSQL;
  //qryFijarEsquema.Close;
  //Prepared := true;
  ShowMessage(qryFijarEsquema.Parameters.Items[0].Name);
  qryFijarEsquema.Parameters.ParamByName('Parametro').Value := True; //qryEsquemasColoresIdColor.Value;
  qryFijarEsquema.ExecSQL;

end;

function TColoresForm.CambiarEventoMenu(Menu: TMenu): Boolean;

	Procedure CambiarEventoItem(Nivel: Integer; Item: TmenuItem);
	Var
		i: Integer;
	begin
    if (Nivel <> 0) then
       Item.OnDrawItem := DrawMenuItem;

		for i := 0 to Item.Count - 1 do begin
			CambiarEventoItem(Nivel + 1, Item.Items[i]);
		end;
	end;

begin
	CambiarEventoItem(0, Menu.Items);
	Result := True;
end;


procedure TColoresForm.DBGEsquemasColoresDblClick(Sender: TObject);
begin
    FColorMenu:=RGB( StrToInt('$'+Copy(Trim(qryEsquemasColoresColorNormal.Text), 6, 2)),
                     StrToInt('$'+Copy(Trim(qryEsquemasColoresColorNormal.Text), 4, 2)),
                     StrToInt('$'+Copy(Trim(qryEsquemasColoresColorNormal.Text), 2, 2))
                   );
    FColorText:=RGB( StrToInt('$'+Copy(Trim(qryEsquemasColoresColorTexto.Text), 6, 2)),
                     StrToInt('$'+Copy(Trim(qryEsquemasColoresColorTexto.Text), 4, 2)),
                     StrToInt('$'+Copy(Trim(qryEsquemasColoresColorTexto.Text), 2, 2))
                   );
    FColorMenuSel:=RGB( StrToInt('$'+Copy(Trim(qryEsquemasColoresColorSeleccionado.Text), 6, 2)),
                        StrToInt('$'+Copy(Trim(qryEsquemasColoresColorSeleccionado.Text), 4, 2)),
                        StrToInt('$'+Copy(Trim(qryEsquemasColoresColorSeleccionado.Text), 2, 2))
                   );
    FColorTextSel:=RGB( StrToInt('$'+Copy(Trim(qryEsquemasColoresColorTextoSeleccionado.Text), 6, 2)),
                        StrToInt('$'+Copy(Trim(qryEsquemasColoresColorTextoSeleccionado.Text), 4, 2)),
                        StrToInt('$'+Copy(Trim(qryEsquemasColoresColorTextoSeleccionado.Text), 2, 2))
                      );

  pnlEjemplos.Color := FColorMenu;
  PCEjemplo.Canvas.Brush.Color := FColorMenu;
  PCEjemplo.Canvas.Font.Color := FColorText;


  Invalidate;
  Update;
  Refresh;
  Repaint;
  Application.ProcessMessages;

end;

procedure TColoresForm.DrawMenuItem(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
begin
  with ACanvas do
  begin

    if Selected then
    begin
      Brush.Color := FColorMenuSel;
      Font.Color := FColorTextSel;
    end
    else
    begin
      Brush.Color := FCOlorMEnu;
      Font.Color := FColorText;
    end;

    if (Parent = nil) and (TMenuItem(Sender).MenuIndex = 1 ) and not Selected then
        ARect.Right := Width;

    FillRect(ARect);
    DrawText(ACanvas.Handle, PChar(TMenuItem(Sender).Caption), Length(TMenuItem(Sender).Caption), ARect, DT_SINGLELINE or DT_VCENTER);
  end;
end;



procedure TColoresForm.FormShow(Sender: TObject);
begin
 qryEsquemasColores.Open;
 CambiarEventoMenu(MainMenu);
end;

procedure TColoresForm.PCEjemploDrawTab(Control: TCustomTabControl;
  TabIndex: Integer; const Rect: TRect; Active: Boolean);
var
  AText: string;
  APoint: TPoint;
begin
  with (Control as TPageControl).Canvas do
  begin
    if Active then
    begin
      Brush.Color := FColorMenuSel;
      Font.Color := FColorTextSel;
    end
    else
    begin
      Brush.Color := FColorMenu;
      Font.Color := FColorText;
    end;
    FillRect(Rect);
    AText := TPageControl(Control).Pages[TabIndex].Caption;
    with Control.Canvas do
    begin
      APoint.x := (Rect.Right - Rect.Left) div 2 - TextWidth(AText) div 2;
      APoint.y := (Rect.Bottom - Rect.Top) div 2 - TextHeight(AText) div 2;
      TextRect(Rect, Rect.Left + APoint.x, Rect.Top + APoint.y, AText);
    end;
  end;
end;

end.
