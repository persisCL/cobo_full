unit ABMColores;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DPSControls;

type
  TFormColores = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Notebook: TNotebook;
    Colores: TADOTable;
    Label15: TLabel;
    txt_CodigoColor: TNumericEdit;
    qry_MaxColor: TADOQuery;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
	procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    function DBList1Process(Tabla: TDataSet; var Texto: String): Boolean;
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
  public
    { Public declarations }
	function Inicializa(MDIChild: Boolean): Boolean;
  end;

var
  FormColores: TFormColores;

implementation
resourcestring
    MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Color seleccionado?';
    MSG_DELETE_ERROR		= 'No se pudo eliminar el Color seleccionado.';
    MSG_DELETE_CAPTION		= 'Eliminar Color';
    MSG_ACTUALIZAR_ERROR    = 'No se pudieron actualizar los datos del Color.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Color';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n';
    
{$R *.DFM}

function TFormColores.Inicializa(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
	if not OpenTables([Colores]) then exit;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	Result := True;
end;

procedure TFormColores.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFormColores.DBList1Click(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	    txt_CodigoColor.Value	:= FieldByName('CodigoColor').AsInteger;
		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
	end;
end;

procedure TFormColores.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoColor').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TFormColores.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    		:= False;
    dblist1.Estado     		:= modi;
	Notebook.PageIndex 		:= 1;
    groupb.Enabled     		:= True;
    txt_CodigoColor.Enabled	:= False;
    txt_Descripcion.setFocus;
end;

procedure TFormColores.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormColores.Limpiar_Campos();
begin
	txt_CodigoColor.Clear;
	txt_Descripcion.Clear;
end;

procedure TFormColores.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormColores.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
			(Sender AS TDbList).Table.Delete;
		Except
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormColores.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    txt_CodigoColor.Enabled := False;
	txt_Descripcion.SetFocus;
end;

procedure TFormColores.BtnAceptarClick(Sender: TObject);
begin
    if not ValidateControls([txt_Descripcion],
                [trim(txt_Descripcion.text) <> ''],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_DESCRIPCION]) then exit;
 	Screen.Cursor := crHourGlass;
	With DbList1.Table do begin
		Try
			if DbList1.Estado = Alta then begin
            	Append;
                qry_MaxColor.Open;
                txt_CodigoColor.Value := qry_MaxColor.FieldByNAme('CodigoColor').AsInteger + 1;
				qry_MaxColor.Close;
			end else begin
            	Edit;
            end;
			FieldByName('CodigoColor').AsInteger 	:= Trunc(txt_CodigoColor.Value);
			if Trim(txt_Descripcion.text) = '' then
            	FieldByName('Descripcion').Clear
            else
				FieldByName('Descripcion').AsString	 := Trim(txt_Descripcion.text);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormColores.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormColores.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormColores.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormColores.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
    txt_CodigoColor.Enabled := True;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

function TFormColores.DBList1Process(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Texto := Tabla.FieldByName('CodigoColor').AsString + ' ' +
	  Tabla.FieldByName('Descripcion').AsString;
	Result := True;
end;

end.
