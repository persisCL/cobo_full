{********************************** File Header ********************************
File Name   : ABMPreguntas.pas
Author      : dcalani
Date Created: 10/03/2004
Language    : ES-AR
Description : Se encarga de la Gestion de las Preguntas que luego se utilizaran
              en las encuestas.
*******************************************************************************}

unit ABMPreguntas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, Util, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, ADODB, BuscaTab,
  Variants, DMConnection, DPSControls, Peatypes, ListBoxEx, DBListEx,
  Provider, DBClient, Validate, DateEdit, ABMPreguntaSolicitud, FrmHacerEncuesta,
  RStrings;

type
  TFormPreguntas = class(TForm)
    Panel1: TPanel;
    pnl_BotonesGeneral: TPanel;
    Notebook: TNotebook;
    ATGrupoPreguntas: TAbmToolbar;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Dbl_Preguntas: TAbmList;
    ObtenerOpcionesPreguntas: TADOStoredProc;
    dsPregunta: TDataSource;
    dspPregunta: TDataSetProvider;
    cdsPregunta: TClientDataSet;
    Panel4: TPanel;
    GroupBox4: TGroupBox;
    Panel5: TPanel;
    GroupBox1: TGroupBox;
    dbl_OpcionesPreguntas: TDBListEx;
    dbl_Categoria: TDBListEx;
    ObtenerPreguntas: TADOStoredProc;
    ObtenerFuentesxPregunta: TADOStoredProc;
    DSSolicitud: TDataSource;
    cdsSolicitud: TClientDataSet;
    dspSolicitud: TDataSetProvider;
    cdsSolicitudCodigoPregunta: TIntegerField;
    cdsSolicitudCodigoFuenteSolicitud: TIntegerField;
    cdsSolicitudProbabilidad: TIntegerField;
    cdsSolicitudFuentesSolicitud: TStringField;
    cdsPreguntaCodigoPregunta: TIntegerField;
    cdsPreguntaCodigoOpcionesPregunta: TSmallintField;
    cdsPreguntaDescripcion: TStringField;
    ActualizarMaestroPreguntas: TADOStoredProc;
    EliminarMaestroPreguntas: TADOStoredProc;
    ActualizarMaestroOpcionesPreguntas: TADOStoredProc;
    ActualizarPreguntasFuenteSolicitud: TADOStoredProc;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    txt_descripcion: TEdit;
    cb_TipoPregunta: TComboBox;
    cb_TipoDato: TComboBox;
    cb_PreguntasBorradas: TCheckBox;
    Label1: TLabel;
    txt_desde: TDateEdit;
    Label5: TLabel;
    txt_hasta: TDateEdit;
    Panel6: TPanel;
    Label6: TLabel;
    Panel7: TPanel;
    Label7: TLabel;
    btn_Orden: TUpDown;
    btn_InsertarCategoria: TButton;
    btn_BorrarCategoria: TButton;
    btn_ModificarCategoria: TButton;
    btn_Insertar: TButton;
    btn_Borrar: TButton;
    btn_Modificar: TButton;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnCancelarClick(Sender: TObject);
    procedure Dbl_PreguntasInsert(Sender: TObject);
    procedure Dbl_PreguntasEdit(Sender: TObject);
    procedure Dbl_PreguntasClick(Sender: TObject);
    procedure Dbl_PreguntasDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure Dbl_PreguntasRefresh(Sender: TObject);
    function Dbl_PreguntasProcess(Tabla: TDataSet;
      var Texto: String): Boolean;
    procedure ATGrupoPreguntasClose(Sender: TObject);
    procedure btn_InsertarClick(Sender: TObject);
    procedure btn_BorrarClick(Sender: TObject);
    procedure cdsPreguntaAfterDelete(DataSet: TDataSet);
    procedure FormPaint(Sender: TObject);
    procedure cb_TipoPreguntaChange(Sender: TObject);
    procedure cdsSolicitudAfterDelete(DataSet: TDataSet);
    procedure btn_InsertarCategoriaClick(Sender: TObject);
    procedure btn_BorrarCategoriaClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure Dbl_PreguntasDelete(Sender: TObject);
    procedure btn_ModificarClick(Sender: TObject);
    procedure btn_ModificarCategoriaClick(Sender: TObject);
    procedure cb_PreguntasBorradasClick(Sender: TObject);
    procedure btn_OrdenClick(Sender: TObject; Button: TUDBtnType);
  private
    procedure HabilitarCamposEdicion(Habilitar:Boolean;SoloFuenteSolicitud:Boolean=False);
    procedure Limpiar_Campos;
    procedure Volver_Campos;
    function DarCodigoTipoPregunta:Integer;
    function DarCodigoTipoDato:Integer;
    function FueradeVigencia:Boolean;
  public
    function Inicializar(MDIChild: Boolean): Boolean;
  end;

var
  FormPreguntas: TFormPreguntas;

implementation

resourcestring

    MSG_CAPTION_PREGUNTA = 'Pregunta de encuestas';
    MSG_BORRAR_PREGUNTA = '�Esta seguro de elimunar esta Pregunta?';
    MSG_ERROR_DUPLICADO_FUENTE = 'La fuente ya se encuentra cargado.';
    MSG_ERROR_DESCRIP = 'Debe especificar una Descripcion.';
    MSG_ERROR_FUENTES = 'Debe especificar por lo menos una fuente.';
    MSG_ERROR_ITEM1 = 'Debe Espeficificar por lo menos un Itmes.';
    MSG_ERROR_ITEM2 = 'Debe Espeficificar por lo menos dos Itmes.';
    MSG_ACTUALIZAR_ERROR = 'No se pudo insertar/actualizar la pregunta.';
    MSG_ERROR_BORRAR_PREGUNTA = 'No se pudo borrar la pregunta.';
    MSG_ERROR_BORRAR_ITEM = 'No se puede borrar este Item porque ya forma parte de una respuesta.';
{$R *.dfm}

procedure TFormPreguntas.BtnSalirClick(Sender: TObject);
begin
    close;
end;

procedure TFormPreguntas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     action := caFree;
end;

function TFormPreguntas.Inicializar(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	Result := False;

    cdsPregunta.CreateDataSet;
    cdsSolicitud.CreateDataSet;

    Volver_Campos;

    ObtenerPreguntas.Parameters.ParamByName('@baja').Value:=0;

    if not(OpenTables([ObtenerPreguntas])) then exit;

    Dbl_Preguntas.Reload;

   	Notebook.PageIndex := 0;
	Result := True;
end;

procedure TFormPreguntas.HabilitarCamposEdicion(Habilitar:Boolean;SoloFuenteSolicitud:Boolean=False);
begin
    Dbl_Preguntas.Enabled:=not(Habilitar);
    cb_TipoPregunta.Enabled:=Dbl_Preguntas.Enabled;

    txt_descripcion.Enabled:=Habilitar and not(SoloFuenteSolicitud);
    cb_TipoPregunta.Enabled:=txt_descripcion.Enabled;
    cb_TipoDato.Enabled:=txt_descripcion.Enabled;
    if cb_TipoDato.Enabled then cb_TipoPregunta.OnChange(cb_TipoPregunta);
    txt_desde.Enabled:=txt_descripcion.Enabled;
    if not(SoloFuenteSolicitud) then txt_hasta.Enabled := Habilitar
    else txt_hasta.Enabled:= not(FueradeVigencia);

    dbl_Categoria.Enabled:=not(Dbl_Preguntas.Enabled);
    btn_InsertarCategoria.Enabled:=dbl_Categoria.Enabled;
    cdsSolicitud.AfterOpen(cdsSolicitud);

    Notebook.PageIndex:=ord(habilitar);
    if Habilitar then begin
        if SoloFuenteSolicitud then dbl_Categoria.SetFocus
        else txt_descripcion.SetFocus;
    end else begin
        Dbl_Preguntas.Reload;
        Dbl_Preguntas.SetFocus;
    end;
end;

procedure TFormPreguntas.Limpiar_Campos;
begin
    txt_descripcion.Clear;
    txt_desde.Clear;
    txt_hasta.Clear;
    cdsPregunta.EmptyDataSet;
    cdsSolicitud.EmptyDataSet;
    CargarTiposPregunta(DMConnections.BaseCAC,cb_TipoPregunta);
    CargarTiposDatoPregunta(DMConnections.BaseCAC,cb_TipoDato,DarCodigoTipoPregunta);
    cb_TipoPregunta.OnChange(cb_TipoPregunta);
end;
procedure TFormPreguntas.Volver_Campos;
begin
    Dbl_Preguntas.Estado:=Normal;
	Limpiar_Campos;
    HabilitarCamposEdicion(false);
end;

procedure TFormPreguntas.BtnCancelarClick(Sender: TObject);
begin
    Volver_Campos;
    Dbl_Preguntas.Reload;
end;

procedure TFormPreguntas.Dbl_PreguntasInsert(Sender: TObject);
begin
    Limpiar_Campos;
    HabilitarCamposEdicion(true);
end;

procedure TFormPreguntas.Dbl_PreguntasEdit(Sender: TObject);
begin
    HabilitarCamposEdicion(true,ObtenerPreguntas.FieldByName('Realizada').AsBoolean);
end;

procedure TFormPreguntas.Dbl_PreguntasClick(Sender: TObject);
begin
    with ObtenerPreguntas do begin
        txt_descripcion.Text:=FieldByName('Descripcion').AsString;
        txt_desde.Date:=FieldByName('FechaInicio').AsDateTime;
        txt_hasta.Date:=FieldByName('FechaFin').AsDateTime;

        CargarTiposPregunta(DMConnections.BaseCAC,cb_TipoPregunta,FieldByName('CodigoTipoPregunta').AsInteger);
        cb_TipoPregunta.OnChange(cb_TipoPregunta);
        CargarTiposDatoPregunta(DMConnections.BaseCAC,cb_TipoDato,DarCodigoTipoPregunta,FieldByName('CodigoTipoDato').AsInteger);

        ObtenerOpcionesPreguntas.close;
        ObtenerOpcionesPreguntas.Parameters.ParamByName('@CodigoPregunta').Value:=FieldByName('CodigoPregunta').Value;
        if not OpenTables([ObtenerOpcionesPreguntas]) then exit;
        cdsPregunta.Data:=dspPregunta.Data;

        ObtenerFuentesxPregunta.close;
        ObtenerFuentesxPregunta.Parameters.ParamByName('@CodigoPregunta').Value:=FieldByName('CodigoPregunta').Value;
        if not OpenTables([ObtenerFuentesxPregunta]) then exit;
        cdsSolicitud.Data:=dspSolicitud.Data;


        if FieldByName('Baja').AsBoolean then ATGrupoPreguntas.Habilitados:=[btAlta, btSalir, btBuscar]
        else begin
            if FueradeVigencia then ATGrupoPreguntas.Habilitados:=[btAlta, btSalir, btBuscar, btBaja]
            else ATGrupoPreguntas.Habilitados:=[btAlta, btModi, btSalir, btBuscar, btBaja];
        end;

    end;
end;

procedure TFormPreguntas.Dbl_PreguntasDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
	With Sender.Canvas do begin

        if Tabla.FieldByName('baja').AsBoolean then begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clYellow
            else
                Sender.Canvas.Font.Color := clRed;
		end else begin
            if (Tabla.FieldByName('FechaFin').AsDateTime<>0) and (Tabla.FieldByName('FechaFin').AsDateTime<now) then begin
                if odSelected in State then
                    Sender.Canvas.Font.Color := clYellow
                else
                    Sender.Canvas.Font.Color := clGreen;
            end else begin
                if odSelected in State then
                    Sender.Canvas.Font.Color := clWindow
                else
                    Sender.Canvas.Font.Color := clWindowText;
            end;
		end;

		FillRect(Rect);
        TextOut(Cols[0] + 1, Rect.Top,Tabla.FieldByName('Descripcion').AsString);
        TextOut(Cols[1] + 1, Rect.Top,Tabla.FieldByName('TiposPregunta').AsString);
        TextOut(Cols[2] + 1, Rect.Top,Tabla.FieldByName('TiposDatoPregunta').AsString);
        TextOut(Cols[3] + 1, Rect.Top,iif(Tabla.FieldByName('Realizada').AsBoolean,MSG_SI,MSG_NO));
        TextOut(Cols[4] + 1, Rect.Top,FormatDateTime('dd/mm/yyyy',Tabla.FieldByName('FechaInicio').AsDateTime));
        TextOut(Cols[5] + 1, Rect.Top,iif(Tabla.FieldByName('FechaFin').AsDateTime=0,'',FormatDateTime('dd/mm/yyyy',Tabla.FieldByName('FechaFin').AsDateTime)));
    end;
end;

procedure TFormPreguntas.Dbl_PreguntasRefresh(Sender: TObject);
begin
	if Dbl_Preguntas.Empty then Limpiar_Campos;
end;

function TFormPreguntas.Dbl_PreguntasProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
    with Tabla do begin
        Texto:=fieldbyname('Descripcion').asstring;
    end;
	Result := True;
end;

procedure TFormPreguntas.ATGrupoPreguntasClose(Sender: TObject);
begin
    close;
end;

procedure TFormPreguntas.btn_InsertarClick(Sender: TObject);
var
    aux: String;
begin
    if InputQuery(MSG_CAPTION_PREGUNTA, self.caption, aux) and (trim(aux)<>'') then begin
        with cdsPregunta do begin
            Append;
            FieldbyName('Descripcion').AsString:=trim(aux);
            Post;
        end;
    end;
    dbl_OpcionesPreguntas.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_BorrarClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormPreguntas.btn_BorrarClick(Sender: TObject);
var
    Aux_CodigoOpcionesPregunta:Integer;
begin
    if MsgBox(MSG_QUESTION_BAJA_ESTA_SEGURO, MSG_CAPTION_PREGUNTA, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        with cdsPregunta do
            Aux_CodigoOpcionesPregunta:=QueryGetValueInt(DMConnections.BaseCAC,
            format('SELECT CodigoOpcionesPregunta FROM RespuestasxPreguntasMultiples  WITH (NOLOCK) WHERE CodigoPregunta = %d AND CodigoOpcionesPregunta = %d',
            [FieldByname('CodigoPregunta').AsInteger,FieldByname('CodigoOpcionesPregunta').AsInteger]));
        if Aux_CodigoOpcionesPregunta > 0 then
            MsgBox(MSG_ERROR_BORRAR_ITEM,MSG_CAPTION_PREGUNTA,MB_ICONSTOP)
        else begin
            cdsPregunta.Delete;
            dbl_OpcionesPreguntas.SetFocus;
        end;
    end;
end;

procedure TFormPreguntas.cdsPreguntaAfterDelete(DataSet: TDataSet);
begin
    //btn_Borrar.Enabled:=not(Dbl_Preguntas.Enabled) and (cdsPregunta.Active) and (cdsPregunta.RecordCount>0);
    btn_Borrar.Enabled:= (btn_Insertar.Enabled) and (cdsPregunta.Active) and (cdsPregunta.RecordCount>0);
    btn_Modificar.Enabled:=btn_Borrar.Enabled;
end;

procedure TFormPreguntas.FormPaint(Sender: TObject);
begin
    //dbl_OpcionesPreguntas.Columns.Items[0].Width:=dbl_OpcionesPreguntas.Width-10;
end;

procedure TFormPreguntas.cb_TipoPreguntaChange(Sender: TObject);
begin
    CargarTiposDatoPregunta(DMConnections.BaseCAC,cb_TipoDato,DarCodigoTipoPregunta);
    cb_TipoDato.Enabled := not (Dbl_Preguntas.Enabled) and (cb_TipoDato.Items.Count > 1);
    btn_insertar.Enabled:=  (txt_descripcion.Enabled) and
                            ((DarCodigoTipoPregunta = CONST_PREGUNTA_MULTIPLE) or
                            (DarCodigoTipoPregunta = CONST_PREGUNTA_EXCLUYENTE));
    btn_Orden.Enabled:=btn_Insertar.Enabled;
    dbl_OpcionesPreguntas.Enabled:= {(txt_descripcion.Enabled)  and } ((DarCodigoTipoPregunta=CONST_PREGUNTA_MULTIPLE) or (DarCodigoTipoPregunta=CONST_PREGUNTA_EXCLUYENTE));
    if not(dbl_OpcionesPreguntas.Enabled) then cdsPregunta.EmptyDataSet;
    cdsPregunta.AfterOpen(cdsPregunta);
end;

procedure TFormPreguntas.cdsSolicitudAfterDelete(DataSet: TDataSet);
begin
    btn_BorrarCategoria.Enabled := not(Dbl_Preguntas.Enabled) and (cdsSolicitud.Active) and (cdsSolicitud.RecordCount>0);
    btn_ModificarCategoria.Enabled:=btn_BorrarCategoria.Enabled;
end;

procedure TFormPreguntas.btn_InsertarCategoriaClick(Sender: TObject);
var
    f:TFormPreguntaSolicitud;
    error:Boolean;
begin
    Application.CreateForm(TFormPreguntaSolicitud,f);
    if (f.inicializar()) and (f.ShowModal=mrOk) then
    begin
        with cdsSolicitud do begin
            error:=false;
            first;
            while not(eof) do begin
                if FieldByName('CodigoFuenteSolicitud').AsInteger=f.CodigoFuenteSolicitud then begin
                    error:=true;
                    MsgBox(MSG_ERROR_DUPLICADO_FUENTE,MSG_CAPTION_PREGUNTA,MB_ICONSTOP);
                end;
                next;
            end;

            if not(error) then begin
                Append;
                FieldbyName('CodigoFuenteSolicitud').AsInteger:=f.CodigoFuenteSolicitud;
                FieldbyName('Probabilidad').AsInteger:=f.Probabilidad;
                FieldbyName('FuentesSolicitud').AsString:=f.FuenteSolicitud;
                Post;
            end;
            first;
        end;
    end;
    dbl_Categoria.SetFocus;
    f.Release;

end;

procedure TFormPreguntas.btn_BorrarCategoriaClick(Sender: TObject);
var STR_FUENTE:ansistring;
begin
    if MsgBox(format(MSG_QUESTION_BAJA,[STR_FUENTE]), MSG_CAPTION_PREGUNTA, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            cdsSolicitud.Delete;
            dbl_Categoria.SetFocus;
    end;
end;

procedure TFormPreguntas.BtnAceptarClick(Sender: TObject);
var
    nroPregunta:integer;
begin
    if not(ValidateControls([txt_descripcion,dbl_Categoria,txt_desde,txt_desde,txt_hasta],
        [trim(txt_descripcion.Text)<>'',
        dbl_Categoria.DataSource.DataSet.RecordCount>0,
         txt_desde.date<>nulldate,
         ((txt_desde.Enabled) and (txt_desde.date>=now)) or (not(txt_desde.Enabled)),
         (txt_hasta.date=nulldate) or (txt_hasta.date>=txt_desde.date)],
        MSG_CAPTION_PREGUNTA,
        [MSG_ERROR_DESCRIP,
        MSG_ERROR_FUENTES,
        format(MSG_VALIDAR_DEBE_LA,[FLD_FECHA]),
        MSG_VALIDAR_FECHA_MAYOR_HOY,
        MSG_VALIDAR_ORDEN_FECHA])) then exit;


    if (DarCodigoTipoPregunta=CONST_PREGUNTA_MULTIPLE) and (cdsPregunta.RecordCount<1) then begin
        MsgBoxBalloon(MSG_ERROR_ITEM1,MSG_CAPTION_PREGUNTA,MB_ICONSTOP,dbl_OpcionesPreguntas);
        exit;
    end;

    if (DarCodigoTipoPregunta=CONST_PREGUNTA_EXCLUYENTE) and (cdsPregunta.RecordCount<2) then begin
        MsgBoxBalloon(MSG_ERROR_ITEM2,MSG_CAPTION_PREGUNTA,MB_ICONSTOP,dbl_OpcionesPreguntas);
        exit;
    end;

    try
        try
            DMConnections.BaseCAC.BeginTrans;

            // Actualizo la pregunta
            with ActualizarMaestroPreguntas do begin
                close;

                if Dbl_Preguntas.Estado=Alta then Parameters.ParamByName('@CodigoPregunta').value:=null
                else Parameters.ParamByName('@CodigoPregunta').value:=ObtenerPreguntas.FieldByName('CodigoPregunta').AsInteger;

                Parameters.ParamByName('@Descripcion').value:=trim(txt_descripcion.Text);
                Parameters.ParamByName('@CodigoTipoPregunta').value:=DarCodigoTipoPregunta;
                Parameters.ParamByName('@CodigoTipoDato').value:=DarCodigoTipoDato;
                Parameters.ParamByName('@Baja').value:=0;
                Parameters.ParamByName('@FechaInicio').value:=txt_desde.Date;
                Parameters.ParamByName('@FechaFin').value:=iif(txt_hasta.Date=NullDate,null,txt_hasta.Date);
                ExecProc;

                nroPregunta:=Parameters.ParamByName('@CodigoPreguntaSalida').Value;
                close;
            end;


            if (not ObtenerPreguntas.FieldByName('Realizada').AsBoolean) or
               (Dbl_Preguntas.Estado = Alta) then begin

                // Borro todos los items de la pregunta e inserto los nuevos
                QueryExecute(DMConnections.BaseCAC,format('DELETE MaestroOpcionesPreguntas WHERE CodigoPregunta = %d',[nroPregunta]));
                with ActualizarMaestroOpcionesPreguntas do begin
                    cdsPregunta.First;
                    while not(cdsPregunta.eof) do begin
                        close;
                        Parameters.ParamByName('@CodigoPregunta').Value:=nroPregunta;
                        Parameters.ParamByName('@Descripcion').Value:=cdsPregunta.FieldByName('Descripcion').Value;
                        Parameters.ParamByName('@Orden').value:=Null;
                        Parameters.ParamByName('@CodigoOpcionesPregunta').value:=Null;
                        ExecProc;
                        cdsPregunta.next;
                    end;
                end;
            end else begin //solo actualizo el orden
                with ActualizarMaestroOpcionesPreguntas do begin
                    cdsPregunta.First;
                    while not(cdsPregunta.eof) do begin
                        close;
                        Parameters.ParamByName('@CodigoPregunta').Value:=nroPregunta;
                        Parameters.ParamByName('@Descripcion').Value:=cdsPregunta.FieldByName('Descripcion').Value;
                        Parameters.ParamByName('@Orden').Value:=cdsPregunta.RecNo;
                        Parameters.ParamByName('@CodigoOpcionesPregunta').value:=cdsPregunta.FieldByName('CodigoOpcionesPregunta').Value;

                        ExecProc;
                        cdsPregunta.next;
                    end;
                end;
            end;


            // Borro todos las fuentes de la pregunta e inserto los nuevos
            QueryExecute(DMConnections.BaseCAC,format(
                        'DELETE PreguntasFuenteSolicitud WHERE CodigoPregunta = %d',
                        [nroPregunta]));
            with ActualizarPreguntasFuenteSolicitud do begin
                cdsSolicitud.First;
                while not(cdsSolicitud.eof) do begin
                    close;
                    Parameters.ParamByName('@CodigoPregunta').Value:=nroPregunta;
                    Parameters.ParamByName('@CodigoFuenteSolicitud').Value:=cdsSolicitud.FieldByName('CodigoFuenteSolicitud').Value;
                    Parameters.ParamByName('@Probabilidad').Value:=cdsSolicitud.FieldByName('Probabilidad').Value;
                    ExecProc;
                    cdsSolicitud.next;
                end;
            end;

            DMConnections.BaseCAC.CommitTrans;
        except
            On E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_CAPTION_PREGUNTA, MB_ICONSTOP);
            end;
        end;
    finally
        Volver_Campos;
        Dbl_Preguntas.Reload;        
    end;
end;

procedure TFormPreguntas.Dbl_PreguntasDelete(Sender: TObject);
begin
    try
        if MsgBox(MSG_BORRAR_PREGUNTA, MSG_CAPTION_PREGUNTA, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            try
                if ObtenerPreguntas.FieldByName('Realizada').AsBoolean then begin

                    with ActualizarMaestroPreguntas do begin
                        close;

                        Parameters.ParamByName('@CodigoPregunta').value:=ObtenerPreguntas.FieldByName('CodigoPregunta').AsInteger;
                        Parameters.ParamByName('@Descripcion').value:=trim(txt_descripcion.Text);
                        Parameters.ParamByName('@CodigoTipoPregunta').value:=DarCodigoTipoPregunta;
                        Parameters.ParamByName('@CodigoTipoDato').value:=DarCodigoTipoDato;
                        Parameters.ParamByName('@Baja').value:=True;
                        Parameters.ParamByName('@FechaInicio').value:=txt_desde.Date;
                        Parameters.ParamByName('@FechaFin').value:=iif(txt_hasta.Date=NullDate,null,txt_hasta.Date);
                        ExecProc;

                        close;
                    end;

                end else begin
                    with EliminarMaestroPreguntas do begin
                        close;
                        Parameters.ParamByName('@CodigoPregunta').Value:=ObtenerPreguntas.FieldByName('CodigoPregunta').Value;
                        ExecProc;
                        if Parameters.ParamByName('@MotivoError').Value<>null then MsgBox(Parameters.ParamByName('@MotivoError').Value,MSG_CAPTION_PREGUNTA,MB_ICONSTOP);
                        close;
                    end;
                end;
            Except
                On E: Exception do begin
                    MsgBoxErr(MSG_ERROR_BORRAR_PREGUNTA, e.message, MSG_CAPTION_PREGUNTA, MB_ICONSTOP);
                end;
            end;
        end;
    finally
        Dbl_Preguntas.Estado:=Normal;
        Dbl_Preguntas.Reload;
        dbl_Preguntas.SetFocus;
    end;
end;

function TFormPreguntas.DarCodigoTipoPregunta:Integer;
begin
    result:=StrToInt(StrRight(cb_TipoPregunta.Text,10));
end;

function TFormPreguntas.DarCodigoTipoDato:Integer;
begin
    result:=StrToInt(StrRight(cb_TipoDato.Text,10));
end;
procedure TFormPreguntas.btn_ModificarClick(Sender: TObject);
var
    aux: String;
begin
    aux:=cdsPregunta.FieldbyName('Descripcion').AsString;
    if InputQuery(MSG_CAPTION_PREGUNTA, self.caption, aux) and (trim(aux)<>'') then begin
        with cdsPregunta do begin
            edit;
            FieldbyName('Descripcion').AsString:=trim(aux);
            Post;
        end;
    end;
    dbl_OpcionesPreguntas.SetFocus;
end;

procedure TFormPreguntas.btn_ModificarCategoriaClick(Sender: TObject);
var
    f:TFormPreguntaSolicitud;
    error:Boolean;
    pos:TBookmark;
    nroRegistro:Integer;
begin
    Application.CreateForm(TFormPreguntaSolicitud,f);
    with cdsSolicitud do begin
        if (f.inicializar(FieldbyName('CodigoFuenteSolicitud').AsInteger,FieldbyName('Probabilidad').AsInteger)) and (f.ShowModal=mrOk) then
        begin
            error:=false;
            nroRegistro:=RecNo;
            pos:=GetBookmark;
            first;
            while not(eof) do begin
                if (FieldByName('CodigoFuenteSolicitud').AsInteger=f.CodigoFuenteSolicitud) and (nroRegistro<>RecNo) then begin
                    error:=true;
                    MsgBoxBalloon(MSG_ERROR_DUPLICADO_FUENTE,MSG_CAPTION_PREGUNTA,MB_ICONSTOP,Dbl_Categoria);
                end;
                next;
            end;

            GotoBookmark(pos);
            if not(error) then begin
                Edit;
                FieldbyName('CodigoFuenteSolicitud').AsInteger:=f.CodigoFuenteSolicitud;
                FieldbyName('Probabilidad').AsInteger:=f.Probabilidad;
                FieldbyName('FuentesSolicitud').AsString:=f.FuenteSolicitud;
                Post;
            end;
        end;
    end;
    dbl_Categoria.SetFocus;
    f.Release;
end;

procedure TFormPreguntas.cb_PreguntasBorradasClick(Sender: TObject);
begin
    if cb_PreguntasBorradas.Checked then
        ObtenerPreguntas.Parameters.ParamByName('@baja').Value:=null
    else
        ObtenerPreguntas.Parameters.ParamByName('@baja').value:=0;

    if not(OpenTables([ObtenerPreguntas])) then exit;
    Dbl_Preguntas.Reload;
end;

function TFormPreguntas.FueradeVigencia:Boolean;
begin
    if (ObtenerPreguntas.active) and ((ObtenerPreguntas.FieldByName('FechaFin').AsDateTime=0) or (ObtenerPreguntas.FieldByName('FechaFin').AsDateTime>now)) then
        result:=False
    else
        result:=True;
end;
procedure TFormPreguntas.btn_OrdenClick(Sender: TObject;
  Button: TUDBtnType);

  procedure IrA(Pos:integer);
  begin
        cdsPregunta.First;
        while not(cdsPregunta.eof) and (cdsPregunta.RecNo<pos) do cdsPregunta.Next;
  end;

var
    AuxOrigen,AuxDestino:String;
begin
    with cdsPregunta do begin
        if Button=btNext then begin //hace referecia al anterior NO al siguiente
            if RecNo>1 then begin
                AuxOrigen:=FieldbyName('Descripcion').AsString;
                ira(RecNo-1);
                AuxDestino:=FieldbyName('Descripcion').AsString;
                edit;
                FieldbyName('Descripcion').AsString:=AuxOrigen;
                post;
                Next;
                Edit;
                FieldbyName('Descripcion').AsString:=AuxDestino;
                Post;
                ira(RecNo-1);
            end;
        end else begin
            if RecNo<recordcount then begin
                AuxOrigen:=FieldbyName('Descripcion').AsString;
                next;
                AuxDestino:=FieldbyName('Descripcion').AsString;
                edit;
                FieldbyName('Descripcion').AsString:=AuxOrigen;
                post;
                ira(RecNo-1);
                Edit;
                FieldbyName('Descripcion').AsString:=AuxDestino;
                Post;
                next;
            end;
        end;
    end;
end;

end.



