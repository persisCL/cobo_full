object ABMFormaPagoForm: TABMFormaPagoForm
  Left = 106
  Top = 61
  Caption = 'Mantenimiento de Formas de Pago'
  ClientHeight = 578
  ClientWidth = 817
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupB: TPanel
    Left = 0
    Top = 354
    Width = 817
    Height = 185
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Lcodigo: TLabel
      Left = 10
      Top = 11
      Width = 44
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Ldescripcion: TLabel
      Left = 11
      Top = 35
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 10
      Top = 59
      Width = 139
      Height = 13
      Caption = 'C'#243'digo &Entrada Usuario:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 10
      Top = 83
      Width = 102
      Height = 13
      Caption = 'Cobro Anticipado:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 11
      Top = 107
      Width = 51
      Height = 13
      Caption = 'Pantalla:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 10
      Top = 132
      Width = 84
      Height = 13
      Caption = 'C'#243'digo Banco:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 11
      Top = 156
      Width = 88
      Height = 13
      Caption = 'C'#243'digo Tarjeta:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtCodigo: TNumericEdit
      Left = 155
      Top = 8
      Width = 105
      Height = 21
      Color = 16444382
      Enabled = False
      TabOrder = 0
    end
    object txtDescripcion: TEdit
      Left = 155
      Top = 32
      Width = 388
      Height = 21
      Color = 16444382
      TabOrder = 1
    end
    object cbCobroAnticipado: TCheckBox
      Left = 155
      Top = 82
      Width = 17
      Height = 17
      Color = clBtnFace
      ParentColor = False
      TabOrder = 3
    end
    object edtCodigoEntradaUsuario: TEdit
      Left = 155
      Top = 56
      Width = 46
      Height = 21
      Color = 16444382
      MaxLength = 2
      TabOrder = 2
    end
    object nedtCodigoBanco: TNumericEdit
      Left = 155
      Top = 129
      Width = 46
      Height = 21
      Color = 16444382
      TabOrder = 5
    end
    object nedtCodigoTarjeta: TNumericEdit
      Left = 155
      Top = 153
      Width = 46
      Height = 21
      Color = 16444382
      TabOrder = 6
    end
    object vcbPantallas: TVariantComboBox
      Left = 155
      Top = 105
      Width = 206
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 4
      Items = <>
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 539
    Width = 817
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 620
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 817
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbarClose
  end
  object ListaFormasPago: TAbmList
    Left = 0
    Top = 33
    Width = 817
    Height = 321
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'74'#0'C'#243'digo           '
      
        #0'247'#0'Descripci'#243'n                                                ' +
        '             '
      #0'118'#0'C'#243'd. Entrada Usuario   '
      #0'98'#0'Cobro Anticipado   '
      #0'193'#0'Pantalla                                                 '
      #0'73'#0'C'#243'd. Banco   '
      #0'75'#0'C'#243'd. Tarjeta   '
      #0'17'#0'   ')
    HScrollBar = True
    RefreshTime = 100
    Table = tblFormasPago
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaFormasPagoClick
    OnProcess = ListaFormasPagoProcess
    OnDrawItem = ListaFormasPagoDrawItem
    OnRefresh = ListaFormasPagoRefresh
    OnInsert = ListaFormasPagoInsert
    OnDelete = ListaFormasPagoDelete
    OnEdit = ListaFormasPagoEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object tblFormasPago: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'FormasPago'
    Left = 344
    Top = 117
  end
  object spActualizaFormasPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarFormaPago;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFormaPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoEntradaUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@AceptaCobroAnticipado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoPantalla'
        DataType = ftSmallint
        Size = 2
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        DataType = ftInteger
        Size = 4
        Value = Null
      end
      item
        Name = '@CodigoTarjeta'
        DataType = ftInteger
        Size = 4
        Value = Null
      end>
    Left = 412
    Top = 216
  end
  object dsFormasPago: TDataSource
    DataSet = tblFormasPago
    Left = 414
    Top = 92
  end
  object spEliminarFormaPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarFormaPago;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFormaPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Resultado'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 576
    Top = 168
  end
end
