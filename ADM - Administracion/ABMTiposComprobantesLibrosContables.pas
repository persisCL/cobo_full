{--------------------------------------------------------------------------

Author      :
Date        :
Decription  :

Firma       :   SS_1147_MBE_20140819
Description :   Se agrega una validaci�n antes de grabar.

---------------------------------------------------------------------------}
unit ABMTiposComprobantesLibrosContables;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls,RStrings, Validate, DateEdit, VariantComboBox;

type
  TFormABMTiposComprobantesLibrosContables = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    TiposComprobantesLibrosContables: TADOTable;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    TiposComprobantesLibrosContablesTipoComprobante: TStringField;
    TiposComprobantesLibrosContablesIdLibroContable: TIntegerField;
    TiposComprobantesLibrosContablesUsuarioModificacion: TStringField;
    TiposComprobantesLibrosContablesFechaModificacion: TDateTimeField;
    CbTipoComprobante: TVariantComboBox;
    CbLibroContable: TVariantComboBox;
    Label2: TLabel;
    QryLibrosContables: TADOQuery;
    QryTiposComprobantes: TADOQuery;
    TiposComprobantesLibrosContablesUsuarioCreacion: TStringField;
    TiposComprobantesLibrosContablesFechaCreacion: TDateTimeField;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
    procedure BuscarTextoenCombo(Combo: TVariantComboBox; Valor: Variant);
    procedure LlenarDatoscombo(Combo: TVariantComboBox; Datos: TAdoQuery; CampoCaption,CampoValor: String);
  public
    { Public declarations }
    function Inicializa: boolean;

  end;


resourcestring
    FLD_TIPO_COMPROBANTE = 'Tipo Comprobante';
    FLD_TIPO_COMPROBANTE_RELACION = 'Relaci�n Tipo Comprobante - Libro Contable';

var
  FormABMTiposComprobantesLibrosContables: TFormABMTiposComprobantesLibrosContables;

implementation

{$R *.DFM}

procedure TFormABMTiposComprobantesLibrosContables.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
end;

function TFormABMTiposComprobantesLibrosContables.Inicializa: boolean;
begin
    CenterForm(Self);
	if not OpenTables([TiposComprobantesLibrosContables]) then
		Result := False
	else begin
    	Notebook.PageIndex := 0;
		Result := True;
        VolverCampos;
		DbList1.Reload;
        LlenarDatoscombo(CbTipoComprobante, QryTiposComprobantes, 'Descripcion','TipoComprobante');
        LlenarDatoscombo(CbLibroContable, QryLibrosContables, 'Descripcion','IdLibroContable');
    end;
end;

procedure TFormABMTiposComprobantesLibrosContables.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormABMTiposComprobantesLibrosContables.DBList1Click(Sender: TObject);
begin
    BuscarTextoenCombo(CbTipoComprobante, TiposComprobantesLibrosContables.FieldByName('Tipocomprobante').AsVariant);
    BuscarTextoenCombo(CbLibrocontable, TiposComprobantesLibrosContables.FieldByName('IdLibroContable').AsVariant);
end;

procedure TFormABMTiposComprobantesLibrosContables.DBList1Delete(
  Sender: TObject);
resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar la Relaci�n Comprobante - Libro Contable?';
    MSG_DELETE_ERROR		= 'No se puede eliminar la relaci�n Comprobante - Libro Contable porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Relaci�n Comprobante - Libro Contable';
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) =
		IDYES then begin
		try
			TiposComprobantesLibrosContables.Delete;
		Except
			On E: EDataBaseError do begin
				TiposComprobantesLibrosContables.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DBList1.Reload;
	end;
	DBList1.Estado       := Normal;
	DBList1.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormABMTiposComprobantesLibrosContables.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('TipoComprobante').AsString);
        TextOut(Cols[1], Rect.Top, Tabla.FieldByName('IdLibroContable').AsString);
	end;
end;

procedure TFormABMTiposComprobantesLibrosContables.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado     := modi;
end;

procedure TFormABMTiposComprobantesLibrosContables.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCampos;
    dblist1.Estado     := Alta;
end;

procedure TFormABMTiposComprobantesLibrosContables.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormABMTiposComprobantesLibrosContables.Limpiar_Campos;
begin
    CbTipoComprobante.ItemIndex := -1;
    CbLibroContable.ItemIndex := -1;
end;

procedure TFormABMTiposComprobantesLibrosContables.LlenarDatoscombo(
  Combo: TVariantComboBox; Datos: TAdoQuery; CampoCaption,CampoValor: String);
var
    i, cont:integer;
begin
    Datos.Open;
    cont := Datos.RecordCount;
    Combo.Clear;
    Datos.First;
    for i := 0 to cont - 1 do begin
        Combo.Items.Add(Datos.FieldByName(CampoCaption).AsString, Datos.FieldByName(CampoValor).AsVariant);
        Datos.Next;
    end;
    Datos.Close;
end;

procedure TFormABMTiposComprobantesLibrosContables.AbmToolbar1Close(Sender: TObject);
begin
    close;
end;

procedure TFormABMTiposComprobantesLibrosContables.BtnAceptarClick(Sender: TObject);
resourcestring                                                                                                  //SS_1147_MBE_20140819
    MSG_FALTA_TIPO  = 'No ha seleccionado el Tipo de Comprobante';                                              //SS_1147_MBE_20140819
    MSG_FALTA_LIBRO = 'No ha seleccionado el Libro Contable';                                                   //SS_1147_MBE_20140819

    function ComprobanteCargado(Tipo:String;Posicion:Integer=-1):Boolean;
    var
        pos:TBookmark;
    begin
        result:=False;
        with TiposComprobantesLibrosContables do begin
            pos:=GetBookmark;
            First;
            while not(eof) and (result=False) do begin
                if (Posicion<>RecNo) and (uppercase(trim(FieldByName('TipoComprobante').AsString))=uppercase(trim(Tipo))) then
                        Result:=True;
                next;
            end;
            GotoBookmark(pos);
        end;
    end;


begin                                                                                                           //SS_1147_MBE_20140819
{
    if trim(CbTipoComprobante.Text) = EmptyStr then exit;
    if trim(CbLibroContable.Text) = EmptyStr then exit;
}                                                                                                               //SS_1147_MBE_20140819
    if not ValidateControls(    [CbTipoComprobante, CbLibroContable],                                           //SS_1147_MBE_20140819
                                [CbTipoComprobante.ItemIndex >= 0, CbLibroContable.ItemIndex >= 0],             //SS_1147_MBE_20140819
                                Caption,                                                                        //SS_1147_MBE_20140819
                                [MSG_FALTA_TIPO, MSG_FALTA_LIBRO]                                               //SS_1147_MBE_20140819
                            ) then Exit;                                                                        //SS_1147_MBE_20140819


    if ComprobanteCargado(CbTipoComprobante.Value,iif(DbList1.Estado = Alta,-1,TiposComprobantesLibrosContables.RecNo)) then begin
        MsgBoxBalloon(format(MSG_ERROR_DUPLICADO_EL,[FLD_TIPO_COMPROBANTE]),format(MSG_CAPTION_GESTION,[FLD_TIPO_COMPROBANTE_RELACION]),MB_ICONSTOP,CbTipoComprobante);
        Exit;
    end;

 	Screen.Cursor := crHourGlass;
	With TiposComprobantesLibrosContables do begin
		Try
			if DbList1.Estado = Alta then Append
			else Edit;

            FieldByName('IdLibroContable').Value:= CbLibroContable.Value;
            FieldByName('TipoComprobante').Value:= CbTipoComprobante.Value;
            FieldByName('UsuarioModificacion').Value:= UsuarioSistema;
            FieldByName('FechaModificacion').Value:= NowBase(DMConnections.BaseCAC);
            if DbList1.Estado = Alta then begin
                FieldByName('UsuarioCreacion').Value:= UsuarioSistema;
                FieldByName('FechaCreacion').Value:= NowBase(DMConnections.BaseCAC);
            end;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(format(MSG_ERROR_ACTUALIZAR,[FLD_TIPO_COMPROBANTE_RELACION]), E.message, format(MSG_CAPTION_GESTION,[FLD_TIPO_COMPROBANTE_RELACION]), MB_ICONSTOP);
                Screen.Cursor	:= crDefault;
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFormABMTiposComprobantesLibrosContables.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormABMTiposComprobantesLibrosContables.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormABMTiposComprobantesLibrosContables.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormABMTiposComprobantesLibrosContables.BuscarTextoenCombo(Combo: TVariantComboBox; Valor: Variant);
var
    i:integer;
begin
    for i := 0 to Combo.Items.Count - 1 do begin
        Combo.ItemIndex := i;
        if Combo.Items.Items[i].Value = Valor then begin
            Break;
        end;
    end;
end;


procedure TFormABMTiposComprobantesLibrosContables.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    CbTipoComprobante.SetFocus;
end;

end.
