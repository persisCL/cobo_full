{********************************** File Header ********************************
File Name   : ABMCallesTramos.pas
Author      : dcalani
Date Created: 27/02/2004
Language    : ES-AR
Description : Permite cargar los datos de un tramos y validarlos,
              NO carga actualizar la base de datos.
*******************************************************************************}

unit ABMCallesTramos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DmiCtrls, StdCtrls, DPSControls,Util,utildb,UtilProc,PeaProcs,PeaTypes,
  DB, ADODB;

type
  TFormTramosCalles = class(TForm)
    GroupBox2: TGroupBox;
    Label11: TLabel;
    txtCodigoPostalPAR: TEdit;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    Label4: TLabel;
    txt_NumeroDesde: TNumericEdit;
    Label5: TLabel;
    txt_NumeroHasta: TNumericEdit;
    Label1: TLabel;
    txtCodigoPostalImpar: TEdit;
    Label2: TLabel;
    cbCAD: TComboBox;
    Label3: TLabel;
    Label6: TLabel;
    txt_latitud: TNumericEdit;
    txt_longitud: TNumericEdit;
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    function GetNumeroDesde:Integer;
    function GetNumeroHasta:Integer;
    function ValidarNumeracionYClaves:boolean;
    function GetCAD: String;
    function GetCodigoPostalImpar: AnsiString;
    function GetCodigoPostalPar: AnsiString;
    function GetLongitud:Real;
    function GetLatitud:Real;
  public
    { Public declarations }
    function Inicializar(NumDesde:Integer=0;NumHasta:Integer=0;CAD:AnsiString=''; CodigoPostalPar:AnsiString='';CodigoPostalImPar:AnsiString='';Latitud:Real=0;Longitud:Real=0):boolean;
    property NumeroDesde:Integer read GetNumeroDesde;
    property NumeroHasta:Integer read GetNumeroHasta;
    property CAD:String read GetCAD;
    property CodigoPostalImpar: AnsiString read GetCodigoPostalImpar;
    property CodigoPostalPar: AnsiString read GetCodigoPostalPar;
    property Latitud:Real read GetLatitud;
    property Longitud:Real read GetLongitud;
  end;

var
  FormTramosCalles: TFormTramosCalles;

implementation
uses DMConnection, ABMMaestroCalles;

resourcestring
    MSG_CAPTION_TRAMOS= 'Edici�n de Tramos.';
    MSG_ERROR_CAPTION = 'No se puede cargar este tramo.';
    MSG_ERROR_CARGA   = 'Error al inicializar';
    MSG_NUMERODESDE   = 'Debe ingresar el n�mero desde de la calle';
    MSG_NUMEROHASTA   = 'Debe ingresar el n�mero hasta de la calle';
    MSG_CAD   = 'Debe ingresar el n�mero CAD';

{$R *.dfm}
function TFormTramosCalles.Inicializar(NumDesde:Integer=0;NumHasta:Integer=0;CAD:AnsiString=''; CodigoPostalPar:AnsiString='';CodigoPostalImPar:AnsiString='';Latitud:Real=0;Longitud:Real=0):boolean;
    procedure CargarCADs;
    var i: integer;
    begin
        cbCAD.Clear;
        for i := 0  to  20 do
        begin
            cbCAD.Items.Add(intToStr(i));
        end;
    end;
begin
	try
		result:=true;
		txt_NumeroDesde.Value:=NumDesde;
		txt_NumeroHasta.Value:=NumHasta;
        CargarCADs;
        (*
        if CAD = '0' then
    		cbCAD.ItemIndex := 0
        else
        	cbCAD.ItemIndex := 1;*)
        if trim(CAD) <> '' then
            cbCAD.ItemIndex := StrToInt(trim(CAD));

  		txtCodigoPostalImpar.Text:=trim(CodigoPostalImpar);
  		txtCodigoPostalPar.Text:=trim(CodigoPostalPar);
        txt_longitud.Value:=Latitud;
        txt_longitud.Value:=Longitud;

	except
		On E: Exception do begin
			result:=false;
            MsgBoxErr(MSG_ERROR_CARGA, E.Message, MSG_ERROR_CAPTION, MB_ICONSTOP);
        end;

    end;

end;


function TFormTramosCalles.GetNumeroDesde:Integer;
begin
    result:=txt_NumeroDesde.ValueInt;
end;

function TFormTramosCalles.GetNumeroHasta:Integer;
begin
    result:=txt_NumeroHasta.ValueInt;
end;


procedure TFormTramosCalles.BtnAceptarClick(Sender: TObject);
begin
    if not(ValidateControls([txt_NumeroDesde,txt_NumeroHasta, cbCAD],
        [trim(txt_NumeroDesde.Text) <> '',
         trim(txt_NumeroHasta.Text) <> '',
         trim(cbCAD.Text)<>''],
        MSG_CAPTION_TRAMOS,
        [MSG_NUMERODESDE,MSG_NUMEROHASTA,MSG_CAD])) then exit;
    if not(ValidarNumeracionYClaves) then exit;

    ModalResult:=mrOk;
end;

procedure TFormTramosCalles.BtnCancelarClick(Sender: TObject);
begin
    close;
end;

{******************************** Function Header ******************************
Function Name: TFormCalles.ValidarNumeracionYClaves
Author       : dcalani
Date Created : 20/02/2004
Description  : Valida que el rango cargado sea valido
Parameters   :
Return Value : boolean
*******************************************************************************}
function TFormTramosCalles.ValidarNumeracionYClaves:boolean;
begin
    result:=True;

    if txt_NumeroDesde.Value > txt_NumeroHasta.Value then begin
        MsgBoxBalloon('MSG_RANGOINCORRECTO', MSG_CAPTION_TRAMOS, MB_ICONSTOP,txt_NumeroDesde);
        result:=False;
        exit;
    end;

end;

function TFormTramosCalles.GetCAD: String;
begin
    result := trim(cbCAD.text);
end;

function TFormTramosCalles.GetCodigoPostalImpar: AnsiString;
begin
    result := trim(txtCodigoPostalImpar.text);
end;

function TFormTramosCalles.GetCodigoPostalPar: AnsiString;
begin
    result := trim(txtCodigoPostalPar.text);
end;

function TFormTramosCalles.GetLatitud: Real;
begin
    result:=txt_latitud.Value;
end;

function TFormTramosCalles.GetLongitud: Real;
begin
    Result:=txt_longitud.Value;
end;

end.
