object FormTarjetasDeDebito: TFormTarjetasDeDebito
  Left = 109
  Top = 97
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Administraci'#243'n de Tarjetas de D'#233'bito'
  ClientHeight = 557
  ClientWidth = 838
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlInferior: TPanel
    Left = 0
    Top = 519
    Width = 838
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Label8: TLabel
      Left = 39
      Top = 13
      Width = 48
      Height = 13
      Caption = 'Eliminado.'
    end
    object Notebook: TNotebook
      Left = 641
      Top = 0
      Width = 197
      Height = 38
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        ExplicitWidth = 0
        ExplicitHeight = 0
        object BtnSalir: TDPSButton
          Left = 112
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 27
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 107
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
    object Panel3: TPanel
      Left = 20
      Top = 12
      Width = 12
      Height = 14
      BevelOuter = bvNone
      Color = clRed
      TabOrder = 1
    end
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 838
    Height = 35
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object alTarjetasDebito: TAbmList
    Left = 0
    Top = 35
    Width = 838
    Height = 233
    TabStop = True
    TabOrder = 2
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = 12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'119'#0'C'#243'digo'
      #0'235'#0'Raz'#243'n Social'
      #0'213'#0'Nombre de Fantas'#237'a')
    HScrollBar = True
    Table = TarjetasDebito
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = alTarjetasDebitoClick
    OnDrawItem = alTarjetasDebitoDrawItem
    OnRefresh = alTarjetasDebitoRefresh
    OnInsert = alTarjetasDebitoInsert
    OnDelete = alTarjetasDebitoDelete
    OnEdit = alTarjetasDebitoEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object pcDatosPersonal: TPageControl
    Left = 0
    Top = 268
    Width = 838
    Height = 251
    ActivePage = tsTarjetaDebito
    Align = alBottom
    TabOrder = 3
    object tsCliente: TTabSheet
      Caption = 'Identificaci'#243'n'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object pnlDatosPersonal: TPanel
        Left = 0
        Top = 0
        Width = 830
        Height = 223
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object lblApellido: TLabel
          Left = 9
          Top = 43
          Width = 50
          Height = 13
          Caption = 'Apellido:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblFechaNacCreacion: TLabel
          Left = 419
          Top = 121
          Width = 89
          Height = 13
          Caption = 'Fecha Nacimiento:'
        end
        object lblNombre: TLabel
          Left = 9
          Top = 68
          Width = 48
          Height = 13
          Caption = 'Nombre:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 9
          Top = 94
          Width = 91
          Height = 13
          Caption = 'Tipo/Nro. Doc.:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblSexo: TLabel
          Left = 419
          Top = 93
          Width = 27
          Height = 13
          Caption = 'Sexo:'
        end
        object Label23: TLabel
          Left = 9
          Top = 18
          Width = 67
          Height = 13
          Caption = 'Personer'#237'a:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblApellidoMaterno: TLabel
          Left = 421
          Top = 43
          Width = 82
          Height = 13
          Caption = 'Apellido Materno:'
        end
        object Label2: TLabel
          Left = 11
          Top = 118
          Width = 74
          Height = 13
          Caption = 'Pa'#237's de Origen:'
        end
        object Label10: TLabel
          Left = 11
          Top = 141
          Width = 47
          Height = 13
          Caption = 'Actividad:'
        end
        object Label11: TLabel
          Left = 421
          Top = 68
          Width = 82
          Height = 13
          Caption = 'Situaci'#243'n de IVA:'
        end
        object dtFechaNacimiento: TDateEdit
          Left = 524
          Top = 115
          Width = 90
          Height = 21
          AutoSelect = False
          TabOrder = 8
          Date = -693594.000000000000000000
        end
        object txtApellido: TEdit
          Left = 132
          Top = 38
          Width = 278
          Height = 21
          Color = 16444382
          MaxLength = 30
          TabOrder = 1
        end
        object txtNombre: TEdit
          Left = 132
          Top = 63
          Width = 278
          Height = 21
          Color = 16444382
          MaxLength = 30
          TabOrder = 3
        end
        object mcTipoNumeroDocumento: TMaskCombo
          Left = 132
          Top = 88
          Width = 277
          Height = 21
          Items = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Color = 16444382
          ComboWidth = 145
          ItemIndex = -1
          OmitCharacterRequired = False
          TabOrder = 5
        end
        object cbPersoneria: TComboBox
          Left = 132
          Top = 12
          Width = 145
          Height = 21
          Style = csDropDownList
          Color = 16444382
          Enabled = False
          ItemHeight = 0
          TabOrder = 0
          OnChange = cbPersoneriaChange
          Items.Strings = (
            'F'#237'sica                          F'
            'Jur'#237'dica                       J')
        end
        object txtApellidoMaterno: TEdit
          Left = 524
          Top = 38
          Width = 278
          Height = 21
          MaxLength = 30
          TabOrder = 2
        end
        object cbSexo: TComboBox
          Left = 524
          Top = 89
          Width = 145
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 0
          TabOrder = 6
          Items.Strings = (
            '(Ninguno)'
            
              'Masculino                                                       ' +
              '                      M'
            
              'Femenino                                                        ' +
              '                      F')
        end
        object cbSituacionIVA: TComboBox
          Left = 524
          Top = 63
          Width = 278
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 4
        end
        object cbLugarNacimiento: TComboBox
          Left = 132
          Top = 113
          Width = 278
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 7
        end
        object cbActividades: TComboBox
          Left = 132
          Top = 137
          Width = 278
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 9
        end
        object btnBuscar: TBitBtn
          Left = 332
          Top = 8
          Width = 77
          Height = 25
          Caption = '&Buscar'
          TabOrder = 10
          Visible = False
          OnClick = btnBuscarClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
            3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
            33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
            333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          NumGlyphs = 2
        end
      end
    end
    object tsDomicilio: TTabSheet
      Caption = 'Domicilio'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object pnlDomicilio: TPanel
        Left = 0
        Top = 0
        Width = 830
        Height = 223
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          830
          223)
        object dblDomicilios: TDBListEx
          Left = 16
          Top = 12
          Width = 796
          Height = 173
          Anchors = [akLeft, akTop, akRight]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 95
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriOrigenTipoDato'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 200
              Header.Caption = 'Ubicaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Arial'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriCiudad'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 200
              Header.Caption = 'Direcci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Arial'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DireccionCompleta'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 200
              Header.Caption = 'Detalle'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Detalle'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'Domicilio Entrega'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
            end>
          DataSource = dsDomicilios
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnDrawText = dblDomiciliosDrawText
        end
        object btnAgregarDomicilio: TDPSButton
          Left = 589
          Top = 192
          Anchors = [akTop, akRight]
          Caption = '&Agregar'
          TabOrder = 1
          OnClick = Agregar
        end
        object btnEditarDomicilio: TDPSButton
          Left = 664
          Top = 192
          Anchors = [akTop, akRight]
          Caption = '&Editar'
          TabOrder = 2
          OnClick = btnEditarDomicilioClick
        end
        object btnEliminarDomicilio: TDPSButton
          Left = 739
          Top = 192
          Anchors = [akTop, akRight]
          Caption = 'E&liminar'
          TabOrder = 3
          OnClick = btnEliminarDomicilioClick
        end
      end
    end
    object tsContacto: TTabSheet
      Caption = 'Contacto'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object pnlMediosContacto: TPanel
        Left = 0
        Top = 0
        Width = 830
        Height = 223
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          830
          223)
        object dblMediosComunicacion: TDBListEx
          Left = 16
          Top = 12
          Width = 796
          Height = 173
          Anchors = [akLeft, akTop, akRight]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriTipoOrigenDato'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Medio de Contacto'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriMedioContacto'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 255
              Header.Caption = 'Detalle'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Detalle'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 80
              Header.Caption = 'Hora desde'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'HorarioDesde'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 80
              Header.Caption = 'Hora hasta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'HorarioHasta'
            end>
          DataSource = dsMediosComunicacion
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnDrawText = dblMediosComunicacionDrawText
        end
        object btnAgregarMedioComunicacion: TDPSButton
          Left = 589
          Top = 192
          Anchors = [akTop, akRight]
          Caption = '&Agregar'
          TabOrder = 1
          OnClick = btnAgregarMedioComunicacionClick
        end
        object btnEditarMedioComunicacion: TDPSButton
          Left = 664
          Top = 192
          Anchors = [akTop, akRight]
          Caption = '&Editar'
          TabOrder = 2
          OnClick = btnEditarMedioComunicacionClick
        end
        object btnEliminarMedioComunicacion: TDPSButton
          Left = 739
          Top = 192
          Anchors = [akTop, akRight]
          Caption = 'E&liminar'
          TabOrder = 3
          OnClick = btnEliminarMedioComunicacionClick
        end
      end
    end
    object tsTarjetaDebito: TTabSheet
      Caption = 'Tarjeta de D'#233'bito'
      ImageIndex = 3
      object pnlDatosTarjetaDebito: TPanel
        Left = 0
        Top = 0
        Width = 830
        Height = 223
        Align = alClient
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 21
          Width = 91
          Height = 13
          Caption = '&Primer car'#225'cter:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 167
          Top = 20
          Width = 112
          Height = 13
          Caption = '&Longitud de Banda:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 16
          Top = 51
          Width = 47
          Height = 13
          Caption = 'Prefijos:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object chkActivo: TCheckBox
          Left = 15
          Top = 152
          Width = 134
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Activo:'
          TabOrder = 5
        end
        object txt_caracterinicialbanda: TNumericEdit
          Left = 134
          Top = 17
          Width = 26
          Height = 21
          Color = 16444382
          MaxLength = 2
          TabOrder = 0
        end
        object txt_longitudbanda: TNumericEdit
          Left = 286
          Top = 15
          Width = 26
          Height = 21
          Color = 16444382
          MaxLength = 2
          TabOrder = 1
        end
        object lb_prefijos: TListBox
          Left = 136
          Top = 48
          Width = 225
          Height = 101
          ItemHeight = 13
          TabOrder = 2
        end
        object btnAgregarPrefijo: TDPSButton
          Left = 373
          Top = 52
          Width = 79
          Height = 26
          Caption = '<< A&gregar'
          TabOrder = 3
          OnClick = btnAgregarPrefijoClick
        end
        object btnQuitarPrefijo: TDPSButton
          Left = 373
          Top = 80
          Width = 79
          Height = 26
          Caption = '>> &Quitar'
          TabOrder = 4
          OnClick = btnQuitarPrefijoClick
        end
      end
    end
    object tsDLLsPago: TTabSheet
      Caption = 'DLL de Pago'
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object pnlDLLsPago: TPanel
        Left = 0
        Top = 0
        Width = 830
        Height = 223
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label3: TLabel
          Left = 24
          Top = 16
          Width = 94
          Height = 13
          Caption = 'Operador Log'#237'stico:'
        end
        object cbOperadoresLogisticos: TComboBox
          Left = 152
          Top = 12
          Width = 293
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 0
          OnChange = cbOperadoresLogisticosChange
        end
        object sgDllOperacion: TStringGrid
          Left = 24
          Top = 44
          Width = 705
          Height = 165
          ColCount = 2
          DefaultRowHeight = 20
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          ScrollBars = ssVertical
          TabOrder = 1
          ColWidths = (
            418
            258)
          RowHeights = (
            20
            20
            20
            20
            20)
        end
      end
    end
  end
  object TarjetasDebito: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'VW_TarjetasDebito'
    Left = 284
    Top = 176
  end
  object ObtenerMediosComunicacionPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMediosComunicacionPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 84
    Top = 72
  end
  object ActualizarDatosTarjetaDebito: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosTarjetaDebito;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CaracterInicialBanda'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@LongitudBanda'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ValidarPrefijo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 264
    Top = 120
  end
  object QueryTemp: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 460
    Top = 184
  end
  object dsMediosComunicacion: TDataSource
    DataSet = cdsMediosComunicacion
    Left = 112
    Top = 72
  end
  object cdsMediosComunicacion: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoTipoOrigenDato'
        DataType = ftSmallint
      end
      item
        Name = 'CodigoMedioContacto'
        DataType = ftSmallint
      end
      item
        Name = 'DescriMedioContacto'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TipoMedio'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DescriTipoOrigenDato'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Valor'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'CodigoDomicilio'
        DataType = ftInteger
      end
      item
        Name = 'DescriDomicilio'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Detalle'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'HorarioDesde'
        DataType = ftDateTime
      end
      item
        Name = 'HorarioHasta'
        DataType = ftDateTime
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Activo'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoDomicilioRecNo'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'cdsMediosComunicacionIndex1'
        Fields = 'CodigoMedioContacto; CodigoTipoOrigenDato'
        Options = [ixUnique]
      end>
    IndexName = 'cdsMediosComunicacionIndex1'
    Params = <>
    ProviderName = 'dspMediosComunicacion'
    StoreDefs = True
    Left = 140
    Top = 72
    object cdsMediosComunicacionCodigoTipoOrigenDato: TSmallintField
      FieldName = 'CodigoTipoOrigenDato'
    end
    object cdsMediosComunicacionCodigoMedioContacto: TSmallintField
      FieldName = 'CodigoMedioContacto'
    end
    object cdsMediosComunicacionDescriMedioContacto: TStringField
      FieldName = 'DescriMedioContacto'
      FixedChar = True
      Size = 50
    end
    object cdsMediosComunicacionTipoMedio: TStringField
      FieldName = 'TipoMedio'
      FixedChar = True
      Size = 1
    end
    object cdsMediosComunicacionDescriTipoOrigenDato: TStringField
      FieldName = 'DescriTipoOrigenDato'
      Size = 50
    end
    object cdsMediosComunicacionValor: TStringField
      FieldName = 'Valor'
      Size = 255
    end
    object cdsMediosComunicacionCodigoDomicilio: TIntegerField
      FieldName = 'CodigoDomicilio'
    end
    object cdsMediosComunicacionDescriDomicilio: TStringField
      FieldName = 'DescriDomicilio'
      Size = 255
    end
    object cdsMediosComunicacionDetalle: TStringField
      FieldName = 'Detalle'
      Size = 255
    end
    object cdsMediosComunicacionHorarioDesde: TDateTimeField
      FieldName = 'HorarioDesde'
    end
    object cdsMediosComunicacionHorarioHasta: TDateTimeField
      FieldName = 'HorarioHasta'
    end
    object cdsMediosComunicacionObservaciones: TStringField
      FieldName = 'Observaciones'
      Size = 100
    end
    object cdsMediosComunicacionActivo: TBooleanField
      FieldName = 'Activo'
    end
    object cdsMediosComunicacionCodigoDomicilioRecNo: TIntegerField
      FieldName = 'CodigoDomicilioRecNo'
    end
  end
  object dspMediosComunicacion: TDataSetProvider
    DataSet = ObtenerMediosComunicacionPersona
    Left = 168
    Top = 72
  end
  object EliminarTarjetaDebito: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTarjetaDebito;1'
    Parameters = <>
    Left = 300
    Top = 120
  end
  object qryObtenerDLLPago: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'OperadorLogistico1'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'OperadorLogistico2'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Tarjeta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      'LugaresDeVenta.CodigoOperadorLogistico,'
      
        'LugaresDeVenta.RazonSocialOperador AS RazonSocialOperadorLogisti' +
        'co,'
      'LugaresDeVenta.CodigoPersona AS CodigoLugarDeVenta,'
      'LugaresDeVenta.Apellido AS RazonSocialLugarDeVenta,'
      'TarjetasDebitoLugaresDeVenta.DLLOperacion'
      'FROM'
      
        'VW_LUGARESDEVENTA AS LugaresDeVenta, TarjetasDebitoLugaresDeVent' +
        'a'
      'WHERE'
      
        '((:OperadorLogistico1 = 0) or (LugaresDeVenta.CodigoOperadorLogi' +
        'stico = :OperadorLogistico2)) AND'
      
        'TarjetasDebitoLugaresDeVenta.CodigoLugarDeVenta =* LugaresDeVent' +
        'a.CodigoPersona AND'
      'TarjetasDebitoLugaresDeVenta.CodigoTarjeta = :Tarjeta')
    Left = 508
    Top = 212
  end
  object ActualizarDLLOperacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDLLOperacionTarjetaDebito;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DLLOperacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 536
    Top = 212
  end
  object qry_prefijos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoTarjeta'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  *'
      'FROM'
      '  TarjetasDebitoPrefijos'
      'WHERE'
      '  CodigoTarjeta = :CodigoTarjeta')
    Left = 424
    Top = 184
  end
  object ObtenerDomiciliosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomiciliosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 80
    Top = 152
  end
  object dsDomicilios: TDataSource
    DataSet = cdsDomicilios
    Left = 108
    Top = 152
  end
  object cdsDomicilios: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoDomicilio'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTipoOrigenDato'
        DataType = ftSmallint
      end
      item
        Name = 'DescriTipoOrigenDato'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoCalle'
        DataType = ftInteger
      end
      item
        Name = 'Numero'
        DataType = ftInteger
      end
      item
        Name = 'Piso'
        DataType = ftSmallint
      end
      item
        Name = 'Dpto'
        Attributes = [faFixed]
        DataType = ftString
        Size = 4
      end
      item
        Name = 'CalleDesnormalizada'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Detalle'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoPostal'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Activo'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoTipoEdificacion'
        DataType = ftSmallint
      end
      item
        Name = 'DescriTipoEdificacion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 15
      end
      item
        Name = 'DomicilioEntrega'
        DataType = ftBoolean
      end
      item
        Name = 'CalleRelacionadaUno'
        DataType = ftInteger
      end
      item
        Name = 'NumeroCalleRelacionadaUno'
        DataType = ftInteger
      end
      item
        Name = 'CalleRelacionadaDos'
        DataType = ftInteger
      end
      item
        Name = 'NumeroCalleRelacionadaDos'
        DataType = ftInteger
      end
      item
        Name = 'Normalizado'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTipoCalle'
        DataType = ftSmallint
      end
      item
        Name = 'DescriCalle'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoPais'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriPais'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoRegion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriRegion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoComuna'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriComuna'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoCiudad'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriCiudad'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DireccionCompleta'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'DomicilioCompleto'
        DataType = ftString
        Size = 200
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'dspDomicilios'
    StoreDefs = True
    Left = 136
    Top = 152
    object cdsDomiciliosCodigoDomicilio: TIntegerField
      FieldName = 'CodigoDomicilio'
    end
    object cdsDomiciliosCodigoPersona: TIntegerField
      FieldName = 'CodigoPersona'
    end
    object cdsDomiciliosCodigoTipoOrigenDato: TSmallintField
      FieldName = 'CodigoTipoOrigenDato'
    end
    object cdsDomiciliosDescriTipoOrigenDato: TStringField
      FieldName = 'DescriTipoOrigenDato'
      Size = 50
    end
    object cdsDomiciliosCodigoCalle: TIntegerField
      FieldName = 'CodigoCalle'
    end
    object cdsDomiciliosNumero: TIntegerField
      FieldName = 'Numero'
    end
    object cdsDomiciliosPiso: TSmallintField
      FieldName = 'Piso'
    end
    object cdsDomiciliosDpto: TStringField
      FieldName = 'Dpto'
      FixedChar = True
      Size = 4
    end
    object cdsDomiciliosCalleDesnormalizada: TStringField
      FieldName = 'CalleDesnormalizada'
      Size = 100
    end
    object cdsDomiciliosDetalle: TStringField
      FieldName = 'Detalle'
      Size = 50
    end
    object cdsDomiciliosCodigoPostal: TStringField
      FieldName = 'CodigoPostal'
      FixedChar = True
      Size = 10
    end
    object cdsDomiciliosActivo: TBooleanField
      FieldName = 'Activo'
    end
    object cdsDomiciliosCodigoTipoEdificacion: TSmallintField
      FieldName = 'CodigoTipoEdificacion'
    end
    object cdsDomiciliosDescriTipoEdificacion: TStringField
      FieldName = 'DescriTipoEdificacion'
      FixedChar = True
      Size = 15
    end
    object cdsDomiciliosDomicilioEntrega: TBooleanField
      FieldName = 'DomicilioEntrega'
    end
    object cdsDomiciliosCalleRelacionadaUno: TIntegerField
      FieldName = 'CalleRelacionadaUno'
    end
    object cdsDomiciliosNumeroCalleRelacionadaUno: TIntegerField
      FieldName = 'NumeroCalleRelacionadaUno'
    end
    object cdsDomiciliosCalleRelacionadaDos: TIntegerField
      FieldName = 'CalleRelacionadaDos'
    end
    object cdsDomiciliosNumeroCalleRelacionadaDos: TIntegerField
      FieldName = 'NumeroCalleRelacionadaDos'
    end
    object cdsDomiciliosNormalizado: TIntegerField
      FieldName = 'Normalizado'
    end
    object cdsDomiciliosCodigoTipoCalle: TSmallintField
      FieldName = 'CodigoTipoCalle'
    end
    object cdsDomiciliosDescriCalle: TStringField
      FieldName = 'DescriCalle'
      Size = 100
    end
    object cdsDomiciliosCodigoPais: TStringField
      FieldName = 'CodigoPais'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosDescriPais: TStringField
      FieldName = 'DescriPais'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosDescriRegion: TStringField
      FieldName = 'DescriRegion'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosCodigoComuna: TStringField
      FieldName = 'CodigoComuna'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosDescriComuna: TStringField
      FieldName = 'DescriComuna'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosCodigoCiudad: TStringField
      FieldName = 'CodigoCiudad'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosDescriCiudad: TStringField
      FieldName = 'DescriCiudad'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosDireccionCompleta: TStringField
      FieldName = 'DireccionCompleta'
      Size = 111
    end
    object cdsDomiciliosDomicilioCompleto: TStringField
      FieldName = 'DomicilioCompleto'
      Size = 200
    end
  end
  object dspDomicilios: TDataSetProvider
    DataSet = ObtenerDomiciliosPersona
    Left = 168
    Top = 152
  end
  object ActualizarDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@LugarNacimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@FechaNacimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoActividad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoSituacionIVA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoDomicilioEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PIN'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 4
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 424
    Top = 76
  end
  object ActualizarMedioComunicacionPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioComunicacionPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@HorarioDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 452
    Top = 76
  end
  object ObtenerDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 481
    Top = 76
  end
  object EliminarDomiciliosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarDomiciliosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ListaDomicilios'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 600
    Top = 140
  end
  object EliminarTodosDomiciliosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTodosDomiciliosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 628
    Top = 140
  end
  object ActualizarDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoEdificacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Depto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 658
    Top = 140
  end
  object ActualizarDomicilioEntregaPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioEntregaPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilioEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 686
    Top = 140
  end
  object ActualizarDomicilioRelacionado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioRelacionado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 714
    Top = 140
  end
end
