unit ABMApartadosEvento;

interface

uses
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ComCtrls, ToolWin, ImgList, ExtCtrls, StdCtrls,
  Validate, TimeEdit, VariantComboBox, DB, ADODB, Provider, DBClient, DMConnection,
  UtilDB, UtilProc, Util, DateEdit, PeaProcs;

type
  TfrmApartadosEvento = class(TForm)
    ilImagenes: TImageList;
    pnl1: TPanel;
    tlbBotonera: TToolBar;
    btnSalir: TToolButton;
    btn1: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    btn2: TToolButton;
    btnImprimir: TToolButton;
    btn3: TToolButton;
    btnBuscar: TToolButton;
    pnlInferior: TPanel;
    nb1: TNotebook;
    btnSalir1: TButton;
    btnCancelar: TButton;
    btnGuardar: TButton;
    pnlMedio: TPanel;
    dbgrdEventos: TDBGrid;
    pnl4: TPanel;
    spEventosTransitosPendientesApartar_SELECT: TADOStoredProc;
    dtstprEventos: TDataSetProvider;
    cdsEventos: TClientDataSet;
    dsEventos: TDataSource;
    lbl1: TLabel;
    edtCodigoEvento: TEdit;
    lbl2: TLabel;
    vcbPuntoCobro: TVariantComboBox;
    lbl3: TLabel;
    edtDescripcion: TEdit;
    dateDesde: TDateEdit;
    timeDesde: TTimeEdit;
    dateHasta: TDateEdit;
    timeHasta: TTimeEdit;
    lbl4: TLabel;
    lbl5: TLabel;
    lblEstadoEvento: TLabel;
    spEventosTransitosPendientesApartar_INSERT: TADOStoredProc;
    spEventosTransitosPendientesApartar_UPDATE: TADOStoredProc;
    spEventosTransitosPendientesApartar_DELETE: TADOStoredProc;
    btnAnular: TButton;
    spEventosTransitosPendientesApartar_ANULAR: TADOStoredProc;
    procedure btnSalir1Click(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure cdsEventosAfterScroll(DataSet: TDataSet);
    procedure btnCancelarClick(Sender: TObject);
    procedure dbgrdEventosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnGuardarClick(Sender: TObject);
    procedure dateDesdeChange(Sender: TObject);
    procedure timeDesdeChange(Sender: TObject);
    procedure dateHastaChange(Sender: TObject);
    procedure timeHastaChange(Sender: TObject);
    procedure dbgrdEventosDblClick(Sender: TObject);
    procedure btnAnularClick(Sender: TObject);
  private
    { Private declarations }
    Accion: Integer; // 0: Consultar; 1: Insertar; 2: Actualizar
    EstadoEvento: Integer; // 0: Pendiente; 1: En curso; 2: Finalizado; 3: Anulado    
    FGuardandoDatos: Boolean;
    
    procedure Limpiar_Campos;
    procedure CargarRegistros;
    procedure CargarEstadoEvento;
    procedure CargarDatosRegistro;
    procedure HabilitarDeshabilitarControles(Estado: Boolean);
    function ValidarFechaHoraDesde: Boolean;
    function ValidarFechaHoraHasta: Boolean;
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

var
  frmApartadosEvento: TfrmApartadosEvento;

implementation

{$R *.dfm}

function TfrmApartadosEvento.Inicializar: Boolean;
var
    sp: TADOStoredProc;
begin
    Result := True;
    Screen.Cursor := crHourGlass;

    Accion := 0;
    lblEstadoEvento.Caption := EmptyStr;

    vcbPuntoCobro.Clear;
    sp := TADOStoredProc.Create(Self);
    try
        try
            sp.Connection := DMConnections.BaseCOP;
            sp.ProcedureName := 'ObtenerPuntosCobroMonitor';
            sp.CommandTimeout := 30;
            sp.Parameters.Refresh;
            sp.Open;
            vcbPuntoCobro.Items.Add('(seleccione)','');
            while not sp.Eof do begin
                vcbPuntoCobro.Items.Add(
                    sp.FieldByName('Descripcion').AsString,
                    sp.FieldByName('NumeroPuntoCobro').AsInteger);
                sp.Next;
            end;
            sp.Close;
            
            Limpiar_Campos;
            CargarRegistros;
            HabilitarDeshabilitarControles(False);
        except
            on e: Exception do begin
                MsgBoxErr('Error inicializando el formulario', e.Message, 'Error', MB_ICONERROR);
                Result := False;
            end;
        end;
    finally
        sp.Free;
        Screen.Cursor := crDefault;
    end;
end;

procedure TfrmApartadosEvento.CargarRegistros;
begin
    spEventosTransitosPendientesApartar_SELECT.Parameters.Refresh;
    spEventosTransitosPendientesApartar_SELECT.ExecProc;

    if spEventosTransitosPendientesApartar_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
        raise Exception.Create(spEventosTransitosPendientesApartar_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

    cdsEventos.Data := dtstprEventos.Data;
end;

procedure TfrmApartadosEvento.HabilitarDeshabilitarControles(Estado: Boolean);
begin
    btnSalir.Enabled        := not Estado;
    btnAgregar.Enabled      := not Estado;
    btnEliminar.Enabled     := (not Estado) and (cdsEventos.RecordCount > 0);
    btnEditar.Enabled       := (not Estado) and (cdsEventos.RecordCount > 0);
    dbgrdEventos.Enabled    := (not Estado) and (cdsEventos.RecordCount > 0);

    vcbPuntoCobro.Enabled   := Estado;
    dateDesde.Enabled       := Estado;
    timeDesde.Enabled       := Estado;
    dateHasta.Enabled       := Estado;
    timeHasta.Enabled       := Estado;
    edtDescripcion.Enabled  := Estado;

    btnAnular.Enabled       := False;

    if Estado then
        nb1.ActivePage := 'pgAltaModi'
    else
        nb1.ActivePage := 'pgSalir';
end;

procedure TfrmApartadosEvento.Limpiar_Campos;
begin
    lblEstadoEvento.Caption := EmptyStr;
    edtCodigoEvento.Text := EmptyStr;
    vcbPuntoCobro.ItemIndex := 0;
    dateDesde.Date := Now + 1;
    dateHasta.Date := Now + 2;
    timeDesde.Time := 0;
    timeHasta.Time := 0;
    edtDescripcion.Text := EmptyStr;
end;

procedure TfrmApartadosEvento.timeDesdeChange(Sender: TObject);
begin
    ValidarFechaHoraDesde;
end;

procedure TfrmApartadosEvento.timeHastaChange(Sender: TObject);
begin
    ValidarFechaHoraHasta;
end;

procedure TfrmApartadosEvento.btnAgregarClick(Sender: TObject);
begin
    Accion := 1;
    Limpiar_Campos;
    HabilitarDeshabilitarControles(True);
    edtDescripcion.SetFocus;
end;

procedure TfrmApartadosEvento.btnAnularClick(Sender: TObject);
resourcestring
    MSG_TITLE = 'Anular Evento';
    MSG_CONFIRM = 'Anular un Evento finalizado enviar� a Validaci�n Autom�tica todo los Tr�nsitos apartados por el mismo. �Desea continuar?';
    MSG_ERROR_ANULAR = 'Ha habido un error al anular el Evento';
    MSG_EXITO = 'El Evento ha sido anulado';
var
    I: Integer;
begin
    if MsgBox(MSG_CONFIRM, MSG_TITLE, MB_YESNO+MB_ICONQUESTION) = ID_NO then
        Exit;

    try
        spEventosTransitosPendientesApartar_ANULAR.Parameters.Refresh;
        spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@NumeroPuntoCobro').Value := cdsEventos.FieldByName('NumeroPuntoCobro').Value;
        spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@FechaHoraDesdeAnular').Value := cdsEventos.FieldByName('FechaHoraAlta').Value;
        spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@FechaHoraHastaAnular').Value := cdsEventos.FieldByName('FechaHoraBaja').Value;  
        spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@CodigoEvento').Value := cdsEventos.FieldByName('CodigoEvento').Value;
        spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        spEventosTransitosPendientesApartar_ANULAR.ExecProc;

        if (spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@RETURN_VALUE').Value < 0) then
            raise Exception.Create(spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@ErrorDescription').Value);

    except
        on e: Exception do begin
            MsgBoxErr(Format(MSG_ERROR_ANULAR, [cdsEventos.FieldByName('FechaHoraAlta').Value,
                                                cdsEventos.FieldByName('FechaHoraBaja').Value]),
                e.Message, 'Error', MB_ICONERROR);
            Exit;
        end;
    end;

    MsgBox(MSG_EXITO, MSG_TITLE, MB_ICONINFORMATION);

    Accion := 0;
    CargarRegistros;
    HabilitarDeshabilitarControles(False);
end;

procedure TfrmApartadosEvento.btnCancelarClick(Sender: TObject);
begin
    Accion := 0;
    Limpiar_Campos;
    HabilitarDeshabilitarControles(False);
    CargarDatosRegistro;
end;

procedure TfrmApartadosEvento.btnEditarClick(Sender: TObject);
begin
    Accion := 2;
    HabilitarDeshabilitarControles(True);
    CargarEstadoEvento;
    edtDescripcion.SetFocus;
end;

procedure TfrmApartadosEvento.btnEliminarClick(Sender: TObject);
resourcestring                                             
    MSG_TITLE = 'Eliminar Evento';
    MSG_CONFIRM = '�Desea eliminar el Evento seleccionado?';
    MSG_ERROR = 'Error eliminando el Evento';
    MSG_EN_CURSO = 'No es posible eliminar el Evento porque se encuentra en curso o finalizado';
    MSG_ELIMINADO = 'El Evento ha sido eliminado';
begin
    if MsgBox(MSG_CONFIRM, MSG_TITLE, MB_YESNO+MB_ICONQUESTION) = ID_NO then
        Exit;

    if cdsEventos.FieldByName('FechaHoraAlta').AsDateTime <= NowBase(DMConnections.BaseCOP) then begin
        MsgBox(MSG_EN_CURSO, 'Error', MB_ICONSTOP);
        Exit;
    end;

    try
        spEventosTransitosPendientesApartar_DELETE.Parameters.Refresh;
        spEventosTransitosPendientesApartar_DELETE.Parameters.ParamByName('@CodigoEvento').Value := cdsEventos.FieldByName('CodigoEvento').AsInteger;
        spEventosTransitosPendientesApartar_DELETE.ExecProc;

        if spEventosTransitosPendientesApartar_DELETE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
            raise Exception.Create(spEventosTransitosPendientesApartar_DELETE.Parameters.ParamByName('@ErrorDescription').Value);
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, 'Error', MB_ICONERROR);
            Exit;
        end;
    end;

    MsgBox(MSG_ELIMINADO, MSG_TITLE, MB_ICONINFORMATION);

    CargarRegistros;
end;

procedure TfrmApartadosEvento.btnSalir1Click(Sender: TObject);
begin
    Close;
end;

procedure TfrmApartadosEvento.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmApartadosEvento.cdsEventosAfterScroll(DataSet: TDataSet);
begin
    if FGuardandoDatos then Exit;    

    CargarDatosRegistro;
end;

procedure TfrmApartadosEvento.CargarDatosRegistro;
begin
    edtCodigoEvento.Text := cdsEventos.FieldByName('CodigoEvento').AsString;
    vcbPuntoCobro.Value := cdsEventos.FieldByName('NumeroPuntoCobro').AsInteger;
    dateDesde.Date := cdsEventos.FieldByName('FechaHoraAlta').AsDateTime;
    timeDesde.Time := cdsEventos.FieldByName('FechaHoraAlta').AsDateTime - dateDesde.Date;
    dateHasta.Date := cdsEventos.FieldByName('FechaHoraBaja').AsDateTime;
    timeHasta.Time := cdsEventos.FieldByName('FechaHoraBaja').AsDateTime - dateHasta.Date;
    edtDescripcion.Text := cdsEventos.FieldByName('Descripcion').AsString;
    CargarEstadoEvento;
end;

procedure TfrmApartadosEvento.dateDesdeChange(Sender: TObject);
begin
    ValidarFechaHoraDesde;
end;

procedure TfrmApartadosEvento.dateHastaChange(Sender: TObject);
begin
    ValidarFechaHoraHasta;
end;

procedure TfrmApartadosEvento.dbgrdEventosDblClick(Sender: TObject);
begin
    btnEditarClick(btnEditar);
end;

procedure TfrmApartadosEvento.dbgrdEventosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
    if cdsEventos.FieldByName('Anulado').AsBoolean then begin
        dbgrdEventos.Canvas.Font.Color := clRed;
        dbgrdEventos.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
end;

procedure TfrmApartadosEvento.CargarEstadoEvento;
resourcestring
    STR_ANULADO = 'El Evento se encuentra finalizado, y ha sido Anulado.';        
    STR_PENDIENTE = 'El Evento se encuentra pendiente y puede ser modificado libremente.';
    STR_EN_CURSO = 'El Evento se encuentra en curso. S�lo podr� aumentar la fecha de fin.';
    STR_FINALIZADO = 'El Evento se encuentra finalizado. S�lo podr� acotar el periodo de fechas o Anularlo.';
var
    Ahora: TDateTime;
begin
    btnAnular.Enabled := False;
    if (Accion = 1) or (cdsEventos.RecordCount = 0) then Exit;

    if cdsEventos.FieldByName('Anulado').AsBoolean then begin
        EstadoEvento := 3;
        lblEstadoEvento.Caption := STR_ANULADO;
        lblEstadoEvento.Update;     
        vcbPuntoCobro.Value := cdsEventos.FieldByName('NumeroPuntoCobro').AsInteger;
        dateDesde.Date := cdsEventos.FieldByName('FechaHoraAlta').AsDateTime;
        timeDesde.Time := cdsEventos.FieldByName('FechaHoraAlta').AsDateTime - dateDesde.Date;
        dateHasta.Date := cdsEventos.FieldByName('FechaHoraBaja').AsDateTime;
        timeHasta.Time := cdsEventos.FieldByName('FechaHoraBaja').AsDateTime - dateHasta.Date;
        vcbPuntoCobro.Enabled := False;
        dateDesde.Enabled := False;
        timeDesde.Enabled := False;    
        dateHasta.Enabled := False;
        timeHasta.Enabled := False;
        Exit;
    end;

    Ahora := NowBase(DMConnections.BaseCOP);

    if (cdsEventos.FieldByName('FechaHoraAlta').AsDateTime > Ahora) then begin
        EstadoEvento := 0;
        lblEstadoEvento.Caption := STR_PENDIENTE;
        lblEstadoEvento.Update;
        Exit;
    end;

    if (cdsEventos.FieldByName('FechaHoraAlta').AsDateTime <= Ahora)
      and (cdsEventos.FieldByName('FechaHoraBaja').AsDateTime >= Ahora) then begin
        EstadoEvento := 1;
        lblEstadoEvento.Caption := STR_EN_CURSO;
        lblEstadoEvento.Update;
        vcbPuntoCobro.Value := cdsEventos.FieldByName('NumeroPuntoCobro').AsInteger;
        dateDesde.Date := cdsEventos.FieldByName('FechaHoraAlta').AsDateTime;
        timeDesde.Time := cdsEventos.FieldByName('FechaHoraAlta').AsDateTime - dateDesde.Date;
        vcbPuntoCobro.Enabled := False;
        dateDesde.Enabled := False;
        timeDesde.Enabled := False;
        Exit;
    end;

    if (cdsEventos.FieldByName('FechaHoraBaja').AsDateTime < Ahora) then begin
        EstadoEvento := 2;
        lblEstadoEvento.Caption := STR_FINALIZADO;
        lblEstadoEvento.Update;                
        vcbPuntoCobro.Value := cdsEventos.FieldByName('NumeroPuntoCobro').AsInteger;
        vcbPuntoCobro.Enabled := False;
        btnAnular.Enabled := (Accion = 2);
        Exit;
    end;
end;

function TfrmApartadosEvento.ValidarFechaHoraDesde: Boolean;
resourcestring
    MSG_FECHA_DESDE = 'El inicio del evento debe ser a futuro';
    MSG_ACOTAR = 'Solo puede acotar el rango de fechas, no extenderlo';
var
    FechaHoraDesde,
    Ahora: TDateTime;
begin
    Result := True;
    if Accion = 0 then Exit;

    Ahora := NowBase(DMConnections.BaseCOP);
    FechaHoraDesde := dateDesde.Date + timeDesde.Time;
    if (EstadoEvento = 0) and (FechaHoraDesde < Ahora) then begin
        MsgBoxBalloon(MSG_FECHA_DESDE, 'Error', MB_ICONSTOP, dateDesde);
        dateDesde.Date := Ahora + 1;
        timeDesde.Time := 0;
        Result := False;
        Exit;
    end;

    if Accion = 1 then Exit;

    if (EstadoEvento = 2) and (FechaHoraDesde < cdsEventos.FieldByName('FechaHoraAlta').AsDateTime) then begin
        MsgBoxBalloon(MSG_ACOTAR, 'Error', MB_ICONSTOP, dateDesde);
        dateDesde.Date := cdsEventos.FieldByName('FechaHoraAlta').AsDateTime;
        timeDesde.Time := cdsEventos.FieldByName('FechaHoraAlta').AsDateTime - dateDesde.Date;
        Result := False;
        Exit;
    end;
end;

function TfrmApartadosEvento.ValidarFechaHoraHasta: Boolean;
resourcestring
    MSG_FECHA_DESDE = 'El fin del evento debe ser a futuro';
    MSG_EXTENDER = 'El fin del evento solo puede extenderse';
    MSG_ACOTAR = 'Solo puede acotar el rango de fechas, no extenderlo';
var
    FechaHoraHasta,
    Ahora: TDateTime;
begin                        
    Result := True;
    if Accion = 0 then Exit;

    Ahora := NowBase(DMConnections.BaseCOP);
    FechaHoraHasta := dateHasta.Date + timeHasta.Time;
    if (EstadoEvento = 0) and (FechaHoraHasta < Ahora) then begin
        MsgBoxBalloon(MSG_FECHA_DESDE, 'Error', MB_ICONSTOP, dateHasta);
        dateHasta.Date := Ahora + 2;
        timeHasta.Time := 0;     
        Result := False;
        Exit;
    end;

    if Accion = 1 then Exit;    

    if (EstadoEvento = 1) and (FechaHoraHasta < cdsEventos.FieldByName('FechaHoraBaja').AsDateTime) then begin
        MsgBoxBalloon(MSG_EXTENDER, 'Error', MB_ICONSTOP, dateHasta);
        dateHasta.Date := cdsEventos.FieldByName('FechaHoraBaja').AsDateTime;
        timeHasta.Time := cdsEventos.FieldByName('FechaHoraBaja').AsDateTime - dateHasta.Date;   
        Result := False;
        Exit;
    end;

    if (EstadoEvento = 2) and (FechaHoraHasta > cdsEventos.FieldByName('FechaHoraBaja').AsDateTime) then begin
        MsgBoxBalloon(MSG_ACOTAR, 'Error', MB_ICONSTOP, dateHasta);
        dateHasta.Date := cdsEventos.FieldByName('FechaHoraBaja').AsDateTime;
        timeHasta.Time := cdsEventos.FieldByName('FechaHoraBaja').AsDateTime - dateHasta.Date;   
        Result := False;
        Exit;
    end;
end;

procedure TfrmApartadosEvento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmApartadosEvento.btnGuardarClick(Sender: TObject);
resourcestring
    MSG_FALTA_PDC = 'Debe seleccionar un Punto de Cobro';       
    MSG_FALTA_FDESDE = 'Debe ingresar una fecha de inicio';
    MSG_FALTA_HDESDE = 'Debe ingresar una hora de inicio';
    MSG_FALTA_FHASTA = 'Debe ingresar una fecha de fin';
    MSG_FALTA_HHASTA = 'Debe ingresar una hora de fin';
    MSG_FALTA_DESC = 'Debe ingresar una descripci�n';
    MSG_RANGO_FECHAS = 'La fecha hora de inicio no puede ser mayor que la de fin';
    MSG_ERROR_INSERT = 'Error al insertar evento';
    MSG_ERROR_UPDATE = 'Error al actualizar evento';
    MSG_ERROR_ACOTAR = 'Error acotando el rango de fechas de %s a %s';
    MSG_ACOTAR_FECHAS = 'Acotar el rango de un Evento finalizado enviar� a Validaci�n Autom�tica todo los Tr�nsitos apartados por el Evento. �Desea continuar?';
var
    FechaHoraDesde, FechaHoraHasta: TDateTime;
    I: Integer;
begin
    CargarEstadoEvento;

    if vcbPuntoCobro.ItemIndex = 0 then begin
        MsgBoxBalloon(MSG_FALTA_PDC, 'Error', MB_ICONSTOP, vcbPuntoCobro);
        Exit;
    end;

    if dateDesde.Text = EmptyStr then begin
        MsgBoxBalloon(MSG_FALTA_FDESDE, 'Error', MB_ICONSTOP, dateDesde);
        Exit;
    end;

    if timeDesde.Text = EmptyStr then begin
        MsgBoxBalloon(MSG_FALTA_HDESDE, 'Error', MB_ICONSTOP, timeDesde);
        Exit;
    end;

    if dateHasta.Text = EmptyStr then begin
        MsgBoxBalloon(MSG_FALTA_FHASTA, 'Error', MB_ICONSTOP, dateHasta);
        Exit;
    end;

    if timeHasta.Text = EmptyStr then begin
        MsgBoxBalloon(MSG_FALTA_HHASTA, 'Error', MB_ICONSTOP, timeHasta);
        Exit;
    end;         

    if edtDescripcion.Text = EmptyStr then begin
        MsgBoxBalloon(MSG_FALTA_DESC, 'Error', MB_ICONSTOP, edtDescripcion);
        Exit;
    end;

    FechaHoraDesde := dateDesde.Date + timeDesde.Time;
    FechaHoraHasta := dateHasta.Date + timeHasta.Time;

    if FechaHoraDesde >= FechaHoraHasta then begin
        MsgBoxBalloon(MSG_RANGO_FECHAS, 'Error', MB_ICONSTOP, dateHasta);
        Exit;
    end;

    if not ValidarFechaHoraDesde then Exit;

    if not ValidarFechaHoraHasta then Exit;

    try
        cdsEventos.DisableControls;
        FGuardandoDatos := True;

        if Accion = 1 then begin
            try
                spEventosTransitosPendientesApartar_INSERT.Parameters.Refresh;
                spEventosTransitosPendientesApartar_INSERT.Parameters.ParamByName('@NumeroPuntoCobro').Value := vcbPuntoCobro.Value;
                spEventosTransitosPendientesApartar_INSERT.Parameters.ParamByName('@FechaHoraAlta').Value := FechaHoraDesde;
                spEventosTransitosPendientesApartar_INSERT.Parameters.ParamByName('@FechaHoraBaja').Value := FechaHoraHasta;
                spEventosTransitosPendientesApartar_INSERT.Parameters.ParamByName('@Descripcion').Value := edtDescripcion.Text;
                spEventosTransitosPendientesApartar_INSERT.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                spEventosTransitosPendientesApartar_INSERT.ExecProc;

                if (spEventosTransitosPendientesApartar_INSERT.Parameters.ParamByName('@RETURN_VALUE').Value < 0) then
                    raise Exception.Create(spEventosTransitosPendientesApartar_INSERT.Parameters.ParamByName('@ErrorDescription').Value);

                edtCodigoEvento.Text := spEventosTransitosPendientesApartar_INSERT.Parameters.ParamByName('@CodigoEvento').Value;

                cdsEventos.Append;
                
                for I := 0 to cdsEventos.Fields.Count - 1 do
                    cdsEventos.Fields[I].ReadOnly := False;

                cdsEventos.FieldByName('CodigoEvento').Value := spEventosTransitosPendientesApartar_INSERT.Parameters.ParamByName('@CodigoEvento').Value;
                cdsEventos.FieldByName('NumeroPuntoCobro').Value := vcbPuntoCobro.Value;
                cdsEventos.FieldByName('DescripcionPuntoCobro').Value := vcbPuntoCobro.Text;
                cdsEventos.FieldByName('FechaHoraAlta').Value := FechaHoraDesde;
                cdsEventos.FieldByName('FechaHoraBaja').Value := FechaHoraHasta;
                cdsEventos.FieldByName('Descripcion').Value := edtDescripcion.Text;
                cdsEventos.FieldByName('Anulado').Value := False;
                cdsEventos.FieldByName('AnuladoDesc').Value := 'NO';
                cdsEventos.FieldByName('UsuarioCreacion').Value := UsuarioSistema;
                cdsEventos.FieldByName('FechaHoraCreacion').Value := Now;
                cdsEventos.Post;                                
            except
                on e: Exception do begin
                    MsgBoxErr(MSG_ERROR_INSERT, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        end;      

        if Accion = 2 then begin
            try
                if (EstadoEvento = 2)
                    and ((FechaHoraDesde > cdsEventos.FieldByName('FechaHoraAlta').Value)
                    or (FechaHoraHasta < cdsEventos.FieldByName('FechaHoraBaja').Value)) then
                begin
                    if MsgBox(MSG_ACOTAR_FECHAS, 'Confirmaci�n', MB_ICONQUESTION+MB_YESNO) = ID_NO then
                        Exit;
                end;

                spEventosTransitosPendientesApartar_UPDATE.Parameters.Refresh;
                spEventosTransitosPendientesApartar_UPDATE.Parameters.ParamByName('@CodigoEvento').Value := edtCodigoEvento.Text;
                spEventosTransitosPendientesApartar_UPDATE.Parameters.ParamByName('@NumeroPuntoCobro').Value := vcbPuntoCobro.Value;
                spEventosTransitosPendientesApartar_UPDATE.Parameters.ParamByName('@FechaHoraAlta').Value := FechaHoraDesde;
                spEventosTransitosPendientesApartar_UPDATE.Parameters.ParamByName('@FechaHoraBaja').Value := FechaHoraHasta;
                spEventosTransitosPendientesApartar_UPDATE.Parameters.ParamByName('@Descripcion').Value := edtDescripcion.Text;
                spEventosTransitosPendientesApartar_UPDATE.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                spEventosTransitosPendientesApartar_UPDATE.ExecProc;

                if (spEventosTransitosPendientesApartar_UPDATE.Parameters.ParamByName('@RETURN_VALUE').Value < 0) then
                    raise Exception.Create(spEventosTransitosPendientesApartar_UPDATE.Parameters.ParamByName('@ErrorDescription').Value);

                if EstadoEvento = 2 then begin
                    try
                        if FechaHoraDesde > cdsEventos.FieldByName('FechaHoraAlta').Value then begin
                            spEventosTransitosPendientesApartar_ANULAR.Parameters.Refresh;
                            spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@NumeroPuntoCobro').Value := cdsEventos.FieldByName('NumeroPuntoCobro').Value;
                            spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@FechaHoraDesdeAnular').Value := cdsEventos.FieldByName('FechaHoraAlta').Value;
                            spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@FechaHoraHastaAnular').Value := FechaHoraDesde;
                            spEventosTransitosPendientesApartar_ANULAR.ExecProc;

                            if (spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@RETURN_VALUE').Value < 0) then
                                raise Exception.Create(spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@ErrorDescription').Value);
                        end;
                    except
                        on e: Exception do begin
                            MsgBoxErr(Format(MSG_ERROR_ACOTAR, [cdsEventos.FieldByName('FechaHoraAlta').Value, FechaHoraDesde]),
                                e.Message, 'Error', MB_ICONERROR);
                            Exit;
                        end;
                    end;

                    try
                        if FechaHoraHasta < cdsEventos.FieldByName('FechaHoraBaja').Value then begin
                            spEventosTransitosPendientesApartar_ANULAR.Parameters.Refresh;             
                            spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@NumeroPuntoCobro').Value := cdsEventos.FieldByName('NumeroPuntoCobro').Value;
                            spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@FechaHoraDesdeAnular').Value := FechaHoraHasta;
                            spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@FechaHoraHastaAnular').Value := cdsEventos.FieldByName('FechaHoraBaja').Value;
                            spEventosTransitosPendientesApartar_ANULAR.ExecProc;

                            if (spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@RETURN_VALUE').Value < 0) then
                                raise Exception.Create(spEventosTransitosPendientesApartar_ANULAR.Parameters.ParamByName('@ErrorDescription').Value);
                        end;
                    except
                        on e: Exception do begin
                            MsgBoxErr(Format(MSG_ERROR_ACOTAR, [FechaHoraHasta, cdsEventos.FieldByName('FechaHoraBaja').Value]),
                                e.Message, 'Error', MB_ICONERROR);
                            Exit;
                        end;
                    end;
                end;
                
                cdsEventos.Edit;
                cdsEventos.FieldByName('NumeroPuntoCobro').Value := vcbPuntoCobro.Value;
                cdsEventos.FieldByName('DescripcionPuntoCobro').Value := vcbPuntoCobro.Text;
                cdsEventos.FieldByName('FechaHoraAlta').Value := FechaHoraDesde;
                cdsEventos.FieldByName('FechaHoraBaja').Value := FechaHoraHasta;
                cdsEventos.FieldByName('Descripcion').Value := edtDescripcion.Text;
                cdsEventos.FieldByName('UsuarioModificacion').Value := UsuarioSistema;
                cdsEventos.FieldByName('FechaHoraModificacion').Value := Now;
                cdsEventos.Post;                                
            except
                on e: Exception do begin
                    MsgBoxErr(MSG_ERROR_UPDATE, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        end;
    finally
        cdsEventos.EnableControls;    
        FGuardandoDatos := False;
    end;

    Accion := 0;
    HabilitarDeshabilitarControles(False);
end;

end.
