{*******************************************************************************

Etiqueta    : TASK_105_MGO_20161230
Descripci�n : Se elimina columna y campo para Concesionaria
              Se agregan Sentido, TotalKms y CantidadPorticos

*******************************************************************************}

unit ABMDomain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList, XMLIntf, XMLDoc,
  ToolWin, ComCtrls, UtilProc, Util, StrUtils, PeaProcs, DBCtrls, UtilDB,       // TASK_105_MGO_20161230
  VariantComboBox, DmiCtrls, Mask;


resourcestring
	MSG_ERROR_UPDATE = 'Error Actualizando Registro';
 	MSG_ERROR_INSERT = 'Error Agregando Registro';
  	MSG_ERROR_DELETE = 'Error Eliminando Registro';
   	MSG_ERROR_SELECT = 'Error Consultando Registros';
    HINT_SALIR       = 'Salir del ABM';
    HINT_AGREGAR     = 'Agregar Domain';
    HINT_ELIMINAR    = 'Eliminar Domain';
    HINT_EDITAR      = 'Editar Domain';
    ANSW_ELIMINAR    = 'Est� seguro de eliminar este Domain? - ';
    ANSW_SALIR       = 'Confirma que desea salir?';

type
  TfrmABMDomain = class(TForm)
    procSelect: TADOStoredProc;
    dsGrid: TDataSource;
    Grid: TDBGrid;
    pnlSuperior: TPanel;
    pnlControles: TPanel;
    pnlInferior: TPanel;
    Botonera: TToolBar;
    Imagenes: TImageList;
    btnSalir: TToolButton;
    ImageList1: TImageList;
    ToolButton2: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    ToolButton6: TToolButton;
    btnImprimir: TToolButton;
    ToolButton8: TToolButton;
    btnBuscar: TToolButton;
    edDescripcion: TEdit;
    lbl02: TLabel;
    btnGuardar: TButton;
    btnCancelar: TButton;
    procInsert: TADOStoredProc;
    procDelete: TADOStoredProc;
    procUpdate: TADOStoredProc;
    lbl01: TLabel;
    edDomainID: TEdit;
    procSelectDomainID: TSmallintField;
    procSelectDescripcion: TStringField;
    procSelectCodigoConcesionaria: TWordField;
    procSelectUsuarioCreacion: TStringField;
    procSelectFechaHoraCreacion: TDateTimeField;
    procSelectUsuarioModificacion: TStringField;
    // INICIO : TASK_105_MGO_20161230
    procSelectFechaHoraModificacion: TDateTimeField;
    qrySentido: TADOQuery;
    fltfldSelectTotalKms: TFloatField;
    intgrfldSelectCantidadPorticos: TIntegerField;
    // FIN : TASK_105_MGO_20161230
    procedure FormCreate(Sender: TObject);
    procedure HabilitaBotones(Botones : string);
    function TraeRegistros : Boolean;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure procSelectAfterScroll(DataSet: TDataSet);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    function Inicializar(MDIChild: Boolean): Boolean;
    procedure HabilitarDeshabilitarControles(Estado : Boolean);
    function GetCamposPK:string;                                                // TASK_105_MGO_20161230
    //procedure vcbSentidoChange(Sender: TObject);                              // TASK_151_MGO_20170314  // TASK_105_MGO_20161230
  private
    { Private declarations }
    CantidadRegistros : integer;
    Posicion          : TBookmark;
    Accion            : Integer; // 1=agregar   2=eliminar  3=editar
    Retorno           : Integer;
    MensajeError      : string;
    FIELDS_PK         : string;

  public
    { Public declarations }
  end;

var
  frmABMDomain: TfrmABMDomain;

implementation

uses DMConnection;

{$R *.dfm}

procedure TfrmABMDomain.HabilitarDeshabilitarControles(Estado : Boolean);
var
  i : Integer;
begin
   { INICIO : TASK_105_MGO_20161230
   for i := 0 to ComponentCount - 1 do
    if ContainsText('TEdit, ', Components[i].ClassName) then begin
      if Components[i].ClassName = 'TEdit' then
        if TEdit(Components[i]).Name = 'edDomainID' then
          TEdit(Components[i]).ReadOnly := Estado
        else
          TEdit(Components[i]).ReadOnly := not Estado;


    end;
    }
    edDomainID.Enabled := Estado;
    edDescripcion.Enabled := Estado;
    { INICIO : TASK_151_MGO_20170314
    vcbSentido.Enabled := Estado;
    edtTotalKms.Enabled := False;
    txtCantidadPorticos.Enabled := False;
    if Estado then begin
        edtTotalKms.Enabled := (vcbSentido.Value <> '');
        txtCantidadPorticos.Enabled := (vcbSentido.Value <> '');
    end;
    } // FIN : TASK_151_MGO_20170314
    // FIN : TASK_105_MGO_20161230
end;


function TfrmABMDomain.GetCamposPK : string;
var
  qry           : TADOQuery;
  XML, Salida   : string;
  DocXML        : IXMLDocument;
  Nodo          : IXMLNode;
begin

  qry := TADOQuery.Create(nil);
  qry.Connection := DMConnections.BaseBO_Master;
  qry.SQL.Text :=   'SELECT                                                                                                          ' +
                    '   CAST(                                                                                                        ' +
                    '       (SELECT COLUMN_NAME AS Nombre                                                                            ' +
                    '           FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A                                                          ' +
                    '               LEFT JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B ON A.CONSTRAINT_NAME = B.CONSTRAINT_NAME  ' +
                    '           where A.TABLE_SCHEMA + ''.'' + A.TABLE_NAME = ''dbo.Domain'' and CONSTRAINT_TYPE = ''PRIMARY KEY'' ' +
                    '       FOR XML PATH(''Campo''), ROOT(''Campos''), TYPE)                                                         ' +
                    '       AS VARCHAR(8000)) AS Campos                                                                              ' ;
  qry.Open;
  XML := qry.FieldByName('Campos').AsString;
  qry.Close;
  FreeAndNil(qry);

  DocXML          := TXMLDocument.Create(nil);
  DocXML.XML.Text := XML;
  DocXML.Active   := True;

  Nodo := DocXML.DocumentElement.ChildNodes.FindNode('Campo');

  Salida := EmptyStr;

  repeat
    if Nodo <> nil then begin
      Salida := Salida  + Nodo.ChildNodes.Nodes[0].NodeValue + ', ';
      Nodo   := Nodo.NextSibling;
    end;
  until Nodo = nil;

  DocXML.Active   := False;
  FreeAndNil(Nodo);

  Result          := Salida;

end;


function TfrmABMDomain.Inicializar(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
    Position := poMainFormCenter;
	Result := True;
end;


function TfrmABMDomain.TraeRegistros;
var
  Resultado : Boolean;

begin
    Accion := 0;    // TASK_105_MGO_20161230
    try
        { INICIO : TASK_151_MGO_20170314
        // INICIO : TASK_105_MGO_20161230
        qrySentido.Open;
        vcbSentido.Items.Clear;
        vcbSentido.Items.Add('(Ninguno)','');
        while not qrySentido.Eof do begin
            vcbSentido.Items.Add(qrySentido.FieldByName('Descripcion').AsString,
                                qrySentido.FieldByName('Sentido').AsString);
            qrySentido.Next;
        end;
        qrySentido.Close;
        vcbSentido.ItemIndex := 0;
        // FIN : TASK_105_MGO_20161230
        } // FIN : TASK_151_MGO_20170314

        procSelect.Parameters.Refresh;
        procSelect.Close;
        procSelect.Open;
        Retorno := procSelect.Parameters.ParamByName('@RETURN_VALUE').Value;
        if Retorno <> 0 then begin
            MensajeError := procSelect.Parameters.ParamByName('@ErrorDescription').Value;
            Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
        end;
        CantidadRegistros := procSelect.RecordCount;
        try
            procSelect.GotoBookmark(Posicion);
        except
        end;

        Resultado := True;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
            Resultado := False;
        end;
    end;
    Result := Resultado;
end;

{ INICIO : TASK_151_MGO_20170314
// INICIO : TASK_105_MGO_20161230
procedure TfrmABMDomain.vcbSentidoChange(Sender: TObject);
begin
    if Accion > 0 then begin
        edtTotalKms.Enabled := (vcbSentido.Value <> '');
        txtCantidadPorticos.Enabled := (vcbSentido.Value <> '');
    end;
end;
// FIN : TASK_105_MGO_20161230
} // FIN : TASK_151_MGO_20170314

procedure TfrmABMDomain.btnAgregarClick(Sender: TObject);
begin
  HabilitaBotones('000000110');

  { INICIO : TASK_105_MGO_20161230
  with edDomainID do begin
      Enabled  := True;
      ReadOnly := False;
      Text     := '';
      SetFocus;
  end;
  with edDescripcion do begin
      Enabled := True;
      Text    := '';
  end;
   with edCodigo do begin
      Enabled := True;
      Text    := '';
  end;
  }                
  Accion := 1;
  edDomainID.Text := '';
  edDescripcion.Text := '';
  { INICIO : TASK_151_MGO_20170314
  vcbSentido.ItemIndex := 0;
  edtTotalKms.Text := '';
  txtCantidadPorticos.Value := 0;
  } // FIN : TASK_151_MGO_20170314
  HabilitarDeshabilitarControles(True);
  // FIN : TASK_105_MGO_20161230
end;

procedure TfrmABMDomain.btnCancelarClick(Sender: TObject);
begin
  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
  try
    procSelect.GotoBookmark(Posicion);
  except
  end;

  with edDomainID do begin
    Enabled := False;
    Text    := procSelect.FieldByName('DomainID').AsString;
  end;

  with edDescripcion do begin
    Enabled := False;
    Text    := procSelect.FieldByName('Descripcion').AsString;
  end;
  { INICIO : TASK_105_MGO_20161230
  with edCodigo do begin
    Enabled := False;
    Text    := procSelect.FieldByName('CodigoConcesionaria').AsString;
  end;
  } // FIN : TASK_105_MGO_20161230
end;

procedure TfrmABMDomain.btnEditarClick(Sender: TObject);
begin
  HabilitaBotones('000000110');

  HabilitarDeshabilitarControles(True);

  { INICIO : TASK_151_MGO_20170314
  with edDomainID do begin
      Enabled := True;
      SetFocus;
  end;
  } // FIN : TASK_151_MGO_20170314

  with edDescripcion do begin
      Enabled := True;
      SetFocus;     // TASK_151_MGO_20170314
  end;
  { INICIO : TASK_105_MGO_20161230
  with edCodigo do begin
      Enabled := True;
  end;
  } // FIN : TASK_105_MGO_20161230
  Accion := 3;
end;

procedure TfrmABMDomain.btnEliminarClick(Sender: TObject);
begin
  HabilitaBotones('000000000');
  Accion := 2;
  try
    Posicion := procSelect.GetBookmark;
  except
  end;
  if Application.MessageBox(PChar(ANSW_ELIMINAR + procSelect.FieldByName('Descripcion').AsString),'Pregunta',MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    with procDelete do begin
        Parameters.Refresh;
        Parameters.ParamByName('@DomainID').Value  := procSelect.FieldByName('DomainID').AsInteger;
        try
          ExecProc;
          Retorno := procDelete.Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
            MensajeError := procDelete.Parameters.ParamByName('@ErrorDescription').Value;
            MsgBoxErr(MSG_ERROR_DELETE, MensajeError, Caption, MB_ICONERROR);
          end;
        except
          on E : Exception do begin
            MsgBoxErr(MSG_ERROR_DELETE, E.Message, Caption, MB_ICONERROR);
          end;
        end;
    end;
    TraeRegistros;
    if CantidadRegistros > 0 then
      HabilitaBotones('111100001')
    else
      HabilitaBotones('110000001');
end;

procedure TfrmABMDomain.btnGuardarClick(Sender: TObject);
resourcestring                                                                  // TASK_105_MGO_20161230
    QRY_EXISTS_ID = 'SELECT TOP 1 1 FROM Domain (NOLOCK) WHERE DomainID = %s';  // TASK_105_MGO_20161230
var                                                                             // TASK_105_MGO_20161230
    CodigoConcesionariaNativa: Integer;                                         // TASK_105_MGO_20161230
    vTotalKms: Double;
    Code: Integer;
begin

    try
        StrToInt(edDomainID.Text);
    except
        Application.MessageBox('Ingrese el Domain','Problema', MB_ICONEXCLAMATION);
        Exit;
    end;


    if Length(edDescripcion.Text) = 0 then begin
        Application.MessageBox('Ingrese la Descripci�n del Domain','Problema', MB_ICONEXCLAMATION);
        Exit;
    end;
    { INICIO : TASK_105_MGO_20161230
    try
        StrToInt(edCodigo.Text);
        except
        Application.MessageBox('Ingrese el C�digo de la Concesionaria','Problema', MB_ICONEXCLAMATION);
        Exit;
    end;
    }
    CodigoConcesionariaNativa := ObtenerCodigoConcesionariaNativa();
    { INICIO : TASK_151_MGO_20170314
    if edtTotalKms.Text <> EmptyStr then begin
        vTotalKms := 0;
        Val(ReplaceStr(edtTotalKms.Text,',','.'), vTotalKms, Code);
        if Code <> 0 then begin
            MsgBoxBalloon('Formato decimal de Kms incorrecto','Error',MB_ICONSTOP,edtTotalKms);
            Exit;
        end;

        if vTotalKms > 9999 then begin
            MsgBoxBalloon('El largo total es demasiado alto (M�x: 9999)','Error',MB_ICONSTOP,edtTotalKms);
            Exit;
        end;
    end;
    // FIN : TASK_105_MGO_20161230
    } // FIN : TASK_151_MGO_20170314

  try
    Posicion := procSelect.GetBookmark;
  except
  end;
  if Accion = 1 then begin
    // INICIO : TASK_105_MGO_20161230
    if QueryGetValueInt(DMConnections.BaseBO_Master, Format(QRY_EXISTS_ID, [edDomainID.Text])) = 1 then begin
        MsgBoxBalloon('El ID ingresado ya existe','Error',MB_ICONSTOP,edDomainID);
        Exit;
    end;
    // FIN : TASK_105_MGO_20161230

    with procInsert do begin
        Parameters.Refresh;
        Parameters.ParamByName('@DomainID').Value               := StrToInt(edDomainID.Text);
        Parameters.ParamByName('@Descripcion').Value            := edDescripcion.Text;
        //Parameters.ParamByName('@CodigoConcesionaria').Value    := StrToInt(edCodigo.Text);   // TASK_105_MGO_20161230
        Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionariaNativa;   // TASK_105_MGO_20161230
        Parameters.ParamByName('@UsuarioCreacion').Value        := UsuarioSistema;
        { INICIO : TASK_151_MGO_20170314
        // INICIO : TASK_105_MGO_20161230
        if vcbSentido.Value <> '' then begin
            Parameters.ParamByName('@Sentido').Value            := vcbSentido.Value;
            Parameters.ParamByName('@TotalKms').Value           := IIf(vTotalKms > 0, vTotalKms, Null);
            Parameters.ParamByName('@CantidadPorticos').Value   := txtCantidadPorticos.ValueInt;
        end;
        // FIN : TASK_105_MGO_20161230
        } // FIN : TASK_151_MGO_20170314

        try
          ExecProc;
          Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
            MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
            MsgBoxErr(MSG_ERROR_INSERT, MensajeError, Caption, MB_ICONERROR);
          end;
        except
          on E : Exception do begin
            MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
          end;
        end;
    end;
  end;

  if Accion = 3 then begin
    with procUpdate do begin
        Parameters.Refresh;
        Parameters.ParamByName('@DomainID').Value               := procSelect.FieldByName('DomainID').Value;
        Parameters.ParamByName('@DomainIDNew').Value            := StrToInt(edDomainID.Text);
        //Parameters.ParamByName('@CodigoConcesionaria').Value    := StrToInt(edCodigo.Text);   // TASK_105_MGO_20161230
        Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionariaNativa;   // TASK_105_MGO_20161230
        Parameters.ParamByName('@Descripcion').Value            := edDescripcion.Text;
        Parameters.ParamByName('@UsuarioModificacion').Value    := UsuarioSistema;
        { INICIO : TASK_151_MGO_20170314
        // INICIO : TASK_105_MGO_20161230
        if vcbSentido.Value <> '' then begin                                  
            Parameters.ParamByName('@Sentido').Value            := vcbSentido.Value;
            Parameters.ParamByName('@TotalKms').Value           := IIf(vTotalKms > 0, vTotalKms, Null);
            Parameters.ParamByName('@CantidadPorticos').Value   := txtCantidadPorticos.ValueInt;
        end;
        // FIN : TASK_105_MGO_20161230
        } // FIN : TASK_151_MGO_20170314
        try
          ExecProc;
          Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
            MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
            MsgBoxErr(MSG_ERROR_UPDATE, MensajeError, Caption, MB_ICONERROR);
          end;
        except
          on E : Exception do begin
            MsgBoxErr(MSG_ERROR_UPDATE, E.Message, Caption, MB_ICONERROR);
          end;
        end;
    end;
  end;

  // INICIO : TASK_105_MGO_20161230
  //edDomainID.Enabled       := False;
  //edDescripcion.Enabled    := False;
  //edCodigo.Enabled         := False;
  HabilitarDeshabilitarControles(False);
  // FIN : TASK_105_MGO_20161230
  TraeRegistros;
  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
end;

procedure TfrmABMDomain.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMDomain.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    { INICIO : TASK_151_MGO_20170314
    if Application.MessageBox(PChar(ANSW_SALIR),'Pregunta', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
        Action := caFree
    else
        Action := caNone;
    }
    Action := caFree;
    // FIN : TASK_151_MGO_20170314
end;

procedure TfrmABMDomain.FormCreate(Sender: TObject);
var
  SP_Aux : TADOStoredProc;
begin
  btnSalir.Hint     := HINT_SALIR;
  btnAgregar.Hint   := HINT_AGREGAR;
  btnEliminar.Hint  := HINT_ELIMINAR;
  btnEditar.Hint    := HINT_EDITAR;

  procSelect.Connection                     := DMConnections.BaseBO_Master;
  procInsert.Connection                     := DMConnections.BaseBO_Master;
  procUpdate.Connection                     := DMConnections.BaseBO_Master;
  procDelete.Connection                     := DMConnections.BaseBO_Master;

  FIELDS_PK := GetCamposPK;

  TraeRegistros;
  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
end;

procedure TfrmABMDomain.HabilitaBotones(Botones : string);
begin
  btnSalir.Enabled      := Botones[1] = '1';
  btnAgregar.Enabled    := Botones[2] = '1';
  btnEliminar.Enabled   := Botones[3] = '1';
  btnEditar.Enabled     := Botones[4] = '1';
  btnImprimir.Enabled   := Botones[5] = '1';
  btnBuscar.Enabled     := Botones[6] = '1';
  btnGuardar.Enabled    := Botones[7] = '1';
  btnCancelar.Enabled   := Botones[8] = '1';
  Grid.Enabled          := Botones[9] = '1';
end;

procedure TfrmABMDomain.procSelectAfterScroll(DataSet: TDataSet);
begin
    edDomainID.Text       := DataSet.FieldByName('DomainID').AsString;
    edDescripcion.Text    := DataSet.FieldByName('Descripcion').AsString;
    { INICIO : TASK_151_MGO_20170314
    // INICIO : TASK_105_MGO_20161230
    //edCodigo.Text         := DataSet.FieldByName('CodigoConcesionaria').AsString;
    vcbSentido.Value := DataSet.FieldByName('Sentido').AsString;
    edtTotalKms.Text := DataSet.FieldByName('TotalKms').AsString;
    txtCantidadPorticos.Value := DataSet.FieldByName('CantidadPorticos').AsInteger;
    // FIN : TASK_105_MGO_20161230
    } // FIN : TASK_151_MGO_20170314
end;

end.



