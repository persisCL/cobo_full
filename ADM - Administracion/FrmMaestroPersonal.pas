{********************************** File Header *********************************
File Name   : FrmMaestroPersonal
Author      : Tinao, Diego <dtinao@dpsautomation.com>
Date Created: 04-Sep-2003
Language    : ES-AR
Description : Esta Unit realiza la administraci�n del Maestro de Personal
    		  que realizar� las tareas de Mantenimiento.
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Date		: 27-Feb-2004
Author		: Marrone, Pablo <pmarrone@dpsautomation.com>
Description : Eliminaci�n Campo Activo de Medios de Comunicacion y de Domicilios
--------------------------------------------------------------------------------
Date		: 06-Abr-2004
Author		: Calani, Daniel <dcalani@dpsautomation.com>
Description : Insercion de los frema de personal, domicilio y medio de contacto
********************************************************************************
Revision 3:
    Author : lcanteros
    Date : 01/07/2008
    Description : Mdificaciones de SS 639

Revision 4:
    Author : mpiazza
    Date : 09/02/2009
    Description : Mdificaciones de SS 639 creacion de usuario cuando ya
    posee convenio, esto significa que puede tener convenio y ser usuario
    del sistema

Firma       : SS_1419_NDR_20151130
Descripcion : El SP ActualizarDatosPersona tiene el nuevo parametro @CodigoUsuario
              para grabarlo en los campos de auditoria de la tabla Personas y en
              HistoricoPersonas

Firma       : SS_1433_NDR_20151222
Descripcion : Corregir error transaccional (se cambian transacciones Delphi por transacciones SQL)
}

unit FrmMaestroPersonal;

interface

uses         
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util,
  UtilProc, UtilDb, Abm_obj, OleCtrls, DmiCtrls, Grids,DmConnection, ADODB,
  Variants, ComCtrls, PeaProcs, Validate, BuscaTab, MaskCombo,
  DPSControls, Peatypes, TimeEdit, Provider, DBClient, ListBoxEx, DBListEx,
  FrmEditarDomicilio, FrmEditarMedioComunicacion, DBGrids, BuscaClientes,
  freListDomicilios, frePersonas, freListMediosContactos;

type
  TFormMaestroPersonal = class(TForm)
    pnlInferior: TPanel;
	AbmToolbar1: TAbmToolbar;
    Notebook: TNotebook;
    alMaestroPersonal: TAbmList;
	MaestroPersonal: TADOTable;
	pcDatosPersonal: TPageControl;
    tsCliente: TTabSheet;
    tsDomicilio: TTabSheet;
    tsContacto: TTabSheet;
	tsSueldos: TTabSheet;
    pnlSueldos: TPanel;
    Label1: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    txtSueldoBruto: TNumericEdit;
    txtSueldoNeto: TNumericEdit;
	cbTiposSueldo: TComboBox;
    Label19: TLabel;
    cbCategoria: TComboBox;
    ActualizarDatosMaestroPersonal: TADOStoredProc;
    chkActivo2: TCheckBox;
    pnlDomicilio: TPanel;
    ActualizarMedioComunicacionPersona: TADOStoredProc;
    pnlMediosContacto: TPanel;
    EliminarMaestroPersonal: TADOStoredProc;
	EliminarDomiciliosPersona: TADOStoredProc;
	ActualizarDomicilio: TADOStoredProc;
    ActualizarDomicilioEntregaPersona: TADOStoredProc;
    ActualizarDomicilioRelacionado: TADOStoredProc;
    ActualizarDatosPersona: TADOStoredProc;
    ObtenerDatosPersona: TADOStoredProc;
	EliminarTodosDomiciliosPersona: TADOStoredProc;
    pnlDatosPersonal: TPanel;
    FrePersona: TFramePersona;
    FreDomicilios: TFrameListDomicilios;
    FreMediosContactos: TFrameListMediosContactos;
    Label3: TLabel;
    Panel1: TPanel;
    PNL_Chk_Borrar: TPanel;
    chkActivo: TCheckBox;
    tmrInicializarBusqueda: TTimer;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    tmrValidarRUT: TTimer;
	procedure AbmToolbar1Close(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnCancelarClick(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
    procedure alMaestroPersonalClick(Sender: TObject);
    procedure alMaestroPersonalDelete(Sender: TObject);
    procedure alMaestroPersonalDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure alMaestroPersonalEdit(Sender: TObject);
	procedure alMaestroPersonalInsert(Sender: TObject);
	procedure alMaestroPersonalRefresh(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FreDomiciliosbtnAgregarDomicilioClick(Sender: TObject);
    procedure FrePersonabtn_BuscarClick(Sender: TObject);
    procedure FrePersonatxt_DocumentoChange(Sender: TObject);
    procedure alMaestroPersonalKeyPress(Sender: TObject; var Key: Char);
    procedure tmrInicializarBusquedaTimer(Sender: TObject);
    procedure tmrValidarRUTTimer(Sender: TObject);

  private
	{ Private declarations }
    FDatosPersonales: TDatosPersonales;
    FContenidoBuscar: String;
    FValidandoRUT, FAsociadoAConvenio: boolean;
	procedure HabilitarCamposEdicion(habilitar : Boolean);
	procedure Limpiar_Campos;
	procedure Volver_Campos;
    procedure Cargar_Campos;
    //function EstaAsociadoaAlgunConvenioCN(NumeroDocumento,TipoDocumento:String): boolean;		//SS_1147_MCA_20140408
    function EstaAsociadoaAlgunConvenioNativo(NumeroDocumento,TipoDocumento:String): boolean;       //SS_1147_MCA_20140408
    function ObtenerDatosPersonas(Var RegistroPersona:TDatosPersonales; TipoDocumento:String='';NumeroDocumento:String=''):Boolean;
    function EsUsuario(NumeroDocumento,TipoDocumento:String): boolean;
    procedure HabilitarModificarSiAsociadoAConvenio(Valor: boolean);
    procedure ProcesarValidarRUT(Sender: TObject);
    function EsRUTValido (Rut: string): boolean;
  public
	{ Public declarations }
	Function Inicializar(MDIChild: Boolean = True): Boolean;

  end;

var
    FormMaestroPersonal:    TFormMaestroPersonal;
    CodigoPersonaActual:    integer;
    RecNoDomicilioEntrega:  integer;
    FCodigoDomicilioEntrega:integer;
implementation

uses RStrings;
{$R *.DFM}

{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.Volver_Campos
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    None
  Return Value:  None
  Description:   Vuelve al estado Inicial los controles del Form
********************************************************************************}
procedure TFormMaestroPersonal.Volver_Campos ;
begin
    CodigoPersonaActual := -1;
	alMaestroPersonal.Estado := Normal;
    HabilitarCamposEdicion(False);
	Notebook.PageIndex		:= 0;
    FrePersona.Inicializar();
//    FreDomicilios.inicializar();
//    FreMediosContactos.inicializar();
    alMaestroPersonal.Reload;
	alMaestroPersonal.Refresh;
    alMaestroPersonal.SetFocus;
	Screen.Cursor := crDefault;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.Inicializar
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    MDIChild: Boolean = True
  Return Value:  Boolean
  Description:   Inicializa el Form de Maestro de Personal. Primero determina el
    	formato en que se va a mostrar la pantalla seg�n el par�metro MDIChild y
    	luego intenta abrir la tabla "MaestroPersonal"
********************************************************************************}
function TFormMaestroPersonal.Inicializar(MDIChild: Boolean = True): Boolean;
Var
	S: TSize;
begin
    FValidandoRUT := false;
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
    pcDatosPersonal.ActivePage := tsCliente;
	if not OpenTables([MaestroPersonal]) then Exit;

    FrePersona.Inicializar();
//    FreDomicilios.inicializar();
//    FreMediosContactos.inicializar();
    CargarCategoriasPersonal(DMConnections.BaseCAC, cbCategoria);

	CargarTiposSueldo(DMConnections.BaseCAC, cbTiposSueldo);

	Volver_Campos;

    tsDomicilio.TabVisible := false;
    tsContacto.TabVisible := false;
    tsSueldos.TabVisible := False;

   	Notebook.PageIndex := 0;
	Result := True;
end;



{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.Limpiar_Campos
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    None
  Return Value:  None
  Description:   Limpia los valores que figuran en los controles.
********************************************************************************}
procedure TFormMaestroPersonal.Limpiar_Campos;
begin
	//Datos De la Persona
    FrePersona.Inicializar();

    // Domicilios
//    FreDomicilios.inicializar();

    // Medios de contacots
//    FreMediosContactos.inicializar();

    //Datos del Empleado.
    cbCategoria.ItemIndex := 0;
	txtSueldoBruto.Clear;
	txtSueldoNeto.Clear;
    cbTiposSueldo.ItemIndex := -1;
end;

procedure TFormMaestroPersonal.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormMaestroPersonal.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormMaestroPersonal.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormMaestroPersonal.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
    Cargar_Campos;
end;

{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.BtnAceptarClick
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Inserta o Modifica los datos de un Empleado utilizando el
  	procedimiento almacenado "ActualizarMaestroPersonal".
    Antes de Realizar dichas operaciones verifica que esten completos los
    compos considerados obligatorios.
********************************************************************************}
procedure TFormMaestroPersonal.BtnAceptarClick(Sender: TObject);

    function GrabarDomicilioRelacionado(CodigoDomicilio:Integer;CodigoPais,CodigoRegion,CodigoComuna:String;CodigoCalle:Integer;Numero:Integer=-1):Boolean; begin
        try
            result:=True;
            with ActualizarDomicilioRelacionado.Parameters do begin

                ParamByName('@CodigoDomicilio').Value:=CodigoDomicilio;
                ParamByName('@CodigoPais').Value:=CodigoPais;
                ParamByName('@CodigoRegion').Value:=CodigoRegion;
                ParamByName('@CodigoComuna').Value:=CodigoComuna;
                ParamByName('@CodigoCalle').Value:=CodigoCalle;
                ParamByName('@Numero').Value:=iif(Numero<0,NULL,Numero);

            end;
            ActualizarDomicilioRelacionado.ExecProc;
            ActualizarDomicilioRelacionado.Close;
        except
            On E: exception do begin
                MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[FLD_DOMICILIO_RELACIONADO]), E.Message, Format(MSG_CAPTION_ACTUALIZAR,[FLD_DOMICILIO_RELACIONADO]), MB_ICONSTOP);
                Result:=False;
            end;
        end;
    end;
var
	TodoOk: Boolean;
//	CodigosEliminar: AnsiString;
//	i,FCodigoDomicilio: integer;
begin
    if (alMaestroPersonal.Estado in [Alta, Modi]) and FAsociadoAConvenio then begin
    { valido el rut pq es el unico campo que esta activo cuando pertenece a un convenio
     ya que no se pueden modificar los datos, sino puede fallar
     la validaci�n en los otros controles deshabilitados}
        if not ValidateControls([FrePersona.txt_Documento, FrePersona.txt_Documento],
            [FrePersona.txt_Documento.Text <> EmptyStr, EsRUTValido(FrePersona.txt_Documento.Text)],
             MSG_CAPTION_VALIDA,
            [MSG_ERROR_DOCUMENTO, MSG_ERROR_DOCUMENTO]) then Exit;

    end
    else begin // es modificacion o alta sin convenio asociado
        if not(FrePersona.Validar) then begin
            pcDatosPersonal.ActivePageIndex := 0;
            exit;
        end;

        if FrePersona.Personeria<>PERSONERIA_FISICA then begin
    	    MsgBoxBalloon(FLD_PERSONERIA+' Inv�lida',self.Caption,MB_ICONSTOP,FrePersona.cb_Personeria);
            exit;
        end;
    end;

(*
    if not(FreDomicilios.TieneDomiciliosCargados) then begin
        pcDatosPersonal.ActivePageIndex := 1;
        MsgBoxBalloon(format(MSG_VALIDAR_CANTIDAD_DATOS,[FLD_DOMICILIO]),Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_PERSONAL]),MB_ICONSTOP,FreDomicilios.dblDomicilios);
        exit;
    end;

    if not(FreMediosContactos.TieneMediosCargados) then begin
        pcDatosPersonal.ActivePageIndex := 2;
        MsgBoxBalloon(format(MSG_VALIDAR_CANTIDAD_DATOS,[FLD_MEDIO_CONTACTO]),Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_PERSONAL]),MB_ICONSTOP,FreMediosContactos.dblMedios);
        exit;
    end;
*)
	TodoOK          := False;

	//HASTA ACA TODO BIEN
	Screen.Cursor   := crHourGlass;
  //DMConnections.BaseCAC.BeginTrans;                                                   //SS_1433_NDR_20151222
  QueryExecute(DmConnections.BaseCAC, 'BEGIN TRANSACTION trxMaestroPersonal');          //SS_1433_NDR_20151222
	try
		try
            ActualizarDatosPersona.Close;
            With ActualizarDatosPersona.Parameters do begin
                Refresh;                                                               //SS_1433_NDR_20151222
                ParamByName('@CodigoPersona').Value:=FrePersona.CodigoPersona;
                ParamByName('@Personeria').Value:=FrePersona.Personeria;
                ParamByName('@Apellido').Value:=iif(FrePersona.Apellido='',FrePersona.RazonSocial,FrePersona.Apellido);
                ParamByName('@ApellidoMaterno').Value:=iif(FrePersona.ApellidoMaterno='',null,FrePersona.ApellidoMaterno);
                ParamByName('@Nombre').Value:=iif(FrePersona.Nombre='',null,FrePersona.Nombre);
                ParamByName('@CodigoDocumento').Value:=FrePersona.CodigoDocumento;
                ParamByName('@NumeroDocumento').Value:=FrePersona.RUT;
                ParamByName('@Sexo').Value:=iif(FrePersona.Sexo='',null,FrePersona.Sexo);
                ParamByName('@LugarNacimiento').Value:=FrePersona.CodigoPais;
                ParamByName('@FechaNacimiento').Value:=iif(FrePersona.FechaNacimientoOCreacion=NullDate,Null,FrePersona.FechaNacimientoOCreacion);
                ParamByName('@CodigoActividad').Value:= iif(FrePersona.CodigoActividad<=0,Null,FrePersona.CodigoActividad);
                ParamByName('@ApellidoContactoComercial').Value:=iif(FrePersona.ApellidoContacto='',Null,FrePersona.ApellidoContacto);
                ParamByName('@ApellidoMaternoContactoComercial').Value:=iif(FrePersona.ApellidoMaternoContacto='',Null,FrePersona.ApellidoMaternoContacto);
                ParamByName('@NombreContactoComercial').Value:=iif(FrePersona.NombreContacto='',Null,FrePersona.NombreContacto);
                ParamByName('@SexoContactoComercial').Value:=iif(FrePersona.SexoContacto='',Null,FrePersona.SexoContacto);
                ParamByName('@EmailContactoComercial').Value:=iif(FrePersona.Email='',Null,FrePersona.Email);
                ParamByName('@CodigoSituacionIVA').Value:=iif(FrePersona.CodigoSituacionIVA='',Null,FrePersona.CodigoSituacionIVA);
                ParamByName('@Password').Value:=NULL;
                ParamByName('@Pregunta').Value:=NULL;
                ParamByName('@Respuesta').Value:=NULL;
                ParamByName('@CodigoUsuario').Value:=UsuarioSistema;            //SS_1419_NDR_20151130
				//JLO 20160513
                ParamByName('@Giro').Value := null;        //JLO 20160513
            end;

            ActualizarDatosPersona.ExecProc;
            CodigoPersonaActual :=  ActualizarDatosPersona.Parameters.ParamByName('@CodigoPersona').Value;
            ActualizarDatosPersona.Close;

            with EliminarMaestroPersonal.Parameters do begin
            	ParamByName('@CodigoPersona').Value := CodigoPersonaActual;
                ParamByName('@Deshabilitar').Value := not(chkActivo.Checked);
            end;
            EliminarMaestroPersonal.ExecProc;
        except
			  On E: Exception do begin
                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION trxMaestroPersonal END');    //SS_1433_NDR_20151222
                ActualizarDatosPersona.Close;
                MsgBoxErr( Format(MSG_ERROR_ACTUALIZAR,[STR_EMPLEADO]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_PERSONAL]), MB_ICONSTOP);
                Screen.Cursor := crDefault;
                exit;
            end;
        end;
(*
        // Ahora grabo los domicilios
        // Borrar todos los domicilios que figuran en la base que ya no pertenecen al cliente.
        if FreDomicilios.TieneDomiciliosBorrados then begin
            try
                with FreDomicilios.DomiciliosBorrados do begin
                    First;
                    CodigosEliminar:='';
                    while not(eof) do begin
                        CodigosEliminar:=CodigosEliminar+FieldByName('CodigoDomicilio').AsString+',';
                        next;
                    end;
                    CodigosEliminar:=copy(CodigosEliminar,1,length(CodigosEliminar)-1);
                end;
                EliminarDomiciliosPersona.ExecProc;
                EliminarDomiciliosPersona.Close;
              except
                  On E: Exception do begin
                      EliminarDomiciliosPersona.Close;
                      MsgBoxErr( Format(MSG_ERROR_ACTUALIZAR,[STR_EMPLEADO]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_PERSONAL]), MB_ICONSTOP);
                      Screen.Cursor := crDefault;
                      exit;
                  end;
              end;
        end;

        //Ahora grabo los domicilios que quedaron
        FCodigoDomicilioEntrega:=-1;
        if FreDomicilios.TieneDomiciliosCargados then begin
            try
                with FreDomicilios.DomiciliosCargados do begin
                    First;
                    while not(eof) do begin
                        FCodigoDomicilioEntrega:=iif(FieldByName('DomicilioEntrega').AsBoolean,FieldByName('CodigoDomicilio').AsInteger,FCodigoDomicilioEntrega);
                        with ActualizarDomicilio.Parameters do begin
                            ParamByName('@CodigoPersona').Value:=CodigoPersonaActual;
                            ParamByName('@CodigoTipoDomicilio').value:=FieldByName('TipoDomicilio').AsInteger;
                            ParamByName('@CodigoPais').value:=FieldByName('CodigoPais').AsString;
                            ParamByName('@CodigoRegion').value:=FieldByName('CodigoRegion').AsString;
                            ParamByName('@CodigoComuna').value:=FieldByName('CodigoComuna').AsString;
                            ParamByName('@DescripcionCiudad').value:=iif(Trim(FieldByName('DescripcionCiudad').AsString)='',Null,FieldByName('DescripcionCiudad').AsString);
                            ParamByName('@CodigoCalle').value:=iif(Trim(FieldByName('CodigoCalle').AsString)='',Null,FieldByName('CodigoCalle').AsInteger);
                            ParamByName('@CalleDesnormalizada').value:=iif(Trim(FieldByName('CalleDesnormalizada').AsString)='',Null,FieldByName('CalleDesnormalizada').AsString);
                            ParamByName('@Numero').value:=FieldByName('Numero').AsString;
                            ParamByName('@CodigoPostal').value:=iif(Trim(FieldByName('CodigoPostal').AsString)='',Null,FieldByName('CodigoPostal').AsString);
                            ParamByName('@Detalle').value:=iif(Trim(FieldByName('Detalle').AsString)='',Null,FieldByName('Detalle').AsString);
                            ParamByName('@CodigoDomicilio').value:=FieldByName('CodigoDomicilio').AsInteger;
                        end;
                        ActualizarDomicilio.ExecProc;

                        // si tiene, grabo los domicilios relacionados
                        if FieldByName('Normalizado').AsInteger=0 then begin
                            if not(GrabarDomicilioRelacionado(FieldByName('CodigoDomicilio').AsInteger,
                                                              FieldByName('CodigoPais').AsString,
                                                              FieldByName('CodigoRegion').AsString,
                                                              FieldByName('CodigoComuna').AsString,
                                                              FieldByName('CalleRelacionadaUno').AsInteger,
                                                              FormatearNumeroCalle(FieldByName('NumeroCalleRelacionadaUno').AsString))
                                ) or
                                not(GrabarDomicilioRelacionado(FieldByName('CodigoDomicilio').AsInteger,
                                                              FieldByName('CodigoPais').AsString,
                                                              FieldByName('CodigoRegion').AsString,
                                                              FieldByName('CodigoComuna').AsString,
                                                              FieldByName('CalleRelacionadaDos').AsInteger,
                                                              FormatearNumeroCalle(FieldByName('NumeroCalleRelacionadaDos').AsString))
                                ) then begin
                                    ActualizarDomicilioRelacionado.Close;
                                    Screen.Cursor := crDefault;
                                    exit;
                            end;
                        end;
                        ActualizarDomicilio.close;
                        next;
                    end;
                end;
    		except
                On E: exception do begin
                    MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[FLD_DOMICILIO_RELACIONADO]), E.Message, Format(MSG_CAPTION_ACTUALIZAR,[FLD_DOMICILIO_RELACIONADO]), MB_ICONSTOP);
                    ActualizarDomicilio.close;
                    ActualizarDomicilioRelacionado.Close;
                    Screen.Cursor := crDefault;
                    exit;
                end;
            end;
        end;

        //Actualizo en los datos de la persona el Codigo de Domicilio de Entrega.
        try
            with ActualizarDomicilioEntregaPersona.Parameters do begin
                ParamByName('@CodigoPersona').Value             := CodigoPersonaActual;
                ParamByName('@CodigoDomicilioEntrega').Value    := iif(FCodigoDomicilioEntrega< 1, null, FCodigoDomicilioEntrega);
            end;
            ActualizarDomicilioEntregaPersona.ExecProc;
            ActualizarDomicilioEntregaPersona.Close;
		except
            On E: Exception do begin
                ActualizarDomicilioEntregaPersona.Close;
				MsgBoxErr( Format(MSG_ERROR_ACTUALIZAR,[FLD_DOMICILIO]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_DATOS_DOMICILIO]), MB_ICONSTOP);
                Screen.Cursor := crDefault;
                exit;
            end;
        end;

        //Ahora grabo los Medios de Contacto
        QueryExecute( DMConnections.BaseCAC, 'DELETE FROM MediosComunicacion WHERE CodigoPersona = ' + IntToStr(CodigoPersonaActual));

        if FreMediosContactos.TieneMediosCargados then begin
            with FreMediosContactos.MediosCargados do begin
                First;
                While not(Eof) do begin
                    try
                        FCodigoDomicilio := -1;
                        if (Trim(fieldByName('Valor').AsString) = '') then begin
                            // estoy grabando un Medio de Contacto con Domicilio.
                            FCodigoDomicilio := fieldByName('CodigoDomicilio').AsInteger;
                            if (FCodigoDomicilio < 0) then begin
                                //Es un nuevo Domicilio y lo tengo que buscar.
                                for i:= 0 to FreDomicilios.DomiciliosCargados.RecordCount-1 do begin
                                    if i = FieldByName('CodigoDomicilioRecNo').AsInteger then begin
                                        FCodigoDomicilio := FreDomicilios.DomiciliosCargados.FieldByName('CodigoDomicilio').AsInteger;
                                        break;
                                    end;
                                end;
                            end;
                        end;

                        if (FCodigoDomicilio > 0) or (Trim(fieldByName('Valor').AsString) <> '') then begin
                            with ActualizarMedioComunicacionPersona.Parameters do begin
                                ParamByName('@CodigoPersona').Value         := CodigoPersonaActual;
                                ParamByName('@IndiceMedioComunicacion').Value   := -1;//fieldByName('IndiceMedioComunicacion').AsInteger;
                                ParamByName('@CodigoTipoMedioContacto').Value  := fieldByName('CodigoTipoMedioCOntacto').asInteger;
                                ParamByName('@CodigoArea').Value            := iif( fieldByName('CodigoArea').AsInteger = 0, null, fieldByName('CodigoArea').AsInteger);
                                ParamByName('@Valor').Value                 := iif( Trim(fieldByName('Valor').AsString) = '', null, Trim(fieldByName('Valor').AsString));
                                ParamByName('@Anexo').Value                 := iif( Trim(fieldByName('Anexo').AsString) = '', null, Trim(fieldByName('Anexo').AsString));
                                ParamByName('@CodigoDomicilio').Value       := iif( FCodigoDomicilio < 1, null, FCodigoDomicilio);
                                ParamByName('@HorarioDesde').Value          := iif( fieldByName('HorarioDesde').AsDateTime <= nulldate, null, fieldByName('HorarioDesde').AsDateTime);
                                ParamByName('@HorarioHasta').Value          := iif( fieldByName('HorarioDesde').AsDateTime <= nulldate, null, fieldByName('HorarioHasta').AsDateTime);
                                ParamByName('@Observaciones').Value         := iif( Trim(fieldByName('Observaciones').AsString) = '', null, Trim(fieldByName('Observaciones').AsString));
                            end;
                            ActualizarMedioComunicacionPersona.ExecProc;
                            ActualizarMedioComunicacionPersona.Close;
                        end;
                    except
                        on E: exception do begin
                            MsgBoxErr( Format(MSG_ERROR_ACTUALIZAR,[FLD_MEDIO_CONTACTO]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[FLD_MEDIO_CONTACTO]), MB_ICONSTOP);
                            ActualizarMedioComunicacionPersona.Close;
                            Screen.Cursor := crDefault;
                            exit;
                        end;
                    end;
                    Next;
                end;
            end;
        end;
*)
		//Ahora grabo los datos propios del empleado
        try
            with ActualizarDatosMaestroPersonal.Parameters do begin
                ParamByName('@CodigoPersona').Value             := CodigoPersonaActual;
                ParamByName('@CodigoCategoriaEmpleado').Value   := 1;//iif( Ival(StrRight(cbCategoria.Text, 10)) < 1, null, Ival(StrRight(cbCategoria.Text, 10)));
                ParamByName('@CodigoTipoSueldo').Value          := iif( Ival(StrRight(cbTiposSueldo.Text, 10)) < 1, null, Ival(StrRight(cbTiposSueldo.Text, 10)));
                ParamByName('@SueldoBruto').Value               := iif( txtSueldoBruto.Value < 0, null, txtSueldoBruto.Value * 100);
                ParamByName('@SueldoNeto').Value                := iif( txtSueldoNeto.Value < 0, null, txtSueldoNeto.Value * 100);
                ParamByName('@Activo').Value                    := chkActivo.Checked;
            end;
            ActualizarDatosMaestroPersonal.ExecProc;
            ActualizarDatosMaestroPersonal.Close;
		except
            On E: Exception do begin
                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION trxMaestroPersonal END');    //SS_1433_NDR_20151222
                ActualizarDatosMaestroPersonal.Close;
				MsgBoxErr( Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_PERSONAL]), e.message, Format(STR_MAESTRO_PERSONAL,[STR_DATOS_EMPLEADO]), MB_ICONSTOP);
                Screen.Cursor := crDefault;
                exit;
            end;
        end;

        //Fin Datos Empleado
        TodoOK := true;
    finally
        if TodoOK then //DMConnections.BaseCAC.CommitTrans                                                                //SS_1433_NDR_20151222
            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION trxMaestroPersonal END')     //SS_1433_NDR_20151222
        else //DMConnections.BaseCAC.RollbackTrans;                                                                       //SS_1433_NDR_20151222
            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION trxMaestroPersonal END');  //SS_1433_NDR_20151222

        Screen.Cursor := crDefault;
    	Volver_Campos;
        Cargar_Campos;
	end;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalClick
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Completa los campos en pantalla utilizando la informaci�n del registro
    actual de la tabla "MaestroPersonal"
********************************************************************************}
procedure TFormMaestroPersonal.alMaestroPersonalClick(Sender: TObject);
begin
    if MaestroPersonal.FieldByName('CodigoPersona').AsInteger <> CodigoPersonaActual then Cargar_Campos;
end;

{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalDelete
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Elimina un registro de la tabla "MaestroPersonal".
	Primero intenta eliminarlo, en caso de no ser posible debido a la integridad
	referencial, modifica el estado del registro marc�ndolo como inactivo
	mediante la ejecuci�n de un query.
********************************************************************************}
procedure TFormMaestroPersonal.alMaestroPersonalDelete(Sender: TObject);
//var
//    EsUsuario: boolean;
begin
//  EsUsuario := false;
//    EsUsuario :=  QueryGetValue(DMConnections.BaseCAC,
//                        'SELECT 1 FROM UsuariosSistemas ' +
//                        'WHERE CodigoPersona =  '+ MaestroPersonal.FieldByName('CodigoPersona').AsString) <> '';
//    if EsUsuario then
//        MsgBox(MSG_ERROR_PERSONAL_ES_USUARIO, Format(MSG_CAPTION_BAJA,[STR_EMPLEADO]), MB_ICONSTOP)
//    else begin

        if (QueryGetValueInt(DMConnections.BaseCAC,'EXEC PersonaPertencienteAConvenio '+MaestroPersonal.FieldByName('CodigoPersona').AsString)=1) then begin
    		MsgBox(MSG_CAPTION_PERSONA_CONVENIO,self.Caption,MB_ICONSTOP);
            Volver_Campos;
            Exit;
        end;

    	Screen.Cursor := crHourGlass;
        If MsgBox(Format(MSG_QUESTION_BAJA,[STR_EMPLEADO]), STR_CONFIRMACION, MB_YESNO) = IDYES then begin
            try
                //DMConnections.BaseCAC.BeginTrans;                                             //SS_1433_NDR_20151222
                QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION trxMaestroPersonal');    //SS_1433_NDR_20151222

                EliminarMaestroPersonal.Parameters.ParamByName('@CodigoPersona').Value := MaestroPersonal.FieldByName('CodigoPersona').AsInteger;
                EliminarMaestroPersonal.Parameters.ParamByName('@Deshabilitar').Value := True;
                EliminarMaestroPersonal.ExecProc;
//                EliminarMaestroPersonal.Close;

                //DMConnections.BaseCAC.CommitTrans;                                                                          //SS_1433_NDR_20151222
                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION trxMaestroPersonal END');    //SS_1433_NDR_20151222
                alMaestroPersonal.Reload;
            Except
                On E: exception do begin
                    MsgBoxErr(Format(MSG_ERROR_BAJA,[STR_EMPLEADO]), E.message, Format(MSG_CAPTION_BAJA,[STR_EMPLEADO]), MB_ICONSTOP);
//                    EliminarMaestroPersonal.Close;
                    //DMConnections.BaseCAC.RollbackTrans;                                                                        //SS_1433_NDR_20151222
                    QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION trxMaestroPersonal END');    //SS_1433_NDR_20151222
                end;
            end;
        end;
//    end;
	Volver_Campos;
	Screen.Cursor      := crDefault;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalDrawItem
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TDBList; Tabla: TDataSet; Rect: TRect;
  				 State: TOwnerDrawState; Cols: TColPositions
  Return Value:  None
  Description:   Dibuja el la grilla los valores del registro actual de la tabla
				 "MaestroEmpleado"
********************************************************************************}
procedure TFormMaestroPersonal.alMaestroPersonalDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas  do begin
		FillRect(Rect);
        if Tabla.FieldByName('Activo').AsBoolean then begin
        	if odSelected in State then
				Sender.Canvas.Font.Color := clWindow
			else
	        	Sender.Canvas.Font.Color := clWindowText;
		end else begin
			if odSelected in State then
				Sender.Canvas.Font.Color := clYellow
			else
	        	Sender.Canvas.Font.Color := clRed;
		end;

        with Tabla do begin
            TextOut(Cols[0], Rect.Top, IStr(FieldByName('CodigoPersona').AsInteger, 10));
            TextOut(Cols[1], Rect.Top, Trim(FieldByName('Apellido').AsString));
            TextOut(Cols[2], Rect.Top, Trim(FieldByName('Nombre').AsString));
		end;
	end;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalEdit
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Prepara los controles para la Modificaci�n de un registro.
********************************************************************************}
procedure TFormMaestroPersonal.alMaestroPersonalEdit(Sender: TObject);
begin
    alMaestroPersonal.Estado   	:= modi;
    HabilitarCamposEdicion(True);
    case pcDatosPErsonal.ActivePageIndex of
        0: if FrePersona.txt_Documento.Enabled then FrePersona.txt_Documento.SetFocus
        	else chkActivo.SetFocus;
        1: FreDomicilios.dblDomicilios.SetFocus;
        2: FreMediosContactos.dblMedios.SetFocus;
        3: cbCategoria.SetFocus;
    end;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalInsert
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Prepara los controles para la inserci�n de un registro.
********************************************************************************}
procedure TFormMaestroPersonal.alMaestroPersonalInsert(Sender: TObject);
begin
	Limpiar_Campos;
    CodigoPersonaActual         := -1;

    FDatosPersonales.Personeria := PERSONERIA_FISICA;
    FDatosPersonales.LugarNacimiento := PAIS_CHILE;
    FrePersona.Inicializar(FDatosPersonales);

	HabilitarCamposEdicion(True);
    alMaestroPersonal.Estado    := alta;
    chkActivo.Checked           := True;
    pcDatosPersonal.ActivePage  := tsCliente;
    FrePersona.txt_Documento.SetFocus;
end;

procedure TFormMaestroPersonal.alMaestroPersonalRefresh(Sender: TObject);
begin
	if alMAestroPersonal.Empty then Limpiar_Campos;
end;

{******************************** Function Header ******************************
Function Name: TFormMaestroPersonal.HabilitarCamposEdicion
Author       : dcalani
Date Created : 06/04/2004
Description  : Habilita o deshabilita los los paneles en una edici�n
Parameters   : habilitar: Boolean
Return Value : None
    Revision 1:
    Author : lcanteros
    Date : 01/07/2008
    Description : arregle problema del notebook que no volvia al estado normal
    y los botones del abm quedaban deshabilitados en algunas oportunidades
    Revision 2:
    Author : mpiazza
    Date : 10/02/2009
    Description : Mdificaciones de SS 639 creacion de usuario
*******************************************************************************}
procedure TFormMaestroPersonal.HabilitarCamposEdicion(habilitar: Boolean);
begin
    pcDatosPersonal.Enabled	   	    := Habilitar;
    FAsociadoAConvenio := false;
    EnableControlsInContainer(pnlDatosPersonal,habilitar);
    FrePersona.cb_Personeria.Enabled:=False;
    alMaestroPersonal.Enabled	    := not Habilitar;
    if not habilitar then alMaestroPersonal.Estado := Normal;

    Notebook.PageIndex := ord(habilitar);
//    pnlDatosPersonal.Enabled	    := Habilitar;
//    pnlDomicilio.Enabled            := Habilitar;
//	pnlMediosContacto.Enabled	    := Habilitar;
//    pnlSueldos.Enabled			    := Habilitar;
end;
{-----------------------------------------------------------------------------
  Procedure: HabilitarModificarSiAsociadoAConvenio
  Author:    lcanteros
  Date:      01-Jul-2008
  Arguments: Valor: boolean
  Result:    None
  Description: modifica la habilitacionde controles si la persona esta asociada
  a un convenio, los datos no se pueden modificar desde este lugar
  History:
-----------------------------------------------------------------------------}
procedure TFormMaestroPersonal.HabilitarModificarSiAsociadoAConvenio(Valor: boolean);
begin
    with FrePersona do begin
        cb_Personeria.Enabled := Valor;
        txtApellido.Enabled := Valor;
        txtNombre.Enabled := Valor;
        cbLugarNacimiento.Enabled := Valor;
        txtApellidoMaterno.Enabled := Valor;
        cb_Sexo.Enabled := Valor;
        txtFechaNacimiento.Enabled := Valor;
    end;
end;

procedure TFormMaestroPersonal.FormShow(Sender: TObject);
begin
	CodigoPersonaActual := -1;
end;

{******************************** Function Header ******************************
Function Name: TFormMaestroPersonal.Cargar_Campos
Author       : dcalani
Date Created : 06/04/2004
Description  : Carga los datos de una persona en los diferentes controles para porder
                ser editado o visualizado.
Parameters   : None
Return Value : None
*******************************************************************************}
procedure TFormMaestroPersonal.Cargar_Campos;
begin


    MaestroPersonal.DisableControls;

	with MaestroPersonal do begin
        //Cargar Datos Personales
        FrePersona.CargarDatosPersona(FieldByName('CodigoPersona').AsInteger);

        // Cargamos los domicilios
//        FreDomicilios.CargarDomiciliosPersona(FieldByName('CodigoPersona').AsInteger);

        // Cargamos los Medios de Comunicacion
//        FreMediosContactos.CargarMediosContactosPersona(FieldByName('CodigoPersona').AsInteger);

        //Cargar Datos del Empleado
//        CargarCategoriasPersonal(DMConnections.BaseCAC, cbCategoria, FieldByName('CodigoCategoriaEmpleado').AsInteger);
//		CargarTiposSueldo(DMConnections.BaseCAC, cbTiposSueldo, FieldByName('CodigoTipoSueldo').AsInteger);
//		txtSueldoBruto.Value	:= FieldByName('SueldoBruto').AsInteger / 100;
//		txtSueldoNeto.Value	    := FieldByName('SueldoNeto').AsInteger / 100;
        chkActivo.Checked       := FieldByName('Activo').AsBoolean;

        CodigoPersonaActual     := FieldByName('CodigoPersona').AsInteger;
	end;
    MaestroPersonal.EnableControls;

end;

procedure TFormMaestroPersonal.FreDomiciliosbtnAgregarDomicilioClick(
  Sender: TObject);
begin
//  FreDomicilios.btnAgregarDomicilioClick(Sender);
end;

procedure TFormMaestroPersonal.FrePersonabtn_BuscarClick(Sender: TObject);
begin
  FrePersona.btn_BuscarClick(Sender);
end;
{-----------------------------------------------------------------------------
  Procedure: EsRUTValido
  Author:    lcanteros
  Date:      07-Jul-2008
  Arguments: Rut: string
  Result:    boolean
  Description: valida el RUT
  History:
-----------------------------------------------------------------------------}
function TFormMaestroPersonal.EsRUTValido(Rut: string): boolean;
begin
    result := (QueryGetValue(DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
                             StrLeft(Rut, (Length(Rut) -1))  +
                            ''',''' + Rut[Length(Rut)] + '''') <> '0');

end;

//function TFormMaestroPersonal.EstaAsociadoaAlgunConvenioCN(NumeroDocumento,	//SS_1147_MCA_20140408
function TFormMaestroPersonal.EstaAsociadoaAlgunConvenioNativo(NumeroDocumento,   //SS_1147_MCA_20140408
  TipoDocumento: String): boolean;
var
    sp:TADOStoredProc;
begin
    sp:=TADOStoredProc.Create(nil);
    try
        sp.Connection:=DMConnections.BaseCAC;
        //sp.ProcedureName:='AsociadoConvenioCN';								//SS_1147_MCA_20140408
        sp.ProcedureName:='AsociadoConvenioNativo';                             //SS_1147_MCA_20140408
        with sp.Parameters do begin
            Refresh;
            ParamByName('@CodigoDocumento').Value := TipoDocumento;
            ParamByName('@NumeroDocumento').Value := NumeroDocumento;
            ParamByName('@Asociado').Value := NULL;
        end;
        sp.ExecProc;
        Result := sp.Parameters.ParamByName('@Asociado').Value;
	finally
		sp.close;
		sp.Destroy;
	end;

end;

{-----------------------------------------------------------------------------
  Procedure: FrePersonatxt_DocumentoChange
  Author:
  Date:
  Arguments: Sender: TObject
  Result:    None
  Description:
  History:
    Revision 1:
    Author : lcanteros
    Date : 01/07/2008
    Description : Se permite ingresar un empleado que tenga convenio y tenga
    un medio de pago automatico con costanera norte (SS 639)
-----------------------------------------------------------------------------}
procedure TFormMaestroPersonal.FrePersonatxt_DocumentoChange(Sender: TObject);
begin
    tmrValidarRUT.Enabled := true;
end;

function TFormMaestroPersonal.ObtenerDatosPersonas(
  var RegistroPersona: TDatosPersonales; TipoDocumento,
  NumeroDocumento: String): Boolean;
var
    sp:TADOStoredProc;
begin
    sp:=TADOStoredProc.Create(nil);
    try
        try
            result:=False;
            sp.Connection:=DMConnections.BaseCAC;
            sp.ProcedureName:='ObtenerDatosPersona';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@CodigoPersona').Value:= null;
                ParamByName('@CodigoDocumento').Value:=TipoDocumento;
                ParamByName('@NumeroDocumento').Value:=NumeroDocumento;
            end;
            sp.open;
            if sp.RecordCount>0 then begin
                with RegistroPersona do begin
                    Personeria:=(Sp.FieldByName('Personeria').AsString+' ')[1];
                    if Personeria=PERSONERIA_JURIDICA then begin
                        RazonSocial:=Trim(Sp.FieldByName('Apellido').AsString);
                        Apellido:=Trim(Sp.FieldByName('ApellidoContactoComercial').AsString);
                        ApellidoMaterno:=Trim(Sp.FieldByName('ApellidoMaternoContactoComercial').AsString);
                        Nombre:=Trim(Sp.FieldByName('NombreContactoComercial').AsString);
                        Sexo:=(Sp.FieldByName('SexoContactoComercial').AsString+' ')[1];
                    end else begin
                        RazonSocial:='';
                        Apellido:=Trim(Sp.FieldByName('Apellido').AsString);
                        ApellidoMaterno:=Trim(Sp.FieldByName('ApellidoMaterno').AsString);
                        Nombre:=Trim(Sp.FieldByName('Nombre').AsString);
                        sexo:=(Sp.FieldByName('Sexo').AsString+' ')[1];
                    end;

                    CodigoPersona:=Sp.FieldByName('CodigoPersona').AsInteger;
    		        TipoDocumento:=Trim(sp.FieldByName('CodigoDocumento').Value);
	    	        NumeroDocumento:=Trim(sp.FieldByName('NumeroDocumento').Value);

                    if sp.FieldByName('FechaNacimiento').IsNull then FechaNacimiento:=NullDate
                    else FechaNacimiento:=sp.FieldByName('FechaNacimiento').AsDateTime;

                    CodigoActividad:=iif(sp.FieldByName('CodigoActividad').IsNull,-1,sp.FieldByName('CodigoActividad').AsInteger);
                    LugarNacimiento:=Trim(Sp.FieldByName('LugarNacimiento').AsString);

                    if sp.FieldByName('Password').IsNull then Password:=''
                    else Password:=Trim(sp.FieldByName('Password').Value);

                    if sp.FieldByName('Pregunta').IsNull then Pregunta:=''
                    else Trim(sp.FieldByName('Pregunta').AsString);

                    if sp.FieldByName('Respuesta').IsNull then Respuesta:=''
                    else Respuesta:=Trim(sp.FieldByName('Respuesta').AsString);

                    result:=True;
                end;
            end else begin
                result:=False;
//                TFreDatoPresona.LimpiarRegistro(RegistroPersona);
            end;
        except
            on E: exception do begin
                MsgBoxErr( 'Error al obtener datos personas', e.message, 'Error al obtener datos personas RUT:'+NumeroDocumento, MB_ICONSTOP);
                Result := False;
            end;
        end;
    finally
        sp.close;
        sp.Free;
    end;

end;
{-----------------------------------------------------------------------------
  Procedure: ProcesarValidarRUT
  Author:    lcanteros
  Date:      07-Jul-2008
  Arguments: Sender: TObject
  Result:    None
  Description:
  History:

  Revision 1:
    Author : mpiazza
    Date : 09/02/2009
    Description : Mdificaciones de SS 639 creacion de usuario
-----------------------------------------------------------------------------}
procedure TFormMaestroPersonal.ProcesarValidarRUT(Sender: TObject);
resourcestring
    MSG_PERSONA_ASOCIADA_A_CONVENIO = 'Esta persona est� asociada a un convenio';
    CAPTION_AVISO = 'AVISO';
var
    RegPersona: TDatosPersonales;
begin
    if not FrePersona.Enabled then Exit;

    FrePersona.txt_DocumentoChange(Sender);
    FAsociadoAConvenio := false; //supongo que no esta asociado a convenio
    if FrePersona.RUTValido then begin
        if EsUsuario(FrePersona.RUT, FrePersona.CodigoDocumento) then begin
            MsgBoxErr('Ya Existe un usuario con este RUT.', 'S�lo puede haber un usuario por persona.', 'Agregar usuario', MB_ICONERROR);
            Exit;
        end;
        if ObtenerDatosPersonas(RegPersona, FrePersona.CodigoDocumento, FrePersona.RUT) then
            FrePersona.Inicializar(RegPersona);

        //HabilitarCamposEdicion(true);
        // si el modo es alta y esta asociado a algun convenio informo al operador
        //y no se permite cambiar los datos
        //if  EstaAsociadoaAlgunConvenioCN(FrePersona.RUT, FrePersona.CodigoDocumento) or	//SS_1147_MCA_20140408
        if  EstaAsociadoaAlgunConvenioNativo(FrePersona.RUT, FrePersona.CodigoDocumento) or	//SS_1147_MCA_20140408

            (QueryGetValueInt(DMConnections.BaseCAC,'EXEC PersonaPertencienteAConvenio '+ IntToStr(RegPersona.CodigoPersona))=1) then begin
            FAsociadoAConvenio := true;
            //MsgBox(MSG_PERSONA_ASOCIADA_A_CONVENIO, CAPTION_AVISO, MB_ICONINFORMATION);
            //HabilitarModificarSiAsociadoAConvenio(False);
            HabilitarModificarSiAsociadoAConvenio(true);
        end;
    end;
end;

function TFormMaestroPersonal.EsUsuario(NumeroDocumento,
  TipoDocumento: String): boolean;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.EsUsuario(''' + Trim(TipoDocumento) + ''', ''' + Trim(NumeroDocumento) + ''')') = 1;
end;

procedure TFormMaestroPersonal.alMaestroPersonalKeyPress(Sender: TObject;
  var Key: Char);
begin
    tmrInicializarBusqueda.Enabled := False;
    FContenidoBuscar := FContenidoBuscar + Key;
    if MaestroPersonal.Locate('Apellido', FContenidoBuscar, [loPartialKey, loCaseInsensitive]) then begin
        alMaestroPersonal.Reload;
        tmrInicializarBusqueda.Enabled := True;
    end
    else
        FContenidoBuscar := EmptyStr;
end;

procedure TFormMaestroPersonal.tmrInicializarBusquedaTimer(
  Sender: TObject);
begin
    tmrInicializarBusqueda.Enabled := False;
    FContenidoBuscar := EmptyStr;
end;
{-----------------------------------------------------------------------------
  Procedure: tmrValidarRUTTimer
  Author:    lcanteros
  Date:      07-Jul-2008
  Arguments: Sender: TObject
  Result:    None
  Description: Genera la validacion del RUT luego de un segundo para no hacer
  una validaci�n cada vez que se tipea un caracter
  History:
-----------------------------------------------------------------------------}
procedure TFormMaestroPersonal.tmrValidarRUTTimer(Sender: TObject);
begin
    tmrValidarRUT.Enabled := false;
    try
      if not FValidandoRUT then begin
        FValidandoRUT := true;
        ProcesarValidarRUT(Sender);
      end;
    finally
      FValidandoRUT := false;
    end;
end;

end.
