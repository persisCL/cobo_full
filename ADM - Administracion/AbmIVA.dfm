object Form_ABM_IVA: TForm_ABM_IVA
  Left = 248
  Top = 157
  Caption = 'Al'#237'cuotas de I.V.A.'
  ClientHeight = 326
  ClientWidth = 218
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object pnlButtons: TPanel
    Left = 0
    Top = 276
    Width = 218
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 280
    ExplicitWidth = 226
    DesignSize = (
      218
      50)
    object Notebook1: TNotebook
      Left = 16
      Top = 8
      Width = 201
      Height = 38
      Anchors = [akRight, akBottom]
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'Salir'
        object btnSalir: TButton
          Left = 120
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Salir'
          TabOrder = 0
          OnClick = btnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        Caption = 'Editing'
        object btnAceptar: TButton
          Left = 45
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btnAceptarClick
        end
        object btnCancelar: TButton
          Left = 120
          Top = 8
          Width = 75
          Height = 25
          Cancel = True
          Caption = 'Cancelar'
          TabOrder = 1
          OnClick = btnCancelarClick
        end
      end
    end
  end
  object pnlEditing: TPanel
    Left = 0
    Top = 228
    Width = 218
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    ExplicitTop = 232
    ExplicitWidth = 226
    object Label1: TLabel
      Left = 204
      Top = 23
      Width = 12
      Height = 16
      Caption = '%'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 9
      Top = 8
      Width = 76
      Height = 13
      Caption = 'Fecha  de Inicio'
    end
    object Label3: TLabel
      Left = 126
      Top = 8
      Width = 68
      Height = 13
      Caption = 'Tasa de I.V.A.'
    end
    object neTasaIVA: TNumericEdit
      Left = 124
      Top = 20
      Width = 81
      Height = 21
      Enabled = False
      TabOrder = 0
      Decimals = 2
    end
    object deFechaInicio: TDateEdit
      Left = 8
      Top = 20
      Width = 90
      Height = 21
      AutoSelect = False
      Enabled = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 218
    Height = 35
    Habilitados = [btAlta, btBaja, btModi]
    ExplicitWidth = 226
  end
  object AbmList1: TAbmList
    Left = 0
    Top = 35
    Width = 218
    Height = 193
    Ctl3D = True
    ParentCtl3D = False
    TabStop = True
    TabOrder = 3
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'128'#0'Fecha                              '
      
        #0'211'#0'IVA                                                        ' +
        '      ')
    RefreshTime = 300
    Table = tblIVA
    Style = lbOwnerDrawFixed
    OnClick = AbmList1Click
    OnDrawItem = AbmList1DrawItem
    OnRefresh = AbmList1Refresh
    OnInsert = AbmList1Insert
    OnDelete = AbmList1Delete
    OnEdit = AbmList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object tblIVA: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'IVA'
    Left = 48
    Top = 112
    object tblIVAFechaInicio: TDateTimeField
      DisplayLabel = 'Fecha Inicio'
      DisplayWidth = 35
      FieldName = 'FechaInicio'
      Required = True
    end
    object tblIVATasaIVA: TIntegerField
      DisplayLabel = 'I.V.A.'
      FieldName = 'TasaIVA'
    end
  end
end
