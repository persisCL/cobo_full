unit ABMTiposHorario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB,DPSControls;

type
  TFormTiposHorario = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Notebook: TNotebook;
    TiposHorario: TADOTable;
    txt_TipoHorario: TEdit;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    MemoComentarios: TMemo;
    Label2: TLabel;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormTiposHorario: TFormTiposHorario;

implementation
resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Tipo de Horario.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Tipo de Horario';
    
{$R *.DFM}

function TFormTiposHorario.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Result := False;
	if not OpenTables([TiposHorario]) then exit;
	DbList1.Reload;
    Notebook.PageIndex := 0;
   	Result := True;
end;

procedure TFormTiposHorario.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormTiposHorario.DBList1Click(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
		txt_tipoHorario.Text 	:= FieldByName('TipoHorario').AsString;
		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
        MemoComentarios.Text    := FieldByName('Comentarios').AsString;
	end;
end;

procedure TFormTiposHorario.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Trim(Tabla.FieldbyName('TipoHorario').AsString));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TFormTiposHorario.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    dblist1.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;
    txt_TipoHorario.Enabled := False;
    txt_Descripcion.SetFocus;
end;

procedure TFormTiposHorario.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormTiposHorario.Limpiar_Campos();
begin
	txt_TipoHorario.Clear;
	txt_Descripcion.Clear;
    MemoComentarios.Clear;
end;

procedure TFormTiposHorario.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormTiposHorario.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
	With DbList1.Table do begin
		Try
			Edit;
			FieldByName('TipoHorario').AsString		:= Trim(txt_TipoHorario.text);
			FieldByName('Descripcion').AsString		:= Trim(txt_Descripcion.text);
			FieldByName('Comentarios').AsString		:= Trim(MemoComentarios.text);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormTiposHorario.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormTiposHorario.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormTiposHorario.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormTiposHorario.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    txt_TipoHorario.Enabled := True;
    groupb.Enabled     := False;
end;

end.
