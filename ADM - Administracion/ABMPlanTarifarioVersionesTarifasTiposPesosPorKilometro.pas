unit ABMPlanTarifarioVersionesTarifasTiposPesosPorKilometro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ListBoxEx, DBListEx, DB, ADODB,
  Utilproc, DmiCtrls,Util;

type
  TFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro = class(TForm)
    Panel2: TPanel;
    btnCancela: TButton;
    Panel3: TPanel;
    dbePlanTarifarioVersionesTarifasTipos_Obtener: TDBListEx;
    btnIngresaModificaPesosXKM: TButton;
    Panel1: TPanel;
    dsPlanTarifarioVersionesTarifasTipos_Obtener: TDataSource;
    lblPesosXKM: TLabel;
    lblConcesionaria: TLabel;
    lblShowConcesionaria: TLabel;
    btnCancelar: TButton;
    nedPesosXKM: TNumericEdit;
    lblShowPesosXKM: TLabel;
    spPlanTarifarioVersionesTarifasTipos_Obtener: TADOStoredProc;
    spPlanTarifarioVersionesTarifasTipos_PesosXKM_Actualizar: TADOStoredProc;
    procedure dbePlanTarifarioVersionesTarifasTipos_ObtenerClick(
      Sender: TObject);
    procedure btnIngresaModificaPesosXKMClick(Sender: TObject);
    procedure nedPesosXKMChange(Sender: TObject);
  private
    { Private declarations }
    FVersionHabilitar: Integer;
    FID_PlanTarifarioVersionesTarifasTipos: Integer;
    FCodigoConcesionaria: Integer;
    FPesosPorKilometro: Integer;

  public
    { Public declarations }
    Function Inicializa(_CodigoConcesionaria: Integer; _VersionHabilitar:Integer; _spPlanTarifarioVersionesTarifasTipos_Obtener: TADOStoredProc): Boolean;
  end;

var
  FrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro: TFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro;

implementation

uses DMConnection, ABMTarifasPorPuntoDeCobro;

ResourceString

  MSG_ERROR_BDD                         = 'Se produjo un problema con la base de datos ';
  MSG_ERROR_DBB_CAPTION                 = 'Error Base Datos';

  MSG_ACTUALIZAPESOSXKM_ERROR           = 'No se pudo actualizar el valor en pesos ingresado ';
  MSG_ACTUALIZAPESOSXKM_ERROR_CAPTION   = 'Error al actualizar';

{$R *.dfm}

procedure TFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro.btnIngresaModificaPesosXKMClick(Sender: TObject);
begin
  if (nedPesosXKM.Text <> '') then begin
    //spPlanTarifarioVersionesTarifasTipos_PesosXKM_Actualizar := TADOStoredProc.Create(nil);
   // try

      with spPlanTarifarioVersionesTarifasTipos_PesosXKM_Actualizar do begin

        Parameters.Refresh;
        Parameters.ParamByName('@ID_PlanTarifarioVersionesTarifasTipos').Value := FID_PlanTarifarioVersionesTarifasTipos;
        Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := FVersionHabilitar;
        Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConcesionaria;
        Parameters.ParamByName('@PesosPorKilometro').Value :=  nedPesosXKM.Text;
        Parameters.ParamByName('@Usuario').Value := UsuarioSistema;

        ExecProc;

        if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then
        begin
          MsgBox('Pesos por kilometro modificado correctamente','Atenci�n');
        end else begin
          raise exception.Create(MSG_ACTUALIZAPESOSXKM_ERROR);
        end;
      end;
      dsPlanTarifarioVersionesTarifasTipos_Obtener.DataSet := FormTarifasPorPuntoDeCobro.CargaTarifaTipoPesosXKM(FCodigoConcesionaria, FVersionHabilitar);
      lblShowConcesionaria.Caption := dsPlanTarifarioVersionesTarifasTipos_Obtener.DataSet.FieldByName('NombreCorto').AsString;
    //except
    //  On E: EDataBaseError do
    //  begin
    //    if DMConnections.BaseCOP.InTransaction then DMConnections.BaseCOP.RollbackTrans;
    //    MsgBox(MSG_ERROR_BDD + ': ' + E.Message, MSG_ERROR_DBB_CAPTION);
    //  end;
    //  On E: Exception do
    //  begin
    //    DMConnections.BaseCOP.RollbackTrans;
    //    MsgBox(E.Message, MSG_ACTUALIZAPESOSXKM_ERROR_CAPTION);
    //  end;
    //end;
  end
  else
  begin
    MsgBox('Debe ingresar un valor en pesos v�lido','Atenci�n');
  end;
end;

procedure TFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro.dbePlanTarifarioVersionesTarifasTipos_ObtenerClick(
  Sender: TObject);
var
  _pesosXKM: Variant;
begin
  btnIngresaModificaPesosXKM.Enabled := True;
  //si el dataset no esta activo salgo
  if (Sender as TDBListEx).DataSource.Dataset.Active = false then exit;

  FID_PlanTarifarioVersionesTarifasTipos := (Sender as TDBListEx).DataSource.DataSet.FieldByName('ID_PlanTarifarioVersionesTarifasTipos').Value;
  FCodigoConcesionaria := (Sender as TDBListEx).DataSource.DataSet.FieldByName('CodigoConcesionaria').Value;

  _pesosXKM := (Sender as TDBListEx).DataSource.DataSet.FieldByName('PesosPorKilometro').Value;

  if VarType(_pesosXKM) = varInt64 then begin
    FPesosPorKilometro := _pesosXKM;
  end else begin
    FPesosPorKilometro := 0;
  end;

  nedPesosXKM.Value := FPesosPorKilometro;

end;

Function TFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro.Inicializa(_CodigoConcesionaria: Integer; _VersionHabilitar: Integer; _spPlanTarifarioVersionesTarifasTipos_Obtener: TADOStoredProc): Boolean;
begin
  nedPesosXKM.Value := 0;
  FVersionHabilitar := _VersionHabilitar;
  FCodigoConcesionaria := _CodigoConcesionaria;

  dsPlanTarifarioVersionesTarifasTipos_Obtener.DataSet := FormTarifasPorPuntoDeCobro.CargaTarifaTipoPesosXKM(_CodigoConcesionaria, _VersionHabilitar);
  lblShowConcesionaria.Caption := dsPlanTarifarioVersionesTarifasTipos_Obtener.DataSet.FieldByName('NombreCorto').AsString;

  dbePlanTarifarioVersionesTarifasTipos_Obtener.Refresh;

  btnIngresaModificaPesosXKM.Enabled := false;

  Result := True;

end;



procedure TFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro.nedPesosXKMChange(
  Sender: TObject);
begin
  lblShowPesosXKM.Caption := FormatFloat( '0.000', (nedPesosXKM.Value / 1000) );
  lblShowPesosXKM.Refresh;
end;

end.
