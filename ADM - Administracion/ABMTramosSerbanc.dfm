object frmABMTramosGastosSerbanc: TfrmABMTramosGastosSerbanc
  Left = 195
  Top = 98
  ActiveControl = btnAceptar
  Caption = 'ABM Tarifas de Gastos de Cobranza'
  ClientHeight = 508
  ClientWidth = 744
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 440
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object pnlButtons: TPanel
    Left = 0
    Top = 458
    Width = 744
    Height = 50
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 446
    ExplicitWidth = 668
    DesignSize = (
      744
      50)
    object Notebook1: TNotebook
      Left = 542
      Top = 8
      Width = 201
      Height = 38
      Anchors = [akRight, akBottom]
      PageIndex = 1
      TabOrder = 0
      ExplicitLeft = 466
      object TPage
        Left = 0
        Top = 0
        Caption = 'Salir'
        object btnSalir: TButton
          Left = 120
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Salir'
          TabOrder = 0
          OnClick = btnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        Caption = 'Editing'
        object btnAceptar: TButton
          Left = 23
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btnAceptarClick
        end
        object btnCancelar: TButton
          Left = 112
          Top = 8
          Width = 75
          Height = 25
          Cancel = True
          Caption = 'Cancelar'
          TabOrder = 1
          OnClick = btnCancelarClick
        end
      end
    end
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 744
    Height = 35
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbar1Close
    ExplicitWidth = 668
  end
  object AbmList1: TAbmList
    Left = 0
    Top = 35
    Width = 744
    Height = 150
    Ctl3D = True
    ParentCtl3D = False
    TabStop = True
    TabOrder = 2
    Align = alTop
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'98'#0'C'#243'digo de Tarifa    '
      #0'178'#0'Fecha de Activaci'#243'n                        '
      #0'168'#0'Actualizada                                   '
      #0'89'#0'Usuario               ')
    RefreshTime = 300
    Table = tblTarifas
    Style = lbOwnerDrawFixed
    OnClick = AbmList1Click
    OnDrawItem = AbmList1DrawItem
    OnInsert = AbmList1Insert
    OnDelete = AbmList1Delete
    OnEdit = AbmList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object pnlData: TPanel
    Left = 0
    Top = 185
    Width = 744
    Height = 273
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 668
    ExplicitHeight = 261
    DesignSize = (
      744
      273)
    object pnlTarifa: TPanel
      Left = 0
      Top = 0
      Width = 744
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      ExplicitWidth = 668
      DesignSize = (
        744
        41)
      object Label5: TLabel
        Left = 152
        Top = 14
        Width = 100
        Height = 13
        Caption = 'Fecha de Activaci'#243'n:'
      end
      object lblEstadoTarifa: TLabel
        Left = 650
        Top = 14
        Width = 71
        Height = 13
        Alignment = taCenter
        Anchors = [akRight, akBottom]
        Caption = 'lblEstadoTarifa'
        ExplicitLeft = 572
      end
      object lblCodigoTarifa: TLabel
        Left = 10
        Top = 14
        Width = 37
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object dateActivacionTarifa: TDateEdit
        Left = 260
        Top = 11
        Width = 193
        Height = 21
        AutoSelect = False
        Enabled = False
        TabOrder = 0
        Date = -693594.000000000000000000
      end
    end
    object btnAgregarTramo: TButton
      Left = 640
      Top = 40
      Width = 97
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Agregar Tramo'
      Enabled = False
      TabOrder = 1
      OnClick = btnAgregarTramoClick
      ExplicitLeft = 564
    end
    object btnEditarTramo: TButton
      Left = 640
      Top = 68
      Width = 97
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Editar Tramo'
      Enabled = False
      TabOrder = 2
      OnClick = btnEditarTramoClick
      ExplicitLeft = 564
    end
    object btnEliminarTramo: TButton
      Left = 640
      Top = 96
      Width = 97
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Eliminar Tramo'
      Enabled = False
      TabOrder = 3
      OnClick = btnEliminarTramoClick
      ExplicitLeft = 564
    end
    object dblTramos: TDBListEx
      Left = 0
      Top = 41
      Width = 632
      Height = 232
      Align = alLeft
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Monto Inicial'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'MontoInicial'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Monto Final'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'MontoFinal'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Monto Tramo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'MontoTramo'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Porcentaje a Aplicar'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'PorcentajeGasto'
        end>
      DataSource = dsTramos
      DragReorder = True
      ParentColor = False
      TabOrder = 4
      TabStop = True
      OnDrawText = dblTramosDrawText
      ExplicitLeft = 2
      ExplicitTop = 38
    end
    object txtCodigoTarifa: TNumericEdit
      Left = 51
      Top = 11
      Width = 54
      Height = 21
      Color = clInactiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaptionText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
    end
  end
  object tblTarifas: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    IndexFieldNames = 'CodigoTarifa'
    TableName = 'TarifasGastosCobranza'
    Left = 272
    Top = 120
  end
  object spObtenerTarifaGastosCobranzaVigente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTarifaGastosCobranzaVigente'
    Parameters = <>
    Left = 109
    Top = 129
  end
  object cdsTramos: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoTarifa'
        DataType = ftInteger
      end
      item
        Name = 'MontoInicial'
        DataType = ftInteger
      end
      item
        Name = 'MontoFinal'
        DataType = ftInteger
      end
      item
        Name = 'MontoTramo'
        DataType = ftInteger
      end
      item
        Name = 'PorcentajeGasto'
        DataType = ftBCD
        Precision = 6
        Size = 2
      end>
    IndexDefs = <
      item
        Name = 'cdsTramosIndex1'
        Fields = 'MontoInicial'
        Options = [ixPrimary, ixUnique]
      end>
    IndexName = 'cdsTramosIndex1'
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = 0
      end
      item
        DataType = ftInteger
        Name = '@CodigoTarifa'
        ParamType = ptInput
        Size = -1
      end>
    ProviderName = 'dspTramos'
    StoreDefs = True
    Left = 488
    Top = 121
  end
  object spObtenerTramosTarifaGastosCobranza: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTramosTarifaGastosCobranza'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftSmallint
        Direction = pdReturnValue
        Value = 0
      end
      item
        Name = '@CodigoTarifa'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Left = 584
    Top = 64
  end
  object spBorrarTramos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BorrarTramosGastosCobranza'
    Parameters = <>
    Left = 432
    Top = 57
  end
  object spAgregarTramo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarTramoGastosCobranza'
    Parameters = <>
    Left = 336
    Top = 57
  end
  object dsTramos: TDataSource
    DataSet = cdsTramos
    Left = 584
    Top = 121
  end
  object dspTramos: TDataSetProvider
    DataSet = spObtenerTramosTarifaGastosCobranza
    Left = 368
    Top = 121
  end
  object spActualizarTarifaGastosCobranza: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTarifaGastosCobranza'
    Parameters = <>
    Left = 80
    Top = 73
  end
  object spAgregarTarifaGastosCobranza: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarTarifaGastosCobranza'
    Parameters = <>
    Left = 200
    Top = 57
  end
end
