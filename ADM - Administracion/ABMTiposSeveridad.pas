{-----------------------------------------------------------------------------
 File Name: ABMTiposSeveridad 
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit ABMTiposSeveridad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls;

type
  TFormTiposSeveridad = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    TiposSeveridad: TADOTable;
    txt_CodigoSeveridad: TNumericEdit;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormTiposSeveridad: TFormTiposSeveridad;

implementation
resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar este Tipo de Severidad?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Tipo de Severidad porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION  	= 'Eliminar Tipo de Severidad';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Tipo de Severidad.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Tipo de Severidad';

{$R *.DFM}

procedure TFormTiposSeveridad.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
    txt_CodigoSeveridad.Enabled	:= True;
end;

function TFormTiposSeveridad.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([TiposSeveridad]) then
		Result := False
	else begin
		Result := True;
		DbList1.Reload;
	end;
   	Notebook.PageIndex := 0;
end;

procedure TFormTiposSeveridad.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormTiposSeveridad.DBList1Click(Sender: TObject);
begin
	with TiposSeveridad do begin
		txt_CodigoSeveridad.Value	:= FieldByName('CodigoSeveridad').AsInteger;
		txt_Descripcion.text		:= FieldByName('Descripcion').AsString;
	end;
end;

procedure TFormTiposSeveridad.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 1 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormTiposSeveridad.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado     := modi;
end;

procedure TFormTiposSeveridad.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCampos;
    dblist1.Estado     := Alta;
end;

procedure TFormTiposSeveridad.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormTiposSeveridad.Limpiar_Campos;
begin
	txt_CodigoSeveridad.Clear;
	txt_Descripcion.Clear;
end;

procedure TFormTiposSeveridad.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormTiposSeveridad.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			TiposSeveridad.Delete;
		Except
			On E: Exception do begin
				TiposSeveridad.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	VolverCampos;
	Screen.Cursor      := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}
procedure TFormTiposSeveridad.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
	With TiposSeveridad do begin
		Try
			if DbList1.Estado = Alta then begin
            	Append;
                txt_CodigoSeveridad.value := QueryGetValueInt(DMConnections.BaseCAC, 'Select ISNULL(MAX(CodigoSeveridad), 0) + 1 FROM TiposSeveridad WITH (NOLOCK) ');
                FieldByName('CodigoSeveridad').value	:= txt_CodigoSeveridad.value;
			end else Edit;
			FieldByName('Descripcion').AsString			:= txt_Descripcion.text;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFormTiposSeveridad.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormTiposSeveridad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormTiposSeveridad.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormTiposSeveridad.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    txt_CodigoSeveridad.Enabled	:= False;
    txt_Descripcion.SetFocus;
end;

end.
