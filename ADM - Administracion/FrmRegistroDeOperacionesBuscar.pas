{********************************** File Header *********************************
File Name   : FrmRegistroDeOperacionesBuscar.pas
Author      : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created: 02/05/2003
Language    : ES-AR
Description : Formulario que despliega las opciones de b�squeda de los eventos
              desplegados en pantalla..
********************************************************************************}
unit FrmRegistroDeOperacionesBuscar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, UtilProc, ExtCtrls, DB, ADODB;

type
  TFormRegistroDeOperacionesBuscar = class(TForm)
    btnAceptar: TButton;
    btnCancelar: TButton;
    pnlBuscar: TPanel;
    labCampo: TLabel;
    labCondicion: TLabel;
    labValor: TLabel;
    labDireccion: TLabel;
    labComienzo: TLabel;
    cbCondiciones: TComboBox;
    cbCampos: TVariantComboBox;
    cbComienzos: TComboBox;
    cbDirecciones: TVariantComboBox;
    txtValor: TEdit;        procedure btnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormRegistroDeOperacionesBuscar: TFormRegistroDeOperacionesBuscar;

implementation

{$R *.dfm}

procedure TFormRegistroDeOperacionesBuscar.btnAceptarClick(Sender: TObject);
resourcestring
     MSG_ERROR_ACEPTAR = 'Debe completar el campo para poder continuar.';
begin
	if (cbCampos.Value = Null) then begin
          MsgBoxBalloon(MSG_ERROR_ACEPTAR, Application.Title, MB_ICONINFORMATION + MB_OK, cbCampos);
     end
	else if (cbCondiciones.Text = '') then begin
          MsgBoxBalloon(MSG_ERROR_ACEPTAR, Application.Title, MB_ICONINFORMATION + MB_OK, cbCondiciones);
     end
     else if (txtValor.Text = '') then begin
          MsgBoxBalloon(MSG_ERROR_ACEPTAR, Application.Title, MB_ICONINFORMATION + MB_OK, txtValor);
     end
     else begin
          Close;
          ModalResult := mrOk;
     end;
end;

procedure TFormRegistroDeOperacionesBuscar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caHide;
end;

end.
