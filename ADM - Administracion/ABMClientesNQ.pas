{********************************** Unit Header ********************************
File Name : ABMClientesNQ.pas
Author : mvitali
Date Created: 17/10/2005
Language : ES-AR
Description :  ABM de Clientes NQ

Autor       :   CQuezadaI
Fecha       :   18 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se agrega la limpieza del campo Raz�n Social
*******************************************************************************}

unit ABMClientesNQ;

interface

uses
  //ABM ClientesNQ
  DMConnection, UtilProc, Util, RStrings, utildb, PeaTypes, PeaProcs,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants;

type
  TfrmABMClientesNQ = class(TForm)
	GroupB: TPanel;
    PAbajo: TPanel;
	Notebook: TNotebook;
    tblClientesNQ: TADOTable;
    txtCodigo: TNumericEdit;
    AbmToolbar: TAbmToolbar;
    spActualizarClientesNQ: TADOStoredProc;
    lblCodigo: TLabel;
    lbClientesNQ: TAbmList;
    dsClientesNQ: TDataSource;
    EliminarClienteNQ: TADOStoredProc;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    txtCodigoPostal: TEdit;
    lblCodigoPostal: TLabel;
    txtNumero: TEdit;
    lblNumero: TLabel;
    lblCalle: TLabel;
    cbRegion: TComboBox;
    lblRegion: TLabel;
    lblDetalle: TLabel;
    txtDetalle: TEdit;
    lblPersoneria: TLabel;
    cbPersoneria: TComboBox;
    lblDocumento: TLabel;
    txtDocumento: TEdit;
    lblRazonSocial: TLabel;
    txtRazonSocial: TEdit;
    txtApellidoMaterno: TEdit;
    lblApellidoMaterno: TLabel;
    txtApellido: TEdit;
    lblApellido: TLabel;
    lblNombre: TLabel;
    txtNombre: TEdit;
    lblSexo: TLabel;
    cbSexo: TComboBox;
    lblComuna: TLabel;
    cbComuna: TComboBox;
    txtCalle: TEdit;
    procedure cbPersoneriaChange(Sender: TObject);
    procedure cbRegionChange(Sender: TObject);
    procedure cbComunaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
   	procedure lbClientesNQRefresh(Sender: TObject);
   	function  lbClientesNQProcess(Tabla: TDataSet; var Texto: String): Boolean;
   	procedure lbClientesNQDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure lbClientesNQClick(Sender: TObject);
	procedure lbClientesNQInsert(Sender: TObject);
	procedure lbClientesNQEdit(Sender: TObject);
	procedure lbClientesNQDelete(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure Salir(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
    FCodigoRegionAnterior: AnsiString;
    FCodigoComunaAnterior:AnsiString;
    function GetPersoneria: AnsiString;
    function GetApellido: AnsiString;
    function GetApellidoMaterno: AnsiString;
    function GetNombre: AnsiString;
    function GetNumeroDocumento: AnsiString;
    function GetSexo: AnsiString;
    function GetRazonSocial: String;
    function GetRegion: AnsiString;
    function GetComuna: AnsiString;
    function GetNumero: AnsiString;
    function GetCalle: AnsiString;
    function GetDetalle: AnsiString;
    function GetCodigoPostal: AnsiString;
    function GetDescriRegion: AnsiString;
    function GetDescriComuna: AnsiString;

    procedure HabilitarCamposPersoneria(Personeria: AnsiString);
	procedure LimpiarCampos;
	procedure VolverCampos;
    procedure CargarCampos;
  public
	{ Public declarations }
    property Personeria: AnsiString read GetPersoneria;
    property Apellido: AnsiString read GetApellido;
    property ApellidoMaterno: AnsiString read GetApellidoMaterno;
    property Nombre: AnsiString read GetNombre;
    property NumeroDocumento: AnsiString read GetNumeroDocumento;
    property Sexo: AnsiString read GetSexo;
    property RazonSocial: AnsiString read GetRazonSocial;
    //Domicilio
    property Region: AnsiString read GetRegion;
    property Comuna: AnsiString read GetComuna;
    property Numero: AnsiString read GetNumero;
    property Calle: AnsiString read GetCalle;
    property Detalle: AnsiString read GetDetalle;
    property CodigoPostal: AnsiString read GetCodigoPostal;
    property DescriRegion: AnsiString read GetDescriRegion;
    property DescriComuna: AnsiString read GetDescriComuna;
	function Inicializar: Boolean;
  end;

var
  FABMClientesNQ: TfrmABMClientesNQ;

resourcestring
	STR_MAESTRO_CLIENTESNQ	= 'Maestro de ClientesNQ';

implementation

{$R *.DFM}

{******************************** Function Header ******************************
Function Name: Inicializar
Author : mvitali
Date Created : 17/10/2005
Description : Inicializa el formulario
Parameters : None
Return Value : Boolean
*******************************************************************************}

function TfrmABMClientesNQ.Inicializar:Boolean;
Var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	//S := GetFormClientSize(Application.MainForm);
	//SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblClientesNQ]) then exit;
	Notebook.PageIndex := 0;
	lbClientesNQ.Reload;
    CenterForm(Self);
    FCodigoRegionAnterior := '-1';
    FCodigoComunaAnterior := '-1';
    CargarRegiones(DMConnections.BaseCAC, cbRegion, PAIS_CHILE, REGION_SANTIAGO);
    CargarComunas(DMConnections.BaseCAC,cbComuna,PAIS_CHILE,REGION_SANTIAGO);
	Result := True;
end;

{******************************** Function Header ******************************
Function Name: LimpiarCampos
Author : mvitali
Date Created : 17/10/2005
Description : Vuelve los controles de ingreso a su estado original
Parameters : None
Return Value : None
*******************************************************************************}


procedure TfrmABMClientesNQ.LimpiarCampos;
begin
    CargarRegiones(DMConnections.BaseCAC, cbRegion, PAIS_CHILE, REGION_SANTIAGO);
	cbRegion.OnChange(cbRegion);
    CargarComunas(DMConnections.BaseCAC,cbComuna,PAIS_CHILE,REGION_SANTIAGO);
    cbComuna.OnChange(cbComuna);
    CargarPersoneria(cbPersoneria,PERSONERIA_FISICA);
    cbPersoneria.OnChange(cbPersoneria);
    CargarSexos(cbSexo,SEXO_MASCULINO);
    txtCodigo.Clear;
    txtDocumento.Clear;
    txtApellido.Clear;
    txtApellidoMaterno.Clear;
    txtNombre.Clear;
    txtDetalle.Clear;
    txtCalle.Clear;
    txtNumero.Clear;
    txtCodigoPostal.Clear;
    txtRazonSocial.Clear;   // SS_1147_CQU_20140714
end;

procedure TfrmABMClientesNQ.FormShow(Sender: TObject);
begin
	lbClientesNQ.Reload;
end;

procedure TfrmABMClientesNQ.lbClientesNQRefresh(Sender: TObject);
begin
	 if lbClientesNQ.Empty then LimpiarCampos;
end;

procedure TfrmABMClientesNQ.VolverCampos;
begin
	lbClientesNQ.Estado := Normal;
	lbClientesNQ.Enabled:= True;
	ActiveControl       := lbClientesNQ;
	Notebook.PageIndex  := 0;
	Groupb.Enabled      := False;
	lbClientesNQ.Reload;
end;

{******************************** Function Header ******************************
Function Name: lbClientesNQDrawItem
Author : mvitali
Date Created : 17/10/2005
Description : Al dibujar una celda de la lista
Parameters : Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
Return Value : None
*******************************************************************************}


procedure TfrmABMClientesNQ.lbClientesNQDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoClienteNQ').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Apellido').AsString));
		TextOut(Cols[2], Rect.Top, Trim(Tabla.FieldbyName('Nombre').AsString));
	end;
end;

procedure TfrmABMClientesNQ.lbClientesNQClick(Sender: TObject);
begin
    CargarCampos;
end;

function TfrmABMClientesNQ.lbClientesNQProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
	Result := True;
end;

procedure TfrmABMClientesNQ.lbClientesNQInsert(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled          	:= True;
	lbClientesNQ.Enabled := False;
	Notebook.PageIndex      	:= 1;
	ActiveControl           	:= txtDocumento;
end;

procedure TfrmABMClientesNQ.lbClientesNQEdit(Sender: TObject);
begin
	lbClientesNQ.Enabled:= False;
	lbClientesNQ.Estado := modi;
	Notebook.PageIndex := 1;
	groupb.Enabled     := True;
	ActiveControl:= txtDocumento;
end;

{******************************** Function Header ******************************
Function Name: lbClientesNQDelete
Author : mvitali
Date Created : 17/10/2005
Description : Elimina un cliente de la BD
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}

procedure TfrmABMClientesNQ.lbClientesNQDelete(Sender: TObject);
resourcestring
  MSG_DELETE_CAPTION		   = 'Eliminar Cliente.';
begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_CLIENTESNQ]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with EliminarClienteNQ do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoClienteNQ').Value := tblClientesNQ.FieldbyName('CodigoClienteNQ').Value;
                Parameters.ParamByName('@Resultado').Value := 0;
                ExecProc;
            end;
		Except
			On E: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_CLIENTESNQ]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_CLIENTESNQ]), MB_ICONSTOP);
			end
		end
	end;

	lbClientesNQ.Reload;
	lbClientesNQ.Estado := Normal;
	lbClientesNQ.Enabled := True;
	Notebook.PageIndex := 0;
	Screen.Cursor := crDefault;
end;

{******************************** Function Header ******************************
Function Name: BtnAceptarClick
Author : mvitali
Date Created : 17/10/2005
Description : Valida los controles y Acepta los cambios en las BD
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}

procedure TfrmABMClientesNQ.BtnAceptarClick(Sender: TObject);
begin

    //Validacion personeria y documento
    if not ValidateControls([cbPersoneria, txtDocumento],
    [Personeria <> '',
    (NumeroDocumento <> '') and ValidarRUT(DMConnections.BaseCAC, NumeroDocumento)],
    Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_CLIENTESNQ]),
    [Format(MSG_VALIDAR_DEBE_LA,[FLD_PERSONERIA]), MSG_ERROR_DOCUMENTO]) then Exit;


    //Validacion de la persona o razon social
    if Personeria = PERSONERIA_FISICA then begin

        if not ValidateControls([txtApellido, txtNombre, cbSexo],
        [Apellido <> '', Nombre <> '', Sexo <> ''],
        Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_CLIENTESNQ]),
        [Format(MSG_VALIDAR_DEBE_EL,[FLD_APELLIDO]),
        Format(MSG_VALIDAR_DEBE_EL,[FLD_NOMBRE]),
        Format(MSG_VALIDAR_DEBE_EL,[FLD_SEXO])]) then Exit;

    end else begin

        if not ValidateControls([txtRazonSocial], [(RazonSocial <> '')],
        Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_CLIENTESNQ]),
        [Format(MSG_VALIDAR_DEBE_LA,[FLD_RAZON_SOCIAL])]) then Exit;

    end;

    //Validacion del Domiclio
    if not ValidateControls([cbRegion, cbComuna, txtCalle, txtNumero],
    [Region <> '', Comuna <> '', Calle <> '', numero <> ''],
    Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_CLIENTESNQ]),
    [Format(MSG_VALIDAR_DEBE_LA,[FLD_REGION]),
    Format(MSG_VALIDAR_DEBE_LA,[FLD_COMUNA]),
    Format(MSG_VALIDAR_DEBE_LA,[FLD_CALLE]),
    Format(MSG_VALIDAR_DEBE_EL,[FLD_NUMERO])]) then Exit;

	with lbClientesNQ do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarClientesNQ, Parameters do begin
					ParamByName('@CodigoClienteNQ').Value := txtCodigo.ValueInt;
                    if Personeria = PERSONERIA_JURIDICA then
					ParamByName('@Apellido').Value := RazonSocial
                    else ParamByName('@Apellido').Value := Apellido;
                    ParamByName('@Personeria').Value := Personeria;
                    ParamByName('@ApellidoMaterno').Value := ApellidoMaterno;
                    ParamByName('@Nombre').Value := Nombre;
                    ParamByName('@NumeroDocumento').Value := NumeroDocumento;
                    ParamByName('@Sexo').Value := Sexo;
                    ParamByName('@Calle').Value := Calle;
                    ParamByName('@Numero').Value := Numero;
                    ParamByName('@DetalleDomicilio').Value := Detalle;
                    ParamByName('@CodigoPostal').Value := CodigoPostal;
                    ParamByName('@CodigoComuna').Value := Comuna;
                    ParamByName('@CodigoRegion').Value := Region;
					ExecProc;
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_CLIENTESNQ]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_CLIENTESNQ]), MB_ICONSTOP);
				end;
			end;
		finally
            VolverCampos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

procedure TfrmABMClientesNQ.BtnCancelarClick(Sender: TObject);
begin
	VolverCampos;
end;

procedure TfrmABMClientesNQ.Salir(Sender: TObject);
begin
	 Close;
end;

procedure TfrmABMClientesNQ.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 Action := caFree;
end;

function TfrmABMClientesNQ.GetPersoneria: AnsiString;
begin

{******************************** Function Header ******************************
Function Name: Funciones Get's
Author : mvitali
Date Created : 17/10/2005
Description : Devuelven el texto de los cotroles
Parameters : None
Return Value : AnsiString
*******************************************************************************}

    if trim(cbPersoneria.Text) <> '' then Result:=StrRight(trim(cbPersoneria.Text), 1)
    else Result := '';
end;

function TfrmABMClientesNQ.GetApellido: AnsiString;
begin
    if Personeria = PERSONERIA_JURIDICA then Result := ''
	else Result := Trim(txtApellido.Text);
end;

function TfrmABMClientesNQ.GetApellidoMaterno: AnsiString;
begin
    if Personeria = PERSONERIA_JURIDICA then Result := ''
    else Result := Trim(txtApellidoMaterno.Text);
end;

function TfrmABMClientesNQ.GetNombre: AnsiString;
begin
    if Personeria = PERSONERIA_JURIDICA then Result := ''
    else Result:=Trim(txtNombre.Text);
end;

function TfrmABMClientesNQ.GetNumeroDocumento: AnsiString;
begin
    Result:=Trim(txtDocumento.Text);
end;

function TfrmABMClientesNQ.GetSexo: AnsiString;
begin
    if Personeria=PERSONERIA_JURIDICA then Result := ''
    else
    Result:=StrRight(trim(cbSexo.Text), 1);
end;

function TfrmABMClientesNQ.GetRazonSocial: String;
begin
    if Personeria = PERSONERIA_JURIDICA then Result := Trim(txtRazonSocial.Text)
    else Result:='';
end;

function TfrmABMClientesNQ.GetRegion: AnsiString;
begin
    if cbRegion.ItemIndex >= 0 then
    Result := Trim(StrRight(cbRegion.Text, 10))
    else Result := '';
end;

function TfrmABMClientesNQ.GetComuna: AnsiString;
begin
    if cbRegion.ItemIndex >= 0 then
    Result := Trim(StrRight(cbComuna.Text, 10))
    else Result := '';;
end;

function TfrmABMClientesNQ.GetNumero: AnsiString;
begin
    Result := trim(txtNumero.Text);
end;

function TfrmABMClientesNQ.GetCalle: AnsiString;
begin
    Result := trim(txtCalle.text);
end;

function TfrmABMClientesNQ.GetDetalle: AnsiString;
begin
    Result := trim(txtDetalle.Text);
end;

function TfrmABMClientesNQ.GetCodigoPostal: AnsiString;
begin
    Result := Trim(txtCodigoPostal.Text);
end;

function TfrmABMClientesNQ.GetDescriRegion: AnsiString;
begin
    Result:=Trim(StrLeft(cbRegion.Text, 200));
end;

function TfrmABMClientesNQ.GetDescriComuna: AnsiString;
begin
    Result:=trim(StrLeft(cbComuna.Text, 200));
end;

{******************************** Function Header ******************************
Function Name: CargarCampos
Author : mvitali
Date Created : 17/10/2005
Description : Carga los controles con datos desde la BD
Parameters : None
Return Value : None
*******************************************************************************}

procedure TfrmABMClientesNQ.CargarCampos;
begin
    with tblClientesNQ do begin
        txtCodigo.Value	:= FieldByName('CodigoClienteNQ').AsInteger;
        CargarPersoneria(cbPersoneria, Trim(FieldByName('Personeria').AsString),false,true);
        cbPersoneria.OnChange(cbPersoneria);
        CargarSexos(cbSexo, iif(Personeria=PERSONERIA_JURIDICA, '', Trim(FieldByName('Sexo').AsString)));
        txtDocumento.Text := Trim(FieldByName('NumeroDocumento').AsString);
        txtNombre.Text := iif(Personeria = PERSONERIA_JURIDICA, '', Trim(FieldByName('Nombre').AsString));
        txtApellido.Text := iif(Personeria = PERSONERIA_JURIDICA, '', Trim(FieldByName('Apellido').AsString));
        txtApellidoMaterno.Text := iif(Personeria = PERSONERIA_JURIDICA, '', Trim(FieldByName('ApellidoMaterno').AsString));
        txtRazonSocial.Text := iif(Personeria = PERSONERIA_JURIDICA, Trim(FieldByName('Apellido').AsString), '');
        txtCalle.Text := Trim(FieldByName('Calle').AsString);
        txtNumero.Text := Trim(FieldByName('Numero').AsString);
        txtCodigoPostal.Text := Trim(FieldByName('CodigoPostal').AsString);
        txtDetalle.Text := Trim(FieldByName('DetalleDomicilio').AsString);
        CargarRegiones(DMConnections.BaseCAC, cbRegion, PAIS_CHILE,Trim(FieldByName('CodigoRegion').AsString));
        cbRegion.OnChange(cbRegion);
        CargarComunas(DMConnections.BaseCAC,cbComuna, PAIS_CHILE , Trim(FieldByName('CodigoRegion').AsString), Trim(FieldByName('CodigoComuna').AsString),false,true);
        cbComuna.OnChange(cbComuna);
    end;
end;

{******************************** Function Header ******************************
Function Name: Funciones Change
Author : mvitali
Date Created : 17/10/2005
Description : Cargan las listas en los combos y actualizan los valores segun lo seleccionado
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}

procedure TfrmABMClientesNQ.cbComunaChange(Sender: TObject);
begin
    if FCodigoComunaAnterior <> Comuna then begin
		FCodigoComunaAnterior:= Comuna;
    end;
end;

procedure TfrmABMClientesNQ.cbRegionChange(Sender: TObject);
begin
    if FCodigoRegionAnterior <> Region then begin
        CargarComunas(DMConnections.BaseCAC, cbComuna, PAIS_CHILE, Region, '', false ,true);
        cbComuna.OnChange(cbcomuna);
        FCodigoRegionAnterior:= Region;
    end;
end;

procedure TfrmABMClientesNQ.cbPersoneriaChange(Sender: TObject);
begin
    HabilitarCamposPersoneria(Personeria);
end;

{******************************** Function Header ******************************
Function Name: HabilitarCamposPersoneria
Author : mvitali
Date Created : 17/10/2005
escription : Habilita/Desahabilita los controles segun la persoenr�a
Parameters : Personeria: AnsiString
Return Value : None
*******************************************************************************}

procedure TfrmABMClientesNQ.HabilitarCamposPersoneria(Personeria: AnsiString);
begin
    if Personeria = PERSONERIA_JURIDICA then begin
        txtApellido.Enabled := false;
        lblApellido.Enabled := false;
        txtApellidoMaterno.Enabled := false;
        lblApellidoMaterno.Enabled := false;
        txtNombre.Enabled := false;
        lblNombre.Enabled := false;
        cbSexo.Enabled := false;
        lblSexo.Enabled := false;
        lblRazonSocial.Enabled:= true;//cambia visible por enable jconcheyro 28/12/2005
        txtRazonSocial.Enabled:= true;//cambia visible por enable jconcheyro 28/12/2005
    end else begin
        txtApellido.Enabled := true;
        lblApellido.Enabled := true;
        txtApellidoMaterno.Enabled := true;
        lblApellidoMaterno.Enabled := true;
        txtNombre.Enabled := true;
        lblNombre.Enabled := true;
        cbSexo.Enabled := true;
        lblSexo.Enabled := true;
        lblRazonSocial.Enabled:= false; //cambia visible por enable jconcheyro 28/12/2005
        txtRazonSocial.Enabled:= false; //cambia visible por enable jconcheyro 28/12/2005
    end;
end;

end.


