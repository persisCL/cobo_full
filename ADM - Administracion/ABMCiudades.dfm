object FormCiudades: TFormCiudades
  Left = 232
  Top = 113
  Width = 784
  Height = 565
  Caption = 'Mantenimiento de Ciudades'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 776
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object dblCiudades: TAbmList
    Left = 0
    Top = 33
    Width = 776
    Height = 212
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'201'#0'Pa'#237's'
      #0'187'#0'Regi'#243'n'
      #0'41'#0'Ciudad')
    HScrollBar = True
    RefreshTime = 100
    Table = Ciudades
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dblCiudadesClick
    OnProcess = dblCiudadesProcess
    OnDrawItem = dblCiudadesDrawItem
    OnRefresh = dblCiudadesRefresh
    OnInsert = dblCiudadesInsert
    OnDelete = dblCiudadesDelete
    OnEdit = dblCiudadesEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 245
    Width = 776
    Height = 247
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label2: TLabel
      Left = 35
      Top = 87
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txt_descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 35
      Top = 63
      Width = 44
      Height = 13
      Caption = '&C'#243'digo:'
      FocusControl = txt_CodigoCiudad
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 35
      Top = 14
      Width = 31
      Height = 13
      Caption = '&Pa'#237's:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 35
      Top = 38
      Width = 45
      Height = 13
      Caption = '&Regi'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 35
      Top = 112
      Width = 56
      Height = 13
      Caption = 'C&omunas:'
      FocusControl = txt_descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_descripcion: TEdit
      Left = 111
      Top = 83
      Width = 266
      Height = 21
      Hint = 'Se muestran s'#243'lo aquellos paises que tienen provinicas'
      Color = 16444382
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
    object txt_CodigoCiudad: TEdit
      Left = 111
      Top = 59
      Width = 54
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 2
    end
    object cb_pais: TComboBox
      Left = 111
      Top = 10
      Width = 266
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      MaxLength = 4
      TabOrder = 0
      OnChange = cb_paisChange
    end
    object cb_regiones: TComboBox
      Left = 111
      Top = 34
      Width = 266
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      MaxLength = 4
      TabOrder = 1
      OnChange = cb_regionesChange
    end
    object clbComunas: TCheckListBox
      Left = 112
      Top = 112
      Width = 633
      Height = 125
      Columns = 4
      ItemHeight = 13
      TabOrder = 4
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 492
    Width = 776
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 579
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object BuscaTablaPaises: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    HelpContext = 0
    Dataset = qry_Paises
    Left = 472
    Top = 137
  end
  object BuscaTablaRegiones: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    HelpContext = 0
    Dataset = qry_Regiones
    Left = 500
    Top = 137
  end
  object qry_Paises: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from paises')
    Left = 472
    Top = 72
  end
  object qry_Regiones: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    Filtered = True
    Parameters = <
      item
        Name = 'CodigoPais'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      'CodigoPais, CodigoRegion, Descripcion'
      'FROM Regiones'
      'WHERE '
      '(:CodigoPais = Regiones.CodigoPais)')
    Left = 501
    Top = 72
  end
  object Ciudades: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'Ciudades'
    Left = 193
    Top = 84
  end
  object ObtenerComunasCiudad: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerComunasCiudad;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end>
    Left = 528
    Top = 72
  end
  object EliminarCiudad: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarCiudad;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end>
    Left = 556
    Top = 72
  end
  object AgregarCiudadComuna: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarCiudadComuna;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end>
    Left = 584
    Top = 72
  end
  object EliminarCiudadComuna: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarCiudadComuna;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end>
    Left = 612
    Top = 72
  end
end
