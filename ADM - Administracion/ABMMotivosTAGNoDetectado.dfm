object FormABMMotivosTAGNoDetectado: TFormABMMotivosTAGNoDetectado
  Left = 144
  Top = 142
  Width = 640
  Height = 480
  Caption = 'Modificaci'#243'n de Motivos de Telev'#237'a No Detectado'
  Color = clBtnFace
  Constraints.MinHeight = 480
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolBar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 632
    Height = 33
    Habilitados = [btModi, btSalir]
    OnClose = AbmToolBarClose
  end
  object pnl_Bottom: TPanel
    Left = 0
    Top = 407
    Width = 632
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object lbl_HintDeshabilitado: TLabel
      Left = 35
      Top = 12
      Width = 64
      Height = 13
      Caption = 'Deshabilitado'
    end
    object nb_Botones: TNotebook
      Left = 435
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btn_Salir: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btn_SalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object btn_Aceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 25
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btn_AceptarClick
        end
        object btn_Cancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = btn_CancelarClick
        end
      end
    end
    object pnl_HintDeshabilitado: TPanel
      Left = 16
      Top = 12
      Width = 12
      Height = 13
      BevelOuter = bvNone
      Color = clRed
      TabOrder = 1
    end
  end
  object dbl_Motivos: TAbmList
    Left = 0
    Top = 33
    Width = 632
    Height = 283
    TabStop = True
    TabOrder = 2
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'86'#0'C'#243'digo               '
      #0'187'#0'Descripci'#243'n                                         ')
    HScrollBar = True
    RefreshTime = 100
    Table = tbl_MotivosTAGNoDetectado
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dbl_MotivosClick
    OnDrawItem = dbl_MotivosDrawItem
    OnRefresh = dbl_MotivosRefresh
    OnEdit = dbl_MotivosEdit
    Access = [accModi]
    Estado = Normal
    ToolBar = AbmToolBar
  end
  object pnl_Datos: TPanel
    Left = 0
    Top = 316
    Width = 632
    Height = 91
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 3
    object Ldescripcion: TLabel
      Left = 16
      Top = 44
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_CodigoMotivo: TLabel
      Left = 16
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object txt_Descripcion: TEdit
      Left = 92
      Top = 40
      Width = 388
      Height = 21
      Color = 16444382
      MaxLength = 50
      TabOrder = 1
    end
    object txt_CodigoMotivo: TNumericEdit
      Left = 92
      Top = 12
      Width = 105
      Height = 21
      TabStop = False
      Color = clBtnFace
      MaxLength = 3
      ReadOnly = True
      TabOrder = 0
      Decimals = 0
    end
    object chb_Habilitado: TCheckBox
      Left = 13
      Top = 65
      Width = 92
      Height = 20
      Alignment = taLeftJustify
      Caption = '&Habilitado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object ds_MotivosTAGNoDetectado: TDataSource
    DataSet = tbl_MotivosTAGNoDetectado
    Left = 76
    Top = 104
  end
  object tbl_MotivosTAGNoDetectado: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'MotivosTAGNoDetectado'
    Left = 76
    Top = 60
  end
  object sp_ActualizarMotivoTAGNoDetectado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMotivoTAGNoDetectado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMotivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@BajaLogica'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 268
    Top = 60
  end
end
