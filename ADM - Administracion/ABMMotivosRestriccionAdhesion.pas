unit ABMMotivosRestriccionAdhesion;
//------------------------------------------------------------------
// firma        : SS-1006-GVI-20120718
// Description  : Mantener los motivos de Restriccion Adhesion PA
//------------------------------------------------------------------

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB, DMConnection,
  CheckLst, ComCtrls, variants, DPSControls;

type
  TFormMotivosRestriccionAdhesion = class(TForm)
    abmtlbr1: TAbmToolbar;
    lstLista: TAbmList;
    pnl1: TPanel;
    lbl1: TLabel;
    lbl2: TLabel;
    edtCodigoMotivoRestriccion: TNumericEdit;
    edtDescripcion: TEdit;
    pnl2: TPanel;
    pnl3: TPanel;
    Notebook: TNotebook;
    btnBtnSalir: TButton;
    btnBtnAceptar: TButton;
    btnBtnCancelar: TButton;
    MotivosRestriccionAdhesionPAK: TADOTable;
    lblTexto: TLabel;
    mmoTexto: TMemo;

    function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
    procedure abmtlbr1Close(Sender: TObject);
    procedure lstListaInsert(Sender: TObject);
    procedure lstListaEdit(Sender: TObject);
    procedure lstListaDelete(Sender: TObject);
    procedure lstListaClick(Sender: TObject);
    procedure lstListaRefresh(Sender: TObject);
    procedure lstListaDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect;
      State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure btnBtnAceptarClick(Sender: TObject);
    procedure btnBtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBtnSalirClick(Sender: TObject);

  private
    { Private declarations }
    procedure Limpiar_Campos;
  public
    { Public declarations }
    Function Inicializa : boolean;
  end;

var
  FormMotivosRestriccionAdhesion: TFormMotivosRestriccionAdhesion;

implementation

resourcestring
      MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Motivo de Restricci�n Adhesi�n?';
      MSG_DELETE_ERROR		= 'No se puede eliminar el Motivo de Restricci�n Adhesi�n porque hay datos que dependen de �l.';
      MSG_DELETE_CAPTION 		= 'Eliminar Motivo de Restricci�n Adhesi�n';
      MSG_TIPO_ORDEN_SERVICIO = 'No se pudieron actualizar los Tipos de Orden de Servicio asociados al Motivo de Restricci�n Adhesi�n.';

      MSG_DESCRIPCION   = 'Debe especificarse la Descripci�n del Motivo de Restricci�n Adhesi�n';
      MSG_TEXTO         = 'Debe especificarse el Texto del Motivo de Restricci�n Adhesi�n';
      MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar el Motivo de Restricci�n Adhesi�n.';
      CAPTION_VALIDAR_MOTIVO_Inhabilitacion	= 'Actualizar Motivo de Restricci�n Adhesi�n P.A.';


      CAPTION_TIPO_ORDEN_SERVICIO = 'Actualizar Tipos de Orden de Servicio';
      MSG_INSERTAR_MOTIVO_Inhabilitacion = 'No se pudo insertar el nuevo Motivo de Restricci�n Adhesi�n.';
      CAPTION_INSERTAR_MOTIVO_Inhabilitacion = 'Insertar Motivo Restricci�n Adhesi�n';

{$R *.dfm}

procedure TFormMotivosRestriccionAdhesion.abmtlbr1Close(Sender: TObject);
begin
    Close;
end;

procedure TFormMotivosRestriccionAdhesion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFormMotivosRestriccionAdhesion.FormShow(Sender: TObject);
begin
    lstLista.Reload;
end;

function TFormMotivosRestriccionAdhesion.Inicializa: boolean;
Var S: TSize;
begin
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);

    if not OpenTables([MotivosRestriccionAdhesionPAK]) then
        Result := False
    else begin
        Result := True;
        lstLista.Reload;
        lstLista.Enabled := true;
        lstLista.SetFocus;
        edtDescripcion.Enabled := False;
        mmoTexto.Enabled := False;
    end;
    Notebook.PageIndex := 0;
end;

procedure TFormMotivosRestriccionAdhesion.Limpiar_Campos();
begin
    edtCodigoMotivoRestriccion.Clear;
    edtDescripcion.Clear;
    mmoTexto.Clear;
end;

function TFormMotivosRestriccionAdhesion.ListaProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
    Result := True;
    Texto :=  PadR(Trim(Tabla.FieldByName('CodigoMotivo').AsString), 4, ' ' ) + ' '+
    Tabla.FieldByName('Descripcion').AsString;
    Tabla.FieldByName('Texto').AsString;
end;


procedure TFormMotivosRestriccionAdhesion.lstListaClick(Sender: TObject);
begin
    with MotivosRestriccionAdhesionPAK do begin
          edtCodigoMotivoRestriccion.Value := FieldByName('CodigoMotivo').AsInteger;
          edtDescripcion.Text	 := Trim(FieldByName('Descripcion').AsString);
          mmoTexto.Text := Trim(FieldByName('Texto').AsString);
    end;
end;

procedure TFormMotivosRestriccionAdhesion.lstListaDelete(Sender: TObject);
begin
    Screen.Cursor := crHourGlass;
    If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) = IDYES then begin
        DMConnections.BaseCAC.BeginTrans;
        try
            try
                MotivosRestriccionAdhesionPAK.Delete;
                DMConnections.BaseCAC.CommitTrans;
            except
                On E: EDataBaseError do begin
                    MotivosRestriccionAdhesionPAK.Cancel;
                    DMConnections.BaseCAC.RollbackTrans;
                    MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
                end
                else DMConnections.BaseCAC.RollbackTrans;;
            end;
        finally
            lstLista.Estado := Normal;
            lstLista.Enabled := True;
            edtDescripcion.Enabled := False;
            mmoTexto.Enabled := False;
            Notebook.PageIndex := 0;
            Screen.Cursor := crDefault;
            lstLista.Reload;
        end;
    end;
end;

procedure TFormMotivosRestriccionAdhesion.lstListaDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
    With Sender.Canvas, Tabla  do begin
		    FillRect(Rect);
		    TextOut(Cols[0], Rect.Top, FieldByName('CodigoMotivo').AsString);
		    TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
        TextOut(Cols[2], Rect.Top, FieldByName('Texto').AsString);
	  end;
end;

procedure TFormMotivosRestriccionAdhesion.lstListaEdit(Sender: TObject);
begin
    Screen.Cursor := crHourGlass;
    lstLista.Estado := Modi;
    edtDescripcion.Enabled := True;
    mmoTexto.Enabled := True;
    lstLista.Enabled := False;
    Notebook.PageIndex := 1;
    edtDescripcion.SetFocus;
    Screen.Cursor := crDefault;
end;

procedure TFormMotivosRestriccionAdhesion.lstListaInsert(Sender: TObject);
begin
    Screen.Cursor := crHourGlass;
    lstLista.Estado := Alta;
    Limpiar_Campos;
    edtDescripcion.Enabled := true;
    mmoTexto.Enabled := true;
    lstLista.Enabled := False;
    Notebook.PageIndex := 1;
    edtDescripcion.SetFocus;
    Screen.Cursor := crDefault;
end;

procedure TFormMotivosRestriccionAdhesion.lstListaRefresh(Sender: TObject);
begin
    if lstLista.Empty then Limpiar_Campos();
end;

procedure TFormMotivosRestriccionAdhesion.btnBtnAceptarClick(Sender: TObject);

 {function execAgregarMotivosRestriccionAdhesionPA:Integer;
  begin
      result := 0;
      try
          with AgregarMotivosRestriccionAdhesionPA do begin
              Parameters.Refresh;
              Parameters.ParamByName('@Descripcion').Value := edtDescripcion.Text;
              Parameters.ParamByName('@Text').Value := mmoTexto.Text;
              Parameters.ParamByName('@CodigoMotivo').Value := null;
              execProc;
              result := Parameters.ParamByName('@CodigoMotivo').value;
          end;
      except
          On E: Exception do begin
              MsgBoxErr(MSG_INSERTAR_MOTIVO_Inhabilitacion, E.message, CAPTION_INSERTAR_MOTIVO_Inhabilitacion, MB_ICONSTOP);
          end;
      end;
  end; }

  function ActualizarMotivoInhabilitacion: Boolean;
  begin
      result := true;
      with MotivosRestriccionAdhesionPAK do begin
          edit;
	        try
              FieldByName('Descripcion').AsString := Trim(edtDescripcion.Text);
              FieldByName('Texto').AsString := Trim(mmoTexto.Text);
              Post;
   		    except
              On E: Exception do begin
                Result := False;
                Cancel;
                MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, CAPTION_VALIDAR_MOTIVO_Inhabilitacion, MB_ICONSTOP);
              end;
	   	    end;
	    end;
  end;

begin
    Screen.Cursor := crHourGlass;
    if not ValidateControls([edtDescripcion,mmoTexto],
                            [Trim(edtDescripcion.text) <> '',Trim(mmoTexto.text) <> ''],
                            CAPTION_VALIDAR_MOTIVO_Inhabilitacion,
                            [MSG_DESCRIPCION,MSG_TEXTO]) then exit;

	  Screen.Cursor := crHourGlass;
    try
        DMConnections.BaseCAC.BeginTrans;
        // Ahora agrego o modifico el motivo de Inhabilitacion.
        {if (lstLista.Estado = Alta) then begin
            edtCodigoMotivoRestriccion.valueint := execAgregarMotivosRestriccionAdhesionPA;
            if (edtCodigoMotivoRestriccion.ValueInt = 0) then begin
                DMConnections.BaseCAC.RollbackTrans;
                exit;
            end;
        end
        else }
        if not ActualizarMotivoInhabilitacion then begin
            DMConnections.BaseCAC.RollbackTrans;
            exit;
        end;
        DMConnections.BaseCAC.CommitTrans;
    finally
        edtDescripcion.Enabled := False;
        mmoTexto.Enabled := False;
	      lstLista.Estado := Normal;
    	  lstLista.Enabled := True;
	      lstLista.SetFocus;
    	  Notebook.PageIndex := 0;
	      Screen.Cursor := crDefault;
    end;

end;

procedure TFormMotivosRestriccionAdhesion.btnBtnCancelarClick(Sender: TObject);
begin
    edtDescripcion.Enabled := False;
    mmoTexto.Enabled := False;
    lstLista.Estado := Normal;
    lstLista.Enabled := True;
    lstLista.SetFocus;
    Notebook.PageIndex := 0;
end;

procedure TFormMotivosRestriccionAdhesion.btnBtnSalirClick(Sender: TObject);
begin
    Close;
end;

end.
