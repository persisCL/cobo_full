object FormEditarTarifaPuntoCobro: TFormEditarTarifaPuntoCobro
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Editar Tarifa de Punto Cobro'
  ClientHeight = 242
  ClientWidth = 379
  Color = clBtnFace
  Constraints.MaxHeight = 320
  Constraints.MaxWidth = 395
  Constraints.MinHeight = 280
  Constraints.MinWidth = 395
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblPuntoCobro: TLabel
    Left = 11
    Top = 24
    Width = 64
    Height = 13
    Caption = 'Punto Cobro:'
  end
  object lblCategoria: TLabel
    Left = 24
    Top = 51
    Width = 51
    Height = 13
    Caption = 'Categor'#237'a:'
  end
  object lblTipoDia: TLabel
    Left = 33
    Top = 78
    Width = 42
    Height = 13
    Caption = 'Tipo D'#237'a:'
  end
  object lblTipoTarifa: TLabel
    Left = 20
    Top = 105
    Width = 55
    Height = 13
    Caption = 'Tipo Tarifa:'
  end
  object lblImporte: TLabel
    Left = 33
    Top = 186
    Width = 42
    Height = 13
    Caption = 'Importe:'
  end
  object cbbPuntosCobro: TVariantComboBox
    Left = 81
    Top = 21
    Width = 288
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items = <>
  end
  object cbbTipoDia: TVariantComboBox
    Left = 81
    Top = 75
    Width = 288
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 2
    Items = <>
  end
  object grp_Hora_Desde_Hasta: TGroupBox
    Left = 81
    Top = 129
    Width = 259
    Height = 48
    Caption = 'Hora'
    TabOrder = 4
    object lblHoraDesde: TLabel
      Left = 12
      Top = 21
      Width = 29
      Height = 13
      Caption = 'desde'
    end
    object lblHoraHasta: TLabel
      Left = 143
      Top = 19
      Width = 27
      Height = 13
      Caption = 'hasta'
    end
    object edtHoraDesde: TTimeEdit
      Left = 47
      Top = 17
      Width = 58
      Height = 21
      AutoSelect = False
      TabOrder = 0
      AllowEmpty = False
      ShowSeconds = False
    end
    object edtHoraHasta: TTimeEdit
      Left = 175
      Top = 16
      Width = 58
      Height = 21
      AutoSelect = False
      TabOrder = 1
      AllowEmpty = False
      ShowSeconds = False
    end
  end
  object cbbTipoTarifa: TVariantComboBox
    Left = 81
    Top = 102
    Width = 288
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 3
    OnChange = cbbTipoTarifaChange
    Items = <>
  end
  object txtImporte: TEdit
    Left = 81
    Top = 183
    Width = 150
    Height = 21
    TabOrder = 5
  end
  object btnGuardar: TButton
    Left = 215
    Top = 210
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 6
    OnClick = btnGuardarClick
  end
  object btnCancelar: TButton
    Left = 296
    Top = 210
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 7
    OnClick = btnCancelarClick
  end
  object cbbCategoria: TVariantComboBox
    Left = 81
    Top = 48
    Width = 288
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 1
    Items = <>
  end
end
