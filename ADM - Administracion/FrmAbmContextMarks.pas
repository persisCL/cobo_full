{-----------------------------------------------------------------------------
 Unit Name: FrmAbmContextMarks
 Author:    lgisuk
 Date: 05/12/2007
 Purpose: Form para modificar el proveedor y concesionaria del ContextMark.
    El ToolBar tiene deshabilitados los botones Agregar y Eliminar.
    El TAbmList tiene deshabilitados los Access accAlta y accBaja.
-----------------------------------------------------------------------------}
unit FrmAbmContextMarks;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, DbList, Abm_obj, DB, ADODB,
  DMConnection, RStrings, UtilProc, StrUtils, UtilDB, Util, PeaTypes, VariantComboBox;

type
  TFAbmContextMarks = class(TForm)
    AbmToolbar: TAbmToolbar;
    lb_ContextMarks: TAbmList;
    pnl_Datos: TPanel;
    lbl_proveedortag: TLabel;
    Panel2: TPanel;
    Notebook: TNotebook;
    ds_ContextMark: TDataSource;
    btn_Salir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    SPObtenerContextMarks: TADOStoredProc;
    SPActualizarContextMark: TADOStoredProc;
    lbl_Concesionaria: TLabel;
    CbConcesionaria: TVariantComboBox;
    CBProveedorTag: TVariantComboBox;
    procedure AbmToolbarClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure lb_ContextMarksClick(Sender: TObject);
    procedure lb_ContextMarksDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure lb_ContextMarksEdit(Sender: TObject);
    procedure lb_ContextMarksRefresh(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure LimpiarCampos;
    procedure VolverCampos;
  public
    { Public declarations }
	function Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
  end;

var
  FAbmContextMarks: TFAbmContextMarks;

resourcestring
	STR_MAESTRO_CONTEXTMARKS = 'Maestro de ContextMarks';

implementation


{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author: lgisuk
  Date Created: 05/12/2007
  Description: Inicializacion de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFAbmContextMarks.Inicializar(txtCaption: String; MDIChild: boolean): Boolean;

     {-----------------------------------------------------------------------------
      Function Name: CargarProveedoresTag
      Author:    lgisuk
      Date Created: 05/12/2007
      Description: cargo los proveedores de televias
      Parameters: Combo:TVariantComboBox
      Return Value: None
    -----------------------------------------------------------------------------}
    procedure CargarProveedoresTag(Combo:TVariantComboBox);
    const
        CONST_NINGUNO = '(Seleccionar)';
    var
        SP: TAdoStoredProc;
    begin
        try
            Combo.Items.Add(CONST_NINGUNO, 0);
            try
                SP := TADOStoredProc.Create(nil);
                SP.Connection := DMConnections.BaseCAC;
                SP.ProcedureName := 'ObtenerProveedoresTags';
                SP.Open;
                while not sp.Eof do begin
                    Combo.Items.Add(SP.fieldbyname('Descripcion').asstring,SP.fieldbyname('CodigoProveedorTag').asinteger);
                    SP.Next;
                end;
                SP.Close;
                Combo.ItemIndex:=0;
            except
            end;
        finally
            FreeAndNil(SP);
        end;
     end;

    {-----------------------------------------------------------------------------
      Function Name: CargarConcesionaria
      Author:    lgisuk
      Date Created: 05/12/2007
      Description: cargo las concesionarias
      Parameters: Combo:TVariantComboBox
      Return Value: None
    -----------------------------------------------------------------------------}
    procedure CargarConcesionarias(Combo:TVariantComboBox);
    const
        CONST_NINGUNO = '(Seleccionar)';
    var
        SP: TAdoStoredProc;
    begin
        try
            Combo.Items.Add(CONST_NINGUNO, 0);
            try
                SP := TADOStoredProc.Create(nil);
                SP.Connection := DMConnections.BaseCAC;
                SP.ProcedureName := 'ObtenerConcesionarias';
                SP.Open;
                while not sp.Eof do begin
                    Combo.Items.Add(SP.fieldbyname('Descripcion').asstring,SP.fieldbyname('CodigoConcesionaria').asinteger);
                    SP.Next;
                end;
                SP.Close;
                Combo.ItemIndex:=0;
            except
            end;
        finally
            FreeAndNil(SP);
        end;
     end;

var
	S: TSize;
begin
	Result := False;

	if MDIChild then begin
        FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	Notebook.PageIndex := 0;

  CenterForm(Self);
  Caption := AnsiReplaceStr(txtCaption, '&', '');

	if not OpenTables([SPObtenerContextMarks]) then Exit;

  //Cargo los proveedores de televias
  CargarProveedoresTag(CBProveedorTag);
  //Cargo las concesionarias
  CargarConcesionarias(CBConcesionaria);

  // Limpiar controles de ingreso de datos.
  LimpiarCampos;

  KeyPreview := True;
	lb_contextmarks.Reload;
	Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: LimpiarCampos
  Author: lgisuk
  Date Created: 05/12/2007
  Description: Limpia los campos del formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.LimpiarCampos;
begin
    CBProveedorTag.Text := '';
    CBConcesionaria.Text := '';
end;

{-----------------------------------------------------------------------------
  Function Name: VolverCampos
  Author: lgisuk
  Date Created: 05/12/2007
  Description: Restablezco solo lectura
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.VolverCampos;
begin
	lb_ContextMarks.Estado := Normal;
	lb_ContextMarks.Enabled:= True;
	ActiveControl       := lb_ContextMarks;
	Notebook.PageIndex  := 0;
	pnl_Datos.Enabled      := False;
	lb_ContextMarks.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: lb_contextmarksdrawitem
  Author: lgisuk
  Date Created:  05/12/2007
  Description:   obtiene la información a mostrar en la grilla
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.lb_ContextMarksDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	with Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(FieldbyName('ContextMark').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Istr(FieldbyName('IssuerIdentifier').AsInteger, 10));
		TextOut(Cols[2], Rect.Top, Istr(FieldbyName('TypeOfContract').AsInteger, 10));
		TextOut(Cols[3], Rect.Top, Istr(FieldbyName('ContextVersion').AsInteger, 10));
		TextOut(Cols[4], Rect.Top, Trim(FieldbyName('Proveedor').AsString));
		TextOut(Cols[5], Rect.Top, Trim(FieldbyName('Concesionaria').AsString));
	end;
end;

{-----------------------------------------------------------------------------
  Function Name:  lb_ContextMarksEdit
  Author: lgisuk
  Date Created: 05/12/2007
  Description: Coloca al formulario en modo de edicion
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.lb_ContextMarksEdit(Sender: TObject);
begin
	  lb_contextmarks.Enabled  := False;
    lb_contextmarks.Estado   := Modi;
  	Notebook.PageIndex      := 1;
    pnl_Datos.Enabled       := True;
    CbProveedorTag.Enabled := True;
    CbConcesionaria.Enabled := True;
    ActiveControl           := CBProveedorTag;
end;

{-----------------------------------------------------------------------------
  Function Name: lb_ContextMarksClick
  Author: lgisuk
  Date Created: 05/12/2007
  Description: Carga los campos de edicion
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.lb_ContextMarksClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   CbProveedorTag.Value	    := FieldByname('CodigoProveedorTag').AsInteger;
 	   CbConcesionaria.Value	    := FieldByname('CodigoConcesionaria').AsInteger;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: BTNAceptarClick
  Author: lgisuk
  Date Created: 05/12/2007
  Description: Actualiza los datos del registro
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_PROVEEDOR_TAG_NO_VACIO = 'Debe Completar el campo Proveedor Tag.';
    MSG_CONCESIONARIA_NO_VACIO = 'Debe Completar el campo Concesionaria.';
    CAPTION_TITULO = 'ContextMarks';
begin
    //verifico que hayan completado el campo proveedor tag
    if CbProveedorTag.Value = 0 then begin
        MsgBox(MSG_PROVEEDOR_TAG_NO_VACIO, CAPTION_TITULO, MB_ICONSTOP);
        Exit;
    end;

    //verifico que hayan completado el campo concesionaria
    if CbConcesionaria.Value = 0 then begin
        MsgBox(MSG_CONCESIONARIA_NO_VACIO, CAPTION_TITULO, MB_ICONSTOP);
        Exit;
    end;

    with lb_ContextMarks do begin
        Screen.Cursor := crHourGlass;
        try
            try
                with spActualizarContextMark, Parameters do begin
                    Refresh;
                    ParamByName('@ContextMark').Value := Table.FieldByName('ContextMark').AsInteger;

                    ParamByName('@CodigoProveedorTAG').Value := CbProveedorTag.Value;
                    ParamByName('@CodigoConcesionaria').Value := CbConcesionaria.Value;

                    ExecProc;
                end;
            except
                on E: EDataBaseError do begin
                    MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,
                        [STR_MAESTRO_CONTEXTMARKS]), E.Message,
                        Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_CONTEXTMARKS]),
                        MB_ICONSTOP);
                end;
            end;
        finally
            VolverCampos;
            Screen.Cursor:= crDefault;
            Reload;
        end;
    end; // with

end;

{-----------------------------------------------------------------------------
  Function Name: AbmToolbarClose
  Author: lgisuk
  Date Created: 05/12/2007
  Description: Cierra el formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.AbmToolbarClose(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormShow
  Author: lgisuk
  Date Created: 05/12/2007
  Description:  Actualiza la grilla
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.FormShow(Sender: TObject);
begin
    lb_ContextMarks.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnCancelarClick
  Author: lgisuk
  Date Created: 05/12/2007
  Description: Vuelve a modo de lectura
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.BtnCancelarClick(Sender: TObject);
begin
    VolverCampos;
end;

{-----------------------------------------------------------------------------
  Function Name:  lb_ContextMarksRefresh
  Author: lgisuk
  Date Created: 05/12/2007
  Description: si esta vacio la grila borra los campos
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.lb_ContextMarksRefresh(Sender: TObject);
begin
    if lb_contextmarks.Empty then LimpiarCampos;
end;

{-----------------------------------------------------------------------------
  Function Name:  btn_SalirClick
  Author:  lgisuk
  Date Created:  05/12/2007
  Description:   Cierro el formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name:  FormClose
  Author: lgisuk
  Date Created: 05/12/2007
  Description:  Lo libero de memoria
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFAbmContextMarks.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
