{********************************** File Header *********************************
File Name   : frmABMTarifasInfractores
Author      : dAllegretti
Date Created: 07/05/2008
Language    : ES-AR
Description : Realiza la administraci�n de las tarifas de infractores.

Revision 1:
Date: 20/05/2008
Author: dAllegretti
Description: Se corrigen Bugs del Sistema

Revision: SS_FASE2_INFRACTORES_PDO_20110501
Description: Se actualizan las funcionalidades del ABM de Tarifas Infractores,
    para que gestione las Tarifas por Concesionaria.
********************************************************************************}
unit frmABMTarifasInfractores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilProc, ExtCtrls, DbList, Abm_obj, DMConnection, DB, ADODB,
  StdCtrls, DmiCtrls, UtilDB, VariantComboBox, Util, ComCtrls, Validate,
  DateEdit, PeaProcs, PeaProcsCN;

type
  TABMTarifasInfractoresForm = class(TForm) 
    tbTarifasInfractores: TAbmToolbar;
    lbTarifasInfractores: TAbmList;
    pnlTarifasInfractores: TPanel;
    pnlBotones: TPanel;
    lblFechaActivacion: TLabel;
    lblCategoria: TLabel;
    lblImporte: TLabel;
    txtImporte: TEdit;
    qryCategorias: TADOQuery;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    cbCategoria: TVariantComboBox;
    SpTarifasInfractores: TADOStoredProc;
    deFechaActivacion: TDateEdit;
    Panel1: TPanel;
    vcbConcesionaria: TVariantComboBox;
    Label1: TLabel;
    procedure lbTarifasInfractoresDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RefrescarCamposRegistroActual;
	  procedure LimpiarCampos;
    procedure lbTarifasInfractoresClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure lbTarifasInfractoresInsert(Sender: TObject);
    procedure lbTarifasInfractoresDelete(Sender: TObject);
    procedure lbTarifasInfractoresRefresh(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    function Abm(Estado: TAbmState) : boolean;
    procedure ActualizarLista;
    procedure txtImporteKeyPress(Sender: TObject; var Key: Char);
    procedure tbTarifasInfractoresClose(Sender: TObject);
    function ValidarFechaActivacion(FechaActivacion: TDateTime; Categoria: Integer): boolean;
    function VerificarFechaActivacionConFechaNI(FechaActivacion: TDateTime; Categoria: Integer; Var FechaMaxima: TDateTime): Boolean;
    procedure Button1Click(Sender: TObject);
    procedure vcbConcesionariaChange(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitarCamposEdicion(Habilitar : Boolean);
    procedure PantallaModoNavegacion;
    function CargarTarifasConcesionaria(pConcesionaria: Variant): Boolean;  // SS_FASE2_INFRACTORES_PDO_20110501
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

var
    ABMTarifasInfractoresForm : TABMTarifasInfractoresForm;

implementation

Const
    SP_ALTA = 'InsertarTarifasInfractores';
    SP_MODIF = 'ActualizarTarifasInfractores';
    SP_ELIMINAR = 'BorrarTarifasInfractores';
    SP_VALIDAR_FECHA = 'VerificarFechaActivacionTarifasInfractores';
    SP_VALIDAR_FECHA_NI = 'VerificarFechaActivacionConFechaNI';
{$R *.dfm}

ResourceString
    CAPTION_INFO = 'Informaci�n';
    CAPTION_ERROR = 'Error';
    MSG_ERROR_CATEGORIA = 'Debe seleccionar una categor�a valida';
    MSG_IMPORTE_ERROR = 'Debe ingresar un importe v�lido';
    MSG_FECHA_ERROR = 'Debe ingresar una fecha de activaci�n';
    MSG_FECHA_ERROR2 = 'Debe ingresar una fecha de activaci�n superior a la fecha actual.';    // SS_FASE2_INFRACTORES_PDO_20110501
    MSG_COMBINACION_EXISTENTE = 'Ya existe una tarifa para la fecha y categor�a seleccionada, verifique la combinaci�n fecha activaci�n y la categor�a no pueden traslaparse';
    MSG_ERROR_FECHA_ACTIVACION_NI = 'Existen infracciones facturadas con fecha %s, no puede ingresar una fecha anterior';
    MSG_ERROR_ELIMINACION = 'Existen infracciones facturadas que utilizan la tarifa que usted quiere eliminar';

	MSG_ACTUALIZAR_ERROR 	= 'No se pudieron actualizar los datos de la tarifa seleccionada.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Tarifas Infractores';



{******************************** Function Header ******************************
Function Name: ValidarFechaActivacion
Author: dAllegretti
Date Created: 12/05/2008
Description: Realiza la validacion de la nueva fecha de activacion con respecto a las
existentes en la tabla TarifasInfractores
Parameters: FechaActivacion: TDateTime; Categoria: Integer
Return Value: boolean
*******************************************************************************}
function TABMTarifasInfractoresForm.ValidarFechaActivacion(FechaActivacion: TDateTime; Categoria: Integer): Boolean;
var spValidarFecha: TADOStoredProc;
    RetornoValidacionFecha: integer;
begin
    spValidarFecha := TADOStoredProc.Create(Self);
    spValidarFecha.Connection := DMConnections.BaseCAC;
    with spValidarFecha , Parameters do begin
        ProcedureName := SP_VALIDAR_FECHA;
        Refresh;
        ParamByName('@FechaActivacion').Value := FechaActivacion;
        ParamByName('@Categoria').Value := Categoria;
        ParamByName('@CodigoConcesionaria').Value := vcbConcesionaria.Value;    // SS_FASE2_INFRACTORES_PDO_20110501
    end;
    spValidarFecha.ExecProc;
    RetornoValidacionFecha := spValidarFecha.Parameters.ParamByName('@RETURN_VALUE').Value;
    Result:= RetornoValidacionFecha = -1;
    spValidarFecha.Close;
    spValidarFecha.Free;
end;

procedure TABMTarifasInfractoresForm.vcbConcesionariaChange(Sender: TObject);
begin
    CargarTarifasConcesionaria(vcbConcesionaria.Value);
end;

{******************************** Function Header ******************************
Function Name: VerificarFechaActivacionConFechaNI
Author: dAllegretti
Date Created: 12/05/2008
Description: Realiza la validacion de la nueva fecha de activacion con respecto a las
existentes en la tabla de Comprobantes
Parameters: FechaActivacion: TDateTime; Categoria: Integer; Var FechaMaximia: TDateTime
Return Value: boolean
*******************************************************************************}
function TABMTarifasInfractoresForm.VerificarFechaActivacionConFechaNI(FechaActivacion: TDateTime; Categoria: Integer; Var FechaMaxima: TDateTime ): Boolean;
var spValidarFecha: TADOStoredProc;
    RetornoValidacionFecha: integer;
begin
    spValidarFecha := TADOStoredProc.Create(Self);
    spValidarFecha.Connection := DMConnections.BaseCAC;
    with spValidarFecha , Parameters do begin
        ProcedureName := SP_VALIDAR_FECHA_NI;
        Refresh;
        ParamByName('@FechaActivacion').Value := FechaActivacion;
        ParamByName('@Categoria').Value := Categoria;
        ParamByName('@FechaResultado').Value := FechaMaxima;
    end;
    spValidarFecha.ExecProc;
    RetornoValidacionFecha := spValidarFecha.Parameters.ParamByName('@RETURN_VALUE').Value;
    FechaMaxima := spValidarFecha.Parameters.ParamByName('@FechaResultado').Value;
    Result:= RetornoValidacionFecha = -1;
    spValidarFecha.Close;
    spValidarFecha.Free;
end;


{******************************** Function Header ******************************
Function Name: Abm
Author: dAllegretti
Date Created: 08/05/2008
Description: Realiza el ABM sobre la tabla TarifasInfractores
Parameters: Estado: TAbmState
Return Value: boolean
*******************************************************************************}
function TABMTarifasInfractoresForm.Abm(Estado: TAbmState) : boolean;
var spAbm: TADOStoredProc;
    Retorno: Integer;
    FechaMaxima: TDateTime;
begin
    Result := False;
    spAbm := TADOStoredProc.Create(Self);
    try
      try
        if (cbCategoria.ItemIndex = 0) then begin
          MsgBoxBalloon( MSG_ERROR_CATEGORIA, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP, cbCategoria);
          Exit;
        end;
        if (txtImporte.Text = '0') or (txtImporte.Text = '') then begin
          MsgBoxBalloon( MSG_IMPORTE_ERROR, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP, txtImporte);
          Exit;
        end;
        //Revision 1
        if (deFechaActivacion.Date = (-693594)) then begin
          MsgBoxBalloon( MSG_FECHA_ERROR, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP, deFechaActivacion);
          Exit;
        end;

        if (deFechaActivacion.Date <= NowBase(DMConnections.BaseCAC)) then begin                        // SS_FASE2_INFRACTORES_PDO_20110501
          MsgBoxBalloon( MSG_FECHA_ERROR2, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP, deFechaActivacion);     // SS_FASE2_INFRACTORES_PDO_20110501
          Exit;                                                                                         // SS_FASE2_INFRACTORES_PDO_20110501
        end;                                                                                            // SS_FASE2_INFRACTORES_PDO_20110501

        spAbm.Connection := DMConnections.BaseCAC;
        case Estado of
          Alta: begin
            with spAbm , Parameters do begin
              if ValidarFechaActivacion(deFechaActivacion.Date,cbCategoria.ItemIndex) then begin
                MsgBoxBalloon( MSG_COMBINACION_EXISTENTE, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP, deFechaActivacion);
                Exit;
              end;
              {  // SS_FASE2_INFRACTORES_PDO_20110501
              if VerificarFechaActivacionConFechaNI(deFechaActivacion.Date,cbCategoria.ItemIndex, FechaMaxima) then begin
                MsgBoxBalloon(Format(MSG_ERROR_FECHA_ACTIVACION_NI,[DateToStr(FechaMaxima)]),
                     MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP, deFechaActivacion);
                Exit;
              end;
              }  // SS_FASE2_INFRACTORES_PDO_20110501
              ProcedureName := SP_ALTA;
              Refresh;
              ParamByName('@FechaActivacion').Value := deFechaActivacion.Date;
              ParamByName('@Categoria').Value := cbCategoria.ItemIndex;
              ParamByName('@importe').Value := (StrToInt(Trim(txtImporte.Text))*100);
              ParamByName('@UsuarioCreacion').Value := UsuarioSistema;
              ParamByName('@CodigoConcesionaria').Value := vcbConcesionaria.Value;  // SS_FASE2_INFRACTORES_PDO_20110501
            end;
          end;
          {Modi: begin
            with spAbm , Parameters do begin
              ProcedureName := SP_MODIF;
              Refresh;
              ParamByName('@CodigoTarifaDayPassBALI').Value := SPTarifasInfractores.FieldByName('CodigoTarifaDayPassBALI').AsInteger;
              ParamByName('@FechaActivacion').Value := deFechaActivacion.Date;
              ParamByName('@Categoria').Value := cbCategoria.ItemIndex;
              ParamByName('@importe').Value := (StrToInt(Trim(txtImporte.Text))*100);
              ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
            end;
          end;   }
          Baja: begin
            with spAbm , Parameters do begin
              ProcedureName := SP_ELIMINAR;
              Refresh;
              ParamByName('@CodigoTarifaDayPassBALI').Value := SPTarifasInfractores.FieldByName('CodigoTarifaDayPassBALI').AsInteger;
              ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
            end;
          end;
        end;
        spAbm.ExecProc;
        Retorno := spAbm.Parameters.ParamByName('@RETURN_VALUE').Value;
        if (Retorno = -1) and (Estado = Alta)then begin
          MsgBox(MSG_COMBINACION_EXISTENTE, CAPTION_INFO);
          exit;
        end
        else begin
            if (Retorno = -1) and (Estado = Baja) then begin
                MsgBox(MSG_ERROR_ELIMINACION,CAPTION_ERROR, MB_ICONERROR);
                Exit;
            end;
        end;
        Result := True;
        except
          On E: EDataBaseError do begin
            MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
          end;
        end;
    finally
      spAbm.Close;
      spAbm.Free;
    end;
end;
{******************************** Function Header ******************************
Function Name: BtnAceptarClick
Author: dAllegretti
Date Created: 08/05/2008
Description: Ejecuta el ABM de TarifasInfractores llamando al procedimiento Abm
             y pasando por par�metro el estado del AbmList
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.BtnAceptarClick(Sender: TObject);
begin
    if Abm(lbTarifasInfractores.Estado) then begin
        Screen.Cursor := crHourGlass;
        PantallaModoNavegacion;
    end;
end;

{******************************** Function Header ******************************
Function Name: BtnCancelarClick
Author: dAllegretti
Date Created: 08/05/2008
Description: Cancela la edici�n de la tarifa infractor llamando al procedimiento
              PantallaModoNavegacion
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.BtnCancelarClick(Sender: TObject);
begin
    PantallaModoNavegacion;
    RefrescarCamposRegistroActual;
end;

{******************************** Function Header ******************************
Function Name: BtnSalirClick
Author: dAllegretti
Date Created: 08/05/2008
Description: Llama al metodo para cerrar el formulario
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TABMTarifasInfractoresForm.Button1Click(Sender: TObject);
begin

end;

{******************************** Function Header ******************************
Function Name: HabilitarCamposEdicion
Author: dAllegretti
Date Created: 08/05/2008
Description: Habilita/Deshabilita los campos para edici�n de un registro
Parameters: Habilitar: Boolean
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.HabilitarCamposEdicion(Habilitar: Boolean);
begin
    lbTarifasInfractores.Enabled := not Habilitar;
    pnlTarifasInfractores.Enabled := Habilitar;
    Notebook.PageIndex := ord(Habilitar);
    Screen.Cursor := crDefault;
end;

{******************************** Function Header ******************************
Function Name: PantallaModoNavegacion
Author: dAllegretti
Date Created: 08/05/2008
Description: Se establece el formulario en modo de navegaci�n de registros
Parameters: None
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.PantallaModoNavegacion;
begin
    lbTarifasInfractores.Estado := Normal;
    HabilitarCamposEdicion(False);
	Notebook.PageIndex := 0;
end;

{******************************** Function Header ******************************
Function Name: RefrescarCamposRegistroActual
Author: dAllegretti
Date Created: 08/05/2008
Description: Por cada tarifa infractor seleccionada (grilla), se muestra
el valor de todos los campos en el panel de detalle
Parameters: None
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.RefrescarCamposRegistroActual;
begin

    with spTarifasInfractores do begin
        deFechaActivacion.Date := FieldByName('FechaActivacion').AsDateTime;
        txtImporte.Text := Trim(FieldByName('Importe').AsString);
        cbCategoria.ItemIndex := FieldByName('Categoria').AsInteger;
	end;
end;

procedure TABMTarifasInfractoresForm.tbTarifasInfractoresClose(Sender: TObject);
begin
    close;
end;

procedure TABMTarifasInfractoresForm.txtImporteKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not(key in ['0','1','2','3','4','5','6','7','8','9', #8]) then key := #0;
end;

{******************************** Function Header ******************************
Function Name: LimpiarCampos
Author: dAllegretti
Date Created: 08/05/2008
Description: Coloca valores por defecto a todos los campos del panel de detalle
Parameters: None
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.LimpiarCampos;
begin
    deFechaActivacion.Date := NowBaseSmall(DMConnections.BaseCAC); // SS_FASE2_INFRACTORES_PDO_20110501
    txtImporte.Text := '';
    cbCategoria.ItemIndex := 0;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author: dAllegretti
Date Created: 08/05/2008
Description: libera la instancia del form al cerrar
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    action := caFree;
end;
{******************************** Function Header ******************************
Function Name: Inicializar
Author: dAllegretti
Date Created: 08/05/2008
Description: Proceso inicial que se ejecuta la crear el formulario y lo inicializa.
Parameters: None
Return Value: Boolean
*******************************************************************************}
function TABMTarifasInfractoresForm.Inicializar: Boolean;
    resourcestring
        ERROR_MSG = 'Error de Inicializacion';
    var
	    S: TSize;
begin
    try                                                                            // SS_FASE2_INFRACTORES_PDO_20110501
        Result := False;
        FormStyle := fsMDIChild;
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
        CenterForm(Self);

        CargarComboConcesionarias(DMConnections.BaseCAC, vcbConcesionaria, False); // SS_FASE2_INFRACTORES_PDO_20110501

        Result := CargarTarifasConcesionaria(vcbConcesionaria.Value);              // SS_FASE2_INFRACTORES_PDO_20110501
    except                                                                         // SS_FASE2_INFRACTORES_PDO_20110501
        on e: Exception do begin                                                   // SS_FASE2_INFRACTORES_PDO_20110501
            ShowMsgBoxCN(e, Self);                                                 // SS_FASE2_INFRACTORES_PDO_20110501
        end;                                                                       // SS_FASE2_INFRACTORES_PDO_20110501
    end;                                                                           // SS_FASE2_INFRACTORES_PDO_20110501
end;

{******************************** Function Header ******************************
Function Name: lbTipoMedioPagoClick
Author: dAllegretti
Date Created: 08/05/2008
Description: Proceso que se ejecuta al seleccionar un registro de la grilla.
             Recupera todos los campos del registro seleccionado en el panel de detalle
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.lbTarifasInfractoresClick(Sender: TObject);
begin
    RefrescarCamposRegistroActual;
end;

{******************************** Function Header ******************************
Function Name: ActualizarLista
Author: dAllegretti
Date Created: 08/05/2008
Description: Refresca los datos de la lista
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.ActualizarLista;
begin
    SpTarifasInfractores.Close;
    SpTarifasInfractores.Open;
end;

{******************************** Function Header ******************************
Function Name: lbTipoMedioPagoDelete
Author: dAllegretti
Date Created: 08/05/2008
Description: Proceso para eliminar el registro seleccionado en la grilla
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.lbTarifasInfractoresDelete(Sender: TObject);
resourcestring
    MSG_CONFIRMA_ELIMINAR_TARIFA_INFRACCION = '�Est� seguro de querer eliminar el registro seleccionado?';
	MSG_ELIMINAR_TARIFA_INFRACCION_CAPTION  = 'Eliminar Tarifa Infracci�n';

begin

	Screen.Cursor := crHourGlass;
    lbTarifasInfractores.Estado := Baja;
    if MessageBox(self.Handle, Pchar(MSG_CONFIRMA_ELIMINAR_TARIFA_INFRACCION), Pchar(MSG_ELIMINAR_TARIFA_INFRACCION_CAPTION), MB_YESNO or MB_ICONQUESTION) = idyes then Abm(lbTarifasInfractores.Estado);
    lbTarifasInfractores.Estado := Normal;
    ActualizarLista;
    PantallaModoNavegacion;
end;

{******************************** Function Header ******************************
Function Name: lbTipoMedioPagoDrawItem
Author: dAllegretti
Date Created: 08/05/2008
Description: Coloca el valor de las columnas en la grilla
Parameters: Sender: TDBList, Tabla: TDataSet, Rect: TRect, State: TOwnerDrawState, Cols: TColPositions
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.lbTarifasInfractoresDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	with Sender.Canvas  do begin
    FillRect(Rect);
    with Tabla do begin
        TextOut(Cols[0], Rect.Top, Trim(Fieldbyname('FechaActivacion').AsString));
        TextOut(Cols[1], Rect.Top, Fieldbyname('Descripcion').AsString);
        TextOut(Cols[2], Rect.Top, Fieldbyname('Importe').AsString);
    end;
 //   RefrescarCamposRegistroActual;
  end;
end;

{******************************** Function Header ******************************
Function Name: lbTipoMedioPagoInsert
Author: dAllegretti
Date Created: 08/05/2008
Description: Pone la pantalla en modo de inserci�n de un nuevo registro
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.lbTarifasInfractoresInsert(Sender: TObject);
begin
	LimpiarCampos;
    HabilitarCamposEdicion(True);
    lbTarifasInfractores.Estado := alta;
	deFechaActivacion.SetFocus;
end;

{******************************** Function Header ******************************
Function Name: lbTipoMedioPagoRefresh
Author: dAllegretti
Date Created: 08/05/2008
Description: Proceso de refresh de la grilla. Si no hay registros en la grilla,
             coloca valores por defecto a todos los campos del panel de detalle.
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTarifasInfractoresForm.lbTarifasInfractoresRefresh(Sender: TObject);
begin
	if lbTarifasInfractores.Empty then LimpiarCampos;
end;

{
    Firma: SS_FASE2_INFRACTORES_PDO_20110501
    Descripci�n: Carga en la grilla, las tarifas correspondientes a la concesionaria seleccionada.
}
function TABMTarifasInfractoresForm.CargarTarifasConcesionaria(pConcesionaria: Variant): Boolean;
    var
        i, CantidadCategorias: Integer;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        try
            Result := False;

            with SpTarifasInfractores do begin
                if Active then Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConcesionaria').Value := pConcesionaria;
            end;
            if not OpenTables([spTarifasInfractores]) then exit;
            PantallaModoNavegacion;
            lbTarifasInfractores.Reload;
            Notebook.PageIndex := 0;
            qryCategorias.Active := True;
            CantidadCategorias := qryCategorias.RecordCount;
            qryCategorias.First;
            cbCategoria.Items.Clear;
            cbCategoria.Items.Add('SELECCIONAR',0);
            for i := 0 to CantidadCategorias - 1 do begin
                cbCategoria.Items.Add(qryCategorias.FieldByName('Descripcion').AsString, qryCategorias.FieldByName('Categoria').AsInteger);
                qryCategorias.Next;
            end;
            RefrescarCamposRegistroActual;
            if (ExisteAcceso('Eliminar_TarifasInfractores')) then tbTarifasInfractores.Habilitados := [btAlta,btBaja,btSalir,btBuscar]
                else tbTarifasInfractores.Habilitados := [btAlta, btSalir, btBuscar];
            Result := True;
        except
            On E: exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

end.
