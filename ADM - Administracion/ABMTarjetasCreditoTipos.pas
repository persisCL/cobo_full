{********************************** File Header *********************************
File Name   : ABMTarjetasCreditoTipos
Author      : Gonzalez, Damian <dgonzalez@dpsautomation.com>
Date Created: 02-Mar-2004
Language    : ES-AR
Description : Esta Unit realiza la administraci�n de los tipos de tarjetas de
                credito. Por ejemplo, Master Card Comun, Master Card Gold, etc. 

Revision :
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura

Date        :   08-05-2014
Firma       :   SS_1147_CQU_20140508
Author      :   CQuezadaI
Description :   Se corrige un problema al cargar la ventana
                y es que no deja seleccionado el tipo de tarjeta de cr�dito
********************************************************************************}
unit ABMTarjetasCreditoTipos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  Util, UtilProc, OleCtrls, DmiCtrls, ComCtrls, PeaProcs, DMConnection,
  ADODB, variants, DPSControls, PeaTypes, VariantComboBox, StrUtils;             //TASK_119_JMA_20170322

ResourceString
	MSG_CAPTION_VALIDAR_PREFIJO 		= 'Validar Prefijos de Tarjeta de Cr�dito';
  	MSG_ERROR_SELECCIONAR_ITEM			= 'Debe seleccionar un item de la lista.';
	MSG_NUEVO_PREFIJO_QUERY				= 'Nuevo Prefijo: ';
    MSG_NUEVO_PREFIJO_CAPTION			= 'Agregar Prefijo';
    MSG_FILTRO_TARJETAS                 = 'Debe indicar la tarjeta de cr�dito filtro.';
    MSG_ERROR                           = 'El largo del prefijo debe ser el indicado en Longitud de Prefijos.';            //TASK_119_JMA_20170322
    MSG_ERROR2                          = 'El prefijo debe ser un numero entero.';                                         //TASK_119_JMA_20170322
    MSG_ERROR3                          = 'El prefijo ingresado ya existe en la lista.';                                   //TASK_119_JMA_20170322
    MSG_VALIDAR_CARACTER_INICIAL        = 'Debe ingresar la Longitud de Prefijo de la banda';                              //TASK_119_JMA_20170322
    MSG_VALIDAR_CAPTION 		        = 'Validar datos del tipo de tarjeta de cr�dito';                                  //TASK_119_JMA_20170322
    MSG_VALIDAR_CARACTER_INICIAL2       = 'Logitud Prefijo debe ser un valor entre entre 1 y 10 inclusive';                //TASK_119_JMA_20170322
type
  TFormTarjetasCreditoTipo = class(TForm)
    AbmToolbar1: TAbmToolbar;
    alTarjetaCreditoTipo: TAbmList;
    gbTarjetaCreditoTipo: TPanel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Label2: TLabel;
    Notebook: TNotebook;
    ActualizarDatosTarjetaCreditoTipos: TADOStoredProc;
    TarjetasCreditoTipos: TADOTable;
    EliminarTarjetasCreditoTipos: TADOStoredProc;
    Label1: TLabel;
    txt_caracterinicialbanda: TNumericEdit;
    Label5: TLabel;
    txt_longitudbanda: TNumericEdit;
    Label6: TLabel;
    lb_prefijos: TListBox;
    chkActivo: TCheckBox;
    qry_prefijos: TADOQuery;
    Label15: TLabel;
    txt_codigo: TNumericEdit;
    cbValidarPrefijo: TCheckBox;
    Label3: TLabel;
    Panel1: TPanel;
    btnAgregarPrefijo: TButton;
    btnQuitarPrefijo: TButton;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    cbFamiliaTarjetas: TVariantComboBox;
    Label4: TLabel;
    QryFamiliasTarjetas: TADOQuery;
    procedure BtnCancelarClick(Sender: TObject);
    procedure alTarjetaCreditoTipoDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure alTarjetaCreditoTipoEdit(Sender: TObject);
    procedure alTarjetaCreditoTipoRefresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure alTarjetaCreditoTipoDelete(Sender: TObject);
    procedure alTarjetaCreditoTipoInsert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure alTarjetaCreditoTipoClick(Sender: TObject);
    procedure btnAgregarPrefijoClick(Sender: TObject);
    procedure btnQuitarPrefijoClick(Sender: TObject);
    procedure txt_caracterinicialbandaKeyPress(Sender: TObject; var Key: Char);
  private
	{ Private declarations }
    CodigoTipoTarjetaCredito: integer;
    procedure HabilitarCamposEdicion(habilitar : Boolean);
	procedure Limpiar_Campos;
    procedure Volver_Campos;
    procedure RefrescarCamposRegistroActual;
  public
	{ Public declarations }
    function Inicializar(MDIChild: Boolean = True; CodigoTarjeta: integer = 0): Boolean;
  end;

var
  FormTarjetasCreditoTipo: TFormTarjetasCreditoTipo;

implementation
resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del tipo de Tarjeta de cr�dito.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar tipo de Tarjeta de cr�dito';

{$R *.DFM}

procedure TFormTarjetasCreditoTipo.HabilitarCamposEdicion(habilitar: Boolean);
begin
    alTarjetaCreditoTipo.Enabled    := not Habilitar;
	gbTarjetaCreditoTipo.Enabled    := Habilitar;
	Notebook.PageIndex 		        := ord(habilitar);
    btnAgregarPrefijo.Enabled       := habilitar;                         //TASK_119_JMA_20170322
    btnQuitarPrefijo.Enabled        := habilitar;                         //TASK_119_JMA_20170322
end;

procedure TFormTarjetasCreditoTipo.Volver_Campos ;
begin
	alTarjetaCreditoTipo.Estado     := Normal;
    HabilitarCamposEdicion(False);
//    lb_prefijos.Clear;
	Notebook.PageIndex      := 0;
end;

function TFormTarjetasCreditoTipo.Inicializar(MDIChild: Boolean = True; CodigoTarjeta: integer = 0): Boolean;
ResourceString
    LBL_FORM_CAPTION = 'Tipos de tarjetas de %s';
Var
	S: TSize;
    DescripcionTarjeta: AnsiString;
    i,contfamilias : integer;
begin
    DescripcionTarjeta := '';
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;

(*
    FCodigoTarjeta := CodigoTarjeta;
    if FCodigoTarjeta > 0 then begin
        TarjetasCreditoTipos.Filtered   := True;
        TarjetasCreditoTipos.Filter     := 'CodigoTipoTarjeta = ' + intToStr(CodigoTipoTarjeta);
        DescripcionTarjeta              := BuscarApellidoNombreCliente(DMConnections.BaseCAC, FCodigoTarjeta);
        Caption                         := Format(LBL_FORM_CAPTION, [DescripcionTarjeta]);
    end else begin
        TarjetasCreditoTipos.Filtered   := False;
        TarjetasCreditoTipos.Filter     := '';
    end;
*)

	if not OpenTables([TarjetasCreditoTipos]) then exit;
    TarjetasCreditoTipos.Sort := 'CodigoTarjeta ASC';
    CodigoTipoTarjetaCredito := -1;
	Volver_Campos;
    alTarjetaCreditoTipo.Reload;
   	Notebook.PageIndex := 0;
	Result := True;

    try
        QryFamiliasTarjetas.Active := True;
        contfamilias := QryFamiliasTarjetas.RecordCount;
        QryFamiliasTarjetas.First;
        cbFamiliaTarjetas.Items.Clear;
        cbFamiliaTarjetas.Items.Add('SELECCIONAR',0);
        for i := 0 to contfamilias - 1 do begin
            cbFamiliaTarjetas.Items.Add(QryFamiliasTarjetas.FieldByName('Descripcion').AsString, QryFamiliasTarjetas.FieldByName('CodigoFamiliaTarjetaCredito').AsInteger);
            QryFamiliasTarjetas.Next;
        end;
        QryFamiliasTarjetas.Active := False;
        RefrescarCamposRegistroActual;  // SS_1147_CQU_20140508
    except
        On E: exception do begin
            exit;
        end;
    end;
end;

procedure TFormTarjetasCreditoTipo.BtnCancelarClick(Sender: TObject);
begin
    RefrescarCamposRegistroActual;       //TASK_119_JMA_20170322
	Volver_Campos;
end;

procedure TFormTarjetasCreditoTipo.alTarjetaCreditoTipoDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas  do begin
        FillRect(Rect);
        if Tabla.FieldByName('Activo').AsBoolean then begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clWindow
            else
                Sender.Canvas.Font.Color := clWindowText;
        end else begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clYellow
            else
                Sender.Canvas.Font.Color := clRed;
        end;
        With Tabla do begin
            //TextOut(Cols[0] + 1, Rect.Top, Trim(Fieldbyname('CodigoTipoTarjetaCredito').AsString));
            TextOut(Cols[0] + 1, Rect.Top, Trim(Fieldbyname('CodigoTipoTarjetaCredito').AsString));
            TextOut(Cols[1], Rect.Top, Fieldbyname('Descripcion').AsString);
            TextOut(Cols[2], Rect.Top, Fieldbyname('CaracterInicialBanda').AsString);
            TextOut(Cols[3], Rect.Top, Fieldbyname('LongitudBanda').AsString);
            TextOut(Cols[4], Rect.Top, iif(Fieldbyname('ValidarPrefijo').AsBoolean, MSG_SI, MSG_NO));
            TextOut(Cols[5], Rect.Top, iif(Fieldbyname('Activo').AsBoolean, MSG_SI, MSG_NO));
            TextOut(Cols[6], Rect.Top, Fieldbyname('CodigoFamiliaTarjetaCredito').AsString);
        end;
    end;
end;

procedure TFormTarjetasCreditoTipo.alTarjetaCreditoTipoEdit(Sender: TObject);
begin
    HabilitarCamposEdicion(True);
    alTarjetaCreditoTipo.Estado     := modi;
	txt_Descripcion.SetFocus;
end;

procedure TFormTarjetasCreditoTipo.alTarjetaCreditoTipoRefresh(Sender: TObject);
begin
	if alTarjetaCreditoTipo.Empty then Limpiar_Campos;
end;

procedure TFormTarjetasCreditoTipo.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormTarjetasCreditoTipo.alTarjetaCreditoTipoDelete(Sender: TObject);
resourcestring
	MSG_ELIMINAR_TIPO_TC_CAPTION            = 'Eliminar Tipo de tarjeta de cr�dito';
	MSG_ELIMINAR_TIPO_TC_ERROR              = 'No se puede eliminar este tipo de tarjeta de cr�dito porque hay datos que dependen de �l.';
  	MSG_ELIMINAR_TIPO_TC_ERROR2              = 'Error eliminando tarjeta de credito';                              //TASK_119_JMA_20170322
	MSG_ELIMINAR_TIPO_TC_QUESTION           = '�Est� seguro de querer eliminar este tipo de tarjeta de cr�dito?';
	MSG_ELIMINAR_TIPO_TC_QUESTION_CAPTION   = 'Confirmaci�n...';
    MSG_VALIDAR_REGISTRO_TBK                = 'EL Registro Transbank no puede ser Eliminado';
Const
    CODIGO_TBK = '7';
begin

    //valido que el registro transbank no pueda ser eliminado
    if TarjetasCreditoTipos.FieldByName('CodigoTipoTarjetaCredito').asstring = CODIGO_TBK then begin
        msgbox(MSG_VALIDAR_REGISTRO_TBK);
        exit;
    end;

	Screen.Cursor := crHourGlass;
    try
        If MsgBox(MSG_ELIMINAR_TIPO_TC_QUESTION, MSG_ELIMINAR_TIPO_TC_CAPTION, MB_YESNO) = IDYES then begin
            try 
                EliminarTarjetasCreditoTipos.Parameters.Refresh;
                EliminarTarjetasCreditoTipos.Parameters.ParamByName('@CodigoTipoTarjetaCredito').Value := TarjetasCreditoTipos.FieldByName('CodigoTipoTarjetaCredito').AsInteger;
                EliminarTarjetasCreditoTipos.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;         //TASK_119_JMA_20170322
                EliminarTarjetasCreditoTipos.ExecProc;
                EliminarTarjetasCreditoTipos.Close;
{INICIO: TASK_119_JMA_20170322}
                if EliminarTarjetasCreditoTipos.Parameters.ParamByName('@RETURN_VALUE').Value = -1 then
                begin
                    if AnsiContainsStr(EliminarTarjetasCreditoTipos.Parameters.ParamByName('@ErrorDescription').Value, 'REFERENCE') then
                    begin
                        if MsgBox('El Tipo de tarjeta de credito no se puede eliminar porque ya ha sido utilizado en el sistema' + Chr(13) +
                                ' �Desea desactivarla?','Atenci�n', MB_YESNO) = mrYes then
                         begin
                             alTarjetaCreditoTipo.Estado := modi;
                             chkActivo.Checked := False;
                             BtnAceptarClick(nil);
                         end;
                    end
                    else
                        raise Exception.Create(EliminarTarjetasCreditoTipos.Parameters.ParamByName('@ErrorDescription').Value);
                end;
{TERMINO: TASK_119_JMA_20170322}
            Except
                On E: exception do begin
                    MsgBoxErr(MSG_ELIMINAR_TIPO_TC_ERROR2, E.message, MSG_ELIMINAR_TIPO_TC_CAPTION, MB_ICONSTOP);          //TASK_119_JMA_20170322
                    EliminarTarjetasCreditoTipos.Close;
                end;
            end;
        end;
        Volver_Campos;
    finally
	    Screen.Cursor := crDefault;
    end;
end;

procedure TFormTarjetasCreditoTipo.alTarjetaCreditoTipoInsert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCamposEdicion(True);
    txt_caracterinicialbanda.Enabled := True;                                //TASK_119_JMA_20170322
    alTarjetaCreditoTipo.Estado := alta;
    chkActivo.Checked  := True;
	txt_Descripcion.SetFocus;
end;

procedure TFormTarjetasCreditoTipo.BtnAceptarClick(Sender: TObject);
ResourceString
	MSG_ACTUALIZAR_CAPTION		    = 'Actualizar tipo de tarjeta de cr�dito';
    MSG_ACTUALIZAR_ERROR		    = 'No se pudieron actualizar los datos del tipo de tarjeta de cr�dito';
    //MSG_VALIDAR_CAPTION 		    = 'Validar datos del tipo de tarjeta de cr�dito';                                        //TASK_119_JMA_20170322
    MSG_VALIDAR_TARJETA             = 'Debe ingresar la tarjeta de cr�dito';
	MSG_VALIDAR_DESCRIPCION 	    = 'Debe ingresar una Descripci�n';
    //MSG_VALIDAR_CARACTER_INICIAL    = 'Debe ingresar el caracter inicial de la banda';                                     //TASK_119_JMA_20170322
    MSG_VALIDAR_LONGITUD_BANDA      = 'Debe ingresar la longitud de la banda';                                               //TASK_119_JMA_20170322
    MSG_VALIDAR_LONGITUD_BANDA2      = 'La longitud de la banda debe ser mayor o igual a Longitud de Prefijo';
    MSG_ACTUALIZAR_CAPTION_PREFIJO	= 'Actualizar Prefijo de Tarjeta de Cr�dito';
    MSG_VALIDAR_REGISTRO_TBK        = 'EL Registro Transbank no puede ser Modificado';
    MSG_ERROR_FAMILIA_TARJETA       = 'Debe Seleccionar un valor de familia de tarjetas de cr�dito';
    MSG_VALIDAR_PREFIJOS            = 'Debe ingresar al menos 1 prefijo';                                                    //TASK_119_JMA_20170322
Const
    CODIGO_TBK = '7';
var
    FTipoTarjetaCredito: integer;
    //TodoOk: Boolean;                  //TASK_119_JMA_20170322
    i: Integer;
    Prefijos : String;                  //TASK_119_JMA_20170322
begin


//Valido que la familia se insert� bien
   	if (cbFamiliaTarjetas.ItemIndex = 0) then begin
		MsgBoxBalloon( MSG_ERROR_FAMILIA_TARJETA, MSG_VALIDAR_CAPTION, MB_ICONSTOP, cbFamiliaTarjetas);
		Exit;
    end;

    //valido que el registro transbank no sea modificado
    if  txt_codigo.text = CODIGO_TBK then begin
        MsgBox( MSG_VALIDAR_REGISTRO_TBK);
        Exit;
    end;

   	if (Trim(txt_Descripcion.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDAR_DESCRIPCION, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txt_Descripcion);
		Exit;
    end;

	if (trim(txt_caracterinicialbanda.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDAR_CARACTER_INICIAL, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txt_caracterinicialbanda);
    	Exit;
    end;

	if (trim(txt_longitudbanda.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDAR_LONGITUD_BANDA, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txt_longitudbanda);
    	Exit;
    end;
    
{INICIO: TASK_119_JMA_20170322}
    if (StrToInt(trim(txt_longitudbanda.Text)) < StrToInt(trim(txt_caracterinicialbanda.Text))) then begin
		MsgBoxBalloon( MSG_VALIDAR_LONGITUD_BANDA2, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txt_longitudbanda);
    	Exit;
    end;

    if lb_prefijos.Count = 0 then
    begin
       MsgBoxBalloon(MSG_VALIDAR_PREFIJOS, MSG_VALIDAR_CAPTION, MB_ICONSTOP, lb_prefijos);
       Exit;
    end;
{TERMINO: TASK_119_JMA_20170322}

	Screen.Cursor := crHourGlass;
{INICIO: TASK_119_JMA_20170322
    TodoOk := False;
    try
    	DMConnections.BaseCAC.BeginTrans;
        Try
            if alTarjetaCreditoTipo.Estado = Alta then begin
                FTipoTarjetaCredito := -1;
            end else begin
                FTipoTarjetaCredito := TarjetasCreditoTipos.fieldByName('CodigoTipoTarjetaCredito').AsInteger;
            end;

            with ActualizarDatosTarjetaCreditoTipos.Parameters do begin
                ParamByName('@CodigoTipoTarjetaCredito').Value := iif(FTipoTarjetaCredito = -1, null, FTipoTarjetaCredito);
                ParamByName('@Descripcion').Value		    := iif(Trim(txt_Descripcion.Text)= '', null, Trim(txt_Descripcion.Text));
                ParamByName('@CodigoTarjeta').Value         := null;
                ParamByName('@CaracterInicialBanda').Value  := iif(txt_CaracterInicialBanda.Valueint < 1, null, txt_CaracterInicialBanda.Valueint);
                ParamByName('@LongitudBanda').Value         := iif(txt_LongitudBanda.Valueint < 1, null, txt_LongitudBanda.Valueint);
                ParamByName('@ValidarPrefijo').Value        := cbValidarPrefijo.Checked;
//                ParamByName('@ValidarPrefijo').Value        := (lb_prefijos.Items.Count > 0);
                ParamByName('@Activo').Value                := chkActivo.Checked;
                ParamByName('@CodigoFamiliaTarjeta').Value  := cbFamiliaTarjetas.value;
            end;
            ActualizarDatosTarjetaCreditoTipos.ExecProc;
            FTipoTarjetaCredito := ActualizarDatosTarjetaCreditoTipos.Parameters.ParamByName('@CodigoTipoTarjetaCredito').Value;
            ActualizarDatosTarjetaCreditoTipos.Close;

            if alTarjetaCreditoTipo.Estado = Modi then begin
                // Para grabar la lista de prefijos, primero borro los anteriores.
                // en el alta no los borro, obviamente!
                try
                   querytemp.close;
                   querytemp.SQL.clear;
                   querytemp.SQL.add('DELETE FROM TarjetasCreditoPrefijos WHERE CodigoTipoTarjetaCredito =  ' + IntToStr(FTipoTarjetaCredito));
                   querytemp.execsql;
                except
                    On E: EDataBaseError do begin
                        raise Exception.Create(MSG_ACTUALIZAR_CAPTION_PREFIJO + #13#10 +
                                                E.message);
                    end;
                end;
            end;

            // Agregar los nuevos
            for i := 0 to lb_prefijos.items.count -1 do begin
                // Escribir uno por uno.
                try
                    querytemp.close;
                    querytemp.SQL.clear;
                    querytemp.SQL.add('INSERT INTO TarjetasCreditoPrefijos (codigoTipoTarjetaCredito, Prefijo)');
                    querytemp.SQL.add('VALUES ( ' + IntToStr(FTipoTarjetaCredito));
                    querytemp.SQL.add(', ''' + trim(lB_prefijos.items[i]) +  ''')');
                    querytemp.execsql;
                except
                    On E: Exception do begin
                        raise Exception.Create(MSG_ACTUALIZAR_CAPTION_PREFIJO + #13#10 +
                                                E.message);
                    end;
                 end;
            end;
            TodoOk := True;
}
    try
        try

            if alTarjetaCreditoTipo.Estado = Alta then begin
                FTipoTarjetaCredito := -1;
            end else begin
                FTipoTarjetaCredito := TarjetasCreditoTipos.fieldByName('CodigoTipoTarjetaCredito').AsInteger;
            end;

            Prefijos := '';
            for I := 0 to lb_prefijos.Count - 1 do
            begin
                if Prefijos <> '' then
                    Prefijos := Prefijos + ';';
                Prefijos := Prefijos + lb_prefijos.Items[i];
            end;

            with ActualizarDatosTarjetaCreditoTipos.Parameters do begin
                Refresh;
                ParamByName('@CodigoTipoTarjetaCredito').Value := iif(FTipoTarjetaCredito = -1, null, FTipoTarjetaCredito);
                ParamByName('@Descripcion').Value		    := iif(Trim(txt_Descripcion.Text)= '', null, Trim(txt_Descripcion.Text));
                ParamByName('@CodigoTarjeta').Value         := null;
                ParamByName('@CaracterInicialBanda').Value  := iif(txt_CaracterInicialBanda.Valueint < 1, null, txt_CaracterInicialBanda.Valueint);
                ParamByName('@LongitudBanda').Value         := iif(txt_LongitudBanda.Valueint < 1, null, txt_LongitudBanda.Valueint);
                ParamByName('@ValidarPrefijo').Value        := cbValidarPrefijo.Checked;
                ParamByName('@Activo').Value                := chkActivo.Checked;
                ParamByName('@CodigoFamiliaTarjeta').Value  := cbFamiliaTarjetas.value;
                ParamByName('@Prefijos').Value  := Prefijos;
                ParamByName('@Usuario').Value  := UsuarioSistema;
            end;
            ActualizarDatosTarjetaCreditoTipos.ExecProc;

            if ActualizarDatosTarjetaCreditoTipos.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                raise Exception.Create(ActualizarDatosTarjetaCreditoTipos.Parameters.ParamByName('@ErrorDescription').Value);

            FTipoTarjetaCredito := ActualizarDatosTarjetaCreditoTipos.Parameters.ParamByName('@CodigoTipoTarjetaCredito').Value;

            Volver_Campos;
            alTarjetaCreditoTipo.Reload;
            RefrescarCamposRegistroActual;
{TERMINO: TASK_119_JMA_20170322}

        except
            On E: Exception do begin
                MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                ActualizarDatosTarjetaCreditoTipos.Close;
                Exit;
            end;
        end;
    finally
{INICIO: TASK_119_JMA_20170322
    	if TodoOk then begin
        	DMConnections.BaseCAC.CommitTrans;
            Volver_Campos;
            Screen.Cursor := crDefault;
        end else begin
	        DMConnections.BaseCAC.RollbackTrans;
            Screen.Cursor := crDefault;
		end;
        alTarjetaCreditoTipo.Reload;
        RefrescarCamposRegistroActual;
}
        if ActualizarDatosTarjetaCreditoTipos.Active then        
            ActualizarDatosTarjetaCreditoTipos.Close;
        Screen.Cursor := crDefault;
{TERMINO: TASK_119_JMA_20170322}
    end;
end;

procedure TFormTarjetasCreditoTipo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormTarjetasCreditoTipo.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormTarjetasCreditoTipo.Limpiar_Campos;
begin
    txt_codigo.Clear;
	txt_Descripcion.clear;
    txt_caracterinicialbanda.Clear;
    txt_longitudbanda.Clear;
    lb_prefijos.Clear;
    cbValidarPrefijo.Checked := false;
    chkActivo.Checked := true;
    cbFamiliaTarjetas.ItemIndex := 0;
end;

procedure TFormTarjetasCreditoTipo.alTarjetaCreditoTipoClick(Sender: TObject);
begin
    if TarjetasCreditoTipos.FieldByName('CodigoTipoTarjetaCredito').AsInteger = CodigoTipoTarjetaCredito then Exit;

    TarjetasCreditoTipos.DisableControls;
    RefrescarCamposRegistroActual;
end;

procedure TFormTarjetasCreditoTipo.btnAgregarPrefijoClick(Sender: TObject);
Var
	Prefijo: AnsiString;
    volver : Boolean;                                                                               //TASK_119_JMA_20170322
    i: Integer;                                                                                     //TASK_119_JMA_20170322
begin
{INICIO: TASK_119_JMA_20170322
	Prefijo := '';
    if InputQuery( MSG_NUEVO_PREFIJO_CAPTION, MSG_NUEVO_PREFIJO_QUERY, Prefijo) and
      (Prefijo <> '') then begin
        if (Length(Prefijo)<=10) then lb_Prefijos.Items.Add(Uppercase(Prefijo))
        else MsgBox(MSG_ERROR,MSG_CAPTION_VALIDAR_PREFIJO,MB_ICONSTOP);
    end;

}
    if (trim(txt_caracterinicialbanda.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDAR_CARACTER_INICIAL, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txt_caracterinicialbanda);
    	Exit;
    end;

    if (StrToInt(trim(txt_caracterinicialbanda.Text)) > 10) then begin
		MsgBoxBalloon( MSG_VALIDAR_CARACTER_INICIAL2, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txt_caracterinicialbanda);
    	Exit;
    end;
    
    Prefijo := '';
    volver:= True;
    while volver do
    begin
        volver := False;
        if InputQuery( MSG_NUEVO_PREFIJO_CAPTION, MSG_NUEVO_PREFIJO_QUERY, Prefijo) and
          (Prefijo <> '') then begin
            if (Length(Prefijo) <> StrToInt(trim(txt_caracterinicialbanda.Text))) then
            begin
                MsgBox(MSG_ERROR,MSG_CAPTION_VALIDAR_PREFIJO,MB_ICONSTOP);
                volver := True;
            end
            else if not IsInteger(Trim(Prefijo)) then
            begin
                MsgBox(MSG_ERROR2,MSG_CAPTION_VALIDAR_PREFIJO,MB_ICONSTOP);
                volver := True;
            end
            else
            begin
                for I := 0 to lb_prefijos.Count - 1 do
                begin
                   if Trim(Prefijo) = Trim(lb_prefijos.Items[i]) then
                   begin
                        MsgBox(MSG_ERROR3,MSG_CAPTION_VALIDAR_PREFIJO,MB_ICONSTOP);
                        volver := True;
                        Break;
                   end;
                end;
            end;

            if not volver then
            begin
                lb_Prefijos.Items.Add(Uppercase(Prefijo));
                txt_caracterinicialbanda.Enabled := False;
            end;
        end;
    end;

{TERMINO: TASK_119_JMA_20170322}
end;

procedure TFormTarjetasCreditoTipo.btnQuitarPrefijoClick(Sender: TObject);
begin
	if lb_prefijos.itemindex < 0 then begin
		MsgBox( MSG_ERROR_SELECCIONAR_ITEM, MSG_CAPTION_VALIDAR_PREFIJO, MB_ICONSTOP);
	end else begin
		lb_prefijos.items.delete(lb_prefijos.itemindex);
        if lb_prefijos.Items.Count = 0 then                              //TASK_119_JMA_20170322
            txt_caracterinicialbanda.Enabled := True;                    //TASK_119_JMA_20170322
	end;
end;

procedure TFormTarjetasCreditoTipo.RefrescarCamposRegistroActual;
var
i : integer;
begin
	With TarjetasCreditoTipos do begin
        txt_codigo.Value           := FieldByName('CodigoTipoTarjetaCredito').AsInteger;
		txt_descripcion.text  	   := Trim(FieldByName('Descripcion').AsString);
        CodigoTipoTarjetaCredito   := FieldByName('CodigoTipoTarjetaCredito').AsInteger;

        //Datos propios de la Tarjeta
		txt_caracterinicialbanda.value  := FieldByName('CaracterInicialBanda').AsInteger;
		txt_longitudbanda.value         := FieldByName('LongitudBanda').AsInteger;

        cbFamiliaTarjetas.ItemIndex := 0;
        for i := 0 to cbFamiliaTarjetas.Items.Count - 1 do begin
            if FieldByName('CodigoFamiliaTarjetaCredito').AsInteger = cbFamiliaTarjetas.Items.Items[i].Value then break;
            cbFamiliaTarjetas.ItemIndex :=  cbFamiliaTarjetas.ItemIndex + 1;
        end;


		// Prefijos
		qry_prefijos.Parameters.ParamByName('CodigoTipoTarjetaCredito').Value :=  FieldByName('CodigoTipoTarjetaCredito').AsInteger;
		qry_prefijos.Open;
		lb_Prefijos.Clear;
		While not qry_prefijos.Eof do begin
			lb_prefijos.Items.Add(qry_prefijos.FieldByName('Prefijo').AsString);
			qry_prefijos.Next;
		end;
		qry_prefijos.Close;

        cbValidarPrefijo.Checked  := FieldByName('ValidarPrefijo').AsBoolean;
        chkActivo.Checked         := FieldByName('Activo').AsBoolean;
	end;
end;

{INICIO: TASK_119_JMA_20170322}
procedure TFormTarjetasCreditoTipo.txt_caracterinicialbandaKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (Key in ['0'..'9', #8])then
      Key := #0;
end;
{TERMINO: TASK_119_JMA_20170322}
end.
