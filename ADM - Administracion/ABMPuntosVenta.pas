unit ABMPuntosVenta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  Util, UtilProc, OleCtrls, DmiCtrls, ComCtrls, PeaProcs, DMConnection,
  ADODB, variants, DPSControls, BuscaTab;

type
  TFormPuntosVenta = class(TForm)
    AbmToolbar1: TAbmToolbar;
    alPuntoVenta: TAbmList;
    gbPuntoVenta: TPanel;
    Panel2: TPanel;
    txtDescripcion: TEdit;
    Label2: TLabel;
    txt_Telefono: TEdit;
    txt_DireccionIP: TEdit;
    Label1: TLabel;
    Label4: TLabel;
    Notebook: TNotebook;
    ActualizarDatosPuntoVenta: TADOStoredProc;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    btLugarDeVenta: TBuscaTabla;
    cbTipoLugarVenta: TComboBox;
    Label5: TLabel;
    chkActivo: TCheckBox;
    Label6: TLabel;
    qryLugaresDeVentaPorOperador: TADOQuery;
    PuntosVenta: TADOTable;
    EliminarPuntoVenta: TADOStoredProc;
    pnlPuntosVenta: TPanel;
    Label8: TLabel;
    cbOperadorLogistico: TComboBox;
    Label7: TLabel;
    cbLugarDeVenta: TComboBox;
    procedure BtnCancelarClick(Sender: TObject);
    procedure alPuntoVentaDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure alPuntoVentaEdit(Sender: TObject);
    procedure alPuntoVentaRefresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure alPuntoVentaDelete(Sender: TObject);
    procedure alPuntoVentaInsert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure alPuntoVentaClick(Sender: TObject);
    procedure cbOperadorLogisticoChange(Sender: TObject);
    function alPuntoVentaProcess(Tabla: TDataSet;
      var Texto: String): Boolean;
    procedure cbLugarDeVentaChange(Sender: TObject);
  private
	{ Private declarations }
    FCodigoLugarDeVenta: integer;
    FCodigoOperadorLogistico: integer;
    procedure HabilitarCamposEdicion(habilitar : Boolean);
	procedure Limpiar_Campos;
    procedure Volver_Campos;
  public
	{ Public declarations }
    function Inicializar(MDIChild: Boolean = True; CodigoLugarDeVenta: integer = 0; CodigoOperadorLogistico: integer = 0): Boolean;
  end;

var
  FormPuntosVenta: TFormPuntosVenta;

implementation

uses RStrings;

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar este Punto de Venta?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Punto de Venta porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Punto de Venta';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Punto de Venta.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Punto de Venta';


{$R *.DFM}



procedure TFormPuntosVenta.HabilitarCamposEdicion(habilitar: Boolean);
begin
    alPuntoVenta.Enabled    := not Habilitar;
	gbPuntoVenta.Enabled    := Habilitar;
	Notebook.PageIndex 		:= ord(habilitar);
end;

procedure TFormPuntosVenta.Volver_Campos ;
begin
	alPuntoVenta.Estado     := Normal;
    HabilitarCamposEdicion(False);
	Notebook.PageIndex      := 0;
//    alPuntoVenta.SetFocus;
end;

function TFormPuntosVenta.Inicializar(MDIChild: Boolean = True; CodigoLugarDeVenta: integer = 0; CodigoOperadorLogistico: integer = 0): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
   		Visible := False;
	end;
	Result := False;

	if not OpenTables([PuntosVenta, qryLugaresDeVentaPorOperador]) then exit;
    CargarOperadoresLogisticos(DMConnections.BaseCAC, cbOperadorLogistico, CodigoOperadorLogistico);
    CargarTiposPuntoVenta(DMConnections.BaseCAC, cbTipoLugarVenta);

    if CodigoOperadorLogistico > 0 then begin //Es llamado desde Lugares de Venta

        Caption := Format(MSG_CAPTION_AGREGAR_MULTIPLE,[STR_PUNTOS_VENTA, Trim(StrLeft(cbOperadorLogistico.Text, 40)), Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT NombreDeFantasia FROM VW_LugaresDeVenta WHERE CodigoLugarDeVenta = ' + IntToStr(CodigoLugarDeVenta)))]);
        pnlPuntosVenta.Visible          := False;
        FCodigoLugarDeVenta             := CodigoLugarDeVenta;
        FCodigoOperadorLogistico        := CodigoOperadorLogistico;

    end else begin
        pnlPuntosVenta.Visible          := True;

        if cbOperadorLogistico.Items.Count > 0 then begin
            cbOperadorLogistico.ItemIndex := 0;
            FCodigoOperadorLogistico := Ival(StrRight(cbOperadorLogistico.text, 10));
            CargarLugaresDeVenta(DMConnections.BaseCAC, cbLugarDeVenta, FCodigoOperadorLogistico);
        end;

        if cbLugarDeVenta.Items.Count > 0 then begin
            cbLugarDeVenta.ItemIndex := 0;
            FCodigoLugarDeVenta := Ival(StrRight(cbLugarDeVenta.text, 10));
        end;
    end;

    PuntosVenta.Filtered            := True;
    PuntosVenta.Filter              := 'CodigoLugarDeVenta = ' + intToStr(FCodigoLugarDeVenta);

	Volver_Campos;
    alPuntoVenta.Reload;
	Result := True;
end;

procedure TFormPuntosVenta.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFormPuntosVenta.alPuntoVentaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas  do begin
        FillRect(Rect);
        if Tabla.FieldByName('Activo').AsBoolean then begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clWindow
            else
                Sender.Canvas.Font.Color := clWindowText;
        end else begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clYellow
            else
                Sender.Canvas.Font.Color := clRed;
        end;
        With Tabla do begin
            TextOut(Cols[0] + 1, Rect.Top, Trim(Fieldbyname('CodigoPuntoVenta').AsString));
            TextOut(Cols[1], Rect.Top, Fieldbyname('Descripcion').AsString);
            TextOut(Cols[2], Rect.Top, Fieldbyname('DireccionIP').AsString);
            TextOut(Cols[3], Rect.Top, Fieldbyname('Telefono').AsString);
        end;
    end;
end;

procedure TFormPuntosVenta.alPuntoVentaEdit(Sender: TObject);
begin
    HabilitarCamposEdicion(True);
    alPuntoVenta.Estado     := modi;
	txtDescripcion.SetFocus;
end;

procedure TFormPuntosVenta.alPuntoVentaRefresh(Sender: TObject);
begin
	if alPuntoVenta.Empty then Limpiar_Campos;
end;

procedure TFormPuntosVenta.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormPuntosVenta.alPuntoVentaDelete(Sender: TObject);
resourcestring
	MSG_ELIMINAR_PUNTO_VENTA_CAPTION            = 'Eliminar Punto de Venta';
	MSG_ELIMINAR_PUNTO_VENTA_ERROR              = 'No se puede eliminar este Punto de Venta porque hay datos que dependen de �l.';
	MSG_ELIMINAR_PUNTO_VENTA_QUESTION           = '�Est� seguro de querer eliminar este Punto de Venta';
	MSG_ELIMINAR_PUNTO_VENTA_QUESTION_CAPTION   = 'Confirmaci�n...';

begin
	Screen.Cursor := crHourGlass;

    If MsgBox(MSG_ELIMINAR_PUNTO_VENTA_QUESTION, MSG_ELIMINAR_PUNTO_VENTA_CAPTION, MB_YESNO) = IDYES then begin
        try
            EliminarPuntoVenta.Parameters.ParamByName('@CodigoPuntoVenta').Value := PuntosVenta.FieldByName('CodigoPuntoVenta').AsInteger;
            EliminarPuntoVenta.ExecProc;
            EliminarPuntoVenta.Close;
        Except
            On E: exception do begin
                MsgBoxErr(MSG_ELIMINAR_PUNTO_VENTA_ERROR, E.message, MSG_ELIMINAR_PUNTO_VENTA_CAPTION, MB_ICONSTOP);
                EliminarPuntoVenta.Close;
            end;
        end;
    end;
	Volver_Campos;
	Screen.Cursor      := crDefault;
end;

procedure TFormPuntosVenta.alPuntoVentaInsert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCamposEdicion(True);
    alPuntoVenta.Estado     := alta;
    chkActivo.Checked       := True;
	txtDescripcion.SetFocus;
end;

procedure TFormPuntosVenta.BtnAceptarClick(Sender: TObject);
ResourceString
	MSG_ACTUALIZAR_CAPTION		    = 'Actualizar Punto de Venta';
    MSG_ACTUALIZAR_ERROR		    = 'No se pudieron actualizar los datos del Punto de Venta';
    MSG_VALIDAR_CAPTION 		    = 'Validar datos del Punto de Venta';
	MSG_VALIDAR_DESCRIPCION 	    = 'Debe ingresar una Descripci�n para detallar el Punto de Venta';
    MSG_VALIDAR_OPERADOR_LOGISTICO  = 'Debe seleccionar un Operador Log�stico para detallar el Punto de Venta';
    MSG_VALIDAR_LUGAR_DE_VENTA      = 'Debe seleccionar un Lugar de Venta para detallar el Punto de Venta';
var
    FPuntoDeVenta: integer;
    TodoOk: Boolean;
begin

   	if (Trim(txtDescripcion.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDAR_DESCRIPCION, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtDescripcion);
		Exit;
    end;


	//HASTA ACA TODO BIEN
	Screen.Cursor := crHourGlass;
    TodoOk := False;
    try
    	DMConnections.BaseCAC.BeginTrans;
        Try
            if alPuntoVenta.Estado = Alta then begin
                FPuntoDeVenta       := -1;
            end else begin
                FPuntoDeVenta       := PuntosVenta.fieldByName('CodigoPuntoVenta').AsInteger;
            end;

            With ActualizarDatosPuntoVenta.Parameters do begin
                ParamByName('@Descripcion').Value			    := iif(Trim(txtDescripcion.Text)= '', null, Trim(txtDescripcion.Text));
                ParamByName('@Telefono').Value 				    := iif(Trim(txt_Telefono.Text)= '', null, Trim(txt_Telefono.Text));
                ParamByName('@DireccionIP').Value 			    := iif(Trim(txt_DireccionIP.Text)= '', null, Trim(txt_DireccionIP.Text));
				ParamByName('@Activo').Value 				    := chkActivo.Checked;
                ParamByName('@CodigoTipoPuntoVenta').Value      := Trim(StrRight(cbTipoLugarVenta.Text, 10));
                ParamByName('@CodigoLugarDeVenta').Value        := FCodigoLugarDeVenta;
                ParamByName('@CodigoPuntoVenta').Value          := FPuntoDeVenta;
            end;
            ActualizarDatosPuntoVenta.ExecProc;
            ActualizarDatosPuntoVenta.Close;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                ActualizarDatosPuntoVenta.Close;
                Exit;
            end;
        end;

        TodoOk := True;
    finally
    	if TodoOk then begin
        	DMConnections.BaseCAC.CommitTrans;
            Volver_Campos;
            Screen.Cursor := crDefault;
        end else begin
	        DMConnections.BaseCAC.RollbackTrans;
            Screen.Cursor := crDefault;
		end;
    end;

end;

procedure TFormPuntosVenta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormPuntosVenta.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormPuntosVenta.Limpiar_Campos;
begin
	txtdescripcion.clear;
	txt_Telefono.clear;
	txt_DireccionIP.clear;
	cbTipoLugarVenta.itemindex      := 0;
end;


procedure TFormPuntosVenta.alPuntoVentaClick(Sender: TObject);
begin
	With PuntosVenta do begin
		txtdescripcion.text  	   := Trim(FieldByName('Descripcion').AsString);
		txt_Telefono.text  		   := Trim(FieldByName('Telefono').AsString);
		txt_DireccionIP.text  	   := FieldByName('DireccionIP').AsString;
        CargarTiposPuntoVenta(DMConnections.BaseCAC, cbTipoLugarVenta, FieldByName('CodigoTipoPuntoVenta').AsInteger);
        chkActivo.Checked := FieldByName('Activo').AsBoolean;
	end;
end;

procedure TFormPuntosVenta.cbOperadorLogisticoChange(Sender: TObject);
begin
    FCodigoOperadorLogistico := Ival(StrRight(cbOperadorLogistico.text, 10));
    CargarLugaresDeVenta(DMConnections.BaseCAC, cbLugarDeVenta, FCodigoOperadorLogistico);
    if cbLugarDeVenta.Items.Count > 0 then begin
        cbLugarDeVenta.ItemIndex := 0;
        FCodigoLugarDeVenta := Ival(StrRight(cbLugarDeVenta.text, 10));
    end;
    PuntosVenta.Filter := 'CodigoOperadorLogistico = ' + intToStr(FCodigoOperadorLogistico) +
                          ' and CodigoLugarDeVenta = ' + intToStr(FCodigoLugarDeVenta);
    PuntosVenta.Filtered := True;
    alPuntoVenta.Reload;
end;

function TFormPuntosVenta.alPuntoVentaProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
    Result := True;
end;

procedure TFormPuntosVenta.cbLugarDeVentaChange(Sender: TObject);
begin
    FCodigoLugarDeVenta := Ival(StrRight(cbLugarDeVenta.text, 10));
    PuntosVenta.Filter := 'CodigoLugarDeVenta = ' + intToStr(FCodigoLugarDeVenta);
    PuntosVenta.Filtered := True;
    alPuntoVenta.Reload;
end;

end.
