object FABMOrigenDeCasos: TFABMOrigenDeCasos
  Left = 107
  Top = 114
  Caption = 'Mantenimiento de Origen de Casos'
  ClientHeight = 567
  ClientWidth = 809
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupB: TPanel
    Left = 0
    Top = 428
    Width = 809
    Height = 100
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Lcodigo: TLabel
      Left = 8
      Top = 11
      Width = 44
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Ldescripcion: TLabel
      Left = 8
      Top = 60
      Width = 163
      Height = 13
      Caption = '&Descripci'#243'n Origen de Caso:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Ltipo: TLabel
      Left = 8
      Top = 35
      Width = 92
      Height = 13
      Caption = 'Origen de Caso:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtCodigo: TNumericEdit
      Left = 175
      Top = 8
      Width = 83
      Height = 21
      Color = 16444382
      Enabled = False
      TabOrder = 0
    end
    object txtDescripcion: TEdit
      Left = 175
      Top = 60
      Width = 388
      Height = 21
      Color = 16444382
      TabOrder = 2
    end
    object txtNombre: TEdit
      Left = 177
      Top = 33
      Width = 211
      Height = 21
      Color = 16444382
      TabOrder = 1
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 528
    Width = 809
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 612
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 809
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbarClose
  end
  object ListaEntidades: TAbmList
    Left = 0
    Top = 33
    Width = 809
    Height = 395
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'58'#0'C'#243'digo'
      #0'248'#0'Origen de Caso'
      #0'140'#0'Descripci'#243'n Origen de Caso')
    HScrollBar = True
    RefreshTime = 100
    Table = tblOrigenCasos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaEntidadesClick
    OnProcess = ListaEntidadesProcess
    OnDrawItem = ListaEntidadesDrawItem
    OnRefresh = ListaEntidadesRefresh
    OnInsert = ListaEntidadesInsert
    OnDelete = ListaEntidadesDelete
    OnEdit = ListaEntidadesEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object tblOrigenCasos: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'FuentesReclamo'
    Left = 84
    Top = 81
  end
  object spActualizarFuentesReclamo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarFuentesReclamo'
    Parameters = <>
    Left = 220
    Top = 80
  end
  object dsOrigenDeCasos: TDataSource
    DataSet = tblOrigenCasos
    Left = 86
    Top = 136
  end
  object spEliminarFuentesReclamo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarFuentesReclamo'
    Parameters = <>
    Left = 224
    Top = 136
  end
end
