{-----------------------------------------------------------------------------
 Unit Name: ABMTiposDeCasos
 Author:  CFU
 Description: ABM de Tipos de Casos
 	La tabla ese llama TiposOrdenServicio
-----------------------------------------------------------------------------}
unit ABMTiposDeCasos;

interface

uses
  //ABM Tipos de Casos
  DMConnection,
  UtilProc,
  Util,
  RStrings,
  utildb,
  PeaTypes,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants, Spin, VariantComboBox;

type
  TFABMTiposDeCasos = class(TForm)
	GroupB: TPanel;
    PAbajo: TPanel;
	Notebook: TNotebook;
    txtCodigo: TNumericEdit;
    AbmToolbar: TAbmToolbar;
    Lcodigo: TLabel;
    Ldescripcion: TLabel;
    txtDescripcion: TEdit;
    tblTiposDeCasos: TADOTable;
    dsTiposDeCasos: TDataSource;
    spActualizarTiposOrdenServicio: TADOStoredProc;
    spEliminarTiposOrdenServicio: TADOStoredProc;
    ListaTiposDeCasos: TAbmList;
    Ltipo: TLabel;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    cbPrioridad: TVariantComboBox;
    Label1: TLabel;
    seDias: TSpinEdit;
    Label2: TLabel;
    seHoras: TSpinEdit;
    seMinutos: TSpinEdit;
    Label3: TLabel;
    Label4: TLabel;
    procedure FormShow(Sender: TObject);
   	procedure ListaTiposDeCasosRefresh(Sender: TObject);
   	function  ListaTiposDeCasosProcess(Tabla: TDataSet; var Texto: String): Boolean;
   	procedure ListaTiposDeCasosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaTiposDeCasosClick(Sender: TObject);
	procedure ListaTiposDeCasosInsert(Sender: TObject);
	procedure ListaTiposDeCasosEdit(Sender: TObject);
	procedure ListaTiposDeCasosDelete(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure AbmToolbarClose(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FABMTiposDeCasos: TFABMTiposDeCasos;

const
	MINUTOS_POR_DIA		= 1440;
    MINUTOS_POR_HORA	= 60;
    HORAS_POR_DIA		= 24;

resourcestring
	STR_MAESTRO_TiposDeCasos	= 'Maestro de Tipos de Casos';


implementation

{$R *.DFM}

function TFABMTiposDeCasos.Inicializa:Boolean;
Var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblTiposDeCasos]) then 
    	Exit;
	Notebook.PageIndex := 0;
	ListaTiposDeCasos.Reload;
	Result := True;
end;

procedure TFABMTiposDeCasos.LimpiarCampos;
begin
	txtCodigo.Clear;
	txtDescripcion.Clear;
    seDias.Clear;
    seHoras.Clear;
    seMinutos.Clear;
    cbPrioridad.ItemIndex := -1;
end;

procedure TFABMTiposDeCasos.FormShow(Sender: TObject);
begin
	ListaTiposDeCasos.Reload;
end;

procedure TFABMTiposDeCasos.ListaTiposDeCasosRefresh(Sender: TObject);
begin
	 if ListaTiposDeCasos.Empty then 
     	LimpiarCampos;
end;

procedure TFABMTiposDeCasos.Volver_Campos;
begin
	ListaTiposDeCasos.Estado := Normal;
	ListaTiposDeCasos.Enabled:= True;

	ActiveControl       := ListaTiposDeCasos;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;
	ListaTiposDeCasos.Reload;
end;


{-----------------------------------------------------------------------------
  Function Name: ListaTiposDeCasosDrawItem
  Author:
  Date Created:
  Description: Muestro los Registros
  Parameters: Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMTiposDeCasos.ListaTiposDeCasosDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
    TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('TipoOrdenServicio').AsInteger, 10));
    TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
    TextOut(Cols[2], Rect.Top, Tabla.FieldbyName('PriodidadPorDefecto').AsString);
    TextOut(Cols[3], Rect.Top, Tabla.FieldbyName('TiempoResolucion').AsString);
	end;
end;

procedure TFABMTiposDeCasos.ListaTiposDeCasosClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   txtCodigo.Value			:= FieldByName('TipoOrdenServicio').AsInteger;
	   txtDescripcion.Text		:= FieldByName('Descripcion').AsString;
       cbPrioridad.Value		:= FieldByname('PriodidadPorDefecto').Value;
       seDias.Value				:= Trunc(FieldByName('TiempoResolucion').AsInteger/MINUTOS_POR_DIA);
       seHoras.Value			:= Trunc(((FieldByName('TiempoResolucion').AsInteger/MINUTOS_POR_DIA) - seDias.Value) * HORAS_POR_DIA);
       seMinutos.Value			:= Round(((((FieldByName('TiempoResolucion').AsInteger/MINUTOS_POR_DIA) - seDias.Value) * HORAS_POR_DIA)-seHoras.Value) * MINUTOS_POR_HORA);
       //TASK_052_FSI_201703 :Los elementos NO Editables, deben estar bloqueados.
       txtCodigo.Enabled         := FieldbyName('Editable').Value;
       txtDescripcion.Enabled    := FieldbyName('Editable').Value;
       cbPrioridad.Enabled       := FieldbyName('Editable').Value;
       seDias.Enabled            := FieldbyName('Editable').Value;
       seHoras.Enabled           := FieldbyName('Editable').Value;
       seMinutos.Enabled         := FieldbyName('Editable').Value;
       //TASK_052_FSI_201703 : FIN!!Los elementos NO Editables, deben estar bloqueados.
	end;
end;

function TFABMTiposDeCasos.ListaTiposDeCasosProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
	Texto := Tabla.FieldByName('Descripcion').AsString;
	Result := True;
end;

procedure TFABMTiposDeCasos.ListaTiposDeCasosInsert(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled          	:= True;
	ListaTiposDeCasos.Enabled	:= False;
	Notebook.PageIndex      	:= 1;
	ActiveControl           	:= txtDescripcion;
end;

procedure TFABMTiposDeCasos.ListaTiposDeCasosEdit(Sender: TObject);
begin
  if(txtCodigo.Enabled) then begin
      ListaTiposDeCasos.Enabled:= False;
      ListaTiposDeCasos.Estado := modi;

      Notebook.PageIndex := 1;
      groupb.Enabled     := True;

      ActiveControl:= txtDescripcion
  end
  else begin

    MsgBoxErr('No se puede editar dato maestro: '+txtCodigo.Text,txtDescripcion.Text, Format(MSG_CAPTION_EDITAR,[STR_MAESTRO_TiposDeCasos]), MB_ICONSTOP);
    Volver_Campos;
    Screen.Cursor:= crDefault;
    ListaTiposDeCasos.Reload;
    ListaTiposDeCasos.Estado := Normal;
    ListaTiposDeCasos.Enabled:= True;
  end;
end;
//TASK_053_FSI_201703 : se agregan filtro de editar o no.
procedure TFABMTiposDeCasos.ListaTiposDeCasosDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;

  if(txtCodigo.Enabled) then begin
      If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_TiposDeCasos]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        try
                with spEliminarTiposOrdenServicio do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoTipoOrdenServicio').Value := tblTiposDeCasos.FieldbyName('TipoOrdenServicio').Value;
                    ExecProc;
                end;
        Except
          On E: Exception do begin
            MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_TiposDeCasos]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_TiposDeCasos]), MB_ICONSTOP);
          end
        end
      end;
    end
  else begin

    MsgBoxErr('No se puede eliminar dato maestro: '+txtCodigo.Text,txtDescripcion.Text, Format(MSG_CAPTION_EDITAR,[STR_MAESTRO_TiposDeCasos]), MB_ICONSTOP);
  end;

	ListaTiposDeCasos.Reload;
	ListaTiposDeCasos.Estado := Normal;
	ListaTiposDeCasos.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFABMTiposDeCasos.BtnAceptarClick(Sender: TObject);
resourcestring
	MSG_ERROR_TIEMPO_RESOLUCION	= 'El tiempo de resolución debe ser mayor a 0 minutos';
	FLD_PRIORIDAD 				= 'Prioridad';
var
	TiempoResolucion: Integer;
begin
	if not ValidateControls([txtDescripcion, cbPrioridad],
	  [(Trim(txtDescripcion.Text) <> ''), cbPrioridad.ItemIndex > -1],
	  Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_TiposDeCasos]),
	  [Format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION]), Format(MSG_VALIDAR_DEBE_LA,[FLD_PRIORIDAD])]) then begin
		Exit;
	end;

    TiempoResolucion	:= 0;
    if seDias.Value > 0 then
	    TiempoResolucion	:= TiempoResolucion + (seDias.Value * MINUTOS_POR_DIA);

    if seHoras.Value > 0 then
	    TiempoResolucion	:= TiempoResolucion + (seHoras.Value * MINUTOS_POR_HORA);

    if seMinutos.Value > 0 then
	    TiempoResolucion	:= TiempoResolucion + (seMinutos.Value);

    if TiempoResolucion <= 0 then begin
    	MsgBox(MSG_ERROR_TIEMPO_RESOLUCION, STR_MAESTRO_TiposDeCasos, MB_ICONSTOP);
        seDias.SetFocus;
        Exit;
    end;

        
	with ListaTiposDeCasos do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarTiposOrdenServicio, Parameters do begin
                    Parameters.Refresh;
					ParamByName('@CodigoTipoOrdenServicio').Value 	:= txtCodigo.ValueInt;
					ParamByName('@Descripcion').Value 				:= Trim(txtDescripcion.Text);
                    ParamByName('@Prioridad').Value					:= cbPrioridad.Value;
                    ParamByName('@Usuario').Value					:= UsuarioSistema;
                    ParamByName('@TiempoResolucion').Value			:= TiempoResolucion;
					ExecProc;
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_TiposDeCasos]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_TiposDeCasos]), MB_ICONSTOP);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

procedure TFABMTiposDeCasos.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFABMTiposDeCasos.AbmToolbarClose(Sender: TObject);
begin
	Close;
end;

procedure TFABMTiposDeCasos.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFABMTiposDeCasos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;


end.

