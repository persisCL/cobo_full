{-----------------------------------------------------------------------------
 File Name: ActHorario 
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit ActHorario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, validate, TimeEdit, Db, DBTables, UtilDb, Util, UtilProc,
  ExtCtrls, ADODB, DPSControls;

type
  TFormActualizarHorario = class(TForm)
    cb_TiposHorario: TComboBox;
    dt_HoraInicio: TTimeEdit;
    Label1: TLabel;
    Label2: TLabel;
    qry_TiposHorario: TADOQuery;
    Bevel1: TBevel;
    btn_Aceptar: TDPSButton;
    Button2: TDPSButton;
    procedure Button2Click(Sender: TObject);
    procedure cb_TiposHorarioClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Function Inicializa(TipoHorario: AnsiString; Hora: TDateTime): Boolean;
  end;

var
  FormActualizarHorario: TFormActualizarHorario;

implementation

Uses
	DMConnection;

{$R *.DFM}

function TFormActualizarHorario.Inicializa(TipoHorario: AnsiString;
  Hora: TDateTime): Boolean;
var
	i, indice: integer;
begin
	Result := False;
    if not OpenTables([qry_TiposHorario]) then Exit;
    cb_TiposHorario.Items.Clear;
    qry_TiposHorario.First;
    i:= 0;
    indice := 0;
    While not qry_TiposHorario.Eof do begin
    	cb_TiposHorario.Items.Add(
        	PadR(qry_TiposHorario.FieldByName('Descripcion').AsString, 200, ' ') +
			Trim(qry_TiposHorario.FieldByName('TipoHorario').AsString)
        );
        if qry_TiposHorario.FieldByName('TipoHorario').AsString = TipoHorario then indice := i;
    	qry_TiposHorario.Next;
        inc(i);
    end;
    cb_TiposHorario.ItemIndex := indice;
    qry_TiposHorario.Close;
    dt_HoraInicio.Time := Hora;
    Result := True;
end;

procedure TFormActualizarHorario.Button2Click(Sender: TObject);
begin
	Close;
end;

procedure TFormActualizarHorario.cb_TiposHorarioClick(Sender: TObject);
begin
	btn_Aceptar.Enabled := ('00:00' = dt_HoraInicio.Text) or (IsValidTime(dt_HoraInicio.Text));
end;

end.
