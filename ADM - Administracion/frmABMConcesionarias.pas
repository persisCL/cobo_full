{---------------------------------------------------------------------
               frmABMConcesionarias

Author		: mbecerra
Date		: 18-Junio-2010
Description	:	(Ref Fase 2)
            	ABM de Concesionarias y Estacionamientos, con sus entradas y salidas

Revision	: 1
Author		: Nelson Droguett
Date		: 30-Julio-2010
Description	:	(Ref Fase 2 - Carta Certificada)
            1.- Quitar el boton ELIMINAR del abm
            2.- No permitir el ingreso de caracteres reservados en los campos de texto, solo
            	letras, numeros y espacios en blanco.
            3.- Que se incluyan solamente los tipos de conceptos relacionados a conceptosmovimiento
                que se incluyen en Facturas y/o Notas de Cobro. Ordenado Alfabeticamente.
            4.- Si la concesionaria tiene datos, no debe permitir cambiarla de tipoConcesionaria.
            5.- Que valide la ruta de las im�genes, evitando grabar una ruta inexistente
            6.- En las entradas y salidas de estacionamientos no permitir insertar entradas o salidas cuyos nombres cortos  y codigos externos
                ya existan para otras entradas o salidas segun sea el caso.

Revision    : 2
Author      : Nelson Droguett Sierra
Date        : 24-Enero-2011
Description : (Ref.Fase2.SF046)
            1.-Modificar en la pantalla el nombre del campo
                tipo concepto Peaje   x  Concepto Per�odo Actual
                tipo concepto Peaje anterior    x   Concepto Per�odo Anterior
                Ambos campos no son obligatorios, en el caso de tener contenido se deber� validar con
                un campo existente en la tabla cocneptos movimientos en un combobox que mostrar� la
                totalidad de los conceptos que esten activos y que pueden ser incluidos en Notas de Cobro
                o facturas.

              2.-Solapas entrada y salida estacionamientos, solo deben estar habilitadas para
              los tipos concesionaria Estacionamiento.
Firma       : SF-046-NDR-20110124

Firma       : PAR00133-NDR-20110705
Description : Validar que el ingreso de conceptos periodo y periodo anterior son incluyentes, o no ingreso ninguno
                o ingreso ambos

Firma       : SS_1006_NDR_20120926
Description : Agregar BIT InformarFoliosYPagos

Firma       : SS_1006_1015_CQU_20121001
Description : Agrega campo CodigoConceptoAjusteCobroPeaje por lo tanto se debe agregar la l�gica necesaria
              para que se agregue desde este ABM.
              Se corrigen algunos detalles del ABM.

Firma       : SS_660_CQU_20121010
Description : Agrega campo CodigoConceptoInfraBoletoMorosidadCat1, CodigoConceptoInfraBoletoMorosidadCat2,
             CodigoConceptoInfraBoletoMorosidadCat3 y CodigoConceptoInfraBoletoMorosidadCat4
             por lo tanto se debe agregar la l�gica necesaria para que se agregue desde este ABM.

Firma       : SS_660_CQU_20130711
Descripcion : Se agrega el chk que indica si puede inhabilitar para lista amarilla

Autor       :   CQuezadaI
Fecha       :   18 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se cambia de posici�n una validaci�n
----------------------------------------------------------------------}
unit frmABMConcesionarias;

interface

uses
  DMConnection,
  UtilProc,
  UtilDB,
  Util,
  PeaProcs,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, ListBoxEx, DBListEx, StdCtrls, VariantComboBox,
  DB, ADODB, ComCtrls,
  SysUtilsCN, DmiCtrls, PeaProcsCN, ImgList;

type
  TABMConcesionariasForm = class(TForm)
    Panel1: TPanel;
    btnSalir: TSpeedButton;
    btnInsertar: TSpeedButton;
    btnEliminar: TSpeedButton;
    btnModificar: TSpeedButton;
    dblConcesionarias: TDBListEx;
    aqryConcesionarias: TADOQuery;
    pcConcesionarias: TPageControl;
    tsConcesionarias: TTabSheet;
    tsEntradas: TTabSheet;
    tsSalidas: TTabSheet;
    Label1: TLabel;
    edtCodigo: TEdit;
    Label2: TLabel;
    edtDescripcion: TEdit;
    Label6: TLabel;
    edtNombreCorto: TEdit;
    chkFacturacionInterna: TCheckBox;
    chkTarifaUnica: TCheckBox;
    Label5: TLabel;
    edtRuta: TEdit;
    Label7: TLabel;
    vcbConceptoPeriodoActual: TVariantComboBox;        //SF-046-NDR-20110124
    Label8: TLabel;
    vcbConceptoPeriodoAnterior: TVariantComboBox;      //SF-046-NDR-20110124
    Label10: TLabel;
    vcbTipoConcesionaria: TVariantComboBox;
    dsConcesionarias: TDataSource;
    aqryTiposConcesionarias: TADOQuery;
    pnlEntradas: TPanel;
    dblEntradas: TDBListEx;
    Label3: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    chkEntradaActivo: TCheckBox;
    edtEntradaCod: TEdit;
    edtEntradaNombreCorto: TEdit;
    edtEntradaDescripcion: TEdit;
    Panel2: TPanel;
    btnEntraInsertar: TSpeedButton;
    btnEntraEliminar: TSpeedButton;
    btnEntraModificar: TSpeedButton;
    Panel4: TPanel;
    btnSalidaInsertar: TSpeedButton;
    btnSalidaEliminar: TSpeedButton;
    btnSalidaModificar: TSpeedButton;
    dblSalidas: TDBListEx;
    pnlSalidas: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    chkSalidaActivo: TCheckBox;
    edtSalidaCodigo: TEdit;
    edtSalidaNombreCorto: TEdit;
    edtSalidaDescripcion: TEdit;
    Panel3: TPanel;
    nbGrabar: TNotebook;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    aqryEntradas: TADOQuery;
    aqrySalidas: TADOQuery;
    dsEntradas: TDataSource;
    dsSalidas: TDataSource;
    Label16: TLabel;
    edtSalidaCodExterno: TEdit;
    Label17: TLabel;
    edtEntradaCodExterno: TEdit;
    spAgregarEntradaEstacionamiento: TADOStoredProc;
    spAgregarSalidaEstacionamiento: TADOStoredProc;
    spObtenerConceptosComprobante: TADOStoredProc;
    chkFacturaCN: TCheckBox;
    vcbConceptoBoletoInfractor: TVariantComboBox;
    vcbConceptoGastosCobranzaInfractor: TVariantComboBox;
    vcbConceptoInteresesInfractor: TVariantComboBox;
    Label11: TLabel;
    Label15: TLabel;
    Label18: TLabel;
    spObtenerConceptosInfractores: TADOStoredProc;                //SF-046-NDR-20110124
    vcbConceptoInteresesExento: TVariantComboBox;				// SS_1015_PDO_20120524	
    vcbConceptoInteresesAfecto: TVariantComboBox;				// SS_1015_PDO_20120524
    Label19: TLabel;											// SS_1015_PDO_20120524
    Label20: TLabel;											// SS_1015_PDO_20120524
    Label21: TLabel;											// SS_1015_PDO_20120524
    nedtOrdenCalculoIntereses: TNumericEdit;
    chkPermiteReclamo: TCheckBox;     // SS_1006_1015_MCO_20120904
    lnCheck: TImageList;             // SS_1015_PDO_20120524         // SS_1006_1015_MCO_20120904
    chkInformarFoliosYPagos: TCheckBox;        //SS_1006_NDR_20120926
    lblConceptoAjusteCobroPeaje: TLabel;                        // SS_1006_1015_CQU_20121001
    vcbConceptoAjusteCobroPeaje: TVariantComboBox;              // SS_1006_1015_CQU_20121001
    grpCodigoConceptoMorosos: TGroupBox;                        // SS_660_CQU_20121010
    lblConceptoBoletoInfractorMorosidadCat1: TLabel;            // SS_660_CQU_20121010
    lblConceptoBoletoInfractorMorosidadCat2: TLabel;            // SS_660_CQU_20121010
    lblConceptoBoletoInfractorMorosidadCat3: TLabel;            // SS_660_CQU_20121010
    lblConceptoBoletoInfractorMorosidadCat4: TLabel;            // SS_660_CQU_20121010
    vcbConceptoBoletoInfractorMorosidadCat1: TVariantComboBox;  // SS_660_CQU_20121010
    vcbConceptoBoletoInfractorMorosidadCat2: TVariantComboBox;  // SS_660_CQU_20121010
    vcbConceptoBoletoInfractorMorosidadCat3: TVariantComboBox;  // SS_660_CQU_20121010
    vcbConceptoBoletoInfractorMorosidadCat4: TVariantComboBox;
    chkPuedeListaAmarilla: TCheckBox;
    tsDatosBasico: TTabSheet;
    txt_RUT: TEdit;
    lblRUT: TLabel;
    txt_DV: TEdit;
    lblRazonSocial: TLabel;
    txt_RazonSocial: TEdit;
    txt_Giro: TEdit;
    lblGiro: TLabel;
    lblCodigoSII: TLabel;
    txt_CodigoSII: TEdit;
    txt_Direccion: TEdit;
    lblDireccion: TLabel;
    lblPais: TLabel;
    cbbPais: TVariantComboBox;
    cbbRegion: TVariantComboBox;
    cbbComuna: TVariantComboBox;
    lblRegion: TLabel;
    lblComuna: TLabel;
    chkEsNativa: TCheckBox;
    txt_CodigoActividadEconomica: TEdit;
    lblCodigoActividadEconomica: TLabel;  // SS_660_CQU_20121010
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dsConcesionariasDataChange(Sender: TObject; Field: TField);
    procedure btnSalirClick(Sender: TObject);
    procedure btnInsertarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnModificarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure dsEntradasDataChange(Sender: TObject; Field: TField);
    procedure dsSalidasDataChange(Sender: TObject; Field: TField);
    procedure btnEntraInsertarClick(Sender: TObject);
    procedure btnSalidaInsertarClick(Sender: TObject);
    procedure btnEntraEliminarClick(Sender: TObject);
    procedure btnSalidaEliminarClick(Sender: TObject);
    procedure btnEntraModificarClick(Sender: TObject);
    procedure btnSalidaModificarClick(Sender: TObject);
    procedure pcConcesionariasChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure edtDescripcionKeyPress(Sender: TObject; var Key: Char);
    procedure edtRutaKeyPress(Sender: TObject; var Key: Char);
    procedure dblConcesionariasDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure cbbRegionChange(Sender: TObject);          // SS_1006_1015_MCO_20120904
  private
    { Private declarations }
    //FTipoConceptoPeajePeriodo,                                   //SF-046-NDR-20110124
    //FTipoConceptoPeajePeriodoAnterior,                           //SF-046-NDR-20110124
    FTipoConcesionariaAutopista,
    FTipoConcesionariaEstacionamiento : integer;
    FEstadoGrilla, FTipoCambio,
    FEstadoEntrada, FEstadoSalida : integer;
    FConcesionariaEsEstacionamiento : boolean;
{INICIO:  TASK_008_JMA_20160429_Unificaci�n tablas datos Concesionaria}
    procedure CargarPaises;
    procedure CargarRegiones(CodigoPais: string);
    procedure CargarComunas(CodigoRegion: string);
{TERMINO:  TASK_008_JMA_20160429_Unificaci�n tablas datos Concesionaria}
  public
    { Public declarations }
    function Inicializar : boolean;
    procedure MostrarDatosConcesionaria;
    procedure MostrarDatosEntradas;
    procedure MostrarDatosSalidas;
    //Rev.2 / 24-Enero-2011 / Nelson Droguett Sierra----------------------------
    //procedure CargarTipoConceptoPeajes;                  //SF-046-NDR-20110124
    procedure CargarConceptosPeajes;                       //SF-046-NDR-20110124
    //FinRev.2------------------------------------------------------------------
    procedure CargarTiposConcesionarias;                 //SF-046-NDR-20110124

    procedure MostrarBotones(DeCualTab : byte);
    procedure LimpiarCampos(DeCualTab : byte);

    function ValidarDatosConcesionaria : boolean;
    function ValidarDatosEntradas : boolean;
    function ValidarDatosSalidas : boolean;
    function GrabarDatosConcesionaria : boolean;
    function GrabarDatosEntrada : boolean;
    function GrabarDatosSalida : boolean;

    procedure CargarConceptosInfractores;
    procedure RefrescarQueryConcesionaria;

  end;

var
  ABMConcesionariasForm: TABMConcesionariasForm;

implementation

{$R *.dfm}

const
	//estado de edici�n
    cEstadoBrowse = 1;
    cEstadoInsert = 2;
    cEstadoModify = 3;

    //a qu� objeto afecta la edici�n
    cEsConcesionaria = 1;
    cEsEntradaEstacionamiento = 2;
    cEsSalidaEstacionamiento = 3;

{--------------------------------------------------------
        Inicializar

Author		: mbecerra
Date		: 21-Junio-2010
Description	:		(Ref Fase 2)
            	Inicializa el formulario de Concesionarias

--------------------------------------------------------------------}
function TABMConcesionariasForm.Inicializar;
resourcestring
    //SQL_TIPO_PEAJE		= 'SELECT dbo.CONST_TIPO_CONCEPTO_PEAJES_PERIODO()';                //SF-046-NDR-20110124
	  //SQL_TIPO_PEAJE_ANT	= 'SELECT dbo.CONST_TIPO_CONCEPTO_PEAJES_PERIODO_ANTERIOR()';     //SF-046-NDR-20110124
    SQL_TIPO_CONCE_AUT	= 'SELECT dbo.CONST_TIPO_CONCESIONARIA_AUTOPISTA()';
    SQL_TIPO_CONCE_EST	= 'SELECT dbo.CONST_TIPO_CONCESIONARIA_ESTACIONAMIENTO()';

    MSG_ERROR_INI		= 'Ocurri� un error al inicializar el ABM';
    MSG_CAPTION			= 'ABM Concesionarias';

var
	S: TSize;
begin
	Result := True;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
  Caption := MSG_CAPTION;

  try
    tsEntradas.TabVisible := False;
    tsSalidas.TabVisible  := False;

  	aqryConcesionarias.Open;

  	//Cargar los Tipos
    //FTipoConceptoPeajePeriodo			:= QueryGetValueInt(DMConnections.BaseCAC, SQL_TIPO_PEAJE);         //SF-046-NDR-20110124
  	//FTipoConceptoPeajePeriodoAnterior	:= QueryGetValueInt(DMConnections.BaseCAC, SQL_TIPO_PEAJE_ANT); //SF-046-NDR-20110124
    FTipoConcesionariaAutopista			:= QueryGetValueInt(DMConnections.BaseCAC, SQL_TIPO_CONCE_AUT);
    FTipoConcesionariaEstacionamiento	:= QueryGetValueInt(DMConnections.BaseCAC, SQL_TIPO_CONCE_EST);

    //Comboboxes
    //Rev.2 / 24-Enero-2011 / Nelson Droguett Sierra----------------------------
    //CargarTipoConceptoPeajes();
    //vcbTipoPeajePeriodoAnterior.Clear;
    //vcbTipoPeajePeriodoAnterior.Items.Assign(vcbTipoPeajePeriodo.Items);
    //FinRev.2-------------------------------------------------------------------

    CargarTiposConcesionarias();
    CargarConceptosInfractores;
   
{INICIO: TASK_008_JMA_20160429}
    CargarPaises();
    CargarRegiones(cbbPais.Value);
{TERMINO: TASK_008_JMA_20160429}

    //indicar que estamos en Browse
    FEstadoGrilla := cEstadoBrowse;
    FEstadoEntrada := cEstadoBrowse;
    FEstadoSalida := cEstadoBrowse;

    FConcesionariaEsEstacionamiento := False;
    dsConcesionariasDataChange(nil, nil);
    pcConcesionarias.ActivePage := tsConcesionarias;
    MostrarBotones(cEsConcesionaria);



  except on e:exception do begin
    MsgBoxErr( MSG_ERROR_INI, e.Message, Caption, MB_ICONERROR );
    Result := False;
  end;

  end;

end;

{--------------------------------------------------------
        MostrarDatosConcesionaria

Author		: mbecerra
Date		: 21-Junio-2010
Description	:		(Ref Fase 2)
            	Despliega los datos asociados al registro de la concesionaria
                que se est� visualizando

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.MostrarDatosConcesionaria;
begin
    with aqryConcesionarias do begin
        if not Eof then begin
            edtCodigo.Text			:= FieldByName('CodigoConcesionaria').AsString;
            edtDescripcion.Text		:= Trim(FieldByName('Descripcion').AsString);
            edtNombreCorto.Text		:= Trim(FieldByName('NombreCorto').AsString);
            edtRuta.Text			:= Trim(FieldByName('PathImagenes').AsString);
            chkFacturaCN.Checked    := FieldByName('TransitosLosfacturaCN').AsBoolean;
            chkPermiteReclamo.Checked    := FieldByName('PermiteReclamos').AsBoolean; // SS_1006_1015_MCO_20120904
            chkInformarFoliosYPagos.Checked    := FieldByName('InformarFoliosYPagos').AsBoolean; // SS_1006_NDR_20120926

            nedtOrdenCalculoIntereses.Value := FieldByName('OrdenCalculoIntereses').AsInteger;		// SS_1015_PDO_20120524
{INICIO: TASK_008_JMA_20160429}
            txt_RUT.Text :=  FieldByName('RUT').AsString;
            txt_DV.Text :=  FieldByName('DigitoVerificador').AsString;
            txt_RazonSocial.Text := FieldByName('RazonSocial').AsString;
            txt_Giro.Text := FieldByName('Giro').AsString;
            txt_CodigoActividadEconomica.Text := FieldByName('CodigoActividadEconomica').AsString;
            txt_CodigoSII.Text := FieldByName('CodigoSIISucursal').AsString;
            txt_Direccion.Text := FieldByName('Direccion').AsString;
            if FieldByName('CodigoRegion').AsString <> '' then
                cbbRegion.ItemIndex := cbbRegion.Items.IndexOfValue(FieldByName('CodigoRegion').AsString)
            else if cbbRegion.Items.Count > 0 then
                cbbRegion.ItemIndex := 0;
            CargarComunas(cbbRegion.Value);
            if FieldByName('CodigoComuna').AsString <> '' then
                cbbComuna.ItemIndex := cbbComuna.Items.IndexOfValue(FieldByName('CodigoComuna').AsString)
            else if cbbComuna.Items.Count > 0 then
                cbbComuna.ItemIndex := 0;
            chkEsNativa.Checked := False;
            if FieldByName('EsConcesionariaNativa').AsBoolean <> null then
               chkEsNativa.Checked := FieldByName('EsConcesionariaNativa').AsBoolean;
{TERMINO: TASK_008_JMA_20160429}

            //Rev.2 / 24-Enero-2010 / Nelson Droguett Sierra -------------------
        	  //vcbTipoPeajePeriodo.ItemIndex			    := vcbTipoPeajePeriodo.Items.IndexOfValue(FieldByName('CodigoTipoConceptoPeajePeriodo').Value);                 //SF-046-NDR-20110124
            //vcbTipoPeajePeriodoAnterior.ItemIndex	:= vcbTipoPeajePeriodoAnterior.Items.IndexOfValue(FieldByName('CodigoTipoConceptoPeajePeriodoAnterior').Value); //SF-046-NDR-20110124
            CargarConceptosPeajes();                                                                                                                                //SF-046-NDR-20110124

            vcbConceptoPeriodoAnterior.Items.Assign(vcbConceptoPeriodoActual.Items);                                                                                //SF-046-NDR-20110124

            vcbConceptoInteresesExento.Items.Assign(vcbConceptoPeriodoActual.Items);																				// SS_1015_PDO_20120524
            vcbConceptoInteresesAfecto.Items.Assign(vcbConceptoPeriodoActual.Items);																				// SS_1015_PDO_20120524
            vcbConceptoAjusteCobroPeaje.Items.Assign(vcbConceptoPeriodoAnterior.Items);                                                                             // SS_1006_1015_CQU_20121001

            vcbConceptoPeriodoActual.ItemIndex  := vcbConceptoPeriodoActual.Items.IndexOfValue(FieldByName('CodigoConceptoPeriodo').Value);                         //SF-046-NDR-20110124
            vcbConceptoPeriodoAnterior.ItemIndex  := vcbConceptoPeriodoAnterior.Items.IndexOfValue(FieldByName('CodigoConceptoPeriodoAnterior').Value);             //SF-046-NDR-20110124

            vcbConceptoInteresesExento.ItemIndex := vcbConceptoInteresesExento.Items.IndexOfValue(FieldByName('CodigoConceptoInteresExento').Value);				// SS_1015_PDO_20120524
            vcbConceptoInteresesAfecto.ItemIndex := vcbConceptoInteresesAfecto.Items.IndexOfValue(FieldByName('CodigoConceptoInteresAfecto').Value);				// SS_1015_PDO_20120524
            vcbConceptoAjusteCobroPeaje.ItemIndex := vcbConceptoAjusteCobroPeaje.Items.IndexOfValue(FieldByName('CodigoAjusteCobroPeaje').Value);                   // SS_1006_1015_CQU_20121001
            //FinRev.2----------------------------------------------------------
        	  vcbTipoConcesionaria.ItemIndex			  := vcbTipoConcesionaria.Items.IndexOfValue(FieldByName('CodigoTipoConcesionaria').Value);
            chkFacturacionInterna.Checked	:= FieldByName('FacturacionInterna').AsBoolean;
            chkTarifaUnica.Checked			:= FieldByName('TarifaUnica').AsBoolean;
            FConcesionariaEsEstacionamiento := (FieldByName('CodigoTipoConcesionaria').AsInteger = FTipoConcesionariaEstacionamiento);
            //Rev.2 / 24-Enero-2010 / Nelson Droguett Sierra -------------------
            if NOT(FConcesionariaEsEstacionamiento) and                         //SF-046-NDR-20110124
              (pcConcesionarias.ActivePage<>tsConcesionarias) then              //SF-046-NDR-20110124
                  pcConcesionarias.ActivePage:=tsConcesionarias;                //SF-046-NDR-20110124
            tsEntradas.TabVisible := FConcesionariaEsEstacionamiento;              //SF-046-NDR-20110124
            tsSalidas.TabVisible := FConcesionariaEsEstacionamiento;               //SF-046-NDR-20110124
            //FinRev.2----------------------------------------------------------
            CargarConceptosInfractores;												// SS_1015_PDO_20120524
            vcbConceptoBoletoInfractor.ItemIndex          := vcbConceptoBoletoInfractor.Items.IndexOfValue(FieldByName('CodigoConceptoInfraBoletoPrejudicial').Value);
            vcbConceptoGastosCobranzaInfractor.ItemIndex  := vcbConceptoGastosCobranzaInfractor.Items.IndexOfValue(FieldByName('CodigoConceptoInfraGastosCobranza').Value);
            vcbConceptoInteresesInfractor.ItemIndex       := vcbConceptoInteresesInfractor.Items.IndexOfValue(FieldByName('CodigoConceptoInfraIntereses').Value);
            vcbConceptoBoletoInfractorMorosidadCat1.ItemIndex := vcbConceptoBoletoInfractorMorosidadCat1.Items.IndexOfValue(FieldByName('CodigoConceptoInfraBoletoMorosidadCat1').Value);   // SS_660_CQU_20121010
            vcbConceptoBoletoInfractorMorosidadCat2.ItemIndex := vcbConceptoBoletoInfractorMorosidadCat2.Items.IndexOfValue(FieldByName('CodigoConceptoInfraBoletoMorosidadCat2').Value);   // SS_660_CQU_20121010
            vcbConceptoBoletoInfractorMorosidadCat3.ItemIndex := vcbConceptoBoletoInfractorMorosidadCat3.Items.IndexOfValue(FieldByName('CodigoConceptoInfraBoletoMorosidadCat3').Value);   // SS_660_CQU_20121010
            vcbConceptoBoletoInfractorMorosidadCat4.ItemIndex := vcbConceptoBoletoInfractorMorosidadCat4.Items.IndexOfValue(FieldByName('CodigoConceptoInfraBoletoMorosidadCat4').Value);   // SS_660_CQU_20121010

            chkPuedeListaAmarilla.Checked := FieldByName('PermiteInhabilitarListaAmarilla').AsBoolean;  // SS_660_CQU_20130711
        end;
    end;

end;


{--------------------------------------------------------
        MostrarDatosEntradas

Author		: mbecerra
Date		: 23-Junio-2010
Description	:		(Ref Fase 2)
            	Despliega los datos correspondientes a las Entradas
                asociados a las concesionarias de tipo Estacionamiento

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.MostrarDatosEntradas;
begin
	aqryEntradas.Close;
    LimpiarCampos(cEsEntradaEstacionamiento);
    if FConcesionariaEsEstacionamiento then begin
        aqryEntradas.Open;
        dsEntradasDataChange(nil, nil);
    end;

end;


{--------------------------------------------------------
        MostrarDatosSalidas

Author		: mbecerra
Date		: 23-Junio-2010
Description	:		(Ref Fase 2)
            	Despliega los datos correspondientes a las Salidas
                asociados a las concesionarias de tipo Estacionamiento

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.MostrarDatosSalidas;
begin
	aqrySalidas.Close;
    LimpiarCampos(cEsSalidaEstacionamiento);
    if FConcesionariaEsEstacionamiento then begin
        aqrySalidas.Open;
        dsSalidasDataChange(nil, nil);
    end;

end;

procedure TABMConcesionariasForm.pcConcesionariasChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := (	(FEstadoGrilla = cEstadoBrowse) and
    					(FEstadoEntrada = cEstadoBrowse) and
                        (FEstadoSalida = cEstadoBrowse) );

end;

{--------------------------------------------------------
        dblConcesionariasDrawText

Author		: mcabello
Date		: 04-Septiembre-2012
Description	:
            	Visualiza en la Grilla el campo PermiteReclamos de forma de Checkbox
  SS-1006-1015-MCO-20120904

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.dblConcesionariasDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
  with Sender do begin

      if (Column.FieldName = 'PermiteReclamos') then begin
          Text := '';
          Canvas.FillRect(Rect);
          bmp := TBitMap.Create;
          DefaultDraw := False;
          try
              if (aqryConcesionarias.FieldByName('PermiteReclamos').AsBoolean ) then begin
                  lnCheck.GetBitmap(0, Bmp);
              end
              else begin
                  lnCheck.GetBitmap(1, Bmp);
              end;

              Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
          finally
              bmp.Free;
          end; // finally

      end;
                                         
      if (Column.FieldName = 'InformarFoliosYPagos') then begin                                 // SS_1006_1015_CQU_20121001
          Text := '';                                                                           // SS_1006_1015_CQU_20121001
          Canvas.FillRect(Rect);                                                                // SS_1006_1015_CQU_20121001
          bmp := TBitMap.Create;                                                                // SS_1006_1015_CQU_20121001
          DefaultDraw := False;                                                                 // SS_1006_1015_CQU_20121001
          try                                                                                   // SS_1006_1015_CQU_20121001
              if (aqryConcesionarias.FieldByName('InformarFoliosYPagos').AsBoolean ) then begin // SS_1006_1015_CQU_20121001
                  lnCheck.GetBitmap(0, Bmp);                                                    // SS_1006_1015_CQU_20121001
              end                                                                               // SS_1006_1015_CQU_20121001
              else begin                                                                        // SS_1006_1015_CQU_20121001
                  lnCheck.GetBitmap(1, Bmp);                                                    // SS_1006_1015_CQU_20121001
              end;                                                                              // SS_1006_1015_CQU_20121001
                                                                                                // SS_1006_1015_CQU_20121001
              Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);           // SS_1006_1015_CQU_20121001
          finally                                                                               // SS_1006_1015_CQU_20121001
              bmp.Free;                                                                         // SS_1006_1015_CQU_20121001
          end; // finally                                                                       // SS_1006_1015_CQU_20121001
                                                                                                // SS_1006_1015_CQU_20121001
      end;
  end;
end;


{--------------------------------------------------------
        dsConcesionariasDataChange

Author		: mbecerra
Date		: 21-Junio-2010
Description	:		(Ref Fase 2)
            	Despliega los datos asociados al registro que se
                est� visualizando

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.dsConcesionariasDataChange(Sender: TObject;
  Field: TField);
begin
    Try
        CambiarEstadoCursor(CURSOR_RELOJ);

        if (Field = nil) and (FEstadoGrilla = cEstadoBrowse) then begin
            MostrarDatosConcesionaria();
            MostrarDatosEntradas();
            MostrarDatosSalidas();
        end;
    Finally
        CambiarEstadoCursor(CURSOR_DEFECTO);
    End;
end;


{--------------------------------------------------------
        dsEntradasDataChange

Author		: mbecerra
Date		: 23-Junio-2010
Description	:		(Ref Fase 2)
            	Despliega los datos asociados al registro de Entrada
                de Estacionamientos, que se est� visualizando

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.dsEntradasDataChange(Sender: TObject;
  Field: TField);
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);

        if (Field = nil) and (FEstadoEntrada = cEstadoBrowse) and (not aqryEntradas.Eof) then begin
            edtEntradaCod.Text			:= aqryEntradas.FieldByName('CodigoEntradaEstacionamiento').AsString;
            edtEntradaNombreCorto.Text	:= aqryEntradas.FieldByName('NombreCorto').AsString;
            edtEntradaDescripcion.Text	:= aqryEntradas.FieldByName('Descripcion').AsString;
            edtEntradaCodExterno.Text	:= aqryEntradas.FieldByName('CodigoExterno').AsString;
        end;
    finally
        pnlEntradas.Enabled := (FEstadoEntrada in [cEstadoInsert, cEstadoModify]);
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

{--------------------------------------------------------
        dsSalidasDataChange

Author		: mbecerra
Date		: 23-Junio-2010
Description	:		(Ref Fase 2)
            	Despliega los datos asociados al registro de Salida
                de Estacionamientos, que se est� visualizando

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.dsSalidasDataChange(Sender: TObject;
  Field: TField);
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);

        if (Field = nil) and (FEstadoSalida = cEstadoBrowse) and (not aqrySalidas.Eof) then begin
            edtSalidaCodigo.Text		:= aqrySalidas.FieldByName('CodigoSalidaEstacionamiento').AsString;
            edtSalidaNombreCorto.Text	:= aqrySalidas.FieldByName('NombreCorto').AsString;
            edtSalidaDescripcion.Text	:= aqrySalidas.FieldByName('Descripcion').AsString;
            edtSalidaCodExterno.Text	:= aqrySalidas.FieldByName('CodigoExterno').AsString;
        end;
    finally
        pnlSalidas.Enabled := (FEstadoSalida in [cEstadoInsert, cEstadoModify]);
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

//Rev.1 / 30-Julio-2010 / Nelson Droguett Sierra -------------------------------------------------
procedure TABMConcesionariasForm.edtDescripcionKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in ['0'..'9','A'..'Z','a'..'z',' ',#8, #13]) then
    Key := #0;
end;

procedure TABMConcesionariasForm.edtRutaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in ['0'..'9','A'..'Z','a'..'z',' ','\','/','-','_',':',#8, #13]) then
    Key := #0;
end;
//FinRev.1----------------------------------------------------------------------------------------

{--------------------------------------------------------
        FormClose

Author		: mbecerra
Date		: 21-Junio-2010
Description	:		(Ref Fase 2)
            	Fuerza el cierre en el formulario de tipo Child

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

{--------------------------------------------------------
        ValidarDatosConcesionaria

Author		: mbecerra
Date		: 24-Junio-2010
Description	:		(Ref Fase 2)
                Valida que est�n todos los datos obligatorios para grabar
                una concesionaria

                Asimismo, si es edici�n, valida que no pueda cambiar de tipo
                de concesionaria si existen registros que lo impiden

--------------------------------------------------------------------}
function TABMConcesionariasForm.ValidarDatosConcesionaria;

resourcestring
	MSG_VALIDA_DESC			= 'No ha ingresado la Descripci�n de la Concesionaria';
    MSG_VALIDA_NOMB			= 'No ha ingresado el NombreCorto de la Concesionaria';
    MSG_VALIDA_PPER			= 'No ha indicado el Tipo de Concepto correspondiente al Tipo Peajes de Per�odo';
    MSG_VALIDA_PPAN			= 'No ha indicado el Tipo de Concepto correspondiente al Tipo Peajes Per�odo Anterior';
    MSG_VALIDA_TCON			= 'No ha indicado el Tipo de Concesionaria';
    MSG_VALIDA_CACP         = 'No ha indicado el Tipo de Concepto correspondiente al Ajuste de Cobro de Peaje'; // SS_1006_1015_CQU_20121001
    MSG_NO_PUEDE			= 'No puede cambiar de Tipo de Concesionaria, pues hay datos relacionados';
    MSG_VALIDA_RUT  		= 'Rut ingresado no es v�lido';  //TASK_008_RME_20160516
    //Rev.1 / 30-Julio-2010 / Nelson Droguett Sierra
    MSG_NOMBRE_CORTO_EXISTE = 'El Nombre Corto ingresado ya existe para otra Concesionaria';
    MSG_ERROR_DIRECTORIO    = 'No se pudo acceder al directorio de im�genes de la concesionaria';
    MSG_ERROR_ORDEN_CALCULO = 'Debe indicar un N�mero de Orden mayor que cero para el C�lculo de Intereses';     							// SS_1015_PDO_20120524
    MSG_ERROR_ORDEN_CALCULO_YA_ASIGNADO = 'El Orden para el C�lculo de Intereses ya est� asignado a la concesionaria %s';	// SS_1015_PDO_20120524

var
	NuevoTipoConcesionaria : integer;
	intConcesionaria:integer;
    ConcesionariaValidacion,					// SS_1015_PDO_20120524
    ErrorConcesionariaValidacion: String;		// SS_1015_PDO_20120524
    
	// SS_1015_PDO_20120524 Inicio Bloque
    function ValidarOrdenCalculoIntereses: string;
        const
            cSQL_ValidarConcesionariaOrdenCalculoIntereses = 'SELECT dbo.ValidarConcesionariaOrdenCalculoIntereses(%s, %d)';
            MSG_ERROR_VALIDACION   = 'Se produjo un error al Validar el Orden de C�lculo de Intereses. Error: %s';
            TITLE_ERROR_VALIDACION = 'Validaci�n Orden C�lculo Intereses';
    begin
        try
            ConcesionariaValidacion := QueryGetValue(DMConnections.BaseCAC, Format(cSQL_ValidarConcesionariaOrdenCalculoIntereses, [edtCodigo.Text, nedtOrdenCalculoIntereses.ValueInt]));
            if Trim(ConcesionariaValidacion) = EmptyStr then begin
                Result := EmptyStr;
            end
            else begin
                Result := Format(MSG_ERROR_ORDEN_CALCULO_YA_ASIGNADO, [Trim(ConcesionariaValidacion)]);
            end;
        except
            on e:Exception do begin
                ShowMsgBoxCN(TITLE_ERROR_VALIDACION, Format(MSG_ERROR_VALIDACION, [e.Message]), MB_ICONERROR, Application.MainForm);
            end;
        end;
    end;

	// SS_1015_PDO_20120524 Fin Bloque

    
begin
	//Validar que est�n todos los datos
    Result := False;
    //ErrorConcesionariaValidacion := ValidarOrdenCalculoIntereses;									 // SS_1147_CQU_20140714    // SS_1015_PDO_20120524

    if ValidateControls(                                                                             //PAR00133-NDR-20110705
    					[	edtDescripcion,                                                          //PAR00133-NDR-20110705
    						edtNombreCorto,                                                          //PAR00133-NDR-20110705
                            vcbTipoConcesionaria,                                                    //PAR00133-NDR-20110705
                            nedtOrdenCalculoIntereses,												 // SS_1015_PDO_20120524
                            nedtOrdenCalculoIntereses												 // SS_1015_PDO_20120524
    					],                                                                           //PAR00133-NDR-20110705
                        [	edtDescripcion.Text <> '',                                               //PAR00133-NDR-20110705
                            edtNombreCorto.Text <> '',                                               //PAR00133-NDR-20110705
                            vcbTipoConcesionaria.ItemIndex >= 0,                                     //PAR00133-NDR-20110705
                            nedtOrdenCalculoIntereses.Value > 0,									 // SS_1015_PDO_20120524
                            ErrorConcesionariaValidacion = EmptyStr									 // SS_1015_PDO_20120524
                        ],                                                                           //PAR00133-NDR-20110705
                        Caption,                                                                     //PAR00133-NDR-20110705
                        [	MSG_VALIDA_DESC,                                                         //PAR00133-NDR-20110705
              	            MSG_VALIDA_NOMB,                                                         //PAR00133-NDR-20110705
                            MSG_VALIDA_TCON,                                                         //PAR00133-NDR-20110705
                            MSG_ERROR_ORDEN_CALCULO,												 // SS_1015_PDO_20120524
                            ErrorConcesionariaValidacion											 // SS_1015_PDO_20120524
                        ]                                                                            //PAR00133-NDR-20110705
    				) then begin                                                                     //PAR00133-NDR-20110705

{INICIO: TASK_008_JMA_20160429}
{INICIO: TASK_007_RME_20160512}

            if ((Length(Trim(txt_RUT.Text)) > 0) or (Length(txt_DV.Text) > 0))
                        and not ValidarRUT(DMConnections.BaseCAC, Trim(txt_RUT.Text) + txt_DV.text) then
            begin
                pcConcesionarias.ActivePage := tsDatosBasico;
                MsgBoxBalloon(MSG_VALIDA_RUT, Caption, MB_ICONWARNING, txt_Rut);    // TASK_008_RME_20160516
                Result:=False;
                Exit;
            end;
{TERMINO: TASK_007_RME_20160512}
{TERMINO: TASK_008_JMA_20160429}

        ErrorConcesionariaValidacion := ValidarOrdenCalculoIntereses;								 // SS_1147_CQU_20140714

        if vcbConceptoPeriodoActual.ItemIndex < 0 then                                               //PAR00133-NDR-20110705
            if vcbConceptoPeriodoAnterior.ItemIndex >=0 then                                         //PAR00133-NDR-20110705
            begin                                                                                    //PAR00133-NDR-20110705
            	MsgBoxBalloon(MSG_VALIDA_PPER, Caption, MB_ICONWARNING, vcbConceptoPeriodoActual);   //PAR00133-NDR-20110705
                Result:=False;                                                                       //PAR00133-NDR-20110705
                Exit;                                                                                //PAR00133-NDR-20110705
            end;                                                                                     //PAR00133-NDR-20110705
        if vcbConceptoPeriodoAnterior.ItemIndex <0 then                                              //PAR00133-NDR-20110705
            if vcbConceptoPeriodoActual.ItemIndex >=0 then                                           //PAR00133-NDR-20110705
            begin                                                                                    //PAR00133-NDR-20110705
            	MsgBoxBalloon(MSG_VALIDA_PPAN, Caption, MB_ICONWARNING, vcbConceptoPeriodoAnterior); //PAR00133-NDR-20110705
                Result:=False;                                                                       //PAR00133-NDR-20110705
                Exit;                                                                                //PAR00133-NDR-20110705
            end;                                                                                     //PAR00133-NDR-20110705
        if (vcbConceptoPeriodoAnterior.ItemIndex >=0) or (vcbConceptoPeriodoActual.ItemIndex >=0) then begin
            if vcbConceptoAjusteCobroPeaje.ItemIndex < 0 then begin                                      // SS_1006_1015_CQU_20121001
                MsgBoxBalloon(MSG_VALIDA_CACP, Caption, MB_ICONWARNING, vcbConceptoAjusteCobroPeaje);    // SS_1006_1015_CQU_20121001
                Result := False;                                                                         // SS_1006_1015_CQU_20121001
                Exit;                                                                                    // SS_1006_1015_CQU_20121001
            end;                                                                                         // SS_1006_1015_CQU_20121001
        end;

    	//ver si ha cambiado de tipo (de Estacionamiento a Autopista)                                
        NuevoTipoConcesionaria := vcbTipoConcesionaria.Value;                                        
        //Rev.1 / 30-Julio-2010 / Nelson Droguett Sierra -----------------------------------------
        intConcesionaria := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerConcesionariaPorNombreCorto(''' + Trim(edtNombreCorto.Text) + ''')');
        if (aqryConcesionarias.FieldByName('CodigoTipoConcesionaria').AsInteger <> NuevoTipoConcesionaria)
        	and (NuevoTipoConcesionaria = FTipoConcesionariaAutopista)
            and ((not aqryEntradas.IsEmpty) or (not aqrySalidas.IsEmpty)) then begin
        	MsgBoxBalloon(MSG_NO_PUEDE, Caption, MB_ICONWARNING, vcbTipoConcesionaria);
        end
        //Rev.1 / 30-Julio-2010 / Nelson Droguett Sierra -----------------------------------------
        //Revisar si el nombre corto existe para otra concesionaria en el caso de ser una
        //concesionaria nueva
        else if ((FEstadoGrilla = cEstadoInsert) AND ( intConcesionaria > 0)) then begin
        	MsgBoxBalloon(MSG_NOMBRE_CORTO_EXISTE, Caption, MB_ICONWARNING, edtNombreCorto);
        end
        else if ((FEstadoGrilla = cEstadoModify) AND (( intConcesionaria  > 0 ) AND ( intConcesionaria <> StrToInt(Trim(edtCodigo.Text))))) then begin
        	 MsgBoxBalloon(MSG_NOMBRE_CORTO_EXISTE, Caption, MB_ICONWARNING, edtNombreCorto);
        end
        //Verificar la ruta de las imagenes.
        else if (edtRuta.Text <> '') and (not DirectoryExists(Trim(edtRuta.Text))) then begin
        	 MsgBoxBalloon(MSG_ERROR_DIRECTORIO, Caption, MB_ICONWARNING, edtRuta);
        end
        else
	        Result := True;
        //FinRev.1--------------------------------------------------------------------------------
    end;
end;


{--------------------------------------------------------
        ValidarDatosEntradas

Author		: mbecerra
Date		: 24-Junio-2010
Description	:		(Ref Fase 2)
                Valida que est�n todos los datos obligatorios para grabar
                una entrada de estacionamiento

--------------------------------------------------------------------}
function TABMConcesionariasForm.ValidarDatosEntradas;
resourcestring
	MSG_VALIDA_DESC			= 'No ha ingresado la Descripci�n de la Entrada';
    MSG_VALIDA_NOMB			= 'No ha ingresado el NombreCorto de la Entrada';
    MSG_COD_EXTERNO			= 'No ha ingresado el C�digo Externo de la Entrada';
    MSG_NO_ACTIVO			= '�Est� seguro de que desea dejar esta entrada como INACTIVA?';
    //Rev.1 / 04-Agosto-2010 / Nelson Droguett Sierra ----------------------------------------------------------------------------------------------
    MSG_NOM_CORTO_EXISTE	= 'Nombre Corto ya existe para otra Entrada';
    MSG_COD_EXTERNO_EXISTE	= 'C�digo Externo ya existe para otra Entrada';
begin
	Result := False;
    if ValidateControls(
    					[	edtEntradaNombreCorto,
                            edtEntradaDescripcion,
                            edtEntradaCodExterno
    					],
                        [	edtEntradaNombreCorto.Text <> '',
                            edtEntradaDescripcion.Text <> '',
                            edtEntradaCodExterno.text <> ''
                        ],
                        Caption,
                        [	MSG_VALIDA_DESC,
                        	MSG_VALIDA_NOMB,
                            MSG_COD_EXTERNO
                        ]
    				) then begin
        if chkEntradaActivo.Checked then Result := True
        else begin
        	if MsgBox(MSG_NO_ACTIVO, Caption, MB_ICONQUESTION + MB_YESNO) = IDYES then Result := True;
        end;
        //Rev.1 / 04-Agosto-2010 / Nelson Droguet Sierra --------------------------------------------------------------------------------------------
        if (FEstadoEntrada=cEstadoInsert) then begin
          if Result then begin
             if QueryGetValueInt(DMConnections.BaseCAC,
                                 format('SELECT CodigoEntradaEstacionamiento FROM EntradasEstacionamiento (NOLOCK) WHERE NombreCorto=''%s''',[Trim(edtEntradaNombreCorto.Text)])) > 0 then begin
               MsgBoxBalloon(MSG_NOM_CORTO_EXISTE, Caption, MB_ICONWARNING, edtEntradaNombreCorto);
               Result := False;
             end;
          end;
          if Result then begin
             if QueryGetValueInt(DMConnections.BaseCAC,
                                 format('SELECT CodigoEntradaEstacionamiento FROM EntradasEstacionamiento (NOLOCK) WHERE CodigoExterno=''%s''',[Trim(edtEntradaCodExterno.Text)])) > 0 then begin
               MsgBoxBalloon(MSG_COD_EXTERNO_EXISTE, Caption, MB_ICONWARNING, edtEntradaCodExterno);
               Result := False;
             end;
          end;
        end;
        //FinRev.1 ---------------------------------------------------------------------------------------------------------------------------------
    end;
end;

{--------------------------------------------------------
        ValidaDatosSalidas

Author		: mbecerra
Date		: 24-Junio-2010
Description	:		(Ref Fase 2)
                Valida que est�n todos los datos obligatorios para grabar
                una salida de estacionamiento

--------------------------------------------------------------------}
function TABMConcesionariasForm.ValidarDatosSalidas;
resourcestring
	MSG_VALIDA_DESC			= 'No ha ingresado la Descripci�n de la Salida';
    MSG_VALIDA_NOMB			= 'No ha ingresado el NombreCorto de la Salida';
    MSG_COD_EXTERNO			= 'No ha ingresado el C�digo Externo de la Salida';
    MSG_NO_ACTIVO			= '�Est� seguro de que desea dejar esta Salida como INACTIVA?';
    //Rev.1 / 04-Agosto-2010 / Nelson Droguett Sierra ----------------------------------------------------------------------------------------------
    MSG_NOM_CORTO_EXISTE	= 'Nombre Corto ya existe para otra Salida';
    MSG_COD_EXTERNO_EXISTE	= 'C�digo Externo ya existe para otra Salida';    
begin
	Result := False;
    if ValidateControls(
    					[	edtSalidaNombreCorto,
                            edtSalidaDescripcion,
                            edtSalidaCodExterno
    					],
                        [	edtSalidaNombreCorto.Text <> '',
                            edtSalidaDescripcion.Text <> '',
                            edtSalidaCodExterno.text <> ''
                        ],
                        Caption,
                        [	MSG_VALIDA_DESC,
                        	MSG_VALIDA_NOMB,
                            MSG_COD_EXTERNO
                        ]
    				) then begin
        if chkSalidaActivo.Checked then Result := True
        else begin
        	if MsgBox(MSG_NO_ACTIVO, Caption, MB_ICONQUESTION + MB_YESNO) = IDYES then Result := True;
        end;
        //Rev.1 / 04-Agosto-2010 / Nelson Droguet Sierra --------------------------------------------------------------------------------------------
        if (FEstadoSalida=cEstadoInsert) then begin
          if Result then begin
             if QueryGetValueInt(DMConnections.BaseCAC,
                                 format('SELECT CodigoSalidaEstacionamiento FROM SalidasEstacionamiento (NOLOCK) WHERE NombreCorto=''%s''',[Trim(edtSalidaNombreCorto.Text)])) > 0 then begin
               MsgBoxBalloon(MSG_NOM_CORTO_EXISTE, Caption, MB_ICONWARNING, edtSalidaNombreCorto);
               Result := False;
             end;
          end;
          if Result then begin
             if QueryGetValueInt(DMConnections.BaseCAC,
                                 format('SELECT CodigoSalidaEstacionamiento FROM SalidasEstacionamiento (NOLOCK) WHERE CodigoExterno=''%s''',[Trim(edtSalidaCodExterno.Text)])) > 0 then begin
               MsgBoxBalloon(MSG_COD_EXTERNO_EXISTE, Caption, MB_ICONWARNING, edtSalidaCodExterno);
               Result := False;
             end;
          end;
        end;
        //FinRev.1 ---------------------------------------------------------------------------------------------------------------------------------
    end;
    
end;

{INICIO: TASK_008_JMA_20160429
{--------------------------------------------------------
        GrabarDatosConcesionaria

Author		: mbecerra
Date		: 24-Junio-2010
Description	:		(Ref Fase 2)
                Graba la Edici�n o la Inserci�n de un registro de Concesionaria

--------------------------------------------------------------------
function TABMConcesionariasForm.GrabarDatosConcesionaria;
resourcestring
    MSG_EXITO				= 'Concesionaria grabada con �xito';
    MSG_FRACASO				= 'Error al grabar Concesionaria';
    MSG_FRACASO_MOP			= 'Error al crear la tabla TransaccionesCAC_%s en la base MOPCAC';

var
    CodigoRetorno : integer;
begin
    Result := False;
	//Grabar
    DMConnections.BaseCAC.Execute('BEGIN TRAN spAgregarConcesionaria');
    with spAgregarConcesionaria do begin
        if Active then Close;
        Parameters.Refresh;

        if FEstadoGrilla = cEstadoInsert then Parameters.ParamByName('@CodigoConcesionaria').Value := NULL
        else Parameters.ParamByName('@CodigoConcesionaria').Value := edtCodigo.Text;

        Parameters.ParamByName('@Descripcion').Value							:= edtDescripcion.Text;
        Parameters.ParamByName('@FacturacionInterna').Value 					:= chkFacturacionInterna.Checked;
        Parameters.ParamByName('@PathImagenes').Value							:= edtRuta.Text;
        Parameters.ParamByName('@NombreCorto').Value							:= edtNombreCorto.Text;
        //Rev.2 / 24-Enero-2011 / Nelson Droguett Sierra----------------------------
//        Parameters.ParamByName('@CodigoTipoConceptoPeajePeriodo').Value			:= vcbTipoPeajePeriodo.Value;               //SF-046-NDR-20110124
//        Parameters.ParamByName('@CodigoTipoConceptoPeajePeriodoAnterior').Value	:= vcbTipoPeajePeriodoAnterior.Value;   //SF-046-NDR-20110124
        Parameters.ParamByName('@CodigoConceptoPeriodo').Value			        := AsignarValorComboEstado(vcbConceptoPeriodoActual);                    //SF-046-NDR-20110124
        Parameters.ParamByName('@CodigoConceptoPeriodoAnterior').Value	        := AsignarValorComboEstado(vcbConceptoPeriodoAnterior);              //SF-046-NDR-20110124
        //FinRev.2------------------------------------------------------------------
        Parameters.ParamByName('@TarifaUnica').Value							:= chkTarifaUnica.Checked;
        Parameters.ParamByName('@CodigoTipoConcesionaria').Value				:= vcbTipoConcesionaria.Value;
        Parameters.ParamByName('@TransitosLosFacturaCN').Value                  := chkFacturaCN.Checked;

        Parameters.ParamByName('@CodigoConceptoInfraBoletoPrejudicial').Value   := AsignarValorComboEstado(vcbConceptoBoletoInfractor);
        Parameters.ParamByName('@CodigoConceptoInfraGastosCobranza').Value	    := AsignarValorComboEstado(vcbConceptoGastosCobranzaInfractor);
        Parameters.ParamByName('@CodigoConceptoInfraIntereses').Value	        := AsignarValorComboEstado(vcbConceptoInteresesInfractor);

        Parameters.ParamByName('@CodigoConceptoInteresExento').Value	        := AsignarValorComboEstado(vcbConceptoInteresesExento);		// SS_1015_PDO_20120524
        Parameters.ParamByName('@CodigoConceptoInteresAfecto').Value	        := AsignarValorComboEstado(vcbConceptoInteresesAfecto);		// SS_1015_PDO_20120524
        Parameters.ParamByName('@OrdenCalculoIntereses').Value	                := nedtOrdenCalculoIntereses.Value;							// SS_1015_PDO_20120524
        Parameters.ParamByName('@PermiteReclamos').Value                        :=   chkPermiteReclamo.Checked;         // SS_1006_1015_MCO_20120904
        Parameters.ParamByName('@InformarFoliosYPagos').Value                   :=   chkInformarFoliosYPagos.Checked;   // SS_1006_NDR_20120926
        Parameters.ParamByName('@CodigoConceptoAjusteCobroPeaje').Value         := AsignarValorComboEstado(vcbConceptoAjusteCobroPeaje);    // SS_1006_1015_CQU_20121001
        Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat1').Value := AsignarValorComboEstado(vcbConceptoBoletoInfractorMorosidadCat1);    // SS_660_CQU_20121010
        Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat2').Value := AsignarValorComboEstado(vcbConceptoBoletoInfractorMorosidadCat2);    // SS_660_CQU_20121010
        Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat3').Value := AsignarValorComboEstado(vcbConceptoBoletoInfractorMorosidadCat3);    // SS_660_CQU_20121010
        Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat4').Value := AsignarValorComboEstado(vcbConceptoBoletoInfractorMorosidadCat4);    // SS_660_CQU_20121010
        Parameters.ParamByName('@PermiteInhabilitarListaAmarilla').Value        := chkPuedeListaAmarilla.Checked;   // SS_660_CQU_20130711

        ExecProc;
        CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
        if CodigoRetorno < 0 then begin
        	DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAgregarConcesionaria');
        	MsgBox(MSG_FRACASO, Caption, MB_ICONERROR);
        end
        else begin
        	DMConnections.BaseCAC.Execute('COMMIT TRAN spAgregarConcesionaria');
        	Result := True;
            MsgBox(MSG_EXITO, Caption, MB_ICONINFORMATION);
            RefrescarQueryConcesionaria;
            FEstadoGrilla := cEstadoBrowse;
            MostrarBotones(cEsConcesionaria);
            dsConcesionariasDataChange(nil, nil);
        end;
    end;
end;
}
function TABMConcesionariasForm.GrabarDatosConcesionaria;
resourcestring
    MSG_EXITO				= 'Datos de Concesionaria guardados exitosamente.';
    MSG_FRACASO				= 'Ocurri� un Error al guardar datos de la Concesionaria';
    MSG_FRACASO_MOP			= 'Ocurri� un Error al crear la tabla TransaccionesCAC_%s en la base MOPCAC';

var
    resultado : integer;
    spConcesionaria : TADOStoredProc;
begin
    Result := False;
    spConcesionaria:= TADOStoredProc.Create(nil);
    spConcesionaria.Connection := DMConnections.BaseCAC;
    spConcesionaria.ProcedureName := 'ADM_Concesionaria_UPDATE';
    spConcesionaria.Parameters.Refresh;

    spConcesionaria.Parameters.ParamByName('@CodigoConcesionaria').Value := edtCodigo.Text;
    spConcesionaria.Parameters.ParamByName('@Descripcion').Value							:= edtDescripcion.Text;
    spConcesionaria.Parameters.ParamByName('@FacturacionInterna').Value 					:= chkFacturacionInterna.Checked;
    spConcesionaria.Parameters.ParamByName('@PathImagenes').Value							:= edtRuta.Text;
    spConcesionaria.Parameters.ParamByName('@NombreCorto').Value							:= edtNombreCorto.Text;
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoPeriodo').Value			        := AsignarValorComboEstado(vcbConceptoPeriodoActual);
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoPeriodoAnterior').Value	        := AsignarValorComboEstado(vcbConceptoPeriodoAnterior);
    spConcesionaria.Parameters.ParamByName('@TarifaUnica').Value							:= chkTarifaUnica.Checked;
    spConcesionaria.Parameters.ParamByName('@CodigoTipoConcesionaria').Value				:= vcbTipoConcesionaria.Value;
    spConcesionaria.Parameters.ParamByName('@TransitosLosFacturaCN').Value                  := chkFacturaCN.Checked;
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoInfraBoletoPrejudicial').Value   := AsignarValorComboEstado(vcbConceptoBoletoInfractor);
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoInfraGastosCobranza').Value	    := AsignarValorComboEstado(vcbConceptoGastosCobranzaInfractor);
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoInfraIntereses').Value	        := AsignarValorComboEstado(vcbConceptoInteresesInfractor);
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoInteresExento').Value	        := AsignarValorComboEstado(vcbConceptoInteresesExento);
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoInteresAfecto').Value	        := AsignarValorComboEstado(vcbConceptoInteresesAfecto);
    spConcesionaria.Parameters.ParamByName('@OrdenCalculoIntereses').Value	                := nedtOrdenCalculoIntereses.Value;
    spConcesionaria.Parameters.ParamByName('@PermiteReclamos').Value                        := chkPermiteReclamo.Checked;
    spConcesionaria.Parameters.ParamByName('@InformarFoliosYPagos').Value                   := chkInformarFoliosYPagos.Checked;
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoAjusteCobroPeaje').Value         := AsignarValorComboEstado(vcbConceptoAjusteCobroPeaje);
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat1').Value := AsignarValorComboEstado(vcbConceptoBoletoInfractorMorosidadCat1);
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat2').Value := AsignarValorComboEstado(vcbConceptoBoletoInfractorMorosidadCat2);
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat3').Value := AsignarValorComboEstado(vcbConceptoBoletoInfractorMorosidadCat3);
    spConcesionaria.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat4').Value := AsignarValorComboEstado(vcbConceptoBoletoInfractorMorosidadCat4);
    spConcesionaria.Parameters.ParamByName('@PermiteInhabilitarListaAmarilla').Value        := chkPuedeListaAmarilla.Checked;
    spConcesionaria.Parameters.ParamByName('@RUT').Value                                    := iif(txt_RUT.Text = EmptyStr, NULL, txt_RUT.Text); //TASK_008_RME_20160516
    spConcesionaria.Parameters.ParamByName('@DigitoVerificador').Value                      := iif(txt_RUT.Text = EmptyStr, NULL, txt_DV.Text); //TASK_008_RME_20160516
    spConcesionaria.Parameters.ParamByName('@RazonSocial').Value                            := txt_RazonSocial.Text;
    spConcesionaria.Parameters.ParamByName('@Giro').Value                                   := txt_Giro.Text;
    spConcesionaria.Parameters.ParamByName('@CodigoActividadEconomica').Value               := txt_CodigoActividadEconomica.Text;
    spConcesionaria.Parameters.ParamByName('@CodigoSIISucursal').Value                      := iif(txt_CodigoSII.Text = EmptyStr, NULL, txt_CodigoSII.Text); //NULL; //txt_CodigoSII.Text;
    spConcesionaria.Parameters.ParamByName('@Direccion').Value                              := txt_Direccion.Text;
    spConcesionaria.Parameters.ParamByName('@CodigoPais').Value                             := cbbPais.Value;
    spConcesionaria.Parameters.ParamByName('@CodigoRegion').Value                           := cbbRegion.Value;
    spConcesionaria.Parameters.ParamByName('@CodigoComuna').Value                           := cbbComuna.Value;
    spConcesionaria.Parameters.ParamByName('@EsConcesionariaNativa').Value                  := chkEsNativa.Checked;

    try
        spConcesionaria.ExecProc;
        resultado := spConcesionaria.Parameters.ParamByName('@RETURN_VALUE').Value;
        if resultado < 0 then begin
        	MsgBox(MSG_FRACASO, Caption, MB_ICONERROR);
        end
        else begin
        	Result := True;
            MsgBox(MSG_EXITO, Caption, MB_ICONINFORMATION);
            RefrescarQueryConcesionaria;
            FEstadoGrilla := cEstadoBrowse;
            MostrarBotones(cEsConcesionaria);
            dsConcesionariasDataChange(nil, nil);
        end;
    finally
        FreeAndNil(spConcesionaria);
    end;
end;

{--------------------------------------------------------
        GrabarDatosConcesionaria

Author		: mbecerra
Date		: 24-Junio-2010
Description	:		(Ref Fase 2)
                Graba la Edici�n o la Inserci�n de un registro de Entrada
                de Estacionamiento

--------------------------------------------------------------------}
function TABMConcesionariasForm.GrabarDatosEntrada;
resourcestring
    MSG_EXITO				= 'Entrada grabada con �xito';
    MSG_FRACASO				= 'Error al grabar Entrada';

var
    CodigoRetorno : integer;
begin
	Result := False;
    with spAgregarEntradaEstacionamiento do begin
    	if FEstadoEntrada = cEstadoInsert then Parameters.ParamByName('@CodigoEntradaEstacionamiento').Value := NULL
        else Parameters.ParamByName('@CodigoEntradaEstacionamiento').Value := edtEntradaCod.Text;

        Parameters.ParamByName('@NombreCorto').Value := edtEntradaNombreCorto.Text;
        Parameters.ParamByName('@Descripcion').Value := edtEntradaDescripcion.Text;
        Parameters.ParamByName('@CodigoConcesionaria').Value := aqryConcesionarias.FieldByName('CodigoConcesionaria').Value;
        Parameters.ParamByName('@CodigoExterno').Value := edtEntradaCodExterno.Text;
        Parameters.ParamByName('@Activo').Value := chkEntradaActivo.Checked;
        Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        ExecProc;
        CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
        if CodigoRetorno < 0 then MsgBox(MSG_FRACASO, Caption, MB_ICONERROR)
        else begin
        	Result := True;
        	MsgBox(MSG_EXITO, Caption, MB_ICONINFORMATION);
            aqryEntradas.Close;
            aqryEntradas.Open;
            FEstadoEntrada := cEstadoBrowse;
            MostrarBotones(cEsEntradaEstacionamiento);
            MostrarBotones(cEsConcesionaria);
            dsEntradasDataChange(nil, nil);
        end;
    end;

end;

{--------------------------------------------------------
        GrabarDatosConcesionaria

Author		: mbecerra
Date		: 24-Junio-2010
Description	:		(Ref Fase 2)
                Graba la Edici�n o la Inserci�n de un registro de Salida
                de Estacionamiento

--------------------------------------------------------------------}
function TABMConcesionariasForm.GrabarDatosSalida;
resourcestring
    MSG_EXITO				= 'Salida grabada con �xito';
    MSG_FRACASO				= 'Error al grabar Salida';

var
    CodigoRetorno : integer;
begin
	Result := False;
    with spAgregarSalidaEstacionamiento do begin
    	if FEstadoSalida = cEstadoInsert then Parameters.ParamByName('@CodigoSalidaEstacionamiento').Value := NULL
        else Parameters.ParamByName('@CodigoSalidaEstacionamiento').Value := edtSalidaCodigo.Text;

        Parameters.ParamByName('@NombreCorto').Value := edtSalidaNombreCorto.Text;
        Parameters.ParamByName('@Descripcion').Value := edtSalidaDescripcion.Text;
        Parameters.ParamByName('@CodigoConcesionaria').Value := aqryConcesionarias.FieldByName('CodigoConcesionaria').Value;
        Parameters.ParamByName('@CodigoExterno').Value := edtSalidaCodExterno.Text;
        Parameters.ParamByName('@Activo').Value := chkSalidaActivo.Checked;
        Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        ExecProc;
        CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
        if CodigoRetorno < 0 then MsgBox(MSG_FRACASO, Caption, MB_ICONERROR)
        else begin
        	Result := True;
        	MsgBox(MSG_EXITO, Caption, MB_ICONINFORMATION);
            aqrySalidas.Close;
            aqrySalidas.Open;
            FEstadoSalida := cEstadoBrowse;
            MostrarBotones(cEsSalidaEstacionamiento);
            MostrarBotones(cEsConcesionaria);
            dsSalidasDataChange(nil, nil);
        end;
    end;

end;

{--------------------------------------------------------
        BtnAceptarClick

Author		: mbecerra
Date		: 22-Junio-2010
Description	:		(Ref Fase 2)
                Graba la Edici�n o la Inserci�n de un registro de Concesionaria,
                Entrada o Salida de estacionamiento

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.BtnAceptarClick(Sender: TObject);
begin
    if FTipoCambio = cEsConcesionaria then begin
    	if ValidarDatosConcesionaria() then GrabarDatosConcesionaria();
    end
    else if FTipoCambio = cEsEntradaEstacionamiento then begin
    	if ValidarDatosEntradas() then GrabarDatosEntrada();
    end
    else if FTipoCambio = cEsSalidaEstacionamiento then begin
    	if ValidarDatosSalidas() then GrabarDatosSalida();
    end;

end;

{--------------------------------------------------------
        BtnCancelarClick

Author		: mbecerra
Date		: 22-Junio-2010
Description	:		(Ref Fase 2)
                Cancela la Edici�n o la Inserci�n de un registro

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.BtnCancelarClick(Sender: TObject);
begin
	//habilitar botones superiores si se estaba editando una entrada o salida
    if (FEstadoEntrada <> cEstadoBrowse) or (FEstadoSalida <> cEstadoBrowse) then begin
        btnInsertar.Enabled		:= True;
		btnEliminar.Enabled		:= True;
		btnModificar.Enabled	:= True;
    end;

    if FTipoCambio = cEsConcesionaria then begin
    	FEstadoGrilla := cEstadoBrowse;
    	dsConcesionariasDataChange(nil, nil);
    end
    else if FTipoCambio = cEsEntradaEstacionamiento then begin
    	FEstadoEntrada := cEstadoBrowse;
        dsEntradasDataChange(nil, nil);
    end
    else if FTipoCambio = cEsSalidaEstacionamiento then begin
    	FEstadoSalida := cEstadoBrowse;
        dsSalidasDataChange(nil, nil);
    end;

    MostrarBotones(FTipoCambio);

end;

{--------------------------------------------------------
        btnEliminarClick

Author		: mbecerra
Date		: 23-Junio-2010
Description	:		(Ref Fase 2)
                Permite la eliminaci�n de un registro de la tabla Concesionarias

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.btnEliminarClick(Sender: TObject);
resourcestring
	MSG_SEGURO		= 'Est� seguro de eliminar la concesionaria %s ?';
    MSG_ELIMINAR	= 'No se puede eliminar la concesionaria. Tiene informaci�n relacionada.';
    MSG_TITULO		= 'Eliminar concesionaria';
    MSG_ELIMINADA	= 'La Concesionaria ha sido eliminada';
    MSG_SQL			= 'DELETE FROM Concesionarias WHERE CodigoConcesionaria = %d';
begin
    if (FEstadoGrilla = cEstadoBrowse) and (not aqryConcesionarias.Eof) then begin
    	if MsgBox(Format(MSG_SEGURO, [aqryConcesionarias.FieldByName('NombreCorto').AsString]), MSG_TITULO, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            try
            	DMConnections.BaseCAC.Execute(Format(MSG_SQL, [aqryConcesionarias.FieldByName('CodigoConcesionaria').AsInteger]));
                MsgBox(MSG_ELIMINADA, MSG_TITULO, MB_OK + MB_ICONINFORMATION);
                RefrescarQueryConcesionaria;
                dsConcesionariasDataChange(nil, nil);
            except on e:exception do begin
                    MsgBoxErr(MSG_ELIMINAR, e.Message, MSG_TITULO, MB_ICONERROR);
            	end;

            end;
        end;

    end;

end;

{--------------------------------------------------------
        btnInsertarClick

Author		: mbecerra
Date		: 22-Junio-2010
Description	:		(Ref Fase 2)
                Habilita la inserci�n de una nueva concesionaria

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.btnInsertarClick(Sender: TObject);
begin
	FTipoCambio := cEsConcesionaria;
    FEstadoGrilla	:= cEstadoInsert;
    MostrarBotones(cEsConcesionaria);
    LimpiarCampos(cEsConcesionaria);
    pcConcesionarias.ActivePage := tsConcesionarias;
    edtDescripcion.SetFocus;
end;


{--------------------------------------------------------
        btnModificarClick

Author		: mbecerra
Date		: 23-Junio-2010
Description	:		(Ref Fase 2)
                Habilita la modificaci�n de una concesionaria

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.btnModificarClick(Sender: TObject);
begin
	FTipoCambio := cEsConcesionaria;
    FEstadoGrilla := cEstadoModify;
    MostrarBotones(cEsConcesionaria);
    if pcConcesionarias.ActivePage = tsConcesionarias then
        edtDescripcion.SetFocus
    else if pcConcesionarias.ActivePage = tsDatosBasico then
        txt_RUT.SetFocus;
end;

{--------------------------------------------------------
        btnSalidaEliminarClick

Author		: mbecerra
Date		: 25-Junio-2010
Description	:		(Ref Fase 2)
            	Permite la eliminaci�n de un registro de salida
                de estacionamiento

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.btnSalidaEliminarClick(Sender: TObject);
resourcestring
    MSG_NO_ES_ESTACIONAMIENTO = 'No puede modificar. La Concesionaria no es de tipo Estacionamiento';
    
	MSG_SEGURO		= 'Est� seguro de eliminar la salida %s ?';
    MSG_ELIMINAR	= 'Error al eliminar la salida: %s';
    MSG_TITULO		= 'Eliminar Salida';
    MSG_ELIMINADA	= 'La Salida ha sido eliminada';
    MSG_SQL			= 'DELETE FROM SalidasEstacionamiento WHERE CodigoSalidaEstacionamiento = %d';
begin
    if FConcesionariaEsEstacionamiento then begin
    	if (FEstadoSalida = cEstadoBrowse) and (not aqrySalidas.Eof) then begin
	    	if MsgBox(Format(MSG_SEGURO, [aqrySalidas.FieldByName('NombreCorto').AsString]), MSG_TITULO, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            	try
            		DMConnections.BaseCAC.Execute(Format(MSG_SQL, [aqrySalidas.FieldByName('CodigoSalidaEstacionamiento').AsInteger]));
                	MsgBox(MSG_ELIMINADA, MSG_TITULO, MB_OK + MB_ICONINFORMATION);
                	aqrySalidas.Close;
                	aqrySalidas.Open;
                	dsSalidasDataChange(nil, nil);
            	except on e:exception do begin
                    	MsgBox(Format(MSG_ELIMINAR, [e.Message]), MSG_TITULO, MB_ICONERROR);
            		end;
            	end;
        	end;
    	end;
    end
    else MsgBox(MSG_NO_ES_ESTACIONAMIENTO, Caption, MB_ICONEXCLAMATION); 

end;

{--------------------------------------------------------
        btnSalidaInsertarClick

Author		: mbecerra
Date		: 24-Junio-2010
Description	:		(Ref Fase 2)
            	Permite la inserci�n de un registro de salida
                de estacionamiento

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.btnSalidaInsertarClick(Sender: TObject);
resourcestring
    MSG_NO_ES_ESTACIONAMIENTO = 'No puede modificar. La Concesionaria no es de tipo Estacionamiento';

begin
    if FConcesionariaEsEstacionamiento then begin
    	FTipoCambio := cEsSalidaEstacionamiento;
    	FEstadoSalida := cEstadoInsert;
    	MostrarBotones(cEsSalidaEstacionamiento);
    	LimpiarCampos(cEsSalidaEstacionamiento);
    	pcConcesionarias.ActivePage := tsSalidas;
    	edtSalidaNombreCorto.SetFocus;
    end
    else  MsgBox(MSG_NO_ES_ESTACIONAMIENTO, Caption, MB_ICONEXCLAMATION);
end;

{--------------------------------------------------------
        btnSalirClick

Author		: mbecerra
Date		: 21-Junio-2010
Description	:		(Ref Fase 2)
            	Cierra la ventana del ABM de concesionarias

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

{--------------------------------------------------------
        CargarTipoConceptoPeajes

Author		: mbecerra
Date		: 22-Junio-2010
Description	:		(Ref Fase 2)
                Llena la combobox con los tipos de conceptos
                que sean de peajes.

--------------------------------------------------------------------}
//Rev.2 / 24-Enero-2011 / Nelson Droguett Sierra---------------------------
//procedure TABMConcesionariasForm.CargarTipoConceptoPeajes;                                         //SF-046-NDR-20110124
//begin                                                                                              //SF-046-NDR-20110124
//    with spObtenerTiposConceptosMovimientos do begin                                               //SF-046-NDR-20110124
//    	Close;                                                                                       //SF-046-NDR-20110124
//        //Rev.1 / 30-Julio-2010 / Nelson Droguett Sierra ----------------------------------------- //SF-046-NDR-20110124
//        Parameters.Refresh;                                                                        //SF-046-NDR-20110124
//    	Parameters.ParamByName('@SoloFacturables').Value := 1;                                       //SF-046-NDR-20110124
//        //FinRev.1-------------------------------------------------------------------------------- //SF-046-NDR-20110124
//        Open;                                                                                      //SF-046-NDR-20110124
//        vcbTipoPeajePeriodo.Clear;                                                                 //SF-046-NDR-20110124
//        while not Eof do begin                                                                     //SF-046-NDR-20110124
//            vcbTipoPeajePeriodo.Items.InsertItem(	FieldByName('Descripcion').AsString,             //SF-046-NDR-20110124
//                                                	FieldByName('CodigoTipoConcepto').Value);        //SF-046-NDR-20110124
//            Next;                                                                                  //SF-046-NDR-20110124
//        end;                                                                                       //SF-046-NDR-20110124
//                                                                                                   //SF-046-NDR-20110124
//        Close;                                                                                     //SF-046-NDR-20110124
//    end;                                                                                           //SF-046-NDR-20110124
//end;                                                                                               //SF-046-NDR-20110124

procedure TABMConcesionariasForm.CargarConceptosPeajes;                                                                                                 //SF-046-NDR-20110124
begin                                                                                                                                                   //SF-046-NDR-20110124
    with spObtenerConceptosComprobante do begin                                                                                                         //SF-046-NDR-20110124
    	  Close;                                                                                                                                        //SF-046-NDR-20110124
        Parameters.Refresh;                                                                                                                             //SF-046-NDR-20110124
    	Parameters.ParamByName('@TipoComprobante').Value := QueryGetValue(DMConnections.BaseCAC, 'SELECT DBO.CONST_TIPO_COMPROBANTE_NOTA_COBRO()');     //SF-046-NDR-20110124
//        Parameters.ParamByName('@CodigoConcesionaria').Value := aqryConcesionarias.FieldByName('CodigoConcesionaria').AsInteger;						// SS_1015_PDO_20120524
        Parameters.ParamByName('@CodigoConcesionaria').Value := null;                                                           						// SS_1015_PDO_20120524
        Open;                                                                                                                                           //SF-046-NDR-20110124

        vcbConceptoPeriodoActual.Clear;                                                                                                                 //SF-046-NDR-20110124

        vcbConceptoInteresesExento.Clear;																												// SS_1015_PDO_20120524
        vcbConceptoInteresesAfecto.Clear;
        vcbConceptoAjusteCobroPeaje.Clear;  // SS_1006_1015_CQU_20121001																												// SS_1015_PDO_20120524

        while not Eof do begin                                                                                                                          //SF-046-NDR-20110124
            vcbConceptoPeriodoActual.Items.InsertItem(	FieldByName('Descripcion').AsString,                                                            //SF-046-NDR-20110124
                                                	      FieldByName('CodigoConcepto').Value);                                                         //SF-046-NDR-20110124
            Next;                                                                                                                                       //SF-046-NDR-20110124
        end;                                                                                                                                            //SF-046-NDR-20110124
        Close;                                                                                                                                          //SF-046-NDR-20110124
    end;                                                                                                                                                //SF-046-NDR-20110124
end;                                                                                                                                                    //SF-046-NDR-20110124
//FinRev.2------------------------------------------------------------------


{--------------------------------------------------------
        CargarTiposConcesionarias

Author		: mbecerra
Date		: 21-Junio-2010
Description	:		(Ref Fase 2)
            	Carga los tipos de concesionarias

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.CargarTiposConcesionarias;
begin
    with aqryTiposConcesionarias do begin
    	Close;
        Open;
        vcbTipoConcesionaria.Clear;
        while not Eof do begin
            vcbTipoConcesionaria.Items.InsertItem(	FieldByName('Descripcion').AsString,
                                                	FieldByName('CodigoTipoConcesionaria').Value);
            Next;
        end;

        Close;
    end;
end;





{--------------------------------------------------------
        MostrarBotones

Author		: mbecerra
Date		: 22-Junio-2010
Description	:		(Ref Fase 2)
                Despliega los botones de grabaci�n o de despliegue

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.MostrarBotones;
begin
    if DeCualTab = cEsConcesionaria then begin
    {INICIAR: TASK_008_JMA_20160429
		btnInsertar.Enabled		:= (FEstadoGrilla = cEstadoBrowse);
		btnEliminar.Enabled		:= (FEstadoGrilla = cEstadoBrowse);
        }
    {TERMINO: TASK_008_JMA_20160429}
		btnModificar.Enabled	:= (FEstadoGrilla = cEstadoBrowse);
    end;

    if DeCualTab = cEsEntradaEstacionamiento then begin
    	btnEntraInsertar.Enabled	:= (FEstadoEntrada = cEstadoBrowse);
        btnEntraEliminar.Enabled	:= (FEstadoEntrada = cEstadoBrowse);
        btnEntraModificar.Enabled	:= (FEstadoEntrada = cEstadoBrowse);
    end;

    if DeCualTab = cEsSalidaEstacionamiento then begin
    	btnSalidaInsertar.Enabled	:= (FEstadoSalida = cEstadoBrowse);
        btnSalidaEliminar.Enabled	:= (FEstadoSalida = cEstadoBrowse);
        btnSalidaModificar.Enabled	:= (FEstadoSalida = cEstadoBrowse);
    end;


    dblConcesionarias.Enabled   := (FEstadoGrilla = cEstadoBrowse) and (FEstadoEntrada = cEstadoBrowse) and (FEstadoSalida = cEstadoBrowse);
    tsConcesionarias.Enabled	:= (FEstadoGrilla in [cEstadoInsert, cEstadoModify]);
{INICIO: TASK_008_JMA_20160429}
    tsDatosBasico.Enabled       := tsConcesionarias.Enabled;
{TERMINADO: TASK_008_JMA_20160429}
    pnlEntradas.Enabled			:= (FEstadoEntrada in [cEstadoInsert, cEstadoModify]);
    pnlSalidas.Enabled			:= (FEstadoSalida in [cEstadoInsert, cEstadoModify]);

    if (FEstadoGrilla in [cEstadoInsert, cEstadoModify])
    	or (FEstadoEntrada in [cEstadoInsert, cEstadoModify])
        	or (FEstadoSalida in [cEstadoInsert, cEstadoModify]) then begin
    	nbGrabar.PageIndex := 1;
    end
    else nbGrabar.PageIndex := 0;

    //inhabilitar botones superiores si se est� modificando una Entrada o una Salida
    if (FEstadoEntrada <> cEstadoBrowse) or (FEstadoSalida <> cEstadoBrowse) then begin
    {INICIAR: TASK_008_JMA_20160429
    	btnInsertar.Enabled		:= False;
		btnEliminar.Enabled		:= False;
    }
    {TERMINADO: TASK_008_JMA_20160429}
		btnModificar.Enabled	:= False;
    end;

end;

{--------------------------------------------------------
        LimpiarCampos

Author		: mbecerra
Date		: 22-Junio-2010
Description	:		(Ref Fase 2)
                Limpia los campos indicados

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.LimpiarCampos;
begin
    if DeCualTab = cEsConcesionaria then begin
        edtCodigo.Text := '';
        edtDescripcion.Text := '';
        edtNombreCorto.Text := '';
        chkFacturacionInterna.Checked := False;
        chkTarifaUnica.Checked := False;
        chkPermiteReclamo.Checked := False; // SS-1006-1015-MCO-20120904
        chkInformarFoliosYPagos.Checked := False; // SS_1006_NDR_20120926
        edtRuta.Text := '';
        //vcbTipoPeajePeriodo.ItemIndex := -1;                    //SF-046-NDR-20110124
        //vcbTipoPeajePeriodoAnterior.ItemIndex := -1;            //SF-046-NDR-20110124
        vcbConceptoPeriodoActual.ItemIndex := -1;                //SF-046-NDR-20110124
        vcbConceptoPeriodoAnterior.ItemIndex := -1;              //SF-046-NDR-20110124
        vcbTipoConcesionaria.ItemIndex := -1;
        nedtOrdenCalculoIntereses.Value := 0;					 // SS_1015_PDO_20120524
        vcbConceptoAjusteCobroPeaje.ItemIndex := -1;             // SS_1006_1015_CQU_20121001
        vcbConceptoBoletoInfractor.ItemIndex := -1;              // SS_1006_1015_CQU_20121001
        vcbConceptoGastosCobranzaInfractor.ItemIndex := -1;      // SS_1006_1015_CQU_20121001
        vcbConceptoInteresesInfractor.ItemIndex := -1;           // SS_1006_1015_CQU_20121001
        vcbConceptoInteresesExento.ItemIndex := -1;              // SS_1006_1015_CQU_20121001
        vcbConceptoInteresesAfecto.ItemIndex := -1;              // SS_1006_1015_CQU_20121001
        vcbConceptoBoletoInfractorMorosidadCat1.ItemIndex:= -1; // SS_660_CQU_20121010
        vcbConceptoBoletoInfractorMorosidadCat2.ItemIndex:= -1; // SS_660_CQU_20121010
        vcbConceptoBoletoInfractorMorosidadCat3.ItemIndex:= -1; // SS_660_CQU_20121010
        vcbConceptoBoletoInfractorMorosidadCat4.ItemIndex:= -1; // SS_660_CQU_20121010

{INICIO: TASK_008_JMA_20160429}
        chkEsNativa.Checked := False;
        txt_RUT.Clear;
        txt_DV.Clear;
        txt_RazonSocial.Clear;
        txt_Giro.Clear;
        txt_CodigoActividadEconomica.Clear;
        txt_CodigoSII.Clear;
        txt_Direccion.Clear;
        if cbbRegion.Items.Count>0 then
            cbbRegion.ItemIndex:=0;
        CargarComunas(cbbRegion.Value);
{TERMINADO: TASK_008_JMA_20160429}
    end;

    if DeCualTab = cEsEntradaEstacionamiento then begin
    	edtEntradaCod.Text := '';
        edtEntradaNombreCorto.Text := '';
        edtEntradaDescripcion.Text := '';
        edtEntradaCodExterno.Text := '';
    end;

    if DeCualTab = cEsSalidaEstacionamiento then begin
        edtSalidaCodigo.Text := '';
        edtSalidaNombreCorto.Text := '';
        edtSalidaDescripcion.Text := '';
        edtSalidaCodExterno.Text := '';
    end;

end;

{--------------------------------------------------------
        btnEntraEliminarClick

Author		: mbecerra
Date		: 25-Junio-2010
Description	:		(Ref Fase 2)
                Permite la eliminaci�n de un registro de entrada
                de estacionamiento

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.btnEntraEliminarClick(Sender: TObject);
resourcestring
    MSG_NO_ES_ESTACIONAMIENTO = 'No puede modificar. La Concesionaria no es de tipo Estacionamiento';

	MSG_SEGURO		= 'Est� seguro de eliminar la entrada %s ?';
    MSG_ELIMINAR	= 'Error al eliminar la entrada: %s';
    MSG_TITULO		= 'Eliminar Entrada';
    MSG_ELIMINADA	= 'La Entrada ha sido eliminada';
    MSG_SQL			= 'DELETE FROM EntradasEstacionamiento WHERE CodigoEntradaEstacionamiento = %d';
begin
    if FConcesionariaEsEstacionamiento then begin
	    if (FEstadoEntrada = cEstadoBrowse) and (not aqryEntradas.Eof) then begin
        	if MsgBox(Format(MSG_SEGURO, [aqryEntradas.FieldByName('NombreCorto').AsString]), MSG_TITULO, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            	try
       	     		DMConnections.BaseCAC.Execute(Format(MSG_SQL, [aqryEntradas.FieldByName('CodigoEntradaEstacionamiento').AsInteger]));
                	MsgBox(MSG_ELIMINADA, MSG_TITULO, MB_OK + MB_ICONINFORMATION);
                	aqryEntradas.Close;
                	aqryEntradas.Open;
                	dsEntradasDataChange(nil, nil);
            	except on e:exception do begin
                    	MsgBox(Format(MSG_ELIMINAR, [e.Message]), MSG_TITULO, MB_ICONERROR);
            		end;
            	end;
        	end;

    	end;
    end
    else MsgBox(MSG_NO_ES_ESTACIONAMIENTO, Caption, MB_ICONEXCLAMATION);

end;


{--------------------------------------------------------
        btnEntraInsertarClick

Author		: mbecerra
Date		: 24-Junio-2010
Description	:		(Ref Fase 2)
                Permite la inserci�n de un registro de entrada
                de estacionamiento

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.btnEntraInsertarClick(Sender: TObject);
resourcestring
    MSG_NO_ES_ESTACIONAMIENTO = 'No puede modificar. La Concesionaria no es de tipo Estacionamiento';

begin
    if FConcesionariaEsEstacionamiento then begin
    	FTipoCambio := cEsEntradaEstacionamiento;
    	FEstadoEntrada := cEstadoInsert;
    	MostrarBotones(cEsEntradaEstacionamiento);
    	LimpiarCampos(cEsEntradaEstacionamiento);
    	pcConcesionarias.ActivePage := tsEntradas;
    	edtEntradaNombreCorto.SetFocus;
    end
    else  MsgBox(MSG_NO_ES_ESTACIONAMIENTO, Caption, MB_ICONEXCLAMATION);

end;


{--------------------------------------------------------
        btnModificarClick

Author		: mbecerra
Date		: 25-Junio-2010
Description	:		(Ref Fase 2)
                Habilita la modificaci�n de una entrada

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.btnEntraModificarClick(Sender: TObject);
resourcestring
    MSG_NO_ES_ESTACIONAMIENTO = 'No puede modificar. La Concesionaria no es de tipo Estacionamiento';
begin
	if FConcesionariaEsEstacionamiento then begin
		FTipoCambio := cEsEntradaEstacionamiento;
    	FEstadoEntrada := cEstadoModify;
    	MostrarBotones(cEsEntradaEstacionamiento);
    	pcConcesionarias.ActivePage := tsEntradas;
    	edtEntradaNombreCorto.SetFocus;
    end
    else  MsgBox(MSG_NO_ES_ESTACIONAMIENTO, Caption, MB_ICONEXCLAMATION);
end;


{--------------------------------------------------------
        btnModificarClick

Author		: mbecerra
Date		: 25-Junio-2010
Description	:		(Ref Fase 2)
                Habilita la modificaci�n de una Salida

--------------------------------------------------------------------}
procedure TABMConcesionariasForm.btnSalidaModificarClick(Sender: TObject);
resourcestring
    MSG_NO_ES_ESTACIONAMIENTO = 'No puede modificar. La Concesionaria no es de tipo Estacionamiento';
begin
	if FConcesionariaEsEstacionamiento then begin
		FTipoCambio := cEsSalidaEstacionamiento;
    	FEstadoSalida := cEstadoModify;
    	MostrarBotones(cEsSalidaEstacionamiento);
    	pcConcesionarias.ActivePage := tsSalidas;
    	edtSalidaNombreCorto.SetFocus;
	end
    else  MsgBox(MSG_NO_ES_ESTACIONAMIENTO, Caption, MB_ICONEXCLAMATION);
end;

procedure TABMConcesionariasForm.CargarConceptosInfractores;
begin                                                                                                                                                   
    with spObtenerConceptosInfractores do begin
        if Active then Close;
        Parameters.Refresh;																												// SS_1015_PDO_20120524
        Parameters.ParamByName('@CodigoConcesionaria').Value := aqryConcesionarias.FieldByName('CodigoConcesionaria').AsInteger;		// SS_1015_PDO_20120524
        Open;

        vcbConceptoBoletoInfractor.Clear;
        vcbConceptoGastosCobranzaInfractor.Clear;
        vcbConceptoInteresesInfractor.Clear;
        vcbConceptoBoletoInfractorMorosidadCat1.Clear;                                              // SS_660_CQU_20121010
        vcbConceptoBoletoInfractorMorosidadCat2.Clear;                                              // SS_660_CQU_20121010
        vcbConceptoBoletoInfractorMorosidadCat3.Clear;                                              // SS_660_CQU_20121010
        vcbConceptoBoletoInfractorMorosidadCat4.Clear;                                              // SS_660_CQU_20121010

        while not Eof do begin
            vcbConceptoBoletoInfractor.Items.InsertItem
                (
                    FieldByName('Descripcion').AsString,
                    FieldByName('CodigoConcepto').Value
                );
            Next;                                                                                                                                       
        end;

        Close;

        vcbConceptoGastosCobranzaInfractor.Items.Assign(vcbConceptoBoletoInfractor.Items);                                                                                //SF-046-NDR-20110124
        vcbConceptoInteresesInfractor.Items.Assign(vcbConceptoBoletoInfractor.Items);                                                                                //SF-046-NDR-20110124
        vcbConceptoBoletoInfractorMorosidadCat1.Items.Assign(vcbConceptoBoletoInfractor.Items);    // SS_660_CQU_20121010
        vcbConceptoBoletoInfractorMorosidadCat2.Items.Assign(vcbConceptoBoletoInfractor.Items);    // SS_660_CQU_20121010
        vcbConceptoBoletoInfractorMorosidadCat3.Items.Assign(vcbConceptoBoletoInfractor.Items);    // SS_660_CQU_20121010
        vcbConceptoBoletoInfractorMorosidadCat4.Items.Assign(vcbConceptoBoletoInfractor.Items);    // SS_660_CQU_20121010
    end;
end;

procedure TABMConcesionariasForm.RefrescarQueryConcesionaria;
    var
        lPuntero: TBookmark;
begin
    if aqryConcesionarias.Active and (not aqryConcesionarias.IsEmpty) then try
        CambiarEstadoCursor(CURSOR_RELOJ);

        lPuntero := aqryConcesionarias.GetBookmark;
        aqryConcesionarias.Close;
        aqryConcesionarias.Open;
    finally
        if aqryConcesionarias.BookmarkValid(lPuntero) then begin
            aqryConcesionarias.GotoBookmark(lPuntero);
        end;

        aqryConcesionarias.FreeBookmark(lPuntero);
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

{INICIO:  TASK_008_JMA_20160429_Unificaci�n tablas datos Concesionaria}
procedure TABMConcesionariasForm.CargarPaises;
var
    spPaises : TADOStoredProc;
begin

    spPaises := TADOStoredProc.Create(nil);
    spPaises.Connection := DMConnections.BaseCAC;
    spPaises.ProcedureName := 'ObtenerPaises';
    try
        spPaises.Open;
        if spPaises.RecordCount > 0 then
        begin
            cbbPais.Clear;
            while not spPaises.Eof do begin
                cbbPais.Items.InsertItem(spPaises.FieldByName('Descripcion').AsString, spPaises.FieldByName('CodigoPais').Value);
                spPaises.Next;
            end;
            cbbPais.ItemIndex := cbbPais.Items.IndexOfValue('CHI');
        end;
    finally
        spPaises.close;
    end;

end;

procedure TABMConcesionariasForm.CargarRegiones(CodigoPais: string);
var
    spRegiones : TADOStoredProc;
begin

    spRegiones := TADOStoredProc.Create(nil);
    spRegiones.Connection := DMConnections.BaseCAC;
    spRegiones.ProcedureName := 'ObtenerRegiones';
    try
        spRegiones.Open;
        if spRegiones.RecordCount > 0 then
        begin
            cbbRegion.Clear;
            while not spRegiones.Eof do begin
                if spRegiones.FieldByName('CodigoPais').AsString = CodigoPais then
                    cbbRegion.Items.InsertItem(spRegiones.FieldByName('Region').AsString, spRegiones.FieldByName('CodigoRegion').Value);
                spRegiones.Next;
            end;
            cbbRegion.ItemIndex := 0;
        end;
    finally
        spRegiones.close;
    end;

end;

procedure TABMConcesionariasForm.cbbRegionChange(Sender: TObject);
begin
    CargarComunas(cbbRegion.Value);
end;

procedure TABMConcesionariasForm.CargarComunas(CodigoRegion: string);
var
    spComunas : TADOStoredProc;
begin

    spComunas := TADOStoredProc.Create(nil);
    spComunas.Connection := DMConnections.BaseCAC;
    spComunas.ProcedureName := 'ObtenerComunasChileRegion';
    spComunas.Parameters.Refresh;
    spComunas.Parameters.ParamByName('@CodigoRegion').Value := CodigoRegion;
    try
        spComunas.Open;
        if spComunas.RecordCount > 0 then
        begin
            cbbComuna.Clear;
            while not spComunas.Eof do begin
                cbbComuna.Items.InsertItem(spComunas.FieldByName('Descripcion').AsString, spComunas.FieldByName('CodigoComuna').Value);
                spComunas.Next;
            end;
            cbbComuna.ItemIndex := 0;
        end;
    finally
        spComunas.close;
    end;

end;


{TERMINO: TASK_008_JMA_20160429_Unificaci�n tablas datos Concesionaria}

end.
