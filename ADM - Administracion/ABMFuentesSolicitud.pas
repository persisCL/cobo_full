{-----------------------------------------------------------------------------
 File Name:  ABMFuentesSolicitud
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}

unit ABMFuentesSolicitud;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls, VariantComboBox,PeaTypes;
       
type
  TFormFuentesSolicitud = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    FuentesSolicitud: TADOTable;
    txt_CodigoFuenteSolicitud: TNumericEdit;
    Notebook: TNotebook;
    Label3: TLabel;
    txt_CantPregunta: TNumericEdit;
    cb_Desactivada: TCheckBox;
    txt_dominio: TEdit;
    Label2: TLabel;
    cbTipoFuenteSolicitud: TVariantComboBox;
    Label4: TLabel;
    Label5: TLabel;
    Panel1: TPanel;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializar: boolean;

  end;

var
  FormFuentesSolicitud: TFormFuentesSolicitud;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar esta fuente de solicitud?';
    MSG_DELETE_ERROR		= 'No se puede eliminar esta fuente de solicitud porque hay datos que dependen del misma.';
    MSG_DELETE_CAPTION		= 'Eliminar Fuente de Solicitud';
    MSG_ACTUALIZAR_ERROR  	= 'No se pudieron actualizar los datos de la fuente de solicitud.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Fuente de Solicitud';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n';
    MSG_MEDIOCONTACTO       = 'Debe seleccionar el medio de contacto';
    MSG_CANTPREGUNTA        = 'La cantidad de pregunta debe ser menor a 256.';
    MSG_ERROR_DOMINIO       = 'El dominio ya se encuentra cargado en otra fuente.';
{$R *.DFM}

procedure TFormFuentesSolicitud.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
    txt_CodigoFuenteSolicitud.Enabled	:= True;
end;

function TFormFuentesSolicitud.Inicializar: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([FuentesSolicitud]) then
		Result := False
	else begin
       	Notebook.PageIndex := 0;
		Result := True;
		DbList1.Reload;
	end;
end;

procedure TFormFuentesSolicitud.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormFuentesSolicitud.DBList1Click(Sender: TObject);
begin
	with FuentesSolicitud do begin
        CargarTiposFuenteSolicitud(cbTipoFuenteSolicitud, false, FieldByName('TipoFuente').AsString[1]);
		txt_CodigoFuenteSolicitud.Value	:= FieldByName('CodigoFuenteSolicitud').AsInteger;
		txt_Descripcion.text	        := FieldByName('Descripcion').AsString;
        txt_CantPregunta.Value          := FieldByName('CantidadPregunta').AsInteger;
        cb_Desactivada.Checked          := not (FieldByName('Activo').AsBoolean);
        txt_dominio.text	            := FieldByName('Dominio').AsString;
	end;
end;

procedure TFormFuentesSolicitud.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas  do begin
		FillRect(Rect);
        if Tabla.FieldByName('Activo').AsBoolean then begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clWindow
            else
                Sender.Canvas.Font.Color := clWindowText;
        end else begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clYellow
            else
                Sender.Canvas.Font.Color := clRed;
        end;

      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('CodigoFuenteSolicitud').AsString);
        TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldByName('Descripcion').AsString));
        TextOut(Cols[2], Rect.Top, Tabla.FieldByName('CantidadPregunta').AsString);
        TextOut(Cols[3], Rect.Top, iif(Tabla.FieldByName('Activo').Asboolean,MSG_SI,MSG_NO) );
        TextOut(Cols[4], Rect.Top, Tabla.FieldByName('Dominio').AsString);
	end;
end;

procedure TFormFuentesSolicitud.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado     := modi;
end;

procedure TFormFuentesSolicitud.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCampos;
    dblist1.Estado     := Alta;
end;

procedure TFormFuentesSolicitud.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormFuentesSolicitud.Limpiar_Campos;
begin
	txt_CodigoFuenteSolicitud.Clear;
	txt_Descripcion.Clear;
    cb_Desactivada.Checked:=false;
    txt_dominio.Clear;
end;

procedure TFormFuentesSolicitud.AbmToolbar1Close(Sender: TObject);
begin
    close;
end;

procedure TFormFuentesSolicitud.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			FuentesSolicitud.Delete;
		Except
			On E: Exception do begin
				FuentesSolicitud.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	VolverCampos;
	Screen.Cursor      := crDefault;
end;


{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: None
  Return Value: Sender: TObject

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormFuentesSolicitud.BtnAceptarClick(Sender: TObject);
begin
    if not ValidateControls([txt_Descripcion, txt_CantPregunta,txt_dominio],
                [trim(txt_Descripcion.text) <> '',
                (trim(txt_CantPregunta.Text)='') or (txt_CantPregunta.ValueInt<256),
                (trim(txt_dominio.Text)='') or (trim(QueryGetValue(DMConnections.BaseCAC,format('SELECT CodigoFuenteSolicitud FROM  FuentesSolicitud  WITH (NOLOCK) WHERE Dominio = ''%s'' and CodigoFuenteSolicitud<>%d',[trim(txt_dominio.Text),txt_CodigoFuenteSolicitud.valueint])))='')],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_DESCRIPCION, MSG_CANTPREGUNTA,MSG_ERROR_DOMINIO]) then exit;

 	Screen.Cursor := crHourGlass;
	With FuentesSolicitud do begin
		Try
            if DbList1.Estado = Alta then begin
                Append;
                txt_CodigoFuenteSolicitud.value := QueryGetValueInt(DMConnections.BaseCAC,
                    'Select ISNULL(MAX(CodigoFuenteSolicitud), 0) + 1 FROM FuentesSolicitud  WITH (NOLOCK) ');
                FieldByName('CodigoFuenteSolicitud').value		:= txt_CodigoFuenteSolicitud.value;
            end else Edit;
            FieldByName('Descripcion').AsString	 := txt_Descripcion.text;
            FieldByName('CantidadPregunta').AsInteger	:= txt_CantPregunta.ValueInt;
            FieldByName('Activo').AsBoolean := not(cb_Desactivada.Checked);
            FieldByName('Dominio').AsString := trim(txt_dominio.Text);
            FieldByName('TipoFuente').AsString := trim(cbTipoFuenteSolicitud.Value);
            Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFormFuentesSolicitud.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormFuentesSolicitud.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormFuentesSolicitud.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormFuentesSolicitud.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    txt_CodigoFuenteSolicitud.Enabled	:= False;
    txt_dominio.Enabled:=true;
    txt_Descripcion.SetFocus;
end;

end.
