{-----------------------------------------------------------------------------
 File Name: ABMRegiones
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura


Firma       :   SS_1147_MBE_20140814
Description :   Se agrega la validaci�n de que el Insertar o Modificar una regi�n, �sta
                no exista en otro registro para el mismo pa�s.

                Se elimina la funci�n TieneComunas, pues s�lo busca por el c�digo de regi�n
                sin considerar el c�digo pais.

                Se corrige la obtenci�n del c�digo de regi�n (MAX) + 1, pues s�lo trae dos d�gitos

-----------------------------------------------------------------------------}
unit ABMRegiones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  Util, UtilProc, OleCtrls, DmiCtrls, Mask, ComCtrls, PeaProcs, DMConnection,
  ADODB, DPSControls, Peatypes;

type
  TFormRegiones = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label15: TLabel;
    Panel2: TPanel;
    Panel1: TPanel;
    Notebook: TNotebook;
    txt_Descripcion: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    txt_CodigoRegion: TEdit;
    cb_pais: TComboBox;
    Regiones: TADOTable;
    qry_Paises: TADOQuery;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure cb_paisChange(Sender: TObject);
  private
	{ Private declarations }
	procedure Limpiar_Campos;
  public
	{ Public declarations }
    function Inicializa(MDIChild: Boolean): Boolean;
  end;

var
  FormRegiones: TFormRegiones;

implementation

resourcestring
    MSG_DELETE_QUESTION		  = '�Est� seguro de querer eliminar la Regi�n seleccionada?';
    MSG_DELETE_ERROR		  = 'No se pudo eliminar la Regi�n seleccionada';
    MSG_ERROR_RELATED_RECORDS = 'No se pudo eliminar la Regi�n seleccionada porque existen Comunas relacionados';
    MSG_DELETE_CAPTION		  = 'Eliminar Regi�n';
    MSG_ACTUALIZAR_ERROR	  = 'No se pudieron actualizar los datos de la Regi�n seleccionada.';
    MSG_ACTUALIZAR_CAPTION	  = 'Actualizar Regi�n';
    MSG_DELETE_COMUNAS        = 'La Regi�n tiene comunas vinculadas.';

{$R *.DFM}

function TFormRegiones.Inicializa(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	if not OpenTables([Regiones, qry_Paises]) then
		Result := False
	else begin
        CargarPaises(DMConnections.BaseCAC, cb_pais, PAIS_CHILE);
		qry_paises.Close;
	    DbList1.Reload;
	    Result := True;
(*
    	cb_pais.items.Clear;
        While not qry_paises.eof do begin
			cb_pais.items.add(
            	PadR(qry_paises.FieldByName('Descripcion').AsString, 200 , ' ') +
			 	Trim(qry_paises.FieldByName('CodigoPais').AsString));
            qry_paises.Next;
		end;
		qry_paises.Close;
	    DbList1.Reload;
	    Result := True;
*)
	end;
   	Notebook.PageIndex := 0;
end;

procedure TFormRegiones.BtnCancelarClick(Sender: TObject);
begin
   	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

procedure TFormRegiones.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var
	i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[1], Rect.Top, tabla.Fieldbyname('Descripcion').AsString);
		for i := 0 to cb_pais.items.count - 1 do begin
			if StrRight(cb_pais.items[i], 3) =
			  Trim(FieldByName('CodigoPais').AsString) then begin
				TextOut(Cols[0] + 1, Rect.Top, Trim(Copy(cb_pais.items[i], 1, 200)));
				Break;
			end;
		end;
	end;
end;

procedure TFormRegiones.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    dblist1.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;
    txt_Descripcion.SetFocus;
end;

procedure TFormRegiones.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormRegiones.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormRegiones.DBList1Delete(Sender: TObject);

    function ExistenComunasRelacionadas : Boolean;                                                                         
        resourcestring                                                                                                          //SS_1147_MBE_20140814
            vSQL = 'SELECT 1 FROM Comunas (NOLOCK) WHERE CodigoPais = ''%s'' AND CodigoRegion = ''%s'' ';                       //SS_1147_MBE_20140814
        var                                                                                                                     //SS_1147_MBE_20140814
            vTiene : Integer;                                                                                                   //SS_1147_MBE_20140814
    begin
        {                                                                                                                       //SS_1147_MBE_20140814
        Result := StrtoBool(QueryGetValue(DMConnections.BaseCAC,                                                                //SS_1147_MBE_20140814
                                          'select dbo.TieneComunas(''' + txt_CodigoRegion.Text + ''')'))                        //SS_1147_MBE_20140814
        }                                                                                                                       //SS_1147_MBE_20140814

        vTiene := QueryGetValueInt(DMConnections.BaseCAC, Format(vSQL, [    Regiones.FieldByName('CodigoPais').AsString,        //SS_1147_MBE_20140814
                                                                            Regiones.FieldByName('Codigoregion').AsString ]     //SS_1147_MBE_20140814
                                                                ));                                                             //SS_1147_MBE_20140814
        Result := (vTiene = 1);                                                                                                 //SS_1147_MBE_20140814
    end;

begin
    if ExistenComunasRelacionadas then begin
        MsgBox(MSG_ERROR_RELATED_RECORDS, MSG_DELETE_CAPTION, MB_ICONSTOP);
        DbList1.Estado  := Normal;
        DbList1.Enabled := True;
        Exit;
    end;

    If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        try
            Screen.Cursor := crHourGlass;
            try
                Regiones.Delete;
            Except
                On E: Exception do begin
                    Regiones.Cancel;
                    MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
                end;
            end;
            DbList1.Reload;
            DbList1.Estado     := Normal;
            DbList1.Enabled    := True;
            Notebook.PageIndex := 0;
        finally
            Screen.Cursor := crDefault;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBList1Insert
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormRegiones.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
//    txt_CodigoRegion.Text:=trim(QueryGetValue(DMConnections.BaseCAC,'SELECT MAX(CodigoRegion)+1 AS CodigoRegion From Regiones  WITH (NOLOCK) '));         //SS_1147_MBE_20140814
    txt_CodigoRegion.Text := '';                                                                             //SS_1147_MBE_20140814
	txt_Descripcion.setFocus;
//    CargarPaises(DMConnections.BaseCAC, cb_pais, PAIS_CHILE);                                              //SS_1147_MBE_20140814
end;

procedure TFormRegiones.BtnAceptarClick(Sender: TObject);
resourcestring                                                                                               //SS_1147_MBE_20140814
    MSG_PAIS        = 'Falta seleccionar el Pa�s al que pertenece la Regi�n';                                //SS_1147_MBE_20140814
    MSG_DESC        = 'Debe ingresar la descripci�n de la Regi�n';                                           //SS_1147_MBE_20140814
    MSG_CODIGO      = 'Falta el c�digo de la regi�n';                                                        //SS_1147_MBE_20140814
    MSG_YA_EXISTE   = 'Ya existe una Regi�n con esta Descripci�n';                                           //SS_1147_MBE_20140814
    MSG_SQL_INSERT  = 'SELECT 1 FROM Regiones (NOLOCK) WHERE CodigoPais = ''%s'' AND Descripcion = ''%s'' '; //SS_1147_MBE_20140814
    MSG_SQL_UPDATE  = ' AND CodigoRegion <> ''%s'' ';                                                        //SS_1147_MBE_20140814
var
    vSQL : string;                                                                                           //SS_1147_MBE_20140814
    vExiste : Boolean;                                                                                       //SS_1147_MBE_20140814
begin

    if not ValidateControls(    [txt_CodigoRegion, cb_pais, txt_Descripcion],                                //SS_1147_MBE_20140814
                                [Trim(txt_CodigoRegion.Text) <> '', cb_pais.ItemIndex >= 0, Trim(txt_Descripcion.Text) <> ''],                  //SS_1147_MBE_20140814
                                Caption,                                                                     //SS_1147_MBE_20140814
                                [MSG_CODIGO, MSG_PAIS, MSG_DESC]) then Exit;                                 //SS_1147_MBE_20140814

    //ver que no exista otra regi�n con el mismo nombre                                                      //SS_1147_MBE_20140814
    if DBList1.Estado = Alta then begin                                                                      //SS_1147_MBE_20140814
        vSQL := Format(MSG_SQL_INSERT, [StrRight(cb_pais.Text,3), txt_Descripcion.Text]);                    //SS_1147_MBE_20140814
    end                                                                                                      //SS_1147_MBE_20140814
    else begin                                                                                               //SS_1147_MBE_20140814
        vSQL := MSG_SQL_INSERT + MSG_SQL_UPDATE;                                                             //SS_1147_MBE_20140814
        vSQL := Format(vSQL, [  StrRight(cb_pais.Text,3),                                                    //SS_1147_MBE_20140814
                                Trim(txt_Descripcion.Text),                                                  //SS_1147_MBE_20140814
                                Regiones.FieldByName('CodigoRegion').AsString]);                             //SS_1147_MBE_20140814
    end;                                                                                                     //SS_1147_MBE_20140814

    vExiste := (QueryGetValueInt(DMConnections.BaseCAC, vSQL) = 1);                                          //SS_1147_MBE_20140814
    if vExiste then begin                                                                                    //SS_1147_MBE_20140814
        MsgBoxBalloon(MSG_YA_EXISTE, Caption, MB_ICONEXCLAMATION, txt_Descripcion);                          //SS_1147_MBE_20140814
        Exit;                                                                                                //SS_1147_MBE_20140814
    end;                                                                                                     //SS_1147_MBE_20140814


 	Screen.Cursor := crHourGlass;
	With Regiones do begin
		Try
			if DbList1.Estado = Alta then
				Append
			else
				Edit;
		    FieldByName('CodigoPais').AsString 	 		:= StrRight(cb_pais.items[cb_pais.ItemIndex], 3);
			FieldByName('Descripcion').AsString  		:= Trim(txt_Descripcion.text);
			FieldByName('CodigoRegion').AsString  		:= txt_CodigoRegion.text;
  			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
    groupb.Enabled     := False;
  	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	DbList1.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormRegiones.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormRegiones.BtnSalirClick(Sender: TObject);
begin
     close;
end;

{------------------------------------------
                cb_paisChange

Author      : mbecerra
Date        : 14-08-2014
Description :   SS_1147_MBE_20140814
                Obtiene el siguiente c�digo de regi�n en caso de ser una alta

-----------------------------------------------}
procedure TFormRegiones.cb_paisChange(Sender: TObject);
resourcestring
    MSG_SQL_CODIGO  =   'SELECT ISNULL(MAX(CodigoRegion),0) + 1 FROM Regiones WITH (NOLOCK) ' +
                        ' WHERE CodigoPais = ''%s'' ';

var
    vCodigo : Integer;
    vSQL : string;
begin
    //obtiene el siguiente c�digo de regi�n en caso de ser una alta
    if (DBList1.Estado = Alta) and (cb_pais.ItemIndex >= 0) then begin
        vSQL := Format(MSG_SQL_CODIGO, [StrRight(cb_pais.Text,3)]);
        vCodigo := QueryGetValueInt(DMConnections.BaseCAC, vSQL);
        txt_CodigoRegion.Text := Format('%.03d', [vCodigo]);
    end;

end;

procedure TFormRegiones.Limpiar_Campos;
begin
	txt_codigoregion.clear;
	txt_descripcion.clear;
	cb_pais.itemindex := -1;
end;


procedure TFormRegiones.DBList1Click(Sender: TObject);
Var
	i, Index: Integer;
begin
	With Regiones do begin
		txt_codigoRegion.text := FieldByName('CodigoRegion').AsString;
		txt_descripcion.text := Trim(FieldByName('Descripcion').AsString);
		Index := 0;
		for i := 0 to cb_pais.items.count - 1 do begin
			if StrRight(cb_pais.items[i], 3) = FieldByName('CodigoPais').AsString then begin
				Index := i;
				Break;
			end;
		end;
		cb_pais.ItemIndex := Index;
	end;
end;

end.
