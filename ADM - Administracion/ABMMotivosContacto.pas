unit ABMMotivosContacto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB, DMConnection,
  CheckLst, ComCtrls, variants, DPSControls;

type
  TFormMotivosContacto = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    Panel1: TPanel;
    Notebook: TNotebook;
    MotivosContacto: TADOTable;
    EliminarVinculoTipoOS: TADOQuery;
    InsertarMotivoContacto: TADOStoredProc;
    Panel3: TPanel;
    Label15: TLabel;
    neCodigoMotivoContacto: TNumericEdit;
    txtDescripcion: TEdit;
    Label1: TLabel;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;

	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);

  private
	{ Private declarations }
    procedure Limpiar_Campos;
  public
	{ Public declarations }
	Function Inicializa : boolean;
  end;

var
  FormMotivosContacto  : TFormMotivosContacto;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Motivo de Contacto?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Motivo de Contacto porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Motivo de Contacto';
    MSG_TIPO_ORDEN_SERVICIO = 'No se pudieron actualizar los Tipos de Orden de Servicio asociados al Motivo de Contacto.';

    MSG_DESCRIPCION         = 'Debe especificarse la descripci�n del Motivo de Contacto';
    MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar el Motivo de Contacto.';
    CAPTION_VALIDAR_MOTIVO_CONTACTO	= 'Actualizar Motivo de Contacto';


    CAPTION_TIPO_ORDEN_SERVICIO = 'Actualizar Tipos de Orden de Servicio';
    MSG_INSERTAR_MOTIVO_CONTACTO = 'No se pudo insertar el nuevo Motivo de Contacto.';
    CAPTION_INSERTAR_MOTIVO_CONTACTO = 'Insertar Motivo Contacto';



{$R *.DFM}

function TFormMotivosContacto.Inicializa: boolean;
Var S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);

	if not OpenTables([MotivosContacto]) then
    	Result := False
    else begin
    	Result := True;
       	Lista.Reload;
        Lista.Enabled := true;
        Lista.SetFocus;
	end;
   	Notebook.PageIndex := 0;
end;

procedure TFormMotivosContacto.Limpiar_Campos();
begin
	neCodigoMotivoContacto.Clear;
	txtDescripcion.Clear;
end;

function TFormMotivosContacto.ListaProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	Result := True;
	Texto :=  PadR(Trim(Tabla.FieldByName('CodigoMotivo').AsString), 4, ' ' ) + ' '+
	  Tabla.FieldByName('Descripcion').AsString;
end;

procedure TFormMotivosContacto.BtSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormMotivosContacto.ListaInsert(Sender: TObject);
begin
	Screen.Cursor   := crHourGlass;
	Lista.Estado    := Alta;
	Limpiar_Campos;
    txtDescripcion.Enabled 	:= true;
	Lista.Enabled   := False;
	Notebook.PageIndex := 1;
   	txtDescripcion.SetFocus;
	Screen.Cursor   := crDefault;
end;

procedure TFormMotivosContacto.ListaEdit(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Modi;
 	txtDescripcion.Enabled 	        := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
   	txtDescripcion.SetFocus;
	Screen.Cursor    := crDefault;
end;

procedure TFormMotivosContacto.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) = IDYES then begin
        DMConnections.BaseCAC.BeginTrans;
        try
            try
                MotivosContacto.Delete;
                DMConnections.BaseCAC.CommitTrans;
            except
                On E: EDataBaseError do begin
                    MotivosContacto.Cancel;
                    DMConnections.BaseCAC.RollbackTrans;
                    MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
                end else
                    DMConnections.BaseCAC.RollbackTrans;;
            end;
        finally
            Lista.Estado     	:= Normal;
            Lista.Enabled    	:= True;
            txtDescripcion.Enabled := False;
            Notebook.PageIndex 	:= 0;
            Screen.Cursor      	:= crDefault;
		    Lista.Reload;
        end;
    end;
end;

procedure TFormMotivosContacto.ListaClick(Sender: TObject);
begin
	with MotivosContacto do begin
        neCodigoMotivoContacto.Value := FieldByName('CodigoMotivo').AsInteger;
        txtDescripcion.text	 := Trim(FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormMotivosContacto.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos();
end;

procedure TFormMotivosContacto.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormMotivosContacto.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, FieldByName('CodigoMotivo').AsString);
		TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormMotivosContacto.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

procedure TFormMotivosContacto.BtnAceptarClick(Sender: TObject);

  function execInsertarMotivoContacto:Integer;
  begin
      result := 0;
      try
          with InsertarMotivoContacto do begin
              Parameters.ParamByName('@Descripcion').Value := txtDescripcion.Text;
              execProc;
              result := Parameters.ParamByName('@CodigoMotivoContacto').value;
          end;
      except
          On E: Exception do begin
              MsgBoxErr(MSG_INSERTAR_MOTIVO_CONTACTO, E.message, CAPTION_INSERTAR_MOTIVO_CONTACTO, MB_ICONSTOP);
          end;
      end;
  end;

  function ActualizarMotivoContacto: Boolean;
  begin
      result := true;
      with MotivosContacto do begin
          edit;
	      try
    		  FieldByName('Descripcion').AsString 	  := Trim(txtDescripcion.Text);
			  Post;
   		  except
              On E: Exception do begin
                  Result := False;
 			      Cancel;
			      MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, CAPTION_VALIDAR_MOTIVO_CONTACTO, MB_ICONSTOP);
              end;
	   	  end;
	  end;
  end;

begin
	Screen.Cursor := crHourGlass;
    if not ValidateControls([txtDescripcion],
      [Trim(txtDescripcion.text) <> ''],
      CAPTION_VALIDAR_MOTIVO_CONTACTO,
      [MSG_DESCRIPCION]) then exit;

	Screen.Cursor := crHourGlass;
    try
        DMConnections.BaseCAC.BeginTrans;
        // Ahora agrego o modifico el motivo de contacto.
        if (Lista.Estado = Alta) then begin
            neCodigoMotivoContacto.valueint := ExecInsertarMotivoContacto;
            if (neCodigoMotivoContacto.ValueInt = 0) then begin
                DMConnections.BaseCAC.RollbackTrans;
                exit;
            end;
        end else
            if not ActualizarMotivoContacto then begin
                DMConnections.BaseCAC.RollbackTrans;
                exit;
            end;
        DMConnections.BaseCAC.CommitTrans;
    finally
        txtDescripcion.Enabled 	:= False;
	    Lista.Estado     := Normal;
    	Lista.Enabled    := True;
	    Lista.SetFocus;
    	Notebook.PageIndex := 0;
	    Screen.Cursor 	   := crDefault;
    end;
end;

procedure TFormMotivosContacto.BtnCancelarClick(Sender: TObject);
begin
 	txtDescripcion.Enabled 	        := False;
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;

procedure TFormMotivosContacto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormMotivosContacto.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

end.
