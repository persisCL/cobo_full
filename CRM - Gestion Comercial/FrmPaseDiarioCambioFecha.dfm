object PaseDiarioConsultaEdicionForm: TPaseDiarioConsultaEdicionForm
  Left = 224
  Top = 138
  Caption = 'Pase Diario - Consulta por patente'
  ClientHeight = 466
  ClientWidth = 791
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 799
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 791
    Height = 81
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 791
      Height = 76
      Align = alTop
      Caption = 'Filtros'
      TabOrder = 0
      DesignSize = (
        791
        76)
      object Label1: TLabel
        Left = 11
        Top = 48
        Width = 70
        Height = 13
        Caption = 'Fecha  &Desde:'
        FocusControl = txt_FechaDesde
      end
      object Label2: TLabel
        Left = 225
        Top = 48
        Width = 64
        Height = 13
        Caption = '&Fecha Hasta:'
        FocusControl = txt_FechaHasta
      end
      object Label6: TLabel
        Left = 12
        Top = 21
        Width = 37
        Height = 13
        Caption = '&Patente'
      end
      object Label4: TLabel
        Left = 224
        Top = 21
        Width = 79
        Height = 13
        Caption = '&N'#250'mero de Serie'
      end
      object lblMostrar: TLabel
        Left = 472
        Top = 20
        Width = 96
        Height = 13
        Caption = 'Mostrar los primeros '
      end
      object lblRegistros: TLabel
        Left = 633
        Top = 21
        Width = 67
        Height = 13
        Caption = 'Pases Diarios.'
      end
      object txt_FechaDesde: TDateEdit
        Left = 90
        Top = 44
        Width = 122
        Height = 21
        AutoSelect = False
        TabOrder = 3
        Date = -693594.000000000000000000
      end
      object txt_FechaHasta: TDateEdit
        Left = 309
        Top = 44
        Width = 122
        Height = 21
        AutoSelect = False
        TabOrder = 4
        Date = 36526.000000000000000000
      end
      object txtPatente: TEdit
        Left = 90
        Top = 17
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 0
      end
      object btn_Filtrar: TButton
        Left = 704
        Top = 10
        Width = 75
        Height = 25
        Hint = 'Filtrar Pases Diarios'
        Anchors = [akTop, akRight]
        Caption = '&Filtrar'
        Default = True
        TabOrder = 5
        OnClick = btn_FiltrarClick
      end
      object btn_Limpiar: TButton
        Left = 704
        Top = 40
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '&Limpiar'
        TabOrder = 6
        OnClick = btn_LimpiarClick
      end
      object txtCantidadRegistros: TNumericEdit
        Left = 571
        Top = 17
        Width = 57
        Height = 21
        AutoSize = False
        TabOrder = 2
      end
      object txtNumeroSerie: TNumericEdit
        Left = 310
        Top = 17
        Width = 154
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 22
        TabOrder = 1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 81
    Width = 791
    Height = 344
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 1
    object dbl_DayPass: TDBListEx
      Left = 0
      Top = 0
      Width = 791
      Height = 344
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          MinWidth = 80
          MaxWidth = 150
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_DayPassColumns0HeaderClick
          FieldName = 'Patente'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          MinWidth = 120
          MaxWidth = 200
          Header.Caption = 'N'#250'mero serie'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_DayPassColumns0HeaderClick
          FieldName = 'ContractSerialNumber'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          MinWidth = 80
          MaxWidth = 250
          Header.Caption = 'Tipo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_DayPassColumns0HeaderClick
          FieldName = 'TipoDayPassDesc'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          MinWidth = 80
          MaxWidth = 250
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_DayPassColumns0HeaderClick
          FieldName = 'DescripcionEstado'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 125
          MinWidth = 100
          MaxWidth = 250
          Header.Caption = 'Fecha venta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_DayPassColumns0HeaderClick
          FieldName = 'FechaVenta'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 125
          MinWidth = 100
          MaxWidth = 250
          Header.Caption = 'Fecha uso'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_DayPassColumns0HeaderClick
          FieldName = 'FechaUso'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 125
          MinWidth = 100
          MaxWidth = 250
          Header.Caption = 'Fecha vencimiento'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaVencimiento'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 125
          MinWidth = 100
          MaxWidth = 250
          Header.Caption = 'Fecha devoluci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_DayPassColumns0HeaderClick
          FieldName = 'FechaDevolucion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          MinWidth = 100
          MaxWidth = 500
          Header.Caption = 'Nombre contacto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_DayPassColumns0HeaderClick
          FieldName = 'NombreContacto'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          MinWidth = 100
          MaxWidth = 250
          Header.Caption = 'RUT contacto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_DayPassColumns0HeaderClick
          FieldName = 'RUTContacto'
        end>
      DataSource = dsObtenerDayPass
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = btnCambiarFechaClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 425
    Width = 791
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 791
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        791
        41)
      object BtnSalir: TButton
        Left = 703
        Top = 7
        Width = 79
        Height = 26
        Hint = 'Salir del visualizador de Solicitudes'
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Salir'
        TabOrder = 0
        OnClick = BtnSalirClick
      end
      object btnCambiarFecha: TButton
        Left = 579
        Top = 7
        Width = 118
        Height = 26
        Hint = 'Cambia la Fecha de Utilizaci'#243'n del Pase Diario'
        Anchors = [akRight, akBottom]
        Caption = '&Cambiar Fecha'
        Enabled = False
        TabOrder = 1
        OnClick = btnCambiarFechaClick
      end
    end
  end
  object spObtenerDayPass: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterScroll = spObtenerDayPassAfterScroll
    CommandTimeout = 300
    ProcedureName = 'ObtenerDayPass'
    Parameters = <>
    Left = 168
    Top = 248
  end
  object dsObtenerDayPass: TDataSource
    DataSet = spObtenerDayPass
    Left = 200
    Top = 248
  end
  object spActualizarFechaUsoDayPass: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ActualizarFechaUsoDayPass'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroDayPass'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaUso'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 272
    Top = 249
  end
end
