unit FrmDatosContactoComunicacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, MaskCombo, peatypes, util, utilproc,
  frmDatoPesona, frePersonas, FreDomicilio, FreTelefono, DB, ADODB;

type
  TFormDatosContactoComunicacion = class(TForm)
    Label4: TLabel;
    pnl_Botones: TPanel;
    txt_Rut: TEdit;
    pnl_DatosPersona: TPanel;
    lblApellido: TLabel;
    txt_apellido: TEdit;
    txt_apellidoMaterno: TEdit;
    lblApellidoMaterno: TLabel;
    txtNumeroCalle: TEdit;
    Label2: TLabel;
    txtEmailParticular: TEdit;
    txtTelefonoParticular: TEdit;
    txtDomicilio: TEdit;
    txt_nombre: TEdit;
    lblNombre: TLabel;
    Label1: TLabel;
    Label12: TLabel;
    Label21: TLabel;
    ObtenerDatosPersona: TADOStoredProc;
    ObtenerMediosComunicacionPersonaFormato: TADOStoredProc;
    ObtenerDomiciliosPersona: TADOStoredProc;
    btnAceptar: TButton;
    btnSalir: TButton;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure txt_RutKeyPress(Sender: TObject; var Key: Char);
    procedure txt_RutChange(Sender: TObject);
  private
    { Private declarations }
    FCodigoPersona: Integer;
    function GetContactoComunicacion: TDatosContactoComunicacion;
    procedure SetCodigoPersona(const Value: Integer);
    procedure Limpiar;
  public
    { Public declarations }
    FDatosContactoComunicacion: TDatosContactoComunicacion;
    function Inicializar(): Boolean;
    function InicializarModificacion(cont: TDatosContactoComunicacion): boolean;
    property ContactoComunicacion: TDatosContactoComunicacion read GetContactoComunicacion;
    property CodigoPersona: Integer read FCodigoPersona write SetCodigoPersona;
  end;

var
  FormDatosContactoComunicacion: TFormDatosContactoComunicacion;

implementation

uses DMConnection, Peaprocs;

{$R *.dfm}

function TFormDatosContactoComunicacion.Inicializar(): Boolean;
begin
    fillChar(FDatosContactoComunicacion, sizeof(TDatosContactoComunicacion), #0);
    FCodigoPersona := -1;
    Result := True;
end;

function TFormDatosContactoComunicacion.GetContactoComunicacion: TDatosContactoComunicacion;
var
    Tmp: TDatosContactoComunicacion;
begin
    //Guardo en la Variable los datos de los Campos
    tmp := FDatosContactoComunicacion;
    with Tmp do begin
        Persona.CodigoPersona   := FCodigoPersona;
        Persona.NumeroDocumento := Trim(txt_Rut.Text);
        Persona.Apellido        := Trim(txt_Apellido.Text);
        Persona.ApellidoMaterno := Trim(txt_ApellidoMaterno.Text);
        Persona.Nombre          := Trim(txt_Nombre.Text);
        Domicilio               := Trim(txtDomicilio.Text);
        NumeroCalle             := Trim(txtNumeroCalle.Text);
        Telefono                := Trim(txtTelefonoParticular.Text);
        Email                   := Trim(txtEmailParticular.Text);
    end;
    Result := tmp;
end;


procedure TFormDatosContactoComunicacion.btnAceptarClick(Sender: TObject);
ResourceString
    CAPTION_VALIDAR_DATOS_CONTACTO  = 'Validar datos del Contacto';
    MSG_VALIDAR_EMAIL               = 'La direcci�n de correo especificada es err�nea';
    MSG_VALIDAR_RUT                 = 'El N�mero de documento es incorrecto';
    MSG_VALIDAR_APELLIDO            = 'Debe ingresar el apellido.';
    MSG_VALIDAR_NOMBRE              = 'Debe ingresar el nombre.';
begin
    if length(Trim(txt_Rut.Text)) < 7  then begin
        MsgBoxBalloon(MSG_VALIDAR_RUT, CAPTION_VALIDAR_DATOS_CONTACTO, MB_ICONSTOP, txt_Rut);
        txt_Rut.SetFocus;
        Exit;
    end;

    if txt_apellido.Enabled then begin
        if  (trim(txt_apellido.Text) = '') then begin
            MsgBoxBalloon(MSG_VALIDAR_APELLIDO, CAPTION_VALIDAR_DATOS_CONTACTO, MB_ICONSTOP, txt_apellido);
            txt_apellido.SetFocus;
            Exit;
        end;
        if  (trim(txt_nombre.Text) = '') then begin
            MsgBoxBalloon(MSG_VALIDAR_APELLIDO, CAPTION_VALIDAR_DATOS_CONTACTO, MB_ICONSTOP, txt_nombre);
            txt_nombre.SetFocus;
            Exit;
        end;
        if (Trim(txtemailParticular.Text) <> '') and not IsValidEmailList(Trim(txtemailParticular.Text)) then begin
            MsgBoxBalloon(MSG_VALIDAR_EMAIL, CAPTION_VALIDAR_DATOS_CONTACTO, MB_ICONSTOP, txtemailParticular);
            txtemailParticular.SetFocus;
            Exit;
        end;
    end;

    ModalResult := mrOK;
end;

procedure TFormDatosContactoComunicacion.btnSalirClick(Sender: TObject);
begin
    Close;
end;

function TFormDatosContactoComunicacion.InicializarModificacion(
  cont: TDatosContactoComunicacion): boolean;
begin
    FDatosContactoComunicacion  := cont;
    txt_Rut.Text                := trim(Cont.Persona.NumeroDocumento);
    txt_Rut.OnChange(txt_Rut);
    if cont.Persona.CodigoPersona < 0 then begin
        txt_apellido.Text           := Cont.Persona.Apellido;
        txt_apellidoMaterno.Text    := Cont.Persona.ApellidoMaterno;
        txt_nombre.Text             := Cont.Persona.Nombre;
        txtDomicilio.Text           := Cont.Domicilio;
        txtNumeroCalle.Text         := Cont.NumeroCalle;
        txtTelefonoParticular.Text  := Cont.Telefono;
        txtEmailParticular.Text     := Cont.Email;
    end;
    Result := True;
end;

procedure TFormDatosContactoComunicacion.txt_RutKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9','K','k', #8]) then
       Key := #0;
end;

procedure TFormDatosContactoComunicacion.txt_RutChange(Sender: TObject);
begin
    if Length(txt_Rut.Text) >= 7 then begin
        ObtenerDatosPersona.Parameters.ParamByName('@CodigoPersona').Value := null;
        ObtenerDatosPersona.Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
        ObtenerDatosPersona.Parameters.ParamByName('@NumeroDocumento').Value := txt_Rut.Text;
        ObtenerDatosPersona.Open;
        if ObtenerDatosPersona.RecordCount > 0 then begin
            // Datos de la persona
            CodigoPersona := ObtenerDatosPersona.FieldByName('CodigoPersona').Value;
            txt_apellido.Text := ObtenerDatosPersona.FieldByName('Apellido').Value;
            txt_apellidoMaterno.Text := ObtenerDatosPersona.FieldByName('ApellidoMaterno').Value;
            txt_nombre.Text := ObtenerDatosPersona.FieldByName('Nombre').Value;

            // telefono
            ObtenerMediosComunicacionPersonaFormato.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
            ObtenerMediosComunicacionPersonaFormato.Parameters.ParamByName('@Formato').Value := TMC_TELEFONO;
            ObtenerMediosComunicacionPersonaFormato.Open;
            if ObtenerMediosComunicacionPersonaFormato.RecordCount > 0 then
                txtTelefonoParticular.Text := ObtenerMediosComunicacionPersonaFormato.FieldByName('Detalle').Value;

            ObtenerMediosComunicacionPersonaFormato.close;

            // Email
            ObtenerMediosComunicacionPersonaFormato.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
            ObtenerMediosComunicacionPersonaFormato.Parameters.ParamByName('@Formato').Value := TMC_EMAIL;
            ObtenerMediosComunicacionPersonaFormato.Open;
            if ObtenerMediosComunicacionPersonaFormato.RecordCount > 0 then
                txtTelefonoParticular.Text := ObtenerMediosComunicacionPersonaFormato.FieldByName('Detalle').Value;

            ObtenerMediosComunicacionPersonaFormato.close;

            // Calle
            ObtenerDomiciliosPersona.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
            ObtenerDomiciliosPersona.Parameters.ParamByName('@CodigoDomicilio').Value := Null;
            ObtenerDomiciliosPersona.Parameters.ParamByName('@CodigoTipoDomicilio').Value := Null;
            ObtenerDomiciliosPersona.Parameters.ParamByName('@Principal').Value := True;
            ObtenerDomiciliosPersona.Open;
            if ObtenerDomiciliosPersona.RecordCount > 0 then begin
                txtDomicilio.Text := ObtenerDomiciliosPersona.FieldByName('DireccionCompleta').Value;
                txtNumeroCalle.Text := ObtenerDomiciliosPersona.FieldByName('Numero').Value;
            end;
            ObtenerDomiciliosPersona.Close;

        end else CodigoPersona := -1;
        ObtenerDatosPersona.Close;
    end else CodigoPersona := -1;
end;

procedure TFormDatosContactoComunicacion.SetCodigoPersona(
  const Value: Integer);
begin
  FCodigoPersona := Value;
  EnableControlsInContainer(pnl_DatosPersona, FCodigoPersona < 1);
  if FCodigoPersona < 1 then Limpiar;
end;

procedure TFormDatosContactoComunicacion.Limpiar;
begin
    txt_apellido.Clear;
    txt_apellidoMaterno.Clear;
    txt_nombre.Clear;
    txtDomicilio.Clear;
    txtNumeroCalle.Clear;
    txtTelefonoParticular.Clear;
    txtEmailParticular.Clear;
end;

end.
