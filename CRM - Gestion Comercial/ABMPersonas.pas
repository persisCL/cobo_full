{

Etiqueta	:	TASK_077_MGO_20161012
Descripci�n	:	Redise�o completo del ABM con datos de contacto y domicilio

}


unit ABMPersonas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList,
  ToolWin, ComCtrls, UtilProc, Util, DBClient, DBCtrls, Mask, Provider,
  StrUtils, DSIntf, PatronBusqueda, XMLIntf, XMLDoc, UtilDB, Validate, DateEdit,
  FreDomicilio, FreTelefono, DmiCtrls, Convenios, PeaTypes, PeaProcs;


resourcestring
	MSG_ERROR_UPDATE = 'Error Actualizando Registro';
 	MSG_ERROR_INSERT = 'Error Agregando Registro';
  	MSG_ERROR_DELETE = 'Error Eliminando Registro';
   	MSG_ERROR_SELECT = 'Error Consultando Registros';
    MSG_ERROR_RUT    = 'RUT incorrecto, reintente por favor';
    MSG_ERROR_RUT_EXISTE    = 'El RUT ingresado ya Existe.';
    MSG_ERROR_SEXO   = 'Sexo incorrecto, debe ser F (Femenino) � M (M�sculino).';
    MSG_ERROR_NOMBRE = 'Debe indicar el Nombre.';
    MSG_ERROR_APELLIDO = 'Debe indicar el Apellido Paterno.';
    MSG_ERROR_CP     = 'El c�digo postal debe ser num�rico.';
    MSG_ERROR_RS     = 'Debe indicar la Raz�n Social de la empresa.';
    MSG_ERROR_GIRO   = 'Debe indicar el Giro de la empresa.';	
    MSG_GET_DATA     = 'Cargando datos, por favor espere...';
    HINT_SALIR       = 'Salir del ABM';
    HINT_AGREGAR     = 'Agregar Nuevo Cliente';
    HINT_ELIMINAR    = 'Eliminar Cliente';
    HINT_EDITAR      = 'Editar Cliente';
    ANSW_ELIMINAR    = 'Est� seguro de eliminar este Registro de Cliente?';
    ANSW_SALIR       = 'Confirma que desea salir?';
    MSG_ERROR_TURNO  = 'Debe tener un turno abierto para insertar o actualizar Clientes';   // TASK_126_MGO_20170127

    CAPTION_FORM     = 'ABM de Clientes';


type

  TStatusWindowHandle = type HWND;

  TClientDataSetAccess = Class(TClientDataSet);

  TfrmABMPersonas = class(TForm)
    pnlSuperior: TPanel;
    pnlControles: TPanel;
    pnlInferior: TPanel;
    Botonera: TToolBar;
    Imagenes: TImageList;
    btnSalir: TToolButton;
    ImageList1: TImageList;
    ToolButton2: TToolButton;
    btnAgregar: TToolButton;
    btnEditar: TToolButton;
    btnGuardar: TButton;
    btnCancelar: TButton;
    pg01: TPageControl;
    tabGeneral: TTabSheet;
    lbl01: TLabel;
    lbl02: TLabel;
    lbl03: TLabel;
    lblNombre: TLabel;
    lbl05: TLabel;
    lbl06: TLabel;
    tsMediosContacto: TTabSheet;
    tsDomicilio: TTabSheet;
    frmtlfFreTelefonoPrincipal: TFrameTelefono;
    frmtlfFreTelefonoSecundario: TFrameTelefono;
    lblEmail: TLabel;
    nbNBook_Email: TNotebook;
    edtEMailContacto: TEdit;
    edtEmailParticular: TEdit;
    frmdmclFMDomicilioPrincipal: TFrameDomicilio;
    dbgrdPersonas: TDBGrid;
    dsPersonas: TDataSource;
    cdsPersonas: TClientDataSet;
    spCRM_Personas_SELECT: TADOStoredProc;
    txtTop: TNumericEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    edtNombre_Filtro: TEdit;
    edtApellido_Filtro: TEdit;
    edtApellidoMaterno_Filtro: TEdit;
    edtRUT_Filtro: TEdit;
    edtSexo_Filtro: TEdit;
    btnBuscar: TButton;
    edtRUT: TEdit;
    edtApellido: TEdit;
    edtApellidoMaterno: TEdit;
    edtNombre: TEdit;
    edtSexo: TEdit;
    dateFechaNacimiento: TDateEdit;
    chkProspectos: TCheckBox;
    spCRM_Personas_INSERT: TADOStoredProc;
    spCRM_Personas_UPDATE: TADOStoredProc;
    lbl7: TLabel;
    edtGiro_Filtro: TEdit;
    lbl8: TLabel;
    cbbPersoneria: TComboBox;
    pnlJuridico: TPanel;
    lbl9: TLabel;
    lbl10: TLabel;
    edtGiro: TEdit;
    edtRazonSocial: TEdit;
    lbl11: TLabel;
    cbbPersoneria_Filtro: TComboBox;
    dtstprPersonas: TDataSetProvider;

    procedure FormCreate(Sender: TObject);
    procedure HabilitaBotones(Botones : string);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    function Inicializar(MDIChild: Boolean): Boolean;
    procedure PonerFocoEnPrimerControl;
    procedure HabilitarDeshabilitarControles(Estado : Boolean);
    procedure btnBuscarClick(Sender: TObject);
    function EsRUTValido(Rut: string): boolean;
    procedure Limpiar_Campos;
    procedure ObtenerDatosPersona;
    procedure dbgrdPersonasCellClick(Column: TColumn);
    procedure cdsPersonasAfterScroll(DataSet: TDataSet);
    procedure ActualizarClases;
    procedure dbgrdPersonasDblClick(Sender: TObject);
    procedure cbbPersoneriaChange(Sender: TObject);
    procedure edtRUTKeyPress(Sender: TObject; var Key: Char);			// TASK_126_MGO_20170127			
    procedure edtRUT_FiltroKeyPress(Sender: TObject; var Key: Char);	// TASK_126_MGO_20170127
    procedure edtSexoKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);		            // TASK_126_MGO_20170127
  private
    { Private declarations }
    CantidadRegistros           : integer;
    Posicion                    : TBookmark;
    Accion                      : Integer; // 1=agregar   2=eliminar  3=editar
    Retorno                     : Integer;
    MensajeError                : string;
    FCargandoDatos              : Boolean;
    NroRUT_                     : string;     // TASK_88_PAN_20170215
    Retorna_                    : Boolean;    // TASK_88_PAN_20170215
  public
    procedure Inicio(retorna: Boolean; var NroRUT: string);  // TASK_88_PAN_20170215
  published
  Persona                       : TPersonaConvenio;  // TASK_88_PAN_20170215

  end;

var
  frmABMPersonas: TfrmABMPersonas;

implementation

uses DMConnection, CommCtrl;

{$R *.dfm}
procedure TfrmABMPersonas.Inicio(retorna: Boolean; var NroRUT:  string);   // TASK_88_PAN_20170215
begin                                                                      // TASK_88_PAN_20170215
  Inicializar(False);                                                      // TASK_88_PAN_20170215
  btnAgregarClick(frmABMPersonas);                                         // TASK_88_PAN_20170215
  NroRUT_:= NroRUT;                                                        // TASK_88_PAN_20170215
  Retorna_:= retorna;                                                      // TASK_88_PAN_20170215
  if retorna then NroRUT:= NroRUT_;                                        // TASK_88_PAN_20170215
end;                                                                       // TASK_88_PAN_20170215

function TfrmABMPersonas.EsRUTValido(Rut: string): boolean;
begin
    result := (QueryGetValue(DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
                             StrLeft(Rut, (Length(Rut) -1))  +
                            ''',''' + Rut[Length(Rut)] + '''') <> '0');

end;

function TfrmABMPersonas.Inicializar(MDIChild: Boolean): Boolean;
resourcestring
    STR_TELEFONO_PRINCIPAL      = 'Tel�fono Principal';
	STR_TELEFONO_ALTERNATIVO    = 'Tel�fono Alternativo';
Var
	S: TSize;
begin
    if MDIChild then begin
        FormStyle := fsMDIChild;
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
    end else begin
        FormStyle := fsNormal;
        Visible := False;
    end;

    Limpiar_Campos;
	Accion := 0;
    HabilitarDeshabilitarControles(False);
    frmtlfFreTelefonoPrincipal.Inicializa(STR_TELEFONO_PRINCIPAL, false);
    frmtlfFreTelefonoSecundario.Inicializa(STR_TELEFONO_ALTERNATIVO, false, false, true);
    frmdmclFMDomicilioPrincipal.Inicializa();

    CargarPersoneria(cbbPersoneria_Filtro);
    cbbPersoneria_Filtro.ItemIndex := 0;
    CargarPersoneria(cbbPersoneria);
    cbbPersoneria.ItemIndex := 0;
    pnlJuridico.Visible := False;

    if CantidadRegistros > 0 then
        HabilitaBotones('111101001')
    else
        HabilitaBotones('110001001');

    Position := poMainFormCenter;
	Result := True;
end;

procedure TfrmABMPersonas.btnAgregarClick(Sender: TObject);
begin
    // INICIO : TASK_126_MGO_20170127
    if GNumeroTurno < 1 then begin
        MsgBox(MSG_ERROR_TURNO, 'Turno Cerrado', MB_ICONSTOP);
        Exit;
    end;
    // FIN : TASK_126_MGO_20170127

    HabilitaBotones('000001110');
    pg01.ActivePage := tabGeneral;
    Limpiar_Campos;
    HabilitarDeshabilitarControles(True);
    PonerFocoEnPrimerControl;

    Persona := TPersonaConvenio.Create;

    Accion := 1;
end;

procedure TfrmABMPersonas.btnBuscarClick(Sender: TObject);
var
    I: Integer;
begin
    FCargandoDatos := True;
    Screen.Cursor := crHourGlass;
    try
        try
            spCRM_Personas_SELECT.Close;
            spCRM_Personas_SELECT.Parameters.Refresh;
            spCRM_Personas_SELECT.Parameters.ParamByName('@Personeria').Value := StrRight(cbbPersoneria_Filtro.Items[cbbPersoneria_Filtro.ItemIndex], 1);
            spCRM_Personas_SELECT.Parameters.ParamByName('@Nombre').Value := edtNombre_Filtro.Text;
            spCRM_Personas_SELECT.Parameters.ParamByName('@Apellido').Value := edtApellido_Filtro.Text;
            spCRM_Personas_SELECT.Parameters.ParamByName('@ApellidoMaterno').Value := edtApellidoMaterno_Filtro.Text;
            spCRM_Personas_SELECT.Parameters.ParamByName('@RUT').Value := edtRUT_Filtro.Text;
            spCRM_Personas_SELECT.Parameters.ParamByName('@Sexo').Value := edtSexo_Filtro.Text;                    
            spCRM_Personas_SELECT.Parameters.ParamByName('@Giro').Value := edtGiro_Filtro.Text;
            spCRM_Personas_SELECT.Parameters.ParamByName('@Prospecto').Value := IIf(chkProspectos.Checked, 1, 0);
            spCRM_Personas_SELECT.Parameters.ParamByName('@Top').Value := txtTop.ValueInt;
            spCRM_Personas_SELECT.Open;

            if spCRM_Personas_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spCRM_Personas_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

            cdsPersonas.Data := dtstprPersonas.Data;

            CantidadRegistros := cdsPersonas.RecordCount;
        except
            on e: Exception do
                MsgBoxErr('Hubo un error al obtener el listado de clientes', e.Message, 'Error', MB_ICONERROR);
        end;
    finally
        Screen.Cursor := crDefault;
        FCargandoDatos := False;
    end;

    cdsPersonas.First;

    if CantidadRegistros > 0 then
        HabilitaBotones('111101001')
    else
        HabilitaBotones('110001001');

    HabilitarDeshabilitarControles(False);
    dbgrdPersonas.SetFocus;
end;

procedure TfrmABMPersonas.btnCancelarClick(Sender: TObject);
    var
        i   : Integer;
begin
    if CantidadRegistros > 0 then
        HabilitaBotones('111101001')
    else
        HabilitaBotones('110001001');
    Limpiar_Campos;
	Accion := 0;
    HabilitarDeshabilitarControles(False);
    dbgrdPersonas.SetFocus;

    ObtenerDatosPersona;
end;

procedure TfrmABMPersonas.Limpiar_Campos;
begin
    edtRUT.Clear;
    edtApellido.Clear;
    edtApellidoMaterno.Clear;
    edtNombre.Clear;
    dateFechaNacimiento.Clear;
    edtSexo.Clear;
    frmtlfFreTelefonoPrincipal.CargarTelefono(CODIGO_AREA_SANTIAGO,'',-1);
    frmtlfFreTelefonoSecundario.CargarTelefono(CODIGO_AREA_SANTIAGO,'',-1);
    edtEmailParticular.Clear;
    frmdmclFMDomicilioPrincipal.Inicializa;
end;

procedure TfrmABMPersonas.PonerFocoEnPrimerControl;
var
  i, ControlIndex, TabOrderMenor : Integer;
begin

  ControlIndex  := 99;
  TabOrderMenor := 99;

  for i := 0 to pg01.ActivePage.ControlCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox',TWinControl(pg01.ActivePage.Controls[i]).ClassType.ClassName) then
      if (TWinControl(pg01.ActivePage.Controls[i]).TabOrder < TabOrderMenor) then begin
        TabOrderMenor := TWinControl(pg01.ActivePage.Controls[i]).TabOrder;
        ControlIndex  := i;
      end;

  if ControlIndex < 99 then
    TWinControl(pg01.ActivePage.Controls[ControlIndex]).SetFocus;

end;

procedure TfrmABMPersonas.HabilitarDeshabilitarControles(Estado : Boolean);
var
  i : Integer;
begin
    // Tab General
    cbbPersoneria.Enabled := Estado;
    edtRUT.Enabled := Estado;
    edtApellido.Enabled := Estado;
    edtApellidoMaterno.Enabled := Estado;
    edtNombre.Enabled := Estado;
    dateFechaNacimiento.Enabled := Estado;
    edtSexo.Enabled := Estado;

    if pnlJuridico.Visible then begin
        edtRazonSocial.Enabled := Estado;
        edtGiro.Enabled := Estado;        
        dateFechaNacimiento.Enabled := False;
    end;

    // Tab Medios Contacto
    tsMediosContacto.Enabled := Estado;
    frmtlfFreTelefonoPrincipal.Enabled := Estado;
    frmtlfFreTelefonoSecundario.Enabled := Estado;
    edtEmailParticular.Enabled := Estado;

    // Tab Domicilio
    tsDomicilio.Enabled := Estado;
    frmdmclFMDomicilioPrincipal.Enabled := Estado;

    btnGuardar.Enabled := Estado;
    btnCancelar.Enabled := Estado;
end;

procedure TfrmABMPersonas.ObtenerDatosPersona;
begin
    if FCargandoDatos then Exit;

    Limpiar_Campos;

    if CantidadRegistros = 0 then Exit;

    Persona := TPersonaConvenio.Create(cdsPersonas.FieldByName('CodigoPersona').AsInteger);

    CargarPersoneria(cbbPersoneria, Persona.Datos.Personeria);

    edtRUT.Text := Persona.Datos.NumeroDocumento;
    edtApellido.Text := Persona.Datos.Apellido;
    edtApellidoMaterno.Text := Persona.Datos.ApellidoMaterno;
    edtNombre.Text := Persona.Datos.Nombre;

    edtSexo.Text := Persona.Datos.Sexo;

    if Persona.Datos.Personeria = PERSONERIA_FISICA then begin
        dateFechaNacimiento.Date := Persona.Datos.FechaNacimiento;
        pnlJuridico.Visible := False;
        lblNombre.Caption := 'Nombre';
    end else if Persona.Datos.Personeria = PERSONERIA_JURIDICA then begin
        pnlJuridico.Visible := True;
        lblNombre.Caption := 'Nombre Contacto';
        edtRazonSocial.Text := Persona.Datos.RazonSocial;
        edtGiro.Text := Persona.Datos.Giro;
    end;

    if Persona.Telefonos.TieneMedioPrincipal then
        frmtlfFreTelefonoPrincipal.CargarTelefono(
            Persona.Telefonos.MedioPrincipal.CodigoArea,
            Persona.Telefonos.MedioPrincipal.Valor,
            Persona.Telefonos.MedioPrincipal.CodigoTipoMedioContacto,
            Persona.Telefonos.MedioPrincipal.HoraDesde,
            Persona.Telefonos.MedioPrincipal.HoraHasta);

    if Persona.Telefonos.TieneMedioSecundario then
        frmtlfFreTelefonoSecundario.CargarTelefono(
            Persona.Telefonos.MedioSecundario.CodigoArea,
            Persona.Telefonos.MedioSecundario.Valor,
            Persona.Telefonos.MedioSecundario.CodigoTipoMedioContacto,
            Persona.Telefonos.MedioSecundario.HoraDesde,
            Persona.Telefonos.MedioSecundario.HoraHasta);

    if Persona.Domicilios.TieneDomicilioPrincipal then        
        frmdmclFMDomicilioPrincipal.CargarDatosDomicilio(Persona.Domicilios.DomicilioPrincipal);

    if Persona.EMail.TieneMedioPrincipal then
        edtEmailParticular.Text := Persona.Email.MedioPrincipal.Valor;

end;

procedure TfrmABMPersonas.btnEditarClick(Sender: TObject);
var
  i : Integer;
begin         
    // INICIO : TASK_126_MGO_20170127
    if GNumeroTurno < 1 then begin
        MsgBox(MSG_ERROR_TURNO, 'Turno Cerrado', MB_ICONSTOP);
        Exit;
    end;
    // FIN : TASK_126_MGO_20170127
    
    HabilitaBotones('000000110');
    HabilitarDeshabilitarControles(True);
    PonerFocoEnPrimerControl;

    Accion := 3;
    Posicion := cdsPersonas.GetBookmark;
end;

procedure TfrmABMPersonas.btnEliminarClick(Sender: TObject);
begin
  HabilitaBotones('000000000');

   Dec(CantidadRegistros);

  if CantidadRegistros > 0 then
    HabilitaBotones('111101001')
  else
    HabilitaBotones('110001001');

end;
procedure TfrmABMPersonas.ActualizarClases;
var
    AuxTelefono:TTipoMedioComunicacion;
	AuxDomicilio: TDatosDomicilio;
	unIndiceMedio: integer;
begin
    if (Persona <> nil) then begin
        //telefono principal
        if trim(frmtlfFreTelefonoPrincipal.NumeroTelefono) <> '' then begin
            with AuxTelefono do begin
                CodigoTipoMedioContacto := frmtlfFreTelefonoPrincipal.CodigoTipoTelefono;
                CodigoArea := frmtlfFreTelefonoPrincipal.CodigoArea;
                Valor := frmtlfFreTelefonoPrincipal.NumeroTelefono;
                Anexo := frmtlfFreTelefonoPrincipal.Anexo;
                HoraDesde := frmtlfFreTelefonoPrincipal.HorarioDesde;
                HoraHasta := frmtlfFreTelefonoPrincipal.HorarioHasta;
                Principal := True;
                if Persona.Telefonos.TieneMedioPrincipal then
                    Indice := Persona.Telefonos.MediosComunicacion[Persona.Telefonos.IndiceMedioPrincipal].Indice
                else
                    Indice := -1;
            end;
            if Persona.Telefonos.TieneMedioPrincipal then
                Persona.Telefonos.MediosComunicacion[Persona.Telefonos.IndiceMedioPrincipal] := AuxTelefono
            else
                Persona.Telefonos.AgregarMediocomunicacion(AuxTelefono);
        end else
            if Persona.Telefonos.CantidadMediosComunicacion >= 1 then
                Persona.Telefonos.QuitarMediocomunicacion(Persona.Telefonos.IndiceMedioPrincipal);

        //telefono secudnario
        if trim(frmtlfFreTelefonoSecundario.NumeroTelefono) <> '' then begin
            with AuxTelefono do begin
                CodigoTipoMedioContacto := frmtlfFreTelefonoSecundario.CodigoTipoTelefono;
                CodigoArea := frmtlfFreTelefonoSecundario.CodigoArea;
                Valor := frmtlfFreTelefonoSecundario.NumeroTelefono;
                Anexo := frmtlfFreTelefonoSecundario.Anexo;
                HoraDesde := frmtlfFreTelefonoSecundario.HorarioDesde;
                HoraHasta := frmtlfFreTelefonoSecundario.HorarioHasta;
                Principal := False;
                if Persona.Telefonos.TieneMedioSecundario then
                    Indice := Persona.Telefonos.MediosComunicacion[Persona.Telefonos.IndiceMedioSecundario].Indice
                else
                    Indice := -1;
            end;
            if Persona.Telefonos.TieneMedioSecundario then
                Persona.Telefonos.MediosComunicacion[Persona.Telefonos.IndiceMedioSecundario] := AuxTelefono
            else
                Persona.Telefonos.AgregarMediocomunicacion(AuxTelefono);
        end else   //Lo reemplazo para que borre el telefono correcto.
            if Persona.Telefonos.CantidadMediosComunicacion >= 2 then begin
                //Me fijo si existe un indice para un telefono secunadario y lo borro.
                unIndiceMedio := Persona.Telefonos.RetornarIndiceTelefonoSecundario;
                if unIndiceMedio <> -1 then
                    Persona.Telefonos.QuitarMediocomunicacion(unIndiceMedio);
            end;

        // domicilio principal
        if frmdmclFMDomicilioPrincipal.DescripcionCalle <> '' then begin        // TASK_162_MGO_20170331
            AuxDomicilio := frmdmclFMDomicilioPrincipal.RegDatosDomicilio;
            AuxDomicilio.Principal := True;
            AuxDomicilio.DomicilioEntrega :=  Persona.Domicilios.DomicilioPrincipal.DomicilioEntrega;
            Persona.Domicilios.EditarDomicilio(0, AuxDomicilio);
        end else                                                                // TASK_162_MGO_20170331
            if Persona.Domicilios.CantidadDomiciliosPersonas >= 1 then          // TASK_162_MGO_20170331
                Persona.Domicilios.QuitarDomicilio(0);                          // TASK_162_MGO_20170331

        // mail
        if trim(edtEmailParticular.Text) <> '' then begin
            with AuxTelefono do begin
                CodigoTipoMedioContacto := TIPO_MEDIO_CONTACTO_E_MAIL;
                Valor := edtEmailParticular.Text;
                Principal := True;
                if Persona.EMail.CantidadMediosComunicacion > 0 then
                    Indice := Persona.EMail.MediosComunicacion[0].Indice
                else
                    indice := -1;
            end;
            if Persona.EMail.CantidadMediosComunicacion > 0 then
                Persona.EMail.MediosComunicacion[0] := AuxTelefono
            else
                Persona.EMail.AgregarMediocomunicacion(AuxTelefono);
        end else
            if Persona.EMail.CantidadMediosComunicacion >= 1 then
                Persona.EMail.QuitarMediocomunicacion(0);

    end;
end;

procedure TfrmABMPersonas.btnGuardarClick(Sender: TObject);
resourcestring
    QRY_EXISTE_RUT = 'SELECT 1 FROM Personas WITH (NOLOCK) WHERE NumeroDocumento = ''%s'' AND CodigoPersona <> %d';
var
    i     : integer;
    Exito : Boolean;
    Rut   : string;
    Personeria: string;
begin
    Personeria := RightStr(cbbPersoneria.Items[cbbPersoneria.ItemIndex], 1);

    Rut := edtRut.Text;
    Rut := UpperCase(Rut);
    while Length(Rut) < 9 do
        Rut := '0' + Rut;

    Rut        := Copy(Rut,1,9);
    edtRut.Text := Rut;
    if not EsRUTValido(Rut) then begin
        MsgBoxBalloon(MSG_ERROR_RUT, Caption, MB_ICONSTOP, edtRut);
        Exit;
    end;

    if QueryGetValueInt(DMConnections.BaseCAC, Format(QRY_EXISTE_RUT, [Rut, Persona.CodigoPersona])) = 1 then begin
        MsgBoxBalloon(MSG_ERROR_RUT_EXISTE, Caption, MB_ICONSTOP, edtRut);
        Exit;
    end;

    //if edtSexo.Text <> EmptyStr then                                          // TASK_126_MGO_20170127
    edtSexo.Text := UpperCase(edtSexo.Text);                                    // TASK_126_MGO_20170127
    if not ((edtSexo.text = 'F') or (edtSexo.text = 'M')) then begin
        MsgBoxBalloon(MSG_ERROR_SEXO, Caption, MB_ICONSTOP, edtSexo);
        Exit;
    end;

    if edtNombre.Text = EmptyStr then begin
        MsgBoxBalloon(MSG_ERROR_NOMBRE, Caption, MB_ICONSTOP, edtNombre);
        Exit;
    end;

    if edtApellido.Text = EmptyStr then begin
        MsgBoxBalloon(MSG_ERROR_APELLIDO, Caption, MB_ICONSTOP, edtApellido);
        Exit;
    end;

    if not PermitirSoloDigitos(frmdmclFMDomicilioPrincipal.txt_CodigoPostal.Text) then begin
        MsgBoxBalloon(MSG_ERROR_CP, Caption, MB_ICONSTOP, frmdmclFMDomicilioPrincipal.txt_CodigoPostal);
        Exit;
    end;

    if Personeria = PERSONERIA_JURIDICA then begin
        if edtRazonSocial.Text = EmptyStr then begin
            MsgBoxBalloon(MSG_ERROR_RS, Caption, MB_ICONSTOP, edtRazonSocial);
            Exit;
        end;
        
        if edtGiro.Text = EmptyStr then begin
            MsgBoxBalloon(MSG_ERROR_GIRO, Caption, MB_ICONSTOP, edtGiro);
            Exit;
        end;
    end;

    Exito := True;
    try
        Screen.Cursor := crHourGlass;

        if Accion = 1 then begin
            spCRM_Personas_INSERT.Close;

            spCRM_Personas_INSERT.Parameters.Refresh;                                                       
            spCRM_Personas_INSERT.Parameters.ParamByName('@CodigoPersona').Value    := Persona.CodigoPersona;
            spCRM_Personas_INSERT.Parameters.ParamByName('@Personeria').Value       := Personeria;
            spCRM_Personas_INSERT.Parameters.ParamByName('@Apellido').Value         := edtApellido.Text;
            spCRM_Personas_INSERT.Parameters.ParamByName('@ApellidoMaterno').Value  := edtApellidoMaterno.Text;
            spCRM_Personas_INSERT.Parameters.ParamByName('@Nombre').Value           := edtNombre.Text;
            spCRM_Personas_INSERT.Parameters.ParamByName('@NumeroDocumento').Value  := edtRUT.Text;
            spCRM_Personas_INSERT.Parameters.ParamByName('@Sexo').Value             := IIf(Trim(edtSexo.Text) <> '', edtSexo.Text, Null);
            if Personeria = PERSONERIA_FISICA then
                spCRM_Personas_INSERT.Parameters.ParamByName('@FechaNacimiento').Value  := IIf(not dateFechaNacimiento.IsEmpty, dateFechaNacimiento.Date, Null);
            if Personeria = PERSONERIA_JURIDICA then begin
                spCRM_Personas_INSERT.Parameters.ParamByName('@RazonSocial').Value  := edtRazonSocial.Text;
                spCRM_Personas_INSERT.Parameters.ParamByName('@Giro').Value  := edtGiro.Text;
            end;
            spCRM_Personas_INSERT.Parameters.ParamByName('@UsuarioCreacion').Value  := UsuarioSistema;

            try
                spCRM_Personas_INSERT.ExecProc;
                Retorno := spCRM_Personas_INSERT.Parameters.ParamByName('@RETURN_VALUE').Value;
                if Retorno <> 0 then begin
                    Exito := False;
                    MensajeError := spCRM_Personas_INSERT.Parameters.ParamByName('@ErrorDescription').Value;
                    MsgBoxErr(MSG_ERROR_INSERT, MensajeError, Caption, MB_ICONERROR);
                end else begin
                    Persona.CodigoPersona := spCRM_Personas_INSERT.Parameters.ParamByName('@CodigoPersona').Value;
                    Persona := TPersonaConvenio.Create(Persona.CodigoPersona);
                    ActualizarClases;
                    Persona.Guardar;

                    Limpiar_Campos;     // TASK_126_MGO_20170127
                end;
            except
                on E : Exception do begin
                    Exito := False;
                    MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
                end;
            end;

            //Inc(CantidadRegistros);   // TASK_126_MGO_20170127
        end;

        if Accion = 3 then begin
            spCRM_Personas_UPDATE.Close;

            spCRM_Personas_UPDATE.Parameters.Refresh;                                                      
            spCRM_Personas_UPDATE.Parameters.ParamByName('@CodigoPersona').Value        := Persona.CodigoPersona;
            spCRM_Personas_UPDATE.Parameters.ParamByName('@Personeria').Value       := Personeria;
            spCRM_Personas_UPDATE.Parameters.ParamByName('@Apellido').Value             := edtApellido.Text;
            spCRM_Personas_UPDATE.Parameters.ParamByName('@ApellidoMaterno').Value      := edtApellidoMaterno.Text;
            spCRM_Personas_UPDATE.Parameters.ParamByName('@Nombre').Value               := edtNombre.Text;
            spCRM_Personas_UPDATE.Parameters.ParamByName('@NumeroDocumento').Value      := edtRUT.Text;
            spCRM_Personas_UPDATE.Parameters.ParamByName('@Sexo').Value                 := IIf(Trim(edtSexo.Text) <> '', edtSexo.Text, Null);
            if Personeria = PERSONERIA_FISICA then
                spCRM_Personas_UPDATE.Parameters.ParamByName('@FechaNacimiento').Value  := IIf(not dateFechaNacimiento.IsEmpty, dateFechaNacimiento.Date, Null);
            if Personeria = PERSONERIA_JURIDICA then begin
                spCRM_Personas_UPDATE.Parameters.ParamByName('@RazonSocial').Value  := edtRazonSocial.Text;
                spCRM_Personas_UPDATE.Parameters.ParamByName('@Giro').Value  := edtGiro.Text;
            end;
            spCRM_Personas_UPDATE.Parameters.ParamByName('@UsuarioModificacion').Value  := UsuarioSistema;

            try
                spCRM_Personas_UPDATE.ExecProc;
                Retorno := spCRM_Personas_UPDATE.Parameters.ParamByName('@RETURN_VALUE').Value;
                if Retorno <> 0 then begin
                    Exito := False;
                    MensajeError := spCRM_Personas_UPDATE.Parameters.ParamByName('@ErrorDescription').Value;
                    MsgBoxErr(MSG_ERROR_UPDATE, MensajeError, Caption, MB_ICONERROR);
                end else begin
                    ActualizarClases;
                    Persona.Guardar;

                    cdsPersonas.Edit;
                    cdsPersonas.FieldByName('Apellido').Value           := edtApellido.Text;
                    cdsPersonas.FieldByName('ApellidoMaterno').Value    := edtApellidoMaterno.Text;
                    cdsPersonas.FieldByName('Nombre').Value             := edtNombre.Text;
                    cdsPersonas.FieldByName('NumeroDocumento').Value    := edtRUT.Text;
                    cdsPersonas.FieldByName('Sexo').Value               := edtSexo.Text;
                    if not dateFechaNacimiento.IsEmpty then
                        cdsPersonas.FieldByName('FechaNacimiento').Value    := dateFechaNacimiento.Date;
                        
                    cdsPersonas.Post;
                end;
            except
                on E : Exception do begin
                    Exito := False;
                    MsgBoxErr(MSG_ERROR_UPDATE, E.Message, Caption, MB_ICONERROR);
                end;
            end;
        end;

        if Exito then begin       
            HabilitarDeshabilitarControles(False);
            if CantidadRegistros > 0 then
                HabilitaBotones('111101001')
            else
                HabilitaBotones('110001001');
        end;
    finally
        Screen.Cursor := crDefault;
    end;
    if Retorna_ then Close;                                                         // TASK_88_PAN_20170215
end;

procedure TfrmABMPersonas.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMPersonas.cbbPersoneriaChange(Sender: TObject);
var
    vPersoneria: string;
begin
    vPersoneria := RightStr(cbbPersoneria.Items[cbbPersoneria.ItemIndex], 1);
    if vPersoneria = PERSONERIA_FISICA then begin
        pnlJuridico.Visible := False;
        lblNombre.Caption := 'Nombre';
    end else if vPersoneria = PERSONERIA_JURIDICA then begin
        pnlJuridico.Visible := True;
        lblNombre.Caption := 'Nombre Contacto';
    end;

    HabilitarDeshabilitarControles(Accion > 0);
end;

procedure TfrmABMPersonas.cdsPersonasAfterScroll(DataSet: TDataSet);
begin
    ObtenerDatosPersona;
end;

procedure TfrmABMPersonas.dbgrdPersonasCellClick(Column: TColumn);
begin
    ObtenerDatosPersona;
end;

procedure TfrmABMPersonas.dbgrdPersonasDblClick(Sender: TObject);
begin
    btnEditar.Click;
end;

// INICIO : TASK_126_MGO_20170127
procedure TfrmABMPersonas.edtRUTKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9','K','k', #8]) then
       Key := #0
end;

procedure TfrmABMPersonas.edtRUT_FiltroKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9','K','k', #8]) then
       Key := #0
end;

procedure TfrmABMPersonas.edtSexoKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key  in ['f','F','m','M',#8]) then
       Key := #0
end;
// FIN : TASK_126_MGO_20170127

procedure TfrmABMPersonas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmABMPersonas.FormCreate(Sender: TObject);
var
  i         : Integer;
begin
  retorna_                      := False;            // TASK_88_PAN_20170215
  Caption                       := CAPTION_FORM;
  btnSalir.Hint                 := HINT_SALIR;
  btnAgregar.Hint               := HINT_AGREGAR;
  btnEditar.Hint                := HINT_EDITAR;

  if CantidadRegistros > 0 then
    HabilitaBotones('111101001')
  else
    HabilitaBotones('110001001');
end;

procedure TfrmABMPersonas.FormShow(Sender: TObject); // TASK_88_PAN_20170215
begin                                       // TASK_88_PAN_20170215
  if Retorna_ then begin                    // TASK_88_PAN_20170215
    edtRUT.SetFocus; edtRUT.text:= NroRUT_; // TASK_88_PAN_20170215
  end;                                      // TASK_88_PAN_20170215
end;                                        // TASK_88_PAN_20170215

procedure TfrmABMPersonas.HabilitaBotones(Botones : string);
begin
    btnSalir.Enabled      := Botones[1] = '1';
    btnAgregar.Enabled    := Botones[2] = '1';
    btnEditar.Enabled     := Botones[4] = '1';
    btnBuscar.Enabled     := Botones[6] = '1';
    btnGuardar.Enabled    := Botones[7] = '1';
    btnCancelar.Enabled   := Botones[8] = '1';
    dbgrdPersonas.Enabled := Botones[9] = '1';
end;

end.



