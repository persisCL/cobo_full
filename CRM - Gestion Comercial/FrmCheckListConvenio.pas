{
-----------------------------------------------------------------------------------------
Firma       : SS-842-NDR-20110912
Description : Se agrega el campo FechaObservacion a la grilla de observaciones. Se habilitan
            2 solapas, una con las observaciones del convenio y otra para mostrar el historico
            de las observaciones del convenio
            Se habilita un bot�n de b�squeda para consultar el historico en un rango de fechas.
            Se incluye un checkBox "Incluye Eliminadas" para mostrar o no las observaciones de baja.

Firma       : SS_1194_CQU_20140702
Descripci�n : Se corrige un problema gatillado por la 842, en el stored ObtenerConvenioObservacion
              se dej� que cuando el UsuarioCreacion sea "SS842", se devolver� nulos
              los campos UsuarioCreacion y FechaObservacion. Esto genera
              que la grilla muestre una fecha por defecto y se confunde el usuario
-----------------------------------------------------------------------------------------
}


unit FrmCheckListConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FrmObservacionesGeneral, StdCtrls, DPSControls, ExtCtrls,
  CheckLst, VariantComboBox, DmConnection, DB, ADODB, PeaTypes, Convenios,
  DbList, Abm_obj, DBClient, Util, ListBoxEx, DBListEx, UtilProc,
  FechaVencimientoCheckList,UtilDB, ComCtrls, Validate, DateEdit;               //SS-842-NDR-20110912

type
   TListaCheck = array of integer;

type
  TFormCheckListConvenio = class(TForm)
    Bevel2: TBevel;
	ConvenioCheckListBox: TVariantCheckListBox;
	Label1: TLabel;
    Bevel1: TBevel;
    Label2: TLabel;
	btnAgregar: TButton;
    btnModificar: TButton;
	btnEliminar: TButton;
    cdsObservaciones: TClientDataSet;
    dblObservaciones: TDBListEx;
    dsObservaciones: TDataSource;
    btnAceptar: TButton;
    btnCancelar: TButton;
    CuentasCheckListBox: TVariantCheckListBox;
    Label3: TLabel;
    btnEditarVencimiento: TButton;
    pgcConvenioObservaciones: TPageControl;                                     //SS-842-NDR-20110912
    tsObservaciones: TTabSheet;                                                 //SS-842-NDR-20110912
    tsHistorico: TTabSheet;                                                     //SS-842-NDR-20110912
    pnlBuscar: TPanel;                                                          //SS-842-NDR-20110912
    deHistoricoDesde: TDateEdit;                                                //SS-842-NDR-20110912
    deHistoricoHasta: TDateEdit;                                                //SS-842-NDR-20110912
    lblHistoricoDesde: TLabel;                                                  //SS-842-NDR-20110912
    lblHistoricoHasta: TLabel;                                                  //SS-842-NDR-20110912
    btnBuscar: TButton;                                                         //SS-842-NDR-20110912
    DBLEHistoricoObservaciones: TDBListEx;                                      //SS-842-NDR-20110912
    spObtenerHistoricoConvenioObservaciones: TADOStoredProc;                    //SS-842-NDR-20110912
    dsHistoricoConvenioObservaciones: TDataSource;                              //SS-842-NDR-20110912
    chkIncluyeDeBaja: TCheckBox;                                                //SS-842-NDR-20110912
    procedure btn_TerminarClick(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
	procedure dblObservacionesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
	  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnModificarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure dblObservacionesDblClick(Sender: TObject);
	procedure cdsObservacionesAfterPost(DataSet: TDataSet);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CuentasCheckListBoxClickCheck(Sender: TObject);
    procedure btnEditarVencimientoClick(Sender: TObject);
    procedure CuentasCheckListBoxClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure deHistoricoDesdeChange(Sender: TObject);
    procedure deHistoricoHastaChange(Sender: TObject);
    procedure chkIncluyeDeBajaClick(Sender: TObject);
    procedure DBLEHistoricoObservacionesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);                                  //SS-842-NDR-20110912
  private
    FCobrar: TStringList;
    FCheckListCuenta: TChekListCuenta;
    FCheckListConvenio: TCheckListConvenio;
    FListObservacionConvenio: TListObservacionConvenio;
    FModoPantalla: TStateConvenio;
    FItemsNoDesmarcables: TStringList;
    function GetTListObservacionConvenio: TListObservacionConvenio;
    function GetListaCheck: TCheckListConvenio;
    function GetListaCheckCuenta: TChekListCuenta;
    function PuedeDesmarcar(Indice: Integer): Boolean;
  public
    FCodigoConvenio: Integer;                                                   //SS-842-NDR-20110912
    FModificado: Boolean;                                                       //SS_960_PDO_20120114
    property ListaCheck: TCheckListConvenio read GetListaCheck;
    property ListaCheckCuenta: TChekListCuenta read GetListaCheckCuenta;
    property ListaObservacion: TListObservacionConvenio read GetTListObservacionConvenio;
    property VehiculosACobrar: TStringList read FCobrar;
	Function Inicializa(ModoPantalla: TStateConvenio;  CheckListConvenio: TCheckListConvenio; ListObservacionConvenio: TListObservacionConvenio; CheckListCuenta: TChekListCuenta): Boolean;
  end;

var
  FormCheckListConvenio: TFormCheckListConvenio;

implementation

{$R *.dfm}

{ TFormCheckListConvenio }

(*function TFormCheckListConvenio.GetListaCheck: TListaCheck;
var
    i: integer;
begin
    setlength(result, ConvenioCheckListBox.Items.Count -1);
    for i:=0 to ConvenioCheckListBox.Items.Count -1 do begin
        result[i] := ConvenioCheckListBox.Items[i].Value;
    end;
end;*)

function TFormCheckListConvenio.Inicializa(ModoPantalla: TStateConvenio;  CheckListConvenio: TCheckListConvenio; ListObservacionConvenio: TListObservacionConvenio; CheckListCuenta: TChekListCuenta): Boolean;
var
    i: integer;
begin
    pgcConvenioObservaciones.ActivePageIndex:=0;                                //SS-842-NDR-20110912
    cdsObservaciones.Filter := 'Activo <> 0';
	cdsObservaciones.Filtered := True;
	cdsObservaciones.CreateDataSet;

	FCheckListCuenta := CheckListCuenta;
    FCheckListConvenio := CheckListConvenio;
	FListObservacionConvenio := ListObservacionConvenio;

    FItemsNoDesmarcables.Clear;

	for i := 0 to FListObservacionConvenio.Count - 1 do begin
		cdsObservaciones.Append;
		cdsObservaciones.FieldByName('IndiceObservacion').AsInteger := ListObservacionConvenio.Items[i].IndiceObservacion;
		cdsObservaciones.FieldByName('Descripcion').AsString := ListObservacionConvenio.Items[i].Observacion;
		cdsObservaciones.FieldByName('Activo').AsBoolean := ListObservacionConvenio.Items[i].Activo;
		cdsObservaciones.FieldByName('FechaObservacion').AsDateTime := ListObservacionConvenio.Items[i].FechaObservacion;                             //SS-842-NDR-20110912
		cdsObservaciones.Post;
	end;
    // Check de Convenios
    i := 0;
	while (CheckListConvenio.Cantidad -1 >= i) do begin
		ConvenioCheckListBox.Items.Add(CheckListConvenio.Items[i].Descripcion,CheckListConvenio.Items[i].CodigoCheckList);
		i:= i + 1;
	end;

    //La VariantCheckList requiere que se cargue y luego que se Marque (Bug?)
	i:= 0;
	while (CheckListConvenio.Cantidad -1 >= i) do begin
		ConvenioCheckListBox.Checked[i] := FCheckListConvenio.Items[i].Marcado;

		i:= i + 1;
	end;

    //Check de cuentas
    if assigned(FCheckListCuenta) then begin
        i := 0;
        while (CheckListCuenta.Cantidad - 1 >= i) do begin
            if not CheckListCuenta.Items[i].Eliminar then
                CuentasCheckListBox.Items.Add(CheckListCuenta.Items[i].Descripcion, i);
            i:= i + 1;
        end;

        //La VariantCheckList requiere que se cargue y luego que se Marque (Bug?)
        // con este modelo se suponia que cada elemento en CheckListCuenta iba a tener un
        // checkListBox
        // como se saca el elemento de Pago televia en indemnizaciones, ahora las listas
        // tiene distinta cantidad de elementos
        // entonces revisamos la lista de checklistboxes que guardaron el puntero a la checklistconvenio
        i:= 0;
//        while (CheckListCuenta.Cantidad -1 >= i) do begin
//            if not CheckListCuenta.Items[i].Eliminar then
//                CuentasCheckListBox.Checked[i] := FCheckListCuenta.Items[i].Marcado;
//                if FCheckListCuenta.Items[i].Marcado then FItemsNoDesmarcables.Add( IntToStr(i) );
//            i:= i + 1;
//        end;

        while CuentasCheckListBox.Items.Count -1 >= i do begin
            if not CheckListCuenta.Items[CuentasCheckListBox.Items[i].Value ].Eliminar then
                CuentasCheckListBox.Checked[i] := CheckListCuenta.Items[CuentasCheckListBox.Items[i].Value ].Marcado;
            if CheckListCuenta.Items[CuentasCheckListBox.Items[i].Value ].Marcado then
                FItemsNoDesmarcables.Add( IntToStr(CuentasCheckListBox.Items[i].Value) );
            i:= i + 1;
        end;
    end;

	FModoPantalla := ModoPantalla;

	btnAgregar.Enabled := ModoPantalla <> scNormal;
	btnModificar.Enabled := (cdsObservaciones.RecordCount > 0);
	btnEliminar.Enabled := (ModoPantalla <> scNormal) and (cdsObservaciones.RecordCount > 0);;
    btnEditarVencimiento.Enabled := ModoPantalla <> scNormal;
    btnAceptar.Enabled := ModoPantalla <> scNormal;


	ConvenioCheckListBox.Enabled := btnAgregar.Enabled;

	result := True;
end;

procedure TFormCheckListConvenio.btn_TerminarClick(Sender: TObject);
var
	i: integer;
begin
    inherited;
    i:= 0;
	while i <= ConvenioCheckListBox.Count-1 do begin
        if ConvenioCheckListBox.Checked[i] then
            FCheckListCuenta.Marcar(CuentasCheckListBox.Items[i].Value);
           //             FCheckListCuenta.Marcar(i);ya no estan sincronizados
        inc(i);
    end;
end;

procedure TFormCheckListConvenio.btnAgregarClick(Sender: TObject);
var
    f: TFormObservacionesGeneral;
begin
    Application.CreateForm(TFormObservacionesGeneral, f);
    if f.Inicializa('', False, scAlta) and (f.ShowModal = mrOk) and (trim(f.Observaciones) <> '') then begin
        cdsObservaciones.Append;
        cdsObservaciones.FieldByName('IndiceObservacion').AsInteger := -1;
        cdsObservaciones.FieldByName('Descripcion').AsString := f.Observaciones;
        cdsObservaciones.FieldByName('Activo').AsBoolean := True;
        cdsObservaciones.FieldByName('FechaObservacion').AsDateTime := QueryGetValueDateTime(DmConnections.BaseCAC, 'SELECT GETDATE()');                           //SS-842-NDR-20110912
		cdsObservaciones.Post;
        FModificado := True;        // SS_960_PDO_20120114
    end;
end;

procedure TFormCheckListConvenio.btnBuscarClick(Sender: TObject);                     //SS-842-NDR-20110912
begin                                                                                 //SS-842-NDR-20110912
  with spObtenerHistoricoConvenioObservaciones do                                     //SS-842-NDR-20110912
  begin                                                                               //SS-842-NDR-20110912
    Close;                                                                            //SS-842-NDR-20110912
    Parameters.ParamByName('@CodigoConvenio').Value:= FCodigoConvenio;                //SS-842-NDR-20110912
    if deHistoricoDesde.Date <> NullDate then                                         //SS-842-NDR-20110912
        Parameters.ParamByName('@FechaDesde').Value := deHistoricoDesde.Date          //SS-842-NDR-20110912
    else                                                                              //SS-842-NDR-20110912
        Parameters.ParamByName('@FechaDesde').Value := Null;                          //SS-842-NDR-20110912

    if deHistoricoHasta.Date <> NullDate then                                         //SS-842-NDR-20110912
        Parameters.ParamByName('@FechaHasta').Value := deHistoricoHasta.Date          //SS-842-NDR-20110912
    else                                                                              //SS-842-NDR-20110912
        Parameters.ParamByName('@FechaHasta').Value := Null;                          //SS-842-NDR-20110912

    Parameters.ParamByName('@IncluyeDeBaja').Value:= chkIncluyeDeBaja.Checked;        //SS-842-NDR-20110912
    Open;                                                                             //SS-842-NDR-20110912
  end;                                                                                //SS-842-NDR-20110912
end;                                                                                  //SS-842-NDR-20110912

procedure TFormCheckListConvenio.btnEliminarClick(Sender: TObject);
begin
	if cdsObservaciones.Active and (cdsObservaciones.RecordCount > 0) then begin
        if cdsObservaciones.FieldByName('IndiceObservacion').AsInteger <> -1 then begin
            cdsObservaciones.Edit;
            cdsObservaciones.FieldByName('Activo').AsBoolean := False;
            cdsObservaciones.FieldByName('Modificada').AsBoolean := True;       // SS_842_PDO_20111214
			cdsObservaciones.Post;
        end
        else cdsObservaciones.Delete;

        FModificado := True;        // SS_960_PDO_20120114
    end;
end;

procedure TFormCheckListConvenio.dblObservacionesDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    if Column.FieldName = 'Descripcion' then
        Text := StringReplace(Text, #13#10, ' ', [rfReplaceAll]);
    if Column.FieldName = 'IndiceObservacion' then
        Text := StringReplace(Text, '-1', '<Nuevo>', [rfReplaceAll]);

    if  (Column.FieldName = 'FechaObservacion') and                         // SS_1194_CQU_20140702
        (cdsObservaciones.FieldByName('FechaObservacion').AsDateTime <= 0)  // SS_1194_CQU_20140702
    then Text := '';                                                        // SS_1194_CQU_20140702
end;

procedure TFormCheckListConvenio.deHistoricoDesdeChange(Sender: TObject);
begin
    if spObtenerHistoricoConvenioObservaciones.Active then spObtenerHistoricoConvenioObservaciones.Close;

end;

procedure TFormCheckListConvenio.deHistoricoHastaChange(Sender: TObject);
begin
    if spObtenerHistoricoConvenioObservaciones.Active then spObtenerHistoricoConvenioObservaciones.Close;
end;

procedure TFormCheckListConvenio.btnModificarClick(Sender: TObject);
var
    f: TFormObservacionesGeneral;
begin
	Application.CreateForm(TFormObservacionesGeneral, f);
	if f.Inicializa(cdsObservaciones.FieldByName('Descripcion').AsString, False, FModoPantalla) and (f.ShowModal = mrOk) and (trim(f.Observaciones) <> '') then begin
		cdsObservaciones.Edit;
		cdsObservaciones.FieldByName('Descripcion').AsString := f.Observaciones;
        cdsObservaciones.FieldByName('Modificada').AsBoolean := True;       // SS_842_PDO_20111214
        cdsObservaciones.Post;
        FModificado := True;        // SS_960_PDO_20120114
    end;
	btnEliminar.Enabled := FModoPantalla <> scNormal;
end;

procedure TFormCheckListConvenio.btnAceptarClick(Sender: TObject);
var
    ObservacionConvenio: TObservacionConvenio;
begin
	cdsObservaciones.First;
    FListObservacionConvenio.Clear;
    cdsObservaciones.Filtered := False;
    while not cdsObservaciones.Eof do begin
		ObservacionConvenio := TObservacionConvenio.Create;
		ObservacionConvenio.IndiceObservacion := cdsObservaciones.FieldbyName('IndiceObservacion').AsInteger;
		ObservacionConvenio.Observacion := cdsObservaciones.FieldbyName('Descripcion').AsString;
        ObservacionConvenio.Activo := cdsObservaciones.FieldbyName('Activo').AsBoolean;
        ObservacionConvenio.FechaObservacion := cdsObservaciones.FieldbyName('FechaObservacion').AsDateTime;            //SS-842-NDR-20110912
        ObservacionConvenio.Modificada := cdsObservaciones.FieldbyName('Modificada').AsBoolean;                         //SS_842_PDO_20111214
        FListObservacionConvenio.Add(ObservacionConvenio);
        cdsObservaciones.Next;
    end;
end;

function TFormCheckListConvenio.GetTListObservacionConvenio: TListObservacionConvenio;
begin
    result := FListObservacionConvenio;
end;


function TFormCheckListConvenio.GetListaCheck: TCheckListConvenio;
var
	i: Integer;
begin
	for i := 0 to ConvenioCheckListBox.Count - 1 do begin
		if ConvenioCheckListBox.Checked[i] then FCheckListConvenio.Marcar(ConvenioCheckListBox.Items[i].Value)
		else FCheckListConvenio.DesMarcar(ConvenioCheckListBox.Items[i].value);
	end;

	Result := FCheckListConvenio;
end;

procedure TFormCheckListConvenio.dblObservacionesDblClick(Sender: TObject);
begin
	if btnModificar.Enabled then btnModificar.Click;
end;

procedure TFormCheckListConvenio.cdsObservacionesAfterPost(
  DataSet: TDataSet);
begin
	btnModificar.Enabled := cdsObservaciones.RecordCount > 0;
	btnEliminar.Enabled := btnModificar.Enabled;
end;

procedure TFormCheckListConvenio.chkIncluyeDeBajaClick(Sender: TObject);
begin
    if spObtenerHistoricoConvenioObservaciones.Active then spObtenerHistoricoConvenioObservaciones.Close;
end;

procedure TFormCheckListConvenio.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
	if Key = #27 then begin
		Key := #0;

		Close;
		ModalResult := mrCancel
	end
end;

function TFormCheckListConvenio.GetListaCheckCuenta: TChekListCuenta;
var
	i : integer;
begin
	for i := 0 to CuentasCheckListBox.Count - 1 do
		if CuentasCheckListBox.Checked[i] then
			FCheckListCuenta.Marcar(CuentasCheckListBox.Items[i].Value);
            //			FCheckListCuenta.Marcar(i);      ya no estan sincronizados
	Result := FCheckListCuenta;
end;

procedure TFormCheckListConvenio.CuentasCheckListBoxClickCheck(
  Sender: TObject);
resourcestring
	MSG_NO_SE_PUEDE_DESMARCAR = 'No se puede desmarcar';
	MSG_NO_SE_PUEDE_MARCAR = 'Este Item no puede ser marcado por el Operador';
	//MSG_CONFIRMAR_CARGO_POR_TAG = 'Esto generara un cargo por el valor de un televia, �Desea hacer el cargo?';
begin
	if FCheckListCuenta.Items[CuentasCheckListBox.Items[CuentasCheckListBox.ItemIndex].Value].PuedeCambiarOperador then begin

		if not CuentasCheckListBox.Checked[CuentasCheckListBox.ItemIndex] then begin
            //si puedo desmarcar lo desmarco
            if PuedeDesmarcar(CuentasCheckListBox.ItemIndex) then begin

    			CuentasCheckListBox.Checked[CuentasCheckListBox.ItemIndex] := False;
//  rev5          if FCobrar.IndexOf(FCheckListCuenta.Items[CuentasCheckListBox.ItemIndex].Patente) >= 0 then
//                    FCobrar.Delete(FCobrar.IndexOf(FCheckListCuenta.Items[CuentasCheckListBox.ItemIndex].Patente));

// rev5           if BuscarCheckCuenta(CONST_PENDIENTE_PAGAR_TAG, FCheckListCuenta.Items[CuentasCheckListBox.ItemIndex].Patente) >= 0 then
//                    CuentasCheckListBox.Checked[BuscarCheckCuenta(CONST_PENDIENTE_PAGAR_TAG, FCheckListCuenta.Items[CuentasCheckListBox.ItemIndex].Patente)] := False;

            end else begin
                CuentasCheckListBox.Checked[CuentasCheckListBox.ItemIndex] := True;
                MsgBox(MSG_NO_SE_PUEDE_DESMARCAR, Caption, MB_ICONINFORMATION);
            end;

		end;// else   rev5 ya no se pregunta mas para crear el cargo por perdida que despues se acreditaba
            //la pantalla de perdida ahora tiene un combo de motivo y si lo quieren  acreditar haran NC2BO o CK2NK
//			case MsgBox(MSG_CONFIRMAR_CARGO_POR_TAG, Caption, MB_YESNOCANCEL + MB_ICONINFORMATION) of
//				ID_CANCEL: CuentasCheckListBox.Checked[CuentasCheckListBox.ItemIndex] := False;
//				ID_YES: FCobrar.Add(FCheckListCuenta.Items[CuentasCheckListBox.ItemIndex].Patente);
//                ID_NO: begin
//                        if BuscarCheckCuenta(CONST_PENDIENTE_PAGAR_TAG, FCheckListCuenta.Items[CuentasCheckListBox.ItemIndex].Patente) >= 0 then
//                            CuentasCheckListBox.Checked[BuscarCheckCuenta(CONST_PENDIENTE_PAGAR_TAG, FCheckListCuenta.Items[CuentasCheckListBox.ItemIndex].Patente)] := True;
//                    end;
//			end;

	end else begin

		CuentasCheckListBox.Checked[CuentasCheckListBox.ItemIndex] := not CuentasCheckListBox.Checked[CuentasCheckListBox.ItemIndex];
		MsgBox(MSG_NO_SE_PUEDE_MARCAR, Caption, MB_ICONINFORMATION);

	end;
end;

procedure TFormCheckListConvenio.btnEditarVencimientoClick(Sender: TObject);
var
    FrmFechaVto: TFrmFechaVencimientoCheckList;
begin
    if CuentasCheckListBox.ItemIndex = -1 then Exit;

    FrmFechaVto := TFrmFechaVencimientoCheckList.Create(Self);
    try
        FrmFechaVto.FechaVencimiento := FCheckListCuenta.Items[CuentasCheckListBox.ItemIndex].Vencimiento;
        if FrmFechaVto.ShowModal = mrOk then begin
            FCheckListCuenta.ActualizarVencimiento(CuentasCheckListBox.ItemIndex, FrmFechaVto.FechaVencimiento);
            CuentasCheckListBox.Items[CuentasCheckListBox.ItemIndex].Caption :=
                FCheckListCuenta.ArmarDescripcion(FCheckListCuenta.Items[CuentasCheckListBox.ItemIndex].CodigoCheckList,
                                                  FCheckListCuenta.Items[CuentasCheckListBox.ItemIndex].Patente,
                                                  FCheckListCuenta.Items[CuentasCheckListBox.ItemIndex].Vencimiento);

            FModificado := True;        // SS_960_PDO_20120114
        end;
    finally
        FrmFechaVto.Free;
    end;
end;

procedure TFormCheckListConvenio.CuentasCheckListBoxClick(Sender: TObject);
begin
    btnEditarVencimiento.Enabled := (FCheckListCuenta.Items[CuentasCheckListBox.Items[CuentasCheckListBox.ItemIndex].Value].Vencimiento <> nulldate)
                                    and not CuentasCheckListBox.Checked[CuentasCheckListBox.ItemIndex];
end;

procedure TFormCheckListConvenio.FormCreate(Sender: TObject);
begin
    FCobrar := TStringList.Create;
    FItemsNODesmarcables := TStringList.Create;
end;

procedure TFormCheckListConvenio.FormDestroy(Sender: TObject);
begin
    FCobrar.Free;
    FItemsNODesmarcables.Free;
	FCheckListCuenta := nil;
    FCheckListConvenio := nil;
	FListObservacionConvenio := nil;
	FCheckListCuenta.Free;
    FCheckListConvenio.Free;
	FListObservacionConvenio.Free;
end;



function TFormCheckListConvenio.PuedeDesmarcar(Indice: Integer): Boolean;
begin
    Result := (FItemsNoDesmarcables.IndexOf(IntToStr(Indice)) < 0);
end;

procedure TFormCheckListConvenio.DBLEHistoricoObservacionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;      // SS_842_PDO_20111212
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);                      // SS_842_PDO_20111212
begin                                                                                                                                   // SS_842_PDO_20111212
    if spObtenerHistoricoConvenioObservaciones.Active then begin                                                                        // SS_842_PDO_20111212
        if spObtenerHistoricoConvenioObservaciones.RecordCount > 0 then begin                                                           // SS_842_PDO_20111212
            if not spObtenerHistoricoConvenioObservaciones.FieldByName('FechaBaja').IsNull then begin                                   // SS_842_PDO_20111212
                Sender.Canvas.Font.Color := clRed;                                                                                      // SS_842_PDO_20111212
            end;                                                                                                                        // SS_842_PDO_20111212
        end;                                                                                                                            // SS_842_PDO_20111212
    end;                                                                                                                                // SS_842_PDO_20111212
end;                                                                                                                                    // SS_842_PDO_20111212

end.
