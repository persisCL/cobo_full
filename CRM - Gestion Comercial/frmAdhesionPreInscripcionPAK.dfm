object AdhesionPreInscripcionPAKForm: TAdhesionPreInscripcionPAKForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = ' Adhesi'#243'n PreInscripciones Arauco TAG'
  ClientHeight = 136
  ClientWidth = 712
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    712
    136)
  PixelsPerInch = 96
  TextHeight = 13
  object grpFiltros: TGroupBox
    Left = 4
    Top = 3
    Width = 703
    Height = 82
    Anchors = [akLeft, akTop, akRight]
    Caption = '  Filtros de Consulta  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      703
      82)
    object Label1: TLabel
      Left = 19
      Top = 21
      Width = 26
      Height = 13
      Caption = 'RUT:'
    end
    object lblNombreCli: TLabel
      Left = 93
      Top = 52
      Width = 70
      Height = 13
      Caption = 'lblNombreCli'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblPersoneria: TLabel
      Left = 169
      Top = 52
      Width = 61
      Height = 13
      Caption = 'lblPersoneria'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblRUT: TLabel
      Left = 52
      Top = 52
      Width = 36
      Height = 13
      Caption = 'lblRUT'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btn_Filtrar: TButton
      Left = 616
      Top = 16
      Width = 75
      Height = 25
      Hint = 'Consultar '
      Anchors = [akTop, akRight]
      Caption = '&Consultar'
      Default = True
      TabOrder = 1
      OnClick = btn_FiltrarClick
    end
    object peNumeroDocumento: TPickEdit
      Left = 52
      Top = 19
      Width = 134
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 0
      OnChange = peNumeroDocumentoChange
      OnKeyPress = peNumeroDocumentoKeyPress
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object btn_Limpiar: TButton
      Left = 616
      Top = 47
      Width = 75
      Height = 25
      Hint = 'Filtrar Solicitud'
      Anchors = [akTop, akRight]
      Caption = '&Limpiar'
      TabOrder = 2
      OnClick = btn_LimpiarClick
    end
  end
  object btnAdherir: TButton
    Left = 620
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Adherir'
    TabOrder = 1
    OnClick = btnAdherirClick
  end
  object spConveniosEnListaAmarillaPorRut: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ConveniosEnListaAmarillaPorRut;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Rut'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 80
    Top = 88
  end
  object dsConveniosEnListaAmarilla: TDataSource
    DataSet = spConveniosEnListaAmarillaPorRut
    Left = 120
    Top = 88
  end
end
