{********************************** File Header ********************************
File Name : FrmSolicitudContacto.pas
Author :
Date Created:
Language : ES-AR
Description :


Revision : 1
Author : jconcheyro
Date Created: 03/02/2006
Language : ES-AR
Description : en IniciarCargaConRut se estaba suponiendo que el AuxMediosComunicacion[0]
era siempre un telefono cuando en realidad puede ser un mail.

Revision 2:
    Author : ggomez
    Date : 26/07/2006
    Description : Agregu� las variables para los par�metros generales para
    	la generaci�n del XML de Novedades.

Revisi�n 3:
	Author      : jconcheyro
	Date        : 27/06/2006
	Description : Organizamos los Commit de manera que no sean condicionados por errores de los XML


Revisi�n 4:
	Author      : jconcheyro
	Date        : 30/01/2007
	Description : calculo de la fecha de fin de garantia del TAG. indemnizaciones de televias

Revisi�n 6:
	Author      : nefernandez
	Date        : 25/04/2007
	Description : Cambi� la conversi�n del par�metro ContractSerialNumber
    (de asInteger a AsFloat)

Revision 7:
	Author		: Fsandi
	Date 		: 02/05/2007
	Description : Se elimino un if sobrante al cargar los datos del contacto, que evitaba que se mostrara
				  correctamente el telefono principal.

Revision 8:
	Author		: Fsandi
	Date 		: 08/05/2007
	Description : Se permite la modificaci�n de datos de contacto tambien para clientes que tenga convenio activo de CN


Revision 8:
	Author		: Fsandi
	Date 		: 18/05/2007
	Description : Cuando se carga un RUT ahora tambi�n se carga el GIRO en ObtenerDatosPersonas


Revision 9:
	Author		: jconcheyro
	Date 		: 22/05/2007
	Description : Faltaban unos first en el cdsIndemnizaciones que generaban problemas.

Revision 11:
	Author		: jconcheyro
	Date 		: 23/05/2007
	Description : Se generan logs de operaciones sobre las acciones de los usuarios
    que van a la tabla AgregarAuditoriaConveniosCAC


Revision 12:
	Author		: Fsandi
	Date 		: 01-06-2007
	Description : Se permite modificar la fecha de alta de una cuenta al agregar un vehiculo

Revision 13:
	Author : vpaszkowicz
    Date : 29/08/2007
    Description : Comento la parte del llenado de las tablas con la fecha alta
    para evitar traslapes.
Revision : 15
    Author : vpaszkowicz
    Date : 13/09/2007
    Description : Controlo que si existen los 2 tipos de tel�fonos, estos no
    esten duplicados en el momento de grabar (PuedeGuardarGenerico)

Revision : 16
    Author : Fsandi
    Date : 20/09/2007
    Description : Se habilita la opcion de recuperar un vehiculo robado al momento de dar
                de alta una cuenta.
Revision : 18
    Author : dAllegretti
    Date : 06/10/2008
    Description : Se agrega la funcionalidad de los convenios sin cuentas
                    .- Se cambia el caption del boton Imprimir por Guardar

Revision 17
Author: mbecerra
Date: 29-Dic-2008
Description:	(Ref SS 769) Se agregan los cambios de esta SS.

Revision 19
Author: mbecerra
Date: 15-Junio-2009
Description:		A pedido del cliente se quita la validaci�n de que "existen
        	otros convenios asociados a un cliente, si no ha ingresado veh�culos
            a este convenio nuevo  (funci�n PuedeGuardarContrato).

Revision 20
    Author : pdominguez
    Date   : 09/09/2009
    Description : SS 733
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            ImprimirConvenioEtiqueta

Revision 21
Author: mbecerra
Date: 28-Septiembre-2009
Description:	(Ref. SS 833)
        	Se agrega la opci�n del Documento electr�nico a generar durante la
            facturaci�n.

Revision 22
Author: Nelson Droguett Sierra
Date: 29-Octubre-2009
Description:	(Ref. SS 841)
        	Se agrega la opci�n un BIT para hablitar los vehiculos activos en
            lista blanca AMB

Revision 23
Author: Nelson Droguett Sierra
Date: 01-Diciembre-2009
Description:	(Ref. SS 841)
        	Se valida el acceso al BIT Lista Blanca Habilitado AMB

Revision : 24
    Author : pdominguez
    Date   : 04/12/2009
    Description : SS 836
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            ImprimirConvenioEtiqueta
Revision 24:
    Author : mpiazza
    Date   : 19/10/2009
    Description : SS 787
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
                GrabarPersona
                InicializarFormulario,
                GuardarContrato,

Revision : 25
    Author : pdominguez
    Date   : 29/03/2010
    Description : SS 719
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            ManejarPersonaSolicitud

Revision : 26
    Author : jjofre
    Date   : 12/04/2010
    Description : SS 468
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            GrabarDomicioConvenio
            GuardarContrato

Revision    : 4
Author      : mbecerra
Date        : 13-Abril-2011
Description :       (Ref SS 948)
                Se modifica la funci�n ValidarTagsEnArriendo para desplegar
                el importe correcto en el mensaje al usuario

Firma       : SS-948-MBE-20110413

Description : Cuando se intenta guardar el formulario, se comenta el cierre de este
              si no se ha guardado los datos.
Firma       : SS_972_ALA_20110718

Description : Se agrega validaci�n en m�todo PuedeGuardarContrato
              para que se pueda agregar solamente un ConvenioSinCuenta
              y se pueda agregar convenios con medios de pago excluyendo de la
              validaci�n de los medios de pago a los ConvenioSinCuenta.
Firma       : SS_628_ALA_20110720
Description : Se quitan los par�metros del m�todo ObtenerConvenioMedioPago.
Firma       : SS_628_ALA_20110818


Firma       : SS-1006-NDR-20111105
Description : Se implementa el bit HabilitadoPA para las listas de acceso de parque arauco
              Si se modifica el Bit del convenio, este afecta todas las cuentas
                que descienden de el. Si se cambia el Bit de una cuenta, solo
                afecta a la cuenta..

Firma       : SS-628-NDR-20111122
Description : Cuando ingreso un convenio nuevo, este siempre es de CN por lo cual siempre deben poderse modificar todos los datos ingresados.
              Cuando se ingresa a la ventana de modificaci�n de convenios, siempre se podr�n modificar todos los datos cuando el convenio es
              de CN, independientemente del estado en que se encuentre el mismo  (ALTA; BAJA; SUSPENDIDO; ANULADO LO que sea).<--FBE

Firma       : SS_628_ALA_20111202
Description : Se incluye checkbox chkConvenioSinCuenta para indicar si un Convenio es sin cuenta o no.
              Se modifica la funci�n EsConvenioSinCuenta, para que tome el valor desde el checkbox
              Si el convenio es sin cuenta se impide el ingreso de Cuentas.
              Adem�s se alinean los controles del sem�foro para su correcta visualizaci�n.

Firma       : SS_628_ALA_20111216
Description : Se agrego mensaje de confirmaci�n, cuando se guarde un nuevo convenio y no tenga el checkbox �Sin Cuenta�
              activado y no posea Cuentas asociadas, para activar autom�ticamente el checkbox �Sin Cuenta�.

Firma       : SS_628_ALA_20111219
Description : Se actualiza llamada a funcion VerificarExisteConvenioSinCuenta, se entrega 0 en
              parametro ExcluirEsteConvenio, ya que se debe verificar que no exista ningun convenio
              sin cuenta para este cliente.

Firma       : SS_628_ALA_20111223
Description : Se agrega validaci�n que verifica si existe un convenio sin cuenta para otro convenio de esta persona
              cuando se verifica si el checkbox "Sin cuenta" no se encuentra marcado (SS_628_ALA_20111216).

Firma       : SS-1006-NDR-20120614
Descripcion : El Bit HabilitadoPA ahora se llama AdheridoPA
              Nueva operatoria de la Habilitacion de Convenio y Cuentas para EPA

Firma       : SS-1092-MVI-20130627
Descripcion : Se reemplaza la funci�n que valida el mail de "IsValidEMail" a "IsValidEMailCN"
              Esto para validar "." despu�s del "@" y que existan caracteres entre estos.
              Adem�s se cambia reemplaza la variable MSG_VALIDAR_SINTAXIS_EMAIL por MSG_VALIDAR_DIRECCION_EMAIL

Firma		: SS_660_MCA_20140114
Descripcion	: se valida si el convenio esta en LA o en proceso notificando al operador de tal estado.

=============================================================
No se entiende que revision toca

Author: ebaeza
date: 14-03-2014
firma: SS_1171_EBA_20140311
description: SS_1171
      procedure / functions
      ImprimirConvenioEtiqueta

    -.Se agrega la obtencion de la Razon social y el rut asociado, para la impresion
      del anexo III juridico, cuando la personeria sea igual a 'J'


Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1147_MCA_20150410
Descripcion : Si la concesionaria es CN entonces los chk de AdheridoPA e InhabilitadoPA se dejan visibles.

Firma       : SS_1419_NDR_20151130
Descripcion : Se envia el parametro @CodigoUsuario al SP ActualizarDatosPersona
            Para grabarlo en los campos de auditoria de la tabla Personas y en
            HistoricoPersonas


Firma       : TASK_004_ECA_20160411
Descripcion : Se agrega campo para el manejo de el CodigoConvenioFacturacion que
              sera utilizado para la facturacion
              Se agrega captura para el RUT de Facturacion.
              Se cre grupo Datos de Facturacion para ordenar la pantalla

Firma       : TASK_008_ECA_20160502
Descripcion : Se agrega Registro �nico Convenio RNUT
              Modificar la funcionalidad actual de Alta de Convenio, para que s�lo
              permita un registro �nico en la tabla por RUT de Cliente, es decir que
              cada cliente s�lo pueda tener un registro de Convenio Nativo del tipo RNUT
              Se agrega llama al procedimiento [InsertarHistoricoConvenio]

Etiqueta    : TASK_055_MGO_20160801
Descripci�n : Se agrega propiedad for�neo para la baja forzada

Etiqueta	: TASK_077_MGO_20161012
Descripci�n : Se corrige caso en el que el email puede mostrarse como tel�fono secundario

Etiqueta	: TASK_081_MGO_20161102
Descripci�n : Se agregan barras de scroll
*******************************************************************************}



unit FrmSolicitudContacto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons,
  Dateedit, DB, ADODB, UtilDB, Util, DBTables, UtilProc, PeaProcs,
  Grids, DBGrids, math, FrmObservacionesGeneral,
  Peatypes, ComCtrls,DPSControls, DBListEx, frmMediosPagos,
  Provider, DBClient, Menus, VariantComboBox,
  FreDomicilio, ConstParametrosGenerales, FreTelefono,
  FrmEditarDomicilio, UtilMails, Validate, ListBoxEx, frmDatoPesona,
  RStrings,frmDocPresentada,frmMediosPagosConvenio,frmFirmarConvenio,
  frmMedioEnvioDocumentosCobro,FrmRepresentantes,frmImprimirConvenio,
  Convenios, frmCheckListConvenio, frmMultimpleMedioPago, FrmModificacionConvenio,
  FrmNoFacturarTransitosDeCuenta, FrmBonificacionFacturacionDeCuenta, FrmMsgNormalizar, XMLCAC,
  frmFacturacionManual,
  SysUtilsCN,
  PeaProcsCN,
  frmMuestraMensaje,
  frmReporteContratoAdhesionPA, RBSetUp, Printers, frmConsultaAltaPArauco,
  frmCodigoConvenioFacturacion, BuscaClientes, frmConvenioCuentasRUT,           // TASK_096_MGO_20161220
  frmSeleccionarGrupoFacturacion;                                               // TASK_096_MGO_20161220

type
  TTipoSolicitud = (SOLICITUDCONTACTO, SINDEFINIR, INTERMEDIO);  //IMTERMEDIO: ES CUANDO ESTOY EN LA ETAPA DE TOMAR UNA SOLICITUD Y GENERAR EL CONVENIO
  TTipoImpresion = (tiConvenio, tiEtiqueta);

  TMediosComunicacion = array of TTipoMedioComunicacion;
  TDomicilios = array of TDatosDomicilio;

  TFormSolicitudContacto = class(TForm)
	ActualizarConvenio: TADOStoredProc;
	ActualizarConvenioMedioPagoAutomaticoPAC: TADOStoredProc;
	ActualizarConvenioMedioPagoAutomaticoPAT: TADOStoredProc;
	ActualizarConvenioMediosEnvioDocumentos: TADOStoredProc;
	ActualizarCuenta: TADOStoredProc;
	ActualizarDatosCliente: TADOStoredProc;
	ActualizarDatosContacto: TADOStoredProc;
	ActualizarDatosPersona: TADOStoredProc;
	ActualizarDocumentacionPresentadaConvenios: TADOStoredProc;
	ActualizarDomicilio: TADOStoredProc;
	ActualizarDomicilioContacto: TADOStoredProc;
	ActualizarDomicilioEntregaContacto: TADOStoredProc;
	ActualizarDomicilioRelacionadoContacto: TADOStoredProc;
	ActualizarEmailContacto: TADOStoredProc;
	ActualizarMaestroVehiculos: TADOStoredProc;
	ActualizarMedioComunicacionContacto: TADOStoredProc;
	ActualizarMedioComunicacionPersona: TADOStoredProc;
	ActualizarMedioPagoAutomaticoPAC: TADOStoredProc;
	ActualizarMedioPagoAutomaticoPAT: TADOStoredProc;
	ActualizarOperacionesFuenteSC: TADOStoredProc;
	ActualizarPersonasDomicilios: TADOStoredProc;
	ActualizarSolicitudContacto: TADOStoredProc;
	ActualizarTurnoMovimientoEntrega: TADOStoredProc;
	ActualizarVehiculosContacto: TADOStoredProc;
	BlanquearPasswordPersona: TADOStoredProc;
	cb_POS: TVariantComboBox;
	cb_RecInfoMail: TCheckBox;
	cbFuentesOrigenContacto: TComboBox;
	CrearOrdenServicioAltaCuenta: TADOStoredProc;
	dblVehiculos: TDBListEx;
	dspVehiculosContacto: TDataSetProvider;
	dsVehiculosContacto: TDataSource;
	FMDomicilioPrincipal: TFrameDomicilio;
	FreDatoPersona:TFreDatoPresona;
	FreTelefonoPrincipal: TFrameTelefono;
	FreTelefonoSecundario: TFrameTelefono;
	GBDomicilios: TGroupBox;
	GBMediosComunicacion: TGroupBox;
	GBOtrosDatos: TGroupBox;
	Label2: TLabel;
	lbl_EstadoSolicitud: TLabel;
	lbl_POS: TLabel;
	lbl_Pregunta: TLabel;
	lbl_RepresentanteLegal: TLabel;
	lbl_Ubicacion: TLabel;
	lblEmail: TLabel;
	lblEnviar: TLabel;
	lblPreguntaFuenteOrigenContacto: TLabel;
	mnu_EnviarMail: TMenuItem;
	NB_Botones: TNotebook;
	NBook_Email: TNotebook;
	ObtenerDatosMedioPagoAutomaticoSolicitud: TADOStoredProc;
	ObtenerDatosSCContacto: TADOStoredProc;
	ObtenerDomiciliosContacto: TADOStoredProc;
	ObtenerMediosComunicacionContacto: TADOStoredProc;
	ObtenerVehiculo: TADOStoredProc;
	ObtenerVehiculosContacto: TADOStoredProc;
	//Panel1: TPanel;	// TASK_081_MGO_20161102
	Panel1: TScrollbox;	// TASK_081_MGO_20161102
	Panel6: TPanel;
	pnl_Botones: TPanel;
	pnl_BotonesVehiculos: TPanel;
	pnl_RepLegal: TPanel;
	pnlDatosContacto: TPanel;
	pnlVehiculos: TPanel;
	PopupMenu: TPopupMenu;
	QryTraerMediosComunicacion: TADOQuery;
	spActualizarRepresentantesConvenios: TADOStoredProc;
	spObtenerRepresentantesUltimoConvenio: TADOStoredProc;
	txt_UbicacionFisicaCarpeta: TEdit;
	txtEMailContacto: TEdit;
	txtEmailParticular: TEdit;
    btn_DocRequerido: TButton;
    btn_RepresentanteLegal: TButton;
    btnAgregarVehiculo: TButton;
    btnEditarVehiculo: TButton;
    btnEliminarVehiculo: TButton;
    btn_AsignarTag: TButton;
    btn_EximirFacturacion: TButton;
    btn_BonificarFacturacion: TButton;
    btnSalir: TButton;
    btnNuevaSolicitud: TButton;
    btn_NuevoContrato: TButton;
    btnGuardarDatos: TButton;
    btn_Cancelar: TButton;
    btn_Imprimir: TButton;
    btn_CancelarContrato: TButton;
    btn_GuardarSolicitud: TButton;
    btn_SalirSolo: TButton;
    btn_BlanquearClave: TButton;
    btn_CancelarConvenioPuro: TButton;
    btn_ImprimirConvenioPuro: TButton;
    cdsIndemnizaciones: TClientDataSet;
    spGenerarMovimientoCuentaConTelevia: TADOStoredProc;
    SPAgregarAuditoriaConveniosCAC: TADOStoredProc;
    spConveniosActivos: TADOStoredProc;
    spObtenerCliente: TADOStoredProc;

    cb_RecibeClavePorMail: TCheckBox;                                                   //SS-1006-NDR-20120614
    spAgregarMovimientoListasDeAcceso: TADOStoredProc;
    cdsVehiculosContacto: TClientDataSet;
    spExistenMovimientosFacturacionInmediata: TADOStoredProc;
    GBFacturacion: TGroupBox;
    lbl1: TLabel;
    RBDomicilioEntregaOtro: TRadioButton;
    RBDomicilioEntrega: TRadioButton;
    BtnEditarDomicilioEntrega: TButton;
    lbl_OtroDomicilio: TLabel;
    lblTipoConvenio: TLabel;
    lbl_PagoAutomatico: TLabel;
    cb_PagoAutomatico: TVariantComboBox;
    btn_EditarPagoAutomatico: TButton;
    lbl_DescripcionMedioPago: TLabel;
    lbl2: TLabel;
    vcbTipoDocumentoElectronico: TVariantComboBox;
    cbbTipoConvenio: TVariantComboBox;
    btn_ConvenioFacturacion: TButton;
    txt_NumeroConvenioFacturacion: TEdit;
    btn_MedioDoc: TButton;
    lbl3: TLabel;
    lbl4: TLabel;
    txt_RUTFacturacion: TEdit;
    btn_RUTFacturacion: TButton;
    lbl_NombreRutFacturacion: TLabel;
    btnConvFactVehiculo: TButton;
    chkCtaComercialPredeterminada: TCheckBox;
    chkInactivaDomicilioRNUT: TCheckBox;
    chkInactivaMediosComunicacionRNUT: TCheckBox;
    cbbTipoCliente: TVariantComboBox;                                  //TASK_040_ECA_20160628
    lblTipoCliente: TLabel;                                            //SS_628_ALA_20111202
    spRegistrarMovimientoStock: TADOStoredProc;
    lblGrupoFacturacion: TLabel;
    btnGrupoFacturacion: TButton;
	procedure btn_editarClick(Sender: TObject);
	procedure btnGuardarDatosClick(Sender: TObject);
	procedure txtTelefonoParticularKeyPress(Sender: TObject;
      var Key: Char);
	procedure txtTelefonoComercialKeyPress(Sender: TObject; var Key: Char);
	procedure txtTelefonoMovilKeyPress(Sender: TObject; var Key: Char);
    procedure btn_CancelarClick(Sender: TObject);
	procedure btnEditarVehiculoClick(Sender: TObject);
    procedure btnEliminarVehiculoClick(Sender: TObject);
	procedure btnAgregarVehiculoClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtDocumentoExit(Sender: TObject);
	procedure txtDocumentoKeyPress(Sender: TObject; var Key: Char);
	procedure btnNuevaSolicitudClick(Sender: TObject);
    procedure dblVehiculosDblClick(Sender: TObject);
    procedure txtEmailParticularExit(Sender: TObject);
    procedure txtDocumentoKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
	procedure FreTelefonoSecundariotxtTelefonoExit(Sender: TObject);
	procedure ExitCombos(Sender: TObject);
    procedure RBDomicilioEntregaOtroClick(Sender: TObject);
    procedure BtnEditarDomicilioEntregaClick(Sender: TObject);
    procedure btn_EditarPagoAutomaticoClick(Sender: TObject);
    procedure cb_PagoAutomaticoChange(Sender: TObject);
    procedure FMDomicilioPrincipaltxt_numeroCalleExit(Sender: TObject);
	procedure txtDocumentoChange(Sender: TObject);
    procedure FreDatoPresona1cbPersoneriaChange(Sender: TObject);
	procedure FreDatoPersonatxtDocumentoExit(Sender: TObject);

	procedure FreDatoPersonatxtDocumentoKeyDown(Sender: TObject;
	  var Key: Word; Shift: TShiftState);
	procedure FreDatoPersonatxtDocumentoKeyPress(Sender: TObject;
      var Key: Char);
    procedure btn_SalirSoloClick(Sender: TObject);
    procedure btn_ImprimirClick(Sender: TObject);
	procedure cb_POSChange(Sender: TObject);
    procedure btn_CancelarContratoClick(Sender: TObject);
    procedure btn_RepresentanteLegalClick(Sender: TObject);
	procedure btn_DocRequeridoClick(Sender: TObject);
	procedure btn_GuardarSolicitudClick(Sender: TObject);
	procedure btn_MedioDocClick(Sender: TObject);
	procedure btn_AsignarTagClick(Sender: TObject);
    procedure txtEMailContactoChange(Sender: TObject);
    procedure btn_BlanquearClaveClick(Sender: TObject);
	procedure cb_ObservacionesClick(Sender: TObject);
    procedure btn_VerObservacionesClick(Sender: TObject);
	procedure cdsVehiculosContactoAfterScroll(DataSet: TDataSet);
    procedure mnu_EnviarMailClick(Sender: TObject);
    procedure PopupMenuPopup(Sender: TObject);
    procedure lblEnviarMouseEnter(Sender: TObject);
	procedure lblEnviarMouseLeave(Sender: TObject);
    procedure lblEnviarClick(Sender: TObject);
    procedure dblVehiculosColumnsHeaderClick(Sender: TObject);
    procedure btn_EximirFacturacionClick(Sender: TObject);
    procedure btn_BonificarFacturacionClick(Sender: TObject);
	procedure FreDatoPersonatxtDocumentoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure vcbTipoDocumentoElectronicoChange(Sender: TObject);
    procedure ObtenerGrupoFacturacion;                                          // TASK_096_MGO_20161220
    procedure ActualizarDescripcionGrupoFacturacion;                            // TASK_096_MGO_20161220
    procedure dblVehiculosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;	//SS_916_PDO_20120111
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure FreDatoPersonatxtDocumentoChange(Sender: TObject);
    //procedure cb_AdheridoPAEnter(Sender: TObject);                //TASK_004_ECA_20160411
    //procedure chkConvenioSinCuentaEnter(Sender: TObject);         //TASK_004_ECA_20160411
    //procedure cb_AdheridoPAClick(Sender: TObject);                //SS_916_PDO_20120111 //TASK_004_ECA_20160411
    procedure ConvenioFacturacion();                                   // TASK_004_ECA_20160411
    procedure btn_ConvenioFacturacionClick(Sender: TObject);
    procedure cbbTipoConvenioChange(Sender: TObject);
    procedure btn_RUTFacturacionClick(Sender: TObject);
    procedure btnConvFactVehiculoClick(Sender: TObject);
    procedure btnGrupoFacturacionClick(Sender: TObject);

  private

	// variables de control de accesos
	FRepresentantesValidados,
	DocControlada:Boolean;
	DocControladaAnt:Boolean;
	MedioDocControlada:Boolean;
	RepLegal:Boolean;
	FTelMandanteHabilitado:Boolean;
	FObservaciones : String;
	//FEsconvenioCN,															//SS_1147_MCA_20140408
    FEsConvenioNativo,															//SS_1147_MCA_20140408
	FEditarRepresentateLegal1,
	FEditarRepresentateLegal2,
	FEditarRepresentateLegal3: Boolean;

    POS_Anterior:Integer;
    PuedeEditarSolicitud:Boolean;
	RegMedioEnvioCorreo: TMedioEnvioDocumentosCobro;
    FechaEntrega:TDate;
    FRUTCargado: Boolean;
	FCodigoSolicitud: Integer;
    FCodigoFuenteSolicitud: integer;
    FCodigoFuenteOrigenContacto: integer;
	FIndiceDomicilio,
	FIndiceDomicilioAlternativo,
    FIndiceDomicilioEntrega: Integer;
	FFechaInicio: TDateTime;
	FFechaFin: TDateTime;
    IndiceDomicilioPrincipalActual, IndiceDomicilioAlternativoActual: integer;
	ModoPantalla: TStateConvenio;
	FTipoSolicitud:TTipoSolicitud;
	EsNueva: boolean;
	EmailOrigenSolicitud: AnsiString;
	DomicilioAlternativo: TDatosDomicilio;
    DatosMedioPago: TDatosMediosPago;
	EstaCargandoPantalla: boolean;
	DatoPersona:TDatosPersonales;
	RepresentateLegal1:TDatosPersonales;
    RepresentateLegal2:TDatosPersonales;
	RepresentateLegal3:TDatosPersonales;
	FDocumentacionPresentada: RegDocumentacionPresentada;
	FDocumentacionPresentadaPorOS: ATOrdenServicio;
	FRegDatoMandante:TDatosPersonales;
    Cargando : Boolean;
    IndiceMedioComunicacionMandante: Integer;
    FCheckListConvenio: TCheckListConvenio;
    FListObservacionConvenio: TListObservacionConvenio;
	FPuntoEntrega: Integer;
	FPuntoVenta: Integer;


    FImprimirAnexos3y4 : byte; {indica si debe imprimir anexos 3 y 4
    								0 = no
                                    3 = anexo 3
                                    7 = anexo 3 y 4  (4 por si s�lo no existe)}


    FCodigoConceptoAIngresar : integer;		{indica si debe colocar el concepto 213 � 214 si es
    										arriendo en cuotas. 0 = respetar el que viene en el arreglo}
    FCodigoConcesionariaNativa:Integer;		//SS_1147_MCA_20140408

    FRNUTUTF8: Integer;
    FPathNovedadXML: String;
    FEsConvenioSinCuenta: Boolean;
    FCodigoConvenio: Integer;   // SS_964_PDO_20110531

    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216

    FUltimaBusqueda: TBusquedaCliente; //TASK_004_ECA_20160411

    //procedure ArmarListaPatentes(var Cadena: TStringList; sacarPatente: AnsiString = ''; sacarPatenteRVM: AnsiString = '');       //TASK_106_JMA_20170206
    procedure ArmarListaPatentes(var Cadena: TStringList; sacarPatente: AnsiString = '');                                           //TASK_106_JMA_20170206
    procedure ArmarListaTags(var Cadena: TStringList; sacarTag: AnsiString = '');
	function CargarSolicitudContacto(codigoSolicitudContacto: integer): boolean;//(IndicePersonaActual: Integer; TipoDocumento, NumeroDocumento: string): Boolean;
	function  PuedeGuardarGenerico:Boolean;
	function  PuedeGuardarContrato:Boolean;
	function  GuardarMediosComunicacionContacto (IndicePersonaActual, CodigoSolicitudContactoActual: integer):Boolean;
	procedure GuardarMedioComunicacion(IndicePersonaActual, CodigoSolicitudContactoActual, TipoOrigen, CodigoArea: integer; Valor, Anexo: AnsiString);
	procedure HabilitarBotonesVehiculos;
	procedure HabilitarPaneles(habilitar : Boolean);
	procedure CargarVehiculosContacto(CodigoSolicitudContacto: integer);
	procedure LimpiarCampos;
	procedure LimpiarDocumentacion;
    procedure HabilitarBotones;
    procedure SolicitudNueva;
    procedure CargarSolicitudExistente(CodigoSolicitud,IndiceContacto:Integer);
    procedure HabilitarControles(habilita: boolean = true);
    procedure CambioPagoAutomatico(MostrarPantallita: boolean = true);
	procedure ObtenerDatosRepresentantes(CodigoPersona: LongInt);

    procedure SetModoPantalla(Modo: TStateConvenio; TipoSolicitud: TTipoSolicitud);
    function InicializarFormulario: Boolean;
    function GuardarSolicitudContacto: Boolean;
    procedure ActualizarMediosEnvioDocumentacion;
	function GuardarContrato(NumeroContrato: String): Boolean;
    function DarCaptionMensaje:String;
    function GrabarMediosComunicacion(CodigoTipoMedioContacto:Integer;
        CodigoArea:Smallint;Valor:String;Anexo,CodigoDomicilio:integer;HorarioDesde,HorarioHasta:TDateTime;
		Observaciones:String;CodigoPersona:Integer;Principal:Boolean;EstadoVerificacion:String; CodigoMedioComunicacion: Integer):Integer;

	function GrabarDomicioConvenio(CodigoPersona:Integer;Region,Comuna:String;CodigoCalle,CodigoSegmento:Integer;
        CalleDesnormalizada:String;Principal:Boolean;Numero,CodigoPostal,Detalle:String; Depto:string ; CodigoDomicilio: Integer = -1):Integer;
    procedure LlamarMedioPagoSolicitud;
    procedure LlamarMedioPagoConvenio;
    function GrabarPersona(Personeria,RazonSocial,Apellido,ApellidoMaterno,Nombre,Sexo,Giro:String;
        CodigoPersona:Integer;Codigodocumento,NumeroDocumento,LugarNacimiento:String;FechaNacimiento:TDateTime;
		CodigoActividad:Integer;Password, Pregunta, Respuesta:String;RecibeClavePorMail: boolean = false): Integer;
	function SecuenciaGuardarSolicitudContacto: Boolean;
{INICIO:  TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)
	function ImprimirConvenioEtiqueta(NumeroConvenio:String; TipoImpresion:TTipoImpresion; FechaAlta : TDateTime; var MotivoCanelacion:TMotivoCancelacion):Boolean;
}
{TERMINO:  TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)}
	procedure ManejarPersonaSolicitud(TipoDocumento,NumeroDocumento:String);

    function EjecutarSP(StoredProc:TADOStoredProc;Var Mensaje:String; CantidadIntentos:Integer=3):Boolean;

	//Manejo de Convenios Puros
    function ObtenerDatosPersonas(CodigoPersona:Integer;Var RegistroPersona:TDatosPersonales; TipoDocumento:String='';NumeroDocumento:String=''):Boolean;
    function ObtenerDomicilioPersonas(CodigoPersona:Integer;Var RegistroDomicilio:TDomicilios; Principal:Boolean=False):Boolean;
    function ObtenerMedioComunicacion(CodigoPersona:Integer;var MediosComunicacion:TMediosComunicacion):boolean;
    function TieneConvenio(NumeroDocumento,TipoDocumento:String; SoloActivos: Boolean = False; SoloConstanera: Boolean = False): Boolean;
	function DarPrimerConvenio(NumeroDocumento,TipoDocumento:String; SoloActivos: Boolean = False; SoloConstanera: Boolean = False): String;
    function TieneSolicitud(NumeroDocumento,TipoDocumento:String): Boolean;
	//function EstaAsociadoaAlgunConvenioCN(NumeroDocumento,TipoDocumento:String): boolean;				//SS_1147_MCA_20140408
    function EstaAsociadoaAlgunConvenioNativo(NumeroDocumento,TipoDocumento:String): boolean;           //SS_1147_MCA_20140408
    function ValidarDatosRepresentates: Boolean;

    procedure ArmarEximirFacturacion(RecNoVehiculo: Integer; var RecordEximir: TEximirFacturacion);
    procedure ArmarBonificarFacturacion(RecNoVehiculo: Integer; var RecordBonificar: TBonificarFacturacion);
    procedure ActualizarEximicionFacturacion(RecNoVehiculo: Integer; RecordEximir: TEximirFacturacion);
    procedure ActualizarBonificacionFacturacion(RecNoVehiculo: Integer; RecordBonificar: TBonificarFacturacion);

    procedure AcomodarBotonesVehiculo;

    function ObtenerCantidadVehiculosConvenio: Integer;
    procedure ArmarListaVouchers( var Lista: TStringList);
    function RegistrarLogOperacionConvenio(NumeroDocumento: string;
      CodigoConvenio: integer; Patente:string ; IndiceVehiculo: integer; Etiqueta : string;
      CodigoPuntoEntrega: integer; Usuario, Accion: string): boolean;
    procedure MostrarMensajeRUTConvenioCuentas(NumeroDocumento: String);
	procedure CargarPantalla(Persona: TDatosPersonales; Domicilio: TDatosDomicilio);
    function EsConvenioSinCuenta() : Boolean;                                   //SS_628_ALA_20110720
    procedure ImprimirContratoAdhesionPA;
    procedure CargarTipoConvenios;                                              //TASK_004_ECA_20160411
    function BuscarNombreRUT(CodigoPersona: Integer): string;                   //TASK_004_ECA_20160411

    procedure ExcluirTipoConvenios(NumeroDocumento: String);                    //TASK_008_ECA_20160502
    function TieneConvenioEnBaja(NumeroDocumento: String): Boolean;             //TASK_008_ECA_20160502
    procedure CargarDatosFacturacion(NumeroDocumento: String);                  //TASK_008_ECA_20160502
    procedure InsertarHistoricoConvenio(CodigoConvenio: Integer);               //TASK_008_ECA_20160502

    procedure OnClickVehiculoRUTSelected(CodigoConvenio: Integer; CodigoVehiculo: Integer ); //TASK_009_ECA_20160506
    procedure MostrarVehiculosRUT();                                                         //TASK_009_ECA_20160506
    procedure AgregarVehiculosCuentaComercial(Vehiculo: array of TCuentaVehiculo);                    //TASK_009_ECA_20160506
    procedure GuardarVehiculosCuentaComercial (CodConvFact: Integer);                        //TASK_009_ECA_20160506
    procedure ActualizarVehiculosCuentaComercial(CodigoConvenioRUT,CodigoConvenioFacturacion,CodigoVehiculoRUT: Integer; Usuario: string);   //TASK_009_ECA_20160506

    procedure ActualizarPersonaCuentaComercial(CodigoPersona,CuentaComercial: Integer);   //TASK_012_ECA_20160514
    procedure InicializarValores();                                                       //TASK_012_ECA_20160514

    procedure CargarConvenioClienteTipo;                                              //TASK_040_ECA_20160628

    function DatosConvenio(NumeroConvenio: string; PuntoVenta, PuntoEntrega: Integer): TDatosConvenio;                  //TASK_048_ECA_20160706
 protected
	FTipoContacto: AnsiString;
	FCodigoCliente: integer;
	FCodigoPersona: integer;
	FActualizarBusqueda: Boolean;
	FCodigoComunicacion: Integer;
	FFechaHoraInicioComunicacion: TDateTime;
	FIndicePersona: integer;
	FFuente:integer;
	FPuedeModificar: Boolean;
	procedure Loaded; override;

  public
	{ Public declarations }
    function Inicializa(MDIChild: Boolean; CaptionForm: AnsiString;
        PuntoEntrega, PuntoVenta: Integer; CodigoSolicitud: Integer = -1;
        Modo: TStateConvenio = scNormal; IndicePersona: Integer = -1;
        TipoSolicitud: TTipoSolicitud = SolicitudContacto;
        Fuente: Integer = FUENTE_CALLCENTER; PuedeModificar: Boolean = True; EsConvenioSinCuenta: Boolean = False): Boolean; overload;
    function InicializaConvenioConValores(Persona: TDatosPersonales; Domicilio: TDatosDomicilio; MDIChild: Boolean; CaptionForm: AnsiString;
        PuntoEntrega, PuntoVenta: Integer; CodigoSolicitud: Integer = -1;
        Modo: TStateConvenio = scNormal; IndicePersona: Integer = -1;
        TipoSolicitud: TTipoSolicitud = SolicitudContacto;
        Fuente: Integer = FUENTE_CALLCENTER; PuedeModificar: Boolean = True): Boolean;
    property PuntoEntrega: Integer read FPuntoEntrega;
	property PuntoVenta: Integer read FPuntoVenta;

    function ValidarTagsEnArriendo : boolean;

    function EsGiroValido(Giro : string; MostrarMensaje : boolean) : boolean;

	procedure IniciarCargaConRut;overload;
	procedure IniciarCargaConRut(Modosilencioso: boolean);overload;

    procedure OnClickConvenioSelected(NumeroConvenio: String; CodigoConvenio: Integer);  //TASK_004_ECA_20160411
    procedure OnClickVehiculoSelected(NumeroConvenio: String; CodigoConvenio: Integer);  //TASK_004_ECA_20160411

    function ValidarCuentaRNUTConCuentasActivas(NumeroDocumento: string): Boolean;       //TASK_016_ECA_20160526
  end;


var
  FormSolicitudContacto: TFormSolicitudContacto;

  CodigoConvenioFacturacion : Integer;            //TASK_004_ECA_20160411
  CodigoClienteFacturacion  : Integer;            //TASK_004_ECA_20160411
  CodigoGrupoFacturacion    : Integer;              // TASK_096_MGO_20161220
  CODIGO_TIPO_CONVENIO_RNUT : Integer;             //TASK_004_ECA_20160411
  CODIGO_TIPO_CONVENIO_CTA_COMERCIAL: Integer;    //TASK_004_ECA_20160411
  CODIGO_TIPO_CONVENIO_INFRACTOR: Integer;        //TASK_004_ECA_20160411

  CodigoConvFactVehiculo    : Integer;            //TASK_004_ECA_20160411
  NumeroConvFactVehiculo    : string;            //TASK_004_ECA_20160411

  CodigoConvenioRUT         : Integer;            //TASK_005_ECA_20160420
  CodigoVehiculoRUT         : Integer;            //TASK_005_ECA_20160420

  CuentaComercialPredeterminada : Integer;       //TASK_012_ECA_20160514

  PuedeCrearCuentaComercial: Boolean;            //TASK_016_ECA_20160526
implementation

uses DMConnection, FrmSCDatosVehiculo,
	frmSeleccionSolicitudDuplicada, Mensajes, FrmHacerEncuesta, Main;

{$R *.dfm}

resourceString
    MSG_GUARDAR_DATOS_CAPTION = '&Guardar Datos';
    CAPTION_EDITAR_PAGO='Editar &pago';

    CAPTION_MEDIO_PAGO='Editar &Pago';
	CAPTION_MEDIO_PAGO_VER='Ver &Pago';
	CAPTION_OTRO_DOMICILIO='Editar &domicilio de facturaci�n';
    CAPTION_OTRO_DOMICILIO_VER='Ver &domicilio de facturaci�n';



{ TNavWindowDatosCliente }

{-----------------------------------------------------------------------------
Function Name   : ValidarTagsEnArriendo
Author          : mbecerra
Date Created    : 29-Dic-2008
Description     : Ref SS 769) Despliega los mensajes apropiados cuando se da de alta un veh�culo
            	que indican si el costo asociado por arriendo de telev�a en cuotas.
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.ValidarTagsEnArriendo;
resourcestring
	MSG_ERROR_TAGS_ARRIENDO = 'Error al validar Tags en arriendo';
 //MSG_COBRO_450           = 'Debido a que posee env�o de Nota de Cobro por correo postal el cargo mensual ser� de $450. �Est� seguro de continuar?';            //SS-948-MBE-20110413
    //MSG_COBRO_390           = 'Debido a que posee env�o de Nota de Cobro por correo electr�nico el cargo mensual ser� de $390. �Est� seguro de continuar?';       //SS-948-MBE-20110413
    MSG_COBRO_POSTAL        = 'Debido a que posee env�o de Nota de Cobro por correo postal, el cargo mensual de su Cuota de Arriendo de TAG ser� de $%d. �Est� seguro de continuar?';               //SS-948-MBE-20110413
    MSG_COBRO_MAIL          = 'Debido a que posee env�o de Nota de Cobro por correo electr�nico, el cargo mensual de su Cuota de Arriendo de TAG ser� de $%d. �Est� seguro de continuar?';          //SS-948-MBE-20110413
{INICIO: TASK_005_JMA_20160418	
    MSG_SQL_MAIL            = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL()';
    MSG_SQL_POSTAL          = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL()';
}
	MSG_SQL_MAIL            = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL(dbo.ObtenerConcesionariaNativa())';
    MSG_SQL_POSTAL          = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL(dbo.ObtenerConcesionariaNativa())';
{TERMINO: TASK_005_JMA_20160418}

    MSG_SQL_VALOR			= 'SELECT dbo.ObtenerImporteTotalConceptoMovimiento(%d, ''%s'')';  		   //SS-948-MBE-20110413
var
    CuotasMail, CuotasPostal,
    ValorPostal, ValorMail : Integer;																   //SS-948-MBE-20110413
    FechaDeHoy : TDateTime;																			   //SS-948-MBE-20110413

	Indice : integer;
begin
	FCodigoConceptoAIngresar := 0;
    CuotasMail		:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_MAIL);
    CuotasPostal	:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_POSTAL);
    FechaDeHoy		:= NowBase(DMConnections.BaseCAC);                 							   //SS-948-MBE-20110413
    ValorPostal		:= QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_VALOR, [CuotasPostal, FormatDateTime('yyyymmdd', FechaDeHoy)]));				//SS-948-MBE-20110413
    ValorMail		:= QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_VALOR, [CuotasMail, FormatDateTime('yyyymmdd', FechaDeHoy)]));				//SS-948-MBE-20110413
    ValorPostal		:= ValorPostal div 100;                                                 	   //SS-948-MBE-20110413
    ValorMail		:= ValorMail div 100;                                                          //SS-948-MBE-20110413


	try
    	FImprimirAnexos3y4 := 0;
        Result := False;
        cdsVehiculosContacto.First;
        while not (Result or cdsVehiculosContacto.Eof) do begin
            Result := (cdsVehiculosContacto.FieldByName('Almacen').Value = CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS);
            cdsVehiculosContacto.Next;
    	end;

        if Result then begin    //ver si tiene env�o por correo electr�nico
            FImprimirAnexos3y4 := 3;
            FCodigoConceptoAIngresar := CuotasPostal;
            Result := False;
        	Indice := 0;
        	while (not Result) and (Indice <  Length(RegMedioEnvioCorreo)) do begin
                Result := (	(RegMedioEnvioCorreo[Indice].CodigoTipoMedioEnvio = CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO) and
                            (RegMedioEnvioCorreo[Indice].CodigoMedioEnvioDocumento = CONST_MEDIO_ENVIO_DOCUMENTACION_COBRO) );
                Inc(Indice);
        	end;

            if Result then begin
            	Result := (MsgBox(Format(MSG_COBRO_MAIL, [ValorMail]), Caption, MB_YESNO + MB_ICONQUESTION) = ID_YES);   			//SS-948-MBE-20110413
                FImprimirAnexos3y4 := 7;
                FCodigoConceptoAIngresar := CuotasMail;
            end
            else Result := (MsgBox(Format(MSG_COBRO_POSTAL, [ValorPostal]), Caption, MB_YESNO + MB_ICONQUESTION) = IDYES);			//SS-948-MBE-20110413

        end
        else Result := True;

    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR_TAGS_ARRIENDO, e.Message, Caption, MB_ICONERROR);
            Result := False;
    	end;
    end;

end;

{-----------------------------------------------------------------------------
Procedure Name  : Inicializa
Author          : dcepeda       ()
Date Created    : 10-Mayo-2010  ()
Description     : Inicializa los objetos en pantalla, seg�n condiciones, permisos.
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.Inicializa(MDIChild: Boolean; CaptionForm: AnsiString;
    PuntoEntrega, PuntoVenta: Integer; CodigoSolicitud: Integer = -1;
    Modo: TStateConvenio = scNormal; IndicePersona: Integer = -1;
    TipoSolicitud: TTipoSolicitud = SolicitudContacto;
    Fuente: Integer = FUENTE_CALLCENTER; PuedeModificar: Boolean = True; EsConvenioSinCuenta: Boolean = False): Boolean;
resourcestring
    STR_FUENTE   = ' - Fuente ';
    STR_CONVENIO = 'Convenio';

    MSG_PARAM_XML_NOT_FOUND = 'Ha ocurrido un error intentando obtener el par�metro %s';
    MSG_PARAM_XML_TITLE     = 'Error obteniendo un par�metro general de la interfaz RNUT-XML';

    CONST_CODIGO_TIPO_CONVENIO_RNUT='SELECT dbo.CONST_CODIGO_TIPO_CONVENIO_RNUT() ';                    //TASK_004_ECA_20160411
    CONST_CODIGO_TIPO_CONVENIO_CTA_COMERCIAL= 'SELECT dbo.CONST_CODIGO_TIPO_CONVENIO_CTA_COMERCIAL() '; //TASK_004_ECA_20160411
    CONST_CODIGO_TIPO_CONVENIO_INFRACTOR= 'SELECT dbo.CONST_CODIGO_TIPO_CONVENIO_INFRACTOR() ';         //TASK_004_ECA_20160411
Var
	S: TSize;
    ok: Boolean;
begin
    ok := True;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    // Si no podemos obtener el par�metro general de la interfaz RNUT no podemos inicializar el form
    if not ObtenerParametroGeneral(DMConnections.BaseCAC,'PATH_ARCHIVO_SALIDA_XML', FPathNovedadXML) then begin
        Result := False;
        MsgBoxErr(MSG_PARAM_XML_TITLE, Format(MSG_PARAM_XML_NOT_FOUND, ['PATH_ARCHIVO_SALIDA_XML']), Caption, MB_ICONERROR);
        Exit;
    end;
    // Si no podemos obtener el par�metro general de la interfaz RNUT de la codificaci�n UTF-8 no podemos inicializar el form
    if not ObtenerParametroGeneral(DMConnections.BaseCAC,'RNUT_CODIFICAR_UTF8', FRNUTUTF8) then begin
        Result := False;
        MsgBoxErr(MSG_PARAM_XML_TITLE, Format(MSG_PARAM_XML_NOT_FOUND, ['RNUT_CODIFICAR_UTF8']), Caption, MB_ICONERROR);
        Exit;
    end;

    
	try
        FPuntoEntrega := PuntoEntrega;
		FPuntoVenta := PuntoVenta;
        EstaCargandoPantalla := True;
		ModoPantalla := Modo;
        FTipoSolicitud := TipoSolicitud;
        FPuedeModificar := PuedeModificar;

        vcbTipoDocumentoElectronico.Clear;
        vcbTipoDocumentoElectronico.Items.Add(TDCBX_AUTOMATICO_STR, TDCBX_AUTOMATICO_COD);
        vcbTipoDocumentoElectronico.Items.Add(DescriTipoComprobante(TC_BOLETA), TC_BOLETA);
        vcbTipoDocumentoElectronico.Items.Add(DescriTipoComprobante(TC_FACTURA), TC_FACTURA);
        vcbTipoDocumentoElectronico.ItemIndex := 0;

	    if MDIChild then begin
		    //S := GetFormClientSize(Application.MainForm);	// TASK_081_MGO_20161102
		    //SetBounds(0, 0, S.cx, S.cy);					// TASK_081_MGO_20161102
	    end else begin
			FormStyle := fsNormal;
			Visible := False;
		end;

        if not(InicializarFormulario) then begin
			Result := False;
			Exit;
        end;

        Cargando := False;
        Self.FCodigoSolicitud := CodigoSolicitud;

        FFechaInicio := NowBase(DMConnections.BaseCAC);

        case FTipoSolicitud of
			SOLICITUDCONTACTO:  begin
                    if (self.FCodigoSolicitud = -1) then
						Self.SolicitudNueva
                    else CargarSolicitudContacto(self.FCodigoSolicitud);

                    if ModoPantalla = scModi then begin
                        if (IndicePersona > 0) then CargarSolicitudExistente(CodigoSolicitud, IndicePersona)
                        else begin
                            Result := False;
                            Exit;
						end;
                    end;
			end;
        end;

        FFuente                             := Fuente;
        //chkConvenioSinCuenta.Enabled        := ExisteAcceso('BIT_Convenio_Sin_Cuenta');        //SS_628_ALA_20111202		//SS_916_PDO_20120111
        FEsConvenioSinCuenta                := EsConvenioSinCuenta;
        Self.Caption                        := CaptionForm + STR_FUENTE + Trim(QueryGetValue(DMConnections.BaseCAC, Format('SELECT Descripcion FROM FuentesSolicitud (NOLOCK) WHERE CodigoFuenteSolicitud = %d', [FFuente])));
		SetModoPantalla(ModoPantalla, FTipoSolicitud);

        FCodigoConcesionariaNativa := ObtenerCodigoConcesionariaNativa;			//SS_1147_MCA_20140408
        //if FCodigoConcesionariaNativa = CODIGO_VS then                      //SS_1147_MCA_20150410 //TASK_004_ECA_20160411
        //    cb_AdheridoPA.Visible := False;                                 //SS_1147_MCA_20150410 //TASK_004_ECA_20160411

        //cb_HabilitadoAMB.Enabled            := ExisteAcceso('BIT_Habilitado_Lista_Blanca_AMB') ;         //SS-1006-NDR-20111105
        //cb_HabilitadoPA.Enabled            := ExisteAcceso('BIT_Habilitado_Lista_Acceso_PA') ;             //SS-1006-NDR-20111105		//SS_916_PDO_20120111


        //INICIO: TASK_004_ECA_20160411
        CODIGO_TIPO_CONVENIO_RNUT:= QueryGetValueInt(DMConnections.BaseCAC,CONST_CODIGO_TIPO_CONVENIO_RNUT);
        CODIGO_TIPO_CONVENIO_CTA_COMERCIAL:= QueryGetValueInt(DMConnections.BaseCAC,CONST_CODIGO_TIPO_CONVENIO_CTA_COMERCIAL);
        CODIGO_TIPO_CONVENIO_INFRACTOR:= QueryGetValueInt(DMConnections.BaseCAC,CONST_CODIGO_TIPO_CONVENIO_INFRACTOR);

        CargarTipoConvenios;
        cbbTipoConvenio.Enabled:=False;
        btn_RUTFacturacion.Enabled:=False;
        btn_ConvenioFacturacion.Enabled:= False;
        lbl_NombreRutFacturacion.Caption:='';
        //FIN: TASK_004_ECA_20160411
         btn_DocRequerido.Visible:= False;

         cbbTipoCliente.Visible:=False;  //TASK_040_ECA_20160628
         lblTipoCliente.Visible:=False;  //TASK_040_ECA_20160628
         CargarConvenioClienteTipo;      //TASK_040_ECA_20160628
         
        CodigoGrupoFacturacion := 0;                                            // TASK_096_MGO_20161220
        ActualizarDescripcionGrupoFacturacion;                                  // TASK_096_MGO_20161220
    except
        on E: Exception do begin
			MsgBoxErr(Format(MSG_ERROR_INICIALIZACION, [STR_CONVENIO]), E.Message,
                CaptionForm, MB_ICONSTOP);
			ok := False;
        end;
	end;
    //EnableControlsInContainer(pnlVehiculos, True);		//SS_916_PDO_20120111
    EstaCargandoPantalla := False;
	Result := ok;
end;


{-------------------------------------------------------------------------------
Function Name   : InicializaConvenioConValores
Author          : dAllegretti
Date Created    : 24/10/2008
Description     : Inicializar utilizado para cargar al form con datos desde la facturacion de infracciones
-------------------------------------------------------------------------------}
function TFormSolicitudContacto.InicializaConvenioConValores(Persona: TDatosPersonales; Domicilio: TDatosDomicilio; MDIChild: Boolean;
    CaptionForm: AnsiString; PuntoEntrega, PuntoVenta: Integer; CodigoSolicitud: Integer = -1;
    Modo: TStateConvenio = scNormal; IndicePersona: Integer = -1;
    TipoSolicitud: TTipoSolicitud = SolicitudContacto;
    Fuente: Integer = FUENTE_CALLCENTER; PuedeModificar: Boolean = True): Boolean;
Var
    ok: Boolean;
begin
    Ok := Inicializa(MDIChild,CaptionForm, PuntoEntrega, PuntoVenta, CodigoSolicitud, Modo, IndicePersona, TipoSolicitud, Fuente, PuedeModificar, True);
    if Ok then CargarPantalla(Persona, Domicilio);
    IniciarCargaConRut;
    cb_PagoAutomatico.Value := TPA_NINGUNO;
    CambioPagoAutomatico(True);
	Result := Ok;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_editarClick
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_editarClick(Sender: TObject);
begin
	Close;
end;

{-----------------------------------------------------------------------------
Procedure Name  : CargarSolicitudContacto
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.CargarSolicitudContacto(CodigoSolicitudContacto: integer): boolean;
    function EsMedioPagoPAC(TipoMedioPago: AnsiString = ''): boolean;
    begin
        result := Trim(QueryGetValue(DMConnections.BaseCAC,'select dbo.CONST_MEDIO_PAGO_AUTOMATICO_PAC()')) = TipoMedioPago;
	end;
	function EsMedioPagoPAT(TipoMedioPago: AnsiString = ''): boolean;
    begin
        result := Trim(QueryGetValue(DMConnections.BaseCAC,'select dbo.CONST_MEDIO_PAGO_AUTOMATICO_PAT()')) = TipoMedioPago;
    end;
var
    IndicePersonaActual: integer;
    CargueElPrincipal: boolean;
    Domicilio: TDatosDomicilio;
    TipoMedioPagoAutomatico: integer;
	DescripMedioPago:String;
    DescripcionEstadoSolicitud:String;
    Modo:Variant;
begin
    EstaCargandoPantalla := true;
    IndiceDomicilioPrincipalActual := -1;
    IndiceDomicilioAlternativoActual := -1;
    FIndiceDomicilioEntrega := -1;
    lbl_OtroDomicilio.Caption := '';
    lbl_OtroDomicilio.Hint:='';
    ObtenerDatosSCContacto.close;
	ObtenerDatosSCContacto.Parameters.ParamByName('@CodigoSolicitudContacto').value := CodigoSolicitudContacto;

	try
		ObtenerDatosSCContacto.Open;
		Result := ObtenerDatosSCContacto.RecordCount > 0;
	except
		Result := False;
	end;
	if not Result then Exit;

    // Estado Solicitud
    PuedeEditarSolicitud:=ObtenerDatosSCContacto.FieldByName('PermiteEdicion').AsBoolean;
	DescripcionEstadoSolicitud:=ObtenerDatosSCContacto.FieldByName('DescripcionEstadoSolicitud').AsString;

    //FUENTE DE LA SOLICITUD
	FCodigoFuenteSolicitud := QueryGetValueInt(DMConnections.BaseCAC,
        'SELECT IsNull(CodigoFuenteSolicitud, 0) FROM SolicitudContacto WITH (NOLOCK) WHERE CodigoSolicitudContacto = '
		+ IntToStr(CodigoSolicitudContacto));


    //MEDIO POR EL QUE SE ENTERO DE LA INSCRIPCION FUENTEORIGENCONTACTO
    FCodigoFuenteOrigenContacto := QueryGetValueInt(DMConnections.BaseCAC,
        'SELECT IsNull(CodigoFuenteOrigenContacto, 0) FROM SolicitudContacto WITH (NOLOCK) WHERE CodigoSolicitudContacto = '+ IntToStr(CodigoSolicitudContacto));
    CargarFuentesOrigenContacto(DMConnections.BaseCAC, cbFuentesOrigenContacto, FCodigoFuenteOrigenContacto);


    //DATOS CONTACTOS
	With ObtenerDatosSCContacto do begin
        DatoPersona.Personeria:=FieldByName('Personeria').AsString[1];
        DatoPersona.NumeroDocumento := UpperCase(TrimRight(FieldByName('NumeroDocumento').AsString));
		DatoPersona.Password:=FieldByName('Password').AsString;
		DatoPersona.Pregunta:=FieldByName('Pregunta').AsString;
        DatoPersona.Respuesta:=FieldByName('Respuesta').AsString;

        if (FieldByName('Personeria').AsString = PERSONERIA_FISICA) then begin
			DatoPersona.Nombre := TrimRight(FieldByName('Nombre').AsString);
		    DatoPersona.Apellido := TrimRight(FieldByName('Apellido').AsString);
			DatoPersona.ApellidoMaterno := TrimRight(FieldByName('ApellidoMaterno').AsString);
		    DatoPersona.Sexo:=FieldByName('Sexo').AsString[1];
			DatoPersona.FechaNacimiento:=FieldByName('FechaNacimiento').AsDateTime;
		end
        else
		begin
            if (FieldByName('Personeria').AsString = PERSONERIA_JURIDICA) then begin
                DatoPersona.RazonSocial := TrimRight(FieldByName('Apellido').AsString);
                DatoPersona.Apellido := TrimRight(FieldByName('ApellidoContactoComercial').AsString);
                DatoPersona.ApellidoMaterno := TrimRight(FieldByName('ApellidoMaternoContactoComercial').AsString);
                DatoPersona.Nombre := TrimRight(FieldByName('NombreContactoComercial').AsString);
                DatoPersona.Sexo:= FieldByName('SexoContactoComercial').AsString[1];

            end;
		end;

        DatoPersona.Giro:=FieldByName('Giro').AsString;
        IndicePersonaActual := FieldByName('IndicePersona').AsInteger;
        DatoPersona.CodigoPersona:=IndicePersonaActual;
        if (FieldByName('IndiceDomicilioEntrega').IsNull) then
            FIndiceDomicilioEntrega := -1
		else
			FIndiceDomicilioEntrega := FieldByName('IndiceDomicilioEntrega').AsInteger;
        if (FieldByName('TipoMedioPagoAutomatico').IsNull) then
            TipoMedioPagoAutomatico := -1
        else
            TipoMedioPagoAutomatico := FieldByName('TipoMedioPagoAutomatico').AsInteger;


		cb_pos.Enabled := False;
        if cb_pos.Enabled then  begin cb_POS.OnChange(cb_POS); POS_Anterior:=integer(cb_POS.Value) end
        else lbl_POS.Caption:=STR_PUNTO_ENTREGA_NO_DISPONIBLES;

        Close;
        FreDatoPersona.RegistrodeDatos:=DatoPersona;
	end;


    //***** DOMICILIO ******//
    //Son dos domicilios, uno principal y el otro alternativo.
    //El primero que ingresa es el Principal.
    with ObtenerDomiciliosContacto do begin
		Close;
		Parameters.Refresh;
        Parameters.ParamByName('@CodigoSolicitudContacto').Value   := CodigoSolicitudContacto;
        Parameters.ParamByName('@IndicePersona').Value             := IndicePersonaActual;
        Open;
        First;
        FIndiceDomicilio := FieldByName('IndiceDomicilio').AsInteger;
        IndiceDomicilioPrincipalActual := FieldByName('IndiceDomicilio').AsInteger;
        with Domicilio do begin
            CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
			CodigoSegmento := iif(FieldByName('CodigoSegmento').IsNull, -1, FieldByName('CodigoSegmento').AsInteger);
            NumeroCalleSTR := FieldByName('Numero').AsString;
			NumeroCalle := FormatearNumeroCalle(FieldByName('Numero').AsString);
			Detalle := FieldByName('Detalle').AsString;
            CodigoPais := FieldByName('CodigoPais').AsString;
            CodigoRegion := FieldByName('CodigoRegion').AsString;
            CodigoComuna := FieldByName('CodigoComuna').AsString;
			CalleDesnormalizada := iif(FieldByName('CodigoCalle').IsNull, FieldByName('CalleDesnormalizada').AsString,'');
            DescripcionCiudad := '';
            CodigoPostal := FieldByName('CodigoPostal').AsString;
		end;
		FMDomicilioPrincipal.CargarDatosDomicilio(Domicilio);
        Next;
        //Significa que hay otro domicilio.
        if not ObtenerDomiciliosContacto.Eof then begin
			FIndiceDomicilioAlternativo := FieldByName('IndiceDomicilio').AsInteger;
			IndiceDomicilioAlternativoActual := FieldByName('IndiceDomicilio').AsInteger;
            //Aca tengo que guardarlo en el Registro...
            with DomicilioAlternativo do begin
                IndiceDomicilio := FieldByName('IndiceDomicilio').AsInteger;
                IndiceDomicilioAlternativoActual := FieldByName('IndiceDomicilio').AsInteger;
                CodigoTipoDomicilio := FieldByName('TipoDomicilio').AsInteger;
                CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
                CodigoSegmento := iif(FieldByName('CodigoSegmento').IsNull, -1,FieldByName('CodigoSegmento').AsInteger);
                NumeroCalleSTR := FieldByName('Numero').AsString;
				NumeroCalle := FormatearNumeroCalle(FieldByName('Numero').AsString);
                Detalle:=FieldByName('Detalle').AsString;
				CodigoPais:=FieldByName('CodigoPais').AsString;
                CodigoRegion:=FieldByName('CodigoRegion').AsString;
                CodigoComuna:=FieldByName('CodigoComuna').AsString;
				CalleDesnormalizada := iif(FieldByName('CodigoCalle').IsNull, FieldByName('CalleDesnormalizada').AsString,'');

                if FieldByName('CodigoCalle').IsNull then
                    CalleDesnormalizada := FieldByName('CalleDesnormalizada').AsString
                else
					CalleDesnormalizada := QueryGetValue(DMConnections.BaseCAC,
                        FORMAT('SELECT DBO.ObtenerDescripcionCalle (''%s'', ''%s'', ''%s'', %d) ', [CodigoPais,CodigoRegion,CodigoComuna, CodigoCalle]));

				DescripcionCiudad := '';
                CodigoPostal := FieldByName('CodigoPostal').AsString;
				DomicilioEntrega :=  IndiceDomicilioAlternativoActual = FIndiceDomicilioEntrega;

                Descripcion := ArmarDomicilioSimple(DMConnections.BaseCAC,
                                CalleDesnormalizada, NumeroCalleSTR,
                                0, '', Detalle);
                lbl_OtroDomicilio.Caption := Descripcion;
                lbl_OtroDomicilio.Hint:=Descripcion;

                if (FIndiceDomicilioEntrega <> -1) and (IndiceDomicilioAlternativoActual = FIndiceDomicilioEntrega) then
                    RBDomicilioEntregaOtro.Checked := true
				else RBDomicilioEntrega.Checked := true;
            end;
		end;
        if (FIndiceDomicilioEntrega = -1) then begin
            RBDomicilioEntrega.Checked := false;
            RBDomicilioEntregaOtro.Checked := false;
        end
		else begin
            if FIndiceDomicilioEntrega = IndiceDomicilioPrincipalActual then begin
                RBDomicilioEntrega.Checked := true;
			end else begin
                if FIndiceDomicilioEntrega = IndiceDomicilioAlternativoActual then
                    RBDomicilioEntregaOtro.Checked := true;
            end;
        end;
		Close;

    end;

	txtEmailParticular.Text := '';
	txtEmailParticular.OnChange(txtEmailParticular);

    CargueElPrincipal := false; //una vez que lo cargu�, cargo el secundario.

    with QryTraerMediosComunicacion do
	begin
        Close;
		Parameters.ParamByName('CodigoSolicitudContacto').Value := CodigoSolicitudContacto;
        Parameters.ParamByName('IndicePersona').Value := IndicePersonaActual;
        Open;
        First;
        while not EOF do
        begin
            if (FieldByName('FormatoMedioContacto').AsString = Peatypes.TMC_TELEFONO) then begin
				//Puede ser uno de los dos tel�fonos, principal o secundarios
				//se toma como principal al de menor �ndice.
                if not CargueElPrincipal then begin
                    FreTelefonoPrincipal.CargarTelefono(FieldByName('CodigoArea').AsInteger,Trim(FieldByName('Valor').AsString),FieldByName('CodigoTipoMedioContacto').AsInteger,FieldByName('HorarioDesde').AsDateTime,FieldByName('HorarioHasta').AsDateTime);
                    CargueElPrincipal := true;
                end
				else begin
					FreTelefonoSecundario.CargarTelefono(FieldByName('CodigoArea').AsInteger,Trim(FieldByName('Valor').AsString),FieldByName('CodigoTipoMedioContacto').AsInteger);
				end;
            end
            else begin
                if (FieldByName('FormatoMedioContacto').AsString = Peatypes.TMC_EMAIL) then begin
                    if NBook_Email.PageIndex=0 then begin
                    	txtEMailContacto.Text := Trim(FieldByName('Valor').AsString);
                        txtEMailContacto.OnChange(txtEMailContacto);
                    end else begin
						txtEmailParticular.Text := Trim(FieldByName('Valor').AsString);
                        txtEmailParticular.OnChange(txtEmailParticular);
					end;
                end;
            end;
            Next;
        end;
    end;

    case TipoMedioPagoAutomatico of
		-1: begin
			//marco el item que indique que falta ingresar.
            //cb_PagoAutomatico.Value := TipoMedioPagoAutomatico;
			cb_PagoAutomatico.ItemIndex:=0;
            lbl_DescripcionMedioPago.Caption:=SELECCIONAR;
			DatosMedioPago.TipoMedioPago:=NINGUNO;
		end;
        0: begin //ninguno
            //Puede ser que haya elegido explicitamente Ninguno, Pat o Pac
			cb_PagoAutomatico.Value := TipoMedioPagoAutomatico;
            cb_PagoAutomatico.OnChange(cb_PagoAutomatico);
            lbl_DescripcionMedioPago.Caption:='';
            DatosMedioPago.TipoMedioPago:=NINGUNO;
        end;
        else begin
			//Puede ser que haya elegido explicitamente Ninguno, Pat o Pac
            cb_PagoAutomatico.Value := TipoMedioPagoAutomatico;

            //***** CARGAR DATOS DEL MEDIO DE PAGO AUTOM�TICO *****//

            with ObtenerDatosMedioPagoAutomaticoSolicitud do
			begin
                Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoSolicitudContacto').Value := CodigoSolicitudContacto;
				Open;
                //Revisar si encontr� alguno o No.
                if not EOF then begin
					First;
                    DatosMedioPago.RUT := FieldByName('NumeroDocumento').AsString;
					DatosMedioPago.Nombre := FieldByName('Nombre').AsString;
					DatosMedioPago.Telefono := FieldByName('Telefono').AsString;
                    DatosMedioPago.CodigoArea  := FieldByName('CodigoArea').AsInteger;

                    if EsMedioPagoPAC (FieldByName('TipoMedioPago').AsString) then begin
                        DatosMedioPago.TipoMedioPago := PAC;
						DatosMedioPago.PAC.TipoCuenta := FieldByName('CodigoTipoCuentaBancaria').AsInteger;
                        DatosMedioPago.PAC.CodigoBanco := FieldByName('CodigoBanco').AsInteger;
                        DatosMedioPago.PAC.NroCuentaBancaria := FieldByName('NroCuentaBancaria').AsString;
                        DatosMedioPago.PAC.Sucursal := FieldByName('Sucursal').AsString;
					end
                    else begin
						if EsMedioPagoPAT (FieldByName('TipoMedioPago').AsString) then begin
                            DatosMedioPago.TipoMedioPago := PAT;
                            DatosMedioPago.PAT.TipoTarjeta := FieldByName('CodigoTipoTarjetaCredito').AsInteger;
                            DatosMedioPago.PAT.NroTarjeta := FieldByName('NumeroTarjetaCredito').AsString;
                            DatosMedioPago.PAT.FechaVencimiento := FieldByName('FechaVencimiento').AsString;
                            DatosMedioPago.PAT.EmisorTarjetaCredito := iif(trim(FieldByName('CodigoEmisorTarjetaCredito').AsString)='',-1,FieldByName('CodigoEmisorTarjetaCredito').AsInteger);
                        end;
                    end;
					if not(TFormMediosPagos.ArmarMedioPago(DatosMedioPago,DescripMedioPago)) then DescripMedioPago:='';
                    lbl_DescripcionMedioPago.Caption:=DescripMedioPago;
				end else begin
                    lbl_DescripcionMedioPago.Caption:='';
                end;
			end;
		end;
    end;
    //***** CARGAR LOS DATOS DEL VEH�CULO *****//
    with ObtenerVehiculosContacto do begin
        Close;
        Parameters.ParamByName('@CodigoSolicitudContacto').Value   := CodigoSolicitudContacto;
        Open;
		cdsVehiculosContacto.Data := dspVehiculosContacto.Data;
        if not ObtenerVehiculosContacto.IsEmpty then HabilitarBotonesVehiculos;
		Close;
    end;
	  EstaCargandoPantalla := false;
    FCodigoSolicitud := CodigoSolicitudContacto;

    if not(PuedeEditarSolicitud) then modo:=scNormal else modo:= ModoPantalla;
    SetModoPantalla(Modo , FTipoSolicitud);
    if trim(DescripcionEstadoSolicitud)<>'' then lbl_EstadoSolicitud.Caption:='Estado Solicitud: '+DescripcionEstadoSolicitud;

end;

{-----------------------------------------------------------------------------
Procedure Name  : Loaded
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.Loaded;
begin
	inherited;
	Color := $00FFFDFB;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btnGuardarDatosClick
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btnGuardarDatosClick(Sender: TObject);
begin
	if SecuenciaGuardarSolicitudContacto then begin
        LimpiarCampos;
        HabilitarControles(false);
        FreDatoPersona.txtDocumento.SetFocus;
        if ModoPantalla = scModi then close;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : GuardarMediosComunicacionContacto
Author          :
Date Created    :
Description     : Se hacen las llamadas para actualizar los atributos de
                Medios de Comunicaci�n
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.GuardarMediosComunicacionContacto (IndicePersonaActual, CodigoSolicitudContactoActual: integer):Boolean;
resourcestring
    MSG_ACTUALIZAR_MEDIO_CONTACTO_ERROR = 'No se pudo actualizar el medio de comunicaci�n del contacto';
    MSG_ACTUALIZAR_MEDIO_CONTACTO_CAPTION = 'Actualizar contacto';
begin

    //Guardo los datos en la tabla MediosComunicacion
	try
        QueryExecute( DMConnections.BaseCAC,
        'DELETE FROM SolicitudContactoMediosComunicacion WHERE IndicePersona = ' + IntToStr(IndicePersonaActual) +
        ' AND CodigoSolicitudContacto = ' + IntToStr(CodigoSolicitudContactoActual));

		//Telefono Particular
		if (FreTelefonoPrincipal.CargoTelefono) then begin
            GuardarMedioComunicacion(IndicePersonaActual, CodigoSolicitudContactoActual,
              FreTelefonoPrincipal.CodigoTipoTelefono, FreTelefonoPrincipal.CodigoArea,FreTelefonoPrincipal.NumeroTelefono, '');
		end;

        //Telefono Comercial
        if (FreTelefonoSecundario.CargoTelefono) then begin
            GuardarMedioComunicacion(IndicePersonaActual, CodigoSolicitudContactoActual,
			  FreTelefonoSecundario.CodigoTipoTelefono, FreTelefonoSecundario.CodigoArea,FreTelefonoSecundario.NumeroTelefono, '');
		end;

		//Email Particular
        if NBook_Email.PageIndex=0 then begin
            if (Trim(txtEMailContacto.Text) <> '') then
                GuardarMedioComunicacion(IndicePersonaActual, CodigoSolicitudContactoActual,
                  TIPO_MEDIO_CONTACTO_E_MAIL, 0, Trim(txtEMailContacto.Text), '');
        end else begin
            if (Trim(txtEmailParticular.Text) <> '') then
                GuardarMedioComunicacion(IndicePersonaActual, CodigoSolicitudContactoActual,
				  TIPO_MEDIO_CONTACTO_E_MAIL, 0, Trim(txtEmailParticular.Text), '');
        end;
        Result := True;

    except
		on E: exception do begin
			MsgBoxErr( MSG_ACTUALIZAR_MEDIO_CONTACTO_ERROR, e.message, MSG_ACTUALIZAR_MEDIO_CONTACTO_CAPTION, MB_ICONSTOP);
            ActualizarMedioComunicacionContacto.Close;
            Screen.Cursor := crDefault;
            Result := False;
            Exit;
		end;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : GuardarMedioComunicacion
Author          :
Date Created    :
Description     : Se actualizan los atributos de medios de comunicaci�n en Base de Datos.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.GuardarMedioComunicacion(IndicePersonaActual, CodigoSolicitudContactoActual,
  TipoOrigen, CodigoArea: integer; Valor, Anexo: AnsiString);
begin
    ActualizarMedioComunicacionContacto.Close;
	with ActualizarMedioComunicacionContacto.Parameters do begin
		Refresh;
        ParamByName('@IndicePersona').Value         := IndicePersonaActual;
        ParamByName('@CodigoSolicitudContacto').Value   := CodigoSolicitudContactoActual;
        ParamByName('@CodigoTipoMedioContacto').Value  := TipoOrigen;
        ParamByName('@Valor').Value                 := Valor;
		ParamByName('@IndiceDomicilio').Value       := null;
        ParamByName('@CodigoArea').Value            := CodigoArea;
        ParamByName('@HorarioDesde').Value          := FreTelefonoPrincipal.HorarioDesde;
        ParamByName('@HorarioHasta').Value          := FreTelefonoPrincipal.HorarioHasta;
        ParamByName('@Observaciones').Value         := null;
		ParamByName('@Anexo').Value                 := iif ((Trim(Anexo) <> ''), Trim(Anexo), null);
		ActualizarMedioComunicacionContacto.ExecProc;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : txtTelefonoParticularKeyPress
Author          :
Date Created    :
Description     : Valida que solo se ingresen valores numericos y gui�n en
                tel�fono particular.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.txtTelefonoParticularKeyPress(
  Sender: TObject; var Key: Char);
begin
	if not (Key  in ['0'..'9','-', #8]) then
        Key := #0;
end;

{-----------------------------------------------------------------------------
Procedure Name  : txtTelefonoComercialKeyPress
Author          :
Date Created    :
Description     : Valida que solo se ingresen valores numericos y gui�n en
                tel�fono Comercial.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.txtTelefonoComercialKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9','-', #8]) then
		Key := #0;
end;

{-----------------------------------------------------------------------------
Procedure Name  : txtTelefonoMovilKeyPress
Author          :
Date Created    :
Description     : Valida que solo se ingresen valores numericos y gui�n en
                tel�fono Movil.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.txtTelefonoMovilKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9','-', #8]) then
        Key := #0;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_CancelarClick
Author          :
Date Created    :
Description     : Cancela el ingreso de un alta de Convenio.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_CancelarClick(Sender: TObject);
resourcestring
    ESTA_SEGURO_CANCELAR = '�Esta seguro de cancelar el ingreso?';
begin
    //Si esta en modo alta y ya ingres� algo pregunta
    if (RBDomicilioEntrega.Enabled) or (FreDatoPersona.txtDocumento.Enabled) then begin
        if ModoPantalla = scAlta then begin
            if MsgBox(ESTA_SEGURO_CANCELAR, self.Caption, MB_YESNO) = mrYes then begin
                btnSalir.OnClick(sender);
            end else begin
                if FreDatoPersona.txtDocumento.Enabled then FreDatoPersona.txtDocumento.SetFocus
                else begin
                    if FreDatoPersona.RegistrodeDatos.Personeria = PERSONERIA_JURIDICA  then FreDatoPersona.txtNombre_Contacto.setfocus
                    else FreDatoPersona.txt_Nombre.setfocus;
                end;
            end;
		end
        else btnSalir.OnClick(sender);
	end else begin
        LimpiarCampos;
        HabilitarControles(false);
        FreDatoPersona.txtDocumento.SetFocus;
        FreDatoPersona.txtDocumento.SelStart:=length(FreDatoPersona.txtDocumento.Text);
        FreDatoPersona.txtDocumento.SelLength:=0;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : HabilitarBotonesVehiculos
Author          :
Date Created    :
Description     : Habilita los botones relacionados a Veh�culos.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.HabilitarBotonesVehiculos;
begin
    btnEditarVehiculo.Enabled           := (dblVehiculos.DataSource.DataSet.Active) and (dblVehiculos.DataSource.DataSet.RecordCount > 0);
    btnEliminarVehiculo.Enabled         := btnEditarVehiculo.Enabled;
//	btn_AsignarTag.Enabled              := btnEditarVehiculo.Enabled;       // SS_916_PDO_20120111
	btn_AsignarTag.Enabled              := False;                           // SS_916_PDO_20120111
    btn_EximirFacturacion.Enabled       := btnEditarVehiculo.Enabled;
    btn_BonificarFacturacion.Enabled    := btnEditarVehiculo.Enabled;
    btnConvFactVehiculo.Enabled         := btnEditarVehiculo.Enabled;       // TASK_004_ECA_20160411
end;

{-----------------------------------------------------------------------------
Procedure Name  : btnEliminarVehiculoClick
Author          :
Date Created    :
Description     : Funcionalidad que elimina Veh�culo.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btnEliminarVehiculoClick(Sender: TObject);
ResourceString
    MSG_DELETE_VEHICULO_QUESTION   = '�Desea eliminar este Veh�culo?';
    MSG_DELETE_VEHICULO_CAPTION    = 'Eliminar Veh�culo';
	MSG_DELETE_VEHICULO_ERROR      = 'No se ha podido eliminar el Veh�culo';
var
	f: TformDocPresentada;
    auxCant : Integer;
    PatenteAEliminar: string;
begin

	if cdsVehiculosContacto.RecordCount = 0 then Exit;
	Screen.Cursor   := crHourGlass;
	If MsgBox(MSG_DELETE_VEHICULO_QUESTION, MSG_DELETE_VEHICULO_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try

            if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, 0, cdsVehiculosContacto.FieldByName('Patente').AsString,
              cdsVehiculosContacto.FieldByName('IndiceVehiculo').AsInteger,cdsVehiculosContacto.FieldByName('SerialNumberaMostrar').AsString,
              FPuntoEntrega, UsuarioSistema, 'Baja')  then
                raise Exception.create('Error registrando auditoria');

            PatenteAEliminar := dsVehiculosContacto.DataSet.FieldByName('Patente').AsString;
			TformDocPresentada.BorrarRegistro(
				FDocumentacionPresentadaPorOS[0].Documentacion,
                dsVehiculosContacto.DataSet.FieldByName('Patente').AsString,
				dsVehiculosContacto.DataSet.FieldByName('TipoPatente').AsString);
			dsVehiculosContacto.DataSet.Delete;

			Application.CreateForm(TformDocPresentada,f);
			f.Inicializar(LimpiarCaracteresLabel(btn_DocRequerido.Caption), TClientDataSet(dsVehiculosContacto.DataSet),
                    FDocumentacionPresentadaPorOS[0].Documentacion, FreDatoPersona.RegistrodeDatos.Personeria, DatosMedioPago.TipoMedioPago,
					iif(trim(RepresentateLegal1.Nombre) = '', '', ArmarNombrePersona(RepresentateLegal1.Personeria, RepresentateLegal1.Nombre, RepresentateLegal1.Apellido, RepresentateLegal1.ApellidoMaterno)),
                    iif(trim(RepresentateLegal2.Nombre) = '', '', ArmarNombrePersona(RepresentateLegal2.Personeria, RepresentateLegal2.Nombre, RepresentateLegal2.Apellido, RepresentateLegal2.ApellidoMaterno)),
                    iif(trim(RepresentateLegal3.Nombre) = '', '', ArmarNombrePersona(RepresentateLegal3.Personeria, RepresentateLegal3.Nombre, RepresentateLegal3.Apellido, RepresentateLegal3.ApellidoMaterno)),
                    TformMedioEnvioDocumentosCobro.EnvioDocumentoCobroViaMail(RegMedioEnvioCorreo), auxCant, ModoPantalla);
            f.Release;

           DocControlada := DocControlada or (length(FDocumentacionPresentadaPorOS[0].Documentacion) = auxCant);

            cdsIndemnizaciones.First;
            while not cdsIndemnizaciones.Eof do begin
                if cdsIndemnizaciones.FieldByName('Patente').AsString = PatenteAEliminar then
                    cdsIndemnizaciones.Delete
                else
                    cdsIndemnizaciones.Next;
            end;

		Except
			On E: Exception do begin
				dsVehiculosContacto.DataSet.Cancel;
				MsgBoxErr( MSG_DELETE_VEHICULO_ERROR, e.message, MSG_DELETE_VEHICULO_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Screen.Cursor   := crDefault;
    HabilitarBotonesVehiculos;

    //INICIO: TASK_033_ECA_20160610
    if (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) then
    begin
        btnEditarVehiculo.Enabled:=False;
        btn_AsignarTag.Enabled:=False;
        btnConvFactVehiculo.Enabled:=False;
        btn_Imprimir.Enabled:=True;
        btnEliminarVehiculo.Enabled:=True;
        btn_EximirFacturacion.Enabled:=True;
        btn_BonificarFacturacion.Enabled:=True;
    end;
    //FIN: TASK_033_ECA_20160610
end;

{-----------------------------------------------------------------------------
Procedure Name  : btnEditarVehiculoClick
Author          :
Date Created    :
Description     : Permite modificar atributos de un veh�culo.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btnEditarVehiculoClick(Sender: TObject);
ResourceString
	MSG_ERROR_EDITAR_VEHICULO      = 'No se pudieron modificar los datos del veh�culo';
	MSG_EDITAR_VEHICULO_CAPTION    = 'Actualizar datos del veh�culo';
var
    f : TFormSCDatosVehiculo;
    Cadena, CadenaTags: TStringList;
    dondeEstaba: TBookmark;
    NoCargar: string;
    Estado: Variant;
    PatenteAnterior: string;
begin
    if dblVehiculos.DataSource.DataSet.RecordCount = 0 then Exit;

    Cadena := TStringList.Create;
	CadenaTags := TStringList.Create;
    dondeEstaba := cdsVehiculosContacto.GetBookmark;
	try
        if (cdsVehiculosContacto.fieldbyname('ContractSerialNumber').IsNull) then
            NoCargar := ''
        else
            NoCargar := cdsVehiculosContacto.fieldbyname('ContractSerialNumber').Value;

		//ArmarListaPatentes(Cadena, Trim(dsVehiculosContacto.DataSet.FieldByName('Patente').AsString), Trim(dsVehiculosContacto.DataSet.FieldByName('PatenteRVM').AsString));      //TASK_106_JMA_20170206
        ArmarListaPatentes(Cadena, Trim(dsVehiculosContacto.DataSet.FieldByName('Patente').AsString));        //TASK_106_JMA_20170206

		ArmarListaTags(CadenaTags, Trim(NoCargar));
       cdsVehiculosContacto.GotoBookmark(dondeEstaba);
     finally
        cdsVehiculosContacto.FreeBookmark(dondeEstaba);
    end;

  if (FTipoSolicitud=INTERMEDIO) then Estado:= Peatypes.ConvenioModiVehiculo else Estado:= Peatypes.Solicitud;

    PatenteAnterior := dsVehiculosContacto.DataSet.FieldByName('Patente').AsString;
	Application.CreateForm(TFormSCDatosVehiculo, f);
	if f.InicializarEditar(MSG_CAPTION_EDITAR_VEHICULO, PuntoEntrega,													//TASK_106_JMA_20170206
        Trim(dsVehiculosContacto.DataSet.FieldByName('TipoPatente').AsString),
        Trim(dsVehiculosContacto.DataSet.FieldByName('Patente').AsString),
        //Trim(dsVehiculosContacto.DataSet.FieldByName('PatenteRVM').AsString),                                         //TASK_106_JMA_20170206
        dsVehiculosContacto.DataSet.FieldByName('Modelo').AsString,
        dsVehiculosContacto.DataSet.FieldByName('CodigoMarca').AsInteger,
        dsVehiculosContacto.DataSet.FieldByName('AnioVehiculo').AsInteger,
        //dsVehiculosContacto.DataSet.FieldByName('CodigoTipoVehiculo').AsInteger,                                      //TASK_106_JMA_20170206
        dsVehiculosContacto.DataSet.FieldByName('CodigoColor').AsInteger,
		Trim(dsVehiculosContacto.DataSet.FieldByName('DigitoVerificadorPatente').AsString),
        //Trim(dsVehiculosContacto.DataSet.FieldByName('DigitoVerificadorPatenteRVM').AsString),                         //TASK_106_JMA_20170206
		-1,
        dsVehiculosContacto.DataSet.FieldByName('Observaciones').AsString,
        FCodigoSolicitud,
        Cadena,
        (FTipoSolicitud = INTERMEDIO), CadenaTags,
        iif(dsVehiculosContacto.DataSet.FieldByName('ContextMark').IsNull, -1, dsVehiculosContacto.DataSet.FieldByName('ContextMark').AsInteger),
        dsVehiculosContacto.DataSet.FieldByName('SerialNumberaMostrar').AsString,
        // False, True, -1, cdsVehiculosContacto, False, FreDatoPersona.RegistrodeDatos.CodigoPersona, Estado, nil,     // SS_916_PDO_20120111
        False, False, -1, cdsVehiculosContacto, False, FreDatoPersona.RegistrodeDatos.CodigoPersona, Estado, nil,       // SS_916_PDO_20120111
        dsVehiculosContacto.DataSet.FieldByName('Recuperado').AsBoolean
        )
		and (f.ShowModal = mrOK) then begin
        //Cargo el Vehiculo en la Lista.
        try

            with cdsVehiculosContacto do begin
                if UpperCase(trim(FieldByName('Patente').AsString))<> UpperCase(f.Patente) then  begin
					DocControlada:=False; // si edito la patente de un auto, debe volver a controlar la documentacion
					TformDocPresentada.BorrarRegistro(FDocumentacionPresentadaPorOS[0].Documentacion,
                                    FieldByName('Patente').AsString,FieldByName('TipoPatente').AsString);
				end;

                Edit;
                FieldByName('TipoPatente').AsString             := f.TipoPatente;
                FieldByName('Patente').AsString                 := f.Patente;
                FieldByName('CodigoMarca').AsInteger            := f.Marca;
                FieldByName('DescripcionMarca').AsString        := f.DescripcionMarca;
				FieldByName('Modelo').AsString                  := f.DescripcionModelo;
                FieldByName('CodigoColor').AsInteger            := f.Color;
				FieldByName('DescripcionColor').AsString        := f.DescripcionColor;
                FieldByName('AnioVehiculo').AsInteger           := f.Anio;
                //FieldByName('CodigoTipoVehiculo').AsInteger     := f.CodigoTipo;                             //TASK_106_JMA_20170206
                //FieldByName('DescripcionTipoVehiculo').AsString := f.DescripcionTipo;                        //TASK_106_JMA_20170206
                FieldByName('Observaciones').AsString           := Trim(f.Observaciones);
                FieldByName('ContextMark').Value                := f.ContextMark;
                FieldByName('ContractSerialNumber').Value       := f.SerialNumber;
                FieldByName('SerialNumberaMostrar').Value       := f.NumeroTag;
				FieldByName('TieneAcoplado').Value              := null;
                FieldByName('DigitoVerificadorPatente').Value   := f.DigitoVerificador;
				FieldByName('DigitoVerificadorValido').Value    := iif(f.DigitoVerificadorCorrecto,1,0);
                //FieldByName('PatenteRVM').Value                 := f.PatenteRVM;                               //TASK_106_JMA_20170206
                //FieldByName('DigitoVerificadorPatenteRVM').Value:= f.DigitoVerificadorRVM;                     //TASK_106_JMA_20170206

                if F.DatoCuenta.Vehiculo.Robado then begin
                    FieldByName('Robado').Value := True;
                end else begin
                    FieldByName('Robado').Value := False;
                end;

                if F.DatoCuenta.Vehiculo.Recuperado then begin
                    FieldByName('Recuperado').Value := True;
                end else begin
                    FieldByName('Recuperado').Value := False;
                end;

                post;

                if f.Patente <> PatenteAnterior then begin //cambio la pantente, actualizo el clientdataset con los datos de indemnizaciones
                    cdsIndemnizaciones.First;
                    while not cdsIndemnizaciones.Eof do begin
                        if cdsIndemnizaciones.FieldByName('Patente').AsString = PatenteAnterior then begin
                            cdsIndemnizaciones.Edit;
                            cdsIndemnizaciones.FieldByName('Patente').AsString := f.Patente;
                            cdsIndemnizaciones.Post;
                        end;
                        cdsIndemnizaciones.Next;
                    end;
                end;

            end;
		except
            On E: Exception do begin
				MsgBoxErr( MSG_ERROR_EDITAR_VEHICULO, E.message, MSG_EDITAR_VEHICULO_CAPTION, MB_ICONSTOP);
                dsVehiculosContacto.DataSet.Cancel;
            end;
		end;
    end;
	Cadena.Free;
    CadenaTags.Free;
    f.Release;
    dblVehiculos.Refresh;
    HabilitarBotonesVehiculos;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btnAgregarVehiculoClick
Author          : dcepeda       ()
Date Created    : 10-Mayo-2010  ()
Description     : Funcionalidad que agrega un nuevo veh�culo a Convenio.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btnAgregarVehiculoClick(Sender: TObject);
ResourceString
    MSG_ERROR_EDITAR_VEHICULO               = 'No se pudieron modificar los datos del veh�culo';
    MSG_EDITAR_VEHICULO_CAPTION             = 'Actualizar datos del veh�culo';
    MSG_ES_SIN_CUENTA                       = 'No puede agregar Cuentas a este Convenio ya que es de tipo Infractor';      //SS_628_ALA_20111202 //TASK_004_ECA_20160411
    MSG_CTA_COMERCIAL                       = 'No puede agregar Cuentas a este Convenio ya que es de tipo Cuenta Comercial';      //TASK_004_ECA_20160411
    MSG_CUENTA_COMERCIAL                    = 'Debe tener un Convenio RNUT con cuentas activas para poder asignar vehiculos a una Cuenta Comercial';  // TASK_005_ECA_20160407 // TASK_016_ECA_20160526
var
	f : TFormSCDatosVehiculo;
	Cadena, CadenaTags: TStringList;
    Estado : Variant;
    ListaVouchers: TStringList;
begin

    //INICIO: TASK_009_ECA_20160506
    if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL then
    begin
        if PuedeCrearCuentaComercial then
            MostrarVehiculosRUT()
        else
             MsgBoxBalloon(MSG_CUENTA_COMERCIAL, Self.Caption, MB_ICONSTOP,cbbTipoConvenio); //TASK_016_ECA_20160526
        Exit;
    end;
    //FIN: TASK_009_ECA_20160506

    if EsConvenioSinCuenta = False then begin                                                                       //SS_628_ALA_20111202


        Application.CreateForm(TFormSCDatosVehiculo, f);
        Cadena := TStringList.Create;
        CadenaTags := TStringList.Create;
        ArmarListaPatentes(Cadena);
        ArmarListaTags(CadenaTags);
        ListaVouchers:= TStringList.Create;
        ArmarListaVouchers(ListaVouchers);

        if (FTipoSolicitud=INTERMEDIO) then Estado := Peatypes.ConvenioAltaVehiculo else Estado := Peatypes.Solicitud;

{INICIO: TASK_106_JMA_20170206
        if f.Inicializar(MSG_CAPTION_AGREGAR_VEHICULO,
                        PuntoEntrega, iif(cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').AsInteger = 0, 1,
                        cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').AsInteger),
                        FCodigoSolicitud,
                        Cadena,
                        (FTipoSolicitud = INTERMEDIO),
                        CadenaTags,
                        -1,
                        -1,
                        cdsVehiculosContacto,
                        FreDatoPersona.RegistrodeDatos.CodigoPersona,
                        Estado,
                        ListaVouchers)
        and (f.ShowModal = mrOK) then begin
}

        if f.Inicializar(MSG_CAPTION_AGREGAR_VEHICULO,
                        PuntoEntrega,
                        FCodigoSolicitud,
                        Cadena,
                        (FTipoSolicitud = INTERMEDIO),     //EsConvenio
                        CadenaTags,                        //ListaTags
                        -1,                                //ContextMark
                        -1,                                //CodigoConvenio
                        cdsVehiculosContacto,
                        FreDatoPersona.RegistrodeDatos.CodigoPersona,
                        Estado,
                        ListaVouchers)
        and (f.ShowModal = mrOK) then begin
{TERMINO: TASK_106_JMA_20170206}
            //Cargo el Vehiculo en la Lista.
            try
                //Registra auditoria estado anterior  Rev 11
                if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, 0 , F.Patente, 0, f.NumeroTag, FPuntoEntrega, UsuarioSistema, 'Alta')  then
                    raise Exception.create('Error registrando auditoria');

                with cdsVehiculosContacto do begin
                    Append;
                    FieldByName('TipoPatente').AsString             := f.TipoPatente;
                    FieldByName('Patente').AsString                 := f.Patente;
                    FieldByName('Modelo').AsString                  := f.DescripcionModelo;
                    FieldByName('CodigoMarca').AsInteger            := f.Marca;
                    FieldByName('DescripcionMarca').AsString        := f.DescripcionMarca;
                    FieldByName('CodigoColor').AsInteger            := f.Color;
                    FieldByName('DescripcionColor').AsString        := f.DescripcionColor;
                    FieldByName('AnioVehiculo').AsInteger           := f.Anio;
                    //FieldByName('CodigoTipoVehiculo').AsInteger     := f.CodigoTipo;                                    //TASK_106_JMA_20170206
                    //FieldByName('DescripcionTipoVehiculo').AsString := f.DescripcionTipo;                               //TASK_106_JMA_20170206
                    FieldByName('Observaciones').AsString           := Trim(f.Observaciones);
                    FieldByName('ContextMark').Value                := f.ContextMark;
                    FieldByName('ContractSerialNumber').Value       := f.SerialNumber;
                    FieldByName('SerialNumberaMostrar').Value       := f.NumeroTag;
                    FieldByName('TieneAcoplado').Value              := null;
                    FieldByName('DigitoVerificadorPatente').Value   := f.DigitoVerificador;
                    FieldByName('DigitoVerificadorValido').Value    := iif(f.DigitoVerificadorCorrecto,1,0);
                    //FieldByName('PatenteRVM').Value                 := f.PatenteRVM;                                      //TASK_106_JMA_20170206
                    //FieldByName('DigitoVerificadorPatenteRVM').Value:= f.DigitoVerificadorRVM;                            //TASK_106_JMA_20170206
                    FieldByName('TeleviaNuevo').Value               := True;
                    FieldByName('Foraneo').Value                    := f.Foraneo;               // TASK_055_MGO_20160801

                    FieldByName('FechaInicioOriginalEximicion').Value   := Null;
                    FieldByName('FechaInicioEximicion').Value           := Null;
                    FieldByName('FechaFinalizacionEximicion').Value     := Null;
                    FieldByName('CodigoMotivoEximicion').Value          := 0;
                    FieldByName('FechaInicioEximicionEditable').Value   := True;
                    FieldByName('FechaFinEximicionEditable').Value      := True;

                    FieldByName('FechaInicioOriginalBonificacion').Value    := Null;
                    FieldByName('FechaInicioBonificacion').Value            := Null;
                    FieldByName('FechaFinalizacionBonificacion').Value      := Null;
                    FieldByName('PorcentajeBonificacion').Value             := 0;
                    FieldByName('ImporteBonificacion').Value                := 0;
                    FieldByName('FechaInicioBonificacionEditable').Value    := True;
                    FieldByName('FechaFinBonificacionEditable').Value       := True;

                    //if F.CambioFecha then                                 // SS_916_PDO_20120111
                    FieldByName('FechaAltaCuenta').Value:= f.FechaAlta;     // SS_916_PDO_20120111

                    if F.DatoCuenta.Vehiculo.Robado then begin
                        FieldByName('Robado').Value := True;
                    end else begin
                        FieldByName('Robado').Value := False;
                    end;

                    if F.DatoCuenta.Vehiculo.Recuperado then begin
                        FieldByName('Recuperado').Value := True;
                    end else begin
                        FieldByName('Recuperado').Value := False;
                    end;

                    if f.AlmacenDestino = CONST_ALMACEN_EN_COMODATO then FieldByName('Almacen').Value := CONST_STR_ALMACEN_COMODATO;
                    if f.AlmacenDestino = CONST_ALMACEN_ARRIENDO then FieldByName('Almacen').Value := CONST_STR_ALMACEN_ARRIENDO;
                    if f.AlmacenDestino = CONST_ALMACEN_ENTREGADO_EN_CUOTAS then FieldByName('Almacen').Value := CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS;

                    FieldByName('FechaDeVencimientoTag').Value := f.FechaVencimientoTag;      //REV.3

                    FieldByName('CodigoConvenioFacturacion').Value := 0;
                    FieldByName('NumeroConvenioFacturacion').Value := '';
{INICIO: TASK_106_JMA_20170206}
                    FieldByName('CodigoCategoriaInterurbana').Value := f.DatoCuenta.CodigoCategoriaInterurbana;
                    FieldByName('CodigoCategoriaUrbana').Value := f.DatoCuenta.CodigoCategoriaUrbana;
                    FieldByName('CategoriaInterurbana').Value := f.DatoCuenta.CategoriaInterurbana;
                    FieldByName('CategoriaUrbana').Value := f.DatoCuenta.CategoriaUrbana;
{TERMINO: TASK_106_JMA_20170206}

                    Post;
                end;

                f.cdConceptos.First;
                while not f.cdConceptos.eof do begin
                    cdsIndemnizaciones.Append;
                    cdsIndemnizaciones.FieldByName('CodigoConcepto').Value          := f.cdConceptos.FieldByName('CodigoConcepto').AsInteger;
                    cdsIndemnizaciones.FieldByName('DescripcionMotivo').Value       := f.cdConceptos.FieldByName('DescripcionMotivo').AsString;
                    cdsIndemnizaciones.FieldByName('ContractSerialNumber').Value    := f.SerialNumber;
                    cdsIndemnizaciones.FieldByName('ContextMark').Value             := f.ContextMark;
                    cdsIndemnizaciones.FieldByName('CodigoAlmacen').Value           := f.AlmacenDestino ;
                    cdsIndemnizaciones.FieldByName('TipoAsignacionTag').Value       := f.TipoAsignacionTag;
                    cdsIndemnizaciones.FieldByName('Observaciones').Value           := f.NumeroVoucher;
                    cdsIndemnizaciones.FieldByName('Patente').Value                 := f.Patente ;
                    cdsIndemnizaciones.Post;
                    f.cdConceptos.Next;
                end;

                DocControlada := False; // si agrego un auto, debe volver a controlar la documentacion
            except
                On E: Exception do begin
                    MsgBoxErr( MSG_ERROR_EDITAR_VEHICULO, E.message, MSG_EDITAR_VEHICULO_CAPTION, MB_ICONSTOP);
                    dsVehiculosContacto.DataSet.Cancel;
                end;
            end;
        end;
        Cadena.Free;
        CadenaTags.Free;
        ListaVouchers.Free;
        f.Release;
        HabilitarBotonesVehiculos;
    end                                                                                                              //SS_628_ALA_20111202
    else begin                                                                                                       //SS_628_ALA_20111202
        //MsgBoxBalloon(MSG_ES_SIN_CUENTA, Self.Caption, MB_ICONSTOP,chkConvenioSinCuenta);                           //SS_628_ALA_20111202 //TASK_004_ECA_20160411
        if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_INFRACTOR then
            MsgBoxBalloon(MSG_ES_SIN_CUENTA, Self.Caption, MB_ICONSTOP,cbbTipoConvenio)                           //SS_628_ALA_20111202   //TASK_004_ECA_20160411
        else
            MsgBoxBalloon(MSG_CTA_COMERCIAL, Self.Caption, MB_ICONSTOP,cbbTipoConvenio);                          //TASK_004_ECA_20160411
    end;
    end;

{-----------------------------------------------------------------------------
Procedure Name  : CargarVehiculosContacto
Author          :
Date Created    :
Description     : se cargan los veh�culos asociados a contacto.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.CargarVehiculosContacto(CodigoSolicitudContacto: integer);
begin
    //Primero se cierra el Stored Procedure
	ObtenerVehiculosContacto.Close;
	ObtenerVehiculosContacto.Parameters.ParamByName('@CodigoSolicitudContacto').Value := CodigoSolicitudContacto;
	if not OpenTables([ObtenerVehiculosContacto]) then Exit;
	ObtenerVehiculosContacto.First;
	cdsVehiculosContacto.Data := dspVehiculosContacto.Data;
    cdsVehiculosContacto.Open;
    ObtenerVehiculosContacto.Close;
    HabilitarBotonesVehiculos;
end;

{-----------------------------------------------------------------------------
Procedure Name  : HabilitarPaneles
Author          :
Date Created    :
Description     : Habilita paneles para edici�n.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.HabilitarPaneles(habilitar : Boolean);
Begin
    pnlDatosContacto.Enabled := habilitar;
    pnl_BotonesVehiculos.Enabled:= habilitar;
    HabilitarBotonesVehiculos;

    //cb_HabilitadoAMB.Enabled := ExisteAcceso('BIT_Habilitado_Lista_Blanca_AMB') ;   //SS-106-NDR-20111105
    //cb_AdheridoPA.Enabled := ExisteAcceso('BIT_Adherido_Lista_Acceso_PA') ;          //SS-1006-NDR-20120614 //SS-106-NDR-20111105 //TASK_004_ECA_20160411
    //chkConvenioSinCuenta.Enabled := ExisteAcceso('BIT_Convenio_Sin_Cuenta');        //SS_628_ALA_20111202 //TASK_004_ECA_20160411
end;

{-----------------------------------------------------------------------------
Procedure Name  : LimpiarCampos
Author          :
Date Created    :
Description     : Limpia, reinicializa atributos en pantalla.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.LimpiarCampos;
var											// SS_1072_PDO_20121127
    f:TformMedioEnvioDocumentosCobro;		// SS_1072_PDO_20121127
begin
    //variables de control
    DocControlada:=False;
	DocControladaAnt:=False;
    MedioDocControlada:=False;
	RepLegal:=False;
	FTelMandanteHabilitado:=False;
    IndiceMedioComunicacionMandante := -1;

    POS_Anterior:=-1;
	FreDatoPersona.Panel1.Enabled:=True;
	FreDatoPersona.txtDocumento.Enabled:=True;
    FreDatoPersona.lblRutRun.Enabled:=True;
    FreDatoPersona.Limpiar;
    txtEmailParticular.Clear;
    txtEmailParticular.Tag := -1;
    txtEMailContacto.Clear;
    txtEMailContacto.tag := -1;
	FreTelefonoPrincipal.CargarTelefono(CODIGO_AREA_SANTIAGO,'',-1);
    FreTelefonoSecundario.CargarTelefono(CODIGO_AREA_SANTIAGO,'',-1);

    if (FTipoSolicitud=SOLICITUDCONTACTO) or (FTipoSolicitud=INTERMEDIO) or (FTipoSolicitud=SINDEFINIR) then
        CargarVehiculosContacto(-1);

	CargarFuentesOrigenContacto(DMConnections.BaseCAC, cbFuentesOrigenContacto);
    FMDomicilioPrincipal.Limpiar;

    // MEJORA PERFORMANCE
    FMDomicilioPrincipal.inicializa();

	//set el registro de persona
    FreDatoPersona.LimpiarRegistro(DatoPersona);
	FreDatoPersona.RegistrodeDatos:=DatoPersona; // es para que el frame tome estos valores

    // datos del representate legal
	FreDatoPersona.LimpiarRegistro(RepresentateLegal1);
	FreDatoPersona.LimpiarRegistro(RepresentateLegal2);
	FreDatoPersona.LimpiarRegistro(RepresentateLegal3);

    //label
    lbl_RepresentanteLegal.Caption:=SELECCIONAR;
    lbl_DescripcionMedioPago.Caption:=SELECCIONAR;
    lbl_EstadoSolicitud.Caption:='';

    // PAC
    TFormMediosPagos.LimpiarRegistro(DatosMedioPago);
    CargarMediosPagoAutomatico(cb_PagoAutomatico);
    cb_PagoAutomatico.OnChange(cb_PagoAutomatico);

	// Medio Envio Doc
	TformMedioEnvioDocumentosCobro.LimpiarRegistro(RegMedioEnvioCorreo);
	// SS_1072_PDO_20121127 Inicio Bloque
	try
		Application.CreateForm(TformMedioEnvioDocumentosCobro, f);
        f.rbNotaCobroEmail.Checked := True;
        RegMedioEnvioCorreo := f.RegistroMedioEvioDoc;
    finally
    	FreeAndNil(f);
    end;
	// SS_1072_PDO_20121127 Fin Bloque

	// Observaciones
    FObservaciones := '';

    TformEditarDomicilio.LimpiarRegistroDomicilio(DomicilioAlternativo);
    DomicilioAlternativo.CodigoTipoDomicilio := TIPO_DOMICILIO_COMERCIAL;

    btn_EditarPagoAutomatico.Caption:=CAPTION_EDITAR_PAGO;
    LimpiarDocumentacion;

    // check del otro domicilio
    RBDomicilioEntrega.Checked := true;
    RBDomicilioEntrega.OnClick(RBDomicilioEntrega);
end;

{-----------------------------------------------------------------------------
Procedure Name  : HabilitarBotones
Author          :
Date Created    :
Description     : Da nombre a desplegar en bot�n Guardar Datos.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.HabilitarBotones;
Begin
    btnGuardarDatos.Caption := MSG_GUARDAR_DATOS_CAPTION;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btnSalirClick
Author          :
Date Created    :
Description     : Salir de la pantalla.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
Procedure Name  : FormClose
Author          :
Date Created    :
Description     : Cierra pantalla registrando en Log.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.FormClose(Sender: TObject;
  var Action: TCloseAction);
ResourceString
    MSG_ERROR_AUDITORIA     = 'Error guardando datos auditoria';
begin

    if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, 0, '', 0, '', FPuntoEntrega, UsuarioSistema, 'Cancelar')  then
        MsgBoxErr(MSG_ERROR_AUDITORIA, MSG_ERROR_AUDITORIA, STR_ERROR, MB_ICONSTOP);

    FCheckListConvenio.Free;
    FListObservacionConvenio.Free;
    Action :=  caFree;
end;

{-----------------------------------------------------------------------------
Procedure Name  : SolicitudNueva
Author          :
Date Created    :
Description     : Configura pantalla para editar nueva solicitud en pantalla.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.SolicitudNueva;
resourcestring
    STR_ALTA_SOL = 'Alta de Solicitud';
begin
    self.caption:= STR_ALTA_SOL;
    HabilitarPaneles(True);
    LimpiarCampos;
	HabilitarBotones;
	FreDatoPersona.txtDocumento.SetFocus;
end;

{-----------------------------------------------------------------------------
Procedure Name  : txtDocumentoExit
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.txtDocumentoExit(Sender: TObject);
begin
    // Verifico si el contacto ya existe
    if btn_Cancelar.Focused or btnSalir.Focused then exit;
end;

{-----------------------------------------------------------------------------
Procedure Name  : txtDocumentoKeyPress
Author          :
Date Created    :
Description     : Valida que no se puedan ingresar valores distintos a los indicados.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.txtDocumentoKeyPress(Sender: TObject;
  var Key: Char);
begin
	if not (Key  in ['0'..'9','K','k', #8]) then
       Key := #0
end;

{-----------------------------------------------------------------------------
Procedure Name  : btnNuevaSolicitudClick
Author          :
Date Created    :
Description     : llama a Funcionalidad que configura la pantalla para una solicitud nueva.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btnNuevaSolicitudClick(Sender: TObject);
begin
    SolicitudNueva;
end;

{-----------------------------------------------------------------------------
Procedure Name  : dblVehiculosDblClick
Author          :
Date Created    :
Description     : Permite editar veh�culos desplegados en grilla.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.dblVehiculosDblClick(Sender: TObject);
begin
	if btnEditarVehiculo.Enabled and btnEditarVehiculo.visible then btnEditarVehiculo.Click;
end;

procedure TFormSolicitudContacto.dblVehiculosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column.FieldName = 'FechaAltaCuenta' then begin
        Text := FormatDateTime('dd"-"mm"-"yyyy hh:mm',cdsVehiculosContacto.FieldByName('FechaAltaCuenta').AsDateTime);
    end;
    if Column.FieldName = 'FechaDeVencimientoTag' then begin
        Text := FormatDateTime('dd"-"mm"-"yyyy hh:mm',cdsVehiculosContacto.FieldByName('FechaDeVencimientoTag').AsDateTime);
    end;
    // INICIO : TASK_055_MGO_20160801
    if (cdsVehiculosContacto.FieldByName('Foraneo').AsBoolean) then begin
        if odSelected in State then
            Sender.Canvas.Font.Color := clMaroon
        else
            Sender.Canvas.Font.Color := clMaroon;
    end;
    // FIN : TASK_055_MGO_20160801
end;

{-----------------------------------------------------------------------------
Procedure Name  : CargarSolicitudExistente
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.CargarSolicitudExistente(CodigoSolicitud,
  IndiceContacto: Integer);
begin
    FCodigoSolicitud := CodigoSolicitud;
    CargarSolicitudContacto(FCodigoSolicitud);
    FIndicePersona := IndiceContacto;
    EsNueva := False;
end;

{-----------------------------------------------------------------------------
Procedure Name  : HabilitarControles
Author          :
Date Created    :
Description     : Configura habilitaci�n de objetos pantalla seg�n permisos y condiciones.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.HabilitarControles(habilita: boolean = true);
var
    aux:Boolean;
begin
	FFechaInicio := NowBase(DMConnections.BaseCAC);
    EnableControlsInContainer(FreDatoPersona, habilita);
    FreDatoPersona.Panel1.Enabled:=true;
    FreDatoPersona.txtDocumento.Enabled:=not(habilita);
    FreDatoPersona.lblRutRun.Enabled:=not(habilita);

    EnableControlsInContainer(FreTelefonoPrincipal, habilita);
    EnableControlsInContainer(FreTelefonoSecundario, habilita);
	lblEmail.Enabled := habilita;
	txtEmailParticular.ReadOnly := not habilita;
	txtEMailContacto.ReadOnly := not habilita;

    if not(habilita) then aux:=False
    else begin
        if ((FMDomicilioPrincipal.EsCalleNormalizada) and not(FMDomicilioPrincipal.cb_Comunas.Enabled))  then aux:=False
		else aux:=True;
    end;
    EnableControlsInContainer(GBDomicilios, habilita);
    FMDomicilioPrincipal.cb_Comunas.Enabled:=aux;
    EnableControlsInContainer(GBOtrosDatos, habilita);



	if habilita then begin                                                                                 // SS_916_PDO_20120111
        //btn_VerObservaciones.Enabled := cb_Observaciones.Checked;                                        // SS_916_PDO_20120111   //TASK_065_GLE_20170408
        vcbTipoDocumentoElectronico.Enabled := ExisteAcceso('Combo_Modificar_Documento_Electronico');      // SS_916_PDO_20120111
    end;                                                                                                   // SS_916_PDO_20120111

	EnableControlsInContainer(pnl_BotonesVehiculos, habilita);
    HabilitarBotonesVehiculos;
    EnableControlsInContainer(pnl_RepLegal, habilita);

    btnGuardarDatos.Enabled := habilita;
	btn_Imprimir.Enabled := habilita;
    btn_GuardarSolicitud.Enabled := habilita;

	BtnEditarDomicilioEntrega.enabled := RBDomicilioEntregaOtro.checked and habilita;
    if habilita then btn_EditarPagoAutomatico.Enabled := (cb_PagoAutomatico.ItemIndex > 1);

    if (self.Active) and (FreDatoPersona.txtDocumento.Enabled) then begin
        FreDatoPersona.txtDocumento.SetFocus;
		FreDatoPersona.txtDocumento.SelStart:=length(FreDatoPersona.txtDocumento.Text);
		FreDatoPersona.txtDocumento.SelLength:=0;
    end;
    lblPreguntaFuenteOrigenContacto.Enabled := False;
    cbFuentesOrigenContacto.Enabled := False;
	lbl_Pregunta.Enabled := False;
    cb_POS.Enabled := False;
    lbl_POS.Enabled := False;

    

    //INICIO: TASK_004_ECA_20160411
//    if habilita then begin                                                                   // SS_916_PDO_20120111
//        //cb_HabilitadoAMB.Enabled := ExisteAcceso('BIT_Habilitado_Lista_Blanca_AMB') ;      //SS-106-NDR-20111105
//        cb_AdheridoPA.Enabled := ExisteAcceso('BIT_Adherido_Lista_Acceso_PA') ;              //SS-1006-NDR-20120614 //SS-106-NDR-20111105
//        chkConvenioSinCuenta.Enabled := ExisteAcceso('BIT_Convenio_Sin_Cuenta');             //SS_628_ALA_20111202
//    end;                                                                                     // SS_916_PDO_20120111
    //FIN: TASK_004_ECA_20160411
    // ---------------------------------------------------------------------------------
end;

{-----------------------------------------------------------------------------
Procedure Name  : txtEmailParticularExit
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.txtEmailParticularExit(Sender: TObject);
begin
    if (trim(txtEmailParticular.Text) <> '') and (not IsValidEmail(trim(txtEmailParticular.Text))) then begin
		MsgBoxBalloon(MSG_VALIDAR_DIRECCION_EMAIL, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, txtEmailParticular);
        txtEmailParticular.SetFocus;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : txtDocumentoKeyDown
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.txtDocumentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (Shift = []) and (Key = VK_ESCAPE) then close;
end;

{-----------------------------------------------------------------------------
Procedure Name  : ArmarListaPatentes
Author          :
Date Created    :
Description     : Ingresa lista de patentes convenio a lista: Cadena.
-----------------------------------------------------------------------------}
//procedure TFormSolicitudContacto.ArmarListaPatentes(var Cadena: TStringList; sacarPatente: AnsiString = ''; sacarPatenteRVM: AnsiString = '');       //TASK_106_JMA_20170206
procedure TFormSolicitudContacto.ArmarListaPatentes(var Cadena: TStringList; sacarPatente: AnsiString = '');                                           //TASK_106_JMA_20170206
begin
    if not (cdsVehiculosContacto.IsEmpty) then begin
        cdsVehiculosContacto.First;
        while not cdsVehiculosContacto.Eof do
        begin
            if (not cdsVehiculosContacto.FieldByName('Patente').IsNull) and (sacarPatente <> trim(cdsVehiculosContacto.FieldByName('Patente').Value)) then begin
                Cadena.Add(cdsVehiculosContacto.FieldByName('Patente').Value);
            end;
{INICIO: TASK_106_JMA_20170206
			if (not cdsVehiculosContacto.FieldByName('PatenteRVM').IsNull) and (sacarPatenteRVM <> trim(cdsVehiculosContacto.FieldByName('PatenteRVM').Value)) then begin
				Cadena.Add(cdsVehiculosContacto.FieldByName('PatenteRVM').Value);
            end;
TERMINO: TASK_106_JMA_20170206}
            cdsVehiculosContacto.Next;
        end;
	end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : ArmarListaTags
Author          :
Date Created    :
Description     : Ingresa lista de atributo N�mero Serial de Contrato de convenio a lista: cadena
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.ArmarListaTags(var Cadena: TStringList; sacarTag: AnsiString = '');
var
    Tag: string;
begin
    if not (cdsVehiculosContacto.IsEmpty) then begin
		cdsVehiculosContacto.First;
		while not cdsVehiculosContacto.Eof do
        begin
            if (cdsVehiculosContacto.fieldbyname('ContractSerialNumber').IsNull) then
				Tag := ''
            else
                Tag := cdsVehiculosContacto.fieldbyname('ContractSerialNumber').Value;

            if (trim(tag)<>'') and (sacarTag <> Tag) then begin
                Cadena.Add(cdsVehiculosContacto.FieldByName('ContractSerialNumber').Value);
			end;
            cdsVehiculosContacto.Next;
		end;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : ArmarListaVouchers
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.ArmarListaVouchers(var Lista: TStringList);
begin
    if not (cdsVehiculosContacto.IsEmpty) then begin
		cdsIndemnizaciones.First;
		while not cdsIndemnizaciones.Eof do begin
            if not (cdsIndemnizaciones.FieldByName('Observaciones').IsNull) then
                Lista.Add(cdsIndemnizaciones.FieldByName('Observaciones').AsString);
            cdsIndemnizaciones.Next;
		end;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : FreTelefonoSecundariotxtTelefonoExit
Author          :
Date Created    :
Description     : Valida que telefono secundario este bien ingresado.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.FreTelefonoSecundariotxtTelefonoExit(
  Sender: TObject);
begin
    if trim(FreTelefonoSecundario.txtTelefono.Text) <> '' then
		FreTelefonoSecundario.TelefonoValido;
end;

{-----------------------------------------------------------------------------
Procedure Name  : ExitCombos
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.ExitCombos(Sender: TObject);
begin
	TEdit(Sender).Text := PrimeraLetraMayuscula(TEdit(Sender).Text);
end;

{-----------------------------------------------------------------------------
Procedure Name  : RBDomicilioEntregaOtroClick
Author          :
Date Created    :
Description     : Permite habilitar bot�n Domicilio entrega.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.RBDomicilioEntregaOtroClick(
  Sender: TObject);
begin
    if (ActiveControl = Sender) then //INICIO: TASK_053_JMA_20160930
    begin
        BtnEditarDomicilioEntrega.Enabled := (RBDomicilioEntregaOtro.Checked) and (ModoPantalla <> scNormal);

        if RBDomicilioEntrega.Checked then begin
            TformEditarDomicilio.LimpiarRegistroDomicilio(DomicilioAlternativo);
            lbl_OtroDomicilio.Hint := '';
            lbl_OtroDomicilio.Caption:='';
        end else begin
            if (trim(DomicilioAlternativo.CodigoComuna) = '') then
                BtnEditarDomicilioEntrega.OnClick(sender);
        end;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : BtnEditarDomicilioEntregaClick
Author          :
Date Created    :
Description     : Permite editar atributos Domicilio.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.BtnEditarDomicilioEntregaClick(
  Sender: TObject);
var
	f: TFormEditarDomicilio;
begin
    Application.CreateForm(TFormEditarDomicilio, f);
    //DomicilioAlternativo
    if f.Inicializar(
        LimpiarCaracteresLabel(BtnEditarDomicilioEntrega.Caption), DomicilioAlternativo,1,false,False,ModoPantalla)
        and (f.ShowModal = mrOK) then begin
            DomicilioAlternativo:=f.RegistroDomicilio;
			DomicilioAlternativo.DomicilioEntrega:=True; // si cargo otro domicilio, este es el de entrega
            lbl_OtroDomicilio.Caption := f.DomicilioSimple;
            lbl_OtroDomicilio.Hint:=f.DomicilioSimple;
    end else begin
			if trim(DomicilioAlternativo.CodigoComuna)='' then begin // no hay ningun domicilio cargado
                RBDomicilioEntrega.Checked:=True;
                RBDomicilioEntrega.SetFocus;
                RBDomicilioEntrega.OnClick(RBDomicilioEntrega);
                lbl_OtroDomicilio.Caption := '';
                lbl_OtroDomicilio.Hint := '';
            end;
    end;
    f.Release;
end;

{-----------------------------------------------------------------------------
Procedure Name  : CambioPagoAutomatico
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.CambioPagoAutomatico(
  MostrarPantallita: boolean);
var
  f                         : TformDocPresentada;
  auxCant                   : Integer;
  ExcluirConvenioSinCuenta  : Integer;                                                                                                      //SS_628_ALA_20110720
begin

    lbl_DescripcionMedioPago.Caption:=SELECCIONAR;
    btn_EditarPagoAutomatico.Enabled := (cb_PagoAutomatico.Value<>NULL) and (cb_PagoAutomatico.Value > TPA_NINGUNO);
    if cb_PagoAutomatico.ItemIndex >= 1 then begin
        TformDocPresentada.BorrarRegistro(FDocumentacionPresentadaPorOS[0].Documentacion,CODIGO_DOC_MANDATO_PAC);
        TformDocPresentada.BorrarRegistro(FDocumentacionPresentadaPorOS[0].Documentacion,CODIGO_DOC_TARJETA_CREDITO);
        TformDocPresentada.BorrarRegistro(FDocumentacionPresentadaPorOS[0].Documentacion,CODIGO_DOC_MANDATO_PAT);
        case cb_PagoAutomatico.Value of
            TPA_NINGUNO: begin
                DatosMedioPago.TipoMedioPago:=NINGUNO;
				lbl_DescripcionMedioPago.Caption:='';

                if not(DocControlada) and DocControladaAnt then DocControlada:=True;
                if EsConvenioSinCuenta then ExcluirConvenioSinCuenta := 0 else ExcluirConvenioSinCuenta := 1;                               //SS_628_ALA_20110720

                //Controlo si ya tiene este medio de pago cargado
                //INICIO: TASK_022_ECA_20160601
               {* if ObtenerConvenioMedioPago(DMConnections.BaseCAC,
                                            FreDatoPersona.RegistrodeDatos.NumeroDocumento, -1,
                                            TPA_NINGUNO,
                                            //-1, '', '', -1, -1,                                                                            //SS_628_ALA_20110818
                                            ExcluirConvenioSinCuenta) > -1 then                                                              //SS_628_ALA_20110720
                        MsgBoxBalloon(MSG_ERROR_MEDIO_PAGO_DUPLICADO ,MSG_CAPTION_ERROR_MEDIO_PAGO, MB_ICONINFORMATION, cb_PagoAutomatico);*}
                //FIN: TASK_022_ECA_20160601        
                        end;
            TPA_PAT: begin
                DocControladaAnt:=DocControlada;
                if MostrarPantallita then btn_EditarPagoAutomatico.OnClick(self);
            end;
            TPA_PAC: begin

                Application.CreateForm(TformDocPresentada,f);
                f.Inicializar(LimpiarCaracteresLabel(btn_DocRequerido.Caption), TClientDataSet(dsVehiculosContacto.DataSet),
                        FDocumentacionPresentadaPorOS[0].Documentacion, FreDatoPersona.RegistrodeDatos.Personeria,PAC,
                        iif(trim(RepresentateLegal1.Nombre)='','', ArmarNombrePersona(RepresentateLegal1.Personeria,RepresentateLegal1.Nombre,RepresentateLegal1.Apellido,RepresentateLegal1.ApellidoMaterno)),
                        iif(trim(RepresentateLegal2.Nombre)='','', ArmarNombrePersona(RepresentateLegal2.Personeria,RepresentateLegal2.Nombre,RepresentateLegal2.Apellido,RepresentateLegal2.ApellidoMaterno)),
                        iif(trim(RepresentateLegal3.Nombre)='','', ArmarNombrePersona(RepresentateLegal3.Personeria,RepresentateLegal3.Nombre,RepresentateLegal3.Apellido,RepresentateLegal3.ApellidoMaterno)),
                        TformMedioEnvioDocumentosCobro.EnvioDocumentoCobroViaMail(RegMedioEnvioCorreo),auxCant, ModoPantalla, False);

                FDocumentacionPresentadaPorOS[0].Documentacion := f.DocumentacionPresentada;
                f.Release;

                if MostrarPantallita then btn_EditarPagoAutomatico.OnClick(self);
            end;
        end;
	end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_EditarPagoAutomaticoClick
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_EditarPagoAutomaticoClick(
  Sender: TObject);
begin
    if FTipoSolicitud = SOLICITUDCONTACTO then LlamarMedioPagoSolicitud
    else LlamarMedioPagoConvenio;
end;

{-----------------------------------------------------------------------------
Procedure Name  : cb_PagoAutomaticoChange
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.cb_PagoAutomaticoChange(Sender: TObject);
begin
    CambioPagoAutomatico(true);
    ObtenerGrupoFacturacion;                 // TASK_096_MGO_20161220
end;

{-----------------------------------------------------------------------------
Procedure Name  : FMDomicilioPrincipaltxt_numeroCalleExit
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.FMDomicilioPrincipaltxt_numeroCalleExit(
  Sender: TObject);
begin
  FMDomicilioPrincipal.txt_numeroCalleExit(Sender);
end;

{-----------------------------------------------------------------------------
Procedure Name  : IniciarCargaConRut
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.IniciarCargaConRut;
//INICIO: TASK_012_ECA_20160514
 function ObtenerCuentaComercialPredetrminada(NumeroDocumento: String): Integer;
    begin
        Result := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT isNull(dbo.ObtenerCtaComercialPredeterminada(''%s''),0)', [NumeroDocumento]));
    end;
 //FIN: TASK_012_ECA_20160514
Resourcestring
	MSG_DOCUMENTO_TITLE = 'Contacto Existente';
    MSG_DOCUMENTO_CAPTION = '%s existente verifique el nombre del interesado y/o n�mero de %s.';
    MSG_MODIFICAR_DATOS_CAPTION = '&Modificar';
    SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaPersonaEnListaAmarilla(dbo.ObtenerCodigoPersonaRUT(''%s''))';    				// SS_660_MCA_20140114
    SQL_ESTAPERSONAENPROCESOLISTAAMARILLA = 'SELECT dbo.EstaPersonaEnProcesoListaAmarilla(dbo.ObtenerCodigoPersonaRUT(''%s''))';   	// SS_660_MCA_20140114
    MSG_INHABILITADO_LISTA_AMARILLA = 'RUT inhabilitado por Lista Amarilla, No es posible ingresar un nuevo convenio.';   			//SS_660_MCA_20140114
    MSG_PROCESO_LISTA_AMARILLA = 'El RUT a lo menos tiene un convenio en proceso de Lista Amarilla';					  			//SS_660_MCA_20140114
Var
	EsListaAmarilla: Boolean;                                                   					//SS_660_MCA_20140114
    EsProcesoListaAmarilla: string;                                                                 //SS_660_MCA_20140114
    AuxRegDatosPersonas:TDatosPersonales;
    AuxMediosComunicacion:TMediosComunicacion;
    AuxDomicilios:TDomicilios;
	FConvenio: TformMultimpleMedioPago;
	fModif : TFormModificacionConvenio;
    PuedeEditarDomicilio: Boolean; i: integer;
    ConvenioBaja: Boolean;
begin
    InicializarValores;                           //TASK_012_ECA_20160514

	DatoPersona:=FreDatoPersona.RegistrodeDatos;

	if (length(Trim(DatoPersona.NumeroDocumento)) < 8)  or (length(Trim(DatoPersona.NumeroDocumento)) >9) then begin
		if FMDomicilioPrincipal.cb_BuscarCalle.Enabled then HabilitarControles(false);
		Exit;
	end;

	if (QueryGetValue(DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
		  StrLeft(DatoPersona.NumeroDocumento, (Length(DatoPersona.NumeroDocumento) -1)) +
		  ''',''' +
		  DatoPersona.NumeroDocumento[Length(DatoPersona.NumeroDocumento)] + '''') = '0') then
  	begin
		HabilitarControles(false);
		Exit;
	end;

	if (FTipoSolicitud = INTERMEDIO) then
  	begin
		AuxRegDatosPersonas.CodigoPersona := -1;
		FCheckListConvenio := TCheckListConvenio.Create(-1);
		FListObservacionConvenio := TListObservacionConvenio.Create(-1);
        //valido si el rut esta en lista amarilla
        EsListaAmarilla := StrToBool(QueryGetValue(DMConnections.BaseCAC,           	//SS_660_MCA_20140114
        								Format(SQL_ESTACONVENIOENLISTAAMARILLA,     	//SS_660_MCA_20140114
                                        [DatoPersona.NumeroDocumento])));           	//SS_660_MCA_20140114
    	EsProcesoListaAmarilla := QueryGetValue(DMConnections.BaseCAC,           		//SS_660_MCA_20140114
        								Format(SQL_ESTAPERSONAENPROCESOLISTAAMARILLA,   //SS_660_MCA_20140114
                                        [DatoPersona.NumeroDocumento]));           		//SS_660_MCA_20140114
    	if EsListaAmarilla then
        begin        	                                                            	//SS_660_MCA_20140114
        	MsgBox(MSG_INHABILITADO_LISTA_AMARILLA, self.Caption, MB_ICONWARNING);  	//SS_660_MCA_20140114
            HabilitarControles(False);                                                  //SS_660_MCA_20140114
            LimpiarCampos;                                                              //SS_660_MCA_20140114
            exit;                                                                   	//SS_660_MCA_20140114
        end;                                                                        	//SS_660_MCA_20140114
        if EsProcesoListaAmarilla <> EmptyStr then                                      //SS_660_MCA_20140114
    	begin                                                                           //SS_660_MCA_20140114
            MsgBox(MSG_PROCESO_LISTA_AMARILLA, self.Caption, MB_ICONWARNING);  			//SS_660_MCA_20140114
        end;                                                                            //SS_660_MCA_20140114


		// Si tiene convenio en CN, llamo al form de manejo de multiple convenio para que elija uno.
		if FPuedeModificar then
    	begin
			if TieneConvenio(DatoPersona.NumeroDocumento, DatoPersona.TipoDocumento, True, True) then
      		begin
				//FEsconvenioCN := True;										//SS_1147_MCA_20140408
                FEsConvenioNativo := True;										//SS_1147_MCA_20140408

				Application.CreateForm(TformMultimpleMedioPago, FConvenio);
				if not FConvenio.Inicializar(DatoPersona.TipoDocumento, DatoPersona.NumeroDocumento) then
        		begin
					FConvenio.Release;
					exit;
				end;

				if FConvenio.ShowModal = mrOK then
        		begin
					if FConvenio.Modificar then
          			begin
						FConvenio.Release;
						Self.Release;

						Application.CreateForm(TFormModificacionConvenio, fModif);
						if fModif.Inicializa(False,MSG_CAPTION_MODIFICAR_CONVENIO, FConvenio.NumeroConvenio, PuntoVenta, PuntoEntrega ,scModi, FFuente, False)  then fModif.ShowModal;
						FreeAndNil(fModif);

						exit;
					end;
					FConvenio.Release;
				end
        		else
        		begin
					FConvenio.Release;
					exit;
				end;

			end
			else
      		begin
				//FEsconvenioCN := False										//SS_1147_MCA_20140408
                FEsConvenioNativo := False;										//SS_1147_MCA_20140408
			end;
		end;

        // Si es un alta de convenio, se verifica si existen
        // observaciones a mostrar de las cuentas del RUT ingresado
        MostrarMensajeRUTConvenioCuentas(Trim(DatoPersona.NumeroDocumento));

        Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
        //INICIO: TASK_008_ECA_20160502
        //validamos si tiene un convenio en baja
        ConvenioBaja:=TieneConvenioEnBaja(DatoPersona.NumeroDocumento);
        //FIN: TASK_008_ECA_20160502

		if TieneConvenio(DatoPersona.NumeroDocumento,DatoPersona.TipoDocumento, False, False)
		  or
			((not TieneSolicitud(DatoPersona.NumeroDocumento, DatoPersona.TipoDocumento)) and
             ObtenerDatosPersonas(-1,AuxRegDatosPersonas,DatoPersona.TipoDocumento,DatoPersona.NumeroDocumento))
             or ConvenioBaja then
    	begin
			// Si tiene algun convenio lo cargo

        	if ObtenerDatosPersonas(-1,AuxRegDatosPersonas,DatoPersona.TipoDocumento,DatoPersona.NumeroDocumento) then
        	begin

				//cargo los medios de comunicacion
				//if ObtenerDomicilioPersonas(AuxRegDatosPersonas.CodigoPersona,AuxDomicilios) then		//TASK_076_MGO_20161003
                if ObtenerDomicilioPersonas(AuxRegDatosPersonas.CodigoPersona,AuxDomicilios,True) then	//TASK_076_MGO_20161003
                begin

					Cargando := True;
					FreDatoPersona.RegistrodeDatos := AuxRegDatosPersonas; // cargo la persona
					Cargando := False;

					if Length(AuxDomicilios) > 0 then FMDomicilioPrincipal.CargarDatosDomicilio(AuxDomicilios[0]);

					if ObtenerMedioComunicacion(AuxRegDatosPersonas.CodigoPersona,AuxMediosComunicacion) and
				  	(length(AuxMediosComunicacion)>0)  then
          			begin
						if AuxMediosComunicacion[0].CodigoTipoMedioContacto = TIPO_MEDIO_CONTACTO_E_MAIL then
            			begin
                			if FreDatoPersona.RegistrodeDatos.Personeria=PERSONERIA_JURIDICA then
                			begin
                                txtEMailContacto.Text:=AuxMediosComunicacion[0].Valor;
                                txtEMailContacto.OnChange(txtEMailContacto);
                                txtEMailContacto.Tag := AuxMediosComunicacion[0].Indice;
                            end
                            else
                            begin
                                txtEmailParticular.Text:=AuxMediosComunicacion[0].Valor;
                                txtEmailParticular.OnChange(txtEmailParticular);
                                txtEmailParticular.Tag := AuxMediosComunicacion[0].Indice;
                			end;
                        end
                        else
                        begin
                            //se estaba accediendo al elemento 0 como si obligatoriamente fuera un Telefono.
                            //se agreg� todo el bloque if superior.
                            FreTelefonoPrincipal.CargarTelefono(AuxMediosComunicacion[0].CodigoArea,AuxMediosComunicacion[0].Valor,AuxMediosComunicacion[0].CodigoTipoMedioContacto,AuxMediosComunicacion[0].HoraDesde, AuxMediosComunicacion[0].HoraHasta);
                            FreTelefonoPrincipal.CodigoMedioComunicacion := AuxMediosComunicacion[0].Indice;
						end;

						if Length(AuxMediosComunicacion)>1 then
            			begin
							if AuxMediosComunicacion[1].CodigoTipoMedioContacto = TIPO_MEDIO_CONTACTO_E_MAIL then begin
								if FreDatoPersona.RegistrodeDatos.Personeria=PERSONERIA_JURIDICA then begin
									txtEMailContacto.Text:=AuxMediosComunicacion[1].Valor;
									txtEMailContacto.OnChange(txtEMailContacto);
									txtEMailContacto.Tag := AuxMediosComunicacion[1].Indice;
								end else begin
									txtEmailParticular.Text:=AuxMediosComunicacion[1].Valor;
									txtEmailParticular.OnChange(txtEmailParticular);
									txtEmailParticular.Tag := AuxMediosComunicacion[1].Indice;
								end;
							end else begin
								FreTelefonoSecundario.CargarTelefono(AuxMediosComunicacion[1].CodigoArea,AuxMediosComunicacion[1].Valor,AuxMediosComunicacion[1].CodigoTipoMedioContacto);
								FreTelefonoSecundario.CodigoMedioComunicacion := AuxMediosComunicacion[1].Indice;
							end;
						end;

						if Length(AuxMediosComunicacion)>2 then begin
                            { INICIO : TASK_077_MGO_20161012
							FreTelefonoSecundario.CargarTelefono(AuxMediosComunicacion[1].CodigoArea,AuxMediosComunicacion[1].Valor,AuxMediosComunicacion[1].CodigoTipoMedioContacto);
							FreTelefonoSecundario.CodigoMedioComunicacion := AuxMediosComunicacion[1].Indice;
                            }
                            if AuxMediosComunicacion[2].CodigoTipoMedioContacto = TIPO_MEDIO_CONTACTO_E_MAIL then begin
                                if FreDatoPersona.RegistrodeDatos.Personeria=PERSONERIA_JURIDICA then begin
                                    txtEMailContacto.Text:=AuxMediosComunicacion[2].Valor;
                                    txtEMailContacto.Tag := AuxMediosComunicacion[2].Indice;
                                    txtEMailContacto.OnChange(txtEMailContacto);
                                end else begin
                                    txtEmailParticular.Text:=AuxMediosComunicacion[2].Valor;
                                    txtEmailParticular.Tag := AuxMediosComunicacion[2].Indice;
                                    txtEmailParticular.OnChange(txtEmailParticular);
                                end;
                            end else begin
                            	FreTelefonoSecundario.CargarTelefono(AuxMediosComunicacion[2].CodigoArea,AuxMediosComunicacion[2].Valor,AuxMediosComunicacion[2].CodigoTipoMedioContacto);
								FreTelefonoSecundario.CodigoMedioComunicacion := AuxMediosComunicacion[2].Indice;
                            end;
							// FIN : TASK_077_MGO_20161012
						end;
					end;
					//Habilito los controles
					HabilitarControles;
					EnableControlsInContainer(FreDatoPersona,true);
					FreDatoPersona.lbl_Personeria.Enabled := Trim(FreDatoPersona.RegistrodeDatos.NumeroDocumento) = '';
					FreDatoPersona.cbPersoneria.Enabled := FreDatoPersona.lbl_Personeria.Enabled;

                    if (AuxRegDatosPersonas.Personeria = PERSONERIA_JURIDICA) then begin
						ObtenerDatosRepresentantes (AuxRegDatosPersonas.CodigoPersona);
					end;

                    // El dato de contactos se puede modificar sin importar que convenios activos tenga el cliente
                    // pero si se modifica un contacto, esto se reflejara en todos los demas convenios del cliente
                    EnableControlsInContainer(FreTelefonoPrincipal, True);
                  	EnableControlsInContainer(FreTelefonoSecundario, True);
                  	EnableControlsInContainer(NBook_Email,true);


					// Si no tiene ningun convenio activo con CN, le dejo modificar cualquier dato
					if TieneConvenio(AuxRegDatosPersonas.NumeroDocumento, AuxRegDatosPersonas.TipoDocumento, True, True)
						//or EstaAsociadoaAlgunConvenioCN(AuxRegDatosPersonas.NumeroDocumento, AuxRegDatosPersonas.TipoDocumento) then			//SS_1147_MCA_20140408
                        or EstaAsociadoaAlgunConvenioNativo(AuxRegDatosPersonas.NumeroDocumento, AuxRegDatosPersonas.TipoDocumento) then        //SS_1147_MCA_20140408
          			begin

						FreDatoPersona.lblRutRun.Enabled := True;
						FreDatoPersona.txtDocumento.Enabled := True;
						FreDatoPersona.txtFechaNacimiento.Enabled := AuxRegDatosPersonas.FechaNacimiento = NullDate;
						FreDatoPersona.lbl_FechaNacimiento.Enabled := FreDatoPersona.txtFechaNacimiento.Enabled;
                        FreDatoPersona.txt_Nombre.Enabled := True; // (Trim(FreDatoPersona.txt_Nombre.Text) = '') or not FEsconvenioCN;                              //SS-628-NDR-20111122
                        FreDatoPersona.txt_Apellido.Enabled := True; //  (Trim(FreDatoPersona.txt_Apellido.Text) = '') or not FEsconvenioCN;                         //SS-628-NDR-20111122
                        FreDatoPersona.txt_ApellidoMaterno.Enabled := True; //  (Trim(FreDatoPersona.txt_ApellidoMaterno.Text) = '') or not FEsconvenioCN;           //SS-628-NDR-20111122
                        FreDatoPersona.txtFechaNacimiento.Enabled := True; //  (FreDatoPersona.txtFechaNacimiento.Date = NullDate) or not FEsconvenioCN;             //SS-628-NDR-20111122
						FreDatoPersona.cbSexo.Enabled := True; //  (FreDatoPersona.cbSexo.ItemIndex <= 0 ) or not FEsconvenioCN;                                     //SS-628-NDR-20111122
						FreDatoPersona.lbl_Sexo.Enabled := FreDatoPersona.cbSexo.Enabled;
						FreDatoPersona.cbSexo_Contacto.Enabled := True; //  (FreDatoPersona.cbSexo_Contacto.ItemIndex <= 0) or not FEsconvenioCN;                    //SS-628-NDR-20111122
						FreDatoPersona.lblSexo_Contacto.Enabled := FreDatoPersona.cbSexo_Contacto.Enabled;

            			if not (FreDatoPersona.cbSexo.Enabled or FreDatoPersona.txt_Nombre.Enabled or FreDatoPersona.txt_Apellido.Enabled) then
                            EnableControlsInContainer(FreDatoPersona, False);

                        EnableControlsInContainer(FMDomicilioPrincipal, FMDomicilioPrincipal.DescripcionCalle = '');
                        FMDomicilioPrincipal.Enabled := FMDomicilioPrincipal.Comuna = '';
                        FMDomicilioPrincipal.Pnl_Abajo.Enabled := FMDomicilioPrincipal.Comuna = '';
                        FMDomicilioPrincipal.cb_Comunas.Enabled := FMDomicilioPrincipal.Comuna = '';
                        FMDomicilioPrincipal.lbl_Comuna.Enabled := FMDomicilioPrincipal.Comuna = '';

                    end
                    else
                    begin
						EnableControlsInContainer(FMDomicilioPrincipal, True);

						FreDatoPersona.txt_ApellidoMaterno.Enabled := True;
						FreDatoPersona.txt_Nombre.Enabled := True;
						FreDatoPersona.txt_Apellido.Enabled := True;
					end;
            if ConvenioBaja then
            begin
                CargarDatosFacturacion(DatoPersona.NumeroDocumento)
            end;
            //INICIO: TASK_004_ECA_20160411
            cbbTipoConvenio.Enabled:=True;
            txt_RUTFacturacion.Text:=DatoPersona.NumeroDocumento;
            CodigoClienteFacturacion:= AuxRegDatosPersonas.CodigoPersona;
            lbl_NombreRutFacturacion.Caption:=BuscarNombreRUT(CodigoClienteFacturacion);
            btn_RUTFacturacion.Enabled:=True;
            btn_ConvenioFacturacion.Enabled:=True;
            CodigoConvenioFacturacion:=0;
            txt_NumeroConvenioFacturacion.Text:='';

            if (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) or (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_INFRACTOR)  then
            begin
                btn_ConvenioFacturacion.Enabled:=False;
                btn_AsignarTag.Enabled:=False;
                if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_INFRACTOR then
                    btn_RUTFacturacion.Enabled:=False;
            end;
            //FIN: TASK_004_ECA_20160411

            //INICIO:TASK_012_ECA_20160514
            chkCtaComercialPredeterminada.Visible:=False;
            chkCtaComercialPredeterminada.Checked:=False;
            CuentaComercialPredeterminada:=ObtenerCuentaComercialPredetrminada(DatoPersona.NumeroDocumento);
            //FIN: TASK_012_ECA_20160514

            ExcluirTipoConvenios(DatoPersona.NumeroDocumento); //TASK_008_ECA_20160502

            PuedeCrearCuentaComercial:=ValidarCuentaRNUTConCuentasActivas(DatoPersona.NumeroDocumento); //TASK_016_ECA_20160526
        end
        else
        begin
          MsgBox(MSG_ERROR_OBTENER_PERSONA,Caption,MB_ICONSTOP);
          Exit;
        end;
		end
      else
      begin
				//Habilito los controles
				HabilitarControles;
				EnableControlsInContainer(FreDatoPersona,true);
      end;
    end
    else
    begin
            Cargando := True;
            ManejarPersonaSolicitud(DatoPersona.TipoDocumento,DatoPersona.NumeroDocumento);

            Cargando := False;

            DatoPersona:=FreDatoPersona.RegistrodeDatos;

            //INICIO: TASK_004_ECA_20160411
            cbbTipoConvenio.Enabled:=True;
            txt_RUTFacturacion.Text:=DatoPersona.NumeroDocumento;
            CodigoClienteFacturacion:= iif(AuxRegDatosPersonas.CodigoPersona<0,0,AuxRegDatosPersonas.CodigoPersona);
            lbl_NombreRutFacturacion.Caption:=BuscarNombreRUT(CodigoClienteFacturacion);

            btn_RUTFacturacion.Enabled:=True;
            btn_ConvenioFacturacion.Enabled:=True;
            //FIN: TASK_004_ECA_20160411
    	end;
  	end;

  if (FTipoSolicitud = SOLICITUDCONTACTO) then
  begin
  end;

	FRepresentantesValidados := False;
  // habilita el combo de acuerdo a los permisos
  FreDatoPersona.cboTipoCliente.Enabled :=  ExisteAcceso('combo_CambiarTipoCliente');
  FreDatopersona.cboSemaforo1.Enabled := ExisteAcceso('combo_CambiarTipoCliente');
  FreDatopersona.cboSemaforo3.Enabled := ExisteAcceso('combo_CambiarTipoCliente');
  if FreDatoPersona.cboTipoCliente.Enabled then begin
      for I := 0 to FreDatoPersona.cboTipoCliente.ControlCount- 1 do
          FreDatoPersona.cboTipoCliente.Controls[i].Enabled := true;
  end;

  if FreDatoPersona.cboSemaforo1.Enabled  then begin
      FreDatoPersona.lblSemaforo1.Enabled := true;
      for i := 0 to FreDatoPersona.cboSemaforo1.ControlCount - 1 do
          FreDatoPersona.cboSemaforo1.Controls[i].Enabled := true;
  end;

  if FreDatoPersona.cboSemaforo3.Enabled  then begin
      FreDatoPersona.lblSemaforo3.Enabled := true;
      for i := 0 to FreDatoPersona.cboSemaforo3.ControlCount - 1 do
          FreDatoPersona.cboSemaforo3.Controls[i].Enabled := true;
  end;


  FreDatoPersona.lblTipo.Enabled := FreDatoPersona.cboTipoCliente.Enabled;
  FreDatoPersona.lblSemaforo1.Enabled := FreDatoPersona.cboSemaforo1.Enabled;
  FreDatoPersona.lblSemaforo3.Enabled := FreDatoPersona.cboSemaforo3.Enabled;

//  cb_AdheridoPA.Checked := FreDatoPersona.RegistrodeDatos.AdheridoPA;                            //SS-1006-NDR-20120715 //TASK_004_ECA_20160411

  //INICIO: TASK_038_ECA_20160625
  if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_RNUT then
  begin
     chkInactivaDomicilioRNUT.Visible:=True;
     chkInactivaMediosComunicacionRNUT.Visible:=True;
     chkInactivaDomicilioRNUT.Checked:=False;
     chkInactivaMediosComunicacionRNUT.Checked:=False;
  end;
  //FIN: TASK_038_ECA_20160625

  //INICIO: TASK_040_ECA_20160628
  if (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_RNUT) OR (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) then
  begin
    cbbTipoCliente.Visible:=True;
    lblTipoCliente.Visible:=True;
  end;
  //FIN: TASK_040_ECA_20160628


  Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;

{-----------------------------------------------------------------------------
Function Name   : txtDocumentoChange
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.txtDocumentoChange(Sender: TObject);
begin
    IniciarCargaConRut;
end;

{-----------------------------------------------------------------------------
Function Name   : MostrarMensajeRUTConvenioCuentas
Author          : nefernandez
Date Created    : 11/02/2008
Description     :  SS 631: Se muestra un aviso con las observaciones de las cuentas
                del Cliente (RUT) en cuesti�n.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.MostrarMensajeRUTConvenioCuentas(NumeroDocumento: String);
var
    Texto: AnsiString;
begin
    if ( NumeroDocumento <> '' ) then begin
        Texto := MensajeRUTConvenioCuentas(DMCOnnections.BaseCAC, NumeroDocumento);
        if Texto <> '' then
            MsgBox(Texto, Self.Caption, MB_ICONWARNING);
    end;
end;

{-------------------------------------------------------------------------------
Function Name   : FreDatoPresona1cbPersoneriaChange
Author          :
Date Created    :
Description     :
-------------------------------------------------------------------------------}
procedure TFormSolicitudContacto.FreDatoPresona1cbPersoneriaChange(
  Sender: TObject);
resourcestring
    CAPTION_DOMICILIO_PARTICULAR =  'Domicilio Particular';
    CAPTION_DOMICILIO_COMERCIAL = 'Domicilio Comercial';
begin
    FreDatoPersona.cbPersoneriaChange(Sender);

    txtEMailContacto.Clear;
    txtEmailParticular.clear;
    txtEmailParticular.OnChange(txtEmailParticular);
    txtEMailContacto.OnChange(txtEMailContacto);

	if FreDatoPersona.Personeria=PERSONERIA_JURIDICA then begin
        if (FTipoSolicitud = INTERMEDIO) then NBook_Email.PageIndex:=0
		else NBook_Email.PageIndex:=1;

		GBDomicilios.Caption:=CAPTION_DOMICILIO_COMERCIAL;
		pnl_RepLegal.Visible:=(FTipoSolicitud=INTERMEDIO); 
	end else begin
		NBook_Email.PageIndex:=1;
		GBDomicilios.Caption:=CAPTION_DOMICILIO_PARTICULAR;
		pnl_RepLegal.Visible:=False;
	end;

	//Al cambiar la personeria, cambia la documentacion requerida
    LimpiarDocumentacion;
	DocControlada := false;
end;

{-------------------------------------------------------------------------------
Function Name   : FreDatoPersonatxtDocumentoExit
Author          :
Date Created    :
Description     :
-------------------------------------------------------------------------------}
procedure TFormSolicitudContacto.FreDatoPersonatxtDocumentoChange(Sender: TObject);
    var
        NroRUT: string;
begin
    with  (Sender as TEdit) do begin
        if (Tag = 2) then try
            TPanelMensajesForm.MuestraMensaje('Inicializando Formulario ...', True);
            Tag := 1;
            NroRUT := Trim(Text);
            //LimpiarCampos;
            InicializarFormulario;
            NB_Botones.PageIndex := 2;
            Text := NroRUT;
            SelStart := Length(Text);
            Tag := 0;
        finally
            TPanelMensajesForm.OcultaPanel;
        end;
    end;
end;

procedure TFormSolicitudContacto.FreDatoPersonatxtDocumentoExit(
  Sender: TObject);
begin
    // Verifico si el contacto ya existe
    if btn_Cancelar.Focused or btnSalir.Focused then exit;
end;

{-------------------------------------------------------------------------------
Function Name   : FreDatoPersonatxtDocumentoKeyDown
Author          :
Date Created    :
Description     :
-------------------------------------------------------------------------------}
procedure TFormSolicitudContacto.FreDatoPersonatxtDocumentoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if (Shift = []) and (Key = VK_ESCAPE) then close;
end;

{-------------------------------------------------------------------------------
Function Name   : FreDatoPersonatxtDocumentoKeyPress
Author          :
Date Created    :
Description     :
-------------------------------------------------------------------------------}
procedure TFormSolicitudContacto.FreDatoPersonatxtDocumentoKeyPress(
  Sender: TObject; var Key: Char);
begin
  FreDatoPersona.txtDocumentoKeyPress(Sender, Key);
    if not (Key  in ['0'..'9','K','k', #8]) then
       Key := #0

end;

{-------------------------------------------------------------------------------
Function Name   : FreDatoPersonatxtDocumentoKeyUp
Author          : nefernandez
Date Created    : 15/11/2007
Description     : SS 632: Cuando se presiona la tecla Enter se inicia la Carga de datos del RUT indicado.
                Esto estaba previamente en el evento OnChange
-------------------------------------------------------------------------------}
procedure TFormSolicitudContacto.FreDatoPersonatxtDocumentoKeyUp(
  Sender: TObject; var Key: Word; Shift: TShiftState);
{INICIO: TASK_106_JMA_20170206
var                  //TASK_047_GLE_20170203
    rut : String;    //TASK_047_GLE_20170203
TERMINO: TASK_106_JMA_20170206}
begin
    if TForm(sender).name = 'TFormFacturacionInfracciones' then begin
      if Key = 13 then begin
          Key := 0;
        if (ModoPantalla <> scNormal) and (ModoPantalla <> scModi) and (not Cargando) then IniciarCargaConRut(true);
      end;
    end else begin
      if Key = 13 then begin
          Key := 0;
{INICIO: //TASK_106_JMA_20170206
          //INICIA: TASK_047_GLE_20170203
          //Agregar validaci�n RUT seg�n caso uso CU.COBO.CRM.201.
          //Si el formato del RUT no es v�lido el Sistema muestra un mensaje indicando
          //que el RUT ingresado no es v�lido
          rut := FreDatoPersona.txtDocumento.Text;

          if (QueryGetValueInt(DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
		    StrLeft(rut, (Length(rut) -1)) + ''',''' + rut[Length(rut)] + '''') = 0) then
          begin
            MsgBoxBalloon('Formato RUT No V�lido', 'Formato RUT', MB_ICONSTOP, FreDatoPersona.txtDocumento);
          end;
          //TERMINA: TASK_047_GLE_20170203
TERMINO: //TASK_106_JMA_20170206}
        if (ModoPantalla <> scNormal) and (ModoPantalla <> scModi) and (not Cargando) then IniciarCargaConRut;
      end;
    end;
end;

{-------------------------------------------------------------------------------
Function Name   : InicializarFormulario
Author          :
Date Created    :
Description     :
-------------------------------------------------------------------------------}
function TFormSolicitudContacto.InicializarFormulario: Boolean;
resourcestring
    STR_PRINC_TEL = 'Tel�fono principal';
    STR_ALT_TEL   = 'Tel�fono alternativo:';
var
    TituloMensaje, DescripcionMensajeFuente, Comentario: String;
begin
	try
        PuedeEditarSolicitud := True;
        TituloMensaje := '';
        DescripcionMensajeFuente := '';
        Comentario := '';
        lbl_OtroDomicilio.Caption := '';
        lbl_OtroDomicilio.Hint := '';
        FRUTCargado := False;
        cb_RecibeClavePorMail.checked := false;
        FreDatoPersona.LimpiarRegistro(DatoPersona);
		GestionarComponentesyHints(Self, DMConnections.BaseCAC, [lbl_PagoAutomatico, lbl_DescripcionMedioPago, lblPreguntaFuenteOrigenContacto, lbl_Pregunta, lbl_POS]);
        btn_EditarPagoAutomatico.Enabled := False;
        NB_Botones.PageIndex := 0; //nuevo
        NBook_Email.PageIndex := 0;
        HabilitarControles(False);
//        MEJORA PERFORMANCE
        LimpiarCampos;

        ObtenerParametroGeneral(DMConnections.BaseCAC, PARAM_EMAIL_ORIGEN_SOLICITUD, EmailOrigenSolicitud);
//       MEJORA PERFORMANCE
        CargarVehiculosContacto(-1);
        FechaEntrega := NullDate;

        FreTelefonoPrincipal.Inicializa(STR_PRINC_TEL, true);
        FreTelefonoSecundario.Inicializa(STR_ALT_TEL, false, false, true);
        FMDomicilioPrincipal.inicializa();

        ObtenerMensaje(DMConnections.BaseCAC, MENSAJE_SOLICITUD_FUENTE_ORIGEN_CONTACTO,
                    TituloMensaje, DescripcionMensajeFuente, Comentario);
        lblPreguntaFuenteOrigenContacto.Caption := DescripcionMensajeFuente;
        cbFuentesOrigenContacto.Left := lblPreguntaFuenteOrigenContacto.Left +
                                        lblPreguntaFuenteOrigenContacto.Width + 25;

        TFormMediosPagosConvenio.LimpiarMandante(FRegDatoMandante);


        //cb_HabilitadoAMB.Enabled := ExisteAcceso('BIT_Habilitado_Lista_Blanca_AMB') ;   //SS-1006-NDR-20111105
        //cb_HabilitadoPA.Enabled := ExisteAcceso('BIT_Habilitado_Lista_Acceso_PA') ;   //SS-1006-NDR-20111105		// SS_916_PDO_20120111
        //chkConvenioSinCuenta.Enabled := ExisteAcceso('BIT_Convenio_Sin_Cuenta');        //SS_628_ALA_20111202		// SS_916_PDO_20120111
        // ---------------------------------------------------------------------------------
         
        Result := True;
    except
        Result := False;
    end;
end;


{-------------------------------------------------------------------------------
Function Name   : IniciarCargaConRut
Author          :
Date Created    :
Description     :
-------------------------------------------------------------------------------}
procedure TFormSolicitudContacto.IniciarCargaConRut(Modosilencioso: boolean);
Resourcestring
	MSG_DOCUMENTO_TITLE = 'Contacto Existente';
    MSG_DOCUMENTO_CAPTION = '%s existente verifique el nombre del interesado y/o n�mero de %s.';
    MSG_MODIFICAR_DATOS_CAPTION = '&Modificar';
Var
    AuxRegDatosPersonas:TDatosPersonales;
    AuxMediosComunicacion:TMediosComunicacion;
    AuxDomicilios:TDomicilios;
	FConvenio: TformMultimpleMedioPago;
	fModif : TFormModificacionConvenio;
    PuedeEditarDomicilio: Boolean; i: integer;
begin
	DatoPersona:=FreDatoPersona.RegistrodeDatos;
	//valido estos datos antes, asi no llamo al SP
	if (length(Trim(DatoPersona.NumeroDocumento)) < 8)  or (length(Trim(DatoPersona.NumeroDocumento)) >9) then begin
		if FMDomicilioPrincipal.cb_BuscarCalle.Enabled then HabilitarControles(false);
		Exit;
	end;

	if (QueryGetValue(DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
		  StrLeft(DatoPersona.NumeroDocumento, (Length(DatoPersona.NumeroDocumento) -1)) +
		  ''',''' +
		  DatoPersona.NumeroDocumento[Length(DatoPersona.NumeroDocumento)] + '''') = '0') then begin
		HabilitarControles(false);
		Exit;
	end;

	if (FTipoSolicitud = INTERMEDIO) then begin
		AuxRegDatosPersonas.CodigoPersona := -1;
		FCheckListConvenio := TCheckListConvenio.Create(-1);
		FListObservacionConvenio := TListObservacionConvenio.Create(-1);

		// Si tiene convenio en CN, llamo al form de manejo de multiple convenio para que elija uno.
		if FPuedeModificar then begin
			if TieneConvenio(DatoPersona.NumeroDocumento, DatoPersona.TipoDocumento, True, True) then begin
				//FEsconvenioCN := True;										//SS_1147_MCA_20140408
                FEsConvenioNativo := True;										//SS_1147_MCA_20140408
				Application.CreateForm(TformMultimpleMedioPago, FConvenio);
				if not FConvenio.Inicializar(DatoPersona.TipoDocumento, DatoPersona.NumeroDocumento) then begin
					FConvenio.Release;
					exit;
				end;

				if FConvenio.ShowModal = mrOK then begin
					if FConvenio.Modificar then begin
						FConvenio.Release;
						Self.Release;

						Application.CreateForm(TFormModificacionConvenio, fModif);
						if fModif.Inicializa(False,MSG_CAPTION_MODIFICAR_CONVENIO, FConvenio.NumeroConvenio, PuntoVenta, PuntoEntrega ,scModi, FFuente, False)  then fModif.ShowModal;
						FreeAndNil(fModif);

						exit;
					end;
					FConvenio.Release;
				end else begin
					FConvenio.Release;
					exit;
				end;

			end
			else begin
				//FEsconvenioCN := False										//SS_1147_MCA_20140408
                FEsConvenioNativo := False;										//SS_1147_MCA_20140408
			end;
		end;

        MostrarMensajeRUTConvenioCuentas(Trim(DatoPersona.NumeroDocumento));


		if TieneConvenio(DatoPersona.NumeroDocumento,DatoPersona.TipoDocumento, False, False)
		  or
			((not TieneSolicitud(DatoPersona.NumeroDocumento, DatoPersona.TipoDocumento)) and
             ObtenerDatosPersonas(-1,AuxRegDatosPersonas,DatoPersona.TipoDocumento,DatoPersona.NumeroDocumento))

			 then begin
			// Si tiene algun convenio lo cargo

		   if ObtenerDatosPersonas(-1,AuxRegDatosPersonas,DatoPersona.TipoDocumento,DatoPersona.NumeroDocumento) then begin

				//cargo los medios de comunicacion
				if ObtenerDomicilioPersonas(AuxRegDatosPersonas.CodigoPersona,AuxDomicilios) then begin

					Cargando := True;
					FreDatoPersona.RegistrodeDatos := AuxRegDatosPersonas; // cargo la persona
					Cargando := False;

					if Length(AuxDomicilios) > 0 then FMDomicilioPrincipal.CargarDatosDomicilio(AuxDomicilios[0]);

					if ObtenerMedioComunicacion(AuxRegDatosPersonas.CodigoPersona,AuxMediosComunicacion) and
					(length(AuxMediosComunicacion)>0)  then begin
						if AuxMediosComunicacion[0].CodigoTipoMedioContacto = TIPO_MEDIO_CONTACTO_E_MAIL then begin

                            if FreDatoPersona.RegistrodeDatos.Personeria=PERSONERIA_JURIDICA then begin
                                txtEMailContacto.Text:=AuxMediosComunicacion[0].Valor;
                                txtEMailContacto.OnChange(txtEMailContacto);
                                txtEMailContacto.Tag := AuxMediosComunicacion[0].Indice;
                            end else begin
                                txtEmailParticular.Text:=AuxMediosComunicacion[0].Valor;
                                txtEmailParticular.OnChange(txtEmailParticular);
                                txtEmailParticular.Tag := AuxMediosComunicacion[0].Indice;
                            end;
                        end else begin
                            //se estaba accediendo al elemento 0 como si obligatoriamente fuera un Telefono.
                            //se agreg� todo el bloque if superior.
                            FreTelefonoPrincipal.CargarTelefono(AuxMediosComunicacion[0].CodigoArea,AuxMediosComunicacion[0].Valor,AuxMediosComunicacion[0].CodigoTipoMedioContacto,AuxMediosComunicacion[0].HoraDesde, AuxMediosComunicacion[0].HoraHasta);
                            FreTelefonoPrincipal.CodigoMedioComunicacion := AuxMediosComunicacion[0].Indice;

						end;

						if Length(AuxMediosComunicacion)>1 then  begin
							if AuxMediosComunicacion[1].CodigoTipoMedioContacto = TIPO_MEDIO_CONTACTO_E_MAIL then begin
								if FreDatoPersona.RegistrodeDatos.Personeria=PERSONERIA_JURIDICA then begin
									txtEMailContacto.Text:=AuxMediosComunicacion[1].Valor;
									txtEMailContacto.OnChange(txtEMailContacto);
									txtEMailContacto.Tag := AuxMediosComunicacion[1].Indice;
								end else begin
									txtEmailParticular.Text:=AuxMediosComunicacion[1].Valor;
									txtEmailParticular.OnChange(txtEmailParticular);
									txtEmailParticular.Tag := AuxMediosComunicacion[1].Indice;
								end;
							end else begin
								FreTelefonoSecundario.CargarTelefono(AuxMediosComunicacion[1].CodigoArea,AuxMediosComunicacion[1].Valor,AuxMediosComunicacion[1].CodigoTipoMedioContacto);
								FreTelefonoSecundario.CodigoMedioComunicacion := AuxMediosComunicacion[1].Indice;
							end;
						end;

						if Length(AuxMediosComunicacion)>2 then begin
							FreTelefonoSecundario.CargarTelefono(AuxMediosComunicacion[1].CodigoArea,AuxMediosComunicacion[1].Valor,AuxMediosComunicacion[1].CodigoTipoMedioContacto);
							FreTelefonoSecundario.CodigoMedioComunicacion := AuxMediosComunicacion[1].Indice;
							if FreDatoPersona.RegistrodeDatos.Personeria=PERSONERIA_JURIDICA then begin
								txtEMailContacto.Text:=AuxMediosComunicacion[2].Valor;
								txtEMailContacto.Tag := AuxMediosComunicacion[2].Indice;
								txtEMailContacto.OnChange(txtEMailContacto);
							end else begin
								txtEmailParticular.Text:=AuxMediosComunicacion[2].Valor;
								txtEmailParticular.Tag := AuxMediosComunicacion[2].Indice;
								txtEmailParticular.OnChange(txtEmailParticular);
							end;
						end;
					end;
					//Habilito los controles
					HabilitarControles;
					EnableControlsInContainer(FreDatoPersona,true);
					FreDatoPersona.lbl_Personeria.Enabled := Trim(FreDatoPersona.RegistrodeDatos.NumeroDocumento) = '';
					FreDatoPersona.cbPersoneria.Enabled := FreDatoPersona.lbl_Personeria.Enabled;

					if (AuxRegDatosPersonas.Personeria = PERSONERIA_JURIDICA) then begin
						ObtenerDatosRepresentantes (AuxRegDatosPersonas.CodigoPersona);
					end;
                    EnableControlsInContainer(FreTelefonoPrincipal, True);
                    EnableControlsInContainer(FreTelefonoSecundario, True);
                    EnableControlsInContainer(NBook_Email,true);

					// Si no tiene ningun convenio activo con CN, le dejo modificar cualquier dato
					if TieneConvenio(AuxRegDatosPersonas.NumeroDocumento, AuxRegDatosPersonas.TipoDocumento, True, True)
						//or EstaAsociadoaAlgunConvenioCN(AuxRegDatosPersonas.NumeroDocumento, AuxRegDatosPersonas.TipoDocumento) then begin			//SS_1147_MCA_20140408
                        or EstaAsociadoaAlgunConvenioNativo(AuxRegDatosPersonas.NumeroDocumento, AuxRegDatosPersonas.TipoDocumento) then begin          //SS_1147_MCA_20140408

						FreDatoPersona.lblRutRun.Enabled := True;
						FreDatoPersona.txtDocumento.Enabled := True;
						FreDatoPersona.txtFechaNacimiento.Enabled := AuxRegDatosPersonas.FechaNacimiento = NullDate;
						FreDatoPersona.lbl_FechaNacimiento.Enabled := FreDatoPersona.txtFechaNacimiento.Enabled;
                        //FreDatoPersona.txt_Nombre.Enabled := (Trim(FreDatoPersona.txt_Nombre.Text) = '') or not FEsconvenioCN;					    //SS_1147_MCA_20140408
                        FreDatoPersona.txt_Nombre.Enabled := (Trim(FreDatoPersona.txt_Nombre.Text) = '') or not FEsConvenioNativo;					    //SS_1147_MCA_20140408
                        //FreDatoPersona.txt_Apellido.Enabled := (Trim(FreDatoPersona.txt_Apellido.Text) = '') or not FEsconvenioCN;				    //SS_1147_MCA_20140408
                        FreDatoPersona.txt_Apellido.Enabled := (Trim(FreDatoPersona.txt_Apellido.Text) = '') or not FEsConvenioNativo;				    //SS_1147_MCA_20140408
                        //FreDatoPersona.txt_ApellidoMaterno.Enabled := (Trim(FreDatoPersona.txt_ApellidoMaterno.Text) = '') or not FEsconvenioCN;	    //SS_1147_MCA_20140408
                        FreDatoPersona.txt_ApellidoMaterno.Enabled := (Trim(FreDatoPersona.txt_ApellidoMaterno.Text) = '') or not FEsConvenioNativo;    //SS_1147_MCA_20140408
                        //FreDatoPersona.txtFechaNacimiento.Enabled := (FreDatoPersona.txtFechaNacimiento.Date = NullDate) or not FEsconvenioCN;        //SS_1147_MCA_20140408
                        FreDatoPersona.txtFechaNacimiento.Enabled := (FreDatoPersona.txtFechaNacimiento.Date = NullDate) or not FEsConvenioNativo;      //SS_1147_MCA_20140408
						//FreDatoPersona.cbSexo.Enabled := (FreDatoPersona.cbSexo.ItemIndex <= 0 ) or not FEsconvenioCN;                                //SS_1147_MCA_20140408
                        FreDatoPersona.cbSexo.Enabled := (FreDatoPersona.cbSexo.ItemIndex <= 0 ) or not FEsConvenioNativo;                              //SS_1147_MCA_20140408
						FreDatoPersona.lbl_Sexo.Enabled := FreDatoPersona.cbSexo.Enabled;
						//FreDatoPersona.cbSexo_Contacto.Enabled := (FreDatoPersona.cbSexo_Contacto.ItemIndex <= 0) or not FEsconvenioCN;				//SS_1147_MCA_20140408
                        FreDatoPersona.cbSexo_Contacto.Enabled := (FreDatoPersona.cbSexo_Contacto.ItemIndex <= 0) or not FEsConvenioNativo; 			//SS_1147_MCA_20140408
						FreDatoPersona.lblSexo_Contacto.Enabled := FreDatoPersona.cbSexo_Contacto.Enabled;

                        if not (FreDatoPersona.cbSexo.Enabled or FreDatoPersona.txt_Nombre.Enabled or FreDatoPersona.txt_Apellido.Enabled) then
                            EnableControlsInContainer(FreDatoPersona, False);

                        EnableControlsInContainer(FMDomicilioPrincipal, FMDomicilioPrincipal.DescripcionCalle = '');
                        FMDomicilioPrincipal.Enabled := FMDomicilioPrincipal.Comuna = '';
                        FMDomicilioPrincipal.Pnl_Abajo.Enabled := FMDomicilioPrincipal.Comuna = '';
                        FMDomicilioPrincipal.cb_Comunas.Enabled := FMDomicilioPrincipal.Comuna = '';
                        FMDomicilioPrincipal.lbl_Comuna.Enabled := FMDomicilioPrincipal.Comuna = '';

                    end else begin
						EnableControlsInContainer(FMDomicilioPrincipal, True);

						FreDatoPersona.txt_ApellidoMaterno.Enabled := True;
						FreDatoPersona.txt_Nombre.Enabled := True;
						FreDatoPersona.txt_Apellido.Enabled := True;
					end;
                end else begin
                    MsgBox(MSG_ERROR_OBTENER_PERSONA,Caption,MB_ICONSTOP);
                    Exit;
				end;

                // pongo el focus
        if not Modosilencioso then begin
          if (FreDatoPersona.Personeria = PERSONERIA_JURIDICA) and (FreDatoPersona.txtNombre_Contacto.Enabled)
          then begin
            FreDatoPersona.txtNombre_Contacto.SetFocus
          end
          else begin
            if (FreDatoPersona.Personeria = PERSONERIA_FISICA) and (FreDatoPersona.txt_Nombre.Enabled)
            then begin
              FreDatoPersona.txt_Nombre.SetFocus
            end
            else begin
              if FreDatoPersona.lbl_Sexo.Enabled then
                FreDatoPersona.cbSexo.SetFocus
              else
                cb_PagoAutomatico.SetFocus;
            end;
          end;
        end;

			end else begin
				//Habilito los controles
				HabilitarControles;
				EnableControlsInContainer(FreDatoPersona,true);

        if Modosilencioso then begin

        end else begin
            // pongo el focus
            if (FreDatoPersona.Personeria = PERSONERIA_JURIDICA) and (FreDatoPersona.txtNombre_Contacto.Enabled)
            then begin
              FreDatoPersona.txtNombre_Contacto.SetFocus
            end
            else begin
              if FreDatoPersona.txt_Nombre.Enabled  then
                FreDatoPersona.txt_Nombre.SetFocus
              else
                FreDatoPersona.txtDocumento.SetFocus
              end
            end;
        end;
            end else begin
                Cargando := True;
                if Modosilencioso then begin

                end else begin
                  ManejarPersonaSolicitud(DatoPersona.TipoDocumento,DatoPersona.NumeroDocumento);
                end;

                Cargando := False;

          DatoPersona:=FreDatoPersona.RegistrodeDatos;

            end;
  end;

    if (FTipoSolicitud = SOLICITUDCONTACTO) then begin

    end;

	FRepresentantesValidados := False;
    // habilita el combo de acuerdo a los permisos
    FreDatoPersona.cboTipoCliente.Enabled :=  ExisteAcceso('combo_CambiarTipoCliente');
    FreDatopersona.cboSemaforo1.Enabled := ExisteAcceso('combo_CambiarTipoCliente');
    FreDatopersona.cboSemaforo3.Enabled := ExisteAcceso('combo_CambiarTipoCliente');

    if FreDatoPersona.cboTipoCliente.Enabled then begin
        for I := 0 to FreDatoPersona.cboTipoCliente.ControlCount- 1 do
            FreDatoPersona.cboTipoCliente.Controls[i].Enabled := true;
    end;

    if FreDatoPersona.cboSemaforo1.Enabled  then begin
        FreDatoPersona.lblSemaforo1.Enabled := true;
        for i := 0 to FreDatoPersona.cboSemaforo1.ControlCount - 1 do
            FreDatoPersona.cboSemaforo1.Controls[i].Enabled := true;
    end;

    if FreDatoPersona.cboSemaforo3.Enabled  then begin
        FreDatoPersona.lblSemaforo3.Enabled := true;
        for i := 0 to FreDatoPersona.cboSemaforo3.ControlCount - 1 do
            FreDatoPersona.cboSemaforo3.Controls[i].Enabled := true;
    end;

    FreDatoPersona.lblTipo.Enabled := FreDatoPersona.cboTipoCliente.Enabled;     FreDatoPersona.lblSemaforo1.Enabled := FreDatoPersona.cboSemaforo1.Enabled;
    FreDatoPersona.lblSemaforo3.Enabled := FreDatoPersona.cboSemaforo3.Enabled;
    
    //cb_HabilitadoAMB.Enabled := ExisteAcceso('BIT_Habilitado_Lista_Blanca_AMB') ;   //SS-1006-NDR-20111105
    //cb_AdheridoPA.Enabled := ExisteAcceso('BIT_Adherido_Lista_Acceso_PA') ;           //SS-1006-NDR-20120614 //SS-1006-NDR-20111105 //TASK_004_ECA_20160411
    //chkConvenioSinCuenta.Enabled := ExisteAcceso('BIT_Convenio_Sin_Cuenta');          //SS_628_ALA_20111202 //TASK_004_ECA_20160411
    // ---------------------------------------------------------------------------------
    //cb_AdheridoPA.Checked:=DatoPersona.AdheridoPA;                                    //SS-1006-NDR-20120715 //TASK_004_ECA_20160411
end;

{-------------------------------------------------------------------------------
Function Name   : SetModoPantalla
Author          :
Date Created    :
Description     :
-------------------------------------------------------------------------------}
procedure TFormSolicitudContacto.SetModoPantalla(Modo: TStateConvenio;
  TipoSolicitud: TTipoSolicitud);
begin
    EnableControlsInContainer(NB_Botones, True);
    FTipoSolicitud := TipoSolicitud;
    ModoPantalla := Modo;
    case TipoSolicitud of
        SolicitudContacto: begin
            //cargo el combo de Punto de Entrega
            NB_Botones.PageIndex := 1;
            btn_MedioDoc.Visible := False;
            btn_DocRequerido.Visible := False;
            btn_AsignarTag.Visible := False;
            cbFuentesOrigenContacto.Visible := False; //true; //TASK_019_20160531
            lbl_Pregunta.Visible := true;
            cb_POS.Visible := true;
            lbl_POS.Visible := true;
            lbl_Ubicacion.Visible := False;
            txt_UbicacionFisicaCarpeta.Visible := False;
            cb_RecInfoMail.Visible := False;
            //cb_Observaciones.Visible := False;        //TASK_065_GLE_20170408
			//btn_VerObservaciones.Visible := False;    //TASK_065_GLE_20170408
        end;
        INTERMEDIO: begin
            //Set check de documentacion
            NB_Botones.PageIndex := 2;
            btn_MedioDoc.Visible := true;
            btn_DocRequerido.Visible := true;
            btn_AsignarTag.Visible := true;
            cbFuentesOrigenContacto.Visible := False; //true; //TASK_019_20160531
            lbl_Pregunta.Visible := true;
            cb_POS.Visible := true;
            lbl_POS.Visible := true;
            lbl_Ubicacion.Visible := True;
            txt_UbicacionFisicaCarpeta.Visible := True;
            //cb_Observaciones.Visible := True;             //TASK_065_GLE_20170408
            //btn_VerObservaciones.Visible := True;         //TASK_065_GLE_20170408
            cb_RecInfoMail.Visible := True;
        end;
        SinDefinir: NB_Botones.PageIndex := 0; // nueva
    end;

    case Modo of
        scNormal: begin //Esto es consulta
            HabilitarControles(False);

            btn_DocRequerido.Enabled := True;
            btn_MedioDoc.Enabled := True;

            BtnEditarDomicilioEntrega.Enabled := RBDomicilioEntregaOtro.Checked;
            BtnEditarDomicilioEntrega.caption := CAPTION_OTRO_DOMICILIO_VER;

			btn_EditarPagoAutomatico.Enabled := (DatosMedioPago.TipoMedioPago = PAT) or (DatosMedioPago.TipoMedioPago = PAC);
            btn_EditarPagoAutomatico.Caption := CAPTION_MEDIO_PAGO_VER;

            btn_RepresentanteLegal.Enabled := FreDatoPersona.RegistrodeDatos.Personeria = PERSONERIA_JURIDICA;

            FreDatoPersona.cbPersoneria.Enabled := False;
            FreDatoPersona.txtDocumento.ReadOnly := True;
            btnEditarVehiculo.Visible   := false;
            btnAgregarVehiculo.Visible  := false;
            btnEliminarVehiculo.Visible := false;
            btn_AsignarTag.Visible      := False;
            btn_EximirFacturacion.Visible       := False;
            btn_BonificarFacturacion.Visible    := False;

            pnlVehiculos.Enabled := True;
            dblVehiculos.Enabled := True;
            NB_Botones.PageIndex := 3; //pageSalir
        end;
        scModi: begin
            btn_EditarPagoAutomatico.Caption := CAPTION_MEDIO_PAGO;
            BtnEditarDomicilioEntrega.Caption := CAPTION_OTRO_DOMICILIO;

            btn_RepresentanteLegal.Enabled := FreDatoPersona.RegistrodeDatos.Personeria = PERSONERIA_JURIDICA;

            btnEditarVehiculo.Visible   := true;
            btnAgregarVehiculo.Visible  := true;
            btnEliminarVehiculo.Visible := true;
            btn_EximirFacturacion.Visible       := ExisteAcceso('boton_EximirFacturacion');
            btn_BonificarFacturacion.Visible    := ExisteAcceso('boton_BonificarFacturacion');
            HabilitarControles;
        end;
        else begin //Alta, Baja
            //if FreDatoPersona.txtDocumento.Enabled then FreDatoPersona.txtDocumento.SetFocus;

            btn_EditarPagoAutomatico.Caption := CAPTION_MEDIO_PAGO;
            BtnEditarDomicilioEntrega.Caption := CAPTION_OTRO_DOMICILIO;

            btnEditarVehiculo.Visible   := true;
            btnAgregarVehiculo.Visible  := true;
            btnEliminarVehiculo.Visible := true;
            btn_EximirFacturacion.Visible := ExisteAcceso('boton_EximirFacturacion');
            btn_BonificarFacturacion.Visible := ExisteAcceso('boton_BonificarFacturacion');
        end;
    end;

    AcomodarBotonesVehiculo;
end;

{-------------------------------------------------------------------------------
Function Name   : PuedeGuardarGenerico
Author          :
Date Created    :
Description     :
-------------------------------------------------------------------------------}
function TFormSolicitudContacto.PuedeGuardarGenerico: Boolean;
resourcestring
    MSG_COMO_SE_ENTERO = 'Responda a la pregunta: ';
    ERROR_DATOS_MEDIO_PAGO = 'Los Datos del Medio de Pago son incorrectos.';
    CAPTION_ERROR           = 'Error en tel�fonos del contacto.';
    MSG_ERROR_TELS_IGUALES  = 'Los tel�fonos principal y alternativo est�n duplicados.';

    MSG_FALTA_DOCUMENTO_E	= 'Debe seleccionar el documento electr�nico';

var
    F: TFormMsgNormalizar;
begin
    Result := False;
    DatoPersona:=FreDatoPersona.RegistrodeDatos;

    if not(FreDatoPersona.ValidarDatos) then begin
        Result:=False;
        exit;
    end;

    if not(FreTelefonoPrincipal.ValidarTelefono) then begin
        Result := False;
        Exit;
    end;

    if not(FreTelefonoSecundario.ValidarTelefono) then begin
        Result := False;
        Exit;
    end;

    //Validar el dominio y sint�xis del mail
	if NBook_Email.PageIndex=0 then begin
        if (EsComponenteObligatorio(txtEMailContacto) and (trim(txtEMailContacto.Text)='')) then begin
            MsgBoxBalloon(MSG_DEBE_DIRECCION_EMAIL,Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, txtEMailContacto);
            txtEMailContacto.SetFocus;
            result:=False;
            exit;
        end;

        if (EsComponenteObligatorio(txtEMailContacto) or (trim(txtEMailContacto.Text)<>'')) and (not(IsValidEMailCN(trim(txtEMailContacto.Text)))) then begin                    //SS-1092-MVI-20130627
    		MsgBoxBalloon(MSG_VALIDAR_DIRECCION_EMAIL, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, txtEMailContacto);                                              //SS-1092-MVI-20130627
            txtEMailContacto.SetFocus;
            Result := False;
	    	Exit;
        end;


		if (EsComponenteObligatorio(txtEMailContacto) or (trim(txtEMailContacto.Text)<>'')) and (not ValidarEmail(DMConnections.BaseCAC, trim(txtEMailContacto.Text))) then begin
    		MsgBoxBalloon(MSG_VALIDAR_DIRECCION_EMAIL, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, txtEMailContacto);
            txtEMailContacto.SetFocus;
            Result := False;
	    	Exit;
		end;
	end else begin
		if EsComponenteObligatorio(txtEmailParticular) and (trim(txtEmailParticular.Text)='') then begin
			MsgBoxBalloon(MSG_DEBE_DIRECCION_EMAIL,Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, txtEmailParticular);
			txtEmailParticular.SetFocus;
			result:=False;
			exit;
		end;

		if (EsComponenteObligatorio(txtEmailParticular) or (trim(txtEmailParticular.Text)<>'')) and (not(IsValidEMailCN(trim(txtEmailParticular.Text)))) then begin                  //SS-1092-MVI-20130627
			MsgBoxBalloon(MSG_VALIDAR_DIRECCION_EMAIL, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, txtEmailParticular);                                              //SS-1092-MVI-20130627
			txtEmailParticular.SetFocus;
			Result := False;
			Exit;
		end;

	end;

	if (FreDatoPersona.Personeria = PERSONERIA_JURIDICA) and (not FRepresentantesValidados) and (not ValidarDatosRepresentates) then begin
		MsgBoxBalloon( MSG_VALIDAR_REPRESENTANTES_LEGALES,
					Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, btn_RepresentanteLegal);
		Result := False;
		Exit;
	end;

	if not (FMDomicilioPrincipal.ValidarDomicilio) then begin
		result:=False;
		Exit;
	end;

	if (RBDomicilioEntrega.Checked = false) and (RBDomicilioEntregaOtro.Checked = false) then begin
		MsgBoxBalloon(MSG_VALIDAR_DOMICILIO_ENTREGA, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, RBDomicilioEntrega);
		RBDomicilioEntrega.SetFocus;
		Result := False;
		Exit;
	end;
	{Si eligi� Otro como domicilio de entrega debe estar ingresado
	Si no lo carg�, no tiene selec }
	if (RBDomicilioEntregaOtro.Checked) and (DomicilioAlternativo.CodigoComuna = '') then begin
		MsgBoxBalloon(MSG_VALIDAR_DOMICILIO_ENTREGA, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, BtnEditarDomicilioEntrega);
		BtnEditarDomicilioEntrega.SetFocus;
		Result := False;
		Exit;
	end;

	if cb_PagoAutomatico.ItemIndex<=0 then begin
		MsgBoxBalloon(format(MSG_VALIDAR_DEBE_EL,[STR_FORMA_PAGO]),Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),MB_ICONSTOP,cb_PagoAutomatico);
		cb_PagoAutomatico.SetFocus;
		result:=false;
		exit;
	end;

//	if (cbFuentesOrigenContacto.ItemIndex < 0) then begin
//		MsgBoxBalloon( MSG_COMO_SE_ENTERO + ' "'+lblPreguntaFuenteOrigenContacto.Caption + '"',
//					Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, cbFuentesOrigenContacto);
//		cbFuentesOrigenContacto.SetFocus;
//		Result := False;
//		Exit;
//	end;

    if FMDomicilioPrincipal.Enabled then begin
    	// Si est� habilitado el ingreso de domicilio principal verifica si est� normalizado
        if not(FMDomicilioPrincipal.EsCalleNormalizada) then begin
            Application.CreateForm(TFormMsgNormalizar, F);
            if f.Inicializa(Caption) and (f.ShowModal = mrCancel) then begin
                f.Release;
                FMDomicilioPrincipal.ForzarBusqueda;
                result := False;
                Exit;
            end;
            f.Release;
        end

        else begin
            if not(FMDomicilioPrincipal.EsNumeroNormalizada) then begin
				if MsgBox(format(MSG_VALIDAR_DOMICILIO_NRO_DESNORAMLIZADO,[STR_DOMICILIO_PARTICULAR]), self.caption, MB_YESNO or MB_ICONWARNING) = IDNO then begin
                    result:=False;
                    exit;
                end;
            end;
        end;
	end;

    if RBDomicilioEntregaOtro.Checked then begin
        if DomicilioAlternativo.CodigoCalle<=0 then begin
            if MsgBox(format(MSG_VALIDAR_DOMICILIO_DESNORAMLIZADO,[STR_DOMICILIO_ALTERNATIVO]), self.caption, MB_YESNO or MB_ICONWARNING) = IDNO then begin
                result:=False;
                exit;
            end;
        end
        else begin
            if DomicilioAlternativo.CodigoSegmento<=0 then begin
                if MsgBox(format(MSG_VALIDAR_DOMICILIO_NRO_DESNORAMLIZADO,[STR_DOMICILIO_ALTERNATIVO]), self.caption, MB_YESNO or MB_ICONWARNING) = IDNO then begin
                    result:=False;
                    exit;
                end;
            end;
        end;
    end;

    if not ValidateControls(	[vcbTipoDocumentoElectronico],
    							[vcbTipoDocumentoElectronico.ItemIndex >= 0],
                                Caption,
                                [MSG_FALTA_DOCUMENTO_E])  then begin
        Result := False;
        Exit;
    end;

    //Si se seleccion� boleta, y es una persona, entonces limpiar el Giro
    if	(FreDatoPersona.Personeria = PERSONERIA_FISICA) and (
    		(vcbTipoDocumentoElectronico.Value = TC_BOLETA) or
            (vcbTipoDocumentoElectronico.Value = TDCBX_AUTOMATICO_COD)
    	) then begin
        DatoPersona.Giro := '';
        FreDatoPersona.txtGiro.Text := '';
    end;

    result:=True;
end;

{-------------------------------------------------------------------------------
Procedure Name  : GuardarSolicitudcontacto
Author          :
Date Created    :
Description     :
-------------------------------------------------------------------------------}
function TFormSolicitudContacto.GuardarSolicitudcontacto: Boolean;
resourcestring
	// Mensajes de confirmaci�n
    MSG_DATOS_CONTACTO          = '�Est� seguro de que desea cambiar los datos del Contacto?';
    MSG_DOCUMENTO               = 'El n�mero de documento especificado es incorrecto.';
    CAPTION_ACTUALIZAR_CONTACTO = 'Actualizar Contacto';
    MSG_ACTUALIZAR_ERROR		= 'No se pudieron actualizar los datos del contacto.';
    MSG_ACTUALIZAR_DOMICILIO_ERROR = 'No se pudo actualizar el domicilio del contacto.';
    // Mensajes de resultado
	MSG_ACTUALIZACION           = 'Los datos se actualizaron correctamente.';
    MSG_SU_NRO_DE_PIN           = 'Su n�mero de PIN es: ';
    MSG_PATENTE_REPETIDA        = 'La patente %s est� repetida en esta solicitud.';
var
    CodigoMensajeFinal, IndicePersonaActual:integer;
    TipoFuente: AnsiString;
    TodoOk: boolean;
    FMens: TFormMensajes;
    textoMensaje: AnsiString;
	ContenidoEmail: AnsiString;
    CodigoEmail: Integer;
    ErrorEmail: AnsiString;
    CodigoSContactoActual:Integer;
    FEncuesta: TFormHacerEncuesta;
    SexoEmail: char;
    ApellidoEmail, ApellidoMaternoEmail, NombreEmail, PersonaEmail,
    PasswordEmail, DireccionEmail,
    MensajeEmail, AsuntoEmail,
    PlantillaEmail: AnsiString;
    Aux: String;
begin
	result := False;
    IndicePersonaActual   := FIndicePersona;
    CodigoSContactoActual := FCodigoSolicitud;
	DatoPersona := FreDatoPersona.RegistrodeDatos;
    ApellidoEmail := '';
    ApellidoMaternoEmail := '';
	NombreEmail := '';
    PersonaEmail := '';
    PasswordEmail := '';


        //****** INICIO GRABAR DATOS DE LA SOLICITUD ******//
        DMConnections.BaseCAC.BeginTrans;
        FFechaFin := now;
        TodoOk := false;
        try

            try
                // grabar solicitud contacto
                ActualizarSolicitudContacto.Close;

				with ActualizarSolicitudContacto.Parameters do begin
                    Refresh;

					if cb_POS.Items.Count > 1 then begin
                        if FechaEntrega <> NullDate then
                            ParamByName('@CodigoEstadoSolicitud').Value := ESTADO_SOLICITUD_AGENDADA
                        else
                            ParamByName('@CodigoEstadoSolicitud').Value := ESTADO_SOLICITUD_PENDIENTE;
                    end else
                        ParamByName('@CodigoEstadoSolicitud').Value := ESTADO_SOLICITUD_PENDIENTE;

                    ParamByName('@CodigoEstadoResultado').Value := NULL;
                    ParamByName('@Observaciones').Value := '';
					if cb_POS.Items.Count>1 then begin
                        ParamByName('@CodigoPuntoEntrega').Value := strtoint(trim(cb_POS.Items[cb_POS.ItemIndex].Value));
                        if FechaEntrega = NullDate then
                            ParamByName('@FechaCitaPuntoEntrega').Value := null
                        else
                            ParamByName('@FechaCitaPuntoEntrega').Value := FechaEntrega;

                    end else begin
						ParamByName('@CodigoPuntoEntrega').Value := null;
                        ParamByName('@FechaCitaPuntoEntrega').Value := null;
                    end;

                    if (CodigoSContactoActual = -1) then begin //Significa que es una Solicitud Nueva
                        ParamByName('@CodigoSolicitudContacto').Value := -1;
                        ParamByName('@CodigoFuenteSolicitud').Value := FFuente;
                        ParamByName('@CodigoFuenteOrigenContacto').Value := StrToInt(Trim(StrRight(cbFuentesOrigenContacto.Text, 10)));
                        ParamByName('@FechaHoraCreacion').Value := FFechaInicio;
                    end else begin
                        ParamByName('@CodigoSolicitudContacto').Value := CodigoSContactoActual;
                        ParamByName('@CodigoFuenteOrigenContacto').Value := StrToInt(Trim(StrRight(cbFuentesOrigenContacto.Text, 10)));
                        ParamByName('@CodigoFuenteSolicitud').Value := null;
                        ParamByName('@FechaHoraCreacion').Value := null;
                    end;
                end;
                if not(EjecutarSP(ActualizarSolicitudContacto, aux)) then
                    raise Exception.Create(Aux);

                CodigoSContactoActual := ActualizarSolicitudContacto.Parameters.ParamByName('@CodigoSolicitudContacto').Value;
                ActualizarSolicitudContacto.close;


				   // graba datos contacto
                ActualizarDatosContacto.Close;
                With ActualizarDatosContacto.Parameters do begin
                    Refresh;
                    DatoPersona:=FreDatoPersona.RegistrodeDatos;
                    ParamByName('@Personeria').Value        := DatoPersona.Personeria;

                    if DatoPersona.Personeria = PERSONERIA_FISICA then begin
                        ParamByName('@Nombre').Value			                := iif(DatoPersona.Nombre= '', null, DatoPersona.Nombre);
                        ParamByName('@Apellido').Value			                := iif(DatoPersona.Apellido= '', null, DatoPersona.Apellido);
                        ParamByName('@ApellidoMaterno').Value	                := iif(DatoPersona.ApellidoMaterno= '', null, DatoPersona.ApellidoMaterno);
                        ParamByName('@Sexo').Value 				                := iif(DatoPersona.Sexo <> null, DatoPersona.Sexo, null); //DatoPersona.Sexo;  //TASK_047_GLE_20170203
                        ParamByName('@ApellidoMaternoContactoComercial').Value	:= null;
						ParamByName('@NombreContactoComercial').Value	        := null;
                        ParamByName('@SexoContactoComercial').Value	            := null;
                        ParamByName('@EmailContactoComercial').Value	        := null;
                    end else if DatoPersona.Personeria = PERSONERIA_JURIDICA then begin
                        ParamByName('@ApellidoMaterno').Value	                := null;
                        ParamByName('@Apellido').Value	                        := iif(DatoPersona.RazonSocial= '', null, DatoPersona.RazonSocial);
						ParamByName('@Sexo').Value 				                := null;
                        ParamByName('@Nombre').Value			                := null;
                        ParamByName('@ApellidoContactoComercial').Value	        := iif(DatoPersona.Apellido= '', null, DatoPersona.Apellido);
                        ParamByName('@ApellidoMaternoContactoComercial').Value	:= iif(DatoPersona.ApellidoMaterno= '', null, DatoPersona.ApellidoMaterno);
                        ParamByName('@NombreContactoComercial').Value	        := iif(DatoPersona.Nombre='', null, DatoPersona.Nombre);
                        ParamByName('@SexoContactoComercial').Value	            := iif(DatoPersona.Sexo <> null, DatoPersona.Sexo, null); //DatoPersona.Sexo;  //TASK_047_GLE_20170203
                        ParamByName('@EmailContactoComercial').Value	        := null;
                    end;

                    ParamByName('@CodigoDocumento').Value	    := 'RUT';
                    ParamByName('@NumeroDocumento').Value	    := DatoPersona.NumeroDocumento;

					ParamByName('@LugarNacimiento').Value 	    := null;
                    ParamByName('@FechaNacimiento').Value 	    := iif( DatoPersona.FechaNacimiento < 0, null, DatoPersona.FechaNacimiento);
                    ParamByName('@CodigoSituacionIVA').Value    := null;
                    ParamByName('@CodigoActividad').Value 	    := null;

                    ParamByName('@CodigoSolicitudContacto').Value   := CodigoSContactoActual;
                    ParamByName('@IndicePersona').Value 	        := IndicePersonaActual;
                    ParamByName('@Password').Value                  := trim(FreTelefonoPrincipal.NumeroTelefono);
                end;
				if not(EjecutarSP(ActualizarDatosContacto, aux)) then
                    raise Exception.Create(Aux);

                IndicePersonaActual := ActualizarDatosContacto.Parameters.ParamByName('@IndicePersona').Value;
                CodigoSContactoActual := ActualizarDatosContacto.Parameters.ParamByName('@CodigoSolicitudContacto').Value;
                if (ActualizarDatosContacto.Parameters.ParamByName('@Password').Value = null) then
                    PasswordEmail := ''
                else
                    PasswordEmail := ActualizarDatosContacto.Parameters.ParamByName('@Password').Value;

                ActualizarDatosContacto.Close;

                // graba fuentesSC
                ActualizarOperacionesFuenteSC.Close;
                With ActualizarOperacionesFuenteSC.Parameters do begin
                    Refresh;
                    ParamByName('@CodigoSolicitudContacto').Value := CodigoSContactoActual;
                    ParamByName('@FechaHoraInicio').Value := FFechaInicio;
                    ParamByName('@Operador').Value   := UsuarioSistema;
                end;
                //ActualizarOperacionesFuenteSC.ExecProc;
				if not(EjecutarSP(ActualizarOperacionesFuenteSC, aux)) then
					raise Exception.Create(Aux);

                ActualizarOperacionesFuenteSC.Close;
            except
                On E: Exception do begin
					ActualizarDatosContacto.Close;
                    MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONSTOP);
                    Exit;
                end;
            end;

            ActualizarDomicilioContacto.Parameters.Refresh;
            //****** DOMICILIO PRINCIPAL ******//
            with ActualizarDomicilioContacto.Parameters do begin
                Refresh;
                ParamByName('@CodigoSolicitudContacto').Value   := CodigoSContactoActual;
                ParamByName('@IndicePersona').Value         := IndicePersonaActual;
                ParamByName('@TipoDomicilio').Value         := iif(DatoPersona.Personeria=PERSONERIA_JURIDICA, TIPO_DOMICILIO_COMERCIAL, TIPO_DOMICILIO_PARTICULAR);
                ParamByName('@CodigoCalle').Value           := iif(FMDomicilioPrincipal.CodigoCalle = -1, null, FMDomicilioPrincipal.CodigoCalle);
                ParamByName('@Numero').Value                := FMDomicilioPrincipal.Numero;
				ParamByName('@Detalle').Value               := iif(FMDomicilioPrincipal.Detalle = '', null, FMDomicilioPrincipal.Detalle);
                ParamByName('@CalleDesnormalizada').Value   := iif(FMDomicilioPrincipal.CodigoCalle>-1, null,  FMDomicilioPrincipal.DescripcionCalle);
                ParamByName('@CodigoPostal').Value          := iif(FMDomicilioPrincipal.CodigoPostal='', null,  FMDomicilioPrincipal.CodigoPostal);
				ParamByName('@CodigoPais').Value            := FMDomicilioPrincipal.Pais;
                ParamByName('@CodigoRegion').Value          := FMDomicilioPrincipal.Region;
                ParamByName('@CodigoComuna').Value          := iif(trim(FMDomicilioPrincipal.Comuna) = '', Null, FMDomicilioPrincipal.Comuna);
                ParamByName('@DescripcionCiudad').Value     := iif(FMDomicilioPrincipal.Ciudad = '', null,  FMDomicilioPrincipal.Ciudad);
                ParamByName('@CodigoSegmento').Value        := iif(FMDomicilioPrincipal.CodigoSegmento = -1, null, FMDomicilioPrincipal.CodigoSegmento);
                ParamByName('@IndiceDomicilio').Value       := iif(IndiceDomicilioPrincipalActual < 1, null, IndiceDomicilioPrincipalActual);
            end;
            if not(EjecutarSP(ActualizarDomicilioContacto, aux)) then
					raise Exception.Create(Aux);

            IndiceDomicilioPrincipalActual :=  ActualizarDomicilioContacto.Parameters.ParamByName('@IndiceDomicilio').Value;
            if RBDomicilioEntrega.Checked then
                FIndiceDomicilioEntrega := IndiceDomicilioPrincipalActual;

            if RBDomicilioEntregaOtro.Checked then begin
                //****** DOMICILIO ALTERNATIVO ******//
                with ActualizarDomicilioContacto.Parameters do begin
                    Refresh;
					ParamByName('@CodigoSolicitudContacto').Value := CodigoSContactoActual;
                    ParamByName('@IndicePersona').Value         := IndicePersonaActual;
                    ParamByName('@TipoDomicilio').Value  := DomicilioAlternativo.CodigoTipoDomicilio;
                    ParamByName('@CodigoCalle').Value := iif(DomicilioAlternativo.CodigoCalle = -1, null, DomicilioAlternativo.CodigoCalle);
                    ParamByName('@Numero').Value      := DomicilioAlternativo.NumeroCalleSTR;
                    ParamByName('@Detalle').Value      := iif(DomicilioAlternativo.Detalle = '', null, DomicilioAlternativo.Detalle);
                    ParamByName('@CalleDesnormalizada').Value   := iif(DomicilioAlternativo.CodigoCalle > -1, null,  DomicilioAlternativo.CalleDesnormalizada);
                    ParamByName('@CodigoPostal').Value          := iif(DomicilioAlternativo.CodigoPostal = '', null,  DomicilioAlternativo.CodigoPostal);
                    ParamByName('@CodigoPais').Value            := DomicilioAlternativo.CodigoPais;
                    ParamByName('@CodigoRegion').Value          := DomicilioAlternativo.CodigoRegion;
                    ParamByName('@CodigoComuna').Value          := iif(trim(DomicilioAlternativo.CodigoComuna)='',Null,DomicilioAlternativo.CodigoComuna);
                    ParamByName('@DescripcionCiudad').Value     := iif(DomicilioAlternativo.DescripcionCiudad = '', null,  DomicilioAlternativo.DescripcionCiudad);
                    ParamByName('@CodigoSegmento').Value        := iif(DomicilioAlternativo.CodigoSegmento = -1, null, DomicilioAlternativo.CodigoSegmento);
                    ParamByName('@IndiceDomicilio').Value       := iif(IndiceDomicilioAlternativoActual < 1, null, IndiceDomicilioAlternativoActual);
                end;
                if not(EjecutarSP(ActualizarDomicilioContacto, aux)) then
                    raise Exception.Create(Aux);

				IndiceDomicilioAlternativoActual :=  ActualizarDomicilioContacto.Parameters.ParamByName('@IndiceDomicilio').Value;
                if RBDomicilioEntregaOtro.Checked then
                    FIndiceDomicilioEntrega := IndiceDomicilioAlternativoActual;
			end;

            //Actualizar el Domicilio de entrega.
            with ActualizarDomicilioEntregaContacto do
            begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoSolicitudContacto').Value := CodigoSContactoActual;
                Parameters.ParamByName('@IndicePersona').Value := IndicePersonaActual;
                Parameters.ParamByName('@IndiceDomicilioEntrega').Value := FIndiceDomicilioEntrega;
            end;
            if not(EjecutarSP(ActualizarDomicilioEntregaContacto,aux)) then
                raise Exception.Create(Aux);


            //Ahora grabo los Medios de Contacto
			if not GuardarMediosComunicacionContacto(IndicePersonaActual, CodigoSContactoActual) then Exit;

            //****** Actualizar Medio de pago automatico ******//
            if cb_PagoAutomatico.Value > TPA_NINGUNO then begin
                if DatosMedioPago.TipoMedioPago = Peatypes.PAC then begin
                    with ActualizarMedioPagoAutomaticoPAC do
					begin
                        Parameters.Refresh;
                        Parameters.ParamByName('@CodigoSolicitudContacto').Value := CodigoSContactoActual;
                        Parameters.ParamByName('@CodigoTipoCuentaBancaria').Value := DatosMedioPago.PAC.TipoCuenta;
                        Parameters.ParamByName('@CodigoBanco').Value := DatosMedioPago.PAC.CodigoBanco;
                        Parameters.ParamByName('@NroCuentaBancaria').Value := DatosMedioPago.PAC.NroCuentaBancaria;
                        Parameters.ParamByName('@Sucursal').Value := DatosMedioPago.PAC.Sucursal;
                        Parameters.ParamByName('@NumeroDocumento').Value := DatosMedioPago.RUT;
                        Parameters.ParamByName('@Nombre').Value := DatosMedioPago.Nombre;
                        Parameters.ParamByName('@Telefono').Value := DatosMedioPago.Telefono;
						Parameters.ParamByName('@CodigoArea').Value := DatosMedioPago.CodigoArea;
                    end;
					if not(EjecutarSP(ActualizarMedioPagoAutomaticoPAC, aux)) then
                        raise Exception.Create(Aux);

                end
                else begin
                    if DatosMedioPago.TipoMedioPago = Peatypes.PAT then begin
                        with ActualizarMedioPagoAutomaticoPAT do
                        begin
                            Parameters.Refresh;
							Parameters.ParamByName('@CodigoSolicitudContacto').Value := CodigoSContactoActual;
                            Parameters.ParamByName('@CodigoTipoTarjetaCredito').value := DatosMedioPago.PAT.TipoTarjeta;
                            Parameters.ParamByName('@NumeroTarjetaCredito').value := DatosMedioPago.PAT.NroTarjeta;
                            Parameters.ParamByName('@FechaVencimiento').value := DatosMedioPago.PAT.FechaVencimiento;
                            Parameters.ParamByName('@CodigoEmisorTarjetaCredito').value := DatosMedioPago.PAT.EmisorTarjetaCredito;
                            stringreplace(DatosMedioPago.PAT.FechaVencimiento, dateseparator , '/',  [rfReplaceAll]);
                            Parameters.ParamByName('@NumeroDocumento').Value := DatosMedioPago.RUT;
                            Parameters.ParamByName('@Nombre').Value := DatosMedioPago.Nombre;
                            Parameters.ParamByName('@Telefono').Value := DatosMedioPago.Telefono;
							Parameters.ParamByName('@CodigoArea').Value := DatosMedioPago.CodigoArea;
                        end;
                        if not(EjecutarSP(ActualizarMedioPagoAutomaticoPAT, aux)) then
                            raise Exception.Create(Aux);

                    end;
                end;
            end else begin
                // Delete from ambas tablas de tipo de pago y actualizo la solicitud
                QueryExecute( DMConnections.BaseCAC,
                    'DELETE FROM MedioPagoAutomaticoPAC WHERE CodigoSolicitudContacto = ' + IntToStr(CodigoSContactoActual));
				QueryExecute( DMConnections.BaseCAC,
					'DELETE FROM MedioPagoAutomaticoPAT WHERE CodigoSolicitudContacto = ' + IntToStr(CodigoSContactoActual));
                QueryExecute( DMConnections.BaseCAC,
                    'UPDATE SolicitudContacto SET TipoMedioPagoAutomatico = 0 WHERE CodigoSolicitudContacto = ' + IntToStr(CodigoSContactoActual));
            end;
             //Ahora se Guardan los Vehiculos, primero los elimino y se vuelven a cargar
            QueryExecute( DMConnections.BaseCAC,
				'DELETE FROM SolicitudContactoVehiculos WHERE CodigoSolicitudContacto = ' + IntToStr(CodigoSContactoActual));

            ActualizarVehiculosContacto.Close;
            if not (cdsVehiculosContacto.IsEmpty) then begin
                cdsVehiculosContacto.First;
                While not cdsVehiculosContacto.Eof do begin
                    try


                        with ActualizarVehiculosContacto do begin
                            Parameters.Refresh;
							Parameters.ParamByName('@CodigoSolicitudContacto').Value := CodigoSContactoActual;
                            Parameters.ParamByName('@IndiceVehiculo').Value := null; //ver si esta bien poner esto ac�..
                            Parameters.ParamByName('@TipoPatente').Value  := cdsVehiculosContacto.FieldByName('TipoPatente').Value;
                            Parameters.ParamByName('@Patente').Value := cdsVehiculosContacto.FieldByName('Patente').Value;
                            Parameters.ParamByName('@CodigoMarca').Value := cdsVehiculosContacto.FieldByName('CodigoMarca').Value;
                            Parameters.ParamByName('@Modelo').Value := cdsVehiculosContacto.FieldByName('Modelo').Value;
                            Parameters.ParamByName('@AnioVehiculo').Value := cdsVehiculosContacto.FieldByName('AnioVehiculo').Value;
                            Parameters.ParamByName('@CodigoColor').Value := cdsVehiculosContacto.FieldByName('CodigoColor').Value;
                            //Parameters.ParamByName('@CodigoTipoVehiculo').Value := cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').Value;          //TASK_106_JMA_20170206
                            Parameters.ParamByName('@Observaciones').Value := cdsVehiculosContacto.FieldByName('Observaciones').Value;
                        end;
                        if not(EjecutarSP(ActualizarVehiculosContacto, aux)) then
							raise Exception.Create(Aux);
                        ActualizarVehiculosContacto.Close;

                    except
                        on E: exception do begin
                            MsgBoxErr( Format(MSG_ERROR_ACTUALIZAR,[FLD_VEHICULOS]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[FLD_VEHICULOS]), MB_ICONSTOP);
                            ActualizarVehiculosContacto.Close;
                            Exit;
                        end;
                    end;

                    //INICIO	: 20160921 CFU TASK_039_CFU_20160921-CRM_Actualiza_Stock_TAGs
                    try
                        with spRegistrarMovimientoStock, Parameters do begin
                            Close;
                            Parameters.Refresh;
                            ParamByName('@CodigoOperacion').Value 			:= CONST_ALMACEN_ENTREGADO_EN_CUOTAS;
                            ParamByName('@TipoMovimiento').Value 			:= CONST_MOVIMIENTO_NOMINADO;
                            ParamByName('@CodigoEmbalaje').Value 			:= NULL;
                            ParamByName('@NumeroInicialTAG').Value 			:= PadL(trim(cdsVehiculosContacto.FieldByName('ContractSerialNumber').AsString), 11, '0');
                            ParamByName('@NumeroFinalTAG').Value 			:= PadL(trim(cdsVehiculosContacto.FieldByName('ContractSerialNumber').AsString), 11, '0');
                            ParamByName('@CategoriaTAG').Value 				:= (QueryGetValue(DMConnections.BaseCAC, 'select CodigoCategoria from MaestroTags with (nolock) where ContractSerialNumber = ' + cdsVehiculosContacto.FieldByName('ContractSerialNumber').AsString));
                            ParamByName('@Cantidad').Value 					:= 1;
                            ParamByName('@EstadoControl').Value 			:= Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_CONTROL_CALIDAD_ACEPTADO()'));
                            ParamByName('@EstadoSituacion').Value 			:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_ESTADO_SITUACION_DISPONIBLE()');
                            ParamByName('@CodigoAlmacenOrigen').Value 		:= PuntoEntrega;

                            if cdsVehiculosContacto.FieldByName('Almacen').Value = CONST_STR_ALMACEN_COMODATO then 
                            	ParamByName('@CodigoAlmacenDestino').Value := CONST_ALMACEN_EN_COMODATO;
                            if cdsVehiculosContacto.FieldByName('Almacen').Value = CONST_STR_ALMACEN_ARRIENDO then 
                            	ParamByName('@CodigoAlmacenDestino').Value := CONST_ALMACEN_ARRIENDO;
                            if cdsVehiculosContacto.FieldByName('Almacen').Value = CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS then 
                            	ParamByName('@CodigoAlmacenDestino').Value := CONST_ALMACEN_ENTREGADO_EN_CUOTAS;

                            ParamByName('@GuiaDespacho').Value 				:= Null;
                            ParamByName('@Usuario').Value 					:= UsuarioSistema;
                            ParamByName('@FechaHoraEvento').Value 			:= NowBase(DMConnections.BaseCAC);
                            ParamByName('@MotivoEntrega').Value 			:= null;
                            ParamByName('@ConstanciaCarabinero').Value 		:= null;
                            ParamByName('@FechaHoraConstancia').Value 		:= null;
                            ParamByName('@DescripcionMotivoRegistro').Value := null;
                            ParamByName('@CodigoComunicacion').Value 		:= null;
                            ExecProc
                        end;
                    except
                        on E: exception do begin
                            MsgBoxErr( Format(MSG_ERROR_ACTUALIZAR,[FLD_TAG]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[FLD_TAG]), MB_ICONSTOP);
                            spRegistrarMovimientoStock.Close;
                            Exit;
                        end;
            		end;
                    //TERMINO	: 20160921 CFU
                        cdsVehiculosContacto.Next;
                end;
            end;
            ActualizarVehiculosContacto.Close;
            FFechaInicio := nulldate;
            if (cb_POS.Enabled) and (cb_POS.Value <> POS_Anterior) then QueryExecute(DMConnections.BaseCAC, format('EXEC ActualizarProximoDiaPuntoEntrega %d', [integer(cb_POS.Value)]));

            TodoOK := True;
			result := True;
		finally
            if TodoOK then DMConnections.BaseCAC.CommitTrans
			else DMConnections.BaseCAC.RollbackTrans;
            Screen.Cursor := crDefault;
        end;

        //****** FIN GRABAR DATOS DE LA SOLICITUD ******/

        if EsNueva then begin
            //******** ENCUESTA **********//
            Application.CreateForm(TFormHacerEncuesta, FEncuesta);
            if (FEncuesta.inicializar(FFuente, CodigoSContactoActual)) then FEncuesta.ShowModal;
            FEncuesta.Release;

            //******** ENVIAR EMAIL  *********//

           if ((((DatoPersona.Personeria = PERSONERIA_FISICA)) and (Trim(txtEmailParticular.Text)<> ''))  or
				(((DatoPersona.Personeria = PERSONERIA_JURIDICA)) and (Trim(txtEMailContacto.Text) <> '')) ) then begin
                SexoEmail := chr(164); //para inicializarla en algun valor
                if DatoPersona.Personeria = PERSONERIA_FISICA then begin
                    ApellidoEmail := iif(DatoPersona.Apellido = '', '', DatoPersona.Apellido);
                    ApellidoMaternoEmail := iif(DatoPersona.ApellidoMaterno = '', '', DatoPersona.ApellidoMaterno);
                    NombreEmail := iif(DatoPersona.Nombre = '', '', DatoPersona.Nombre);
					SexoEmail := DatoPersona.sexo;
                    DireccionEmail := iif(Trim(txtEmailParticular.Text)= '', '', Trim(txtEmailParticular.Text));
                    PersonaEmail := trim(QueryGetValue(DMConnections.BaseCAC, Format(
                      'SELECT dbo.ArmarNombrePersona(''%s'', ''%s'', ''%s'',''%s'')',
                      [PERSONERIA_FISICA, ApellidoEmail, ApellidoMaternoEmail, NombreEmail])));
                end else if DatoPersona.Personeria = PERSONERIA_JURIDICA then begin
                    ApellidoEmail := iif(DatoPersona.Apellido = '', '', DatoPersona.Apellido);
                    ApellidoMaternoEmail := iif(DatoPersona.ApellidoMaterno = '', '', DatoPersona.ApellidoMaterno);
					NombreEmail := iif(DatoPersona.Nombre= '', '', DatoPersona.Nombre);
                    SexoEmail := DatoPersona.Sexo;
					DireccionEmail := iif(Trim(txtEMailContacto.Text) = '', '', Trim(txtEMailContacto.Text));
                    PersonaEmail := trim(QueryGetValue(DMConnections.BaseCAC, Format(
					  'SELECT dbo.ArmarRazonSocialPersona(''%s'', ''%s'', ''%s'',''%s'')',
                      [DatoPersona.RazonSocial, ApellidoEmail, ApellidoMaternoEmail, NombreEmail])));
                end;
                ObtenerParametroGeneral(DMConnections.BaseCAC, PARAM_PLANTILLA_EMAIL_ALTA_SOLICITUD, PlantillaEmail);
                ObtenerContenidoMail(DMConnections.BaseCAC, CONTENIDO_EMAIL_ALTA_SOLICITUD, MensajeEmail, AsuntoEmail);
                if ArmarMailSolicitud(PlantillaEmail, ApellidoEmail, ApellidoMaternoEmail, NombreEmail, PasswordEmail,
                                    MensajeEmail, SexoEmail, ContenidoEmail, ErrorEmail) then begin
                    if InsertarEMailSalida(DMConnections.BaseCAC, AsuntoEmail,
                      DireccionEmail, PersonaEmail, ContenidoEmail, CodigoEmail) then begin
						//Actualizar el Domicilio de entrega.
                        with ActualizarEmailContacto do
                        begin
                            Parameters.Refresh;
                            Parameters.ParamByName('@CodigoSolicitudContacto').Value := CodigoSContactoActual;
                            Parameters.ParamByName('@CodigoEmailAlta').Value := CodigoEmail;
                        end;
						if not(EjecutarSP(ActualizarEmailContacto, aux)) then
                            raise Exception.Create(Aux);

                    end;
                end;
            end;

            //******** MENSAJE FINAL ********//
            CodigoMensajeFinal := 0;
            //tomar primero el codigo de tipo de fuente de con Trim(StrRight(cbFuente.Text, 10))
            TipoFuente := QueryGetValue(DMConnections.BaseCAC,
			    'Select TipoFuente from FuentesSolicitud WITH (NOLOCK) where codigoFuenteSolicitud = ''' + inttostr(FFuente)+ '''');
            if TipoFuente = TF_CALL_CENTER then
                CodigoMensajeFinal := MENSAJE_SOLICITUD_FINAL_CALL_CENTER
			else begin
                if TipoFuente = TF_WEB then
                    CodigoMensajeFinal := MENSAJE_SOLICITUD_FINAL_WEB
                else
					if TipoFuente = TF_CUPON then
                        CodigoMensajeFinal := MENSAJE_SOLICITUD_FINAL_CUPON;
			end;
            textoMensaje := '';
            if (CodigoMensajeFinal <> 0) then begin
                Application.CreateForm(TFormMensajes, FMens);
                if (FMens.inicializa(DMConnections.BaseCAC, CodigoMensajeFinal)) then FMens.ShowModal;
                FMens.Release;
            end else
                MsgBox( MSG_ACTUALIZACION, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONINFORMATION);
    end;

end;

{-------------------------------------------------------------------------------
Procedure Name  : btn_SalirSoloClick
Author          :
Date Created    :
Description     :
-------------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_SalirSoloClick(Sender: TObject);
begin
	btnSalir.Click;
end;

{-------------------------------------------------------------------------------
Procedure Name  : btn_ImprimirClick
Author          :
Date Created    :
Description     :
-------------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_ImprimirClick(Sender: TObject);

    function ReservarNumeroConvenio(TipoDocumento, NumeroDocumento: String): String;
    begin
		//Result := Trim(QueryGetValue(DMConnections.BaseCAC, Format('EXEC ReservarNumeroConvenio ''%s'', ''%s''', [TipoDocumento, NumeroDocumento]))); TASK_008_ECA_20160502
        Result := Trim(QueryGetValue(DMConnections.BaseCAC, Format('EXEC ReservarNumeroConvenio ''%s'', ''%s'''+ ',' + string(cbbTipoConvenio.Value), [TipoDocumento, NumeroDocumento])));
    end;

    function ObtenerNumeroConvenioGenerado(CodigoConvenio: Integer): string;
    begin
        Result := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerNumeroConvenio(%d)', [CodigoConvenio]));
    end;

resourcestring
    STR_NUMERO = ' - Numero: ';
    MSG_CUENTA_COMERCIAL = 'Debe tener un Convenio RNUT con cuentas activas para poder crear una Cuenta Comercial';  //TASK_016_ECA_20160526
    MSG_CUENTA_COMERCIAL_SIN_CTA = 'Debe agregar un Veh�culo!';  // TASK_037_ECA_20160622
var
	f: TformFirmarConvenio;
    fFacturacionManual: TFacturacionManualForm;     // SS_964_PDO_20110531
    
	RespuestaForm, i: Integer;
	NumeroConvenio: String;
    NumeroConvenioVersion: String;
	TipoDocumento, NumeroDocumento: String;
	AuxMotivoCanelacion: TMotivoCancelacion;
	AuxGuardoOk: Boolean;
    Cuentas: array of TCuentaVehiculo;
    form : TFormImprimirConvenio;
    DatosConvenio: TDatosConvenio;
    ImpCaratula, ImpAnexo1, ImpMandato, ExisteVehiculos: Boolean;
begin
	if not ((PuedeGuardarGenerico) AND (PuedeGuardarContrato)) then Exit;

    if not ValidarTagsEnArriendo() then Exit;

	AuxGuardoOk := False;

    NumeroConvenioVersion:= ReservarNumeroConvenio(DatoPersona.TipoDocumento, DatoPersona.NumeroDocumento);
	NumeroConvenio :=  copy(NumeroConvenioVersion,1,length(NumeroConvenioVersion)-3);

	TipoDocumento := trim(DatoPersona.TipoDocumento);
	NumeroDocumento := trim(DatoPersona.NumeroDocumento);
	//Creamos el form de opciones (Confirmar, Reimprimir y Cancelar)
	Application.CreateForm(TformFirmarConvenio, f);
	f.Inicializar(Self.Caption + STR_NUMERO + FormatearNumeroConvenio(NumeroConvenio));
	try
    	Respuestaform := f.ShowModal;
	    if Respuestaform <> mrCancel then begin

        	// Se guarda los datos del convenio
            // Auditoria
            if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, 0, '', 0, '', FPuntoEntrega, UsuarioSistema, 'Guardar')  then
    	        raise Exception.create('Error registrando auditoria');

            AuxGuardoOK := GuardarContrato(NumeroConvenioVersion);

            if not AuxGuardoOK then begin
            	MsgBox(MSG_ERROR_SOLICITUD_CONVENIO, MSG_ERROR_SOLICITUD_CONVENIO, MB_ICONERROR);
                // Si no se pudo grabar, se sale
            	Exit;
            end;

            NumeroConvenio:=ObtenerNumeroConvenioGenerado(FCodigoConvenio);

            //Se muestra el mensaje indicando que el Convenio se ingres� correctamente
            MsgBox(MSG_CAPTION_CONVENIO_OK, Self.Caption + ': ' + FormatearNumeroConvenio(NumeroConvenio), MB_ICONINFORMATION);



        	//Se imprime directamente los documentos del convenio
{----------------------------------------------------IMPRIMIR DOCUMENTO----------------------------------------------------------------}
            ImpCaratula:= false;
            ImpAnexo1 := false;
            ImpMandato := False;
            ExisteVehiculos:= False;   //TASK_037_ECA_20160622

            //INICIO: TASK_037_ECA_20160622
            DatosConvenio := TDatosConvenio.Create(NumeroConvenio,PuntoVenta,PuntoEntrega);
            if (DatosConvenio.CodigoTipoConvenio = CODIGO_TIPO_CONVENIO_RNUT) or (DatosConvenio.CodigoTipoConvenio = CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) then
            begin
                if DatosConvenio.CodigoTipoConvenio = CODIGO_TIPO_CONVENIO_RNUT then
                begin
                    SetLength(Cuentas, DatosConvenio.Cuentas.CantidadVehiculos);
                    for i := 0 to DatosConvenio.Cuentas.CantidadVehiculos - 1 do
                    begin
                         ImpAnexo1 := True;
                         Cuentas[i] := DatosConvenio.Cuentas.Vehiculos[i].Cuenta;
                    end;
                end;

                if DatosConvenio.CodigoTipoConvenio = CODIGO_TIPO_CONVENIO_CTA_COMERCIAL then
                   if cdsVehiculosContacto.RecordCount>0 then
                          ExisteVehiculos := True;

                  If (DatosConvenio.MedioPago.MedioPago.TipoMedioPago <> Ninguno)
                            and ( ImpAnexo1 or  ExisteVehiculos  ) then
                            ImpMandato := True;

                Application.CreateForm(TFormImprimirConvenio,form);
                if not form.ImprimirAltaConvenio(ImpCaratula, ImpAnexo1, ImpMandato, Cuentas, DatosConvenio) then
                begin
                    if AuxMotivoCanelacion= ERROR then begin
                        MsgBox(MSG_ERROR_IMPRIMIR_CONVENIO, MSG_CAPTION_IMPRESION_CONVENIOS, MB_ICONERROR);
                    end;
                    FreeAndNil(form);
                    Exit;
                end;
            end;
{-----------------------------------------------FIN IMPRIMIR DOCUMENTO-----------------------------------------------}
        end;

    finally
        if Assigned(f) then FreeAndNil(f);

        if AuxGuardoOK then try                                                                                                                          
            with spExistenMovimientosFacturacionInmediata do begin
                if Active then Close;

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConvenio').Value   := FCodigoConvenio;
                Parameters.ParamByName('@TieneMovimientos').Value := NULL;
                ExecProc;

                if Parameters.ParamByName('@TieneMovimientos').Value = True then begin
                    if FindFormOrCreate(TFacturacionManualForm, fFacturacionManual) then begin
                        fFacturacionManual.Release
                    end
                    else FindFormOrCreate(TFacturacionManualForm, fFacturacionManual);

                    if FFacturacionManual.Inicializar(TC_FACTURACION_INMEDIATA, False ) then begin
                        FFacturacionManual.peRUTCliente.Text := DatoPersona.NumeroDocumento;
                        FFacturacionManual.peRUTCliente.OnChange(nil);
                        FFacturacionManual.btnBuscar.Click;
                        FFacturacionManual.cbConveniosCliente.ItemIndex := FFacturacionManual.cbConveniosCliente.Items.IndexOfValue(FCodigoConvenio);
                        FFacturacionManual.cbConveniosCliente.OnChange(nil);
                        FFacturacionManual.Show;
                    end;
                end;
            end;

            ModalResult := mrOk;
            Close;
        except                                                                                                                                           
            on e:Exception do begin                                                                                                                      
                ShowMsgBoxCN(e, Self);                                                                                                                   
                ModalResult := mrCancel;                                                                                                                 
                Close;                                                                                                                                   
            end;                                                                                                                                         
        end;
    end;
end;

{-------------------------------------------------------------------------------
Function Name   : PuedeGuardarContrato
Author          : dcepeda       ()
Date Created    : 10-Mayo-2010  ()
Description     : Funcionalidad que verifica si las condiciones para poder guardar
                un Convenio son correctas.
-------------------------------------------------------------------------------}
function TFormSolicitudContacto.PuedeGuardarContrato: Boolean;
resourcestring
    STR_FALTA_INGRESAR                                          = 'Falta ingresar los %s';
    MSG_MEDIO_ENVIO_DETALLE_FACTURACION_SUPERA_LIMITE_VEHICULOS = 'Tiene especificado un Medio de Env�o que no es compatible con la cantidad de veh�culos del convenio.';
    MSG_CAMBIO_MEDIO_ENVIO_DETALLE_FACTURACION                  = 'La cantidad de veh�culos del convenio a superado la cantidad m�xima de veh�culos'
        + CRLF + 'para recibir el Detalle de Tr�nsitos impreso junto a la Nota de Cobro.'
        + CRLF + 'A partir de este momento lo podr� obtener desde un archivo disponible desde la Web.';
    GUARDAR_CONVENIO_SIN_CUENTA                                 = 'Va a guardar un convenio sin cuenta. �Est� seguro que desea continuar con la operaci�n?';
    MSG_ERROR_PERMISOS_CONVENIO_SIN_CUENTAS                     = 'No posee permisos para la creaci�n de un convenio sin cuentas, por favor comuniquese con un Administrador del sistema';
    MSG_ERROR_PERMISOS_CONVENIO_CON_CUENTAS                     = 'No posee permisos para la creaci�n de un convenio, por favor comuniquese con un Administrador del sistema';
    MSG_ERROR_EXISTEN_CONVENIOS_ACTIVOS                         = 'El Cliente seleccionado posee convenios activos';
    MSG_EROR_EXISTE_CONVENIO_SIN_CUENTA                         = 'El Cliente ya cuenta con un Convenio Sin Cuenta. Por lo tanto debe ingresar Cuentas a este Convenio.';                           //SS_628_ALA_20110720
    MSG_CONFIRMACION_CONVENIO_SIN_CUENTA                        = 'Usted se encuentra grabando un convenio que no posee Cuentas asociadas, pero no ha seleccionado el checkbox Sin Cuenta. ' + CRLF +      //SS_628_ALA_20111216
                                                                  '�Desea activar el Convenio Sin Cuenta?';                                                                                                //SS_628_ALA_20111216
var
    Book                    : TBookmark;
    error                   : String;
    auxDatosTurno           : TDatosTurno;
    f                       : TformMedioEnvioDocumentosCobro;
    CantidadVehiculos       : Integer;
    i                       : Integer;
    TieneEmail              : Boolean;
    CantidadMaximaVehiculos : Integer;
    CodigoCliente           : Integer;
    ConvenioSinCuenta       : Boolean;
begin
    Result := False;

    if not VerTurnoAbierto(DMConnections.BaseCAC, PuntoEntrega, PuntoVenta ,UsuarioSistema, auxDatosTurno) then begin
        MsgBox(MSG_ERROR_TURNO_ABRIR, Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]), MB_ICONSTOP);
        result := False;
        Exit;
    end;

	if not(RepLegal) and (FreDatoPersona.RegistrodeDatos.Personeria=PERSONERIA_JURIDICA)then begin
		MsgBoxBalloon(format(STR_FALTA_INGRESAR,[STR_REPRESENTANTE]),Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),MB_ICONSTOP,btn_RepresentanteLegal);
		btn_RepresentanteLegal.SetFocus;
		result:=False;
		exit;
    end;

    if (cb_PagoAutomatico.ItemIndex < 0) then begin
        MsgBoxBalloon(format(MSG_VALIDAR_DEBE_EL,[STR_FORMA_PAGO]),Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),MB_ICONSTOP,cb_PagoAutomatico);
        cb_PagoAutomatico.SetFocus;
        result:=False;
        exit;
    end;

    if (cb_PagoAutomatico.ItemIndex > 1) and
            not (TFormMediosPagosConvenio.ValidarMandante(CONST_ORIGEN_MEDIO_PAGO_COSTANERA,
                FRegDatoMandante, DatoPersona.NumeroDocumento, error)) then begin
        MsgBoxBalloon(MSG_ERROR_MEDIO_PAGO,Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),MB_ICONSTOP,cb_PagoAutomatico);
        cb_PagoAutomatico.SetFocus;
        result:=False;
        exit;
	end;

    if (cb_PagoAutomatico.ItemIndex > 1) and // es el item Ninguno
		(not (TFormMediosPagosConvenio.ValidarMedioPago(DatosMedioPago,
            CONST_ORIGEN_MEDIO_PAGO_COSTANERA,
            FreDatoPersona.RegistrodeDatos.NumeroDocumento, error))) then begin
        MsgBoxBalloon(MSG_ERROR_MEDIO_PAGO,Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),MB_ICONSTOP,cb_PagoAutomatico);
		cb_PagoAutomatico.SetFocus;
        Result := False;
        Exit;
    end;

    ConvenioSinCuenta := EsConvenioSinCuenta();                                                                                                 
    //Si el actual convenio es sin cuenta verificamos que no exista alguno ya creado.                                                           
    if ConvenioSinCuenta then begin                                                                                                             
        if VerificarExisteConvenioSinCuenta(DMConnections.BaseCAC, DatoPersona.NumeroDocumento, 0) then begin                                   
            MsgBoxBalloon(MSG_EROR_EXISTE_CONVENIO_SIN_CUENTA, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, dblVehiculos);     
            dblVehiculos.SetFocus;                                                                                                              
            Result := False;                                                                                                                    
            Exit;                                                                                                                               
        end;                                                                                                                                    
    end;

    //controlar medio doc
    (* Si no entr� nunca al form de Medios de Env�o y NO Tiene e-mail, obtener los m�nimos datos. *)
    if (((FreDatoPersona.Personeria=PERSONERIA_JURIDICA) and  (trim(txtEMailContacto.Text) = EmptyStr)) or
       ((FreDatoPersona.Personeria=PERSONERIA_FISICA) and  (trim(txtEmailParticular.Text) = EmptyStr))) and
        not(MedioDocControlada) and not(TformMedioEnvioDocumentosCobro.EnvioDocumentoCobroViaMail(RegMedioEnvioCorreo)) then begin
        	MedioDocControlada := True;
            Application.CreateForm(TformMedioEnvioDocumentosCobro, f);
            RegMedioEnvioCorreo := f.RegistroMedioEvioDoc;
			f.Destroy;
    end;

    (* Si no entr� nunca al form de Medios de Env�o y TIENE e-mail, mostrar mensaje para que ingrese datos. *)
    if not(MedioDocControlada) then begin
			MsgBoxBalloon(MSG_ERROR_MEDIO_ENVIO_DOCUMENTACION, Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]),
                MB_ICONSTOP, btn_MedioDoc);
            btn_MedioDoc.SetFocus;
            result := False;
            exit;
    end;

    // Medio de envio de Doc
    if FreDatoPersona.Personeria = PERSONERIA_JURIDICA then TieneEmail := (Trim(txtEMailContacto.Text) <> EmptyStr)
    else TieneEmail := (Trim(txtEmailParticular.Text) <> EmptyStr);

    for i := 0 to (Length(RegMedioEnvioCorreo) - 1) do begin
        if (RegMedioEnvioCorreo[i].CodigoTipoMedioEnvio = CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO)
                and (not TieneEmail) then begin
            if FreDatoPersona.Personeria = PERSONERIA_JURIDICA then begin
            MsgBoxBalloon(MSG_ERROR_MEDIO_ENVIO_DOC, Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]),
                    MB_ICONSTOP, txtEMailContacto);
                txtEMailContacto.SetFocus;
            end
            else begin
            	MsgBoxBalloon(MSG_ERROR_MEDIO_ENVIO_DOC, Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]),
                	MB_ICONSTOP, txtEmailParticular);
                txtEmailParticular.SetFocus;
            end;
            Result := False;
            Exit;
        end;

        (* Si existe al Medio de Env�o Detalle de Facturaci�n Impreso junto
        con Nota de Cobro, controlar que la cantidad de veh�culos del
        convenio NO supere el M�ximo para el env�o de Faturaci�n Detallada
        Gratis.
        Si supera la cantidad m�xima y NO tiene E-mail, mostrar un mensaje
        de error.
        Si supera la cantidad m�xima y TIENE E-mail, mostrar un mensaje
        indicando que a partir de ahora se le enviar� por E-mail. *)
        if (RegMedioEnvioCorreo[i].CodigoTipoMedioEnvio = CONST_TIPO_MEDIO_ENVIO_CORREO_POSTAL)
                and (RegMedioEnvioCorreo[i].CodigoMedioEnvioDocumento = CONST_MEDIO_ENVIO_DETALLE_FACTURACION) then begin

            ObtenerParametroGeneral(DMConnections.BaseCAC, 'TOPE_VEHICULOS_FACT_DETALLADA', CantidadMaximaVehiculos);
            if (cdsVehiculosContacto.RecordCount > CantidadMaximaVehiculos) then begin
                if (not TieneEmail) then begin
                    MsgBoxBalloon(MSG_MEDIO_ENVIO_DETALLE_FACTURACION_SUPERA_LIMITE_VEHICULOS,
                        Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]),
                        MB_ICONSTOP, btn_MedioDoc);
                    btn_MedioDoc.SetFocus;
                    Result := False;
                    Exit;
                end else begin
                    MsgBox(MSG_CAMBIO_MEDIO_ENVIO_DETALLE_FACTURACION,
                        Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]),
                        MB_ICONINFORMATION);
                end;
            end;

        end;
    end; // for

    if not ConvenioSinCuenta then begin
        CantidadVehiculos := 0;
        with cdsVehiculosContacto do begin
            Book:=GetBookmark;
            first;
            while not(eof) do begin

                Inc(CantidadVehiculos);

                if not FieldByName('Foraneo').AsBoolean then begin
                    if ObtenerConvenioVehiculo(DMConnections.BaseCAC,fieldbyname('TipoPatente').AsString,fieldbyname('Patente').AsString)<>'' then begin
                        MsgBoxBalloon(format(MSG_ERROR_DUPLICADO,[STR_PATENTE]),Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, dblVehiculos);
                        dblVehiculos.SetFocus;
                        result:=False;
                        exit;
                    end;
                end;

                if (trim(fieldbyname('ContractSerialNumber').AsString)='') or (trim(fieldbyname('ContractSerialNumber').AsString)='0') then begin
                    MsgBoxBalloon(format(MSG_VALIDAR_DEBE_EL,[FLD_TAG]),Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, dblVehiculos);
                    dblVehiculos.SetFocus;
                    result:=False;
                    exit;
                end;
                
                next;
            end;
            GotoBookmark(Book);
            FreeBookmark(Book);
        end;
    end;
    if (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_RNUT) AND (cdsVehiculosContacto.RecordCount = 0)  then
    begin
        MsgBox('Debe Agregar un Veh�culo!', Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]), MB_ICONSTOP); 
        dblVehiculos.SetFocus;
        result:=False;
        exit;
    end;

    // INICIO : TASK_096_MGO_20161220
    if CodigoGrupoFacturacion = 0 then begin
        MsgBoxBalloon('Debe seleccionar un Grupo de Facturaci�n', 'Grupo de Facturaci�n', MB_ICONSTOP, btnGrupoFacturacion);
        Result := False;
        Exit;
    end;
    // FIN : TASK_096_MGO_20161220

    Result := True;
end;

{-------------------------------------------------------------------------------
Function Name   : DarCaptionMensaje
Author          :
Date Created    :
Description     :
--------------------------------------------------------------------------------}
function TFormSolicitudContacto.DarCaptionMensaje: String;
begin
	if fTipoSolicitud=SolicitudContacto then result:=STR_SOLICITUD
	else result:=STR_CONTRATO;
end;

{-------------------------------------------------------------------------------
Function Name   : GuardarContrato
Author          : dcepeda       ()
Date Created    : 20-05-2010    ()
Description     : Funcionalidad que permite guardar un Contrato eventualmente con
                cuenta(s) asociada(s) en la base de datos.
--------------------------------------------------------------------------------}
function TFormSolicitudContacto.GuardarContrato(NumeroContrato: String): Boolean;
resourcestring
    MSG_ERROR_SAVE_PERSON   = 'Error al grabar Persona';
    MSG_ERROR_DOM           = 'Error al obtener domicilio de Persona';
    MSG_ERROR_SAVE_DOM      = 'Error al grabar Domicilio Principal de Persona';
    MSG_ERROR_SAVE_TEL      = 'Error al grabar medio de Comunicaci�n - Telefono';
    MSG_ERROR_SAVE_TEL_SEC  = 'Error al grabar medio de Comunicaci�n - Telefono Secundario';
    MSG_ERROR_SAVE_MAIL     = 'Error al grabar medio de Comunicaci�n - Mail';
    MSG_ERROR_SAVE_DOM_SEC  = 'Error al grabar Domicilio Secundario de Persona';
    MSG_ERROR_SAVE_MAND     = 'Error al grabar Mandante';
    MSG_ERROR_SAVE_TEL_MAND = 'Error al grabar medio de Comunicaci�n - Telefono del Mandante';
    MSG_ERROR_SAVE_REPLEG1  = 'Error al grabar Representante legal1';
    MSG_ERROR_SAVE_REPLEG2  = 'Error al grabar Representante legal2';
    MSG_ERROR_SAVE_REPLEG3  = 'Error al grabar Representante Legal3';
	MSG_CONFIRMATION        = 'Confirmar Cambios Convenio';
    MSG_XMLFILE_ERROR       = 'No se gener� archivo de novedades XML';

var
    TodoOk: Boolean;
    sp: TADOStoredProc;
    CodigoDomicilioTitularCuenta, CodigoDomicilioSecundario, CodigoPersonaTitularContrato: Integer;
    CodigoConvenio, CodigoPersonaMandante: Integer;
    CodigoPersonaRepresentateLega1, CodigoPersonaRepresentateLega2, CodigoPersonaRepresentateLega3,
    CodigoVehiculo, IndiceVehiculo, i: Integer;
    DescriError: AnsiString;
    ConstREPRESENTATE, ConstESTADOALTA: Integer;
	AuxDomicilios: TDomicilios;
    FechaPresentacionDocumentacion: TDateTime;
	MensajeError: String;
	auxDatosTurno: TDatosTurno;
    FechaVencimientoGarantia: TDateTime;
    AniosVencimientoGarantiaTelevia: integer;
    TipoAsignacionTag: string;
    AuxFechaCreacion: TDateTime;
    AuxFechaCreacionSinHora: TDateTime;
    ValorValidado: Boolean;
    AuxFechaCreacionFormateada, AuxFechaCreacionSinHoraFormateada: string;

begin
	//****** INICIO GRABAR DATOS DEL CONTRATO ******//

    (* Verificar que el turno que se est� por utilizar est� abierto y sea del
    usuario logueado. *)
    if VerTurnoAbierto(DMConnections.BaseCAC, PuntoEntrega, PuntoVenta, UsuarioSistema, auxDatosTurno) then begin
        GNumeroTurno := auxDatosTurno.NumeroTurno;
    end else begin
        if (auxDatosTurno.NumeroTurno < 1) or (auxDatosTurno.Estado = TURNO_ESTADO_CERRADO) then GNumeroTurno := -1;

        if (auxDatosTurno.NumeroTurno <> -1)
                and ((auxDatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                and (Trim(auxDatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
            MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [auxDatosTurno.CodigoUsuario]),
                Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]), MB_ICONSTOP);
            Exit;
        end else begin
            MsgBox(MSG_ERROR_TURNO_CERRADO, Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]), MB_ICONSTOP);
            Exit;
        end;
    end;

	TodoOk := false;

	Screen.Cursor := crHourGlass;
    try
        try
			ConstESTADOALTA := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_ESTADO_CONVENIO_ALTA()');
			ConstREPRESENTATE := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_REPRESENTANTE_LEGAL()');

			DatoPersona := FreDatoPersona.RegistrodeDatos;
			CodigoPersonaMandante := -1;

			DMConnections.BaseCAC.BeginTrans;

			//Persona
			CodigoPersonaTitularContrato := ObtenerCodigoPersona(DMConnections.BaseCAC, DatoPersona.NumeroDocumento, DatoPersona.TipoDocumento);
			CodigoPersonaTitularContrato := GrabarPersona(DatoPersona.Personeria,
                                          iif(DatoPersona.Personeria = PERSONERIA_JURIDICA, DatoPersona.RazonSocial, ''),
										  DatoPersona.Apellido,
                                          DatoPersona.ApellidoMaterno,
										  DatoPersona.Nombre,
										  DatoPersona.Sexo,
                                          DatoPersona.Giro,
										  CodigoPersonaTitularContrato,
										  DatoPersona.TipoDocumento,
                                          DatoPersona.NumeroDocumento,
                                          DatoPersona.LugarNacimiento,
                                          DatoPersona.FechaNacimiento,
										  DatoPersona.CodigoActividad,
                                          DatoPersona.Password,
										  DatoPersona.Pregunta,
                                          DatoPersona.Respuesta,
                                          cb_RecibeClavePorMail.checked);

            if CodigoPersonaTitularContrato < 1 then
                raise Exception.Create(MSG_ERROR_SAVE_PERSON);

            //cliente
            with ActualizarDatosCliente.Parameters do begin
                Refresh;                                                                   //TASK_106_JMA_20170206
                ParamByName('@CodigoCliente').Value := CodigoPersonaTitularContrato;
            end;
            ActualizarDatosCliente.ExecProc;
            ActualizarDatosCliente.Close;

            //Domicilio Persona
            if not(ObtenerDomicilioPersonas(DatoPersona.CodigoPersona, AuxDomicilios, true)) then
                raise Exception.Create(MSG_ERROR_DOM);

            if FMDomicilioPrincipal.Enabled then begin

                CodigoDomicilioTitularCuenta := GrabarDomicioConvenio(CodigoPersonaTitularContrato,
                                        FMDomicilioPrincipal.Region,
                                        FMDomicilioPrincipal.Comuna,
                                        FMDomicilioPrincipal.CodigoCalle,
                    FMDomicilioPrincipal.CodigoSegmento,
                                        iif(FMDomicilioPrincipal.EsCalleNormalizada, '', FMDomicilioPrincipal.DescripcionCalle),
                                        True,
                                        FMDomicilioPrincipal.Numero,
                                        FMDomicilioPrincipal.CodigoPostal,
                                        FMDomicilioPrincipal.Detalle,
                                        FMDomicilioPrincipal.Depto);

            end else CodigoDomicilioTitularCuenta := AuxDomicilios[0].CodigoDomicilio;

            if CodigoDomicilioTitularCuenta < 0 then
                raise Exception.Create(MSG_ERROR_SAVE_DOM);

			//Medios de Comunicacion titular
            if (cdsVehiculosContacto.RecordCount <> 0) OR
                ((cdsVehiculosContacto.RecordCount = 0) And (FreTelefonoPrincipal.txtTelefono.Text <> '')) then begin
                if FreTelefonoPrincipal.txtTelefono.Enabled then begin
                    with FreTelefonoPrincipal do begin
                        if GrabarMediosComunicacion(CodigoTipoTelefono, CodigoArea,
                                NumeroTelefono, Anexo, -1, HorarioDesde, HorarioHasta, '',
                                CodigoPersonaTitularContrato, true, 'N', CodigoMedioComunicacion) < 0 then //n -> NO DETERMINADO
                            raise Exception.Create(MSG_ERROR_SAVE_TEL);
                    end;
                end;
            end;
            if (cdsVehiculosContacto.RecordCount <> 0) OR
                ((cdsVehiculosContacto.RecordCount = 0) And (FreTelefonoSecundario.txtTelefono.Text <> '')) then begin
                with FreTelefonoSecundario do begin
                    if (NumeroTelefono <> '') and (txtTelefono.Enabled) then begin
                        if GrabarMediosComunicacion(CodigoTipoTelefono, CodigoArea,
                                NumeroTelefono, Anexo, -1, HorarioDesde, HorarioHasta,
                                '', CodigoPersonaTitularContrato, False, 'N',
                                CodigoMedioComunicacion) < 0 then //n -> NO DETERMINADO
                            raise Exception.Create(MSG_ERROR_SAVE_TEL_SEC);
                    end;
                end;
            end;
            if FreDatoPersona.Personeria = PERSONERIA_JURIDICA then begin
                if (trim(txtEMailContacto.Text) <> '') and (txtEMailContacto.Enabled) then begin
                    if GrabarMediosComunicacion(TIPO_MEDIO_CONTACTO_E_MAIL, -1,
                            trim(txtEMailContacto.Text), -1, -1, StrToDateTime(HORA_DESDE),
                            StrToDateTime(HORA_HASTA), '', CodigoPersonaTitularContrato,
                            false, 'N', txtEMailContacto.Tag ) < 0 then //n -> NO DETERMINADO
                        raise Exception.Create(MSG_ERROR_SAVE_MAIL);

                end;
            end else begin
                if (trim(txtEmailParticular.Text) <> '') and (txtEmailParticular.Enabled) then begin
                    if GrabarMediosComunicacion(TIPO_MEDIO_CONTACTO_E_MAIL, -1,
                            trim(txtEmailParticular.Text), -1, -1, StrToDateTime(HORA_DESDE),
                            StrToDateTime(HORA_HASTA), '', CodigoPersonaTitularContrato,
                            false, 'N', txtEmailParticular.Tag ) < 0 then //n -> NO DETERMINADO
                        raise Exception.Create(MSG_ERROR_SAVE_MAIL);
                end;
            end;

            //Domicilio Secundario o de facturacion de Persona
            CodigoDomicilioSecundario := -1;
            if DomicilioAlternativo.NumeroCalle <> -1 then begin
                CodigoDomicilioSecundario := GrabarDomicioConvenio(CodigoPersonaTitularContrato,
                    DomicilioAlternativo.CodigoRegion, DomicilioAlternativo.CodigoComuna,
                    DomicilioAlternativo.CodigoCalle, DomicilioAlternativo.CodigoSegmento,
                    iif(trim(DomicilioAlternativo.CalleDesnormalizada) = '', '', DomicilioAlternativo.CalleDesnormalizada),
                    false, DomicilioAlternativo.NumeroCalleSTR, DomicilioAlternativo.CodigoPostal, DomicilioAlternativo.Detalle , DomicilioAlternativo.Dpto);

                if CodigoDomicilioSecundario < 0 then
                    raise Exception.Create(MSG_ERROR_SAVE_DOM_SEC);
            end;

            // Mandante - Datos personales
            if (DatosMedioPago.TipoMedioPago = PAC) or (DatosMedioPago.TipoMedioPago = PAT) then begin
                // controlo que no sea el titular
                if (DatoPersona.TipoDocumento = FRegDatoMandante.TipoDocumento) and (DatoPersona.NumeroDocumento = FRegDatoMandante.NumeroDocumento) then
                    CodigoPersonaMandante := CodigoPersonaTitularContrato
                else begin
                    CodigoPersonaMandante := ObtenerCodigoPersona(DMConnections.BaseCAC, FRegDatoMandante.NumeroDocumento, FRegDatoMandante.TipoDocumento);
                    CodigoPersonaMandante := GrabarPersona(FRegDatoMandante.Personeria,
                        iif(FRegDatoMandante.Personeria = PERSONERIA_JURIDICA, FRegDatoMandante.RazonSocial, ''),
                        FRegDatoMandante.Apellido, FRegDatoMandante.ApellidoMaterno, FRegDatoMandante.Nombre, '',
                        FRegDatoMandante.Giro, CodigoPersonaMandante, FRegDatoMandante.TipoDocumento,
                        FRegDatoMandante.NumeroDocumento, FRegDatoMandante.LugarNacimiento, FRegDatoMandante.FechaNacimiento,
                        FRegDatoMandante.CodigoActividad, FRegDatoMandante.Password, FRegDatoMandante.Pregunta, FRegDatoMandante.Respuesta);
                end;
                if CodigoPersonaMandante < 0 then
                    raise Exception.Create(MSG_ERROR_SAVE_MAND);

                // grabo el telefono como un medio de comunicacion
                if (trim(DatosMedioPago.Telefono) <> '') and (FTelMandanteHabilitado) then begin//not((DatoPersona.TipoDocumento=FRegDatoMandante.TipoDocumento) and (DatoPersona.NumeroDocumento=FRegDatoMandante.NumeroDocumento)) then begin
                    if GrabarMediosComunicacion(TIPO_MEDIO_CONTACTO_TE_PARTICULAR, DatosMedioPago.CodigoArea,
                            DatosMedioPago.Telefono, -1, -1, StrToTime(HORA_DESDE), StrToTime(HORA_HASTA),
                            '', CodigoPersonaMandante, true, 'N', IndiceMedioComunicacionMandante) < 0 then //n -> NO DETERMINADO
                        raise Exception.Create(MSG_ERROR_SAVE_TEL_MAND);
                end;
            end;

			// CONVENIOS
            with ActualizarConvenio.Parameters do begin
                Refresh;                                                                                       //TASK_106_JMA_20170206
                ParamByName('@NumeroConvenio').Value                    := NumeroContrato;
                ParamByName('@CodigoCliente').Value                     := CodigoPersonaTitularContrato;
                ParamByName('@CodigoEstadoConvenio').Value              := ConstESTADOALTA;
                ParamByName('@TipoMedioPagoAutomatico').Value           := cb_PagoAutomatico.Value;
                ParamByName('@CodigoDomicilioFacturacion').Value        := iif(DomicilioAlternativo.NumeroCalle<>-1,CodigoDomicilioSecundario,CodigoDomicilioTitularCuenta);
                ParamByName('@CicloFacturacion').Value                  := 1; // es el default
                ParamByName('@TipoTotalizacionDocumentosCobro').Value   := 1; // es el default
                ParamByName('@FechaInicioVigencia').Value               := NowBase(DMConnections.BaseCAC);
                ParamByName('@UbicacionFisicaCarpeta').Value            := Trim(txt_UbicacionFisicaCarpeta.Text);
                ParamByName('@CodigoPersonaMedioPagoAutomatico').Value  := iif(CodigoPersonaMandante < 1, null, CodigoPersonaMandante);
                ParamByName('@CodigoCampania').Value                    := null;
                ParamByName('@CodigoUsuarioActualizacion').Value        := UsuarioSistema;
                ParamByName('@AccionRNUT').Value                        := iif(cdsVehiculosContacto.RecordCount <> 0, XML_ALTA, XML_NADA);
                ParamByName('@Observaciones').Value                     := FObservaciones;
                ParamByName('@PlanComercial').Value                     := 1; //defaul
                ParamByName('@CodigoConvenio').Value                    := null;
                ParamByName('@CodigoConcesionaria').Value				:= FCodigoConcesionariaNativa;
                ParamByName('@RecibirInfoPorMail').Value                := cb_RecInfoMail.Checked;
                ParamByName('@NumeroConvenioRNUT').Value                := NumeroContrato;
                ParamByName('@FechaBajaRNUT').Value                     := null;
                ParamByName('@FechaActualizacionMedioPago').Value       := ParamByName('@FechaInicioVigencia').Value; //Para no ejecutar otra vez una consulta a la bd
                ParamByName('@SinCuenta').Value                         := False;
                ParamByName('@Usuario').Value                           := UsuarioSistema;

                ParamByName('@ConvenioFacturacion').Value               := CodigoConvenioFacturacion;        
                ParamByName('@CodigoTipoConvenio').Value                := Integer(cbbTipoConvenio.Value);   
                ParamByName('@CodigoClienteFacturacion').Value          := CodigoClienteFacturacion;          
                ParamByName('@CodigoGrupoFacturacion').Value            := CodigoGrupoFacturacion;      // TASK_096_MGO_20161220

                if vcbTipoDocumentoElectronico.Value = TDCBX_AUTOMATICO_COD then begin
                    ParamByName('@TipoComprobanteFiscalAEmitir').Value  := NULL;
                end
                else begin
                    ParamByName('@TipoComprobanteFiscalAEmitir').Value  := vcbTipoDocumentoElectronico.Value;
                end;
                ParamByName('@AdheridoPA').Value                     := False;

                if (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_RNUT) OR (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) then
                    ParamByName('@CodigoTipoCliente').Value:= Integer(cbbTipoCliente.Value);

            end;
            ActualizarConvenio.ExecProc;

            if ActualizarConvenio.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                raise Exception.Create(ActualizarConvenio.Parameters.ParamByName('@ErrorDescription').Value );

            CodigoConvenio := ActualizarConvenio.Parameters.ParamByName('@CodigoConvenio').Value;
            FCodigoConvenio := CodigoConvenio;
            ActualizarConvenio.Close;



            FCheckListConvenio.Guardar(CodigoConvenio);
			FListObservacionConvenio.Guardar(CodigoConvenio);

			// Mandante - Datos de la cuenta
            if (DatosMedioPago.TipoMedioPago = PAC) or (DatosMedioPago.TipoMedioPago = PAT) then begin
                case DatosMedioPago.TipoMedioPago of
                    PAC: begin
                        with ActualizarConvenioMedioPagoAutomaticoPAC.Parameters do begin
                            Refresh;                                                                   //TASK_106_JMA_20170206
                            ParamByName('@CodigoConvenio').Value            := CodigoConvenio;
                            ParamByName('@CodigoTipoCuentaBancaria').Value  := DatosMedioPago.PAC.TipoCuenta;
                            ParamByName('@CodigoBanco').Value               := DatosMedioPago.PAC.CodigoBanco;
                            ParamByName('@NroCuentaBancaria').Value         := DatosMedioPago.PAC.NroCuentaBancaria;
                            ParamByName('@Sucursal').Value                  := DatosMedioPago.PAC.Sucursal;
                        end;
                        ActualizarConvenioMedioPagoAutomaticoPAC.ExecProc;
                        ActualizarConvenioMedioPagoAutomaticoPAC.Close;
                    end;
                    PAT: begin
                        with ActualizarConvenioMedioPagoAutomaticoPAT.Parameters do begin
                            Refresh;                                                                      //TASK_106_JMA_20170206
                            ParamByName('@CodigoConvenio').Value                := CodigoConvenio;
                            ParamByName('@CodigoTipoTarjetaCredito').Value      := DatosMedioPago.PAT.TipoTarjeta;
                            ParamByName('@NumeroTarjetaCredito').Value          := DatosMedioPago.PAT.NroTarjeta;
                            ParamByName('@FechaVencimiento').Value              := DatosMedioPago.PAT.FechaVencimiento;
                            ParamByName('@CodigoEmisorTarjetaCredito').Value    := DatosMedioPago.PAT.EmisorTarjetaCredito;
                        end;
                        ActualizarConvenioMedioPagoAutomaticoPAT.ExecProc;
                        ActualizarConvenioMedioPagoAutomaticoPAT.Close;
                    end;
                end;
            end;

            //INICIO: TASK_009_ECA_20160506
            if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_RNUT then
            begin
                // maestro vehiculos
                cdsVehiculosContacto.First;
                while not(cdsVehiculosContacto.Eof) do begin

                    with ActualizarMaestroVehiculos.Parameters do begin
                        Refresh;                                                          //TASK_106_JMA_20170206
                        CodigoVehiculo := QueryGetValueInt(DMConnections.BaseCAC, format('EXEC ObtenerVehiculo ''%s'',''%s''', [cdsVehiculosContacto.FieldByName('Patente').AsString, cdsVehiculosContacto.FieldByName('TipoPatente').AsString]));
                        ParamByName('@CodigoTipoPatente').Value         := iif(trim(cdsVehiculosContacto.FieldByName('TipoPatente').AsString) = '', PATENTE_CHILE, trim(cdsVehiculosContacto.FieldByName('TipoPatente').AsString));
                        ParamByName('@Patente').Value                   := trim(cdsVehiculosContacto.FieldByName('Patente').AsString);
                        ParamByName('@DigitoVerificadorPatente').Value  := trim(cdsVehiculosContacto.FieldByName('DigitoVerificadorPatente').AsString);
                        ParamByName('@CodigoMarca').Value               := cdsVehiculosContacto.FieldByName('CodigoMarca').AsInteger;
                        ParamByName('@Modelo').Value                    := iif(trim(cdsVehiculosContacto.FieldByName('Modelo').AsString) = '', null, cdsVehiculosContacto.FieldByName('Modelo').AsString);
                        ParamByName('@AnioVehiculo').Value              := cdsVehiculosContacto.FieldByName('AnioVehiculo').AsInteger;
                        //ParamByName('@CodigoColor').Value               := iif(trim(cdsVehiculosContacto.FieldByName('CodigoColor').AsString) = '', null, cdsVehiculosContacto.FieldByName('CodigoColor').AsInteger); //TASK_016_ECA_20160526
                        ParamByName('@CodigoColor').Value               := iif(cdsVehiculosContacto.FieldByName('CodigoColor').AsInteger = 0, null, cdsVehiculosContacto.FieldByName('CodigoColor').AsInteger); //TASK_016_ECA_20160526
                        //ParamByName('@CodigoTipoVehiculo').Value        := cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').AsInteger;                                  //TASK_106_JMA_20170206
                        ParamByName('@TieneAcoplado').Value             := cdsVehiculosContacto.FieldByName('TieneAcoplado').AsInteger;
                        //ParamByName('@CodigoCategoria').Value           := ObtenerCategoriaVehiculo(DMConnections.BaseCAC, cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').AsInteger);     //TASK_106_JMA_20170206
                        ParamByName('@DigitoVerificadorValido').Value   := cdsVehiculosContacto.FieldByName('DigitoVerificadorValido').AsInteger;
                        ParamByName('@CodigoVehiculo').Value            := CodigoVehiculo;
{INICIO: TASK_106_JMA_20170206
                        if Trim(cdsVehiculosContacto.FieldByName('PatenteRVM').AsString) = '' then
                            ParamByName('@PatenteRVM').Value    := Null
                        else
                            ParamByName('@PatenteRVM').Value    := Trim(cdsVehiculosContacto.FieldByName('PatenteRVM').AsString);

                        if Trim(cdsVehiculosContacto.FieldByName('DigitoVerificadorPatenteRVM').AsString) = '' then
                            ParamByName('@DigitoVerificadorPatenteRVM').Value := Null
                        else
                            ParamByName('@DigitoVerificadorPatenteRVM').Value := Trim(cdsVehiculosContacto.FieldByName('DigitoVerificadorPatenteRVM').AsString);
TERMINO: TASK_106_JMA_20170206}

                        if cdsVehiculosContacto.FieldByName('Robado').AsBoolean then begin
                            if cdsVehiculosContacto.FieldByName('Recuperado').AsBoolean then begin
                                ParamByName('@Robado').Value := False;
                            end else begin
                                ParamByName('@Robado').Value := True;
                            end;
                        end else begin
                            ParamByName('@Robado').Value := False;
                        end;


                        ActualizarMaestroVehiculos.ExecProc;

                         //INICIO: TASK_016_ECA_20160526
                        if ActualizarMaestroVehiculos.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                            raise Exception.Create(ActualizarMaestroVehiculos.Parameters.ParamByName('@ErrorDescription').Value );
                        //FINI: TASK_016_ECA_20160526

                        CodigoVehiculo := ActualizarMaestroVehiculos.Parameters.ParamByName('@CodigoVehiculo').Value;
                        ActualizarMaestroVehiculos.close;

                        cdsIndemnizaciones.First;
                        while not cdsIndemnizaciones.Eof do begin
                            if cdsIndemnizaciones.FieldByName('Patente').Value = Trim(cdsVehiculosContacto.FieldByName('Patente').AsString) then begin
                                TipoAsignacionTag := cdsIndemnizaciones.FieldByName('TipoAsignacionTag').AsString;
                                Break;
                            end;
                            cdsIndemnizaciones.Next;
                        end;


                        //calculo de la fecha de fin de garantia del TAG. indemnizaciones de televias
                        //REV.3
                        if cdsVehiculosContacto.FieldByName('FechaDeVencimientoTag').AsDateTime > 0 then begin
                            FechaVencimientoGarantia := cdsVehiculosContacto.FieldByName('FechaDeVencimientoTag').AsDateTime;
                        end
                        else begin

                            if TipoAsignacionTag = CONST_OP_TRANSFERENCIA_TAG_NUEVO then begin
                                ObtenerParametroGeneral(DMConnections.BaseCAC, 'ANIOS_VTO_GARANTIA_TAG', AniosVencimientoGarantiaTelevia);

                                if not VarIsNull(cdsVehiculosContacto.FieldByName('FechaAltaCuenta').Value) then
                                    FechaVencimientoGarantia := IncMonth( cdsVehiculosContacto.FieldByName('FechaAltaCuenta').Value, (12 * AniosVencimientoGarantiaTelevia))
                                else FechaVencimientoGarantia := IncMonth( NowBase(DMConnections.BaseCAC), (12 * AniosVencimientoGarantiaTelevia  ));

                            end else begin     // CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA
                                FechaVencimientoGarantia := QueryGetValueDateTime(DMConnections.BaseCAC,
                                format('select dbo.ObtenerVencimientoGarantiaTag(%d,%10.0f)',[cdsVehiculosContacto.FieldByName('ContextMark').AsInteger, cdsVehiculosContacto.FieldByName('ContractSerialNumber').AsFloat]));
                            end;
                        end;
                        //FIN REV.3

                        with ActualizarCuenta.Parameters do begin
                            Refresh;
                            ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                            ParamByName('@CodigoVehiculo').Value := CodigoVehiculo;
                            ParamByName('@IndiceVehiculo').Value := null;
                            ParamByName('@DetallePasadas').Value := 0;
{INICIO: TASK_106_JMA_20170206
                            ParamByName('@CodigoCategoria').Value := ObtenerCategoriaVehiculo(DMConnections.BaseCAC, cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').AsInteger);
}
                            ParamByName('@CodigoCategoria').Value := cdsVehiculosContacto.FieldByName('CodigoCategoriaInterurbana').Value;
                            ParamByName('@CodigoCategoriaUrbana').Value := cdsVehiculosContacto.FieldByName('CodigoCategoriaUrbana').Value;
{TERMINO: TASK_106_JMA_20170206}
                            ParamByName('@CentroCosto').Value := null;

                            if not VarIsNull(cdsVehiculosContacto.FieldByName('FechaAltaCuenta').Value) then
                                ParamByName('@FechaAltaCuenta').Value := cdsVehiculosContacto.FieldByName('FechaAltaCuenta').Value
                            else ParamByName('@FechaAltaCuenta').Value := NowBase(DMConnections.BaseCAC);

                            ParamByName('@FechaBajaCuenta').Value := null;
                            ParamByName('@CodigoEstadoCuenta').Value := ESTADO_CUENTA_VEHICULO_ACTIVO;
                            ParamByName('@ContextMark').Value := cdsVehiculosContacto.FieldByName('ContextMark').AsInteger;
                            ParamByName('@ContractSerialNumber').Value := dblVehiculos.DataSource.DataSet.FieldByName('ContractSerialNumber').AsFloat;
                            ParamByName('@FechaVencimientoTAG').Value := FechaVencimientoGarantia;
                            ParamByName('@FechaAltaTAG').Value := ParamByName('@FechaAltaCuenta').Value;
                            ParamByName('@FechaBajaTAG').Value := null;
                            ParamByName('@CodigoPuntoEntrega').Value := PuntoEntrega;
                            ParamByName('@Usuario').Value := UsuarioSistema;
                            ParamByName('@ResponsableInstalacion').Value := 1; //Atencion va el responsable tomarlo de algun lado
                            ParamByName('@AccionRNUT').Value := XML_ALTA;
                            ParamByName('@Foraneo').Value := cdsVehiculosContacto.FieldByName('Foraneo').Value;    // TASK_055_MGO_20160801

                            // Datos de la Eximici�n.
                            ParamByName('@FechaInicioOriginalEximicion').Value := cdsVehiculosContacto.FieldByName('FechaInicioOriginalEximicion').Value;
                            ParamByName('@FechaInicioEximicion').Value := cdsVehiculosContacto.FieldByName('FechaInicioEximicion').Value;
                            ParamByName('@FechaFinalizacionEximicion').Value := cdsVehiculosContacto.FieldByName('FechaFinalizacionEximicion').Value;
                            ParamByName('@CodigoMotivoEximicion').Value := cdsVehiculosContacto.FieldByName('CodigoMotivoEximicion').AsInteger;
                            ParamByName('@Patente').Value := Trim(cdsVehiculosContacto.FieldByName('Patente').AsString);

                            // Datos de la Bonificaci�n.
                            ParamByName('@FechaInicioOriginalBonificacion').Value := cdsVehiculosContacto.FieldByName('FechaInicioOriginalBonificacion').Value;
                            ParamByName('@FechaInicioBonificacion').Value := cdsVehiculosContacto.FieldByName('FechaInicioBonificacion').Value;
                            ParamByName('@FechaFinalizacionBonificacion').Value := cdsVehiculosContacto.FieldByName('FechaFinalizacionBonificacion').Value;
                            ParamByName('@PorcentajeBonificacion').Value := cdsVehiculosContacto.FieldByName('PorcentajeBonificacion').Value;
                            ParamByName('@ImporteBonificacion').Value := cdsVehiculosContacto.FieldByName('ImporteBonificacion').Value;
                            if cdsVehiculosContacto.FieldByName('Almacen').Value = CONST_STR_ALMACEN_COMODATO then ParamByName('@CodigoAlmacenDestino').Value := CONST_ALMACEN_EN_COMODATO;
                            if cdsVehiculosContacto.FieldByName('Almacen').Value = CONST_STR_ALMACEN_ARRIENDO then ParamByName('@CodigoAlmacenDestino').Value := CONST_ALMACEN_ARRIENDO;
                            if cdsVehiculosContacto.FieldByName('Almacen').Value = CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS then ParamByName('@CodigoAlmacenDestino').Value := CONST_ALMACEN_ENTREGADO_EN_CUOTAS;

                            ParamByName('@CodigoConvenioFacturacion').Value := IIf(cdsVehiculosContacto.FieldByName('CodigoConvenioFacturacion').Value=0,CodigoConvenio,cdsVehiculosContacto.FieldByName('CodigoConvenioFacturacion').Value); //TASK_004_ECA_20160411

                        end;

                        // Inicio Bloque: SS_916_PDO_20120111
                        {
                        //Se verifica si se puede grabar la fecha de alta sin hora
                        AuxFechaCreacionSinHora := Trunc(ActualizarCuenta.Parameters.ParamByName('@FechaAltaCuenta').Value);
                        AuxFechaCreacionFormateada := FormatDateTime('yyyymmdd hh:nn', ActualizarCuenta.Parameters.ParamByName('@FechaAltaCuenta').Value);
                        AuxFechaCreacionSinHoraFormateada := FormatDateTime('yyyymmdd hh:nn', AuxFechaCreacionSinHora);
                        ValorValidado := False;
                        TryStrToBool(QueryGetValue(DMConnections.BaseCAC,
                                                    FORMAT('SELECT DBO.ValidarFechaAltaCuentaPatenteTag (''%s'', %11.0f, %d, ''%s'', ''%s'') ',
                                                            [trim(cdsVehiculosContacto.FieldByName('Patente').AsString),
                                                             dblVehiculos.DataSource.DataSet.FieldByName('ContractSerialNumber').AsFloat,
                                                             cdsVehiculosContacto.FieldByName('ContextMark').AsInteger,
                                                             AuxFechaCreacionSinHoraFormateada,
                                                             AuxFechaCreacionFormateada])),ValorValidado);
                        if ValorValidado then begin
                            ActualizarCuenta.Parameters.ParamByName('@FechaAltaCuenta').Value := AuxFechaCreacionSinHora;
                            cdsVehiculosContacto.Edit;
                            cdsVehiculosContacto.FieldByName('FechaAltaCuenta').Value := ActualizarCuenta.Parameters.ParamByName('@FechaAltaCuenta').Value;
                            cdsVehiculosContacto.Post;
                        end;
                        }
                        // Fin Bloque: SS_916_PDO_20120111

                        ActualizarCuenta.ExecProc;

                        //INICIO: TASK_013_ECA_20160520
                        if ActualizarCuenta.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                            raise Exception.Create(ActualizarCuenta.Parameters.ParamByName('@ErrorDescription').Value );
                        //FIN: TASK_013_ECA_20160520

                        IndiceVehiculo := ActualizarCuenta.Parameters.ParamByName('@IndiceVehiculo').Value;
                        ActualizarCuenta.Close;

                        //Cargar datos de indemnizaciones para el veh�culo
                        cdsIndemnizaciones.First;
                        while not cdsIndemnizaciones.Eof do begin
                            if cdsIndemnizaciones.FieldByName('Patente').Value = Trim(cdsVehiculosContacto.FieldByName('Patente').AsString) then begin
                                Refresh;                                                                   //TASK_106_JMA_20170206
                                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@IndiceVehiculo').Value := IndiceVehiculo;  //en las altas de vehiculos
                                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@FechaHora').Value := NowBase(DMConnections.BaseCAC);

                                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@DescripcionMotivo').Value := cdsIndemnizaciones.FieldByName('DescripcionMotivo').AsString;
                                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@ContractSerialNumber').Value := cdsVehiculosContacto.FieldByName('ContractSerialNumber').AsFloat;
                                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@ContextMark').Value := cdsVehiculosContacto.FieldByName('ContextMark').AsInteger;
                                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@NumeroMovimiento').Value := Null;


                                if cdsVehiculosContacto.FieldByName('Almacen').Value = CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS then begin
                                    spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoAlmacen').Value := CONST_ALMACEN_ENTREGADO_EN_CUOTAS;
                                    spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoConcepto').Value := FCodigoConceptoAIngresar;
                                end
                                else begin
                                    spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoConcepto').Value := cdsIndemnizaciones.FieldByName('CodigoConcepto').AsInteger;
                                    if cdsVehiculosContacto.FieldByName('Almacen').Value = CONST_STR_ALMACEN_COMODATO then spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoAlmacen').Value := CONST_ALMACEN_EN_COMODATO
                                    else if cdsVehiculosContacto.FieldByName('Almacen').Value = CONST_STR_ALMACEN_ARRIENDO then spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoAlmacen').Value := CONST_ALMACEN_ARRIENDO
                                end;

                                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@TipoAsignacionTag').Value := cdsIndemnizaciones.FieldByName('TipoAsignacionTag').AsString;
                                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@Observaciones').Value := cdsIndemnizaciones.FieldByName('Observaciones').AsString;
                                spGenerarMovimientoCuentaConTelevia.ExecProc;
                                spGenerarMovimientoCuentaConTelevia.Close;
                            end;
                            cdsIndemnizaciones.Next;
                        end;

                        with ActualizarTurnoMovimientoEntrega.Parameters do begin
                            Refresh;                                                                   //TASK_106_JMA_20170206
                            ParamByName('@NumeroTurno').Value := auxDatosTurno.NumeroTurno;
                            //ParamByName('@CodigoTipoVehiculo').Value := cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').AsInteger;   //TASK_106_JMA_20170206
                            ParamByName('@TieneAcoplado').Value := cdsVehiculosContacto.FieldByName('TieneAcoplado').AsInteger;
                            ParamByName('@CodigoVehiculo').Value := CodigoVehiculo;                                                            //TASK_106_JMA_20170206
                            ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                        end;
                        ActualizarTurnoMovimientoEntrega.ExecProc;

                        if ActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                            raise Exception.Create(ActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@ErrorDescription').Value );

                        ActualizarTurnoMovimientoEntrega.close;


                        //if cb_HabilitadoAMB.Checked then                                                                                                                                                     //SS-1006-NDR-20111105
                        //begin                                                                                                                                                                                //SS-1006-NDR-20111105
                        //  With spHabilitarListaBlanca do                                                                                                                                                     //SS-1006-NDR-20111105
                        //  begin                                                                                                                                                                              //SS-1006-NDR-20111105
                        //    Close;                                                                                                                                                                           //SS-1006-NDR-20111105
                        //    Parameters.Refresh;                                                                                                                                                              //SS-1006-NDR-20111105
                        //    Parameters.ParamByName('@ContractSerialNumber').Value := cdsVehiculosContacto.FieldByName('ContractSerialNumber').AsFloat;                                                       //SS-1006-NDR-20111105
                        //    Parameters.ParamByName('@Etiqueta').Value := SerialNumberToEtiqueta(cdsVehiculosContacto.FieldByName('ContractSerialNumber').AsString);                                          //SS-1006-NDR-20111105
                        //    Parameters.ParamByName('@Patente').Value := cdsVehiculosContacto.FieldByName('Patente').AsString;                                                                                //SS-1006-NDR-20111105
                        //    Parameters.ParamByName('@CodigoCategoria').Value := ObtenerCategoriaVehiculo(DMConnections.BaseCAC, cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').AsInteger);           //SS-1006-NDR-20111105
                        //    Parameters.ParamByName('@Novedad').Value := 'A';                                                                                                                                 //SS-1006-NDR-20111105
                        //    Parameters.ParamByName('@GeneradoAutomaticamente').Value := 0;                                                                                                                   //SS-1006-NDR-20111105
                        //    Parameters.ParamByName('@CambioMasivo').Value := 0;                                                                                                                              //SS-1006-NDR-20111105
                        //    Parameters.ParamByName('@DescripcionError').Value := '';                                                                                                                         //SS-1006-NDR-20111105
                        //    ExecProc;                                                                                                                                                                        //SS-1006-NDR-20111105
                        //  end;                                                                                                                                                                               //SS-1006-NDR-20111105
                        //end;                                                                                                                                                                                 //SS-1006-NDR-20111105
    //                    if cb_HabilitadoPA.Checked then                                                                                                                     //SS-1006-NDR-20111105
    //                    begin                                                                                                                                               //SS-1006-NDR-20111105
    //                      With spAgregarMovimientoListasDeAcceso do begin                                                                                                   //SS-1006-NDR-20111105
    //                        Close;                                                                                                                                          //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@TipoMovimiento').Value := 'A';                                                                                         //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@ContractSerialNumber').Value := cdsVehiculosContacto.FieldByName('ContractSerialNumber').AsFloat;                      //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@ContextMark').Value          := cdsVehiculosContacto.FieldByName('ContextMark').AsFloat;                               //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@CodigoConcesionaria').Value  := CODIGO_CN;                                                                             //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@Patente').Value              := cdsVehiculosContacto.FieldByName('Patente').AsFloat;                                   //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@IdEstadoListaAcceso').Value  := Null;                                                                                     //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@FechaEstado').Value          := NowBase(DMConnections.basecac);                                                                 //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@CodigoRespuestaWebService').Value := Null;                                                                             //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@RespuestaWebService').Value := Null;                                                                                   //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@FechaRespuestaWebService').Value := Null;                                                                              //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@Reintentos').Value := 0;                                                                              //SS-1006-NDR-20111105
    //                        Parameters.ParamByName('@MensajeError').Value := '';                                                                                            //SS-1006-NDR-20111105
    //                        ExecProc;                                                                                                                                       //SS-1006-NDR-20111105
    //                      end;                                                                                                                                              //SS-1006-NDR-20111105
    //                    end;                                                                                                                                                                                 //SS-1006-NDR-20111105
                        // ----------------------------------------------------------------------
                    end;

                    cdsVehiculosContacto.next;
                end;

                // Documentacion Presentada: En este caso hay una sola, de Alta de Convenio
                FechaPresentacionDocumentacion := NowBase(DMConnections.BaseCAC); //para que todos tengan la misma fecha hora y me ahorro un select por documento

                for i := 0 to length(FDocumentacionPresentadaPorOS[0].Documentacion) - 1 do begin
                    with ActualizarDocumentacionPresentadaConvenios.Parameters do begin
                        Refresh;                                                                   //TASK_106_JMA_20170206
                        ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                        ParamByName('@CodigoDocumentacionRespaldo').Value := FDocumentacionPresentadaPorOS[0].Documentacion[i].CodigoDocumentacionRespaldo;
                        ParamByName('@FechaPresentacion').Value := FechaPresentacionDocumentacion;
                        ParamByName('@PathArchivo').Value := null;

                        if FDocumentacionPresentadaPorOS[0].Documentacion[i].IndiceVehiculoRelativo <= 0 then begin
                            ParamByName('@TipoPatente').Value := null;
                            ParamByName('@Patente').Value := null;
                        end else begin
                            ParamByName('@TipoPatente').Value := FDocumentacionPresentadaPorOS[0].Documentacion[i].TipoPatente;
                            ParamByName('@Patente').Value := FDocumentacionPresentadaPorOS[0].Documentacion[i].Patente;
                        end;
                        ParamByName('@NoCorresponde').Value := iif(FDocumentacionPresentadaPorOS[0].Documentacion[i].NoCorresponde, 1, 0);
                        ParamByName('@CodigoDocumentacionPresentada').Value := null;
                        ParamByName('@FechaBaja').Value := Null;
                    end;
                    ActualizarDocumentacionPresentadaConvenios.ExecProc;
                    ActualizarDocumentacionPresentadaConvenios.Close;
                end;
            end; //if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_RNUT then
            //FIN: TASK_009_ECA_20160506

            // medio de envio de documentacion
            ActualizarMediosEnvioDocumentacion;
            with ActualizarConvenioMediosEnvioDocumentos.Parameters do begin
                Refresh;                                                                   //TASK_106_JMA_20170206
                ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                ParamByName('@Usuario').Value := UsuarioSistema;

				for i := 0 to length(RegMedioEnvioCorreo) - 1 do begin
                    ParamByName('@CodigoMedioEnvioDocumento').Value := RegMedioEnvioCorreo[i].CodigoMedioEnvioDocumento;
					ParamByName('@CodigoTipoMedioEnvio').Value := RegMedioEnvioCorreo[i].CodigoTipoMedioEnvio;
					ActualizarConvenioMediosEnvioDocumentos.ExecProc;
                    ActualizarConvenioMediosEnvioDocumentos.Close;
                end;

            end;

            //Representantes legales
			if FreDatoPersona.RegistrodeDatos.Personeria=PERSONERIA_JURIDICA then begin
				CodigoPersonaRepresentateLega2 := -1;
				CodigoPersonaRepresentateLega3 := -1;

				// representate legal nro 1
				if (RepresentateLegal1.NumeroDocumento = FreDatoPersona.RegistrodeDatos.NumeroDocumento) and
					(RepresentateLegal1.TipoDocumento = FreDatoPersona.RegistrodeDatos.TipoDocumento) then

					CodigoPersonaRepresentateLega1 := CodigoPersonaTitularContrato

				else begin
					with RepresentateLegal1 do begin
						CodigoPersonaRepresentateLega1 := ObtenerCodigoPersona(DMConnections.BaseCAC, NumeroDocumento, TipoDocumento);
						CodigoPersonaRepresentateLega1 := GrabarPersona(Personeria, RazonSocial, Apellido, ApellidoMaterno, Nombre, Sexo, Giro,
													CodigoPersonaRepresentateLega1, TipoDocumento, NumeroDocumento, LugarNacimiento, FechaNacimiento,
													CodigoActividad, Password, Pregunta, Respuesta);
					end;
				end;

				if CodigoPersonaRepresentateLega1<1 then
					raise Exception.Create(MSG_ERROR_SAVE_REPLEG1);
				if trim(RepresentateLegal2.Nombre) <> '' then begin
					// representate legal nro 2
					if (RepresentateLegal2.NumeroDocumento = FreDatoPersona.RegistrodeDatos.NumeroDocumento) and
						(RepresentateLegal2.TipoDocumento = FreDatoPersona.RegistrodeDatos.TipoDocumento) then

						CodigoPersonaRepresentateLega2 := CodigoPersonaTitularContrato
					else begin
						with RepresentateLegal2 do begin
							CodigoPersonaRepresentateLega2 := ObtenerCodigoPersona(DMConnections.BaseCAC, NumeroDocumento, TipoDocumento);
							CodigoPersonaRepresentateLega2 := GrabarPersona(Personeria, RazonSocial, Apellido, ApellidoMaterno, Nombre, Sexo, Giro,
														CodigoPersonaRepresentateLega2, TipoDocumento, NumeroDocumento, LugarNacimiento, FechaNacimiento,
														CodigoActividad, Password, Pregunta, Respuesta);
						end;
					end;

					if CodigoPersonaRepresentateLega2 < 1 then
    					raise Exception.Create(MSG_ERROR_SAVE_REPLEG2);
				end;

				if trim(RepresentateLegal3.Nombre) <> '' then begin
					// representate legal nro 3

					if (RepresentateLegal3.NumeroDocumento = FreDatoPersona.RegistrodeDatos.NumeroDocumento) and
						(RepresentateLegal3.TipoDocumento = FreDatoPersona.RegistrodeDatos.TipoDocumento) then

						CodigoPersonaRepresentateLega3 := CodigoPersonaTitularContrato

					else begin
						with RepresentateLegal3 do begin
							CodigoPersonaRepresentateLega3 := ObtenerCodigoPersona(DMConnections.BaseCAC, NumeroDocumento, TipoDocumento);
							CodigoPersonaRepresentateLega3 := GrabarPersona(Personeria, RazonSocial, Apellido, ApellidoMaterno, Nombre, Sexo, Giro,
														CodigoPersonaRepresentateLega3, TipoDocumento, NumeroDocumento, LugarNacimiento, FechaNacimiento,
														CodigoActividad, Password, Pregunta, Respuesta);
						end;
					end;

					if CodigoPersonaRepresentateLega3 < 1 then
    					raise Exception.Create(MSG_ERROR_SAVE_REPLEG3);
				end;

				with spActualizarRepresentantesConvenios, Parameters do begin
                    Refresh;                                                                   //TASK_106_JMA_20170206
					ParamByName ('@CodigoClienteConvenio').Value := CodigoPersonaTitularContrato;
					if CodigoPersonaRepresentateLega1 > 0 then ParamByName ('@CodigoRepLegal1').Value := CodigoPersonaRepresentateLega1;
					if CodigoPersonaRepresentateLega2 > 0 then ParamByName ('@CodigoRepLegal2').Value := CodigoPersonaRepresentateLega2;
					if CodigoPersonaRepresentateLega3 > 0 then ParamByName ('@CodigoRepLegal3').Value := CodigoPersonaRepresentateLega3;

					ExecProc;
					Close;
				end;
			end;
		   // seteo el estado de la solicitud a finalizada
            QueryExecute(DMConnections.BaseCAC, Format('UPDATE SolicitudContacto SET CodigoConvenio = %d, CodigoEstadoSolicitud=%d WHERE CodigoSolicitudContacto=%d', [CodigoConvenio, ESTADO_SOLICITUD_CONVENIO_FIRMADO, FCodigoSolicitud]));


           //INICIO: TASK_005_ECA_20160420
           if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL then
                GuardarVehiculosCuentaComercial(CodigoConvenio);
           //FIN: TASK_005_ECA_20160420

           //INICIO: TASK_005_ECA_20160420
           if (chkCtaComercialPredeterminada.Checked) and (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) then
               ActualizarPersonaCuentaComercial(CodigoPersonaTitularContrato,CodigoConvenio);
           //FIN: TASK_005_ECA_20160420

           //INICIO: ACTUALIZAMOS EL HISTORICO DE CONVENIO TASK_008_ECA_20160502
           InsertarHistoricoConvenio(CodigoConvenio);

           if Integer(cbbTipoConvenio.Value) =  CODIGO_TIPO_CONVENIO_RNUT then  begin
{PENDIENTE: TASK_106_JMA_20170206 descomentar cuando exista SOR
Verificar este procedimiento para descomentar la linea que genera el campo TAG_serie}
               QueryExecute(DMConnections.BaseCAC, Format('EXEC XML_Convenios_GenerarArchivo %d', [CodigoConvenio]));
           end;


            DMConnections.BaseCAC.CommitTrans;


            // No se puede preguntar por la concesionaria pues s�lo se dan de alta convenios de CN.
            DescriError := EmptyStr;
//            if cdsVehiculosContacto.RecordCount > 0 then
//                if not GenerarNovedadXML}(DMConnections.BaseCAC, CodigoConvenio, FCodigoConcesionariaNativa , FPathNovedadXML,
//                        iif(FRNUTUTF8 = 1, True, False), DescriError) then begin
//                    if DescriError <> EmptyStr then begin
//                        MsgBoxErr(MSG_XMLFILE_ERROR, DescriError, MSG_CONFIRMATION, MB_OK);
//                    end;
//                end;


           //FIN: TASK_008_ECA_20160502
		   TodoOk := True;
		except
			  On E: Exception do begin
				  TodoOk:=False;
                    DMConnections.BaseCAC.RollbackTrans;
				  Screen.Cursor := crDefault;
				  MensajeError := e.message;
                    MsgBoxErr( format(MSG_ERROR_ACTUALIZAR, [STR_CONVENIO]), MensajeError, MSG_ERROR_SOLICITUD_CONTRATO, MB_ICONSTOP);
				end;
		end;
    finally
        Screen.Cursor := crDefault;
        Result := TodoOk;
    end;
end;

procedure TFormSolicitudContacto.cb_POSChange(Sender: TObject);
begin
end;

{-----------------------------------------------------------------------------
Function Name   : GrabarMediosComunicacion
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
function  TFormSolicitudContacto.GrabarMediosComunicacion(CodigoTipoMedioContacto:Integer;
    CodigoArea:Smallint;Valor:String;Anexo,CodigoDomicilio:integer;HorarioDesde,HorarioHasta:TDateTime;
    Observaciones:String;CodigoPersona:Integer;Principal:Boolean;EstadoVerificacion:String;  CodigoMedioComunicacion: Integer):Integer;
begin
	try
        with ActualizarMedioComunicacionPersona.Parameters do begin
			ParamByName('@CodigoTipoMedioContacto').Value := CodigoTipoMedioContacto;
            ParamByName('@CodigoArea').Value := CodigoArea;
            ParamByName('@Valor').Value := Valor;
            ParamByName('@Anexo').Value := Anexo;
			ParamByName('@CodigoDomicilio').Value := CodigoDomicilio;
            ParamByName('@HorarioDesde').Value := HorarioDesde;
            ParamByName('@HorarioHasta').Value := HorarioHasta;
            ParamByName('@Observaciones').Value := Observaciones;
            ParamByName('@CodigoPersona').Value := CodigoPersona;
			ParamByName('@Principal').Value := Principal;
            ParamByName('@EstadoVerificacion').Value := EstadoVerificacion;
            ParamByName('@CodigoMedioComunicacion').Value := iif(CodigoMedioComunicacion < 0, NULL, CodigoMedioComunicacion);
        end;
		ActualizarMedioComunicacionPersona.ExecProc;
        result:=ActualizarMedioComunicacionPersona.Parameters.ParamByName('@CodigoMedioComunicacion').Value;
		ActualizarMedioComunicacionPersona.close;
	except
          On E: Exception do begin
			  result:=-1;
                MsgBoxErr( MSG_ERROR_SOLICITUD_CONVENIO, e.message, MSG_ERROR_SOLICITUD_CONVENIO, MB_ICONSTOP);
          end;
    end;

end;

{-----------------------------------------------------------------------------
Function Name   : GrabarDomicioConvenio
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.GrabarDomicioConvenio(CodigoPersona:Integer;Region,Comuna:String;CodigoCalle,CodigoSegmento:Integer;
    CalleDesnormalizada:String;Principal:Boolean;Numero,CodigoPostal,Detalle:String; Depto:string ; CodigoDomicilio: Integer):Integer;
begin
        try
            with ActualizarDomicilio.Parameters do begin
				Refresh;
                ParamByName('@CodigoTipoDomicilio').Value:=TIPO_DOMICILIO_PARTICULAR;
                ParamByName('@CodigoPais').Value:=PAIS_CHILE;
                ParamByName('@CodigoRegion').Value:=Region;
				ParamByName('@CodigoComuna').Value:=Comuna;
                ParamByName('@DescripcionCiudad').Value:=null;
				ParamByName('@CodigoCalle').Value:=CodigoCalle;
                ParamByName('@CodigoSegmento').Value:=CodigoSegmento;
                ParamByName('@CalleDesnormalizada').Value:=iif(CodigoCalle>=0,null,CalleDesnormalizada);
                ParamByName('@Numero').Value:=Numero;
				ParamByName('@Piso').Value:=null;
                ParamByName('@Depto').Value:=Depto;
                ParamByName('@CodigoPostal').Value:=CodigoPostal;
                ParamByName('@Detalle').Value:=Detalle;
                ParamByName('@CodigoDomicilio').Value:= iif(CodigoDomicilio < 1, null, CodigoDomicilio);
            end;
			ActualizarDomicilio.ExecProc;
            result:=ActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value;
            ActualizarDomicilio.close;

			// DOMICILIO PERSONA
			with ActualizarPersonasDomicilios.Parameters do begin
				ParamByName('@CodigoPersona').Value:=CodigoPersona;
                ParamByName('@CodigoDomicilio').Value:=Result;
				ParamByName('@Principal').Value:=iif(Principal,1,0);
				ParamByName('@Activo').Value:=1;
            end;
            ActualizarPersonasDomicilios.ExecProc;
			ActualizarPersonasDomicilios.close;
        except
              On E: Exception do begin
                  result:=-1;
                    MsgBoxErr( MSG_ERROR_SOLICITUD_CONVENIO, e.message, MSG_ERROR_SOLICITUD_CONVENIO, MB_ICONSTOP);
              end;
        end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : LlamarMedioPagoSolicitud
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.LlamarMedioPagoSolicitud;
var
    f:TFormMediosPagos;
	DescripMedioPago,error:String;
  TipoMedioPago: Variant;
begin
	Application.CreateForm(TFormMediosPagos,f);

    if (cb_PagoAutomatico.Value = TPA_PAT) then TipoMedioPago := PAT else TipoMedioPago := PAC;

    DatosMedioPago.TipoMedioPago := TipoMedioPago;
    f.inicializar(DatosMedioPago, ModoPantalla, FreDatoPersona.RegistrodeDatos.NumeroDocumento,
        upperCase(format('%s %s %s', [trim(FreDatoPersona.RegistrodeDatos.Nombre), trim(FreDatoPersona.RegistrodeDatos.Apellido), trim(FreDatoPersona.RegistrodeDatos.ApellidoMaterno)])),
		self.FreTelefonoPrincipal.CodigoArea, self.FreTelefonoPrincipal.NumeroTelefono, btn_EditarPagoAutomatico.Caption=CAPTION_MEDIO_PAGO_VER);
	if f.ShowModal = mrok then DatosMedioPago := f.Registro
    else begin
        if f.ValidarRegistro(DatosMedioPago,error, FreDatoPersona.RegistrodeDatos) then begin
            CargarMediosPagoAutomatico(cb_PagoAutomatico,iif(DatosMedioPago.TipoMedioPago=PAT,TPA_PAT,TPA_PAC));
            btn_EditarPagoAutomatico.Enabled:=true;
        end;
    end;
    if not(F.ArmarMedioPago(DatosMedioPago,DescripMedioPago)) then DescripMedioPago:=UPPERCASE(STR_ERROR);
    lbl_DescripcionMedioPago.Caption:=DescripMedioPago;

    f.Release;
end;

{-----------------------------------------------------------------------------
Procedure Name  : LlamarMedioPagoConvenio
Author          :
Date Created    :
Description     : se cargan los atributos relacionados a Medios de Pagos.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.LlamarMedioPagoConvenio;
var
    f:TFormMediosPagosConvenio;
	DescripMedioPago,error:String;
    TipoMedioPago : Variant;
begin
    Application.CreateForm(TFormMediosPagosConvenio,f);

  if (cb_PagoAutomatico.Value = TPA_PAT) then TipoMedioPago := PAT else TipoMedioPago := PAC;

    DatosMedioPago.TipoMedioPago := TipoMedioPago;
	f.inicializar(LimpiarCaracteresLabel(btn_EditarPagoAutomatico.Caption),
                  DatosMedioPago,
                  FRegDatoMandante,FreDatoPersona.RegistrodeDatos,
                  CONST_ORIGEN_MEDIO_PAGO_COSTANERA,
                  ModoPantalla,
                  self.FreTelefonoPrincipal.CodigoArea,
                  self.FreTelefonoPrincipal.NumeroTelefono,
                  btn_EditarPagoAutomatico.Caption = CAPTION_MEDIO_PAGO_VER);
    if f.ShowModal = mrok then begin
        DocControlada := DocControlada and (DatosMedioPago.TipoMedioPago<>PAT);
        DatosMedioPago := f.RegistroMedioPago;
		FRegDatoMandante:=f.RegistroPersonaMandante;
        IndiceMedioComunicacionMandante := f.Telefono.Indice;
        FTelMandanteHabilitado:=f.TelefonoHabilitado;
    end else begin
		if f.ValidarMedioPago(DatosMedioPago,
                CONST_ORIGEN_MEDIO_PAGO_COSTANERA,
                FreDatoPersona.RegistrodeDatos.NumeroDocumento, error) then begin
            CargarMediosPagoAutomatico(cb_PagoAutomatico,iif(DatosMedioPago.TipoMedioPago=PAT,TPA_PAT,TPA_PAC));
			btn_EditarPagoAutomatico.Enabled:=true;
		end;
    end;
    if not(F.ArmarMedioPago(DatosMedioPago, False, -1, DescripMedioPago)) then DescripMedioPago := UPPERCASE(STR_ERROR);
	lbl_DescripcionMedioPago.Caption:=DescripMedioPago;

	f.Release;
end;

{-----------------------------------------------------------------------------
Procedure Name  : GrabarPersona
Author          :
Date Created    :
Description     : Modifica atributos asociados a Persona en Base de Datos.
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.GrabarPersona(Personeria,RazonSocial,Apellido,ApellidoMaterno,Nombre,Sexo, Giro:String;
	CodigoPersona:Integer;Codigodocumento,NumeroDocumento,LugarNacimiento:String;FechaNacimiento:TDateTime;
    CodigoActividad:Integer;Password, Pregunta, Respuesta:String; RecibeClavePorMail: boolean = false): Integer;
begin
    try
        with ActualizarDatosPersona.Parameters do begin
            ParamByName('@Personeria').Value:=Personeria;
			if Personeria=PERSONERIA_JURIDICA then begin
                ParamByName('@ApellidoContactoComercial').Value:=Apellido;
				ParamByName('@ApellidoMaternoContactoComercial').Value:=ApellidoMaterno;
                ParamByName('@NombreContactoComercial').Value:=Nombre;
				ParamByName('@SexoContactoComercial').Value:=Sexo;
                ParamByName('@Apellido').Value:=RazonSocial;
				ParamByName('@ApellidoMaterno').Value:=Null;
                ParamByName('@Nombre').Value:=Null;
                ParamByName('@Sexo').Value:=Null;

            end else begin
                ParamByName('@Apellido').Value:=Apellido;
				ParamByName('@ApellidoMaterno').Value:=ApellidoMaterno;
                ParamByName('@Nombre').Value:=Nombre;
                ParamByName('@Sexo').Value:=iif(trim(Sexo)='',null,sexo);
                ParamByName('@ApellidoContactoComercial').Value:=Null;
				ParamByName('@ApellidoMaternoContactoComercial').Value:=Null;
                ParamByName('@NombreContactoComercial').Value:=Null;
                ParamByName('@SexoContactoComercial').Value:=Null;

            end;

            ParamByName('@Giro').Value				:= Giro;
			ParamByName('@CodigoPersona').Value		:= CodigoPersona;
            ParamByName('@CodigoDocumento').Value	:= iif(FreDatoPersona.RegistrodeDatos.TipoDocumento='',TIPO_DOCUMENTO_RUT,FreDatoPersona.RegistrodeDatos.TipoDocumento);
            ParamByName('@NumeroDocumento').Value	:= NumeroDocumento;
            ParamByName('@LugarNacimiento').Value	:= iif(Trim(LugarNacimiento) = '', null, Trim(LugarNacimiento));
			ParamByName('@FechaNacimiento').Value	:= iif(FechaNacimiento=NullDate,null,FechaNacimiento);

            ParamByName('@CodigoActividad').Value			:= CodigoActividad;
			ParamByName('@EmailContactoComercial').Value	:= null;
            ParamByName('@CodigoSituacionIVA').Value		:= null;

			ParamByName('@Password').Value		:= Password;
            ParamByName('@Pregunta').Value		:= Pregunta;
			ParamByName('@Respuesta').Value		:= Respuesta;
            ParamByName('@TipoCliente').Value	:= FreDatoPersona.cboTipoCliente.KeyValue;
            ParamByName('@RecibeClavePorMail').Value := RecibeClavePorMail ;
            ParamByNAme('@Semaforo1').Value     := FreDatoPersona.cboSemaforo1.KeyValue;
            ParamByNAme('@Semaforo3').Value     := FreDatoPersona.cboSemaforo3.KeyValue;
            ParamByName('@CodigoUsuario').Value := UsuarioSistema;                        //SS_1419_NDR_20151130

            //INICIO: TASK_038_ECA_20160625
            if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_RNUT then
            begin
                ParamByName('@InactivaDomicilioRNUT').Value :=  chkInactivaDomicilioRNUT.Checked;
                ParamByName('@InactivaMedioComunicacionRNUT').Value :=  chkInactivaMediosComunicacionRNUT.Checked;
            end;
            //FIN: TASK_038_ECA_20160625
        end;
        ActualizarDatosPersona.ExecProc;
		Result:=ActualizarDatosPersona.Parameters.ParamByName('@CodigoPersona').Value;
        ActualizarDatosPersona.close;
	except
          On E: Exception do begin
              result:=-1;
                MsgBoxErr( MSG_ERROR_SOLICITUD_CONVENIO, e.message, MSG_ERROR_SOLICITUD_CONVENIO, MB_ICONSTOP);
		  end;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_CancelarContratoClick
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_CancelarContratoClick(
  Sender: TObject);
begin
    btnSalir.Click;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_RepresentanteLegalClick
Author          :
Date Created    :
Description     : Carga Pantalla Representante legal.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_RepresentanteLegalClick(
  Sender: TObject);
resourcestring
    STR_Y_OTROS = ' y otro/s';
var
   f:TFormRepresentantes;
begin
	Application.CreateForm(TFormRepresentantes,f);

	f.inicializar(LimpiarCaracteresLabel(btn_RepresentanteLegal.Caption),
				RepresentateLegal1,
				RepresentateLegal2,
				RepresentateLegal3,
				FreDatoPersona.RegistrodeDatos.NumeroDocumento,
				//FEsconvenioCN,												//SS_1147_MCA_20140408
                FEsConvenioNativo,												//SS_1147_MCA_20140408
				ModoPantalla);

	//f.btnBorrarUno.Enabled := FEditarRepresentateLegal1 or (not FEsConvenioCN); //SS_1147_MCA_20140408
	//f.btnBorrarDos.Enabled := FEditarRepresentateLegal2 or (not FEsConvenioCN); //SS_1147_MCA_20140408
	//f.btnBorrarTres.Enabled:= FEditarRepresentateLegal3 or (not FEsConvenioCN); //SS_1147_MCA_20140408
    f.btnBorrarUno.Enabled := FEditarRepresentateLegal1 or (not FEsConvenioNativo); //SS_1147_MCA_20140408
	f.btnBorrarDos.Enabled := FEditarRepresentateLegal2 or (not FEsConvenioNativo); //SS_1147_MCA_20140408
	f.btnBorrarTres.Enabled:= FEditarRepresentateLegal3 or (not FEsConvenioNativo); //SS_1147_MCA_20140408
	if f.ShowModal = mrok then begin
		FRepresentantesValidados := True;
		
		if RepresentateLegal1.NumeroDocumento<>f.RepresentanteUno.NumeroDocumento then begin
				DocControlada:=false;
				TformDocPresentada.BorrarRegistroRepresentantes(FDocumentacionPresentadaPorOS[0].Documentacion,PRIMERO);
		end;

		if RepresentateLegal2.NumeroDocumento<>f.RepresentanteDos.NumeroDocumento then begin
				DocControlada:=false;
				TformDocPresentada.BorrarRegistroRepresentantes(FDocumentacionPresentadaPorOS[0].Documentacion,SEGUNDO);
		end;

		if RepresentateLegal3.NumeroDocumento<>f.RepresentanteTres.NumeroDocumento then begin
				DocControlada:=false;
				TformDocPresentada.BorrarRegistroRepresentantes(FDocumentacionPresentadaPorOS[0].Documentacion,TERCERO);
		end;

		RepresentateLegal1:=f.RepresentanteUno;
		RepresentateLegal2:=f.RepresentanteDos;
		RepresentateLegal3:=f.RepresentanteTres;
		lbl_RepresentanteLegal.Caption:=ArmarNombrePersona(RepresentateLegal1.Personeria,RepresentateLegal1.Nombre,RepresentateLegal1.Apellido,RepresentateLegal1.ApellidoMaterno)+iif(trim(RepresentateLegal2.Nombre)<>'',STR_Y_OTROS,'');
	end;

	RepLegal:= (Trim(RepresentateLegal1.NumeroDocumento) <> '') or
		(Trim(RepresentateLegal2.NumeroDocumento) <> '') or
		(Trim(RepresentateLegal3.NumeroDocumento) <> '');

	f.Release;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_DocRequeridoClick
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_DocRequeridoClick(Sender: TObject);
var
    f: TformDocPresentada;
    auxCant : Integer;
begin
    Application.CreateForm(TformDocPresentada,f);
    f.Inicializar(LimpiarCaracteresLabel(btn_DocRequerido.Caption), TClientDataSet(dsVehiculosContacto.DataSet),
            FDocumentacionPresentadaPorOS[0].Documentacion, FreDatoPersona.RegistrodeDatos.Personeria,DatosMedioPago.TipoMedioPago,
			iif(trim(RepresentateLegal1.Nombre)='','', ArmarNombrePersona(RepresentateLegal1.Personeria,RepresentateLegal1.Nombre,RepresentateLegal1.Apellido,RepresentateLegal1.ApellidoMaterno)),
            iif(trim(RepresentateLegal2.Nombre)='','', ArmarNombrePersona(RepresentateLegal2.Personeria,RepresentateLegal2.Nombre,RepresentateLegal2.Apellido,RepresentateLegal2.ApellidoMaterno)),
            iif(trim(RepresentateLegal3.Nombre)='','', ArmarNombrePersona(RepresentateLegal3.Personeria,RepresentateLegal3.Nombre,RepresentateLegal3.Apellido,RepresentateLegal3.ApellidoMaterno)),
            TformMedioEnvioDocumentosCobro.EnvioDocumentoCobroViaMail(RegMedioEnvioCorreo),auxCant, ModoPantalla, True);

    if f.ShowModal = mrOk then begin
        FDocumentacionPresentadaPorOS[0].Documentacion := f.DocumentacionPresentada;
        DocControlada := true;
	end;
	f.Release;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_GuardarSolicitudClick
Author          :
Date Created    :
Description     : Guarda solicitud y reinicializa atributos pantalla.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_GuardarSolicitudClick(
  Sender: TObject);
begin
    if MsgBox(MSG_QUESTION_GUARDAR_SOLICTUD, self.Caption, MB_YESNO) = mrYes then begin
        if SecuenciaGuardarSolicitudContacto then begin //guarda bien
            LimpiarCampos;
            HabilitarControles(false);
            FreDatoPersona.txtDocumento.SetFocus;
            Close;
        end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name   : SecuenciaGuardarSolicitudContacto
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.SecuenciaGuardarSolicitudContacto: Boolean;
var
    error: String;
begin
    result := False;
	if PuedeGuardarGenerico then begin

		if (cb_PagoAutomatico.ItemIndex>1) and // es el item Ninguno
            (not(TFormMediosPagos.ValidarRegistro(DatosMedioPago, error, FreDatoPersona.RegistrodeDatos))) then begin
            MsgBoxBalloon(Format(MSG_ERROR_NUMERO_TARJETA,[error]), Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),MB_ICONSTOP,cb_PagoAutomatico);
            cb_PagoAutomatico.SetFocus;
            Result := False;
            Exit;
        end;


        if (cb_POS.Items.Count>1) and (cb_POS.ItemIndex<=0) then begin
            MsgBoxBalloon(MSG_DEBE_PUNTO_ENTREGA, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),MB_ICONSTOP,cb_POS);
            cb_POS.SetFocus;
			result:=False;
			Exit;
		end;

        //Si no se ingresa ningun veh�culo le pregunta si quiere continuar
    	if (dblVehiculos.DataSource.DataSet.RecordCount = 0) then begin
            if MsgBox(MSG_ESTA_SEGURO_SIN_VEHICULOS, self.Caption,
                        MB_ICONWARNING or MB_YESNO) <> mrYes then begin
                btnAgregarVehiculo.SetFocus;
                result:=False;
    		    Exit;
            end;
        end;

        // guarda solicitud
        Screen.Cursor   := crHourGlass;
        if not(GuardarSolicitudcontacto) then begin
            MsgBox(MSG_ERROR_SOLICITUD_CONTACTO ,MSG_ERROR_SOLICITUD_CONTACTO,MB_ICONERROR);
            Screen.Cursor   := crDefault;
            result:=False;
            exit;
        end;

		Screen.Cursor   := crDefault;

        result := True;
    end;

end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_MedioDocClick
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_MedioDocClick(Sender: TObject);
var
    f:TformMedioEnvioDocumentosCobro;
    TieneEmail: Boolean;
begin
	Application.CreateForm(TformMedioEnvioDocumentosCobro, f);

    if FreDatoPersona.Personeria = PERSONERIA_JURIDICA then TieneEmail := (Trim(txtEMailContacto.Text) <> EmptyStr)
    else TieneEmail := (Trim(txtEmailParticular.Text) <> EmptyStr);

	if f.Inicializar(RegMedioEnvioCorreo, TieneEmail, ObtenerCantidadVehiculosConvenio, ModoPantalla) then begin
        if f.ShowModal = mrok then begin
            RegMedioEnvioCorreo := f.RegistroMedioEvioDoc;
            MedioDocControlada  := True;
		end;
    end;
    f.Release;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_AsignarTagClick
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_AsignarTagClick(Sender: TObject);

{INICIO: TASK_106_JMA_20170206}
    function ObtenerNumeroConvenioGenerado(CodigoConvenio: Integer): string;
    begin
        Result := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerNumeroConvenio(%d)', [CodigoConvenio]));
    end;
{TERMINO: TASK_106_JMA_20170206}

ResourceString
    MSG_ERROR_EDITAR_VEHICULO      = 'No se pudieron modificar los datos del veh�culo';
    MSG_EDITAR_VEHICULO_CAPTION    = 'Actualizar datos del veh�culo';
var
    f : TFormSCDatosVehiculo;
    Cadena, CadenaTags: TStringList;
    dondeEstaba: TBookmark;
    NoCargar: string;
    Estado: Variant;
    NumeroConvenio: String;                    //TASK_106_JMA_20170206
    DatosConvenio: TDatosConvenio;             //TASK_106_JMA_20170206   
begin
    if cdsVehiculosContacto.RecordCount = 0 then Exit;
    Cadena := TStringList.Create;
    CadenaTags := TStringList.Create;
	dondeEstaba := cdsVehiculosContacto.GetBookmark;
    try
		if (cdsVehiculosContacto.fieldbyname('ContractSerialNumber').IsNull) then
			NoCargar := ''
        else
            NoCargar := cdsVehiculosContacto.fieldbyname('ContractSerialNumber').Value;
        ArmarListaPatentes(Cadena, Trim(dsVehiculosContacto.DataSet.FieldByName('Patente').AsString));
        ArmarListaTags(CadenaTags, Trim(NoCargar));
       cdsVehiculosContacto.GotoBookmark(dondeEstaba);
     finally
        cdsVehiculosContacto.FreeBookmark(dondeEstaba);
    end;
    Application.CreateForm(TFormSCDatosVehiculo, f);

    if(FTipoSolicitud = INTERMEDIO) then Estado := Peatypes.ConvenioModiVehiculo else Estado:= Peatypes.Solicitud;

    NumeroConvenio := ObtenerNumeroConvenioGenerado(FCodigoConvenio);                                                  //TASK_106_JMA_20170206
    DatosConvenio := TDatosConvenio.Create(NumeroConvenio,PuntoVenta,PuntoEntrega);                                   //TASK_106_JMA_20170206

    if f.InicializarEditar(MSG_CAPTION_EDITAR_VEHICULO, PuntoEntrega,													//TASK_106_JMA_20170206
        Trim(dsVehiculosContacto.DataSet.FieldByName('TipoPatente').AsString),
        Trim(dsVehiculosContacto.DataSet.FieldByName('Patente').AsString),
        //Trim(dsVehiculosContacto.DataSet.FieldByName('PatenteRVM').AsString),                                       //TASK_106_JMA_20170206
        dsVehiculosContacto.DataSet.FieldByName('Modelo').AsString,
        dsVehiculosContacto.DataSet.FieldByName('CodigoMarca').AsInteger,
        dsVehiculosContacto.DataSet.FieldByName('AnioVehiculo').AsInteger,
        //dsVehiculosContacto.DataSet.FieldByName('CodigoTipoVehiculo').AsInteger,                                    //TASK_106_JMA_20170206
        dsVehiculosContacto.DataSet.FieldByName('CodigoColor').AsInteger,
        Trim(dsVehiculosContacto.DataSet.FieldByName('DigitoVerificadorPatente').AsString),
		//Trim(dsVehiculosContacto.DataSet.FieldByName('DigitoVerificadorPatenteRVM').AsString),                      //TASK_106_JMA_20170206
        -1,
        dsVehiculosContacto.DataSet.FieldByName('Observaciones').AsString,
        FCodigoSolicitud,
        Cadena,
        True,
        CadenaTags,
        -1,
        cdsVehiculosContacto.FieldByName('SerialNumberaMostrar').AsString,
        True,
        True,
        -1,
        cdsVehiculosContacto,
        False,
        -1,
        Estado,
        nil,
        dsVehiculosContacto.DataSet.FieldByName('Recuperado').AsBoolean,                                             //TASK_106_JMA_20170206
        DatosConvenio)                                                                                               //TASK_106_JMA_20170206
        and (f.ShowModal = mrOK) then begin
		//Carga el Vehiculo en la Lista.
        try

            if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, 0, f.Patente, cdsVehiculosContacto.FieldByName('IndiceVehiculo').AsInteger, cdsVehiculosContacto.FieldByName('SerialNumberaMostrar').AsString, FPuntoEntrega, UsuarioSistema, 'BajaxCambioTag')  then
                raise Exception.create('Error registrando auditoria');

            if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, 0, f.Patente, cdsVehiculosContacto.FieldByName('IndiceVehiculo').AsInteger, f.NumeroTag, FPuntoEntrega, UsuarioSistema, 'AltaxCambioTag')  then
                raise Exception.create('Error registrando auditoria');


			with cdsVehiculosContacto do begin
                if UpperCase(trim(FieldByName('Patente').AsString))<> UpperCase(f.Patente) then  begin
                    DocControlada:=False; // si se agrega un auto, debe volver a controlar la documentacion
                    TformDocPresentada.BorrarRegistro(FDocumentacionPresentadaPorOS[0].Documentacion,FieldByName('Patente').AsString,FieldByName('TipoPatente').AsString);
                end;
                Edit;
                FieldByName('TipoPatente').AsString             := f.TipoPatente;
                FieldByName('Patente').AsString                 := f.Patente;
                FieldByName('CodigoMarca').AsInteger            := f.Marca;
                FieldByName('DescripcionMarca').AsString        := f.DescripcionMarca;
                FieldByName('Modelo').AsString                  := f.DescripcionModelo;
                FieldByName('CodigoColor').AsInteger            := f.Color;
                FieldByName('DescripcionColor').AsString        := f.DescripcionColor;
				FieldByName('AnioVehiculo').AsInteger           := f.Anio;
                //FieldByName('CodigoTipoVehiculo').AsInteger     := f.CodigoTipo;                           //TASK_106_JMA_20170206
                //FieldByName('DescripcionTipoVehiculo').AsString := f.DescripcionTipo;                      //TASK_106_JMA_20170206
                FieldByName('Observaciones').AsString           := Trim(f.Observaciones);
                FieldByName('ContextMark').Value                := f.ContextMark;
                FieldByName('ContractSerialNumber').Value       := f.SerialNumber;
                FieldByName('SerialNumberaMostrar').Value       := f.NumeroTag;
                FieldByName('TieneAcoplado').Value              := Null;
                FieldByName('DigitoVerificadorPatente').Value   := f.DigitoVerificador;
                FieldByName('DigitoVerificadorValido').Value    := iif(f.DigitoVerificadorCorrecto,1,0);

                if F.DatoCuenta.Vehiculo.Robado then begin
                    FieldByName('Robado').Value := True;
                end else begin
                    FieldByName('Robado').Value := False;
                end;

                if F.DatoCuenta.Vehiculo.Recuperado then begin
                    FieldByName('Recuperado').Value := True;
                end else begin
                    FieldByName('Recuperado').Value := False;
                end;

                Edit;
			end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_EDITAR_VEHICULO, E.message, MSG_EDITAR_VEHICULO_CAPTION, MB_ICONSTOP);
                dsVehiculosContacto.DataSet.Cancel;
            end;
        end;
	end;
    Cadena.Free;
	CadenaTags.Free;
	f.Release;
	dblVehiculos.Refresh;
	HabilitarBotonesVehiculos;
end;

{INICIO:  TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)
-----------------------------------------------------------------------------
Function Name   : ImprimirConvenioEtiqueta
Author          :
Date Created    :
Description     : Imprime Convenio en pantalla de funcionalidad.

Revision: 1
Author: ebaeza
Date: 14/03/2014
Firma: SS_1171_EBA_20140311
Description: SS_1171
    -.Se agrega la obtencion de la Razon social y el rut asociado, para la impresion
      del anexo III juridico, cuando la personeria sea igual a 'J'
-----------------------------------------------------------------------------
function TFormSolicitudContacto.ImprimirConvenioEtiqueta(NumeroConvenio: String; TipoImpresion:TTipoImpresion; FechaAlta : TDateTime;var MotivoCanelacion:TMotivoCancelacion):Boolean;
var
	F:TFormImprimirConvenio;
	Vehiculos: TRegCuentaABM;
    MotivoCancelacion: TMotivoCancelacion;
    AniosVencimientoGarantiaTelevia: integer;

    DatosAnexo3 : TDatosAnexo3;
    DatosAnexo4 : TDatosAnexo4;
    ListaAnexo3 : TStringList;	//guarda lista de telev�as para anexo3
    IndiceLista : integer;
begin
    ListaAnexo3 := TStringList.Create;
	Application.CreateForm(TFormImprimirConvenio,F);
    if FEsConvenioSinCuenta then f.RBInterface.ModalPreview := True;

    if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_RNUT then
        with dsVehiculosContacto.DataSet do begin
            First;
            while not eof do begin

                if FieldByName('Almacen').AsString = CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS then begin
                    ListaAnexo3.Add(cdsVehiculosContacto.FieldByName('SerialNumberAMostrar').AsString);
                end;

                SetLength(Vehiculos,length(Vehiculos)+1);
                Vehiculos[length(Vehiculos)-1].Cuenta.Vehiculo.Patente:=FieldByName('Patente').Value;
                Vehiculos[length(Vehiculos)-1].Cuenta.Vehiculo.DigitoPatente:=FieldByName('DigitoVerificadorPatente').value;
                Vehiculos[length(Vehiculos)-1].Cuenta.Vehiculo.CodigoMarca:=FieldByName('CodigoMarca').Value;
                Vehiculos[length(Vehiculos)-1].Cuenta.Vehiculo.Marca:=FieldByName('DescripcionMarca').Value;
                Vehiculos[length(Vehiculos)-1].Cuenta.Vehiculo.Modelo:=FieldByName('Modelo').Value;
                Vehiculos[length(Vehiculos)-1].Cuenta.Vehiculo.Anio:=FieldByName('AnioVehiculo').Value;
                Vehiculos[length(Vehiculos)-1].Cuenta.Vehiculo.CodigoTipo:=FieldByName('CodigoTipoVehiculo').Value;



                if not VarIsNull(FieldByName('FechaAltaCuenta').Value) then begin
                    Vehiculos[length(Vehiculos)-1].Cuenta.FechaCreacion := FieldByName('FechaAltaCuenta').Value;

                end
                else begin
                    Vehiculos[length(Vehiculos)-1].Cuenta.FechaCreacion := NowBase(DMConnections.BaseCAC);

                end;
                Vehiculos[length(Vehiculos)-1].Cuenta.FechaAltaTag := Vehiculos[length(Vehiculos)-1].Cuenta.FechaCreacion;

                Vehiculos[length(Vehiculos)-1].Cuenta.ContextMark := FieldByName('ContextMark').Value;
                Vehiculos[length(Vehiculos)-1].Cuenta.ContactSerialNumber:=FieldByName('ContractSerialNumber').Value;
                Vehiculos[length(Vehiculos)-1].Cuenta.TieneAcoplado := False;
                Vehiculos[length(Vehiculos)-1].TipoMovimiento := ttmAlta;

                Vehiculos[length(Vehiculos)-1].Cuenta.SerialNumberaMostrar:=FieldByName('SerialNumberaMostrar').Value; //TASK_009_ECA_20160506
                Vehiculos[length(Vehiculos)-1].Cuenta.DescripcionTipoVehiculo:=FieldByName('DescripcionTipoVehiculo').Value; //TASK_009_ECA_20160506

                cdsIndemnizaciones.First;
                while not cdsIndemnizaciones.eof do begin
                    if cdsIndemnizaciones.FieldByName('Patente').AsString = FieldByName('Patente').Value then begin
                        if cdsIndemnizaciones.FieldByName('TipoAsignacionTag').Value  = CONST_OP_TRANSFERENCIA_TAG_NUEVO then begin
                            ObtenerParametroGeneral(DMConnections.BaseCAC, 'ANIOS_VTO_GARANTIA_TAG', AniosVencimientoGarantiaTelevia);
                           if not (Vehiculos[length(Vehiculos)-1].Cuenta.FechaCreacion = NullDate) then
                                Vehiculos[length(Vehiculos)-1].Cuenta.FechaVencimientoTag := IncMonth( Vehiculos[length(Vehiculos)-1].Cuenta.FechaCreacion, (12 * AniosVencimientoGarantiaTelevia  ))
                            else
                                Vehiculos[length(Vehiculos)-1].Cuenta.FechaVencimientoTag := IncMonth( NowBase(DMConnections.BaseCAC), (12 * AniosVencimientoGarantiaTelevia  ))
                        end else begin     // CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA
                            Vehiculos[length(Vehiculos)-1].Cuenta.FechaVencimientoTag := QueryGetValueDateTime(DMConnections.BaseCAC,
                              format('select dbo.ObtenerVencimientoGarantiaTag(%d,%10.0f)',[FieldByName('ContextMark').AsInteger, FieldByName('ContractSerialNumber').AsFloat]));
                        end;
                        Break;
                    end;
                    cdsIndemnizaciones.Next;
                end;
                Next;
            end;
            First;
        end;

	if TipoImpresion = tiConvenio then begin

        result := f.ImprimirConvenio(NumeroConvenio,
                    tiAlta,
					FreDatoPersona.RegistrodeDatos,
                    FreTelefonoPrincipal.MedioComunicacion,
                    FreTelefonoSecundario.MedioComunicacion,
                    FMDomicilioPrincipal.RegDatosDomicilio,
                    DomicilioAlternativo,
                    DatosMedioPago,
					Vehiculos,
                    RepresentateLegal1,
                    RepresentateLegal2,
                    RepresentateLegal3,
                    FDocumentacionPresentadaPorOS[0].Documentacion,
                    FRegDatoMandante,
                    TformMedioEnvioDocumentosCobro.EnvioDocumentoCobroViaMail(RegMedioEnvioCorreo,true),
                    iif(FreDatoPersona.Personeria=PERSONERIA_JURIDICA,txtEMailContacto.Text,txtEmailParticular.Text), UsuarioSistema, ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,PuntoEntrega),
                    FechaAlta,
                    MotivoCanelacion,
                    PuntoEntrega,
                    PuntoVenta);
    end
	else
		result:=F.ImprimirEtiquetas(NumeroConvenio,FreDatoPersona.RegistrodeDatos,Vehiculos, MotivoCancelacion);


    IndiceLista := 0;
	Result := True;
	while Result and (IndiceLista < ListaAnexo3.Count) do begin

        if FreDatoPersona.RegistrodeDatos.Personeria = PERSONERIA_JURIDICA then begin
            DatosAnexo3.APaterno := RepresentateLegal1.Apellido;
            DatosAnexo3.Amaterno := RepresentateLegal1.ApellidoMaterno;
            DatosAnexo3.Nombre	 := RepresentateLegal1.Nombre;
            DatosAnexo3.Cedula	 := RepresentateLegal1.NumeroDocumento;

            DatosAnexo3.RazonSocial := FreDatoPersona.RegistrodeDatos.RazonSocial;         // SS_1171_EBA_20140311
            DatosAnexo3.RutRazonSocial := FreDatoPersona.RegistrodeDatos.NumeroDocumento;  // SS_1171_EBA_20140311
        end
        else begin
            DatosAnexo3.APaterno := FreDatoPersona.RegistrodeDatos.Apellido;
            DatosAnexo3.Amaterno := FreDatoPersona.RegistrodeDatos.ApellidoMaterno;
            DatosAnexo3.Nombre	 := FreDatoPersona.RegistrodeDatos.Nombre;
            DatosAnexo3.Cedula	 := FreDatoPersona.RegistrodeDatos.NumeroDocumento;
        end;

        DatosAnexo3.Televia			:= ListaAnexo3[IndiceLista];
        DatosAnexo3.DomicilioCalle	:= FMDomicilioPrincipal.RegDatosDomicilio.Descripcion;
        DatosAnexo3.DomicilioNumero	:= FMDomicilioPrincipal.RegDatosDomicilio.NumeroCalleSTR;
        DatosAnexo3.DomicilioComuna	:= BuscarDescripcionComuna(	DMConnections.BaseCAC,	FMDomicilioPrincipal.RegDatosDomicilio.CodigoPais,
	                		                                                            FMDomicilioPrincipal.RegDatosDomicilio.CodigoRegion,
	                        		                                                    FMDomicilioPrincipal.RegDatosDomicilio.CodigoComuna);
		//Result := f.ImprimirAnexo3o4(3, FImprimirAnexos3y4 > 3, DatosAnexo3, DatosAnexo4);											// SS_1171_EBA_20140311
        Result := f.ImprimirAnexo3o4(3, FImprimirAnexos3y4 > 3, DatosAnexo3, DatosAnexo4, FreDatoPersona.RegistrodeDatos.Personeria);	// SS_1171_EBA_20140311

		Inc(IndiceLista);
    end;

    //El anexo IV se imprime una sola vez
    if FImprimirAnexos3y4 > 3 then begin
        if FreDatoPersona.RegistrodeDatos.Personeria = PERSONERIA_JURIDICA then begin
        	DatosAnexo3.APaterno	:= FreDatoPersona.RegistrodeDatos.RazonSocial;
            DatosAnexo4.RepAPaterno	:= RepresentateLegal1.Apellido;
            DatosAnexo4.RepAmaterno	:= RepresentateLegal1.ApellidoMaterno;
            DatosAnexo4.RepNombre	:= RepresentateLegal1.Nombre;
            DatosAnexo4.RepCedula	:= RepresentateLegal1.NumeroDocumento;
        	DatosAnexo4.EMail		:= txtEMailContacto.Text;
            DatosAnexo4.CliCedula   := FreDatoPersona.txtDocumento.Text;	// SS_948_PDO_20120704
        end
        else begin
        	DatosAnexo4.RepAPaterno := '';
            DatosAnexo4.EMail := txtEmailParticular.Text;
        end;

    	F.ImprimirAnexo3o4(4, True, DatosAnexo3, DatosAnexo4);
    end;

    ListaAnexo3.Free;



    f.Release;
end;
}
{TERMINO:TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)}

{-----------------------------------------------------------------------------
Function Name   : ObtenerDatosPersonas
Author          :
Date Created    :
Description     : Permite obtener set de datos personas para ser usadas en varables.
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.ObtenerDatosPersonas(
  CodigoPersona: Integer; var RegistroPersona: TDatosPersonales; TipoDocumento:String='';NumeroDocumento:String=''): Boolean;
resourcestring
    MSG_QUERY_DATA_ERROR     = 'Error al obtener datos personas';
    MSG_QUERY_DATA_ERROR_RUT = 'Error al obtener datos personas RUT:';
var
    sp:TADOStoredProc;
begin
	sp:=TADOStoredProc.Create(nil);
    try
        try
            result:=False;
            sp.Connection:=DMConnections.BaseCAC;
            sp.ProcedureName:='ObtenerDatosPersona';
            with sp.Parameters do begin
                Refresh;
                if trim(NumeroDocumento)<>'' then ParamByName('@CodigoPersona').Value:= null
                else ParamByName('@CodigoPersona').Value:=CodigoPersona;

                if Trim(TipoDocumento)='' then ParamByName('@CodigoDocumento').Value:=null
                else ParamByName('@CodigoDocumento').Value:=TipoDocumento;

                if trim(NumeroDocumento)='' then ParamByName('@NumeroDocumento').Value:=null
        				else ParamByName('@NumeroDocumento').Value:=NumeroDocumento;
            end;
            sp.open;
            if sp.RecordCount>0 then begin
                with RegistroPersona do begin
                    Personeria:=(Sp.FieldByName('Personeria').AsString+' ')[1];
					          if Personeria=PERSONERIA_JURIDICA then
                    begin
                      RazonSocial:=Trim(Sp.FieldByName('Apellido').AsString);
                      Apellido:=Trim(Sp.FieldByName('ApellidoContactoComercial').AsString);
                      ApellidoMaterno:=Trim(Sp.FieldByName('ApellidoMaternoContactoComercial').AsString);
						          Nombre:=Trim(Sp.FieldByName('NombreContactoComercial').AsString);
                      Sexo:=(Sp.FieldByName('SexoContactoComercial').AsString+' ')[1];
                      Giro:=Trim(Sp.FieldByName('Giro').AsString);
                    end
                    else
                    begin
                      RazonSocial:='';
                      Apellido:=Trim(Sp.FieldByName('Apellido').AsString);
                      ApellidoMaterno:=Trim(Sp.FieldByName('ApellidoMaterno').AsString);
                      Nombre:=Trim(Sp.FieldByName('Nombre').AsString);
                      sexo:=(Sp.FieldByName('Sexo').AsString+' ')[1];
                    end;

                    CodigoPersona:=Sp.FieldByName('CodigoPersona').AsInteger;
					          TipoDocumento:=Trim(sp.FieldByName('CodigoDocumento').Value);
	    	            NumeroDocumento:=Trim(sp.FieldByName('NumeroDocumento').Value);
                    AdheridoPA:=sp.FieldByName('AdheridoPA').AsBoolean;                     //SS-1006-NDR-20120715
                    if sp.FieldByName('FechaNacimiento').IsNull then
                      FechaNacimiento:=NullDate
                    else
                      FechaNacimiento:=sp.FieldByName('FechaNacimiento').AsDateTime;

					          CodigoActividad:=iif(sp.FieldByName('CodigoActividad').IsNull,-1,sp.FieldByName('CodigoActividad').AsInteger);

                    LugarNacimiento:=Trim(Sp.FieldByName('LugarNacimiento').AsString);

                    if sp.FieldByName('Password').IsNull then
                      Password:=''
                    else
                      Password:=Trim(sp.FieldByName('Password').Value);

                    if sp.FieldByName('Pregunta').IsNull then
                      Pregunta:=''
                    else
                      Trim(sp.FieldByName('Pregunta').AsString);

					          if sp.FieldByName('Respuesta').IsNull then
                      Respuesta:=''
					          else
                      Respuesta:=Trim(sp.FieldByName('Respuesta').AsString);

                    if not sp.FieldByName('TipoCliente').IsNull then
                        TipoCliente := sp.FieldByName('TipoCliente').AsInteger;
                    // SS_690_20100902
                    if not sp.FieldByName('Semaforo1').IsNull then
                        Semaforo1 := sp.FieldByName('Semaforo1').AsInteger;

                    if not sp.FieldByName('Semaforo3').IsNull then
                        Semaforo3 := sp.FieldByName('Semaforo3').AsInteger;
                    // Fin SS_690_20100902
                    result:=True;
                end;
            end else begin
				result:=False;
                TFreDatoPresona.LimpiarRegistro(RegistroPersona);
            end;
        except
            on E: exception do begin
                MsgBoxErr( MSG_QUERY_DATA_ERROR, e.message, MSG_QUERY_DATA_ERROR_RUT + NumeroDocumento, MB_ICONSTOP);
				Result := False;
            end;
        end;
    finally
        sp.close;
        sp.Free; //TASK_013_ECA_20160520
    end;

end;

{-----------------------------------------------------------------------------
Procedure Name  : ObtenerMedioComunicacion
Author          :
Date Created    :
Description     : Permite obtener atributos de Medios de Comunicaci�n asociados
                a personas.
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.ObtenerMedioComunicacion(CodigoPersona:Integer;var MediosComunicacion:TMediosComunicacion):boolean;
var
    sp:TADOStoredProc;
    i:Integer;
begin
    sp:=TADOStoredProc.create(nil);
    try
        try
            sp.Connection:=DMConnections.BaseCAC;
			sp.ProcedureName:='ObtenerMediosComunicacionPersona';

            with sp.Parameters do begin
                Refresh;
                ParamByName('@CodigoPersona').Value:=CodigoPersona;
                ParamByName('@CodigoTipoMedioContacto').Value:=null;
			end;
            sp.Open;

            SetLength(MediosComunicacion,0);
            if (sp.RecordCount>0)  then begin
                SetLength(MediosComunicacion,sp.RecordCount);

                sp.First;
                i:=0;
				while not(sp.Eof) do begin
                    MediosComunicacion[i].CodigoArea := sp.FieldByName('CodigoArea').AsInteger;
                    MediosComunicacion[i].Valor := trim(sp.FieldByName('Valor').AsString);
                    MediosComunicacion[i].CodigoTipoMedioContacto := sp.FieldByName('CodigoTipoMedioContacto').AsInteger;
                    MediosComunicacion[i].Anexo := sp.FieldByName('Anexo').AsInteger;
                    MediosComunicacion[i].HoraDesde := sp.FieldByName('HorarioDesde').AsDateTime;
                    MediosComunicacion[i].HoraHasta := sp.FieldByName('HorarioHasta').AsDateTime;
                    MediosComunicacion[i].Indice := sp.FieldByName('CodigoMedioComunicacion').AsInteger;
                    inc(i);
                    sp.Next;
                end;
            end;

            sp.close;
			result:=True;
        except
			on E: exception do begin
                MsgBoxErr( Caption, e.message, Caption, MB_ICONSTOP);
				result:=False;
            end;
        end;
    finally
        SP.free;  
    end;

end;

{-----------------------------------------------------------------------------
Procedure Name  : ManejarPersonaSolicitud
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.ManejarPersonaSolicitud(TipoDocumento,NumeroDocumento:String);
var
    SP:TADOStoredProc;
    IndiceContacto: integer;
    f: TformSeleccionSolicitudDuplicada;

begin
    SP:=TADOStoredProc.Create(nil);
    try
        sp.Connection:=DMConnections.BaseCAC;

        // valido si esta en algun convenio
		//Controlo que no tenga un convenio cargado
        if TieneConvenio(NumeroDocumento,TipoDocumento, False, True) then begin
            MsgBox(MSG_ERROR_ALTA_RUT_CONVENIO,Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP);
            exit;
        end;

        IndiceContacto := StrtoInt(QueryGetValue(DMConnections.BaseCAC, 'select dbo.ExisteContacto(''RUT'','''
                          + DatoPersona.NumeroDocumento + ''')'));

        if IndiceContacto > 0 then begin

            Application.CreateForm(TformSeleccionSolicitudDuplicada, f);
            if (f.inicializar('', DatoPersona.NumeroDocumento, True, FTipoSolicitud = SOLICITUDCONTACTO)) and (f.ShowModal = mrOk) then begin
				FreDatoPersona.txtDocumento.Enabled := False;
                HabilitarControles;
				if f.CodigoSolicitud = 0 then begin
                    // Hizo click en Solicitud Nueva
                    FCodigoSolicitud := -1;
                    EsNueva := True;

                    if ObtenerDatosPersonas(-1,DatoPersona,TIPO_DOCUMENTO_RUT,FreDatoPersona.RegistrodeDatos.NumeroDocumento) then
						FreDatoPersona.RegistrodeDatos:=DatoPersona;
                end else begin //Selecciono una solicitud y hay que cargarla
                    CargarSolicitudExistente(f.CodigoSolicitud,IndiceContacto);
                end;

                btn_EditarPagoAutomatico.Enabled:=(cb_PagoAutomatico.ItemIndex > 1);
                if not(PuedeEditarSolicitud) and (btn_EditarPagoAutomatico.Enabled) then // es una solicitud que puede ser modificada, pero tiene medio de pagoa ser mostrada
                        btn_EditarPagoAutomatico.Caption:=CAPTION_MEDIO_PAGO_VER
                else btn_EditarPagoAutomatico.Caption:=CAPTION_EDITAR_PAGO;

                if FreDatoPersona.cbPersoneria.Enabled then begin
                    if FreDatoPersona.RegistrodeDatos.Personeria=PERSONERIA_JURIDICA then FreDatoPersona.txtRazonSocial.SetFocus
                    else FreDatoPersona.txt_Nombre.SetFocus;
                end;
            end else
				if f.ModalResult = mrYes then begin
                    MsgBox(MSG_CAPTION_LLAMADA_REGISTRADA, self.Caption, MB_ICONINFORMATION);
                    Close;
				end else FreDatoPersona.txtDocumento.Clear;
            f.Release;

		end else begin
            FIndicePersona := -1;
            FCodigoSolicitud := -1;
            EsNueva := True;
            HabilitarControles;
            FreDatoPersona.txtDocumento.Enabled := true;
            FreDatoPersona.lblRutRun.enabled := true;
            if Self.Visible then begin
                FreDatoPersona.txtDocumento.SetFocus;
                FreDatoPersona.txtDocumento.SelStart:=length(FreDatoPersona.txtDocumento.Text);
                FreDatoPersona.txtDocumento.SelLength:=0;
            end;
        end;
    finally
         if sp <> nil then begin
            sp.close;
			sp.free;  //TASK_013_ECA_20160520
         end;
    end;

end;

{-----------------------------------------------------------------------------
Function Name   : TieneConvenio
Author          :
Date Created    :
Description     : Retorna Verdadero o Falso si un rut tiene o no primer Convenio.
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.TieneConvenio(NumeroDocumento,
  TipoDocumento: String; SoloActivos : Boolean = False; SoloConstanera: Boolean = False): Boolean;
begin
    Result := DarPrimerConvenio(NumeroDocumento, TipoDocumento, SoloActivos, SoloConstanera) <> '';
end;

{-----------------------------------------------------------------------------
Function Name   : ObtenerDomicilioPersonas
Author          : dcalani
Date Created    : 14/01/2005
Description     : Devuelve el Codigo del Primer Convenio del rut pasado como parametro
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.DarPrimerConvenio(NumeroDocumento,TipoDocumento:String; SoloActivos: Boolean = False; SoloConstanera: Boolean = False): String;
var
    sp:TADOStoredProc;
begin
    Result := '';
    sp:=TADOStoredProc.Create(nil);
    try
		sp.Connection:=DMConnections.BaseCAC;
        sp.ProcedureName:='ObtenerConvenios';
        with sp.Parameters do begin
            Refresh;
            ParamByName('@CodigoDocumento').Value := TipoDocumento;
            ParamByName('@NumeroDocumento').Value := NumeroDocumento;
            ParamByName('@SoloActivos').Value := iif(SoloActivos, 1 ,0);
			ParamByName('@SoloContanera').Value := iif(SoloConstanera, 1, 0);
        end;
        sp.Open;

        if sp.RecordCount > 0 then Result := sp.FieldByName('NumeroConvenio').AsString;

	finally
		sp.close;
		sp.Destroy;
	end;

end;

{-----------------------------------------------------------------------------
Function Name	: ObtenerDomicilioPersonas
Author          :
Date Created    :
Description     : Devuelve Verdadero o Falso si una persona tiene Domicilio(s).
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.ObtenerDomicilioPersonas(
  CodigoPersona: Integer; var RegistroDomicilio: TDomicilios;
  Principal:Boolean=False):Boolean;
var
	sp:TADOStoredProc;
begin
	sp:=TADOStoredProc.Create(nil);
	try
		try
			sp.Connection:=DMConnections.BaseCAC;
			sp.ProcedureName:='ObtenerDomiciliosPersona';
			with sp.Parameters do begin
				Refresh;
				ParamByName('@CodigoPersona').Value:=CodigoPersona;
				ParamByName('@CodigoDomicilio').Value:=Null;
				ParamByName('@CodigoTipoDomicilio').Value:=Null;
				ParamByName('@Principal').Value:=iif(Principal,1,null);
			end;
			sp.open;
			SetLength(RegistroDomicilio,0);
			while not(sp.Eof) do begin
				SetLength(RegistroDomicilio,length(RegistroDomicilio)+1);
				with RegistroDomicilio[Length(RegistroDomicilio)-1] do begin
					CodigoDomicilio:=sp.FieldByName('CodigoDomicilio').AsInteger;
					Descripcion:=sp.FieldByName('DescriCalle').AsString;
					CodigoTipoDomicilio:=sp.FieldByName('CodigoTipoDomicilio').AsInteger;
					CodigoCalle:=sp.FieldByName('CodigoCalle').AsInteger;
					CodigoSegmento:=sp.FieldByName('CodigoSegmento').AsInteger;
					NumeroCalle:=FormatearNumeroCalle(sp.FieldByName('Numero').AsString);
					NumeroCalleSTR:=sp.FieldByName('Numero').AsString;
					Detalle:=sp.FieldByName('Detalle').AsString;
					CodigoPostal:=sp.FieldByName('CodigoPostal').AsString;
					CodigoPais:=sp.FieldByName('CodigoPais').AsString;
					CodigoRegion:=sp.FieldByName('CodigoRegion').AsString;
					CodigoComuna:=sp.FieldByName('CodigoComuna').AsString;
					CalleDesnormalizada:=sp.FieldByName('CalleDesnormalizada').AsString;
					Normalizado:=trim(sp.FieldByName('CalleDesnormalizada').AsString)='';
				end;
				sp.Next;
			end;

			result:=True;
		except
			on E: exception do begin
				MsgBoxErr(Caption,e.Message,Caption,MB_ICONSTOP);
				result:=False;
			end;
		end;
	finally
		sp.close;
		sp.Destroy;
	end;
end;

{-----------------------------------------------------------------------------
Procedure Name	: EjecutarSP
Author          :
Date Created    :
Description     : Cantidad de intentos por defecto para ejecutar un Procedimiento
                Almacenado.
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.EjecutarSP(
  StoredProc: TADOStoredProc;Var Mensaje:String; CantidadIntentos:Integer=3):Boolean;
var
	MensajeAux,MensajeAuxPasado:String;
begin
	if CantidadIntentos=0 then begin
		result:=false;
		Mensaje:='';
		exit;
	end;

	try
	  StoredProc.ExecProc;
	  result:=True;
	except
		on E: exception do begin
            MensajeAux:=e.Message;
            if pos('DEAD',UpperCase(MensajeAux))>0 then begin //deadlock
                result:=EjecutarSP(StoredProc,MensajeAuxPasado,CantidadIntentos-1);
                if not(result) then begin
                    if MensajeAuxPasado='' then Mensaje:=MensajeAux
                    else Mensaje:=MensajeAuxPasado;
                end;
            end else begin //otro error
                result:=False;
				Mensaje:=MensajeAux;
            end;
        end;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name	: txtEMailContactoChange
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.txtEMailContactoChange(Sender: TObject);
begin
	if ((TEdit(sender) = txtEMailContacto) and (FreDatoPersona.Personeria=PERSONERIA_JURIDICA)) or
		((TEdit(sender) = txtEmailParticular) and (FreDatoPersona.Personeria=PERSONERIA_FISICA)) then begin
       if (trim(TEdit(Sender).Text)='') then begin
			if MedioDocControlada then
				if TformMedioEnvioDocumentosCobro.EnvioDocumentoCobroViaMail(RegMedioEnvioCorreo) then MedioDocControlada:=False;
        end;
	end;
    cb_RecInfoMail.Enabled := Trim(TEdit(Sender).Text) <> '';
    if not cb_RecInfoMail.Enabled then cb_RecInfoMail.Checked := False;
end;

{-----------------------------------------------------------------------------
Procedure Name	: LimpiarDocumentacion
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.LimpiarDocumentacion;
begin
	SetLength(FDocumentacionPresentada,1);
    SetLength(FDocumentacionPresentadaPorOS,1);
    FDocumentacionPresentadaPorOS[0].CodigoTipoOrdenServicio := 1; ///**** FIXME cambiar por constante
end;

{-----------------------------------------------------------------------------
Procedure Name	: btn_BlanquearClaveClick
Author          :
Date Created    :
Description     : Permite obtener otra clave.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_BlanquearClaveClick(Sender: TObject);
begin
    if MsgBox(MSG_QUETION_BLANQUEAR_CLAVE,Caption,MB_ICONQUESTION or MB_YESNO) = IDYES then begin
        try
            with BlanquearPasswordPersona do begin
                Parameters.ParamByName('@CodigoDocumento').Value := FreDatoPersona.RegistrodeDatos.TipoDocumento;
                Parameters.ParamByName('@NumeroDocumento').Value := FreDatoPersona.RegistrodeDatos.NumeroDocumento;
                Parameters.ParamByName('@Password').Value        := ''; // es un parametro de salida
                ExecProc;
                MsgBox(format(MSG_CAPTION_BLANQUEAR_CLAVE,[Trim(Parameters.ParamByName('@Password').Value)]),Caption,MB_ICONINFORMATION);
            end;
        Except
            on e:Exception do MsgBoxErr(MSG_ERROR_BLANQUEAR_CLAVE,e.Message,Caption,MB_ICONSTOP);
        end;
        BlanquearPasswordPersona.close;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : cb_ObservacionesClick
Author          :
Date Created    :
Description     : Valida condici�n para checkbox Observaciones al hacer click.
-----------------------------------------------------------------------------}



procedure TFormSolicitudContacto.cb_ObservacionesClick(Sender: TObject);
begin
    //btn_VerObservaciones.Enabled := cb_Observaciones.Checked;
    if Self.Visible then begin
		//if btn_VerObservaciones.Enabled then btn_VerObservaciones.Click       //TASK_065_GLE_20170408
        //else FObservaciones := '';                                            //TASK_065_GLE_20170408
        FObservaciones := '';                                                   //TASK_065_GLE_20170408
    end;

end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_VerObservacionesClick
Author          :
Date Created    :
Description     : despliega pantalla con Lista de Observaciones.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_VerObservacionesClick(
  Sender: TObject);
var
    f : TFormCheckListConvenio;
begin
    Application.CreateForm(TFormCheckListConvenio, f);
    if f.Inicializa(ModoPantalla, FCheckListConvenio, FListObservacionConvenio, nil) then f.ShowModal;

    FListObservacionConvenio := f.ListaObservacion;
	FCheckListConvenio := f.ListaCheck;

    //cb_Observaciones.Checked := (FListObservacionConvenio.Count > 0) or FCheckListConvenio.TieneMarcados;     //TASK_065_GLE_20170408
    //btn_VerObservaciones.Enabled := cb_Observaciones.Checked;                                                 //TASK_065_GLE_20170408

    f.Free;
end;

{-----------------------------------------------------------------------------
Procedure Name  : cdsVehiculosContactoAfterScroll
Author          :
Date Created    :
Description     : Se van refrescando botones.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.cdsVehiculosContactoAfterScroll(
  DataSet: TDataSet);
var
 valor : Integer; //TASK_033_ECA_20160610
begin
    //INICIO: TASK_033_ECA_20160610
    Valor:= Integer(cbbTipoConvenio.Value);
    if (Valor<>CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) and (valor<>CODIGO_TIPO_CONVENIO_INFRACTOR) then
    begin
        btnEditarVehiculo.Enabled           := (dblVehiculos.DataSource.DataSet.Active) and (dblVehiculos.DataSource.DataSet.RecordCount > 0);// and (dblVehiculos.DataSource.DataSet.FieldByName('CodigoConcesionaria').Value = 0);
        btn_AsignarTag.Enabled              := btnEditarVehiculo.Enabled;
        btnEliminarVehiculo.Enabled         := btnEditarVehiculo.Enabled;
        btn_EximirFacturacion.Enabled       := btnEditarVehiculo.Enabled;
        btn_BonificarFacturacion.Enabled    := btnEditarVehiculo.Enabled;
    end;
    //FIN: TASK_033_ECA_20160610
end;

{-----------------------------------------------------------------------------
Function Name   : EstaAsociadoaAlgunConvenioNativo
Author          :
Date Created    :
Description     : Devuelve Verdadero o Falso si existe algun convenio Asociado.
-----------------------------------------------------------------------------}
//function TFormSolicitudContacto.EstaAsociadoaAlgunConvenioCN(                 //SS_1147_MCA_20140408
function TFormSolicitudContacto.EstaAsociadoaAlgunConvenioNativo(               //SS_1147_MCA_20140408
  NumeroDocumento, TipoDocumento: String): boolean;
var
	sp:TADOStoredProc;
begin
    sp:=TADOStoredProc.Create(nil);
    try
        sp.Connection:=DMConnections.BaseCAC;
        //sp.ProcedureName:='AsociadoConvenioCN';                                 //SS_1147_MCA_20140408
        sp.ProcedureName:='AsociadoConvenioNativo';                                 //SS_1147_MCA_20140408
		with sp.Parameters do begin
            Refresh;
            ParamByName('@CodigoDocumento').Value := TipoDocumento;
            ParamByName('@NumeroDocumento').Value := NumeroDocumento;
            ParamByName('@Asociado').Value := NULL;
        end;
        sp.ExecProc;
        Result := sp.Parameters.ParamByName('@Asociado').Value;
	finally
		sp.close;
		sp.Destroy;
	end;

end;

{-----------------------------------------------------------------------------
Function Name   : TieneSolicitud
Author          :
Date Created    :
Description     : Devuelve Verdadero o Falso si Rut Tiene Solicitud.
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.TieneSolicitud(NumeroDocumento,
  TipoDocumento: String): Boolean;

var
    sp:TADOStoredProc;
begin
	sp:=TADOStoredProc.Create(nil);
    try
        Result := False;
		sp.Connection:=DMConnections.BaseCAC;
        sp.ProcedureName:='ObtenerSolicitudContacto';
        with sp.Parameters do begin
            Refresh;
            ParamByName('@FechaHoraCreacion').Value := Null;
            ParamByName('@FechaHoraFin').Value := Null;
            ParamByName('@CodigoFuenteSolicitud').Value := Null;
            ParamByName('@CodigoEstadoSolicitud').Value := Null;
            ParamByName('@CantidadVehiculos').Value := Null;
            ParamByName('@Personeria').Value := Null;
            ParamByName('@RUT').Value := NumeroDocumento;
            ParamByName('@Apellido').Value := Null;
			ParamByName('@Patente').Value := Null;
            ParamByName('@TOP').Value := Null;
            ParamByName('@OrderBy').Value := Null;
        end;
        sp.Open;
        Result := sp.RecordCount > 0;
	finally
		sp.close;
		sp.Destroy;
	end;
end;

{-----------------------------------------------------------------------------
Procedure Name   : mnu_EnviarMailClick
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.mnu_EnviarMailClick(Sender: TObject);
begin
    if not InvocarCorreoPredeterminado(TEdit(PopupMenu.PopupComponent), TEdit(PopupMenu.PopupComponent).Text, '', '') then begin
        MsgBoxErr(MSG_ERROR_INVOCAR_CORREO_PREDETERMINADO, MSG_ERROR_DETALLE_INVOCAR_CORREO_PREDETERMINADO, MSG_CAPTION_ENVIO, MB_ICONERROR);
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : PopupMenuPopup
Author          :
Date Created    :
Description     : Habilita Men� Enviar Mail seg�n condici�n.
-----------------------------------------------------------------------------}

procedure TFormSolicitudContacto.PopupMenuPopup(Sender: TObject);
begin
	 mnu_EnviarMail.Enabled := (Trim(TEdit(TPopupMenu(Sender).PopupComponent).Text) <> '') and IsValidEMail(Trim(TEdit(TPopupMenu(Sender).PopupComponent).Text));
end;

{-----------------------------------------------------------------------------
Procedure Name  : lblEnviarMouseEnter
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.lblEnviarMouseEnter(Sender: TObject);
begin
    if NBook_Email.PageIndex = 0 then begin
		if ((Trim(txtEMailContacto.Text) = '') or not IsValidEMail(Trim(txtEMailContacto.Text))) then Exit;
    end
    else
        if ((Trim(txtEmailParticular.Text) = '') or not IsValidEMail(Trim(txtEmailParticular.Text))) then Exit;

    lblEnviar.Cursor := crHandPoint;
    lblEnviar.Font.Style := lblEnviar.Font.Style + [fsUnderline];
    lblEnviar.Font.Color := clblue;
end;

{-----------------------------------------------------------------------------
Procedure Name   : lblEnviarMouseLeave
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.lblEnviarMouseLeave(Sender: TObject);
begin
    lblEnviar.Cursor := crDefault;
    lblEnviar.Font.Style := lblEnviar.Font.Style - [fsUnderline];
    lblEnviar.Font.Color := clBlack;
end;

{-----------------------------------------------------------------------------
Procedure Name   : lblEnviarClick
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.lblEnviarClick(Sender: TObject);
begin
    if lblEnviar.Cursor = crHandPoint then begin
        if NBook_Email.PageIndex = 0 then begin
			if not InvocarCorreoPredeterminado(txtEMailContacto, txtEMailContacto.Text, '', '') then begin
                MsgBoxErr(MSG_ERROR_INVOCAR_CORREO_PREDETERMINADO, MSG_ERROR_DETALLE_INVOCAR_CORREO_PREDETERMINADO, MSG_CAPTION_ENVIO, MB_ICONERROR);
			end;
        end
        else begin
            if not InvocarCorreoPredeterminado(txtEmailParticular, txtEmailParticular.Text, '', '') then begin
                MsgBoxErr(MSG_ERROR_INVOCAR_CORREO_PREDETERMINADO, MSG_ERROR_DETALLE_INVOCAR_CORREO_PREDETERMINADO, MSG_CAPTION_ENVIO, MB_ICONERROR);
            end;
        end;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name   : ObtenerDatosRepresentantes
Author          :
Date Created    :
Description     :  Obtener atributos desde Base de Datos de Representantes Legales.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.ObtenerDatosRepresentantes (CodigoPersona: LongInt);
resourcestring
	MSG_ERROR_LEER_REPRESENTANTES = 'No puedieron leerse los datos de los representantes legales';
begin
	try
		with spObtenerRepresentantesUltimoConvenio, Parameters do begin
			ParamByName ('@CodigoPersonaConvenio').Value := CodigoPersona;
			Open;

			if not eof then begin
				ObtenerDatosPersonas(FieldByName ('CodigoPersona').AsInteger, RepresentateLegal1, '', '');
				Next
			end;

			if not eof then begin
				ObtenerDatosPersonas(FieldByName ('CodigoPersona').AsInteger, RepresentateLegal2, '', '');
				Next
			end;

			if not eof then ObtenerDatosPersonas(FieldByName ('CodigoPersona').AsInteger, RepresentateLegal3, '', '');

			FEditarRepresentateLegal1 := RepresentateLegal1.CodigoPersona <= 0;
			FEditarRepresentateLegal2 := RepresentateLegal2.CodigoPersona <= 0;
			FEditarRepresentateLegal3 := RepresentateLegal3.CodigoPersona <= 0;

			lbl_RepresentanteLegal.Caption:=ArmarNombrePersona(RepresentateLegal1.Personeria,RepresentateLegal1.Nombre,RepresentateLegal1.Apellido,RepresentateLegal1.ApellidoMaterno)+iif(trim(RepresentateLegal2.Nombre)<>'',' y otro/s','');

			RepLegal:= (RepresentateLegal1.CodigoPersona > 0) or
				(RepresentateLegal2.CodigoPersona > 0) or
				(RepresentateLegal3.CodigoPersona > 0);

			Close
		end
	except
		on E: exception do begin
			MsgBoxErr (MSG_ERROR_LEER_REPRESENTANTES, e.Message, self.Caption, MB_ICONSTOP)
		end
	end
end;


{-----------------------------------------------------------------------------
Function Name   : ValidarDatosRepresentates
Author          :
Date Created    : 16/03/2005
Description     : Valida que existan los datos minimos de los RepLegales
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.ValidarDatosRepresentates: Boolean;

	function DatosValidosRepresentante (RepresentateLegal : TDatosPersonales) : Boolean;
	var
		validacionRUT:Integer;
	begin
		validacionRUT:=-1;
		if trim(RepresentateLegal.NumeroDocumento) <> '' then begin
			RepresentateLegal.NumeroDocumento := PadL(trim(RepresentateLegal.NumeroDocumento), 9, '0');
			validacionRUT:=QueryGetValueInt(DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
				  StrLeft(RepresentateLegal.NumeroDocumento, (Length(RepresentateLegal.NumeroDocumento) -1))  +
				  ''',''' +
				  RepresentateLegal.NumeroDocumento[Length(RepresentateLegal.NumeroDocumento)] + '''');
		end;

		result:=(trim(RepresentateLegal.NumeroDocumento)<>'') and
				(validacionRUT = 1) and
				(Trim (RepresentateLegal.Nombre) <> '') and
				(Trim (RepresentateLegal.Apellido) <> '') and
				(Trim (RepresentateLegal.Sexo) <> '')
	end;

begin
	Result :=   (RepresentateLegal1.CodigoPersona > 0) and DatosValidosRepresentante (RepresentateLegal1) and
				((RepresentateLegal2.CodigoPersona < 1) or
					((RepresentateLegal2.CodigoPersona > 0) and (DatosValidosRepresentante (RepresentateLegal2)))) and
				((RepresentateLegal3.CodigoPersona < 1) or
					((RepresentateLegal3.CodigoPersona > 0) and (DatosValidosRepresentante (RepresentateLegal3))))
end;

{-----------------------------------------------------------------------------
Procedure Name  : dblVehiculosColumnsHeaderClick
Author          :
Date            :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.dblVehiculosColumnsHeaderClick(
  Sender: TObject);
var
    i: Integer;
begin
    if not cdsVehiculosContacto.Active then
        Exit;

    for i := 0 to dblVehiculos.Columns.Count-1 do
        if dblVehiculos.Columns[i] <> Sender then
            dblVehiculos.Columns[i].Sorting := csNone;

    if TDBListExColumn(Sender).Sorting = csAscending then
        TDBListExColumn(Sender).Sorting := csDescending
    else
        TDBListExColumn(Sender).Sorting := csAscending;

    with cdsVehiculosContacto do begin
        DisableControls;
        try
            if IndexName <> EmptyStr then
                DeleteIndex('xOrder');
            AddIndex('xOrder', TDBListExColumn(Sender).FieldName, [],
                     iif(TDBListExColumn(Sender).Sorting = csDescending, TDBListExColumn(Sender).FieldName, EmptyStr));
            IndexName := 'xOrder';
        finally
            EnableControls;
        end;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_EximirFacturacionClick
Author          :
Date            :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_EximirFacturacionClick(
  Sender: TObject);
resourcestring
    MSG_ERROR_EXIMIR_FACTURACION = 'No se pudieron modificar los datos del veh�culo';
var
    f: TFormNoFacturarTransitosDeCuenta;
    CodigoConvenio, IndiceVehiculo: Integer; //ORIGINAL

    EximirFacturacion: TEximirFacturacion;
    BonificarFacturacion: TBonificarFacturacion;
    FilaVehiculo: Integer;
begin
    if dblVehiculos.DataSource.DataSet.RecordCount = 0 then Exit;

    Application.CreateForm(TFormNoFacturarTransitosDeCuenta, f);
    try
        (* Armar las estructuras para pasar como par�metro al form de eximir. *)
        CodigoConvenio := -1;
        IndiceVehiculo := -1;
        FilaVehiculo := cdsVehiculosContacto.RecNo;
        ArmarEximirFacturacion(FilaVehiculo, EximirFacturacion);
        ArmarBonificarFacturacion(FilaVehiculo, BonificarFacturacion);

        if f.Inicializar(ModoPantalla,
                CodigoConvenio,
                IndiceVehiculo,
                EximirFacturacion,
                BonificarFacturacion,
                Trim(cdsVehiculosContacto.FieldByName('Patente').AsString),
                Trim(cdsVehiculosContacto.FieldByName('DigitoVerificadorPatente').AsString),
                cdsVehiculosContacto.FieldByName('SerialNumberaMostrar').AsString)
                and (f.ShowModal = mrOK) then begin

            try
                (* Obtener los datos modificados. *)
                // Auditoria Rev 11
                if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, 0, cdsVehiculosContacto.FieldByName('Patente').AsString,
                  cdsVehiculosContacto.FieldByName('IndiceVehiculo').AsInteger, cdsVehiculosContacto.FieldByName('SerialNumberaMostrar').AsString,
                  FPuntoEntrega, UsuarioSistema, 'Eximir')  then
                    raise Exception.create('Error registrando auditoria');

                EximirFacturacion := f.GetEximirFacturacion;
                (* Actualizar los datos del vehiculo. *)
                ActualizarEximicionFacturacion(FilaVehiculo, EximirFacturacion);

            except
                On E: Exception do begin
                    MsgBoxErr(MSG_ERROR_EXIMIR_FACTURACION, E.Message,
                        Caption, MB_ICONSTOP);
                end;
            end; // except
        end; // if Inicializar
    finally
        f.Release;
    end; // finally
    HabilitarBotonesVehiculos;
    //INICIO: TASK_033_ECA_20160610
    if (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) then
    begin
        btnEditarVehiculo.Enabled:=False;
        btn_AsignarTag.Enabled:=False;
        btnConvFactVehiculo.Enabled:=False;
        btn_Imprimir.Enabled:=True;
        btnEliminarVehiculo.Enabled:=True;
        btn_EximirFacturacion.Enabled:=True;
        btn_BonificarFacturacion.Enabled:=True;
    end;
    //FIN: TASK_033_ECA_20160610
end;

{-----------------------------------------------------------------------------
Procedure Name  : btn_BonificarFacturacionClick
Author          :
Date            :
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.btn_BonificarFacturacionClick(
  Sender: TObject);
resourcestring
    MSG_ERROR_BONIFICAR_FACTURACION = 'No se pudieron modificar los datos del veh�culo';
var
    f: TFormBonificacionFacturacionDeCuenta;
    CodigoConvenio, IndiceVehiculo: Integer;
    EximirFacturacion: TEximirFacturacion;
    BonificarFacturacion: TBonificarFacturacion;
    FilaVehiculo: Integer;
begin
    if dblVehiculos.DataSource.DataSet.RecordCount = 0 then Exit;

    Application.CreateForm(TFormBonificacionFacturacionDeCuenta, f);
    try
        (* Armar las estructuras para pasar como par�metro al form de eximir. *)
        CodigoConvenio := -1;
        IndiceVehiculo := -1;
        FilaVehiculo := cdsVehiculosContacto.RecNo;
        ArmarBonificarFacturacion(FilaVehiculo, BonificarFacturacion);
        ArmarEximirFacturacion(FilaVehiculo, EximirFacturacion);

        if f.Inicializar(ModoPantalla,
                CodigoConvenio,
                IndiceVehiculo,
                BonificarFacturacion,
                EximirFacturacion,
                Trim(cdsVehiculosContacto.FieldByName('Patente').AsString),
                Trim(cdsVehiculosContacto.FieldByName('DigitoVerificadorPatente').AsString),
                cdsVehiculosContacto.FieldByName('SerialNumberaMostrar').AsString)
                and (f.ShowModal = mrOK) then begin

            try
                (* Obtener los datos modificados. *)

                if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, 0, cdsVehiculosContacto.FieldByName('Patente').AsString,
                  cdsVehiculosContacto.FieldByName('IndiceVehiculo').AsInteger, cdsVehiculosContacto.FieldByName('SerialNumberaMostrar').AsString,
                  FPuntoEntrega, UsuarioSistema, 'Bonificar')  then
                    raise Exception.create('Error registrando auditoria');

                BonificarFacturacion := f.GetBonificarFacturacion;
                (* Actualizar los datos del vehiculo. *)
                ActualizarBonificacionFacturacion(FilaVehiculo, BonificarFacturacion);

            except
                On E: Exception do begin
                    MsgBoxErr(MSG_ERROR_BONIFICAR_FACTURACION, E.Message,
                        Caption, MB_ICONSTOP);
                end;
            end; // except
        end; // if Inicializar
    finally
        f.Release;
    end; // finally
    HabilitarBotonesVehiculos;
    //INICIO: TASK_033_ECA_20160610
    if (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) then
    begin
        btnEditarVehiculo.Enabled:=False;
        btn_AsignarTag.Enabled:=False;
        btnConvFactVehiculo.Enabled:=False;
        btn_Imprimir.Enabled:=True;
        btnEliminarVehiculo.Enabled:=True;
        btn_EximirFacturacion.Enabled:=True;
        btn_BonificarFacturacion.Enabled:=True;
    end;
    //FIN: TASK_033_ECA_20160610
end;

{-----------------------------------------------------------------------------
Procedure Name  : TFormSolicitudContacto.ArmarBonificarFacturacion
Author          : ggomez
Date            : 13-May-2005
Description     : Arma la estructura con los datos de la bonificaci�n de facturaci�n
                de un veh�culo.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.ArmarBonificarFacturacion(
  RecNoVehiculo: Integer; var RecordBonificar: TBonificarFacturacion);
begin
    (* Posicionarse en la fila del registro del cual obtener los datos. *)
    if cdsVehiculosContacto.RecNo <> RecNoVehiculo then begin
        cdsVehiculosContacto.RecNo := RecNoVehiculo;
    end;

    RecordBonificar.ExisteEnBaseDeDatos := False;

    if cdsVehiculosContacto.FieldByName('FechaInicioOriginalBonificacion').IsNull then begin
        RecordBonificar.FechaInicioOriginal := NullDate;
    end else begin
        RecordBonificar.FechaInicioOriginal := cdsVehiculosContacto.FieldByName('FechaInicioOriginalBonificacion').AsDateTime;
    end;

    if cdsVehiculosContacto.FieldByName('FechaInicioBonificacion').IsNull then begin
        RecordBonificar.FechaInicio := NullDate;
    end else begin
        RecordBonificar.FechaInicio := cdsVehiculosContacto.FieldByName('FechaInicioBonificacion').AsDateTime;
    end;

    if cdsVehiculosContacto.FieldByName('FechaFinalizacionBonificacion').IsNull then begin
        RecordBonificar.FechaFinalizacion := NullDate;
    end else begin
        RecordBonificar.FechaFinalizacion := cdsVehiculosContacto.FieldByName('FechaFinalizacionBonificacion').AsDateTime;
    end;

    if cdsVehiculosContacto.FieldByName('PorcentajeBonificacion').IsNull then begin
        RecordBonificar.Porcentaje := 0;
    end else begin
        RecordBonificar.Porcentaje := cdsVehiculosContacto.FieldByName('PorcentajeBonificacion').AsFloat;
    end;

    if cdsVehiculosContacto.FieldByName('ImporteBonificacion').IsNull then begin
        RecordBonificar.Importe := 0;
    end else begin
        RecordBonificar.Importe := cdsVehiculosContacto.FieldByName('ImporteBonificacion').AsVariant;
    end;

    RecordBonificar.Usuario := EmptyStr;
    RecordBonificar.FechaHoraModificacion := NullDate;

    RecordBonificar.FechaInicioEditable := cdsVehiculosContacto.FieldByName('FechaInicioBonificacionEditable').AsBoolean;
    RecordBonificar.FechaFinalizacionEditable := cdsVehiculosContacto.FieldByName('FechaFinBonificacionEditable').AsBoolean;

end;

{-----------------------------------------------------------------------------
Procedure Name  : TFormSolicitudContacto.ArmarEximirFacturacion
Author          : ggomez
Date            : 13-May-2005
Description     : Arma la estructura con los datos de la eximici�n de facturaci�n
                de un veh�culo.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.ArmarEximirFacturacion(
  RecNoVehiculo: Integer; var RecordEximir: TEximirFacturacion);
begin
    (* Posicionarse en la fila del registro del cual obtener los datos. *)
    if cdsVehiculosContacto.RecNo <> RecNoVehiculo then begin
        cdsVehiculosContacto.RecNo := RecNoVehiculo;
    end;

    RecordEximir.ExisteEnBaseDeDatos := False;
    if cdsVehiculosContacto.FieldByName('FechaInicioOriginalEximicion').IsNull then begin
        RecordEximir.FechaInicioOriginal := NullDate;
    end else begin
        RecordEximir.FechaInicioOriginal := cdsVehiculosContacto.FieldByName('FechaInicioOriginalEximicion').AsDateTime;
    end;

    if cdsVehiculosContacto.FieldByName('FechaInicioEximicion').IsNull then begin
        RecordEximir.FechaInicio := NullDate;
    end else begin
        RecordEximir.FechaInicio := cdsVehiculosContacto.FieldByName('FechaInicioEximicion').AsDateTime;
    end;

    if cdsVehiculosContacto.FieldByName('FechaFinalizacionEximicion').IsNull then begin
        RecordEximir.FechaFinalizacion := NullDate;
    end else begin
        RecordEximir.FechaFinalizacion := cdsVehiculosContacto.FieldByName('FechaFinalizacionEximicion').AsDateTime;
    end;

    if cdsVehiculosContacto.FieldByName('CodigoMotivoEximicion').IsNull then begin
        RecordEximir.CodigoMotivoNoFacturable := 0;
    end else begin
        RecordEximir.CodigoMotivoNoFacturable := cdsVehiculosContacto.FieldByName('CodigoMotivoEximicion').AsInteger;
    end;

    RecordEximir.Usuario := EmptyStr;
    RecordEximir.FechaHoraModificacion := NullDate;

    RecordEximir.FechaInicioEditable := cdsVehiculosContacto.FieldByName('FechaInicioEximicionEditable').AsBoolean;
    RecordEximir.FechaFinalizacionEditable := cdsVehiculosContacto.FieldByName('FechaFinEximicionEditable').AsBoolean;

end;

{-----------------------------------------------------------------------------
Procedure Name  : TFormSolicitudContacto.ActualizarBonificionFacturacion
Author          : ggomez
Date            : 13-May-2005
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.ActualizarBonificacionFacturacion(
  RecNoVehiculo: Integer; RecordBonificar: TBonificarFacturacion);
begin
    (* Posicionarse en la fila del registro a actualizar. *)
    if cdsVehiculosContacto.RecNo <> RecNoVehiculo then begin
        cdsVehiculosContacto.RecNo := RecNoVehiculo;
    end;

    with cdsVehiculosContacto do begin
        try
            Edit;

            if RecordBonificar.FechaInicioOriginal <= 0 then begin
                FieldByName('FechaInicioOriginalBonificacion').Value := Null;
            end else begin
                FieldByName('FechaInicioOriginalBonificacion').AsDateTime := RecordBonificar.FechaInicioOriginal;
            end;

            if RecordBonificar.FechaInicio <= 0 then begin
                FieldByName('FechaInicioBonificacion').Value := Null;
            end else begin
                FieldByName('FechaInicioBonificacion').AsDateTime := RecordBonificar.FechaInicio;
            end;

            if RecordBonificar.FechaFinalizacion <= 0 then begin
                FieldByName('FechaFinalizacionBonificacion').Value := Null;
            end else begin
                FieldByName('FechaFinalizacionBonificacion').AsDateTime := RecordBonificar.FechaFinalizacion;
            end;

            if RecordBonificar.Porcentaje <= 0 then begin
                FieldByName('PorcentajeBonificacion').Value := Null;
            end else begin
                FieldByName('PorcentajeBonificacion').AsFloat := RecordBonificar.Porcentaje;
            end;

            if RecordBonificar.Importe <= 0 then begin
                FieldByName('ImporteBonificacion').Value := Null;
            end else begin
                FieldByName('ImporteBonificacion').asVariant := RecordBonificar.Importe;
            end;

            Post;
        except
            On E: Exception do begin
                Cancel;
                raise;
            end;
        end; // except
    end; // with
end;

{-----------------------------------------------------------------------------
Procedure Name  : TFormSolicitudContacto.ActualizarEximicionFacturacion
Author          : ggomez
Date            : 13-May-2005
Description     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.ActualizarEximicionFacturacion(
  RecNoVehiculo: Integer; RecordEximir: TEximirFacturacion);
begin
    (* Posicionarse en la fila del registro a actualizar. *)
    if cdsVehiculosContacto.RecNo <> RecNoVehiculo then begin
        cdsVehiculosContacto.RecNo := RecNoVehiculo;
    end;

    with cdsVehiculosContacto do begin
        try
            Edit;

            if RecordEximir.FechaInicioOriginal <= 0 then begin
                FieldByName('FechaInicioOriginalEximicion').Value := Null;
            end else begin
                FieldByName('FechaInicioOriginalEximicion').AsDateTime := RecordEximir.FechaInicioOriginal;
            end;

            if RecordEximir.FechaInicio <= 0 then begin
                FieldByName('FechaInicioEximicion').Value := Null;
            end else begin
                FieldByName('FechaInicioEximicion').AsDateTime := RecordEximir.FechaInicio;
            end;

            if RecordEximir.FechaFinalizacion <= 0 then begin
                FieldByName('FechaFinalizacionEximicion').Value := Null;
            end else begin
                FieldByName('FechaFinalizacionEximicion').AsDateTime := RecordEximir.FechaFinalizacion;
            end;

            if RecordEximir.CodigoMotivoNoFacturable <= 0 then begin
                FieldByName('CodigoMotivoEximicion').AsInteger := 0;
            end else begin
                FieldByName('CodigoMotivoEximicion').AsInteger := RecordEximir.CodigoMotivoNoFacturable;
            end;

            Post;
        except
            On E: Exception do begin
                Cancel;
                raise;
            end;
        end; // except
    end; // with

end;

{-----------------------------------------------------------------------------
Procedure Name  : AcomodarBotonesVehiculo
Author          :
Date            :
Descripcion     :
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.AcomodarBotonesVehiculo;
begin
    if not btnAgregarVehiculo.Visible then begin
        btnEditarVehiculo.Left          := btnEditarVehiculo.Left - btnAgregarVehiculo.Width;
        btnEliminarVehiculo.Left        := btnEliminarVehiculo.Left - btnAgregarVehiculo.Width;
        btn_AsignarTag.Left             := btn_AsignarTag.Left - btnAgregarVehiculo.Width;
        btn_EximirFacturacion.Left      := btn_EximirFacturacion.Left - btnAgregarVehiculo.Width;
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btnAgregarVehiculo.Width;
    end;
    if not btnEditarVehiculo.Visible then begin
        btnEliminarVehiculo.Left        := btnEliminarVehiculo.Left - btnEditarVehiculo.Width;
        btn_AsignarTag.Left             := btn_AsignarTag.Left - btnEditarVehiculo.Width;
        btn_EximirFacturacion.Left      := btn_EximirFacturacion.Left - btnEditarVehiculo.Width;
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btnEditarVehiculo.Width;
    end;
    if not btnEliminarVehiculo.Visible then begin
        btn_AsignarTag.Left             := btn_AsignarTag.Left - btnEliminarVehiculo.Width;
        btn_EximirFacturacion.Left      := btn_EximirFacturacion.Left - btnEliminarVehiculo.Width;
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btnEliminarVehiculo.Width;
    end;
    if not btn_AsignarTag.Visible then begin
        btn_EximirFacturacion.Left      := btn_EximirFacturacion.Left - btn_AsignarTag.Width;
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btn_AsignarTag.Width;
    end;
    if not btn_EximirFacturacion.Visible then begin
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btn_EximirFacturacion.Width;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : ObtenerCantidadVehiculosConvenio
Author          : ggomez
Date            : 16-Jun-2005
Descripcion     : Retorna la cantidad de vehiculos del convenio.
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.ObtenerCantidadVehiculosConvenio: Integer;
begin
    Result := cdsVehiculosContacto.RecordCount;
end;

{-----------------------------------------------------------------------------
Procedure Name  : ActualizarMediosEnvioDocumentacion
Author          :
Date            :
Descripcion     : Modifica medios de env�o de documentaci�n seg�n restricciones.
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.ActualizarMediosEnvioDocumentacion;
var
    i, CantidadMaximaVehiculos: Integer;
begin
    (* Actualiza los Medios de Env�o de Documentaci�n seg�n las restricciones
    sobre los datos de los Medios de Env�o de Documentaci�n. *)
    for i := 0 to (Length(RegMedioEnvioCorreo) - 1) do begin
        (* Si existe al Medio de Env�o Detalle de Facturaci�n Impreso junto
        con Nota de Cobro, controlar que la cantidad de veh�culos del
        convenio NO supere el M�ximo para el env�o de Faturaci�n Detallada
        Gratis.
        Si supera la cantidad m�xima, cambiar el Tipo Medio de Env�o a E-mail. *)
        if ( RegMedioEnvioCorreo[i].CodigoTipoMedioEnvio = CONST_TIPO_MEDIO_ENVIO_CORREO_POSTAL)
                and (RegMedioEnvioCorreo[i].CodigoMedioEnvioDocumento = CONST_MEDIO_ENVIO_DETALLE_FACTURACION) then begin

            ObtenerParametroGeneral(DMConnections.BaseCAC, 'TOPE_VEHICULOS_FACT_DETALLADA', CantidadMaximaVehiculos);
            if (cdsVehiculosContacto.RecordCount > CantidadMaximaVehiculos) then begin
                RegMedioEnvioCorreo[i].CodigoTipoMedioEnvio := CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO;
            end;
        end;

        if ( RegMedioEnvioCorreo[i].CodigoTipoMedioEnvio = CONST_TIPO_MEDIO_ENVIO_CORREO_POSTAL)
                and (RegMedioEnvioCorreo[i].CodigoMedioEnvioDocumento = CONST_MEDIO_ENVIO_DETALLE_FACTURACION) then begin

            ObtenerParametroGeneral(DMConnections.BaseCAC, 'TOPE_VEHICULOS_FACT_DETALLADA', CantidadMaximaVehiculos);
            if (cdsVehiculosContacto.RecordCount > CantidadMaximaVehiculos) then begin
                RegMedioEnvioCorreo[i].CodigoTipoMedioEnvio := CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO;
            end;
        end;
    end; // for
end;

{-----------------------------------------------------------------------------
Procedure Name  : RegistrarLogOperacionConvenio
Author          :
Date            :
Descripcion     : Registra Auditor�a en Log.
-----------------------------------------------------------------------------}
function TFormSolicitudContacto.RegistrarLogOperacionConvenio( NumeroDocumento: string ; CodigoConvenio: integer ; Patente: string ; IndiceVehiculo: integer; Etiqueta: string ;  CodigoPuntoEntrega: integer; Usuario: string ; Accion: string): boolean;
ResourceString
    MSG_ERROR_AUDITORIA_CAC = 'Error guardando operaciones en Convenios CAC';
begin
    Result := False ;
    try
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@Operacion').Value       :=  'A' ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@NumeroDocumento').Value :=  NumeroDocumento ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@CodigoConvenio').Value  :=  CodigoConvenio ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@Etiqueta').Value        :=  Etiqueta       ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@Patente').Value         :=  Patente        ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@IndiceVehiculo').Value  :=  IndiceVehiculo       ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@CodigoPuntoEntrega').Value := CodigoPuntoEntrega ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@Usuario').Value         := Usuario               ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@HostName').Value        := GetMachineName        ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@Accion').Value          := Accion                ;
        SPAgregarAuditoriaConveniosCAC.ExecProc;
        if SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@RETURN_VALUE').Value = 0 then Result := True;
    except
        On e: exception do begin
            MsgBoxErr(MSG_ERROR_AUDITORIA_CAC, E.message, MSG_ERROR_AUDITORIA_CAC, MB_ICONSTOP);
        end;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  : CargarPantalla
Author          : dAllegretti
Date            : 27/10/2008
Descripcion     : Carga la pantalla con la informacion para el nuevo convenio
-----------------------------------------------------------------------------}
procedure TFormSolicitudContacto.CargarPantalla(Persona: TDatosPersonales; Domicilio: TDatosDomicilio);
begin
    FreDatoPersona.txtDocumento.Text := Persona.NumeroDocumento;
    if Persona.CodigoPersona > 0 then IniciarCargaConRut
    else begin
        if ((Persona.Apellido <> '') And (FreDatoPersona.txt_Apellido.Text = '')) then begin
            FreDatoPersona.txt_Apellido.Text := Persona.Apellido;
            FreDatoPersona.txt_Nombre.Text := Persona.Nombre;
            FreDatoPersona.cbPersoneria.ItemIndex := 0;
        end
        else
            if ((Persona.RazonSocial <> '') And (FreDatoPersona.txtRazonSocial.Text = ''))  then begin
                FreDatoPersona.txtRazonSocial.Text := Persona.RazonSocial;
                FreDatoPersona.cbPersoneria.ItemIndex := 1;
            end;
        FreDatoPersona.cbPersoneria.OnChange(Self);
        FMDomicilioPrincipal.CargarDatosDomicilio(Domicilio);
    end;
end;

{-----------------------------------------------------------------------
Procedure Name  : EsGiroValido
Author          : mbecerra
Date            : 28-Septiembre-2009
Description     : (Ref. SS 833)
                Valida si el Giro indicado para el usuario persona es v�lido
------------------------------------------------------------------------}
function TFormSolicitudContacto.EsGiroValido;
resourcestring
	MSG_ERROR_GIRO_INVALIDO		= 'El Giro ingresado es inv�lido';
    MSG_ERROR_GIRO_CARACTERES	= 'El Giro contiene caracteres inv�lidos';

var
    GiroAux, Mensaje : string;
    i, Largo : integer;
begin
	GiroAux := Trim(Giro);
    Largo	:= Length(GiroAux);
    Mensaje := '';
	if Largo < 5 then Mensaje := MSG_ERROR_GIRO_INVALIDO;

    i := 1;
    while (Mensaje = '') and (i < Largo) do begin
        if not (GiroAux[i] in ['0'..'9', 'a'..'z','A'..'Z', ',', '.', ' ']) then begin
        	Mensaje := MSG_ERROR_GIRO_CARACTERES;
        end;

        Inc(i);
    end;

    Result := (Mensaje = '');
    if MostrarMensaje and (not Result) then MsgBox(Mensaje, Caption, MB_ICONERROR);
    
end;

{-----------------------------------------------------------------------
Procedure Name  : vcbTipoDocumentoElectronicoChange
Author          : mbecerra
Date            : 01-Octubre-2009
Description     : (Ref. SS 833)
                Permite asignar el tipo de comprobante fiscal al Convenio
------------------------------------------------------------------------}
procedure TFormSolicitudContacto.vcbTipoDocumentoElectronicoChange(Sender: TObject);
resourcestring
	MSG_TITULO_GIRO		= 'Para Clientes que emiten factura';
    MSG_INGRESE_GIRO	= 'Ingrese Giro:';

var
	Giro, Comprobante, Personeria : string;
begin
    if (ActiveControl = Sender) and (vcbTipoDocumentoElectronico.ItemIndex >= 0) then begin
        Comprobante := vcbTipoDocumentoElectronico.Value;
        Personeria	:= FreDatoPersona.cbPersoneria.Text;
        Personeria	:= Trim(Copy(Personeria, Length(Personeria) - 5, 10));
        if 	(Comprobante = TC_FACTURA) and (Personeria = PERSONERIA_FISICA) then begin
            Giro := FreDatoPersona.txtGiro.Text;
            if InputQuery(MSG_TITULO_GIRO, MSG_INGRESE_GIRO, Giro) then begin
                if EsGiroValido(Giro, True) then begin
                    FreDatoPersona.txtGiro.Text := Giro;
                    DatoPersona.Giro			:= Giro;
                end
                else vcbTipoDocumentoElectronico.ItemIndex := vcbTipoDocumentoElectronico.Items.IndexOfValue(TC_BOLETA);
            end
            else vcbTipoDocumentoElectronico.ItemIndex := vcbTipoDocumentoElectronico.Items.IndexOfValue(TC_BOLETA);
        end;
    end;



end;

// INICIO : TASK_096_MGO_20161220
procedure TFormSolicitudContacto.btnGrupoFacturacionClick(Sender: TObject);
var
    f: TfrmSeleccionarGrupoFacturacionForm;
begin
    Application.CreateForm(TfrmSeleccionarGrupoFacturacionForm, f);
    try
        if f.Inicializar(CodigoGrupoFacturacion) then
            if f.ShowModal = mrOk then begin
                CodigoGrupoFacturacion := f.FCodigoGrupoFacturacion;
                ActualizarDescripcionGrupoFacturacion;
            end;
    finally
        f.Release;
    end;
end;

procedure TFormSolicitudContacto.ObtenerGrupoFacturacion;
resourcestring
    QRY_OBTENER_GRUPO = 'SELECT dbo.ObtenerGrupoFacturacion(%d)';
var
    vPagoAutomatico: Integer;
begin
    if cb_PagoAutomatico.ItemIndex <= 0 then Exit;

    vPagoAutomatico := cb_PagoAutomatico.Value;
    try
        CodigoGrupoFacturacion := QueryGetValueInt(DMConnections.BaseCAC, Format(QRY_OBTENER_GRUPO, [vPagoAutomatico]));
    except
        CodigoGrupoFacturacion := 0;
    end;
    ActualizarDescripcionGrupoFacturacion;
end;

procedure TFormSolicitudContacto.ActualizarDescripcionGrupoFacturacion;
resourcestring
    STR_GRUPO_FACTURACION = 'Grupo Facturaci�n: %d - %s';   
    STR_SIN_GRUPO_FACTURACION = 'Sin Grupo de Facturaci�n';
    STR_ERROR = 'Hubo un error obteniendo el Grupo de Facturaci�n.';
var
    spObtenerGruposFacturacionDetallado: TADOStoredProc;
begin
    spObtenerGruposFacturacionDetallado := TADOStoredProc.Create(Self);
    try
        try
            spObtenerGruposFacturacionDetallado.Connection := DMConnections.BaseCAC;
            spObtenerGruposFacturacionDetallado.ProcedureName := 'ObtenerGruposFacturacionDetallado';
            spObtenerGruposFacturacionDetallado.CommandTimeout := 30;

            spObtenerGruposFacturacionDetallado.Parameters.Refresh;
            spObtenerGruposFacturacionDetallado.Parameters.ParamByName('@CodigoGrupoFacturacion').Value := CodigoGrupoFacturacion;
            spObtenerGruposFacturacionDetallado.Open;

            if spObtenerGruposFacturacionDetallado.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spObtenerGruposFacturacionDetallado.Parameters.ParamByName('@ErrorDescription').Value);

            if spObtenerGruposFacturacionDetallado.RecordCount = 0 then
                lblGrupoFacturacion.Caption := STR_SIN_GRUPO_FACTURACION
            else
                lblGrupoFacturacion.Caption := Format(STR_GRUPO_FACTURACION,
                                                [spObtenerGruposFacturacionDetallado.FieldByName('CodigoGrupoFacturacion').AsInteger,
                                                spObtenerGruposFacturacionDetallado.FieldByName('Descripcion').AsString]);
            lblGrupoFacturacion.Update;
        except
            on e: Exception do
                MsgBoxErr(STR_ERROR, e.Message, 'Error', MB_ICONERROR);
        end;
    finally
        spObtenerGruposFacturacionDetallado.Free;
    end;
end;
// FIN : TASK_096_MGO_20161220

{-----------------------------------------------------------------------
Procedure Name  : EsConvenioSinCuenta
Author          : alabra
Date            : 21-07-2011
Description     : Retorna si el Convenio actual es un Convenio Sin Cuenta
Firma           : SS_628_ALA_20110720
------------------------------------------------------------------------}
function TFormSolicitudContacto.EsConvenioSinCuenta: Boolean;
var
    valor : Integer;
begin
    //Result := cdsVehiculosContacto.RecordCount = 0;                                             //SS_628_ALA_20111202
    Result := False;  //chkConvenioSinCuenta.Checked;                                             //SS_628_ALA_20111202 //TASK_004_ECA_20160411
    Valor:= Integer(cbbTipoConvenio.Value);                                                       //TASK_004_ECA_20160411
    if (Valor=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) or (valor=CODIGO_TIPO_CONVENIO_INFRACTOR) then  //TASK_004_ECA_20160411
        Result := True;                                                                           //TASK_004_ECA_20160411
end;

//INICIO: TASK_004_ECA_20160411
//procedure TFormSolicitudContacto.cb_AdheridoPAClick(Sender: TObject);
//	var
//	    spValidarRestriccionesAdhesionPAKAnexo: TADOStoredProc;
//		SituacionesARegularizar: Boolean;
//	const
//    	cSQLValidarPersonaAdheridaPA = 'SELECT dbo.ValidarPersonaAdheridaPAK(%d)';
//begin
//    if cb_AdheridoPA.Checked then begin
//        if not QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarPersonaAdheridaPA, [FreDatoPersona.RegistrodeDatos.CodigoPersona])) then try
//            try
//                spValidarRestriccionesAdhesionPAKAnexo := TADOStoredProc.Create(nil);
//
//                with spValidarRestriccionesAdhesionPAKAnexo do begin
//                    Connection := DMConnections.BaseCAC;
//                    ProcedureName := 'ValidarRestriccionesAdhesionPAKAnexo';
//                    Parameters.Refresh;
//                    Parameters.ParamByName('@CodigoPersona').Value := FreDatoPersona.RegistrodeDatos.CodigoPersona;
//                    Parameters.ParamByName('@DevolverValidaciones').Value := 0;
//                    Parameters.ParamByName('@InfraccionesPendientes').Value := 0;
//                    Parameters.ParamByName('@TAGsVencidos').Value := 0;
//                    Parameters.ParamByName('@CuentasSuspendidas').Value := 0;
//                    ExecProc;
//
//                    SituacionesARegularizar :=
//                        (Parameters.ParamByName('@InfraccionesPendientes').Value > 0)
//                        or (Parameters.ParamByName('@TAGsVencidos').Value > 0)
//                        or (Parameters.ParamByName('@CuentasSuspendidas').Value > 0);
//                end;
//
//                if SituacionesARegularizar then try
//                    try
//                        FormConsultaAltaPAraucoSoloConsulta := TFormConsultaAltaPArauco.ModalCreate(nil);
//                        if FormConsultaAltaPAraucoSoloCOnsulta.Inicializar(FreDatoPersona.RegistrodeDatos.NumeroDocumento) then begin
//                            FormConsultaAltaPAraucoSoloCOnsulta.ShowModal;
//                        end;
//                    except
//                        on e: Exception do begin
//                            ShowMsgBoxCN(e, Self);
//                        end;
//                    end;
//                finally
//                    if Assigned(FormConsultaAltaPAraucoSoloCOnsulta) then FreeAndNil(FormConsultaAltaPAraucoSoloCOnsulta);
//                end;
//            except
//                on e: Exception do begin
//                    ShowMsgBoxCN(e, Self);
//                end;
//            end;
//        finally
//            if Assigned(spValidarRestriccionesAdhesionPAKAnexo) then FreeAndNil(spValidarRestriccionesAdhesionPAKAnexo);
//        end;
//    end;
//end;

//procedure TFormSolicitudContacto.cb_AdheridoPAEnter(Sender: TObject);
//	resourcestring
//        MSG_ERROR_REPRESENTANTE_LEGAL_TITULO  = 'Validaci�n Adhesi�n PA';
//        MSG_ERROR_REPRESENTANTE_LEGAL_MENSAJE = 'Se requiere un Representante Legal para poder realizar la adhesi�n al servicio Arauco TAG.';
//        MSG_ERROR_CONVENIO_SIN_CUENTA         = 'No se puede adherir a PA, pues el convenio est� marcado como convenio Sin Cuentas.';
//begin
//	if chkConvenioSinCuenta.Checked then begin
//		// SS_1072_PDO_20121127 Inicio Bloque
//    	chkConvenioSinCuenta.Checked := False;
////        MsgBoxBalloon(
////            MSG_ERROR_CONVENIO_SIN_CUENTA,
////            MSG_ERROR_REPRESENTANTE_LEGAL_TITULO,
////            MB_ICONERROR,
////            chkConvenioSinCuenta);
////
////        chkConvenioSinCuenta.SetFocus;
////        Abort;
//		// SS_1072_PDO_20121127 Fin Bloque
//
//
//    end
//    else if FreDatoPersona.RegistrodeDatos.Personeria = PERSONERIA_JURIDICA then begin
//        if RepresentateLegal1.CodigoPersona = -1 then begin
//                MsgBoxBalloon(
//                    MSG_ERROR_REPRESENTANTE_LEGAL_MENSAJE,
//                    MSG_ERROR_REPRESENTANTE_LEGAL_TITULO,
//                    MB_ICONERROR,
//                    btn_RepresentanteLegal);
//
//                btn_RepresentanteLegal.SetFocus;
//                Abort;
//        end;
//    end;
//
//end;
//FIN: TASK_004_ECA_20160411
procedure TFormSolicitudContacto.ImprimirContratoAdhesionPA;
	var
        EMailPersona,
        NombrePersona,
        Firma,
        NumeroDocumento: string;
        ClienteAdheridoEnvioEMail: Boolean;
        ReporteContratoAdhesionPAForm: TReporteContratoAdhesionPAForm;
        FechaAdhesion:TDateTime;
        RBInterfaceConfig: TRBConfig;
        CodigoPersona: Integer;
        spValidarRestriccionesAdhesionPAKAnexo: TADOStoredProc;			// SS_1006_PDO_20121122
        SituacionesARegularizar: AnsiString;							// SS_1006_PDO_20121122
    const
    	cSQLObtenerEmailPersona                  = 'SELECT dbo.ObtenerEmailPersona(%d)';
        cSQLFechaAdhesion                        = 'SELECT dbo.ObtenerFechaAdhesionPA(%d)';
        cSQLValidarConveniosPersonaEnvioPorEMail = 'SELECT dbo.ValidarConveniosPersonaEnvioPorEMail(%d)';
        cSQLObtenerNombrePersona				 = 'SELECT dbo.ObtenerNombrePersona(%d)';
        cSQLObtenerCodigoPersona                 = 'SELECT dbo.ObtenerCodigoPersonaRUT(''%s'')';
        cSQLArmarRazonSocialyRepLegalEmpresa     = 'SELECT dbo.ArmarRazonSocialyRepLegalEmpresa(%d)';
begin
    try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
        	// SS_1006_PDO_20121122 Inicio Bloque
        	spValidarRestriccionesAdhesionPAKAnexo := TADOStoredProc.Create(nil);
            with spValidarRestriccionesAdhesionPAKAnexo do begin
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'ValidarRestriccionesAdhesionPAKAnexo';
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
                Parameters.ParamByName('@DevolverValidaciones').Value := 1;
                Open;

                if RecordCount > 0 then begin
                	while not Eof do begin

                        if SituacionesARegularizar <> EmptyStr then begin
                        	SituacionesARegularizar := SituacionesARegularizar + '\par \par ';
                        end;

                        SituacionesARegularizar := SituacionesARegularizar + ' \tab ' + '- ' + AnsiUpperCase(FieldByName('Descripcion').AsString);

                        Next;
                    end;
                end;
            end;
            // SS_1006_PDO_20121122 Fin Bloque

            CodigoPersona := QueryGetIntegerValue(DMConnections.BaseCAC, Format(cSQLObtenerCodigoPersona, [FreDatoPersona.RegistrodeDatos.NumeroDocumento]));
            FechaAdhesion:= QueryGetDateValue(DMConnections.BaseCAC, Format(cSQLFechaAdhesion, [FCodigoConvenio]));
            EMailPersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerEmailPersona, [CodigoPersona]));
            ClienteAdheridoEnvioEMail := QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarConveniosPersonaEnvioPorEMail, [CodigoPersona]));

            NumeroDocumento := FreDatoPersona.RegistrodeDatos.NumeroDocumento;

            if FreDatoPersona.RegistrodeDatos.Personeria = PERSONERIA_JURIDICA then begin
	            NombrePersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLArmarRazonSocialyRepLegalEmpresa, [FCodigoConvenio]));
	            CodigoPersona := QueryGetIntegerValue(DMConnections.BaseCAC, Format(cSQLObtenerCodigoPersona, [RepresentateLegal1.NumeroDocumento]));
	            Firma := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerNombrePersona, [CodigoPersona]));
            end
            else begin
	            NombrePersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerNombrePersona, [CodigoPersona]));
            	Firma := NombrePersona;
            end;

            ReporteContratoAdhesionPAForm := TReporteContratoAdhesionPAForm.Create(nil);

            with ReporteContratoAdhesionPAForm do begin

                Inicializar(
                	NombrePersona,
                    Firma,
                    Trim(NumeroDocumento),
                    EMailPersona,
                    FechaAdhesion,
                    ClienteAdheridoEnvioEMail,
                    False,						    // SS_1006_PDO_20121122
                    False,                          // SS_1006_PDO_20121122
                    SituacionesARegularizar);       // SS_1006_PDO_20121122

                RBInterface.ModalPreview := False;
                RBInterfaceConfig := RBInterface.GetConfig;

                with RBInterfaceConfig do begin
	                Copies       := 2;
                    MarginTop    := 0;
                    MarginLeft   := 0;
                    MarginRight  := 0;
                    MarginBottom := 0;
                    Orientation  := poPortrait;
	                {$IFDEF DESARROLLO or TESTKTC}
                    	PaperName    := 'Letter';
                    {$ELSE}
                    	PaperName    := 'Carta';
                    {$ENDIF}
                    ShowUser     := False;
                    ShowDateTime := False;
                end;

                RBInterface.SetConfig(RBInterfaceConfig);
                RBInterface.Execute(True);
            end;
        except
        	on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	if Assigned(ReporteContratoAdhesionPAForm) then FreeAndNil(ReporteContratoAdhesionPAForm);
        // SS_1006_PDO_20121122 Inicio Bloque
        if Assigned(spValidarRestriccionesAdhesionPAKAnexo) then begin
        	if spValidarRestriccionesAdhesionPAKAnexo.Active then spValidarRestriccionesAdhesionPAKAnexo.Close;
            FreeAndNil(spValidarRestriccionesAdhesionPAKAnexo);
        end;
        // SS_1006_PDO_20121122 Fin BLoque
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;


//INICIO: TASK_004_ECA_20160411
//procedure TFormSolicitudContacto.chkConvenioSinCuentaEnter(Sender: TObject);
//    resourcestring
//        MSG_NO_ES_SIN_CUENTA = 'No puede marcar este convenio como sin cuenta, ya que ha agregado Cuentas';
//        MSG_ES_PAK           = 'No puede marcar este convenio como sin cuenta, ya que est� Adherido a PA';
//begin
//	if cb_AdheridoPA.Checked then begin
//		// SS_1072_PDO_20121127 Inicio bloque
//    	cb_AdheridoPA.Checked := False;
////        MsgBoxBalloon(MSG_ES_PAK, Self.Caption, MB_ICONSTOP, cb_AdheridoPA);
////        cb_AdheridoPA.SetFocus;
////        Abort;
//		// SS_1072_PDO_20121127 Fin Bloque
//    end
//    else if (cdsVehiculosContacto.RecordCount > 0)  then begin
//        MsgBoxBalloon(MSG_NO_ES_SIN_CUENTA, Self.Caption, MB_ICONSTOP, dblVehiculos);
//        dblVehiculos.SetFocus;
//        Abort;
//    end;
//end;
//FIN: TASK_004_ECA_20160411

//INICIO: TASK_004_ECA_20160411
procedure TFormSolicitudContacto.cbbTipoConvenioChange(Sender: TObject);
var
    Valor:Integer;
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    //INICIO: TASK_012_ECA_20160514
    btn_RUTFacturacion.Enabled:=True;
    chkCtaComercialPredeterminada.Visible:=False;
    chkCtaComercialPredeterminada.Checked:=False;

    cbbTipoCliente.Visible:=True;                  //TASK_040_ECA_20160628
    lblTipoCliente.Visible:=True;                  //TASK_040_ECA_20160628

    Valor:= Integer(cbbTipoConvenio.Value);
    if (Valor=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) or (Valor=CODIGO_TIPO_CONVENIO_INFRACTOR)  then
    begin
        //EsConvenioSinCuenta:=True;
        btn_ConvenioFacturacion.Enabled:=False;
        btn_AsignarTag.Enabled:=False;
        CodigoConvenioFacturacion:=0;
        txt_NumeroConvenioFacturacion.Text:='';

        chkCtaComercialPredeterminada.Visible:=True;
        chkCtaComercialPredeterminada.Checked:=True;
        chkCtaComercialPredeterminada.Enabled:=False;

        if CuentaComercialPredeterminada<>0 then
        begin
           chkCtaComercialPredeterminada.Checked:=False;
           chkCtaComercialPredeterminada.Enabled:=True;
        end;

        if Valor=CODIGO_TIPO_CONVENIO_INFRACTOR then
        begin
            btn_RUTFacturacion.Enabled:=False;
            CodigoClienteFacturacion:= FreDatoPersona.RegistrodeDatos.CodigoPersona;
            lbl_NombreRutFacturacion.Caption:=BuscarNombreRUT(CodigoClienteFacturacion);
            chkCtaComercialPredeterminada.Visible:=False;
            chkCtaComercialPredeterminada.Checked:=False;
            cbbTipoCliente.Visible:=False; //TASK_040_ECA_20160628
            lblTipoCliente.Visible:=False; //TASK_040_ECA_20160628
        end;
        //FIN: TASK_012_ECA_20160514
        chkInactivaDomicilioRNUT.Visible:=False;                                 //TASK_038_ECA_20160625
        chkInactivaMediosComunicacionRNUT.Visible:=False;                        //TASK_038_ECA_20160625
        chkInactivaDomicilioRNUT.Checked :=False;                                 //TASK_038_ECA_20160625
        chkInactivaMediosComunicacionRNUT.Checked:=False;                        //TASK_038_ECA_20160625
    end
    else
    begin
        //EsConvenioSinCuenta:=False;
        btn_ConvenioFacturacion.Enabled:=True;
        btn_AsignarTag.Enabled:=True;
        chkInactivaDomicilioRNUT.Visible:=True;                                 //TASK_038_ECA_20160625
        chkInactivaMediosComunicacionRNUT.Visible:=True;                        //TASK_038_ECA_20160625
    end;
    Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;

procedure TFormSolicitudContacto.CargarTipoConvenios;
var
    sp: TADOStoredProc;
    i: Integer;
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'CRM_ConvenioTipos_SELECT';
    sp.Open;
    if not sp.IsEmpty then
    begin
        cbbTipoConvenio.Items.Clear;
         for i := 0 to sp.RecordCount - 1 do
         begin
            cbbTipoConvenio.Items.Add(sp.FieldByName('Descripcion').AsString,sp.FieldByName('CodigoTipoConvenio').AsString);
            sp.Next;
         end;
         cbbTipoConvenio.ItemIndex:=0;
    end;
    sp.Close;
    sp.Free; //TASK_013_ECA_20160520
    Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;

procedure TFormSolicitudContacto.OnClickConvenioSelected(NumeroConvenio: String; CodigoConvenio: Integer);
begin
    txt_NumeroConvenioFacturacion.Text := NumeroConvenio;
    CodigoConvenioFacturacion:= CodigoConvenio;
end;

procedure TFormSolicitudContacto.OnClickVehiculoSelected(NumeroConvenio: String; CodigoConvenio: Integer);
begin
    CodigoConvFactVehiculo := CodigoConvenio;
    NumeroConvFactVehiculo   := NumeroConvenio;
end;

procedure TFormSolicitudContacto.ConvenioFacturacion();
var
    frm : TformCodigoConvenioFacturacion;
    Personeria : string;
begin
    Application.CreateForm(TformCodigoConvenioFacturacion, frm);

    if not frm.Inicializa(FreDatoPersona.txtDocumento.Text)
    then
        frm.Release
    else
    begin
        Personeria:= Trim(StrRight(FreDatoPersona.cbPersoneria.Text, 10));
        frm.txt_Personeria.Text:= FreDatoPersona.cbPersoneria.Text;
        frm.txt_RUT.Text:=FreDatoPersona.txtDocumento.Text;
        if (Personeria=PERSONERIA_JURIDICA) then
        begin
            frm.txt_Nombre.Text:=  FreDatoPersona.txtNombre_Contacto.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txtApellido_Contacto.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txtApellidoMaterno_Contacto.Text;
            frm.txt_RazonSocial.Text:=FreDatoPersona.txtRazonSocial.Text;
            frm.txt_RazonSocial.Visible:=True;
            frm.lblRazonSocial.Visible:=True;
        end
        else
        begin
            frm.txt_Nombre.Text:= FreDatoPersona.txt_Nombre.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txt_Apellido.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txt_ApellidoMaterno.Text;
        end;
        frm.OnClickRegSeleccionado:= OnClickConvenioSelected;
        frm.ShowModal;
        frm.Free;
    end;
end;
procedure TFormSolicitudContacto.btn_ConvenioFacturacionClick(Sender: TObject);
begin
    ConvenioFacturacion();
end;

function TFormSolicitudContacto.BuscarNombreRUT(CodigoPersona: Integer): string;
var
    sp: TADOStoredProc;
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    Result:='';
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'ObtenerDatosCliente';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoDocumento').Value := null;
    sp.Parameters.ParamByName('@NumeroDocumento').Value := null;
    sp.Parameters.ParamByName('@CodigoCliente').Value := CodigoPersona;
    sp.Open;
    if not sp.IsEmpty then
    begin
        Result:=Trim(sp.FieldByName('Nombre').AsString);
    end;
    sp.Close;
    sp.Free;  //TASK_013_ECA_20160520
    Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;

procedure TFormSolicitudContacto.btn_RUTFacturacionClick(Sender: TObject);
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda)then begin
    	if (f.ShowModal = mrok) then begin
            FUltimaBusqueda := F.UltimaBusqueda;
			txt_RUTFacturacion.Text := Trim(f.Persona.NumeroDocumento);
            CodigoClienteFacturacion:= QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT CodigoPersona FROM Personas (NOLOCK) WHERE CodigoDocumento = ''RUT'' AND NumeroDocumento = ''%s''',
            [PadL(Trim(txt_RUTFacturacion.Text), 9, '0')]));
            lbl_NombreRutFacturacion.Caption:=BuscarNombreRUT(CodigoClienteFacturacion);
	   	end;
	end;
    f.Release;
end;

procedure TFormSolicitudContacto.btnConvFactVehiculoClick(Sender: TObject);
var
    frm : TformCodigoConvenioFacturacion;
    Personeria : string;
begin

    if dblVehiculos.DataSource.DataSet.RecordCount = 0 then Exit;

    Application.CreateForm(TformCodigoConvenioFacturacion, frm);

    if not frm.Inicializa(FreDatoPersona.txtDocumento.Text)
    then
        frm.Release
    else
    begin
        Personeria:= Trim(StrRight(FreDatoPersona.cbPersoneria.Text, 10));
        frm.txt_Personeria.Text:= FreDatoPersona.cbPersoneria.Text;
        frm.txt_RUT.Text:=FreDatoPersona.txtDocumento.Text;
        if (Personeria=PERSONERIA_JURIDICA) then
        begin
            frm.txt_Nombre.Text:=  FreDatoPersona.txtNombre_Contacto.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txtApellido_Contacto.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txtApellidoMaterno_Contacto.Text;
            frm.txt_RazonSocial.Text:=FreDatoPersona.txtRazonSocial.Text;
            frm.txt_RazonSocial.Visible:=True;
            frm.lblRazonSocial.Visible:=True;
        end
        else
        begin
            frm.txt_Nombre.Text:= FreDatoPersona.txt_Nombre.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txt_Apellido.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txt_ApellidoMaterno.Text;
        end;
        frm.OnClickRegSeleccionado:= OnClickVehiculoSelected;
        frm.ShowModal;
        frm.Free;

        with cdsVehiculosContacto do begin
           Edit;
           FieldByName('CodigoConvenioFacturacion').Value := CodigoConvFactVehiculo;
           FieldByName('NumeroConvenioFacturacion').Value := NumeroConvFactVehiculo;
           post;
        end;
        dblVehiculos.Refresh;
        HabilitarBotonesVehiculos;
    end;
end;
//FIN: TASK_004_ECA_20160411

//INICIO: TASK_008_ECA_20160502
procedure TFormSolicitudContacto.ExcluirTipoConvenios(NumeroDocumento: String);
    function ObtenerCodigoTipoConvenio(NumeroDocumento: String): Integer;
    begin
        Result := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConvenioTipo(''%s'')', [NumeroDocumento]));
    end;
    function ObtenerCodigoTipoConvenioINFRACTOR(NumeroDocumento: String): Integer;
    begin
        Result := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConvenioTipoINFRACTOR(''%s'')', [NumeroDocumento]));
    end;
var
    i:Integer;
    CodigoTipoConvenioVigente: Integer;
    CodigoTipoConvenioINFRACTOR: Integer;
    s: TObject; // TASK_012_ECA_20160514
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    CargarTipoConvenios;
    CodigoTipoConvenioVigente:=ObtenerCodigoTipoConvenio(NumeroDocumento);
    CodigoTipoConvenioINFRACTOR:=ObtenerCodigoTipoConvenioINFRACTOR(NumeroDocumento);

    i:= 0;
    while i < cbbTipoConvenio.Items.Count do
    //for i:= 0 to cbbTipoConvenio.Items.Count - 1 do
    begin
        if (Integer(cbbTipoConvenio.Items[i].Value)= CodigoTipoConvenioVigente) OR (Integer(cbbTipoConvenio.Items[i].Value)= CodigoTipoConvenioINFRACTOR) then
        begin
          cbbTipoConvenio.Items.Delete(i);
          i:= 0;
        end
        else
            i:= i+ 1;
    end;
   cbbTipoConvenio.ItemIndex:=0;
   cbbTipoConvenioChange(s); // TASK_012_ECA_20160514
   Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;

//validar si tiene un convenio tipo RNUT en baja
function TFormSolicitudContacto.TieneConvenioEnBaja(NumeroDocumento: String): Boolean;
var
    Convenio: Integer;
begin
  Convenio:=0;
  Result:=False;

  Convenio:=QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConveniodeBaja(''%s'')', [NumeroDocumento]));
  if Convenio>0 then
     Result:=True;

end;

procedure TFormSolicitudContacto.CargarDatosFacturacion(NumeroDocumento: String);
var
    sp: TADOStoredProc;
    i: Integer;
    ConvenioAux: Integer;
    ConvFactAux: Integer;
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    ConvenioAux:=0;
    ConvenioAux:=QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConveniodeBaja(''%s'')', [NumeroDocumento]));
    ConvFactAux:=0;
    ConvFactAux:=QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConvenioFacturacion(%d)', [ConvenioAux]));

    if ConvFactAux > 0 then
    begin
        sp:= TADOStoredProc.Create(nil);
        sp.Connection:=DMConnections.BaseCAC;
        sp.ProcedureName := 'ConvenioFacturacion_SELECT';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoConvenio').Value := ConvFactAux;
        sp.Open;
        if not sp.IsEmpty then
        begin
            CodigoConvenioFacturacion := sp.FieldByName('CodigoConvenio').AsInteger;
            CodigoClienteFacturacion  := sp.FieldByName('CodigoClienteFacturacion').AsInteger;
            txt_NumeroConvenioFacturacion.Text:= sp.FieldByName('NumeroConvenio').AsString;
            txt_RUTFacturacion.Text:=sp.FieldByName('NumeroDocumento').AsString;
            lbl_NombreRutFacturacion.Caption:=sp.FieldByName('NombreConvenioFacturacion').AsString;
        end;
        sp.Close;
        sp.Free;  //TASK_013_ECA_20160520
    end;
    Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;
//4673

//Permite insertar en el historico de convenios
procedure TFormSolicitudContacto.InsertarHistoricoConvenio(CodigoConvenio: Integer);
var
    sp: TADOStoredProc;
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'InsertarHistoricoConvenio';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
    sp.ExecProc;
    Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;
//FIN: TASK_008_ECA_20160502

//INICIO: TASK_009_ECA_20160506
procedure TFormSolicitudContacto.OnClickVehiculoRUTSelected(CodigoConvenio: Integer; CodigoVehiculo: Integer );
begin
    CodigoConvenioRUT:= CodigoConvenio;
    CodigoVehiculoRUT:= CodigoVehiculo;
end;

procedure TFormSolicitudContacto.MostrarVehiculosRUT;
var
    frm: TformConvenioCuentasRUT;
    Personeria, vehiculosAgregados : string;
    i: Integer;
begin
   Application.CreateForm(TformConvenioCuentasRUT, frm);

   if cdsVehiculosContacto.RecordCount>0 then //TASK_033_ECA_20160610
       cdsVehiculosContacto.First;            //TASK_033_ECA_20160610

    while not cdsVehiculosContacto.eof do
    begin
        vehiculosAgregados:= vehiculosAgregados + Trim(cdsVehiculosContacto.FieldByName('Patente').AsString) + ',';
        cdsVehiculosContacto.Next;
    end;

    if cdsVehiculosContacto.RecordCount>0 then
       cdsVehiculosContacto.First;

    if not frm.Inicializar(Trim(FreDatoPersona.txtDocumento.Text),CodigoConvenioFacturacion,vehiculosAgregados)    then
        frm.Release
    else
    begin
        CodigoConvenioRUT:= 0;
        CodigoVehiculoRUT:= 0;
        Personeria:= Trim(StrRight(FreDatoPersona.cbPersoneria.Text, 10));
        frm.txt_Personeria.Text:= FreDatoPersona.cbPersoneria.Text;
        frm.txt_RUT.Text:=FreDatoPersona.txtDocumento.Text;
        if (Personeria=PERSONERIA_JURIDICA) then
        begin
            frm.txt_Nombre.Text:=  FreDatoPersona.txtNombre_Contacto.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txtApellido_Contacto.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txtApellidoMaterno_Contacto.Text;
            frm.txt_RazonSocial.Text:=FreDatoPersona.txtRazonSocial.Text;
            frm.txt_RazonSocial.Visible:=True;
            frm.lblRazonSocial.Visible:=True;
        end
        else
        begin
            frm.txt_Nombre.Text:= FreDatoPersona.txt_Nombre.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txt_Apellido.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txt_ApellidoMaterno.Text;
        end;
        frm.OnClickRegSeleccionado:= OnClickVehiculoRUTSelected;
        frm.ShowModal;
        //if CodigoConvenioRUT<>0 then  //TASK_051_ECA_20160707
        AgregarVehiculosCuentaComercial(frm.Vehiculo);
        frm.Free;
    end;
end;

//INICIO: TASK_051_ECA_20160707
procedure TFormSolicitudContacto.AgregarVehiculosCuentaComercial(Vehiculo: array of TCuentaVehiculo);
var
 almacen: string;
  i: Integer;
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    if cdsVehiculosContacto.RecordCount>0 then
       cdsVehiculosContacto.Last;

    for i := 0 to Length(Vehiculo)-1 do
    begin
        almacen:=iif(Vehiculo[i].CodigoAlmacenDestino= CONST_ALMACEN_EN_COMODATO,CONST_STR_ALMACEN_COMODATO,iif(Vehiculo[i].CodigoAlmacenDestino = CONST_ALMACEN_ARRIENDO,CONST_STR_ALMACEN_ARRIENDO,
        iif(Vehiculo[i].CodigoAlmacenDestino= CONST_ALMACEN_ENTREGADO_EN_CUOTAS,CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS,'')));

        with cdsVehiculosContacto do begin
            Append;
            FieldByName('TipoPatente').AsString             := '';
            FieldByName('Patente').AsString                 := Vehiculo[i].Vehiculo.Patente;
            FieldByName('Modelo').AsString                  := Vehiculo[i].Vehiculo.Modelo;
            FieldByName('CodigoMarca').Value                := 0;
            FieldByName('DescripcionMarca').AsString        := Vehiculo[i].Vehiculo.Marca;
            FieldByName('CodigoColor').AsInteger            := 0;
            FieldByName('DescripcionColor').AsString        := '';
            FieldByName('AnioVehiculo').Value               := Vehiculo[i].Vehiculo.Anio;
            FieldByName('CodigoConvenio').Value             := Vehiculo[i].CodigoConvenioRNUT;
            //FieldByName('DescripcionTipoVehiculo').AsString := Vehiculo[i].Vehiculo.Tipo;                                      //TASK_106_JMA_20170206
            FieldByName('Observaciones').AsString           := '';
            FieldByName('ContextMark').Value                := 0;
            FieldByName('ContractSerialNumber').Value       := Vehiculo[i].Vehiculo.CodigoVehiculo;
            FieldByName('SerialNumberaMostrar').Value       := Vehiculo[i].SerialNumberaMostrar;
            FieldByName('TieneAcoplado').Value              := null;
            FieldByName('DigitoVerificadorPatente').Value   := '';
            FieldByName('DigitoVerificadorValido').Value    := 0;
            //FieldByName('PatenteRVM').Value                 := Vehiculo[i].Vehiculo.Patente;                                    //TASK_106_JMA_20170206
            //FieldByName('DigitoVerificadorPatenteRVM').Value:= '';                                                              //TASK_106_JMA_20170206
            FieldByName('TeleviaNuevo').Value               := True;
            FieldByName('FechaInicioOriginalEximicion').Value   := Null;
            FieldByName('FechaInicioEximicion').Value           := Null;
            FieldByName('FechaFinalizacionEximicion').Value     := Null;
            FieldByName('CodigoMotivoEximicion').Value          := 0;
            FieldByName('FechaInicioEximicionEditable').Value   := True;
            FieldByName('FechaFinEximicionEditable').Value      := True;
            FieldByName('FechaInicioOriginalBonificacion').Value    := Null;
            FieldByName('FechaInicioBonificacion').Value            := Null;
            FieldByName('FechaFinalizacionBonificacion').Value      := Null;
            FieldByName('PorcentajeBonificacion').Value             := 0;
            FieldByName('ImporteBonificacion').Value                := 0;
            FieldByName('FechaInicioBonificacionEditable').Value    := True;
            FieldByName('FechaFinBonificacionEditable').Value       := True;
            FieldByName('FechaAltaCuenta').Value:= Vehiculo[i].FechaCreacion;
            FieldByName('Robado').Value := False;
            FieldByName('Recuperado').Value := False;
            FieldByName('Almacen').Value := almacen;
            FieldByName('FechaDeVencimientoTag').Value := Vehiculo[i].FechaBajaTag;
            FieldByName('CodigoConvenioFacturacion').Value := 0;
            FieldByName('NumeroConvenioFacturacion').Value := '';

            Post;
        end;
    end;
    cdsVehiculosContacto.First;
    btnEditarVehiculo.Enabled:=False;
    btn_AsignarTag.Enabled:=False;
    btnConvFactVehiculo.Enabled:=False;
    btn_Imprimir.Enabled:=True;
    btnEliminarVehiculo.Enabled:=True;
    btn_EximirFacturacion.Enabled:=True;        //TASK_033_ECA_20160610
    btn_BonificarFacturacion.Enabled:=True;     //TASK_033_ECA_20160610
    Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;
//FIN: TASK_051_ECA_20160707

procedure TFormSolicitudContacto.GuardarVehiculosCuentaComercial(CodConvFact: Integer);
begin
    if cdsVehiculosContacto.RecordCount>0 then
       cdsVehiculosContacto.First;

    while not cdsVehiculosContacto.eof do
    begin
            ActualizarVehiculosCuentaComercial(cdsVehiculosContacto.FieldByName('CodigoConvenio').AsInteger,
                                               CodConvFact,
                                               cdsVehiculosContacto.FieldByName('ContractSerialNumber').AsInteger,
                                               UsuarioSistema);

        cdsVehiculosContacto.Next;
    end;
end;

procedure TFormSolicitudContacto.ActualizarVehiculosCuentaComercial(CodigoConvenioRUT,CodigoConvenioFacturacion,CodigoVehiculoRUT: Integer; Usuario: string);
var
    sp: TADOStoredProc;
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'ConvenioCuentasRUT_UPDATE';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenioRUT;
    sp.Parameters.ParamByName('@CodigoConvenioFacturacion').Value := CodigoConvenioFacturacion;
    sp.Parameters.ParamByName('@CodigoVehiculo').Value := CodigoVehiculoRUT;
    sp.Parameters.ParamByName('@Usuario').Value := Usuario;
    sp.Parameters.ParamByName('@opcion').Value := 'I';
    //INICIO: TASK_033_ECA_20160610
    sp.Parameters.ParamByName('@FechaInicioOriginalEximicion').Value := cdsVehiculosContacto.FieldByName('FechaInicioOriginalEximicion').Value;
    sp.Parameters.ParamByName('@FechaInicioEximicion').Value := cdsVehiculosContacto.FieldByName('FechaInicioEximicion').Value;
    sp.Parameters.ParamByName('@FechaFinalizacionEximicion').Value := cdsVehiculosContacto.FieldByName('FechaFinalizacionEximicion').Value;
    sp.Parameters.ParamByName('@CodigoMotivoEximicion').Value := cdsVehiculosContacto.FieldByName('CodigoMotivoEximicion').AsInteger;
    sp.Parameters.ParamByName('@Patente').Value := Trim(cdsVehiculosContacto.FieldByName('Patente').AsString);
    sp.Parameters.ParamByName('@FechaInicioOriginalBonificacion').Value := cdsVehiculosContacto.FieldByName('FechaInicioOriginalBonificacion').Value;
    sp.Parameters.ParamByName('@FechaInicioBonificacion').Value := cdsVehiculosContacto.FieldByName('FechaInicioBonificacion').Value;
    sp.Parameters.ParamByName('@FechaFinalizacionBonificacion').Value := cdsVehiculosContacto.FieldByName('FechaFinalizacionBonificacion').Value;
    sp.Parameters.ParamByName('@PorcentajeBonificacion').Value := cdsVehiculosContacto.FieldByName('PorcentajeBonificacion').Value;
	sp.Parameters.ParamByName('@ImporteBonificacion').Value := cdsVehiculosContacto.FieldByName('ImporteBonificacion').Value;
    //FIN: TASK_033_ECA_20160610
    sp.ExecProc;
    Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;
//FIN: TASK_009_ECA_20160506

//INICIO: TASK_012_ECA_20160514
procedure TFormSolicitudContacto.ActualizarPersonaCuentaComercial(CodigoPersona,CuentaComercial: Integer);
var
    sp: TADOStoredProc;
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'CRM_PersonasCuentaComercial_UPDATE';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
    sp.Parameters.ParamByName('@CuentaComercial').Value := CuentaComercial;
    sp.ExecProc;
    Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;

procedure TFormSolicitudContacto.InicializarValores();
begin
    chkCtaComercialPredeterminada.Visible:=False;
    chkCtaComercialPredeterminada.Checked:=False;
    CuentaComercialPredeterminada:=0;
    CargarTipoConvenios;
    FreDatoPersona.txt_Apellido.Text:='';
    FreDatoPersona.txt_ApellidoMaterno.Text:='';
    {INICIO	: TASK_123_CFU_20170317
    FreDatoPersona.cbSexo.ItemIndex:= 0;
    }
    FreDatoPersona.cbSexo.ItemIndex:= -1;
    //TERMINO	: TASK_123_CFU_20170317
    FreDatoPersona.txt_Nombre.Text:='';
    FreDatoPersona.txtFechaNacimiento.Clear;
    FreDatoPersona.txtApellidoMaterno_Contacto.Text:='';
    FreDatoPersona.txtNombre_Contacto.Text:='';
    FreDatoPersona.txtApellido_Contacto.Text:='';
    FreDatoPersona.txtRazonSocial.Text:='';
    FreDatoPersona.txtGiro.Text:='';
    txtEmailParticular.Text:='';
    txtEMailContacto.Text:='';
    FreTelefonoPrincipal.Limpiar;
    FMDomicilioPrincipal.Limpiar;
    FMDomicilioPrincipal.cb_Comunas.ItemIndex:=0;
    txt_UbicacionFisicaCarpeta.Clear;
    cb_RecInfoMail.Checked:=False;
    //cb_Observaciones.Checked:=False;                                          //TASK_065_GLE_20170408
    //cb_RecibeClavePorMail.Checked:=False;                                     //TASK_065_GLE_20170408
end;
//FIN: TASK_012_ECA_20160514

//INICIO: TASK_016_ECA_20160526
//Se valida que existe al menos una cuenta RNUT con vehiculos activos
function TFormSolicitudContacto.ValidarCuentaRNUTConCuentasActivas(NumeroDocumento: string): Boolean;
var
    CodigoConvenioConCuentas: Integer;
begin
    Result:=False;
    CodigoConvenioConCuentas := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConvenioTipoRNUTConCuentas(''%s'')', [NumeroDocumento]));
    if CodigoConvenioConCuentas<>0 then
        Result:=True;
end;
//FIN: TASK_016_ECA_20160526

//INICIO: TASK_040_ECA_20160628
procedure TFormSolicitudContacto.CargarConvenioClienteTipo;
var
    sp: TADOStoredProc;
    i: Integer;
begin
    Screen.Cursor   := crHourGlass;
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'CRM_ConvenioClienteTipo_SELECT';
    sp.Open;
    if not sp.IsEmpty then
    begin
        cbbTipoCliente.Items.Clear;
         for i := 0 to sp.RecordCount - 1 do
         begin
            cbbTipoCliente.Items.Add(sp.FieldByName('Descripcion').AsString,sp.FieldByName('CodigoTipoCliente').AsString);
            sp.Next;
         end;
         cbbTipoCliente.ItemIndex:=0;
    end;
    sp.Close;
    sp.Free;
    Screen.Cursor   := crDefault;
end;
//FIN: TASK_040_ECA_20160628

//INICIO: TASK_048_ECA_20160706
function TFormSolicitudContacto.DatosConvenio(NumeroConvenio: string; PuntoVenta, PuntoEntrega: Integer): TDatosConvenio;
var
   Datos : TDatosConvenio;
begin
    DatoPersona :=  FreDatoPersona.RegistrodeDatos;
//    Datos.Create(nil);
    with Datos do
    begin
        {PuntoVenta              := PuntoVenta;
        PuntoEntrega            := PuntoEntrega;
        NumeroConvenio          := Trim(NumeroConvenio);
        CodigoConvenio          := 0;
       CodigoConvenioExterno   := 0; }
     //   CodigoPersona           := DatoPersona.CodigoPersona;
        {CodigoEstadoConvenio    := 1; //Alta
        TieneObservacionesBD    := False;
        UbicacionFisicaCarpeta  := FieldByName('UbicacionFisicaCarpeta').AsString;
        NoGenerarIntereses := FieldByName('NoGenerarIntereses').AsBoolean;
        FechaAlta := FieldByName('FechaAlta').AsDateTime;
        FechaActualizacionMedioPago := FieldByName('FechaActualizacionMedioPago').AsDateTime;  //SS_1068_NDR_20120910
        CodigoConcesionaria := FieldByName('CodigoConcesionaria').AsInteger;
        RecibirInfoPorMail := FieldByName('RecibirInfoPorMAil').AsBoolean;
        NumeroConvenioRNUT := Trim(FieldByName('NumeroConvenioRNUT').AsString);
        FechaBajaRNUT := iif(FieldByName('FechaBajaRnut').IsNull, nulldate, FieldByName('FechaBajaRnut').AsDateTime);
        CodigoDomicilioFacturacion := FieldByName('CodigoDomicilioFacturacion').AsInteger;
        Personeria := QueryGetValue(DMConnections.BaseCAC,'select personeria from personas  WITH (NOLOCK) where CodigoPersona = ' + InttoStr(FCodigoPersona));
        DocumentoElectronicoAImprimir := FieldByName('TipoComprobanteFiscalAEmitir').AsString;
        CodigoConvenioFacturacion := FieldByName('CodigoConvenioFacturacion').AsInteger;          //TASK_004_ECA_20160411
        CodigoTipoConvenio        := FieldByName('CodigoTipoConvenio').AsInteger;                 //TASK_004_ECA_20160411
        CodigoClienteFacturacion  := FieldByName('CodigoClienteFacturacion').AsInteger;           //TASK_004_ECA_20160411
        CodigoTipoCliente         := FieldByName('CodigoTipoCliente').AsInteger;   }

    end;

end;
//FIN: TASK_048_ECA_20160706

end.




