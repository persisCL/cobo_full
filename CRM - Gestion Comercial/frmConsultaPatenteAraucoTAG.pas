{-------------------------------------------------------------------------------
File Name       :   FrmConsultaPatenteAraucoTAG.pas
Author          :   mvillarroel
Date Created    :   06/08/2013
Solicitud       :   1118
Description     :   Pantalla que permite la obtenci�n de los accesos de un cliente
                    en Parque Arauco, a trav�s del Rut o una patente definida.
-------------------------------------------------------------------------------}

unit frmConsultaPatenteAraucoTAG;

interface

uses
  DMConnection,
  Util,
  UtilDB,
  UtilProc,
  PeaProcs,
  PeaTypes,
  BuscaClientes,
  ADODB,
  SysUtilsCN,
  //Otros
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, DmiCtrls, ExtCtrls, ListBoxEx, DBListEx,
  DB, ComCtrls, Validate, DateEdit, PeaProcsCN;

type
  TConsultaPatenteAraucoTAGForm = class(TForm)
    pnl1: TPanel;
    grpFiltros: TGroupBox;
    lbl1: TLabel;
    btn_Filtrar: TButton;
    btn_Limpiar: TButton;
    pnl2: TPanel;
    lbl2: TLabel;
    edtPatente: TEdit;
    edtRUTCliente: TPickEdit;
    spObtenerPatentesConvenio: TADOStoredProc;
    spObtenerHistoricoListaDeAccesoConvenio: TADOStoredProc;
    dsObtenerHistoricoListaDeAccesoConvenio: TDataSource;
    spObtenerHistoricoConveniosInhabilitados: TADOStoredProc;
    dsObtenerHistoricoConveniosInhabilitados: TDataSource;
    cbbPatente: TVariantComboBox;
    cbbConvenio: TVariantComboBox;
    lbl3: TLabel;
    edtHistListaAccesoDesde: TDateEdit;
    edtHistListaAccesoHasta: TDateEdit;
    lbl4: TLabel;
    lbl6: TLabel;
    pnl3: TPanel;
    pnl4: TPanel;
    spl1: TSplitter;
    grp1: TGroupBox;
    lstDBLEAdhesionPA: TDBListEx;
    grp2: TGroupBox;
    lstDBLEInhabilitaciones: TDBListEx;
    pnl5: TPanel;
    grp3: TGroupBox;
    lblApellidoNombre: TLabel;
    lbl5: TLabel;
    lblLDomicilio: TLabel;
    lbl7: TLabel;
    spObtenerCliente: TADOStoredProc;
    spObtenerConvenioPorPatente: TADOStoredProc;
    btnSalir: TButton;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    procedure edtRUTClienteOnClick(Sender: TObject);
    procedure edtRUTClienteKeyPress(Sender: TObject; var Key: Char);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure cbbConvenioChange(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure cbbConvenioDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBLEAdhesionPAColumnsHeaderClick(Sender: TObject);
    procedure edtRUTClienteKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPatenteKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtRUTClienteChange(Sender: TObject);
    procedure edtPatenteChange(Sender: TObject);
    procedure ObtenerDomicilio;



  private
    { Private declarations }
    FUltimaBusqueda : TBusquedaCliente;
    Function CargarCliente(RutCliente: String): Boolean;

  public
    { Public declarations }
    Function Inicializar : boolean;
    Function CargarComboPatentes:Boolean;
    Procedure CargarTablaHistoricosListaAcceso;
    Procedure CargarTablaHistoricosInhabilitadosListaAcceso;
    Procedure LimpiaCampos;

  end;

var
  ConsultaPatenteAraucoTAGForm: TConsultaPatenteAraucoTAGForm;

implementation

{$R *.dfm}

{------------------------------------------------------------------------------
Function Name  :   Inicializar
Description    :   Funci�n que se ejecuta al momento de iniciar el formulario.
------------------------------------------------------------------------------}
function TConsultaPatenteAraucoTAGForm.Inicializar;

begin
       edtRUTCliente.setFocus;
       LimpiaCampos;
       Result := True;
end;

{------------------------------------------------------------------------------
Procedure Name  :   edtPatenteChange
Description    :   Procedimiento encargado de limpiar en caso de que se cambie el
                    valor de la patente y el foco est� en ella.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.edtPatenteChange(Sender: TObject);
begin

   if edtPatente.Focused then begin

        edtRUTCliente.Clear;
        lblApellidoNombre.Caption := '';
        lblLDomicilio.Caption := '';
        cbbPatente.Items.Clear;
        cbbConvenio.Items.Clear;
        edtHistListaAccesoDesde.Clear;
        edtHistListaAccesoHasta.Clear;
        if spObtenerHistoricoListaDeAccesoConvenio.Active  then spObtenerHistoricoListaDeAccesoConvenio.Close;
        if spObtenerHistoricoConveniosInhabilitados.Active then spObtenerHistoricoConveniosInhabilitados.Close;
        if btn_Filtrar.Enabled then btn_Filtrar.Enabled := False;

   end;

end;

{------------------------------------------------------------------------------
Procedure Name  :   edtPatenteKeyUp
Description    :   Procedimiento que valida si se presiona ENTER para realizar
                    la consulta.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.edtPatenteKeyUp(Sender: TObject;
var Key: Word; Shift: TShiftState);
var
  convenio: Integer;
resourcestring
	MSG_ERROR_PATENTES   = 'No se encontraron convenios para la Patente ingresada.';

begin

  if Key = 13 then begin




      with spObtenerConvenioPorPatente do begin
            close;
              Parameters.ParamByName('@Patente').value := edtPatente.text;
            Open;
            if not eof then begin

               convenio := FieldByName('CodigoConvenio').AsInteger;
              end;


           edtRUTCliente.Text := FieldByName('NumeroDocumento').AsString;
           CargarConveniosRUT(DMCOnnections.BaseCAC, cbbConvenio, 'RUT', edtRUTCliente.Text, False, convenio);
           CargarCliente(FieldByName('NumeroDocumento').AsString);


      end;

       if not ValidateControls([edtPatente],
        [not spObtenerConvenioPorPatente.IsEmpty],
        Caption,
         [MSG_ERROR_PATENTES]) then Exit;

       cbbConvenio.OnChange(Self);

       btn_Filtrar.Enabled := True;

  end;

end;




{------------------------------------------------------------------------------
Procedure Name  :   edtRUTClienteChange
Description    :   Procedimiento encargado de limpiar en caso de que se cambie el
                    valor del Rut y el foco en �l.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.edtRUTClienteChange(Sender: TObject);
begin

    if edtRUTCliente.Focused then begin

        if btn_Filtrar.Enabled then btn_Filtrar.Enabled := False;
        if cbbPatente.Visible = True then cbbPatente.Visible := False;
        if edtPatente.Visible = False then edtPatente.Visible := True;
        edtPatente.Text :=    EmptyStr;
        cbbPatente.Items.Clear;
        cbbConvenio.Items.Clear;
        edtHistListaAccesoDesde.Clear;
        edtHistListaAccesoHasta.Clear;
        if spObtenerHistoricoListaDeAccesoConvenio.Active   then spObtenerHistoricoListaDeAccesoConvenio.Close;
        if spObtenerHistoricoConveniosInhabilitados.Active  then spObtenerHistoricoConveniosInhabilitados.Close;
        edtPatente.Clear;
        lblApellidoNombre.Caption := '';
        lblLDomicilio.Caption := '';

    end;

end;

{------------------------------------------------------------------------------
Procedure Name  :   edtRUTClienteKeyPress
Description     :   Limita los caracteres a ingresar para el RUT, a n�meros y K.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.edtRUTClienteKeyPress(Sender: TObject;
  var Key: Char);
begin
    Key := UpCase(KEY);
      if not (Key  in ['0'..'9','K','k', #8]) then
         Key := #0
end;

{------------------------------------------------------------------------------
Procedure Name  :   edtRUTClienteKeyUp
Description    :   Procedimiento que valida si se presiona ENTER para realizar
                    la consulta o si se Borran alg�n datos para limpiar el formulario.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.edtRUTClienteKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
ResourceString
   rsRUTNoValido = 'El RUT ingresado NO es v�lido.';
   rsNoExistenConvenios = 'No se encontraron convenios para el Rut ingresado.';
    
begin

    if Key = 13 then begin


        if not ValidateControls([edtRUTCliente],
        [ValidarRUT(DMConnections.BaseCAC, edtRUTCliente.Text)],
        Caption,
         [rsRUTNoValido]) then Exit;

        if Length(trim(edtRUTCliente.Text))= 8 then edtRUTCliente.Text := '0' + edtRUTCliente.Text;


        CargarConveniosRUT(DMCOnnections.BaseCAC, cbbConvenio, 'RUT', edtRUTCliente.Text);

        if not ValidateControls([edtRUTCliente],
        [cbbConvenio.Items.Count > 0],
        Caption,
         [rsNoExistenConvenios]) then Exit;

        CargarComboPatentes;
        CargarCliente(edtRUTCliente.Text);
        btn_Filtrar.Enabled := True;


    end;

end;

{------------------------------------------------------------------------------
Procedure Name  :   edtRUTClienteOnClick
Description     :   Este SP permite la habilitaci�n de la ventana que ayuda a
                    encontrar al cliente con mayores par�metros de b�squeda.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.edtRUTClienteOnClick(Sender: TObject);
var
    f: TFormBuscaClientes;
    letra: Word;
begin
    FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);

      application.createForm(TFormBuscaClientes,F);
      if F.Inicializa(FUltimaBusqueda) then begin
          F.ShowModal;
          if F.ModalResult = idOk then begin
              FUltimaBusqueda       :=    F.UltimaBusqueda;
              edtRUTCliente.Text    :=    F.Persona.NumeroDocumento;
              edtRUTCliente.setFocus;
              letra := 13;
              edtRUTCliente.OnKeyUp( nil, letra, [ ssShift ] );

          end;
      end;
      F.free;
end;

{------------------------------------------------------------------------------
Procedure Name  :   FormClose
Description    :   Procedimiento que ayuda a cerrar el formulario.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   Action:= caFree;
end;

{------------------------------------------------------------------------------
Procedure Name  :   FormDestroy
Description    :   Procedimiento que ayuda a cerrar el formulario.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.FormDestroy(Sender: TObject);
begin
  ConsultaPatenteAraucoTAGForm := nil;
end;

{------------------------------------------------------------------------------
Procedure Name  :   btnSalirClick
Description     :   Este SP cierra la ventana actual.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.btnSalirClick(Sender: TObject);
begin
    close;
end;

{------------------------------------------------------------------------------
Procedure Name  :   btn_FiltrarClick
Description     :   Se llama a las funciones que entregan los datos a las tablas
                    con el hisorial de acceso.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.btn_FiltrarClick(Sender: TObject);
ResourceString
  rsMSG_NO_HAY_DATOS = 'No se encontraron registros para los datos ingresados';
  rsFechaNoValida = 'Fecha hasta es menor a fecha desde.';

begin


      if not ValidateControls([ edtHistListaAccesoHasta],
            [edtHistListaAccesoDesde.Date <= edtHistListaAccesoHasta.Date],
            Caption,
             [rsFechaNoValida]) then Exit;


    CargarTablaHistoricosListaAcceso;
    CargarTablaHistoricosInhabilitadosListaAcceso;

     if (spObtenerHistoricoConveniosInhabilitados.IsEmpty)  and (spObtenerHistoricoListaDeAccesoConvenio.IsEmpty ) then begin

         MsgBox(rsMSG_NO_HAY_DATOS, Caption, MB_ICONEXCLAMATION);

     end;



end;

{------------------------------------------------------------------------------
Procedure Name  :   btn_LimpiarClick
Description     :   Se llama a la funci�n que limpia los valores del formulario.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.btn_LimpiarClick(Sender: TObject);
begin
    LimpiaCampos;
end;

{------------------------------------------------------------------------------
Function Name   :   LimpiaCampos
Description     :   Se limpian los datos del formulario.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.LimpiaCampos;
begin
      edtRUTCliente.Clear;
      edtPatente.Clear;
      cbbPatente.Items.Clear;
      cbbConvenio.Items.Clear;
      edtHistListaAccesoDesde.Clear;
      edtHistListaAccesoHasta.Clear;
     if spObtenerHistoricoListaDeAccesoConvenio.Active then spObtenerHistoricoListaDeAccesoConvenio.Close;
     if spObtenerHistoricoConveniosInhabilitados.Active then spObtenerHistoricoConveniosInhabilitados.Close;
     if btn_Filtrar.Enabled then btn_Filtrar.Enabled := False;
      lblApellidoNombre.Caption := '';
      lblLDomicilio.Caption := '';

      if   cbbPatente.Visible then begin

        cbbPatente.Visible                :=    False;
        edtPatente.Visible                :=    True;

      end
      else begin

        edtPatente.Visible                :=    True;

      end;

end;


{------------------------------------------------------------------------------
Procedure Name  :   lstDBLEAdhesionPAColumns
Description     :   Permite ordenar la tabla al pinchar en la cabecera.
------------------------------------------------------------------------------}

procedure TConsultaPatenteAraucoTAGForm.DBLEAdhesionPAColumnsHeaderClick(
  Sender: TObject);
begin
    DBListExColumnHeaderClickGenerico(Sender);
end;

{------------------------------------------------------------------------------
Function Name   :   CargarComboPatentes
Description     :   Se cargan las patentes de acuerdo a los convenios encontrados
                    del RUT ingresado.
------------------------------------------------------------------------------}
Function TConsultaPatenteAraucoTAGForm.CargarComboPatentes:Boolean;
resourcestring
	MSG_ERROR            = 'Error';
	MSG_ERROR_PATENTES   = 'Error obteniendo las patentes del convenio';
var
 i: Integer;
begin

    try

           i := -1;

           with spObtenerPatentesConvenio do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoConvenio').Value   :=    cbbConvenio.Value;
            Parameters.ParamByName('@TraerBajas').Value       :=    False;
            Open;

              cbbPatente.Clear;
              cbbPatente.Items.Add('Todas', -1);



                while not Eof do begin

                  cbbPatente.Items.Add(FieldByName('Patente').AsString, 0);

                  if (edtPatente.text <> '') and  (Trim(FieldByName('Patente').AsString) = edtPatente.text) then begin

                     i := cbbPatente.Items.Count - 1;


                  end;

                  Next;
               end;



           end;

           cbbPatente.Visible                :=    True;
           edtPatente.Visible                :=    False;
           edtPatente.Text                   :=    EmptyStr;


            if (i <> -1) then cbbPatente.ItemIndex := i
              else cbbPatente.ItemIndex := 0;


          Result := True;

    except
          on e: exception do begin

            MsgBoxErr( MSG_ERROR_PATENTES, e.Message, MSG_ERROR, MB_ICONSTOP);
            Result := False;

          end;
    end;

 end;

{------------------------------------------------------------------------------
Procedure Name   :   CargarTablaHistoricosListaAcceso
Description     :   Se carga la tabla con las listas de acceso de Parque Arauco,
                    seg�n la patente seleccionada.
------------------------------------------------------------------------------}
Procedure TConsultaPatenteAraucoTAGForm.CargarTablaHistoricosListaAcceso;

ResourceString
    TXT_ERROR_HISTORICO_LISTASDEACCESO = 'Error buscando el historico de adhesion a lista de acceso ';
    MSG_ERROR                          = 'Error';


begin



   with spObtenerHistoricoListaDeAccesoConvenio do begin

     try
        Close;
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoConvenio').Value        :=    cbbConvenio.Value;

         if cbbPatente.Items.Count > 0 then begin

            if cbbPatente.ItemIndex > 0 then  begin

              Parameters.ParamByName('@Patente').Value         :=    cbbPatente.text;

            end
            else begin

              Parameters.ParamByName('@Patente').Value         :=    null;

            end;

          end
         else begin

             Parameters.ParamByName('@Patente').Value          :=    edtPatente.Text;

         end;


        if edtHistListaAccesoDesde.Date <> NullDate then  begin

            Parameters.ParamByName('@FechaDesde').Value     :=    edtHistListaAccesoDesde.Date;
        end
        else begin

            Parameters.ParamByName('@FechaDesde').Value     :=    Null;

        end;

        if edtHistListaAccesoHasta.Date <> NullDate then    begin

            Parameters.ParamByName('@FechaHasta').Value     :=    edtHistListaAccesoHasta.Date;
        end
        else begin

            Parameters.ParamByName('@FechaHasta').Value     :=    Null;

        end;

        Open;

      except
        on e: Exception do begin

            MsgBoxErr(TXT_ERROR_HISTORICO_LISTASDEACCESO, e.Message, MSG_ERROR, MB_ICONWARNING);

        end;

      end;

   end;
end;

{------------------------------------------------------------------------------
Function Name   :   cbbConvenioChange
Description     :   Al cambiar el convenio se llena el combo de la patente.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.cbbConvenioChange(Sender: TObject);
begin

        edtHistListaAccesoDesde.Clear;
        edtHistListaAccesoHasta.Clear;
        if spObtenerHistoricoListaDeAccesoConvenio.Active then spObtenerHistoricoListaDeAccesoConvenio.Close;
        if spObtenerHistoricoConveniosInhabilitados.Active then spObtenerHistoricoConveniosInhabilitados.Close;

        CargarComboPatentes;

end;


{------------------------------------------------------------------------------
Procedure Name   :  cbbConvenioDrawItem
Description     :   Procedimiento que identifica los convenios de baja para
                    asignar los colores.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.cbbConvenioDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbbConvenio.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;                                                                                    

end;

{------------------------------------------------------------------------------
Procedure Name   :   CargarTablaHistoricosInhabilitadosListaAcceso
Description     :   Se carga la tabla con las listas de historico de inhabilitaciones,
                    seg�n el convenio seleccionado.
------------------------------------------------------------------------------}
Procedure TConsultaPatenteAraucoTAGForm.CargarTablaHistoricosInhabilitadosListaAcceso;
ResourceString
    TXT_ERROR_HISTORICO_INHABILITACIONES = 'Error buscando el historico de inhabilitaciones ';
    MSG_ERROR                            = 'Error';


begin


   with spObtenerHistoricoConveniosInhabilitados do begin
     try
          Close;
          Parameters.Refresh;
          Parameters.ParamByName('@CodigoConvenio').Value   :=    cbbConvenio.Value;

          if edtHistListaAccesoDesde.Date <> NullDate then begin

              Parameters.ParamByName('@FechaDesde').Value   :=    edtHistListaAccesoDesde.Date;

          end
          else begin

              Parameters.ParamByName('@FechaDesde').Value   :=    Null;

          end;

          if edtHistListaAccesoHasta.Date <> NullDate then begin

              Parameters.ParamByName('@FechaHasta').Value   :=    edtHistListaAccesoHasta.Date;
          end
          else begin

               Parameters.ParamByName('@FechaHasta').Value  :=    Null;

          end;

          Open;

      except
          on e: Exception do begin

              MsgBoxErr(TXT_ERROR_HISTORICO_INHABILITACIONES, e.Message, MSG_ERROR, MB_ICONWARNING);

          end;
      end;
   end;
end;


{------------------------------------------------------------------------------
Function Name  :   CargarCliente
Description    :   Funci�n para cargar los datos del cliente.
------------------------------------------------------------------------------}
function TConsultaPatenteAraucoTAGForm.CargarCliente(RutCliente: String): Boolean;
var
  FApellido, FApellidoMaterno, FNombre: AnsiString;
begin

    with spObtenerCliente do begin
	    close;
       if (length(RutCliente) = 8) then begin
            RutCliente := '0' + RutCliente;
       end;

	      Parameters.ParamByName('@NumeroDocumento').value  :=  RutCliente;
        Parameters.ParamByName('@CodigoDocumento').value  :=  TIPO_DOCUMENTO_RUT;
	    Open;
        if not eof then begin

            FApellido         :=  Trim(FieldByName('Apellido').asString);
            FApellidoMaterno  :=  Trim(FieldByName('ApellidoMaterno').asString);
            FNombre           :=  Trim(FieldByName('Nombre').asString);

            if (FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
                lblApellidoNombre.Caption :=  ArmaNombreCompleto(FApellido, FApellidoMaterno, FNombre)
            else
                lblApellidoNombre.Caption :=  ArmaRazonSocial(FApellido, FNombre);

            ObtenerDomicilio;

        end else begin

            lblApellidoNombre.Caption := '';

        end;
    end;

      Result := True;

end;



{------------------------------------------------------------------------------
Function Name  :   CargarCliente
Description    :   Funci�n para cargar los datos del cliente.
------------------------------------------------------------------------------}
procedure TConsultaPatenteAraucoTAGForm.ObtenerDomicilio;
resourcestring
	MSG_ERROR                   = 'Error';
	MSG_ERROR_GETTING_ADDRESS   = 'Error obteniendo el domicilio del cliente';
begin
    if cbbConvenio.ItemIndex >= 0 then begin

      with spObtenerDomicilioFacturacion, Parameters do begin

          Close;
          ParamByName('@CodigoConvenio').Value := cbbConvenio.Value;
        	try
        		ExecProc;
                lblLDomicilio.Caption := ParamByName('@Domicilio').Value + ' - ' + ParamByName('@Comuna').Value + ' - ' + ParamByName('@Region').Value;
    			except
        		on e: exception do begin
            		MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            	end;
        	end;
    	end;
    end else lblLDomicilio.Caption := EmptyStr;
end;



end.

