{********************************** File Header ********************************
File Name   : frmConsultaTransitosARevalidar
Author      : mbecerra
Date Created: 07-04-2008
Language    : ES-CL
Description : Permite obtener un listado de los tr�nsitos a revalidar.
            	Condiciones: para ello invoca el PA ObtenerTrasitosARevalidar

Revision 1:
Author: mbecerra
Date: 22-04-2008
Description:
            A petici�n del cliente se agrega que los transitos con estado 15 sean
            visualizados tambi�n.
            Y que los transitos en estado 15, 17 y 18 sean visualizados en rojo,
            pero que no puedan ser seleccionados para ser revalidados.

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.

Firma       : SS_1147_NDR_20140603
Descripcion : Se corrige observacion de que no se limpia el conteo de registros al presionar LIMPIAR (Observaciones Q&A 1147)
--------------------------------------------------------------------------------}

unit frmConsultaTransitosARevalidar;

interface

uses
  DmConnection,
  UtilProc,
  peaprocs,
  util,
  UtilDB,
  Peatypes,
  {INICIO:	20160721 CFU
  ImagenesTransitos,
  frmRevalidarTransito,
  }
  ImagenesTransitosSPC,
  frmRevalidarTransitoSPC,
  //TERMINO: 20160721 CFU
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ListBoxEx, DBListEx, StdCtrls, VariantComboBox, TimeEdit,
  Validate, DateEdit, StrUtils, DB, ADODB, ImgList;

type
  TConsultaTransitosARevalidarForm = class(TForm)
    Panel4: TPanel;
    lblTotales: TLabel;
    btnSalir: TButton;
    dsTransitos: TDataSource;
    ilCheck: TImageList;
    ilCamara: TImageList;
    spObtenerTransitosARevalidar: TADOStoredProc;
    dblTransitos: TDBListEx;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    txtFechaDesde: TDateEdit;
    txtFechaHasta: TDateEdit;
    txtHoraDesde: TTimeEdit;
    txtHoraHasta: TTimeEdit;
    btnRevalidar: TButton;
    btnFiltrar: TButton;
    btnLimpiar: TButton;
    txtPatente: TEdit;
    procedure btnLimpiarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure dblTransitosCheckLink(Sender: TCustomDBListEx;
      Column: TDBListExColumn; var IsLink: Boolean);
    procedure dblTransitosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblTransitosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnRevalidarClick(Sender: TObject);
    procedure txtPatenteKeyPress(Sender: TObject; var Key: Char);
    procedure OrdenarPorEncabezado(Sender: TObject);
    procedure dblTransitosKeyPress(Sender: TObject; var Key: Char);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
  end;

var
  ConsultaTransitosARevalidarForm: TConsultaTransitosARevalidarForm;

implementation

{$R *.dfm}

const
	CONST_COLUMNA_IMAGEN = 9;

{Version 1
	Se puso valores fijos, ya que las constantes del tipo
    dbo.CONST_ESTADO_TRANSITO_POSIBLE_INFRACTOR()  no fueron encontradas para los
    c�digos 17 y 18}
    ESTADO_TRANSITO_INFRACCION_FACTURADA = 15;
    ESTADO_TRANSITO_NO_FACTURABLE_RNUT   = 17;
    ESTADO_TRANSITO_NO_FACTURABLE_POSIBLE_INFRACTOR = 18;

{******************************** Function Header ******************************
Function Name: btnFiltrarClick
Author : mbecerra
Date Created : 03/04/2008
Description : realiza una validaci�n de los par�metros ingresado y luego invoca
            	al procedimiento almacenado ObtenerTransitosARevalidar
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TConsultaTransitosARevalidarForm.btnFiltrarClick(Sender: TObject);
resourcestring
    MSG_LICENSE_PLATE			= 'Debe ingresar un valor de patente';
    MSG_ERROR_LOADING_TRANSITOS	= 'Error cargando Tr�nsitos. SP: ObtenerTransitosARevalidar';
    MSG_VALIDAR_ORDEN_FECHAS	= 'La Fecha desde no puede ser superior a la Fecha Hasta';
    MSG_BUSQUEDA_VACIA			= 'No se encontr� ning�n registro con esos par�metros de b�squeda';
begin
	if Trim(txtPatente.Text) = '' then begin
    	MsgBoxBalloon(MSG_LICENSE_PLATE, Caption, MB_ICONSTOP, txtPatente);
        Exit;
    end;

    if (txtFechaDesde.Date <> nulldate) and (txtFechaHasta.Date <> nulldate) and (txtFechaDesde.Date > txtFechaHasta.Date) then begin
    	MsgBoxBalloon(MSG_VALIDAR_ORDEN_FECHAS, Caption, MB_ICONSTOP, txtFechaDesde);
        Exit;
    end;

    {limpiar lista de tr�nsitos ==> no hay ninguno seleccionado}
    lstListaTransitosARevalidar.Clear;
    lstListaPatenteyCategoriaARevalidar.Clear;

    spObtenerTransitosARevalidar.DisableControls;
    try
        spObtenerTransitosARevalidar.Close;
        spObtenerTransitosARevalidar.CommandTimeout := 500;
        spObtenerTransitosARevalidar.Parameters.ParamByName('@Patente').Value		:= iif(txtPatente.Text = '', NULL, txtPatente.Text);
        spObtenerTransitosARevalidar.Parameters.ParamByName('@FechaDesde').Value	:= iif(txtFechaDesde.Date = NullDate, Null, FormatDateTime('yyyymmdd hh:nn',(txtFechaDesde.Date + txtHoraDesde.Time)));
        spObtenerTransitosARevalidar.Parameters.ParamByName('@FechaHasta').Value	:= iif(txtFechaHasta.Date = NullDate, Null, FormatDateTime('yyyymmdd hh:nn',(txtFechaHasta.Date + txtHoraHasta.Time)));

		if not(OpenTables([spObtenerTransitosARevalidar])) then begin
			  MsgBox(MSG_ERROR_LOADING_TRANSITOS, Caption, MB_ICONERROR);
        end;

        if spObtenerTransitosARevalidar.IsEmpty then begin
            lblTotales.Caption := '';
    	end
        else begin
        	lblTotales.Caption := 'Visualizados ' + IntToStr(spObtenerTransitosARevalidar.RecordCount) +	' tr�nsitos';
    	end;

        if spObtenerTransitosARevalidar.IsEmpty then begin
            MsgBox(MSG_BUSQUEDA_VACIA, Caption, MB_ICONINFORMATION);
        end;

    finally
	    spObtenerTransitosARevalidar.EnableControls;
    end;

	dblTransitos.SetFocus;

end;

{******************************** Function Header ******************************
Function Name: btnLimpiarClick
Author : mbecerra
Date Created : 03/04/2008
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TConsultaTransitosARevalidarForm.btnLimpiarClick(Sender: TObject);
begin
    txtFechaHasta.Clear;
    txtFechaDesde.Clear;
    txtPatente.Clear;
    txtHoraDesde.Time := 0;
    txtHoraHasta.Time := 0;

    lblTotales.Caption := '';                                                   //SS_1147_NDR_20140603

    if spObtenerTransitosARevalidar.Active then
    	spObtenerTransitosARevalidar.Close;
end;

procedure TConsultaTransitosARevalidarForm.btnRevalidarClick(Sender: TObject);
ResourceString
    DEBE_SELECCIONAR = 'Debe seleccionar al menos un Tr�nsito';
    
var
	{INICIO:  20160721 CFU
	f: TRevalidarTransitoForm;
	}
	f: TRevalidarTransitoSPCForm;
	//TERMINO:	20160721 CFU
begin
    if lstListaTransitosARevalidar.Count <= 0 then begin
        MsgBox(DEBE_SELECCIONAR, Caption, MB_OK + MB_ICONEXCLAMATION);
    	exit;
    end; 
    
    {INICIO:	20160721 CFU
	Application.CreateForm(TRevalidarTransitoForm, f);
    f.Inicializar('Revalidar - Cambiar Patente y/o Categor�a');
    }
    Application.CreateForm(TRevalidarTransitoSPCForm, f);
    f.Inicializar('Revalidar - Cambiar Patente');
    //TERMINO:	20160721 CFU
    f.ShowModal;
    f.Free;

    {refrescar la lista para visualizar los cambio que hizo el usuario}
    btnFiltrar.Click;

end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : mbecerra
Date Created : 03/04/2008
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TConsultaTransitosARevalidarForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TConsultaTransitosARevalidarForm.dblTransitosCheckLink(
  Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
    if Column = dblTransitos.Columns[CONST_COLUMNA_IMAGEN] then begin
		IsLink := (spObtenerTransitosARevalidar.FieldByName('RegistrationAccessibility').AsInteger > 0);
    end;
end;


{******************************** Function Header ******************************
Function Name: OrdenarPorEncabezado
Author : mbecerra
Date Created : 23/04/2008
Description : Ordena el conjunto de datos de acuerdo al header seleccionado por el operador
Parameters : Sender : TObject (header clicked)
Return Value : None
*******************************************************************************}
procedure TConsultaTransitosARevalidarForm.OrdenarPorEncabezado(
  Sender: TObject);
begin
	if spObtenerTransitosARevalidar.IsEmpty then Exit;

	  if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   	  else TDBListExColumn(sender).Sorting := csAscending;

	  spObtenerTransitosARevalidar.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : mbecerra
Date Created : 03/04/2008
Description : El objetivo es dibujar la camara que indica que hay imagen referencial
Parameters : Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean
Return Value : None
*******************************************************************************}
procedure TConsultaTransitosARevalidarForm.dblTransitosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    EsEstadoNoCheck : boolean;
begin
	{revision 1: No colocar la Check si el estado es 15, 17 y 18}
    EsEstadoNoCheck := (spObtenerTransitosARevalidar.FieldByName('CodigoEstado').AsInteger in
    	[ESTADO_TRANSITO_INFRACCION_FACTURADA,
         ESTADO_TRANSITO_NO_FACTURABLE_RNUT,
         ESTADO_TRANSITO_NO_FACTURABLE_POSIBLE_INFRACTOR]);
    if Column = dblTransitos.Columns[0] then begin
    	if EsEstadoNoCheck  then begin
        	DefaultDraw := True;
        end
        else begin
        	ilCheck.Draw(dblTransitos.Canvas, Rect.Left + 5, Rect.Top + 1, iif(lstListaTransitosARevalidar.IndexOf(spObtenerTransitosARevalidar.FieldByName('NumCorrCA').AsString) >= 0, 1, 0));
        	DefaultDraw := False;
        	ItemWidth   := ilCheck.Width + 2;
        end;
    end
    else if (Column = dblTransitos.Columns[CONST_COLUMNA_IMAGEN]) and (spObtenerTransitosARevalidar.FieldByName('RegistrationAccessibility').AsInteger > 0) then begin
		ilCamara.Draw(dblTransitos.Canvas, Rect.Left + 20, Rect.Top + 1, iif(spObtenerTransitosARevalidar.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0));
		DefaultDraw := False;
		ItemWidth   := ilCamara.Width + 2;
    end;

    {revisi�n 1: pintar de rojo toda la fila para aquellos tr�nsitos que est�n en
     estado 15,17,y 18}
    if EsEstadoNoCheck then begin
    	Sender.Canvas.Font.Color := clRed;
    end
    else begin
    	if odSelected in State then
        	Sender.Canvas.Font.Color := clWindow
        else
        	Sender.Canvas.Font.Color := clWindowText;
    end;



end;

procedure TConsultaTransitosARevalidarForm.dblTransitosKeyPress(Sender: TObject;
  var Key: Char);
begin
	if Key = ' ' then begin
    	dblTransitos.OnLinkClick(dblTransitos, dblTransitos.Columns[0]);
    end;

end;

{********************************** Function Header ****************************
Function Name: FormClose
Author : mbecerra
Date Created : 07/04/2008
Description :
Parameters : Sender: TCustomDBListEx; Column: TDBListExColumn
Return Value : None
*******************************************************************************}
procedure TConsultaTransitosARevalidarForm.dblTransitosLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
var
    i : integer;
    NumCorrCA, Estado, Categoria, Patente, CodigoCat, CodigoEst, Fecha, Portico  : string;
    EsItaliano : string;   // SS_1091_CQU_20130516
    f: TfrmImagenesTransitosSPC;
begin
    if (Column = dblTransitos.Columns[0]) and
    	(not (spObtenerTransitosARevalidar.FieldByName('CodigoEstado').AsInteger in
        	 [ESTADO_TRANSITO_INFRACCION_FACTURADA,
    	     ESTADO_TRANSITO_NO_FACTURABLE_RNUT,
       	     ESTADO_TRANSITO_NO_FACTURABLE_POSIBLE_INFRACTOR]) ) then begin
    	NumCorrCA := Trim(spObtenerTransitosARevalidar.FieldByName('NumCorrCA').AsString);
        Patente   := Trim(spObtenerTransitosARevalidar.FieldByName('Patente').AsString);
        Estado    := Trim(spObtenerTransitosARevalidar.FieldByName('Estado').AsString);
        Categoria := Trim(spObtenerTransitosARevalidar.FieldByName('Categoria').AsString);
        CodigoCat := Trim(spObtenerTransitosARevalidar.FieldByName('CodigoCategoria').AsString);
        CodigoEst := Trim(spObtenerTransitosARevalidar.FieldByName('CodigoEstado').AsString);
        Fecha     := Trim(FormatDateTime('ddmmyyyyhhnnsszz', spObtenerTransitosARevalidar.FieldByName('FechaHoraTransito').AsDateTime));
        Portico   := Trim(spObtenerTransitosARevalidar.FieldByName('Portico').AsString);
        EsItaliano:= BoolToStr(spObtenerTransitosARevalidar.FieldByName('EsItaliano').AsBoolean);  // SS_1091_CQU_20130516
        i := lstListaTransitosARevalidar.IndexOf(NumCorrCA);
        if i >= 0 then begin
        	lstListaTransitosARevalidar.Delete(i);
            lstListaPatenteyCategoriaARevalidar.Delete(i);
        end
        else begin
        	lstListaTransitosARevalidar.Add(NumCorrCa);
            //lstListaPatenteyCategoriaARevalidar.Add(NumCorrCA + #9 + Patente + #9 + Estado + #9 + Categoria + #9 + CodigoCat + #9 + CodigoEst + #9 + Fecha + #9 + Portico);                   // SS_1091_CQU_20130516
            lstListaPatenteyCategoriaARevalidar.Add(NumCorrCA + #9 + Patente + #9 + Estado + #9 + Categoria + #9 + CodigoCat + #9 + CodigoEst + #9 + Fecha + #9 + Portico + #9 + EsItaliano);   // SS_1091_CQU_20130516
        end;

        Sender.Invalidate;
        Sender.Repaint;

    end
	else if Column = dblTransitos.Columns[CONST_COLUMNA_IMAGEN] then begin
		//MostrarVentanaImagen(Self, spObtenerTransitosARevalidar['NumCorrCA'], spObtenerTransitosARevalidar['FechaHoraTransito']); // SS_1091_CQU_20130516
        {INICIO:	20160722 CFU
        MostrarVentanaImagen(Self,                                                                                                  // SS_1091_CQU_20130516
            spObtenerTransitosARevalidar['NumCorrCA'],                                                                              // SS_1091_CQU_20130516
            spObtenerTransitosARevalidar['FechaHoraTransito'],                                                                      // SS_1091_CQU_20130516
            spObtenerTransitosARevalidar['EsItaliano']);                                                                            // SS_1091_CQU_20130516
        }
    	Application.CreateForm(TfrmImagenesTransitosSPC, f);
    	f.MostrarVentanaImagen(Self,
            spObtenerTransitosARevalidar['NumCorrCA'],
            spObtenerTransitosARevalidar['RegistrationAccessibility']);
    	f.ShowModal;
    	f.Free;
        //TERMINO:	20160722 CFU
	end;
end;

procedure TConsultaTransitosARevalidarForm.Edit1KeyPress(Sender: TObject;
  var Key: Char);
begin
  TEdit(Sender).Text := TEdit(Sender).Text + IntToStr(Ord(Key)) + '-' 
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : mbecerra
Date Created : 03/04/2008
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TConsultaTransitosARevalidarForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
    lstListaTransitosARevalidar.Free;
    lstListaPatenteyCategoriaARevalidar.Free;
end;

{******************************** Function Header ******************************
Function Name: FormShow
Author : mbecerra
Date Created : 03/04/2008
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TConsultaTransitosARevalidarForm.FormShow(Sender: TObject);
begin
	txtPatente.SetFocus;
	lstListaTransitosARevalidar         := TStringList.Create;
    lstListaPatenteyCategoriaARevalidar := TStringList.Create;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author : mbecerra
Date Created : 03/04/2008
Description : Inicializa la ventana del formulario
Parameters : txtCaption: String; MDIChild: Boolean
Return Value : None
*******************************************************************************}
function TConsultaTransitosARevalidarForm.Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
    var S: TSize;
begin
	Result := True;
	try
    	if MDIChild then begin
            S := GetFormClientSize(Application.MainForm);
            SetBounds(0, 0, S.cx, S.cy);
        end else begin
            FormStyle := fsNormal;
            Visible := False;
        end;
        CenterForm(Self);
        Caption := AnsiReplaceStr(txtCaption, '&', '');
        btnLimpiar.Click;
        txtFechaDesde.Clear;
        txtFechaHasta.Clear;
    except
    	Result := False;
    end;

	lblTotales.Caption := '';

end;

{******************************** Function Header ******************************
Function Name:   txtPatenteKeyPress
Author : mbecerra
Date Created : 03/04/2008
Description : Inicializa la ventana del formulario
Parameters : Sender: TObject;  Key : char
Return Value : None

Revision 1:
Author: mbecerra
Date:  20-05-2008
Description:
            Se agregan las teclas 	Ctrl-C  = #3,		Ctrl-V  = #22
            						Ctrl-X  = #24,		Ctrl-Z  = #26
            para que el usuario pueda hacer Copy&Paste como en Windows

*******************************************************************************}
procedure TConsultaTransitosARevalidarForm.txtPatenteKeyPress(Sender: TObject;
  var Key: Char);
begin
	if not (Key  in [#3, #22, #24, #26, '0'..'9','a'..'z','A'..'Z', Char(VK_BACK)]) then
        Key := #0;
end;

end.
