object FDevolverTelevia: TFDevolverTelevia
  Left = 307
  Top = 90
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Devolucion de Televia'
  ClientHeight = 362
  ClientWidth = 421
  Color = clBtnFace
  Constraints.MaxHeight = 404
  Constraints.MaxWidth = 437
  Constraints.MinHeight = 400
  Constraints.MinWidth = 429
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object gb_DevolverTelevia: TGroupBox
    Left = 8
    Top = 216
    Width = 405
    Height = 111
    Caption = 'Devolver Televia'
    TabOrder = 1
    object Label3: TLabel
      Left = 18
      Top = 30
      Width = 33
      Height = 13
      Caption = 'Estado'
    end
    object cb_EstadoTagDevuelto: TVariantComboBox
      Left = 140
      Top = 25
      Width = 197
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cb_EstadoTagDevueltoChange
      Items = <>
    end
    object ck_DevueltoPorDuenio: TCheckBox
      Left = 16
      Top = 72
      Width = 137
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Devuelto por el Due'#241'o'
      TabOrder = 1
    end
  end
  object gb_BuscarTelevia: TGroupBox
    Left = 8
    Top = 8
    Width = 405
    Height = 209
    Caption = 'Buscar Televia'
    TabOrder = 0
    object Label4: TLabel
      Left = 16
      Top = 27
      Width = 42
      Height = 13
      Caption = 'Etiqueta:'
    end
    object LVerde: TLabel
      Left = 36
      Top = 135
      Width = 28
      Height = 13
      Caption = 'Verde'
    end
    object ledVerde: TLed
      Left = 18
      Top = 136
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clGreen
      ColorOff = clBtnFace
    end
    object ledRojo: TLed
      Left = 18
      Top = 170
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clGray
      ColorOff = clBtnFace
    end
    object lblLroja: TLabel
      Left = 36
      Top = 170
      Width = 22
      Height = 13
      Caption = 'Roja'
    end
    object ledAmarilla: TLed
      Left = 116
      Top = 136
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clYellow
      ColorOff = clBtnFace
    end
    object Lamarilla: TLabel
      Left = 141
      Top = 135
      Width = 36
      Height = 13
      Caption = 'Amarilla'
    end
    object Lnegra: TLabel
      Left = 141
      Top = 170
      Width = 29
      Height = 13
      Caption = 'Negra'
    end
    object ledNegra: TLed
      Left = 116
      Top = 170
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clBlack
      ColorOff = clBtnFace
    end
    object Lestado: TLabel
      Left = 92
      Top = 100
      Width = 35
      Height = 13
      Caption = 'lEstado'
    end
    object Label2: TLabel
      Left = 15
      Top = 100
      Width = 36
      Height = 13
      Caption = 'Estado:'
    end
    object Label5: TLabel
      Left = 16
      Top = 62
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object Label7: TLabel
      Left = 16
      Top = 81
      Width = 61
      Height = 13
      Caption = 'RUT Cliente:'
    end
    object lDuenio: TLabel
      Left = 92
      Top = 62
      Width = 36
      Height = 13
      Caption = 'lDuenio'
    end
    object lRUTDuenio: TLabel
      Left = 92
      Top = 81
      Width = 53
      Height = 13
      Caption = 'lRutDuenio'
    end
    object EEtiqueta: TEdit
      Left = 92
      Top = 24
      Width = 181
      Height = 21
      MaxLength = 15
      TabOrder = 0
      OnChange = EEtiquetaChange
      OnKeyPress = EEtiquetaKeyPress
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 329
    Width = 421
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      421
      33)
    object btn_Devolver: TButton
      Left = 244
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Devolver'
      Default = True
      TabOrder = 0
      OnClick = btn_DevolverClick
    end
    object btn_Salir: TButton
      Left = 332
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Salir'
      TabOrder = 1
      OnClick = btn_SalirClick
    end
  end
  object ObtenerEstadosConservacionTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerEstadosConservacionTAGs'
    Parameters = <>
    Left = 368
    Top = 136
  end
  object spLeerDatosTAG: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'LeerDatosTAG'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 241
    Top = 105
  end
  object spObtenerUltimoDuenioTelevia: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerUltimoDuenioTelevia'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 305
    Top = 121
  end
  object spAgregarMovimientoCuenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarMovimientoCuenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EsPago'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroFinanciamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@UsuarioCreacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end>
    Left = 257
    Top = 41
  end
  object spDevolverTAGExtraviado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'DevolverTAGExtraviado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoEstadoConservacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 337
    Top = 57
  end
end
