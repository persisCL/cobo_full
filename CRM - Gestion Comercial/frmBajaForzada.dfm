object frmBajaForzadaMasiva: TfrmBajaForzadaMasiva
  Left = 0
  Top = 0
  Caption = 'Baja Forzada Masiva'
  ClientHeight = 352
  ClientWidth = 411
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlOrigen: TPanel
    Left = 0
    Top = 0
    Width = 411
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object btnBuscarArchivo: TSpeedButton
      Left = 361
      Top = 5
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = btnBuscarArchivoClick
    end
    object lblOrigen: TLabel
      Left = 9
      Top = 10
      Width = 36
      Height = 13
      Caption = '&Origen:'
      FocusControl = txtOrigen
    end
    object txtOrigen: TEdit
      Left = 51
      Top = 6
      Width = 305
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
  end
  object pnlAvance: TPanel
    Left = 0
    Top = 33
    Width = 411
    Height = 286
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Presione escape para cancelar'
    TabOrder = 1
    object lblprocesoGeneral: TLabel
      Left = 0
      Top = 273
      Width = 411
      Height = 13
      Align = alBottom
      Alignment = taCenter
      AutoSize = False
      Caption = 'Progreso general'
      ExplicitLeft = 133
      ExplicitTop = 95
      ExplicitWidth = 182
    end
    object lblMensaje: TLabel
      Left = 0
      Top = 243
      Width = 50
      Height = 13
      Align = alBottom
      Caption = 'lblMensaje'
    end
    object pbProgreso: TProgressBar
      Left = 0
      Top = 256
      Width = 411
      Height = 17
      Align = alBottom
      Smooth = True
      Step = 1
      TabOrder = 0
    end
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 319
    Width = 411
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      411
      33)
    object btnProcesar: TButton
      Left = 247
      Top = 3
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Procesar'
      TabOrder = 0
      OnClick = btnProcesarClick
    end
    object btnSalir: TButton
      Left = 328
      Top = 3
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Salir'
      TabOrder = 1
      OnClick = btnSalirClick
    end
  end
  object OpenDialog: TOpenDialog
    Left = 3
    Top = 320
  end
  object spSuspenderCuenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'SuspenderCuenta;1'
    Parameters = <>
    Left = 32
    Top = 320
  end
  object spActualizarLogOperacionesRegistrosProcesados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarLogOperacionesRegistrosProcesados;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RegistrosAProcesar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LineasArchivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 61
    Top = 321
  end
  object spInsertarAuditoriaBajasForzadas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarAuditoriaBajasForzadas;1'
    Parameters = <>
    Left = 93
    Top = 321
  end
  object InsertarHistoricoConvenio: TADOStoredProc
    AutoCalcFields = False
    CacheSize = 50
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    MarshalOptions = moMarshalModifiedOnly
    CommandTimeout = 3000
    EnableBCD = False
    ProcedureName = 'InsertarHistoricoConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 128
    Top = 320
  end
end
