object frmSeleccionarGrupoFacturacionForm: TfrmSeleccionarGrupoFacturacionForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Seleccionar Grupo de Facturaci'#243'n'
  ClientHeight = 100
  ClientWidth = 428
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  DesignSize = (
    428
    100)
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 8
    Top = 8
    Width = 315
    Height = 13
    Caption = 
      'Seleccione el Grupo de Facturaci'#243'n al cual pertenece el Convenio' +
      ':'
  end
  object vcbCodigoGrupoFacturacion: TVariantComboBox
    Left = 8
    Top = 30
    Width = 412
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items = <>
  end
  object btnAceptar: TButton
    Left = 264
    Top = 67
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = btnAceptarClick
    ExplicitTop = 61
  end
  object btnCancelar: TButton
    Left = 345
    Top = 67
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    TabOrder = 2
    OnClick = btnCancelarClick
    ExplicitTop = 61
  end
end
