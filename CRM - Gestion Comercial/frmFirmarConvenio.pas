{-----------------------------------------------------------------------------
 Revision : 1
 Author : nefernandez
 Date : 12/11/2007
 Description : SS 622:
 - Se quita el botones "Reimprimir" e "Imprimir Etiquetas", tambi�n se
 quita el boton de cerrar del formulario.
 - Se modifica el procedure Inicializar para que no reciba el parametro
 que habilitar/deshabilitar el boton "btn_ImprimirEtiquetaTag" pues dicho
 bot�n ya no existe.
-----------------------------------------------------------------------------}
unit frmFirmarConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls,RStrings,UtilProc, PeaProcs;

type
  TformFirmarConvenio = class(TForm)
    txtEstado: TLabel;
    btnFirmarContrato: TButton;
    btnCancelar: TButton;
    procedure btnFirmarContratoClick(Sender: TObject);
    procedure btnReimprimirClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btn_ImprimirEtiquetaTagClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //procedure Inicializar(Caption: TCaption;BotonEtiquetaEnabled:Boolean);
    procedure Inicializar(Caption: TCaption);
  end;

var
  formFirmarConvenio: TformFirmarConvenio;

implementation

uses DMConnection;

{$R *.dfm}

procedure TformFirmarConvenio.btnFirmarContratoClick(Sender: TObject);
begin
    if MsgBox(MSG_QUESTION_GUARDAR_CONVENIO, Caption, MB_ICONWARNING+MB_YESNO) = mrYes then
        ModalResult := mrOk
    else
        ModalResult := mrCancel;
end;

procedure TformFirmarConvenio.btnReimprimirClick(Sender: TObject);
begin
	ModalResult := mrRetry;
end;

procedure TformFirmarConvenio.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

(*
	Si el modal es mrRetry quiere reimprimir, si es mrOk Firma el contrato, si es mrCancel Cancel�
*)
//procedure TformFirmarConvenio.Inicializar(Caption:TCaption;BotonEtiquetaEnabled:Boolean);
procedure TformFirmarConvenio.Inicializar(Caption:TCaption);
begin
	Self.Caption := Caption;
	//btn_ImprimirEtiquetaTag.Enabled:=BotonEtiquetaEnabled;
end;

(*
	Si el modal es mrIgnore, esto debe ser interpretado que se apreto el boto de imprimir etiqueta
*)

procedure TformFirmarConvenio.btn_ImprimirEtiquetaTagClick(
  Sender: TObject);
begin
	ModalResult:=mrIgnore;
end;

end.
