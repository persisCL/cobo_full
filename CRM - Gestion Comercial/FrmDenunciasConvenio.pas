unit FrmDenunciasConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, FreDatosConvenioConsulta,
  FreDatosPersonaConsulta, ListBoxEx, DBListEx, CollPnl;

type
  TFormDenunciasConvenio = class(TForm)
    GBPersona: TGroupBox;
    FrameDatosPersonaConsulta1: TFrameDatosPersonaConsulta;
    GroupBox1: TGroupBox;
    FrameDatosConvenioConsulta1: TFrameDatosConvenioConsulta;
    PanelDeAbajo: TPanel;
    BtnCancelar: TDPSButton;
    btnAceptar: TDPSButton;
    GroupBox2: TGroupBox;
    dblVehiculos: TDBListEx;
    GroupBox3: TGroupBox;
    lblOperacionGestion: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ComboBox1: TComboBox;
    Memo1: TMemo;
    btn_DocRequerido: TDPSButton;
    CollapsablePanel1: TCollapsablePanel;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(CodigoConvenio: integer): Boolean;
  end;

var
  FormDenunciasConvenio: TFormDenunciasConvenio;

implementation

{$R *.dfm}

{ TFormDenunciasConvenio }

function TFormDenunciasConvenio.Inicializar(
  CodigoConvenio: integer): Boolean;
begin
    result := true;
end;

end.
