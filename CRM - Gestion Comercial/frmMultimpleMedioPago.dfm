object formMultimpleMedioPago: TformMultimpleMedioPago
  Left = 128
  Top = 197
  BorderStyle = bsDialog
  Caption = 'Seleccione un Convenio'
  ClientHeight = 288
  ClientWidth = 811
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 35
    Height = 13
    Caption = 'RUT: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl_RUT: TLabel
    Left = 40
    Top = 16
    Width = 89
    Height = 13
    AutoSize = False
    Caption = 'lbl_RUT'
  end
  object Label2: TLabel
    Left = 144
    Top = 16
    Width = 44
    Height = 13
    Caption = 'Cliente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl_Nombre: TLabel
    Left = 192
    Top = 16
    Width = 53
    Height = 13
    Caption = 'lbl_Nombre'
  end
  object Label3: TLabel
    Left = 8
    Top = 40
    Width = 525
    Height = 16
    Caption = 
      'El cliente ya tiene convenio en la Concesionaria, seleccione el ' +
      'convenio que desea usar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object pnl_Botones: TPanel
    Left = 0
    Top = 248
    Width = 811
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      811
      40)
    object btn_Cancelar: TButton
      Left = 568
      Top = 7
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 0
    end
    object btn_Nuevo: TButton
      Left = 648
      Top = 7
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Nuevo'
      TabOrder = 1
      OnClick = btn_NuevoClick
    end
    object btn_Modificar: TButton
      Left = 728
      Top = 7
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Modificar'
      TabOrder = 2
      OnClick = btn_ModificarClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 59
    Width = 811
    Height = 189
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Convenios'
    TabOrder = 1
    object dbl_convenio: TDBListEx
      Left = 2
      Top = 15
      Width = 807
      Height = 172
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Numero de Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroConvenioFormateado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Tipo Medio de Pago'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TipoMedioPago'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 300
          Header.Caption = 'Descripci'#243'n Medio de Pago'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripMedioPago'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Tipo de Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TipoConvenio'
        end>
      DataSource = DataSource
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnClick = dbl_convenioClick
    end
  end
  object ObtenerConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = ObtenerConveniosAfterOpen
    ProcedureName = 'ObtenerConvenios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = '1'
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = '1'
      end
      item
        Name = '@SoloActivos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SoloContanera'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 168
    Top = 112
  end
  object DataSource: TDataSource
    DataSet = ObtenerConvenios
    Left = 136
    Top = 112
  end
  object ObtenerDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 248
    Top = 136
  end
end
