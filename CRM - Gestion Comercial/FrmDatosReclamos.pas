unit FrmDatosReclamos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Navigator, StdCtrls, ExtCtrls, Buttons, Grids,
  DBGrids, WorkflowComp, DB, ADODB, Util, UtilProc, CacProcs, PeaTypes,
  ListBoxEx, DBListEx;

type
  TNavWindowDatosReclamos = class(TNavWindowFrm)
    Display: TWorkflowDisplay;
    Splitter1: TSplitter;
    ds_qry_OS: TDataSource;
    qry_OSTodas: TADOQuery;
    Grilla: TDBListEx;
    procedure ds_qry_OSDataChange(Sender: TObject; Field: TField);
    procedure GrillaDblClick(Sender: TObject);
    procedure GrillaLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
  private
    FFiltro: AnsiString;
    FCodigoCliente: integer;
    FTipoContacto: AnsiString;
	function BuscarOrdenesServicio(Cliente:Integer; TipoContacto: AnsiString; Estado:AnsiString):Boolean;
  public
    Function GetBookmark(Var Bookmark, Description: AnsiString): Boolean; override;
	Function GotoBookmark(Bookmark: AnsiString): Boolean; override;
	class Function CreateBookmark(CodigoCliente: Integer; TipoContacto: AnsiString; Filtro: AnsiString): AnsiString;
    function Inicializa: Boolean; override;
  end;

var
  NavWindowDatosReclamos: TNavWindowDatosReclamos;

implementation

uses
	DMConnection, FrmCliente, FrmDatosContacto, FrmDatosOrdenServicio,
	PeaProcs;

{$R *.dfm}

{ TNavWindowDatosCliente }


{ TNavWindowDatosReclamos }

class function TNavWindowDatosReclamos.CreateBookmark(CodigoCliente: Integer; TipoContacto: AnsiString; Filtro: AnsiString): AnsiString;
begin
    Result := IntToStr(CodigoCliente) + '/' + TipoContacto + '/' + Filtro;
end;

function TNavWindowDatosReclamos.GetBookmark(var Bookmark,
  Description: AnsiString): Boolean;
resourcestring
    MSG_BOOKMARK = 'Lista de Ordenes de Servicio del Cliente %s';
begin
    if Qry_OSTodas.Eof then
        Result := False
    else begin
        Bookmark := CreateBookmark(FCodigoCLiente, FTipoContacto, FFiltro);
        Description := format(MSG_BOOKMARK, [trim(BuscarApellidoNombreCliente(DMConnections.BaseCAC, FCodigoCliente))]);
        Result := True;
    end;
end;

function TNavWindowDatosReclamos.GotoBookmark(
  Bookmark: AnsiString): Boolean;
begin
	FCodigoCliente	:= IVal(ParseParamByNumber(bookmark, 1, '/'));
	//**FEsCliente		:= Boolean(Ival(ParseParamByNumber(bookmark, 2, '/')));
    FTipoContacto	:= Trim(ParseParamByNumber(bookmark, 2, '/'));
    FFiltro			:= Trim(ParseParamByNumber(bookmark, 3, '/'));
    //Delete (Bookmark,Pos('/',bookmark),2);
    result := BuscarOrdenesServicio(FCodigoCliente, FTipoContacto, FFiltro);
end;

function TNavWindowDatosReclamos.Inicializa: Boolean;
begin
    Result := Inherited Inicializa;
end;

function TNavWindowDatosReclamos.BuscarOrdenesServicio(Cliente: Integer; TipoContacto: AnsiString;
  Estado: AnsiString): Boolean;
resourcestring
    CAPTION_FORM             = 'Ordenes de Servicio del Cliente %s';
    CAPTION_ORDEN_SERVICIO   = 'Obtener Ordenes de Servicio';
    MSG_ERROR_ORDEN_SERVICIO   = 'No se han podido obtener las Ordenes de Servicio del Cliente';
begin
   (* Bucamos las ordenes de Servicio de Un Cliente Segun Su Estado *)
	Result := false;
    try
        if Qry_OSTodas.Filtered then Qry_OSTodas.Filtered := false;
        Qry_OSTodas.Close;
        Qry_OSTodas.parameters.ParamByName('Cliente').Value := Cliente;
        Qry_OSTodas.parameters.ParamByName('TipoContacto').Value := TipoContacto;
        Qry_OSTodas.parameters.ParamByName('Estado1').Value := iif(Estado = '*', null, Estado);
        Qry_OSTodas.parameters.ParamByName('Estado2').Value := Estado;
        Qry_OSTodas.Open;
        Self.caption := Format(CAPTION_FORM, [trim(BuscarApellidoNombreCliente(DMConnections.BaseCAC, Cliente))]);
        Result := Qry_OSTodas.RecordCount > 0;
        Grilla.SetFocus;
    except
        on E: Exception do begin
	        Qry_OSTodas.Close;
            MsgBoxErr(MSG_ERROR_ORDEN_SERVICIO, e.Message, CAPTION_ORDEN_SERVICIO, MB_ICONSTOP)
        end;
    end;
end;

procedure TNavWindowDatosReclamos.ds_qry_OSDataChange(Sender: TObject;
  Field: TField);
begin
    Display.Connection := nil;
    Display.WorkflowID := Qry_OSTodas.fieldbyname('CodigoWorkFlow').AsInteger;
    Display.ServiceOrder := Qry_OSTodas.fieldbyname('CodigoOrdenServicio').AsInteger;
    Display.Connection := DMCOnnections.BaseCAC;
end;

procedure TNavWindowDatosReclamos.GrillaDblClick(Sender: TObject);
var
    f: TformDatosOrdenServicio;
begin
	Application.CreateForm(TFormDatosOrdenServicio,f);
    if f.Inicializa(Qry_OSTodas.fieldByname('CodigoOrdenServicio').asInteger) then f.ShowModal;
    f.Release;
end;

procedure TNavWindowDatosReclamos.GrillaLinkClick(Sender: TCustomDBListEx;
  Column: TDBListExColumn);
begin
	if Column.FieldName = 'VerCliente' then begin
		if FTipoContacto = TIPO_CLIENTE then begin
			Navigator.JumpTo(TNavWindowCliente,
			  TNavWindowCliente.CreateBookmark(False, FCodigoCliente, 0, '', '', '', '', '', nulldate, ' ', 0.0, 0, 0, 0, 0.0));
		end else begin
			Navigator.JumpTo(TNavWindowDatosContacto,
			  TNavWindowDatosContacto.CreateBookmark(FCodigoCliente));
		end;
	end;

	if Column.FieldName = 'CodigoOrdenServicio' then Grilla.OnDblClick(Self);
end;

end.
