unit formSuspencionCuenta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, UtilProc, RStrings,
  Dialogs, StdCtrls, Validate, DateEdit, Util, ConstParametrosGenerales, DMConnection, PeaProcs;

type
  TTipoEdicion = (teSuspender, teEditar);
  TfrmSuspencionCuenta = class(TForm)
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    ch_Notificado: TCheckBox;
    txt_FechaDeadline: TDateEdit;
    Label1: TLabel;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
  private
    FTipoEdicion: TTipoEdicion;
    function GetNotificadoPorCliente: Boolean;
    function GetFechaLimite: TDatetime;
    { Private declarations }
  public
    { Public declarations }
    property NotificadoPorCliente: Boolean read GetNotificadoPorCliente;
    property FechaLimite: TDatetime read GetFechaLimite;
    function Inicializar(TipoEdicion: TTipoEdicion; FechaDeadline: TDateTime; Voluntaria: Boolean): Boolean; overload;
    function Inicializar: Boolean; overload;

  end;

var
  frmSuspencionCuenta: TfrmSuspencionCuenta;

implementation

{$R *.dfm}

{ TfrmSuspencionCuenta }

function TfrmSuspencionCuenta.GetFechaLimite: TDatetime;
begin
    Result := txt_FechaDeadline.Date;
end;

function TfrmSuspencionCuenta.GetNotificadoPorCliente: Boolean;
begin
    Result := ch_Notificado.Checked;
end;

function TfrmSuspencionCuenta.Inicializar(TipoEdicion: TTipoEdicion; FechaDeadline: TDateTime; Voluntaria: Boolean): Boolean;
resourcestring
	MSG_INIT_ERROR = 'Error iniciando Suspensi�n de Cuenta';
    MSG_ERROR	   = 'Error';
var
	nDiasDeadLine : integer;
begin
	try
    	// Por default la fecha limite es 30 dias a partir de la fecha de hoy
    	ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_DIAS_LIMITE_SUSPENCION', nDiasDeadLine);

    	if FechaDeadline = NullDate then txt_FechaDeadline.Date := NowBase(DMConnections.BaseCAC) + nDiasDeadLine
    	else txt_FechaDeadline.Date := FechaDeadline;

    	ch_Notificado.Checked := Voluntaria;

        FTipoEdicion := TipoEdicion;

    	Result := True;
    except
    	on e: exception do begin
        	MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Result := False;
        end;
    end;
end;

procedure TfrmSuspencionCuenta.btn_CancelarClick(Sender: TObject);
begin
    close;
end;

procedure TfrmSuspencionCuenta.btn_AceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR_DIA_NO_HABIL = 'La fecha l�mite de Suspensi�n debe ser un d�a h�bil';
    MSG_ERROR_DIA_FERIADO  = 'La fecha l�mite de Suspensi�n no debe ser un d�a feriado';
var
    Detalle: String;
begin
    if StrToDate(DateToStr(FechaLimite)) < StrToDate(DateToStr(NowBase(DMConnections.BaseCAC))) then begin
        MsgBoxBalloon(MSG_ERROR_SUSPENCION, Caption, MB_ICONSTOP, txt_FechaDeadline);
        txt_FechaDeadline.SetFocus;
        exit;
    end;

    if not EsDiaHabil(DMConnections.BaseCAC, FechaLimite) then begin
        MsgBoxBalloon(MSG_ERROR_DIA_NO_HABIL, Caption, MB_ICONSTOP, txt_FechaDeadline);
        txt_FechaDeadline.SetFocus;
        exit;
    end;

    if FTipoEdicion = teEditar then  //Cuando edita la fecha de susp, ademas valido que no sea feriado
        if EsDiaFeriado(DMConnections.BaseCAC, FechaLimite, Detalle) then begin
            MsgBoxBalloon(MSG_ERROR_DIA_FERIADO, Caption, MB_ICONSTOP, txt_FechaDeadline);
            txt_FechaDeadline.SetFocus;
            exit;
        end;

    ModalResult := mrOk;
end;

function TfrmSuspencionCuenta.Inicializar: Boolean;
begin
    Result := Inicializar(teSuspender, nulldate, false);
end;

end.
