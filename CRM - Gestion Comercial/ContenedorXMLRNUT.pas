unit ContenedorXMLRNUT;

interface

uses XMLIntf,XMLDoc,PeaProcs,DmConnection,PeaTypes,SysUtils,ConstParametrosGenerales,Dialogs;

type
    TPatentesRNUT = record
                        Patente:String;
                        RUT:String;
        end;

    TRegPatantesRNUT = array of TPatentesRNUT;

function ObtenerPatentesRNUT:TRegPatantesRNUT;

implementation

function ObtenerPatentesRNUT:TRegPatantesRNUT;
var
    xmlDoc:IXMLDocument;
    Archivo:String;
    aux:TRegPatantesRNUT;
    i:Integer;
    rut:String;
begin
    SetLength(aux,0);

    ObtenerParametroGeneral(DMConnections.BaseCAC,XML_PARAMETRO_ARCHIVO_XML,Archivo);

    xmlDoc := NewXMLDocument('1.0');
    Archivo:=Archivo+'\RNUT.xml';

    if not(FileExists(Archivo)) then begin result:=aux; exit; end;

    xmlDoc.LoadFromFile(Archivo);
    xmlDoc.Encoding := 'UTF-8';
    xmlDoc.StandAlone := 'yes';

    with xmlDoc.DocumentElement.ChildNodes do begin
        for i:=0 to Count-1 do  begin //Contratos
            if (uppercase(Nodes[i].NodeName)='ENTIDAD')  and (Nodes[i].ChildNodes['rol'].Text=XML_ROL_CLIENTE) then begin // encontre el cliente
               rut:=Nodes[i].ChildNodes['RUT'].Text;
            end;

            if (uppercase(Nodes[i].NodeName)='VEHICULO')  then begin
                SetLength(aux,length(aux)+1);
                aux[length(aux)-1].RUT:=rut;
                aux[length(aux)-1].Patente:=trim(Nodes[i].ChildNodes['patente'].Text);
            end;
        end;
    end;

    result:=aux;
end;


end.
