unit FrmSeleccionarReimpresionPlantillas;
{*******************************************************************************
    Revision : 1
    Author : FSandi
    Date : 31/05/2007
    Description : Se agrega la opcion de reimprimir contratos de motos.

Revision 2
Author: mbecerra
Date: 29-Dic-2008
Description:	(Ref SS 769) Se agrega la re-impresi�n de los anexos 3 y 4

Revision : 3
    Author : pdominguez
    Date   : 06/08/2009
    Description : SS 733
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            btn_okClick

Firma       : SS-1006-NDR-20120727
Description : Reimprimir el contrado de adhesion PA.
            Se renombre MotivoCancelacion -> MotivoCancelacionImpresion

Firma       : SS_1380_MCA_20150911
Descripcion : se comenta las linea que hacen referencia a la impresion de la caratula

Firma       : SS_1380_NDR_20151009
Descripcion : Se repone la impresion de caratula
*******************************************************************************}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, frmImprimirConvenio, RStrings, PeaProcs,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Convenios,  ConstParametrosGenerales, Util, UtilProc,
  ADODB,                                                                        //SS-1006-NDR-20120727
  SysUtilsCN,                                                                   //SS-1006-NDR-20120727
  PeaProcsCN,                                                                   //SS-1006-NDR-20120727
  frmReporteContratoAdhesionPA, Peatypes,                                       //SS-1006-NDR-20120727
  RBSetUp, PRinters, UtilDB;
type
  TFrmSeleccionarReimpresion = class(TForm)
    gb_Convenio: TGroupBox;
    sb_Convenio: TScrollBox;
    Panel10: TPanel;
    cbAnverso: TCheckBox;
    cbCaratula: TCheckBox;
    cbMandato: TCheckBox;
    cbEtiqueta: TCheckBox;
    cbAnexoVehiculo: TCheckBox;
    cbAnexoAltaCuenta: TCheckBox;
    btn_Cancelar: TButton;
    btn_ok: TButton;
    CbMotos: TCheckBox;
    cbAnexo3y4: TCheckBox;
    cbContratoAdhesionPA: TCheckBox;
    cbSuscripcionLB: TCheckBox;                                            //SS-1006-NDR-20120727
    function GetImprimirCaratulaJuridica: boolean;
    function GetImprimirCaratulaNatural: boolean;
    function GetImprimirAnversoJuridica: boolean;
    function GetImprimirAnversoNatural: boolean;
    function GetImprimirMandatoPAC: boolean;
    function GetImprimirMandatoPAT: boolean;
    function GetImprimirEtiquetas: boolean;
    function GetImprimirAnexoJuridica: boolean;
    procedure btn_okClick(Sender: TObject);

  private
    FDatosConvenio:TDatosConvenio;
    FDatosConvenioOriginal:TDatosConvenio;
    FTipoMedioPago: TTiposMediosPago;
    FMotivoCancelacionImpresion: TMotivoCancelacion;                            //SS-1006-NDR-20120727
    FImpCaratulaDocGuardado: Boolean;
    FPuntoEntrega: Integer;
    procedure SetMotivoCancelacionImpresion(const Value: TMotivoCancelacion);   //SS-1006-NDR-20120727
    function GetImprimirAltaCuenta: Boolean;
    function GetImprimirAnexoNatural: Boolean;
    function GetImprimirMotos: Boolean;
    procedure ImprimirContratoAdhesionPA;                                       //SS-1006-NDR-20120727

  public
    function Inicializar(Caption: TCaption;const DatosConvenio,DatosConvenioOriginal:TDatosConvenio; PuntoEntrega: Integer; ListaDocumentacion: ATOrdenServicio = Nil; ImpCaratulaDocGuardado: Boolean = False ): Boolean;
    function CantidadMarcados: Integer;
    property ImprimirCaratulaConvenioJuridica:Boolean read GetImprimirCaratulaJuridica;
    property ImprimirCaratulaConvenioNatural:Boolean read GetImprimirCaratulaNatural;
    property ImprimirAnversoConvenioJuridica:Boolean read GetImprimirAnversoJuridica;
    property ImprimirAnversoConvenioNatural:Boolean read GetImprimirAnversoNatural;
    property ImprimirAnexoConvenioJuridica: Boolean read GetImprimirAnexoJuridica;
    property ImprimirAnexoConvenioNatural: Boolean read GetImprimirAnexoNatural;
    property ImprimirMandatoConvenioPAC:Boolean read GetImprimirMandatoPAC;
    property ImprimirMandatoConvenioPAT:Boolean read GetImprimirMandatoPAT;
    property ImprimirEtiquetasConvenio:Boolean read GetImprimirEtiquetas;
    property ImprimirMotos:Boolean read GetImprimirMotos; //Revision 1
    property ImprimirAltaCuenta: Boolean read GetImprimirAltaCuenta;
    property MotivoCancelacionImpresion: TMotivoCancelacion read FMotivoCancelacionImpresion write SetMotivoCancelacionImpresion;                   //SS-1006-NDR-20120727
    property PuntoEntrega: Integer read FPuntoEntrega;
  end;

var
  FrmSeleccionarReimpresion: TFrmSeleccionarReimpresion;

implementation

uses DMConnection;

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}

{ TFrmSeleccionarReimpresion }

function TFrmSeleccionarReimpresion.GetImprimirAnversoJuridica: boolean;
begin
    result := (FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA) and  cbAnverso.Checked;
end;

function TFrmSeleccionarReimpresion.GetImprimirCaratulaJuridica: boolean;
begin
    result := (FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA) and cbCaratula.Checked;
end;

function TFrmSeleccionarReimpresion.GetImprimirMandatoPAC: boolean;
begin
    result := cbMandato.Enabled and (FTipoMedioPago = PAC) and cbMandato.Checked;
end;



function TFrmSeleccionarReimpresion.Inicializar(
    Caption: TCaption;
    const DatosConvenio,DatosConvenioOriginal:TDatosConvenio;
    PuntoEntrega: Integer;
    ListaDocumentacion: ATOrdenServicio = Nil;
    ImpCaratulaDocGuardado: Boolean = False): Boolean;
var
    RegVehiculo: TRegCuentaVehiculos;
    i: Integer;
begin
    //Items reimprimir;
    //Anverso Personeria Natural;
    //Caratula Personeria Natural
    //Mandato PAC o PAT
    //Etiqueta
    //cbAnverso.Enabled  :=  DatosConvenio.EsConvenioCN;						//SS_1147_MCA_20140408
    cbAnverso.Enabled := DatosConvenio.EsConvenioNativo;						//SS_1147_MCA_20140408
    //cbEtiqueta.Enabled :=  DatosConvenio.EsConvenioCN;						//SS_1147_MCA_20140408
    cbEtiqueta.Enabled := DatosConvenio.EsConvenioNativo;						//SS_1147_MCA_20140408
    FImpCaratulaDocGuardado := ImpCaratulaDocGuardado;
    FPuntoEntrega := PuntoEntrega;

    cbContratoAdhesionPA.Enabled := DatosConvenio.AdheridoPA;                   //SS-1006-NDR-20120727

    if DatosConvenio.MedioPago.MedioPago.TipoMedioPago = PAC then begin
        cbMandato.Caption := cbMandato.Caption + ' ' + 'PAC';
        FTipoMedioPago := PAC;
    end
    else begin
        if DatosConvenio.MedioPago.MedioPago.TipoMedioPago = PAT then begin
            cbMandato.Caption := cbMandato.Caption + ' ' + 'PAT';
            FTipoMedioPago := PAT;
        end
        else begin
            cbMandato.Enabled := False;
            FTipoMedioPago := NINGUNO;
        end;
    end;

    //INICIO: TASK_054_ECA_20160708
    cbSuscripcionLB.Enabled:=False;
    cbSuscripcionLB.Visible:=False;
    if DatosConvenio.CodigoTipoConvenio=1 then
    begin
        for i := 0 to DatosConvenio.Cuentas.CantidadVehiculos - 1 do
        begin
          if DatosConvenio.Cuentas.Vehiculos[i].Cuenta.SuscritoListaBlanca then
          begin
              cbSuscripcionLB.Enabled:=True;
              cbSuscripcionLB.Visible:=True;
              Break;
          end;
        end;
    end;
    //FIN: TASK_054_ECA_20160708

    //cbAnexoVehiculo.Enabled := (DatosConvenio.Cuentas.CantidadVehiculos > 0) and DatosConvenio.EsConvenioCN;			//SS_1147_MCA_20140408
    cbAnexoVehiculo.Enabled := (DatosConvenio.Cuentas.CantidadVehiculos > 0) and DatosConvenio.EsConvenioNativo;			//SS_1147_MCA_20140408

    if DatosConvenioOriginal <> nil then DatosConvenio.Cuentas.ObtenerRegVehiculosAgregados(RegVehiculo, DatosConvenioOriginal.Cuentas);
    //cbAnexoAltaCuenta.Enabled := (length(RegVehiculo) > 0) or ((DatosConvenio.Cuentas.CantidadVehiculosEliminados + DatosConvenio.Cuentas.CantidadTagsQuitados + DatosConvenio.Cuentas.CantidadTagSinVehiculos + DatosConvenio.Cuentas.CantidadVehiculosRobados) > 0) and DatosConvenio.EsConvenioCN;			//SS_1147_MCA_20140408
    cbAnexoAltaCuenta.Enabled := (length(RegVehiculo) > 0) or ((DatosConvenio.Cuentas.CantidadVehiculosEliminados + DatosConvenio.Cuentas.CantidadTagsQuitados + DatosConvenio.Cuentas.CantidadTagSinVehiculos + DatosConvenio.Cuentas.CantidadVehiculosRobados) > 0) and DatosConvenio.EsConvenioNativo;		//SS_1147_MCA_20140408
    if not cbAnexoAltaCuenta.Enabled then begin
        if DatosConvenioOriginal <> nil then DatosConvenio.Cuentas.ObtenerRegVehiculosModificados(RegVehiculo, DatosConvenioOriginal.Cuentas);
        //cbAnexoAltaCuenta.Enabled := (length(RegVehiculo) > 0)  and DatosConvenio.EsConvenioCN;		//SS_1147_MCA_20140408
        cbAnexoAltaCuenta.Enabled := (length(RegVehiculo) > 0)  and DatosConvenio.EsConvenioNativo;		//SS_1147_MCA_20140408
    end;

    cbCaratula.Enabled := (DatosConvenio.DocumentacionConvenio.HayDocumentacionAImprimir) or (FImpCaratulaDocGuardado);

    FDatosConvenio := DatosConvenio;
    FDatosConvenioOriginal := DatosConvenioOriginal;
    FMotivoCancelacionImpresion := OK;                                          //SS-1006-NDR-20120727
    result := True;
{INICIO_: TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)-2}
    if FDatosConvenio.CodigoTipoConvenio = 2 then
        cbAnverso.Visible:= False;
{TERMINO: TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)-2}
end;

{******************************** Procedure Header *****************************
Procedure Name: btn_okClick
Author :
Date Created :
Parameters : Sender: TObject
Description :

Revision : 1
    Author : pdominguez
    Date : 06/08/2009
    Description : SS 733
        - Se llama al procedimiento Inicializar del objeto Form: TFormImprimirConvenio.
        - Se cambia la llamada a la impresi�n de la Car�tula del Convenio al
        nuevo procedimiento.
*******************************************************************************}
procedure TFrmSeleccionarReimpresion.btn_okClick(Sender: TObject);
	const
      	cSQLObtenerNombrePersona  = 'SELECT dbo.ObtenerNombrePersona(%d)';

var
    CantCopias : Integer;
    ImprimioAlgo: Boolean;
    Form : TFormImprimirConvenio;
    DatosConvenio: TDatosConvenio;
    i,y,contador:Integer;
    cuentas: array of TCuentaVehiculo;
    cuentasSuscritas: array of TCuentaVehiculo;
    Fechasuscripcion: TDateTime;
    NombreCliente: string;
    EmailCliente : string;
begin

    try
        ImprimioAlgo := False;
        Form := TFormImprimirConvenio.Create(nil);
        Form.Inicializar(FDatosConvenio); // Rev. 1 (SS 733)


        if not (
                cbAnverso.Checked
{INICIO: TASK_018_JMA_20160606 or cbCaratula.Checked }
                or cbMandato.Checked
                or cbSuscripcionLB.Checked       //TASK_054_ECA_20160708
{INICIO: TASK_018_JMA_20160606 or cbEtiqueta.Checked }
{INICIO: TASK_011_JMA_20160505               or cbAnexoVehiculo.Checked }
{INICIO: TASK_018_JMA_20160606 or cbAnexoAltaCuenta.Checked  }
{INICIO: TASK_011_JMA_20160505                cbMotos.Checked }
{INICIO: TASK_018_JMA_20160606 or cbAnexo3y4.Checked  }
{INICIO: TASK_011_JMA_20160505               or cbContratoAdhesionPA.Checked}
                ) then begin                                                       //SS-1006-NDR-20120727
                if MsgBox(MSG_CAPTION_DOC_A_IMPRIMIR,Caption,MB_ICONQUESTION or MB_YESNO) = IDNO then ModalResult := mrCancel
                else ModalResult := mrOK;
                exit;
        end;

         //TO DO Form.ImprimirAltaConvenio(cbCaratula.Checked, cbAnverso.Checked, cbAnexoAltaCuenta.Checked);


{INICIO: TASK_011_JMA_20160505
        // CONVENIO
        if ImprimirAnversoConvenioJuridica and (MotivoCancelacionImpresion = OK) then  begin                                                           //SS-1006-NDR-20120727
            if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CONVENIO_JURIDICA,CantCopias) then MotivoCancelacionImpresion := ERROR     //SS-1006-NDR-20120727
            else FDatosConvenio.Imprimir(IMPRIMIR_CONVENIO,FMotivoCancelacionImpresion,CantCopias,Form, FDatosConvenioOriginal.Cuentas);               //SS-1006-NDR-20120727
            ImprimioAlgo := True;
        end;

         if ImprimirAnversoConvenioNatural and (MotivoCancelacionImpresion = OK) then  begin                                                           //SS-1006-NDR-20120727
            if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CONVENIO_FISICA,CantCopias) then MotivoCancelacionImpresion := ERROR       //SS-1006-NDR-20120727
            else FDatosConvenio.Imprimir(IMPRIMIR_CONVENIO,FMotivoCancelacionImpresion,CantCopias,Form, FDatosConvenioOriginal.Cuentas);               //SS-1006-NDR-20120727
            ImprimioAlgo := True;
        end;
}
{TERMINO: TASK_011_JMA_20160505}

        //ANEXOS
(*        if (ImprimirAnexoConvenioJuridica) and (MotivoCancelacion = OK) then begin
           if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CONVENIO_JURIDICA,CantCopias) then raise Exception.Create('No se encontro el parametro con Cant. de copias');
           FDatosConvenio.Imprimir(IMPRIMIR_ANEXOS, FMotivoCancelacionImpresion,CantCopias,Form, FDatosConvenioOriginal.Cuentas);
           ImprimioAlgo := True;
        end;

        if (ImprimirAnexoConvenioNatural) and (MotivoCancelacion = OK) then begin
           if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CONVENIO_FISICA,CantCopias) then raise Exception.Create('No se encontro el parametro con Cant. de copias');
           FDatosConvenio.Imprimir(IMPRIMIR_ANEXOS, FMotivoCancelacionImpresion,CantCopias,Form, FDatosConvenioOriginal.Cuentas);
           ImprimioAlgo := True;
        end;
*)
        // ALTA CUENTA
(*        if (ImprimirAltaCuenta) and (MotivoCancelacion = OK) then begin
           if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_ALTA_CUENTA,CantCopias) then raise Exception.Create('No se encontro el parametro con Cant. de copias');
            FDatosConvenio.ImprimirAltasBajasModifCuentas(FMotivoCancelacionImpresion, CantCopias, Form ,FDatosConvenioOriginal.Cuentas );
            ImprimioAlgo := True;
        end;*)

{INICIO: TASK_011_JMA_20160505
        // CARATULA
        if ((ImprimirCaratulaConvenioJuridica or ImprimirCaratulaConvenioNatural)) and (MotivoCancelacionImpresion = OK) then  begin                 //SS-1006-NDR-20120727
            // Rev. 1 (SS 733)
//           if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CARATULA,CantCopias) then MotivoCancelacion := ERROR
//           else begin
//               if FImpCaratulaDocGuardado then FDatosConvenio.ImprimirCaratulaGuardada(FMotivoCancelacionImpresion, CantCopias, UsuarioSistema, ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC, PuntoEntrega), Form)
/               else FDatosConvenio.ImprimirCaratula(FMotivoCancelacionImpresion, CantCopias, UsuarioSistema, ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,PuntoEntrega),Form);
//           end;
            if Form.ImprimirCaratulaConvenio then ImprimioAlgo := True;       //SS_1380_NDR_20151009//SS_1380_MCA_20150911
            // Fin Rev.1 (SS 733)
        end;
}
{TERMINO: TASK_011_JMA_20160505}

{INICIO: TASK_018_JMA_20160606
        /// MANDATO
        if (ImprimirMandatoConvenioPAC) and (MotivoCancelacionImpresion = OK) then  begin                                                           //SS-1006-NDR-20120727
           if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_MANDATO_PAC,CantCopias) then MotivoCancelacionImpresion := ERROR         //SS-1006-NDR-20120727
           else FDatosConvenio.Imprimir(IMPRIMIR_MANDATO,FMotivoCancelacionImpresion,CantCopias,Form, FDatosConvenioOriginal.Cuentas);              //SS-1006-NDR-20120727
            ImprimioAlgo := True;
        end;

        if (ImprimirMandatoConvenioPAT) and (MotivoCancelacionImpresion = OK) then  begin                                                          //SS-1006-NDR-20120727
           if not ObtenerParametroGeneral(DMConnections.BaseCAC,                                                                                   //SS-1006-NDR-20120727
                                          iif(FDatosConvenio.MedioPago.MedioPago.PAT.TipoTarjeta = TARJETA_PRESTO ,                                //SS-1006-NDR-20120727
                                                CAN_COPIAS_MANDATO_PAT_PRESTO,                                                                     //SS-1006-NDR-20120727
                                                CAN_COPIAS_MANDATO_PAT                                                                             //SS-1006-NDR-20120727
                                             ),                                                                                                    //SS-1006-NDR-20120727
                                          CantCopias) then MotivoCancelacionImpresion := ERROR                                                     //SS-1006-NDR-20120727
           else FDatosConvenio.Imprimir(IMPRIMIR_MANDATO,FMotivoCancelacionImpresion,CantCopias,Form, FDatosConvenioOriginal.Cuentas);             //SS-1006-NDR-20120727
            ImprimioAlgo := True;
        end;

        // ETIQUETAS
        if (ImprimirEtiquetasConvenio) and (MotivoCancelacionImpresion = OK) then begin                                                           //SS-1006-NDR-20120727
           FDatosConvenio.ImprimirEtiquetas(FMotivoCancelacionImpresion, Form);                                                                   //SS-1006-NDR-20120727
           ImprimioAlgo := True;
        end;
INICIO: TASK_011_JMA_20160505
        //Reimprimir Motos
        if (ImprimirMotos) and (MotivoCancelacionImpresion = OK) then begin                                                                       //SS-1006-NDR-20120727
           FDatosConvenio.Imprimir(REIMPRIMIR_MOTOS,FMotivoCancelacionImpresion,1,Form, FDatosConvenioOriginal.Cuentas);                          //SS-1006-NDR-20120727
           ImprimioAlgo := True;
        end;

TERMINO: TASK_011_JMA_20160505
        //Rev 2
        if cbAnexo3y4.Checked and (MotivoCancelacionImpresion = OK) then begin                                                                   //SS-1006-NDR-20120727
            if not FDatosConvenio.ReimprimirAnexo3y4() then MotivoCancelacionImpresion := Peatypes.ERROR                                         //SS-1006-NDR-20120727
            else ImprimioAlgo := True;
        end;
{INICIO: TASK_011_JMA_20160505
        if cbContratoAdhesionPA.Checked and (MotivoCancelacionImpresion = OK) then        //SS-1006-NDR-20120727
        begin                                                                             //SS-1006-NDR-20120727
           ImprimirContratoAdhesionPA();                                                  //SS-1006-NDR-20120727
           ImprimioAlgo := True;                                                          //SS-1006-NDR-20120727
        end;                                                                              //SS-1006-NDR-20120727
}
{TERMINO: TASK_011_JMA_20160505}
        contador:=0; //TASK_054_ECA_20160708

        SetLength(Cuentas, FDatosConvenio.Cuentas.CantidadVehiculos);
        for i := 0 to FDatosConvenio.Cuentas.CantidadVehiculos - 1 do
        begin
             Cuentas[i] := FDatosConvenio.Cuentas.Vehiculos[i].Cuenta;
             //INICIO: TASK_054_ECA_20160708
             if FDatosConvenio.Cuentas.Vehiculos[i].Cuenta.SuscritoListaBlanca then
                contador:= contador + 1;
             //FIN: TASK_054_ECA_20160708
        end;

        //Application.CreateForm(TFormImprimirConvenio,form);
        ImprimioAlgo := form.ImprimirAltaConvenio(false, cbAnverso.Checked, cbMandato.Checked, Cuentas, FDatosConvenio);

        //INICIO: TASK_054_ECA_20160708
        SetLength(cuentasSuscritas, contador);
        y:=0;
        for i := 0 to FDatosConvenio.Cuentas.CantidadVehiculos - 1 do
        begin
             if FDatosConvenio.Cuentas.Vehiculos[i].Cuenta.SuscritoListaBlanca then
             begin
                cuentasSuscritas[y] := FDatosConvenio.Cuentas.Vehiculos[i].Cuenta;
                y:=y+1;
             end;
        end;

        if cbSuscripcionLB.Checked then
        begin
            Fechasuscripcion:=NowBase(DMConnections.BaseCAC);
            NombreCliente := QueryGetValue(DMConnections.BaseCAC, Format(cSQLObtenerNombrePersona, [FDatosConvenio.Cliente.Datos.CodigoPersona]));
            if FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion > 0 then begin
                EmailCliente := FDatosConvenio.Cliente.EMail.MediosComunicacion[0].Valor
            end
            else EmailCliente := '';

            ImprimioAlgo := form.ImprimirAltaSuscripcionListaBlanca(
                                                                        FDatosConvenio.CodigoConvenio,
                                                                        FDatosConvenio.CodigoPersona,
                                                                        FDatosConvenio.Cliente.Datos.NumeroDocumento,
                                                                        NombreCliente,
                                                                        EmailCliente,
            cuentasSuscritas,
            FDatosConvenio.MedioPago.CodigoMedioPago,
            FDatosConvenio.MediosEnvioPago.EnviaDocumentacionxMail,
            True,
            Fechasuscripcion);
        end;
        //FIN: TASK_054_ECA_20160708

{TERMINO: TASK_018_JMA_20160606}
        if not ImprimioAlgo then MotivoCancelacionImpresion := CANCELADO_USR;             //SS-1006-NDR-20120727
        ModalResult := mrOk;
    finally
        FreeAndNil(Form);
    end;
end;

function TFrmSeleccionarReimpresion.GetImprimirEtiquetas: boolean;
begin
    result := cbEtiqueta.Checked;
end;

function TFrmSeleccionarReimpresion.GetImprimirAnversoNatural: boolean;
begin
    result := (FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_FISICA) and  cbAnverso.Checked;
end;

function TFrmSeleccionarReimpresion.GetImprimirCaratulaNatural: boolean;
begin
    result := (FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_FISICA) and  cbCaratula.Checked;
end;

function TFrmSeleccionarReimpresion.GetImprimirMandatoPAT: boolean;
begin
    result := cbMandato.Enabled and (FTipoMedioPago = PAT) and cbMandato.Checked;
end;


function TFrmSeleccionarReimpresion.GetImprimirMotos: Boolean;
begin
    result := cbMotos.Checked;//Revision 1
end;

procedure TFrmSeleccionarReimpresion.SetMotivoCancelacionImpresion(             //SS-1006-NDR-20120727
  const Value: TMotivoCancelacion);                                             //SS-1006-NDR-20120727
begin                                                                           //SS-1006-NDR-20120727
    FMotivoCancelacionImpresion := Value;                                       //SS-1006-NDR-20120727
end;                                                                            //SS-1006-NDR-20120727

function TFrmSeleccionarReimpresion.GetImprimirAltaCuenta: Boolean;
begin
    result := cbAnexoAltaCuenta.Enabled and cbAnexoAltaCuenta.Checked;
end;

function TFrmSeleccionarReimpresion.GetImprimirAnexoJuridica: boolean;
begin
    result := (FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA) and cbAnexoVehiculo.Checked;
end;

function TFrmSeleccionarReimpresion.GetImprimirAnexoNatural: Boolean;
begin
    result := (FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_FISICA) and cbAnexoVehiculo.Checked;
end;

function TFrmSeleccionarReimpresion.CantidadMarcados: Integer;
begin
    result := 0;
    if ImprimirCaratulaConvenioJuridica then inc(Result);     //SS_1380_NDR_20151009//SS_1380_MCA_20150911
    if ImprimirCaratulaConvenioNatural then inc(Result);      //SS_1380_NDR_20151009//SS_1380_MCA_20150911
    if ImprimirAnversoConvenioJuridica then inc(Result);
    if ImprimirAnversoConvenioNatural then inc(Result);
    if ImprimirAnexoConvenioJuridica then inc(Result);
    if ImprimirAnexoConvenioNatural then inc(Result);
    if ImprimirMandatoConvenioPAC then inc(Result);
    if ImprimirMandatoConvenioPAT then inc(Result);
    if ImprimirEtiquetasConvenio then inc(Result);
    if ImprimirAltaCuenta then inc(Result);
    if ImprimirMotos then inc(Result);    //Revision 1
end;

//INICIO BLOQUE SS-1006-NDR-20120727--------------------------------------------
procedure TFrmSeleccionarReimpresion.ImprimirContratoAdhesionPA;
	var
        EMailPersona,
        NombrePersona,
        Firma,
        NumeroDocumento: string;
        ClienteAdheridoEnvioEMail: Boolean;
        ReporteContratoAdhesionPAForm: TReporteContratoAdhesionPAForm;
        FechaAdhesion:TDateTime;
        RBInterfaceConfig: TRBConfig;
        spValidarRestriccionesAdhesionPAKAnexo: TADOStoredProc;			// SS_1006_PDO_20121122
        SituacionesARegularizar: AnsiString;							// SS_1006_PDO_20121122
    const
    	cSQLObtenerEmailPersona                  = 'SELECT dbo.ObtenerEmailPersona(%d)';
    	cSQLFechaAdhesion                        = 'SELECT dbo.ObtenerFechaAdhesionPA(%d)';
        cSQLValidarConveniosPersonaEnvioPorEMail = 'SELECT dbo.ValidarConveniosPersonaEnvioPorEMail(%d)';
        cSQLObtenerNombrePersona				 = 'SELECT dbo.ObtenerNombrePersona(%d)';
        cSQLArmarRazonSocialyRepLegalEmpresa     = 'SELECT dbo.ArmarRazonSocialyRepLegalEmpresa(%d)';
begin
    try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
        	// SS_1006_PDO_20121122 Inicio Bloque
        	spValidarRestriccionesAdhesionPAKAnexo := TADOStoredProc.Create(nil);
            with spValidarRestriccionesAdhesionPAKAnexo do begin
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'ValidarRestriccionesAdhesionPAKAnexo';
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value := FDatosConvenio.Cliente.CodigoPersona;
                Parameters.ParamByName('@DevolverValidaciones').Value := 1;
                Open;

                if RecordCount > 0 then begin
                	while not Eof do begin

                        if SituacionesARegularizar <> EmptyStr then begin
                        	SituacionesARegularizar := SituacionesARegularizar + '\par \par ';
                        end;

                        SituacionesARegularizar := SituacionesARegularizar + ' \tab ' + '- ' + AnsiUpperCase(FieldByName('Descripcion').AsString);

                        Next;
                    end;
                end;
            end;
            // SS_1006_PDO_20121122 Fin Bloque

            FechaAdhesion             := QueryGetDateValue(DMConnections.BaseCAC, Format(cSQLFechaAdhesion, [FDatosConvenio.CodigoConvenio]));
            EMailPersona              := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerEmailPersona, [FDatosConvenio.Cliente.CodigoPersona]));
            ClienteAdheridoEnvioEMail := QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarConveniosPersonaEnvioPorEMail, [FDatosConvenio.Cliente.CodigoPersona]));

            NumeroDocumento := FDatosConvenio.Cliente.Datos.NumeroDocumento;

            if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA then begin
				NombrePersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLArmarRazonSocialyRepLegalEmpresa, [FDatosConvenio.CodigoConvenio]));
	            Firma := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerNombrePersona, [TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[0].CodigoPersona]));
            end
            else begin
				NombrePersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerNombrePersona, [FDatosConvenio.Cliente.CodigoPersona]));
            	Firma := NombrePersona;
            end;

            ReporteContratoAdhesionPAForm := TReporteContratoAdhesionPAForm.Create(nil);

            with ReporteContratoAdhesionPAForm do
            begin
                Inicializar(  NombrePersona,
                              Firma,
                              Trim(NumeroDocumento),
                              EMailPersona,
                              FechaAdhesion,
                              ClienteAdheridoEnvioEMail,
                              False,						// SS_1006_PDO_20121122
                              False,                        // SS_1006_PDO_20121122
                              SituacionesARegularizar       // SS_1006_PDO_20121122
                           );

                RBInterface.ModalPreview := False;
                RBInterfaceConfig := RBInterface.GetConfig;

                with RBInterfaceConfig do begin
	                Copies       := 2;
                    MarginTop    := 0;
                    MarginLeft   := 0;
                    MarginRight  := 0;
                    MarginBottom := 0;
                    Orientation  := poPortrait;
	                {$IFDEF DESARROLLO or TESTKTC}
                    	PaperName    := 'Letter';
                    {$ELSE}
                    	PaperName    := 'Carta';
                    {$ENDIF}
                    ShowUser     := False;
                    ShowDateTime := False;
                end;

                RBInterface.SetConfig(RBInterfaceConfig);
                RBInterface.Execute(True);
            end;
        except
        	on e: Exception do
          begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	if Assigned(ReporteContratoAdhesionPAForm) then FreeAndNil(ReporteContratoAdhesionPAForm);
        // SS_1006_PDO_20121122 Inicio Bloque
        if Assigned(spValidarRestriccionesAdhesionPAKAnexo) then begin
        	if spValidarRestriccionesAdhesionPAKAnexo.Active then spValidarRestriccionesAdhesionPAKAnexo.Close;
            FreeAndNil(spValidarRestriccionesAdhesionPAKAnexo);
        end;
        // SS_1006_PDO_20121122 Fin BLoque
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;
//FIN BLOQUE SS-1006-NDR-20120727--------------------------------------------

end.
