{-----------------------------------------------------------------------------
 File Name: Frm_AgregarItemFacturacion.pas
 Author:    aunanue
 Date Created: 24/01/2005
 Language: ES-AR
 Description: Form de confirmaci�n para la edici�n de la fecha de un DayPass
 Firma: TASK_012_AUN_20170420-CU.COBO.CRM.PDS.302
-----------------------------------------------------------------------------}
unit Frm_CambiarFechaUsoPaseDiario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, VariantComboBox, DmiCtrls, DB, ADODB,
  PeaProcs, Util, UtilProc, ExtCtrls, ConstParametrosGenerales;

type
  TFrmCambiarFechaUsoPaseDiario = class(TForm)
    Label1: TLabel;
    edFechaUsoAnterior: TDateEdit;
    btnCancelar: TButton;
    btnAceptar: TButton;
    Label3: TLabel;
    edPatente: TEdit;
    Bevel1: TBevel;
    Label2: TLabel;
    edNumeroSerie: TEdit;
    Label4: TLabel;
    edFechaVencimiento: TDateEdit;
    Label5: TLabel;
    edNuevaFechaUso: TDateEdit;
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    //FExtensionDiasPaseDiario : integer;
    FFechaVenta : TDateTime;
  public
    { Public declarations }
    function Inicializar( sPatente, sNumeroSerie : string; dFechaVenta, dFechaVencimiento, dFechaUso: TDateTime ) : Boolean;
  end;

implementation

uses DMConnection, RStrings;

{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 17/02/2005
  Description: Inicializa el Form
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrmCambiarFechaUsoPaseDiario.Inicializar( sPatente, sNumeroSerie : string; dFechaVenta, dFechaVencimiento, dFechaUso: TDateTime ) : Boolean;
resourcestring
	MSG_ERROR_INITIALIZING_FORM = 'Error cargando el formulario de uso de pase diario';
begin
	result := True;
    try
    	//ObtenerParametroGeneral(DMConnections.BaseCAC, 'MAXIMA_EXTENSION_DIAS_PASE_DIARIO', FExtensionDiasPaseDiario);
    	edPatente.Text 			:= sPatente;
    	edNumeroSerie.Text 		:= sNumeroSerie;
        edFechaUsoAnterior.Date := dFechaUso;
    	edFechaVencimiento.Date := dFechaVencimiento;
        edNuevaFechaUso.Date	:= nulldate;
        FFechaVenta             := dFechaVenta;
        Self.ActiveControl      := edNuevaFechaUso;
    except
    	on e: exception do begin
			MsgBoxErr( MSG_ERROR_INITIALIZING_FORM, e.Message, STR_ERROR, MB_ICONERROR);
        	result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAceptarClick
  Author:    flamas
  Date Created: 24/01/2005
  Description: Valida los valores de los campos para poder aceptar
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TFrmCambiarFechaUsoPaseDiario.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_DATE_MUST_HAVE_A_VALUE				= 'La fecha no puede quedar en blanco';
	MSG_DATE_MUST_BE_GREATER_THAN_TODAY		= 'La fecha de uso debe ser posterior al d�a de Venta';
	MSG_DATE_MUST_BE_LOWER_THAN_EXPIRATION	= 'La fecha de uso debe ser inferior a la fecha de vencimiento';
begin
    if ValidateControls(
    	[edNuevaFechaUso, edNuevaFechaUso, edNuevaFechaUso],
    	[   edNuevaFechaUso.Date <> nulldate,
            edNuevaFechaUso.Date >= FFechaVenta {+ FExtensionDiasPaseDiario TASK_012_AUN_20170420-CU.COBO.CRM.PDS.302},
            edNuevaFechaUso.Date <= edFechaVencimiento.Date],
    	caption,
        [   MSG_DATE_MUST_HAVE_A_VALUE,
        	MSG_DATE_MUST_BE_GREATER_THAN_TODAY,
        	MSG_DATE_MUST_BE_LOWER_THAN_EXPIRATION]) then ModalResult := mrOK;
end;

end.
