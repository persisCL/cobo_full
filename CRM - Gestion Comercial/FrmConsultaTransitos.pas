{********************************** Unit Header ********************************
File Name : FrmConsultaTransitosPendientes.pas
Author : vpaszkowicz
Date Created: 11/04/2007
Language : ES-AR
Description : El objetivo es mostrar una serie de tr�nsitos pendientes y poder
adem�s de ver sus datos, mostrar las im�genes en un visor de im�genes.

    Revision : 1
    Date: 23/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura


Revision 2
Author      : mbecerra
Date        : 30-Septiembre-2010
Description :   (Ref SS 926)
                Se valida que la etiqueta ingresada sea v�lida.

Revision 3
Author		: mbecerra
Date		: 22-Junio-2011
Description	: 	(REF SS 965)
            	Si el tr�nsito no tiene categor�a u otro dato obligatorio, debe permitir ver la imagen

Firma		: SS_965_MBE_20110622

  Firma       : SS_1021_HUR_20120328
  Descripcion : Se agregan los campos Velocidad y Carril.
                Se modifican los indices de las columnas en el procedimiento dbl_TransitosDrawText

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.

Author		: Manuel Cabello Ocaranza
Date		: 14-Febrero-2014
Firma		: SS_1170_MCA_20140214
Description : Se agregan 2 columnas nuevas: Illegal Tampering y Not in Holder (TR y NH)

*******************************************************************************}
unit FrmConsultaTransitos;

interface

uses
  DmConnection,
  UtilProc,
  peaprocs,
  util,
  UtilDB,
  Peatypes,
      //MenuesContextuales,
  // General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx, DB, ADODB,
  DmiCtrls, VariantComboBox, Validate, DateEdit,Abm_obj,RStrings,StrUtils,
  ImgList, TimeEdit;

const
  CONST_RANGO_DIAS = 1;

type
  TFormConsultaTransitos = class(TForm)
    Panel1: TPanel;
  	GroupBox1: TGroupBox;
    Panel2: TPanel;
    dbl_Transitos: TDBListEx;
    ObtenerTransitosFiltro: TADOStoredProc;
    Panel3: TPanel;
    Panel4: TPanel;
	Label1: TLabel;
	txt_FechaDesde: TDateEdit;
    txt_FechaHasta: TDateEdit;
    Label2: TLabel;
	Label6: TLabel;
	edPatente: TEdit;
	dsTransitos: TDataSource;
    Label3: TLabel;
    lblTotales: TLabel;
    lnCheck: TImageList;
    ilCamara: TImageList;
    btn_Filtrar: TButton;
    btn_Limpiar: TButton;
    BtnSalir: TButton;
    lblTelevia: TLabel;
    txtTelevia: TEdit;
    Label4: TLabel;
    HoraDesde: TTimeEdit;
    HoraHasta: TTimeEdit;
    Label5: TLabel;
    cbEstado: TVariantComboBox;
    cbCategoria: TVariantComboBox;
    cbPortico: TVariantComboBox;
    procedure FormShow(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure dbl_TransitosColumns0HeaderClick(Sender: TObject);
    procedure dbl_TransitosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure dbl_TransitosLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure dbl_TransitosCheckLink(Sender: TCustomDBListEx;Column: TDBListExColumn; var IsLink: Boolean);
    procedure BtnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtTeleviaKeyPress(Sender: TObject; var Key: Char);
    procedure dbl_TransitosColumns13HeaderClick(Sender: TObject);               //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns14HeaderClick(Sender: TObject);               //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns7HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns8HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns9HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns10HeaderClick(Sender: TObject);               //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns11HeaderClick(Sender: TObject);               //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns12HeaderClick(Sender: TObject);               //SS_1170_MCA_20140214
  private
    procedure CargarComboEstados(EstadoActual: Integer = 0);
    procedure CargarComboCategorias(CategoriaActual: Integer = 0);
    procedure CargarComboPorticos(PorticoActual: Integer = 0);
	{ Private declarations }
  public
	  { Public declarations }
  	function Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
  end;

var
  FormConsultaTransitos: TFormConsultaTransitos;
                               //caso de querer realizar un reclamo sobre
                               //ellos.
implementation

uses frmVisorImagenes;

{$R *.dfm}

const
    //CONST_COLUMNA_IMAGEN = 13;  // SS_1021_PDO_20120420
	//CONST_COLUMNA_IMAGEN = 15;  //SS_1170_MCA_20140214 // SS_1021_PDO_20120420
    CONST_COLUMNA_IMAGEN = 17;  // SS_1170_MCA_20140214

{******************************** Function Header ******************************
Function Name: CargarComboEstados
Author : vpaszkowicz
Date Created : 12/04/2007
Description : Carga los posibles estados de un tr�nsito en un combo.
Parameters : EstadoActual: Integer = 0
Return Value : None

    Revision 1:
    Date: 23/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura
*******************************************************************************}
procedure TFormConsultaTransitos.CargarComboEstados(EstadoActual: Integer = 0);
resourceString
    MSG_CUALQUIER_ESTADO = 'Todos';
var
    indice: Integer;
	  Qry: TADOQuery;
begin
	  Qry := TAdoQuery.Create(nil);
	  try
		    Qry.Connection := DmConnections.BaseCAC;
		    Qry.SQL.Text := ' select * from EstadoTransitos  WITH (NOLOCK) ORDER BY Descripcion';
		    Qry.Open;
		    CbEstado.Clear;
        CbEstado.Items.Add(MSG_CUALQUIER_ESTADO, 0);
        indice := 0;
        While not Qry.Eof do begin
			      CbEstado.Items.Add(Qry.FieldByName('Descripcion').AsString,
            Istr(Qry.FieldByName('Estado').AsInteger));
			      if (Qry.FieldByName('Estado').AsInteger = EstadoActual) then begin
            	indice := CbEstado.Items.Count - 1;
        end;
        Qry.Next;
		end;
		CbEstado.ItemIndex := 0;
	  finally
		    Qry.Close;
		    Qry.Free;
	  end;
end;

{******************************** Function Header ******************************
Function Name: CargarComboEstados
Author : vpaszkowicz
Date Created : 12/04/2007
Description : Carga las categorias en un combo para poder seleccionarlas
Parameters : EstadoActual: Integer = 0
Return Value : None

    Revision 1:
    Date: 23/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura
*******************************************************************************}
procedure TFormConsultaTransitos.CargarComboCategorias(CategoriaActual: Integer = 0);
resourceString
    MSG_CUALQUIER_CATEGORIA = 'Todas';
var
    indice: Integer;
	  Qry: TADOQuery;
begin
	  Qry := TAdoQuery.Create(nil);
	  try
		    Qry.Connection := DmConnections.BaseCAC;
		    Qry.SQL.Text := ' select * from Categorias  WITH (NOLOCK) ORDER BY Descripcion';
		    Qry.Open;
		    CbCategoria.Clear;
        CbCategoria.Items.Add(MSG_CUALQUIER_CATEGORIA, 0);
        indice := 0;
        While not Qry.Eof do begin
			      CbCategoria.Items.Add(Qry.FieldByName('Descripcion').AsString,
            Istr(Qry.FieldByName('Categoria').AsInteger));
			      if (Qry.FieldByName('Categoria').AsInteger = CategoriaActual) then begin
                indice := CbCategoria.Items.Count - 1;
        end;
			  Qry.Next;
		end;
		CbCategoria.ItemIndex := 0;
  	finally
	  	Qry.Close;
		  Qry.Free;
	  end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarComboPorticos
  Author:
  Date Created:  /  /
  Description:
  Parameters: PorticoActual: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 23/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormConsultaTransitos.CargarComboPorticos(PorticoActual: Integer = 0);
resourceString
    MSG_CUALQUIER_PORTICO = 'Todas';
var
    indice: Integer;
	  Qry: TADOQuery;
begin
	  Qry := TAdoQuery.Create(nil);
	  try
		    Qry.Connection := DmConnections.BaseCAC;
		    Qry.SQL.Text := ' select NumeroPuntoCobro, Descripcion from PuntosCobro  WITH (NOLOCK) ORDER BY Descripcion';
		    Qry.Open;
		    CbPortico.Clear;
        CbPortico.Items.Add(MSG_CUALQUIER_PORTICO, 0);
        indice := 0;
        While not Qry.Eof do begin
			      CbPortico.Items.Add(Qry.FieldByName('Descripcion').AsString,
            Istr(Qry.FieldByName('NumeroPuntoCobro').AsInteger));
			      if (Qry.FieldByName('NumeroPuntoCobro').AsInteger = PorticoActual) then begin
                indice := CbPortico.Items.Count - 1;
        end;
			  Qry.Next;
		end;
		CbPortico.ItemIndex := 0;
  	finally
	  	Qry.Close;
		  Qry.Free;
	  end;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author : vpaszkowicz
Date Created : 11/04/2007
Description : Inicializar el formulario.
Parameters : txtCaption: String; MDIChild: Boolean
Return Value : Boolean
*******************************************************************************}
function TFormConsultaTransitos.Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
    var S: TSize;
begin
    Result := True;
	  try
        if MDIChild then begin
            S := GetFormClientSize(Application.MainForm);
            SetBounds(0, 0, S.cx, S.cy);
        end else begin
            FormStyle := fsNormal;
            Visible := False;
        end;
        CenterForm(Self);
        Caption := AnsiReplaceStr(txtCaption, '&', '');
        txt_FechaHasta.Date:=Now;
        btn_Limpiar.Click;
        CargarComboEstados(0);
        CargarComboCategorias(0);
        CargarComboPorticos(0);
	  except
		  result := False;
	  end;
    lblTotales.Caption := '';
end;

{******************************** Function Header ******************************
Function Name: FormShow
Author : vpaszkowicz
Date Created : 11/04/2007
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormConsultaTransitos.FormShow(Sender: TObject);
begin
    edPatente.SetFocus;
end;

{******************************** Function Header ******************************
Function Name: btn_FiltrarClick
Author : vpaszkowicz
Date Created : 11/04/2007
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormConsultaTransitos.btn_FiltrarClick(Sender: TObject);
resourcestring
    MSG_LICENSE_PLATE		    = 'Debe ingresar un valor de patente';
    MSG_ERROR_LOADING_TRANSITS	= 'Error cargando Tr�nsitos';
    MSG_FILTER                  = 'Debe ingresar o la patente o el telev�a o una fecha desde y hasta que permita filtrar los tr�nsitos';
    MSG_TELEVIA                 = 'El n�mero de Telev�a no es correcto';
    MSG_VALIDAR_RANGO_DIAS      = 'Si no filtra por patente o telev�a, la diferencia entre fechas puede ser de %d d�as/s';
    MSG_BUSQUEDA_VACIA          = 'No se encontr� ning�n registro con esos par�metros de b�squeda';
begin
    ObtenerTransitosFiltro.Close;
    
    if not validatecontrols([edPatente],[(Trim(edPatente.Text) <> '')or(Trim(txtTelevia.Text) <> '')
        or((txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate))],caption,[MSG_FILTER]) then exit;

	  if (txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate) and (txt_FechaDesde.Date>txt_FechaHasta.Date) then begin
        MsgBoxBalloon(MSG_VALIDAR_ORDEN_FECHA,Caption,MB_ICONSTOP,txt_FechaDesde);
        Exit;
    end;

    //Verifica que la fecha este entre las 24 hs si no se ingresaron patente o telev�a
    if (Trim(edPatente.Text) = '') and (Trim(txtTelevia.Text) = '') and
      (txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate) and
        ((txt_FechaHasta.Date + HoraHasta.Time) - (txt_FechaDesde.Date + HoraDesde.Time) > CONST_RANGO_DIAS)then begin
        MsgBoxBalloon(Format(MSG_VALIDAR_RANGO_DIAS, [CONST_RANGO_DIAS]),Caption,MB_ICONSTOP,txt_FechaDesde);
        Exit;
    end;

    // Verifica que el numero de televia este compuesto solamente de n�meros y tenga mas de 10 digitos.
    if (txtTelevia.Text <> '') and (
            (Length(Trim(TxtTelevia.Text)) < 10) or (not PermitirSoloDigitos(txtTelevia.Text)) or
            (txtTelevia.Text <> SerialNumberToEtiqueta(EtiquetaToSerialNumber(txtTelevia.Text)))
        ) then  begin

        MsgBoxBalloon(MSG_TELEVIA, Caption, MB_ICONSTOP, txtTelevia);
        Exit;
    end;

    ObtenerTransitosFiltro.DisableControls;
    try
        ObtenerTransitosFiltro.Close;
        ObtenerTransitosFiltro.CommandTimeout := 500;
        ObtenerTransitosFiltro.Parameters.Refresh;
        ObtenerTransitosFiltro.Parameters.ParamByName('@Patente').Value 		 := iif(edPatente.Text = '',null,edpatente.text);
        ObtenerTransitosFiltro.Parameters.ParamByName('@FechaDesde').Value		 := iif(txt_FechaDesde.Date=nulldate,null,FormatDateTime('yyyymmdd hh:nn',(txt_FechaDesde.Date + HoraDesde.Time)));
        ObtenerTransitosFiltro.Parameters.ParamByName('@FechaHasta').Value		 := iif(txt_FechaHasta.Date=nulldate,null,FormatDateTime('yyyymmdd hh:nn',(txt_FechaHasta.Date + HoraHasta.Time)));
        ObtenerTransitosFiltro.Parameters.ParamByName('@Categoria').Value:= iif(CbCategoria.Value = 0, Null, CbCategoria.Value);
        ObtenerTransitosFiltro.Parameters.ParamByName('@Estado').Value:= iif(CbEstado.Value = 0, Null, CbEstado.Value);
        ObtenerTransitosFiltro.Parameters.ParamByName('@Portico').Value:= iif(CbPortico.Value = 0, Null, CbPortico.Value);
        ObtenerTransitosFiltro.Parameters.ParamByName('@CantidadTransitos').Value:= 0;
        ObtenerTransitosFiltro.Parameters.ParamByName('@ContractSerialNumber').Value := iif(txtTelevia.Text = '', Null, Peaprocs.EtiquetaToSerialNumber(txtTelevia.Text));

		if not(OpenTables([ObtenerTransitosFiltro])) then begin
			  MsgBox(MSG_ERROR_LOADING_TRANSITS,Caption, MB_ICONERROR);
        end;

        lblTotales.Caption := iif(ObtenerTransitosFiltro.IsEmpty, '', 'Visualizados ' + IntToStr(ObtenerTransitosFiltro.RecordCount) +
																	' tr�nsitos') ;

        if ObtenerTransitosFiltro.IsEmpty then begin
            MsgBox(MSG_BUSQUEDA_VACIA, Caption, MB_ICONINFORMATION);
        end;

    finally
	    ObtenerTransitosFiltro.EnableControls;
    end;
	  dbl_Transitos.SetFocus;
end;

{******************************** Function Header ******************************
Function Name: btn_LimpiarClick
Author : vpaszkowicz
Date Created : 11/04/2007
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormConsultaTransitos.btn_LimpiarClick(Sender: TObject);
begin
      edPatente.Clear;
      txtTelevia.Clear;
	  txt_FechaDesde.Clear;
	  txt_FechaHasta.Clear;
      HoraDesde.Time := 0;
      HoraHasta.Time := 0;
      CargarComboCategorias;
      CargarComboEstados;
      CargarComboPorticos;
	  ObtenerTransitosFiltro.close;
	  edPatente.SetFocus;
end;

{******************************** Function Header ******************************
Function Name: dbl_TransitosColumns0HeaderClick
Author : vpaszkowicz
Date Created : 11/04/2007
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormConsultaTransitos.dbl_TransitosColumns0HeaderClick(Sender: TObject);
begin
	  if ObtenerTransitosFiltro.IsEmpty then Exit;

	  if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   	  else TDBListExColumn(sender).Sorting := csAscending;

	  ObtenerTransitosFiltro.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

procedure TFormConsultaTransitos.dbl_TransitosColumns10HeaderClick(             //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dbl_Transitos.DataSource.DataSet.Eof then                            //SS_1170_MCA_20140214
		MsgBoxBalloon('Lista Gris',                               				//SS_1170_MCA_20140214
        				'',                                                		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormConsultaTransitos.dbl_TransitosColumns11HeaderClick(             //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dbl_Transitos.DataSource.DataSet.Eof then                            //SS_1170_MCA_20140214
		MsgBoxBalloon('Tomar Fotograf�a',                               	    //SS_1170_MCA_20140214
        				'',                                                		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormConsultaTransitos.dbl_TransitosColumns12HeaderClick(             //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dbl_Transitos.DataSource.DataSet.Eof then                            //SS_1170_MCA_20140214
		MsgBoxBalloon('Cont�cte al Operador',                               	//SS_1170_MCA_20140214
        				'',                                                		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormConsultaTransitos.dbl_TransitosColumns13HeaderClick(             //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dbl_Transitos.DataSource.DataSet.Eof then                            //SS_1170_MCA_20140214
	 	MsgBoxBalloon('Telev�a Removido',                                       //SS_1170_MCA_20140214
        				'',                                                		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormConsultaTransitos.dbl_TransitosColumns14HeaderClick(             //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dbl_Transitos.DataSource.DataSet.Eof then                            //SS_1170_MCA_20140214
		MsgBoxBalloon('Telev�a fuera de su base',                               //SS_1170_MCA_20140214
        				'',                                                		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;

procedure TFormConsultaTransitos.dbl_TransitosColumns7HeaderClick(              //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dbl_Transitos.DataSource.DataSet.Eof then                            //SS_1170_MCA_20140214
		MsgBoxBalloon('Lista Negra',				                            //SS_1170_MCA_20140214
        				'',                                                		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormConsultaTransitos.dbl_TransitosColumns8HeaderClick(              //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dbl_Transitos.DataSource.DataSet.Eof then                            //SS_1170_MCA_20140214
		MsgBoxBalloon('Lista Amarilla',                               			//SS_1170_MCA_20140214
        				'',                                                		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormConsultaTransitos.dbl_TransitosColumns9HeaderClick(              //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dbl_Transitos.DataSource.DataSet.Eof then                            //SS_1170_MCA_20140214
		MsgBoxBalloon('Lista Verde',                               				//SS_1170_MCA_20140214
        				'',                                                		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214


{******************************** Function Header ******************************
Function Name: txtTeleviaKeyPress
Author : vpaszkowicz
Date Created : 11/04/2007
Description :
Parameters : Sender: TObject; var Key: Char
Return Value : None
*******************************************************************************}
procedure TFormConsultaTransitos.txtTeleviaKeyPress(Sender: TObject;
  var Key: Char);
begin
    // #08: Backspace.
    if (not (Key in ['0'..'9', #08])) then Exit;
end;

{******************************** Function Header ******************************
Function Name: dbl_TransitosDrawText
Author : vpaszkowicz
Date Created : 11/04/2007
Description :
Parameters : Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean
Return Value : None
*******************************************************************************}
procedure TFormConsultaTransitos.dbl_TransitosDrawText(Sender: TCustomDBListEx;
Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
var
	i: Integer;
begin
    // Revision 1
    // Armo las imagenes en las celdas de la grilla donde se muestra un campo boolean

    //BlackList

    //if Column = dbl_transitos.Columns[5] then begin
    if Column.FieldName = 'BlackList' then begin
          if (ObtenerTransitosFiltro.FieldByName('BlackList').Value = 0) then i := 0
          else i := 1;
      lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
      DefaultDraw := False;
      ItemWidth   := lnCheck.Width + 2;
    end;
    // YellowList
    // if Column = dbl_transitos.Columns[6] then begin
    if Column.FieldName = 'YellowList' then begin
        if (ObtenerTransitosFiltro.FieldByName('YellowList').Value = 0) then i := 0
        else i := 1;
      lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
      DefaultDraw := False;
      ItemWidth   := lnCheck.Width + 2;
    end;
    //GreenList
    //if Column = dbl_transitos.Columns[7] then begin
    if Column.FieldName = 'GreenList' then begin
        if (ObtenerTransitosFiltro.FieldByName('GreenList').Value = 0) then i := 0
        else i := 1;
      lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
      DefaultDraw := False;
      ItemWidth   := lnCheck.Width + 2;
    end;
    //GrayList
    //if Column = dbl_transitos.Columns[8] then begin
    if Column.FieldName = 'GrayList' then begin
        if(ObtenerTransitosFiltro.FieldByName('GrayList').value = 0) then i := 0
        else i := 1;
      lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
      DefaultDraw := False;
      ItemWidth   := lnCheck.Width + 2;
    end;
    //ExceptionHandling
    if Column.FieldName = 'ExceptionHandling' then begin
        if(ObtenerTransitosFiltro.FieldByName('ExceptionHandling').value = 0) then i := 0
        else i := 1;
        lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
        DefaultDraw := False;
        ItemWidth   := lnCheck.Width + 2;
    end;
    //ExceptionHandling
    if Column.FieldName = 'MMIContactOperator' then begin
        if(ObtenerTransitosFiltro.FieldByName('MMIContactOperator').value = 0) then i := 0
        else i := 1;
        lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
        DefaultDraw := False;
        ItemWidth   := lnCheck.Width + 2;
    end;
    //IllegalTampering
    if Column.FieldName = 'IllegalTampering' then begin                                         //SS_1170_MCA_20140214
        if(ObtenerTransitosFiltro.FieldByName('IllegalTampering').value = 0) then i := 0        //SS_1170_MCA_20140214
        else i := 1;                                                                            //SS_1170_MCA_20140214
        lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);                     //SS_1170_MCA_20140214
        DefaultDraw := False;                                                                   //SS_1170_MCA_20140214
        ItemWidth   := lnCheck.Width + 2;                                                       //SS_1170_MCA_20140214
    end;                                                                                        //SS_1170_MCA_20140214
    //NotInHolder
    if Column.FieldName = 'NotInHolder' then begin                                              //SS_1170_MCA_20140214
        if(ObtenerTransitosFiltro.FieldByName('NotInHolder').value = 0) then i := 0             //SS_1170_MCA_20140214
        else i := 1;                                                                            //SS_1170_MCA_20140214
        lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);                     //SS_1170_MCA_20140214
        DefaultDraw := False;                                                                   //SS_1170_MCA_20140214
        ItemWidth   := lnCheck.Width + 2;                                                       //SS_1170_MCA_20140214
    end;                                                                                        //SS_1170_MCA_20140214

    // Inicio bloque SS_1021_PDO_20120420
    {
    if Column = dbl_Transitos.Columns[7] then begin       // SS_1021_HUR_20120328
      Text := FormatDateTime('dd/mm/yyyy hh:mm', ObtenerTransitosFiltro.FieldByName('FechaHoraTransito').AsDateTime)
    end	else begin
      if (Column = dbl_Transitos.Columns[CONST_COLUMNA_IMAGEN]) and
        (ObtenerTransitosFiltro.FieldByName('RegistrationAccessibility').AsInteger > 0)
      then begin
        ilCamara.Draw(dbl_transitos.Canvas, Rect.Left + 20, Rect.Top + 1,
                iif(ObtenerTransitosFiltro.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0));
        DefaultDraw := False;
        ItemWidth   := ilCamara.Width + 2;
      end
    end;
    }
    if column.FieldName = 'FechaHoraTransito' then begin
      Text := FormatDateTime('dd/mm/yyyy hh:mm', ObtenerTransitosFiltro.FieldByName('FechaHoraTransito').AsDateTime)
    end;

    if (Column = dbl_Transitos.Columns[CONST_COLUMNA_IMAGEN]) and
        (ObtenerTransitosFiltro.FieldByName('RegistrationAccessibility').AsInteger > 0) then begin
        ilCamara.Draw(dbl_transitos.Canvas, Rect.Left + 20, Rect.Top + 1,
                iif(ObtenerTransitosFiltro.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0));
        DefaultDraw := False;
        ItemWidth   := ilCamara.Width + 2;
    end
    // Fin bloque SS_1021_PDO_20120420
end;

{******************************** Function Header ******************************
Function Name: dbl_TransitosLinkClick
Author : vpaszkowicz
Date Created : 11/04/2007
Description :
Parameters : Sender: TCustomDBListEx; Column: TDBListExColumn
Return Value : None
*******************************************************************************}
procedure TFormConsultaTransitos.dbl_TransitosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
Var
	Numcorrco: string;
    EsItaliano : Boolean;                                                           // SS_1091_CQU_20130516
begin
  	if Column = dbl_Transitos.Columns[CONST_COLUMNA_IMAGEN] then
        EsItaliano := ObtenerTransitosFiltro.FieldByName('EsItaliano').AsBoolean;   // SS_1091_CQU_20130516
      MostrarVentanaImagen	(	Self,
      			ObtenerTransitosFiltro['NumCorrCA'],
                ObtenerTransitosFiltro['RegistrationAccessibility'],
                ObtenerTransitosFiltro['FechaHoraTransito'],
                iif(ObtenerTransitosFiltro['Patente'] = Null, '', ObtenerTransitosFiltro['Patente']),
                iif(ObtenerTransitosFiltro['Televia'] = Null, '', ObtenerTransitosFiltro['Televia']),
          		IIf(ObtenerTransitosFiltro['EstadoTransito'] = null, '', ObtenerTransitosFiltro['EstadoTransito']),			//SS_965_MBE_20110622
          		IIf(ObtenerTransitosFiltro['Categoria'] = null, '', ObtenerTransitosFiltro['Categoria']),                   //SS_965_MBE_20110622
          		ObtenerTransitosFiltro['PuntoCobro'],
                EsItaliano                      // SS_1091_CQU_20130516
                			);
end;

{******************************** Function Header ******************************
Function Name: dbl_TransitosCheckLink
Author : vpaszkowicz
Date Created : 11/04/2007
Description :
Parameters : Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean
Return Value : None
*******************************************************************************}
procedure TFormConsultaTransitos.dbl_TransitosCheckLink(Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
    if column = dbl_Transitos.Columns[CONST_COLUMNA_IMAGEN] then begin
		IsLink := (Column = dbl_Transitos.Columns[CONST_COLUMNA_IMAGEN]) and
	  		(ObtenerTransitosFiltro['RegistrationAccessibility'] > 0);
    end;
end;

{******************************** Function Header ******************************
Function Name: BtnSalirClick
Author : vpaszkowicz
Date Created : 11/04/2007
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormConsultaTransitos.BtnSalirClick(Sender: TObject);
begin
	ObtenerTransitosFiltro.Close;
	Close;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : vpaszkowicz
Date Created : 11/04/2007
Description :
Parameters : Sender: TObject; var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TFormConsultaTransitos.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
    CerrarVentanaImagen(Self);
    action := caFree;
end;

end.
