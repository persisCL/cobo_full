object formConvenioPromociones: TformConvenioPromociones
  Left = 141
  Top = 117
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Promociones del convenio'
  ClientHeight = 518
  ClientWidth = 706
  Color = clBtnFace
  Constraints.MaxHeight = 800
  Constraints.MaxWidth = 1000
  Constraints.MinHeight = 240
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GBPromociones: TGroupBox
    Left = 0
    Top = 185
    Width = 361
    Height = 294
    Align = alLeft
    Caption = 'Promociones del convenio'
    TabOrder = 0
    object CheckListPromociones: TCheckListBox
      Left = 2
      Top = 15
      Width = 357
      Height = 277
      Align = alClient
      ItemHeight = 13
      Items.Strings = (
        'Cantidad de Transitos'
        'Importe de viajes'
        'Cantidad de viajes'
        'Importe de viajes')
      TabOrder = 0
    end
  end
  object PanelDeAbajo: TPanel
    Left = 0
    Top = 479
    Width = 706
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      706
      39)
    object BtnCancelar: TDPSButton
      Left = 624
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Cancelar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtnCancelarClick
    end
    object btnAceptar: TDPSButton
      Left = 542
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Aceptar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Aceptar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
  end
  object GBPersona: TGroupBox
    Left = 0
    Top = 0
    Width = 706
    Height = 96
    Align = alTop
    Caption = 'Datos personales'
    TabOrder = 2
    inline FrameDatosPersonaConsulta1: TFrameDatosPersonaConsulta
      Left = 8
      Top = 16
      Width = 544
      Height = 68
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 96
    Width = 706
    Height = 89
    Align = alTop
    Caption = 'Convenio'
    TabOrder = 3
    inline FrameDatosConvenioConsulta1: TFrameDatosConvenioConsulta
      Left = 7
      Top = 13
      Width = 662
      Height = 67
      TabOrder = 0
    end
  end
  object GBDetallePromocion: TGroupBox
    Left = 366
    Top = 185
    Width = 340
    Height = 294
    Align = alRight
    Caption = 'Detalle de la promoci'#243'n'
    TabOrder = 4
    object REditPlanComercial: TRichEdit
      Left = 2
      Top = 15
      Width = 336
      Height = 277
      Align = alClient
      TabOrder = 0
    end
  end
end
