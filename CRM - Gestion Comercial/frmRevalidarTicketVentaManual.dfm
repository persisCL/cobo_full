object RevalidarTicketVentaManualForm: TRevalidarTicketVentaManualForm
  Left = 78
  Top = 17
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  Caption = 'Revalidaci'#243'n de Ticket Venta Manual'
  ClientHeight = 564
  ClientWidth = 974
  Color = 16776699
  Constraints.MinHeight = 579
  Constraints.MinWidth = 900
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 337
    Width = 974
    Height = 5
    Cursor = crVSplit
    Align = alTop
    Color = clSkyBlue
    ParentColor = False
    ExplicitWidth = 858
  end
  object pnlConsulta: TPanel
    Left = 0
    Top = 0
    Width = 974
    Height = 113
    Align = alTop
    TabOrder = 0
    DesignSize = (
      974
      113)
    object Label1: TLabel
      Left = 40
      Top = 27
      Width = 42
      Height = 13
      Caption = 'Patente:'
    end
    object Label2: TLabel
      Left = 273
      Top = 28
      Width = 97
      Height = 13
      Caption = 'Fecha Venta Desde:'
    end
    object Label3: TLabel
      Left = 275
      Top = 55
      Width = 95
      Height = 13
      Caption = 'Fecha Venta Hasta:'
    end
    object Label4: TLabel
      Left = 45
      Top = 54
      Width = 37
      Height = 13
      Caption = 'Estado:'
    end
    object Label5: TLabel
      Left = 299
      Top = 82
      Width = 71
      Height = 13
      Caption = 'Concesionaria:'
    end
    object edtPatente: TEdit
      Left = 88
      Top = 24
      Width = 89
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
    end
    object deDesde: TDateEdit
      Left = 376
      Top = 24
      Width = 121
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
    object deHasta: TDateEdit
      Left = 376
      Top = 51
      Width = 121
      Height = 21
      AutoSelect = False
      TabOrder = 4
      Date = -693594.000000000000000000
    end
    object vcbEstado: TVariantComboBox
      Left = 88
      Top = 51
      Width = 145
      Height = 21
      Style = vcsDropDownList
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 3
      Items = <
        item
          Caption = 'No Asignado'
          Value = '0'
        end
        item
          Caption = 'Asignado'
          Value = '1'
        end
        item
          Caption = 'Todos'
          Value = '2'
        end>
    end
    object btnBuscar: TButton
      Left = 616
      Top = 22
      Width = 75
      Height = 25
      Caption = 'Buscar'
      TabOrder = 7
      OnClick = btnBuscarClick
    end
    object vcbConcesionarias: TVariantComboBox
      Left = 376
      Top = 78
      Width = 209
      Height = 21
      Style = vcsDropDownList
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 6
      Items = <>
    end
    object btnSalir: TButton
      Left = 859
      Top = 22
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Salir'
      TabOrder = 8
      OnClick = btnSalirClick
    end
    object edtHoraVentaDesde: TTimeEdit
      Left = 503
      Top = 24
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 2
      AllowEmpty = False
      ShowSeconds = False
    end
    object edtHoraVentaHasta: TTimeEdit
      Left = 503
      Top = 51
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 5
      AllowEmpty = False
      Time = 0.999305555555555600
      ShowSeconds = False
    end
  end
  object pnlTVM: TPanel
    Left = 0
    Top = 113
    Width = 974
    Height = 224
    Align = alTop
    TabOrder = 1
    object dblListadoTVM: TDBListEx
      Left = 1
      Top = 1
      Width = 972
      Height = 181
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Numero TVM'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroTVM'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 110
          Header.Caption = 'Fecha Hora Venta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHoraVenta'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'NumCorrCA'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'NumCorrCA'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Categor'#237'a'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'Categoria'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'Importe'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 70
          Header.Caption = 'Via Venta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'ViaVenta'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Imagen'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
        end>
      DataSource = dsTicketVentaManual
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnCheckLink = dblListadoTVMCheckLink
      OnDblClick = dblListadoTVMDblClick
      OnDrawText = dblListadoTVMDrawText
      OnLinkClick = dblListadoTVMLinkClick
    end
    object pnlBotonesTVM: TPanel
      Left = 1
      Top = 182
      Width = 972
      Height = 41
      Align = alBottom
      TabOrder = 1
      object btnEditarPatente: TButton
        Left = 866
        Top = 6
        Width = 94
        Height = 25
        Caption = 'Editar Patente'
        TabOrder = 0
        OnClick = btnEditarPatenteClick
      end
    end
  end
  object pnlTransitos: TPanel
    Left = 0
    Top = 342
    Width = 974
    Height = 222
    Align = alClient
    TabOrder = 2
    object dblListadoTransitos: TDBListEx
      Left = 1
      Top = 66
      Width = 972
      Height = 110
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'NumCorrCA'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumCorrCA'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 130
          Header.Caption = 'Patente Detectada'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Patente Cuenta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'PatenteCuenta'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Fecha Transito'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHoraTransito'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Categor'#237'a'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Categoria'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'EstadoTransito'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Telev'#237'a'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Televia'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'NumCorrCO'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumCorrCO'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Imagen'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
        end>
      DataSource = dsObtenerTransitos
      DragReorder = True
      ParentColor = False
      TabOrder = 1
      TabStop = True
      OnCheckLink = dblListadoTransitosCheckLink
      OnDrawText = dblListadoTransitosDrawText
      OnLinkClick = dblListadoTransitosLinkClick
    end
    object pnlFiltroTransitos: TPanel
      Left = 1
      Top = 1
      Width = 972
      Height = 65
      Align = alTop
      TabOrder = 0
      object Label6: TLabel
        Left = 24
        Top = 16
        Width = 48
        Height = 13
        Caption = 'Tr'#225'nsitos:'
      end
      object Label7: TLabel
        Left = 87
        Top = 16
        Width = 42
        Height = 13
        Caption = 'Estados:'
      end
      object lblRangoFecha: TLabel
        Left = 362
        Top = 16
        Width = 172
        Height = 13
        Align = alCustom
        Caption = 'Rango Fecha Transito (+/- minutos)'
      end
      object peEstadosTtos: TPickEdit
        Left = 135
        Top = 11
        Width = 217
        Height = 21
        Enabled = True
        ReadOnly = True
        TabOrder = 0
        EditorStyle = bteTextEdit
        OnButtonClick = peEstadosTtosButtonClick
      end
      object edtRangoFecha: TNumericEdit
        Left = 543
        Top = 13
        Width = 121
        Height = 21
        MaxLength = 7
        TabOrder = 1
        Value = 5.000000000000000000
      end
      object btnBuscarTransitos: TButton
        Left = 753
        Top = 11
        Width = 100
        Height = 25
        Caption = 'Buscar Tr'#225'nsitos'
        TabOrder = 2
        OnClick = btnBuscarTransitosClick
      end
    end
    object pnlBotonesGenerales: TPanel
      Left = 1
      Top = 176
      Width = 972
      Height = 45
      Align = alBottom
      TabOrder = 2
      object btnAsociar: TButton
        Left = 885
        Top = 13
        Width = 75
        Height = 25
        Caption = 'Asociar'
        TabOrder = 0
        OnClick = btnAsociarClick
      end
    end
    object chklstEstadosTtos: TCheckListBox
      Left = 135
      Top = 33
      Width = 217
      Height = 97
      ItemHeight = 13
      TabOrder = 3
      OnExit = OcultarListaEstados
    end
  end
  object spObtenerConcesionarias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConcesionarias'
    Parameters = <>
    Left = 8
    Top = 184
  end
  object spObtenerTransitosEstados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTransitosEstados'
    Parameters = <>
    Left = 40
    Top = 184
  end
  object spObtenerTicketsVentaManual: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterScroll = spObtenerTicketsVentaManualAfterScroll
    ProcedureName = 'ObtenerTicketsVentaManual'
    Parameters = <>
    Left = 72
    Top = 184
  end
  object dsTicketVentaManual: TDataSource
    DataSet = spObtenerTicketsVentaManual
    Left = 104
    Top = 184
  end
  object ilCamara: TImageList
    Left = 136
    Top = 184
    Bitmap = {
      494C010102000800500010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A4A004A4A
      4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4A4A0000EFFF0000EF
      FF0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF0000EF
      FF0000EFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4A4A0000EFFF00B5FF
      FF00B5FFFF00B5FFFF00848484004A4A4A004A4A4A004A4A4A00B5FFFF00B5FF
      FF00B5FFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4A4A0000EFFF00B5FF
      FF00FFFFFF004A4A4A0084848400B5B5B500B5B5B500848484004A4A4A00FFFF
      FF00B5FFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4A4A0000EFFF00B5FF
      FF00FFFFFF004A4A4A008484840084848400848484004A4A4A004A4A4A00FFFF
      FF00B5FFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4A4A0000EFFF00B5FF
      FF00FFFFFF004A4A4A0084848400B5B5B500B5B5B500848484004A4A4A00FFFF
      FF00B5FFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4A4A0000EFFF00B5FF
      FF00B5FFFF00B5FFFF004A4A4A004A4A4A004A4A4A004A4A4A00B5FFFF00B5FF
      FF00B5FFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4A4A0000EFFF0000EF
      FF0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF0000EF
      FF0000EFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A4A004A4A
      4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000004A4A
      4A0000EFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000004A4A
      4A004A4A4A004A4A4A004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001FFFF00000000
      8001FFFF000000008001C0030000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      80018001000000008001C003000000008001E1FF000000008001E1FF00000000
      8001FFFF00000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object spObtenerTransitos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTransitosFiltro'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftString
        Size = 14
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 14
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Portico'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CantidadTransitos'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 8
    Top = 432
  end
  object dsObtenerTransitos: TDataSource
    DataSet = spObtenerTransitos
    Left = 40
    Top = 432
  end
  object spAsignarTransito_A_TVM: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AsignarTransito_A_TVM'
    Parameters = <>
    Left = 848
    Top = 528
  end
end
