unit FrmConvenioExterno;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Peatypes,RStrings,
  Dialogs, ExtCtrls, StdCtrls, ListBoxEx, DBListEx,DMConnection, DB, ADODB,Peaprocs,UtilProc,
  DPSControls,frmMediosPagosConvenio,Util,frmImprimirConvenio,ConstParametrosGenerales;

type
  TFormConvenioExterno = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    DBListEx1: TDBListEx;
    txt_NumeroDocumento: TEdit;
    txt_NumeroConvenio: TEdit;
    Panel1: TPanel;
    btn_ImprimirPAT: TDPSButton;
    BtnSalir: TDPSButton;
    ObtenerVehiculosContratados: TADOStoredProc;
    DataSource: TDataSource;
    btn_ImprimirPAC: TDPSButton;
    procedure BtnSalirClick(Sender: TObject);
    procedure ObtenerVehiculosContratadosAfterOpen(DataSet: TDataSet);
    procedure btn_ImprimirPATClick(Sender: TObject);
  private
    { Private declarations }
  public
    function Inicializar(NumeroConvenio,Caption:String):Boolean;
  end;

var
  FormConvenioExterno: TFormConvenioExterno;

implementation

{$R *.dfm}

procedure TFormConvenioExterno.BtnSalirClick(Sender: TObject);
begin
    close;
end;

//BEGIN : SS_1147Q_20141202--------------------------------------------------------------------
function TFormConvenioExterno.Inicializar(NumeroConvenio,Caption:String): Boolean;
begin
    try
        self.Caption:=Caption;
        with ObtenerVehiculosContratados do begin
            close;
            if ObtenerCodigoConcesionariaNativa=CODIGO_VS then
            begin
              Parameters.ParamByName('@NumeroDocumento').Value:=copy(NumeroConvenio,1,9);
              txt_NumeroDocumento.Text:=copy(NumeroConvenio,1,9);
            end
            else
            begin
              Parameters.ParamByName('@NumeroDocumento').Value:=copy(NumeroConvenio,4,11);
              txt_NumeroDocumento.Text:=copy(NumeroConvenio,4,11);
            end;
            open;
        end;
        txt_NumeroConvenio.Text:=NumeroConvenio;
        result:=True;
    except
       result:=False;
    end;
end;
//END : SS_1147Q_20141202--------------------------------------------------------------------

procedure TFormConvenioExterno.ObtenerVehiculosContratadosAfterOpen(
  DataSet: TDataSet);
begin
    btn_ImprimirPAT.Enabled:=ObtenerVehiculosContratados.RecordCount>0;
    btn_ImprimirPAC.Enabled:=btn_ImprimirPAT.Enabled;
end;

procedure TFormConvenioExterno.btn_ImprimirPATClick(Sender: TObject);
var
    f:TFormMediosPagosConvenio;
    fImprimir:TFormImprimirConvenio;
    auxDatosMedioPago:TDatosMediosPago;
    auxRegDatoMandante:TDatosPersonales;
    auxDatoPersona:TDatosPersonales;
    CantidadCopias:Integer;
    MotivoCanelacion:TMotivoCancelacion;
begin
    Application.CreateForm(TFormMediosPagosConvenio,f);

    auxDatosMedioPago.TipoMedioPago:=iif(TDPSButton(sender)=btn_ImprimirPAT,PAT,PAC);
    f.inicializar(LimpiarCaracteresLabel(Caption),auxDatosMedioPago, auxRegDatoMandante,auxDatoPersona);
    if f.ShowModal = mrok then begin
        Application.CreateForm(TFormImprimirConvenio,fImprimir);
        auxDatosMedioPago:=f.RegistroMedioPago;
        auxRegDatoMandante:=f.RegistroPersonaMandante;

        if MsgBox(MSG_CAPTION_PAPEL_BLANCO,MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONWARNING+MB_YESNO)=mrNo then begin
            f.Destroy;
            fImprimir.Destroy;
            exit;
        end;

        if auxDatosMedioPago.TipoMedioPago=PAC then begin
            ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_MANDATO_PAC,CantidadCopias);
            fImprimir.ImprimirMandatoPAC(trim(txt_NumeroConvenio.Text),auxDatosMedioPago,auxRegDatoMandante,MotivoCanelacion,CantidadCopias);
        end else begin
            ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_MANDATO_PAT,CantidadCopias);
            fImprimir.ImprimirMandatoPAT(trim(txt_NumeroConvenio.Text),auxDatosMedioPago,auxRegDatoMandante,MotivoCanelacion,CantidadCopias);
        end;
        fImprimir.Destroy;
    end;
    f.Destroy;
end;

end.
