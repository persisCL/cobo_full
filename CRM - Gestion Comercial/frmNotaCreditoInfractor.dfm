object NotaCreditoInfractorForm: TNotaCreditoInfractorForm
  Left = 0
  Top = 0
  Caption = 'Nota de Cr'#233'dito a Nota de Cobro Infractor'
  ClientHeight = 578
  ClientWidth = 717
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 717
    Height = 201
    Align = alTop
    Caption = 'Datos del Comprobante:'
    TabOrder = 0
    DesignSize = (
      717
      201)
    object Label1: TLabel
      Left = 24
      Top = 24
      Width = 81
      Height = 13
      Caption = 'Tipo Documento:'
    end
    object Label2: TLabel
      Left = 207
      Top = 24
      Width = 41
      Height = 13
      Caption = 'N'#250'mero:'
    end
    object Label6: TLabel
      Left = 83
      Top = 154
      Width = 48
      Height = 13
      Anchors = []
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 75
      Top = 173
      Width = 56
      Height = 13
      Anchors = []
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 44
      Top = 86
      Width = 87
      Height = 13
      Anchors = []
      Caption = 'Fecha Emisi'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 18
      Top = 107
      Width = 113
      Height = 13
      Anchors = []
      Caption = 'Fecha Vencimiento:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 348
      Top = 86
      Width = 112
      Height = 13
      Anchors = []
      Caption = 'Total Comprobante:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 338
      Top = 107
      Width = 122
      Height = 13
      Anchors = []
      Caption = 'Estado Comprobante:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 100
      Top = 134
      Width = 31
      Height = 13
      Anchors = []
      Caption = 'RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 402
      Top = 134
      Width = 58
      Height = 13
      Anchors = []
      Caption = 'Convenio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 137
      Top = 154
      Width = 47
      Height = 13
      Anchors = []
      Caption = 'lblNombre'
    end
    object lblDomicilio: TLabel
      Left = 137
      Top = 173
      Width = 50
      Height = 13
      Anchors = []
      Caption = 'lblDomicilio'
    end
    object lblFechaEmision: TLabel
      Left = 137
      Top = 86
      Width = 74
      Height = 13
      Anchors = []
      Caption = 'lblFechaEmision'
    end
    object lblFechaVencimiento: TLabel
      Left = 137
      Top = 107
      Width = 96
      Height = 13
      Anchors = []
      Caption = 'lblFechaVencimiento'
    end
    object lblTotalAPagar: TLabel
      Left = 466
      Top = 86
      Width = 69
      Height = 13
      Anchors = []
      Caption = 'lblTotalAPagar'
    end
    object lblEstado: TLabel
      Left = 466
      Top = 107
      Width = 43
      Height = 13
      Anchors = []
      Caption = 'lblEstado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblRUT: TLabel
      Left = 137
      Top = 134
      Width = 30
      Height = 13
      Anchors = []
      Caption = 'lblRUT'
    end
    object lblNumeroConvenio: TLabel
      Left = 466
      Top = 134
      Width = 92
      Height = 13
      Anchors = []
      Caption = 'lblNumeroConvenio'
    end
    object vcbTipoDocumento: TVariantComboBox
      Left = 24
      Top = 40
      Width = 177
      Height = 21
      Style = vcsDropDownList
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 0
      OnChange = vcbTipoDocumentoChange
      Items = <>
    end
    object neNumeroDocumento: TNumericEdit
      Left = 207
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object btnBuscar: TButton
      Left = 344
      Top = 38
      Width = 75
      Height = 25
      Caption = 'Buscar'
      TabOrder = 2
      OnClick = btnBuscarClick
    end
    object btnSalir: TButton
      Left = 625
      Top = 16
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Salir'
      TabOrder = 3
      OnClick = btnSalirClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 201
    Width = 717
    Height = 128
    Align = alTop
    Caption = 'Conceptos Documento:'
    TabOrder = 1
    object DBListEx1: TDBListEx
      Left = 2
      Top = 15
      Width = 713
      Height = 111
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 250
          Header.Caption = 'Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripcionCargo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Monto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescImporte'
        end>
      DataSource = dsDetalle
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 526
    Width = 717
    Height = 52
    Align = alBottom
    TabOrder = 2
    object btnGenerar: TButton
      Left = 287
      Top = 16
      Width = 173
      Height = 25
      Caption = 'Generar Nota de Cr'#233'dito'
      TabOrder = 0
      OnClick = btnGenerarClick
    end
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 329
    Width = 717
    Height = 197
    Align = alClient
    Caption = 'Infracciones'
    TabOrder = 3
    object DBListEx2: TDBListEx
      Left = 2
      Top = 15
      Width = 713
      Height = 180
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Fecha'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaInfraccion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Estado Actual'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescEstado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 200
          Header.Caption = 'Motivo Anulaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescMotivoAnulacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Estado Anterior'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescEstadoAnterior'
        end>
      DataSource = dsInfracciones
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object spComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerEncabezadoComprobanteElectronicoAImprimir;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 456
    Top = 16
  end
  object spObtenerNumeroTipoInternoComprobanteFiscal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandTimeout = 360
    ProcedureName = 'ObtenerNumeroTipoInternoComprobanteFiscal'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 2
        Value = Null
      end>
    Left = 490
    Top = 16
    object StringField1: TStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object LargeintField1: TLargeintField
      FieldName = 'Importe'
    end
    object StringField2: TStringField
      FieldName = 'DescImporte'
      Size = 50
    end
    object WordField1: TWordField
      FieldName = 'CodigoConcepto'
    end
  end
  object spDetalle: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCargosFacturaAImprimir;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 528
    Top = 16
  end
  object dsDetalle: TDataSource
    DataSet = spDetalle
    Left = 528
    Top = 48
  end
  object spInfracciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerInfraccionesAnuladasComprobante;1'
    Parameters = <>
    Left = 560
    Top = 16
  end
  object dsInfracciones: TDataSource
    DataSet = spInfracciones
    Left = 560
    Top = 48
  end
  object spChequearUsuarioConcurrenteAcreditando: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'ChequearUsuarioConcurrenteAcreditando;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroDeSesion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@OtroUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaOtroUsuario'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 467
    Top = 251
  end
  object spCrearProcesoFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 420
    ProcedureName = 'CrearProcesoFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Operador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FacturacionManual'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 502
    Top = 251
  end
  object spAnularNotaCobro: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    LockType = ltBatchOptimistic
    CommandTimeout = 360
    ProcedureName = 'AnularComprobante;1'
    Parameters = <>
    Left = 535
    Top = 251
  end
  object spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'InsertarNumeroProcesoFacturacionEnColaEnvioDBNet'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 566
    Top = 251
    object StringField15: TStringField
      FieldName = 'NumeroConvenioFormateado'
      ReadOnly = True
      FixedChar = True
      Size = 19
    end
    object StringField16: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object IntegerField2: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object StringField17: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object LargeintField3: TLargeintField
      FieldName = 'NumeroComprobante'
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'PeriodoInicial'
      ReadOnly = True
    end
    object DateTimeField6: TDateTimeField
      FieldName = 'PeriodoFinal'
      ReadOnly = True
    end
    object DateTimeField7: TDateTimeField
      FieldName = 'FechaEmision'
      ReadOnly = True
    end
    object DateTimeField8: TDateTimeField
      FieldName = 'FechaVencimiento'
      ReadOnly = True
    end
    object StringField18: TStringField
      FieldName = 'NombreCliente'
      ReadOnly = True
      Size = 152
    end
    object StringField19: TStringField
      FieldName = 'Domicilio'
      ReadOnly = True
      Size = 162
    end
    object StringField20: TStringField
      FieldName = 'Comuna'
      ReadOnly = True
      Size = 203
    end
    object StringField21: TStringField
      FieldName = 'Comentarios'
      Size = 100
    end
    object StringField22: TStringField
      FieldName = 'CodigoPostal'
      ReadOnly = True
      Size = 10
    end
    object StringField23: TStringField
      FieldName = 'SaldoPendiente'
      ReadOnly = True
    end
    object StringField24: TStringField
      FieldName = 'AjusteSencilloAnterior'
      ReadOnly = True
    end
    object StringField25: TStringField
      FieldName = 'AjusteSencilloActual'
      ReadOnly = True
    end
    object StringField26: TStringField
      FieldName = 'TotalAPagar'
      ReadOnly = True
    end
    object WordField2: TWordField
      FieldName = 'CodigoTipoMedioPago'
    end
    object StringField27: TStringField
      FieldName = 'EstadoPago'
      FixedChar = True
      Size = 1
    end
    object LargeintField4: TLargeintField
      FieldName = 'NumeroComprobanteFiscal'
    end
    object StringField28: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
  end
  object spCargarTransitoEnTablaTemporal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'CargarTransitoEnTablaTemporal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IdSesion'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@NumCorrCA'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaProcesamiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 597
    Top = 251
  end
  object spAgregarMovimientoCuentaTemporal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    CommandTimeout = 360
    ProcedureName = 'AgregarMovimientoCuentaTemporal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IdSesion'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaProcesamiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@EsPago'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroFinanciamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Seleccionado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImporteIVA'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcentajeIVA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 627
    Top = 251
  end
  object spObtenerTransitosxInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    CommandTimeout = 360
    ProcedureName = 'ObtenerTransitosxInfraccion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 658
    Top = 252
  end
  object spReactivarInfraccionFacturar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    CommandTimeout = 300
    ProcedureName = 'ReactivarInfraccionFacturar;1'
    Parameters = <>
    Left = 432
    Top = 254
  end
end
