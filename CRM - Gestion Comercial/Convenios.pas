unit Convenios;

{********************************** File Header ********************************
File Name   : Convenios.pas
Author      : cchiappero
Date Created: 13/07/2004
Language    : ES-AR
Description : Clases para la mantenimiento de convenios.
*******************************************************************************}
{*******************************************************************************
                                TODOS

 - Repasar los raise para que la excepcion respete el Error Code original y no
   agrege mas mensajes.
 - Generar constantes de Errores.
 - Terminar los guardar.
 - Generar la manipulaci�n de items.

 - Por compatibilidad dejo los records pero habria que haber puesto los campos
   como datos miembros. Usar los records obliga a usar Tlist
   en vez de clietdatasets lo cual no es muy lindo y es mas laborioso.

 - Faltan algunas Jerarquias de clase como la de personas.
    personas ---Clientes      ------ Fisica
                              ------ Jur�dica.
             ---Mandantes
             ---Representante legal

    MediosPago -- PAC
               -- PAT
               -- Manual


*******************************************************************************
Revision : 1
Author	 : jconcheyro
Date     : 25/01/2005
Description : Metodo TMediosPagoConvenio.Create en la carga de PAT asum�a que
			Mandante.Telefonos siempre tenia MedioPrincipal.
			Se unific� el criterio con la carga de PAC que pregunta si tiene
			Medio Pricipal antes de utilizarlo.

Revision 2:
    Author : ggomez
    Date : 03/08/2006
    Description : Clase TCuentasConvenio:
        - Agregu� la property EstanTodasDadasBajaOSuspendidas.
        - M�todo Create: agregu� el seteo de la variable FEstanTodasDadasBajaOSuspendidas
        para indicar si todas las cuentas del convenio est�n dadas de baja o suspendidas.

Revision : 2
Author	 : jconcheyro
Date     : 24/08/2006
Description : Modificacion de emergencia para que no se generen Movimientos 21 o 23
            CONCEPTO_PAGO_SOPORTE CONCEPTO_PAGO_TELEVIA
            buscar rev2

Revision : 3
Author	 : jconcheyro
Date     : 24/08/2006
Description : Modificacion de emergencia para que no se generen Movimientos 21 o 23
            CONCEPTO_PAGO_SOPORTE CONCEPTO_PAGO_TELEVIA
            buscar rev2

Revision : 4
    Author : vpaszkowicz
    Date : 19/12/2006
    Description : Agrego el CodigoConvenioExterno a TDatosConvenio para generar
    los XMLs de modificacion usandolo como NumeroConvenio.

Revision : 5
    Author : FSandi
    Date : 15/01/2007
    Description : Se Agrego la funcion EsConvenioSuspendido a TDatosConvenio para
    determinar si un convenio esta o no suspendido, el mismo utiliza las nuevas funciones
    TodasLasCuentasDeBaja y AlMenosUnaCuentaActiva de la clase TCuentasConvenio


Revision : 6
    Author : jconcheyro
    Date : 26/01/2007
    Description : Indemnizaciones de telev�as modific� el TCuentasConvenio.Guardar
    en lo que se hac�a con la lista FVehiculosTagsACobrar. Antes era est�tico, ahora tiene
    selecci�n din�mica de cargos a facturar al convenio

Revision : 7
    Author : FSandi
    Date : 02/02/2007
    Description : Se corrigi� la funcion ObtenerRegVehiculosEliminados y se modifico el orden en que
                se llama a esta funci�n dentro de la baja del convenio.
Revision : 8
    Author : FSandi
    Date : 06/02/2007
    Description :   Se modific� la funci�n DigitoVerificadorValido para permitir el control del nuevo formato
                    de patentes.

Revision : 9
    Author : FSandi
    Date : 26/03/2007
    Description : Se creo la funci�n ConvenioTieneMotos dentro de la Clase TDatosConvenio para verificar al momento de
                  imprimir si se debe imprimir el contrato de motos.

Revision : 10
    Author : FSandi
    Date : 31-06-2007
    Description : Se permite reimprimir contratos de motos.

Revision : 11
    Author : FSandi
    Date : 01-06-2007
    Description : Se permite modificar la fecha de alta de una cuenta.
Revision : 12
    Author : vpaszkowicz
    Date : 22/08/2007
    Description : Agrego el par�metro FechaBajaCuenta del store BajaCuentaxCambioVehiculo
    que es Null por default en el store, esto es para evitar traslapes de fecha.
Revision : 15
Author :   lcanteros
Date Created: 14-03-2008
Language : ES-AR
Description : Se agrega la funcionalidad de Tipos de Clientes a la modificacion de Convenios.

Revision : 16
    Author : vpaszkowicz
    Date : 02/05/2008
    Description: Agrego una variable para representar la cuenta suspendida en
    lugar de hacerlo a trav�s de la fecha.

Revision : 17
    Author : lcanteros
    Date : 05/06/2008
    Description: Se pide no incluir en el mandato de CMR Falabella las cuentas
    que esten dadas de baja (SS 670).

Revision 18
Author: mbecerra
Date: 23-Dic-2009
Description:	(Ref SS 769) Se agrega una nueva funci�n a la clase TCuentasConvenio:
                ObtenerIDMotivoMovimientoCuentaTelevia, la cual obtiene el campo
                IDMotivoMovCuentaTelevia del objeto   PMovimientoCuenta.

                Adicionalmente, se agrega el campo IDMotivoMovCuentaTelevia, necesario
                para identificar si se ha seleccionado el concepto "Arriendo en Cuotas".

                Se sobrecarga la funci�n AgregarVehiculoTagACobrar con un par�metro adicional
                IDMotivoMovCuentaTelevia.

                Se modifica la impresi�n del Convenio para imprimir los nuevos anexos:
                Anexo III en caso de Tag nuevo,  Anexo IV en caso de documentos por e-mail

Revision 20:
	Date: 23/02/2009
	Author: mpiazza
	Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
		los bloqueos de tablas en la lectura

Revision :21
    Author : vpaszkowicz
    Date : 05/03/2009
    Description :Corrijo la fecha de vencimiento de la garant�a del Tag.
    Esto es porque en los cambios de tag se cambiaba esta fecha.
    En los cambios de tag no debe cambiarse, ya que estos se cambian siempre
    por garant�a.

Revision : 22
    Author: pdominguez
    Date: 18/03/2009
    Description: SS 733 - Se a�ade la opci�n Packed a los tipos TVehiculosConvenio,
        TMovimientoCuenta, TTagPerdido, para bajar el tama�o de los datos almacenados
        en memoria por estas estructuras.

Revision 23
Author: mbecerra
Date: 07-Julio-2009
Description: 	Se mueve la grabaci�n en TCuentasConvenio.Guardar de
				TagsVehiculosACobrar despu�s de la invocaci�n al stored
                ActualizarCuentaVehiculo  con el fin de obtener el IndiceVehiculo
                que este stored devuelve.

                Adicionalmente se deja de utilizar el IDMotivoMovCuentaTelevia para
                determinar si debe o no desplegar el mensaje de arriendo en cuotas.
                Ahora se utilizar� la misma variable (y el overloading de la funci�n
                AgregarVehiculoTagACobrar ) para almacenar el CodigoConcepto
                agregado.

                Se agrega una funci�n a la clase TDatosConvenio: TieneMedioEnvioPorEMail,
                la cual retorna verdadero si el convenio tiene medio de env�o
                de la nota de cobro por email.

                Adicionalmente, dado que los datos del convenio no son accesibles
                en el scope de la grabaci�n de las cuentas, se crea una variable
                local a este Form: ConvenioTieneMedioEnvioPorEMail, la cual
                es fijado su valor en True o False durante la grabaci�n del convenio.
                Se usa para determinar si debe colocar el concepto Arriendo
                en Cuotas por email o postal

Revision : 24
    Author : pdominguez
    Date   : 06/08/2009
    Description : SS 733
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            TDatosConvenio.Imprimir
            TDatosConvenio.ReimprimirAnexo3y4
            TDatosConvenio.ImprimirAnexo3y4VehNuevos

    Revision : 25
    Author : vpaszkowicz
    Date : 10/08/2009
    Description :
        -Modifico la funci�n TCuentasConvenio.Destroy
        -Modifiqu� la funci�n AgregarVehiculoTagACobrar
        -Cambi� la estructura de TMovimientoCuenta. Ahora su variable TVehiculosConvenio
        es un puntero a registro. De esta forma se refresca el indiceVehiculo cuando
        actualizo la lista original apuntada (ej. FVehiculos).
        -Borr� tambi�n de ese registro la variable IndiceEnFVehiculo, ya que ahora
        accedo por el puntero.
Revision 27
Author: mbecerra
Date: 28-Septiembre-2009
Description:	(Ref. SS 833)
                Se agrega el tipo de documento electr�nico a imprimir, tanto
                en la carga como grabaci�n del convenio.

Revision 28
Author: Nelson Droguett Sierra
Date: 29-Octubre-2009
Description:	(Ref. SS ???)
                Se agrega al convenio el BIT de "habilitado a AMB"


Revision : 29
    Author : pdominguez
    Date   : 04/12/2009
    Description : SS 836
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            TDatosConvenio.ReimprimirAnexo3y4
            TDatosConvenio.ImprimirAnexo3y4VehNuevos

Revision : 30
    Author : jjofre
    Date   : 12/04/2010
    Description : SS 468
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
          TDomiciliosPersona.Create

Revision 30:
    Author : mpiazza
    Date   : 19/10/2009
    Description : SS 787
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
          TDatosPersona.SetDatos
          TDatosPersona.Guardar
          TDatosPersona.Create

Revision : 31
    Author : jjofre
    Date   : 14/04/2010
    Description : SS 468
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
          TDatosConvenio.ObtenerCambios

Revision	: 5
Author		: Nelson Droguett
Date		: 27-Mayo-2010
Description	: SS 887 Se modifican la carga de las cuentas, minimizando los
            	accesos a la base de datos.
Revision	: 6
Author		: Nelson Droguett Sierra
Date		: 28-Julio-2010
Description	: (Ref.SS-901) Validar que el almacenDestino grabado en MovimientosCuentasTelevias,
            MaestroTags y Cuenta, sea concordante con el ConceptoMovimiento elegido
            en el alta del vehiculo, de acuerdo a la modalidad de entrega.
Revision	: 6
Author		: Jose Jofre
Date		: 31-Mayo-2010
Description	: SS 469
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
             class TDatosPersona
             TDatosPersona.Create
             TDatosPersona.Guardar
             TMediosComunicacionPersona.Guardar
             TDatosPersona.SetDatos
             TDatosPersona.SetInactivaDomicilioRNUT
             TDatosPersona.SetInactivaMedioComunicacionRNUT


Firma       : SS-960-ALA-20110713
Descripcion : Se agrega propiedad InicializarNotificacionesTAGs y TieneNotificacionesTAGs
              a la clase TDatosPersona para poder modificar las propiedades en la clase
              TDatosPersonales.

Firma		: SS_973_MBE_20110801
Description	: Se agrega el campo Departamento y DetalleDomicilio a los anexos 3 y 4

Firma       : SS-842-NDR-20110912
Description : Se agrega propiedad FechaObservacion a las Observaciones de Convenio

Firma       : SS_997_PDO_20111014
Description : Se corrige la asiganci�n de los par�metros del procedimiento
                ActualizarMedioComunicacionPersona.

Firma       : SS-1006-NDR-20111105
Description : Se implementa el bit HabilitadoPA para las listas de acceso de parque arauco
              Si se modifica el Bit del convenio, este afecta todas las cuentas
                que descienden de el. Si se cambia el Bit de una cuenta, solo
                afecta a la cuenta..

Firma       : SS_628_ALA_20111201
Description : Se agrega el bit SinCuenta para indicar si un convenio es o no Sin cuenta,
              agreg�ndolo como propiedad e incluy�ndolo en el store �ActualizarConvenio�.

Firma       : SS-1006-NDR-20120614
Descripcion : El Bit HabilitadoPA ahora se llama AdheridoPA
              Nueva operatoria de la Habilitacion de Convenio y Cuentas para EPA

Firma       : SS-1006-NDR-20120715
Descripcion : Si QUITA un vehiculo, le bajo el Bit AdheridoPA

Firma       : SS_1068_NDR_20120910
Description : Se agrega la propiedad FechaActualizacionMedioPago al convenio

Firma       : SS_1006_NDR_20121003
Descripcion : Dejar fuera de AdhesionPA las patentes liberadas.

Firma       : SS_1147_NDR_20140408
Descripcion : ConcesionariaNativa ya no es "1"

Firma       : SS_1147Q_NDR_20141202
Descripcion : Para VS el largo del numero de convenio ya no es 17 (es 12)

Firma       : SS_1147_MCA_20150420
Descripcion : para VS si el largo es menor a 12, se deja el numeroconvenio y no '',
            esto provocaba error en las impresiones de documentos

========================================================================================
      Las revisiones estan desordenadas
Author: ebaeza
date: 14/03/2014
Firma: SS_1171_EBA_20140311
description: SS_1171
      procedimientos / funciones
      ReimprimirAnexo3y4
      ImprimirAnexo3y4VehNuevos

        -.Se llenan los dos campos, "RazonSocial" y "RutRazonSocial" de la estructura que se utiliza
        para enviar los datos a imprimir los que seran utilizados en el nuevo Anexo III Juridico


Firma		: SS_1171_MCA_20140408
Descricion	: se corrige anexo III para persona juridica, no aparece la razon social
Firma       : SS_1380_MCA_20150911
Descripcion : se comenta las linea que hacen referencia a la impresion de la caratula

Firma       : SS_1380_NDR_20151009
Descripcion : Se repone la impresion de caratula

Firma       : SS_1419_NDR_20151130
Descripcion : El SP ActualizarDatosPersona tiene el nuevo parametro @CodigoUsuario
              para grabarlo en los campos de auditoria de la tabla Personas y en
              HistoricoPersonas

Firma       : CAC-CLI-066_MGO_20160218
Descripcion : Se agrega llamada a EMAIL_AgregarCorrespondencia cuando se da de
              baja un convenio

Etiqueta    : 20160603 MGO
Descripci�n : Se agrega propiedad EsForaneo a veh�culos

Etiqueta	: TASK_070_MGO_20160907
Descripci�n	: Se agrega alta forzada
*******************************************************************************}
interface

uses
  SysUtils, Classes, Types, DB, DBClient, ADODB, PeaTypes, PeaProcs, Variants,Util, UtilProc, UtilDB,
  CacProcs, ConstParametrosGenerales, Windows, Dialogs, RStrings, DateUtils, StrUtils, frmDMGuardarConvenios,
  SysUtilsCN;


Const
    ERRORNOEXISTECONVENIO = 1;
    ERRORGRABACIONMEDIOPAGO = 2;
    ERRORCREACIONMEDIOPAGO = 3;
    ERRORNOSEPUEDEBORRARDOMICILIOPRINCIPAL = 4;
    ERRORNOTIENEMEDIOPRINCIPAL = 5;
    ERRORNOTIENEMEDIOSECUNDARIO = 6;
    ERRORMEDIOPAGOERRONEO = 7;
    ERRORFORMATODEMEDIOCOMUNICACION = 8;
    ERRORMEDIOPAGOTIENEMANDANTE = 9;
    ERRORRUTEXISTE = 10;
    ERRORVEHICULOYAASIGNADO = 11;
    ERRORCREARCLASEDOCUMENTACIONGUARDADA = 12;
    ERRORVEHICULOQUITADO = 13;
    ERRORCUENTASUSPENDIDA = 14;
type

  TTipoImpresion = (tiAlta, tiModificacion, tiPerdida, tiEstadoActual);
  TTipoImpresionMotos = (tiNuevas, tiReimpresion);

  EExtendedError  = class(Exception)
  private
    FDetailMessage : string;
    FErrorCode: Integer;
  public
    constructor Create(const Msg, DetailMsg: string; ErrCode : integer);
    property DetailMessage: string read FDetailMessage write FDetailMessage;
    property ErrorCode: integer read FErrorCode write FErrorCode;
  end;

  EConvenioError = class(EExtendedError);
  EPersonaError = class(EExtendedError);
  EDomicilioError = class(EExtendedError);
  EMedioComunicacionError = class(EExtendedError);
  EMedioPagoError = class(EExtendedError);
  ECuentaError = class(EExtendedError);

  TFormatosMediosComunicacion = (TMC_TELEFONO, TMC_EMAIL, TMC_DIRECCION);

  PDatosPersonales = ^TDatosPersonales;

  PMedioComunicacion = ^TTipoMedioComunicacion;

  PDomicilio = ^TDatosDomicilio;

  PRegistroMedioEnvioDocumentosCobro = ^TRegistroMedioEnvioDocumentosCobro;

  PRegistroDocumentacion = ^TRegistroDocumentacion;
  POrdenServicio = ^TOrdenServicio;

// SS 733


  TVehiculosConvenio = packed record
    Cuenta: TCuentaVehiculo;
    Documentacion: TList;
    NumeroMovimientoCuenta: longint;
    EsVehiculoNuevo: Boolean;
    EsTagNuevo: Boolean;
    EsVehiculoModificado: Boolean;
    EsTagVendido: Boolean;
    EsCambioVehiculo: Boolean;
    EsActivacionCuenta: Boolean;
    EsTagPerdido: Boolean;
    EsTagRecuperado: Boolean;
    EsCambioTag: Boolean;
    EsVehiculoRobado: Boolean;
    EsVehiculoRecuperado: Boolean;
    EsCambioEstado: Boolean;   //Refleja si paso en pantalla de alta a suspendida.
    //Rev.1
    //HabilitadoAMB : Boolean;                     //SS-1006-NDR-20111105
    AdheridoPA : Boolean;
    EsForaneo: Boolean; // 20160603 MGO
                             //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
  end;

  PVehiculosConvenio = ^TVehiculosConvenio;

// SS 733
  TMovimientoCuenta = packed record
  //Le cambio el tipo para poder acceder a los cambios en el veh�culo.
    //VehiculosConvenio: TVehiculosConvenio;
    VehiculosConvenio: PVehiculosConvenio;
    CobrarTag: Boolean;
    EntregoSoporte: Boolean;
    OtroConcepto: Integer;
    Importe: Int64;
    DescripcionMotivo: string;
    AlmacenDestino: integer;
    NumeroVoucher: integer;
    TipoAsignacionTag: string;
    IDMotivoMovCuentaTelevia : integer;
  end;

  PMovimientoCuenta = ^TMovimientoCuenta;

// SS 733
  TTagPerdido = packed record
    CodigoConvenio: Integer;
    IndiceVehiculo: Integer;
    FechaLimiteEnvioDenuncia: TDateTime;
    FechaLimiteCambioTelevia: TDateTime;
    CambioTelevia: Boolean;
  end;

  // INICIO : TASK_070_MGO_20160907
  TBajaForzada = packed record
    CodigoBajaForzada: Integer;
    Deshacer: Boolean;
    Patente: string;    
    ConvenioNativo: string;
    TagNativo: string;      
    RUTNativo: string;
    ConvenioForaneo: string;   
    TagForaneo: string;
    RUTForaneo: string;
    NombreForaneo: string;
    FechaHora: TDateTime;
    CodigoUsuario: string;
  end;

  PBajaForzada = ^TBajaForzada;

  TBajasForzadasConvenio = Class
   private
      FCodigoConvenio: Integer;
      FBajasForzadas: TList;
      function ObtenerBajaForzada(Index: Integer): TBajaForzada;
      function ObtenerCantidadBajasForzadas: Integer;
   public
      property CodigoConvenio: Integer read FCodigoConvenio;
      property BajasForzadas[Index: Integer]: TBajaForzada read ObtenerBajaForzada;
      property CantidadBajasForzadas: Integer read ObtenerCantidadBajasForzadas;

      constructor Create(CodigoConvenio: Integer); overload;
      destructor  Destroy; override;

      procedure MarcarBajaForzadaDeshacer(CodigoBajaForzada: Integer; Deshacer: Boolean);  
      procedure AgregarVehiculo(IndiceVehiculo: Integer);
      procedure Guardar;
      function BuscarIndice(CodigoBajaForzada: Integer): Integer;
      function TieneBajaForzada(Patente: String): Boolean;
  end;
  // FIN : TASK_070_MGO_20160907

  TMediosEnvioDocumentosConvenio = Class
   private
      FCodigoConvenio : integer;
      FMediosEnvio : TList;
      FMediosEnvioQuitados : TList;
      function ObtenerMediosEnvioDocumentoConvenio(Index : integer) : TRegistroMedioEnvioDocumentosCobro;
      function ObtenerCantidadMediosEnvio : integer;
      function GetEnviaDocumentacionxMail: Boolean;
      function GetTipoEnvoMail: Integer;
   public
      property CodigoConvenio : integer read FCodigoConvenio;
      property MediosEnvio[Index : integer] : TRegistroMedioEnvioDocumentosCobro read ObtenerMediosEnvioDocumentoConvenio;
      property CantidadMediosEnvio  : integer read ObtenerCantidadMediosEnvio;
      property EnviaDocumentacionxMail : Boolean read GetEnviaDocumentacionxMail;
      property TipoEnvioMail : Integer read GetTipoEnvoMail;

      constructor Create(CodigoConvenio : integer); overload;
      destructor  Destroy; override;

      procedure   AgregarMedioEnvioDocumentosConvenio(const AMedioEnvio : TRegistroMedioEnvioDocumentosCobro);
      procedure   QuitarMedioEnvioDocumentosConvenio(Index : integer);
      procedure   Guardar;
      function  BuscarIndice(CodigoMedioEnvioDocumento, CodigoTipoMedioEnvio:Integer): integer;

  end;

  TCheckListConvenio = Class
    private
        FACheckListConvenio: TACheckListConvenio;
        FCodigoConvenio: Integer;
        function GetListConvenio(Index: integer): TRCheckListConvenio;
        function SetCantidad: integer;
        function GetTieneMarcados: boolean;
        procedure SetListConvenio(Index: integer;
          const Value: TRCheckListConvenio);
    public
        constructor Create(CodigoConvenio: Integer); overload;
        procedure Marcar(CodigoCheckList: integer);
        procedure DesMarcar(CodigoCheckList: integer);
        property Items[Index: integer]: TRCheckListConvenio read GetListConvenio write SetListConvenio;
        property Cantidad: integer read SetCantidad;
        property TieneMarcados: boolean read GetTieneMarcados;
        procedure Guardar(CodigoConvenio: Integer = -1);
  end;

  TObservacionConvenio = Class
    private
        FIndiceObservacion: integer;
        FObservacion: string;
        FActivo: boolean;
        FFechaObservacion: TDateTime;                                           //SS-842-NDR-20110912
        FModificada: Boolean;                                                   //SS_842_PDO_20111212
        function SetIndiceObservacion: integer;
        procedure GetIndiceObservacion(const Value: integer);
        function SetObservacion: String;
        procedure GetObservacion(const Value: string);
        procedure GetActivo(const Value: boolean);
        function SetActivo: boolean;
        procedure GetFechaObservacion(const Value: TDateTime);                  //SS-842-NDR-20110912
        function SetFechaObservacion: TDateTime;                                //SS-842-NDR-20110912
        procedure SetObservacionModificada(const Value: Boolean);               // SS_842_PDO_20111214
        function GetObservacionModificada: Boolean;                             //SS_842_PDO_20111212

    public
        property IndiceObservacion:integer read SetIndiceObservacion write GetIndiceObservacion;
        property Observacion:string read SetObservacion write GetObservacion;
        property Activo: boolean read SetActivo write GetActivo;
        property FechaObservacion: TDateTime read SetFechaObservacion write GetFechaObservacion;	//SS-842-NDR-20110912
        property Modificada: Boolean read GetObservacionModificada write SetObservacionModificada;  // SS_842_PDO_20111214
        constructor Create;
  end;

  TListObservacionConvenio = Class
    private
        FItems: TList;
        FCodigoConvenio: Integer;
        function SetObservacion(index: integer): TObservacionConvenio;
        function Setcount: integer;
        function GetTieneObservaciones: Boolean;
    public
        property Items[index:integer]: TObservacionConvenio read SetObservacion;
        property Count: integer read Setcount;
        property TieneObservaciones: Boolean read GetTieneObservaciones;
        function Add(const ObservacionConvenio: TObservacionConvenio): integer;
        procedure Clear;
        procedure Guardar(CodigoConvenio: Integer = -1);
        constructor Create(CodigoConvenio: Integer); overload;
        destructor Destroy; override;
  end;


  TMediosComunicacionPersona = Class
    private
         FCodigoPersona : integer;
         FTipoMedioComunicacion : TFormatosMediosComunicacion;
         FMediosComunicacion : TList;
         FMediosComunicacionQuitados : TList;
         function ObtenerMediosComunicacionPrincipal: TTipoMedioComunicacion;
         function ObtenerMedioComunicacion(Index : integer) : TTipoMedioComunicacion;
         function ObtenerCantidadMediosComunicacion : integer;
         function ObtenerIndiceMedioPrincipal : Integer;
         function ObtenerMediosComunicacionSecundario: TTipoMedioComunicacion;
         procedure SetMediosComunicacion(Index: Integer;const Value: TTipoMedioComunicacion);
         function GetIndiceMedioPrincipal: Integer;
         function GetTieneMedioPrincipal: Boolean;
         function GetTieneMedioSecundario: Boolean;
         procedure SetCodigoPersona(const Value: integer);
         function GetIndiceMedioSecundario: Integer;
    public
         property CodigoPersona : integer read FCodigoPersona write setCodigoPersona;
         property TipoMedioComunicacion : TFormatosMediosComunicacion read FTipoMedioComunicacion;
         property MedioPrincipal : TTipoMedioComunicacion read ObtenerMediosComunicacionPrincipal;
         property MedioSecundario: TTipoMedioComunicacion read ObtenerMediosComunicacionSecundario;
         property MediosComunicacion[Index : Integer] : TTipoMedioComunicacion read ObtenerMedioComunicacion write SetMediosComunicacion;
         property CantidadMediosComunicacion : integer read ObtenerCantidadMediosComunicacion;
         property TieneMedioPrincipal : Boolean read GetTieneMedioPrincipal;
         property TieneMedioSecundario: Boolean read GetTieneMedioSecundario;
         property IndiceMedioPrincipal:Integer read GetIndiceMedioPrincipal;
         property IndiceMedioSecundario:Integer read GetIndiceMedioSecundario;

         constructor Create(CodigoPersona : integer; FormatoMedioComunicacion: TFormatosMediosComunicacion); overload;
         constructor Create(FormatoMedioComunicacion: TFormatosMediosComunicacion); overload;
         destructor  Destroy; override;
         procedure   AgregarMedioComunicacion(AMedioComunicacion : TTipoMedioComunicacion);
         procedure   QuitarMedioComunicacion(Index : integer);
         procedure   Guardar;
         //Lo agrego porque sino, cuando lo tengo que borrar de una lista me termina borrando el ppal.
         function RetornarIndiceTelefonoSecundario: Integer;
         function ExisteMailABorrar: Boolean;
  end;



  TDomiciliosPersona = Class
    private
         FCodigoPersona : integer;
         FDomicilioPrincipal   : PDomicilio;
         FDomicilios : TList;
         FDomiciliosQuitados : TList;
         FOnChangeDomicilioFacturacion: TNotifyEvent;
         function    ObtenerDomicilioPrincipal: TDatosDomicilio;
         function    ObtenerDomicilioFacturacion: TDatosDomicilio;
         function    ObtenerDomicilioPersonas(Index : Integer) : TDatosDomicilio;
         function    ObtenerCantidadDomiciliosPersonas : integer;
         function    VerificarTieneDomicilioPrincipal : boolean;
         function    VerificarTieneDomicilioFacturacion : boolean;
         procedure SetOnChangeDomicilioFacturacion(const Value: TNotifyEvent);
         function  GetIndiceDomicilioFacturacion: integer;
         function GetIndiceDomicilioPrincipal: integer;
    public
         property CodigoPersona : integer read FCodigoPersona;
         property DomicilioPrincipal   : TDatosDomicilio read ObtenerDomicilioPrincipal;
         property DomicilioFacturacion : TDatosDomicilio read ObtenerDomicilioFacturacion;
         property Domicilios[Index:Integer] : TDatosDomicilio read ObtenerDomicilioPersonas;
         property CantidadDomiciliosPersonas : integer read ObtenerCantidadDomiciliosPersonas;
         property TieneDomicilioPrincipal : boolean read VerificarTieneDomicilioPrincipal;
         property TieneDomicilioFacturacion : boolean read VerificarTieneDomicilioFacturacion;
         property IndiceDomicilioFacturacion: integer read GetIndiceDomicilioFacturacion;
         property IndiceDomicilioPrincipal: integer read GetIndiceDomicilioPrincipal;

         //evento
         property OnChangeDomicilioFacturacion: TNotifyEvent read FOnChangeDomicilioFacturacion write SetOnChangeDomicilioFacturacion;

         //Crea y carga los domicilios de una persona
         constructor Create(CodigoPersona : integer);
         destructor  Destroy; override;

         procedure   AgregarDomicilio(const ADomicilio : TDatosDomicilio);
         procedure   EditarDomicilio(Index: Integer; const ADomicilio: TDatosDomicilio);
         procedure   QuitarDomicilio(Index : integer);
         procedure   SetearDomicilioFacturacion(CodigoDomicilioFacturacion : integer);
         procedure   Guardar;
         function ObtenerIndiceDomicilio(CodigoDomicilio: Integer):Integer;

  end;


  TDatosPersona = class
  private
     FDatos : TDatosPersonales;
     FEsNuevo : Boolean;
     FEsModificado : Boolean;
     FModificoDatosPrincipales: Boolean;
     procedure SetCodigoPersona(const Value: integer); virtual;
     procedure SetDatos(const Value: TDatosPersonales);
     function GetEsModificado: Boolean;
     function GetEsNuevo: Boolean;
     procedure SetRecibeClavePorMail(Const Value : boolean);
     procedure SetInactivaDomicilioRNUT(const Value: Boolean);
     procedure setInactivaMedioComunicacionRNUT(const Value:Boolean);
     procedure SetInicializarNotificacionesTAGs(const Value:Boolean);           //SS-960-ALA-20110713
     procedure SetTieneNotificacionesTAGs(const Value:Boolean);                 //SS-960-ALA-20110713
  protected
     FCodigoPersona : integer;
  public
     property CodigoPersona : integer read FCodigoPersona write SetCodigoPersona;
     property Datos : TDatosPersonales read FDatos write SetDatos;
     property EsNuevo : Boolean read GetEsNuevo;
     property EsModificado : Boolean read GetEsModificado;
     property ModificoDatosPrincipales: Boolean read FModificoDatosPrincipales;
     property RecibeClavePorMail: Boolean read FDatos.RecibeClavePorMail write SetRecibeClavePorMail;
     property InactivaDomicilioRNUT :  Boolean read FDatos.InactivaDomicilioRNUT write SetInactivaDomicilioRNUT ;
     property InactivaMedioComunicacionRNUT :  Boolean read FDatos.InactivaMedioComunicacionRNUT write SetInactivaMedioComunicacionRNUT ;
     property InicializarNotificacionesTAGs : Boolean read FDatos.InicializarNotificacionesTAGs write SetInicializarNotificacionesTAGs;     //SS-960-ALA-20110713
     property TieneNotificacionesTAGs : Boolean read FDatos.TieneNotificacionesTAGs write SetTieneNotificacionesTAGs;                       //SS-960-ALA-20110713
     constructor Create(CodigoPersona : integer); overload; virtual;
     constructor Create; overload;
     destructor Destroy; override;

     function ObtenerDatosPersonales  : TDatosPersonales;
     class function ObtenerDatosPersona(CodigoDocumento, NumeroDocumento : string; var DatosPersonas : TDatosPersonales) : boolean;
     procedure Guardar;
     procedure SetGiro (Giro : string);

  end;

  TRepresentanteLegal = Class(TDatosPersona)
    private
    FNroRepresentante: Integer;
    procedure SetNroRepresentante(const Value: Integer);
    public
        property NroRepresentante: Integer read FNroRepresentante write SetNroRepresentante;
        constructor Create(CodigoPersona : integer; NroRepresentate: Integer); overload;
  end;

  TRepresentantesLegales = class
  private
      FTitularConvenio : Integer;
      FCodigoConvenio : integer;
      FRepresentantes : TList;
      FRepresentantesQuitados : TList;
      function ObtenerRepresentante(Index : integer) : TRepresentanteLegal;
      function ObtenerCantidadRepresentantes : integer;
      function GetCantidadRepresentantesQuitados: integer;
      function GetRepresentantesQuitados(Index: integer): TRepresentanteLegal;
      function VetrificarIntegeridadNroRepresentate(NroRepresentante: Integer; IndiceActual: Integer = -1): Boolean;
  public
      property CodigoConvenio : integer read FCodigoConvenio;
      property Representantes[Index : integer] : TRepresentanteLegal read ObtenerRepresentante;
      property CantidadRepresentantes : integer read ObtenerCantidadRepresentantes;
      property CantidadRepresentantesQuitados: integer read GetCantidadRepresentantesQuitados;
      property RepresentantesQuitados[Index : integer] : TRepresentanteLegal read GetRepresentantesQuitados;
      constructor Create(CodigoConvenio : integer; CodigoTitularConvenio: Integer);
      destructor Destroy; override;
      procedure Guardar;
      procedure QuitarRepresentante(Index : integer); overload;
      procedure QuitarRepresentante(TipoDocumento, NumeroDocumento : string); overload;
      procedure QuitarRepresentanteDeLista(Index : integer); overload;
      procedure QuitarRepresentanteDeLista(TipoDocumento, NumeroDocumento : string); overload;
      procedure AgregarRepresentante(DatosPersonales : TDatosPersonales; NroRepresentante: Integer = -1);
      procedure ModificarRepresentante(Index : integer; DatosPersonales : TDatosPersonales; NroRepresentante: Integer = -1); overload;
      procedure ModificarRepresentante(TipoDocumento, NumeroDocumento : string; DatosPersonales : TDatosPersonales; NroRepresentante: Integer = -1); overload;
      function ObtenerIndice(TipoDocumento, NumeroDocumento : string): Integer;
  end;



  TPersonaConvenio = Class(TDatosPersona)
   private
     FDomicilios : TDomiciliosPersona;
     FTelefonos : TMediosComunicacionPersona;
     FEMail : TMediosComunicacionPersona;
   public
     property Domicilios : TDomiciliosPersona read FDomicilios;
     property Telefonos : TMediosComunicacionPersona read FTelefonos;
     property EMail : TMediosComunicacionPersona read FEmail;

     constructor Create(CodigoPersona : integer); override;
     destructor Destroy; override;
     function ExisteMailABorrar: Boolean;
     procedure Guardar; virtual;
  end;

  TPersonaFisica = Class(TPersonaConvenio);

  TPersonaJuridica = Class(TPersonaConvenio)
   private
      FRepresentantesLegales : TRepresentantesLegales;
      //FEsConvenioCN: Boolean;													//SS_1147_MCA_20140408
      FEsConvenioNativo: Boolean;												//SS_1147_MCA_20140408
   public
      //property EsConvenioCN : Boolean read FEsConvenioCN;						//SS_1147_MCA_20140408
      property EsConvenioNativo: Boolean read FEsConvenioNativo;				//SS_1147_MCA_20140408
      property RepresentantesLegales : TRepresentantesLegales read FRepresentantesLegales;
      procedure Guardar; override;
      //constructor Create(CodigoPersona, CodigoConvenio : integer; EsConvenioCN : Boolean);	//SS_1147_MCA_20140408
      constructor Create(CodigoPersona, CodigoConvenio : integer; EsConvenioNativo : Boolean);	//SS_1147_MCA_20140408
      destructor Destroy; override;
  end;

  TMandante = Class(TDatosPersona)
   private
    FTelefonos : TMediosComunicacionPersona;
    procedure SetCodigoPersona(const Value: integer); override;
   public
    property CodigoPersona : integer read FCodigoPersona write SetCodigoPersona;
    property Telefonos : TMediosComunicacionPersona read FTelefonos;
    constructor Create(CodigoPersona : integer); overload; override;
    constructor Create; reintroduce; overload;
    destructor Destroy; override;
    procedure Guardar; virtual;
  end;


  TMediosPagoConvenio = Class
   private
    FCambioMedioPago: Boolean;
    FCambioDatosTransBank: Boolean;
    FCodigoConvenio : Integer;
    FCodigoMedioPago: Integer;
    FMedioPago : TDatosMediosPago;
    FTipoMedioPago : TTiposMediosPago;
    FMandante : TMandante;
    FOnCambioMedioPago: TNotifyEvent;
    FConfirmado: Boolean;
    FForzarConfirmado: Boolean;
    FOrigenMandato: String;
//    FCambioTipoMedioPago: Boolean;
    procedure SetMedioPago(const Value: TDatosMediosPago);
    procedure SetTipoMedioPago(Value : TTiposMediosPago);
    procedure SetOnCambioMedioPago(const Value: TNotifyEvent);
    function GetTieneMandante: Boolean;
    function GetConfirmado: Boolean;
    function GetForzarConfirmado: Boolean;
    procedure SetForzarConfirmado(const Value: Boolean);

   public
    property CodigoConvenio : Integer read FCodigoconvenio;
    property CodigoMedioPago:Integer read FCodigoMedioPago;
    property MedioPago : TDatosMediosPago read FMedioPago write SetMedioPago;
    property Mandante : TMandante read FMandante;
    property TipoMedioPago : TTiposMediosPago read FTipoMedioPago write SetTipoMedioPago;
    property TieneMandante: Boolean read GetTieneMandante;
    property Confirmado: Boolean read GetConfirmado;
    property ForzarConfirmado: Boolean read GetForzarConfirmado write SetForzarConfirmado;
    property OrigenMandato: String read FOrigenMandato;

    // Evento
    Property OnCambioMedioPago:TNotifyEvent read FOnCambioMedioPago write SetOnCambioMedioPago;

    constructor Create(CodigoConvenio: Integer;
                       CodigoPersonaMedioPago: integer;
                       const  TipoMedioPago: Integer);
    destructor Destroy; override;
    function ObtenerMedioPago : TDatosMediosPago;
    function ObtenerMandante : TMandante;
    function EsCambioMedioPago: Boolean;
    function EsCambioDatosTransBank: Boolean;
    function EsCambioTipoMedioPago: Boolean;

    procedure AgregarMandate;
    procedure Guardar;
  end;

  TRegistrosPersonas =  array of TDatosPersona;
  PDatosPersona = ^TDatosPersona;


  TDocumentacion = class
  private
    FNoCorresponde: Boolean;
    FCodigoDocumentacionRespaldo: Integer;
    FCodigoDocumentacionPresentada: Integer;
    FCodigoConvenio: Integer;
    FFechaBaja: TDatetime;
    FPatente: String;
    FTipoPatente: String;
    FMarcado: Boolean;
    FOrden: integer;
    FSoloImpresion: Integer;
    FExplicacionDefault: String;

    // Son info del ipo de modificacion
    FExplicacion: String;
    FObligatorio: Variant;
    FCodigoTipoOperacion: Integer;
    FEsModificado: Boolean;
    FDescripcion: String;
    FInformacionExtra: String;
    procedure SetCodigoDocumentacionRespaldo(const Value: Integer);
    procedure SetNoCorresponde(const Value: Boolean);
    procedure SetCodigoConvenio(const Value: Integer);
    procedure SetCodigoDocumentacionPresentada(const Value: Integer);
    procedure SetFechaBaja(Value: TDatetime);
    procedure SetPatente(const Value: String);
    procedure SetTipoPatente(const Value: String);
    function GetFechaBajaGuardar: Variant;
    procedure SetMarcado(const Value: Boolean);
    function GetEsNuevo: Boolean;
    function GetExplicacion: String;
    function GetObligatorio: Boolean;
    function GetOrden: integer;
    function GetSoloImpresion: Integer;
    function VerificarInfo: Boolean;
    procedure SetCodigoTipoOperacion(const Value: Integer);
    procedure SetDescripcion(const Value: String);
    function GetMostrarImprimir: Boolean;
    function GetSoloImprimir: Boolean;
    function GetSoloMostrar: Boolean;
    procedure SetInformacionExtra(const Value: String);
  public
    constructor Create(CodigoConvenio, CodigoDocumentacionRespaldo: Integer;
            NoCorresponde: Boolean; Orden: integer; SoloImpresion: Integer;
            Descripcion: String;
            ExplicacionDefault: String = ''; TipoPatente: String = ''; Patente: String = '';
            CodigoDocumentacionPresentada: Integer = -1; FechaBaja: TDatetime = Nulldate); overload;

    constructor Create(CodigoConvenio, CodigoDocumentacionRespaldo: Integer;
            NoCorresponde: Boolean; Orden: integer; SoloImpresion: Integer;
            Descripcion: String;
            Explicacion: String; Obligatorio: Boolean; CodigoTipoOperacion: Integer;
            InformacionExtra: String = ''; ExplicacionDefault: String = ''; TipoPatente: String = ''; Patente: String = '';
            CodigoDocumentacionPresentada: Integer = -1); overload;

    destructor Destroy; override;

    property CodigoDocumentacionRespaldo: Integer read FCodigoDocumentacionRespaldo write SetCodigoDocumentacionRespaldo;
    property NoCorresponde: Boolean read FNoCorresponde write SetNoCorresponde;
    property CodigoConvenio: Integer read FCodigoConvenio write SetCodigoConvenio;
    property CodigoDocumentacionPresentada: Integer read FCodigoDocumentacionPresentada write SetCodigoDocumentacionPresentada;
    property FechaBaja: TDatetime read FFechaBaja write SetFechaBaja;
    property FechaBajaGuardar: Variant read GetFechaBajaGuardar;
    property TipoPatente: String read FTipoPatente write SetTipoPatente;
    property Patente: String read FPatente write SetPatente;
    property Marcado: Boolean read FMarcado write SetMarcado;
    property EsNuevo: Boolean read GetEsNuevo;
    property Explicacion: String read GetExplicacion;
    property Obligatorio: Boolean read GetObligatorio;
    property Orden: integer read GetOrden;
    property SoloImpresion: Integer read GetSoloImpresion ;
    property CodigoTipoOperacion: Integer read FCodigoTipoOperacion write SetCodigoTipoOperacion;
    property EsModificado: Boolean read FEsModificado;
    property Descripcion: String read FDescripcion write SetDescripcion;
    property SoloMostrar: Boolean read GetSoloMostrar;
    property SoloImprimir: Boolean read GetSoloImprimir;
    property MostrarImprimir: Boolean read GetMostrarImprimir;
    property InformacionExtra: String read FInformacionExtra write SetInformacionExtra;
  end;


  TDocumentacionConvenio = class
  private
    FDocumentacion : Array of TDocumentacion;
    FDocumentacionBack : Array of TDocumentacion;
    FOperacionConvenio : Array of integer;
    FCodigoConvenio: Integer;
    procedure SetCodigoConvenio(const Value: Integer);
    procedure AgregarDocumentacion(CodigoConvenio,
                  CodigoDocumentacionRespaldo: Integer; NoCorresponde: Boolean; Orden: integer;
                  SoloImpresion: Integer;  Descripcion: String; ExplicacionDefault: String = ''; TipoPatente: String = '';
                  Patente: String = ''; CodigoDocumentacionPresentada: Integer = -1; FechaBaja: TDatetime = Nulldate);

    procedure AgregarDocumentacionxOperacion(
                  CodigoDocumentacionRespaldo: Integer; NoCorresponde: Boolean; Orden: integer;
                  SoloImpresion: Integer;  Obligatorio:Boolean; CodigoTipoOperacionConvenio: Integer;
                  Descripcion: String; Explicacion: String;  InformacionExtra: String = '';
                  ExplicacionDefault: String = '';
                  TipoPatente: String = ''; Patente: String = '';
                  ForzarNoVisible: Boolean = False);

    procedure CancelarDoc(CodigoDocumentacionRespaldo: Integer; InfoExtra: String; TipoPatente: String = '';
                  Patente: String = ''; CodigoDocumentacionPresentada: Integer = -1);

    function EstaDocumentacion(CodigoDocumentacionRespaldo: Integer; InfoExtra: String; TipoPatente: String = '';
                  Patente: String = ''): Boolean;
    function EstaDocumentacionComoNoObligatoria(
  CodigoDocumentacionRespaldo:Integer): Boolean;
    function GetHayDocumentacionSinControlar: Boolean;
    function GetCantidadDocumentacion: Integer;

    function GetOperacionConvenio(Index: integer): integer;
    function GetCantidadOperacionesConvenio: integer;

    procedure AgregarOperacionConvenio(CodigoTipoOperacionConvenio: integer);
    function GetHayDocumentacionAImprimir: Boolean;
    procedure CompactarRegistro;
    procedure EliminarRegistro(Indice: Integer);
    procedure Compactar;
    procedure VaciarListaBack;
  public
    constructor Create(CodigoConvenio: integer);
    destructor Destroy; override;

    procedure AgregarDocumentacionxTipoOperacion(CodigoTipoOperacion: Integer;
            InformacionExtra: String = ''; TipoPatente: String = ''; Patente: String = '';
            ForzarNoVisible: Boolean = False; CodigoDocumentacionPresentada: Integer = -1);
    procedure EliminarDocumentacionxTipoOperacion(CodigoTipoOperacion: Integer;
            InfoExtra: String = ''; TipoPatente: String = ''; Patente: String = ''; CodigoDocumentacionPresentada: Integer = -1);
    procedure Guardar;
    function ObtenerListaDocumentacionGuardada(var Documentacion : RegDocumentacionPresentada): Boolean;
    function ObtenerListaDocumentacion(var Documentacion : RegDocumentacionPresentada): Boolean;
    procedure VaciarOperacionConvenio;
    function DarDocumentacion(Indice: Integer): TDocumentacion;
    procedure IniciarObtenerCambio;
    procedure FinObtenerCambio;
    procedure ActualizarEstadoDoc(Patente: ShortString);
    procedure ClonarOpcionales;
    procedure ConvertirDocumentacionConvenioSinCuentaAlta(Personeria: string);
    property OperacionConvenio[Index: integer]: integer read GetOperacionConvenio;
    property CantidadOperacionesConvenio: integer read GetCantidadOperacionesConvenio;
    property CodigoConvenio: Integer read FCodigoConvenio write SetCodigoConvenio;
    property HayDocumentacionSinControlar: Boolean read GetHayDocumentacionSinControlar;
    property HayDocumentacionAImprimir: Boolean read GetHayDocumentacionAImprimir;
    property CantidadDocumentacion: Integer read GetCantidadDocumentacion;
  end;

  TChekListCuenta = Class
    private
        FCodigoConvenio: Integer;
        FCheckListCuenta: TACheckListCuenta;
        function SetCantidad: integer;
        function GetListCuenta(Index: integer): TRCheckListCuenta;
        procedure SetListCuenta(Index: integer;
            const Value: TRCheckListCuenta);
        function PuedeOperadorCambiarClick(CodigoCheckList: Integer): Boolean;
    public
      constructor Create(CodigoConvenio: Integer);
      procedure Marcar(Indice: Integer); overload;
      procedure Marcar(CodigoCheckList: integer; IndiceVehiculo: Integer); overload;
      procedure Agregar(CodigoCheckList: integer; Vehiculo: TVehiculosConvenio; Vencimiento: TDateTime);
      property Cantidad: integer read SetCantidad;
      property Items[Index: integer]: TRCheckListCuenta read GetListCuenta write SetListCuenta;
      function TieneItems: Boolean;
      function PuedeCambiarElimnarCuenta(IndiceVehiculo: Integer): Boolean;
      function EntregoDenuncia(IndiceVehiculo: Integer): Boolean;
      function PagoTeleviaPerdido(IndiceVehiculo: Integer): Boolean;
      function ArmarDescripcion(CodigoCheckList: Integer; Patente: String; FechaVencimiento: TDateTime): String;
      procedure ElimnarItems(IndiceVehiculo: Integer);
      procedure ActualizarVencimiento(IndiceVehiculo: Integer; FechaVencimiento: TDateTime);
      procedure Guardar;
  end;

  TCuentasConvenio = class
    private
      FCodigoConvenio : integer;
      FNumeroTurno: longint;
      FVehiculos : TList;        //Esta lista solamente almacena los vehiculos que no estan de baja (es decir solo almacena o activos o suspendidos)
      FVehiculosEliminados : TList;   //Son los vehiculos eliminados historicamente
      FVehiculosQuitados : TList;     //Son los vehiculos eliminados en la mod actual
      (* Almacena una lista con los veh�culos a los que se les tiene que cobrar
      el Tag o se les tiene que cobrar el soporte, ya que no ha sido devuelto. *)
      FVehiculosTagsACobrar : TList;
      FVehiculosCambiarEstadoTags : TList; // Indica un cambio de estado en el tag
      FVehiculosTagsPerdidos: TList;
      FCuentasActivadas: TList;
      FVehiculosEliminadosSinGuardar : TList; //Son los vehiculos que se dieron de alta y de baja sin llegar a guardar en la BD
      FPuntoEntrega: Integer;
      // Para almacenar si en el momento de crear las cuentas del convenio, si
      // todas las cuentas est�n dadas de Baja o Suspendidas.
      FEstanTodasDadasBajaOSuspendidas: Boolean;
      function ObtenerCantidadVehiculos : integer;
      function ObtenerCantidadVehiculosQuitados : integer;
      function GetCantidadTagSinVehiculos: Integer;
      function GetVehiculosEliminados(Index: Integer): TVehiculosConvenio;
      function GetCantidadTagsQuitados: Integer;
      function GetVehiculosTagsQuitado(Index: Integer): TVehiculosConvenio;
      function GetVehiculosRobados(Index: Integer): TVehiculosConvenio;
      function GetVehiculosTagPerdido(Index: Integer): TVehiculosConvenio;
      function GetTagSinVehiculos(Index: Integer): TVehiculosConvenio;
      function GetCantidadVehiculosAgregadosModificados: Integer;
      function GetObtenerCantidadVehiculosNuevos: Integer;
      function GetVehiculosEliminadosSinGuardar(Index: Integer): TVehiculosConvenio;
      function GerCantidadVehiculosEliminadosSinGuardar: Integer;
      function GetCantidadVehiculosRobados: integer;
      function GetCantidadVehiculosACobrar: integer;
      function GetCantidadVehiculosTagPerdido: integer;
      function GetVehiculosTagsACobrar(Index: Integer): TVehiculosConvenio;
      function GetVehiculo(Index : integer) : TVehiculosConvenio;
      procedure SetNumeroTurno(const Value: longint);
      procedure CorregirFechaAlta(unVehiculoAlta: PVehiculosConvenio);
    public
      property CodigoConvenio : integer read FCodigoConvenio;
      property NumeroTurno : longint read FNumeroTurno write SetNumeroTurno;
      property PuntoEntrega: Integer read FPuntoEntrega;
      property Vehiculos[Index : Integer] :  TVehiculosConvenio read GetVehiculo;
      property TagSinVehiculos[Index : Integer]:  TVehiculosConvenio read GetTagSinVehiculos;
      property VehiculosEliminados[Index : Integer] :  TVehiculosConvenio read GetVehiculosEliminados;
      property VehiculosTagsQuitado[Index : Integer] :  TVehiculosConvenio read GetVehiculosTagsQuitado;
      property VehiculosRobados[Index : Integer] :  TVehiculosConvenio read GetVehiculosRobados;
      property VehiculosTagPerdido[Index : Integer] :  TVehiculosConvenio read GetVehiculosTagPerdido;
      property VehiculosTagsACobrar[Index : Integer] :  TVehiculosConvenio read GetVehiculosTagsACobrar;
      property TagsSinVehiculo[Index : Integer] :  TVehiculosConvenio read GetTagSinVehiculos;
      property VehiculosEliminadosSinGuardar[Index : Integer] :  TVehiculosConvenio read GetVehiculosEliminadosSinGuardar;
      property CantidadVehiculosAgregadosModificados : Integer read GetCantidadVehiculosAgregadosModificados;
      property CantidadVehiculos : integer read ObtenerCantidadVehiculos;
      property CantidadVehiculosNuevos : integer read GetObtenerCantidadVehiculosNuevos;
      property CantidadVehiculosEliminados : integer read ObtenerCantidadVehiculosQuitados;
      property CantidadTagSinVehiculos : Integer read GetCantidadTagSinVehiculos;
      property CantidadVehiculosRobados : Integer read GetCantidadVehiculosRobados;
      property CantidadVehiculosTagPerdido : Integer read GetCantidadVehiculosTagPerdido;
      property CantidadVehiculosACobrar : Integer read GetCantidadVehiculosACobrar;
      property CantidadTagsQuitados : Integer read GetCantidadTagsQuitados;
      property CantidadVehiculosEliminadosSinGuardar : Integer read GerCantidadVehiculosEliminadosSinGuardar;
      property EstanTodasDadasBajaOSuspendidas : Boolean read FEstanTodasDadasBajaOSuspendidas;

      constructor Create(CodigoConvenio: Integer; PuntoEntrega: Integer);
      destructor  Destroy; override;
      procedure ObtenerVehiculos(cdsVehiculos : TClientDataset; IncluirEliminados: Boolean = False); overload;
      procedure AgregarVehiculo(const AVehiculo : TVehiculosConvenio);
      procedure AgregarCuentaActivada(Index: Integer; Vehiculo: TVehiculosConvenio);
      procedure AgregarCambioVehiculo(const AVehiculo: TVehiculosConvenio; pFechaAlta: TDateTime);  // SS_916_PDO_20120109
      procedure AgregarDocumentacionVehiculo(IndexVehiculo : integer; ADocumentacion : TRegistroDocumentacion);
      procedure QuitarVehiculo(Index : integer); overload;
      procedure QuitarVehiculo(Patente: String); overload;
      procedure QuitarVehiculoPorCambioVehiculo(Index: integer); overload;
      procedure QuitarVehiculoPorCambioVehiculo(Patente: String); overload;
      procedure QuitarCuentaSuspendida(AVehiculo: TVehiculosConvenio);
      procedure QuitarVehiculoRobado(Index : integer); overload;
      procedure QuitarVehiculoRobado(Patente: String); overload;
      procedure QuitarVehiculoTagPerdido(Index : integer); overload;
      procedure QuitarVehiculoTagPerdido(Patente: String); overload;
      procedure AgregarVehiculoTagACobrar(Index : integer; EstadoConservacion: Integer; CobrarTag, EntregoSoporte: Boolean; ConceptoMovimiento: Integer; DescripcionConceptoMovimiento:string; NumeroVoucher: integer; CodigoAlmacenDestino: integer; TipoAsignacionTag: string ); overload;
      procedure AgregarVehiculoTagACobrar(Patente: String; EstadoConservacion: Integer; CobrarTag, EntregoSoporte: Boolean; ConceptoMovimiento: Integer; DescripcionConceptoMovimiento:string); overload;
      procedure AgregarVehiculoTagACobrar(Index : integer; ConceptoMovimiento: Integer; DescripcionConceptoMovimiento:string; NumeroVoucher: integer; CodigoAlmacenDestino: integer; TipoAsignacionTag: string ); overload;
      procedure AgregarVehiculoTagACobrar(Index, IDMotivoMovCuentaTelevia, ConceptoMovimiento, NumeroVoucher, CodigoAlmacenDestino : integer; DescripcionConceptoMovimiento, TipoAsignacionTag : string ); overload;
      procedure AgregarConceptoAPagar(Index: Integer; CodigoConcepto: Integer; Importe: Integer; Credito: Boolean);
      procedure EditarVehiculo(Index : integer; AVehiculo : TVehiculosConvenio); overload;
      procedure EditarVehiculo(Patente:String; const AVehiculo : TVehiculosConvenio); overload;
      procedure EditarVehiculosSinDevolucion(AVehiculo : TVehiculosConvenio); overload;
      procedure DestruirVehiculo(Index : integer); overload;
      procedure DestruirVehiculo(Patente: String); overload;

      class function ObtenerDatosVehiculo(BuscarPatenteRVM : Boolean; CodigoTipoPatente,Patente : String; var Vehiculo : TVehiculos) : boolean;
      class function  ObtenerVehiculoAsignadoConvenio(CodigoTipoPatente,Patente : String) : integer;
      //procedure Guardar;
      //procedure Guardar(EsConvenioCN:Boolean;SeCambioBitAMB:Boolean=False;HabilitadoAMB:Boolean=False);  //SS-1006-NDR-20111105
      //procedure Guardar(EsConvenioCN:Boolean;SeCambioBitPA:Boolean=False;AdheridoPA:Boolean=False);      //SS_1147_MCA_20140408 //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
      procedure Guardar(EsConvenioNativo:Boolean;SeCambioBitPA:Boolean=False;AdheridoPA:Boolean=False);		//SS_1147_MCA_20140408
      function  BuscarIndice(Patente: String):Integer; overload;
      function  BuscarIndice(SerialNumber: DWORD):Integer; overload;
      function  BuscarCobrado(SerialNumber: DWORD): integer;
      function  BuscarIndiceCambiarEstadoTags(SerialNumber: DWORD): Integer;
      procedure AgregarCambiarEstadoTags(AVehiculo : TVehiculosConvenio);
      procedure QuitarVehiculoSinTag(Index : integer); overload;
      procedure QuitarVehiculoSinTag(Patente: String); overload;
      procedure ObtenerRegVehiculos(var AVehiculos : TRegCuentaVehiculos);
      procedure ObtenerRegVehiculosEliminados(var AVehiculos : TRegCuentaVehiculos; Anexar : Boolean = False);
      procedure ObtenerRegVehiculosTagsQuitado(var AVehiculos : TRegCuentaVehiculos; Anexar : Boolean = False);
      procedure ObtenerRegVehiculosVehiculosRobados(var AVehiculos : TRegCuentaVehiculos; Anexar : Boolean = False);
      procedure ObtenerRegVehiculosVehiculosTagPerdido(var AVehiculos : TRegCuentaVehiculos; Anexar : Boolean = False);
      procedure ObtenerRegVehiculosVehiculosSinTag(var AVehiculos : TRegCuentaVehiculos; Anexar : Boolean = False);
      procedure ObtenerRegVehiculosAgregados(var AVehiculos: TRegCuentaVehiculos; CuentasOriginal: TCuentasConvenio);
      procedure ObtenerRegVehiculosModificados(var AVehiculos: TRegCuentaVehiculos; CuentasOriginal: TCuentasConvenio);
      procedure ObtenerRegVehiculosModificadosTag(var AVehiculos: TRegCuentaVehiculos; ConModifTag : Boolean = False);
      procedure ObtenerRegVehiculosNOModificados(var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean = False);
      procedure ObtenerRegVehiculosActivados(var AVehiculos : TRegCuentaVehiculos; Anexar : Boolean = False);
      procedure ObtenerRegVehiculosTagPerdidos(var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean = False);
      procedure QuitarTagVehiculo(Index : Integer);
      function  DigitoVerificadorValido(Patente,DigitoVerificador:String):Boolean;
      function TieneCuentas: Boolean;
      procedure CambiarEstadoConservacionTAG (Index : integer; EstadoConservacionTAG: integer);
      procedure Suspender(Index : integer; FechaLimite: TDateTime; Voluntario: Boolean);
      procedure ActivarCuenta(Index : integer);
      function TieneCuentasSuspendidas: boolean;
      function TieneTagVendidosSinUsar: Boolean;
      function TieneCuentasActivas: Boolean;
      function ObtenerVehiculo(Patente: String): TVehiculosConvenio;
      function CalcularVencimientoGarantiaTag( Indice: integer ; TipoAsignacionTag: string; var DescriError: string):boolean;overload;
      function CalcularVencimientoGarantiaTag(AVehiculo: PVehiculosConvenio): TDateTime; overload;
      function AlMenosUnaCuentaActiva: Boolean;
      function TodasLasCuentasDeBaja: Boolean;
      function ExisteDadoBaja(unaPatente: string; unContext: integer; unSerial: int64; var pFechaBaja: TDateTime): Boolean;			// SS_916_PDO_20120109
      function ObtenerIDMotivoMovimientoCuentaTelevia(Indice : integer) : integer;
  end;

  TDatosConvenio = class
   private
      FCodigoConvenio : integer;
      FNumeroConvenio : string;
      FCodigoConvenioExterno: string;
      FNumeroConvenioRNUT: string;
      FFechaBajaRNUT: TDateTime;
      FCodigoPersona : integer;
      FCliente : TPersonaConvenio;
      FMedioPago : TMediosPagoConvenio;
      FBajasForzadas: TBajasForzadasConvenio;       // TASK_070_MGO_20160907
      FMediosEnvioPago : TMediosEnvioDocumentosConvenio;
      FCuentas : TCuentasConvenio;
      FCodigoEstadoConvenio : Integer;
      FAccionRNUT : Integer;
      FEstadoRNUT : Integer;
      FUbicacionFisicaCarpeta : String;
      FNoGenerarIntereses: Boolean;
      FTieneObservacionesBD : boolean;
      FFechaAlta : TDateTime;
      FCodigoConcesionaria : Integer;
      YaTrajoObservaciones : Boolean;
      FRecibirInfoPorMail: Boolean;
      FNumeroTurno: longint;
      FDocumentacionConvenio : TDocumentacionConvenio;
      FCheckListConvenio: TCheckListConvenio;
      FChekListCuenta: TChekListCuenta;
      FListObservacionConvenio: TListObservacionConvenio;
      FCodigoDomicilioFacturacion: Integer;
      FPuntoVenta: Integer;
      FPuntoEntrega: Integer;
      FTieneCuentasSuspendidasOtroConvenio: Boolean;
      FTieneTagVendidoSinUsar: Integer;
      FNroAnexoEmitido: Integer;
      FDocumentoElectronicoAImprimir : string;
      //FHabilitadoAMB:Boolean;                       //SS-1006-NDR-20111105
      //FHabilitadoPA:Boolean;                        //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
      FAdheridoPA:Boolean;                            //SS-1006-NDR-20120614
      FSinCuenta   :Boolean;                          //SS_628_ALA_20111201

      FCodigoConvenioFacturacion : Integer;                       //TASK_004_ECA_20160411
      FCodigoTipoConvenio: Integer;                       //TASK_004_ECA_20160411
      FCodigoClienteFacturacion : Integer;                       //TASK_004_ECA_20160411
      FCodigoTipoCliente : Integer;                              //TASK_040_ECA_20160628
      FCodigoGrupoFacturacion : Integer;                                        // TASK_096_MGO_20161220


      (* Indica si el medio de pago del convenio ha cambiado. *)
      FMedioDePagoModificado: Boolean;
      FFechaActualizacionMedioPago: TDateTime;          //SS_1068_NDR_20120910
      FTieneOtrosConveniosEnvioMails: Boolean;
      FDescripcionMensajeRestoEnvioMail: String;
      procedure SetEstadoConvenio(ACodigoEstadoConvenio : integer);
      function GetEstadoConvenio: string;
      procedure SetAccionRNUT(const Value: Integer);
      function GetAccionRNUT: Integer;
      procedure SetEstadoRNUT(const Value: Integer);
      function GetEstadoRNUT: Integer;
      property AccionRNUT : Integer read GetAccionRNUT write SetAccionRNUT;
      property EstadoRNUT : Integer read GetEstadoRNUT write SetEstadoRNUT;
      function GetNumeroConvenioFormateado: String;
      procedure SetUbicacionFisicaCarpeta(const Value: String);
      procedure SetNoGenerarIntereses(const Value: Boolean);
      function GetFechaAlta: TDateTime;
      function GetFechaActualizacionMedioPago: TDateTime; //SS_1068_NDR_20120910
      function GetDescripcionConcesionaria: String;
      //function GetEsConvenioCN: Boolean;   									//SS_1147_MCA_20140408
      function GetEsConvenioNativo: Boolean;                                    //SS_1147_MCA_20140408
      procedure SetRecibirInfoPorMail(const Value: Boolean);
      procedure SetNumeroTurno(const Value: longint);
      function GetNumeroConvenioRNUT: string;
      procedure SetNumeroConvenioRNUT(const Value: string);
      procedure SetCodigoConvenioExterno(const Value: string);
      function GetFechaBajaRNUT: TDateTime;
      procedure SetFechaBajaRNUT(const Value: TDateTime);
      procedure SetListObservacionConvenio(
      const Value: TListObservacionConvenio);
      procedure SetCheckListConvenio(const Value: TCheckListConvenio);
      procedure SetCodigoDomicilioFacturacion(const Value: Integer);
      procedure SetNroAnexoEmitido(const Value: Integer);
      procedure SetChekListCuenta(const Value: TChekListCuenta);
      function GetTieneObservacion: Boolean;
      function GetCantidadVehiculos: Integer;
      procedure ActualizarMediosEnvioDocumentacion;
      procedure ArmarListaConveniosEnvioAutomatico;
      procedure EliminarOtrosMediosEnvio;
   public
      //property EsConvenioCN : Boolean read GetEsConvenioCN;					//SS_1147_MCA_20140408
      property EsConvenioNativo: Boolean read GetEsConvenioNativo;				//SS_1147_MCA_20140408
      property FechaAlta : TDateTime read GetFechaAlta;
      property FechaActualizacionMedioPago : TDateTime read GetFechaActualizacionMedioPago; //SS_1068_NDR_20120910
      property CodigoConvenio : integer read FCodigoConvenio;
      property NumeroConvenio : string read FNumeroConvenio;
      property CodigoConvenioExterno: string read FCodigoConvenioExterno write SetCodigoConvenioExterno;
      property NumeroConvenioRNUT: string read GetNumeroConvenioRNUT write SetNumeroConvenioRNUT;
      property FechaBajaRNUT: TDateTime read GetFechaBajaRNUT write SetFechaBajaRNUT;
      property NumeroConvenioFormateado : String read GetNumeroConvenioFormateado;
      property CodigoPersona : integer read FCodigoPersona;
      property Cliente : TPersonaConvenio read FCliente;
      property MedioPago : TMediosPagoConvenio read FMedioPago;
      property BajasForzadas : TBajasForzadasConvenio read FBajasForzadas;      // TASK_070_MGO_20160907
      property MediosEnvioPago : TMediosEnvioDocumentosConvenio read FMediosEnvioPago;
      property Cuentas : TCuentasConvenio read FCuentas;
      property CodigoEstadoConvenio : integer read FCodigoEstadoConvenio write SetEstadoConvenio;
      property EstadoConvenio : string read GetEstadoConvenio;
      property UbicacionFisicaCarpeta : String read FUbicacionFisicaCarpeta write SetUbicacionFisicaCarpeta;
      property NoGenerarIntereses: Boolean read FNoGenerarIntereses write SetNoGenerarIntereses;
      property CodigoConcesionaria : Integer read FCodigoConcesionaria;
      property DescripcionConcesionaria : String read GetDescripcionConcesionaria;
      property RecibirInfoPorMail : Boolean read FRecibirInfoPorMail write SetRecibirInfoPorMail;
      property NumeroTurno : Longint read FNumeroTurno Write SetNumeroTurno;
      property DocumentacionConvenio: TDocumentacionConvenio read FDocumentacionConvenio;
      property CheckListConvenio: TCheckListConvenio read FCheckListConvenio write SetCheckListConvenio;
      property ListObservacionConvenio: TListObservacionConvenio read FListObservacionConvenio write SetListObservacionConvenio;
      property CodigoDomicilioFacturacion: Integer read FCodigoDomicilioFacturacion write SetCodigoDomicilioFacturacion;
      property PuntoEntrega: Integer read FPuntoEntrega;
      property PuntoVenta: Integer read FPuntoVenta;
      property ChekListCuenta: TChekListCuenta read FChekListCuenta write SetChekListCuenta;
      property TieneObservacion: Boolean read GetTieneObservacion;
      property MedioDePagoModificado: Boolean read FMedioDePagoModificado;
      property CantidadVehiculos: Integer read GetCantidadVehiculos;
      property TipoComprobanteFiscal : string read FDocumentoElectronicoAImprimir write FDocumentoElectronicoAImprimir;
      //property HabilitadoAMB : Boolean read FHabilitadoAMB write FHabilitadoAMB;         //SS-1006-NDR-20111105
      property AdheridoPA : Boolean read FAdheridoPA write FAdheridoPA;                    //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
      property SinCuenta    : Boolean read FSinCuenta write FSinCuenta;                    //SS_628_ALA_20111201

      property CodigoConvenioFacturacion : Integer read FCodigoConvenioFacturacion write FCodigoConvenioFacturacion;                    //TASK_004_ECA_20160411
      property CodigoTipoConvenio : Integer read FCodigoTipoConvenio write FCodigoTipoConvenio;                    //TASK_004_ECA_20160411
      property CodigoClienteFacturacion : Integer read FCodigoClienteFacturacion write FCodigoClienteFacturacion;                    //TASK_004_ECA_20160411
      property CodigoGrupoFacturacion : Integer read FCodigoGrupoFacturacion write FCodigoGrupoFacturacion;     // TASK_096_MGO_20161220

      property CodigoTipoCliente : Integer read FCodigoTipoCliente write FCodigoTipoCliente;                    //TASK_040_ECA_20160628


      //Seteadas para ver si el existen mas convenios de este cliente con envio por mail
      property TieneOtrosConveniosEnvioMails: Boolean read FTieneOtrosConveniosEnvioMails;
      property DescripcionMensajeRestoEnvioMail: String read FDescripcionMensajeRestoEnvioMail;
      constructor Create(NumeroConvenio: String; PuntoVenta, PuntoEntrega: Integer);
      destructor  Destroy; override;
      //procedure Guardar(SeCambioBitAMB:Boolean=False;HabilitadoAMB:Boolean=False); //SS-1006-NDR-20111105
      procedure Guardar(SeCambioBitPA: Boolean=False;                           //SS-1006-NDR-20120614
                        AdheridoPA: Boolean=False;                              //SS-1006-NDR-20120614
                        SeCambioInhabilitado :Boolean=False;                    //SS-1006-NDR-20120614
                        Inhabilitado: Boolean=False;                            //SS-1006-NDR-20120614
                        intMotivoInhabilitacion:Integer=-1);                       //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
      // ----------------------------------------------------------------------------------

      procedure BajaContrato(APuntoEntrega : integer; AUsuario : string);
	  function ObtenerCambios(ConvenioModificado:TDatosConvenio):Boolean;
      function ObtenerEstadoCambioMedioPago(TipoMedioPagoAnterior, TipoMedioPagoActual: TTiposMediosPago; EstadoCambioMedioPago: integer): integer;
      function TieneCuentasSuspendidas: Boolean;
      function TieneTagVendidoSinUsar: Integer;
      // Impresiones
      function Imprimir(CuentasConvenioOriginal: TCuentasConvenio; var Imprimio: Boolean): Boolean; overload;
      function Imprimir(DocumentoAImprimir: Integer; var MotivosCancelacion:TMotivoCancelacion;
        CantidadCopias : integer; FormImprimir : TObject; CuentasOriginal: TCuentasConvenio): Boolean; overload;

      function ImprimirCaratula(var MotivoCancelacion:TMotivoCancelacion;
               CantCopias:Integer;
               Operador : String;
               PuntoEntrega : String;
               FormImprimir : TObject): Boolean;
      function ImprimirEtiquetas(var MotivoCancelacion:TMotivoCancelacion; FormImprimir : TObject; IndiceVehiculo: Integer = -1): Boolean;
      function ImprimirBajaComvenio(var MotivoCancelacion:TMotivoCancelacion; CantidadCopias : integer; FormImprimir : TObject): Boolean;
      function ImprimirCaratulaGuardada(var MotivoCancelacion : TMotivoCancelacion; CantCopias:Integer; Operador : String;
                              PuntoEntrega : String; FormImprimir : TObject): Boolean;
      property NroAnexoEmitido: Integer read FNroAnexoEmitido write SetNroAnexoEmitido;
      function TurnoAbierto: Boolean;
      function ImprimirAnexo3y4VehNuevos : boolean;
      function ReimprimirAnexo3y4 : boolean;
      function TieneMedioEnvioPorEMail : boolean;
      Function EsConvenioSuspendido: Boolean;
      function ConvenioTieneMotosNuevas: Boolean;
      procedure ArmarListaVouchers( var Lista: TStringList);
      function ActualizarConvenio(VersionAnexo1: Integer):Integer; //    TASK_077_JMA_2016112
      procedure ActualizarConvenioInhabilitadoPA(SeCambioInhabilitado :Boolean=False; intMotivoInhabilitacion:Integer=-1); //   TASK_077_JMA_20161124
  end;

  TListaConveniosPersona = array of TDatosConvenio;

  TConvenioFactory = Class
    public
        class procedure ConveniosPersonas(var AConvenios : TListaConveniosPersona; TipoDocumento,  NumeroDocumento: string; PuntoVenta, PuntoEntrega: Integer);
        class procedure ConveniosPersonaComoMandante(var AConvenios : TListaConveniosPersona; TipoDocumento,  NumeroDocumento: string; PuntoVenta, PuntoEntrega: Integer);
        class procedure ConveniosPersonasComoRepresentanteLegal(var AConvenios : TListaConveniosPersona; TipoDocumento,  NumeroDocumento: string; PuntoVenta, PuntoEntrega: Integer);
  end;


implementation

uses DMConnection, frmImprimirConvenio;

const
    CONST_EMAIL = 1;


var
    ConvenioTieneMedioEnvioPorEMail : boolean;
{ EExtendedError }

{Procedure Name	: EExtendedError.Create
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: Constructor del error extendido..
 Parameters   : const Msg, DetailMsg: string; ErrCode: integer
}
constructor EExtendedError.Create(const Msg, DetailMsg: string;
  ErrCode: integer);
begin
        Message := Trim(Msg);
        DetailMessage := Trim(DetailMsg);
        ErrorCode := ErrCode;
end;

{ TDatosConvenio }

{Procedure Name	: TDatosConvenio.ArmarListaVouchers
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
}
procedure TDatosConvenio.ArmarListaVouchers( var Lista: TStringList);
var
    i:integer;
    AMovCuenta: PMovimientoCuenta;
begin
    for i := 0 to FCuentas.FVehiculosTagsACobrar.Count - 1 do begin
        AMovCuenta := FCuentas.FVehiculosTagsACobrar.items[i];
        if AMovCuenta^.NumeroVoucher <> 0 then
            Lista.Add(IntToStr(AMovCuenta^.NumeroVoucher) );
    end;
end;

{Procedure Name	: ArmarListaConveniosEnvioAutomatico
 Author 		: Nelson Droguett Sierra (vpaszkowicz)
 Date Created	: 20-Abril-2010
 Description	: Agrega una descripci�n de los convenios que forman parte del seteo
                  de envio autom�tico de mails, os sea que est�n asociados a esa
                  persona.
 Parameters   	:
}
procedure TDatosConvenio.ArmarListaConveniosEnvioAutomatico;
begin
    FTieneOtrosConveniosEnvioMails := False;
    FDescripcionMensajeRestoEnvioMail := '';
    with TADOStoredProc.Create(nil) do begin
    try
        Connection:=DMConnections.BaseCAC;
        ProcedureName:='ObtenerMedioMailDelRestoConveniosPersona';
        with Parameters do begin
            Refresh;
            ParamByName('@CodigoConvenioExcluido').Value := FCodigoConvenio;
            ParamByName('@CodigoPersona').Value := FCodigoPersona;
        end;
        Open;
        if not isEmpty then begin //Existe alg�n otro Convenio seteado
            //Seteo que hay mas convenios marcados.
            FTieneOtrosConveniosEnvioMails := True;
            While not Eof do begin
                if FDescripcionMensajeRestoEnvioMail <> '' then
                    FDescripcionMensajeRestoEnvioMail := FDescripcionMensajeRestoEnvioMail + CRLF;
                FDescripcionMensajeRestoEnvioMail := FDescripcionMensajeRestoEnvioMail + 'Numero Convenio: ' + Trim(FieldByName('NumeroConvenio').AsString) +
                'Tipo de documento: ' + Trim(FieldByName('DescripcionCodigoMedioEnvio').AsString);
                Next;
            end;
        end;
        except
            Close;
            Free;
        end;
    end;
end;

{Procedure Name	: BajaContrato
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
}
procedure TDatosConvenio.BajaContrato(APuntoEntrega : integer; AUsuario : string);
var										//CAC-CLI-066_MGO_20160218
    vParametros, vNumero: String;		//CAC-CLI-066_MGO_20160218
begin
    with TADOStoredProc.Create(nil) do
    try
       Connection:=DMConnections.BaseCAC;

       ProcedureName:='BajaConvenioCompleto';
       with Parameters do begin
                Refresh;
                Parameters.ParamByName('@CodigoPuntoEntrega').Value   := APuntoEntrega;
                Parameters.ParamByName('@Usuario').Value              := AUsuario;
                Parameters.ParamByName('@CodigoConvenio').Value       := FCodigoConvenio;
                Parameters.ParamByName('@NumeroTurno').Value          := GNumeroTurno;      // SS_937_PDO_20120516
                Parameters.ParamByName('@AccionRnut').Value           := AccionRNUT;
       end;
       ExecProc;
    finally
       close;
       free;
    end;
end;

{Procedure Name	: ConvenioTieneMotosNuevas
 Author 		: Nelson Droguett Sierra (FSandi)
 Date Created	: 20-Abril-2010
 Description	: Verifica si el convenio tiene una nueva moto (para permitir imprimir el contrato)
 Parameters   	:
 Return Value : Boolean
}
function TDatosConvenio.ConvenioTieneMotosNuevas: Boolean;
var
   i :Integer;
   AuxVehiculos : TRegCuentaVehiculos;
   auxDescripTipo, auxDescripCategoria, auxCategoria:String;
begin
    SetLength(AuxVehiculos,0);
    Cuentas.ObtenerRegVehiculosAgregados(AuxVehiculos,FCuentas);
    ConvenioTieneMotosNuevas :=False;
    for i := 1 to Length(AuxVehiculos) do begin
        //ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,AuxVehiculos[i-1].Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);  //TASK_106_JMA_20170206
        if trim(auxCategoria) = XML_VEHICULO_MOTO then begin
            ConvenioTieneMotosNuevas := True;
            Break;
        end;
    end;
end;


{Procedure Name	: TDatosConvenio.Create
 Author 		: dcepeda       (Nelson Droguett Sierra (cchiappero))
 Date Created	: 03-Mayo-2010  (20-Abril-2010)
 Description	:  Constructor de Convenios
                   Agrego al final una llamada al ArmarListaConveniosEnvioAutomatico
                   para que busque todos los convenios de esa persona con sus envios por mail
                   que no sean el actual.
}
constructor TDatosConvenio.Create(NumeroConvenio: String; PuntoVenta, PuntoEntrega: Integer);
resourcestring
    MSG_NOT_EXISTS = 'No Existen Convenio';
    MSG_INIT_ERROR = 'Error al iniciar convenio.';
var
    personeria : string;
begin
    FMedioDePagoModificado := False;
    with TADOStoredProc.Create(nil) do
    try
        try
            FPuntoEntrega := PuntoEntrega;
            FPuntoVenta := PuntoVenta;
            //datos del convenio
            Connection:=DMConnections.BaseCAC;
            ProcedureName:='ObtenerConvenio';
            NumeroConvenio := Trim(NumeroConvenio);
            //if Length(NumeroConvenio) = CANT_CARACTERES_NUMERO_CONVENIO + Length(TXT_CONVENIO_SIN_CUENTA) then                                        //SS_1147Q_NDR_20141202
            if Length(NumeroConvenio) = LargoNumeroConvenioConcesionariaNativa() + Length(TXT_CONVENIO_SIN_CUENTA) then                                 //SS_1147Q_NDR_20141202
            begin
                //if Copy(NumeroConvenio, CANT_CARACTERES_NUMERO_CONVENIO + 1, Length(TXT_CONVENIO_SIN_CUENTA)) = TXT_CONVENIO_SIN_CUENTA then          //SS_1147Q_NDR_20141202
                if Copy(NumeroConvenio, LargoNumeroConvenioConcesionariaNativa() + 1, Length(TXT_CONVENIO_SIN_CUENTA)) = TXT_CONVENIO_SIN_CUENTA then   //SS_1147Q_NDR_20141202
                    //NumeroConvenio := Copy(NumeroConvenio, 1, CANT_CARACTERES_NUMERO_CONVENIO)                                                        //SS_1147Q_NDR_20141202
                    NumeroConvenio := Copy(NumeroConvenio, 1, LargoNumeroConvenioConcesionariaNativa)                                                   //SS_1147Q_NDR_20141202
            end;

            with Parameters do begin
                Refresh;
                ParamByName('@NumeroConvenio').Value:=Trim(NumeroConvenio);
            end;
            Open;

            if not isEmpty then begin // no hay ningun convenio

                FCodigoConvenio:= FieldByName('CodigoConvenio').AsInteger;
                FNumeroConvenio:= Trim(NumeroConvenio);
                FCodigoConvenioExterno:= FieldByName('CodigoConvenioExterno').AsString;
                FCodigoPersona := FieldByName('CodigoCliente').AsInteger;
                FCodigoEstadoConvenio := FieldByName('CodigoEstadoConvenio').AsInteger;
                AccionRNUT := FieldByName('AccionRNUT').AsInteger;
                FTieneObservacionesBD := FieldByName('TieneObservaciones').AsBoolean;
                FUbicacionFisicaCarpeta := FieldByName('UbicacionFisicaCarpeta').AsString;
                FNoGenerarIntereses := FieldByName('NoGenerarIntereses').AsBoolean;
                FFechaAlta := FieldByName('FechaAlta').AsDateTime;
                FFechaActualizacionMedioPago := FieldByName('FechaActualizacionMedioPago').AsDateTime;  //SS_1068_NDR_20120910
                FCodigoConcesionaria := FieldByName('CodigoConcesionaria').AsInteger;
                RecibirInfoPorMail := FieldByName('RecibirInfoPorMAil').AsBoolean;
                NumeroConvenioRNUT := Trim(FieldByName('NumeroConvenioRNUT').AsString);
                FechaBajaRNUT := iif(FieldByName('FechaBajaRnut').IsNull, nulldate, FieldByName('FechaBajaRnut').AsDateTime);
                CodigoDomicilioFacturacion := FieldByName('CodigoDomicilioFacturacion').AsInteger;
                Personeria := QueryGetValue(DMConnections.BaseCAC,'select personeria from personas  WITH (NOLOCK) where CodigoPersona = ' + InttoStr(FCodigoPersona));
                FDocumentoElectronicoAImprimir := FieldByName('TipoComprobanteFiscalAEmitir').AsString;
                //FHabilitadoAMB := FieldByName('HabilitadoAMB').AsBoolean;                               //SS-1006-NDR-20111105
                FAdheridoPA := FieldByName('AdheridoPA').AsBoolean;                                       //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
                FSinCuenta    := FieldByName('SinCuenta').AsBoolean;                                      //SS_628_ALA_20111201

                FCodigoConvenioFacturacion := FieldByName('CodigoConvenioFacturacion').AsInteger;          //TASK_004_ECA_20160411
                FCodigoTipoConvenio        := FieldByName('CodigoTipoConvenio').AsInteger;                 //TASK_004_ECA_20160411
                FCodigoClienteFacturacion  := FieldByName('CodigoClienteFacturacion').AsInteger;           //TASK_004_ECA_20160411
                FCodigoGrupoFacturacion    := FieldByName('CodigoGrupoFacturacion').AsInteger;      // TASK_096_MGO_20161220

                FCodigoTipoCliente         := FieldByName('CodigoTipoCliente').AsInteger;                  //TASK_040_ECA_20160628


                // -----------------------------------------------------------------------------


                //Instancio el titular del convenio obteniendo los datos personales y domicilios
                if Personeria = PERSONERIA_JURIDICA then
                    //FCliente := TPersonaConvenio(TPersonaJuridica.create(FCodigoPersona, FCodigoConvenio, EsConvenioCN))		//SS_1147_MCA_20140408
                    FCliente := TPersonaConvenio(TPersonaJuridica.create(FCodigoPersona, FCodigoConvenio, EsConvenioNativo))	//SS_1147_MCA_20140408
                else begin
                    FCliente := TPersonaConvenio(TPersonaFisica.create(FCodigoPersona));
                end;
                FCliente.Domicilios.SetearDomicilioFacturacion(FieldByName('CodigoDomicilioFacturacion').AsInteger);


                //Instancio el medio de pago asignado al convenio.
                FMedioPago := TMediosPagoConvenio.create(
                                        FieldByName('CodigoConvenio').AsInteger,
                                        FieldByName('CodigoPersonaMedioPagoAutomatico').AsInteger,
                                        FieldByName('TipoMedioPagoAutomatico').AsInteger);

                FBajasForzadas := TBajasForzadasConvenio.Create(FCodigoConvenio);       // TASK_070_MGO_20160907
                FMediosEnvioPago := TMediosEnvioDocumentosConvenio.create(FCodigoConvenio);

                FCuentas := TCuentasConvenio.Create(FCodigoConvenio, PuntoEntrega);
                FDocumentacionConvenio := TDocumentacionConvenio.Create(CodigoConvenio);
                FTieneTagVendidoSinUsar := QueryGetValueInt(DMConnections.BaseCAC, 'EXEC TieneTAGsVendidosSinUsar ' + IntToStr(CodigoConvenio));

                YaTrajoObservaciones := False;

                FCheckListConvenio := TCheckListConvenio.Create(CodigoConvenio);
                FListObservacionConvenio := TListObservacionConvenio.Create(CodigoConvenio);
                FChekListCuenta := TChekListCuenta.Create(CodigoConvenio);

                FTieneCuentasSuspendidasOtroConvenio := QueryGetValueInt(DMConnections.BaseCAC, 'EXEC TieneCuentasSuspendidasEnOtroConvenios ' + IntToStr(CodigoConvenio) ) = 1;
                //Setea todos los envios para ese mail del cliente.
                ArmarListaConveniosEnvioAutomatico;
            end else raise EConvenioError.Create(MSG_NOT_EXISTS, '', ERRORNOEXISTECONVENIO);
        except
            on E: Exception do begin
              if E.ClassType = Exception then
                raise EConvenioError.Create(MSG_INIT_ERROR, e.message, ERRORNOEXISTECONVENIO)
              else raise;
            end;
        end;
    finally
        close;
        free;
    end;
end;


{Procedure Name	: TDatosConvenio.Destroy
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: Destructor de un Convenios
 Parameters   	:
 Return Value   :
}
destructor TDatosConvenio.Destroy;
begin
  if Assigned(FCuentas) then Fcuentas.Free;
  if Assigned(FCliente) then Fcliente.free;
  if Assigned(FMedioPago) then FMedioPago.Free;
  if Assigned(FBajasForzadas) then FBajasForzadas.Free;     // TASK_070_MGO_20160907
  if Assigned(FMediosEnvioPago) then FMediosEnvioPago.free;
  if Assigned(FDocumentacionConvenio) then FDocumentacionConvenio.Free;
  if Assigned(FCheckListConvenio) then FCheckListConvenio.Free;
  if Assigned(FListObservacionConvenio) then FListObservacionConvenio.Free;
  if Assigned(FChekListCuenta) then FChekListCuenta.Free;
  inherited;
end;

{Procedure Name	: TDatosConvenio.EsConvenioSuspendido
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDatosConvenio.EsConvenioSuspendido: Boolean;
// Esta Funcion determina si el convenio se encuentra suspendido verificando si esta activo y no tiene cuentas activas
begin
    Result := False;
    //Verifica que el convenio este activo
    if FCodigoEstadoConvenio = 1 then begin
        //Verifica si el convenio esta activo, pero tiene todas las cuentas de baja, entonces esta suspendido
        if Cuentas.TodasLasCuentasDeBaja then Result := True else begin
            //Verifica si tiene cuentas activas, si no tiene ninguna cuenta activa, entonces esta suspendido
            if not Cuentas.AlMenosUnaCuentaActiva then Result := True;
        end;
    end;
end;

{Procedure Name	: TDatosConvenio.GetAccionRNUT
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TDatosConvenio.GetAccionRNUT: Integer;
begin
    //Veo si en el RNUT no esta dado de baja se pueden hacer modif
    Result := FAccionRNUT;
    if (EstadoRNUT <> StrToInt(XML_BAJA)) then begin
        if  assigned(DocumentacionConvenio) and (DocumentacionConvenio.CantidadOperacionesConvenio > 0) then Result := StrToInt(XML_MODIFICACION);
        if (Cuentas.CantidadVehiculosEliminados > 0) or (Cuentas.CantidadVehiculosAgregadosModificados > 0) (*or (Cuentas.CantidadCuentasNuevasSuspendidas > 0)*) then Result := StrToInt(XML_MODIFICACION);

        //si no tiene vehiculos mando la baja del convenio
        if (Cuentas.CantidadVehiculos = 0) then Result := StrToInt(XML_BAJA);
    end
    else begin
        //Si esta dado de baja en el RNUT le asigno el nuevo numero para enviar al RNUT y le seteo la accion en alta si tiene vehiculos
        if (Cuentas.CantidadVehiculos > 0) then Result := StrToInt(XML_ALTA)
        //si esta dado de baja y la accion es Nada dejo nada, ya que lo que se esta haciendo es dar de baja un convenio sin Vehiculos
        else Result := iif (FAccionRNUT = StrToInt(XML_NADA), StrToInt(XML_NADA), StrToInt(XML_BAJA));
    end;
end;

{Procedure Name	: GetEstadoConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : string
}
function TDatosConvenio.GetEstadoConvenio: string;
begin
    Result := QueryGetValue(DMConnections.BaseCAC, 'select descripcion from estadosconvenios  WITH (NOLOCK) where  codigoEstadoConvenio = ' + inttostr(FCodigoEstadoConvenio));
end;


//BEGIN : SS_!147Q_20141202 --------------------------------------------------------------------------------------
function TDatosConvenio.GetNumeroConvenioFormateado: String;
begin
    //INICIO: TASK_014_ECA_20160520
//    if ObtenerCodigoConcesionariaNativa=CODIGO_VS then
//    begin
//        //if Length(NumeroConvenio) < LargoNumeroConvenioConcesionariaNativa() then Result := '';                           //SS_1147_MCA_20150420
//        if Length(NumeroConvenio) < LargoNumeroConvenioConcesionariaNativa() then Result := NumeroConvenio                  //SS_1147_MCA_20150420
//        else Result := copy(NumeroConvenio, 1, 9) + '-' + StrRight(NumeroConvenio , 3);
//    end
//    else
//    begin
//        if Length(NumeroConvenio) < LargoNumeroConvenioConcesionariaNativa() then Result := ''
//        else Result := StrLeft(NumeroConvenio, 3) + '-' + copy(NumeroConvenio, 4, 11) + '-' + StrRight(NumeroConvenio , 3);
//    end;

     Result := StrLeft(NumeroConvenio, 3) + '-' + copy(NumeroConvenio, 4,  length(NumeroConvenio));
    //FIN: TASK_014_ECA_20160520
end;
//END : SS_!147Q_20141202 --------------------------------------------------------------------------------------




{Procedure Name	: TDatosConvenio.Guardar
 Author 		: dcepeda       (Nelson Droguett Sierra (cchiappero))
 Date Created	: 03-Mayo-2010  (20-Abril-2010)
 Description	: Guarda el Convenio y toda la informaci�n relacionada a la base
}
//procedure TDatosConvenio.Guardar(SeCambioBitAMB: Boolean=False; HabilitadoAMB: Boolean=False); //SS-1006-NDR-20111105
procedure TDatosConvenio.Guardar( SeCambioBitPA: Boolean=False;                 //SS-1006-NDR-20120614
                                  AdheridoPA: Boolean=False;                    //SS-1006-NDR-20120614
                                  SeCambioInhabilitado :Boolean=False;          //SS-1006-NDR-20120614
                                  Inhabilitado: Boolean=False;                  //SS-1006-NDR-20120614
                                  intMotivoInhabilitacion:Integer=-1               //SS-1006-NDR-20120614
                                );                                              //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
resourcestring
    MSG_SAVE_ERROR = 'Error al guardar convenio.';
    //SQL_CONCESIONARIA           = 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO()';      //TASK_077_JMA_20161124_V2
var
    VersionAnexo1: Integer;  //TASK_018_JMA_20160606
    Cambio : Boolean;
    i: Integer;
begin

    FCliente.Guardar;
    if Self.FMedioDePagoModificado then FMedioPago.Guardar;

{INICIO:TASK_018_JMA_20160606}
    Cambio:= false;
    VersionAnexo1 := QueryGetIntegerValue(DMConnections.BaseCAC, 'select dbo.ObtenerVersionAnexo1(' + IntToStr(FCuentas.CodigoConvenio) + ')');
    if FCuentas.CantidadVehiculosAgregadosModificados > 0 then
    begin
        for i := 0 to FCuentas.CantidadVehiculos - 1 do
        begin
            if FCuentas.Vehiculos[i].EsVehiculoNuevo or
                FCuentas.Vehiculos[i].EsTagNuevo or
                FCuentas.Vehiculos[i].EsCambioTag or
                FCuentas.Vehiculos[i].EsCambioVehiculo then
                    Cambio := True;
        end;
    end;
    if Cambio then
        VersionAnexo1 := VersionAnexo1 + 1;
{TERMINO: TASK_018_JMA_20160606}


{INICIO: TASK_077_JMA_20161124
    with TADOStoredProc.Create(nil) do
    try
      Connection        := DMConnections.BaseCAC;
      //ProcedureName     := 'ActualizarConvenio';  TASK_009_ECA_20160506
      ProcedureName     := 'CRM_CONVENIO_UPDATE'; //TASK_009_ECA_20160506
      ExecuteOptions    := [eoExecuteNoRecords];
      try
            with Parameters do begin
                Refresh;
                ParamByName('@NumeroConvenio').Value          := FNumeroConvenio;
                ParamByName('@CodigoCliente').Value           := FCodigoPersona;
                ParamByName('@CodigoEstadoConvenio').Value    := ESTADO_CONVENIO_VIGENTE;
                ParamByName('@Usuario').Value                 := UsuarioSistema;              //SS_1419_NDR_20151130 SS_162_MBE_20140428
                case FMedioPago.MedioPago.TipoMedioPago of
                    PAC     : ParamByName('@TipoMedioPagoAutomatico').Value   := TPA_PAC;
                    PAT     : ParamByName('@TipoMedioPagoAutomatico').Value   := TPA_PAT;
                    NINGUNO : ParamByName('@TipoMedioPagoAutomatico').Value   := TPA_NINGUNO;
                end;
                ParamByName('@CodigoDomicilioFacturacion').Value      := FCliente.Domicilios.Domicilios[FCliente.Domicilios.IndiceDomicilioFacturacion].CodigoDomicilio;
                ParamByName('@CicloFacturacion').Value                := null;
                ParamByName('@TipoTotalizacionDocumentosCobro').Value := null;
                ParamByName('@FechaInicioVigencia').Value             := NowBase(DMConnections.BaseCAC);
                ParamByName('@UbicacionFisicaCarpeta').Value          := FUbicacionFisicaCarpeta;
                if Assigned(FMedioPago.Mandante) and (FMedioPago.Mandante.CodigoPersona > 0) then
                (* Se controla que el CodigoPersona sea > a 0 (cero) ya que
                algunas interfaces envian Null, para el dato del mandante. *)
                ParamByName('@CodigoPersonaMedioPagoAutomatico').Value       := FMedioPago.Mandante.CodigoPersona
                else ParamByName('@CodigoPersonaMedioPagoAutomatico').Value   := NULL;

                ParamByName('@CodigoCampania').Value                := null;
                ParamByName('@CodigoUsuarioActualizacion').Value    := UsuarioSistema;
                ParamByName('@CodigoConvenio').Value                :=CodigoConvenio;
                //ParamByName('@AccionRNUT').Value                    := iif(EsConvenioCN, AccionRNUT, 0);			//SS_1147_MCA_20140408
                ParamByName('@AccionRNUT').Value                    := iif(EsConvenioNativo, AccionRNUT, 0);			//SS_1147_MCA_20140408
                ParamByName('@CodigoConcesionaria').Value           :=FCodigoConcesionaria;
                ParamByName('@NumeroConvenioRNUT').Value            := NumeroConvenioRNUT;
                ParamByName('@RecibirInfoPorMail').Value            := RecibirInfoPorMail;
                ParamByName('@FechaBajaRNUT').Value                 := iif((not Cuentas.TieneCuentasActivas), iif(FechaBajaRNUT <> NULLDATE, FechaBajaRNUT, NowBase(DMConnections.BaseCAC)), NULL);
                if MedioPago.EsCambioDatosTransBank or MedioPago.EsCambioTipoMedioPago then
                    ParamByName('@FechaActualizacionMedioPago').Value   := NowBase(DMConnections.BaseCAC)
                else ParamByName('@FechaActualizacionMedioPago').Value  := NULL;
                ParamByName('@ForzarConfirmado').Value              := MedioPago.ForzarConfirmado;
                ParamByName('@NoGenerarIntereses').Value            := NoGenerarIntereses;
                ParamByName('@Suspendido').Value                    := EsConvenioSuspendido;
                ParamByName('@TipoComprobanteFiscalAEmitir').Value  := iif(FDocumentoElectronicoAImprimir = TDCBX_AUTOMATICO_COD, NULL, FDocumentoElectronicoAImprimir);
                //ParamByName('@HabilitadoAMB').Value               := FHabilitadoAMB;       //SS-1006-NDR-20111105
                ParamByName('@AdheridoPA').Value                    := FAdheridoPA;          //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
                ParamByName('@SinCuenta').Value                     := FSinCuenta;           //SS_628_ALA_20111201
                ParamByName('@ConvenioFacturacion').Value           := FCodigoConvenioFacturacion;          //TASK_004_ECA_20160411
                ParamByName('@CodigoTipoConvenio').Value            := FCodigoTipoConvenio;                 //TASK_004_ECA_20160411
                ParamByName('@CodigoClienteFacturacion').Value      := FCodigoClienteFacturacion;           //TASK_004_ECA_20160411
                ParamByName('@VersionAnexo1').Value                  := VersionAnexo1;           //TASK_018_JMA_20160606

                //INICIO: TASK_004_ECA_20160411
                if FCodigoTipoCliente<>0 then
                   ParamByName('@CodigoTipoCliente').Value                  := FCodigoTipoCliente;
                //FIN: TASK_004_ECA_20160411
            end;
            ExecProc;

            //INICIO: TASK_013_ECA_20160520
            if Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                raise Exception.Create(Parameters.ParamByName('@ErrorDescription').Value );
            //FINI: TASK_013_ECA_20160520

            FCodigoConvenio := Parameters.ParamByName('@CodigoConvenio').Value;
            Close;

            ActualizarMediosEnvioDocumentacion;
            FMediosEnvioPago.Guardar;
            //FCuentas.Guardar(EsConvenioCN,SeCambioBitAMB,HabilitadoAMB); //SS-1006-NDR-20111105
            //FCuentas.Guardar(EsConvenioCN,SeCambioBitPA,AdheridoPA);       //SS_1147_MCA_20140408 //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
            FCuentas.Guardar(EsConvenioNativo,SeCambioBitPA,AdheridoPA);       //SS_1147_MCA_20140408

            FBajasForzadas.Guardar;     // TASK_070_MGO_20160907

            if SeCambioInhabilitado then                                                                       //SS-1006-NDR-20120614
            begin                                                                                              //SS-1006-NDR-20120614
              ProcedureName     := 'ActualizarConvenioInhabilitado';                                           //SS-1006-NDR-20120614
              ExecuteOptions    := [eoExecuteNoRecords];                                                       //SS-1006-NDR-20120614
              with Parameters do                                                                               //SS-1006-NDR-20120614
              begin                                                                                            //SS-1006-NDR-20120614
                Refresh;                                                                                       //SS-1006-NDR-20120614
                ParamByName('@CodigoConvenio').Value            :=FCodigoConvenio;                             //SS-1006-NDR-20120614
                ParamByName('@CodigoConcesionaria').Value       :=QueryGetValueInt( DMConnections.BaseCAC,     //SS-1006-NDR-20120614
                                                                                    SQL_CONCESIONARIA          //SS-1006-NDR-20120614
                                                                                  );                           //SS-1006-NDR-20120614
                ParamByName('@CodigoUsuario').Value             := UsuarioSistema;                             //SS-1006-NDR-20120614
                ParamByName('@CodigoMotivoInhabilitacion').Value:= intMotivoInhabilitacion;                    //SS-1006-NDR-20120614
                ParamByName('@DescripcionError').Value          := '';                                         //SS-1006-NDR-20120614
                if Inhabilitado then ParamByName('@Accion').Value := 'I'                                       //SS-1006-NDR-20120614
                else                 ParamByName('@Accion').Value := 'H';                                      //SS-1006-NDR-20120614
              end;                                                                                             //SS-1006-NDR-20120614
              ExecProc;                                                                                        //SS-1006-NDR-20120614
              Close;                                                                                           //SS-1006-NDR-20120614
            end;                                                                                               //SS-1006-NDR-20120614

            FDocumentacionConvenio.Guardar;
            FCheckListConvenio.Guardar;
            FChekListCuenta.Guardar;
            FListObservacionConvenio.Guardar;
            //Borro los medios de envios que hacen referencia a esta persona si borre el mail
            if TieneOtrosConveniosEnvioMails and FCliente.ExisteMailABorrar then
                EliminarOtrosMediosEnvio;
      except
         on E: Exception do begin
                if E is EExtendedError then
                    raise EConvenioError.create(MSG_SAVE_ERROR, e.message + ' - ' + EExtendedError(e).DetailMessage, 0)
                else raise;
         end;
      end;
    finally
        free;
    end;
 }

    try
        FCodigoConvenio := ActualizarConvenio(VersionAnexo1);

        ActualizarMediosEnvioDocumentacion();

        FMediosEnvioPago.Guardar;

        FCuentas.Guardar(EsConvenioNativo, SeCambioBitPA, AdheridoPA);

        FBajasForzadas.Guardar;

        //ActualizarConvenioInhabilitadoPA(SeCambioInhabilitado, intMotivoInhabilitacion);     //TASK_077_JMA_20161124      Esto solo aplica a Parque Arauco

        FDocumentacionConvenio.Guardar();

        FCheckListConvenio.Guardar();

        FChekListCuenta.Guardar();

        FListObservacionConvenio.Guardar();

        //Borro los medios de envios que hacen referencia a esta persona si borre el mail
        if TieneOtrosConveniosEnvioMails and FCliente.ExisteMailABorrar then
            EliminarOtrosMediosEnvio();

    except
         on E: Exception do begin
                if E is EExtendedError then
                    raise EConvenioError.create(MSG_SAVE_ERROR, e.message + ' - ' + EExtendedError(e).DetailMessage, 0)
                else raise;
         end;
    end;
{TERMINO: TASK_077_JMA_20161124}
end;

{INICIO: TASK_077_JMA_20161124}
function TDatosConvenio.ActualizarConvenio(VersionAnexo1: Integer):Integer;
var
    spActualizarConvenio : TADOStoredProc;
begin

    try
        spActualizarConvenio := TADOStoredProc.Create(nil);
        spActualizarConvenio.Connection        := DMConnections.BaseCAC;
        spActualizarConvenio.ProcedureName     := 'CRM_CONVENIO_UPDATE';
        spActualizarConvenio.ExecuteOptions    := [eoExecuteNoRecords];

        spActualizarConvenio.Parameters.Refresh;
        spActualizarConvenio.Parameters.ParamByName('@NumeroConvenio').Value          := FNumeroConvenio;
        spActualizarConvenio.Parameters.ParamByName('@CodigoCliente').Value           := FCodigoPersona;
        spActualizarConvenio.Parameters.ParamByName('@CodigoEstadoConvenio').Value    := ESTADO_CONVENIO_VIGENTE;
        spActualizarConvenio.Parameters.ParamByName('@Usuario').Value                 := UsuarioSistema;

        case FMedioPago.MedioPago.TipoMedioPago of
            PAC     : spActualizarConvenio.Parameters.ParamByName('@TipoMedioPagoAutomatico').Value   := TPA_PAC;
            PAT     : spActualizarConvenio.Parameters.ParamByName('@TipoMedioPagoAutomatico').Value   := TPA_PAT;
            NINGUNO : spActualizarConvenio.Parameters.ParamByName('@TipoMedioPagoAutomatico').Value   := TPA_NINGUNO;
        end;

        spActualizarConvenio.Parameters.ParamByName('@CodigoDomicilioFacturacion').Value      := FCliente.Domicilios.Domicilios[FCliente.Domicilios.IndiceDomicilioFacturacion].CodigoDomicilio;
        spActualizarConvenio.Parameters.ParamByName('@CicloFacturacion').Value                := null;
        spActualizarConvenio.Parameters.ParamByName('@TipoTotalizacionDocumentosCobro').Value := null;
        spActualizarConvenio.Parameters.ParamByName('@FechaInicioVigencia').Value             := NowBase(DMConnections.BaseCAC);
        spActualizarconvenio.Parameters.ParamByName('@UbicacionFisicaCarpeta').Value          := FUbicacionFisicaCarpeta;

        (* Se controla que el CodigoPersona sea > a 0 (cero) ya que algunas interfaces envian Null, para el dato del mandante. *)
        if Assigned(FMedioPago.Mandante) and (FMedioPago.Mandante.CodigoPersona > 0) then
            spActualizarConvenio.Parameters.ParamByName('@CodigoPersonaMedioPagoAutomatico').Value       := FMedioPago.Mandante.CodigoPersona
        else
            spActualizarConvenio.Parameters.ParamByName('@CodigoPersonaMedioPagoAutomatico').Value   := NULL;

        spActualizarConvenio.Parameters.ParamByName('@CodigoCampania').Value                := null;
        spActualizarConvenio.Parameters.ParamByName('@CodigoUsuarioActualizacion').Value    := UsuarioSistema;
        spActualizarConvenio.Parameters.ParamByName('@CodigoConvenio').Value                :=CodigoConvenio;
        spActualizarConvenio.Parameters.ParamByName('@AccionRNUT').Value                    := iif(EsConvenioNativo, AccionRNUT, 0);
        spActualizarConvenio.Parameters.ParamByName('@CodigoConcesionaria').Value           :=FCodigoConcesionaria;
        spActualizarConvenio.Parameters.ParamByName('@NumeroConvenioRNUT').Value            := NumeroConvenioRNUT;
        spActualizarConvenio.Parameters.ParamByName('@RecibirInfoPorMail').Value            := RecibirInfoPorMail;
        spActualizarConvenio.Parameters.ParamByName('@FechaBajaRNUT').Value                 := iif((not Cuentas.TieneCuentasActivas), iif(FechaBajaRNUT <> NULLDATE, FechaBajaRNUT, NowBase(DMConnections.BaseCAC)), NULL);

        if MedioPago.EsCambioDatosTransBank or MedioPago.EsCambioTipoMedioPago then
            spActualizarConvenio.Parameters.ParamByName('@FechaActualizacionMedioPago').Value   := NowBase(DMConnections.BaseCAC)
        else
            spActualizarConvenio.Parameters.ParamByName('@FechaActualizacionMedioPago').Value  := NULL;

        spActualizarConvenio.Parameters.ParamByName('@ForzarConfirmado').Value              := MedioPago.ForzarConfirmado;
        spActualizarConvenio.Parameters.ParamByName('@NoGenerarIntereses').Value            := NoGenerarIntereses;
        spActualizarConvenio.Parameters.ParamByName('@Suspendido').Value                    := EsConvenioSuspendido;
        spActualizarConvenio.Parameters.ParamByName('@TipoComprobanteFiscalAEmitir').Value  := iif(FDocumentoElectronicoAImprimir = TDCBX_AUTOMATICO_COD, NULL, FDocumentoElectronicoAImprimir);
        spActualizarConvenio.Parameters.ParamByName('@AdheridoPA').Value                    := FAdheridoPA;
        spActualizarConvenio.Parameters.ParamByName('@SinCuenta').Value                     := FSinCuenta;
        spActualizarConvenio.Parameters.ParamByName('@ConvenioFacturacion').Value           := FCodigoConvenioFacturacion;
        spActualizarConvenio.Parameters.ParamByName('@CodigoTipoConvenio').Value            := FCodigoTipoConvenio;
        spActualizarConvenio.Parameters.ParamByName('@CodigoClienteFacturacion').Value      := FCodigoClienteFacturacion;
        spActualizarConvenio.Parameters.ParamByName('@VersionAnexo1').Value                 := VersionAnexo1;
        spActualizarConvenio.Parameters.ParamByName('@CodigoGrupoFacturacion').Value        := FCodigoGrupoFacturacion;     // TASK_096_MGO_20161220

        if FCodigoTipoCliente<>0 then
           spActualizarConvenio.Parameters.ParamByName('@CodigoTipoCliente').Value          := FCodigoTipoCliente;

        spActualizarConvenio.ExecProc;

        if spActualizarConvenio.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
            raise Exception.Create(spActualizarConvenio.Parameters.ParamByName('@ErrorDescription').Value );

        Result := spActualizarConvenio.Parameters.ParamByName('@CodigoConvenio').Value;

    finally
        if Assigned(spActualizarConvenio) and spActualizarConvenio.Active then
             spActualizarconvenio.Close;
        //spActualizarconvenio.free;            //TASK_108_JMA_20170214
        FreeAndNil(spActualizarConvenio);       //TASK_108_JMA_20170214
    end;
end;

procedure TDatosConvenio.ActualizarConvenioInhabilitadoPA(SeCambioInhabilitado :Boolean=False; intMotivoInhabilitacion:Integer=-1);
resourcestring
    SQL_CONCESIONARIA           = 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO()';
var
    spActualizarConvenioInhabilitado: TADOStoredProc;
begin

    try
        spActualizarConvenioInhabilitado := TADOStoredProc.Create(nil);
        spActualizarConvenioInhabilitado.Connection := DMConnections.BaseCAC;
        spActualizarConvenioInhabilitado.ProcedureName := 'ActualizarConvenioInhabilitado';
        spActualizarConvenioInhabilitado.ExecuteOptions := [eoExecuteNoRecords];

        spActualizarConvenioInhabilitado.Parameters.Refresh;
        spActualizarConvenioInhabilitado.Parameters.ParamByName('@CodigoConvenio').Value            := FCodigoConvenio;
        spActualizarConvenioInhabilitado.Parameters.ParamByName('@CodigoConcesionaria').Value       := QueryGetValueInt(DMConnections.BaseCAC, SQL_CONCESIONARIA);
        spActualizarConvenioInhabilitado.Parameters.ParamByName('@CodigoUsuario').Value             := UsuarioSistema;

        if SeCambioInhabilitado then
            spActualizarConvenioInhabilitado.Parameters.ParamByName('@Accion').Value := 'I'
        else
            spActualizarConvenioInhabilitado.Parameters.ParamByName('@Accion').Value := 'H';

        spActualizarConvenioInhabilitado.Parameters.ParamByName('@CodigoMotivoInhabilitacion').Value:= intMotivoInhabilitacion;
        spActualizarConvenioInhabilitado.Parameters.ParamByName('@DescripcionError').Value          := '';

        spActualizarConvenioInhabilitado.ExecProc;
        spActualizarConvenioInhabilitado.Close;

        if spActualizarConvenioInhabilitado.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
            raise Exception.Create(spActualizarConvenioInhabilitado.Parameters.ParamByName('@DescripcionError').Value );

    finally
        if Assigned(spActualizarConvenioInhabilitado) and spActualizarConvenioInhabilitado.Active then
             spActualizarConvenioInhabilitado.Close;
        //spActualizarConvenioInhabilitado.free;
        FreeAndNil(spActualizarConvenioInhabilitado);       //TASK_108_JMA_20170214
    end;

end;
{TERMINO: TASK_077_JMA_20161124}

{******************************** Function Header ******************************
Revision : 1
    Author : nefernandez
    Date : 20/05/2008
    Description : SS 680: Se pasa el par�metro UsuarioSistema para registrar los datos de auditor�a.
*******************************************************************************}
{Procedure Name	:
 Author 		: Nelson Droguett Sierra (nefernandez)
 Date Created	: 20-Abril-2010
 Description	: SS 680: Se pasa el par�metro UsuarioSistema para registrar los
 					datos de auditor�a.
 Parameters   	:
 Return Value   :
}
procedure TDatosConvenio.EliminarOtrosMediosEnvio;
var
    spEnviosPersonaQuitados : TAdoStoredProc;
begin
    spEnviosPersonaQuitados := TADOStoredProc.Create(nil);
    spEnviosPersonaQuitados.Connection := DMConnections.BaseCAC;
    spEnviosPersonaQuitados.ProcedureName := 'BorrarMedioMailDelRestoConveniosPersona';
    spEnviosPersonaQuitados.ExecuteOptions := [eoExecuteNoRecords];
    spEnviosPersonaQuitados.Parameters.Refresh;
    try
        with spEnviosPersonaQuitados.Parameters do begin
            ParamByName('@CodigoPersona').Value := FCodigoPersona;
            ParamByName('@Usuario').Value := UsuarioSistema;
            spEnviosPersonaQuitados.ExecProc;
{INICIO: TASK_106_JMA_20170206}
                if spEnviosPersonaQuitados.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                    raise Exception.Create(spEnviosPersonaQuitados.Parameters.ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}
        end;
    finally
        //spEnviosPersonaQuitados.Free;
        FreeAndNil(spEnviosPersonaQuitados);    //TASK_108_JMA_20170214
    end;
end;

{Procedure Name	: Imprimir
 Author 		: Nelson Droguett Sierra (pdominguez)
 Date Created	: 20-Abril-2010
 Description	: SS 733
        - Cuando el tipo de impresi�n del Convenio es tiEstadoActual se llama a
        la nueva impresi�n del Estado del Convenio.
 Parameters   	: DocumentoAImprimir: Integer; var MotivosCancelacion: TMotivoCancelacion;
    CantidadCopias : integer; FormImprimir : TObject; CuentasOriginal: TCuentasConvenio
Return Value : Boolean
}
function TDatosConvenio.Imprimir(DocumentoAImprimir: Integer;
  var MotivosCancelacion: TMotivoCancelacion; CantidadCopias : integer;
  FormImprimir : TObject; CuentasOriginal: TCuentasConvenio): Boolean;

    function TagPerdido(Patente: ShortString): Boolean;
    var
        i: Integer;
    begin
        i := 0;
        while (i < Cuentas.CantidadVehiculos) and (Trim(Cuentas.Vehiculos[i].Cuenta.Vehiculo.Patente) <> Trim(Patente)) do
            Inc(i);
        Result := (i < Cuentas.CantidadVehiculos) and Cuentas.Vehiculos[i].EsTagPerdido;
    end;


    {Procedure Name	: CargarVehiculos
     Author 		: Nelson Droguett Sierra (nefernandez)
     Date Created	: 20-Abril-2010
     Description	: SS 622: Si la cuenta esta dada de baja se asigna el valor
    correspondiente a TipoMovimiento.
     Parameters   	:
     Return Value   :
    }
    procedure CargarVehiculos( var Vehiculo: TRegCuentaABM; var TipoImpresionConvenio: TTipoImpresion);
    var
        AuxVehiculos : TRegCuentaVehiculos;
        i,x: integer;
    begin
        Cuentas.ObtenerRegVehiculosEliminados(AuxVehiculos);
        Cuentas.ObtenerRegVehiculosTagsQuitado(AuxVehiculos, True);

        SetLength(Vehiculo, length(AuxVehiculos));
        x := 0;
        for i := 0 to length(AuxVehiculos) - 1 do begin
            Vehiculo[x].Cuenta := AuxVehiculos[i];
            Vehiculo[x].TipoMovimiento := ttmBaja;
            inc(x);
        end;

        SetLength(AuxVehiculos,0);
        Cuentas.ObtenerRegVehiculosAgregados(AuxVehiculos,CuentasOriginal);
        SetLength(Vehiculo, Length(Vehiculo) + Length(AuxVehiculos));

        for i := 0 to length(AuxVehiculos) - 1 do begin
            Vehiculo[x].Cuenta := AuxVehiculos[i];
            Vehiculo[x].TipoMovimiento := ttmAlta;
            inc(x);
        end;

        SetLength(AuxVehiculos,0);
        Cuentas.ObtenerRegVehiculosActivados(AuxVehiculos);
        SetLength(Vehiculo, Length(Vehiculo) + Length(AuxVehiculos));
        for i := 0 to length(AuxVehiculos) - 1 do begin
            Vehiculo[x].Cuenta := AuxVehiculos[i];
            Vehiculo[x].TipoMovimiento := ttmActivado;
            inc(x);
        end;

        SetLength(AuxVehiculos,0);
        Cuentas.ObtenerRegVehiculosModificadosTag(AuxVehiculos);
        SetLength(Vehiculo, Length(Vehiculo) + Length(AuxVehiculos));

        for i := 0 to length(AuxVehiculos) - 1 do begin
            Vehiculo[x].Cuenta := AuxVehiculos[i];
            if (Vehiculo[x].Cuenta.EstaSuspendida) then
                Vehiculo[x].TipoMovimiento := ttmSuspendido
            else
                Vehiculo[x].TipoMovimiento := ttmModif;
            inc(x);
        end;

        SetLength(AuxVehiculos,0);
        Cuentas.ObtenerRegVehiculosTagPerdidos(AuxVehiculos);
        SetLength(Vehiculo, Length(Vehiculo) + Length(AuxVehiculos));
        for i := 0 to length(AuxVehiculos) - 1 do begin
            Vehiculo[x].Cuenta := AuxVehiculos[i];
            Vehiculo[x].TipoMovimiento := ttmTagPerdido;
            inc(x);
        end;

        SetLength(AuxVehiculos,0);
        Cuentas.ObtenerRegVehiculosModificadosTag(AuxVehiculos, True);
        SetLength(Vehiculo, Length(Vehiculo) + Length(AuxVehiculos));

        for i := 0 to length(AuxVehiculos) - 1 do begin
            Vehiculo[x].Cuenta := AuxVehiculos[i];
            Vehiculo[x].TipoMovimiento := ttmModifTag;
            inc(x);
        end;

        SetLength(AuxVehiculos,0);
        Cuentas.ObtenerRegVehiculosVehiculosRobados(AuxVehiculos, True);
        SetLength(Vehiculo, Length(Vehiculo) + Length(AuxVehiculos));

        for i := 0 to length(AuxVehiculos) - 1 do begin
            Vehiculo[x].Cuenta := AuxVehiculos[i];
            Vehiculo[x].TipoMovimiento := ttmRobado;
            inc(x);
        end;

        SetLength(AuxVehiculos, 0);
        Cuentas.ObtenerRegVehiculosVehiculosSinTag(AuxVehiculos);
        SetLength(Vehiculo, Length(Vehiculo) + Length(AuxVehiculos));

        for i := 0 to length(AuxVehiculos) - 1 do begin
            Vehiculo[x].Cuenta := AuxVehiculos[i];
            Vehiculo[x].TipoMovimiento := ttmBajaSinTag;
            inc(x);
        end;

        // Si no se imprime ningun vehiculo, sig que se imprime por modificaci�n
        // de otro datos del convenio, por ende imprime todos sus vehiculos como Vigente.

        if (x = 0) then begin
            TipoImpresionConvenio := tiEstadoActual;
            Cuentas.ObtenerRegVehiculosNOModificados(AuxVehiculos);
            SetLength(Vehiculo, Length(Vehiculo) + Length(AuxVehiculos));

            for i := 0 to length(AuxVehiculos) - 1 do begin
                Vehiculo[x].Cuenta := AuxVehiculos[i];
                if TagPerdido(Vehiculo[x].Cuenta.Vehiculo.Patente) then
                    Vehiculo[x].TipoMovimiento := ttmTagPerdido
                else
                    if (Vehiculo[x].Cuenta.FechaBajaCuenta <> NullDate) and not (vehiculo[x].Cuenta.EstaSuspendida) then 
                        Vehiculo[x].TipoMovimiento := ttmBaja
                    else if (Vehiculo[x].Cuenta.EstaSuspendida) then
                        Vehiculo[x].TipoMovimiento := ttmSuspendido
                    else
                        Vehiculo[x].TipoMovimiento := ttmVigente;
                inc(x);
            end;
        end;
    end;




    {Procedure Name	: CargarVehiculosPATFalabella
     Author 		: Nelson Droguett Sierra (lcanteros)
     Date Created	: 20-Abril-2010
     Description	: Ingreso en el array de vehiculos solamente los de
        las cuentas activas.
     Parameters   	: var Vehiculo: TRegCuentaABM
     Return Value   : None
    }
    procedure CargarVehiculosPATFalabella(var Vehiculo: TRegCuentaABM);
    var
        AuxVehiculos : TRegCuentaVehiculos;
        i,x: integer;
    begin
        Cuentas.ObtenerRegVehiculosNOModificados(AuxVehiculos);
        x := 0;
        for i := 0 to length(AuxVehiculos) - 1 do begin
            // si FechaBajaCuenta es <> nulldate puede ser que este dada de baja o suspendida
            if (AuxVehiculos[i].FechaBajaCuenta = NullDate) then begin
                SetLength(Vehiculo, Length(Vehiculo) + 1);
                Vehiculo[x].Cuenta := AuxVehiculos[i];
                if TagPerdido(Vehiculo[x].Cuenta.Vehiculo.Patente) then
                    Vehiculo[x].TipoMovimiento := ttmTagPerdido
                else
                    if (Vehiculo[x].Cuenta.EstaSuspendida) then
                        Vehiculo[x].TipoMovimiento := ttmSuspendido
                    else
                        Vehiculo[x].TipoMovimiento := ttmVigente;
                inc(x);
            end else // si esta suspendida tambien se agrega al mandato
            if (AuxVehiculos[i].EstaSuspendida) then begin
                SetLength(Vehiculo, Length(Vehiculo) + 1);
                Vehiculo[x].Cuenta := AuxVehiculos[i];
                Vehiculo[x].TipoMovimiento := ttmSuspendido;
                inc(x);
            end;
            // y si FechaBajaCuenta es <> nulldate y no esta suspendida => esta dada de baja
            // en este caso no se agrega al mandato
        end;
    end;
resourcestring
    MSG_ORDER_NOT_DEFINED = 'Orden de Servicio no definida - Impresion de Convenio';
var
   TelefonoPrincipal, TelefonoSecundario : TTipoMedioComunicacion;
   Representante1, Representante2, Representante3 : TDatosPersonales;
   Email : TTipoMedioComunicacion;

   AuxVehiculos : TRegCuentaVehiculos;
   Vehiculo : TRegCuentaABM;
   i,x : Integer;
   TipoImpresionConvenio: TTipoImpresion;
   EMailTitular: String;

begin
    Result := True;
    TipoImpresionConvenio := tiModificacion;

    case DocumentoAImprimir of
        IMPRIMIR_BAJA_CONVENIO: begin
                SetLength(AuxVehiculos,0);
                Cuentas.ObtenerRegVehiculos(AuxVehiculos);
                Cuentas.ObtenerRegVehiculosEliminados(AuxVehiculos, True);
                SetLength(Vehiculo, length(AuxVehiculos));
                x := 0;
                for i := 0 to length(AuxVehiculos) - 1 do begin
                    Vehiculo[x].Cuenta := AuxVehiculos[i];
                    Vehiculo[x].TipoMovimiento := ttmBaja;
                    inc(x);
                end;

                if Cliente.Telefonos.TieneMedioPrincipal then
                 TelefonoPrincipal := Cliente.Telefonos.MedioPrincipal
                else
                  TelefonoPrincipal.Valor := '';

                if Cliente.Telefonos.TieneMedioSecundario then
                 TelefonoSecundario := Cliente.Telefonos.MedioSecundario
                else
                  TelefonoSecundario.Valor := '';

                if Cliente.EMail.CantidadMediosComunicacion > 0 then
                 Email := Cliente.EMail.MediosComunicacion[0]
                else
                  Email.Valor := '';

              if (Cliente.Datos.Personeria = PERSONERIA_JURIDICA) then begin
                    with TPersonaJuridica(Cliente).RepresentantesLegales do begin
                        if cantidadRepresentantes > 0 then Representante1 := Representantes[0].Datos;
                        if cantidadRepresentantes > 1 then Representante2 := Representantes[1].Datos;
                        if cantidadRepresentantes > 2 then Representante3 := Representantes[2].Datos;
                    end;

                    TFormImprimirConvenio(FormImprimir).ImprimirConvenioPersonaJuridica(NumeroConvenioFormateado, TipoImpresionConvenio,
                        cliente.Datos,
                        TelefonoPrincipal, TelefonoSecundario, EMail.Valor,
                        Cliente.Domicilios.DomicilioPrincipal, Cliente.Domicilios.DomicilioFacturacion,
                        MedioPago.MedioPago, Representante1, Representante2,
                        Representante3, MediosEnvioPago.EnviaDocumentacionxMail, FechaAlta, MotivosCancelacion ,CantidadCopias,
                        Vehiculo,
                        iif(DocumentoAImprimir = IMPRIMIR_CONVENIO , PLANTILLA_CONVENIO_PERSONA_JURIDICA , PLANTILLA_BAJA_CONVENIO_JURIDICA));
              end else begin
                    TFormImprimirConvenio(FormImprimir).ImprimirConvenioPersonaNatural(NumeroConvenioFormateado,TipoImpresionConvenio, cliente.Datos,
                        TelefonoPrincipal, TelefonoSecundario, EMail.Valor,
                        Cliente.Domicilios.DomicilioPrincipal, Cliente.Domicilios.DomicilioFacturacion,
                        MedioPago.MedioPago, Vehiculo , MediosEnvioPago.EnviaDocumentacionxMail, FechaAlta, MotivosCancelacion ,CantidadCopias,
                        iif(DocumentoAImprimir = IMPRIMIR_CONVENIO, PLANTILLA_CONVENIO_PERSONA_NATURAL,PLANTILLA_BAJA_CONVENIO_NATURAL));
               end;
               FNroAnexoEmitido := TFormImprimirConvenio(FormImprimir).NroAnexoEmitido;
        end;

        IMPRIMIR_CONVENIO: begin
                CargarVehiculos(Vehiculo, TipoImpresionConvenio);
                if Cliente.Telefonos.TieneMedioPrincipal then
                 TelefonoPrincipal := Cliente.Telefonos.MedioPrincipal
                else
                  TelefonoPrincipal.Valor := '';

                if Cliente.Telefonos.TieneMedioSecundario then
                 TelefonoSecundario := Cliente.Telefonos.MedioSecundario
                else
                  TelefonoSecundario.Valor := '';

                if Cliente.EMail.CantidadMediosComunicacion > 0 then
                 Email := Cliente.EMail.MediosComunicacion[0]
                else
                  Email.Valor := '';

                if ConvenioTieneMotosNuevas then TFormImprimirConvenio(FormImprimir).ImprimirContratoMoto(NumeroConvenioFormateado,Vehiculo,Cliente.Datos,TiNuevas, MotivosCancelacion,CantidadCopias);
                if (DocumentoAImprimir = IMPRIMIR_CONVENIO) and (TipoImpresionConvenio = tiEstadoActual) then
                    TFormImprimirConvenio(FormImprimir).ImprimirEstadoConvenio
                else begin
                    if (Cliente.Datos.Personeria = PERSONERIA_JURIDICA) then begin
                        with TPersonaJuridica(Cliente).RepresentantesLegales do begin
                            if cantidadRepresentantes > 0 then Representante1 := Representantes[0].Datos;
                            if cantidadRepresentantes > 1 then Representante2 := Representantes[1].Datos;
                            if cantidadRepresentantes > 2 then Representante3 := Representantes[2].Datos;
                        end;

                        TFormImprimirConvenio(FormImprimir).ImprimirConvenioPersonaJuridica(NumeroConvenioFormateado, TipoImpresionConvenio,
                            cliente.Datos,
                            TelefonoPrincipal, TelefonoSecundario, EMail.Valor,
                            Cliente.Domicilios.DomicilioPrincipal, Cliente.Domicilios.DomicilioFacturacion,
                            MedioPago.MedioPago, Representante1, Representante2,
                            Representante3, MediosEnvioPago.EnviaDocumentacionxMail, FechaAlta, MotivosCancelacion ,CantidadCopias,
                            Vehiculo ,
                            iif(DocumentoAImprimir = IMPRIMIR_CONVENIO , PLANTILLA_CONVENIO_PERSONA_JURIDICA , PLANTILLA_BAJA_CONVENIO_JURIDICA));
                  end else begin
                        TFormImprimirConvenio(FormImprimir).ImprimirConvenioPersonaNatural(NumeroConvenioFormateado, TipoImpresionConvenio, Cliente.Datos,
                            TelefonoPrincipal, TelefonoSecundario, EMail.Valor,
                            Cliente.Domicilios.DomicilioPrincipal, Cliente.Domicilios.DomicilioFacturacion,
                            MedioPago.MedioPago, Vehiculo , MediosEnvioPago.EnviaDocumentacionxMail, FechaAlta, MotivosCancelacion ,CantidadCopias,
                            iif(DocumentoAImprimir = IMPRIMIR_CONVENIO, PLANTILLA_CONVENIO_PERSONA_NATURAL,PLANTILLA_BAJA_CONVENIO_NATURAL));
                   end;
                   FNroAnexoEmitido := TFormImprimirConvenio(FormImprimir).NroAnexoEmitido;
                end;
        end;
        REIMPRIMIR_MOTOS: begin

                CargarVehiculos(Vehiculo, TipoImpresionConvenio);
                if Cliente.Telefonos.TieneMedioPrincipal then
                 TelefonoPrincipal := Cliente.Telefonos.MedioPrincipal
                else
                  TelefonoPrincipal.Valor := '';

                if Cliente.Telefonos.TieneMedioSecundario then
                 TelefonoSecundario := Cliente.Telefonos.MedioSecundario
                else
                  TelefonoSecundario.Valor := '';

                if Cliente.EMail.CantidadMediosComunicacion > 0 then
                 Email := Cliente.EMail.MediosComunicacion[0]
                else
                  Email.Valor := '';
               TFormImprimirConvenio(FormImprimir).ImprimirContratoMoto(NumeroConvenioFormateado,Vehiculo,Cliente.Datos,TiReimpresion, MotivosCancelacion,CantidadCopias);
               FNroAnexoEmitido := TFormImprimirConvenio(FormImprimir).NroAnexoEmitido;
        end;
        IMPRIMIR_MANDATO: begin

            if MedioPago.CodigoMedioPago = TPA_PAT then begin

                // Obtenemos los vehiculos que se van a imprimir en el mandato
                if MedioPago.MedioPago.PAT.TipoTarjeta = TARJETA_CMR then begin
                    CargarVehiculosPATFalabella(Vehiculo);
                    if Length(Vehiculo) = 0 then
                        CargarVehiculos(Vehiculo, TipoImpresionConvenio);
                end;

                EMailTitular := '';
                if Cliente.EMail.CantidadMediosComunicacion > 0 then
                    EMailTitular := Cliente.EMail.MediosComunicacion[0].Valor;

                TFormImprimirConvenio(FormImprimir).ImprimirMandatoPAT(NumeroConvenioFormateado,
                                            MedioPago.MedioPago,
                                            MedioPago.Mandante.Datos,
                                            Cliente.Datos,
                                            Cliente.Domicilios.ObtenerDomicilioPrincipal,
                                            EMailTitular,
                                            Vehiculo,
                                            MotivosCancelacion,
                                            CantidadCopias);
            end else if MedioPago.CodigoMedioPago = TPA_PAC then TFormImprimirConvenio(FormImprimir).ImprimirMandatoPAC(NumeroConvenioFormateado,MedioPago.MedioPago,MedioPago.Mandante.Datos,MotivosCancelacion,CantidadCopias);

            FNroAnexoEmitido := TFormImprimirConvenio(FormImprimir).NroAnexoEmitido;
        end;
        else begin // else del case
            raise Exception.Create(MSG_ORDER_NOT_DEFINED);
        end;
    end;
end;



{ TMediosPagoConvenio }

{Procedure Name	: TMediosPagoConvenio.Create
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: Constructo de Medios de Pago.
 Parameters   	: CodigoConvenio: Integer; CodigoPersonaMedioPago : integer; const TipoMedioPago: Integer; const CodigoDomicilioFacturacion : Integer
 Return Value   : None
}
procedure TMediosPagoConvenio.AgregarMandate;
begin
  if not assigned(FMandante) then
     FMandante:=TMandante.Create;
end;

{Procedure Name	: TMediosPagoConvenio.Create
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
constructor TMediosPagoConvenio.Create(CodigoConvenio: Integer; CodigoPersonaMedioPago : integer; const  TipoMedioPago: Integer);
resourcestring
    MSG_CREATE_ERROR = 'Error al crear medio de pago';
begin
    FCambioMedioPago := False;
    FCambioDatosTransBank := False;
    FCodigoConvenio := CodigoConvenio;
    FMandante := nil;
    FForzarConfirmado := False;
    //FCambioTipoMedioPago := False;
    with TADOStoredProc.Create(nil) do
    try
        try
            Connection:=DMConnections.BaseCAC;
            ProcedureName:='ObtenerDatosMedioPagoAutomatico';
            with Parameters do begin
                  Refresh;
                  ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                  ParamByName('@TipoMediodePago').Value := TipoMedioPago;
            end;
            Open;

            FConfirmado := FieldByName('Confirmado').AsBoolean;
            FOrigenMandato := UpperCase(FieldByName('OrigenMandato').AsString);
            case FieldByName('TipoMedioPago').AsInteger of
                  TPA_PAT: begin
                          //Creo el mandate del medio de pago.
                          if CodigoPersonaMedioPago <> 0 then
                              FMandante:=TMandante.Create(CodigoPersonaMedioPago)
                          else
                              FMandante:=TMandante.Create;

                          //Completo la informacion Realicionada con el medio de pago
                          FMedioPago.TipoMedioPago            := PAT;
                          FMedioPago.PAT.TipoTarjeta          := FieldByName('CodigoTipoTarjetaCredito').AsInteger;
                          FMedioPago.PAT.EmisorTarjetaCredito := FieldByName('CodigoEmisorTarjetaCredito').AsInteger;
                          FMedioPago.PAT.NroTarjeta           := Trim(FieldByName('NumeroTarjetaCredito').AsString);
                          FMedioPago.PAT.FechaVencimiento     := Trim(FieldByName('FechaVencimiento').AsString);
                          FMedioPago.PAT.Confirmado           := FieldByName('Confirmado').AsBoolean;

                          FMedioPago.Nombre := Mandante.Datos.Nombre;
                          if Mandante.Telefonos.TieneMedioPrincipal then begin
                              FMedioPago.Telefono := Mandante.Telefonos.MedioPrincipal.Valor;
                              FMedioPago.CodigoArea := Mandante.Telefonos.MedioPrincipal.CodigoArea;
                          end else begin
                              FMedioPago.Telefono := '';
                              FMedioPago.CodigoArea := 0;
                          end;
                          FMedioPago.CodigoPersona := Mandante.Datos.CodigoPersona;

                          FCodigoMedioPago:=TPA_PAT;
                  end;
                  TPA_PAC: begin
                          if CodigoPersonaMedioPago <> 0 then
                              FMandante:=TMandante.Create(CodigoPersonaMedioPago)
                          else
                              FMandante:=TMandante.Create;

                          //En realidad un mandante nunca tiene domicilio o mail
                          FMedioPago.TipoMedioPago := PAC;
                          FMedioPago.PAC.TipoCuenta         := FieldByName('CodigoTipoCuentaBancaria').AsInteger;
                          FMedioPago.PAC.CodigoBanco        := FieldByName('CodigoBanco').AsInteger;
                          FMedioPago.PAC.NroCuentaBancaria  := Trim(FieldByName('NroCuentaBancaria').AsString);
                          FMedioPago.PAC.Sucursal           := Trim(FieldByName('Sucursal').AsString);
                          FMedioPago.PAC.Confirmado           := FieldByName('Confirmado').AsBoolean;

                          FMedioPago.Nombre := Mandante.Datos.Nombre;
                          if Mandante.Telefonos.TieneMedioPrincipal then begin
                              FMedioPago.Telefono := Mandante.Telefonos.MedioPrincipal.Valor;
                              FMedioPago.CodigoArea := Mandante.Telefonos.MedioPrincipal.CodigoArea;
                          end else begin
                              FMedioPago.Telefono := '';
                              FMedioPago.CodigoArea := 0;
                          end;
                          FMedioPago.CodigoPersona := Mandante.Datos.CodigoPersona;

                          FCodigoMedioPago:=TPA_PAC;
                  end;
                  TPA_NINGUNO : begin
                          FMedioPago.TipoMedioPago := NINGUNO;
                          FCodigoMedioPago:=TPA_NINGUNO;
                  end;
            end;
        except
            on E: Exception do begin
              if E.ClassType = Exception then
                raise EMedioPagoError.Create(MSG_CREATE_ERROR, e.Message, ERRORCREACIONMEDIOPAGO)
              else raise;
            end;
        end;
    finally
        free;
    end;
end;


{Procedure Name	: TMediosPagoConvenio.Destroy
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
destructor TMediosPagoConvenio.Destroy;
begin
  if Assigned(Mandante) then Mandante.Free;
  inherited;
end;

{Procedure Name	: TMediosPagoConvenio.Guardar
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: Guarda el medio de pago.
 Parameters   	:
 Return Value   :
}
function TMediosPagoConvenio.GetTieneMandante: Boolean;
begin
    result := Assigned(FMandante);
end;

{Procedure Name	: TMediosPagoConvenio.Guardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TMediosPagoConvenio.Guardar;
resourcestring
    MSG_SAVE_ERROR = 'Error al grabar medio de pago.';
var
 CodigoCliente : integer;
begin
    with TADOStoredProc.Create(nil) do
    try
        try
            Connection      := DMConnections.BaseCAC;
            ExecuteOptions  := [eoExecuteNoRecords];
            //Guardo el medio de pago anterior del convenio
            ProcedureName := 'InsertarMedioDePagoDesactivado';
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
            Parameters.ParamByName('@OrigenMandato').Value := 'C';
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := NULL;
            Parameters.ParamByName('@BajaUniverso').Value := NULL;
            Parameters.ParamByName('@CodigoRespuestaMandato').Value := NULL;
            Parameters.ParamByName('@CodigoRespuestaRendicion').Value := NULL;
            Parameters.ParamByName('@Motivo').Value := 'Eliminado por el CAC';
            ExecProc;
            //Close;

            case FMedioPago.TipoMedioPago of
                PAC: begin
                      ProcedureName := 'ActualizarConvenioMedioPagoAutomaticoPAC';
                      Parameters.Refresh;
                      Parameters.ParamByName('@CodigoConvenio').Value           := FCodigoConvenio;
                      Parameters.ParamByName('@CodigoTipoCuentaBancaria').Value := FMedioPago.PAC.TipoCuenta;
                      Parameters.ParamByName('@CodigoBanco').Value              := FMedioPago.PAC.CodigoBanco;
                      Parameters.ParamByName('@NroCuentaBancaria').Value        := FMedioPago.PAC.NroCuentaBancaria;
                      Parameters.ParamByName('@Sucursal').Value                 := iif(FMedioPago.PAC.Sucursal = EmptyStr, Null, FMedioPago.PAC.Sucursal);
                      ExecProc;
                      //Close;

                      CodigoCliente := QueryGetValueInt(DMConnections.BaseCAC,
                        'select CodigoCliente from convenio  WITH (NOLOCK) where CodigoConvenio = ' + InttoStr(FCodigoConvenio));
                      if (CodigoCliente <> Mandante.CodigoPersona) then Mandante.Guardar;
                end;
                PAT: begin
                      ProcedureName := 'ActualizarConvenioMedioPagoAutomaticoPAT';
                      Parameters.Refresh;
                      Parameters.ParamByName('@CodigoConvenio').Value           := FCodigoConvenio;
                      Parameters.ParamByName('@CodigoTipoTarjetaCredito').Value := FMedioPago.PAT.TipoTarjeta;
                      Parameters.ParamByName('@NumeroTarjetaCredito').Value     := FMedioPago.PAT.NroTarjeta;
                      Parameters.ParamByName('@FechaVencimiento').Value         := FMedioPago.PAT.FechaVencimiento;
                      Parameters.ParamByName('@CodigoEmisorTarjetaCredito').Value   := FMedioPago.PAT.EmisorTarjetaCredito;
                      ExecProc;
                      //Close;

                      CodigoCliente := QueryGetValueInt(DMConnections.BaseCAC,
                            'select CodigoCliente from convenio  WITH (NOLOCK) where CodigoConvenio = ' + InttoStr(FCodigoConvenio));
                      if (CodigoCliente <> Mandante.CodigoPersona) then Mandante.Guardar;

                end;
                NINGUNO : begin
                      //En ActualizarConvenioMedioPagoAutomaticoPAC y ActualizarConvenioMedioPagoAutomaticoPAT borra de existir la contraparte
                      //Entonces en ninguna debe borrar las dos.
                      QueryExecute(DMConnections.BaseCAC,
                            'DELETE FROM ConvenioMedioPagoAutomaticoPAT WHERE CodigoConvenio = ' + InttoStr(FCodigoConvenio));
                      QueryExecute(DMConnections.BaseCAC,
                            'DELETE FROM ConvenioMedioPagoAutomaticoPAC WHERE CodigoConvenio = ' + InttoStr(FCodigoConvenio));
                end;
            end;

        except
            on E: Exception do begin
                  if E.ClassType = Exception then
                    raise EMedioPagoError.create(MSG_SAVE_ERROR, e.message, ERRORGRABACIONMEDIOPAGO)
                  else raise;
            end;
        end;
    finally
        free;
    end;
end;


{Procedure Name	: TMediosPagoConvenio.ObtenerMandante
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
function TMediosPagoConvenio.ObtenerMandante: TMandante;
resourcestring
    MSG_NOT_EXISTS = 'No Existe Mandante.';
begin
  if assigned(FMandante) then result := FMandante
  else raise EMedioPagoError.create(MSG_NOT_EXISTS,'',0);
end;

function TMediosPagoConvenio.ObtenerMedioPago: TDatosMediosPago;
begin
      result := FMedioPago;
end;


{Procedure Name	: TMediosPagoConvenio.SetTipoMedioPago
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TMediosPagoConvenio.SetTipoMedioPago(Value: TTiposMediosPago);
begin
//    if Value <> MedioPago.TipoMedioPago then FCambioTipoMedioPago := True; // Sete la variable de Cambio de Medio de Pago
    if Value <> MedioPago.TipoMedioPago then FCambioMedioPago := True; // Sete la variable de Cambio de Medio de Pago

    if EsCambioTipoMedioPAgo then begin
        ForzarConfirmado := False; // Si cambio el medio Tipo de Medio de Pago, Saco el Forzado.
        FOrigenMandato := 'C'; // Si cambio, seguro que el nuevo es del CAC
    end;

    FTipoMedioPago := Value;
    FMedioPago.TipoMedioPago := Value;
    case Value of
        PAC:  FCodigoMedioPago := TPA_PAC;
        PAT:  FCodigoMedioPago := TPA_PAT;
        NINGUNO: FCodigoMedioPago := TPA_NINGUNO;
    end;

    if (FMedioPago.TipoMedioPago = NINGUNO) then begin         // SS_628_PDO_20120203
        if  Assigned(FMandante) then FreeAndNil(FMandante);    // SS_628_PDO_20120203
    end                                                        // SS_628_PDO_20120203
    else  AgregarMandate;                                      // SS_628_PDO_20120203

end;


{ TDatosPersona }
{******************************** Function Header ******************************
Procedure Name	: TDatosPersona.Create
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: Constructor de Persona.
 Parameters   	: CodigoPersona: integer
 Return Value   : None
 
Revision 1:
    Author : mpiazza
    Date   : 19/10/2009
    Description : SS 787
        - Se modificaron/a�adieron los siguientes datos ; RecibeClavePorMail
*******************************************************************************}
constructor TDatosPersona.Create(CodigoPersona: integer);
resourcestring
    MSG_CREATE_PERSON_ERROR = 'Error al crear la persona.';
    MSG_CODE_ERROR = 'Debe especificar un C�digo de Persona';
begin
    FEsNuevo := False;
    FEsModificado := False;
    FModificoDatosPrincipales := False;

    FCodigoPersona := CodigoPersona;
    with TADOStoredProc.Create(nil) do
    try
        try
            Connection      := DMConnections.BaseCAC;
            ProcedureName   := 'ObtenerDatosPersona';
            with Parameters do begin
                Refresh;
                if FCodigoPersona > 0 then ParamByName('@CodigoPersona').Value := FCodigoPersona
                else begin
                    raise EPersonaError.Create(MSG_CREATE_PERSON_ERROR, MSG_CODE_ERROR, ERRORCREACIONMEDIOPAGO);
                end;
            end;
            Open;

            if RecordCount > 0 then begin

                //Carga los datos personales
                with FDatos do begin
                    Personeria := (FieldByName('Personeria').AsString + ' ')[1];
                    if Personeria = PERSONERIA_JURIDICA then begin
                        RazonSocial     := Trim(FieldByName('Apellido').AsString);
                        Apellido        := Trim(FieldByName('ApellidoContactoComercial').AsString);
                        ApellidoMaterno := Trim(FieldByName('ApellidoMaternoContactoComercial').AsString);
                        Nombre          := Trim(FieldByName('NombreContactoComercial').AsString);
                        Sexo            := (FieldByName('SexoContactoComercial').AsString + ' ')[1];
                        //REV.21 Giro                := Trim(FieldByName('Giro').AsString);
                    end else begin
                        RazonSocial     := EmptyStr;
                        Apellido        := Trim(FieldByName('Apellido').AsString);
                        ApellidoMaterno := Trim(FieldByName('ApellidoMaterno').AsString);
                        Nombre          := Trim(FieldByName('Nombre').AsString);
                        Sexo            := (FieldByName('Sexo').AsString + ' ')[1];
                    end;
                    RecibeClavePorMail  :=  FieldByName('RecibeClavePorMail').asboolean;
                    Giro			:= Trim(FieldByName('Giro').AsString);		//REV.21
                    CodigoPersona   := FieldByName('CodigoPersona').AsInteger;
                    TipoDocumento   := Trim(FieldByName('CodigoDocumento').Value);
                    NumeroDocumento := Trim(FieldByName('NumeroDocumento').Value);
                    InactivaDomicilioRNUT    := FieldByName('InactivaDomicilioRNUT').asboolean ;
                    InactivaMedioComunicacionRNUT := FieldByName('InactivaMediosComunicacionRNUT').AsBoolean ;
                    if FieldByName('FechaNacimiento').IsNull then FechaNacimiento := NullDate
                    else FechaNacimiento := FieldByName('FechaNacimiento').AsDateTime;

                    CodigoActividad := iif(FieldByName('CodigoActividad').IsNull, -1, FieldByName('CodigoActividad').AsInteger);
                    LugarNacimiento := Trim(FieldByName('LugarNacimiento').AsString);

                    if FieldByName('Password').IsNull then Password := EmptyStr
                    else Password := Trim(FieldByName('Password').Value);

                    if FieldByName('Pregunta').IsNull then Pregunta := EmptyStr
                    else Pregunta := Trim(FieldByName('Pregunta').AsString);

                    if FieldByName('Respuesta').IsNull then Respuesta := EmptyStr
                    else Respuesta := Trim(FieldByName('Respuesta').AsString);
                    if FieldByName('TipoCliente').IsNull then TipoCliente := 0
                    else TipoCliente := FieldByName('TipoCliente').AsInteger;

                    if FieldByName('Semaforo1').IsNull then Semaforo1 := 0
                    else Semaforo1 := FieldByName('Semaforo1').AsInteger;

                    if FieldByName('Semaforo3').IsNull then Semaforo3 := 0
                    else Semaforo3 := FieldByName('Semaforo3').AsInteger;

                    InicializarNotificacionesTAGs := FieldByName('InicializarNotificacionesTAGs').AsBoolean;    // SS_960_PDO_20110608
                    TieneNotificacionesTAGs       := FieldByName('TieneNotificacionesTAGs').AsBoolean;          // SS_960_PDO_20110608
                end;
            end else begin
                raise EExtendedError.Create(MSG_CREATE_PERSON_ERROR, EmptyStr, 0);
            end;
        except
              On E: Exception do begin
                 if E.ClassType = Exception then
                  raise EPersonaError.Create(MSG_CREATE_PERSON_ERROR, E.Message, 0)
                 else raise;
              end;
       end;
    finally
        close;
        free;
    end;

end;


{Procedure Name	: TDatosPersona.Destroy
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: Destructor de la Personas.
 Parameters   	:
 Return Value   :
}
destructor TDatosPersona.Destroy;
begin
  inherited;
end;



{Procedure Name	: TDatosPersona.ObtenerDatosPersonales
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: 
 Parameters   	:
 Return Value   :
}
function TDatosPersona.ObtenerDatosPersonales: TDatosPersonales;
begin
    result := Datos;
end;


{******************************** Function Header ******************************
Procedure Name	: TDatosPersona.Guardar
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: Guarda los Datos de Personas y sus datos dependientes.
 Parameters   	:
 Return Value   :

Revision : 1
    Author : mpiazza
    Date : 16/10/2009
    Description : ss-787 se agrega el campo  RecibeClavePorMail
*******************************************************************************}
procedure TDatosPersona.Guardar;
resourcestring
    MSG_SAVE_PERSON_ERROR = 'Error guardando datos de la persona';
begin
    if EsModificado then begin
        with TADOStoredProc.Create(nil) do
        try
            try
                // Si no tengo los datos obligatorios no guardo, esto es por el caso de los mandantes de las interfaces.
                if ((Datos.Personeria = PERSONERIA_FISICA) and (Datos.Apellido <> EmptyStr)
                       and (Datos.Nombre <> EmptyStr) and (Datos.TipoDocumento <> EmptyStr)
                       and (Datos.NumeroDocumento <> EmptyStr)) or // Caso de Persona Fisica
                   ((Datos.Personeria = PERSONERIA_JURIDICA) and (Datos.RazonSocial <> EmptyStr)) // Caso de persona Juridica
                    then begin

                   Connection      := DMConnections.BaseCAC;
                   ProcedureName   := 'ActualizarDatosPersona';
                   ExecuteOptions  := [eoExecuteNoRecords];
                   with Parameters, Datos do begin
                       Refresh;
                       ParamByName('@Personeria').Value:= Personeria;
                       if Personeria = PERSONERIA_JURIDICA then begin
                           ParamByName('@ApellidoContactoComercial').Value         := Apellido;
                           ParamByName('@ApellidoMaternoContactoComercial').Value  := iif(ApellidoMaterno = EmptyStr, Null, ApellidoMaterno);
                           ParamByName('@NombreContactoComercial').Value           := Nombre;
                           ParamByName('@SexoContactoComercial').Value             := Sexo;
                           ParamByName('@Apellido').Value                          := RazonSocial;
                           ParamByName('@ApellidoMaterno').Value                   := Null;
                           ParamByName('@Nombre').Value                            := Null;
                           ParamByName('@Sexo').Value                              := Null;
                       end else begin
                           ParamByName('@Apellido').Value                          := Apellido;
                           ParamByName('@ApellidoMaterno').Value                   := iif(ApellidoMaterno = EmptyStr, Null, ApellidoMaterno);
                           ParamByName('@Nombre').Value                            := Nombre;
                           ParamByName('@Sexo').Value                              := iif(Trim(Sexo) = EmptyStr, Null, Sexo);
                           ParamByName('@ApellidoContactoComercial').Value         := Null;
                           ParamByName('@ApellidoMaternoContactoComercial').Value  := Null;
                           ParamByName('@NombreContactoComercial').Value           := Null;
                           ParamByName('@SexoContactoComercial').Value             := Null;
                       end;
                       ParamByName('@Giro').Value				:= Giro;
                       ParamByName('@RecibeClavePorMail').Value     := RecibeClavePorMail; //REV.30 SS-787
                       ParamByName('@CodigoPersona').Value     := CodigoPersona;
                       ParamByName('@CodigoDocumento').Value   := TipoDocumento;
                       ParamByName('@NumeroDocumento').Value   := NumeroDocumento;
                       ParamByName('@LugarNacimiento').Value   := iif(Trim(LugarNacimiento) = EmptyStr, Null, Trim(LugarNacimiento));
                       ParamByName('@FechaNacimiento').Value   := iif(FechaNacimiento = NullDate, Null, FechaNacimiento);
                       ParamByName('@CodigoActividad').Value           := CodigoActividad;
                       ParamByName('@EmailContactoComercial').Value    := Null;
                       ParamByName('@CodigoSituacionIVA').Value        := Null;
                       ParamByName('@Password').Value  := Password;
                       ParamByName('@Pregunta').Value  := Pregunta;
                       ParamByName('@Respuesta').Value := Respuesta;
                       ParamByName('@InactivaDomicilioRNUT').Value := InactivaDomicilioRNUT;
                       ParamByName('@InactivaMedioComunicacionRNUT').Value := InactivaMedioComunicacionRNUT;
                       if TipoCliente = 0 then // revision 15
                           ParamByName('@TipoCliente').Value := null
                       else
                           ParamByName('@TipoCliente').Value := TipoCliente;

                       if Semaforo1 = 0 then
                           ParamByName('@Semaforo1').Value := null
                       else
                           ParamByName('@Semaforo1').Value := Semaforo1;

                       if Semaforo3 = 0 then 
                           ParamByName('@Semaforo3').Value := null
                       else
                           ParamByName('@Semaforo3').Value := Semaforo3;
                       ParamByName('@CodigoUsuario').Value := UsuarioSistema;         //SS_1419_NDR_20151130
                   end;
                   ExecProc;
                   FCodigoPersona := Parameters.ParamByName('@CodigoPersona').Value;
                end;
            except
                  On E: Exception do begin
                     if E.ClassType = Exception then
                      raise EPersonaError.Create(MSG_SAVE_PERSON_ERROR, E.Message, 0)
                     else raise;
                  end;
            end;
        finally
            Close;
            Free;
        end;
    end;
end;

{Procedure Name	: TDatosPersona.SetGiro
 Author 		: Nelson Droguett Sierra (mbecerra)
 Date Created	: 20-Abril-2010
 Description	: (Ref SS 833) Cambia el Giro de la clase FDatos
 Parameters   	:
 Return Value   :
}
procedure TDatosPersona.SetGiro(Giro: string);
begin
	FDatos.Giro := Giro;
    FEsModificado := True;
end;

{Procedure Name	: TDatosPersona.Create
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
constructor TDatosPersona.Create;
begin
    Inherited;
    FCodigoPersona := -1;
    FDatos.CodigoPersona := -1;
    FEsNuevo := True;
    FEsModificado := False;
    FModificoDatosPrincipales := False
end;




{Procedure Name	: TDatosConvenio.SetUbicacionFisicaCarpeta
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:const Value: String
 Return Value   :
}
procedure TDatosConvenio.SetUbicacionFisicaCarpeta(const Value: String);
resourcestring
    MSG_LENGTH_ERROR = 'La Ubicaci�n F�sica de la Carpeta no puede exceder los 15 caracteres.';
begin
     if Length(Value) > 15 then raise EConvenioError.Create(MSG_LENGTH_ERROR,'',0);
     FUbicacionFisicaCarpeta := Value;
end;

function TDatosConvenio.GetFechaActualizacionMedioPago: TDateTime;              //SS_1068_NDR_20120910
begin                                                                           //SS_1068_NDR_20120910
    Result := FFechaActualizacionMedioPago;                                     //SS_1068_NDR_20120910
end;                                                                            //SS_1068_NDR_20120910

function TDatosConvenio.GetFechaAlta: TDateTime;
begin
    Result := FFechaAlta;
end;

{Procedure Name	: TDatosConvenio.GetDescripcionConcesionaria
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : String
}
function TDatosConvenio.GetDescripcionConcesionaria: String;
begin
    Result := Trim(QueryGetValue(DMConnections.BaseCAC,'SELECT Descripcion FROM Concesionarias  WITH (NOLOCK) WHERE CodigoConcesionaria = ' + IntToStr(FCodigoConcesionaria)));
end;

{Procedure Name	:  TDatosConvenio.GetEsConvenioCN
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
//function TDatosConvenio.GetEsConvenioCN: Boolean;                             //SS_1147_MCA_20140408
//begin                                                                         //SS_1147_MCA_20140408
//    //Result := CodigoConcesionaria = CODIGO_CN;                              //SS_1147_MCA_20140408
//end;                                                                          //SS_1147_MCA_20140408

function TDatosConvenio.GetEsConvenioNativo: Boolean;                           //SS_1147_MCA_20140408
begin                                                                           //SS_1147_MCA_20140408
    Result := CodigoConcesionaria = ObtenerCodigoConcesionariaNativa;			//SS_1147_MCA_20140408
end;                                                                            //SS_1147_MCA_20140408

{Procedure Name	: TDatosConvenio.SetRecibirInfoPorMail
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Boolean
 Return Value   :
}
procedure TDatosConvenio.SetRecibirInfoPorMail(const Value: Boolean);
begin
  FRecibirInfoPorMail := Value;
end;

{Procedure Name	: TDatosConvenio.SetNumeroTurno
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: longint
 Return Value   :
}
procedure TDatosConvenio.SetNumeroTurno(const Value: longint);
begin
  FNumeroTurno := Value;
  if Assigned(Cuentas) then Cuentas.NumeroTurno := Value;
end;

{Procedure Name	: TDatosConvenio.ObtenerCambios
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
function TDatosConvenio.ObtenerCambios(
  ConvenioModificado: TDatosConvenio): Boolean;

    {Procedure Name	: DomiciliosIguales
     Author 		: Nelson Droguett Sierra ()
     Date Created	: 20-Abril-2010
     Description	:
     Parameters   	: DomicilioModificado,DomicilioOriginal:TDatosDomicilio
     Return Value   : Boolean
    }
    function DomiciliosIguales(DomicilioModificado,DomicilioOriginal:TDatosDomicilio):Boolean;
    begin
    result :=   ( (DomicilioModificado.CodigoCalle = DomicilioOriginal.CodigoCalle) or
                  ((DomicilioModificado.CodigoCalle <> DomicilioOriginal.CodigoCalle) and
                   (Trim(DomicilioModificado.CalleDesnormalizada) = Trim(DomicilioOriginal.CalleDesnormalizada)))
                )
                and
                (Trim(DomicilioModificado.NumeroCalleSTR) = Trim(DomicilioOriginal.NumeroCalleSTR)) and
                (Trim(DomicilioModificado.CodigoRegion) = Trim(DomicilioOriginal.CodigoRegion)) and
                (Trim(DomicilioModificado.CodigoPostal) = Trim(DomicilioOriginal.CodigoPostal)) and
                (Trim(DomicilioModificado.Detalle) = Trim(DomicilioOriginal.Detalle)) and
                (Trim(DomicilioModificado.Dpto) = Trim(DomicilioOriginal.Dpto)) and  //Revision 31
                (Trim(DomicilioModificado.CodigoComuna) = Trim(DomicilioOriginal.CodigoComuna));

    end;

    {Procedure Name	: ObtenerCodigoOperacionRepLegal
     Author 		: Nelson Droguett Sierra ()
     Date Created	: 20-Abril-2010
     Description	:
     Parameters   	: NroRepresentate: Integer; Accion: Integer; EsConvenioSinCuenta: Boolean
     Return Value   : Integer
    }
    function ObtenerCodigoOperacionRepLegal(NroRepresentate: Integer; Accion: Integer; EsConvenioSinCuenta: Boolean): Integer;
    resourcestring
        MSG_INVALID_ACTION = 'Accion no valida';
        MSG_INVALID_REP = 'Nro rep no valido';
    begin
        case NroRepresentate of
            0: begin
                case Accion of
                    1:  if not EsConvenioSinCuenta then
                            Result :=  TOperacionConvenio[tocALTA_REP_LEGAL_1].CodigoTipoOperacionConvenio
                        else Result := TOperacionConvenio[tocALTA_SIN_CUENTA_REP_LEGAL_1].CodigoTipoOperacionConvenio;
                    2: Result := TOperacionConvenio[tocMODIF_REP_LEGAL_1].CodigoTipoOperacionConvenio;
                    3: Result := TOperacionConvenio[tocBAJA_REP_LEGAL_1].CodigoTipoOperacionConvenio;
                else raise Exception.Create(MSG_INVALID_ACTION);
                end;
            end;
            1: begin
                case Accion of
                    1: if not EsConvenioSinCuenta then
                            Result := TOperacionConvenio[tocALTA_REP_LEGAL_2].CodigoTipoOperacionConvenio
                       else Result := TOperacionConvenio[tocALTA_SIN_CUENTA_REP_LEGAL_2].CodigoTipoOperacionConvenio;
                    2: Result := TOperacionConvenio[tocMODIF_REP_LEGAL_2].CodigoTipoOperacionConvenio;
                    3: Result := TOperacionConvenio[tocBAJA_REP_LEGAL_2].CodigoTipoOperacionConvenio;
                else raise Exception.Create(MSG_INVALID_ACTION);
                end;
            end;
            2: begin
                case Accion of
                    1: if not EsConvenioSinCuenta then
                            Result := TOperacionConvenio[tocALTA_REP_LEGAL_3].CodigoTipoOperacionConvenio
                        else Result := TOperacionConvenio[tocALTA_SIN_CUENTA_REP_LEGAL_3].CodigoTipoOperacionConvenio;
                    2: Result := TOperacionConvenio[tocMODIF_REP_LEGAL_3].CodigoTipoOperacionConvenio;
                    3: Result := TOperacionConvenio[tocBAJA_REP_LEGAL_3].CodigoTipoOperacionConvenio;
                else raise Exception.Create(MSG_INVALID_ACTION);
                end;
            end;
        else raise Exception.Create(MSG_INVALID_REP);
        end;
    end;

    {Procedure Name	: VehiculoOrignalSupendido
     Author 		: Nelson Droguett Sierra ()
     Date Created	: 20-Abril-2010
     Description	:
     Parameters   	: Patente: ShortString
     Return Value   : Boolean
    }
    function VehiculoOrignalSupendido(Patente: ShortString): Boolean;
    var
        i: Integer;
    begin
        i := 0;
        while (i < ConvenioModificado.Cuentas.CantidadVehiculos) and (Trim(ConvenioModificado.Cuentas.Vehiculos[i].Cuenta.Vehiculo.Patente) <> Trim(Patente)) do
            Inc(i);
        Result := (i < ConvenioModificado.Cuentas.CantidadVehiculos) //and (ConvenioModificado.Cuentas.Vehiculos[i].Cuenta.Suspendida <> NullDate);
                and (ConvenioModificado.Cuentas.Vehiculos[i].Cuenta.EstaSuspendida);
    end;

    {Procedure Name	: TagOrignalNoPerdido
     Author 		: Nelson Droguett Sierra ()
     Date Created	: 20-Abril-2010
     Description	:
     Parameters   	: Patente: ShortString
     Return Value   : Boolean
    }
    function TagOrignalNoPerdido(Patente: ShortString): Boolean;
    var
        i: Integer;
    begin
        i := 0;
        while (i < ConvenioModificado.Cuentas.CantidadVehiculos) and (Trim(ConvenioModificado.Cuentas.Vehiculos[i].Cuenta.Vehiculo.Patente) <> Trim(Patente)) do
            Inc(i);
        Result := (i < ConvenioModificado.Cuentas.CantidadVehiculos) and not (ConvenioModificado.Cuentas.Vehiculos[i].EsTagPerdido);
    end;

    {Procedure Name	: VehiculoOrignalNoSuspendido
     Author 		: Nelson Droguett Sierra ()
     Date Created	: 20-Abril-2010
     Description	:
     Parameters   	: Patente: ShortString
     Return Value   : Boolean
    }
    function VehiculoOrignalNoSuspendido(Patente: ShortString): Boolean;
    var
        i: Integer;
    begin
        i := 0;
        while (i < ConvenioModificado.Cuentas.CantidadVehiculos) and (Trim(ConvenioModificado.Cuentas.Vehiculos[i].Cuenta.Vehiculo.Patente) <> Trim(Patente)) do
            Inc(i);
        Result := (i < ConvenioModificado.Cuentas.CantidadVehiculos) //and (ConvenioModificado.Cuentas.Vehiculos[i].Cuenta.Suspendida = NullDate);
            and not (ConvenioModificado.Cuentas.Vehiculos[i].Cuenta.EstaSuspendida);
    end;

resourcestring
    MSG_PERS_INVALID_OPERATION = 'Cambio la personeria, operaci�n NO valdia - funci�n ObtenerCambios';
    MSG_RUT_INVALID_OPERATION = 'Cambio el RUT, operaci�n NO valdia - funci�n ObtenerCambios';
var
    x,i, IdxDomFacturacion:Integer;
    InfoExtra: String;
    EsConvenioSinCuenta: Boolean;
begin
    if Cliente.Datos.Personeria <> ConvenioModificado.Cliente.Datos.Personeria then begin
        raise Exception.create(MSG_PERS_INVALID_OPERATION);
        result := False;
        exit;
    end;

    if Cliente.Datos.NumeroDocumento <> ConvenioModificado.Cliente.Datos.NumeroDocumento then begin
        raise Exception.create(MSG_RUT_INVALID_OPERATION);
        result := False;
        exit;
    end;

    try
        DocumentacionConvenio.VaciarOperacionConvenio;
        DocumentacionConvenio.IniciarObtenerCambio;
        with Cliente do begin
            //if EsConvenioCN then begin										//SS_1147_MCA_20140408
            if EsConvenioNativo then begin                                      //SS_1147_MCA_20140408

                EsConvenioSinCuenta := (not ConvenioModificado.Cuentas.TieneCuentas) and (Cuentas.CantidadVehiculos = 0);
                //Convenio sin cuentas con alta de cuenta.
                if (not ConvenioModificado.Cuentas.TieneCuentas) and (Cuentas.CantidadVehiculos > 0) then
                    FDocumentacionConvenio.ConvertirDocumentacionConvenioSinCuentaAlta(FCliente.Datos.Personeria);
                //Clono los opcionales por si esta vez los trajo.
                if EsConvenioSinCuenta then
                    FDocumentacionConvenio.ClonarOpcionales;
                // Cambio del Nombre del Titular
                if (trim(Datos.Nombre) <> trim(ConvenioModificado.Cliente.Datos.Nombre)) or
                    (trim(Datos.Apellido) <> trim(ConvenioModificado.Cliente.Datos.Apellido)) or
                    (trim(Datos.ApellidoMaterno) <> trim(ConvenioModificado.Cliente.Datos.ApellidoMaterno)) or
                    (trim(Datos.RazonSocial) <> trim(ConvenioModificado.Cliente.Datos.RazonSocial)) or
                    (Datos.TipoCliente <> ConvenioModificado.Cliente.Datos.TipoCliente) OR{ or
                    (trim(Datos.Giro) <> trim(ConvenioModificado.Cliente.Datos.Giro))               }
                    (Datos.Semaforo1 <> ConvenioModificado.Cliente.Datos.Semaforo1) OR
                    (Datos.Semaforo3 <> ConvenioModificado.Cliente.Datos.Semaforo3)
                    then
                        DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocMODIF_TITULAR_MAYOR].CodigoTipoOperacionConvenio);


                // datos del titular (menor)
                if ((Datos.Personeria = PERSONERIA_FISICA) and (
                        (UpperCase(Datos.Sexo) <> UpperCase(ConvenioModificado.Cliente.Datos.Sexo)) or
                        (Datos.FechaNacimiento <> ConvenioModificado.Cliente.Datos.FechaNacimiento) or

                        (
                            (Domicilios.CantidadDomiciliosPersonas >= 2 ) and
                            (ConvenioModificado.Cliente.Domicilios.CantidadDomiciliosPersonas >= 1) and
                            (not DomiciliosIguales(Domicilios.Domicilios[0],ConvenioModificado.Cliente.Domicilios.Domicilios[0])
                        ))
                    ))
                    or
                    ((Datos.Personeria = PERSONERIA_JURIDICA) and (
                        (UpperCase(Datos.Apellido) <> UpperCase(ConvenioModificado.Cliente.Datos.Apellido)) or
                        (UpperCase(Datos.ApellidoMaterno) <> UpperCase(ConvenioModificado.Cliente.Datos.ApellidoMaterno)) or
                        (UpperCase(Datos.Nombre) <> UpperCase(ConvenioModificado.Cliente.Datos.Nombre)) or
                        (UpperCase(Datos.Sexo) <> UpperCase(ConvenioModificado.Cliente.Datos.Sexo))
                        or
                        (
                            (Domicilios.CantidadDomiciliosPersonas >= 2 ) and
                            (ConvenioModificado.Cliente.Domicilios.CantidadDomiciliosPersonas >= 1) and
                            (not DomiciliosIguales(Domicilios.Domicilios[0],ConvenioModificado.Cliente.Domicilios.Domicilios[0])
                        ))
                      )) then
                            DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocMODIF_TITULAR_MENOR].CodigoTipoOperacionConvenio);

                // Domicilio de Facturacion
                IdxDomFacturacion := Domicilios.ObtenerIndiceDomicilio(CodigoDomicilioFacturacion);
                if IdxDomFacturacion <> -1 then begin

                    if (Domicilios.CantidadDomiciliosPersonas <> ConvenioModificado.Cliente.Domicilios.CantidadDomiciliosPersonas)
                        or
                       not(DomiciliosIguales(Domicilios.Domicilios[IdxDomFacturacion],
                                             ConvenioModificado.Cliente.Domicilios.Domicilios[IdxDomFacturacion])) then
                            DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocMODIF_DOMICILIO_FAC].CodigoTipoOperacionConvenio);
                end;

                // medios de comunicacion del titular (menor)
                if  (Cliente.Telefonos.CantidadMediosComunicacion >= 1) then begin
                    //Agrego la condici�n por si agregu� el tel�fono principal y cambie de lugar un secuendario en convenios de terceros.
                    if (Telefonos.TieneMedioPrincipal) and (not ConvenioModificado.Cliente.Telefonos.TieneMedioPrincipal)
                    then begin
                        //Siempre se toma como modificacion porque este debe existir si o si
                        DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocMODIF_MEDIO_CONTACTO_TELEFONO_1].CodigoTipoOperacionConvenio);
                    end
                    else begin
                        if (Telefonos.TieneMedioPrincipal) and (ConvenioModificado.Cliente.Telefonos.TieneMedioPrincipal) then begin
                            if ((Telefonos.MedioPrincipal.CodigoArea <> ConvenioModificado.Cliente.Telefonos.MedioPrincipal.CodigoArea) or
                            (Telefonos.MedioPrincipal.CodigoTipoMedioContacto <> ConvenioModificado.Cliente.Telefonos.MedioPrincipal.CodigoTipoMedioContacto) or
                            (Telefonos.MedioPrincipal.Valor <> ConvenioModificado.Cliente.Telefonos.MedioPrincipal.Valor) or
                            (Telefonos.MedioPrincipal.Anexo <> ConvenioModificado.Cliente.Telefonos.MedioPrincipal.Anexo) or
                            (Telefonos.MedioPrincipal.HoraDesde <> ConvenioModificado.Cliente.Telefonos.MedioPrincipal.HoraDesde) or
                            (Telefonos.MedioPrincipal.HoraHasta <> ConvenioModificado.Cliente.Telefonos.MedioPrincipal.HoraHasta)
                            ) then begin
                            //Siempre se toma como modificacion porque este debe existir si o si
                                DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocMODIF_MEDIO_CONTACTO_TELEFONO_1].CodigoTipoOperacionConvenio);
                            end;
                        end;
                    end;
                end;

                if (Cliente.Telefonos.CantidadMediosComunicacion < ConvenioModificado.Cliente.Telefonos.CantidadMediosComunicacion) then begin
                    //Baja de Medio de comunicacion secundario
                    DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocBAJA_MEDIO_CONTACTO_TELEFONO_2].CodigoTipoOperacionConvenio)
                end
                else begin
                   if (Cliente.Telefonos.CantidadMediosComunicacion > ConvenioModificado.Cliente.Telefonos.CantidadMediosComunicacion) then begin
                    //Alta de Medio de comunicacion secundario
                        DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocALTA_MEDIO_CONTACTO_TELEFONO_2].CodigoTipoOperacionConvenio)
                   end
                   else begin
                        if  (Cliente.Telefonos.CantidadMediosComunicacion > 1) and
                            ((Telefonos.MedioSecundario.CodigoArea <> ConvenioModificado.Cliente.Telefonos.MedioSecundario.CodigoArea) or
                            (Telefonos.MedioSecundario.CodigoTipoMedioContacto <> ConvenioModificado.Cliente.Telefonos.MedioSecundario.CodigoTipoMedioContacto) or
                            (Telefonos.MedioSecundario.Valor <> ConvenioModificado.Cliente.Telefonos.MedioSecundario.Valor) or
                            (Telefonos.MedioSecundario.Anexo <> ConvenioModificado.Cliente.Telefonos.MedioSecundario.Anexo) or
                            (Telefonos.MedioSecundario.HoraDesde <> ConvenioModificado.Cliente.Telefonos.MedioSecundario.HoraDesde) or
                            (Telefonos.MedioSecundario.HoraHasta <> ConvenioModificado.Cliente.Telefonos.MedioSecundario.HoraHasta)) then begin
                            DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocMODIF_MEDIO_CONTACTO_TELEFONO_2].CodigoTipoOperacionConvenio);
                        end;
                   end;
                end;

                if (EMail.CantidadMediosComunicacion < ConvenioModificado.Cliente.EMail.CantidadMediosComunicacion) then begin
                    //Baja del mail
                    DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocBAJA_MEDIO_CONTACTO_EMAIL].CodigoTipoOperacionConvenio)
                end
                else begin
                    if (EMail.CantidadMediosComunicacion > ConvenioModificado.Cliente.EMail.CantidadMediosComunicacion) then begin
                    //Alta del mail
                    DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocALTA_MEDIO_CONTACTO_EMAIL].CodigoTipoOperacionConvenio)
                    end
                    else begin
                        if (EMail.CantidadMediosComunicacion > 0) and (EMail.MediosComunicacion[0].Valor <> ConvenioModificado.Cliente.EMail.MediosComunicacion[0].Valor) then begin
                            DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocMODIF_MEDIO_CONTACTO_EMAIL].CodigoTipoOperacionConvenio)
                        end;
                    end
                end;
            end;
        end;


        // Tipo del mandato
        if ((MedioPago.TieneMandante) and (not ConvenioModificado.MedioPago.TieneMandante)) or
           ((not MedioPago.TieneMandante) and (ConvenioModificado.MedioPago.TieneMandante)) or
           ((MedioPago.TieneMandante) and (ConvenioModificado.MedioPago.TieneMandante)) then begin

              if
               (
                 (MedioPago.MedioPago.TipoMedioPago = NINGUNO) and (ConvenioModificado.MedioPago.MedioPago.TipoMedioPago <> NINGUNO)
               ) or
               (
                  (MedioPago.MedioPago.TipoMedioPago <> NINGUNO) and (ConvenioModificado.MedioPago.MedioPago.TipoMedioPago = NINGUNO)
               ) or
               (
               (Trim(MedioPago.Mandante.Datos.NumeroDocumento) <> Trim(ConvenioModificado.MedioPago.Mandante.Datos.NumeroDocumento)) or
               (MedioPago.MedioPago.TipoMedioPago <> ConvenioModificado.MedioPago.MedioPago.TipoMedioPago) or
               (
                (MedioPago.MedioPago.TipoMedioPago = PAT) and
                (
                    (MedioPago.MedioPago.PAT.TipoTarjeta <> ConvenioModificado.MedioPago.MedioPago.PAT.TipoTarjeta) or
                    (MedioPago.MedioPago.PAT.EmisorTarjetaCredito <> ConvenioModificado.MedioPago.MedioPago.PAT.EmisorTarjetaCredito) or
                    (MedioPago.MedioPago.PAT.NroTarjeta <> ConvenioModificado.MedioPago.MedioPago.PAT.NroTarjeta) or
                    (MedioPago.MedioPago.PAT.FechaVencimiento <> ConvenioModificado.MedioPago.MedioPago.PAT.FechaVencimiento)
                )
               ) or
               (
                (MedioPago.MedioPago.TipoMedioPago = PAC) and
                (
                    (MedioPago.MedioPago.PAC.TipoCuenta <> ConvenioModificado.MedioPago.MedioPago.PAC.TipoCuenta) or
                    (MedioPago.MedioPago.PAC.CodigoBanco <> ConvenioModificado.MedioPago.MedioPago.PAC.CodigoBanco) or
                    (MedioPago.MedioPago.PAC.NroCuentaBancaria <> ConvenioModificado.MedioPago.MedioPago.PAC.NroCuentaBancaria) or
                    (MedioPago.MedioPago.PAC.Sucursal <> ConvenioModificado.MedioPago.MedioPago.PAC.Sucursal)
                )
               )
           ) then begin
                //Para completar el Campo que usa la interface de Transbank
                (* Indicar que hubo cambios en el Medio de Pago. *)
                Self.FMedioDePagoModificado := True;

                if MedioPago.MedioPago.TipoMedioPago = PAT then
                    DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocMODIF_TIPO_MANDATE_PAT].CodigoTipoOperacionConvenio)
                else begin
                    if MedioPago.MedioPago.TipoMedioPago = PAC then
                        DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocMODIF_TIPO_MANDATE_PAC].CodigoTipoOperacionConvenio)
                    else
                        DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocMODIF_TIPO_MANDATE_NIN].CodigoTipoOperacionConvenio);
                end;
             end;
        end;

        // Vehiculo BAJA
        for i := 0 to Cuentas.CantidadVehiculosEliminados - 1 do begin
            with Cuentas.VehiculosEliminados[i].Cuenta  do
                DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocBAJA_VEHICULO].CodigoTipoOperacionConvenio, Vehiculo.Patente, Vehiculo.CodigoTipoPatente, Vehiculo.Patente);
        end;

        // Vehiculo Nuevo
        for i := 0 to Cuentas.CantidadVehiculos - 1 do begin
            with Cuentas.Vehiculos[i].Cuenta do begin
                if Cuentas.Vehiculos[i].EsVehiculoNuevo then
                    DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocALTA_VEHICULO].CodigoTipoOperacionConvenio, Vehiculo.Patente ,Vehiculo.CodigoTipoPatente, Vehiculo.Patente);
            end;
         end;

        for i := 0 to Cuentas.CantidadVehiculosEliminadosSinGuardar - 1 do begin
            with Cuentas.VehiculosEliminadosSinGuardar[i].Cuenta  do
                DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocBAJA_VEHICULO].CodigoTipoOperacionConvenio, Vehiculo.Patente, Vehiculo.CodigoTipoPatente, Vehiculo.Patente);
        end;


        // Vehiculo SUSPENDIDO
        for i := 0 to Cuentas.CantidadVehiculos - 1 do begin
            with Cuentas.Vehiculos[i].Cuenta do
                //if (Suspendida <> NullDate) and Cuentas.Vehiculos[i].EsVehiculoModificado and (VehiculoOrignalNoSuspendido(Cuentas.Vehiculos[i].Cuenta.Vehiculo.Patente))  then
                if (Cuentas.Vehiculos[i].Cuenta.EstaSuspendida) and Cuentas.Vehiculos[i].EsVehiculoModificado and (VehiculoOrignalNoSuspendido(Cuentas.Vehiculos[i].Cuenta.Vehiculo.Patente))  then
                    DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocSUSPENDIDO].CodigoTipoOperacionConvenio, Vehiculo.Patente, Vehiculo.CodigoTipoPatente, Vehiculo.Patente);
        end;

        // Cuenta Vehiculo re activada
        for i := 0 to Cuentas.CantidadVehiculos - 1 do begin
            if (Cuentas.Vehiculos[i].EsActivacionCuenta) then
                DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocACTIVACION_VEHICULO].CodigoTipoOperacionConvenio, Cuentas.Vehiculos[i].Cuenta.Vehiculo.Patente, Cuentas.Vehiculos[i].Cuenta.Vehiculo.CodigoTipoPatente, Cuentas.Vehiculos[i].Cuenta.Vehiculo.Patente);
        end;

        // Tag PERDIDO
        for i := 0 to Cuentas.CantidadVehiculos - 1 do begin
            if (Cuentas.Vehiculos[i].EsTagPerdido) and (TagOrignalNoPerdido(Cuentas.Vehiculos[i].Cuenta.Vehiculo.Patente)) then
                DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(TOperacionConvenio[tocPERDIDA_TAG].CodigoTipoOperacionConvenio, Cuentas.Vehiculos[i].Cuenta.Vehiculo.Patente, Cuentas.Vehiculos[i].Cuenta.Vehiculo.CodigoTipoPatente, Cuentas.Vehiculos[i].Cuenta.Vehiculo.Patente);
        end;


        // RepLegales
        if (Cliente is TPersonaJuridica) then begin
            with TPersonaJuridica(Cliente).RepresentantesLegales do begin

                // Alta de Rep Legal
                for x := 0 to 2 do begin
                    if CantidadRepresentantes > x then  begin
                      InfoExtra := ArmarNombrePersona(Representantes[x].Datos.Personeria,
                                  Representantes[x].Datos.Nombre,
                                  Representantes[x].Datos.Apellido,
                                  Representantes[x].Datos.ApellidoMaterno);

                        i := TPersonaJuridica(ConvenioModificado.Cliente).RepresentantesLegales.ObtenerIndice(Representantes[x].Datos.TipoDocumento,Representantes[x].Datos.NumeroDocumento);
                        if (i < 0) then  //no esta en el convenio origianl
                            DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(ObtenerCodigoOperacionRepLegal(x,1, EsConvenioSinCuenta),InfoExtra);
                    end;
                end;

                // Cambio de datos del Rep Legal
                for x := 0 to 2 do begin
                    if CantidadRepresentantes > x then begin
                         InfoExtra := ArmarNombrePersona(Representantes[x].Datos.Personeria,
                                  Representantes[x].Datos.Nombre,
                                  Representantes[x].Datos.Apellido,
                                  Representantes[x].Datos.ApellidoMaterno);

                        i := TPersonaJuridica(ConvenioModificado.Cliente).RepresentantesLegales.ObtenerIndice(Representantes[x].Datos.TipoDocumento,Representantes[x].Datos.NumeroDocumento);
                        if (i >= 0)  then begin //encontre el mismo rep legal en el original
                            if (
                                    (trim(Representantes[x].Datos.Apellido) <> trim(TPersonaJuridica(ConvenioModificado.Cliente).RepresentantesLegales.Representantes[i].Datos.Apellido)) or
                                    (trim(Representantes[x].Datos.ApellidoMaterno) <> trim(TPersonaJuridica(ConvenioModificado.Cliente).RepresentantesLegales.Representantes[i].Datos.ApellidoMaterno)) or
                                    (trim(Representantes[x].Datos.Nombre) <> trim(TPersonaJuridica(ConvenioModificado.Cliente).RepresentantesLegales.Representantes[i].Datos.Nombre))
                                ) then  DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(ObtenerCodigoOperacionRepLegal(x,2,EsConvenioSinCuenta),InfoExtra);



                           if Representantes[x].NroRepresentante <> TPersonaJuridica(ConvenioModificado.Cliente).RepresentantesLegales.Representantes[i].NroRepresentante then begin // Si cambio el orden de representate
                                DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(ObtenerCodigoOperacionRepLegal(TPersonaJuridica(ConvenioModificado.Cliente).RepresentantesLegales.Representantes[i].NroRepresentante, 3, EsConvenioSinCuenta), InfoExtra);
                                DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(ObtenerCodigoOperacionRepLegal(Representantes[x].NroRepresentante, 1, EsConvenioSinCuenta), InfoExtra, '','',true);
                            end;
                        end;
                    end;
                end;

                // Baja de Rep Legal
                for x := 0 to 2 do begin
                    if CantidadRepresentantesQuitados > x then begin
                         InfoExtra := ArmarNombrePersona(RepresentantesQuitados[x].Datos.Personeria,
                                RepresentantesQuitados[x].Datos.Nombre,
                                RepresentantesQuitados[x].Datos.Apellido,
                                RepresentantesQuitados[x].Datos.ApellidoMaterno);

                          DocumentacionConvenio.AgregarDocumentacionxTipoOperacion(ObtenerCodigoOperacionRepLegal(RepresentantesQuitados[x].NroRepresentante, 3, EsConvenioSinCuenta),InfoExtra);
                    end;
                end;

            end;
        end;

        DocumentacionConvenio.FinObtenerCambio;

        result := True;
    except
        on E:Exception do begin
            raise Exception.Create(e.Message);
            result := False;
        end;
    end;
end;

{Procedure Name	: ReimprimirAnexo3y4
 Author 		: Nelson Droguett Sierra (pdominguez)
 Date Created	: 20-Abril-2010
 Description	: SS 733
        - Se llama al procedimiento Inicializar del objeto Form: TFormImprimirConvenio.
                  SS 836
        - Se asignan los datos del Representante Legal al anexo 3 cuando la
        Personer�a del Cliente es Jur�dica.
 Parameters   	:
 Return Value   :

 Revision : 1
 Author: ebaeza
 Date: 13/03/2014
 Firma: SS_1171_EBA_20140311
 Description: SS_1171
        -.Se llenan los dos campos, "RazonSocial" y "RutRazonSocial" de la estructura que se utiliza
        para enviar los datos a imprimir los que seran utilizados en el nuevo Anexo III Juridico
}
function TDatosConvenio.ReimprimirAnexo3y4;
var
	i : integer;
    Encontrado, TieneEmail : Boolean;
    DatosAnexo3 : TDatosAnexo3;
    DatosAnexo4 : TDatosAnexo4;
    Form : TFormImprimirConvenio;
begin
    //Ver si tiene e-mail
    Result := False;
    i := 0;
    while (not Result) and (i < MediosEnvioPago.CantidadMediosEnvio) do begin
    	Result := 	(MediosEnvioPago.MediosEnvio[i].CodigoTipoMedioEnvio = CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO) and
        			(MediosEnvioPago.MediosEnvio[i].CodigoMedioEnvioDocumento = CONST_MEDIO_ENVIO_DOCUMENTACION_COBRO);
        Inc(i);
    end;

    TieneEmail := Result;

    Form := TFormImprimirConvenio.Create(nil);
    Form.Inicializar(Self); // SS 733
    Result := True;
    i := 0;
    Encontrado := False;
    while Result and (i < FCuentas.CantidadVehiculos) do begin
        if FCuentas.Vehiculos[i].Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_ENTREGADO_EN_CUOTAS then begin
        	Encontrado := True;
        	{Anexo3 es uno por veh�culo}
            with DatosAnexo3 do begin
                // (SS 836)
                if (Cliente.Datos.Personeria = PERSONERIA_JURIDICA) and
                    (TPersonaJuridica(Cliente).RepresentantesLegales.CantidadRepresentantes > 0) then begin
                    APaterno := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.Apellido);
                    Amaterno := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.ApellidoMaterno);
                    Nombre	 := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.Nombre);
                    Cedula   := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.NumeroDocumento);
                    //RazonSocial := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.RazonSocial);	// SS_1171_MCA_20140408 // SS_1171_EBA_20140311
                    RazonSocial := Trim(Cliente.Datos.RazonSocial);																//SS_1171_MCA_20140408
                    RutRazonSocial := Trim(Cliente.Datos.NumeroDocumento);														// SS_1171_EBA_20140311
                end
                else begin
                	APaterno	:= Trim(Cliente.Datos.Apellido);
                    Amaterno	:= Trim(Cliente.Datos.ApellidoMaterno);
                    Nombre		:= Trim(Cliente.Datos.Nombre);
                    Cedula		:= Trim(Cliente.Datos.NumeroDocumento);
                end;

                Televia			:= Trim(SerialNumberToEtiqueta(Cuentas.Vehiculos[i].Cuenta.ContactSerialNumber));
                DomicilioCalle	:= Trim(Cliente.Domicilios.DomicilioPrincipal.Descripcion);
                DomicilioNumero	:= Trim(Cliente.Domicilios.DomicilioPrincipal.NumeroCalleSTR);
                DomicilioComuna	:= Trim(BuscarDescripcionComuna(	DMConnections.BaseCAC,	Cliente.Domicilios.DomicilioPrincipal.CodigoPais,
                                                                                    Cliente.Domicilios.DomicilioPrincipal.CodigoRegion,
                                                                                    Cliente.Domicilios.DomicilioPrincipal.CodigoComuna ));
                DomicilioDetalle	:= Trim(Cliente.Domicilios.DomicilioPrincipal.Detalle);			//SS_973_MBE_20110801
                DomicilioDepto		:= Trim(Cliente.Domicilios.DomicilioPrincipal.Dpto);			//SS_973_MBE_20110801
            end;

            //Result := Form.ImprimirAnexo3o4(3, TieneEmail, DatosAnexo3, DatosAnexo4);							// SS_1171_EBA_20140311
            Result := Form.ImprimirAnexo3o4(3, TieneEmail, DatosAnexo3, DatosAnexo4, Cliente.Datos.Personeria);	// SS_1171_EBA_20140311
        end;

        Inc(i);
    end;

    if Encontrado and Result then begin           {Imprimir Anexo 4}
    	if TieneEmail then begin
            with DatosAnexo3 do begin
            	APaterno	:= Trim(Cliente.Datos.Apellido);
                Amaterno	:= Trim(Cliente.Datos.ApellidoMaterno);
                Nombre		:= Trim(Cliente.Datos.Nombre);
                Cedula		:= Trim(Cliente.Datos.NumeroDocumento);
                Televia		:= '';
                DomicilioCalle	:= Trim(Cliente.Domicilios.DomicilioPrincipal.Descripcion);
                DomicilioNumero	:= Trim(Cliente.Domicilios.DomicilioPrincipal.NumeroCalleSTR);
                DomicilioComuna	:= Trim(BuscarDescripcionComuna(	DMConnections.BaseCAC,	Cliente.Domicilios.DomicilioPrincipal.CodigoPais,
                																			Cliente.Domicilios.DomicilioPrincipal.CodigoRegion,
                                                                                    		Cliente.Domicilios.DomicilioPrincipal.CodigoComuna ));
                DomicilioDetalle	:= Trim(Cliente.Domicilios.DomicilioPrincipal.Detalle);			//SS_973_MBE_20110801
                DomicilioDepto		:= Trim(Cliente.Domicilios.DomicilioPrincipal.Dpto);			//SS_973_MBE_20110801

            end;

            with DatosAnexo4 do begin
            	if Cliente.EMail.CantidadMediosComunicacion > 0 then EMail := Trim(Cliente.EMail.MediosComunicacion[0].Valor)
                else EMail := '';

                if	(Cliente.Datos.Personeria = PERSONERIA_JURIDICA) and
                	(TPersonaJuridica(Cliente).RepresentantesLegales.CantidadRepresentantes > 0) then begin
                    DatosAnexo3.APaterno := Trim(Cliente.Datos.RazonSocial);
                    CliCedula   := Cliente.Datos.NumeroDocumento;		// SS_948_PDO_20120704
                    RepAPaterno := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.Apellido);
                    RepAmaterno := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.ApellidoMaterno);
                    RepNombre	:= Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.Nombre);
                    RepCedula	:= Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.NumeroDocumento)
                end
                else RepAPaterno := '';
            end;

			//Result := Form.ImprimirAnexo3o4(4, TieneEmail, DatosAnexo3, DatosAnexo4);							// SS_1171_EBA_20140311
            Result := Form.ImprimirAnexo3o4(4, TieneEmail, DatosAnexo3, DatosAnexo4, Cliente.Datos.Personeria);	// SS_1171_EBA_20140311
        end;
    end;

    Form.Release;

end;

{Procedure Name	: TDatosConvenio.TieneMedioEnvioPorEmail
 Author 		: Nelson Droguett Sierra (mbecerra)
 Date Created	: 20-Abril-2010
 Description	: (Ref. Facturaci�n Electr�nica)
        	Devuelve verdadero si el convenio tiene asignado un medio de
            env�o por correo elecr�nico. Falso en caso contrario
 Parameters   	:
 Return Value   :
}
function TDatosConvenio.TieneMedioEnvioPorEmail;
var
	i : integer;
begin
	Result := False;
    i := 0;
    while (not Result) and (i < MediosEnvioPago.CantidadMediosEnvio) do begin
    	Result := 	(MediosEnvioPago.MediosEnvio[i].CodigoTipoMedioEnvio = CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO) and
        			(MediosEnvioPago.MediosEnvio[i].CodigoMedioEnvioDocumento = CONST_MEDIO_ENVIO_DOCUMENTACION_COBRO);
        Inc(i);
    end;

    ConvenioTieneMedioEnvioPorEMail := Result;
end;


{Procedure Name	: ImprimirAnexo3y4VehNuevos
 Author 		: Nelson Droguett Sierra (pdominguez)
 Date Created	: 20-Abril-2010
 Description	: SS 733
        - Se llama al procedimiento Inicializar del objeto Form:TFormImprimirConvenio.
                  SS 836
        - Se asignan los datos del Representante Legal al anexo 3 cuando la
        Personer�a del Cliente es Jur�dica.

 Parameters   	:
 Return Value   :

 Revision : 1
 Author: ebaeza
 Date: 13/03/2014
 Firma: SS_1171_EBA_20140311
 Description: SS_1171
    -.Se llenan los dos campos, "RazonSocial" y "RutRazonSocial" de la estructura que se utiliza
        para enviar los datos a imprimir los que seran utilizados en el nuevo Anexo III Juridico
}
function TDatosConvenio.ImprimirAnexo3y4VehNuevos;
resourcestring
{INICIO: TASK_005_JMA_20160418	
    MSG_SQL_MAIL	= 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL()';
    MSG_SQL_POSTAL	= 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL()';
}		
	MSG_SQL_MAIL	= 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL(dbo.ObtenerConcesionariaNativa())';
    MSG_SQL_POSTAL	= 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL(dbo.ObtenerConcesionariaNativa())';
{TERMINO: TASK_005_JMA_20160418}	
var
    IDMotivo, ConceptoCuotasPostal, ConceptoCuotasMail,
	i : integer;
    Encontrado, TieneEmail : Boolean;
    DatosAnexo3 : TDatosAnexo3;
    DatosAnexo4 : TDatosAnexo4;
    Form : TFormImprimirConvenio;
begin
    //Imprimir Anexos 3 y 4
    TieneEmail := TieneMedioEnvioPorEMail();

    ConceptoCuotasPostal	:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_POSTAL);
    ConceptoCuotasMail		:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_MAIL);

    //Ver si debe imprimir el anexo III
    Form := TFormImprimirConvenio.Create(nil);
    Form.Inicializar(Self); // SS 733
    Result := True;
    i := 0;
    Encontrado := False;
    while Result and (i < FCuentas.CantidadVehiculosACobrar) do begin
        IDMotivo := FCuentas.ObtenerIDMotivoMovimientoCuentaTelevia(i);
        if (IDMotivo = ConceptoCuotasPostal) or (IDMotivo = ConceptoCuotasMail) then begin
        	Encontrado := True;
        	{Anexo3 es por cada veh�culo}
            with DatosAnexo3 do begin
                // (SS 836)
            	if	(Cliente.Datos.Personeria = PERSONERIA_JURIDICA) and
            		(TPersonaJuridica(Cliente).RepresentantesLegales.CantidadRepresentantes > 0) then begin
                	APaterno := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.Apellido);
                	Amaterno := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.ApellidoMaterno);
                	Nombre	 := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.Nombre);
                	Cedula	 := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.NumeroDocumento);
                  	//RazonSocial := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.RazonSocial);	// SS_1171_EBA_20140311
                    RazonSocial := Trim(Cliente.Datos.RazonSocial);																//SS_1171_MCA_20140408
                  	RutRazonSocial := Trim(Cliente.Datos.NumeroDocumento);														// SS_1171_EBA_20140311
                end
                else begin
                	APaterno	:= Trim(Cliente.Datos.Apellido);
                    Amaterno	:= Trim(Cliente.Datos.ApellidoMaterno);
                    Nombre		:= Trim(Cliente.Datos.Nombre);
                    Cedula		:= Trim(Cliente.Datos.NumeroDocumento);
                end;

                Televia         := Trim(SerialNumberToEtiqueta(PMovimientoCuenta(FCuentas.FVehiculosTagsACobrar.Items[i])^.VehiculosConvenio^.Cuenta.ContactSerialNumber));
                DomicilioCalle	:= Trim(Cliente.Domicilios.DomicilioPrincipal.Descripcion);
                DomicilioNumero	:= Trim(Cliente.Domicilios.DomicilioPrincipal.NumeroCalleSTR);
                DomicilioComuna	:= Trim(BuscarDescripcionComuna(	DMConnections.BaseCAC,	Cliente.Domicilios.DomicilioPrincipal.CodigoPais,
                                                                                    Cliente.Domicilios.DomicilioPrincipal.CodigoRegion,
                                                                                    Cliente.Domicilios.DomicilioPrincipal.CodigoComuna ));
            end;
			//Result := Form.ImprimirAnexo3o4(3, TieneEmail, DatosAnexo3, DatosAnexo4);							// SS_1171_EBA_20140311
            Result := Form.ImprimirAnexo3o4(3, TieneEmail, DatosAnexo3, DatosAnexo4, Cliente.Datos.Personeria);	// SS_1171_EBA_20140311
        end;
        Inc(i);
    end;

    if Encontrado and Result then begin {Imprimir Anexo 4}
    	if TieneEmail then begin
    	with DatosAnexo3 do begin
        	APaterno	:= Trim(Cliente.Datos.Apellido);
        	Amaterno	:= Trim(Cliente.Datos.ApellidoMaterno);
        	Nombre		:= Trim(Cliente.Datos.Nombre);
        	Cedula		:= Trim(Cliente.Datos.NumeroDocumento);
        	Televia		:= '';
        	DomicilioCalle	:= Trim(Cliente.Domicilios.DomicilioPrincipal.Descripcion);
        	DomicilioNumero	:= Trim(Cliente.Domicilios.DomicilioPrincipal.NumeroCalleSTR);
        	DomicilioComuna	:= Trim(BuscarDescripcionComuna(	DMConnections.BaseCAC,	Cliente.Domicilios.DomicilioPrincipal.CodigoPais,
                                                                                        Cliente.Domicilios.DomicilioPrincipal.CodigoRegion,
                                                                                        Cliente.Domicilios.DomicilioPrincipal.CodigoComuna ));
        end;

        with DatosAnexo4 do begin
        	if Cliente.EMail.CantidadMediosComunicacion > 0 then EMail := Trim(Cliente.EMail.MediosComunicacion[0].Valor)
        	else EMail := '';

        	if	(Cliente.Datos.Personeria = PERSONERIA_JURIDICA) and
        		(TPersonaJuridica(Cliente).RepresentantesLegales.CantidadRepresentantes > 0) then begin
            	DatosAnexo3.APaterno := Trim(Cliente.Datos.RazonSocial);
                CliCedula   := Cliente.Datos.NumeroDocumento;		// SS_948_PDO_20120704
            	RepAPaterno := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.Apellido);
            	RepAmaterno := Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.ApellidoMaterno);
            	RepNombre	:= Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.Nombre);
            	RepCedula	:= Trim(TPersonaJuridica(Cliente).RepresentantesLegales.Representantes[0].Datos.NumeroDocumento)
        	end
        	else RepAPaterno := '';
        end;

		//Result := Form.ImprimirAnexo3o4(4, TieneEmail, DatosAnexo3, DatosAnexo4);							// SS_1171_EBA_20140311
        Result := Form.ImprimirAnexo3o4(4, TieneEmail, DatosAnexo3, DatosAnexo4, Cliente.Datos.Personeria);	// SS_1171_EBA_20140311
        end;
    end;

    Form.Release;
end;

{Procedure Name	: Imprimir
 Author 		: Nelson Droguett Sierra (pdominguez)
 Date Created	: 20-Abril-2010
 Description	: SS 733
        - Se llama a la procedimiento Inicializar del Form TFormImprimirConvenio.
 Parameters   	: CuentasConvenioOriginal: TCuentasConvenio; var Imprimio: Boolean
 Return Value   :
}
function TDatosConvenio.Imprimir(CuentasConvenioOriginal: TCuentasConvenio; var Imprimio: Boolean): Boolean;
var
    i : integer;
    CantCopias : Integer;
    MotivoCancelacion: TMotivoCancelacion;
    Form : TFormImprimirConvenio;
    ImprimioCaratula, ImprimioConvenio: boolean;

begin
    Form := TFormImprimirConvenio.Create(nil);
    Form.Inicializar(Self); // SS 733

    ImprimioCaratula:= False;
    ImprimioConvenio:= False;
    MotivoCancelacion := OK;

	for i := 0 to DocumentacionConvenio.CantidadOperacionesConvenio - 1 do begin

        if (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_TIPO_MANDATE_PAT].CodigoTipoOperacionConvenio) then begin
            /// **------------------------------------------------MANDATO PAT **
            if (MotivoCancelacion = OK) then  begin
               if not ObtenerParametroGeneral(DMConnections.BaseCAC,iif(MedioPago.MedioPago.PAT.TipoTarjeta = TARJETA_PRESTO ,CAN_COPIAS_MANDATO_PAT_PRESTO,CAN_COPIAS_MANDATO_PAT),CantCopias) then MotivoCancelacion := Peatypes.ERROR
               else Imprimir(IMPRIMIR_MANDATO,MotivoCancelacion,CantCopias,Form, CuentasConvenioOriginal);
            end;

            // **----------------------------------------------------CARATULA **
            if (MotivoCancelacion = OK) then  begin                                                                                                           //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CARATULA,CantCopias) then MotivoCancelacion := Peatypes.ERROR                  //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               else ImprimirCaratula(MotivoCancelacion, CantCopias, UsuarioSistema, ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,PuntoEntrega),Form); //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               ImprimioCaratula:= True;                                                                                                                       //SS_1380_NDR_20151009//SS_1380_MCA_20150911
            end;                                                                                                                                              //SS_1380_NDR_20151009//SS_1380_MCA_20150911

        end;

        if (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_TIPO_MANDATE_PAC].CodigoTipoOperacionConvenio) then begin
            // **------------------------------------------------MANDATO PAC **
            if (MotivoCancelacion = OK) then  begin
               if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_MANDATO_PAC,CantCopias) then MotivoCancelacion := Peatypes.ERROR
               else Imprimir(IMPRIMIR_MANDATO,MotivoCancelacion,CantCopias,Form, CuentasConvenioOriginal);
            end;

            // **----------------------------------------------------CARATULA **
            if (MotivoCancelacion = OK) then  begin                                                                                                           //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CARATULA,CantCopias) then MotivoCancelacion := Peatypes.ERROR                  //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               else ImprimirCaratula(MotivoCancelacion, CantCopias, UsuarioSistema, ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,PuntoEntrega),Form); //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               ImprimioCaratula:= True;                                                                                                                       //SS_1380_NDR_20151009//SS_1380_MCA_20150911
            end;                                                                                                                                              //SS_1380_NDR_20151009//SS_1380_MCA_20150911
        end;

        if (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_TIPO_MANDATE_NIN].CodigoTipoOperacionConvenio) then begin
            // **---------------------------------------------MANDATO NINGUNO **
            // **----------------------------------------------------CARATULA **
            if (MotivoCancelacion = OK) then  begin                                                                                                           //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CARATULA,CantCopias) then MotivoCancelacion := Peatypes.ERROR                  //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               else ImprimirCaratula(MotivoCancelacion, CantCopias, UsuarioSistema, ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,PuntoEntrega),Form); //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               ImprimioCaratula:= True;                                                                                                                       //SS_1380_NDR_20151009//SS_1380_MCA_20150911
            end;                                                                                                                                              //SS_1380_NDR_20151009//SS_1380_MCA_20150911
        end;

        if (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_TITULAR_MAYOR].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_REP_LEGAL_1].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_REP_LEGAL_2].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_REP_LEGAL_3].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocALTA_REP_LEGAL_1].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocALTA_REP_LEGAL_2].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocALTA_REP_LEGAL_3].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_DOMICILIO_FAC].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_VEHICULO_MAYOR].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_BAJA_CUENTA].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_ALTA_CUENTA].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocBAJA_VEHICULO].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocALTA_VEHICULO].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocBAJA_REP_LEGAL_1].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocBAJA_REP_LEGAL_2].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocBAJA_REP_LEGAL_3].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocBAJA_CONVENIO].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocALTA_MEDIO_CONTACTO_TELEFONO_1].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_MEDIO_CONTACTO_TELEFONO_1].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocBAJA_MEDIO_CONTACTO_TELEFONO_1].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocALTA_MEDIO_CONTACTO_TELEFONO_2].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_MEDIO_CONTACTO_TELEFONO_2].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocBAJA_MEDIO_CONTACTO_TELEFONO_2].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocALTA_MEDIO_CONTACTO_EMAIL].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocMODIF_MEDIO_CONTACTO_EMAIL].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocBAJA_MEDIO_CONTACTO_EMAIL].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocSUSPENDIDO].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocACTIVACION_VEHICULO].CodigoTipoOperacionConvenio)
            or (DocumentacionConvenio.OperacionConvenio[i] = TOperacionConvenio[tocPERDIDA_TAG].CodigoTipoOperacionConvenio) then begin

            // **----------------------------------------------------CONVENIO **
            if (not ImprimioConvenio) and (Cliente.Datos.Personeria = PERSONERIA_JURIDICA) and (MotivoCancelacion = OK) then  begin
                if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CONVENIO_JURIDICA,CantCopias) then MotivoCancelacion := Peatypes.ERROR
                else Imprimir(IMPRIMIR_CONVENIO,MotivoCancelacion,CantCopias,Form, CuentasConvenioOriginal);
            end;

             if (not ImprimioConvenio) and (Cliente.Datos.Personeria = PERSONERIA_FISICA) and (MotivoCancelacion = OK) then  begin
                if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CONVENIO_FISICA,CantCopias) then MotivoCancelacion := Peatypes.ERROR
                else Imprimir(IMPRIMIR_CONVENIO,MotivoCancelacion,CantCopias,Form ,CuentasConvenioOriginal);
            end;
            ImprimioConvenio := True;

            // **----------------------------------------------------CARATULA **
            if (not ImprimioCaratula) and (MotivoCancelacion = OK) then  begin                                                                                //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CARATULA,CantCopias) then MotivoCancelacion := Peatypes.ERROR                  //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               else ImprimirCaratula(MotivoCancelacion, CantCopias, UsuarioSistema, ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,PuntoEntrega),Form); //SS_1380_NDR_20151009//SS_1380_MCA_20150911
               ImprimioCaratula:= True;                                                                                                                       //SS_1380_NDR_20151009//SS_1380_MCA_20150911
            end;

        end;

    end;

    if not ImprimirAnexo3y4VehNuevos() then MotivoCancelacion := PeaTypes.ERROR;

    if MotivoCancelacion = Peatypes.ERROR then
        MsgBox(MSG_ERROR_IMPRIMIR_CONVENIO,STR_ERROR,MB_ICONSTOP);

    //Asigno la prop.
    FNroAnexoEmitido := Form.NroAnexoEmitido;

    result := MotivoCancelacion = OK;

    Imprimio := ImprimioCaratula or ImprimioConvenio;                             //SS_1380_NDR_20151009//SS_1380_MCA_20150911
    //Imprimio := ImprimioConvenio;                                               //SS_1380_NDR_20151009//SS_1380_MCA_20150911

    Form.Free;
end;

{Procedure Name	: TDatosConvenio.GetEstadoRNUT
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TDatosConvenio.GetEstadoRNUT: Integer;
begin
    result := FEstadoRNUT;
end;

{Procedure Name	: TDatosConvenio.SetEstadoRNUT
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Integer
 Return Value   :
}
procedure TDatosConvenio.SetEstadoRNUT(const Value: Integer);
begin
    FEstadoRNUT := Value;
end;

{Procedure Name	: TDatosConvenio.GetNumeroConvenioRNUT
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : string
}
//BEGIN : SS_1147Q_NDR_20141202 -----------------------------------------------------------------
function TDatosConvenio.GetNumeroConvenioRNUT: string;
begin
    if (Cuentas.CantidadVehiculos > 0) and (FechaBajaRNUT <> NULLDATE) then begin
      if ObtenerCodigoConcesionariaNativa=CODIGO_VS then
      begin
        if (Length(Trim(FNumeroConvenioRNUT)) > LargoNumeroConvenioConcesionariaNativa()) then
            FNumeroConvenioRNUT := copy(FNumeroConvenioRNUT,1,LargoNumeroConvenioConcesionariaNativa()) + PadL(inttostr(strtoint(StrRight(Trim(FNumeroConvenioRNUT), 3)) + 1),3,'0')
        else FNumeroConvenioRNUT := Trim(FNumeroConvenioRNUT) + '001';
      end
      else
      begin
        if (Length(Trim(FNumeroConvenioRNUT)) > LargoNumeroConvenioConcesionariaNativa()) then
            FNumeroConvenioRNUT := copy(FNumeroConvenioRNUT,1,LargoNumeroConvenioConcesionariaNativa()) + PadL(inttostr(strtoint(StrRight(Trim(FNumeroConvenioRNUT), 3)) + 1),3,'0')
        else FNumeroConvenioRNUT := Trim(FNumeroConvenioRNUT) + '001';
      end;
      result := Trim(FNumeroConvenioRNUT);
    end
    else result := Trim(FNumeroConvenioRNUT);
end;
//END : SS_1147Q_NDR_20141202 -----------------------------------------------------------------

{Procedure Name	: TDatosConvenio.SetNumeroConvenioRNUT
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: string
 Return Value   :
}
procedure TDatosConvenio.SetNumeroConvenioRNUT(const Value: string);
begin
    FNumeroConvenioRNUT := Value;
end;

{Procedure Name	: TDatosConvenio.SetCodigoConvenioExterno
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:const Value: string
 Return Value   :
}
procedure TDatosConvenio.SetCodigoConvenioExterno(const Value: string);
begin
    FCodigoConvenioExterno := Value;
end;

{Procedure Name	: TDatosConvenio.GetFechaBajaRNUT
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : TDateTime
}
function TDatosConvenio.GetFechaBajaRNUT: TDateTime;
begin
    Result := FFechaBajaRNUT;
end;

{Procedure Name	: TDatosConvenio.SetFechaBajaRNUT
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: TDateTime
 Return Value   :
}
procedure TDatosConvenio.SetFechaBajaRNUT(const Value: TDateTime);
begin
    FFechaBajaRNUT := Value;
end;


{Procedure Name	: TDatosConvenio.ObtenerEstadoCambioMedioPago
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: TipoMedioPagoAnterior, TipoMedioPagoActual: TTiposMediosPago; EstadoCambioMedioPago: integer
 Return Value   : integer
}
function TDatosConvenio.ObtenerEstadoCambioMedioPago(TipoMedioPagoAnterior, TipoMedioPagoActual: TTiposMediosPago; EstadoCambioMedioPago: integer): integer;
begin

    //Veo que tipo de cambio corresponderia de acuerdo a lo que se modifico
    if TipoMedioPagoActual = PAT then begin
        if (TipoMedioPagoAnterior = PAC) then
            result := CONST_ESTADO_CAMBIO_PAC_PAT
        else begin
            if (TipoMedioPagoAnterior = PAT) then
                result := CONST_ESTADO_MODIF_PAT
            else
                result := CONST_ESTADO_ALTA_PAT;
        end;
    end
    else begin
        if TipoMedioPagoActual = PAC then begin
            if TipoMedioPagoAnterior = PAT then
                result := CONST_ESTADO_CAMBIO_PAT_PAC
            else begin
                if (TipoMedioPagoAnterior = PAC) then
                    result := CONST_ESTADO_MODIF_PAC
                else
                    result := CONST_ESTADO_ALTA_PAC;
            end;
        end
        else begin
            if (TipoMedioPagoAnterior = PAT)  then begin
                result := CONST_ESTADO_BAJA_PAT;
            end
            else begin
                result := CONST_ESTADO_BAJA_PAC;
            end;
        end;
    end;

    //Veo que tipo de cambio corresponderia de acuerdo al estado anterior
    if (EstadoCambioMedioPago = CONST_ESTADO_ALTA_PAC) then begin
        if result = CONST_ESTADO_BAJA_PAC then result := CONST_ESTADO_MEDIO_PAGO_SIN_CAMBIO
        else
            if result = CONST_ESTADO_MODIF_PAC then result := CONST_ESTADO_ALTA_PAC
            else if result = CONST_ESTADO_CAMBIO_PAC_PAT then result := CONST_ESTADO_ALTA_PAT;
    end;

    if (EstadoCambioMedioPago = CONST_ESTADO_BAJA_PAC) then begin
        if result = CONST_ESTADO_ALTA_PAC then result := CONST_ESTADO_MODIF_PAC
        else
            if result = CONST_ESTADO_ALTA_PAT then result := CONST_ESTADO_CAMBIO_PAC_PAT;
    end;

    if (EstadoCambioMedioPago = CONST_ESTADO_CAMBIO_PAT_PAC) then begin
        if result = CONST_ESTADO_MODIF_PAC then result := CONST_ESTADO_CAMBIO_PAT_PAC
        else
            if result = CONST_ESTADO_CAMBIO_PAC_PAT then result := CONST_ESTADO_MODIF_PAT
            else if result = CONST_ESTADO_BAJA_PAC then result := CONST_ESTADO_BAJA_PAT;
    end;

    if (EstadoCambioMedioPago = CONST_ESTADO_ALTA_PAT) then begin
        if result = CONST_ESTADO_BAJA_PAT then result := CONST_ESTADO_MEDIO_PAGO_SIN_CAMBIO
        else
            if result = CONST_ESTADO_MODIF_PAT then result := CONST_ESTADO_ALTA_PAT
            else if result = CONST_ESTADO_CAMBIO_PAT_PAC then result := CONST_ESTADO_ALTA_PAC;
    end;

    if (EstadoCambioMedioPago = CONST_ESTADO_BAJA_PAT) then begin
        if result = CONST_ESTADO_ALTA_PAT then result := CONST_ESTADO_MODIF_PAT
        else
            if result = CONST_ESTADO_ALTA_PAC then result := CONST_ESTADO_CAMBIO_PAT_PAC;
    end;

    if (EstadoCambioMedioPago = CONST_ESTADO_CAMBIO_PAC_PAT) then begin
        if result = CONST_ESTADO_MODIF_PAT then result := CONST_ESTADO_CAMBIO_PAC_PAT
        else
            if result = CONST_ESTADO_CAMBIO_PAC_PAT then result := CONST_ESTADO_MODIF_PAT
            else if result = CONST_ESTADO_BAJA_PAT then result := CONST_ESTADO_BAJA_PAC;
    end;
end;

{Procedure Name	: TDatosConvenio.SetListObservacionConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: TListObservacionConvenio
 Return Value   :
}
procedure TDatosConvenio.SetListObservacionConvenio(
  const Value: TListObservacionConvenio);
begin
  FListObservacionConvenio := Value;
end;

{Procedure Name	: TDatosConvenio.SetCheckListConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Value: TCheckListConvenio
 Return Value   :
}
procedure TDatosConvenio.SetCheckListConvenio(
  const Value: TCheckListConvenio);
begin
  FCheckListConvenio := Value;
end;

{Procedure Name	: TDatosConvenio.SetCodigoDomicilioFacturacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:const Value: Integer
 Return Value   :
}
procedure TDatosConvenio.SetCodigoDomicilioFacturacion(
  const Value: Integer);
begin
  FCodigoDomicilioFacturacion := Value;
end;


{Procedure Name	: TDatosConvenio.TieneCuentasSuspendidas
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDatosConvenio.TieneCuentasSuspendidas: Boolean;
begin
    Result := FTieneCuentasSuspendidasOtroConvenio or Cuentas.TieneCuentasSuspendidas;
end;

{Procedure Name	: TDatosConvenio.TieneTagVendidoSinUsar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TDatosConvenio.TieneTagVendidoSinUsar: Integer;
begin
    Result := FTieneTagVendidoSinUsar;
end;

{Procedure Name	: TDatosConvenio.SetNroAnexoEmitido
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Integer
 Return Value   :
}
procedure TDatosConvenio.SetNroAnexoEmitido(const Value: Integer);
begin
  FNroAnexoEmitido := Value;
end;

{Procedure Name	: TDatosConvenio.SetChekListCuenta
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: TChekListCuenta
 Return Value   :
}
procedure TDatosConvenio.SetChekListCuenta(const Value: TChekListCuenta);
begin
  FChekListCuenta := Value;
end;

{Procedure Name	: TDatosConvenio.GetTieneObservacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDatosConvenio.GetTieneObservacion: Boolean;
begin
    Result := FCheckListConvenio.TieneMarcados or (FChekListCuenta.TieneItems) or FListObservacionConvenio.TieneObservaciones;
end;

{Procedure Name	: TDatosConvenio.GetCantidadVehiculos
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TDatosConvenio.GetCantidadVehiculos: Integer;
begin
    Result := FCuentas.CantidadVehiculos;
end;

{Procedure Name	: TDatosConvenio.TurnoAbierto
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDatosConvenio.TurnoAbierto: Boolean;
var
    DatosTurno: TDatosTurno;
begin
    (* Verificar que el turno que se est� por utilizar est� abierto y sea del
    usuario logueado. *)

    Result := VerTurnoAbierto(DMConnections.BaseCAC, PuntoEntrega, PuntoVenta, UsuarioSistema, DatosTurno);
    NumeroTurno := DatosTurno.NumeroTurno;
    if DatosTurno.Estado = TURNO_ESTADO_CERRADO then NumeroTurno := -1;

end;

{Procedure Name	: TDatosConvenio.ActualizarMediosEnvioDocumentacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TDatosConvenio.ActualizarMediosEnvioDocumentacion;
var
    i, CantidadMaximaVehiculos: Integer;
begin
    (* Actualiza los Medios de Env�o de Documentaci�n seg�n las restricciones
    sobre los datos de los Medios de Env�o de Documentaci�n. *)
    for i := 0 to FMediosEnvioPago.FMediosEnvio.Count - 1 do begin
        (* Si existe al Medio de Env�o Detalle de Facturaci�n Impreso junto
        con Nota de Cobro, controlar que la cantidad de veh�culos del
        convenio NO supere el M�ximo para el env�o de Faturaci�n Detallada
        Gratis.
        Si supera la cantidad m�xima, cambiar el Tipo Medio de Env�o a E-mail. *)
        if ( PRegistroMedioEnvioDocumentosCobro(FMediosEnvioPago.FMediosEnvio.Items[i])^.CodigoTipoMedioEnvio = CONST_TIPO_MEDIO_ENVIO_CORREO_POSTAL)
                and (PRegistroMedioEnvioDocumentosCobro(FMediosEnvioPago.FMediosEnvio.Items[i])^.CodigoMedioEnvioDocumento = CONST_MEDIO_ENVIO_DETALLE_FACTURACION) then begin

            ObtenerParametroGeneral(DMConnections.BaseCAC, 'TOPE_VEHICULOS_FACT_DETALLADA', CantidadMaximaVehiculos);
            if (Self.CantidadVehiculos > CantidadMaximaVehiculos) then begin
                PRegistroMedioEnvioDocumentosCobro(FMediosEnvioPago.FMediosEnvio.Items[i])^.CodigoTipoMedioEnvio := CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO;
            end;
        end;
    end; // for
end;

{Procedure Name	: TDatosConvenio.SetNoGenerarIntereses
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Boolean
 Return Value   :
}
procedure TDatosConvenio.SetNoGenerarIntereses(const Value: Boolean);
begin
  FNoGenerarIntereses := Value;
end;

{ TMediosComunicacionPersona }

{Procedure Name	: TMediosComunicacionPersona.AgregarMediocomunicacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: AMedioComunicacion: TTipoMedioComunicacion
 Return Value   :
}
procedure TMediosComunicacionPersona.AgregarMediocomunicacion(
  AMedioComunicacion: TTipoMedioComunicacion);
resourcestring
    MSG_CANT_ERROR = 'Se excedio la cantidad maxima de medios de comunicacion permitidos';
var
 MedioComunicacion : PMedioComunicacion;
begin
    if ((FTipoMedioComunicacion =  TMC_TELEFONO) and (CantidadMediosComunicacion > 2)) or
       ((FTipoMedioComunicacion =  TMC_EMAIL) and (CantidadMediosComunicacion > 1)) then begin
        raise Exception.Create(MSG_CANT_ERROR);
        exit;
    end;


    New(MedioComunicacion);
    MedioComunicacion^ := AMedioComunicacion;
    FMediosComunicacion.Add(MedioComunicacion);
end;

{Procedure Name	: TMediosComunicacionPersona.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoPersona : integer; FormatoMedioComunicacion: TFormatosMediosComunicacion
 Return Value   :
}
constructor TMediosComunicacionPersona.Create(CodigoPersona : integer; FormatoMedioComunicacion: TFormatosMediosComunicacion);
resourcestring
    ERROR_CREAROBJETO = 'Error al intentar obtener medios de comunicaci�n.';
    MSG_FORMAT_ERROR = 'Formato de Medio de Comunicaci�n incorrecto.';
var
    AItem : PMedioComunicacion;
    Formato : char;
begin
    FCodigoPersona := CodigoPersona;
    FTipoMedioComunicacion := TipoMedioComunicacion;
    FMediosComunicacion := TList.Create;
    FMediosComunicacionQuitados := TList.Create;

    case FormatoMedioComunicacion of
     TMC_TELEFONO : Formato := PeaTypes.TMC_TELEFONO;
     TMC_EMAIL : Formato := PeaTypes.TMC_EMAIL;
     TMC_DIRECCION : Formato := PeaTypes.TMC_DIRECCION;
    else
      raise EMedioComunicacionError.Create(MSG_FORMAT_ERROR,'',ERRORFORMATODEMEDIOCOMUNICACION);
    end;


    with TADOStoredProc.create(nil) do
    try
          try
              Connection:=DMConnections.BaseCAC;

              ProcedureName:='ObtenerMediosComunicacionPersonaFormato';
              with Parameters do begin
                  Refresh;
                  ParamByName('@CodigoPersona').Value:=FCodigoPersona;
                  ParamByName('@Formato').Value:= Formato;
              end;
              Open;


              FMediosComunicacion.Clear;
              if (RecordCount > 0)  then begin
                  First;
                  while not(Eof) do begin
                      New(AItem);
                      AItem^.CodigoArea:=FieldByName('CodigoArea').AsInteger;
                      AItem^.Valor:=trim(FieldByName('Valor').AsString);
                      AItem^.CodigoTipoMedioContacto:=FieldByName('CodigoTipoMedioContacto').AsInteger;
                      AItem^.Anexo:=FieldByName('Anexo').AsInteger;
                      AItem^.HoraDesde:=FieldByName('HorarioDesde').AsDateTime;
                      AItem^.HoraHasta:= FieldByName('HorarioHasta').AsDateTime;
                      AItem^.Indice := FieldByName('CodigoMedioComunicacion').AsInteger;
                      AItem^.Principal := FieldByName('Principal').AsBoolean;
                      FMediosComunicacion.Add(AItem);

                      Next;
                  end;
              end;
          except
              On E: Exception do begin
                 if E.ClassType = Exception then
                      raise EExtendedError.Create(ERROR_CREAROBJETO,e.Message, 0)
                 else raise;
              end;


          end;
    finally
       close;
       free;
    end;
end;

{Procedure Name	: TMediosComunicacionPersona.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: FormatoMedioComunicacion: TFormatosMediosComunicacion
 Return Value   :
}
constructor TMediosComunicacionPersona.Create(FormatoMedioComunicacion: TFormatosMediosComunicacion);
begin
    FTipoMedioComunicacion := TipoMedioComunicacion;
    FMediosComunicacion := TList.Create;
    FMediosComunicacionQuitados := TList.Create;
end;

{Procedure Name	: TMediosComunicacionPersona.Destroy
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
destructor TMediosComunicacionPersona.Destroy;
var  i : integer;
     AItem : PMedioComunicacion;
begin
  for i := 0 to FMediosComunicacion.Count - 1 do begin
   AItem := FMediosComunicacion.Items[i];
   Dispose(AItem);
  end;

  for i := 0 to FMediosComunicacionQuitados.Count - 1 do begin
   AItem := FMediosComunicacionQuitados.Items[i];
   Dispose(AItem);
  end;

  FMediosComunicacion.Clear;
  if Assigned(FMediosComunicacion) then FMediosComunicacion.Free;

  FMediosComunicacionQuitados.Clear;
  if Assigned(FMediosComunicacionQuitados) then FMediosComunicacionQuitados.Free;

  inherited;
end;


{Procedure Name	: TMediosComunicacionPersona.GetIndiceMedioPrincipal
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TMediosComunicacionPersona.GetIndiceMedioPrincipal: Integer;
begin
    if not TieneMedioPrincipal then begin
        raise EMedioComunicacionError.Create('No tiene medio principal.','', ERRORNOTIENEMEDIOPRINCIPAL);
        exit;
    end;

    result := ObtenerIndiceMedioPrincipal;
end;

{Procedure Name	: TMediosComunicacionPersona.GetIndiceMedioSecundario
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TMediosComunicacionPersona.GetIndiceMedioSecundario: Integer;
resourcestring
    MSG_HAVE_NOT_SEC = 'No tiene medio secundario.';
begin
    if not TieneMedioSecundario then begin
        raise EMedioComunicacionError.Create(MSG_HAVE_NOT_SEC,'', ERRORNOTIENEMEDIOSECUNDARIO);
        exit;
    end;

    if ObtenerIndiceMedioPrincipal = -1 then Result := 0
    else Result := 1;
end;

{Procedure Name	: TMediosComunicacionPersona.GetTieneMedioSecundario
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TMediosComunicacionPersona.GetTieneMedioSecundario: Boolean;
begin

    Result := (
            (TieneMedioPrincipal and (FMediosComunicacion.Count > 1)) or
            ((not TieneMedioPrincipal) and (FMediosComunicacion.Count > 0)));

end;

{Procedure Name	: TMediosComunicacionPersona.ExisteMailABorrar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TMediosComunicacionPersona.ExisteMailABorrar: Boolean;
var
    indice : integer;
    AItem : PMedioComunicacion;
begin
    Result := False;
    indice := 0;
    while (indice <= FMediosComunicacionQuitados.Count - 1) and not Result do begin
        AItem := FMediosComunicacionQuitados.Items[indice];
        if AItem^.CodigoTipoMedioContacto = CONST_EMAIL then
            Result := True
        else inc(Indice);
    end;
end;

{Procedure Name	: TMediosComunicacionPersona.Guardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TMediosComunicacionPersona.Guardar;
resourcestring
    MSG_SAVE_ERROR = 'Error al guardar medio';
var  i : integer;
     AItem : PMedioComunicacion;
     HoraDesde,
     HoraHasta: string;
begin
    with TADOStoredProc.Create(nil) do
    try
		try
              Connection    := DMConnections.BaseCAC;
              ProcedureName := 'EliminarMedioComunicacionPersona';
              Parameters.Refresh;
              for i := 0 to FMediosComunicacionQuitados.Count - 1 do begin
                  AItem := FMediosComunicacionQuitados.Items[i];
                  with Parameters, AItem^ do begin
                    ParamByName('@CodigoPersona').Value:= FCodigoPersona;
                    ParamByName('@CodigoMedioComunicacion').Value:=AItem^.Indice;
                  //end;                                                    --TASK_106_JMA_20170206
                  ExecProc;
{INICIO: TASK_106_JMA_20170206}
                if ParamByName('@RETURN_VALUE').Value<>0 then
                    raise Exception.Create(ParamByName('@ErrorDescription').Value );
                  end;
{TERMINO: TASK_106_JMA_20170206}
                  
              end;
              close;

              ProcedureName:='ActualizarMedioComunicacionPersona';
              ExecuteOptions := [eoExecuteNoRecords];
              Parameters.Refresh;

              for i := 0 to FMediosComunicacion.count - 1 do begin
                  AItem := FMediosComunicacion.Items[i];
                  with Parameters do begin
                      ParamByName('@CodigoMedioComunicacion').Value:=AItem^.Indice;
                      ParamByName('@CodigoTipoMedioContacto').Value:= AItem^.CodigoTipoMedioContacto;
                      ParamByName('@CodigoArea').Value := iif(AItem^.CodigoArea = -1, NULL, AItem^.CodigoArea);         // SS_997_PDO_20111014
                      ParamByName('@Valor').Value:=AItem^.Valor;
                      ParamByName('@Anexo').Value := iif(AItem^.Anexo = -1, NULL, AItem^.Anexo);                        // SS_997_PDO_20111014
                      ParamByName('@CodigoDomicilio').Value:=-1;
                      if AItem^.HoraDesde = NullDate then begin
                        ParamByName('@HorarioDesde').Value := null;
                      end
                      else begin
                        HoraDesde := (FormatDateTime('2000-01-01 hh:nn:ss', AItem^.HoraDesde));
                        ParamByName('@HorarioDesde').Value := HoraDesde;
                      end;
                      if AItem^.HoraHasta = NullDate then begin
                        ParamByName('@HorarioHasta').Value := null;
                      end
                      else begin
                        HoraHasta := (FormatDateTime('2001-01-01 hh:nn:ss', AItem^.HoraHasta));
                        ParamByName('@HorarioHasta').Value := HoraHasta;
                      end;


                      //ParamByName('@HorarioDesde').Value := iif(AItem^.HoraDesde = NullDate, NULL, AItem^.HoraDesde);   // SS_997_PDO_20111014
                      //ParamByName('@HorarioHasta').Value := iif(AItem^.HoraHasta = NullDate, NULL, AItem^.HoraHasta);   // SS_997_PDO_20111014
                      ParamByName('@Observaciones').Value:='';
                      ParamByName('@CodigoPersona').Value:= CodigoPersona;
                      ParamByName('@Principal').Value := iif(AItem^.Principal, 1, 0);
                      ParamByName('@EstadoVerificacion').Value:='N';
                  //end;                                        //TASK_106_JMA_20170206
                  ExecProc;
{INICIO: TASK_106_JMA_20170206}
                if ParamByName('@RETURN_VALUE').Value<>0 then
                    raise Exception.Create(ParamByName('@ErrorDescription').Value );
                end;
{TERMINO: TASK_106_JMA_20170206}
                  AItem^.Indice := Parameters.ParamByName('@CodigoMedioComunicacion').Value;
              end;
        except
              On E: Exception do begin
                 if E.ClassType = Exception then
                      raise EMedioComunicacionError.create(MSG_SAVE_ERROR,e.message,0)
                 else raise;
              end;
        end;
    finally
        close;
        free;
    end;
end;

{Procedure Name	: TMediosComunicacionPersona.ObtenerCantidadMediosComunicacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TMediosComunicacionPersona.ObtenerCantidadMediosComunicacion: integer;
begin
    result := FMediosComunicacion.Count;
end;

{Procedure Name	: TMediosComunicacionPersona.ObtenerMedioComunicacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   : TTipoMedioComunicacion
}
function TMediosComunicacionPersona.ObtenerMedioComunicacion(
  Index: integer): TTipoMedioComunicacion;
begin
    result := TTipoMedioComunicacion((FMediosComunicacion.items[index])^);
end;


{Procedure Name	: TMediosComunicacionPersona.ObtenerMediosComunicacionPrincipal
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : TTipoMedioComunicacion
}
function TMediosComunicacionPersona.ObtenerMediosComunicacionPrincipal: TTipoMedioComunicacion;
begin
    result := MediosComunicacion[IndiceMedioPrincipal];
end;

{Procedure Name	: TMediosComunicacionPersona.ObtenerMediosComunicacionSecundario
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : TTipoMedioComunicacion
}
function TMediosComunicacionPersona.ObtenerMediosComunicacionSecundario: TTipoMedioComunicacion;
resourcestring
    MSG_HAVE_NOT_SEC = 'No tiene medio secundario.';
begin
    if not TieneMedioSecundario then
        raise EMedioComunicacionError.Create(MSG_HAVE_NOT_SEC,'', ERRORNOTIENEMEDIOSECUNDARIO);

    result := MediosComunicacion[IndiceMedioSecundario];
end;

{Procedure Name	: TMediosComunicacionPersona.ObtenerIndiceMedioPrincipal
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TMediosComunicacionPersona.ObtenerIndiceMedioPrincipal: Integer;
var
    i: Integer;
begin
    result := -1;
    for i := 0 to CantidadMediosComunicacion - 1 do
        if ObtenerMedioComunicacion(i).Principal then Result := i;

end;


{Procedure Name	: RetornarIndiceTelefonoSecundario
 Author 		: Nelson Droguett Sierra (vpaszkowicz)
 Date Created	: 20-Abril-2010
 Description	: Copio esta funci�n solo para tel�fonos para que me retorne el
                  indice de un secundario.
 Parameters   	:
 Return Value   :  Integer
}
function TMediosComunicacionPersona.RetornarIndiceTelefonoSecundario: Integer;
var
    i: Integer;
begin
    result := -1;
    for i := 0 to CantidadMediosComunicacion - 1 do
        if (Self.TipoMedioComunicacion = TMC_TELEFONO) and (not ObtenerMedioComunicacion(i).Principal) then Result := i;
end;

{Procedure Name	: TMediosComunicacionPersona.QuitarMedioComunicacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   :
}
procedure TMediosComunicacionPersona.QuitarMedioComunicacion(
  Index: integer);
begin
    FMediosComunicacionQuitados.Add(FMediosComunicacion.Items[Index]);
    FMediosComunicacion.Delete(Index);
end;


{Procedure Name	: TMediosComunicacionPersona.SetMediosComunicacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer; const Value: TTipoMedioComunicacion
 Return Value   :
}
procedure TMediosComunicacionPersona.SetMediosComunicacion(Index: Integer; const Value: TTipoMedioComunicacion);
begin
    TTipoMedioComunicacion(FMediosComunicacion.items[index]^) := value;
end;

{Procedure Name	: TMediosComunicacionPersona.GetTieneMedioPrincipal
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TMediosComunicacionPersona.GetTieneMedioPrincipal: Boolean;
begin
    Result := ObtenerIndiceMedioPrincipal >= 0;
end;

{Procedure Name	: TMediosComunicacionPersona.setCodigoPersona
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: integer
 Return Value   :
}
procedure TMediosComunicacionPersona.setCodigoPersona(
  const Value: integer);
begin
  FCodigoPersona := Value;
end;

{ TDomiciliosPersona }

{Procedure Name	: TDomiciliosPersona.AgregarDomicilio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: ADomicilio: TDatosDomicilio
 Return Value   :
}
procedure TDomiciliosPersona.AgregarDomicilio(
  const ADomicilio: TDatosDomicilio);
var
 Domicilio : Pdomicilio;
 i: Integer;
begin
    if ADomicilio.DomicilioEntrega then  // seteo el resto de los domicilios para que no sea de entrega
        for i:=0 to CantidadDomiciliosPersonas-1 do
            TDatosDomicilio(FDomicilios.Items[i]^).DomicilioEntrega:=False;

    New(Domicilio);
    Domicilio^ := ADomicilio;
    FDomicilios.Add(Domicilio);

    if (Domicilio^.CodigoDomicilio=DomicilioFacturacion.CodigoDomicilio) and (Assigned(FOnChangeDomicilioFacturacion)) then
        FOnChangeDomicilioFacturacion(self);
end;

{Procedure Name	: TDomiciliosPersona.EditarDomicilio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer; const ADomicilio: TDatosDomicilio
 Return Value   :
}
procedure TDomiciliosPersona.EditarDomicilio(Index: Integer;
  const ADomicilio: TDatosDomicilio);
begin
    TDatosDomicilio((FDomicilios[Index])^):= ADomicilio;

    if (TDatosDomicilio((FDomicilios[Index])^).CodigoDomicilio = DomicilioFacturacion.CodigoDomicilio) and
        (Assigned(FOnChangeDomicilioFacturacion)) then
            FOnChangeDomicilioFacturacion(self);
end;


{Procedure Name	: TDomiciliosPersona.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoPersona: integer
 Return Value   :
}
constructor TDomiciliosPersona.Create(CodigoPersona: integer);
resourcestring
    MSG_DOM_ERROR = 'Error al crear Domicilio.';
var
 ADomicilio : PDomicilio;
begin
    FCodigoPersona := CodigoPersona;

    FDomicilios := TList.Create;
    FDomiciliosQuitados := TList.Create;
    with TADOStoredProc.Create(nil) do
    try
        try
            Connection:=DMConnections.BaseCAC;
            ProcedureName:='ObtenerDomiciliosPersona';
            with Parameters do begin
                Refresh;
                ParamByName('@CodigoPersona').Value:=CodigoPersona;
                ParamByName('@CodigoDomicilio').Value:=Null;
                ParamByName('@CodigoTipoDomicilio').Value:=Null;
                ParamByName('@Principal').Value:=Null;
            end;
            open;

            FDomicilios.Clear;
            while not(Eof) do begin
                New(ADomicilio);
                with ADomicilio^ do begin
                    CodigoDomicilio:=       FieldByName('CodigoDomicilio').AsInteger;
                    Descripcion:=           FieldByName('DescriCalle').AsString;
                    CodigoTipoDomicilio:=   FieldByName('CodigoTipoDomicilio').AsInteger;

                    if FieldByName('CodigoCalle').IsNull then
                        CodigoCalle :=      -1
                    else
                        CodigoCalle :=      FieldByName('CodigoCalle').AsInteger;

                    CodigoSegmento:=        FieldByName('CodigoSegmento').AsInteger;
                    NumeroCalle:=           FormatearNumeroCalle(FieldByName('Numero').AsString);
                    NumeroCalleSTR:=        FieldByName('Numero').AsString;
                    Detalle:=               FieldByName('Detalle').AsString;
                    CodigoPostal:=          FieldByName('CodigoPostal').AsString;
                    CodigoPais:=            FieldByName('CodigoPais').AsString;
                    CodigoRegion:=          FieldByName('CodigoRegion').AsString;
                    CodigoComuna:=          FieldByName('CodigoComuna').AsString;
                    CalleDesnormalizada:=   FieldByName('CalleDesnormalizada').AsString;
                    Normalizado:=           trim(FieldByName('CalleDesnormalizada').AsString)='';
                    Principal :=            FieldByName('Principal').AsBoolean;
                    Piso :=                 0;
                    Dpto :=                 FieldByName('Dpto').AsString;

                    if FieldByName('Principal').AsBoolean then begin
                        FDomicilioPrincipal := ADomicilio;
                    end;

                end;

                FDomicilios.Add(ADomicilio);

                Next;
            end;
            //Si no hay ningun domicilio, cargo un domicilio de default.
            if FDomicilios.Count = 0 then begin
                New(ADomicilio);
                with ADomicilio^ do begin

                    CodigoDomicilio:=       -1;
                    Descripcion:=           '';
                    CodigoTipoDomicilio:=   0;
                    CodigoCalle :=          -1;
                    CodigoSegmento:=        0;
                    NumeroCalle:=           0;
                    NumeroCalleSTR:=        '';
                    Detalle:=               '';
                    CodigoPostal:=          '';
                    CodigoPais:=            PAIS_CHILE;
                    CodigoRegion:=          REGION_SANTIAGO;
                    CodigoComuna:=          '';
                    CalleDesnormalizada:=   '';
                    Normalizado:=           false;
                    Principal :=            false;
                    Piso :=                 0;
                    Dpto :=                 '';

                end;
                FDomicilios.Add(ADomicilio);
            end;
        except
            on E: Exception do begin
              if E.ClassType = Exception then
                raise EDomicilioError.Create(MSG_DOM_ERROR, e.Message, 0)
              else raise;
            end;
        end;
    finally
        close;
        free;
    end;
end;

{Procedure Name	: TDomiciliosPersona.Destroy
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
destructor TDomiciliosPersona.Destroy;
var  i : integer;
begin
  for i := 0 to FDomicilios.Count - 1 do
       Dispose(FDomicilios.Items[i]);

  for i := 0 to FDomiciliosQuitados.Count - 1 do
       Dispose(FDomiciliosQuitados.Items[i]);
  FDomicilios.Clear;
  if Assigned(FDomicilios) then FDomicilios.Free;

  FDomiciliosQuitados.Clear;
  if Assigned(FDomiciliosQuitados) then FDomiciliosQuitados.Free;

  inherited;
end;



{Procedure Name	: TDomiciliosPersona.Guardar
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
 FIXME --- Revisar esto.
}
procedure TDomiciliosPersona.Guardar;
var
 i : integer;
 ADomicilio : PDomicilio;
 spDomicilio, spDomicilioPersona : TADOStoredProc;
 spDomiciliosPersonaQuitados : TAdoStoredProc;
begin
        spDomicilio := TADOStoredProc.Create(nil);
        spDomicilio.Connection := DMConnections.BaseCAC;
        spDomicilio.ProcedureName := 'ActualizarDomicilio';
        spDomicilio.ExecuteOptions := [eoExecuteNoRecords];
        spDomicilio.Parameters.Refresh;

        spDomicilioPersona := TADOStoredProc.Create(nil);
        spDomicilioPersona.Connection := DMConnections.BaseCAC;
        spDomicilioPersona.ProcedureName := 'ActualizarPersonasDomicilios';
        spDomicilioPersona.ExecuteOptions := [eoExecuteNoRecords];
        spDomicilioPersona.Parameters.Refresh;

        spDomiciliosPersonaQuitados := TADOStoredProc.Create(nil);
        spDomiciliosPersonaQuitados.Connection := DMConnections.BaseCAC;
        spDomiciliosPersonaQuitados.ProcedureName := 'EliminarPersonasDomicilios';
        spDomiciliosPersonaQuitados.ExecuteOptions := [eoExecuteNoRecords];
        spDomiciliosPersonaQuitados.Parameters.Refresh;
        try
{INICIO: TASK_053_JMA_20160930 (Se eliminan los with)}
           for i := 0 to FDomiciliosQuitados.Count - 1 do begin
                ADomicilio := FDomiciliosQuitados.Items[i];

                spDomiciliosPersonaQuitados.Parameters.ParamByName('@CodigoPersona').Value:= CodigoPersona;
                spDomiciliosPersonaQuitados.Parameters.ParamByName('@CodigoDomicilio').Value:= ADomicilio^.CodigoDomicilio;
                spDomiciliosPersonaQuitados.ExecProc;
{INICIO: TASK_106_JMA_20170206}
                if spDomiciliosPersonaQuitados.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                    raise Exception.Create(spDomiciliosPersonaQuitados.Parameters.ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}
                spDomiciliosPersonaQuitados.Close;
            end;


            for i := 0 to FDomicilios.Count - 1 do begin
                ADomicilio := FDomicilios.Items[i];

                if ADomicilio^.CodigoTipoDomicilio<0 then
                    spDomicilio.Parameters.ParamByName('@CodigoTipoDomicilio').Value:=null
                else
                    spDomicilio.Parameters.ParamByName('@CodigoTipoDomicilio').Value:=ADomicilio^.CodigoTipoDomicilio;
                spDomicilio.Parameters.ParamByName('@CodigoPais').Value:=PAIS_CHILE;
                spDomicilio.Parameters.ParamByName('@CodigoRegion').Value:= ADomicilio^.CodigoRegion;
                spDomicilio.Parameters.ParamByName('@CodigoComuna').Value:= ADomicilio^.CodigoComuna;
                spDomicilio.Parameters.ParamByName('@DescripcionCiudad').Value:=null;
                spDomicilio.Parameters.ParamByName('@CodigoCalle').Value:=ADomicilio^.CodigoCalle;
                spDomicilio.Parameters.ParamByName('@CodigoSegmento').Value:=ADomicilio^.CodigoSegmento;
                spDomicilio.Parameters.ParamByName('@CalleDesnormalizada').Value:=iif(ADomicilio^.CodigoCalle>=0,null,ADomicilio^.CalleDesnormalizada);
                spDomicilio.Parameters.ParamByName('@Numero').Value := ADomicilio^.NumeroCalleSTR;
                spDomicilio.Parameters.ParamByName('@Piso').Value:=ADomicilio^.Piso;
                spDomicilio.Parameters.ParamByName('@Depto').Value:=ADomicilio^.Dpto;
                spDomicilio.Parameters.ParamByName('@CodigoPostal').Value:=ADomicilio^.CodigoPostal;
                spDomicilio.Parameters.ParamByName('@Detalle').Value:=ADomicilio^.Detalle;
                spDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value:=ADomicilio^.CodigoDomicilio;
                spDomicilio.ExecProc;
{INICIO: TASK_106_JMA_20170206}
                if spDomicilio.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                    raise Exception.Create(spDomicilio.Parameters.ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}
                ADomicilio^.CodigoDomicilio := spDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value;
                spDomicilio.close;

                spDomicilioPersona.Parameters.ParamByName('@CodigoPersona').Value:=CodigoPersona;
                spDomicilioPersona.Parameters.ParamByName('@CodigoDomicilio').Value:=ADomicilio^.CodigoDomicilio;
                spDomicilioPersona.Parameters.ParamByName('@Principal').Value:=ADomicilio^.Principal;
                spDomicilioPersona.Parameters.ParamByName('@Activo').Value:=1;
                spDomicilioPersona.ExecProc;
{INICIO: TASK_106_JMA_20170206}
                if spDomicilioPersona.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                    raise Exception.Create(spDomicilioPersona.Parameters.ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}
                spDomicilioPersona.close;
            end;
 {TERMINO: TASK_053_JMA_20160930}
        finally
            spDomicilio.Free;
            spDomicilioPersona.Free;
            spDomiciliosPersonaQuitados.Free;
        end;
end;


{Procedure Name	: TDomiciliosPersona.ObtenerCantidadDomiciliosPersonas
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TDomiciliosPersona.ObtenerCantidadDomiciliosPersonas: integer;
begin
    result := FDomicilios.Count;
end;

{Procedure Name	: TDomiciliosPersona.ObtenerDomicilioFacturacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : TDatosDomicilio
}
function TDomiciliosPersona.ObtenerDomicilioFacturacion: TDatosDomicilio;
var
    i:Integer;
begin
    for i:=0 to CantidadDomiciliosPersonas-1 do
        if TDatosDomicilio(FDomicilios.Items[i]^).DomicilioEntrega then result:=TDatosDomicilio(FDomicilios.Items[i]^);

end;

{Procedure Name	: TDomiciliosPersona.ObtenerDomicilioPersonas
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer
 Return Value   : TDatosDomicilio
}
function TDomiciliosPersona.ObtenerDomicilioPersonas(Index: Integer): TDatosDomicilio;
resourcestring
    MSG_INDEX_NOT_VALID = 'Error, indice de Domicilio no valido';
begin
    if Index > FDomicilios.Count then begin
        raise Exception.Create(MSG_INDEX_NOT_VALID);
        exit;
    end;

    result := TDatosDomicilio((FDomicilios.Items[Index])^);
end;

{Procedure Name	: TDomiciliosPersona.ObtenerDomicilioPrincipal
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : TDatosDomicilio
}
function TDomiciliosPersona.ObtenerDomicilioPrincipal: TDatosDomicilio;
begin
  if Assigned(FDomicilioPrincipal) then result := TDatosDomicilio(FDomicilioPrincipal^);
end;

{Procedure Name	: TDomiciliosPersona.QuitarDomicilio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   :
}
procedure TDomiciliosPersona.QuitarDomicilio(Index: integer);
var
    BorraFacturacion:Boolean;
begin
    BorraFacturacion:=(TDatosDomicilio((FDomicilios[Index])^).CodigoDomicilio=DomicilioFacturacion.CodigoDomicilio);

    if (FDomicilioPrincipal = FDomicilios.Items[Index]) then
        FDomicilioPrincipal := nil;

    FDomiciliosQuitados.Add(FDomicilios.Items[Index]);
    FDomicilios.Delete(Index);

    if (BorraFacturacion) and (Assigned(FOnChangeDomicilioFacturacion)) then
        FOnChangeDomicilioFacturacion(self);

end;

{Procedure Name	: TDomiciliosPersona.SetearDomicilioFacturacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoDomicilioFacturacion: integer
 Return Value   :
}
procedure TDomiciliosPersona.SetearDomicilioFacturacion(
  CodigoDomicilioFacturacion: integer);
var i : integer;
    ADomicilio : PDomicilio;
begin
  for i := 0 to FDomicilios.Count - 1 do begin
     ADomicilio := FDomicilios.items[i];
     if ADomicilio^.CodigoDomicilio = CodigoDomicilioFacturacion then begin
        TDatosDomicilio(FDomicilios.Items[i]^).DomicilioEntrega:=True;
     end else TDatosDomicilio(FDomicilios.Items[i]^).DomicilioEntrega:=False;
  end;
end;

{Procedure Name	: TDomiciliosPersona.VerificarTieneDomicilioFacturacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : boolean
}
function TDomiciliosPersona.VerificarTieneDomicilioFacturacion: boolean;
begin
    result := IndiceDomicilioFacturacion > 0 ;
end;

{Procedure Name	: TDomiciliosPersona.VerificarTieneDomicilioPrincipal
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : boolean
}
function TDomiciliosPersona.VerificarTieneDomicilioPrincipal: boolean;
begin
    result := IndiceDomicilioPrincipal >= 0;
end;

{Procedure Name	: TDomiciliosPersona.SetOnChangeDomicilioFacturacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: TNotifyEvent
 Return Value   :
}
procedure TDomiciliosPersona.SetOnChangeDomicilioFacturacion(
  const Value: TNotifyEvent);
begin
  FOnChangeDomicilioFacturacion := Value;
end;

{Procedure Name	:  TDomiciliosPersona.GetIndiceDomicilioFacturacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TDomiciliosPersona.GetIndiceDomicilioFacturacion: integer;
var
    i:Integer;
begin
    result:=-1;
    for i:=0 to CantidadDomiciliosPersonas-1 do
        if ObtenerDomicilioPersonas(i).DomicilioEntrega then result:=i;

    if (result = -1) and (CantidadDomiciliosPersonas>=1) then result := 0;
end;

{Procedure Name	: TDomiciliosPersona.GetIndiceDomicilioPrincipal
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TDomiciliosPersona.GetIndiceDomicilioPrincipal: integer;
var
    i:Integer;
begin
    result:=-1;
    for i:=0 to CantidadDomiciliosPersonas-1 do
        if FDomicilios[i] = FDomicilioPrincipal then result:=i;
end;


{Procedure Name	: TDomiciliosPersona.ObtenerIndiceDomicilio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoDomicilio: Integer
 Return Value   : Integer
}
function TDomiciliosPersona.ObtenerIndiceDomicilio(
  CodigoDomicilio: Integer): Integer;
var
    i: Integer;
begin
    Result := -1;
    for i := 0 to CantidadDomiciliosPersonas -1 do
        if Domicilios[i].CodigoDomicilio = CodigoDomicilio then
            Result := i;
end;

{ TCuentasConvenio }
{Procedure Name	: TCuentasConvenio.AgregarDocumentacionVehiculo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: IndexVehiculo: integer; ADocumentacion: TRegistroDocumentacion
 Return Value   :
}
procedure TCuentasConvenio.AgregarDocumentacionVehiculo(
  IndexVehiculo: integer; ADocumentacion: TRegistroDocumentacion);
var
 Vehiculo : TVehiculosConvenio;
 Documentacion : PRegistroDocumentacion;
begin
    New(Documentacion);
    Documentacion^ := ADocumentacion;

    Vehiculo := TVehiculosConvenio((FVehiculos.Items[IndexVehiculo])^);
    Vehiculo.Documentacion.Add(Documentacion);
end;

{Procedure Name	: TCuentasConvenio.AgregarVehiculo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const AVehiculo: TVehiculosConvenio
 Return Value   :
}
procedure TCuentasConvenio.AgregarVehiculo(const AVehiculo: TVehiculosConvenio);
resourcestring
    MSG_VEHICULO_YA_ASIGNADO = 'El Veh�culo ya se encuentra asignado a un convenio';
    MSG_CUENTA_SUSPENDIDA = 'La cuenta ya fue suspendida';
var
 Vehiculo : PVehiculosConvenio;
 i : Integer;
 OtroConvenio: Integer; // 20160603 MGO
begin
    { INICIO : 20160603 MGO
     if (ObtenerVehiculoAsignadoConvenio(AVehiculo.Cuenta.Vehiculo.CodigoTipoPatente,
           AVehiculo.Cuenta.Vehiculo.Patente) > 0) and
        (ObtenerVehiculoAsignadoConvenio(AVehiculo.Cuenta.Vehiculo.CodigoTipoPatente,
          AVehiculo.Cuenta.Vehiculo.Patente) <> CodigoConvenio ) then
            raise ECuentaError.Create(MSG_VEHICULO_YA_ASIGNADO,'',ERRORVEHICULOYAASIGNADO);
    }
    if not AVehiculo.EsForaneo then begin
        OtroConvenio := ObtenerVehiculoAsignadoConvenio(AVehiculo.Cuenta.Vehiculo.CodigoTipoPatente, AVehiculo.Cuenta.Vehiculo.Patente);
        if (OtroConvenio > 0) and (OtroConvenio <> CodigoConvenio) then
            raise ECuentaError.Create(MSG_VEHICULO_YA_ASIGNADO,'',ERRORVEHICULOYAASIGNADO);
    end;
    // FIN : 20160603 MGO
    for i := 0 to FVehiculos.Count - 1 do begin
        if (TVehiculosConvenio(FVehiculos.items[i]^).Cuenta.ContactSerialNumber = AVehiculo.Cuenta.ContactSerialNumber)
            and (TVehiculosConvenio(FVehiculos.items[i]^).Cuenta.EstaSuspendida) then begin
                raise ECuentaError.Create(MSG_CUENTA_SUSPENDIDA,'',ERRORCUENTASUSPENDIDA);
       end;
    end;
    New(Vehiculo);
    Vehiculo^ := AVehiculo;
    Vehiculo^.Documentacion := TList.Create;
    Vehiculo^.EsVehiculoNuevo := True;
    Vehiculo^.EsTagNuevo := True;
    Vehiculo^.EsCambioVehiculo := False;
    Vehiculo^.Cuenta.FechaBajaCuenta := NullDate;
    Vehiculo^.EsActivacionCuenta := False;
    Vehiculo^.EsTagPerdido := False;
    Vehiculo^.EsCambioTag := False;
    FVehiculos.Add(Vehiculo);
end;

{Procedure Name	: TCuentasConvenio.Create
 Author 		: Nelson Droguett Sierra (dAllegretti,vpaszkowicz)
 Date Created	: 20-Abril-2010
 Description	: Se elimina el uso de la FechaModifCuenta ya que pasa a ser uso exclusivo de la auditoria
                  Agrego un nuevo boolean al veh�culo para representar si
        cambio el estado de la cuenta, o sea, si estaba alta y pas� a suspendida. etc-
        Se va a usar para habilitar los botones de la barra de modificaci�n.
        Ej: si suspendo una cuenta pero no grab�, no puedo cambiar de veh�culo.
        En caso contrario es posible.
 Parameters   	: CodigoConvenio: Integer; PuntoEntrega: Integer
 Return Value   :
}
constructor TCuentasConvenio.Create(CodigoConvenio: Integer; PuntoEntrega: Integer);
resourcestring
    MSG_CREATE_ERROR = 'Error al crear la cuenta.';
var
 spVehiculos : TADOStoredProc;
 AVehiculo : PVehiculosConvenio;
begin
 try
    FCodigoConvenio := CodigoConvenio;
    FVehiculos := TList.Create;
    FVehiculosEliminados := TList.Create;
    FVehiculosQuitados := TList.Create;
    FPuntoEntrega := PuntoEntrega;
    FVehiculosCambiarEstadoTags := TList.Create;
    FVehiculosTagsPerdidos := Tlist.Create;
    FCuentasActivadas := Tlist.Create;
    FVehiculosTagsACobrar := TList.Create;
    FVehiculosEliminadosSinGuardar := TList.Create;

    spVehiculos := TADOStoredProc.Create(nil);
    spVehiculos.Connection:=DMConnections.BaseCAC;
    spVehiculos.ProcedureName:='ObtenerCuentas';
    spVehiculos.Parameters.Refresh;

    // Inicializar la variable para indicar que todas las cuentas est�n
    // dadas de Baja o Suspendidas.
    // Las inicializo con True, pues si por alg�n motivo el convenio NO tiene
    // Cuentas, entonces digo que todas las cuentas est�n dadas de Baja o
    // Suspendidas.
    FEstanTodasDadasBajaOSuspendidas := True;
    try
            with spVehiculos do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                Parameters.ParamByName('@TraerBaja').Value := 1;
                open;

                if spVehiculos.RecordCount = 0 then exit;
                FVehiculos.Clear;
                FVehiculosEliminados.Clear;
                while not eof do begin
                    New(AVehiculo);
                    with AVehiculo^ do begin
                         EsVehiculoNuevo := False;
                         EsVehiculoModificado := False;
                         EsTagNuevo := False;
                         EsCambioVehiculo := False;
                         EsActivacionCuenta := False;
                         EsTagRecuperado := False;
                         EsCambioTag := False;
                         EsCambioEstado := False;
                         EsTagPerdido := fieldbyname('TagPerdido').AsBoolean;
                         EsTagVendido := fieldbyname('Vendido').AsBoolean;
                         //Rev.1
                         //Cuenta.HabilitadoAMB              := FieldByName('HabilitadoAMB').AsBoolean;       //SS-1006-NDR-20111105
                         //Cuenta.AdheridoPA                 := FieldByName('AdheridoPA').AsBoolean;            //SS-1006-NDR-20120614 //SS-1006-NDR-20111105 TASK_004_ECA_20160411
                         Cuenta.IndiceVehiculo             := fieldbyname('IndiceVehiculo').AsInteger;//fieldbyname('CodigoVehiculo').AsInteger;
                         Cuenta.Vehiculo.Patente           := fieldbyname('Patente').AsString;
                         Cuenta.Vehiculo.CodigoTipoPatente := fieldbyname('TipoPatente').AsString;
                         //Cuenta.Vehiculo.PatenteRVM        := fieldbyname('PatenteRVM').AsString;                         //TASK_106_JMA_20170206
                         //Cuenta.Vehiculo.DigitoPatenteRVM  := fieldbyname('DigitoVerificadorPatenteRVM').AsString;        //TASK_106_JMA_20170206
                         Cuenta.Vehiculo.CodigoVehiculo    := fieldbyname('CodigoVehiculo').AsInteger;
                         //Rev.5 / 27-Mayo-2010---------------------------------
                         //if fieldbyname('DigitoVerificadorPatente').AsString = '' then
                         //   Cuenta.Vehiculo.DigitoPatente := QueryGetValue(DMConnections.BaseCAC, format('Exec ObtenerDigitoVerificadorPatente ''%s''',[ Trim(fieldbyname('Patente').AsString)]))
                         //else
                         Cuenta.Vehiculo.DigitoPatente := fieldbyname('DigitoVerificadorPatente').AsString;
                         //FinRev.5---------------------------------------------

                         Cuenta.Vehiculo.CodigoMarca   := fieldbyname('CodigoMarca').AsInteger;
                         Cuenta.Vehiculo.Marca         := fieldbyname('DescripcionMarca').AsString;
                         Cuenta.Vehiculo.Modelo        := fieldbyname('Modelo').AsString;
{INICIO: TASK_106_JMA_20170206
                         Cuenta.Vehiculo.CodigoTipo    := fieldbyname('CodigoTipoVehiculo').Value;
                         Cuenta.Vehiculo.Tipo          := fieldbyname('DescripcionTipoVehiculo').Value;
}
                         Cuenta.CodigoCategoriaInterurbana := fieldbyname('CodigoCategoriaInterurbana').Value;
                         Cuenta.CodigoCategoriaUrbana  := fieldbyname('CodigoCategoriaUrbana').Value;
                         Cuenta.CategoriaInterurbana := fieldbyname('CategoriaInterurbana').Value;
                         Cuenta.CategoriaUrbana  := fieldbyname('CategoriaUrbana').Value;
{TERMINO: TASK_106_JMA_20170206}
                         Cuenta.Vehiculo.Anio          := fieldbyname('AnioVehiculo').AsString;
                         Cuenta.Vehiculo.Propiedad     := '';
                         Cuenta.Vehiculo.Robado        := fieldbyname('Robado').AsBoolean;
                         Cuenta.Vehiculo.Foraneo       := False;    // 20160603 MGO
                         Cuenta.ContextMark            := fieldbyname('ContextMark').Value;
                         Cuenta.ContactSerialNumber    := fieldbyname('ContractSerialNumber').Value;
                         Cuenta.ContactSerialNumberOtraConcesionaria := fieldbyname('ContractSerialNumberOtraConsecionaria').Value;
                         Cuenta.TieneAcoplado          := fieldbyname('TieneAcoplado').Value;
                         Cuenta.FechaBajaCuenta        := iif(fieldbyname('FechaBajaCuenta').IsNull, NullDate, fieldbyname('FechaBajaCuenta').AsDateTime);
                         Cuenta.FechaCreacion          := fieldbyname('FechaAltaCuenta').Value;
                         Cuenta.EstadoConservacionTAG  := fieldbyname('CodigoEstadoConservacion').Value;
                         Cuenta.IndicadorListas.Verde  := fieldbyname('IndicadorListaVerde').Value;
                         Cuenta.IndicadorListas.Negra  := fieldbyname('IndicadorListaNegra').Value;
                         Cuenta.IndicadorListas.Gris   := fieldbyname('IndicadorListaGris').Value;
                         Cuenta.IndicadorListas.Amarilla  := fieldbyname('IndicadorListaAmarilla').Value;

{INICIO: TASK_036_JMA_20160707}
                         Cuenta.SuscritoListaBlanca  := fieldbyname('SuscritoListaBlanca').Value;
                         Cuenta.HabilitadoListaBlanca  := fieldbyname('HabilitadoListaBlanca').Value;
{TERMINO: TASK_036_JMA_20160707}

                         Cuenta.DatosConvenioFacturacion := fieldbyname('DatosConvenioFacturacion').Value; //   TASK_014_RME_20170112

                         if not fieldbyname('CodigoConvenioFacturacion').IsNull then                           //TASK_005_ECA_20160420
                            Cuenta.CodigoConvenioFacturacion:= fieldbyname('CodigoConvenioFacturacion').Value; //TASK_004_ECA_20160411 TASK_005_ECA_20160420

                          Cuenta.CodigoConvenioRNUT:=fieldbyname('CodigoConvenio').Value;   //TASK_009_ECA_20160506

                          if not fieldbyname('CodigoColor').IsNull then                     //TASK_016_ECA_20160526
                            Cuenta.Vehiculo.CodigoColor:= fieldbyname('CodigoColor').Value; //TASK_016_ECA_20160526

                         //Tengo que guardar esta variable para ver despu�s si est� suspendida o no.
                         Cuenta.EstaSuspendida := (FieldByName('CodigoEstadoCuenta').Value = ESTADO_CUENTA_VEHICULO_SUSPENDIDA);

                         if fieldbyname('Suspendida').IsNull then Cuenta.Suspendida := NullDate
                         else Cuenta.Suspendida := fieldbyname('Suspendida').AsDateTime;

                         if fieldbyname('SuspensionForzada').IsNull then Cuenta.SuspensionForzada := False
                         else Cuenta.SuspensionForzada := fieldbyname('SuspensionForzada').AsBoolean;

                         if fieldbyname('DeadLineSuspension').IsNull then Cuenta.DeadLineSuspension := NullDate
                         else Cuenta.DeadLineSuspension := fieldbyname('DeadLineSuspension').AsDateTime;

                         if fieldbyname('FechaAltaTAG').IsNull then
                            Cuenta.FechaAltaTag := NullDate
                         else
                            Cuenta.FechaAltaTag := fieldbyname('FechaAltaTAG').Value;

                         if fieldbyname('FechaBajaTAG').IsNull then
                            Cuenta.FechaBajaTag := NullDate
                         else
                            Cuenta.FechaBajaTag := fieldbyname('FechaBajaTAG').Value;

                         if fieldbyname('FechaVencimientoTAG').IsNull then
                            Cuenta.FechaVencimientoTag := NullDate
                         else
                            Cuenta.FechaVencimientoTag := fieldbyname('FechaVencimientoTAG').Value;

                         (* Obtener los datos de la eximici�n de facturaci�n. *)
                         Cuenta.EximirFacturacion.ExisteEnBaseDeDatos := not FieldByName('FechaInicioEximicionFacturacion').IsNull;

                         if FieldByName('FechaInicioEximicionFacturacion').IsNull then begin
                            Cuenta.EximirFacturacion.FechaInicioOriginal := NullDate;
                            Cuenta.EximirFacturacion.FechaInicio := NullDate;
                         end else begin
                            Cuenta.EximirFacturacion.FechaInicioOriginal := FieldByName('FechaInicioEximicionFacturacion').AsDateTime;
                            Cuenta.EximirFacturacion.FechaInicio := FieldByName('FechaInicioEximicionFacturacion').AsDateTime;
                         end;

                         if FieldByName('FechaFinalizacionEximicionFacturacion').IsNull then
                            Cuenta.EximirFacturacion.FechaFinalizacion := NullDate
                         else
                            Cuenta.EximirFacturacion.FechaFinalizacion := FieldByName('FechaFinalizacionEximicionFacturacion').AsDateTime;

                         if FieldByName('CodigoMotivoEximicionFacturacion').IsNull then
                            Cuenta.EximirFacturacion.CodigoMotivoNoFacturable := 0
                         else
                            Cuenta.EximirFacturacion.CodigoMotivoNoFacturable := FieldByName('CodigoMotivoEximicionFacturacion').AsInteger;

                         if FieldByName('CodigoUsuarioEximicionFacturacion').IsNull then
                            Cuenta.EximirFacturacion.Usuario := EmptyStr
                         else
                            Cuenta.EximirFacturacion.Usuario := FieldByName('CodigoUsuarioEximicionFacturacion').AsString;

                         if FieldByName('FechaHoraModificacionEximicionFacturacion').IsNull then
                            Cuenta.EximirFacturacion.FechaHoraModificacion := NullDate
                         else
                            Cuenta.EximirFacturacion.FechaHoraModificacion := FieldByName('FechaHoraModificacionEximicionFacturacion').AsDateTime;

                         Cuenta.EximirFacturacion.FechaInicioEditable := (Cuenta.EximirFacturacion.FechaInicio = NullDate)
                                                                         or (Cuenta.EximirFacturacion.FechaInicio >= NowBase(DMConnections.BaseCAC));
                         Cuenta.EximirFacturacion.FechaFinalizacionEditable := (Cuenta.EximirFacturacion.FechaFinalizacion = NullDate)
                                                                         or (Cuenta.EximirFacturacion.FechaFinalizacion >= NowBase(DMConnections.BaseCAC));

                         (* Obtener los datos de la bonificaci�n de facturaci�n. *)
                         Cuenta.BonificarFacturacion.ExisteEnBaseDeDatos := not FieldByName('FechaInicioBonificacionFacturacion').IsNull;

                         if FieldByName('FechaInicioBonificacionFacturacion').IsNull then begin
                            Cuenta.BonificarFacturacion.FechaInicioOriginal := NullDate;
                            Cuenta.BonificarFacturacion.FechaInicio := NullDate;
                         end else begin
                            Cuenta.BonificarFacturacion.FechaInicioOriginal := FieldByName('FechaInicioBonificacionFacturacion').AsDateTime;
                            Cuenta.BonificarFacturacion.FechaInicio := FieldByName('FechaInicioBonificacionFacturacion').AsDateTime;
                         end;

                         if FieldByName('FechaFinalizacionBonificacionFacturacion').IsNull then
                            Cuenta.BonificarFacturacion.FechaFinalizacion := NullDate
                         else
                            Cuenta.BonificarFacturacion.FechaFinalizacion := FieldByName('FechaFinalizacionBonificacionFacturacion').AsDateTime;

                         if FieldByName('PorcentajeBonificacionFacturacion').IsNull then
                            Cuenta.BonificarFacturacion.Porcentaje := 0
                         else
                            Cuenta.BonificarFacturacion.Porcentaje := FieldByName('PorcentajeBonificacionFacturacion').AsInteger;

                         if FieldByName('ImporteBonificacionFacturacion').IsNull then
                            Cuenta.BonificarFacturacion.Importe := 0
                         else
                            Cuenta.BonificarFacturacion.Importe := FieldByName('ImporteBonificacionFacturacion').AsInteger;

                         if FieldByName('CodigoUsuarioBonificacionFacturacion').IsNull then
                            Cuenta.BonificarFacturacion.Usuario := EmptyStr
                         else
                            Cuenta.BonificarFacturacion.Usuario := FieldByName('CodigoUsuarioBonificacionFacturacion').AsString;

                         if FieldByName('FechaHoraModificacionBonificacionFacturacion').IsNull then
                            Cuenta.BonificarFacturacion.FechaHoraModificacion := NullDate
                         else
                            Cuenta.BonificarFacturacion.FechaHoraModificacion := FieldByName('FechaHoraModificacionBonificacionFacturacion').AsDateTime;


                         Cuenta.BonificarFacturacion.FechaInicioEditable := (Cuenta.BonificarFacturacion.FechaInicio = NullDate)
                                                                            or (Cuenta.BonificarFacturacion.FechaInicio >= NowBase(DMConnections.BaseCAC));
                         Cuenta.BonificarFacturacion.FechaFinalizacionEditable := (Cuenta.BonificarFacturacion.FechaFinalizacion = NullDate)
                                                                            or (Cuenta.BonificarFacturacion.FechaFinalizacion >= NowBase(DMConnections.BaseCAC));

                         AVehiculo^.EsCambioVehiculo := False;

                         Cuenta.CodigoAlmacenDestino := FieldByName('CodigoAlmacen').AsInteger;

                         if fieldbyname('CodigoEstadoCuenta').AsInteger = ESTADO_CUENTA_VEHICULO_BAJA then
                             FVehiculosEliminados.Add(AVehiculo)
                         else
                             FVehiculos.Add(AVehiculo);

                         // Almacenar si todas las Cuentas del Convenio est�n
                         // dadas de Baja o Suspendidas.
                         FEstanTodasDadasBajaOSuspendidas := FEstanTodasDadasBajaOSuspendidas
                                and ( (FieldByName('CodigoEstadoCuenta').AsInteger = ESTADO_CUENTA_VEHICULO_BAJA)
                                    or (FieldByName('CodigoEstadoCuenta').AsInteger = ESTADO_CUENTA_VEHICULO_SUSPENDIDA)
                                    );
                         Documentacion := TList.Create;
                    end;
                    next;
                end;
                close;
            end;
    finally
           spVehiculos.Free;
    end;
 except
    On E: Exception do begin
       if E.ClassType = Exception then
            Raise ECuentaError.Create(MSG_CREATE_ERROR, e.Message, 0)
       else raise;
    end;
 end;
end;



{Procedure Name	: TCuentasConvenio.Destroy
 Author 		: Nelson Droguett Sierra (vpaszkowicz)
 Date Created	: 20-Abril-2010
 Description	: Vac�o primero la lista de veh�culosTagsACobrar, ya que ten-
    dr� punteros compartidos con todas las dem�s.
 Parameters   	:
 Return Value   :
}
destructor TCuentasConvenio.Destroy;
var  i,j : integer;
     AVehiculo : PVehiculosConvenio;
     VehiculoRecienEliminado: Boolean;
begin
    //Primero vacio esta lista, ya que tiene punterios compartidos.
    FVehiculosTagsACobrar.Clear;

  for i := 0 to FCuentasActivadas.Count - 1 do begin
        AVehiculo := FCuentasActivadas.Items[i];
        Dispose(AVehiculo);
  end;
  FCuentasActivadas.Clear;
  FCuentasActivadas.Free;

  for i := 0 to FVehiculosTagsPerdidos.Count - 1 do begin
        AVehiculo := FVehiculosTagsPerdidos.Items[i];
        Dispose(AVehiculo);
  end;
  FVehiculosTagsPerdidos.Clear;
  FVehiculosTagsPerdidos.Free;

  for i := 0 to FVehiculosCambiarEstadoTags.Count - 1 do begin
        AVehiculo := FVehiculosCambiarEstadoTags.Items[i];
        Dispose(AVehiculo);
  end;
  FVehiculosCambiarEstadoTags.Clear;
  FVehiculosCambiarEstadoTags.Free;

  for i := 0 to FVehiculos.Count - 1 do begin
     AVehiculo := FVehiculos.Items[i];
     if Assigned(AVehiculo^.Documentacion) then begin
         for j := 0 to AVehiculo^.Documentacion.Count - 1 do
                 Dispose(AVehiculo^.Documentacion.Items[j]);
         AVehiculo^.Documentacion.Clear;
         AVehiculo^.Documentacion.Free;
     end;
     Dispose(AVehiculo);
  end;
  FVehiculos.Clear;
  FVehiculos.Free;

  for i := 0 to FVehiculosEliminados.Count - 1 do begin
     VehiculoRecienEliminado := False;
     for j := 0 to FVehiculosQuitados.Count - 1 do begin
         if (Trim(TVehiculosConvenio(FVehiculosEliminados.items[i]^).Cuenta.Vehiculo.Patente) = Trim(TVehiculosConvenio(FVehiculosQuitados.items[j]^).Cuenta.Vehiculo.Patente)) then begin
             VehiculoRecienEliminado := True;
             Break;
        end;
     end;
     if VehiculoRecienEliminado then //Si el vehiculo se elimino en los cambios actuales no lo elimino. Se elimina en VehiculosQuitados
        Continue;
     AVehiculo := FVehiculosEliminados.Items[i];
     if assigned(AVehiculo^.Documentacion) then begin
         for j := 0 to AVehiculo^.Documentacion.Count - 1 do
                 Dispose(AVehiculo^.Documentacion.Items[j]);
         AVehiculo^.Documentacion.Clear;
         AVehiculo^.Documentacion.Free;
     end;
     Dispose(AVehiculo);
  end;
  FVehiculosEliminados.Clear;
  FVehiculosEliminados.Free;

  for i := 0 to FVehiculosQuitados.Count - 1 do begin
     AVehiculo := FVehiculosQuitados.Items[i];
     if Assigned(AVehiculo) then begin
         if assigned(AVehiculo^.Documentacion) then begin
             for j := 0 to AVehiculo^.Documentacion.Count - 1 do
                     Dispose(AVehiculo^.Documentacion.Items[j]);
             AVehiculo^.Documentacion.Clear;
             AVehiculo^.Documentacion.Free;
         end;
         Dispose(AVehiculo);
     end;
  end;
  FVehiculosQuitados.Clear;
  FVehiculosQuitados.Free;

  for i := 0 to FVehiculosTagsACobrar.Count -1 do begin
     AVehiculo := FVehiculosTagsACobrar.Items[i];
     Dispose(AVehiculo);
  end;

  FVehiculosTagsACobrar.Clear;
  FVehiculosTagsACobrar.Free;

  for i := 0 to FVehiculosEliminadosSinGuardar.Count -1 do begin
     AVehiculo := FVehiculosEliminadosSinGuardar.Items[i];
     Dispose(AVehiculo);
  end;
  if Assigned(FVehiculosEliminadosSinGuardar) then FVehiculosEliminadosSinGuardar.Free;

  inherited;
end;

{Procedure Name	: TCuentasConvenio.CalcularVencimientoGarantiaTag
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: AVehiculo: PVehiculosConvenio
 Return Value   : TDateTime
}
function TCuentasConvenio.CalcularVencimientoGarantiaTag(AVehiculo: PVehiculosConvenio): TDateTime;
ResourceString
    ERROR_FECHA_VENCIMIENTO_GARANTIA_INVALIDA = 'La fecha de vencimiento de la garant�a del telev�a es inv�lida';
    ERROR_PARAMETRO_ANIOS_VTO_GARANTIA_TAG = 'El par�metro ANIOS_VTO_GARANTIA_TAG es inv�lido';
var
    AniosVencimientoGarantiaTelevia: integer;
begin
    Result := NullDate;
    try
        if AVehiculo.Cuenta.TipoAsignacionTag = CONST_OP_TRANSFERENCIA_TAG_NUEVO then begin
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'ANIOS_VTO_GARANTIA_TAG', AniosVencimientoGarantiaTelevia);
            Result := IncMonth(AVehiculo.Cuenta.FechaAltaTag , (12 * AniosVencimientoGarantiaTelevia));
        end else begin
            Result := QueryGetValueDateTime(DMConnections.BaseCAC,
            Format('select dbo.ObtenerVencimientoGarantiaTag(%d,%d)',[AVehiculo.Cuenta.ContextMark ,AVehiculo.Cuenta.ContactSerialNumber]));
        end;
    except
        on E: Exception do begin
            raise;
            Exit;
        end;
    end;
end;

{Procedure Name	: TCuentasConvenio.ExisteDadoBaja
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:  unaPatente: string; unContext: integer; unSerial: int64
 Return Value   :  Boolean
}
function TCuentasConvenio.ExisteDadoBaja(unaPatente: string; unContext: integer; unSerial: int64; var pFechaBaja: TDateTime): Boolean;   // SS_916_PDO_20120109
var
    unVehiculoBaja : PVehiculosConvenio;
    i: integer;
begin
    Result     := False;                                                                                                                    // SS_916_PDO_20120109
    pFechaBaja := NullDate;                                                                                                                 // SS_916_PDO_20120109

    i := 0;
    while (i <= FVehiculosQuitados.Count - 1) and not Result do begin
        unVehiculoBaja := FVehiculosQuitados.items[i];
        if (Trim(unVehiculoBaja^.Cuenta.Vehiculo.Patente) = Trim(unaPatente)) or
            ((unVehiculoBaja^.Cuenta.ContextMark = unContext) and (unVehiculoBaja^.Cuenta.ContactSerialNumber = unSerial)) then begin       // SS_916_PDO_20120109
                pFechaBaja := unVehiculoBaja^.Cuenta.FechaBajaCuenta;                                                                       // SS_916_PDO_20120109
                Result := True                                                                                                              // SS_916_PDO_20120109
            end                                                                                                                             // SS_916_PDO_20120109
        else inc(i);
    end;
end;

{Procedure Name	: TCuentasConvenio.CorregirFechaAlta
 Author 		: Nelson Droguett Sierra (vpaszkowicz)
 Date Created	: 20-Abril-2010
 Description	: Corrige la fecha de alta para evitar traslapes si existe alg�n veh�culo relacionado a una baja.
 Parameters   	: unVehiculoAlta: PVehiculosConvenio
 Return Value   :
}
procedure TCuentasConvenio.CorregirFechaAlta(unVehiculoAlta: PVehiculosConvenio);
var
    unVehiculoBaja : PVehiculosConvenio;
    indice: integer;
    Exito: Boolean;
begin
    indice := 0;
    Exito := False;
    while (indice <= FVehiculosQuitados.Count - 1) and not Exito do begin
        unVehiculoBaja := FVehiculosQuitados.items[indice];
        if (unVehiculoAlta.EsVehiculoNuevo) or (unVehiculoAlta.EsVehiculoModificado) then begin
            if (Trim(unVehiculoBaja^.Cuenta.Vehiculo.Patente) = unVehiculoAlta^.Cuenta.Vehiculo.Patente) or
                ((unVehiculoBaja^.Cuenta.ContextMark = unVehiculoAlta^.Cuenta.ContextMark) and (unVehiculoBaja^.Cuenta.ContactSerialNumber = unVehiculoAlta^.Cuenta.ContactSerialNumber)) or
                    (unVehiculoBaja^.Cuenta.Vehiculo.CodigoVehiculo = unVehiculoAlta^.Cuenta.Vehiculo.CodigoVehiculo) then begin
                unVehiculoAlta^.Cuenta.FechaCreacion := IncMinute(unVehiculoBaja^.Cuenta.FechaBajaCuenta, 1);
                Exito := True;
            end;
        end;
        inc(indice);
    end;
    Exito := False;
    while (indice <= FCuentasActivadas.Count - 1) and not Exito do begin
        unVehiculoBaja := FCuentasActivadas.items[indice];
        if (unVehiculoAlta.EsVehiculoNuevo) or (unVehiculoAlta.EsVehiculoModificado) then begin
            if (Trim(unVehiculoBaja^.Cuenta.Vehiculo.Patente) = unVehiculoAlta^.Cuenta.Vehiculo.Patente) or
                ((unVehiculoBaja^.Cuenta.ContextMark = unVehiculoAlta^.Cuenta.ContextMark) and (unVehiculoBaja^.Cuenta.ContactSerialNumber = unVehiculoAlta^.Cuenta.ContactSerialNumber)) or
                    (unVehiculoBaja^.Cuenta.Vehiculo.CodigoVehiculo = unVehiculoAlta^.Cuenta.Vehiculo.CodigoVehiculo) then begin
                unVehiculoAlta^.Cuenta.FechaCreacion := IncMinute(unVehiculoBaja^.Cuenta.FechaBajaCuenta, 1);
                Exito := True;
            end;
        end;
        inc(indice);
    end;
end;


{Procedure Name	: TCuentasConvenio.ObtenerIDMotivoMovimientoCuentaTelevia
 Author 		: Nelson Droguett Sierra (mbecerra)
 Date Created	: 20-Abril-2010
 Description	: (Ref SS 769)	Devuelve el campo IDMotivoMovCuentaTelevia de FVehiculosTagsACobrar
 Parameters   	:
 Return Value   :
}
function TCuentasConvenio.ObtenerIDMotivoMovimientoCuentaTelevia;
var
	AMovimiento : PMovimientoCuenta;
begin
	AMovimiento := FVehiculosTagsACobrar.Items[Indice];
    Result := AMovimiento.IDMotivoMovCuentaTelevia;
end;

{Procedure Name	: TCuentasConvenio.guardar
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: Guardar Cuentas/Vehiculos/Documentaci�n presentada.
                  Habilit� una l�nea de c�digo que setea el par�metro @Patente del spCuentas.
                  	Esto se hizo pues se migr� a PROD el SP que utiliza dicho par�metro.
                  Si es baja por cambio de tag le pongo la fecha de baja que cargu� previamente
                  	en el objeto.
                  (SS 694) Se agrega validaci�n de la fecha de alta a las cero horas.
                  Se agrego el par�metro @Usuario al llamado al stored spBajaCuentaxCambioVehiculo.
                  Se agrego el par�metro @Usuario al llamado al stored spSuspenderCuenta.
                  Vuelvo a subir el sp activarCuenta, dado que ubicado al finaldel proceso originaba
                  	traslapes ya que este sp solo actualiza fechas de baja. De esta forma se grababa primero el alta y luego la baja.
                  Se incluye el BIT de habilitacion en lista blanca AMB (SS-841)
 Parameters   	:
 Return Value   :
}
//procedure TCuentasConvenio.guardar(EsConvenioCN:Boolean;SeCambioBitAMB:Boolean=False;HabilitadoAMB:Boolean=False); //SS-1006-NDR-20111105
//procedure TCuentasConvenio.guardar(EsConvenioCN:Boolean;SeCambioBitPA:Boolean=False;AdheridoPA:Boolean=False);   //SS_1147_MCA_20140408  //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
procedure TCuentasConvenio.guardar(EsConvenioNativo:Boolean;SeCambioBitPA:Boolean=False;AdheridoPA:Boolean=False);   //SS_1147_MCA_20140408
resourcestring
    MSG_SAVE_VEHI_ERROR = 'Error al Guardar Vehiculos.';
    MSG_PAGO_TELEVIA_POR_DANIO = 'Pago de Telev�a por Da�o';
    MSG_PAGO_TELEVIA = 'Pago de Telev�a';
    MSG_PAGO_DE_SOPORTE = 'Pago de Soporte';
{INICIO: TASK_005_JMA_20160418	
    MSG_SQL_MAIL	= 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL()';
    MSG_SQL_POSTAL	= 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL()';
}
	MSG_SQL_MAIL	= 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL(dbo.ObtenerConcesionariaNativa())';
    MSG_SQL_POSTAL	= 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL(dbo.ObtenerConcesionariaNativa())';
{TERMINO: TASK_005_JMA_20160418}		
    //Rev.6 / 23-Agosto-2010 / Nelson Droguett Sierra
    MSG_INCONSISTENCIA_TIT = 'Inconsistencia en Almac�n';
    MSG_INCONSISTENCIA_ALM = 'Se detect� una inconsistencia en la grabaci�n de los datos del almacen del telev�a:'+CRLF+
                             'Para la patente %s se esta grabando el almacen %s '+CRLF+'y deberia grabarse el almacen %s .'+CRLF+
                             'Por favor, avise a Sistemas de inmediato.';
var
	ConceptoMail, ConceptoPostal, NuevoIndiceVehiculo, Index, i : integer;
    {
    spVehiculos : TADOStoredProc;
    spCuentas : TADOStoredProc;
    spBajaCuenta :TADOStoredProc;
    spActualizarTurnoMovimientoEntrega:TADOStoredProc;
    spActualizarTurnoMovimientoDevolucion: TADOStoredProc;
    spGenerarMovimientoCuentaConTelevia: TADOStoredProc;
    spSuspenderCuenta: TADOStoredProc;
    spRegistrarTAGVendido: TADOStoredProc;
    spActivarCuenta: TADOStoredProc;
    spBajaCuentaxCambioVehiculo: TADOStoredProc;
    spActualizarCuentaAPerdido: TADOStoredProc;
    //spHabilitarListaBlanca:TAdoStoredProc;                           //SS-1006-NDR-20111105
    spAgregarMovimientoListasDeAcceso:TAdoStoredProc;                  //SS-1006-NDR-20111105
	}
    AVehiculo : PVehiculosConvenio;
    AMovCuenta: PMovimientoCuenta;
    VerificarFechaAltaSinHora: Boolean;
    AuxFechaCreacion: TDateTime;
    AuxFechaCreacionSinHora: TDateTime;
    ValorValidado: Boolean;
    AuxFechaCreacionFormateada, AuxFechaCreacionSinHoraFormateada: string;
    //Rev.6 / 28-Julio-2010  / Nelson Droguett Sierra
    iVerificaAlmacen:Integer;
    j:Integer;
    //-------------------------------------------------

    objGuardar: TDMGuardarConvenios;
begin
    ConceptoMail	:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_MAIL);
    ConceptoPostal	:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_POSTAL);

    try

//        objGuardar := TDMGuardarConvenios.Create(Nil);            //TASK_076_JMA_20161121
        objGuardar := TDMGuardarConvenios.Create();                 //TASK_076_JMA_20161121

        try
            //Guardo las baja de los vehiculos
            for i := 0 to FVehiculosQuitados.Count - 1 do begin
                AVehiculo := FVehiculosQuitados.items[i];
                if AVehiculo.EsCambioVehiculo then begin
{INICIO: TASK_076_JMA_20161121
                    if objGuardar.spBajaCuentaxCambioVehiculo.Active then
                        objGuardar.spBajaCuentaxCambioVehiculo.close;

                    objGuardar.spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@CodigoConvenio').Value := iif(AVehiculo^.Cuenta.CodigoConvenioRNUT=0,CodigoConvenio, AVehiculo^.Cuenta.CodigoConvenioRNUT); // CodigoConvenio;   //TASK_009_ECA_20160506
                    objGuardar.spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@IndiceVehiculo').Value := AVehiculo^.Cuenta.IndiceVehiculo;
                    objGuardar.spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@ContextMark').Value := AVehiculo^.Cuenta.ContextMark;
                    objGuardar.spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@ContractSerialNumber').Value := AVehiculo^.Cuenta.ContactSerialNumber;
                    //Si la fecha de baja de la misma cuenta me va a quedar menor que la de alta, la tengo que corregir.
                    if AVehiculo^.Cuenta.FechaBajaCuenta <= AVehiculo^.Cuenta.FechaCreacion then
                        AVehiculo^.Cuenta.FechaBajaCuenta := incMinute(AVehiculo^.Cuenta.FechaCreacion, 1);
                    //Si la fecha de baja queda menor a la de suspensi�n en la misma cuenta se traslapar� en el
                    //hist�rico de Tags, por lo tanto la baja tiene que ser mayor a su suspensi�n
                    if AVehiculo^.Cuenta.FechaBajaCuenta <= AVehiculo^.Cuenta.Suspendida then
                        AVehiculo^.Cuenta.FechaBajaCuenta := incMinute(AVehiculo^.Cuenta.Suspendida, 1);
                    objGuardar.spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@FechaBajaCuenta').Value := AVehiculo^.Cuenta.FechaBajaCuenta;
                    objGuardar.spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                    objGuardar.spBajaCuentaxCambioVehiculo.ExecProc;
 }
                    objGuardar.BajarCuentaxCambioVehiculo(iif(AVehiculo^.Cuenta.CodigoConvenioRNUT=0,CodigoConvenio, AVehiculo^.Cuenta.CodigoConvenioRNUT),
                                                                AVehiculo^.Cuenta.IndiceVehiculo,
                                                                AVehiculo^.Cuenta.ContextMark,
                                                                AVehiculo^.Cuenta.ContactSerialNumber,
                                                                AVehiculo^.Cuenta.FechaBajaCuenta,
                                                                AVehiculo^.Cuenta.FechaCreacion,
                                                                AVehiculo^.Cuenta.Suspendida,
                                                                UsuarioSistema);
 {TERMINO: TASK_076_JMA_20161121}
                end else begin
                    if GNumeroTurno < 1 then
                        raise Exception.Create(MSG_CAPTION_ESTADO_TURNO_CERRADO);
 {INICIO: TASK_076_JMA_20161121
                    if objGuardar.spBajaCuenta.Active then
                        objGuardar.spBajaCuenta.close;
                    objGuardar.spBajaCuenta.Parameters.Refresh;

                    objGuardar.spBajaCuenta.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                    objGuardar.spBajaCuenta.Parameters.ParamByName('@IndiceVehiculo').Value := AVehiculo^.Cuenta.IndiceVehiculo;
                    objGuardar.spBajaCuenta.Parameters.ParamByName('@ContextMark').Value := AVehiculo^.Cuenta.ContextMark;
                    objGuardar.spBajaCuenta.Parameters.ParamByName('@ContractSerialNumber').Value := AVehiculo^.Cuenta.ContactSerialNumber;
                    objGuardar.spBajaCuenta.Parameters.ParamByName('@CodigoPuntoEntrega').Value := PuntoEntrega;
                    objGuardar.spBajaCuenta.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                    objGuardar.spBajaCuenta.Parameters.ParamByName('@TagEnCliente').Value := AVehiculo^.Cuenta.EstadoConservacionTAG = ESTADO_CONSERVACION_NO_DEVUELTO;
                    objGuardar.spBajaCuenta.Parameters.ParamByName('@TagVendido').Value := AVehiculo^.EsTagVendido;
                    //esto marca la diferencia entre pasarlo a devuelto o extraviado.
                    if AVehiculo^.EsTagPerdido then
                        objGuardar.spBajaCuenta.Parameters.ParamByName('@TagEnCliente').Value := AVehiculo^.EsTagPerdido;

                    if AVehiculo^.EsTagRecuperado then
                        objGuardar.spBajaCuenta.Parameters.ParamByName('@CodigoEstadoConservacion').Value := ESTADO_CONSERVACION_BUENO
                    else
                        objGuardar.spBajaCuenta.Parameters.ParamByName('@CodigoEstadoConservacion').Value := iif((AVehiculo^.Cuenta.EstadoConservacionTAG = 0) or (AVehiculo^.Cuenta.EstadoConservacionTAG = null), null, AVehiculo^.Cuenta.EstadoConservacionTAG);
                    //Si la fecha de baja de la misma cuenta me va a quedar menor que la de alta, la tengo que corregir.
                    if AVehiculo^.Cuenta.FechaBajaCuenta <= AVehiculo^.Cuenta.FechaCreacion then
                        AVehiculo^.Cuenta.FechaBajaCuenta := incMinute(AVehiculo^.Cuenta.FechaCreacion, 1);

                    //Si la fecha de baja queda menor a la de suspensi�n en la misma cuenta se traslapar� en el
                    //hist�rico de Tags, por lo tanto la baja tiene que ser mayor a su suspensi�n
                    if AVehiculo^.Cuenta.FechaBajaCuenta <= AVehiculo^.Cuenta.Suspendida then
                        AVehiculo^.Cuenta.FechaBajaCuenta := incMinute(AVehiculo^.Cuenta.Suspendida, 1);

                    objGuardar.spBajaCuenta.Parameters.ParamByName('@FechaBajaCuenta').Value := AVehiculo^.Cuenta.FechaBajaCuenta;
                    objGuardar.spBajaCuenta.Parameters.ParamByName('@DescError').Value := null;     // SS_916_PDO_20120220

                    objGuardar.spBajaCuenta.Parameters.ParamByName('@CodigoMotivoBaja').Value := AVehiculo^.Cuenta.CodigoMotivoBaja;     //TASK_011_ECA_20160514
                    objGuardar.spBajaCuenta.ExecProc;

                    if (objGuardar.spBajaCuenta.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then
                        raise Exception.Create(objGuardar.spBajaCuenta.Parameters.ParamByName('@ErrorDescription').Value);
}
                    objGuardar.BajarCuenta(CodigoConvenio,
                                                 AVehiculo^.Cuenta.IndiceVehiculo,
                                                 AVehiculo^.Cuenta.ContextMark,
                                                 AVehiculo^.Cuenta.ContactSerialNumber,
                                                 PuntoEntrega,
                                                 AVehiculo^.Cuenta.EstadoConservacionTAG = ESTADO_CONSERVACION_NO_DEVUELTO,
                                                 AVehiculo^.EsTagVendido,
                                                 AVehiculo^.EsTagPerdido,
                                                 AVehiculo^.EsTagRecuperado,
                                                 IIf(AVehiculo^.EsTagRecuperado, ESTADO_CONSERVACION_BUENO, AVehiculo^.Cuenta.EstadoConservacionTAG),
                                                 AVehiculo^.Cuenta.CodigoMotivoBaja,
                                                 AVehiculo^.Cuenta.FechaBajaCuenta,
                                                 AVehiculo^.Cuenta.FechaCreacion,
                                                 AVehiculo^.Cuenta.Suspendida,
                                                 UsuarioSistema);
{TERMINO: TASK_076_JMA_20161121}
                    // copiar a tabla vendidos
                    if (AVehiculo.Cuenta.EstadoConservacionTAG = ESTADO_CONSERVACION_NO_DEVUELTO) and not(AVehiculo.EsTagRecuperado) then
                    begin
{INICIO: TASK_076_JMA_20161121
                        if objGuardar.spRegistrarTAGVendido.Active then
                            objGuardar.spRegistrarTAGVendido.close;
                        objGuardar.spRegistrarTAGVendido.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                        objGuardar.spRegistrarTAGVendido.Parameters.ParamByName('@ContractSerialNumber').Value := AVehiculo^.Cuenta.ContactSerialNumber;
                        objGuardar.spRegistrarTAGVendido.ExecProc;
}
                        objGuardar.RegistrarTAGVendido(CodigoConvenio, AVehiculo^.Cuenta.ContactSerialNumber);
{TERMINO: TASK_076_JMA_20161121}
                    end;

                    if not(AVehiculo^.EsTagVendido) and not(AVehiculo^.EsCambioVehiculo) then begin
                        if GNumeroTurno < 0 then
                            raise Exception.Create(MSG_CAPTION_ESTADO_TURNO_CERRADO);
{INICIO: TASK_076_JMA_20161121
                        //Actualizamos el movimiento para el turno
                        if objGuardar.spActualizarTurnoMovimientoDevolucion.Active then
                            objGuardar.spActualizarTurnoMovimientoDevolucion.close;

                        objGuardar.spActualizarTurnoMovimientoDevolucion.Parameters.ParamByName('@NumeroTurno').Value:= GNumeroTurno;       // SS_937_PDO_20120516
                        objGuardar.spActualizarTurnoMovimientoDevolucion.Parameters.ParamByName('@CodigoTipoVehiculo').Value:= AVehiculo^.Cuenta.Vehiculo.CodigoTipo;
                        objGuardar.spActualizarTurnoMovimientoDevolucion.Parameters.ParamByName('@TieneAcoplado').Value:= AVehiculo^.Cuenta.TieneAcoplado;
                        objGuardar.spActualizarTurnoMovimientoDevolucion.Parameters.ParamByName('@CodigoConvenio').Value:= CodigoConvenio;

                        objGuardar.spActualizarTurnoMovimientoDevolucion.ExecProc;
}
                        //objGuardar.ActualizarTurnoMovimientoDevolucion(GNumeroTurno, CodigoConvenio, AVehiculo^.Cuenta.Vehiculo.CodigoTipo, AVehiculo^.Cuenta.TieneAcoplado); //TASK_106_JMA_20170206_TODO
{TERMINO: TASK_076_JMA_20161121}
                    end;
                    //Bajo lo de grabaci�n para poder blanquear la RVM ya sea
                    //por baja de vehiculo normal o por cambio de veh�culo.
                end; // else if AVehiculo.EsCambioVehiculo
                if (AVehiculo.EsVehiculoNuevo) or (AVehiculo.EsVehiculoModificado) then
                begin
                    if trim(AVehiculo.Cuenta.Vehiculo.Patente) <> '' then
                    begin
{INICIO: TASK_076_JMA_20161121
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoTipoPatente').Value := AVehiculo^.Cuenta.Vehiculo.CodigoTipoPatente;
                        objGuardar.spVehiculos.Parameters.ParamByName('@Patente').Value           := AVehiculo^.Cuenta.Vehiculo.Patente;
                        objGuardar.spVehiculos.Parameters.ParamByName('@DigitoVerificadorPatente').Value := AVehiculo^.Cuenta.Vehiculo.DigitoPatente;
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoMarca').Value       := AVehiculo^.Cuenta.Vehiculo.CodigoMarca;
                        objGuardar.spVehiculos.Parameters.ParamByName('@Modelo').Value            := iif(trim(AVehiculo^.Cuenta.Vehiculo.Modelo)='',null,AVehiculo^.Cuenta.Vehiculo.Modelo);
                        objGuardar.spVehiculos.Parameters.ParamByName('@AnioVehiculo').Value      := AVehiculo^.Cuenta.Vehiculo.Anio;
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoColor').Value       := AVehiculo^.Cuenta.Vehiculo.CodigoColor; //NULL; TASK_016_ECA_20160526
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoTipoVehiculo').Value:= AVehiculo^.Cuenta.Vehiculo.CodigoTipo;
                        objGuardar.spVehiculos.Parameters.ParamByName('@TieneAcoplado').Value     := AVehiculo^.Cuenta.TieneAcoplado;
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoCategoria').Value   := ObtenerCategoriaVehiculo(DMConnections.BaseCAC, AVehiculo^.Cuenta.Vehiculo.CodigoTipo);
                        objGuardar.spVehiculos.Parameters.ParamByName('@DigitoVerificadorValido').Value := DigitoVerificadorValido(AVehiculo^.Cuenta.Vehiculo.Patente, AVehiculo^.Cuenta.Vehiculo.DigitoPatente);
                        objGuardar.spVehiculos.Parameters.ParamByName('@PatenteRVM').Value        := iif(Trim(AVehiculo^.Cuenta.Vehiculo.PatenteRVM) = '', Null, AVehiculo^.Cuenta.Vehiculo.PatenteRVM);
                        objGuardar.spVehiculos.Parameters.ParamByName('@DigitoVerificadorPatenteRVM').Value := iif(Trim(AVehiculo^.Cuenta.Vehiculo.DigitoPatenteRVM) = '', Null, AVehiculo^.Cuenta.Vehiculo.DigitoPatenteRVM);

                        {INICIO: TASK_056_JMA_20161012
                        if AVehiculo^.Cuenta.Vehiculo.Robado then
                        begin
                            if AVehiculo^.Cuenta.Vehiculo.Recuperado then
                                objGuardar.spVehiculos.Parameters.ParamByName('@Robado').Value    := not (AVehiculo^.Cuenta.Vehiculo.Robado)
                            else
                                objGuardar.spVehiculos.Parameters.ParamByName('@Robado').Value    := AVehiculo^.Cuenta.Vehiculo.Robado;
                        end
                        else
                            objGuardar.spVehiculos.Parameters.ParamByName('@Robado').Value        := AVehiculo^.Cuenta.Vehiculo.Robado;
                        //FinINICIO:TASK_056_JMA_20161012
                        if AVehiculo^.Cuenta.Vehiculo.Robado and AVehiculo^.Cuenta.Vehiculo.Recuperado then
                            objGuardar.spVehiculos.Parameters.ParamByName('@Robado').Value := not AVehiculo^.Cuenta.Vehiculo.Robado
                        else
                            objGuardar.spVehiculos.Parameters.ParamByName('@Robado').Value := AVehiculo^.Cuenta.Vehiculo.Robado;
                        {TERMINO: TASK_056_JMA_20161012 FIN TERMINO
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoVehiculo').Value    := AVehiculo^.Cuenta.Vehiculo.CodigoVehiculo;


                        objGuardar.spVehiculos.ExecProc;

                        AVehiculo^.Cuenta.Vehiculo.CodigoVehiculo := objGuardar.spVehiculos.Parameters.ParamByName('@CodigoVehiculo').Value;
}
                        AVehiculo^.Cuenta.Vehiculo.CodigoVehiculo := objGuardar.ActualizarMaestroVehiculos(AVehiculo^.Cuenta.Vehiculo.CodigoTipoPatente,
                                                                                                    AVehiculo^.Cuenta.Vehiculo.Patente,
                                                                                                    AVehiculo^.Cuenta.Vehiculo.DigitoPatente,
                                                                                                    AVehiculo^.Cuenta.Vehiculo.CodigoMarca,
                                                                                                    AVehiculo^.Cuenta.Vehiculo.Modelo,
                                                                                                    AVehiculo^.Cuenta.Vehiculo.Anio,
                                                                                                    AVehiculo^.Cuenta.Vehiculo.CodigoColor,
                                                                                                    //AVehiculo^.Cuenta.Vehiculo.CodigoTipo,          												//TASK_106_JMA_20170206
                                                                                                    AVehiculo^.Cuenta.TieneAcoplado,
                                                                                                    //ObtenerCategoriaVehiculo(DMConnections.BaseCAC, AVehiculo^.Cuenta.Vehiculo.CodigoTipo), 		//TASK_106_JMA_20170206
                                                                                                    DigitoVerificadorValido(AVehiculo^.Cuenta.Vehiculo.Patente, AVehiculo^.Cuenta.Vehiculo.DigitoPatente),
                                                                                                    //AVehiculo^.Cuenta.Vehiculo.PatenteRVM,        												//TASK_106_JMA_20170206
                                                                                                    //AVehiculo^.Cuenta.Vehiculo.DigitoPatenteRVM,  												//TASK_106_JMA_20170206
                                                                                                    AVehiculo^.Cuenta.Vehiculo.Robado,
                                                                                                    AVehiculo^.Cuenta.Vehiculo.Recuperado,
                                                                                                    AVehiculo^.Cuenta.Vehiculo.CodigoVehiculo);
{TERMINO: TASK_076_JMA_20161121}
                        end;
                    end;
                                                                                                                                                                                  //SS-1006-NDR-20111105
             end; // for Vehiculos Quitados

            //Activo las cuentas
            //Se vuelve a subir el sp de activaciones.
            for i := 0 to FCuentasActivadas.Count - 1 do begin
                AVehiculo := FCuentasActivadas.items[i];
{INICIO: TASK_076_JMA_20161121
                objGuardar.spActivarCuenta.Parameters.ParamByName('@CodigoConvenio').Value := iif(AVehiculo^.Cuenta.CodigoConvenioRNUT=0,CodigoConvenio, AVehiculo^.Cuenta.CodigoConvenioRNUT); //CodigoConvenio; TASK_009_ECA_20160506
                objGuardar.spActivarCuenta.Parameters.ParamByName('@IndiceVehiculo').Value := AVehiculo.Cuenta.IndiceVehiculo;
                objGuardar.spActivarCuenta.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                AVehiculo^.Cuenta.FechaBajaCuenta := NowBaseSmall(DMConnections.BaseCAC);
                if AVehiculo^.Cuenta.FechaBajaCuenta <= AVehiculo^.Cuenta.Suspendida then
                    AVehiculo^.Cuenta.FechaBajaCuenta := incMinute(AVehiculo^.Cuenta.Suspendida, 1);
                objGuardar.spActivarCuenta.Parameters.ParamByName('@FechaBajaCuenta').Value := AVehiculo^.Cuenta.FechaBajaCuenta;
                objGuardar.spActivarCuenta.ExecProc;
}
                objGuardar.ActivarCuenta(CodigoConvenio,
                                         AVehiculo^.Cuenta.CodigoConvenioRNUT,
                                         AVehiculo.Cuenta.IndiceVehiculo,
                                         AVehiculo^.Cuenta.FechaBajaCuenta,
                                         AVehiculo^.Cuenta.Suspendida,
                                         UsuarioSistema);
{TERMINO: TASK_076_JMA_20161121}
                //spActivarCuenta.close;
            end;
            // Guardo los vehiculos activos del convenio
            for i := 0 to FVehiculos.Count - 1 do begin
                AVehiculo      := FVehiculos.items[i];
                if (AVehiculo.EsVehiculoNuevo) or (AVehiculo.EsVehiculoModificado) then begin
{INICIO: TASK_076_JMA_20161121
                    //with spVehiculos.Parameters, AVehiculo^ do begin
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoVehiculo').Value    := AVehiculo^.Cuenta.Vehiculo.CodigoVehiculo;
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoTipoPatente').Value := AVehiculo^.Cuenta.Vehiculo.CodigoTipoPatente;
                        objGuardar.spVehiculos.Parameters.ParamByName('@Patente').Value           := AVehiculo^.Cuenta.Vehiculo.Patente;
                        objGuardar.spVehiculos.Parameters.ParamByName('@DigitoVerificadorPatente').Value := AVehiculo^.Cuenta.Vehiculo.DigitoPatente;
                        objGuardar.spVehiculos.Parameters.ParamByName('@PatenteRVM').Value           := iif(Trim(AVehiculo^.Cuenta.Vehiculo.PatenteRVM) = '', Null, AVehiculo^.Cuenta.Vehiculo.PatenteRVM);
                        objGuardar.spVehiculos.Parameters.ParamByName('@DigitoVerificadorPatenteRVM').Value := iif(Trim(AVehiculo^.Cuenta.Vehiculo.DigitoPatenteRVM) = '', Null, AVehiculo^.Cuenta.Vehiculo.DigitoPatenteRVM);
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoMarca').Value := AVehiculo^.Cuenta.Vehiculo.CodigoMarca;
                        objGuardar.spVehiculos.Parameters.ParamByName('@Modelo').Value            := iif(trim(AVehiculo^.Cuenta.Vehiculo.Modelo)='',null,AVehiculo^.Cuenta.Vehiculo.Modelo);
                        objGuardar.spVehiculos.Parameters.ParamByName('@AnioVehiculo').Value      := AVehiculo^.Cuenta.Vehiculo.Anio;
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoColor').Value       := AVehiculo^.Cuenta.Vehiculo.CodigoColor; //TASK_016_ECA_20160526
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoTipoVehiculo').Value:= AVehiculo^.Cuenta.Vehiculo.CodigoTipo;
                        objGuardar.spVehiculos.Parameters.ParamByName('@TieneAcoplado').Value     := AVehiculo^.Cuenta.TieneAcoplado;
                        objGuardar.spVehiculos.Parameters.ParamByName('@CodigoCategoria').Value   := ObtenerCategoriaVehiculo(DMConnections.BaseCAC, AVehiculo^.Cuenta.Vehiculo.CodigoTipo);
                        objGuardar.spVehiculos.Parameters.ParamByName('@DigitoVerificadorValido').Value := DigitoVerificadorValido(AVehiculo^.Cuenta.Vehiculo.Patente, AVehiculo^.Cuenta.Vehiculo.DigitoPatente);
                    {INICIO: TASK_056_JMA_20161012
                    if AVehiculo^.Cuenta.Vehiculo.Robado then
                    begin
                        if AVehiculo^.Cuenta.Vehiculo.Recuperado then
                            objGuardar.spVehiculos.Parameters.ParamByName('@Robado').Value    := not (AVehiculo^.Cuenta.Vehiculo.Robado)
                        else
                            objGuardar.spVehiculos.Parameters.ParamByName('@Robado').Value    := AVehiculo^.Cuenta.Vehiculo.Robado;
                    end
                    else
                        objGuardar.spVehiculos.Parameters.ParamByName('@Robado').Value        := AVehiculo^.Cuenta.Vehiculo.Robado;
                  FIN INICIO: TASK_056_JMA_20161012
                    if AVehiculo^.Cuenta.Vehiculo.Robado and AVehiculo^.Cuenta.Vehiculo.Recuperado then
                        objGuardar.spVehiculos.Parameters.ParamByName('@Robado').Value := not AVehiculo^.Cuenta.Vehiculo.Robado
                    else
                        objGuardar.spVehiculos.Parameters.ParamByName('@Robado').Value := AVehiculo^.Cuenta.Vehiculo.Robado;
                    {TERMINO: TASK_056_JMA_20161012

                    objGuardar.spVehiculos.ExecProc;
                    AVehiculo^.Cuenta.Vehiculo.CodigoVehiculo := objGuardar.spVehiculos.Parameters.ParamByName('@CodigoVehiculo').Value;
}
                    AVehiculo^.Cuenta.Vehiculo.CodigoVehiculo := objGuardar.ActualizarMaestroVehiculos(AVehiculo^.Cuenta.Vehiculo.CodigoTipoPatente,
                                                                                                        AVehiculo^.Cuenta.Vehiculo.Patente,
                                                                                                        AVehiculo^.Cuenta.Vehiculo.DigitoPatente,
                                                                                                        AVehiculo^.Cuenta.Vehiculo.CodigoMarca,
                                                                                                        AVehiculo^.Cuenta.Vehiculo.Modelo,
                                                                                                        AVehiculo^.Cuenta.Vehiculo.Anio,
                                                                                                        AVehiculo^.Cuenta.Vehiculo.CodigoColor,
                                                                                                        //AVehiculo^.Cuenta.Vehiculo.CodigoTipo, 													//TASK_106_JMA_20170206
                                                                                                        AVehiculo^.Cuenta.TieneAcoplado,
                                                                                                        //ObtenerCategoriaVehiculo(DMConnections.BaseCAC, AVehiculo^.Cuenta.Vehiculo.CodigoTipo), 	//TASK_106_JMA_20170206
                                                                                                        DigitoVerificadorValido(AVehiculo^.Cuenta.Vehiculo.Patente, AVehiculo^.Cuenta.Vehiculo.DigitoPatente),
                                                                                                        //AVehiculo^.Cuenta.Vehiculo.PatenteRVM,         											//TASK_106_JMA_20170206
                                                                                                        //AVehiculo^.Cuenta.Vehiculo.DigitoPatenteRVM,    											//TASK_106_JMA_20170206
                                                                                                        AVehiculo^.Cuenta.Vehiculo.Robado,
                                                                                                        AVehiculo^.Cuenta.Vehiculo.Recuperado,
                                                                                                        AVehiculo^.Cuenta.Vehiculo.CodigoVehiculo);
{TERMINO: TASK_076_JMA_20161121}
                    //spVehiculos.close;
                    //Guardo las cuentas suspendidas
                    if AVehiculo^.Cuenta.EstaSuspendida then begin
{INICIO: TASK_076_JMA_20161121
                        objGuardar.spSuspenderCuenta.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                        objGuardar.spSuspenderCuenta.Parameters.ParamByName('@IndiceVehiculo').Value := AVehiculo^.Cuenta.IndiceVehiculo;
                        objGuardar.spSuspenderCuenta.Parameters.ParamByName('@Suspendida').Value := AVehiculo^.Cuenta.Suspendida;
                        objGuardar.spSuspenderCuenta.Parameters.ParamByName('@DeadlineSuspension').Value := iif( AVehiculo^.Cuenta.DeadlineSuspension = NullDate, null, AVehiculo^.Cuenta.DeadlineSuspension);
                        objGuardar.spSuspenderCuenta.Parameters.ParamByName('@SuspensionForzada').Value := AVehiculo^.Cuenta.SuspensionForzada;
                        objGuardar.spSuspenderCuenta.Parameters.ParamByName('@Usuario').Value := UsuarioSistema; //Revision 4
                        objGuardar.spSuspenderCuenta.ExecProc;
}
                        objGuardar.SuspenderCuenta(CodigoConvenio,
                                                    AVehiculo^.Cuenta.IndiceVehiculo,
                                                    AVehiculo^.Cuenta.Suspendida,
                                                    AVehiculo^.Cuenta.DeadlineSuspension,
                                                    AVehiculo^.Cuenta.SuspensionForzada,
                                                    UsuarioSistema);
{TERMINO: TASK_076_JMA_20161121}
                        //spSuspenderCuenta.Close;
                    end else begin
                        if AVehiculo^.EsTagPerdido or AVehiculo^.EsTagRecuperado then begin //Cuentas con tags perdidos

                           if GNumeroTurno < 1 then                                                 //TASK_076_JMA_20161121
                                raise Exception.Create(MSG_CAPTION_ESTADO_TURNO_CERRADO);           //TASK_076_JMA_20161121
{INICIO: TASK_076_JMA_20161121
                           objGuardar.spActualizarCuentaAPerdido.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                           objGuardar.spActualizarCuentaAPerdido.Parameters.ParamByName('@IndiceVehiculo').Value := AVehiculo^.Cuenta.IndiceVehiculo;
                           objGuardar.spActualizarCuentaAPerdido.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                           objGuardar.spActualizarCuentaAPerdido.Parameters.ParamByName('@Recupero').Value := AVehiculo^.EsTagRecuperado;
                           objGuardar.spActualizarCuentaAPerdido.ExecProc;
}
                            objGuardar.ActualizarCuentaAPerdido(CodigoConvenio, AVehiculo^.Cuenta.IndiceVehiculo, AVehiculo^.EsTagRecuperado, UsuarioSistema);
{TERMINO: TASK_076_JMA_20161121}
                           //spActualizarCuentaAPerdido.Close;
                        end else begin // Cuentas Activas
{INICIO: TASK_076_JMA_20161121
                            if objGuardar.spCuentas.Active then
                                objGuardar.spCuentas.Close;
                            objGuardar.spCuentas.Parameters.Refresh; //SS-1006-NDR-20120614
                            objGuardar.spCuentas.Parameters.ParamByName('@CodigoConvenio').Value :=iif(AVehiculo^.Cuenta.CodigoConvenioRNUT=0,CodigoConvenio, AVehiculo^.Cuenta.CodigoConvenioRNUT); //CodigoConvenio;  TASK_009_ECA_20160506
                            //Solo pasamos el indice en caso de una modificacion. En cualquier otro caso null para que cree una nueva cuenta
                            with AVehiculo^ do
                                if EsVehiculoModificado and not (EsVehiculoNuevo or EsTagNuevo or EsTagVendido or EsCambioVehiculo or EsActivacionCuenta or EsTagPerdido or EsTagRecuperado) and not (Cuenta.EstaSuspendida) then
                                    objGuardar.spCuentas.Parameters.ParamByName('@IndiceVehiculo').Value := AVehiculo^.Cuenta.IndiceVehiculo
                                else begin
                                    // Alta de vehiculo
                                    if (not AVehiculo^.EsCambioVehiculo) and (GNumeroTurno < 1) then
                                            raise Exception.Create(MSG_CAPTION_ESTADO_TURNO_CERRADO);
                                    objGuardar.spCuentas.Parameters.ParamByName('@IndiceVehiculo').Value := Null;


                                    // Rev.6 / 28-Julio-2010 / Nelson Droguett Sierra --------------------------------------------------
                                    // Aca se que es un Alta, debo chequear el almacendestino contra el almacen destino del movimientocuenta
                                    // que es el que eligio en el formulario del alta y si no corresponden
                                    // debo forzar el almacendestino que corresponda al concepto del movimiento cuenta para la operacion 4
                                    //y tiposignaciontag=Nuevo
                                    for j := 0 to FVehiculosTagsACobrar.Count - 1 do begin
                                        AMovCuenta := FVehiculosTagsACobrar.items[j];
                                        if ((AMovCuenta^.VehiculosConvenio.Cuenta.ContactSerialNumber = AVehiculo^.Cuenta.ContactSerialNumber) and
                                             (AMovCuenta^.VehiculosConvenio.Cuenta.ContextMark = AVehiculo^.Cuenta.ContextMark )) then
                                        begin
                                          iVerificaAlmacen := QueryGetValueInt(DMConnections.BaseCAC,
                                                                               Format('SELECT dbo.ObtenerAlmacenPorConceptoMovimientoCuenta(%d,%d,''%s'')',
                                                                                      [CONST_OPERACION_TAG_AGREGAR,
                                                                                       AMovCuenta^.OtroConcepto,
                                                                                       AMovCuenta^.TipoAsignacionTag]
                                                                                     )
                                                                               );
                                            if iVerificaAlmacen>0 then
                                            begin
                                                if (AVehiculo^.Cuenta.CodigoAlmacenDestino <> iVerificaAlmacen) then
                                                begin
                                                  //Se pidio eliminar la correccion del dato, pero avisar al usuario con un mensaje
                                                  //que en este momento se esta produciendo una inconsistencia.
                                                  MsgBox(Format(MSG_INCONSISTENCIA_ALM,
                                                                [	Cuenta.Vehiculo.Patente,
                                                                    Trim(QueryGetValue(	DMConnections.BaseCAC,
                                                                                    Format('SELECT Descripcion FROM MaestroAlmacenes (NOLOCK) WHERE CodigoAlmacen = %d',[AVehiculo^.Cuenta.CodigoAlmacenDestino]))),
                                                                    Trim(QueryGetValue(  DMConnections.BaseCAC,
                                                                                    Format('SELECT Descripcion FROM MaestroAlmacenes (NOLOCK) WHERE CodigoAlmacen = %d',[iVerificaAlmacen])))
                                                                ]),MSG_INCONSISTENCIA_TIT,MB_ICONINFORMATION);
                                                end;
                                            end;
                                        end;
                                    end;
                                    //FinRev.6-----------------------------------------------------------------------------
                                end;
                            objGuardar.spCuentas.Parameters.ParamByName('@CodigoVehiculo').Value := AVehiculo^.Cuenta.Vehiculo.CodigoVehiculo;
                            objGuardar.spCuentas.Parameters.ParamByName('@DetallePasadas').Value := 0;
                            objGuardar.spCuentas.Parameters.ParamByName('@CodigoCategoria').Value:= ObtenerCategoriaVehiculo(DMConnections.BaseCAC, AVehiculo^.Cuenta.Vehiculo.CodigoTipo);
                            objGuardar.spCuentas.Parameters.ParamByName('@CentroCosto').Value := null;
                            objGuardar.spCuentas.Parameters.ParamByName('@FechaAltaCuenta').Value := AVehiculo^.Cuenta.FechaCreacion;
                            objGuardar.spCuentas.Parameters.ParamByName('@FechaBajaCuenta').Value := null;
                            objGuardar.spCuentas.Parameters.ParamByName('@CodigoEstadoCuenta').Value := ESTADO_CUENTA_VEHICULO_ACTIVO;
                            objGuardar.spCuentas.Parameters.ParamByName('@Suspendida').Value := NULL;
                            objGuardar.spCuentas.Parameters.ParamByName('@DeadlineSuspension').Value := NULL;
                            objGuardar.spCuentas.Parameters.ParamByName('@SuspensionForzada').Value := False;
                            objGuardar.spCuentas.Parameters.ParamByName('@ContextMark').Value := AVehiculo^.Cuenta.ContextMark;
                            if (AVehiculo^.Cuenta.ContactSerialNumber <> 0) then
                                objGuardar.spCuentas.Parameters.ParamByName('@ContractSerialNumber').Value := AVehiculo^.Cuenta.ContactSerialNumber
                            else //Esto es para que haga solo las bonificaciones en telev�as de terceros y no error haciendo un update en cuentas
                                objGuardar.spCuentas.Parameters.ParamByName('@ContractSerialNumber').Value := AVehiculo^.Cuenta.ContactSerialNumberOtraConcesionaria;

                            //Lo guardo porque lo voy a usar..
                            if not (AVehiculo^.EsActivacionCuenta) and not (AVehiculo^.EsCambioVehiculo) then
                                AVehiculo^.Cuenta.FechaAltaTag := objGuardar.spCuentas.Parameters.ParamByName('@FechaAltaCuenta').Value;
                            objGuardar.spCuentas.Parameters.ParamByName('@FechaAltaTAG').Value := AVehiculo^.Cuenta.FechaAltaTag;
                            objGuardar.spCuentas.Parameters.ParamByName('@FechaBajaTAG').Value := iif((AVehiculo^.Cuenta.FechaBajaTag = NullDate) or (AVehiculo^.Cuenta.FechaBajaTag = 0), Null, AVehiculo^.Cuenta.FechaBajaTag);

                            //Si a pesar de todo es Nulldate y es de terceros, lo dejo en Null porque no se actualiza.
                            if (AVehiculo^.Cuenta.ContactSerialNumber = 0) and (AVehiculo^.Cuenta.ContactSerialNumberOtraConcesionaria <> 0) then
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaVencimientoTAG').Value := Null
                            else begin
                                //Si modifique la fecha de alta (alta retroactiva), la garantia del televia se mantiene.
                                //Si es una transferencia de televia tambien se mantiene. En caso contrario la recalculo por el else.
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaVencimientoTAG').Value := AVehiculo^.Cuenta.FechaVencimientoTag;
                            end;
                            objGuardar.spCuentas.Parameters.ParamByName('@CodigoPuntoEntrega').Value:= PuntoEntrega;
                            objGuardar.spCuentas.Parameters.ParamByName('@Usuario').Value:=UsuarioSistema;
                            objGuardar.spCuentas.Parameters.ParamByName('@ResponsableInstalacion').Value := 1; //Atencion va el responsable tomarlo de algun lado
                            objGuardar.spCuentas.Parameters.ParamByName('@CambiodeVehiculo').Value := AVehiculo^.EsCambioVehiculo;
                            (* Cargar los par�metros para la Eximici�n de Facturaci�n. *)
                            if (AVehiculo^.Cuenta.EximirFacturacion.FechaInicioOriginal <= 0) then begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaInicioOriginalEximicionFacturacion').Value := Null;
                            end else begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaInicioOriginalEximicionFacturacion').Value := AVehiculo^.Cuenta.EximirFacturacion.FechaInicioOriginal;
                            end;

                            if (AVehiculo^.Cuenta.EximirFacturacion.FechaInicio <= 0) then begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaInicioEximicionFacturacion').Value := Null;
                            end else begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaInicioEximicionFacturacion').Value := AVehiculo^.Cuenta.EximirFacturacion.FechaInicio;
                            end;
                            if (AVehiculo^.Cuenta.EximirFacturacion.FechaFinalizacion <= 0) then begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaFinalizacionEximicionFacturacion').Value := Null;
                            end else begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaFinalizacionEximicionFacturacion').Value := AVehiculo^.Cuenta.EximirFacturacion.FechaFinalizacion;
                            end;
                            objGuardar.spCuentas.Parameters.ParamByName('@CodigoMotivoEximicionFacturacion').Value := AVehiculo^.Cuenta.EximirFacturacion.CodigoMotivoNoFacturable;
                            objGuardar.spCuentas.Parameters.ParamByName('@Patente').Value := AVehiculo^.Cuenta.Vehiculo.Patente;

                            (* Cargar los par�metros para la Bonificacion de Facturaci�n. *)
                            if (AVehiculo^.Cuenta.BonificarFacturacion.FechaInicioOriginal <= 0) then begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaInicioOriginalBonificacionFacturacion').Value := Null;
                            end else begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaInicioOriginalBonificacionFacturacion').Value := AVehiculo^.Cuenta.BonificarFacturacion.FechaInicioOriginal;
                            end;
                            if (AVehiculo^.Cuenta.BonificarFacturacion.FechaInicio <= 0) then begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaInicioBonificacionFacturacion').Value := Null;
                            end else begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaInicioBonificacionFacturacion').Value := AVehiculo^.Cuenta.BonificarFacturacion.FechaInicio;
                            end;
                            if (AVehiculo^.Cuenta.BonificarFacturacion.FechaFinalizacion <= 0) then begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaFinalizacionBonificacionFacturacion').Value := Null;
                            end else begin
                                objGuardar.spCuentas.Parameters.ParamByName('@FechaFinalizacionBonificacionFacturacion').Value := AVehiculo^.Cuenta.BonificarFacturacion.FechaFinalizacion;
                            end;

                            if (AVehiculo^.Cuenta.BonificarFacturacion.Porcentaje <= 0) then begin
                                objGuardar.spCuentas.Parameters.ParamByName('@PorcentajeBonificacionFacturacion').Value := Null;
                            end else begin
                                objGuardar.spCuentas.Parameters.ParamByName('@PorcentajeBonificacionFacturacion').Value := AVehiculo^.Cuenta.BonificarFacturacion.Porcentaje;
                            end;

                            if (AVehiculo^.Cuenta.BonificarFacturacion.Importe <= 0) then begin
                                objGuardar.spCuentas.Parameters.ParamByName('@ImporteBonificacionFacturacion').Value := Null;
                            end else begin
                                objGuardar.spCuentas.Parameters.ParamByName('@ImporteBonificacionFacturacion').Value := AVehiculo^.Cuenta.BonificarFacturacion.Importe;
                            end;

                            objGuardar.spCuentas.Parameters.ParamByName('@CodigoAlmacenDestino').Value := AVehiculo^.Cuenta.CodigoAlmacenDestino;

                            objGuardar.spCuentas.Parameters.ParamByName('@AdheridoPA').Value := False;                           //SS_1006_NDR_20121003 TASK_004_ECA_20160411

                            objGuardar.spCuentas.Parameters.ParamByName('@CodigoConvenioFacturacion').Value := AVehiculo^.Cuenta.CodigoConvenioFacturacion; //TASK_004_ECA_20160411
                            objGuardar.spCuentas.Parameters.ParamByName('@Foraneo').Value := AVehiculo^.Cuenta.Vehiculo.Foraneo;   // 20160603 MGO
                            objGuardar.spCuentas.ExecProc;

                            if objGuardar.spCuentas.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                                raise Exception.Create(objGuardar.spCuentas.Parameters.ParamByName('@ErrorDescription').Value);

                            NuevoIndiceVehiculo := objGuardar.spCuentas.Parameters.ParamByName('@IndiceVehiculo').Value;
}

                            if not AVehiculo^.EsVehiculoModificado
                               or (AVehiculo^.EsVehiculoNuevo or AVehiculo^.EsTagNuevo or AVehiculo^.EsTagVendido or AVehiculo^.EsCambioVehiculo or AVehiculo^.EsActivacionCuenta or AVehiculo^.EsTagPerdido or AVehiculo^.EsTagRecuperado)
                               or (AVehiculo^.Cuenta.EstaSuspendida) then
                            begin
                                // Aca se que es un Alta, debo chequear el almacendestino contra el almacen destino del movimientocuenta
                                // que es el que eligio en el formulario del alta y si no corresponden
                                // debo forzar el almacendestino que corresponda al concepto del movimiento cuenta para la operacion 4
                                //y tiposignaciontag=Nuevo
                                for j := 0 to FVehiculosTagsACobrar.Count - 1 do begin
                                    AMovCuenta := FVehiculosTagsACobrar.items[j];
                                    if ((AMovCuenta^.VehiculosConvenio.Cuenta.ContactSerialNumber = AVehiculo^.Cuenta.ContactSerialNumber) and
                                         (AMovCuenta^.VehiculosConvenio.Cuenta.ContextMark = AVehiculo^.Cuenta.ContextMark )) then
                                    begin
                                      iVerificaAlmacen := QueryGetValueInt(DMConnections.BaseCAC,
                                                                           Format('SELECT dbo.ObtenerAlmacenPorConceptoMovimientoCuenta(%d,%d,''%s'')',
                                                                                  [CONST_OPERACION_TAG_AGREGAR,
                                                                                   AMovCuenta^.OtroConcepto,
                                                                                   AMovCuenta^.TipoAsignacionTag]
                                                                                 )
                                                                           );
                                        if iVerificaAlmacen>0 then
                                        begin
                                            if (AVehiculo^.Cuenta.CodigoAlmacenDestino <> iVerificaAlmacen) then
                                            begin
                                              //Se pidio eliminar la correccion del dato, pero avisar al usuario con un mensaje
                                              //que en este momento se esta produciendo una inconsistencia.
                                              MsgBox(Format(MSG_INCONSISTENCIA_ALM,
                                                            [	AVehiculo^.Cuenta.Vehiculo.Patente,
                                                                Trim(QueryGetValue(	DMConnections.BaseCAC,
                                                                                Format('SELECT Descripcion FROM MaestroAlmacenes (NOLOCK) WHERE CodigoAlmacen = %d',[AVehiculo^.Cuenta.CodigoAlmacenDestino]))),
                                                                Trim(QueryGetValue(  DMConnections.BaseCAC,
                                                                                Format('SELECT Descripcion FROM MaestroAlmacenes (NOLOCK) WHERE CodigoAlmacen = %d',[iVerificaAlmacen])))
                                                            ]),MSG_INCONSISTENCIA_TIT,MB_ICONINFORMATION);
                                            end;
                                        end;
                                    end;
                                end;
                                //FinRev.6-----------------------------------------------------------------------------
                            end;

                            NuevoIndiceVehiculo := objGuardar.ActualizarCuenta(iif(AVehiculo^.Cuenta.CodigoConvenioRNUT=0, CodigoConvenio, AVehiculo^.Cuenta.CodigoConvenioRNUT),
                                                                               GNumeroTurno,
                                                                               PuntoEntrega,
                                                                               AVehiculo^.EsActivacionCuenta,
                                                                               AVehiculo^.EsVehiculoModificado,
                                                                               AVehiculo^.EsVehiculoNuevo,
                                                                               AVehiculo^.EsCambioVehiculo,
                                                                               AVehiculo^.EsTagNuevo,
                                                                               AVehiculo^.EsTagVendido,
                                                                               AVehiculo^.EsTagPerdido,
                                                                               AVehiculo^.EsTagRecuperado,
                                                                               AVehiculo^.Cuenta);


{TERMINO: TASK_076_JMA_20161121}
                            if AVehiculo.Cuenta.IndiceVehiculo <= 0 then
                            	AVehiculo.Cuenta.IndiceVehiculo := NuevoIndiceVehiculo;

                        end; // else if AVehiculo^.EsTagPerdido or AVehiculo^.EsTagRecuperado
                    end; // else if AVehiculo^.Cuenta.Suspendida <> NullDate

                    // copiar a tabla vendidos
                    if AVehiculo.EsTagVendido then begin //AVehiculo.Cuenta.EstadoConservacionTAG = ESTADO_CONSERVACION_NO_DEVUELTO then begin
{INICIO: TASK_076_JMA_20161121
                        objGuardar.spRegistrarTAGVendido.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                        objGuardar.spRegistrarTAGVendido.Parameters.ParamByName('@ContractSerialNumber').Value := AVehiculo^.Cuenta.ContactSerialNumber;
                        objGuardar.spRegistrarTAGVendido.ExecProc;
}
                        objGuardar.RegistrarTAGVendido(CodigoConvenio, AVehiculo^.Cuenta.ContactSerialNumber);
{TERMINO: TASK_076_JMA_20161121}
                    end;

                    //Actualizamos el movimiento para el turno
                    if not ((AVehiculo^.EsCambioVehiculo) or (AVehiculo^.EsTagVendido)) then begin
                        if GNumeroTurno < 1 then
                            raise Exception.Create(MSG_CAPTION_ESTADO_TURNO_CERRADO);
{INICIO: TASK_076_JMA_20161121
                        objGuardar.spActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@NumeroTurno').Value:= GNumeroTurno;
                        objGuardar.spActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@CodigoTipoVehiculo').Value:= AVehiculo^.Cuenta.Vehiculo.CodigoTipo;
                        objGuardar.spActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@TieneAcoplado').Value:= AVehiculo^.Cuenta.TieneAcoplado;
                        objGuardar.spActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@CodigoConvenio').Value:= CodigoConvenio;
                        objGuardar.spActualizarTurnoMovimientoEntrega.ExecProc;
}
                        objGuardar.ActualizarTurnoMovimientoEntrega(GNumeroTurno,
                                                                    CodigoConvenio,
                                                                    //AVehiculo^.Cuenta.Vehiculo.CodigoTipo,    //TASK_106_JMA_20170206
                                                                    AVehiculo^.Cuenta.Vehiculo.CodigoVehiculo,  //TASK_106_JMA_20170206
                                                                    AVehiculo^.Cuenta.TieneAcoplado);
{TERMINO: TASK_076_JMA_20161121}
                        //spActualizarTurnoMovimientoEntrega.close;
                    end;
                end; // if (AVehiculo.EsVehiculoNuevo) or (AVehiculo.EsVehiculoModificado)

            end; // for

            //Tags o soportes a cobrar
            for i := 0 to FVehiculosTagsACobrar.Count - 1 do begin
                if GNumeroTurno < 1 then
                    raise Exception.Create(MSG_CAPTION_ESTADO_TURNO_CERRADO);      // SS_937_PDO_20120516

                AMovCuenta := FVehiculosTagsACobrar.items[i];
//                Antes solo se cargaban si tenian asociados conceptos, ahora se cargan todos
{INICIO: TASK_076_JMA_20161121
                objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@IndiceVehiculo').Value := AMovCuenta^.VehiculosConvenio.Cuenta.IndiceVehiculo;
                objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@FechaHora').Value := NowBase(DMConnections.BaseCAC);
                if (AMovCuenta^.OtroConcepto = ConceptoMail) or (AMovCuenta^.OtroConcepto = ConceptoPostal) then begin
                    if ConvenioTieneMedioEnvioPorEMail then
                        objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoConcepto').Value := ConceptoMail
                    else
                        objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoConcepto').Value := ConceptoPostal;
                end
                else
                    objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoConcepto').Value := AMovCuenta^.OtroConcepto;
                objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@DescripcionMotivo').Value := AMovCuenta^.DescripcionMotivo;
                objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@ContractSerialNumber').Value := AMovCuenta^.VehiculosConvenio.Cuenta.ContactSerialNumber;
                objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@ContextMark').Value := AMovCuenta^.VehiculosConvenio.Cuenta.ContextMark;
                objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@NumeroMovimiento').Value := Null;
                objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoAlmacen').Value := AMovCuenta^.AlmacenDestino;
                objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@TipoAsignacionTag').Value := AMovCuenta^.TipoAsignacionTag;
                objGuardar.spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@Observaciones').Value := IntToStr(AMovCuenta^.NumeroVoucher);
                objGuardar.spGenerarMovimientoCuentaConTelevia.ExecProc;
}
                objGuardar.GenerarMovimientoCuentaConTelevia(CodigoConvenio,
                                                             AMovCuenta^.VehiculosConvenio.Cuenta.IndiceVehiculo,
                                                             AMovCuenta^.OtroConcepto,
                                                             ConceptoMail,
                                                             ConceptoPostal,
                                                             ConvenioTieneMedioEnvioPorEMail,
                                                             AMovCuenta^.DescripcionMotivo,
                                                             AMovCuenta^.VehiculosConvenio.Cuenta.ContactSerialNumber,
                                                             AMovCuenta^.VehiculosConvenio.Cuenta.ContextMark,
                                                             AMovCuenta^.AlmacenDestino,
                                                             AMovCuenta^.TipoAsignacionTag,
                                                             AMovCuenta^.NumeroVoucher,
                                                             UsuarioSistema);
{TERMINO: TASK_076_JMA_20161121}
                //spGenerarMovimientoCuentaConTelevia.Close;
            end; // for FVehiculosTagsACobrar.Count
        except
            on E: Exception do begin
                 if E.ClassType = Exception then
                    raise EConvenioError.create(MSG_SAVE_VEHI_ERROR, e.message , 0)
                 else raise;
            end;
        end;
    finally
        {
        spVehiculos.Free;
        spCuentas.Free;
        spBajaCuenta.free;
        spActualizarTurnoMovimientoEntrega.Free;
        spActualizarTurnoMovimientoDevolucion.Free;
        spGenerarMovimientoCuentaConTelevia.Free;
        spSuspenderCuenta.Free;
        spRegistrarTAGVendido.free;
        }

        if Assigned(objGuardar) then FreeAndNil(objGuardar);
    end;
end;

{Procedure Name	: TCuentasConvenio.ObtenerCantidadVehiculos
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TCuentasConvenio.ObtenerCantidadVehiculos: integer;
begin
    result := FVehiculos.Count;
end;

{Procedure Name	: TCuentasConvenio.GetVehiculo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   : TVehiculosConvenio
}
function TCuentasConvenio.GetVehiculo(Index: integer): TVehiculosConvenio;
begin
    result := TVehiculosConvenio((FVehiculos.items[index])^);
end;

{Procedure Name	: TCuentasConvenio.ObtenerVehiculos
 Author 		: Nelson Droguett Sierra (vpaszkowicz,dAllegretti,mbecerra)
 Date Created	: 20-Abril-2010
 Description	: Corrijo la fecha de modificaci�n para que no aparezca en Null.
                  Se elimina el uso de la FechaModifCuenta ya que pasa a ser uso exclusivo de la auditoria

 Parameters   	: cdsVehiculos: TClientDataset; IncluirEliminados: Boolean = False
 Return Value   :
}
procedure TCuentasConvenio.ObtenerVehiculos(cdsVehiculos: TClientDataset; IncluirEliminados: Boolean = False);
var
 i : integer;
 Ahora: TDateTime;
 CodigoFact: Integer;
 NumFact:String;
 CodigoConcesionariaNativa: integer; //TASK_014_RME_20170112

// SELECT CodigoConvenioFacturacion FROM Cuentas WHERE CodigoConvenio=4272063 AND IndiceVehiculo=1
//SELECT NumeroConvenio FROM Convenio WHERE CodigoConvenio=4272063
 function ObtenerConvenioFacturacion(CodigoConvenio, CodigoVehiculo:Integer): Integer;
 var
    tiraSQL: string;
 begin
     tiraSQL:= 'SELECT CodigoConvenioFacturacion FROM Cuentas WHERE CodigoConvenio=' + IntToStr(CodigoConvenio) +' AND CodigoVehiculo='+IntToStr(CodigoVehiculo);
     Result:=QueryGetValueINT(DMConnections.BaseCAC,tiraSQL);
 end;

 function ObtenerNumeroConvenioFacturacion(CodigoConvenio:Integer): string;
  var
    tiraSQL: string;
 begin
     tiraSQL:= 'SELECT NumeroConvenio FROM Convenio WHERE CodigoConvenio=' + IntToStr(CodigoConvenio);
     Result:=Trim(QueryGetStringValue(DMConnections.BaseCAC,tiraSQL));
 end;


 function ObtenerDatosConvenioFacturacion(CodigoConvenio:Integer): string;
 begin
     Result:=Trim(QueryGetStringValue(DMConnections.BaseCAC,Format('select dbo.ObtenerDatosConvenioFacturacion(%s)', [IntToStr(CodigoConvenio)])));
 end;

    {Procedure Name	: InsertarVehiculo
     Author 		: Nelson Droguett Sierra ()
     Date Created	: 20-Abril-2010
     Description	:
     Parameters   	: AVehiculo : PVehiculosConvenio; VehiculoEliminado: Boolean = False
     Return Value   :
    }
    procedure InsertarVehiculo(AVehiculo : PVehiculosConvenio; VehiculoEliminado: Boolean);

    begin

        with AVehiculo^ do begin
            //CodigoFact:=ObtenerConvenioFacturacion(CodigoConvenio,Cuenta.Vehiculo.CodigoVehiculo);
            cdsVehiculos.InsertRecord([
                                        0,  //CodigoSolicitudContacto
                                        Cuenta.Vehiculo.CodigoVehiculo,
                                        Cuenta.Vehiculo.CodigoTipoPatente,
                                        Cuenta.Vehiculo.Patente,
                                        Cuenta.Vehiculo.CodigoMarca,
                                        Cuenta.Vehiculo.Marca,
                                        Cuenta.Vehiculo.Modelo,
                                        Cuenta.Vehiculo.CodigoColor, // 0,  // CodigoColor //TASK_016_ECA_20160526
                                        '', // DescripcionColor
                                        Cuenta.Vehiculo.Anio,
                                        '', // Observaciones
    {INICIO: TASK_106_JMA_20170206
                                            Cuenta.Vehiculo.CodigoTipo,
                                            Cuenta.Vehiculo.Tipo,
    TERMINO: TASK_106_JMA_20170206}
                                        iif(Cuenta.ContactSerialNumber <> 0, Cuenta.ContextMark, 0),
                                        iif(Cuenta.ContactSerialNumber <> 0, Cuenta.ContactSerialNumber, Cuenta.ContactSerialNumberOtraConcesionaria),
                                        Cuenta.Vehiculo.DigitoPatente,
                                        iif(cuenta.TieneAcoplado, 1, 0),
                                        iif(Cuenta.Vehiculo.DigitoPatente = '', 1, 0),
                                        Cuenta.CodigoConvenioRNUT, 
                                        iif(Cuenta.ContactSerialNumber <> 0, Trim(SerialNumberToEtiqueta(inttostr(Cuenta.ContactSerialNumber))), Trim(SerialNumberToEtiqueta(inttostr(Cuenta.ContactSerialNumberOtraConcesionaria)))),
                                        //Cuenta.Vehiculo.PatenteRVM,                                //TASK_106_JMA_20170206
                                        //Cuenta.Vehiculo.DigitoPatenteRVM,                          //TASK_106_JMA_20170206
                                        //1, // CodigoConcesionaria                                     //SS_1147_NDR_20140408

                                        ////TASK_014_RME_20170112 ObtenerCodigoConcesionariaNativa, // CodigoConcesionaria        //SS_1147_NDR_20140408

                                        CodigoConcesionariaNativa, //TASK_014_RME_20170112

                                        EsTagVendido,
                                        Cuenta.EstaSuspendida,
                                        Cuenta.TagEnClienteOtroConvenio,
                                        (EsVehiculoNuevo and not EsCambioVehiculo and not EsTagVendido),
                                        Cuenta.EstadoConservacionTAG,
                                        False, // Robado
                                        False, // Perdido
                                        iif(VehiculoEliminado, 'Eliminado', iif(EsTagNuevo or EsVehiculoNuevo, 'Nuevo', iif(EsCambioVehiculo,'Cambio Veh.',iif(EsTagPerdido,'Tev. Perdido',iif(Cuenta.EstaSuspendida,'Suspendida','Activo'))))),
                                        Cuenta.IndicadorListas.Verde,
                                        Cuenta.IndicadorListas.Negra,
                                        VehiculoEliminado,
                                        iif(Cuenta.FechaBajaCuenta <> NullDate, Cuenta.FechaBajaCuenta, Null),
                                        iif(Cuenta.FechaCreacion <> NullDate, Cuenta.FechaCreacion, Ahora),
                                        iif(Cuenta.FechaVencimientoTag <> NullDate,Cuenta.FechaVencimientoTag, Null),
                                        iif(Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_EN_COMODATO,CONST_STR_ALMACEN_COMODATO,iif(Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_ARRIENDO,CONST_STR_ALMACEN_ARRIENDO,iif(Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_ENTREGADO_EN_CUOTAS,CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS,''))),
                                        iif(Cuenta.Suspendida <> NullDate, Cuenta.Suspendida, Null),
                                        null, // Imagen  TASK_004_ECA_20160411
                                        //Cuenta.HabilitadoAMB]);    //SS-1006-NDR-20111105

                                        Cuenta.CodigoConvenioFacturacion,  //TASK_004_ECA_20160411
{INICIO: TASK_036_JMA_20160707
                                        ObtenerNumeroConvenioFacturacion(Cuenta.CodigoConvenioFacturacion),  // 20160603 MGO  //TASK_004_ECA_20160411
}
//                                        ObtenerDatosConvenioFacturacion(Cuenta.CodigoConvenioFacturacion),

                                        Cuenta.DatosConvenioFacturacion,   //TASK_014_RME_20170112

                                        {TERMINO: TASK_036_JMA_20160707}
                                        Cuenta.Vehiculo.Foraneo, // 20160603 MGO
{INICIO: TASK_036_JMA_20160707}
                                        Cuenta.SuscritoListaBlanca,
                                        Cuenta.HabilitadoListaBlanca
{TERMINO: TASK_036_JMA_20160707}
    {INICIO: TASK_106_JMA_20170206}
                                        ,Cuenta.CodigoCategoriaInterurbana
                                        ,Cuenta.CodigoCategoriaUrbana
                                        ,Cuenta.CategoriaInterurbana
                                        ,Cuenta.CategoriaUrbana
    {TERMINO: TASK_106_JMA_20170206}
                ]);
        end;

    end;

begin
    Ahora := NowBase(DmConnections.BaseCAC);
    CodigoConcesionariaNativa := ObtenerCodigoConcesionariaNativa;  //TASK_014_RME_20170112
    //cdsVehiculos.DisableConstraints;
    cdsVehiculos.EmptyDataSet;
    for i := 0 to FVehiculos.Count-1 do
    	InsertarVehiculo(FVehiculos.Items[i],False);

    if IncluirEliminados then
        for i := 0 to FVehiculosEliminados.Count-1 do
            InsertarVehiculo(FVehiculosEliminados.Items[i], True);

    //cdsVehiculos.EnableConstraints;
    cdsVehiculos.First;
end;

{Procedure Name	: TCuentasConvenio.QuitarVehiculo
 Author 		: Nelson Droguett Sierra (dAllegretti,vpaszkowicz)
 Date Created	: 20-Abril-2010
 Description	: Se elimina el uso de la FechaModifCuenta ya que pasa a ser uso exclusivo de la auditoria
                  Uso un corrector de �ndices para que queden bien los indices de veh�culos
 Parameters   	: Index: integer
 Return Value   :
}
procedure TCuentasConvenio.QuitarVehiculo(Index: integer);
var
 AVehiculo : PVehiculosConvenio;
 indice : integer;
begin
    AVehiculo := FVehiculos.Items[Index];
    AVehiculo.AdheridoPA:=False;                                                //SS-1006-NDR-20120715
    if AVehiculo.Documentacion <> nil then begin
        for indice := 0 to AVehiculo.Documentacion.Count - 1 do begin
          Dispose(AVehiculo^.Documentacion.Items[0]);
          AVehiculo^.Documentacion.Delete(0);
        end;
    end;
    // si ese vehiculo no esta en la base, directamente lo borro de la lista
    if AVehiculo^.EsVehiculoNuevo then FVehiculosEliminadosSinGuardar.Add(FVehiculos.Items[Index])
    else FVehiculosQuitados.Add(AVehiculo);
    FVehiculosEliminados.Add(AVehiculo);
    FVehiculos.Delete(Index);
end;

{Procedure Name	: TCuentasConvenio.EditarVehiculo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:  Index: integer; AVehiculo : TVehiculosConvenio
 Return Value   :
}
procedure TCuentasConvenio.EditarVehiculo(Index: integer; AVehiculo : TVehiculosConvenio);
resourcestring
    MSG_INVALID_INDEX = 'Indice fuera de rango';
begin
    if Index > FVehiculos.Count then begin
        raise Exception.Create(MSG_INVALID_INDEX);
        exit;
    end;
    if (not AVehiculo.EsVehiculoNuevo) and (TVehiculosConvenio((FVehiculos.Items[Index])^).Cuenta.ContactSerialNumber <> AVehiculo.Cuenta.ContactSerialNumber) then
        QuitarTagVehiculo(Index);
    if not AVehiculo.EsVehiculoNuevo then AVehiculo.EsVehiculoModificado := True;
    TVehiculosConvenio((FVehiculos.Items[Index])^) := AVehiculo;
end;


{Procedure Name	: TCuentasConvenio.BuscarIndice
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Patente: String
 Return Value   : Integer
}
function TCuentasConvenio.BuscarIndice(Patente: String): Integer;
var
    i:Integer;
begin
    result:=-1;
    for i:=0 to CantidadVehiculos-1 do
        if trim(UpperCase(Vehiculos[i].Cuenta.Vehiculo.Patente))=UpperCase(trim(patente)) then begin
            result:=i;
            break;
        end;
end;

{Procedure Name	: TCuentasConvenio.EditarVehiculo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Patente: String; const AVehiculo: TVehiculosConvenio
 Return Value   :
}
procedure TCuentasConvenio.EditarVehiculo(Patente: String;
  const AVehiculo: TVehiculosConvenio);
begin
    if BuscarIndice(Patente)>=0 then EditarVehiculo(BuscarIndice(Patente),AVehiculo);
end;

{Procedure Name	: TCuentasConvenio.QuitarVehiculo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Patente: String
 Return Value   :
}
procedure TCuentasConvenio.QuitarVehiculo(Patente: String);
begin
    if BuscarIndice(Patente) >= 0 then QuitarVehiculo(BuscarIndice(Patente));
end;

{Procedure Name	: TCuentasConvenio.ObtenerCantidadVehiculosQuitados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TCuentasConvenio.ObtenerCantidadVehiculosQuitados: integer;
begin
    result := FVehiculosQuitados.Count;
end;

{Procedure Name	: TCuentasConvenio.ObtenerDatosVehiculo
 Author 		: Nelson Droguett Sierra (vpaszkowicz)
 Date Created	: 20-Abril-2010
 Description	: Lo modifico para que, si estoy encontrando un veh�culo por su
    RVM, deje el codigo de veh�culo como -1, ya que se va a usar la RVM como
    normal, y en este caso, no existe como tal, por lo tanto deber� darla de alta.
 Parameters   	: BuscarPatenteRVM : Boolean; CodigoTipoPatente,Patente: String; var Vehiculo: TVehiculos
 Return Value   : boolean
}
class function TCuentasConvenio.ObtenerDatosVehiculo(BuscarPatenteRVM : Boolean; CodigoTipoPatente,Patente: String; var Vehiculo: TVehiculos) : boolean ;
begin

    with TADOStoredProc.Create(nil) do
    try
        Connection:=DMConnections.BaseCAC;
        ProcedureName:='ObtenerDatosVehiculo';
        with Parameters do begin
          refresh;
          ParamByName('@CodigoTipoPatente').Value := CodigoTipoPatente;
          ParamByName('@Patente').Value := Patente;
        end;
        Open;
        if recordcount > 0 then begin
            if (BuscarPatenteRVM) or (UpperCase(Trim(fieldbyname('Patente').AsString)) = UpperCase(Trim(Patente))) then begin
               Vehiculo.Patente       := Trim(fieldbyname('Patente').AsString);
               Vehiculo.DigitoPatente := Trim(fieldbyname('DigitoVerificadorPatente').AsString);
               //Vehiculo.PatenteRVM := Trim(fieldbyname('PatenteRVM').AsString);                      //TASK_106_JMA_20170206
               //Vehiculo.DigitoPatenteRVM := Trim(fieldbyname('DigitoVerificadorPatenteRVM').AsString); //TASK_106_JMA_20170206
               Vehiculo.CodigoVehiculo:= fieldbyname('CodigoVehiculo').AsInteger;
            end else begin
                //Lo enocntr� como RVM, lo tengo q dar de alta.
               Vehiculo.Patente       := Trim(fieldbyname('PatenteRVM').AsString);
               Vehiculo.DigitoPatente := Trim(fieldbyname('DigitoVerificadorPatenteRVM').AsString);
               //Vehiculo.PatenteRVM := '';                                                             //TASK_106_JMA_20170206
               //Vehiculo.DigitoPatenteRVM := '';                                                       //TASK_106_JMA_20170206
               Vehiculo.CodigoVehiculo:= -1;
            end;
            Vehiculo.CodigoTipoPatente := Trim(fieldbyname('TipoPatente').AsString);
            Vehiculo.CodigoMarca   := fieldbyname('CodigoMarca').Value;
            Vehiculo.Marca         := Trim(fieldbyname('DescripcionMarca').Value);
            Vehiculo.Modelo        := Trim(fieldbyname('Modelo').AsString);
            //Vehiculo.CodigoTipo    := fieldbyname('CodigoTipoVehiculo').Value;                //TASK_106_JMA_20170206_TODO
            //Vehiculo.Tipo          := Trim(fieldbyname('DescripcionTipoVehiculo').Value);     //TASK_106_JMA_20170206_TODO
            Vehiculo.Anio          := Trim(fieldbyname('AnioVehiculo').AsString);
            Vehiculo.Robado        := Fieldbyname('Robado').AsBoolean;
            Vehiculo.Foraneo       := False;    // 20160603 MGO
            //INICIO: TASK_016_ECA_20160526
            if not fieldbyname('CodigoColor').IsNull then
                Vehiculo.CodigoColor   := fieldbyname('CodigoColor').AsInteger;
            //FIN: TASK_016_ECA_20160526
        end;
        result := recordcount > 0;
    finally
        close;
        free;
    end;
end;

{Procedure Name	: TCuentasConvenio.ObtenerVehiculoAsignadoConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoTipoPatente,Patente: String
 Return Value   : integer
}
class function TCuentasConvenio.ObtenerVehiculoAsignadoConvenio(
  CodigoTipoPatente,Patente: String): integer;
begin
    result := QueryGetValueInt(DMConnections.BaseCAC,' SELECT  top 1 CodigoConvenio	FROM Cuentas  WITH (NOLOCK) ' +
                   ' INNER JOIN MaestroVehiculos  WITH (NOLOCK) ON (Cuentas.CodigoVehiculo=MaestroVehiculos.CodigoVehiculo) ' +
                   ' WHERE MaestroVehiculos.Patente = ''' + Patente + '''' +
                   ' AND MaestroVehiculos.CodigoTipoPatente = ''' + CodigoTipoPatente + '''' +
                   ' AND Cuentas.FechaBajaCuenta is NULL' +
                   ' AND Cuentas.Suspendida is NULL') ;
end;

{Procedure Name	: TCuentasConvenio.QuitarVehiculoSinTag
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   :
}
procedure TCuentasConvenio.QuitarVehiculoSinTag(Index: integer);
begin
end;

{Procedure Name	: TCuentasConvenio.QuitarVehiculoSinTag
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Patente: String
 Return Value   :
}
procedure TCuentasConvenio.QuitarVehiculoSinTag(Patente: String);
begin
    if BuscarIndice(Patente) >= 0 then QuitarVehiculoSinTag(BuscarIndice(Patente));
end;

{Procedure Name	: TCuentasConvenio.GetCantidadTagSinVehiculos
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TCuentasConvenio.GetCantidadTagSinVehiculos: Integer;
begin
    result := 0;
end;

{Procedure Name	: TCuentasConvenio.BuscarIndice
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: SerialNumber: DWORD
 Return Value   : Integer
}
function TCuentasConvenio.BuscarIndice(SerialNumber: DWORD): Integer;
var
    i:Integer;
begin
    result:=-1;
    for i:=0 to CantidadVehiculos-1 do
        if Vehiculos[i].Cuenta.ContactSerialNumber = SerialNumber then result:=i;
end;

{Procedure Name	: TCuentasConvenio.GetVehiculosEliminados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer
 Return Value   : TVehiculosConvenio
}
function TCuentasConvenio.GetVehiculosEliminados(
  Index: Integer): TVehiculosConvenio;
begin
    result := TVehiculosConvenio((FVehiculosQuitados.items[index])^);
end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculosEliminados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: AVehiculos: TRegCuentaVehiculos; Anexar : Boolean
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculosEliminados(
  var AVehiculos: TRegCuentaVehiculos; Anexar : Boolean);
var
 i, ini : integer;
begin
    if Anexar then begin
        ini := length(AVehiculos);
        SetLength(AVehiculos, Length(AVehiculos) + CantidadVehiculosEliminados);
    end else begin
        ini := 0;
        SetLength(AVehiculos, CantidadVehiculosEliminados);
    end;
    for i := ini to ini + CantidadVehiculosEliminados -1  do
    begin
        AVehiculos[i] := VehiculosEliminados[i-ini].Cuenta;
    end;
end;

{Procedure Name	: TCuentasConvenio.QuitarTagVehiculo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer
 Return Value   :
}
procedure TCuentasConvenio.QuitarTagVehiculo(Index: Integer);
begin
end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculos
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var AVehiculos: TRegCuentaVehiculos
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculos(
  var AVehiculos: TRegCuentaVehiculos);
var
 i : integer;
begin
    SetLength(AVehiculos,CantidadVehiculos);
    for i := 0 to CantidadVehiculos - 1 do  AVehiculos[i] := Vehiculos[i].Cuenta
end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculosAgregados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var AVehiculos: TRegCuentaVehiculos; CuentasOriginal: TCuentasConvenio
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculosAgregados(
  var AVehiculos: TRegCuentaVehiculos; CuentasOriginal: TCuentasConvenio);
var
    i : Integer;
begin
    for i := 0 to CantidadVehiculos - 1 do begin
        if (Vehiculos[i].EsVehiculoNuevo) and (not Vehiculos[i].EsActivacionCuenta) then begin
            SetLength(AVehiculos,Length(AVehiculos)+1);
            AVehiculos[Length(AVehiculos)-1] := Vehiculos[i].Cuenta;
        end
    end;
end;


//Se modifica la funci�n para permitir el control del nuevo formato de patentes  AAAA00
//Dado que los SP ya se corrigieron para recibir ambos tipos de patentes, simplemente se utilizan los
//mismos, sin importar si se envia la antigua patente o la nueva
{Procedure Name	: TCuentasConvenio.DigitoVerificadorValido
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Patente, DigitoVerificador:String
 Return Value   : Boolean
}
function TCuentasConvenio.DigitoVerificadorValido(Patente,
            DigitoVerificador:String):Boolean;
var
    SQL: string;
    spVerificarFormatoPatenteChilena,
    spVerificarPatente: TADOStoredProc;
begin
    if (((Length(Patente) = 6) and
      (UpperCase(Patente)[1] in ['A'..'Z']) and
      (UpperCase(Patente)[2] in ['A'..'Z']) and
      (Patente[3] in ['0'..'9']) and
      (Patente[4] in ['0'..'9']) and
      (Patente[5] in ['0'..'9']) and
      (Patente[6] in ['0'..'9']))
      or
      ((Length(Patente) = 6) and
      (UpperCase(Patente)[1] in ['A'..'Z']) and
      (UpperCase(Patente)[2] in ['A'..'Z']) and
      (UpperCase(Patente)[3] in ['A'..'Z']) and
      (UpperCase(Patente)[4] in ['A'..'Z']) and
      (Patente[5] in ['0'..'9']) and
      (Patente[6] in ['0'..'9'])))
    then begin

        Result := False;
            try
            spVerificarFormatoPatenteChilena := TADOStoredProc.Create(nil);
            with spVerificarFormatoPatenteChilena do begin
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'VerificarFormatoPatenteChilena';
                Parameters.Refresh;
            end;

            spVerificarPatente := TADOStoredProc.Create(nil);
            with spVerificarPatente do begin
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'VerificarPatente';
                Parameters.Refresh;
            end;

            spVerificarFormatoPatenteChilena.Parameters.ParamByName('@Patente').Value := Trim(Patente);
            //ShowMessage('antes VerificarFormatoPatenteChilena '+ Trim(Patente));
            spVerificarFormatoPatenteChilena.ExecProc;
            //ShowMessage('despues VerificarFormatoPatenteChilena '+ Trim(Patente));

            if spVerificarFormatoPatenteChilena.Parameters.ParamByName('@Return_Value').Value = 1 then begin
                //ShowMessage('antes VerificarPatente '+ Trim(Patente) + ' ' + Trim(DigitoVerificador));
                spVerificarPatente.Parameters.ParamByName('@Patente').Value := Trim(Patente);
                spVerificarPatente.Parameters.ParamByName('@Digito').Value := Trim(Patente);
                spVerificarPatente.Open;

                Result := spVerificarPatente.FieldByName('Resultado').AsString = '1';
                //ShowMessage('despues VerificarPatente '+ Trim(Patente) + ' ' + Trim(DigitoVerificador));
            end;
            {
            with SP do begin

                result := False;
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'VerificarFormatoPatenteChilena';
                Parameters.Refresh;
                Parameters.ParamByName('@Patente').Value := Trim(Patente);
                ShowMessage('antes VerificarFormatoPatenteChilena '+ Trim(Patente));
                ExecProc;
                ShowMessage('despues VerificarFormatoPatenteChilena '+ Trim(Patente));
                if Parameters.ParamByName('@Return_Value').Value = 1 then begin
                ShowMessage('antes VerificarPatente '+ Trim(Patente) + ' ' + Trim(DigitoVerificador));
                    //Result := True;
                SQL := format('Exec VerificarPatente ''%s'',''%s''',[Trim(Patente),Trim(DigitoVerificador)]);
                ShowMessage(SQL);
                   SQL := QueryGetValue(DMConnections.BaseCAC, SQL);
                   ShowMessage(SQL);
                   REsult := SQL = '1';
                ShowMessage('despues VerificarPatente '+ Trim(Patente));

                end
               else
                    result := False;

            end;
            }
        finally
            if assigned(spVerificarFormatoPatenteChilena) then FreeAndNil(spVerificarFormatoPatenteChilena);
            if assigned(spVerificarPatente) then FreeAndNil(spVerificarPatente);

        end;
    end else result := UpperCase(DigitoVerificador) = 'Z';
end;

{Procedure Name	: TCuentasConvenio.TieneCuentas
 Author 		: Nelson Droguett Sierra (vpaszkowicz)
 Date Created	: 20-Abril-2010
 Description	:  Sirve para saber si estoy modificando un convenio sin cuentas.
 Parameters   	:
 Return Value   : Boolean
}
function TCuentasConvenio.TieneCuentas: Boolean;
begin
    Result := (FVehiculos.Count > 0) or (FVehiculosEliminados.Count > 0)or (FVehiculosQuitados.Count > 0);
end;

{Procedure Name	: TCuentasConvenio.GetCantidadTagsQuitados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TCuentasConvenio.GetCantidadTagsQuitados: Integer;
begin
    Result := 0;
end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculosTagsQuitado
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var AVehiculos: TRegCuentaVehiculos; Anexar : Boolean = False
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculosTagsQuitado(
  var AVehiculos: TRegCuentaVehiculos; Anexar : Boolean = False);
var
 i,ini : integer;
begin
    if Anexar then begin
        ini := length(AVehiculos);
        SetLength(AVehiculos,length(AVehiculos) + CantidadTagsQuitados);
    end else begin
        ini := 0;
        SetLength(AVehiculos,CantidadTagsQuitados);
    end;

    for i := ini to length(AVehiculos) - 1 do  AVehiculos[i] := VehiculosTagsQuitado[i-ini].Cuenta;
end;

{Procedure Name	: TCuentasConvenio.GetVehiculosTagsQuitado
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer
 Return Value   : TVehiculosConvenio
}
function TCuentasConvenio.GetVehiculosTagsQuitado(
  Index: Integer): TVehiculosConvenio;
begin
end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculosModificados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var AVehiculos: TRegCuentaVehiculos; CuentasOriginal: TCuentasConvenio
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculosModificados(
  var AVehiculos: TRegCuentaVehiculos; CuentasOriginal: TCuentasConvenio);
var
    i : Integer;
begin
    for i := 0 to CantidadVehiculos - 1 do begin
        if (not Vehiculos[i].EsVehiculoNuevo) and (Vehiculos[i].EsVehiculoModificado) then begin
            SetLength(AVehiculos,Length(AVehiculos)+1);
            AVehiculos[Length(AVehiculos)-1] := Vehiculos[i].Cuenta;
        end;
    end;

end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculosModificadosTag
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var AVehiculos: TRegCuentaVehiculos; ConModifTag : Boolean = False
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculosModificadosTag(
  var AVehiculos: TRegCuentaVehiculos; ConModifTag : Boolean = False);
var
    i : Integer;
begin
    for i := 0 to CantidadVehiculos - 1 do begin
        if not Vehiculos[i].EsActivacionCuenta and not Vehiculos[i].EsTagPerdido then
            if ConModifTag then begin
                if (not Vehiculos[i].EsVehiculoNuevo) and (Vehiculos[i].EsVehiculoModificado) and (Vehiculos[i].EsTagNuevo) then begin
                    SetLength(AVehiculos,Length(AVehiculos)+1);
                    AVehiculos[Length(AVehiculos)-1] := Vehiculos[i].Cuenta;
                end;
            end else begin
                if (not Vehiculos[i].EsVehiculoNuevo) and (Vehiculos[i].EsVehiculoModificado) and (not Vehiculos[i].EsTagNuevo) then begin
                    SetLength(AVehiculos,Length(AVehiculos)+1);
                    AVehiculos[Length(AVehiculos)-1] := Vehiculos[i].Cuenta;
                end;
            end;
    end;

end;


{Procedure Name	: TCuentasConvenio.GetCantidadVehiculosAgregadosModificados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TCuentasConvenio.GetCantidadVehiculosAgregadosModificados: Integer;
var
    i : Integer;
begin
    Result := 0;
    for i := 0 to CantidadVehiculos - 1 do
        if (Vehiculos[i].EsVehiculoNuevo) or (Vehiculos[i].EsVehiculoModificado) then inc(Result);
end;



{Procedure Name	: TCuentasConvenio.GerCantidadVehiculosEliminadosSinGuardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TCuentasConvenio.GerCantidadVehiculosEliminadosSinGuardar: Integer;
begin
    Result := FVehiculosEliminadosSinGuardar.Count;
end;

{Procedure Name	: TCuentasConvenio.GetVehiculosEliminadosSinGuardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer
 Return Value   : TVehiculosConvenio
}
function TCuentasConvenio.GetVehiculosEliminadosSinGuardar(
  Index: Integer): TVehiculosConvenio;
begin
    Result := TVehiculosConvenio(FVehiculosEliminadosSinGuardar.Items[Index]^);
end;

{Procedure Name	: TCuentasConvenio.DestruirVehiculo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   :
}
procedure TCuentasConvenio.DestruirVehiculo(Index: integer);
var
 AVehiculo : PVehiculosConvenio;
 i : integer;
begin
    AVehiculo := FVehiculos.Items[Index];
    if AVehiculo.Documentacion <> nil then begin
        for i := 0 to AVehiculo.Documentacion.Count - 1 do begin
          Dispose(AVehiculo^.Documentacion.Items[0]);
          AVehiculo^.Documentacion.Delete(0);
        end;
    end;
    // si ese vehiculo no esta en la base, directamente lo borro de la lista
    FVehiculos.Delete(Index);
end;

{Procedure Name	: TCuentasConvenio.DestruirVehiculo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Patente: String
 Return Value   :
}
procedure TCuentasConvenio.DestruirVehiculo(Patente: String);
begin
    if BuscarIndice(Patente) >= 0 then DestruirVehiculo(BuscarIndice(Patente));
end;

{Procedure Name	: TCuentasConvenio.SetNumeroTurno
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Integer
 Return Value   :
}
procedure TCuentasConvenio.SetNumeroTurno(const Value: Integer);
begin
    FNumeroTurno := Value;
end;

{Procedure Name	: TCuentasConvenio.QuitarVehiculoRobado
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   :
}
procedure TCuentasConvenio.QuitarVehiculoRobado(Index: integer);
begin
end;

{Procedure Name	: TCuentasConvenio.QuitarVehiculoRobado
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Patente: String
 Return Value   :
}
procedure TCuentasConvenio.QuitarVehiculoRobado(Patente: String);
begin
    if BuscarIndice(Patente) >= 0 then QuitarVehiculoRobado(BuscarIndice(Patente));
end;

{Procedure Name	: TCuentasConvenio.AlMenosUnaCuentaActiva
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TCuentasConvenio.AlMenosUnaCuentaActiva: Boolean;
var
    AVehiculo : PVehiculosConvenio;
    i: integer;
begin
    Result := False;
    for i := 0 to FVehiculos.Count - 1 do begin
        AVehiculo := FVehiculos.Items[i];
        //Verifica que no se este activando o dando de alta una cuenta (durante esta modificacion al convenio)
        if (AVehiculo.EsVehiculoNuevo) or (AVehiculo.EsActivacionCuenta) then begin
            Result := True;
        end;
        //Verifica que no tenga cuentas activas (desde antes de la modificacion al convenio)
        //if ((AVehiculo.Cuenta.Suspendida = NullDate) and (AVehiculo.Cuenta.FechaBajaCuenta = Nulldate)) then begin
        if (not(AVehiculo.Cuenta.EstaSuspendida) and (AVehiculo.Cuenta.FechaBajaCuenta = Nulldate)) then begin
            Result := True;
        end;
    end;
end;

{Procedure Name	: TCuentasConvenio.AgregarVehiculoTagACobrar
 Author 		: Nelson Droguett Sierra (mbecerra)
 Date Created	: 20-Abril-2010
 Description	: Overloading de la funci�n del mismo nombre, que recibe un nuevo par�metro:
                  IDMotivoMovCuentaTelevia, para ser agregado a VehiculosTagACobrar.
 Parameters   	: Index: Integer; IDMotivoMovCuentaTelevia: Integer; ConceptoMovimiento: Integer; NumeroVoucher: Integer; CodigoAlmacenDestino: Integer; DescripcionConceptoMovimiento: string; TipoAsignacionTag: string
 Return Value   :
}
procedure TCuentasConvenio.AgregarVehiculoTagACobrar(Index: Integer; IDMotivoMovCuentaTelevia: Integer; ConceptoMovimiento: Integer; NumeroVoucher: Integer; CodigoAlmacenDestino: Integer; DescripcionConceptoMovimiento: string; TipoAsignacionTag: string);
var
	MovCuenta : PMovimientoCuenta;
begin
	AgregarVehiculoTagACobrar(	Index, ConceptoMovimiento, DescripcionConceptoMovimiento,
                                  NumeroVoucher, CodigoAlmacenDestino, TipoAsignacionTag);
    MovCuenta := FVehiculosTagsACobrar[FVehiculosTagsACobrar.Count - 1];
    MovCuenta.IDMotivoMovCuentaTelevia := IDMotivoMovCuentaTelevia;
end;

{Procedure Name	: TCuentasConvenio.AgregarVehiculoTagACobrar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Patente: String; EstadoConservacion: Integer; CobrarTag, EntregoSoporte: Boolean; ConceptoMovimiento: Integer; DescripcionConceptoMovimiento:string
 Return Value   :
}
procedure TCuentasConvenio.AgregarVehiculoTagACobrar(Patente: String; EstadoConservacion: Integer; CobrarTag, EntregoSoporte: Boolean; ConceptoMovimiento: Integer; DescripcionConceptoMovimiento:string);
begin
    if BuscarIndice(Patente) >= 0 then AgregarVehiculoTagACobrar(BuscarIndice(Patente), EstadoConservacion, CobrarTag, EntregoSoporte, ConceptoMovimiento, DescripcionConceptoMovimiento, 0 , 0, '');
end;

{Procedure Name	: TCuentasConvenio.AgregarVehiculoTagACobrar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index, ConceptoMovimiento: Integer; DescripcionConceptoMovimiento: string;
                 NumeroVoucher: integer; CodigoAlmacenDestino: integer; TipoAsignacionTag: string
 Return Value   :
}
procedure TCuentasConvenio.AgregarVehiculoTagACobrar(Index,
  ConceptoMovimiento: Integer; DescripcionConceptoMovimiento: string; NumeroVoucher: integer; CodigoAlmacenDestino: integer; TipoAsignacionTag: string);
begin
    //es la version mas resumida, ya que los demas campos ya no se usan
    AgregarVehiculoTagACobrar(Index, 1, True, True, ConceptoMovimiento, DescripcionConceptoMovimiento, NumeroVoucher, CodigoAlmacenDestino, TipoAsignacionTag);
end;


{Procedure Name	: TCuentasConvenio.AgregarVehiculoTagACobrar
 Author 		: Nelson Droguett Sierra (jconcheyro,vpaszkowicz)
 Date Created	: 20-Abril-2010
 Description	:  Antes se permitia un solo cargo (que podia ser tag y soporte, o solo tag, o solo soporte)
            Ahora se permiten varios por vehiculo.
				Le asigno el veh�culoConvenio como puntero y no como dato apun-
			    tado. Esto es para que los datos compartidos de los veh�culos se refresquen
			    en esta colecci�n.
 Parameters   	:  Index: integer; EstadoConservacion: Integer; CobrarTag, EntregoSoporte: Boolean ; ConceptoMovimiento: Integer; DescripcionConceptoMovimiento:string
 Return Value   :
}
procedure TCuentasConvenio.AgregarVehiculoTagACobrar(Index: integer; EstadoConservacion: Integer;
    CobrarTag, EntregoSoporte: Boolean ; ConceptoMovimiento: Integer; DescripcionConceptoMovimiento:string; NumeroVoucher: integer; CodigoAlmacenDestino: integer; TipoAsignacionTag: string);
var
    AVehiculo: PVehiculosConvenio;
    Vehiculo: PMovimientoCuenta;
begin
    AVehiculo := FVehiculos.Items[Index];
    New(Vehiculo);
    //Le asigno directamente el puntero para que quede apuntando al registro
    //original de la colecci�n de trabajo.
    Vehiculo.VehiculosConvenio := AVehiculo;
    Vehiculo^.CobrarTag := CobrarTag;
    Vehiculo^.EntregoSoporte := EntregoSoporte;
    Vehiculo^.OtroConcepto := ConceptoMovimiento;
    Vehiculo^.Importe := ValorDanio(DMConnections.BaseCAC, EstadoConservacion);
    Vehiculo^.DescripcionMotivo := DescripcionConceptoMovimiento ;
    Vehiculo^.AlmacenDestino := CodigoAlmacenDestino;
    Vehiculo^.NumeroVoucher := NumeroVoucher;
    Vehiculo^.TipoAsignacionTag := TipoAsignacionTag ;
    Vehiculo^.IDMotivoMovCuentaTelevia := 0;
    FVehiculosTagsACobrar.Add(Vehiculo);
end;

{Procedure Name	: TCuentasConvenio.GetCantidadVehiculosRobados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TCuentasConvenio.GetCantidadVehiculosRobados: integer;
begin
    Result := 0;
end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculosVehiculosRobados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculosVehiculosRobados(
  var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean);
var
 i,ini : integer;
begin
    if Anexar then begin
        ini := length(AVehiculos);
        SetLength(AVehiculos,length(AVehiculos) + CantidadVehiculosRobados);
    end else begin
        ini := 0;
        SetLength(AVehiculos,CantidadVehiculosRobados);
    end;

    for i := ini to length(AVehiculos) - 1 do  AVehiculos[i] := VehiculosRobados[i-ini].Cuenta;
end;

{Procedure Name	: TCuentasConvenio.GetVehiculosRobados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer
 Return Value   : TVehiculosConvenio
}
function TCuentasConvenio.GetVehiculosRobados(
  Index: Integer): TVehiculosConvenio;
begin
end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculosVehiculosSinTag
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculosVehiculosSinTag(
  var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean);
var
    ini, i: integer;
begin
    if Anexar then begin
        ini := length(AVehiculos);
        SetLength(AVehiculos,length(AVehiculos) + CantidadTagSinVehiculos);
    end else begin
        ini := 0;
        SetLength(AVehiculos,CantidadTagSinVehiculos);
    end;

    for i := ini to length(AVehiculos) - 1 do  AVehiculos[i] := TagSinVehiculos[i - ini].Cuenta;
end;

{Procedure Name	: TCuentasConvenio.GetTagSinVehiculos
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer
 Return Value   : TVehiculosConvenio
}
function TCuentasConvenio.GetTagSinVehiculos(
  Index: Integer): TVehiculosConvenio;
begin
end;


{Procedure Name	: TCuentasConvenio.EditarVehiculosSinDevolucion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: AVehiculo: TVehiculosConvenio
 Return Value   :
}
procedure TCuentasConvenio.EditarVehiculosSinDevolucion(AVehiculo: TVehiculosConvenio);
var
 Vehiculo : PVehiculosConvenio;
begin
    New(Vehiculo);
    Vehiculo^ := AVehiculo;
    Vehiculo^.Documentacion := TList.Create;
    Vehiculo^.EsVehiculoNuevo := False;
    Vehiculo^.EsVehiculoModificado := True;
    Vehiculo^.EsTagNuevo := True;
    FVehiculos.Add(Vehiculo);
end;

{Procedure Name	: TCuentasConvenio.BuscarCobrado
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: SerialNumber: DWORD
 Return Value   : integer
}
function TCuentasConvenio.BuscarCobrado(SerialNumber: DWORD): integer;
var
    i: integer;
begin
    result := -1;
    for i := 0 to CantidadVehiculosACobrar - 1 do
        if VehiculosTagsACobrar[i].Cuenta.ContactSerialNumber = SerialNumber then result := i;
end;

{Procedure Name	: TCuentasConvenio.GetCantidadVehiculosACobrar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TCuentasConvenio.GetCantidadVehiculosACobrar: integer;
begin
    Result := FVehiculosTagsACobrar.Count;
end;

{Procedure Name	: TCuentasConvenio.GetVehiculosTagsACobrar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer
 Return Value   : TVehiculosConvenio
}
function TCuentasConvenio.GetVehiculosTagsACobrar(
  Index: Integer): TVehiculosConvenio;
resourcestring
    MSG_INVALID_INDEX = 'Indice de veh�culos Tags a cobrar fuera de rango';
begin

    if Index > CantidadVehiculosACobrar then begin
        raise Exception.Create(MSG_INVALID_INDEX);
        exit;
    end;
    result := TVehiculosConvenio((FVehiculosTagsACobrar.items[index])^);

end;

{Procedure Name	: TCuentasConvenio.CalcularVencimientoGarantiaTag
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Indice: integer; TipoAsignacionTag: string; var DescriError:string
 Return Value   : boolean
}
function TCuentasConvenio.CalcularVencimientoGarantiaTag(Indice: integer;
  TipoAsignacionTag: string; var DescriError:string): boolean;
ResourceString
    ERROR_FECHA_VENCIMIENTO_GARANTIA_INVALIDA = 'La fecha de vencimiento de la garant�a del telev�a es inv�lida';
    ERROR_PARAMETRO_ANIOS_VTO_GARANTIA_TAG = 'El par�metro ANIOS_VTO_GARANTIA_TAG es inv�lido';
var
    AniosVencimientoGarantiaTelevia: integer;
    AVehiculo : PVehiculosConvenio;
    FechaVencimientoGarantia: TDateTime;
begin
    Result := False;
    try
        AVehiculo := FVehiculos.Items[Indice];
        if TipoAsignacionTag = CONST_OP_TRANSFERENCIA_TAG_NUEVO then begin
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'ANIOS_VTO_GARANTIA_TAG', AniosVencimientoGarantiaTelevia);
            if (Avehiculo.Cuenta.FechaAltaTag = NullDate) or (Avehiculo.Cuenta.FechaAltaTag = 0) then begin
                AVehiculo.Cuenta.FechaVencimientoTag := IncMonth( NowBase(DMConnections.BaseCAC) , (12 * AniosVencimientoGarantiaTelevia  ));
            end else begin
                AVehiculo.Cuenta.FechaVencimientoTag := IncMonth( Avehiculo.Cuenta.FechaAltaTag , (12 * AniosVencimientoGarantiaTelevia  ));
            end;

            if AniosVencimientoGarantiaTelevia <> NullDate then
                Result := True
            else begin
                DescriError := ERROR_PARAMETRO_ANIOS_VTO_GARANTIA_TAG;
                Exit;
            end;

        end else begin
            FechaVencimientoGarantia := QueryGetValueDateTime(DMConnections.BaseCAC,
              format('select dbo.ObtenerVencimientoGarantiaTag(%d,%d)',[AVehiculo.Cuenta.ContextMark ,AVehiculo.Cuenta.ContactSerialNumber]));
            AVehiculo.Cuenta.FechaVencimientoTag := FechaVencimientoGarantia;
            if FechaVencimientoGarantia <> NullDate then
                Result := True
            else begin
                DescriError := ERROR_FECHA_VENCIMIENTO_GARANTIA_INVALIDA;
                Exit;
            end;
        end;
    except
        on E: Exception do begin
            DescriError := E.message;
            Exit;
        end;
    end;
    Result := True;
end;

{Procedure Name	: TCuentasConvenio.CambiarEstadoConservacionTAG
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer; EstadoConservacionTAG: integer
 Return Value   :
}
procedure TCuentasConvenio.CambiarEstadoConservacionTAG(Index: integer; EstadoConservacionTAG: integer);
var
    AVehiculo : PVehiculosConvenio;
begin
    AVehiculo := FVehiculos.Items[Index];
    AVehiculo.Cuenta.EstadoConservacionTAG := EstadoConservacionTAG;
    EditarVehiculo(Index, AVehiculo^ ); //Actualizo el vehiculo
    //Agrego el cambio del estado en la lista de cambio de estado
    AgregarCambiarEstadoTags(AVehiculo^);

end;

{Procedure Name	: TCuentasConvenio.QuitarVehiculoTagPerdido
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   :
}
procedure TCuentasConvenio.QuitarVehiculoTagPerdido(Index: integer);
begin
end;

{Procedure Name	: TCuentasConvenio.QuitarVehiculoTagPerdido
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Patente: String
 Return Value   :
}
procedure TCuentasConvenio.QuitarVehiculoTagPerdido(Patente: String);
begin
    if BuscarIndice(Patente) >= 0 then QuitarVehiculoTagPerdido(BuscarIndice(Patente));
end;

{Procedure Name	: TCuentasConvenio.GetCantidadVehiculosTagPerdido
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TCuentasConvenio.GetCantidadVehiculosTagPerdido: integer;
begin
    Result := 0;
end;

{Procedure Name	: TCuentasConvenio.GetVehiculosTagPerdido
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer
 Return Value   : TVehiculosConvenio
}
function TCuentasConvenio.GetVehiculosTagPerdido(
  Index: Integer): TVehiculosConvenio;
begin
end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculosVehiculosTagPerdido
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculosVehiculosTagPerdido(
  var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean);
var
 i,ini : integer;
begin
    if Anexar then begin
        ini := length(AVehiculos);
        SetLength(AVehiculos,length(AVehiculos) + CantidadVehiculosRobados);
    end else begin
        ini := 0;
        SetLength(AVehiculos,CantidadVehiculosRobados);
    end;

    for i := ini to length(AVehiculos) - 1 do  AVehiculos[i] := VehiculosRobados[i-ini].Cuenta;
end;


{Procedure Name	: TCuentasConvenio.Suspender
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	: Le agrego un control para borrar la patente RVM en caso de suspen-
si�n y su d�gito verificador.
 Parameters   	: Index: integer; FechaLimite: TDateTime; Voluntario: Boolean
 Return Value   :
}
procedure TCuentasConvenio.Suspender(Index: integer; FechaLimite: TDateTime; Voluntario: Boolean);
resourcestring
    MSG_INVALID_INDEX = 'Indice de veh�culos Suspendido fuera de rango';
var
    AuxVehiculo : TVehiculosConvenio;
begin
    if Index > CantidadVehiculos then begin
        raise Exception.Create(MSG_INVALID_INDEX);
        exit;
    end;

    AuxVehiculo := GetVehiculo(Index);
    if FechaLimite = nulldate then begin // significa que es una activacion
        AuxVehiculo.Cuenta.Suspendida := NullDate;
        AuxVehiculo.Cuenta.DeadlineSuspension := NullDate;
        AuxVehiculo.Cuenta.SuspensionForzada := false;
        AuxVehiculo.Cuenta.EstaSuspendida := False;
        AuxVehiculo.EsCambioEstado := False;
    end else begin
        AuxVehiculo.Cuenta.Suspendida := NowBaseSmall(DMConnections.BaseCAC);
        //Si es mas peque�a o igual que la fecha de alta de ella misma hay que sumarle un minuto.
        //Debido a que la suspensi�n es siempre mayor que el alta.
        //y que solo puedo suspender cuentas que ya hayan sido grabadas.
        if AuxVehiculo.Cuenta.Suspendida <= AuxVehiculo.Cuenta.FechaCreacion then
            AuxVehiculo.Cuenta.Suspendida := incMinute(AuxVehiculo.Cuenta.FechaCreacion, 1);
        AuxVehiculo.Cuenta.DeadlineSuspension := FechaLimite;
        AuxVehiculo.Cuenta.SuspensionForzada := not Voluntario;
        AuxVehiculo.Cuenta.EstaSuspendida := True;
        AuxVehiculo.EsCambioEstado := True;
    end;
{INICIO: TASK_106_JMA_20170206
    if Trim(TVehiculos(AuxVehiculo.Cuenta.Vehiculo).PatenteRVM) <> '' then begin
        TVehiculos(AuxVehiculo.Cuenta.Vehiculo).PatenteRVM := '';
        TVehiculos(AuxVehiculo.Cuenta.Vehiculo).DigitoPatenteRVM := '';
        AuxVehiculo.EsVehiculoModificado := True;
    end;
TERMINO: TASK_106_JMA_20170206}
    EditarVehiculo(Index, AuxVehiculo);
end;

{Procedure Name	: TCuentasConvenio.TieneCuentasSuspendidas
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : boolean
}
function TCuentasConvenio.TieneCuentasSuspendidas: boolean;
var
    i: Integer;
begin
    Result := False;
    for i := 0 to CantidadVehiculos - 1 do
        if Vehiculos[i].Cuenta.EstaSuspendida then Result := True;
end;

{Procedure Name	: TCuentasConvenio.GetObtenerCantidadVehiculosNuevos
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TCuentasConvenio.GetObtenerCantidadVehiculosNuevos: Integer;
var
    i, Cantidad: integer;
begin
    Cantidad := 0;
    for i := 0 to CantidadVehiculos - 1 do begin
        if (Vehiculos[i].EsVehiculoNuevo) then begin
            Cantidad := Cantidad + 1;
        end;
    end;
    result := Cantidad;
end;



{Procedure Name	: TCuentasConvenio.QuitarCuentaSuspendida
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: AVehiculo: TVehiculosConvenio
 Return Value   :
}
procedure TCuentasConvenio.QuitarCuentaSuspendida(AVehiculo: TVehiculosConvenio);
begin
end;

{Procedure Name	: TCuentasConvenio.QuitarVehiculoPorCambioVehiculo
 Author 		: Nelson Droguett Sierra (dAllegretti)
 Date Created	: 20-Abril-2010
 Description	: Se elimina el uso de la FechaModifCuenta ya que pasa a ser uso exclusivo de la auditoria
 Parameters   	: Index: integer
 Return Value   :
}
procedure TCuentasConvenio.QuitarVehiculoPorCambioVehiculo(Index: integer);
var
 AVehiculo : PVehiculosConvenio;
 i : integer;
begin
    AVehiculo := FVehiculos.Items[Index];
    if AVehiculo.Documentacion <> nil then begin
        for i := 0 to AVehiculo.Documentacion.Count - 1 do begin
          Dispose(AVehiculo^.Documentacion.Items[0]);
          AVehiculo^.Documentacion.Delete(0);
        end;
    end;
    // si ese vehiculo no esta en la base, directamente lo borro de la lista
    if AVehiculo^.EsVehiculoNuevo then FVehiculosEliminadosSinGuardar.Add(FVehiculos.Items[Index])
    else begin
        AVehiculo.EsCambioVehiculo := True;
        FVehiculosQuitados.Add(AVehiculo);
    end;
    FVehiculos.Delete(Index);
end;

{Procedure Name	: TCuentasConvenio.QuitarVehiculoPorCambioVehiculo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:  Patente: String
 Return Value   :
}
procedure TCuentasConvenio.QuitarVehiculoPorCambioVehiculo(
  Patente: String);
begin
    if BuscarIndice(Patente) >= 0 then QuitarVehiculoPorCambioVehiculo(BuscarIndice(Patente));
end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculosNOModificados
 Author 		: Nelson Droguett Sierra (nefernandez)
 Date Created	: 20-Abril-2010
 Description	: SS 622: Se agrega la recuperaci�n de los vehiculos dados de baja
                  (para mostrar en re-imprimir convenio), cuyas fechas de bajas no sean anteriores
                  a N dias (N: parametro general "DIAS_DIF_CUENTAS_REIMPRIMIR")
 Parameters   	:
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculosNOModificados(
  var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean);
var
    i: Integer;
    AVehiculoEliminados : PVehiculosConvenio;
    YNow, MNow, DNow, YBaja, MBaja, DBaja : Word;
    DiferenciaDia, DiferenciaDiaParam: Integer;
begin
    if not Anexar then SetLength(AVehiculos, 0);


    for i := 0 to CantidadVehiculos - 1 do begin
        if (not Vehiculos[i].EsVehiculoNuevo) and (not Vehiculos[i].EsVehiculoModificado) and
            (not Vehiculos[i].EsTagNuevo) //and (not Vehiculos[i].EsTagVendido) and  (not Vehiculos[i].EsCambioVehiculo)then begin
              and  (not Vehiculos[i].EsCambioVehiculo)then begin
                SetLength(AVehiculos,Length(AVehiculos) + 1);
                AVehiculos[Length(AVehiculos) - 1] := Vehiculos[i].Cuenta;
        end;
    end;

    ObtenerParametroGeneral(DMConnections.BaseCAC,DIAS_DIF_MIN_FECHAS_CUENTAS_REIMPRIMIR,DiferenciaDiaParam);
    DecodeDate(NowBase(DMConnections.BaseCAC), YNow, MNow, DNow);
    for i := 0 to FVehiculosEliminados.Count - 1 do begin
        AVehiculoEliminados := FVehiculosEliminados.items[i];
        DecodeDate(AVehiculoEliminados.Cuenta.FechaBajaCuenta, YBaja, MBaja, DBaja);
        //DiferenciaDia := StrToInt(IntToStr(YNow) + IntToStr(MNow) + IntToStr(DNow)) - StrToInt(IntToStr(YBaja) + IntToStr(MBaja) + IntToStr(DBaja));
        DiferenciaDia := Trunc((NowBase(DMConnections.BaseCAC)) - Trunc(AVehiculoEliminados.Cuenta.FechaBajaCuenta));
        if DiferenciaDia <= DiferenciaDiaParam then begin
            SetLength(AVehiculos,Length(AVehiculos) + 1);
            AVehiculos[Length(AVehiculos) - 1] := AVehiculoEliminados.Cuenta;
        end;
    end;
end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculosActivados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculosActivados(
  var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean);
var
    i: Integer;
begin
    if not Anexar then SetLength(AVehiculos, 0);

    for i := 0 to CantidadVehiculos - 1 do begin
        if Vehiculos[i].EsActivacionCuenta then begin
            SetLength(AVehiculos,Length(AVehiculos)+1);
            AVehiculos[Length(AVehiculos)-1] := Vehiculos[i].Cuenta;
        end;
    end;

end;

{Procedure Name	: TCuentasConvenio.ObtenerRegVehiculosTagPerdidos
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean
 Return Value   :
}
procedure TCuentasConvenio.ObtenerRegVehiculosTagPerdidos(
  var AVehiculos: TRegCuentaVehiculos; Anexar: Boolean);
var
    i: Integer;
begin
    if not Anexar then SetLength(AVehiculos, 0);

    for i := 0 to CantidadVehiculos - 1 do begin
        if (Vehiculos[i].EsVehiculoModificado) and (Vehiculos[i].EsTagPerdido) then begin
            SetLength(AVehiculos,Length(AVehiculos)+1);
            AVehiculos[Length(AVehiculos)-1] := Vehiculos[i].Cuenta;
        end;
    end;

end;

{Procedure Name	: TCuentasConvenio.BuscarIndiceCambiarEstadoTags
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: SerialNumber: DWORD
 Return Value   : Integer
}
function TCuentasConvenio.BuscarIndiceCambiarEstadoTags(
  SerialNumber: DWORD): Integer;
var
    i:Integer;
begin
    result := -1;
    for i := 0 to FVehiculosCambiarEstadoTags.Count - 1 do begin
        if PVehiculosConvenio(FVehiculosCambiarEstadoTags[i]).Cuenta.ContactSerialNumber = SerialNumber then begin
            result := i;
            break;
        end;
    end;
end;

{Procedure Name	: TCuentasConvenio.AgregarCambiarEstadoTags
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: AVehiculo: TVehiculosConvenio
 Return Value   :
}
procedure TCuentasConvenio.AgregarCambiarEstadoTags(
  AVehiculo: TVehiculosConvenio);
var
    Vehiculo: PVehiculosConvenio;
    indice: Integer;
begin
    //Si esta cargado lo edito, para no tenerlo cargado dos veces
    indice := BuscarIndiceCambiarEstadoTags(AVehiculo.Cuenta.ContactSerialNumber);
    if indice > -1 then begin
        Vehiculo := FVehiculosCambiarEstadoTags[indice];
        Dispose(Vehiculo);
        FVehiculosCambiarEstadoTags.Delete(Indice);
    end;

    New(Vehiculo);
    Vehiculo^ := AVehiculo;
    Vehiculo^.Documentacion := nil;
    FVehiculosCambiarEstadoTags.Add(Vehiculo);
end;

{Procedure Name	: TCuentasConvenio.TieneTagVendidosSinUsar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TCuentasConvenio.TieneTagVendidosSinUsar: Boolean;
begin
    Result := True;
end;

{Procedure Name	: TCuentasConvenio.TodasLasCuentasDeBaja
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TCuentasConvenio.TodasLasCuentasDeBaja: Boolean;
begin
    Result := False;
    if FVehiculos.Count = 0 then Result := True;
end;

{Procedure Name	: TCuentasConvenio.AgregarCambioVehiculo
 Author 		: Nelson Droguett Sierra (dAllegretti)
 Date Created	: 20-Abril-2010
 Description	: Se elimina el uso de la FechaModifCuenta ya que pasa a ser uso exclusivo de la auditoria
 Parameters   	: const AVehiculo: TVehiculosConvenio
 Return Value   :
}
procedure TCuentasConvenio.AgregarCambioVehiculo(const AVehiculo: TVehiculosConvenio; pFechaAlta: TDateTime);   // SS_916_PDO_20120109
resourcestring
    MSG_VEHICULO_YA_ASIGNADO = 'El Veh�culo ya se encuentra asignado a un convenio';
    MSG_CUENTA_SUSPENDIDA = 'La cuenta ya fue suspendida';
var
 Vehiculo : PVehiculosConvenio;
 i : Integer;
begin
     if (ObtenerVehiculoAsignadoConvenio(AVehiculo.Cuenta.Vehiculo.CodigoTipoPatente,
           AVehiculo.Cuenta.Vehiculo.Patente) > 0) and
        (ObtenerVehiculoAsignadoConvenio(AVehiculo.Cuenta.Vehiculo.CodigoTipoPatente,
          AVehiculo.Cuenta.Vehiculo.Patente) <> CodigoConvenio ) then
            raise ECuentaError.Create(MSG_VEHICULO_YA_ASIGNADO,'',ERRORVEHICULOYAASIGNADO);


    for i := 0 to FVehiculos.Count - 1 do begin
        if (TVehiculosConvenio(FVehiculos.items[i]^).Cuenta.ContactSerialNumber = AVehiculo.Cuenta.ContactSerialNumber) and
            (TVehiculosConvenio(FVehiculos.items[i]^).Cuenta.EstaSuspendida) then begin
            raise ECuentaError.Create(MSG_CUENTA_SUSPENDIDA,'',ERRORCUENTASUSPENDIDA);
       end;
    end;

    New(Vehiculo);
    Vehiculo^ := AVehiculo;
    Vehiculo^.Documentacion := TList.Create;
    Vehiculo^.EsVehiculoNuevo := True;
    Vehiculo^.EsTagNuevo := False;
    Vehiculo^.EsCambioTag := False;
    Vehiculo^.EsCambioVehiculo := True;
    Vehiculo^.EsVehiculoRobado := AVehiculo.EsVehiculoRobado;
    Vehiculo^.EsVehiculoRecuperado := AVehiculo.EsVehiculoRecuperado;
    //Vehiculo^.Cuenta.FechaCreacion := NowBaseSmall(DMConnections.BaseCAC);    // SS_916_PDO_20120109
    Vehiculo^.Cuenta.FechaCreacion := pFechaAlta;                               // SS_916_PDO_20120109
    FVehiculos.Add(Vehiculo);
end;

{Procedure Name	: TCuentasConvenio.ActivarCuenta
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   :
}
procedure TCuentasConvenio.ActivarCuenta(Index: integer);
begin
    Suspender(Index, NullDate, false);
end;


{Procedure Name	: TCuentasConvenio.AgregarCuentaActivada
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: Integer; Vehiculo: TVehiculosConvenio
 Return Value   :
}
procedure TCuentasConvenio.AgregarCuentaActivada(Index: Integer; Vehiculo: TVehiculosConvenio);
var
 AVehiculo : PVehiculosConvenio;
 i : integer;
begin
    AVehiculo := FVehiculos.Items[Index];
    //Le agrego los datos del televia.
    Vehiculo.Cuenta.FechaVencimientoTag := AVehiculo.Cuenta.FechaVencimientoTag;
    Vehiculo.Cuenta.FechaAltaTag := AVehiculo.Cuenta.FechaAltaTag;
    if AVehiculo.Documentacion <> nil then begin
        for i := 0 to AVehiculo.Documentacion.Count - 1 do begin
          Dispose(AVehiculo^.Documentacion.Items[0]);
          AVehiculo^.Documentacion.Delete(0);
        end;
    end;

    FCuentasActivadas.Add(AVehiculo);
    FVehiculos.Delete(Index);
    Vehiculo.EsActivacionCuenta := True;
    AgregarVehiculo(Vehiculo);
    Vehiculo := Vehiculos[CantidadVehiculos - 1];
    Vehiculo.EsCambioVehiculo := True;
    Vehiculo.Cuenta.TipoAsignacionTag := CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA;
    EditarVehiculo(Vehiculo.Cuenta.Vehiculo.Patente, Vehiculo);
end;


{Procedure Name	: TCuentasConvenio.ObtenerVehiculo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Patente: String
 Return Value   : TVehiculosConvenio
}
function TCuentasConvenio.ObtenerVehiculo(
  Patente: String): TVehiculosConvenio;
begin
    result := Vehiculos[BuscarIndice(Patente)];

end;

{Procedure Name	: TCuentasConvenio.AgregarConceptoAPagar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index, CodigoConcepto: Integer; Importe: Integer; Credito: Boolean
 Return Value   :
}
procedure TCuentasConvenio.AgregarConceptoAPagar(Index,
  CodigoConcepto: Integer; Importe: Integer; Credito: Boolean);
var
    AVehiculo: PVehiculosConvenio;
    Vehiculo: PMovimientoCuenta;
begin
    AVehiculo := FVehiculos.Items[Index];
    if BuscarCobrado(AVehiculo.Cuenta.ContactSerialNumber) = -1 then begin
        New(Vehiculo);
        Vehiculo^.VehiculosConvenio := AVehiculo;
        Vehiculo^.EntregoSoporte := False;
        Vehiculo^.OtroConcepto := CodigoConcepto;
        Vehiculo^.Importe := Importe;
        if Credito then Vehiculo^.Importe := -Vehiculo^.Importe;

        FVehiculosTagsACobrar.Add(Vehiculo);
    end;

end;

{Procedure Name	: TCuentasConvenio.TieneCuentasActivas
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TCuentasConvenio.TieneCuentasActivas: Boolean;
var
    i: Integer;
begin
    Result := False;
    for i := 0 to ObtenerCantidadVehiculos - 1 do
        if not (vehiculos[i].Cuenta.EstaSuspendida) then Result := True;

end;


{ TRepresentantesLegales }

{Procedure Name	: TRepresentantesLegales.AgregarRepresentante
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: DatosPersonales: TDatosPersonales; NroRepresentante: Integer = -1
 Return Value   :
}
procedure TRepresentantesLegales.AgregarRepresentante(DatosPersonales: TDatosPersonales; NroRepresentante: Integer = -1);
resourcestring
    MSG_REP_INVALID = 'Error, Nro de representante no valido';
var
 Persona : TRepresentanteLegal;
begin
    Persona := TRepresentanteLegal.Create;
    Persona.Datos := DatosPersonales;
    if NroRepresentante < 0 then begin
        Persona.NroRepresentante := FRepresentantes.Count;
        FRepresentantes.Add(Persona);
    end else begin
        if NroRepresentante  > CantidadRepresentantes then begin
            raise Exception.Create(MSG_REP_INVALID);
            exit;
        end;
        if VetrificarIntegeridadNroRepresentate(NroRepresentante) then begin
            Persona.NroRepresentante := NroRepresentante;
            FRepresentantes.Insert(NroRepresentante,Persona);
        end;
    end;
end;

{Procedure Name	: TRepresentantesLegales.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoConvenio : integer; CodigoTitularConvenio: Integer
 Return Value   :
}
constructor TRepresentantesLegales.Create(CodigoConvenio : integer; CodigoTitularConvenio: Integer);
begin
    FTitularConvenio := CodigoTitularConvenio;
    FCodigoConvenio := CodigoConvenio;
    FRepresentantes := TList.create;
    FRepresentantesQuitados := TList.Create;
    with TADOStoredProc.Create(nil) do
    try
        Connection:=DMConnections.BaseCAC;
        ProcedureName:='ObtenerRepresentateLegal';
        with Parameters do begin
            Refresh;
            ParamByName('@CodigoConvenio').Value:= FCodigoConvenio;
        end;
        open;
        while not(Eof) do begin
            FRepresentantes.Add(TRepresentanteLegal.Create(FieldByName('CodigoPersona').AsInteger, FieldByName('NroRepresentate').AsInteger ) );
            next;
        end;
    finally
        close;
        free;
    end;
end;

{Procedure Name	: TRepresentantesLegales.Destroy
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
destructor TRepresentantesLegales.Destroy;
var  i : integer;
begin
  for i := 0 to FRepresentantes.Count - 1 do
    TDatosPersona(FRepresentantes.Items[i]).Free;

  if ASsigned(FRepresentantes) then FRepresentantes.Free;

  for i := 0 to FRepresentantesQuitados.Count - 1 do
    TDatosPersona(FRepresentantesQuitados.Items[i]).Free;

  if ASsigned(FRepresentantesQuitados) then FRepresentantesQuitados.Free;

  inherited;
end;


{Procedure Name	: TRepresentantesLegales.GetCantidadRepresentantesQuitados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TRepresentantesLegales.GetCantidadRepresentantesQuitados: integer;
begin
    result := FRepresentantesQuitados.Count;
end;

{Procedure Name	: TRepresentantesLegales.GetRepresentantesQuitados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   : TRepresentanteLegal
}
function TRepresentantesLegales.GetRepresentantesQuitados(
  Index: integer): TRepresentanteLegal;
resourcestring
    MSG_INVALID_INDEX = 'Indice Fuera de rango';
begin
    if (Index < 0) or (Index > FRepresentantesQuitados.Count) then raise Exception.Create(MSG_INVALID_INDEX)
    else result := FRepresentantesQuitados[Index];
end;

{Procedure Name	: TRepresentantesLegales.guardar
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: Guarda la modificaci�n de Representantes Legales.
 Parameters   	:
 Return Value   :
}
procedure TRepresentantesLegales.guardar;
resourcestring
    MSG_SAVE_ERROR = 'Error al guardar el representante legal.';
var
 i : integer;
begin
with TADOStoredProc.Create(nil) do
    try
       Connection:=DMConnections.BaseCAC;

       ProcedureName:='ActualizarRepresentantesConvenios';
       Parameters.Refresh;
       try
            for i := 0 to FRepresentantes.Count - 1 do begin
                if TRepresentanteLegal(FRepresentantes.Items[i]).EsNuevo or TRepresentanteLegal(FRepresentantes.Items[i]).EsModificado then
                    TRepresentanteLegal(FRepresentantes.Items[i]).Guardar;
            end;

            with Parameters do begin
                ParamByName ('@CodigoClienteConvenio').Value := FTitularConvenio;
                if FRepresentantes.Count > 0 then ParamByName ('@CodigoRepLegal1').Value := TRepresentanteLegal(FRepresentantes.Items[0]).CodigoPersona;
                if FRepresentantes.Count > 1 then ParamByName ('@CodigoRepLegal2').Value := TRepresentanteLegal(FRepresentantes.Items[1]).CodigoPersona;
                if FRepresentantes.Count > 2 then ParamByName ('@CodigoRepLegal3').Value := TRepresentanteLegal(FRepresentantes.Items[2]).CodigoPersona;
            end;

            ExecProc;

            close;
       except
           on E: Exception do begin
              if E.ClassType = Exception then
                raise EPersonaError.create(MSG_SAVE_ERROR, e.message , 0)
              else raise;
           end;
       end;
    finally
        free;
    end;
end;

{Procedure Name	: TRepresentantesLegales.ModificarRepresentante
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer;DatosPersonales: TDatosPersonales; NroRepresentante: Integer = -1
 Return Value   :
}
procedure TRepresentantesLegales.ModificarRepresentante(Index: integer;
  DatosPersonales: TDatosPersonales; NroRepresentante: Integer = -1);
resourcestring
    MSG_INVALID_INDEX = 'Indice del Representante Legal no valido.';
begin
    if CantidadRepresentantes < (Index + 1) then begin
        raise Exception.Create(MSG_INVALID_INDEX);
        exit;
    end;

    Representantes[Index].Datos := DatosPersonales;
    if NroRepresentante > 0 then begin
        if VetrificarIntegeridadNroRepresentate(NroRepresentante, Index) then
            Representantes[Index].NroRepresentante := NroRepresentante;
    end;

end;

{Procedure Name	: TRepresentantesLegales.ModificarRepresentante
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: TipoDocumento, NumeroDocumento: string; DatosPersonales: TDatosPersonales; NroRepresentante: Integer = -1
 Return Value   :
}
procedure TRepresentantesLegales.ModificarRepresentante(TipoDocumento,
  NumeroDocumento: string; DatosPersonales: TDatosPersonales; NroRepresentante: Integer = -1);
begin
    if ObtenerIndice(TipoDocumento,NumeroDocumento) >= 0 then ModificarRepresentante(ObtenerIndice(TipoDocumento,NumeroDocumento), DatosPersonales, NroRepresentante);
end;

{Procedure Name	: TRepresentantesLegales.ObtenerCantidadRepresentantes
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TRepresentantesLegales.ObtenerCantidadRepresentantes: integer;
begin
    result := FRepresentantes.Count;
end;

{Procedure Name	: TRepresentantesLegales.ObtenerIndice
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: TipoDocumento,NumeroDocumento: string
 Return Value   : Integer
}
function TRepresentantesLegales.ObtenerIndice(TipoDocumento,
  NumeroDocumento: string): Integer;
var
    i: Integer;
begin
    result := -1;
    for i := 0 to FRepresentantes.Count - 1 do
     if (TRepresentanteLegal(FRepresentantes[i]).Datos.NumeroDocumento = NumeroDocumento) and
        (TRepresentanteLegal(FRepresentantes[i]).Datos.TipoDocumento = TipoDocumento) then  result := i;
end;

{Procedure Name	: TRepresentantesLegales.ObtenerRepresentante
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   : TRepresentanteLegal
}
function TRepresentantesLegales.ObtenerRepresentante(
  Index: integer): TRepresentanteLegal;
resourcestring
    MSG_INVALID_INDEX = 'Indice Fuera de rango';
begin
    if (Index < 0) or (Index > FRepresentantes.Count) then raise Exception.Create(MSG_INVALID_INDEX)
    else result := FRepresentantes.Items[Index];
end;

{Procedure Name	: TRepresentantesLegales.QuitarRepresentant
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   :
}
procedure TRepresentantesLegales.QuitarRepresentante(Index: integer);
begin
    FRepresentantesQuitados.Add(TRepresentanteLegal(FRepresentantes[Index]));
    FRepresentantes.Delete(Index);
end;


{Procedure Name	: TRepresentantesLegales.QuitarRepresentante
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: TipoDocumento, NumeroDocumento: string
 Return Value   :
}
procedure TRepresentantesLegales.QuitarRepresentante(TipoDocumento,
  NumeroDocumento: string);
begin
    if ObtenerIndice(TipoDocumento,NumeroDocumento) >= 0 then QuitarRepresentante(ObtenerIndice(TipoDocumento,NumeroDocumento));
end;

{Procedure Name	: TRepresentantesLegales.QuitarRepresentanteDeLista
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   :
}
procedure TRepresentantesLegales.QuitarRepresentanteDeLista(
  Index: integer);
begin
    Dispose(FRepresentantes.Items[index]);
    FRepresentantes.Delete(Index);
end;

{Procedure Name	: TRepresentantesLegales.QuitarRepresentanteDeLista
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: TipoDocumento, NumeroDocumento: string
 Return Value   :
}
procedure TRepresentantesLegales.QuitarRepresentanteDeLista(TipoDocumento,
  NumeroDocumento: string);
begin
    if ObtenerIndice(TipoDocumento,NumeroDocumento) >= 0 then QuitarRepresentanteDeLista(ObtenerIndice(TipoDocumento,NumeroDocumento));
end;

{Procedure Name	: TRepresentantesLegales.VetrificarIntegeridadNroRepresentate
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: NroRepresentante: Integer; IndiceActual: Integer
 Return Value   : Boolean
}
function TRepresentantesLegales.VetrificarIntegeridadNroRepresentate(
  NroRepresentante: Integer; IndiceActual: Integer): Boolean;
resourcestring
    MSG_REP_ERROR = 'Error, Rep Legal ya cargado';
var
    i: Integer;
begin
    Result := True;
    for i := 0 to CantidadRepresentantes - 1 do
        if (( IndiceActual < 0) or (IndiceActual <> i)) and
           (ObtenerRepresentante(i).NroRepresentante = NroRepresentante) then
            Result := False;

    if not Result then
        raise Exception.Create(MSG_REP_ERROR);
end;

// INICIO : TASK_070_MGO_20160907
{ TBajasForzadasConvenio }

function TBajasForzadasConvenio.ObtenerCantidadBajasForzadas: Integer;
begin
	Result := FBajasForzadas.Count;
end;

function TBajasForzadasConvenio.ObtenerBajaForzada(Index: integer): TBajaForzada;
begin
	Result := TBajaForzada((FBajasForzadas.Items[Index])^);
end;      

function TBajasForzadasConvenio.TieneBajaForzada(Patente: string): Boolean;
var
    I: Integer;
begin
    Result := False;

    for I := 0  to FBajasForzadas.Count - 1 do
		if (Trim(BajasForzadas[I].Patente) = Trim(Patente)) then
			Result := True;
end;

function TBajasForzadasConvenio.BuscarIndice(CodigoBajaForzada: Integer): Integer;
var
   I: Integer;
begin
	Result := -1;
	for I := 0  to FBajasForzadas.Count - 1 do
		if (BajasForzadas[I].CodigoBajaForzada = CodigoBajaForzada) then 
			Result := I;
end;

procedure TBajasForzadasConvenio.AgregarVehiculo(IndiceVehiculo: Integer);
resourcestring
    MSG_ERROR_SELECT = 'Error al obtener las bajas forzadas del convenio.';
var
    ABajaForzada : PBajaForzada;
begin
    with TADOStoredProc.Create(nil) do
    try
        try
            Connection := DMConnections.BaseCAC;
            ProcedureName := 'ObtenerDatosBajaForzada';
            with Parameters do begin
                Refresh;
                ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
                ParamByName('@IndiceVehiculo').Value := IndiceVehiculo;
            end;

            Open;
            while not(Eof) do begin
                New(ABajaForzada);
                with ABajaForzada^ do begin
                    CodigoBajaForzada	:= FieldByName('CodigoBajaForzada').AsInteger;
                    Deshacer			:= FieldByName('Deshacer').AsBoolean;
                    Patente				:= FieldByName('Patente').AsString;    
                    ConvenioNativo		:= FieldByName('ConvenioNativo').AsString;
                    TagNativo			:= FieldByName('TagNativo').AsString;
                    RUTNativo			:= FieldByName('RUTNativo').AsString;
                    ConvenioForaneo		:= FieldByName('ConvenioForaneo').AsString;
                    TagForaneo			:= FieldByName('TagForaneo').AsString;
                    RUTForaneo			:= FieldByName('RUTForaneo').AsString;
                    NombreForaneo		:= FieldByName('NombreForaneo').AsString;
                    FechaHora			:= FieldByName('FechaHora').AsDateTime;
                    CodigoUsuario		:= FieldByName('CodigoUsuario').AsString;
                end;
                FBajasForzadas.Add(ABajaForzada);
                Next;
            end;
        except
            on E : Exception do begin
              MsgBoxErr(MSG_ERROR_SELECT, E.Message, 'Error', MB_ICONERROR);
            end;
        end;
    finally
        Close;
        Free;
    end;
end;

constructor TBajasForzadasConvenio.Create(CodigoConvenio: Integer);
begin
    FCodigoConvenio := CodigoConvenio;
    FBajasForzadas := TList.Create;
end;

destructor TBajasForzadasConvenio.Destroy;
var
	I : Integer;
	ABajaForzada : PBajaForzada;
begin
	for I := 0 to FBajasForzadas.Count - 1 do
    begin
        ABajaForzada := FBajasForzadas.Items[I];
		Dispose(ABajaForzada);
	end;

	FBajasForzadas.Clear;
	if Assigned(FBajasForzadas) then
        FBajasForzadas.Free;

	inherited;
end;

procedure TBajasForzadasConvenio.MarcarBajaForzadaDeshacer(CodigoBajaForzada: Integer; Deshacer: Boolean);
var
    ABajaForzada: TBajaForzada;
    Indice: Integer;
begin
    Indice := BuscarIndice(CodigoBajaForzada);
    TBajaForzada((FBajasForzadas.Items[Indice])^).Deshacer := Deshacer;
end;

procedure TBajasForzadasConvenio.Guardar;
resourcestring
    MSG_SAVE_ERROR = 'Error guardando alta forzada';
var
    I: Integer;
begin
    for I := 0 to FBajasForzadas.Count - 1 do begin
        if not(TBajaForzada((FBajasForzadas.Items[I])^).Deshacer) then Continue;

        with TADOStoredProc.Create(nil) do
        try
            try
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'GuardarAltaForzada';

                Parameters.CreateParameter('@CodigoBajaForzadaDeshacer', ftInteger, pdInput, 0, 0);
                Parameters.CreateParameter('@CodigoUsuario', ftString, pdInput, 20, '');
                Parameters.CreateParameter('@ErrorDescription', ftString, pdInputOutput, 4000, '');
                
                Parameters.ParamByName('@CodigoBajaForzadaDeshacer').Value := TBajaForzada((FBajasForzadas.Items[I])^).CodigoBajaForzada;
                Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;

                ExecProc;

				//if Parameters.ParamByName('@ErrorDescription').Value <> EmptyStr then	//TASK_106_JMA_20170206
                if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then           	//TASK_106_JMA_20170206
                    raise Exception.Create(Parameters.ParamByName('@ErrorDescription').Value);
            except
               on E: Exception do begin
                  if E.ClassType = Exception then
                    raise EConvenioError.create(MSG_SAVE_ERROR, e.message , 0)
                  else raise;
               end;
            end;
        finally
            Close;
            Free;
        end;
    end;
end;
// FIN : TASK_070_MGO_20160907


{ TMediosEnvioDocumentosConvenio }

{Procedure Name	: TMediosEnvioDocumentosConvenio.AgregarMedioEnvioDocumentosConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const AMedioEnvio: TRegistroMedioEnvioDocumentosCobro
 Return Value   :
}
procedure TMediosEnvioDocumentosConvenio.AgregarMedioEnvioDocumentosConvenio(
  const AMedioEnvio: TRegistroMedioEnvioDocumentosCobro);
var
 MedioEnvio : PRegistroMedioEnvioDocumentosCobro;
begin
    New(MedioEnvio);
    MedioEnvio^ := AMedioEnvio;
    FMediosEnvio.Add(MedioEnvio);
end;

{Procedure Name	: TMediosEnvioDocumentosConvenio.BuscarIndice
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoMedioEnvioDocumento, CodigoTipoMedioEnvio: Integer
 Return Value   : integer
}
function TMediosEnvioDocumentosConvenio.BuscarIndice(
  CodigoMedioEnvioDocumento, CodigoTipoMedioEnvio: Integer): integer;
var
    i:Integer;
begin
    Result := -1;
    for i := 0  to CantidadMediosEnvio -1 do
        if (MediosEnvio[i].CodigoMedioEnvioDocumento = CodigoMedioEnvioDocumento) and
            (MediosEnvio[i].CodigoTipoMedioEnvio = CodigoTipoMedioEnvio) then Result := i;
end;

{Procedure Name	: TMediosEnvioDocumentosConvenio.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoConvenio: integer
 Return Value   :
}
constructor TMediosEnvioDocumentosConvenio.Create(CodigoConvenio: integer);
var
 AMedioEnvio : PRegistroMedioEnvioDocumentosCobro;
begin
    FCodigoConvenio := CodigoConvenio;

    FMediosEnvio := TList.Create;
    FMediosEnvioQuitados := TList.Create;

    with TADOStoredProc.Create(nil) do
    try

      Connection:=DMConnections.BaseCAC;
      ProcedureName:='ObtenerMedioEnvioConvenio';
      with Parameters do begin
          Refresh;
          ParamByName('@CodigoConvenio').Value:= CodigoConvenio;
      end;

      Open;
      while not(Eof) do begin
          New(AMedioEnvio);
          with AMedioEnvio^ do begin
              CodigoMedioEnvioDocumento:=FieldByName('CodigoMedioEnvioDocumento').AsInteger;
              CodigoTipoMedioEnvio:=FieldByName('CodigoTipoMedioEnvio').AsInteger;
          end;
          FMediosEnvio.Add(AMedioEnvio);
          next;
      end;
    finally
        close;
        free;
    end;
end;

{Procedure Name	: TMediosEnvioDocumentosConvenio.Destroy
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
destructor TMediosEnvioDocumentosConvenio.Destroy;
var  i : integer;
     AMedioEnvio : PRegistroMedioEnvioDocumentosCobro;
begin
  for i := 0 to FMediosEnvio.Count - 1 do begin
   AMedioEnvio := FMediosEnvio.Items[i];
   Dispose(AMedioEnvio);
  end;
  FMediosEnvio.Clear;
  if Assigned(FMediosEnvio) then FMediosEnvio.Free;

  for i := 0 to FMediosEnvioQuitados.Count - 1 do begin
   AMedioEnvio := FMediosEnvioQuitados.Items[i];
   Dispose(AMedioEnvio);
  end;
  FMediosEnvioQuitados.Clear;
  if Assigned(FMediosEnvioQuitados) then FMediosEnvioQuitados.Free;

  inherited;
end;




{ TDocumentacionPresentada }


{Procedure Name	: TMediosEnvioDocumentosConvenio.GetEnviaDocumentacionxMail
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TMediosEnvioDocumentosConvenio.GetEnviaDocumentacionxMail: Boolean;
var
    i : Integer;
begin
    Result := False;
    for i := 0 to CantidadMediosEnvio - 1  do Result := Result or ((MediosEnvio[i].CodigoTipoMedioEnvio = TipoEnvioMail) and (MediosEnvio[i].CodigoMedioEnvioDocumento = CONST_DOCUMENTO_COBRO_ELECTRONICO));
end;

{Procedure Name	: TMediosEnvioDocumentosConvenio.GetTipoEnvoMail
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TMediosEnvioDocumentosConvenio.GetTipoEnvoMail: Integer;
begin
    result := QueryGetValueInt(DMConnections.BaseCAC,'select dbo.CONST_MEDIO_ENVIO_DOC_MAIL() ');
end;

{Procedure Name	: TMediosEnvioDocumentosConvenio.Guardar
 Author 		: Nelson Droguett Sierra (nefernandez)
 Date Created	: 20-Abril-2010
 Description	:  SS 680: Se pasa el par�metro UsuarioSistema para registrar los datos de auditor�a.
 Parameters   	:
 Return Value   :
}
procedure TMediosEnvioDocumentosConvenio.Guardar;
resourcestring
    MSG_SAVE_ERROR = 'Error al guardar el medio de envio de documentaci�n.';
var i : integer;
    AMedioEnvio : PRegistroMedioEnvioDocumentosCobro;
begin
    with TADOStoredProc.Create(nil) do
    try
        try
            Connection:=DMConnections.BaseCAC;
            ProcedureName := 'EliminarConvenioMediosEnvioDocumentos';
            with Parameters do begin
                Refresh;
                ParamByName('@CodigoConvenio').Value:=CodigoConvenio;
                ParamByName('@Usuario').Value := UsuarioSistema; //Revision 1
                for i:=0 to FMediosEnvioQuitados.Count - 1 do begin
                    AMedioEnvio := FMediosEnvioQuitados.Items[i];
                    ParamByName('@CodigoMedioEnvioDocumento').Value := AMedioEnvio^.CodigoMedioEnvioDocumento;
                    ParamByName('@CodigoTipoMedioEnvio').Value      := AMedioEnvio^.CodigoTipoMedioEnvio;
                    ExecProc;
{INICIO: TASK_106_JMA_20170206}
                if ParamByName('@RETURN_VALUE').Value<>0 then
                    raise Exception.Create(ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}
                    close;
                end;
            end;
            ProcedureName := 'ActualizarConvenioMediosEnvioDocumentos';
            with Parameters do begin
                Refresh;
                ParamByName('@CodigoConvenio').Value:=CodigoConvenio;
                ParamByName('@Usuario').Value := UsuarioSistema; //Revision 1
                for i:=0 to FMediosEnvio.Count - 1 do begin
                    AMedioEnvio := FMediosEnvio.Items[i];
                    ParamByName('@CodigoMedioEnvioDocumento').Value := AMedioEnvio^.CodigoMedioEnvioDocumento;
                    ParamByName('@CodigoTipoMedioEnvio').Value      := AMedioEnvio^.CodigoTipoMedioEnvio;
                    ExecProc;
{INICIO: TASK_106_JMA_20170206}
                if ParamByName('@RETURN_VALUE').Value<>0 then
                    raise Exception.Create(ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}
                    close;
                end;
            end;
        except
           on E: Exception do begin
              if E.ClassType = Exception then
                raise EConvenioError.create(MSG_SAVE_ERROR, e.message , 0)
              else raise;
           end;
        end;
    finally
        close;
        free;
    end;
end;

{Procedure Name	: TMediosEnvioDocumentosConvenio.ObtenerCantidadMediosEnvio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TMediosEnvioDocumentosConvenio.ObtenerCantidadMediosEnvio: integer;
begin
    result := FMediosEnvio.Count;
end;

{Procedure Name	: TMediosEnvioDocumentosConvenio.ObtenerMediosEnvioDocumentoConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   : TRegistroMedioEnvioDocumentosCobro
}
function TMediosEnvioDocumentosConvenio.ObtenerMediosEnvioDocumentoConvenio(
  Index: integer): TRegistroMedioEnvioDocumentosCobro;
begin
    result := TRegistroMedioEnvioDocumentosCobro((FMediosEnvio.items[Index])^);
end;


{Procedure Name	: TMediosEnvioDocumentosConvenio.QuitarMedioEnvioDocumentosConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   :
}
procedure TMediosEnvioDocumentosConvenio.QuitarMedioEnvioDocumentosConvenio(
  Index: integer);
begin
    FMediosEnvioQuitados.Add(FMediosEnvio.Items[Index]);
    FMediosEnvio.Delete(Index);
end;

{ TConvenioFactory }
{Procedure Name	: TConvenioFactory.ConveniosPersonaComoMandante
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: Obtiene una lista de convenios donde la persona es mandante del medio de pago.
 Parameters   	: var AConvenios: TListaConveniosPersona; TipoDocumento, NumeroDocumento: string
 Return Value   :
}
class procedure TConvenioFactory.ConveniosPersonaComoMandante(
  var AConvenios: TListaConveniosPersona; TipoDocumento,
  NumeroDocumento: string;
  PuntoVenta, PuntoEntrega: Integer);
begin
    with TADOStoredProc.Create(nil) do
    try
        Connection:=DMConnections.BaseCAC;
        ProcedureName:='ObtenerConveniosMandante';
        with Parameters do begin
            Refresh;
            ParamByName('@CodigoDocumento').Value:=TipoDocumento;
            ParamByName('@NumeroDocumento').Value:=NumeroDocumento;
        end;
        Open;
        while Not eof do begin
            SetLength(AConvenios, length(AConvenios) + 1);
            AConvenios[high(AConvenios)] := TDatosConvenio.Create(FieldByName('NumeroConvenio').Value, PuntoVenta, PuntoEntrega);
            next;
        end;
    finally
        close;
        free;
    end;

end;

{Procedure Name	: TConvenioFactory.ConveniosPersonas
 Author 		: Nelson Droguett Sierra (cchiappero)
 Date Created	: 20-Abril-2010
 Description	: Obtiene una lista de convenios donde la persona es titular del convenio
 Parameters   	: var AConvenios : TListaConveniosPersona; TipoDocumento,  NumeroDocumento: string; PuntoVenta, PuntoEntrega: Integer
 Return Value   :
}
class procedure TConvenioFactory.ConveniosPersonas(var AConvenios : TListaConveniosPersona; TipoDocumento,
  NumeroDocumento: string; PuntoVenta, PuntoEntrega: Integer);
begin
    with TADOStoredProc.Create(nil) do
    try
        Connection:=DMConnections.BaseCAC;
        ProcedureName:='ObtenerConvenios';
        with Parameters do begin
            Refresh;
            ParamByName('@CodigoDocumento').Value:=TipoDocumento;
            ParamByName('@NumeroDocumento').Value:=NumeroDocumento;
            ParamByName('@SoloActivos').Value := False;
        end;
        Open;
        while Not eof do begin
            SetLength(AConvenios, length(AConvenios) + 1);
            AConvenios[high(AConvenios)] := TDatosConvenio.Create(FieldByName('NumeroConvenio').Value, PuntoVenta, PuntoEntrega);
            next;
        end;
    finally
        close;
        free;
    end;
end;

{Procedure Name	: TMediosPagoConvenio.SetMedioPago
 Author 		: Nelson Droguett Sierra (nefernandez)
 Date Created	: 20-Abril-2010
 Description	: Se corrige la condici�n para determinar si hubo cambio en los
    datos del medio de pago.
 Parameters   	: const Value: TDatosMediosPago
 Return Value   :
}
procedure TMediosPagoConvenio.SetMedioPago(const Value: TDatosMediosPago);
resourcestring
    MSG_ERROR = 'Medio de Pago erroneo.';
begin
  case Value.TipoMedioPago of
    PAT: begin
        FCodigoMedioPago := TPA_PAT;
        FTipoMedioPago := PAT;
    end;
    PAC: begin
        FCodigoMedioPago := TPA_PAC;
        FTipoMedioPago := PAC;
    end;
    NINGUNO: begin
        FCodigoMedioPago := TPA_NINGUNO;
        FTipoMedioPago := NINGUNO;
    end;
  else
    raise EMedioComunicacionError.Create(MSG_ERROR,'', ERRORMEDIOPAGOERRONEO);
    Exit;
  end;

  //Verifico si cambio algun dato del medio de pago
  // Se diferencia el Tipo Medio Pago para comparar solo los datos que le corresponden
  if ( FMedioPago.TipoMedioPago <> Value.TipoMedioPago )  // Tipo Medio Pago
      or
     ( ( FMedioPago.TipoMedioPago = PAC ) and // Si es PAC se verifica si hay algun cambio en sus datos
       ( (FMedioPago.PAC.TipoCuenta <> Value.PAC.TipoCuenta) or
         (FMedioPago.PAC.CodigoBanco <> Value.PAC.CodigoBanco) or
         (FMedioPago.PAC.NroCuentaBancaria <> Value.PAC.NroCuentaBancaria) or
         (FMedioPago.PAC.Sucursal <> Value.PAC.Sucursal) or
         (FMedioPago.PAC.NumeroCheque <> Value.PAC.NumeroCheque) )
     ) or
     ( ( FMedioPago.TipoMedioPago = PAT ) and // Si es PAT se verifica si hay algun cambio en sus datos
       ( (FMedioPago.PAT.TipoTarjeta <> Value.PAT.TipoTarjeta) or
         (FMedioPago.PAT.EmisorTarjetaCredito <> Value.PAT.EmisorTarjetaCredito) or
         (FMedioPago.PAT.NroTarjeta <> Value.PAT.NroTarjeta) or
         (FMedioPago.PAT.FechaVencimiento <> Value.PAT.FechaVencimiento) or
         (FMedioPago.PAT.Cuotas <> Value.PAT.Cuotas) )
     ) then
                FCambioMedioPago := True;

  if (FMedioPago.TipoMedioPago <> Value.TipoMedioPago) or
     (
        (FMedioPago.TipoMedioPago = PAT) and
        (FMedioPago.PAT.TipoTarjeta <> Value.PAT.TipoTarjeta)
     ) or
     (
        (FMedioPago.TipoMedioPago = PAC) and
        (FMedioPago.PAC.NroCuentaBancaria <> Value.PAC.NroCuentaBancaria)
     ) or
     // Se agrega la comparaci�n para evaluar si hay un cambio en
     // los datos del medio de pago PAT y Tarjeta CMR
     // Nota: Como ya se usaba el atributo FCambioDatosTransBank para ver si se
     // tiene que modificar el campo 'FechaActualizacionMedioPago', se sigue
     // usando esta variable tambien para ver si cambio los datos del mandato CMR
     (
        (FMedioPago.TipoMedioPago = PAT) and
        (FMedioPago.PAT.TipoTarjeta = TARJETA_CMR ) and
        (FCambioMedioPago)
     )   then begin
        FCambioDatosTransBank := True;
        FMedioPago.PAT.Confirmado := False;
        FMedioPago.PAC.Confirmado := False;
  end;

  FMedioPago := Value;
  // Como aqui se sobreescribe FMedioPago con el "Value" recibido
  // se pierde la asignaci�n del atributo "Confirmado" si es que se ingres� en
  // el "if" de arriba. Entonces si ingreso a dicho "if" volvemos a restaurar
  // los valores asignados
  if FCambioDatosTransBank then begin
        FMedioPago.PAT.Confirmado := False;
        FMedioPago.PAC.Confirmado := False;
  end;

  if Assigned(OnCambioMedioPago) then OnCambioMedioPago(self);
end;

{Procedure Name	: TMediosPagoConvenio.SetOnCambioMedioPago
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: TNotifyEvent
 Return Value   :
}
procedure TMediosPagoConvenio.SetOnCambioMedioPago(
  const Value: TNotifyEvent);
begin
  FOnCambioMedioPago := Value;
end;


{Procedure Name	: TDatosPersona.SetCodigoPersona
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: integer
 Return Value   :
}
procedure TDatosPersona.SetCodigoPersona(const Value: integer);
begin
  FCodigoPersona := Value;
end;

{Procedure Name	: TDatosPersona.GetEsModificado
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDatosPersona.GetEsModificado: Boolean;
begin
    Result := FEsModificado;
end;

{Procedure Name	: TDatosPersona.GetEsNuevo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDatosPersona.GetEsNuevo: Boolean;
begin
    Result := FEsNuevo;
end;
{******************************** Function Header ******************************
Procedure Name	: TDatosPersona.SetDatos
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: TDatosPersonales
 Return Value   :
 
Revision : 1
    Author : mpiazza
    Date : 16/10/2009
    Description : ss-787 se genera nueva validacion por campo nuevo RecibeClavePorMail

******************************************************************************}
procedure TDatosPersona.SetDatos(const Value: TDatosPersonales);
resourcestring
    MSG_RUT_ERROR = 'Ya existe una persona con ese RUT.';
var
 DatosPersonaValidacion : TDatosPersonales;
begin
    //si la persona es nueva  verifico que no exista el rut.
    if (Datos.CodigoPersona = -1) and ObtenerDatosPersona(Datos.TipoDocumento, Datos.NumeroDocumento, DatosPersonaValidacion) then
          raise EPersonaError.Create(MSG_RUT_ERROR,'', ERRORRUTEXISTE);
    if (Trim(Datos.RazonSocial) <> Trim(Value.RazonSocial)) or
       (Trim(Datos.Personeria) <> Trim(Value.Personeria)) or
       (Trim(Datos.TipoDocumento) <> Trim(Value.TipoDocumento)) or
       (Trim(Datos.NumeroDocumento) <> Trim(Value.NumeroDocumento)) or
       (Trim(Datos.Apellido) <> Trim(Value.Apellido)) or
       (Trim(Datos.ApellidoMaterno) <> Trim(Value.ApellidoMaterno)) or
       (Trim(Datos.Nombre) <> Trim(Value.Nombre)) or
       (Trim(Datos.Sexo) <> Trim(Value.Sexo)) or
       (Trim(Datos.LugarNacimiento) <> Trim(Value.LugarNacimiento)) or
       (Datos.FechaNacimiento <> Value.FechaNacimiento) or
       (Datos.Giro <> Value.Giro) or
       (Datos.TipoCliente <> Value.TipoCliente)
        or
       (Datos.RecibeClavePorMail <> Value.RecibeClavePorMail) or
       (Datos.InactivaDomicilioRNUT <> Value.InactivaDomicilioRNUT) or
       (Datos.InactivaMedioComunicacionRNUT <> Value.InactivaMedioComunicacionRNUT) or
       (Datos.Semaforo1 <> Value.Semaforo1) or
       (Datos.Semaforo3 <> Value.Semaforo3)
        then
        FEsModificado := True;
    if (Trim(Datos.RazonSocial) <> Trim(Value.RazonSocial)) or
       (Trim(Datos.Personeria) <> Trim(Value.Personeria)) or
       (Trim(Datos.TipoDocumento) <> Trim(Value.TipoDocumento)) or
       (Trim(Datos.NumeroDocumento) <> Trim(Value.NumeroDocumento)) or
       (Trim(Datos.Apellido) <> Trim(Value.Apellido)) or
       (Trim(Datos.ApellidoMaterno) <> Trim(Value.ApellidoMaterno)) or
       (Trim(Datos.Nombre) <> Trim(Value.Nombre))  then
        FModificoDatosPrincipales := True;
    if value.CodigoPersona > 0 then FCodigoPersona := Value.CodigoPersona;
	FDatos := Value;
end;

{******************************** Function Header ******************************
Function Name: SetRecibeClavePorMail
Author       : mpiazza
Date Created : 20/04/2010
Description  : Ref: ss-787: se genera el procedimiento para poder asignar el valor a RecibeClavePorMail
Parameters   : const Value: boolean
Return Value : None
*******************************************************************************}
procedure TDatosPersona.SetRecibeClavePorMail(const Value: boolean);
begin
  FDatos.RecibeClavePorMail := Value;
  FEsModificado := True;
end;

{******************************** Function Header ******************************
Function Name: SetTieneNotificacionesTAGs
Author       : alabra
Date Created : 13-07-2011
Parameters   : const Value: boolean
Return Value : None
*******************************************************************************}
procedure TDatosPersona.SetTieneNotificacionesTAGs(const Value: Boolean);
begin
    FDatos.TieneNotificacionesTAGs := Value;
end;

{******************************** Function Header ******************************
Function Name: SetInicializarNotificacionesTAGs
Author       : alabra
Date Created : 13-07-2011
Parameters   : const Value: boolean
Return Value : None
*******************************************************************************}
procedure TDatosPersona.SetInicializarNotificacionesTAGs(const Value: Boolean);
begin
    FDatos.InicializarNotificacionesTAGs := Value;
end;

{Procedure Name	: TConvenioFactory.ConveniosPersonasComoRepresentanteLegal
 Author 		: Nelson Droguett Sierra (chiappero)
 Date Created	: 20-Abril-2010
 Description	: Obtiene una lista dce convenios donde la persona es representante legal del convenio
 Parameters   	: var AConvenios: TListaConveniosPersona; TipoDocumento, NumeroDocumento: string
 Return Value   :
}
class procedure TConvenioFactory.ConveniosPersonasComoRepresentanteLegal(
  var AConvenios: TListaConveniosPersona; TipoDocumento,
  NumeroDocumento: string;
  PuntoVenta, PuntoEntrega: Integer);
begin
    with TADOStoredProc.Create(nil) do
    try
        Connection:=DMConnections.BaseCAC;
        ProcedureName:='ObtenerConveniosRepresentante';
        with Parameters do begin
            Refresh;
            ParamByName('@CodigoDocumento').Value:=TipoDocumento;
            ParamByName('@NumeroDocumento').Value:=NumeroDocumento;
        end;
        Open;
        while Not eof do begin
            SetLength(AConvenios, length(AConvenios) + 1);
            AConvenios[high(AConvenios)] := TDatosConvenio.Create(FieldByName('NumeroConvenio').Value, PuntoVenta, PuntoEntrega);
            next;
        end;
    finally
        close;
        free;
    end;

end;

{ TPersonaConvenio }
{Procedure Name	: TPersonaConvenio.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoPersona: integer
 Return Value   :
}
constructor TPersonaConvenio.Create(CodigoPersona: integer);
begin
    inherited;
    //Carga los Domicilios instancia los domicilios
    FDomicilios := TDomiciliosPersona.Create(FCodigoPersona);
    FTelefonos := TMediosComunicacionPersona.create(FCodigoPersona, TMC_TELEFONO);
    FEmail     := TMediosComunicacionPersona.create(FCodigoPersona, TMC_EMAIL);
end;

{Procedure Name	: TPersonaConvenio.Destroy
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
destructor TPersonaConvenio.Destroy;
begin
    if Assigned(Domicilios) then Domicilios.Free;
    if Assigned(EMail) then EMail.Free;
    if Assigned(Telefonos) then Telefonos.Free;
    inherited;
end;

{Procedure Name	: TPersonaConvenio.ExisteMailABorrar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TPersonaConvenio.ExisteMailABorrar: Boolean;
begin
    Result := False;
    if Assigned(EMail) then
        Result := EMail.ExisteMailABorrar;
end;


{Procedure Name	: TPersonaConvenio.Guardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TPersonaConvenio.Guardar;
begin
    inherited;
    //Guardo el cliente
    with TADOStoredProc.Create(nil) do
    try
        Connection:=DMConnections.BaseCAC;
        ProcedureName:='ActualizarDatosCliente';
        with Parameters do begin
                Refresh;
                ParamByName('@CodigoCliente').Value:=FCodigoPersona;
        end;
        ExecProc;
{INICIO: TASK_106_JMA_20170206}
        if Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
            raise Exception.Create(Parameters.ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}
    finally
        close;
        free;
    end;
    FDomicilios.Guardar;
    FTelefonos.Guardar;
    FEMail.Guardar;
end;


{ TPersonaJuridica }
{Procedure Name	: TPersonaJuridica.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoPersona, CodigoConvenio: integer; EsConvenioCN: Boolean
 Return Value   :
}
//constructor TPersonaJuridica.Create(CodigoPersona, CodigoConvenio: integer; EsConvenioCN: Boolean);		//SS_1147_MCA_20140408
constructor TPersonaJuridica.Create(CodigoPersona, CodigoConvenio: integer; EsConvenioNativo: Boolean);		//SS_1147_MCA_20140408
begin
  inherited create(CodigoPersona);
  //FEsConvenioCN := EsConvenioCN;												//SS_1147_MCA_20140408
  FEsConvenioNativo := EsConvenioNativo;										//SS_1147_MCA_20140408
  FRepresentantesLegales := TRepresentantesLegales.create(CodigoConvenio, CodigoPersona);
end;

{Procedure Name	: TPersonaJuridica.Destroy
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
destructor TPersonaJuridica.Destroy;
begin
  if Assigned(FRepresentantesLegales) then FRepresentantesLegales.free;
  inherited;
end;

{Procedure Name	: TPersonaJuridica.Guardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TPersonaJuridica.Guardar;
begin
  inherited;
  //if FEsConvenioCN then FRepresentantesLegales.Guardar;						//SS_1147_MCA_20140408
  if FEsConvenioNativo then FRepresentantesLegales.Guardar;						//SS_1147_MCA_20140408
end;

{ TMandante }

{Procedure Name	: TMandante.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoPersona: integer
 Return Value   :
}
constructor TMandante.Create(CodigoPersona: integer);
begin
  FCodigoPersona := CodigoPersona;
  inherited;
  FTelefonos := TMediosComunicacionPersona.create(FCodigoPersona, TMC_TELEFONO);
end;

{Procedure Name	: TMandante.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
constructor TMandante.Create;
begin
  FTelefonos := TMediosComunicacionPersona.create(TMC_TELEFONO);
end;

{Procedure Name	: TMandante.Destroy
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
destructor TMandante.Destroy;
begin
  if Assigned(Telefonos) then Telefonos.Free;
  inherited;
end;

{Procedure Name	: TMandante.Guardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TMandante.Guardar;
begin
  inherited;
  FTelefonos.CodigoPersona := Self.CodigoPersona;
  FTelefonos.Guardar;
end;

{Procedure Name	: TMandante.SetCodigoPersona
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: integer
 Return Value   :
}
procedure TMandante.SetCodigoPersona(const Value: integer);
begin
    FCodigoPersona := Value;
    if Assigned(FTelefonos) then FTelefonos.Free;
    FTelefonos := TMediosComunicacionPersona.create(FCodigoPersona, TMC_TELEFONO);
end;

{Procedure Name	: TDatosPersona.ObtenerDatosPersona
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoDocumento, NumeroDocumento: string; var DatosPersonas: TDatosPersonales
 Return Value   : boolean
}
class function TDatosPersona.ObtenerDatosPersona(CodigoDocumento,
  NumeroDocumento: string; var DatosPersonas: TDatosPersonales): boolean;
begin
    with TADOStoredProc.Create(nil) do
    try
        Connection:=DMConnections.BaseCAC;
        ProcedureName:='ObtenerConveniosRepresentante';
        with Parameters do begin
            Refresh;
            ParamByName('@CodigoDocumento').Value := CodigoDocumento;
            Parameters.ParamByName('@NumeroDocumento').Value := NumeroDocumento;
        end;
        Open;
       if RecordCount > 0 then begin
           DatosPersonas.CodigoPersona := FieldByName('CodigoPersona').AsInteger;
           if FieldByName('CodigoPersona').Value = PERSONERIA_JURIDICA then
            DatosPersonas.Personeria := PERSONERIA_JURIDICA
           else DatosPersonas.Personeria := PERSONERIA_FISICA;
           DatosPersonas.Nombre := Trim(FieldByName('Nombre').AsString);
           DatosPersonas.Apellido := Trim(FieldByName('Apellido').AsString);
           DatosPersonas.ApellidoMaterno := Trim(FieldByName('ApellidoMaterno').AsString);
           DatosPersonas.FechaNacimiento := FieldByName('FechaNacimiento').AsDateTime;
           DatosPersonas.Sexo := (Trim(FieldByName('Sexo').AsString)+SEXO_MASCULINO)[1];
           DatosPersonas.LugarNacimiento := Trim(FieldByName('LugarNacimiento').AsString);
           DatosPersonas.CodigoActividad := FieldByName('CodigoActividad').AsInteger;
           DatosPersonas.Password := Trim(FieldByName('Password').AsString);
           DatosPersonas.Pregunta := Trim(FieldByName('Pregunta').AsString);
           DatosPersonas.Respuesta := Trim(FieldByName('Respuesta').AsString);
           DatosPersonas.TipoDocumento := Trim(FieldByName('CodigoDocumento').AsString);
           result := true;
       end else result := false;
    finally
        Close;
        free;
    end;
end;

{Procedure Name	: TDatosConvenio.ImprimirBajaComvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var MotivoCancelacion:TMotivoCancelacion; CantidadCopias : integer; FormImprimir : TObject
 Return Value   : Boolean
}
function TDatosConvenio.ImprimirBajaComvenio(var MotivoCancelacion:TMotivoCancelacion; CantidadCopias : integer; FormImprimir : TObject): Boolean;
begin
    Result := Imprimir(IMPRIMIR_BAJA_CONVENIO,MotivoCancelacion,CantidadCopias, FormImprimir, nil);
end;

{Procedure Name	: TDatosConvenio.ImprimirCaratula
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var MotivoCancelacion: TMotivoCancelacion;
                  CantCopias: Integer;
                  Operador : String;
                  PuntoEntrega : String;
                  FormImprimir : TObject
 Return Value   : Boolean
}
function TDatosConvenio.ImprimirCaratula(var MotivoCancelacion: TMotivoCancelacion;
    CantCopias: Integer;
    Operador : String;
    PuntoEntrega : String;
    FormImprimir : TObject): Boolean;
var
    Documentacion : RegDocumentacionPresentada;
begin
    DocumentacionConvenio.ObtenerListaDocumentacion(Documentacion);
    result := TFormImprimirConvenio(FormImprimir).ImprimirCaratula(Cliente.Datos.Personeria, NumeroConvenioFormateado, cliente.Datos, Documentacion, MotivoCancelacion, CantCopias, Operador, PuntoEntrega);
    FNroAnexoEmitido := TFormImprimirConvenio(FormImprimir).NroAnexoEmitido;
end;

{Procedure Name	: TDatosConvenio.ImprimirCaratulaGuardada
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var MotivoCancelacion : TMotivoCancelacion; CantCopias:Integer; Operador : String;
            PuntoEntrega : String; FormImprimir : TObject
 Return Value   : Boolean
}
function TDatosConvenio.ImprimirCaratulaGuardada(var MotivoCancelacion : TMotivoCancelacion; CantCopias:Integer; Operador : String;
            PuntoEntrega : String; FormImprimir : TObject): Boolean;
resourcestring
    MSG_CARAT_PRINT = 'Impresion Cartula';
    MSG_IS_NOT_DOC = 'No hay Documentacion para imprimir la caratula';
var
    RegDocumentacion : RegDocumentacionPresentada;
begin
    Result := False;
    if DocumentacionConvenio.ObtenerListaDocumentacionGuardada(RegDocumentacion) then begin
        result := TFormImprimirConvenio(FormImprimir).ImprimirCaratula(Cliente.Datos.Personeria, NumeroConvenioFormateado, cliente.Datos, RegDocumentacion, MotivoCancelacion, CantCopias, Operador, PuntoEntrega);
        FNroAnexoEmitido := TFormImprimirConvenio(FormImprimir).NroAnexoEmitido;
    end
    else
        MsgBox(MSG_CARAT_PRINT,MSG_IS_NOT_DOC,MB_ICONINFORMATION);

end;

{Procedure Name	: TDatosConvenio.ImprimirEtiquetas
 Author 		: Nelson Droguett Sierra (dcalani)
 Date Created	: 20-Abril-2010
 Description	: Imprime las etiqueta de todos los vehiculos, salvo cuando se especifique el indice
               del vehiculo que se quiere imprimir la etiqueta
 Parameters   	: var MotivoCancelacion:TMotivoCancelacion; IndiceVehiculo: Integer = -1
 Return Value   : Boolean
}
function TDatosConvenio.ImprimirEtiquetas(var MotivoCancelacion:TMotivoCancelacion;  FormImprimir : TObject; IndiceVehiculo: Integer = -1) : Boolean;
resourcestring
    MSG_INVALID_INDEX = 'Error, indice de vehiculo fuera de rango';
var
    i: integer;
    Vehiculo: array of TCuentaABM;
begin
    if IndiceVehiculo < 0 then begin
        SetLength(Vehiculo, Cuentas.CantidadVehiculos);
        for i:= 0 to Cuentas.CantidadVehiculos-1 do begin
            Vehiculo[i].Cuenta := Cuentas.Vehiculos[i].Cuenta;
        end;
    end else begin
        if IndiceVehiculo > Cuentas.CantidadVehiculos then begin
            raise Exception.Create(MSG_INVALID_INDEX);
            exit;
        end;
        SetLength(Vehiculo, 1);
        Vehiculo[0].Cuenta := Cuentas.Vehiculos[IndiceVehiculo].Cuenta;
    end;

    result := TFormImprimirConvenio(FormImprimir).ImprimirEtiquetas(NumeroConvenioFormateado, cliente.Datos, Vehiculo, MotivoCancelacion);
    FNroAnexoEmitido := TFormImprimirConvenio(FormImprimir).NroAnexoEmitido;
end;

{Procedure Name	: TDatosConvenio.SetAccionRNUT
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Integer
 Return Value   :
}
procedure TDatosConvenio.SetAccionRNUT(const Value: Integer);
begin
  FAccionRNUT := Value;
end;

{Procedure Name	: TDatosConvenio.setEstadoConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: ACodigoEstadoConvenio: integer
 Return Value   :
}
procedure TDatosConvenio.setEstadoConvenio(ACodigoEstadoConvenio: integer);
begin
    FCodigoEstadoConvenio := ACodigoEstadoConvenio;
end;

{ TDocumentacionConvenio }

{Procedure Name	: TDocumentacionConvenio.AgregarDocumentacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoConvenio,
  CodigoDocumentacionRespaldo: Integer; NoCorresponde: Boolean; Orden: integer;
  SoloImpresion: Integer;  Descripcion: String; ExplicacionDefault: String = '';  TipoPatente: String = '';
  Patente: String = ''; CodigoDocumentacionPresentada: Integer = -1; FechaBaja: TDatetime = Nulldate
 Return Value   :
}
procedure TDocumentacionConvenio.AgregarDocumentacion(CodigoConvenio,
  CodigoDocumentacionRespaldo: Integer; NoCorresponde: Boolean; Orden: integer;
  SoloImpresion: Integer;  Descripcion: String; ExplicacionDefault: String = '';  TipoPatente: String = '';
  Patente: String = ''; CodigoDocumentacionPresentada: Integer = -1; FechaBaja: TDatetime = Nulldate);
begin
    SetLength(FDocumentacion, length(FDocumentacion) + 1);
    FDocumentacion[high(FDocumentacion)] := TDocumentacion.Create(CodigoConvenio, CodigoDocumentacionRespaldo, NoCorresponde,
                                    Orden, SoloImpresion, Descripcion, ExplicacionDefault, TipoPatente, Patente, CodigoDocumentacionPresentada, FechaBaja);
end;

{Procedure Name	: TDocumentacionConvenio.AgregarDocumentacionxOperacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:  CodigoDocumentacionRespaldo: Integer; NoCorresponde: Boolean; Orden,
  SoloImpresion: Integer; Obligatorio: Boolean; CodigoTipoOperacionConvenio: Integer;
  Descripcion: String; Explicacion, InformacionExtra, ExplicacionDefault, TipoPatente, Patente: String;
  ForzarNoVisible: Boolean
 Return Value   :
}
procedure TDocumentacionConvenio.AgregarDocumentacionxOperacion(
  CodigoDocumentacionRespaldo: Integer; NoCorresponde: Boolean; Orden,
  SoloImpresion: Integer; Obligatorio: Boolean; CodigoTipoOperacionConvenio: Integer;
  Descripcion: String; Explicacion, InformacionExtra, ExplicacionDefault, TipoPatente, Patente: String;
  ForzarNoVisible: Boolean);
begin
    if ForzarNoVisible then SoloImpresion := 1;
    SetLength(FDocumentacion, length(FDocumentacion) + 1);
    FDocumentacion[high(FDocumentacion)] := TDocumentacion.Create(
                                  FCodigoConvenio,
                                  CodigoDocumentacionRespaldo,
                                  NoCorresponde,
                                  Orden,
                                  SoloImpresion,
                                  Descripcion,
                                  Explicacion,
                                  Obligatorio,
                                  CodigoTipoOperacionConvenio,
                                  InformacionExtra,
                                  ExplicacionDefault,
                                  TipoPatente,
                                  Patente,
                                  -1);

end;

{Procedure Name	: TDocumentacionConvenio.AgregarDocumentacionxTipoOperacion
 Author 		: Nelson Droguett Sierra (dcalani)
 Date Created	: 20-Abril-2010
 Description	: Agrega la doc, deacuerdo al tipo de operacion, el ult parametro
            se usa para el caso particular de los rep legales, los cuales al cambiar
            de posicion (de tercero a segundo), se debe acutalizar la doc, pero no
            se debe informar al operador
 Parameters   	: CodigoTipoOperacion: Integer; InformacionExtra, TipoPatente, Patente: String;
  ForzarNoVisible: Boolean; CodigoDocumentacionPresentada: Integer
 Return Value   :
}
procedure TDocumentacionConvenio.AgregarDocumentacionxTipoOperacion(
  CodigoTipoOperacion: Integer; InformacionExtra, TipoPatente, Patente: String;
  ForzarNoVisible: Boolean; CodigoDocumentacionPresentada: Integer);
begin
    with TADOStoredProc.Create(nil) do begin
        try
            Connection := DMConnections.BaseCAC;
            ProcedureName := 'ObtenerDocumentacionxTipoOperacionConvenio';
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoTipoOperacionConvenio').Value := CodigoTipoOperacion;
            Parameters.ParamByName('@Personeria').Value := Null;
            Parameters.ParamByName('@Tipo').Value := Null;
            AgregarOperacionConvenio(CodigoTipoOperacion);
            Open;
            while not eof do begin
                if FieldByName('Cancela').AsInteger = 0 then begin
                    // Cargo la doc de la Operacion
                    if not EstaDocumentacion(FieldByName('CodigoDocumentacionRespaldo').AsInteger,
                                    InformacionExtra, TipoPatente, Patente) then
                        AgregarDocumentacionxOperacion(
                                      FieldByName('CodigoDocumentacionRespaldo').AsInteger,
                                      False,
                                      FieldByName('Orden').AsInteger,
                                      FieldByName('SoloImpresion').AsInteger,
                                      FieldByName('Obligatorio').AsBoolean,
                                      FieldByName('CodigoTipoOperacionConvenio').AsInteger,
                                      FieldByName('Descripcion').AsString,
                                      FieldByName('Explicacion').AsString,
                                      InformacionExtra,
                                      FieldByName('ExplicacionDefault').AsString,
                                      TipoPatente,
                                      Patente,
                                      ForzarNoVisible);
                end else begin
                    // Anulo la doc de la operacion inversa
                    CancelarDoc(FieldByName('CodigoDocumentacionRespaldo').AsInteger,InformacionExtra ,TipoPatente, Patente, CodigoDocumentacionPresentada);
                end;
                Next;
            end;
        finally
            close;
            free;
        end;
    end;
end;


{Procedure Name	: TDocumentacionConvenio.AgregarOperacionConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoTipoOperacionConvenio: integer
 Return Value   :
}
procedure TDocumentacionConvenio.AgregarOperacionConvenio(
  CodigoTipoOperacionConvenio: integer);
begin
    SetLength(FOperacionConvenio, Length(FOperacionConvenio)+1);
    FOperacionConvenio[Length(FOperacionConvenio)-1] := CodigoTipoOperacionConvenio;
end;

{Procedure Name	: TDocumentacionConvenio.CancelarDoc
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoDocumentacionRespaldo: Integer; InfoExtra, TipoPatente, Patente: String;
  CodigoDocumentacionPresentada: Integer
 Return Value   :
}
procedure TDocumentacionConvenio.CancelarDoc(
  CodigoDocumentacionRespaldo: Integer; InfoExtra, TipoPatente, Patente: String;
  CodigoDocumentacionPresentada: Integer);
var
    i: Integer;
begin
    for i := 0 to High(FDocumentacion) do
        if (FDocumentacion[i].CodigoDocumentacionRespaldo = CodigoDocumentacionRespaldo) and
        ( (Patente = '') or ((FDocumentacion[i].Patente = Patente) and (FDocumentacion[i].TipoPatente = TipoPatente)) ) and
           (FDocumentacion[i].FechaBaja = NullDate) and
           (
            (FDocumentacion[i].InformacionExtra = InfoExtra) or
            ((FDocumentacion[i].InformacionExtra = '') and (InfoExtra <> ''))
// fixme            (FDocumentacion[i].CodigoDocumentacionPresentada = CodigoDocumentacionPresentada)
            )then
                FDocumentacion[i].FechaBaja := NowBase(DMConnections.BaseCAC);
end;

{Procedure Name	: TDocumentacionConvenio.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoConvenio: integer
 Return Value   :
}
constructor TDocumentacionConvenio.Create(CodigoConvenio: integer);
var
    StoredProcedure: TADOStoredProc;
begin
    StoredProcedure := TADOStoredProc.Create(nil);
    FCodigoConvenio := CodigoConvenio;
    try
      StoredProcedure.Connection := DMConnections.BaseCAC;
      StoredProcedure.ProcedureName := 'ObtenerDocumentacionPresentadaConvenio';
      StoredProcedure.Parameters.Refresh;
      StoredProcedure.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
      StoredProcedure.Open;
      while not StoredProcedure.eof do begin
          AgregarDocumentacion( FCodigoConvenio,
                                StoredProcedure.FieldByName('CodigoDocumentacionRespaldo').AsInteger,
                                StoredProcedure.FieldByName('NoCorresponde').AsBoolean,
                                StoredProcedure.FieldByName('Orden').AsInteger,
                                StoredProcedure.FieldByName('SoloImpresion').AsInteger,
                                StoredProcedure.FieldByName('Descripcion').AsString,
                                StoredProcedure.FieldByName('ExplicacionDefault').AsString,
                                StoredProcedure.FieldByName('TipoPatente').AsString,
                                StoredProcedure.FieldByName('Patente').AsString,
                                StoredProcedure.FieldByName('CodigoDocumentacionPresentada').AsInteger,
                                iif(StoredProcedure.FieldByName('FechaBaja').IsNull, NullDate, StoredProcedure.FieldByName('FechaBaja').AsDateTime) );

          StoredProcedure.Next;
      end;
    finally
       StoredProcedure.close;
       StoredProcedure.free;
    end;
end;

{Procedure Name	: TDocumentacionConvenio.DarDocumentacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Indice: Integer
 Return Value   : TDocumentacion
}
function TDocumentacionConvenio.DarDocumentacion(
  Indice: Integer): TDocumentacion;
begin
    if (Indice < 0) or (Indice > High(FDocumentacion)) then begin
        raise Exception.Create('Indice fuera de rango');
        exit;
    end;

    result := FDocumentacion[Indice];
end;

{Procedure Name	: TDocumentacionConvenio.Destroy
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
destructor TDocumentacionConvenio.Destroy;
var
    i: Integer;
begin
  for i := 0 to high(FDocumentacion) do
    FDocumentacion[i].Free;

    VaciarListaBack;

  inherited;
end;

{Procedure Name	: TDocumentacionConvenio.EstaDocumentacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:  CodigoDocumentacionRespaldo:Integer; InfoExtra, TipoPatente,
                    Patente: String
 Return Value   : Boolean
}
function TDocumentacionConvenio.EstaDocumentacion(
  CodigoDocumentacionRespaldo:Integer; InfoExtra, TipoPatente,
  Patente: String): Boolean;
var
    i: Integer;
begin
    // la infoextra, sirve para rep univocamente el doc, esto es muy util cuando estoy dando de alta y baja muchos rep legales
    Result := False;
    for i := 0 to High(FDocumentacion) do
        if (FDocumentacion[i].CodigoDocumentacionRespaldo = CodigoDocumentacionRespaldo) and
            ( (Patente = '') or ((FDocumentacion[i].Patente = Patente) and (FDocumentacion[i].TipoPatente = TipoPatente)) ) and
            (FDocumentacion[i].FechaBaja = nullDate) and (FDocumentacion[i].InformacionExtra = InfoExtra) then
            Result := True;
end;

{Procedure Name	: TDocumentacionConvenio.EstaDocumentacionComoNoObligatoria
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoDocumentacionRespaldo:Integer
 Return Value   : Boolean
}
function TDocumentacionConvenio.EstaDocumentacionComoNoObligatoria(
  CodigoDocumentacionRespaldo:Integer): Boolean;
var
    i: Integer;
begin
    // la infoextra, sirve para rep univocamente el doc, esto es muy util cuando estoy dando de alta y baja muchos rep legales
    Result := False;
    for i := 0 to High(FDocumentacion) do
        if (FDocumentacion[i].CodigoDocumentacionRespaldo = CodigoDocumentacionRespaldo) and
        (FDocumentacion[i].NoCorresponde = True)  then
            Result := True;
end;

{Procedure Name	: TDocumentacionConvenio.GetCantidadDocumentacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TDocumentacionConvenio.GetCantidadDocumentacion: Integer;
begin
    Result := High(FDocumentacion);
end;


{Procedure Name	: TDocumentacionConvenio.GetCantidadOperacionesConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TDocumentacionConvenio.GetCantidadOperacionesConvenio: integer;
begin
    result := Length(FOperacionConvenio);
end;


{Procedure Name	: TDocumentacionConvenio.GetOperacionConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   : integer
}
function TDocumentacionConvenio.GetOperacionConvenio(
  Index: integer): integer;
begin
    if ((Index >= length(FOperacionConvenio)) or (Index < 0)) then begin
        raise Exception.Create('Indice de operaciones convenio fuera de rango');
        exit;
    end;
    result := FOperacionConvenio[Index];
end;

{Procedure Name	: TDocumentacionConvenio.GetHayDocumentacionSinControlar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDocumentacionConvenio.GetHayDocumentacionSinControlar: Boolean;
var
    i: Integer;
begin
    Result := False;
    for i := 0 to High(FDocumentacion) do
        if (FDocumentacion[i].EsNuevo) and (FDocumentacion[i].FechaBaja = nullDate)
            and (not FDocumentacion[i].Marcado) and (not FDocumentacion[i].SoloImprimir) then Result := True;
end;

{Procedure Name	: TDocumentacionConvenio.Guardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TDocumentacionConvenio.Guardar;
var
    i: Integer;
begin

    with TADOStoredProc.Create(nil) do begin
        try
            Connection := DMConnections.BaseCAC;
            ProcedureName := 'ActualizarDocumentacionPresentadaConvenios';
            Parameters.Refresh;

            for i := 0 to high(FDocumentacion) do begin
                with Parameters, FDocumentacion[i] do begin
                    if ((EsNuevo) and (FechaBaja = nulldate)) or ( (not EsNuevo) and (EsModificado)) then begin
                        ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                        ParamByName('@CodigoDocumentacionRespaldo').Value := CodigoDocumentacionRespaldo;
                        ParamByName('@NoCorresponde').Value := NoCorresponde;
                        ParamByName('@PathArchivo').Value := null;
                        ParamByName('@FechaPresentacion').Value := NowBase(DMConnections.BaseCAC);
                        ParamByName('@FechaBaja').Value := FechaBajaGuardar;
                        ParamByName('@TipoPatente').Value := TipoPatente;
                        ParamByName('@Patente').Value := Patente;
                        ParamByName('@CodigoDocumentacionPresentada').Value := CodigoDocumentacionPresentada;
                        ExecProc;
{INICIO: TASK_106_JMA_20170206}
                    if ParamByName('@RETURN_VALUE').Value<>0 then
                        raise Exception.Create(ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}
                    end;
                end;

            end;
        finally
            close;
            free;
        end;
    end;

end;

{Procedure Name	: TDocumentacionConvenio.ObtenerListaDocumentacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var Documentacion: RegDocumentacionPresentada
 Return Value   : Boolean
}
function TDocumentacionConvenio.ObtenerListaDocumentacion(
  var Documentacion: RegDocumentacionPresentada): Boolean;
var
    i: Integer;
begin
    SetLength(Documentacion, 0);
    for i := 0 to High(FDocumentacion) do begin
        if (FDocumentacion[i].FechaBaja = nullDate) and                                                                              // SS_1013_PDO_20120113
            ((FDocumentacion[i].Marcado) or ((not FDocumentacion[i].Marcado) and (FDocumentacion[i].Patente = ''))) then begin       // SS_1013_PDO_20120113
            SetLength(Documentacion, Length(Documentacion) + 1);
            with Documentacion[High(Documentacion)] do begin
                CodigoDocumentacionRespaldo := FDocumentacion[i].CodigoDocumentacionRespaldo;
                IndiceVehiculoRelativo := -1;
                TipoPatente := FDocumentacion[i].TipoPatente;
                Patente := FDocumentacion[i].Patente;
                Marcado := FDocumentacion[i].Marcado;
                NoCorresponde := FDocumentacion[i].NoCorresponde;
                InfoAdicional := '';
                Obligatorio := FDocumentacion[i].Obligatorio;
                Orden := FDocumentacion[i].Orden;
                SoloImpresion := FDocumentacion[i].SoloImpresion;
            end;
        end;
    end;
    Result := Length(Documentacion) > 0;
end;

{Procedure Name	: TDocumentacionConvenio.ObtenerListaDocumentacionGuardada
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: var Documentacion: RegDocumentacionPresentada
 Return Value   : Boolean
}
function TDocumentacionConvenio.ObtenerListaDocumentacionGuardada(
  var Documentacion: RegDocumentacionPresentada): Boolean;
var
    i: Integer;
begin
    SetLength(Documentacion, 0);
    for i := 0 to High(FDocumentacion) do begin
        if (not FDocumentacion[i].EsNuevo) and (FDocumentacion[i].FechaBaja = nullDate) then begin
            SetLength(Documentacion, Length(Documentacion) + 1);
            with Documentacion[High(Documentacion)] do begin
                CodigoDocumentacionRespaldo := FDocumentacion[i].CodigoDocumentacionRespaldo;
                IndiceVehiculoRelativo := -1;
                TipoPatente := FDocumentacion[i].TipoPatente;
                Patente := FDocumentacion[i].Patente;
                Marcado := FDocumentacion[i].Marcado;
                NoCorresponde := FDocumentacion[i].NoCorresponde;
                InfoAdicional := '';
                Obligatorio := FDocumentacion[i].Obligatorio;
                Orden := FDocumentacion[i].Orden;
                SoloImpresion := FDocumentacion[i].SoloImpresion;
            end;
        end;
    end;
    Result := Length(Documentacion) > 0;
end;

{Procedure Name	: TDocumentacionConvenio.SetCodigoConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Integer
 Return Value   :
}
procedure TDocumentacionConvenio.SetCodigoConvenio(const Value: Integer);
begin
  FCodigoConvenio := Value;
end;

{Procedure Name	: TDocumentacionConvenio.VaciarOperacionConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TDocumentacionConvenio.VaciarOperacionConvenio;
begin
    SetLength(FOperacionConvenio, 0);
end;

{Procedure Name	: TDocumentacionConvenio.EliminarDocumentacionxTipoOperacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:CodigoTipoOperacion: Integer; InfoExtra, TipoPatente, Patente: String;
  CodigoDocumentacionPresentada: Integer
 Return Value   :
}
procedure TDocumentacionConvenio.EliminarDocumentacionxTipoOperacion(
  CodigoTipoOperacion: Integer; InfoExtra, TipoPatente, Patente: String;
  CodigoDocumentacionPresentada: Integer);
begin
    with TADOStoredProc.Create(nil) do begin
        try
            Connection := DMConnections.BaseCAC;

            ProcedureName := 'ObtenerDocumentacionxTipoOperacionConvenio';
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoTipoOperacionConvenio').Value := CodigoTipoOperacion;

            Open;
            while not eof do begin
                if FieldByName('Cancela').AsInteger = 0 then
                    // Cargo la doc de la Operacion
                    CancelarDoc(FieldByName('CodigoDocumentacionRespaldo').AsInteger,
                                InfoExtra,
                                TipoPatente,
                                Patente,
                                CodigoDocumentacionPresentada);
                Next;
            end;
        finally
            close;
            free;
        end;
    end;
end;

{Procedure Name	: TDocumentacionConvenio.GetHayDocumentacionAImprimir
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDocumentacionConvenio.GetHayDocumentacionAImprimir: Boolean;
var
    i: Integer;
begin
    Result := False;
    for i := 0 to High(FDocumentacion) do
        if (FDocumentacion[i].FechaBaja = nullDate) and
            (not FDocumentacion[i].SoloMostrar) then Result := True;
end;

{Procedure Name	: TDocumentacionConvenio.Compactar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TDocumentacionConvenio.Compactar;
var
    i: Integer;
begin
    for i := 0 to CantidadDocumentacion  do
        if DarDocumentacion(i).EsNuevo and (DarDocumentacion(i).FechaBaja <> nulldate) then
            EliminarRegistro(i);
    CompactarRegistro;
end;

{Procedure Name	: TDocumentacionConvenio.EliminarRegistro
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Indice: Integer
 Return Value   :
}
procedure TDocumentacionConvenio.EliminarRegistro(Indice: Integer);
begin
    FDocumentacion[Indice].CodigoDocumentacionRespaldo := -1;
end;

{Procedure Name	: TDocumentacionConvenio.CompactarRegistro
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TDocumentacionConvenio.CompactarRegistro;
var
    i,x: Integer;
begin
    i := 0;
    while i < Length(FDocumentacion) do begin
        if DarDocumentacion(i).CodigoDocumentacionRespaldo = -1 then begin
            for x := i to Length(FDocumentacion) - 2 do
                FDocumentacion[x] := FDocumentacion[x + 1];
            SetLength(FDocumentacion, Length(FDocumentacion) - 1);
        end else inc(i);
    end;
end;

{Procedure Name	: TDocumentacionConvenio.IniciarObtenerCambio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TDocumentacionConvenio.IniciarObtenerCambio;
var
    i: Integer;
    aux: array of TDocumentacion;
begin
    VaciarListaBack;
    for i := 0 to High(FDocumentacion) do begin
        if FDocumentacion[i].EsNuevo then begin
            SetLength(FDocumentacionBack, Length(FDocumentacionBack) + 1);
            FDocumentacionBack[High(FDocumentacionBack)] := FDocumentacion[i];
        end;
    end;
    for i := 0 to High(FDocumentacion) do begin
        if not FDocumentacion[i].EsNuevo then begin
            SetLength(Aux, Length(Aux) + 1);
            Aux[High(Aux)] := FDocumentacion[i];
        end;
    end;
    SetLength(FDocumentacion, Length(Aux));
    for i := 0 to High(Aux) do
        FDocumentacion[i] := Aux[i];

    SetLength(Aux, 0);
end;

{Procedure Name	: TDocumentacionConvenio.FinObtenerCambio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TDocumentacionConvenio.FinObtenerCambio;
    {Procedure Name	: EstaEnBack
     Author 		: Nelson Droguett Sierra ()
     Date Created	: 20-Abril-2010
     Description	:
     Parameters   	: CodigoDocumentacionRespaldo: Integer; InfoExtra, TipoPatente, Patente: String
     Return Value   :Integer
    }
    function EstaEnBack(CodigoDocumentacionRespaldo: Integer; InfoExtra, TipoPatente, Patente: String): Integer;
    var
        i: Integer;
    begin
        Result := -1;
        for i := 0 to High(FDocumentacionBack) do
            if (FDocumentacionBack[i].CodigoDocumentacionRespaldo = CodigoDocumentacionRespaldo) and
                (FDocumentacionBack[i].InformacionExtra = InfoExtra) and
                (FDocumentacionBack[i].TipoPatente = TipoPatente) and
                (FDocumentacionBack[i].Patente = Patente) then
                    Result := i;
    end;
var
    i,aux: Integer;
begin
    for i := 0 to High(FDocumentacion) do begin
        aux := EstaEnBack(FDocumentacion[i].CodigoDocumentacionRespaldo,
                        FDocumentacion[i].InformacionExtra,
                        FDocumentacion[i].TipoPatente,
                        FDocumentacion[i].Patente);
         if aux >= 0 then begin
                        FDocumentacion[i].Marcado := FDocumentacionBack[aux].Marcado;
                        FDocumentacion[i].NoCorresponde := FDocumentacionBack[aux].NoCorresponde;
         end;
    end;
    Compactar;
    VaciarListaBack;
end;

{Procedure Name	: TDocumentacionConvenio.VaciarListaBack
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TDocumentacionConvenio.VaciarListaBack;
var
    i: Integer;
begin
  for i := 0 to High(FDocumentacionBack) do
    FDocumentacionBack[i].Free;

  SetLength(FDocumentacionBack, 0);
end;

{Procedure Name	: TDocumentacionConvenio.ActualizarEstadoDoc
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Patente: ShortString
 Return Value   :
}
procedure TDocumentacionConvenio.ActualizarEstadoDoc(Patente: ShortString);
var
    i, j: Integer;
    Encontro: Boolean;
begin
    for i := 0 to High(FDocumentacion) do
        if (FDocumentacion[i].EsNuevo) and (FDocumentacion[i].FechaBaja = nullDate)
          and (not FDocumentacion[i].Marcado) and (not FDocumentacion[i].SoloImprimir) then begin
            j := High(FDocumentacion);
            Encontro := False;
            while (j > 0) do begin
                if (FDocumentacion[j].Patente = Patente) and
                   (FDocumentacion[j].Patente = FDocumentacion[i].Patente) and
                   (FDocumentacion[j].CodigoDocumentacionRespaldo = FDocumentacion[i].CodigoDocumentacionRespaldo) and
                   (i <> j) then begin
                    Encontro := True;
                    Break;
                end
                else
                    Dec(j);
            end;
            if Encontro then begin
                FDocumentacion[i].Marcado := True;
                FDocumentacion[i].NoCorresponde := FDocumentacion[j].NoCorresponde;
            end;
        end;

end;

{Procedure Name	: TDocumentacionConvenio.ClonarOpcionales
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TDocumentacionConvenio.ClonarOpcionales;
var
    indice: integer;
begin
    for indice := High(FDocumentacion) downto 0 do begin
        if (not FDocumentacion[indice].EsNuevo) and (FDocumentacion[indice].FechaBaja = nullDate)
          and (not FDocumentacion[indice].Marcado) and (not FDocumentacion[indice].SoloImprimir)
          and (FDocumentacion[indice].NoCorresponde) then
            Self.AgregarDocumentacion(FDocumentacion[indice].CodigoConvenio,
                  FDocumentacion[indice].CodigoDocumentacionRespaldo,
                  True,//Seria el No Corresponde;
                  FDocumentacion[indice].Orden,
                  FDocumentacion[indice].SoloImpresion,
                  FDocumentacion[indice].Descripcion);

    end;
end;

{Procedure Name	: TDocumentacionConvenio.ConvertirDocumentacionConvenioSinCuentaAlta
 Author 		: Nelson Droguett Sierra (vpaszkowicz)
 Date Created	: 20-Abril-2010
 Description	: Agrega la documentaci�n necesaria para el alta que no est� en el
convenio sin cuenta
 Parameters   	: Personeria: string
 Return Value   :
}
procedure TDocumentacionConvenio.ConvertirDocumentacionConvenioSinCuentaAlta(Personeria: string);
begin
    with TADOStoredProc.Create(nil) do begin
    try
        Connection := DMConnections.BaseCAC;
        ProcedureName := 'ObtenerDocumentacionxTipoOperacionConvenio';
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoTipoOperacionConvenio').Value := 1;
        Parameters.ParamByName('@Personeria').Value := Personeria;
        Parameters.ParamByName('@Tipo').Value := 'C';
        Open;
        while not eof do begin
            if EstaDocumentacionComoNoObligatoria(FieldByName('CodigoDocumentacionRespaldo').AsInteger)
                and (FieldByName('Obligatorio').asBoolean) then
                        AgregarDocumentacionxOperacion(
                                      FieldByName('CodigoDocumentacionRespaldo').AsInteger,
                                      False,
                                      FieldByName('Orden').AsInteger,
                                      FieldByName('SoloImpresion').AsInteger,
                                      FieldByName('Obligatorio').AsBoolean,
                                      FieldByName('CodigoTipoOperacionConvenio').AsInteger,
                                      FieldByName('Descripcion').AsString,
                                      FieldByName('Explicacion').AsString,
                                      '',
                                      FieldByName('ExplicacionDefault').AsString);
                Next;
            end;
        finally
            close;
            free;
        end;
    end;
end;

{ TDocumentacion }


{Procedure Name	: TDocumentacion.GetFechaBajaGuardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Variant
}
function TDocumentacion.GetFechaBajaGuardar: Variant;
begin
    result := iif(FechaBaja = NullDate, null, FechaBaja);
end;

{Procedure Name	: TDocumentacion.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoConvenio, CodigoDocumentacionRespaldo: Integer;
            NoCorresponde: Boolean; Orden: integer; SoloImpresion: Integer;
            Descripcion: String; ExplicacionDefault: String = ''; TipoPatente: String = ''; Patente: String = '';
            CodigoDocumentacionPresentada: Integer = -1; FechaBaja: TDatetime = Nulldate
 Return Value   :
}
constructor TDocumentacion.Create(CodigoConvenio, CodigoDocumentacionRespaldo: Integer;
            NoCorresponde: Boolean; Orden: integer; SoloImpresion: Integer;
            Descripcion: String; ExplicacionDefault: String = ''; TipoPatente: String = ''; Patente: String = '';
            CodigoDocumentacionPresentada: Integer = -1; FechaBaja: TDatetime = Nulldate);
begin
    FCodigoConvenio := CodigoConvenio;
    FCodigoDocumentacionRespaldo := CodigoDocumentacionRespaldo;
    FNoCorresponde := NoCorresponde;
    FPatente := Patente;
    FTipoPatente := TipoPatente;
    FCodigoDocumentacionPresentada := CodigoDocumentacionPresentada;
    FFechaBaja := FechaBaja;
    FPatente := Patente;
    FCodigoDocumentacionPresentada := CodigoDocumentacionPresentada;
    FOrden := Orden;
    FSoloImpresion := SoloImpresion;
    FExplicacionDefault := ExplicacionDefault;
    FEsModificado := False;
    FDescripcion := Descripcion;
    FInformacionExtra := '';
    FExplicacion := '';
    FObligatorio := Null;
    FCodigoTipoOperacion := -1;
end;

{Procedure Name	: TDocumentacion.Destroy
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
destructor TDocumentacion.Destroy;
begin
  inherited;
end;

{Procedure Name	: TDocumentacion.SetCodigoConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Integer
 Return Value   :
}
procedure TDocumentacion.SetCodigoConvenio(const Value: Integer);
begin
  FCodigoConvenio := Value;
end;

{Procedure Name	: TDocumentacion.SetCodigoDocumentacionPresentada
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Integer
 Return Value   :
}
procedure TDocumentacion.SetCodigoDocumentacionPresentada(
  const Value: Integer);
begin
  FCodigoDocumentacionPresentada := Value;
end;

{Procedure Name	: TDocumentacion.SetCodigoDocumentacionRespaldo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Integer
 Return Value   :
}
procedure TDocumentacion.SetCodigoDocumentacionRespaldo(
  const Value: Integer);
begin
  FCodigoDocumentacionRespaldo := Value;
end;

{Procedure Name	:  TDocumentacion.SetFechaBaja
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Value: TDatetime
 Return Value   :
}
procedure TDocumentacion.SetFechaBaja(Value: TDatetime);
begin
  Value := iif(Value = Null, NullDate, Value);
  if Value <> FFechaBaja then FEsModificado := True;
  FFechaBaja := Value;
end;

{Procedure Name	: TDocumentacion.SetNoCorresponde
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Boolean
 Return Value   :
}
procedure TDocumentacion.SetNoCorresponde(const Value: Boolean);
begin
  FNoCorresponde := Value;
end;

{Procedure Name	: TDocumentacion.SetPatente
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: String
 Return Value   :
}
procedure TDocumentacion.SetPatente(const Value: String);
begin
  FPatente := Trim(Value);
end;

{Procedure Name	: TDocumentacion.SetTipoPatente
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: String
 Return Value   :
}
procedure TDocumentacion.SetTipoPatente(const Value: String);
begin
  FTipoPatente := Trim(Value);
end;

{Procedure Name	: TDocumentacion.SetMarcado
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Boolean
 Return Value   :
}
procedure TDocumentacion.SetMarcado(const Value: Boolean);
begin
  if Value <> Marcado then FEsModificado := True;
  FMarcado := Value;
end;

{Procedure Name	: TDocumentacion.GetEsNuevo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDocumentacion.GetEsNuevo: Boolean;
begin
    Result := FCodigoDocumentacionPresentada < 1;
end;

{Procedure Name	: TDocumentacion.GetExplicacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : String
}
function TDocumentacion.GetExplicacion: String;
begin
    if (FExplicacionDefault = '') or (FExplicacionDefault = null) then begin
        VerificarInfo;
        Result := FExplicacion;
    end else Result := FExplicacionDefault;
end;

{Procedure Name	: TDocumentacion.GetObligatorio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDocumentacion.GetObligatorio: Boolean;
begin
    VerificarInfo;
    if FObligatorio <> null then Result := FObligatorio
    else result := False;
end;

{Procedure Name	: TDocumentacion.GetOrden
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TDocumentacion.GetOrden: integer;
begin
    Result := FOrden;
end;

{Procedure Name	: TDocumentacion.GetSoloImpresion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Integer
}
function TDocumentacion.GetSoloImpresion: Integer;
begin
    Result := FSoloImpresion;
end;

{Procedure Name	: TDocumentacion.VerificarInfo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDocumentacion.VerificarInfo: Boolean;
resourcestring
    MSG_TYPE_OPERATION_INVALID = 'Tipo de Operacion de convenio no valida';
begin
    Result := False;
    if (FObligatorio = null) and (CodigoTipoOperacion > 0) then begin
        with TADOStoredProc.Create(nil) do begin
            try
                Connection := DMConnections.BaseCAC;
                ProcedureName :='ObtenerInfoDocumentacionRequerida';
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoDocumentacionRequerida').Value := CodigoDocumentacionRespaldo;
                Parameters.ParamByName('@CodigoTipoOperacionConvenio').Value := CodigoTipoOperacion;
                Open;
                if recordCount > 1 then begin
                    FExplicacion := FieldByName('Explicacion').AsString;
                    FObligatorio := FieldByName('Obligatorio').AsBoolean;
                end else raise Exception.Create(MSG_TYPE_OPERATION_INVALID);
            finally
                close;
                free;
                Result := True;
            end;
        end;
    end;
end;

{Procedure Name	: TDocumentacion.SetCodigoTipoOperacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Integer
 Return Value   :
}
procedure TDocumentacion.SetCodigoTipoOperacion(const Value: Integer);
begin
  FCodigoTipoOperacion := Value;
end;

{Procedure Name	: TDocumentacion.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoConvenio, CodigoDocumentacionRespaldo: Integer;
            NoCorresponde: Boolean; Orden: integer; SoloImpresion: Integer; Descripcion: String;
            Explicacion: String; Obligatorio: Boolean; CodigoTipoOperacion: Integer;
            InformacionExtra: String = '';
            ExplicacionDefault: String = ''; TipoPatente: String = ''; Patente: String = '';
            CodigoDocumentacionPresentada: Integer = -1)
 Return Value   :
}
constructor TDocumentacion.Create(CodigoConvenio, CodigoDocumentacionRespaldo: Integer;
            NoCorresponde: Boolean; Orden: integer; SoloImpresion: Integer; Descripcion: String;
            Explicacion: String; Obligatorio: Boolean; CodigoTipoOperacion: Integer;
            InformacionExtra: String = '';
            ExplicacionDefault: String = ''; TipoPatente: String = ''; Patente: String = '';
            CodigoDocumentacionPresentada: Integer = -1);
begin
    FCodigoConvenio := CodigoConvenio;
    FCodigoDocumentacionRespaldo := CodigoDocumentacionRespaldo;
    FNoCorresponde := NoCorresponde;
    FPatente := Patente;
    FTipoPatente := TipoPatente;
    FCodigoDocumentacionPresentada := CodigoDocumentacionPresentada;
    FPatente := Patente;
    FCodigoDocumentacionPresentada := CodigoDocumentacionPresentada;
    FFechaBaja := nullDate;
    FOrden := Orden;
    FSoloImpresion := SoloImpresion;
    FExplicacionDefault := ExplicacionDefault;
    FEsModificado := False;
    FDescripcion := Descripcion;
    FInformacionExtra := InformacionExtra;
    FExplicacion := Explicacion;
    FObligatorio := Obligatorio;
    FCodigoTipoOperacion := CodigoTipoOperacion;

end;


{Procedure Name	: TDocumentacion.SetDescripcion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: String
 Return Value   :
}
procedure TDocumentacion.SetDescripcion(const Value: String);
begin
  FDescripcion := Value;
end;

{Procedure Name	: TDocumentacion.GetMostrarImprimir
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDocumentacion.GetMostrarImprimir: Boolean;
begin
    result := SoloImpresion = 0;
end;

{Procedure Name	: TDocumentacion.GetSoloImprimir
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDocumentacion.GetSoloImprimir: Boolean;
begin
    result := SoloImpresion = 1;
end;

{Procedure Name	: TDocumentacion.GetSoloMostrar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TDocumentacion.GetSoloMostrar: Boolean;
begin
    result := SoloImpresion = 2;
end;

{Procedure Name	: TDocumentacion.SetInformacionExtra
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: String
 Return Value   :
}
procedure TDocumentacion.SetInformacionExtra(const Value: String);
begin
  FInformacionExtra := Value;
end;

{ TRepresentanteLegal }

{Procedure Name	: TRepresentanteLegal.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoPersona: integer; NroRepresentate: Integer
 Return Value   :
}
constructor TRepresentanteLegal.Create(CodigoPersona: integer; NroRepresentate: Integer);
begin
    inherited Create(CodigoPersona);
    NroRepresentante := NroRepresentate;
end;

{Procedure Name	: TRepresentanteLegal.SetNroRepresentante
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Integer
 Return Value   :
}
procedure TRepresentanteLegal.SetNroRepresentante(const Value: Integer);
begin
  FNroRepresentante := Value;
end;

{ TCheckListConvenio }

{Procedure Name	: TCheckListConvenio.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoConvenio: Integer
 Return Value   :
}
constructor TCheckListConvenio.Create(CodigoConvenio: Integer);
resourcestring
    MSG_ERROR = 'Error al obtener ChecklistConvenio.';
var
    ObtenerCheckListConvenio: TADOStoredProc;
    i: integer;
begin
    ObtenerCheckListConvenio := TADOStoredProc.Create(nil);
    try
        try
            FCodigoConvenio := CodigoConvenio;
            ObtenerCheckListConvenio.Connection := DMConnections.BaseCAC;
            ObtenerCheckListConvenio.ProcedureName := 'ObtenerCheckListConvenio';
            ObtenerCheckListConvenio.Parameters.Refresh;
            ObtenerCheckListConvenio.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            ObtenerCheckListConvenio.Open;
            SetLength(FACheckListConvenio, ObtenerCheckListConvenio.RecordCount);
            i:= 0;
            while not ObtenerCheckListConvenio.Eof do begin
                FACheckListConvenio[i].CodigoCheckList := ObtenerCheckListConvenio.FieldByName('CodigoCheckList').AsInteger;
                FACheckListConvenio[i].Descripcion := Trim(ObtenerCheckListConvenio.FieldByName('Descripcion').AsString);
                FACheckListConvenio[i].Marcado := ObtenerCheckListConvenio.FieldByName('Marcado').AsBoolean;
                ObtenerCheckListConvenio.Next;
                i:= i + 1;
            end;
        except
             on E: Exception do begin
                raise EConvenioError.create(MSG_ERROR, e.message , 0)
             end;
        end;
    finally
        ObtenerCheckListConvenio.Close;
        ObtenerCheckListConvenio.Free;
    end;
end;

{Procedure Name	: TCheckListConvenio.DesMarcar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoCheckList: integer
 Return Value   :
}
procedure TCheckListConvenio.DesMarcar(CodigoCheckList: integer);
var
    i: integer;
begin
    i:= 0;
    while (i<= Cantidad-1) and (FACheckListConvenio[i].CodigoCheckList <> CodigoCheckList) do
        i:= i + 1;
    if (i<= Cantidad-1) then FACheckListConvenio[i].Marcado := False;
end;

{Procedure Name	: TCheckListConvenio.GetListConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   : TRCheckListConvenio
}
function TCheckListConvenio.GetListConvenio(
  Index: integer): TRCheckListConvenio;
resourcestring
    MSG_INVALID_INDEX = 'Indice CheckListConvenio fuera de Rango';
begin
    if Index > Length(FACheckListConvenio)-1 then begin
        raise Exception.Create(MSG_INVALID_INDEX)
    end;

    result := FACheckListConvenio[Index];
end;

{Procedure Name	: TCheckListConvenio.GetTieneMarcados
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : boolean
}
function TCheckListConvenio.GetTieneMarcados: boolean;
var
    i: integer;
begin
    i:= 0;
    while (i<= Cantidad - 1) and (not FACheckListConvenio[i].Marcado) do
        inc(i);
    result := (i<= Cantidad - 1);
end;

{Procedure Name	: TCheckListConvenio.Guardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:  CodigoConvenio: Integer = -1
 Return Value   :
}
procedure TCheckListConvenio.Guardar(CodigoConvenio: Integer = -1);
resourcestring
    MSG_SAVE_ERROR = 'Error al guardar Convenio CheckList.';
var
    ActualizarConvenioCheckList: TADOStoredProc;
    EliminarConvenioCheckList: TADOStoredProc;
    i: integer;
begin
    ActualizarConvenioCheckList := TADOStoredProc.Create(nil);
    EliminarConvenioCheckList := TADOStoredProc.Create(nil);
    i:= 0;
    try
        try
            if CodigoConvenio <> -1 then FCodigoConvenio := CodigoConvenio;
            EliminarConvenioCheckList.Connection := DMConnections.BaseCAC;
            EliminarConvenioCheckList.ProcedureName := 'EliminarConvenioCheckList';
            EliminarConvenioCheckList.Parameters.Refresh;
            EliminarConvenioCheckList.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            EliminarConvenioCheckList.ExecProc;
 {INICIO: TASK_106_JMA_20170206}
            if EliminarConvenioCheckList.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                raise Exception.Create(EliminarConvenioCheckList.Parameters.ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}

            ActualizarConvenioCheckList.Connection := DMConnections.BaseCAC;
            ActualizarConvenioCheckList.ProcedureName := 'ActualizarConvenioCheckList';
            ActualizarConvenioCheckList.Parameters.Refresh;
            while (i<= Cantidad-1) do begin
                if FACheckListConvenio[i].Marcado then begin
                    ActualizarConvenioCheckList.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
                    ActualizarConvenioCheckList.Parameters.ParamByName('@CodigoCheckList').Value := FACheckListConvenio[i].CodigoCheckList;
                    ActualizarConvenioCheckList.ExecProc;
{INICIO: TASK_106_JMA_20170206}
                if ActualizarConvenioCheckList.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                    raise Exception.Create(ActualizarConvenioCheckList.Parameters.ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}
                end;
                inc(i);
            end;
        except
            on E:Exception do begin
                raise EConvenioError.create(MSG_SAVE_ERROR, e.message, ERRORNOEXISTECONVENIO);
            end;
        end;
    finally
        ActualizarConvenioCheckList.Free;
        EliminarConvenioCheckList.Free;
    end;
end;

{Procedure Name	: TCheckListConvenio.Marcar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoCheckList: integer
 Return Value   :
}
procedure TCheckListConvenio.Marcar(CodigoCheckList: integer);
var
    i: integer;
begin
    i:= 0;
    while (i<= Cantidad-1) and (FACheckListConvenio[i].CodigoCheckList <> CodigoCheckList) do
        i:= i + 1;

    if i<= Cantidad-1 then FACheckListConvenio[i].Marcado := True;
end;

{Procedure Name	: TCheckListConvenio.SetCantidad
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TCheckListConvenio.SetCantidad: integer;
begin
    result := Length(FACheckListConvenio)
end;


{Procedure Name	: TCheckListConvenio.SetListConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer; const Value: TRCheckListConvenio
 Return Value   :
}
procedure TCheckListConvenio.SetListConvenio(Index: integer;
  const Value: TRCheckListConvenio);
resourcestring
    MSG_INVALID_INDEX = 'Indice CheckListConvenio fuera de Rango';
begin
    if Index > Length(FACheckListConvenio)-1 then begin
        raise Exception.Create(MSG_INVALID_INDEX)
    end;
    FACheckListConvenio[Index] := Value;

end;

{ TObservacionConvenio }

{Procedure Name	: TObservacionConvenio.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
constructor TObservacionConvenio.Create;
begin
    FIndiceObservacion := -1;
    FObservacion := '';
    FActivo := False;
end;

{Procedure Name	: TObservacionConvenio.GetActivo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: boolean
 Return Value   :
}
procedure TObservacionConvenio.GetActivo(const Value: boolean);
begin
    FActivo := Value;
end;

{Procedure Name	: TObservacionConvenio.GetIndiceObservacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: integer
 Return Value   :
}
procedure TObservacionConvenio.GetIndiceObservacion(const Value: integer);
begin
    FIndiceObservacion := Value;
end;

{Procedure Name	: TObservacionConvenio.GetObservacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: string
 Return Value   :
}
procedure TObservacionConvenio.GetObservacion(const Value: string);
begin
    FObservacion := Value;
end;

{Procedure Name	: TObservacionConvenio.SetActivo
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TObservacionConvenio.SetActivo: Boolean;
begin
    Result := FActivo;
end;

{Procedure Name	: TObservacionConvenio.SetIndiceObservacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :  integer
}
function TObservacionConvenio.SetIndiceObservacion: integer;
begin
    result := FIndiceObservacion;
end;

{Procedure Name	: TObservacionConvenio.SetObservacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : String
}
function TObservacionConvenio.SetObservacion: String;
begin
    result := FObservacion;
end;

{ TListObservacionConvenio }

{Procedure Name	: TListObservacionConvenio.Add
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const ObservacionConvenio: TObservacionConvenio
 Return Value   : integer
}
function TListObservacionConvenio.Add(
  const ObservacionConvenio: TObservacionConvenio): integer;
begin
    result := FItems.Add(ObservacionConvenio);
end;

{Procedure Name	: TListObservacionConvenio.Clear
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TListObservacionConvenio.Clear;
begin
    FItems.Clear;
end;

{Procedure Name	: TListObservacionConvenio.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoConvenio: Integer
 Return Value   :
}
constructor TListObservacionConvenio.Create(CodigoConvenio: Integer);
resourcestring
    MSG_QUERY_ERROR = 'Error al obtener las observaciones del convenio.';
var
    ObtenerConvenioObservacion: TADOStoredProc;
    ObservacionConvenio: TObservacionConvenio;
begin
    ObtenerConvenioObservacion := TADOStoredProc.Create(nil);
    FItems := TList.Create;
    try
        try
            FCodigoConvenio := CodigoConvenio;
            ObtenerConvenioObservacion.Connection := DMConnections.BaseCAC;
            ObtenerConvenioObservacion.ProcedureName := 'ObtenerConvenioObservacion';
            ObtenerConvenioObservacion.Parameters.Refresh;
            ObtenerConvenioObservacion.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            ObtenerConvenioObservacion.Parameters.ParamByName('@FechaDesde').Value := Null;                                           //SS-842-NDR-20110912
            ObtenerConvenioObservacion.Parameters.ParamByName('@FechaHasta').Value := Null;                                           //SS-842-NDR-20110912
            ObtenerConvenioObservacion.Open;
            while not ObtenerConvenioObservacion.Eof do begin
                ObservacionConvenio := TObservacionConvenio.Create;
                ObservacionConvenio.IndiceObservacion := ObtenerConvenioObservacion.FieldbyName('IndiceObservacion').AsInteger;
                ObservacionConvenio.Observacion := ObtenerConvenioObservacion.FieldbyName('Observacion').AsString;
                ObservacionConvenio.Activo := ObtenerConvenioObservacion.FieldbyName('FechaBaja').IsNull;
                ObservacionConvenio.FechaObservacion := ObtenerConvenioObservacion.FieldByName('FechaObservacion').AsDateTime;       //SS-842-NDR-20110912
                Self.Add(ObservacionConvenio);
                ObtenerConvenioObservacion.Next;
            end;
            ObtenerConvenioObservacion.Close;
        except
            on E: Exception do begin
                raise EConvenioError.create(MSG_QUERY_ERROR, e.message , 0)
            end;
        end;
    finally
        ObtenerConvenioObservacion.Free;
    end;
end;

{Procedure Name	: TListObservacionConvenio.Destroy
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
destructor TListObservacionConvenio.Destroy;
begin
    FItems.Clear;
    inherited;
end;

{Procedure Name	: TListObservacionConvenio.GetTieneObservaciones
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TListObservacionConvenio.GetTieneObservaciones: Boolean;
begin
    Result := Self.Count > 0;
end;

{Procedure Name	: TListObservacionConvenio.Guardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoConvenio: Integer = -1
 Return Value   :
}
procedure TListObservacionConvenio.Guardar(CodigoConvenio: Integer = -1);
resourcestring
    MSG_SAVE_ERROR = 'Error al guardar las Observaciones del Convenio.';
var
    ActualizarConvenioObservacion: TADOStoredProc;
    i: integer;
begin
    ActualizarConvenioObservacion := TADOStoredProc.Create(nil);
    try
        try
            if CodigoConvenio <> -1 then FCodigoConvenio := CodigoConvenio;

            ActualizarConvenioObservacion.ProcedureName := 'ActualizarConvenioObservacion';
            ActualizarConvenioObservacion.Connection := DMConnections.BaseCAC;
            ActualizarConvenioObservacion.Parameters.Refresh;
            for i:=0 to Count-1 do
            begin
              if (Items[i].Modificada) or (Items[i].IndiceObservacion = -1) then
              begin            //SS_842_PDO_20111212
                  ActualizarConvenioObservacion.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
                  ActualizarConvenioObservacion.Parameters.ParamByName('@IndiceObservacion').Value := iif(Items[i].IndiceObservacion = -1, null, Items[i].IndiceObservacion);
                  ActualizarConvenioObservacion.Parameters.ParamByName('@Observacion').Value := Items[i].Observacion;
                  ActualizarConvenioObservacion.Parameters.ParamByName('@Activo').Value := Items[i].Activo;
                  ActualizarConvenioObservacion.Parameters.ParamByName('@FechaObservacion').Value := Items[i].FechaObservacion;                                       //SS-842-NDR-20110912
                  ActualizarConvenioObservacion.Parameters.ParamByName('@UsuarioAuditoria').Value := UsuarioSistema;                                                  //SS-842-NDR-20110912
                  ActualizarConvenioObservacion.Parameters.ParamByName('@FechaAuditoria').Value := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC); //QueryGetValueDateTime(DMConnections.BaseCAC,'SELECT GETDATE()');   //SS-842-NDR-20110912
                  ActualizarConvenioObservacion.ExecProc;
{INICIO: TASK_106_JMA_20170206}
                if ActualizarConvenioObservacion.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                    raise Exception.Create(ActualizarConvenioObservacion.Parameters.ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}
              end;
            end;
        except
            on e:Exception do begin
                raise EConvenioError.create(MSG_SAVE_ERROR, e.message, ERRORNOEXISTECONVENIO);
            end;
        end;
    finally
        ActualizarConvenioObservacion.Free;
    end;

end;

{Procedure Name	: TListObservacionConvenio.Setcount
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TListObservacionConvenio.Setcount: integer;
begin
    Result := FItems.Count;
end;

{Procedure Name	: TListObservacionConvenio.SetObservacion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: index: integer)
 Return Value   : TObservacionConvenio
}
function TListObservacionConvenio.SetObservacion(
  index: integer): TObservacionConvenio;
resourcestring
    MSG_INVALID_INDEX = 'Indice de Observaciones fuera de Rango.';
begin
    if index > FItems.Count then
        raise Exception.Create(MSG_INVALID_INDEX);

    result := TObservacionConvenio(FItems[index]);
end;

{ TChekListCuenta }

{Procedure Name	: TChekListCuenta.Agregar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoCheckList: integer;
  Vehiculo: TVehiculosConvenio; Vencimiento: TDateTime
 Return Value   :
}
procedure TChekListCuenta.Agregar(CodigoCheckList: integer;
  Vehiculo: TVehiculosConvenio; Vencimiento: TDateTime);
begin
    SetLength(FCheckListCuenta, length(FCheckListCuenta) + 1);

    FCheckListCuenta[High(FCheckListCuenta)].CodigoCheckList := CodigoCheckList;
    FCheckListCuenta[High(FCheckListCuenta)].IndiceVehiculo := Vehiculo.Cuenta.IndiceVehiculo;
    FCheckListCuenta[High(FCheckListCuenta)].Descripcion := ArmarDescripcion(CodigoCheckList, Vehiculo.Cuenta.Vehiculo.Patente, Vencimiento);
    FCheckListCuenta[High(FCheckListCuenta)].Patente := Vehiculo.Cuenta.Vehiculo.Patente;

    FCheckListCuenta[High(FCheckListCuenta)].Marcado := False;
    FCheckListCuenta[High(FCheckListCuenta)].Vencimiento := Vencimiento;
    FCheckListCuenta[High(FCheckListCuenta)].PuedeCambiarOperador := PuedeOperadorCambiarClick(FCheckListCuenta[High(FCheckListCuenta)].CodigoCheckList)

end;

{Procedure Name	: TChekListCuenta.Create
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoConvenio: Integer
 Return Value   :
}
constructor TChekListCuenta.Create(CodigoConvenio: Integer);
resourcestring
    MSG_QUERY_ERROR = 'Error al obtener ChecklistCuenta.';
var
    ObtenerCheckListCuenta: TADOStoredProc;
    ActualizarCheckListCuentas: TADOStoredProc; // Actualiza los check, si estos fueron pagados.
    i: Integer;
begin
  ActualizarCheckListCuentas := TADOStoredProc.create(nil);
  ObtenerCheckListCuenta := TADOStoredProc.Create(nil);
    try
        try
            FCodigoConvenio := CodigoConvenio;
            ObtenerCheckListCuenta.Connection := DMConnections.BaseCAC;
            ObtenerCheckListCuenta.ProcedureName := 'ObtenerCheckListCuenta';
            ObtenerCheckListCuenta.Parameters.Refresh;
            ActualizarCheckListCuentas.Connection := DMConnections.BaseCAC;
            ActualizarCheckListCuentas.ProcedureName := 'ActualizarCheckListCuentas';
            ActualizarCheckListCuentas.Parameters.Refresh;
            ActualizarCheckListCuentas.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            ActualizarCheckListCuentas.ExecProc;
            ObtenerCheckListCuenta.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            ObtenerCheckListCuenta.Open;
            SetLength(FCheckListCuenta, ObtenerCheckListCuenta.RecordCount);
            i:= 0;
            while not ObtenerCheckListCuenta.Eof do begin
                FCheckListCuenta[i].CodigoCheckList := ObtenerCheckListCuenta.FieldByName('CodigoCheckList').AsInteger;
                FCheckListCuenta[i].IndiceVehiculo := ObtenerCheckListCuenta.FieldByName('IndiceVehiculo').AsInteger;
                FCheckListCuenta[i].Descripcion := Trim(ObtenerCheckListCuenta.FieldByName('Descripcion').AsString);
                FCheckListCuenta[i].Marcado := ObtenerCheckListCuenta.FieldByName('Activo').AsBoolean;
                FCheckListCuenta[i].Patente := ObtenerCheckListCuenta.FieldByName('Patente').AsString;
                if ObtenerCheckListCuenta.FieldByName('Vencimiento').IsNull then
                    FCheckListCuenta[i].Vencimiento := NullDate
                else
                    FCheckListCuenta[i].Vencimiento := ObtenerCheckListCuenta.FieldByName('Vencimiento').AsDateTime;

                FCheckListCuenta[i].PuedeCambiarOperador := PuedeOperadorCambiarClick(FCheckListCuenta[i].CodigoCheckList);
                FCheckListCuenta[i].Eliminar := False;
                ObtenerCheckListCuenta.Next;
                i:= i + 1;
            end;
        except
             on E: Exception do begin
                raise EConvenioError.create(MSG_QUERY_ERROR, e.message , 0)
             end;
        end;
    finally
        ObtenerCheckListCuenta.Close;
        ObtenerCheckListCuenta.Free;
        ActualizarCheckListCuentas.Close;
        ActualizarCheckListCuentas.Free;
    end;
end;

{Procedure Name	: TChekListCuenta.Marcar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoCheckList, IndiceVehiculo: Integer
 Return Value   :
}
procedure TChekListCuenta.Marcar(CodigoCheckList,
  IndiceVehiculo: Integer);
var
    i: integer;
begin
    i := 0;
    while (i < Cantidad) and
    	  ((FCheckListCuenta[i].CodigoCheckList <> CodigoCheckList) or
           (FCheckListCuenta[i].IndiceVehiculo <> IndiceVehiculo)) do
          i := i + 1;

    if (i <= Cantidad - 1) then FCheckListCuenta[i].Marcado := True;
end;

{Procedure Name	: TChekListCuenta.GetListCuenta
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer
 Return Value   : TRCheckListCuenta
}
function TChekListCuenta.GetListCuenta(Index: integer): TRCheckListCuenta;
resourcestring
    MSG_INVALID_INDEX = 'Indice CheckListCuenta fuera de Rango';
begin
    if Index > Length(FCheckListCuenta)-1 then begin
        raise Exception.Create(MSG_INVALID_INDEX)
    end;

    result := FCheckListCuenta[Index];

end;

{Procedure Name	: TChekListCuenta.Guardar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   :
}
procedure TChekListCuenta.Guardar;
resourcestring
    MSG_SAVE_ERROR = 'Error al guardar Cuentas CheckList.';
var
    ActualizarCuentasCheckList : TADOStoredProc;
    i: integer;
begin
    ActualizarCuentasCheckList := TADOStoredProc.Create(nil);
    i:= 0;
    try
        try
            ActualizarCuentasCheckList.Connection := DMConnections.BaseCAC;
            ActualizarCuentasCheckList.ProcedureName := 'ActualizarCuentasCheckList';
            ActualizarCuentasCheckList.Parameters.Refresh;
            while (i <= Cantidad - 1) do begin
                ActualizarCuentasCheckList.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
                ActualizarCuentasCheckList.Parameters.ParamByName('@CodigoCheckList').Value := FCheckListCuenta[i].CodigoCheckList;
                ActualizarCuentasCheckList.Parameters.ParamByName('@IndiceVehiculo').Value := FCheckListCuenta[i].IndiceVehiculo;
                if FCheckListCuenta[i].Vencimiento = nulldate then
                    ActualizarCuentasCheckList.Parameters.ParamByName('@Vencimiento').Value := null
                else
                    ActualizarCuentasCheckList.Parameters.ParamByName('@Vencimiento').Value := FCheckListCuenta[i].Vencimiento;
                ActualizarCuentasCheckList.Parameters.ParamByName('@Activo').Value := FCheckListCuenta[i].Marcado;
                ActualizarCuentasCheckList.Parameters.ParamByName('@Elimnar').Value := FCheckListCuenta[i].Eliminar;
                ActualizarCuentasCheckList.ExecProc;
{INICIO: TASK_106_JMA_20170206}
                if ActualizarCuentasCheckList.Parameters.ParamByName('@RETURN_VALUE').Value<>0 then
                    raise Exception.Create(ActualizarCuentasCheckList.Parameters.ParamByName('@ErrorDescription').Value );
{TERMINO: TASK_106_JMA_20170206}
                ActualizarCuentasCheckList.Close;
                inc(i);
            end;
        except
            on E:Exception do begin
            raise Exception.create(MSG_SAVE_ERROR);
            end;
        end;
    finally
        ActualizarCuentasCheckList.Free;
    end;
end;

{Procedure Name	: TChekListCuenta.SetCantidad
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : integer
}
function TChekListCuenta.SetCantidad: integer;
begin
    Result := Length(FCheckListCuenta);
end;

{Procedure Name	: TChekListCuenta.SetListCuenta
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Index: integer; const Value: TRCheckListCuenta
 Return Value   :
}
procedure TChekListCuenta.SetListCuenta(Index: integer;
  const Value: TRCheckListCuenta);
resourcestring
    MSG_INVALID_INDEX = 'Indice CheckListCuenta fuera de Rango';
begin
    if Index > Length(FCheckListCuenta)-1 then begin
        raise Exception.Create(MSG_INVALID_INDEX)
    end;
    FCheckListCuenta[Index] := Value;
end;

{Procedure Name	: TChekListCuenta.Marcar
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: Indice: Integer
 Return Value   :
}
procedure TChekListCuenta.Marcar(Indice: Integer);
resourcestring
    MSG_INVALID_INDEX = 'Error en el Indice de TChekListCuenta';
begin
    if (Indice >= 0) and (Indice < cantidad) then Marcar(Items[Indice].CodigoCheckList, Items[Indice].IndiceVehiculo)
    else raise Exception.Create(MSG_INVALID_INDEX);
end;

{Procedure Name	: TChekListCuenta.PuedeOperadorCambiarClick
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoCheckList: Integer
 Return Value   : Boolean
}
function TChekListCuenta.PuedeOperadorCambiarClick(
  CodigoCheckList: Integer): Boolean;
begin
    Result := CodigoCheckList = CONST_ENTREGAR_DENUNCIA;
end;

{Procedure Name	: TChekListCuenta.PuedeCambiarElimnarCuenta
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: IndiceVehiculo: Integer
 Return Value   : Boolean
}
function TChekListCuenta.PuedeCambiarElimnarCuenta(
  IndiceVehiculo: Integer): Boolean;
var
    i: Integer;
    Cant: Integer;
begin
    Cant := 0;
    for i := 0 to Cantidad - 1 do
        if (Items[i].IndiceVehiculo = IndiceVehiculo) and (Items[i].Marcado) then inc(cant);

    Result := Cant = 2; // porque para porder eliminar la cuenta, hay tres check que tienen que estar completos
end;

{Procedure Name	: TChekListCuenta.PagoTeleviaPerdido
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: IndiceVehiculo: Integer
 Return Value   : Boolean
}
function TChekListCuenta.PagoTeleviaPerdido(
  IndiceVehiculo: Integer): Boolean;
var
    i: Integer;
begin
    Result := False;
    for i := 0 to Cantidad - 1 do
        if (Items[i].IndiceVehiculo = IndiceVehiculo) and (Items[i].Marcado) and (Items[i].CodigoCheckList = CONST_PENDIENTE_PAGAR_TAG) then Result := True;
end;

{Procedure Name	: TChekListCuenta.ElimnarItems
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: IndiceVehiculo: Integer
 Return Value   :
}
procedure TChekListCuenta.ElimnarItems(IndiceVehiculo: Integer);
var
    i: Integer;
    Aux: TRCheckListCuenta;
begin
    for i := 0 to Cantidad - 1 do begin
        if Items[i].IndiceVehiculo = IndiceVehiculo then begin
            Aux := Items[i];
            Aux.Eliminar := True;
            Items[i] := Aux;
        end;
    end;
end;

{Procedure Name	: TChekListCuenta.TieneItems
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TChekListCuenta.TieneItems: Boolean;
var
    i: Integer;
begin
    Result := False;
    for i := 0 to Cantidad - 1 do
        if not Items[i].Eliminar then Result := True;
end;

{Procedure Name	: TChekListCuenta.ActualizarVencimiento
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: IndiceVehiculo: Integer; FechaVencimiento: TDateTime
 Return Value   :
}
procedure TChekListCuenta.ActualizarVencimiento(IndiceVehiculo: Integer;
  FechaVencimiento: TDateTime);
begin
    FCheckListCuenta[IndiceVehiculo].Vencimiento := FechaVencimiento;
end;

{Procedure Name	: TChekListCuenta.ArmarDescripcion
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: CodigoCheckList: Integer; Patente: String; FechaVencimiento: TDateTime
 Return Value   : String
}
function TChekListCuenta.ArmarDescripcion(CodigoCheckList: Integer;
  Patente: String; FechaVencimiento: TDateTime): String;
begin
    Result := QueryGetValue(DMConnections.BaseCAC,
            format('SELECT DBO.DESCRIPCION_CHECK_CUENTA(%d, ''%s'', ''%s'')',
            [CodigoCheckList, Patente,  iif(FechaVencimiento = NullDate, '', FormatDateTime('dd/mm/yyyy',FechaVencimiento))]))
end;

{Procedure Name	: TChekListCuenta.EntregoDenuncia
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: IndiceVehiculo: Integer
 Return Value   : Boolean
}
function TChekListCuenta.EntregoDenuncia(IndiceVehiculo: Integer): Boolean;
var
    i: Integer;
begin
    Result := False;
    for i := 0 to Cantidad - 1 do
        if not Items[i].Eliminar and
            (Items[i].IndiceVehiculo = IndiceVehiculo) and
            (Items[i].CodigoCheckList = CONST_ENTREGAR_DENUNCIA) and
            (Items[i].Marcado) then Result := True;
end;

{Procedure Name	: TMediosPagoConvenio.EsCambioMedioPago
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TMediosPagoConvenio.EsCambioMedioPago: Boolean;
begin
    if Assigned(Mandante) then Result := FCambioMedioPago or Mandante.ModificoDatosPrincipales
    else Result := FCambioMedioPago;
end;

{Procedure Name	: TMediosPagoConvenio.GetConfirmado
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TMediosPagoConvenio.GetConfirmado: Boolean;
begin
    if EsCambioDatosTransBank then Result := False
    else Result := FConfirmado;
end;

{Procedure Name	: TMediosPagoConvenio.EsCambioDatosTransBank
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TMediosPagoConvenio.EsCambioDatosTransBank: Boolean;
begin
    Result := FCambioDatosTransBank;
end;

{Procedure Name	: TMediosPagoConvenio.GetForzarConfirmado
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TMediosPagoConvenio.GetForzarConfirmado: Boolean;
begin
    Result := FForzarConfirmado;
end;

{Procedure Name	: TMediosPagoConvenio.SetForzarConfirmado
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	: const Value: Boolean
 Return Value   :
}
procedure TMediosPagoConvenio.SetForzarConfirmado(const Value: Boolean);
begin
    FForzarConfirmado := Value;
end;

{Procedure Name	: TMediosPagoConvenio.EsCambioTipoMedioPago
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
 Parameters   	:
 Return Value   : Boolean
}
function TMediosPagoConvenio.EsCambioTipoMedioPago: Boolean;
begin
    Result := FCambioMedioPago;
//    Result := FCambioTipoMedioPago;
end;

procedure TDatosPersona.SetInactivaDomicilioRNUT(Const Value : boolean);
begin
    Fdatos.InactivaDomicilioRNUT := value;
end;

procedure TDatosPersona.SetInactivaMedioComunicacionRNUT(Const Value : boolean);
begin
    Fdatos.InactivaMedioComunicacionRNUT := value;
end;

procedure TObservacionConvenio.GetFechaObservacion(const Value: TDateTime);     //SS-842-NDR-20110912
begin                                                                           //SS-842-NDR-20110912
    FFechaObservacion := Value;                                                 //SS-842-NDR-20110912
end;                                                                            //SS-842-NDR-20110912

function TObservacionConvenio.SetFechaObservacion: TDateTime;                   //SS-842-NDR-20110912
begin                                                                           //SS-842-NDR-20110912
    Result := FFechaObservacion;                                                //SS-842-NDR-20110912
end;                                                                            //SS-842-NDR-20110912

procedure TObservacionConvenio.SetObservacionModificada(const Value: Boolean);  //SS_842_PDO_20111214
begin                                                                           //SS_842_PDO_20111214
    FModificada := Value;                                                       //SS_842_PDO_20111214
end;                                                                            //SS_842_PDO_20111214

function TObservacionConvenio.GetObservacionModificada: Boolean;                //SS_842_PDO_20111214
begin                                                                           //SS_842_PDO_20111214
    Result := FModificada;                                                      //SS_842_PDO_20111214
end;                                                                            //SS_842_PDO_20111214

end.




