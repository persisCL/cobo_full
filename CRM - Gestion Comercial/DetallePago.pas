unit DetallePago;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Navigator, StdCtrls, ExtCtrls, Buttons, DmiCtrls, validate,
  DbList, DB, ADODB, UtilDB, Grids, DBGrids, Util,
  DBTables, UtilProc, DMConnection, peaprocs, ComCtrls,
  BuscaClientes, peatypes, Dateedit, ListBoxEx, DBListEx;

type
  TNavWindowDetallePago = class(TNavWindowFrm)
    dsDetallePagos: TDataSource;
    ObtenerDatosPagos: TADOStoredProc;
    gb_DetallePagos: TGroupBox;
    dbgDetallePagos: TDBListEx;
	procedure FormCreate(Sender: TObject);
	procedure ActualizarBusqueda();

  private
	FCodigoCliente: integer;
	FTipoComprobante: Char;
	FNumeroComprobante: Double;

  public
	{ Public declarations }
	Procedure RefreshData; override;
	Function GetBookmark(Var Bookmark, Description: AnsiString): Boolean; override;
	Function GotoBookmark(Bookmark: AnsiString): Boolean; override;
	class Function CreateBookmark(CodigoCliente: integer;
	  TipoComprobante: ansiString; NumeroComprobante: Double; Orden: Integer): AnsiString;
	function Inicializa: Boolean; override;
  end;

var
  NavWindowDetallePago: TNavWindowDetallePago;



implementation

{$R *.dfm}

{ TNavWindowDatosCliente }



class function TNavWindowDetallePago.CreateBookmark(CodigoCliente: integer; TipoComprobante: ansiString;
  NumeroComprobante: Double; Orden: Integer): AnsiString;
begin
	Result := format('%d;%s;%f;%d',[CodigoCliente, TipoComprobante, NumeroComprobante, Orden]);
end;

function TNavWindowDetallePago.GetBookmark(var Bookmark, Description: AnsiString): Boolean;
resourcestring
    MSG_BOOKMARK = 'Detalle del comprobante %s - %f';
begin
	Bookmark:= CreateBookmark(FCodigoCliente, FTipoComprobante,	FNumeroComprobante,
	  ObtenerDatosPagos.FieldByName('Orden').asInteger);
	Description := Format(MSG_BOOKMARK, [FTipoComprobante, FNumeroComprobante]);
	Result	:= True;
end;

function TNavWindowDetallePago.GotoBookmark(Bookmark: AnsiString): Boolean;
begin
	try
		// Cargamos los datos del bookmark
		FCodigoCliente	   := IVal(ParseParamByNumber(Bookmark, 1,';'));
		FTipoComprobante   := trim(ParseParamByNumber(Bookmark, 2,';'))[1];
		FNumeroComprobante := IVal(ParseParamByNumber(Bookmark, 3,';'));
		ActualizarBusqueda();
		ObtenerDatosPagos.Locate('TipoComprobante;NumeroComprobante;Orden',
		  VarArrayOf([FTipoCOmprobante, FNumeroComprobante, IVal(ParseParamByNumber(Bookmark, 4,';'))]),[]);
		result := not ObtenerDatosPagos.isEmpty;
	except
		result := false;
	end;
end;

function TNavWindowDetallePago.Inicializa: Boolean;
begin
	Update;
	Result 		:= Inherited Inicializa ;
	with dbgDetallePagos do begin
		Columns[0].Header.Caption  := MSG_COL_TITLE_FORMA_DE_PAGO;
		Columns[1].Header.Caption  := MSG_COL_TITLE_IMPORTE;
		Columns[2].Header.Caption  := MSG_COL_TITLE_FECHA;
		Columns[3].Header.Caption  := MSG_COL_TITLE_CUPON;
		Columns[4].Header.Caption  := MSG_COL_TITLE_AUTORIZACION;
		Columns[5].Header.Caption  := MSG_COL_TITLE_CUOTAS;
		Columns[6].Header.Caption  := MSG_COL_TITLE_TURNO;
		Columns[7].Header.Caption  := MSG_COL_TITLE_VENCIMIENTO;
    end;
end;

procedure TNavWindowDetallePago.RefreshData;
begin
	inherited;
end;

procedure TNavWindowDetallePago.FormCreate(Sender: TObject);
begin
	Update;
end;


procedure TNavWindowDetallePago.ActualizarBusqueda();
Resourcestring
    MSG_DATOS_PAGO             = 'No se pudieron obtener los datos del pago.';
    CAPTION_PAGOS_COMPROBANTE  = 'Obtener Datos del Pago';
begin
	screen.Cursor := crHourGlass;
	try
		with ObtenerDatosPagos do begin
			try
				Close;
				Parameters.ParamByName('@TipoComprobante').Value	:= FTipoComprobante;
				Parameters.ParamByName('@NumeroComprobante').Value	:= FNumeroComprobante;
				Open;
			except
				on E: Exception do begin
					MsgBoxErr(MSG_DATOS_PAGO, e.Message, CAPTION_PAGOS_COMPROBANTE, MB_ICONSTOP);
					Close;
				end;
			end;
		end;
	finally
		screen.Cursor := crDefault;
	end;
end;

end.
