unit DetalleComprobante;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, utilProc,
  util, ADODB,  DMConnection, ComCtrls, navigator,
  ImgList, Grids, DBGrids, DB, reporteFactura, peaTypes,
  ReporteDetalleViajes,  TransitosPorConvenio, DPSControls,
  DBListEx, ListBoxEx;

type
  TNavWindowDetalleComprobante = class(TNavWindowFrm)
	dsDatosComprobante: TDataSource;
	ObtenerDatosComprobante: TADOStoredProc;
	dsFacturacionDetallada: TDataSource;
	ObtenerFacturacionDetallada: TADOStoredProc;
	GroupBox1: TGroupBox;
    gbViajesCuenta: TGroupBox;
    btnIMprimirComprobante: TDPSButton;
	btnImprimirDetalleViajes: TDPSButton;
    dbgDetalleComprobante: TDBListEx;
    dbgViajesFacturados: TDBListEx;
	procedure btnSalirClick(Sender: TObject);
	procedure btnIMprimirComprobanteClick(Sender: TObject);
	procedure dsDatosComprobanteDataChange(Sender: TObject; Field: TField);
	procedure btnImprimirDetalleViajesClick(Sender: TObject);
    procedure dbgViajesFacturadosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
  private
	{ Private declarations }
	FCodigoCOncesionaria, FNumeroViaje, FCodigoCuenta, FNumeroMovimiento, FCodigoCliente: Integer;
	FTipoComprobante: Char;
	FNumeroComprobante: Double;
	FImprimir: AnsiString;
	FActualizar: Boolean;
	procedure CargarComprobantes;
  public
	{ Public declarations }
	class function CreateBookmark(CodigoCliente: Integer; TipoComprobante: ansiString;
	  NumeroComprobante: Double; Imprimir: AnsiString; CodigoCuenta, NumeroMovimiento,
	  CodigoCOncesionaria, NumeroViaje: Integer): AnsiString;
	function GetBookmark(var Bookmark, Description: AnsiString): Boolean; override;
	function GotoBookmark(Bookmark: AnsiString): Boolean; override;
	function inicializa: Boolean; override;
  end;


implementation

{$R *.dfm}

{ TfrmAnularReimprimirFactura }

const
	CONCEPTO_NOTA_CREDITO = 6;
	CONCEPTO_NOTA_DEBITO = 7;

class function TNavWindowDetalleComprobante.CreateBookmark(CodigoCliente: Integer; TipoComprobante: ansiString;
	  NumeroComprobante: Double; Imprimir: AnsiString; CodigoCuenta, NumeroMovimiento,
	  CodigoConcesionaria, NumeroViaje: Integer): AnsiString;
begin
	Result := format('%d;%s;%.0f;%s;%d;%d;%d;%d',[CodigoCliente, TipoComprobante, NumeroComprobante, Imprimir,
	  CodigoCuenta, NumeroMovimiento, CodigoConcesionaria, NumeroViaje]);
end;

function TNavWindowDetalleComprobante.GetBookmark(var Bookmark, Description: AnsiString): Boolean;
resourcestring
    MSG_BOOKMARK = 'Detalle del Comprobante %s: %.0f';
var CodigoConcesionaria, NumeroViaje: Integer;
begin
	if not ObtenerFacturacionDetallada.IsEmpty then begin
		CodigoConcesionaria := ObtenerFacturacionDetallada.FieldByName('CodigoConcesionaria').asInteger;
		NumeroViaje := ObtenerFacturacionDetallada.FieldByName('NumeroViaje').asInteger;
	end else begin
		CodigoConcesionaria := 0;
		NumeroViaje := 0;
	end;
	Bookmark:= CreateBookmark(FCodigoCliente, FTipoComprobante, FNumeroComprobante, FImprimir,
	  ObtenerDatosComprobante.FieldByName('CodigoCuenta').asInteger,
	  ObtenerDatosComprobante.FieldByName('NumeroMovimiento').asInteger,
	  CodigoConcesionaria,	NumeroViaje);
	Description := Format(MSG_BOOKMARK, [FTipoComprobante, FNumeroComprobante]);
	result := true;
end;

function TNavWindowDetalleComprobante.GotoBookmark(Bookmark: AnsiString): Boolean;
begin
	if not inicializa then begin
		result := false;
		exit;
	end;

	try
		FActualizar := False;
		// Cargamos el tipo y numero de comprobante
		FCodigoCliente := IVal(ParseParamByNumber(Bookmark, 1,';'));
		FTipoCOmprobante := trim(ParseParamByNumber(Bookmark, 2,';'))[1];
		FNumeroComprobante := IVal(ParseParamByNumber(Bookmark, 3,';'));
		FImprimir := ParseParamByNumber(Bookmark, 4,';');
		FCodigoCuenta := IVal(ParseParamByNumber(Bookmark, 5,';'));
		FNumeroMovimiento := IVal(ParseParamByNumber(Bookmark, 6,';'));
		FCodigoConcesionaria := IVal(ParseParamByNumber(Bookmark, 7,';'));
		FNumeroViaje := IVal(ParseParamByNumber(Bookmark, 8,';'));
		FActualizar := True;
		CargarComprobantes;

		ObtenerDatosComprobante.Locate('CodigoCuenta;NumeroMovimiento',VarArrayOf([FCodigoCuenta, FNumeroMovimiento]),[]);
		if not ObtenerFacturacionDetallada.isEmpty then
			ObtenerFacturacionDetallada.Locate('CodigoConcesionaria;NumeroViaje',VarArrayOf([FCodigoConcesionaria, FNumeroViaje]),[]);

		result := not ObtenerDatosComprobante.IsEmpty;
	except
		result := false;
	end;
end;

function TNavWindowDetalleComprobante.inicializa: Boolean;
begin
	btnImprimirComprobante.Enabled := false;
	btnImprimirDetalleViajes.Enabled := false;

	Width  := GetFormClientSize(Application.MainForm).cx;
	Height := GetFormClientSize(Application.MainForm).cy;
	CenterForm(Self);
	Result := True;
end;


procedure TNavWindowDetalleComprobante.CargarComprobantes;
begin
	if not FActualizar then exit;

	with dbgDetalleComprobante do begin
		Columns[0].Header.Caption  := MSG_COL_TITLE_CONCESIONARIA;
		Columns[1].Header.Caption  := MSG_COL_TITLE_FECHA;
		Columns[2].Header.Caption  := MSG_COL_TITLE_CODIGO_CUENTA;
		Columns[3].Header.Caption  := MSG_COL_TITLE_DESCRIPCION;
		Columns[4].Header.Caption  := MSG_COL_TITLE_IMPORTE;
	end;

	with dbgViajesFacturados do begin
		Columns[0].Header.Caption  := MSG_COL_TITLE_CONCESIONARIA;
		Columns[1].Header.Caption  := MSG_COL_TITLE_VIAJE;
		Columns[2].Header.Caption  := MSG_COL_TITLE_PATENTE;
		Columns[3].Header.Caption  := MSG_COL_TITLE_CATEGORIA;
		Columns[4].Header.Caption  := MSG_COL_TITLE_FECHA_HORA_DE_INICIO;
		Columns[5].Header.Caption  := MSG_COL_TITLE_PUNTOS_DE_COBRO;
		Columns[6].Header.Caption  := MSG_COL_TITLE_IMPORTE
	end;

// Cargamos la informacion del comprobante
	screen.Cursor := crHourGlass;
	try
		ObtenerDatosComprobante.close;
		ObtenerDatosComprobante.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
		ObtenerDatosComprobante.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
		ObtenerDatosComprobante.open;
		btnImprimirComprobante.enabled :=
		  (FTipoComprobante = TC_FACTURA) and (UpperCase(FImprimir) = 'TRUE');
	finally
		screen.Cursor := crDefault;
	end;
end;

procedure TNavWindowDetalleComprobante.btnSalirClick(Sender: TObject);
begin
	Close;
end;


procedure TNavWindowDetalleComprobante.btnImprimirComprobanteClick(
  Sender: TObject);
var F: TFormReporteFactura;
begin
	Application.createForm(TFormReporteFactura,F);
//	F.Execute(FTipoComprobante, FCodigoCliente, FNumeroComprobante, true, MSG_COPIA_DEMOSTRACION);
	F.free;
end;


procedure TNavWindowDetalleComprobante.dsDatosComprobanteDataChange(
  Sender: TObject; Field: TField);
resourcestring
    CAPTION_VIAJES = 'Viajes facturados';
    CAPTION_VIAJES_CUENTA = 'Viajes facturados a la cuenta: (%d)';
begin
	if Field = nil then begin
		screen.cursor := crHourGlass;
		try
			ObtenerFacturacionDetallada.Close;
			gbViajesCuenta.Caption := CAPTION_VIAJES;
			if (not ObtenerDatosComprobante.Eof)
			  and (ObtenerDatosComprobante.FieldByName('TipoComprobante').asString = TC_FACTURA) then Begin
				 with ObtenerFacturacionDetallada do begin
					 Parameters.ParamByName('@CodigoCuenta').Value := ObtenerDatosComprobante.FieldByName('CodigoCuenta').asInteger;
					 Parameters.ParamByName('@TipoComprobante').Value := ObtenerDatosComprobante.FieldByName('TipoComprobante').asString[1];
					 Parameters.ParamByName('@NumeroComprobante').Value := ObtenerDatosComprobante.FieldByName('NumeroComprobante').asFloat;
					 Open;
					 gbViajesCuenta.Caption := format(CAPTION_VIAJES_CUENTA, [ObtenerDatosComprobante.FieldByName('CodigoCuenta').asInteger]);
				end;
			end;
			btnImprimirDetalleViajes.enabled := not ObtenerFacturacionDetallada.isEmpty;
		finally
			screen.cursor := crDefault;
		end;
	end;
end;

procedure TNavWindowDetalleComprobante.btnImprimirDetalleViajesClick(
  Sender: TObject);
var F: TfrmDetalleViajes;
begin
	Application.CreateForm(TfrmDetalleViajes, F);
	F.Execute(FTipoComprobante, ObtenerDatosComprobante.fieldByName('CodigoCuenta').asInteger,
	  FCodigoCliente, FNumeroComprobante, True, MSG_DETALLE_VIAJES);
	F.free;
end;

procedure TNavWindowDetalleComprobante.dbgViajesFacturadosLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
{	if (Column.FieldName = 'NumeroViaje') and (not ObtenerFacturacionDetallada.isEmpty) then
		Navigator.JumpTo(TNavWindowDetalleViaje,
		  TNavWindowDetalleViaje.CreateBookMark(
		  ObtenerFacturacionDetallada.FieldByName('InfraccionSaldoInsuficiente').asBoolean,
		  ObtenerFacturacionDetallada.FieldByName('InfraccionTAGInhabilitado').asBoolean,
		  FCodigoCliente,
		  ObtenerFacturacionDetallada.FieldByName('CodigoConcesionaria').asInteger,
		  ObtenerFacturacionDetallada.FieldByName('NumeroViaje').asInteger,
		  ObtenerFacturacionDetallada.FieldByName('Patente').asString,
		  ObtenerFacturacionDetallada.FieldByName('Concesionaria').asString,
		  ObtenerFacturacionDetallada.FieldByName('ContractSerialNumber').asFloat,
		  ObtenerFacturacionDetallada.FieldByName('FechaHoraInicio').asDateTime,
		  0,
		  ObtenerFacturacionDetallada.FieldByName('PuntosCobro').asString,
		  ObtenerFacturacionDetallada.FieldByName('Importe').asFloat));
          }
end;

end.
