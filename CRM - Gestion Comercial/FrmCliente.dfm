object NavWindowCliente: TNavWindowCliente
  Left = 70
  Top = 50
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  AutoScroll = False
  Caption = 'Datos del Cliente'
  ClientHeight = 636
  ClientWidth = 874
  Color = 16776699
  Constraints.MinHeight = 492
  Constraints.MinWidth = 652
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 2
    Top = 15
    Width = 870
    Height = 621
    Align = alClient
    BevelOuter = bvNone
    Color = 16776699
    TabOrder = 1
    DesignSize = (
      870
      621)
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1084
      Height = 172
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 3
      object lblNombrePersona: TLabel
        Left = 13
        Top = 41
        Width = 40
        Height = 13
        Caption = 'Nombre:'
        Color = 16776699
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Label5: TLabel
        Left = 13
        Top = 60
        Width = 45
        Height = 13
        Caption = 'Domicilio:'
        Color = 16776699
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Label12: TLabel
        Left = 13
        Top = 154
        Width = 32
        Height = 13
        Caption = 'E-Mail:'
      end
      object Label6: TLabel
        Left = 13
        Top = 101
        Width = 64
        Height = 13
        Caption = 'TE Particular:'
      end
      object Label13: TLabel
        Left = 13
        Top = 136
        Width = 66
        Height = 13
        Caption = 'TE Comercial:'
      end
      object Label14: TLabel
        Left = 13
        Top = 118
        Width = 45
        Height = 13
        Caption = 'TE M'#243'vil:'
      end
      object Label7: TLabel
        Left = 399
        Top = 12
        Width = 31
        Height = 13
        Caption = 'Viajes:'
      end
      object Label8: TLabel
        Left = 399
        Top = 29
        Width = 46
        Height = 13
        Caption = 'Intereses:'
      end
      object Label11: TLabel
        Left = 399
        Top = 47
        Width = 51
        Height = 13
        Caption = 'Impuestos:'
      end
      object Label15: TLabel
        Left = 399
        Top = 64
        Width = 82
        Height = 13
        Caption = 'Otros Conceptos:'
      end
      object label9: TLabel
        Left = 399
        Top = 120
        Width = 99
        Height = 13
        Caption = 'Deuda Sin Financiar:'
      end
      object label16: TLabel
        Left = 399
        Top = 140
        Width = 90
        Height = 13
        Caption = 'Deuda Financiada:'
      end
      object Label3: TLabel
        Left = 399
        Top = 82
        Width = 60
        Height = 13
        Caption = 'Descuentos:'
      end
      object label17: TLabel
        Left = 399
        Top = 159
        Width = 75
        Height = 13
        Caption = 'Total Deuda:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_Viajes: TLabel
        Left = 513
        Top = 12
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
      end
      object txt_Intereses: TLabel
        Left = 513
        Top = 29
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
      end
      object txt_Impuestos: TLabel
        Left = 513
        Top = 47
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
      end
      object txt_OtrosConceptos: TLabel
        Left = 513
        Top = 64
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
      end
      object txt_DeudaSINFinanciar: TLabel
        Left = 513
        Top = 120
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
      end
      object txt_DeudaFinanciada: TLabel
        Left = 513
        Top = 140
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
      end
      object txt_Descuentos: TLabel
        Left = 513
        Top = 82
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
      end
      object txt_TotalDeuda_2: TLabel
        Left = 497
        Top = 159
        Width = 107
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Bevel1: TBevel
        Left = 387
        Top = 149
        Width = 210
        Height = 8
        Shape = bsBottomLine
      end
      object Bevel2: TBevel
        Left = 387
        Top = 112
        Width = 210
        Height = 8
        Shape = bsBottomLine
      end
      object Bevel3: TBevel
        Left = 386
        Top = 91
        Width = 210
        Height = 8
        Shape = bsBottomLine
      end
      object Bevel4: TBevel
        Left = 386
        Top = 4
        Width = 210
        Height = 8
        Shape = bsBottomLine
      end
      object label1: TLabel
        Left = 399
        Top = 99
        Width = 75
        Height = 13
        Caption = 'Total Deuda:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_TotalDeuda_1: TLabel
        Left = 513
        Top = 99
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 13
        Top = 14
        Width = 58
        Height = 13
        Caption = 'Documento:'
        Color = 16776699
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Bevel5: TBevel
        Left = 11
        Top = 30
        Width = 361
        Height = 8
        Shape = bsBottomLine
      end
      object lApellidoNombre: TLabel
        Left = 95
        Top = 41
        Width = 263
        Height = 13
        AutoSize = False
      end
      object lDomicilio: TLabel
        Left = 95
        Top = 59
        Width = 263
        Height = 28
        AutoSize = False
        WordWrap = True
      end
      object lEMail: TLabel
        Left = 95
        Top = 154
        Width = 189
        Height = 13
        AutoSize = False
      end
      object lTEParticular: TLabel
        Left = 95
        Top = 101
        Width = 266
        Height = 13
        AutoSize = False
      end
      object lTEMovil: TLabel
        Left = 95
        Top = 118
        Width = 266
        Height = 13
        AutoSize = False
      end
      object lTEComercial: TLabel
        Left = 95
        Top = 136
        Width = 189
        Height = 13
        AutoSize = False
      end
      object Bevel6: TBevel
        Left = 11
        Top = 91
        Width = 361
        Height = 8
        Shape = bsBottomLine
      end
      object Label18: TLabel
        Left = 625
        Top = 23
        Width = 52
        Height = 13
        Caption = 'Morosidad:'
      end
      object GauMorosidad: TGauge
        Left = 705
        Top = 24
        Width = 104
        Height = 14
        Color = clSkyBlue
        ForeColor = clSkyBlue
        ParentColor = False
        ParentShowHint = False
        Progress = 0
        ShowHint = False
      end
      object Bevel7: TBevel
        Left = 616
        Top = 4
        Width = 201
        Height = 8
        Shape = bsBottomLine
      end
      object Label19: TLabel
        Left = 625
        Top = 111
        Width = 51
        Height = 13
        Caption = 'Contactos:'
      end
      object GauContactos: TGauge
        Left = 705
        Top = 111
        Width = 104
        Height = 14
        Color = clSkyBlue
        ForeColor = clSkyBlue
        ParentColor = False
        ParentShowHint = False
        Progress = 0
        ShowHint = False
      end
      object Label20: TLabel
        Left = 625
        Top = 133
        Width = 56
        Height = 13
        Caption = 'Efectividad:'
      end
      object GauEfectividad: TGauge
        Left = 705
        Top = 133
        Width = 104
        Height = 14
        Color = clSkyBlue
        ForeColor = clSkyBlue
        ParentColor = False
        ParentShowHint = False
        Progress = 0
        ShowHint = False
      end
      object Label21: TLabel
        Left = 625
        Top = 67
        Width = 61
        Height = 13
        Caption = 'Infracciones:'
      end
      object GauInfracciones: TGauge
        Left = 705
        Top = 67
        Width = 104
        Height = 14
        Color = clSkyBlue
        ForeColor = clSkyBlue
        ParentColor = False
        ParentShowHint = False
        Progress = 0
        ShowHint = False
      end
      object Label22: TLabel
        Left = 625
        Top = 89
        Width = 31
        Height = 13
        Caption = 'Viajes:'
      end
      object GauViajes: TGauge
        Left = 705
        Top = 89
        Width = 104
        Height = 14
        Color = clSkyBlue
        ForeColor = clSkyBlue
        ParentColor = False
        ParentShowHint = False
        Progress = 0
        ShowHint = False
      end
      object Label23: TLabel
        Left = 625
        Top = 45
        Width = 73
        Height = 13
        Caption = 'Regularizaci'#243'n:'
      end
      object GauRegularizacion: TGauge
        Left = 705
        Top = 45
        Width = 104
        Height = 14
        Color = clSkyBlue
        ForeColor = clSkyBlue
        ParentColor = False
        ParentShowHint = False
        Progress = 0
        ShowHint = False
      end
      object Label24: TLabel
        Left = 625
        Top = 156
        Width = 47
        Height = 13
        Caption = 'Consumo:'
      end
      object gauConsumo: TGauge
        Left = 705
        Top = 155
        Width = 104
        Height = 14
        Color = clSkyBlue
        ForeColor = clSkyBlue
        ParentColor = False
        ParentShowHint = False
        Progress = 0
        ShowHint = False
      end
      object mcTipoNumeroDocumento: TMaskCombo
        Left = 77
        Top = 10
        Width = 213
        Height = 21
        OnChange = mcTipoNumeroDocumentoChange
        Items = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Color = clWindow
        ComboWidth = 100
        ItemIndex = -1
        Enabled = False
        OmitCharacterRequired = False
        TabOrder = 0
      end
      object btnBuscar: TDPSButton
        Left = 300
        Top = 7
        Width = 65
        Caption = '&Buscar...'
        TabOrder = 1
        OnClick = btnBuscarClick
      end
      object btnEditar: TDPSButton
        Left = 300
        Top = 144
        Width = 65
        Caption = '&Editar...'
        TabOrder = 2
        OnClick = btnEditarClick
      end
    end
    object PageControl: TDPSPageControl
      Left = -2
      Top = 307
      Width = 875
      Height = 317
      ActivePage = ts_Contactos
      Anchors = [akLeft, akTop, akRight, akBottom]
      OwnerDraw = True
      TabIndex = 2
      TabOrder = 0
      TabWidth = 90
      OnChange = PageControlChange
      OnDrawTab = PageControlDrawTab
      PageColor = clWhite
      Color = 14732467
      object ts_Inicio: TTabSheet
        Caption = 'Inicio'
        DesignSize = (
          867
          289)
        object DPSGridInicio: TDBListEx
          Left = 5
          Top = 5
          Width = 858
          Height = 280
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 230
              Header.Caption = 'Evento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Evento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 97
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFecha'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 123
              Header.Caption = 'Id. Evento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'SubEvento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 91
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescripcionEstado'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 117
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporte'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 151
              Header.Caption = 'Observaciones'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Observaciones'
            end>
          DataSource = dsResumenCliente
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
        end
      end
      object ts_cartola: TTabSheet
        Caption = 'Cartola'
        ImageIndex = 1
        DesignSize = (
          867
          289)
        object bdgResumenMovimientos: TDBListEx
          Left = 4
          Top = 5
          Width = 858
          Height = 279
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 116
              Header.Caption = 'Movimiento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Movimiento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 80
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriTipoComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 79
              Header.Caption = 'N'#250'mero'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsUnderline]
              Width = 58
              Header.Caption = 'Det.Mov.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = True
              FieldName = 'Ver'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 78
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFechaComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'Vencimiento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriVencimientoComprobante'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 106
              Header.Caption = 'Deuda Anterior'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriDeudaAnterior'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 74
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteComprobante'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 81
              Header.Caption = 'Deuda Actual'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriDeudaActual'
            end>
          DataSource = dsResumenMovimientosCliente
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnLinkClick = bdgResumenMovimientosLinkClick
        end
      end
      object ts_Contactos: TTabSheet
        Caption = 'Contactos'
        ImageIndex = 2
        DesignSize = (
          867
          289)
        object dg_OrdenesServicio: TDBListEx
          Left = 8
          Top = 8
          Width = 849
          Height = 273
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'Comunicaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'CodigoComunicacion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsUnderline]
              Width = 60
              Header.Caption = 'Ord. Srv.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = True
              FieldName = 'Ver'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Inicio Comunicaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'FechaHoraInicio'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Usuario'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NombreCompleto'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 170
              Header.Caption = 'Tiempo Restante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 130
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescripcionEstado'
            end>
          DataSource = ds_Comunicaciones
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnCheckLink = dg_OrdenesServicioCheckLink
          OnDrawText = dg_OrdenesServicioDrawText
          OnLinkClick = dg_OrdenesServicioLinkClick
        end
      end
      object ts_Viajes: TTabSheet
        Caption = 'Viajes'
        ImageIndex = 3
        DesignSize = (
          867
          289)
        object dbgViajes: TDBListEx
          Left = 5
          Top = 5
          Width = 857
          Height = 280
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 108
              Header.Caption = 'Momento de Inicio'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'FechaHoraInicio'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsUnderline]
              Width = 70
              Header.Caption = 'Det. Viaje'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = True
              FieldName = 'Ver'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 123
              Header.Caption = 'Concesionaria'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriConcesionaria'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 106
              Header.Caption = 'Puntos Recorridos'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'PuntosCobro'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 90
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporte'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 70
              Header.Caption = 'Patente'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Patente'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Cuenta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescripcionCuenta'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 56
              Header.Caption = 'Kms.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'KilometrosRecorridos'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 82
              Header.Caption = 'Fecha Fact.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFechaFacturacion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Tipo Comp.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'TipoComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Nro. Comp.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroComprobante'
            end>
          DataSource = dsViajes
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnLinkClick = dbgViajesLinkClick
        end
      end
      object ts_Facturas: TTabSheet
        Caption = 'Facturas'
        ImageIndex = 4
        DesignSize = (
          867
          289)
        object dbgListaFacturas: TDBListEx
          Left = 5
          Top = 5
          Width = 857
          Height = 280
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 46
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriEstado'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 63
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFechaHora'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 50
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescripcionTipoComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 56
              Header.Caption = 'N'#250'mero'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 71
              Header.Caption = 'Vencimiento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriVencimiento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsUnderline]
              Width = 56
              Header.Caption = 'Det.Cte.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = True
              FieldName = 'Ver'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 68
              Header.Caption = 'Impresi'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFechaHoraImpreso'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Width = 76
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporte'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Viajes'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteViajes'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 58
              Header.Caption = 'Servicios'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteServicios'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 56
              Header.Caption = 'Intereses'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteIntereses'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Impuestos'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteImpuestos'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 65
              Header.Caption = 'Convenios'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteCuotaConvenio'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 90
              Header.Caption = 'Otros Conceptos'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteOtrosConceptos'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 62
              Header.Caption = 'Descuentos'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriDescuentos'
            end>
          DataSource = dsFacturas
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnLinkClick = dbgListaFacturasLinkClick
        end
      end
      object ts_Pagos: TTabSheet
        Caption = 'Pagos'
        ImageIndex = 5
        DesignSize = (
          867
          289)
        object dbgPagos: TDBListEx
          Left = -1
          Top = 8
          Width = 857
          Height = 280
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 58
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriTipoComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 83
              Header.Caption = 'N'#250'mero'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsUnderline]
              Width = 61
              Header.Caption = 'Det.Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = True
              FieldName = 'Ver'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 72
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 69
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFechaPago'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 103
              Header.Caption = 'Lugar de Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'LugarDePago'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 99
              Header.Caption = 'Deuda Anterior'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriDeudaAnterior'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 99
              Header.Caption = 'Deuda Posterior'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriDeudaPosterior'
            end>
          DataSource = dsPagosComprobantes
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnLinkClick = dbgPagosLinkClick
        end
      end
      object ts_refacturacion: TTabSheet
        Caption = 'Refacturaci'#243'n'
        ImageIndex = 6
        DesignSize = (
          867
          289)
        object dbgRefacturacion: TDBListEx
          Left = 5
          Top = 5
          Width = 857
          Height = 280
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 46
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriEstado'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 63
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFechaHora'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 65
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescripcionTipoComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 78
              Header.Caption = 'N'#250'mero'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsUnderline]
              Width = 55
              Header.Caption = 'Det.Cte.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = True
              FieldName = 'Ver'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 112
              Header.Caption = 'Cte. Ajustado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriComprobanteAjustado'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 72
              Header.Caption = 'Vencimiento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriVencimiento'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Width = 76
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporte'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Viajes'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteViajes'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 58
              Header.Caption = 'Servicios'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteServicios'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 56
              Header.Caption = 'Intereses'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteIntereses'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 65
              Header.Caption = 'Impuestos'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteImpuestos'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 65
              Header.Caption = 'Convenios'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteCuotaConvenio'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 90
              Header.Caption = 'Otros Conceptos'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteCuotaConvenio'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 62
              Header.Caption = 'Descuentos'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriDescuentos'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
            end>
          DataSource = dsRefacturacion
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnLinkClick = dbgRefacturacionLinkClick
        end
      end
      object ts_financiamiento: TTabSheet
        Caption = 'Financiamiento'
        ImageIndex = 7
        DesignSize = (
          867
          289)
        object dbgFinanciamientos: TDBListEx
          Left = 5
          Top = 5
          Width = 857
          Height = 280
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 58
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Estado'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 69
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFechaAlta'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 184
              Header.Caption = 'Cuenta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriCuentas'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 105
              Header.Caption = 'Importe Financiado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteAFinanciar'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 81
              Header.Caption = 'Pie Entregado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriPieEntregado'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 69
              Header.Caption = 'Inter'#233's'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Interes'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 83
              Header.Caption = 'Vigencia'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFechaVigencia'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 99
              Header.Caption = 'Importe de Cuota'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriImporteCuota'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 52
              Header.Caption = 'Cuotas'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'CantidadCuotas'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 86
              Header.Caption = 'Cuotas Impagas'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'CantidadCuotasImpagas'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 86
              Header.Caption = 'Cuotas Pagas'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'CantidadCuotasPagas'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 110
              Header.Caption = 'Pr'#243'xima Facturaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriProximaFacturacion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 106
              Header.Caption = 'Pr'#243'ximo Vencimiento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriProximoVencimiento'
            end>
          DataSource = dsFinanciamientos
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
        end
      end
      object ts_infracciones: TTabSheet
        Caption = 'Infracciones'
        ImageIndex = 8
        DesignSize = (
          867
          289)
        object dbgInfracciones: TDBListEx
          Left = 2
          Top = 5
          Width = 856
          Height = 280
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 122
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Estado'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 96
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFechaAlta'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsUnderline]
              Width = 50
              Header.Caption = 'Det Inf.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = True
              FieldName = 'Ver'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 233
              Header.Caption = 'Cuenta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescripcionCuenta'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 273
              Header.Caption = 'Descripci'#243'n Infracci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriMotivoInfraccion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 106
              Header.Caption = 'Expediente'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroExpediente'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 102
              Header.Caption = 'Concesionaria'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescripcionConcesionaria'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Tipo Comp.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriTipoComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Nro. Comp.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroComprobante'
            end>
          DataSource = dsInfracciones
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnLinkClick = dbgInfraccionesLinkClick
        end
      end
      object ts_tag: TTabSheet
        Caption = 'TAG'
        ImageIndex = 9
        DesignSize = (
          867
          289)
        object dpgTag: TDBListEx
          Left = 5
          Top = 5
          Width = 856
          Height = 280
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 90
              Header.Caption = 'N'#250'mero de Tag'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'ContractSerialNumber'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 70
              Header.Caption = 'Patente'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Patente'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 103
              Header.Caption = 'Fecha Asignaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFechaAsignacion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 148
              Header.Caption = 'Categor'#237'a'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Categoria'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 134
              Header.Caption = 'Ubicaci'#243'n Anterior'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'UbicacionAnterior'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 89
              Header.Caption = 'Proveedor'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Proveedor'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 85
              Header.Caption = 'Fecha Ingreso'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriFechaIngreso'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 83
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Estado'
            end>
          DataSource = dsTags
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
        end
      end
    end
    object Panel13: TPanel
      Left = -4
      Top = 174
      Width = 1087
      Height = 4
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      Color = 14732467
      TabOrder = 1
    end
    object Panel10: TPanel
      Left = 0
      Top = 303
      Width = 1082
      Height = 4
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      Color = 14732467
      TabOrder = 2
    end
    object dg_Cuentas: TDBListEx
      Left = 5
      Top = 197
      Width = 860
      Height = 103
      Anchors = [akLeft, akTop, akRight]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          Width = 76
          Header.Caption = 'Cuenta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = True
          FieldName = 'CodigoCuenta'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Width = 89
          Header.Caption = 'Saldo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriSaldo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 187
          Header.Caption = 'Forma de Pago'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripcionPago'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 95
          Header.Caption = 'Plan Comercial'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriPlanComercial'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 70
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 199
          Header.Caption = 'Veh'#237'culo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripcionVehiculo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Fecha Alta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriFechaAlta'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 84
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriEstadoCuenta'
        end>
      DataSource = ds_Cuentas
      DragReorder = True
      ParentColor = False
      TabOrder = 4
      TabStop = True
      OnLinkClick = dg_CuentasLinkClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 874
    Height = 15
    Align = alTop
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 0
    DesignSize = (
      874
      15)
    object Image1: TImage
      Left = 0
      Top = 0
      Width = 15
      Height = 15
      Align = alLeft
      AutoSize = True
      Picture.Data = {
        07544269746D6170AA010000424DAA01000000000000BA000000280000000F00
        00000F0000000100080000000000F0000000232E0000232E0000210000000000
        0000D8CAB100D8CBB300D9CCB400DACDB600DACDB700DACEB700DBCFB800DBCF
        B900DCCFB900DCD0B900DCD0BA00DDD1BC00DDD2BC00DED2BD00DFD3BF00E0D5
        C100E0D5C200E0D6C300E7DED000E8E1D200EBE5D900F1ECE400F4F0E900F5F2
        EC00FAF8F500FBF9F700FBFAF800FCFBF900FCFCFA00FDFDFC00FEFDFD00FEFE
        FE00FFFFFF000F0D1B202020202020202020202020000A051C20202020202020
        2020202020000A051F202020202020202020202020000A031E20202020202020
        2020202020000A0419202020202020202020202020000A021620202020202020
        2020202020000A0012202020202020202020202020000A000C20202020202020
        2020202020000A0002152020202020202020202020000A0000081D2020202020
        2020202020000A0000000918202020202020202020000A000000000A14202020
        2020202020000A0000000000020613171A1F202020000A000000000000000001
        04060A0B0E00100A0A0A0A0A0A0A0A0A0A0A07071100}
    end
    object Image2: TImage
      Left = 859
      Top = 0
      Width = 15
      Height = 15
      Align = alRight
      AutoSize = True
      Picture.Data = {
        07544269746D617026050000424D260500000000000036040000280000000F00
        00000F0000000100080000000000F0000000C40E0000C40E0000000100000000
        0000D8CAB100D8CBB300D9CCB400DACDB600DACDB700DACEB700DBCFB800DBCF
        B900DCCFB900DCD0B900DCD0BA00DDD1BC00DDD2BC00DED2BD00DFD3BF00E0D5
        C100E0D5C200E0D6C300E7DED000E8E1D200EBE5D900F1ECE400F4F0E900F5F2
        EC00FAF8F500FBF9F700FBFAF800FCFBF900FCFCFA00FDFDFC00FEFDFD00FEFE
        FE00FFFFFF000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00002020202020202020202020201B0D0F002020202020202020202020201C05
        0A002020202020202020202020201F050A002020202020202020202020201E03
        0A0020202020202020202020202019040A002020202020202020202020201602
        0A0020202020202020202020202012000A002020202020202020202020200C00
        0A0020202020202020202020201502000A00202020202020202020201D080000
        0A0020202020202020202018090000000A002020202020202020140A00000000
        0A002020201F1A1713060200000000000A000E0B0A0604010000000000000000
        0A001107070A0A0A0A0A0A0A0A0A0A0A1000}
    end
    object Panel4: TPanel
      Left = 15
      Top = 0
      Width = 846
      Height = 17
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 846
        Height = 2
        Align = alTop
        BevelOuter = bvNone
        Color = 14732467
        TabOrder = 0
      end
    end
  end
  object Panel8: TPanel
    Left = 872
    Top = 15
    Width = 2
    Height = 621
    Align = alRight
    BevelOuter = bvNone
    Color = 14732467
    TabOrder = 2
  end
  object Panel7: TPanel
    Left = 0
    Top = 15
    Width = 2
    Height = 621
    Align = alLeft
    BevelOuter = bvNone
    Color = 14732467
    TabOrder = 4
  end
  object Panel11: TPanel
    Left = 390
    Top = 5
    Width = 163
    Height = 16
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = 'Resumen de Deuda'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = True
    ParentFont = False
    TabOrder = 5
  end
  object Panel6: TPanel
    Left = 14
    Top = 5
    Width = 195
    Height = 16
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = 'Datos Personales'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
  end
  object Panel12: TPanel
    Left = 616
    Top = 5
    Width = 106
    Height = 16
    BevelOuter = bvNone
    Caption = 'Indicadores'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = True
    ParentFont = False
    TabOrder = 6
  end
  object pnlCuentas: TPanel
    Left = 6
    Top = 193
    Width = 819
    Height = 18
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = 'Cuentas'
    Color = 16776699
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
  end
  object ObtenerComunicaciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerComunicaciones;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DesdeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 208
    Top = 416
  end
  object ds_Cuentas: TDataSource
    DataSet = ObtenerDetallesCuentasCliente
    Left = 124
    Top = 260
  end
  object ObtenerDetallesCuentasCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDetallesCuentasCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 160
    Top = 260
  end
  object ds_Comunicaciones: TDataSource
    DataSet = ObtenerComunicaciones
    Left = 176
    Top = 416
  end
  object ObtenerFinanciamientos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerFinanciamientos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DesdeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EstadoFinanciamiento'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 528
    Top = 416
  end
  object dsFinanciamientos: TDataSource
    DataSet = ObtenerFinanciamientos
    Left = 496
    Top = 416
  end
  object ObtenerListaComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerListaComprobantes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Vencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 336
    Top = 416
  end
  object dsFacturas: TDataSource
    DataSet = ObtenerListaComprobantes
    Left = 304
    Top = 416
  end
  object ObtenerRefacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerListaComprobantes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Vencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 464
    Top = 416
  end
  object dsRefacturacion: TDataSource
    DataSet = ObtenerRefacturacion
    Left = 432
    Top = 416
  end
  object ObtenerResumenInfracciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerResumenInfracciones;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SoloInfraccionesConfirmadas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@FechaInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 592
    Top = 416
  end
  object dsInfracciones: TDataSource
    DataSet = ObtenerResumenInfracciones
    Left = 560
    Top = 416
  end
  object dsViajes: TDataSource
    DataSet = ObtenerViajesCliente
    Left = 240
    Top = 416
  end
  object ObtenerViajesCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerViajesCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 272
    Top = 416
  end
  object ObtenerResumenMovimientosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerResumenMovimientosCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DesdeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 144
    Top = 416
  end
  object dsResumenMovimientosCliente: TDataSource
    DataSet = ObtenerResumenMovimientosCliente
    Left = 112
    Top = 416
  end
  object dsPagosComprobantes: TDataSource
    DataSet = ObtenerPagosComprobantes
    Left = 368
    Top = 416
  end
  object ObtenerPagosComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerPagosComprobantes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DesdeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 400
    Top = 416
  end
  object dsResumenCliente: TDataSource
    DataSet = ObtenerResumenCliente
    Left = 48
    Top = 416
  end
  object ObtenerResumenCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerResumenCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DesdeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 80
    Top = 416
  end
  object dsTags: TDataSource
    DataSet = ObtenerTags
    Left = 624
    Top = 416
  end
  object ObtenerTags: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTags;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 656
    Top = 416
  end
  object ObtenerPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 136
    Top = 112
  end
  object Timer: TTimer
    Interval = 30000
    OnTimer = TimerTimer
    Left = 688
    Top = 416
  end
  object ObtenerDeudaCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDeudaCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Viajes'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 6
        Precision = 12
        Value = Null
      end
      item
        Name = '@Intereses'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 6
        Precision = 12
        Value = Null
      end
      item
        Name = '@Impuestos'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 6
        Precision = 12
        Value = Null
      end
      item
        Name = '@OtrosConceptos'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 6
        Precision = 12
        Value = Null
      end
      item
        Name = '@FinanCaducados'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 6
        Precision = 12
        Value = Null
      end
      item
        Name = '@Descuentos'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 6
        Precision = 12
        Value = Null
      end
      item
        Name = '@DeudaTotal'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 6
        Precision = 12
        Value = Null
      end
      item
        Name = '@DeudaFinanciada'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 6
        Precision = 12
        Value = Null
      end
      item
        Name = '@DeudaSINFinanciar'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 6
        Precision = 12
        Value = Null
      end>
    Left = 168
    Top = 112
  end
  object ObtenerEstadoCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerEstadoCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadDias'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceViaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ComprobantesVencidos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@OSPendientes'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@FinanACaducar'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@InfNOConfirmadas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@TAGsInhabilitados'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@Morosidad'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@Regularizacion'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@Infracciones'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@Viajes'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@Contactos'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@Efectividad'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@Consumo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end>
    Left = 720
    Top = 416
  end
end
