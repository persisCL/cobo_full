object FormSCDatosVehiculo: TFormSCDatosVehiculo
  Left = 260
  Top = 0
  AutoSize = True
  BorderStyle = bsDialog
  Caption = 'Datos del Veh'#237'culo'
  ClientHeight = 969
  ClientWidth = 439
  Color = clBtnFace
  Constraints.MaxHeight = 1000
  Constraints.MaxWidth = 445
  Constraints.MinWidth = 445
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox: TGroupBox
    AlignWithMargins = True
    Left = 0
    Top = 3
    Width = 439
    Height = 302
    Margins.Left = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Caption = ' Datos del Veh'#237'culo '
    TabOrder = 0
    inline FreDatosVehiculo: TFmeSCDatosVehiculo
      Left = 2
      Top = 15
      Width = 435
      Height = 285
      HorzScrollBar.Visible = False
      VertScrollBar.Visible = False
      Align = alClient
      TabOrder = 0
      TabStop = True
      ExplicitLeft = 2
      ExplicitTop = 15
      ExplicitWidth = 435
      ExplicitHeight = 285
      inherited lblTelevia: TLabel
        Top = 7
        ExplicitTop = 7
      end
      inherited lblVehiculoRobado: TLabel
        Width = 261
        ExplicitWidth = 249
      end
      inherited cb_marca: TComboBox
        Left = 100
        ExplicitLeft = 100
      end
      inherited txt_anio: TNumericEdit
        Left = 100
        ExplicitLeft = 100
      end
      inherited txtDescripcionModelo: TEdit
        Left = 100
        ExplicitLeft = 100
      end
      inherited txtPatente: TEdit
        Left = 100
        OnChange = FreDatosVehiculotxtPatenteChange
        ExplicitLeft = 100
      end
      inherited cb_color: TComboBox
        Left = 100
        ExplicitLeft = 100
      end
      inherited GBListas: TGroupBox
        Left = 3
        Top = 230
        Width = 429
        ExplicitLeft = 3
        ExplicitTop = 230
        ExplicitWidth = 429
      end
      inherited cbbCategoriaInterurbana: TVariantComboBox
        Left = 100
        ExplicitLeft = 100
      end
      inherited cbbCategoriaUrbana: TVariantComboBox
        Left = 100
        ExplicitLeft = 100
      end
      inherited txtNumeroTelevia: TEdit
        Top = 4
        ExplicitTop = 4
      end
    end
  end
  object GroupBoxNuevo: TGroupBox
    Left = 0
    Top = 574
    Width = 439
    Height = 299
    Align = alTop
    Caption = ' Nuevo Veh'#237'culo '
    TabOrder = 3
    inline FreDatosVehiculoNuevo: TFmeSCDatosVehiculo
      Left = 2
      Top = 15
      Width = 435
      Height = 282
      HorzScrollBar.Visible = False
      VertScrollBar.Visible = False
      Align = alClient
      TabOrder = 0
      TabStop = True
      ExplicitLeft = 2
      ExplicitTop = 15
      ExplicitWidth = 435
      ExplicitHeight = 282
      inherited Label14: TLabel
        Top = 132
        ExplicitTop = 132
      end
      inherited Label20: TLabel
        Top = 156
        ExplicitTop = 156
      end
      inherited Label47: TLabel
        Top = 203
        ExplicitTop = 203
      end
      inherited lblTelevia: TLabel
        Top = 9
        ExplicitTop = 9
      end
      inherited Label21: TLabel
        Top = 179
        ExplicitTop = 179
      end
      inherited lblVehiculoRobado: TLabel
        Width = 261
        ExplicitWidth = 249
      end
      inherited lblCategoriaInterurbana: TLabel
        Top = 56
        ExplicitTop = 56
      end
      inherited cb_marca: TComboBox
        Top = 128
        ExplicitTop = 128
      end
      inherited txt_anio: TNumericEdit
        Top = 199
        ExplicitTop = 199
      end
      inherited txtDescripcionModelo: TEdit
        Top = 152
        ExplicitTop = 152
      end
      inherited cb_color: TComboBox
        Top = 175
        ExplicitTop = 175
      end
      inherited GBListas: TGroupBox
        Left = 3
        Top = 226
        Width = 429
        ExplicitLeft = 3
        ExplicitTop = 226
        ExplicitWidth = 429
      end
      inherited cbbCategoriaInterurbana: TVariantComboBox
        Top = 52
        ExplicitTop = 52
      end
      inherited cbbCategoriaUrbana: TVariantComboBox
        Top = 29
        ExplicitTop = 29
      end
      inherited txtNumeroTelevia: TEdit
        Top = 5
        ExplicitTop = 5
      end
    end
  end
  object pnlFechas: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 876
    Width = 433
    Height = 49
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Padding.Top = 3
    Padding.Bottom = 3
    TabOrder = 4
    object lblFechaAltaCuenta: TLabel
      Left = 22
      Top = 6
      Width = 91
      Height = 13
      Caption = 'Fecha Alta Cuenta:'
    end
    object lblFechaVtoTAG: TLabel
      Left = 33
      Top = 28
      Width = 80
      Height = 13
      Caption = 'Fecha Vto. TAG:'
    end
    object BtnFechaAlta: TButton
      Left = 243
      Top = 3
      Width = 60
      Height = 21
      Caption = 'Editar'
      Enabled = False
      TabOrder = 0
      OnClick = BtnFechaAltaClick
    end
    object txtFechaAlta: TEdit
      Left = 122
      Top = 3
      Width = 116
      Height = 21
      Color = clMenuBar
      ReadOnly = True
      TabOrder = 1
    end
    object btnEditFechaVctoTag: TButton
      Left = 243
      Top = 25
      Width = 60
      Height = 21
      Caption = 'Editar'
      TabOrder = 2
      OnClick = btnEditFechaVctoTagClick
    end
    object edtFechaVctoTag: TEdit
      Left = 122
      Top = 25
      Width = 116
      Height = 21
      Color = clMenuBar
      ReadOnly = True
      TabOrder = 3
    end
  end
  object pnlIndemnizaciones: TPanel
    Left = 0
    Top = 305
    Width = 439
    Height = 240
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 1
    object pnlRadioButtons: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 433
      Height = 53
      Align = alTop
      AutoSize = True
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object rgModalidadEntregaTag: TRadioGroup
        Left = 2
        Top = 0
        Width = 242
        Height = 52
        Align = alCustom
        Caption = '  Modalidad de Entrega del Telev'#237'a '
        Columns = 2
        TabOrder = 0
      end
      object rbArriendo: TRadioButton
        Left = 11
        Top = 28
        Width = 79
        Height = 17
        Caption = 'Arriendo'
        Enabled = False
        TabOrder = 1
        OnClick = rbArriendoClick
      end
      object rbArriendoEnCuotas: TRadioButton
        Left = 117
        Top = 12
        Width = 113
        Height = 17
        Caption = 'Arriendo en Cuotas'
        Enabled = False
        TabOrder = 2
        OnClick = rbArriendoEnCuotasClick
      end
      object rbComodato: TRadioButton
        Left = 11
        Top = 12
        Width = 79
        Height = 17
        Caption = 'Comodato'
        Enabled = False
        TabOrder = 3
        OnClick = rbComodatoClick
      end
      object rgGarantiaTag: TRadioGroup
        Left = 249
        Top = 0
        Width = 179
        Height = 53
        Caption = '  Modalidad C'#225'lculo de Garant'#237'a '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Nuevo'
          'Transferencia')
        TabOrder = 4
        OnClick = rgGarantiaTagClick
      end
    end
    object pnlMotivoFacturacion: TPanel
      Left = 0
      Top = 59
      Width = 439
      Height = 31
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object Bevel1: TBevel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 433
        Height = 23
        Align = alTop
        ExplicitLeft = 10
        ExplicitTop = 5
      end
      object Label10: TLabel
        Left = 26
        Top = 8
        Width = 254
        Height = 13
        Caption = 'Para seleccionar un motivo de facturaci'#243'n haga click '
      end
      object lblSeleccionar: TLabel
        Left = 286
        Top = 8
        Width = 22
        Height = 13
        Cursor = crHandPoint
        Caption = 'ac'#225
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold, fsUnderline]
        ParentFont = False
        OnClick = lblSeleccionarClick
      end
    end
    object dblConceptos: TDBListEx
      AlignWithMargins = True
      Left = 3
      Top = 93
      Width = 433
      Height = 77
      Align = alTop
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 60
          Header.Caption = 'Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'CodigoConcepto'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 250
          Header.Caption = 'Descripci'#243'n de Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescripcionMotivo'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'Precio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'PrecioEnPesos'
        end>
      DataSource = dsConceptos
      DragReorder = True
      ParentColor = False
      PopupMenu = mnuGrillaConceptos
      TabOrder = 2
      TabStop = True
      OnContextPopup = dblConceptosContextPopup
      OnDrawDone = dblConceptosDrawDone
      OnKeyDown = dblConceptosKeyDown
    end
    object gbObservacionesAdicionales: TGroupBox
      Left = 0
      Top = 196
      Width = 439
      Height = 44
      Align = alTop
      Caption = ' C'#243'digo de Voucher  '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 3
      object edNumeroVoucher: TNumericEdit
        Left = 8
        Top = 16
        Width = 390
        Height = 21
        TabOrder = 0
        OnExit = edNumeroVoucherExit
      end
    end
    object pnlTotalMontos: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 176
      Width = 433
      Height = 17
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 4
      Visible = False
      object lblTotalMovimientosTxt: TLabel
        Left = 248
        Top = 0
        Width = 98
        Height = 17
        Align = alRight
        AutoSize = False
        Caption = 'Total Movimientos :'
        ExplicitLeft = 3
        ExplicitTop = 2
        ExplicitHeight = 13
      end
      object lblTotalMovimientos: TLabel
        AlignWithMargins = True
        Left = 349
        Top = 0
        Width = 81
        Height = 17
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alRight
        Alignment = taRightJustify
        AutoSize = False
        Caption = '0'
        ExplicitLeft = 351
      end
    end
  end
  object pnlAceptarCancelar: TPanel
    Left = 0
    Top = 928
    Width = 439
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    DesignSize = (
      439
      41)
    object btnAceptar: TButton
      Left = 273
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnCancelar: TButton
      Left = 354
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object pnlModalidadEntregaTelevia: TPanel
    Left = 0
    Top = 545
    Width = 439
    Height = 29
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object lbl1: TLabel
      Left = 50
      Top = 7
      Width = 158
      Height = 13
      Caption = 'Modalidad de entrega del Televia'
    end
    object lblModalidadEntregaTelevia: TLabel
      Left = 253
      Top = 7
      Width = 103
      Height = 13
      Caption = 'Arriendo en Cuota'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object mnuGrillaConceptos: TPopupMenu
    Left = 352
    Top = 428
    object mnuEliminarConcepto: TMenuItem
      Caption = 'Eliminar Concepto'
      OnClick = mnuEliminarConceptoClick
    end
    object AgregarConceptos1: TMenuItem
      Caption = 'Agregar Conceptos'
      OnClick = AgregarConceptos1Click
    end
  end
  object dsConceptos: TDataSource
    DataSet = cdConceptos
    Left = 264
    Top = 424
  end
  object cdConceptos: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Seleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionMotivo'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'PrecioOrigen'
        DataType = ftInteger
      end
      item
        Name = 'Moneda'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Cotizacion'
        DataType = ftFloat
      end
      item
        Name = 'PrecioEnPesos'
        DataType = ftInteger
      end
      item
        Name = 'Comentario'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'IDMotivoMovCuentaTelevia'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 168
    Top = 424
    Data = {
      050100009619E0BD01000000180000000900000000000300000005010C53656C
      656363696F6E61646F02000300000000000E436F6469676F436F6E636570746F
      0400010000000000114465736372697063696F6E4D6F7469766F020049000000
      010005574944544802000200FF000C50726563696F4F726967656E0400010000
      000000064D6F6E656461020049000000010005574944544802000200FF000A43
      6F74697A6163696F6E08000400000000000D50726563696F456E5065736F7304
      000100000000000A436F6D656E746172696F0200490000000100055749445448
      02000200FF001849444D6F7469766F4D6F764375656E746154656C6576696104
      000100000000000000}
  end
  object spValidarFechasAltaCuenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerFechasAltaCuenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@FechaHoraUltimaBaja'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@FechaHoraAltaCuenta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@FechaHoraAltaTAG'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@FechaHoraVtoTAG'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@TAGEnTransferencia'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 252
    Top = 192
  end
end
