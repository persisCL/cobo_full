object EmitirCertificadoInfraccionesForm: TEmitirCertificadoInfraccionesForm
  Left = 0
  Top = 0
  ActiveControl = edPatente
  Caption = ' Emitir Certificado de Infracciones'
  ClientHeight = 519
  ClientWidth = 974
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 700
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 478
    Width = 974
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      974
      41)
    object btnSalir: TButton
      Left = 872
      Top = 7
      Width = 97
      Height = 27
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnSalirClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 73
    Width = 974
    Height = 405
    Align = alClient
    TabOrder = 1
    object dblInfracciones: TDBListEx
      Left = 1
      Top = 1
      Width = 972
      Height = 247
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Patente'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Fecha'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaInfraccion'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Precio'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'PrecioInfraccion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 125
          Header.Caption = 'Estado CN'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'EstadoCN'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 125
          Header.Caption = 'Estado IF'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'EstadoIF'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Fecha Anulaci'#243'n'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaAnulacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 130
          Header.Caption = 'Motivo Anulaci'#243'n'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'MotivoAnulacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 130
          Header.Caption = 'Nombre'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Nombre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Documento'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroDocumento'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 130
          Header.Caption = 'NumeroConvenio'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroConvenio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Tipo Comprobante'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TipoComprobanteFiscal'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Numero Comprobante'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroComprobanteFiscal'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Estado Pago'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'EstadoPago'
        end>
      DataSource = ds_Infracciones
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDrawText = dblInfraccionesDrawText
    end
    object gbDatosCertificado: TGroupBox
      Left = 1
      Top = 248
      Width = 972
      Height = 63
      Align = alBottom
      Caption = ' Datos del Certificado '
      TabOrder = 1
      object lblRUT: TLabel
        Left = 31
        Top = 27
        Width = 77
        Height = 13
        Caption = 'RUT del Cliente:'
      end
      object peRUTCliente: TPickEdit
        Left = 114
        Top = 23
        Width = 128
        Height = 21
        Enabled = False
        TabOrder = 0
        OnChange = peRUTClienteChange
        EditorStyle = bteTextEdit
        OnButtonClick = peRUTClienteButtonClick
      end
      object btnBuscarCliente: TButton
        Left = 272
        Top = 21
        Width = 75
        Height = 25
        Caption = 'Buscar'
        Enabled = False
        TabOrder = 1
        OnClick = btnBuscarClienteClick
      end
    end
    object gbDatosCliente: TGroupBox
      Left = 1
      Top = 311
      Width = 972
      Height = 93
      Align = alBottom
      Caption = ' Datos del Cliente '
      TabOrder = 2
      object lblApellido: TLabel
        Left = 31
        Top = 26
        Width = 41
        Height = 13
        Caption = 'Apellido:'
      end
      object lblDatoApellidoNombre: TLabel
        Left = 78
        Top = 26
        Width = 107
        Height = 13
        Caption = 'lblDatoApellidoNombre'
      end
      object btnImprimirCertificado: TButton
        Left = 31
        Top = 52
        Width = 143
        Height = 25
        Caption = 'Imprimir Certificado'
        Enabled = False
        TabOrder = 0
        OnClick = btnImprimirCertificadoClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 974
    Height = 73
    Align = alTop
    TabOrder = 0
    object lblFechaHasta: TLabel
      Left = 370
      Top = 42
      Width = 64
      Height = 13
      Caption = 'Hasta Fecha:'
    end
    object lblFechaDesde: TLabel
      Left = 190
      Top = 42
      Width = 66
      Height = 13
      Caption = 'Desde Fecha:'
    end
    object lblPatente: TLabel
      Left = 43
      Top = 42
      Width = 42
      Height = 13
      Caption = 'Patente:'
    end
    object Label1: TLabel
      Left = 15
      Top = 16
      Width = 71
      Height = 13
      Caption = 'Concesionaria:'
    end
    object edPatente: TEdit
      Left = 91
      Top = 38
      Width = 81
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 8
      TabOrder = 1
      OnChange = edPatenteChange
      OnKeyDown = edPatenteKeyDown
    end
    object deDesdeFecha: TDateEdit
      Left = 262
      Top = 38
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 2
      Date = -693594.000000000000000000
    end
    object deHastaFecha: TDateEdit
      Left = 443
      Top = 38
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 3
      Date = -693594.000000000000000000
    end
    object btnBuscar: TButton
      Left = 886
      Top = 37
      Width = 75
      Height = 25
      Caption = 'Buscar'
      TabOrder = 4
      OnClick = btnBuscarClick
    end
    object cbConcesionarias: TVariantComboBox
      Left = 91
      Top = 13
      Width = 261
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbConcesionariasChange
      Items = <>
    end
    object rgMorosidad: TRadioGroup
      Left = 552
      Top = 24
      Width = 185
      Height = 43
      Caption = 'Tipo Infracci'#243'n'
      Columns = 2
      Enabled = False
      Items.Strings = (
        'Normal'
        'Morosidad')
      TabOrder = 5
      OnClick = rgMorosidadClick
    end
  end
  object spObtenerInfraccionesAFacturar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerInfraccionesAFacturar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@SePuedeEmitirCertificado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@ValidarInfraccionesImpagas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CambioPersonaRNVM'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@TraerListaAmarilla'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Prepared = True
    Left = 208
    Top = 86
  end
  object cldInfracciones: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'NumeroDocumento'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 11
      end
      item
        Name = 'Nombre'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoRegion'
        Attributes = [faReadonly, faUnNamed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'CodigoComuna'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescripcionComuna'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'calle'
        Attributes = [faReadonly, faUnNamed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Numero'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Detalle'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'NumeroConvenio'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoInfraccion'
        Attributes = [faUnNamed]
        DataType = ftInteger
      end
      item
        Name = 'Patente'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaInfraccion'
        Attributes = [faUnNamed]
        DataType = ftDateTime
      end
      item
        Name = 'EstadoCN'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EstadoIF'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'MotivoAnulacion'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'PrecioInfraccion'
        Attributes = [faUnNamed]
        DataType = ftFloat
      end
      item
        Name = 'CodigoConvenio'
        Attributes = [faUnNamed]
        DataType = ftInteger
      end
      item
        Name = 'CodigoMotivoAnulacion'
        Attributes = [faUnNamed]
        DataType = ftInteger
      end
      item
        Name = 'FechaAnulacion'
        Attributes = [faUnNamed]
        DataType = ftDateTime
      end
      item
        Name = 'CodigoTarifaDayPassBALI'
        Attributes = [faUnNamed]
        DataType = ftInteger
      end
      item
        Name = 'ExisteTransitoNoAnulado'
        Attributes = [faUnNamed]
        DataType = ftBoolean
      end
      item
        Name = 'TipoComprobanteFiscal'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'NumeroComprobanteFiscal'
        Attributes = [faUnNamed]
        DataType = ftInteger
      end
      item
        Name = 'EstadoPago'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CodigoEstadoInterno'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 32
    Top = 86
    Data = {
      0A0300009619E0BD0100000018000000190000000000030000000A030F4E756D
      65726F446F63756D656E746F0100490010000100055749445448020002000B00
      064E6F6D62726501004900100001000557494454480200020032000C436F6469
      676F526567696F6E01004900120001000557494454480200020003000C436F64
      69676F436F6D756E610100490010000100055749445448020002000300114465
      736372697063696F6E436F6D756E610100490010000100055749445448020002
      003C000563616C6C650100490012000100055749445448020002006400064E75
      6D65726F0100490010000100055749445448020002000A0007446574616C6C65
      01004900100001000557494454480200020064000E4E756D65726F436F6E7665
      6E696F0100490010000100055749445448020002001E0010436F6469676F496E
      6672616363696F6E040001001000000007506174656E74650100490010000100
      0557494454480200020014000F4665636861496E6672616363696F6E08000800
      100000000845737461646F434E01004900100001000557494454480200020014
      000845737461646F494601004900000001000557494454480200020014000F4D
      6F7469766F416E756C6163696F6E010049001000010005574944544802000200
      14001050726563696F496E6672616363696F6E08000400100000000E436F6469
      676F436F6E76656E696F040001001000000015436F6469676F4D6F7469766F41
      6E756C6163696F6E04000100100000000E4665636861416E756C6163696F6E08
      0008001000000017436F6469676F5461726966614461795061737342414C4904
      00010010000000174578697374655472616E7369746F4E6F416E756C61646F02
      00030010000000155469706F436F6D70726F62616E746546697363616C010049
      0010000100055749445448020002001E00174E756D65726F436F6D70726F6261
      6E746546697363616C04000100100000000A45737461646F5061676F01004900
      1000010005574944544802000200010013436F6469676F45737461646F496E74
      65726E6F04000100000000000000}
  end
  object ds_Infracciones: TDataSource
    DataSet = cldInfracciones
    Left = 64
    Top = 85
  end
  object spObtenerConveniosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IncluirConveniosDeBaja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 244
    Top = 86
  end
end
