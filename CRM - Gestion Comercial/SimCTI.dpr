program SimCTI;

uses
  Forms,
  SimCTI_Form in 'SimCTI_Form.pas' {Form1},
  BuscaClientes in '..\Comunes\BuscaClientes.pas' {FormBuscaClientes},
  DMConnection in '..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  DeclTag in '\\Dpsnt\Desarrollo de Software\Utiles\Delphi\Hardware\TAG\Decltag.pas',
  DeclHard in '\\Dpsnt\Desarrollo de Software\Utiles\Delphi\Hardware\DeclHard.pas',
  MaskCombo in '..\Componentes\MaskCombo\MaskCombo.pas',
  DPSGrid in '..\Componentes\DPSGrid\DPSGrid.pas',
  WorkflowComp in '..\Componentes\Workflow\WorkflowComp.pas',
  AjusteFacturacion in '..\Facturacion\AjusteFacturacion.pas' {FrmAjusteFacturacion},
  GenerarAjuste in '..\Facturacion\GenerarAjuste.pas' {FrmGenerarAjuste},
  AgregarMovimiento in '..\Facturacion\AgregarMovimiento.pas' {frmAgregarMovimientoAjuste},
  GrabarTag in '..\GestionTags\GrabarTag.pas' {frmGrabarTags},
  GrabacionMedioPago in '..\GestionTags\GrabacionMedioPago.pas' {GrabarMedioPagoForm},
  DeclCtl in '\\Dpsnt\Desarrollo de Software\Utiles\Delphi\Hardware\Contactless\Declctl.pas',
  DPSPageControl in '..\Componentes\PageControl\DPSPageControl.pas',
  ReporteFactura in '..\Facturacion\ReporteFactura.pas' {formReporteFactura},
  ReporteDetalleViajes in '..\Facturacion\ReporteDetalleViajes.pas' {frmDetalleViajes},
  JPEGPlus in '..\Componentes\Jpeg 12bits\JPEGPlus.pas',
  PeaProcs in '..\Comunes\PeaProcs.pas',
  PeaTypes in '..\Comunes\PeaTypes.pas',
  Navigator in '..\Comunes\Navigator.pas' {NavWindowFrm};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TFormSimCTI, FormSimCTI);
  Application.CreateForm(TDMConnections, DMConnections);
  Application.CreateForm(TfrmAgregarMovimientoAjuste, frmAgregarMovimientoAjuste);
  Application.Run;
end.
