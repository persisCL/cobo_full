unit FrmABMClientesAContactar;

{
Firma       : SS_1408_MCA_20151027
Descripcion : ABM que permite ingresar un cliente para enviarle un mensaje
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, DbList, Abm_obj, ExtCtrls, DMConnection, PeaProcsCN, UtilDB,
  Util, StdCtrls, DmiCtrls, BuscaClientes, UtilProc, PeaProcs, StrUtils;

type
  TFrmClientesAContactar = class(TForm)
    Panel1: TPanel;
    abmtlbr1: TAbmToolbar;
    dblClientes: TAbmList;
    tblClientesAContactar: TADOTable;
    pnl2: TPanel;
    peNumeroDocumento: TPickEdit;
    lblRut: TLabel;
    lblNombre: TLabel;
    spObtenerDatosCliente: TADOStoredProc;
    Notebook: TNotebook;
    btnBtnSalir: TButton;
    btnBtnAceptar: TButton;
    btnBtnCancelar: TButton;
    chkOcultarEliminados: TCheckBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure abmtlbr1Close(Sender: TObject);
    procedure dblClientesDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect;
      State: TOwnerDrawState; Cols: TColPositions);
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure btnBtnSalirClick(Sender: TObject);
    procedure dblClientesInsert(Sender: TObject);
    procedure btnBtnCancelarClick(Sender: TObject);
    procedure btnBtnAceptarClick(Sender: TObject);
    procedure peNumeroDocumentoKeyPress(Sender: TObject; var Key: Char);
    procedure dblClientesDelete(Sender: TObject);
    procedure dblClientesClick(Sender: TObject);
    procedure chkOcultarEliminadosClick(Sender: TObject);
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
    FCodigoPersona: Integer;
  public
    { Public declarations }
  function Inicializar(sCaption : string) : boolean;
  end;

var
  ClientesAContactarForm: TFrmClientesAContactar;

implementation

{$R *.dfm}

function TFrmClientesAContactar.Inicializar(sCaption : string): Boolean ;
resourcestring
    MSG_ERROR = 'Error buscando datos de los Clientes a contactar';
    TXT_ERROR = 'Error';
begin
    Result := False;
    if not OpenTables([tblClientesAContactar]) then begin
        ShowMsgBoxCN(TXT_ERROR, MSG_ERROR, MB_ICONERROR, Application.MainForm);
        exit;
    end else
    begin
        Notebook.PageIndex := 0;
        FCodigoPersona:=0;
		dblClientes.Reload;
    end;

	Result := True;
    Caption := sCaption;
end;

procedure TFrmClientesAContactar.abmtlbr1Close(Sender: TObject);
begin
    Close;
end;

procedure TFrmClientesAContactar.btnBtnAceptarClick(Sender: TObject);
resourcestring
    MSG_ACTUALIZAR_CAPTION = 'ABM Clientes a Contactar';
    MSG_SIN_NUMERO_DOCUMENTO = 'Debe ingresar el RUT del Cliente';
    MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar el cliente RUT %s.';
    MSG_CLIENTE_NO_EXISTE = 'El Cliente no existe';
    SQL_EXISTE_CLIENTE_ACTIVO = 'SELECT 1 FROM ClientesAContactar (NOLOCK) WHERE NumeroDocumento = ''%s'' AND FechaBaja IS NULL';
    MSG_ERROR_CLIENTE_EXISTE = 'El Cliente ya se encuentra registrado';
    MSG_CLIENTE_REGISTRADO = 'Cliente registrado existosamente.';
var
    vSQL : string;
    vExiste : Integer;
begin
    if not ValidateControls([peNumeroDocumento, peNumeroDocumento] ,    
                            [peNumeroDocumento.Text <> '', (peNumeroDocumento.Text <> '') AND (lblNombre.Caption <> '')],
                            MSG_ACTUALIZAR_CAPTION,
                            [MSG_SIN_NUMERO_DOCUMENTO, MSG_CLIENTE_NO_EXISTE]) then Exit;

    if dblClientes.Estado = Alta then begin
        vSQL := Format(SQL_EXISTE_CLIENTE_ACTIVO, [peNumeroDocumento.Text]);
        vExiste := QueryGetValueInt(DMConnections.BaseCAC, vSQL);
        if vExiste = 1 then begin
            MsgBoxBalloon(MSG_ERROR_CLIENTE_EXISTE, Caption, MB_ICONEXCLAMATION, peNumeroDocumento);
            Exit;
        end;
    end;
    Screen.Cursor := crHourGlass;
	With tblClientesAContactar do begin
		Try
			if dblClientes.Estado = Alta then Append else Edit;

			FieldByName('NumeroDocumento').AsString 	:= peNumeroDocumento.Text;
            FieldByName('FechaAlta').AsDateTime         := NowBase(tblClientesAContactar.Connection);
            FieldByName('UsuarioCreacion').AsString 	:= UsuarioSistema;
            FieldByName('FechaCreacion').AsDateTime     := NowBase(tblClientesAContactar.Connection);
  			Post;
            MsgBox(MSG_CLIENTE_REGISTRADO, MSG_ACTUALIZAR_CAPTION, MB_ICONINFORMATION)
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( format(MSG_ACTUALIZAR_ERROR,[peNumeroDocumento.Text]), E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                Screen.Cursor 	   := crDefault;
			end;
		end;
	end;
	Notebook.PageIndex	:= 0;
	peNumeroDocumento.Clear;
    peNumeroDocumento.Enabled := False;
    dblClientes.Estado     	:= Normal;
	dblClientes.Enabled    	:= True;
    dblClientes.Reload;
	dblClientes.SetFocus;
    lblNombre.Caption := '';
   	Screen.Cursor 	   := crDefault;
end;

procedure TFrmClientesAContactar.btnBtnCancelarClick(Sender: TObject);
begin
    peNumeroDocumento.Clear;
    peNumeroDocumento.Enabled := False;
    lblNombre.Caption := '';
    Notebook.PageIndex   := 0;
    dblClientes.Estado  := Normal;
	dblClientes.Enabled := True;
    dblClientes.Reload;
	dblClientes.SetFocus; 
end;

procedure TFrmClientesAContactar.btnBtnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFrmClientesAContactar.chkOcultarEliminadosClick(Sender: TObject);
begin
    if chkOcultarEliminados.Checked then
    begin
        tblClientesAContactar.Filtered := False;
        tblClientesAContactar.Filter := 'FechaBaja = NULL';
        tblClientesAContactar.Filtered := True;
    end else
        tblClientesAContactar.Filtered   := False;

    dblClientes.ReLoad;
end;

procedure TFrmClientesAContactar.dblClientesClick(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;

        with (Sender AS TDbList).Table do begin
            peNumeroDocumento.Text := FieldByName('NumeroDocumento').AsString;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TFrmClientesAContactar.dblClientesDelete(Sender: TObject);
resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el cliente %s?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el cliente';
    MSG_DELETE_CAPTION 		= 'Eliminar Clientes';
    MSG_DELETE_CLIENTE		= 'No se puede eliminar el cliente, ya fue dado de baja';
begin
    try
        Screen.Cursor := crHourGlass;
        If MsgBox( Format(MSG_DELETE_QUESTION, [peNumeroDocumento.Text]), MSG_DELETE_CAPTION, MB_YESNO) = IDYES then begin
            With tblClientesAContactar do begin
                Try
                    if FieldByName('FechaBaja').AsString = '' then begin
                        Edit;
                        FieldByName('FechaBaja').AsDateTime         := NowBase(tblClientesAContactar.Connection);;
                        FieldByName('UsuarioModificacion').AsString := UsuarioSistema;
                        FieldByName('FechaModificacion').AsDateTime := NowBase(tblClientesAContactar.Connection);
                        Post;
                    end
                    else begin
                        MsgBox(MSG_DELETE_CLIENTE, MSG_DELETE_CAPTION, MB_ICONSTOP);
                    end;

                except
                    On E: EDataBaseError do begin
                        Cancel;
                        MsgBoxErr( MSG_DELETE_CAPTION, E.message,
                        MSG_DELETE_CAPTION, MB_ICONSTOP);
                    end;
                end;
            end;
        end;
    finally
        dblClientes.Reload;
        Notebook.PageIndex	:= 0;
        peNumeroDocumento.Clear;
        peNumeroDocumento.Enabled := False;
        dblClientes.Estado     	:= Normal;
        dblClientes.Enabled    	:= True;
        dblClientes.Reload;
        dblClientes.SetFocus;
        lblNombre.Caption := '';
        Screen.Cursor 	   := crDefault;
    end;
end;


procedure TFrmClientesAContactar.dblClientesDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        if FieldByName('FechaBaja').AsString <> '' then
            Font.Color := clRed
        else
            Font.Color := clBlack;
            
        TextOut(Cols[0], Rect.Top, Tabla.FieldbyName('IdClientesAContactar').AsString);
		TextOut(Cols[1], Rect.Top, Tabla.FieldbyName('NumeroDocumento').AsString);
        TextOut(Cols[2], Rect.Top, Tabla.FieldbyName('FechaAlta').AsString);
		TextOut(Cols[3], Rect.Top, Tabla.FieldbyName('FechaBaja').AsString);
        TextOut(Cols[4], Rect.Top, Tabla.FieldbyName('FechaCreacion').AsString);
		TextOut(Cols[5], Rect.Top, Tabla.FieldbyName('UsuarioCreacion').AsString);
        TextOut(Cols[6], Rect.Top, Tabla.FieldbyName('FechaModificacion').AsString);
		TextOut(Cols[7], Rect.Top, Tabla.FieldbyName('UsuarioModificacion').AsString);
	end;
end;

procedure TFrmClientesAContactar.dblClientesInsert(Sender: TObject);
begin
    Screen.Cursor    := crHourGlass;
    Notebook.PageIndex   := 1;
    dblClientes.Enabled := False;
    dblClientes.Estado := Alta;
    peNumeroDocumento.Enabled := True;
    peNumeroDocumento.Clear;
    peNumeroDocumento.SetFocus;
    Screen.Cursor    := crDefault;
end;

procedure TFrmClientesAContactar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    action := caFree;
end;


procedure TFrmClientesAContactar.peNumeroDocumentoButtonClick(Sender: TObject);
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda)then begin
    	if (f.ShowModal = mrok) then begin
            FUltimaBusqueda := F.UltimaBusqueda;
			peNumeroDocumento.Text := Trim(f.Persona.NumeroDocumento);
            lblNombre.Caption := IfThen(f.Persona.Personeria='J',f.Persona.RazonSocial,Trim(f.Persona.Apellido)+' '+Trim(f.Persona.ApellidoMaterno)+', '+Trim(f.Persona.Nombre)); 
	   	end;
	end;
    f.Release;
end;

procedure TFrmClientesAContactar.peNumeroDocumentoChange(Sender: TObject);
begin
    lblNombre.Caption := '';
end;

procedure TFrmClientesAContactar.peNumeroDocumentoKeyPress(Sender: TObject;
  var Key: Char);
resourcestring
    MSG_NO_EXISTE_CLIENTE = 'No se encontr� el RUT %s';
begin
    if Key = #13 then
    begin
        Key := #0;
        if Trim(peNumeroDocumento.text) = '' then begin
            peNumeroDocumento.OnButtonClick(nil);
        end
        else
        begin
            peNumeroDocumento.Text := PadL(Trim(peNumeroDocumento.Text), 9, '0');
            FCodigoPersona := QueryGetValueInt(DMConnections.BaseCAC, Format(
                  'SELECT CodigoPersona FROM Personas (NOLOCK) WHERE CodigoDocumento = ''RUT'' AND NumeroDocumento = ''%s''',
                  [PadL(Trim(peNumeroDocumento.Text), 9, '0')]));
            if FCodigoPersona = 0 then begin
                MsgBox(Format(MSG_NO_EXISTE_CLIENTE, [peNumeroDocumento.Text]),Caption, MB_ICONEXCLAMATION);
                Exit;
            end;

            with spObtenerDatosCliente do begin
                try
                    Close;
                    Parameters.ParamByName('@CodigoCliente').Value := FCodigoPersona;
                    Open;
                    lblNombre.Caption:= Trim(spObtenerDatosCliente.FieldByName('Nombre').AsString);
                except on e:exception do
                    begin
                        MsgBox(e.Message, caption, MB_ICONEXCLAMATION);
                        Screen.Cursor := crDefault;
                        exit;
                    end;
                end;
            end;
        end;
    end;
end;

end.
