object FormRepresentantes: TFormRepresentantes
  Left = 44
  Top = 142
  BorderStyle = bsDialog
  Caption = 'Representantes'
  ClientHeight = 373
  ClientWidth = 861
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dbRepresentanteUno: TGroupBox
    Left = -1
    Top = 0
    Width = 862
    Height = 107
    Caption = 'Representante:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object btnBorrarUno: TSpeedButton
      Left = 819
      Top = 18
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
        3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
        03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
        33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
        0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
        3333333337FFF7F3333333333000003333333333377777333333}
      NumGlyphs = 2
      OnClick = btnBorrarUnoClick
    end
    inline FreRepresentanteUno: TFreDatoPresona
      Left = 15
      Top = 16
      Width = 803
      Height = 89
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = True
      ExplicitLeft = 15
      ExplicitTop = 16
      ExplicitWidth = 803
      ExplicitHeight = 89
      inherited Pc_Datos: TPageControl
        Width = 803
        Height = 63
        ExplicitWidth = 803
        ExplicitHeight = 63
        inherited Tab_Fisica: TTabSheet
          ExplicitWidth = 795
          ExplicitHeight = 53
          inherited Label4: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited Label6: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited Label3: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited txt_ApellidoMaterno: TEdit
            Top = 1
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 1
          end
          inherited txt_Apellido: TEdit
            Top = 1
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 1
          end
          inherited cbSexo: TComboBox
            Top = 29
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 29
          end
          inherited txtFechaNacimiento: TDateEdit
            Left = 333
            Top = 29
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitLeft = 333
            ExplicitTop = 29
          end
          inherited txt_Nombre: TEdit
            Top = 1
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 1
          end
        end
      end
      inherited Panel1: TPanel
        Width = 803
        ExplicitWidth = 803
        inherited lbl_Personeria: TLabel
          Left = 247
          Visible = False
          ExplicitLeft = 247
        end
        inherited lblRutRun: TLabel
          Left = 7
          ExplicitLeft = 7
        end
        inherited lblRazonSocial: TLabel
          Visible = False
        end
        inherited cbPersoneria: TComboBox
          Left = 337
          Font.Name = 'MS Sans Serif'
          ParentFont = False
          Visible = False
          ExplicitLeft = 337
        end
        inherited txtDocumento: TEdit
          Left = 112
          Font.Name = 'MS Sans Serif'
          ParentFont = False
          OnChange = FreRepresentanteUnotxtDocumentoChange
          ExplicitLeft = 112
        end
        inherited txtRazonSocial: TEdit
          Visible = False
        end
      end
    end
  end
  object dbRepresentanteDos: TGroupBox
    Left = -1
    Top = 107
    Width = 862
    Height = 108
    Caption = 'Segundo Representante (Opcional)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object btnBorrarDos: TSpeedButton
      Left = 819
      Top = 18
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
        3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
        03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
        33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
        0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
        3333333337FFF7F3333333333000003333333333377777333333}
      NumGlyphs = 2
      OnClick = btnBorrarDosClick
    end
    inline FreRepresentanteDos: TFreDatoPresona
      Left = 15
      Top = 16
      Width = 803
      Height = 89
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = True
      ExplicitLeft = 15
      ExplicitTop = 16
      ExplicitWidth = 803
      ExplicitHeight = 89
      inherited Pc_Datos: TPageControl
        Width = 803
        Height = 63
        ExplicitWidth = 803
        ExplicitHeight = 63
        inherited Tab_Fisica: TTabSheet
          ExplicitWidth = 795
          ExplicitHeight = 53
          inherited Label4: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited Label6: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited Label3: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited txt_ApellidoMaterno: TEdit
            Top = 1
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 1
          end
          inherited txt_Apellido: TEdit
            Top = 1
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 1
          end
          inherited cbSexo: TComboBox
            Top = 29
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 29
          end
          inherited txtFechaNacimiento: TDateEdit
            Left = 333
            Top = 29
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitLeft = 333
            ExplicitTop = 29
          end
          inherited txt_Nombre: TEdit
            Top = 1
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 1
          end
        end
      end
      inherited Panel1: TPanel
        Width = 803
        ExplicitWidth = 803
        inherited lbl_Personeria: TLabel
          Left = 247
          Visible = False
          ExplicitLeft = 247
        end
        inherited lblRutRun: TLabel
          Left = 7
          ExplicitLeft = 7
        end
        inherited lblRazonSocial: TLabel
          Visible = False
        end
        inherited cbPersoneria: TComboBox
          Left = 337
          Font.Name = 'MS Sans Serif'
          ParentFont = False
          Visible = False
          ExplicitLeft = 337
        end
        inherited txtDocumento: TEdit
          Left = 112
          Font.Name = 'MS Sans Serif'
          ParentFont = False
          OnChange = FreRepresentanteUnotxtDocumentoChange
          ExplicitLeft = 112
        end
        inherited txtRazonSocial: TEdit
          Visible = False
        end
      end
    end
  end
  object Panel: TPanel
    Left = 0
    Top = 323
    Width = 861
    Height = 48
    TabOrder = 3
    object btnAceptar: TButton
      Left = 639
      Top = 12
      Width = 95
      Height = 25
      Hint = 'Guardar Datos'
      Caption = '&Aceptar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnSalir: TButton
      Left = 744
      Top = 12
      Width = 92
      Height = 25
      Hint = 'Salir de la Carga'
      Cancel = True
      Caption = '&Salir'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSalirClick
    end
  end
  object dbRepresentanteTres: TGroupBox
    Left = -1
    Top = 215
    Width = 862
    Height = 108
    Caption = 'Tercer Representante (Opcional)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object btnBorrarTres: TSpeedButton
      Left = 819
      Top = 18
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
        3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
        03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
        33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
        0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
        3333333337FFF7F3333333333000003333333333377777333333}
      NumGlyphs = 2
      OnClick = btnBorrarTresClick
    end
    inline FreRepresentanteTres: TFreDatoPresona
      Left = 15
      Top = 16
      Width = 803
      Height = 89
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = True
      ExplicitLeft = 15
      ExplicitTop = 16
      ExplicitWidth = 803
      ExplicitHeight = 89
      inherited Pc_Datos: TPageControl
        Width = 803
        Height = 63
        ExplicitWidth = 803
        ExplicitHeight = 63
        inherited Tab_Fisica: TTabSheet
          ExplicitWidth = 795
          ExplicitHeight = 53
          inherited Label4: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited Label6: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited Label3: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited txt_ApellidoMaterno: TEdit
            Top = 1
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 1
          end
          inherited txt_Apellido: TEdit
            Top = 1
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 1
          end
          inherited cbSexo: TComboBox
            Top = 29
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 29
          end
          inherited txtFechaNacimiento: TDateEdit
            Left = 333
            Top = 29
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitLeft = 333
            ExplicitTop = 29
          end
          inherited txt_Nombre: TEdit
            Top = 1
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 1
          end
        end
      end
      inherited Panel1: TPanel
        Width = 803
        ExplicitWidth = 803
        inherited lbl_Personeria: TLabel
          Left = 247
          Visible = False
          ExplicitLeft = 247
        end
        inherited lblRutRun: TLabel
          Left = 7
          ExplicitLeft = 7
        end
        inherited lblRazonSocial: TLabel
          Visible = False
        end
        inherited cbPersoneria: TComboBox
          Left = 337
          Font.Name = 'MS Sans Serif'
          ParentFont = False
          Visible = False
          ExplicitLeft = 337
        end
        inherited txtDocumento: TEdit
          Left = 112
          Font.Name = 'MS Sans Serif'
          ParentFont = False
          OnChange = FreRepresentanteUnotxtDocumentoChange
          ExplicitLeft = 112
        end
        inherited txtRazonSocial: TEdit
          Visible = False
        end
      end
    end
  end
  object ObtenerDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 744
    Top = 64
  end
end
