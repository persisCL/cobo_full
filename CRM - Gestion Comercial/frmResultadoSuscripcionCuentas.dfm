object formResultadoSuscripcionCuentas: TformResultadoSuscripcionCuentas
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Suscripci'#243'n de Cuentas en Lista Blanca'
  ClientHeight = 297
  ClientWidth = 787
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlGridsPanel: TPanel
    Left = 0
    Top = 0
    Width = 787
    Height = 256
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 706
    object dbl_Cuentas: TDBListEx
      Left = 0
      Top = 41
      Width = 787
      Height = 215
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 130
          Header.Caption = 'Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroConvenio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 200
          Header.Caption = 'Marca Modelo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'MarcaModeloColor'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 350
          Header.Caption = 'Observaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Observacion'
        end>
      DataSource = dsObtenerCuentas
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      ExplicitWidth = 706
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 787
      Height = 41
      Align = alTop
      TabOrder = 1
      ExplicitWidth = 706
      object lblCuentas: TLabel
        Left = 1
        Top = 1
        Width = 57
        Height = 16
        Align = alLeft
        Caption = 'Cuentas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object pnlBottomPanel: TPanel
    Left = 0
    Top = 256
    Width = 787
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 706
    DesignSize = (
      787
      41)
    object BtnSalir: TButton
      Left = 343
      Top = 6
      Width = 74
      Height = 26
      Hint = 'Salir'
      Anchors = []
      Cancel = True
      Caption = '&Aceptar'
      TabOrder = 0
      OnClick = BtnSalirClick
      ExplicitLeft = 304
    end
  end
  object dsObtenerCuentas: TDataSource
    Left = 136
    Top = 96
  end
end
