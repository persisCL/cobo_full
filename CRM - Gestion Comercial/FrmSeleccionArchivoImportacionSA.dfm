object FormSeleccionArchivoImportacionSA: TFormSeleccionArchivoImportacionSA
  Left = 125
  Top = 139
  Width = 730
  Height = 400
  Caption = 'Importar Archivos Santander'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 722
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlImportar: TPanel
    Left = 0
    Top = 0
    Width = 722
    Height = 366
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object dblArchivos: TDBListEx
      Left = 0
      Top = 0
      Width = 722
      Height = 325
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 250
          Header.Caption = 'Archivo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Sorting = csAscending
          IsLink = False
          OnHeaderClick = dblArchivosColumns4HeaderClick
          FieldName = 'Archivo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 90
          Header.Caption = 'Tipo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dblArchivosColumns4HeaderClick
          FieldName = 'Tipo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 170
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dblArchivosColumns4HeaderClick
          FieldName = 'Estado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 235
          Header.Caption = 'Resultado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dblArchivosColumns4HeaderClick
          FieldName = 'Resultado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 115
          Header.Caption = 'Fecha Proceso'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dblArchivosColumns4HeaderClick
          FieldName = 'FechaHora'
        end>
      DataSource = dsArchivos
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnClick = dblArchivosClick
      OnDblClick = dblArchivosDblClick
    end
    object Panel1: TPanel
      Left = 0
      Top = 325
      Width = 722
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        722
        41)
      object progress: TProgressBar
        Left = 8
        Top = 11
        Width = 537
        Height = 16
        Anchors = [akLeft, akRight, akBottom]
        Min = 0
        Max = 100
        TabOrder = 2
      end
      object btnImportar: TButton
        Left = 557
        Top = 8
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Importar'
        Default = True
        Enabled = False
        TabOrder = 0
        OnClick = btnImportarClick
      end
      object btnSalir: TButton
        Left = 637
        Top = 8
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Salir'
        TabOrder = 1
        OnClick = btnSalirClick
      end
    end
  end
  object dsArchivos: TDataSource
    DataSet = spObtenerArchivosImportar
    Left = 88
    Top = 240
  end
  object spActualizarArchivoImportarSantander: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarArchivoImportarSantander;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Archivo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 87
    Top = 336
  end
  object spObtenerArchivosImportar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = spObtenerArchivosImportarAfterOpen
    ProcedureName = 'ObtenerArchivosImportarSantander;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Orden'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 88
    Top = 288
  end
  object spActualizarEstadoArchivoImportar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEstadoArchivoImportarSantander'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Archivo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoResultado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PersonasAgregadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@VehiculosAgregados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 216
    Top = 264
  end
  object cdVehiculos: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoSolicitud'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'Patente'
        Attributes = [faRequired]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'DigitoVerificador'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CodigoMarca'
        Attributes = [faRequired]
        DataType = ftSmallint
      end
      item
        Name = 'Modelo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'AnioVehiculo'
        Attributes = [faRequired]
        DataType = ftSmallint
      end
      item
        Name = 'CodigoTipoVehiculo'
        Attributes = [faRequired]
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'cdVehiculosCodigoSolicitud'
        Fields = 'CodigoSolicitud'
      end>
    IndexFieldNames = 'CodigoSolicitud'
    Params = <>
    StoreDefs = True
    Left = 360
    Top = 56
    Data = {
      D30000009619E0BD010000001800000007000000000003000000D3000F436F64
      69676F536F6C696369747564040001000400000007506174656E746501004900
      04000100055749445448020002000A001144696769746F566572696669636164
      6F7201004900040001000557494454480200020001000B436F6469676F4D6172
      63610200010004000000064D6F64656C6F010049000000010005574944544802
      00020032000C416E696F5665686963756C6F020001000400000012436F646967
      6F5469706F5665686963756C6F02000100040000000000}
    object cdVehiculosCodigoSolicitud: TIntegerField
      DisplayLabel = 'C'#243'digo de Solicitud'
      FieldName = 'CodigoSolicitud'
      Required = True
    end
    object cdVehiculosPatente: TStringField
      FieldName = 'Patente'
      Required = True
      Size = 10
    end
    object cdVehiculosDigitoVerificador: TStringField
      DisplayLabel = 'D'#237'gito Verificador'
      FieldName = 'DigitoVerificador'
      Required = True
      Size = 1
    end
    object cdVehiculosCodigoMarca: TSmallintField
      DisplayLabel = 'C'#243'digo Marca'
      FieldName = 'CodigoMarca'
      Required = True
    end
    object cdVehiculosModelo: TStringField
      FieldName = 'Modelo'
      Size = 50
    end
    object cdVehiculosAnioVehiculo: TSmallintField
      DisplayLabel = 'A'#241'o Veh'#237'culo'
      FieldName = 'AnioVehiculo'
      Required = True
    end
    object cdVehiculosCodigoTipoVehiculo: TSmallintField
      DisplayLabel = 'C'#243'digo Tipo Veh'#237'culo'
      FieldName = 'CodigoTipoVehiculo'
      Required = True
    end
  end
  object cdPersonas: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoSolicitudContacto'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'RUT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 11
      end
      item
        Name = 'DVRUT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Personeria'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Apellido'
        Attributes = [faRequired]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'ApellidoMaterno'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Nombre'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Sexo'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FechaNacimiento'
        DataType = ftDateTime
      end
      item
        Name = 'ApellidoContactoComercial'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'ApellidoMaternoContactoCom'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'NombreContactoComercial'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'SexoContactoComercial'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CodigoRegion'
        Attributes = [faRequired]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'CodigoComuna'
        Attributes = [faRequired]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'Calle'
        Attributes = [faRequired]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Numero'
        Attributes = [faRequired]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'DetalleDomicilio'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TipoTelefonoPrincipal'
        Attributes = [faRequired]
        DataType = ftWord
      end
      item
        Name = 'CodigoAreaTelefonoPrincipal'
        Attributes = [faRequired]
        DataType = ftSmallint
      end
      item
        Name = 'NumeroTelefonoPrincipal'
        Attributes = [faRequired]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'HoraDesde'
        Attributes = [faRequired]
        DataType = ftDateTime
      end
      item
        Name = 'HoraHasta'
        Attributes = [faRequired]
        DataType = ftDateTime
      end
      item
        Name = 'TipoTelefonoAlternativo'
        DataType = ftWord
      end
      item
        Name = 'CodigoAreaTelefonoAlternativo'
        DataType = ftSmallint
      end
      item
        Name = 'NumeroTelefonoAlternativo'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Email'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'TipoMedioPagoAutomatico'
        Attributes = [faRequired]
        DataType = ftWord
      end
      item
        Name = 'CodigoEmisorTarjetaCredito'
        DataType = ftWord
      end
      item
        Name = 'CodigoTipoTarjetaCredito'
        DataType = ftWord
      end
      item
        Name = 'NumeroTarjetaCredito'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaVenciemiento'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoTipoCuentaBancaria'
        DataType = ftWord
      end
      item
        Name = 'CodigoBanco'
        DataType = ftInteger
      end
      item
        Name = 'NroCuentaBancaria'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Sucursal'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'NumeroDocumentoTitular'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'NombreTitular'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TelefonoTitular'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoAreaTelefonoTitular'
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'cdPersonasCodigoSolicitud'
        Fields = 'CodigoSolicitudContacto'
      end>
    Params = <>
    StoreDefs = True
    Left = 424
    Top = 56
    Data = {
      450500009619E0BD010000001800000028000000000003000000450517436F64
      69676F536F6C696369747564436F6E746163746F040001000400000003525554
      0100490004000100055749445448020002000B00054456525554010049000400
      01000557494454480200020001000A506572736F6E6572696101004900040001
      00055749445448020002000100084170656C6C69646F01004900040001000557
      49445448020002003C000F4170656C6C69646F4D617465726E6F010049000000
      0100055749445448020002001E00064E6F6D6272650100490000000100055749
      445448020002003C00045365786F010049000000010005574944544802000200
      01000F46656368614E6163696D69656E746F0800080000000000194170656C6C
      69646F436F6E746163746F436F6D65726369616C010049000000010005574944
      5448020002003C001A4170656C6C69646F4D617465726E6F436F6E746163746F
      436F6D0100490000000100055749445448020002001E00174E6F6D627265436F
      6E746163746F436F6D65726369616C0100490000000100055749445448020002
      003C00155365786F436F6E746163746F436F6D65726369616C01004900000001
      000557494454480200020001000C436F6469676F526567696F6E010049000400
      01000557494454480200020003000C436F6469676F436F6D756E610100490004
      0001000557494454480200020003000543616C6C650100490004000100055749
      445448020002006400064E756D65726F01004900040001000557494454480200
      02000A0010446574616C6C65446F6D6963696C696F0100490000000100055749
      445448020002003200155469706F54656C65666F6E6F5072696E636970616C02
      000200040000001B436F6469676F4172656154656C65666F6E6F5072696E6369
      70616C0200010004000000174E756D65726F54656C65666F6E6F5072696E6369
      70616C020049000400010005574944544802000200FF0009486F726144657364
      65080008000400000009486F726148617374610800080004000000175469706F
      54656C65666F6E6F416C7465726E617469766F02000200000000001D436F6469
      676F4172656154656C65666F6E6F416C7465726E617469766F02000100000000
      00194E756D65726F54656C65666F6E6F416C7465726E617469766F0200490000
      00010005574944544802000200FF0005456D61696C0200490000000100055749
      44544802000200FF00175469706F4D6564696F5061676F4175746F6D61746963
      6F02000200040000001A436F6469676F456D69736F725461726A657461437265
      6469746F020002000000000018436F6469676F5469706F5461726A6574614372
      656469746F0200020000000000144E756D65726F5461726A6574614372656469
      746F010049000000010005574944544802000200140011466563686156656E63
      69656D69656E746F0100490000000100055749445448020002000A0018436F64
      69676F5469706F4375656E746142616E636172696102000200000000000B436F
      6469676F42616E636F0400010000000000114E726F4375656E746142616E6361
      7269610100490000000100055749445448020002001E0008537563757273616C
      0100490000000100055749445448020002003200164E756D65726F446F63756D
      656E746F546974756C61720100490000000100055749445448020002000B000D
      4E6F6D627265546974756C617201004900000001000557494454480200020064
      000F54656C65666F6E6F546974756C6172010049000000010005574944544802
      0002001E0019436F6469676F4172656154656C65666F6E6F546974756C617202
      000100000000000000}
    object cdPersonasCodigoSolicitudContacto: TIntegerField
      FieldName = 'CodigoSolicitudContacto'
      Required = True
    end
    object cdPersonasRUT: TStringField
      FieldName = 'RUT'
      Required = True
      Size = 11
    end
    object cdPersonasDVRUT: TStringField
      FieldName = 'DVRUT'
      Required = True
      Size = 1
    end
    object cdPersonasPersoneria: TStringField
      FieldName = 'Personeria'
      Required = True
      Size = 1
    end
    object cdPersonasApellido: TStringField
      FieldName = 'Apellido'
      Required = True
      Size = 60
    end
    object cdPersonasApellidoMaterno: TStringField
      FieldName = 'ApellidoMaterno'
      Size = 30
    end
    object cdPersonasNombre: TStringField
      FieldName = 'Nombre'
      Size = 60
    end
    object cdPersonasSexo: TStringField
      FieldName = 'Sexo'
      Size = 1
    end
    object cdPersonasFechaNacimiento: TDateTimeField
      FieldName = 'FechaNacimiento'
    end
    object cdPersonasApellidoContactoComercial: TStringField
      FieldName = 'ApellidoContactoComercial'
      Size = 60
    end
    object cdPersonasApellidoMaternoContactoCom: TStringField
      FieldName = 'ApellidoMaternoContactoCom'
      Size = 30
    end
    object cdPersonasNombreContactoComercial: TStringField
      FieldName = 'NombreContactoComercial'
      Size = 60
    end
    object cdPersonasSexoContactoComercial: TStringField
      FieldName = 'SexoContactoComercial'
      Size = 1
    end
    object cdPersonasCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      Required = True
      Size = 3
    end
    object cdPersonasCodigoComuna: TStringField
      FieldName = 'CodigoComuna'
      Required = True
      Size = 3
    end
    object cdPersonasCalle: TStringField
      FieldName = 'Calle'
      Required = True
      Size = 100
    end
    object cdPersonasNumero: TStringField
      FieldName = 'Numero'
      Required = True
      Size = 10
    end
    object cdPersonasDetalleDomicilio: TStringField
      FieldName = 'DetalleDomicilio'
      Size = 50
    end
    object cdPersonasTipoTelefonoPrincipal: TWordField
      FieldName = 'TipoTelefonoPrincipal'
      Required = True
    end
    object cdPersonasCodigoAreaTelefonoPrincipal: TSmallintField
      FieldName = 'CodigoAreaTelefonoPrincipal'
      Required = True
    end
    object cdPersonasNumeroTelefonoPrincipal: TStringField
      FieldName = 'NumeroTelefonoPrincipal'
      Required = True
      Size = 255
    end
    object cdPersonasHoraDesde: TDateTimeField
      FieldName = 'HoraDesde'
      Required = True
    end
    object cdPersonasHoraHasta: TDateTimeField
      FieldName = 'HoraHasta'
      Required = True
    end
    object cdPersonasTipoTelefonoAlternativo: TWordField
      FieldName = 'TipoTelefonoAlternativo'
    end
    object cdPersonasCodigoAreaTelefonoAlternativo: TSmallintField
      FieldName = 'CodigoAreaTelefonoAlternativo'
    end
    object cdPersonasNumeroTelefonoAlternativo: TStringField
      FieldName = 'NumeroTelefonoAlternativo'
      Size = 255
    end
    object cdPersonasEmail: TStringField
      FieldName = 'Email'
      Size = 255
    end
    object cdPersonasTipoMedioPagoAutomatico: TWordField
      FieldName = 'TipoMedioPagoAutomatico'
      Required = True
    end
    object cdPersonasCodigoEmisorTarjetaCredito: TWordField
      DisplayLabel = 'Ente Emisor'
      FieldName = 'CodigoEmisorTarjetaCredito'
    end
    object cdPersonasCodigoTipoTarjetaCredito: TWordField
      FieldName = 'CodigoTipoTarjetaCredito'
    end
    object cdPersonasNumeroTarjetaCredito: TStringField
      FieldName = 'NumeroTarjetaCredito'
    end
    object cdPersonasFechaVenciemiento: TStringField
      FieldName = 'FechaVenciemiento'
      Size = 10
    end
    object cdPersonasCodigoTipoCuentaBancaria: TWordField
      FieldName = 'CodigoTipoCuentaBancaria'
    end
    object cdPersonasCodigoBanco: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object cdPersonasNroCuentaBancaria: TStringField
      FieldName = 'NroCuentaBancaria'
      Size = 30
    end
    object cdPersonasSucursal: TStringField
      FieldName = 'Sucursal'
      Size = 50
    end
    object cdPersonasNumeroDocumentoTitular: TStringField
      FieldName = 'NumeroDocumentoTitular'
      Size = 11
    end
    object cdPersonasNombreTitular: TStringField
      FieldName = 'NombreTitular'
      Size = 100
    end
    object cdPersonasTelefonoTitular: TStringField
      FieldName = 'TelefonoTitular'
      Size = 30
    end
    object cdPersonasCodigoAreaTelefonoTitular: TSmallintField
      FieldName = 'CodigoAreaTelefonoTitular'
    end
  end
  object qryValidarRegion: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'codigoregion'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      'select count(*) '
      'from regiones'
      'where codigopais     =  dbo.CONST_PAIS_CHILE() and'
      '           codigoregion  = :codigoregion')
    Left = 360
    Top = 168
  end
  object qryValidarMarca: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'codigomarca'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select count(*) '
      'from VehiculosMarcas '
      'where codigomarca = :codigomarca')
    Left = 360
    Top = 112
  end
  object qryValidarModelo: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'codigomarca'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'codigomodelo'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'select count(*)'
      'from VehiculosModelos'
      'where codigomarca = :codigomarca and'
      '          codigomodelo = :codigomodelo'
      ' ')
    Left = 444
    Top = 112
  end
  object qryValidarComuna: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'codigoregion'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'codigocomuna'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      'select count(*) from comunas '
      'where codigopais     = dbo.CONST_PAIS_CHILE()  and '
      'codigoregion  = :codigoregion and  '
      'codigocomuna = :codigocomuna')
    Left = 448
    Top = 168
  end
  object qryValidarTipoVehiculo: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoTipoVehiculo'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select count(*)'
      
        'from VehiculosTipos where CodigoTipoVehiculo = :CodigoTipoVehicu' +
        'lo')
    Left = 552
    Top = 112
  end
  object qryValidarTipoTelefono: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoTipoMedioContacto'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'select count(*) from TiposMedioContacto'
      'where '
      '   CodigoTipoMedioContacto = :CodigoTipoMedioContacto and'
      '   formato = '#39'T'#39)
    Left = 552
    Top = 168
  end
  object spCrearSolicitudImportacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'CrearSolicitudImportacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 1916
      end
      item
        Name = '@FechaHoraCreacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@CodigoFuenteSolicitud'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCitaPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoEstadoSolicitud'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFuenteOrigenContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TipoMedioPagoAutomatico'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@LugarNacimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@FechaNacimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoActividad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ApellidoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaternoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@NombreContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@SexoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@EmailContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoSituacionIVA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Pregunta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Respuesta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@EstadoPersonaAdhesion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@TipoDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Dpto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@DescripcionCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoSegmento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContactoTelefonoPrincipal'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoAreaTelefonoPrincipal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ValorTelefonoPrincipal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@HorarioDesdeTelefonoPrincipal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHastaTelefonoPrincipal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContactoeMAIL'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContactoTelefonoAlternativo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoAreaTelefonoAlternativo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ValorTelefonoAlternativo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoTipoCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@CodigoEmisorTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroDocumentoMedioPagoAutomatico'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@NombreMedioPagoAutomatico'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@TelefonoMedioPagoAutomatico'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoAreaMedioPagoAutomatico'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Prepared = True
    Left = 192
    Top = 104
  end
  object spActualizarVehiculosContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarVehiculosContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoMarca'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Modelo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@AnioVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoTipoVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoColor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 9
        Value = Null
      end>
    Left = 192
    Top = 168
  end
  object qryValidarAreaTelefono: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoArea'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select RangoDesde,RangoHasta'
      'from CodigosAreaTelefono'
      'where CodigoArea = :CodigoArea')
    Left = 368
    Top = 232
  end
  object dsPersonas: TDataSource
    DataSet = cdPersonas
    Left = 496
    Top = 56
  end
end
