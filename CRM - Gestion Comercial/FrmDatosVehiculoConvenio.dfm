object FormDatosVehiculoConvenio: TFormDatosVehiculoConvenio
  Left = 196
  Top = 229
  BorderStyle = bsDialog
  Caption = 'Datos del Veh'#237'culo'
  ClientHeight = 322
  ClientWidth = 468
  Color = clBtnFace
  Constraints.MaxHeight = 500
  Constraints.MaxWidth = 700
  Constraints.MinHeight = 240
  Constraints.MinWidth = 470
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBotones: TPanel
    Left = 0
    Top = 286
    Width = 468
    Height = 36
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object btnAceptar: TDPSButton
      Left = 295
      Top = 8
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnCancelar: TDPSButton
      Left = 377
      Top = 8
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
  object GBVehiculo: TGroupBox
    Left = 6
    Top = 8
    Width = 459
    Height = 181
    Caption = ' Datos del Veh'#237'culo '
    TabOrder = 1
    object Label13: TLabel
      Left = 21
      Top = 26
      Width = 49
      Height = 13
      Caption = 'Patente:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 21
      Top = 77
      Width = 40
      Height = 13
      Caption = 'Marca:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 21
      Top = 102
      Width = 38
      Height = 13
      Caption = 'Modelo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label21: TLabel
      Left = 263
      Top = 25
      Width = 27
      Height = 13
      Caption = 'Color:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label47: TLabel
      Left = 21
      Top = 129
      Width = 27
      Height = 13
      Caption = 'A'#241'o:'
      FocusControl = txt_anio
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTipoVehiculo: TLabel
      Left = 21
      Top = 52
      Width = 30
      Height = 13
      Caption = 'Tipo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 21
      Top = 154
      Width = 44
      Height = 13
      Caption = 'Estado:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object cb_color: TComboBox
      Left = 299
      Top = 17
      Width = 126
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 7
      Visible = False
    end
    object cb_marca: TComboBox
      Left = 100
      Top = 73
      Width = 236
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 4
    end
    object cb_modelo: TComboBox
      Left = 344
      Top = 74
      Width = 41
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 6
      Visible = False
    end
    object txt_anio: TNumericEdit
      Left = 100
      Top = 122
      Width = 47
      Height = 21
      Color = 16444382
      MaxLength = 4
      TabOrder = 8
      OnExit = txt_anioExit
      Decimals = 0
    end
    object cbTipoVehiculo: TVariantComboBox
      Left = 100
      Top = 48
      Width = 166
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbTipoVehiculoChange
      Items = <>
    end
    object txtDescripcionModelo: TEdit
      Left = 100
      Top = 97
      Width = 236
      Height = 21
      Hint = 'Modelo'
      CharCase = ecUpperCase
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
    end
    object txtPatente: TEdit
      Left = 100
      Top = 22
      Width = 108
      Height = 21
      Hint = 'Patente'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 10
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnExit = txtPatenteExit
      OnKeyPress = txtPatenteKeyPress
    end
    object txtDigitoVerificador: TEdit
      Left = 213
      Top = 22
      Width = 26
      Height = 21
      Hint = 'D'#237'gito Verificador'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = False
      OnExit = txtDigitoVerificadorExit
      OnKeyPress = txtDigitoVerificadorKeyPress
    end
    object cbConAcoplado: TCheckBox
      Left = 272
      Top = 50
      Width = 66
      Height = 17
      Caption = 'Acoplado'
      TabOrder = 3
    end
    object ComboBox1: TComboBox
      Left = 100
      Top = 147
      Width = 236
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 9
      Text = 'Vigente'
      Items.Strings = (
        'Vigente')
    end
  end
  object GBCuenta: TGroupBox
    Left = 8
    Top = 191
    Width = 457
    Height = 93
    Caption = 'Telev'#237'a'
    TabOrder = 2
    object lblTelevia: TLabel
      Left = 14
      Top = 22
      Width = 48
      Height = 13
      Caption = 'N'#250'mero:'
      FocusControl = txtNumeroTelevia
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel
      Left = 14
      Top = 46
      Width = 44
      Height = 13
      Caption = 'Estado:'
      FocusControl = cb_EstadoTelevia
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel
      Left = 14
      Top = 70
      Width = 86
      Height = 13
      Caption = 'Tipo convenio:'
      FocusControl = cb_TipoConvenioTelevia
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label3: TLabel
      Left = 230
      Top = 21
      Width = 21
      Height = 13
      Caption = 'Alta:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 256
      Top = 21
      Width = 58
      Height = 13
      Caption = '01/01/2005'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 340
      Top = 46
      Width = 24
      Height = 13
      Caption = 'Baja:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 370
      Top = 46
      Width = 58
      Height = 13
      Caption = '01/10/2005'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 341
      Top = 21
      Width = 19
      Height = 13
      Caption = 'Vto:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 370
      Top = 21
      Width = 58
      Height = 13
      Caption = '01/01/2007'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtNumeroTelevia: TEdit
      Left = 100
      Top = 16
      Width = 126
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 15
      TabOrder = 0
      Visible = False
      OnExit = txtNumeroTeleviaExit
    end
    object cb_EstadoTelevia: TVariantComboBox
      Left = 100
      Top = 40
      Width = 236
      Height = 21
      Hint = 'Indique si utiliza una forma de pago autom'#225'tica'
      Style = vcsDropDownList
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Items = <>
    end
    object cb_TipoConvenioTelevia: TVariantComboBox
      Left = 100
      Top = 64
      Width = 236
      Height = 21
      Hint = 'Tipo de convenio'
      Style = vcsDropDownList
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Items = <
        item
          Caption = 'Alta de convenio normal'
        end>
    end
  end
  object VerificarFormatoPatenteChilena: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarFormatoPatenteChilena'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 464
    Top = 166
  end
end
