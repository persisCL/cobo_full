object formSituacionTag: TformSituacionTag
  Left = 317
  Top = 190
  BorderStyle = bsDialog
  Caption = 'formSituacionTag'
  ClientHeight = 150
  ClientWidth = 379
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 142
    Height = 13
    Caption = 'Cambiar el estado del Televia:'
  end
  object Label2: TLabel
    Left = 8
    Top = 40
    Width = 87
    Height = 13
    Caption = 'Vehiculo Patente :'
  end
  object Label3: TLabel
    Left = 8
    Top = 72
    Width = 85
    Height = 13
    Caption = 'Nuevo Estado:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl_Patente: TLabel
    Left = 168
    Top = 16
    Width = 53
    Height = 13
    Caption = 'lbl_Patente'
  end
  object lbl_tag: TLabel
    Left = 168
    Top = 40
    Width = 31
    Height = 13
    Caption = 'lbl_tag'
  end
  object Panel1: TPanel
    Left = 0
    Top = 109
    Width = 379
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      379
      41)
    object btn_Aceptar: TButton
      Left = 268
      Top = 9
      Width = 95
      Height = 25
      Hint = 'Imprimir Contrato'
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btn_AceptarClick
    end
    object btn_Cancelar: TButton
      Left = 164
      Top = 8
      Width = 95
      Height = 25
      Hint = 'Imprimir Contrato'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btn_CancelarClick
    end
  end
  object cb_situacion: TComboBox
    Left = 168
    Top = 64
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    Text = 'cb_Situacion'
  end
end
