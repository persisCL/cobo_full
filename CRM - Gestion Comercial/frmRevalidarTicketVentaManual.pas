{--------------------------------------------------------------------------
                     frmRevalidarTicketVentaManual

Author		: mbecerra
Date		: 02-Agosto-2011
Description	: 		(COP AMB)
            	Formulario que permite la visualizaci�n de los Ticket de Venta
                Manual (Asignados y no asignados) y la visualizaci�n de un
                listado de tr�nsitos relacionados a esos Ticket.
Firma       : SS_976_ALA_20110816
Description : Se incluye un nuevo m�todo llamado �OcultarListaEstados�,
             el cual es llamado en el evento OnExit del control chklstEstadosTtos.
             Adem�s se agrega al listado de tr�nsitos la imagen de estos,
             llamando al formulario ImagenesTransitos.
             Se arregla nombre del control que se obten�a para imprimir la fecha
             en el listado de tr�nsitos, ya que utilizaba el listado de TVM

Firma       : SS_976_ALA_20110818
Description : Se incluye focus a lista de transitos para que cuando se muestre
              se oculte autom�ticamente.

Firma       : SS_976_ALA_20110831
Description : Se modifica llamada al Store �ChequearTransitosLimbo2�,
              por nuevo Store �AsignarTransito_A_TVM�, para la asociaci�n del transito con el TVM.

Firma       : SS_976_ALA_20110908
Description : Se incluye validaci�n para que un ticket de venta manual
              no pueda ser asignado a un tr�nsito si ya est� asociado a un NumCorrCA.
--------------------------------------------------------------------------}
unit frmRevalidarTicketVentaManual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, ExtCtrls, StdCtrls, VariantComboBox, Validate,
  DateEdit, CheckLst, DmiCtrls, DMConnection, DB, ADODB, ConstParametrosGenerales,
  UtilProc, Util, ImgProcs, Diccionario, ImgList, frmImagenesTicketVentaManual, DateUtils,
  TimeEdit, frmMuestraMensaje,
  ImagenesTransitos,                                                            //SS_976_ALA_20110816
  UtilDB;                                                                       //SS_976_ALA_20110831

type
    EstadoTicketVentaManual = (NoAsignado = 0, Asignado = 1, Todos = 2);

type
  TRevalidarTicketVentaManualForm = class(TForm)
    pnlConsulta: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtPatente: TEdit;
    deDesde: TDateEdit;
    deHasta: TDateEdit;
    vcbEstado: TVariantComboBox;
    Label4: TLabel;
    btnBuscar: TButton;
    pnlTVM: TPanel;
    Splitter1: TSplitter;
    pnlTransitos: TPanel;
    dblListadoTVM: TDBListEx;
    dblListadoTransitos: TDBListEx;
    vcbConcesionarias: TVariantComboBox;
    Label5: TLabel;
    pnlFiltroTransitos: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    peEstadosTtos: TPickEdit;
    btnSalir: TButton;
    lblRangoFecha: TLabel;
    spObtenerConcesionarias: TADOStoredProc;
    spObtenerTransitosEstados: TADOStoredProc;
    spObtenerTicketsVentaManual: TADOStoredProc;
    dsTicketVentaManual: TDataSource;
    ilCamara: TImageList;
    spObtenerTransitos: TADOStoredProc;
    dsObtenerTransitos: TDataSource;
    pnlBotonesGenerales: TPanel;
    btnAsociar: TButton;
    edtHoraVentaDesde: TTimeEdit;
    edtHoraVentaHasta: TTimeEdit;
    pnlBotonesTVM: TPanel;
    btnEditarPatente: TButton;
    edtRangoFecha: TNumericEdit;
    btnBuscarTransitos: TButton;
    chklstEstadosTtos: TCheckListBox;
    spAsignarTransito_A_TVM: TADOStoredProc;                                    //SS_976_ALA_20110831
    procedure btnBuscarClick(Sender: TObject);
    procedure peEstadosTtosButtonClick(Sender: TObject);
    procedure dblListadoTVMDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblListadoTVMCheckLink(Sender: TCustomDBListEx;
      Column: TDBListExColumn; var IsLink: Boolean);
    procedure dblListadoTVMLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnSalirClick(Sender: TObject);
    procedure btnBuscarTransitosClick(Sender: TObject);
    procedure btnEditarPatenteClick(Sender: TObject);
    procedure btnAsociarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spObtenerTicketsVentaManualAfterScroll(DataSet: TDataSet);
    procedure dblListadoTVMDblClick(Sender: TObject);
    procedure dblListadoTransitosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure dblListadoTransitosCheckLink(Sender: TCustomDBListEx;                                                                     //SS_976_ALA_20110816
      Column: TDBListExColumn; var IsLink: Boolean);                                                                                    //SS_976_ALA_20110816
    procedure dblListadoTransitosLinkClick(Sender: TCustomDBListEx;                                                                     //SS_976_ALA_20110816
      Column: TDBListExColumn);                                                                                                         //SS_976_ALA_20110816
    procedure OcultarListaEstados(Sender: TObject);                                                                                     //SS_976_ALA_20110816
  private
    { Private declarations }
    FNombresConcesionarias  : TDiccionario;
    FCodigosEstados         : TStringList;
    procedure chklstEstadosTtosChecked(Check : Boolean);
    function ObtenerPatente                 : String;
    function ObtenerEstadoTicketVentaManual : EstadoTicketVentaManual;
    function ObtenerFechaTVMDesde           : TDateTime;
    function ObtenerFechaTVMHasta           : TDateTime;
    function ObtenerCodigoConcesionaria     : Integer;
    function ObtenerNombreConcesionaria     : string;
    function ObtenerNombreCortoConcesionaria(CodigoConcesionaria : Integer) : string;
    function ObtenerEstadosTransitos        : string;
    function ObtenerRangoFechaTransito      : Integer;
    procedure BuscarTransitos(FechaVentaTVM : TDateTime);
    procedure EdicionPatente;
  public
    { Public declarations }
    function Inicializar : Boolean;
    Destructor  Destroy; override;
  end;

var
  RevalidarTicketVentaManualForm: TRevalidarTicketVentaManualForm;

implementation

{$R *.dfm}

procedure TRevalidarTicketVentaManualForm.btnAsociarClick(Sender: TObject);
resourcestring
    ERROR_AL_ASOCIAR_TRANSITO       = 'A ocurrido un error al intentar asociar el transito, por favor contacte al administrador';
    DET_ERROR_AL_ASOCIAR_TRANSITO   = 'NumCorrCA: %d, Error: %s';
    VALIDACION_ESTADOS_TRANSITO     = 'No puede asociar un Tr�nsito que no se encuentre en estados 2, 3 o 10';
    VALIDACION_PATENTES_DISTINTAS   = 'La patente del Ticket de Venta Manual es diferente con la del Transito, por favor seleccione correctamente los items que desea asociar.';
    VALIDACION_TVM_ASIGNADO         = 'El ticket de venta manual ya se encuentra asociado a un tr�nsito (NumCorrCA: %d), y por lo tanto no se puede volver a asociar.';          //SS_976_ALA_20110908
var
    NumCorrCA   : Int64;
    IDTicketTVM : Int64;
    Estado      : Integer;                                                                                                                 //SS_976_ALA_20110831
    Resultado   : String;
    PatenteTVM  : string;
    PatenteTTO  : String;
    PunteroTVM  : TBookmark;
    PunteroTTO  : TBookmark;
    NumCorrCATVM: Int64;                                                                                                                   //SS_976_ALA_20110908
begin
    try
        if not spObtenerTransitos.IsEmpty then begin
            NumCorrCATVM:= spObtenerTicketsVentaManual.FieldByName('NumCorrCA').AsInteger;                                                  //SS_976_ALA_20110908
            if NumCorrCATVM <= 0 then begin                                                                                                //SS_976_ALA_20110908
            PatenteTVM := UpperCase(Trim(IIf(not spObtenerTicketsVentaManual.IsEmpty, spObtenerTicketsVentaManual.FieldByName('Patente').AsString, '')));
            //Si la patente detectada es vacia, tomamos la patente de la cuenta.
            PatenteTTO := UpperCase(Trim(IIf(spObtenerTransitos.FieldByName('Patente').AsString = '',
                                spObtenerTransitos.FieldByName('PatenteCuenta').AsString,
                                spObtenerTransitos.FieldByName('Patente').AsString)));
            if PatenteTVM = PatenteTTO then begin

                Estado      := spObtenerTransitos.FieldByName('Estado').AsInteger;
                NumCorrCA   := spObtenerTransitos.FieldByName('NumCorrCA').AsInteger;
                IDTicketTVM := spObtenerTicketsVentaManual.FieldByName('ID_Transacciones_TVM').AsInteger;                                        //SS_976_ALA_20110831
                if (Estado = 2) OR (Estado = 3) OR (Estado = 10) then begin
                    if NumCorrCA>0 then begin
                        { INICIO BLOQUE SS_976_ALA_20110831
                        spChequearTransitosLimbo2.Close;
                        with spChequearTransitosLimbo2.Parameters do begin
                            Refresh;
                            ParamByName('@NumCorrCA').Value      := NumCorrCA;
                            ParamByName('@MensajeError').Value   := '';
                        end;
                        spChequearTransitosLimbo2.ExecProc;
                        Resultado := spChequearTransitosLimbo2.Parameters.ParamByName('@MensajeError').Value;
                        TERMINO BLOQUE SS_976_ALA_20110831 }
                        QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION TVMTransito');                                                    //SS_976_ALA_20110831
                        spAsignarTransito_A_TVM.Close;                                                                                           //SS_976_ALA_20110831
                        with spAsignarTransito_A_TVM.Parameters do begin                                                                         //SS_976_ALA_20110831
                            Refresh;                                                                                                             //SS_976_ALA_20110831
                            ParamByName('@NumCorrCA').Value             := NumCorrCA;                                                            //SS_976_ALA_20110831
                            ParamByName('@IDTransaccionTVM').Value      := IDTicketTVM;                                                          //SS_976_ALA_20110831
                            ParamByName('@UsuarioModificador').Value    := UsuarioSistema;                                                       //SS_976_ALA_20110831
                            ParamByName('@MensajeError').Value          := '';                                                                   //SS_976_ALA_20110831
                        end;                                                                                                                     //SS_976_ALA_20110831
                        spAsignarTransito_A_TVM.ExecProc;                                                                                        //SS_976_ALA_20110831
                        Resultado := spAsignarTransito_A_TVM.Parameters.ParamByName('@MensajeError').Value;                                      //SS_976_ALA_20110831
                        if Resultado<>'' then begin
                            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION TVMTransito END');                //SS_976_ALA_20110831
                            MsgBoxErr(ERROR_AL_ASOCIAR_TRANSITO,
                                    Format(DET_ERROR_AL_ASOCIAR_TRANSITO, [NumCorrCA, Resultado]),
                                    Self.Caption, MB_ICONERROR);
                        end
                        else begin
                            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION TVMTransito END');                   //SS_976_ALA_20110831
                            //Actualizamos el listdo de TVM y de Transitos
                            //Primero obtenemos la posicion actual del dataset
                            PunteroTVM := dsTicketVentaManual.DataSet.GetBookmark;
                            PunteroTTO := dsObtenerTransitos.DataSet.GetBookmark;

                            TPanelMensajesForm.MuestraMensaje('Actualizando Datos ...', True);
                            try
                                //Refrescamos ambos dataset
                                dsTicketVentaManual.DataSet.Close;
                                dsTicketVentaManual.DataSet.Open;

                                dsObtenerTransitos.DataSet.Open;
                                dsObtenerTransitos.DataSet.Open;
                            finally
                                //Para finalizar asignamos la posici�n.
                                if Assigned(PunteroTVM) then begin
                                    if dsTicketVentaManual.DataSet.BookmarkValid(PunteroTVM) then dsTicketVentaManual.DataSet.GotoBookmark(PunteroTVM);
                                    dsTicketVentaManual.DataSet.FreeBookmark(PunteroTVM);
                                end;
                                if Assigned(PunteroTTO) then begin
                                    if dsObtenerTransitos.DataSet.BookmarkValid(PunteroTTO) then dsObtenerTransitos.DataSet.GotoBookmark(PunteroTTO);
                                    dsObtenerTransitos.DataSet.FreeBookmark(PunteroTTO);
                                end;
                                TPanelMensajesForm.OcultaPanel;
                            end;
                        end;
                    end;
                end
                else
                    MsgBox(VALIDACION_ESTADOS_TRANSITO, Self.Caption, MB_ICONWARNING);
            end
            else
                MsgBox(VALIDACION_PATENTES_DISTINTAS, Self.Caption, MB_ICONWARNING);
            end
            else                                                                                                                                        //SS_976_ALA_20110908
                MsgBox(Format(VALIDACION_TVM_ASIGNADO, [NumCorrCATVM]), Self.Caption, MB_ICONWARNING);                                                  //SS_976_ALA_20110908
        end
        else
            MsgBox('No existen registros para Asociar.', Self.Caption, MB_ICONWARNING);
    except
        on e: exception do begin
                MsgBoxErr(ERROR_AL_ASOCIAR_TRANSITO,
                    Format(DET_ERROR_AL_ASOCIAR_TRANSITO, [NumCorrCA, e.Message]),
                    Self.Caption, MB_ICONERROR);
        end;
    end;
end;

procedure TRevalidarTicketVentaManualForm.btnBuscarClick(Sender: TObject);
begin
    if spObtenerTransitos.Active then spObtenerTransitos.Close;
    if spObtenerTicketsVentaManual.Active then spObtenerTicketsVentaManual.Close;
    
    with spObtenerTicketsVentaManual.Parameters do begin
        Refresh;
        ParamByName('@Patente').Value               := iif(ObtenerPatente = '' , Null, ObtenerPatente);
        ParamByName('@CodigoConcesionaria').Value   := iif(ObtenerCodigoConcesionaria = -1 , Null, ObtenerCodigoConcesionaria);
        ParamByName('@FechaVentaDesde').Value       := iif(ObtenerFechaTVMDesde = nulldate , Null, ObtenerFechaTVMDesde);
        ParamByName('@FechaVentaHasta').Value       := iif(ObtenerFechaTVMHasta = NullDate , Null, ObtenerFechaTVMHasta);
        ParamByName('@EstadoTicketVentaManual').Value := iif(ObtenerEstadoTicketVentaManual = Todos , Null, ObtenerEstadoTicketVentaManual);
    end;
    spObtenerTicketsVentaManual.Open;
end;

procedure TRevalidarTicketVentaManualForm.btnBuscarTransitosClick(
  Sender: TObject);
begin
    spObtenerTicketsVentaManualAfterScroll(nil);
end;

procedure TRevalidarTicketVentaManualForm.btnEditarPatenteClick(
  Sender: TObject);
begin
    EdicionPatente();
end;

procedure TRevalidarTicketVentaManualForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TRevalidarTicketVentaManualForm.BuscarTransitos(FechaVentaTVM: TDateTime);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        
        spObtenerTransitos.Close;
        with spObtenerTransitos.Parameters do begin
            Refresh;
            ParamByName('@FechaDesde').Value    := iif(FechaVentaTVM = nulldate, Null, FormatDateTime('yyyymmdd hh:nn', IncMinute(FechaVentaTVM, (-1 * ObtenerRangoFechaTransito))));
            ParamByName('@FechaHasta').Value    := iif(FechaVentaTVM = nulldate, Null, FormatDateTime('yyyymmdd hh:nn', IncMinute(FechaVentaTVM, ObtenerRangoFechaTransito)));
            ParamByName('@ListaEstados').Value        := iif(ObtenerEstadosTransitos = '', Null, ObtenerEstadosTransitos);
            ParamByName('@CodigoConcesionaria').Value := iif(ObtenerCodigoConcesionaria = -1 , Null, ObtenerCodigoConcesionaria);
            ParamByName('@CantidadTransitos').Value:= 0;
        end;
        spObtenerTransitos.Open;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRevalidarTicketVentaManualForm.chklstEstadosTtosChecked(
  Check: Boolean);
var
  i: Integer;
begin
    for i := 0 to chklstEstadosTtos.Items.Count - 1 do begin
        chklstEstadosTtos.Checked[i] := Check;
    end;
    if not chklstEstadosTtos.Visible then peEstadosTtos.Text := ObtenerEstadosTransitos;
end;
//INICIO BLOQUE SS_976_ALA_20110816
procedure TRevalidarTicketVentaManualForm.dblListadoTransitosCheckLink(
  Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
    if  Column = dblListadoTransitos.Columns[8] then begin
    	IsLink := (spObtenerTransitos.FieldByName('RegistrationAccessibility').AsInteger > 0);
    end;
end;
//TERMINO BLOQUE SS_976_ALA_20110816

procedure TRevalidarTicketVentaManualForm.dblListadoTransitosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column = dblListadoTransitos.Columns[3] then begin                                                                           //SS_976_ALA_20110816
        Text := FormatDateTime('yyyy-mm-dd hh:nn:ss', spObtenerTransitos.FieldByName('FechaHoraTransito').AsDateTime);
    end;
    if Column = dblListadoTransitos.Columns[8] then begin                                                                           //SS_976_ALA_20110816
        ilCamara.Draw(dblListadoTransitos.Canvas, Rect.Left + 20, Rect.Top + 1,                                                     //SS_976_ALA_20110816
                        iif(spObtenerTransitos.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0));                      //SS_976_ALA_20110816
			DefaultDraw := False;                                                                                                   //SS_976_ALA_20110816
			ItemWidth   := ilCamara.Width + 2;                                                                                      //SS_976_ALA_20110816
    end;                                                                                                                            //SS_976_ALA_20110816

end;
//INICIO BLOQUE SS_976_ALA_20110816
procedure TRevalidarTicketVentaManualForm.dblListadoTransitosLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    if  Column = dblListadoTransitos.Columns[8] then begin
    	MostrarVentanaImagen(Self, spObtenerTransitos.FieldByName('NumCorrCA').AsInteger, spObtenerTransitos.FieldByName('FechaHoraTransito').AsDateTime);
    end;
end;
//TERMINO BLOQUE SS_976_ALA_20110816

procedure TRevalidarTicketVentaManualForm.dblListadoTVMCheckLink(
  Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
//permito hacer click en el link o no
    if  Column = dblListadoTVM.Columns[7] then begin
    	IsLink := (spObtenerTicketsVentaManual.FieldByName('NombreArchivoImagen').AsString <> '');
    end;
end;


procedure TRevalidarTicketVentaManualForm.dblListadoTVMDblClick(
  Sender: TObject);
begin
    EdicionPatente();
end;

procedure TRevalidarTicketVentaManualForm.dblListadoTVMDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column = dblListadoTVM.Columns[7] then begin
        ilCamara.Draw(dblListadoTVM.Canvas, Rect.Left + 20, Rect.Top + 1,
                        iif(spObtenerTicketsVentaManual.FieldByName('NombreArchivoImagen').AsString <> '', 1, 0));
			DefaultDraw := False;
			ItemWidth   := ilCamara.Width + 2;
    end;
    if Column = dblListadoTVM.Columns[2] then begin
        Text := FormatDateTime('yyyy-mm-dd hh:nn:ss',spObtenerTicketsVentaManual.FieldByName('FechaHoraVenta').AsDateTime);
    end;    
end;
procedure TRevalidarTicketVentaManualForm.dblListadoTVMLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
var
    IdTransaccionTVM    : Integer;
    NombreImg           : AnsiString;
    Patente             : string;
    FechaVenta          : TDateTime;
    NumCorrCA           : Int64;
    Categoria           : Integer;
    Importe             : Int64;
    f                   : TImagenesTicketVentaManuallForm;
begin
    if Column = dblListadoTVM.Columns[7] then begin
        NombreImg           := spObtenerTicketsVentaManual.FieldByName('NombreArchivoImagen').AsString;
        Patente             := spObtenerTicketsVentaManual.FieldByName('Patente').AsString;
        FechaVenta          := spObtenerTicketsVentaManual.FieldByName('FechaHoraVenta').AsDateTime;
        NumCorrCA           := spObtenerTicketsVentaManual.FieldByName('NumCorrCA').AsInteger;
        Categoria           := spObtenerTicketsVentaManual.FieldByName('Categoria').AsInteger;
        Importe             := spObtenerTicketsVentaManual.FieldByName('Importe').AsInteger;
        IdTransaccionTVM    := spObtenerTicketsVentaManual.FieldByName('ID_Transacciones_TVM').AsInteger;
        Application.CreateForm(TImagenesTicketVentaManuallForm,f);
        if f.Inicializar(IdTransaccionTVM, NombreImg, Importe, NumCorrCA,
            ObtenerNombreCortoConcesionaria(ObtenerCodigoConcesionaria),
            ObtenerNombreConcesionaria, Categoria, Patente, FechaVenta, False) then begin
            f.ShowModal;
        end;
    end;
end;

destructor TRevalidarTicketVentaManualForm.Destroy;
begin
    FreeAndNil(FNombresConcesionarias);
    FreeAndNil(FCodigosEstados);
    inherited;
end;

procedure TRevalidarTicketVentaManualForm.EdicionPatente;
resourcestring
    TVM_ASOCIADO     = 'No puede modificar la patente %s, ya que este Ticket de Venta Manual ya se encuentra asociado a un Transito con NumCorrCA %d';
var
    IdTransaccionTVM    : Integer;
    NombreImg           : AnsiString;
    Patente             : string;
    FechaVenta          : TDateTime;
    NumCorrCA           : Int64;
    Categoria           : Integer;
    Importe             : Int64;
    f                   : TImagenesTicketVentaManuallForm;
    Puntero             : TBookmark;
    AffterScrolEvento   : TDataSetNotifyEvent;
begin
    if not spObtenerTicketsVentaManual.IsEmpty then begin
        NumCorrCA   := spObtenerTicketsVentaManual.FieldByName('NumCorrCA').AsInteger;
        Patente     := spObtenerTicketsVentaManual.FieldByName('Patente').AsString;
        if NumCorrCA = 0 then begin
            NombreImg           := spObtenerTicketsVentaManual.FieldByName('NombreArchivoImagen').AsString;
            IdTransaccionTVM    := spObtenerTicketsVentaManual.FieldByName('ID_Transacciones_TVM').AsInteger;
            FechaVenta          := spObtenerTicketsVentaManual.FieldByName('FechaHoraVenta').AsDateTime;
            Categoria           := spObtenerTicketsVentaManual.FieldByName('Categoria').AsInteger;
            Importe             := spObtenerTicketsVentaManual.FieldByName('Importe').AsInteger;

            //Guardamos la posicion actual del dataset de TVM
            Puntero := dsTicketVentaManual.DataSet.GetBookmark;
            //Guardamos el evento donde se actualiza los datos de los transitos.
            AffterScrolEvento := dsTicketVentaManual.DataSet.AfterScroll;
            //Luego eliminamos el evento, para que no se vuelva a cargar cuando seteemos la posici�n de TVM
            dsTicketVentaManual.DataSet.AfterScroll := nil;

            Application.CreateForm(TImagenesTicketVentaManuallForm,f);
            if f.Inicializar(IdTransaccionTVM, NombreImg, Importe, NumCorrCA,
                ObtenerNombreCortoConcesionaria(ObtenerCodigoConcesionaria),
                ObtenerNombreConcesionaria, Categoria, Patente, FechaVenta, True) then begin
                f.ShowModal;
                TPanelMensajesForm.MuestraMensaje('Actualizando Datos ...', True);
                try
                    dsTicketVentaManual.DataSet.Close;
                    dsTicketVentaManual.DataSet.Open;
                finally
                    if Assigned(Puntero) then begin
                        if dsTicketVentaManual.DataSet.BookmarkValid(Puntero) then dsTicketVentaManual.DataSet.GotoBookmark(Puntero);
                        dsTicketVentaManual.DataSet.FreeBookmark(Puntero);
                        //Volvemos a asignar el evento
                        dsTicketVentaManual.DataSet.AfterScroll := AffterScrolEvento;
                        TPanelMensajesForm.OcultaPanel;
                    end;
                end;
            end;
        end
        else MsgBox(Format(TVM_ASOCIADO, [Patente, NumCorrCA]), Self.Caption, MB_ICONWARNING);
    end
    else MsgBox('No existen registros para Editar', Self.Caption, MB_ICONWARNING);
end;

function TRevalidarTicketVentaManualForm.Inicializar;
resourcestring
    ERROR_INICIALIZANDO     = 'Ha ocurrido un error incializando el formulario';
begin
    try
        try
            chklstEstadosTtos.Visible := False;
            FNombresConcesionarias := TDiccionario.Create;
            vcbConcesionarias.Items.Clear;
            spObtenerConcesionarias.Parameters.Refresh;
            spObtenerConcesionarias.Open;
            while not spObtenerConcesionarias.Eof do begin
                vcbConcesionarias.Items.Add(spObtenerConcesionarias.FieldByName('Descripcion').AsString,
                                            spObtenerConcesionarias.FieldByName('CodigoConcesionaria').AsInteger);

                FNombresConcesionarias.Add(spObtenerConcesionarias.FieldByName('CodigoConcesionaria').AsString,
                                            spObtenerConcesionarias.FieldByName('NombreCorto').AsString);
                spObtenerConcesionarias.Next;
            end;
            spObtenerConcesionarias.Close;

            FCodigosEstados := TStringList.Create;
            chklstEstadosTtos.Items.Clear;
            spObtenerTransitosEstados.Parameters.Refresh;
            spObtenerTransitosEstados.Open;
            while not spObtenerTransitosEstados.Eof do begin
                chklstEstadosTtos.Items.Add(spObtenerTransitosEstados.FieldByName('Estado').AsString + ' ' +
                                            Trim(spObtenerTransitosEstados.FieldByName('Descripcion').AsString));
                FCodigosEstados.Add(spObtenerTransitosEstados.FieldByName('Estado').AsString);

                //Por defecto chequeamos los estados 2, 3 y 10
                if  (spObtenerTransitosEstados.FieldByName('Estado').AsString = '2') or
                    (spObtenerTransitosEstados.FieldByName('Estado').AsString = '3') or
                    (spObtenerTransitosEstados.FieldByName('Estado').AsString = '10') then begin

                    chklstEstadosTtos.Checked[chklstEstadosTtos.Items.Count - 1] := True;
                end;

                spObtenerTransitosEstados.Next;
            end;
            vcbConcesionarias.ItemIndex := 0;
            vcbEstado.ItemIndex := 2;
            //Para finalizar llamamos al metodo ObtenerEstadosTransitos que pegar� los estados en el txt
            peEstadosTtos.Text := ObtenerEstadosTransitos;
        except
		on e: exception do begin
                MsgBoxErr(ERROR_INICIALIZANDO, e.Message, Self.Caption, MB_ICONERROR);
                Result := false;
			    Exit;
    		end;
        end;
        Result := true;
    finally
        
    end;
end;

function TRevalidarTicketVentaManualForm.ObtenerCodigoConcesionaria: Integer;
begin
    if vcbConcesionarias.ItemIndex > -1 then begin
        Result := vcbConcesionarias.Items[vcbConcesionarias.ItemIndex].Value;
    end
    else Result := -1;
end;

function TRevalidarTicketVentaManualForm.ObtenerNombreConcesionaria: string;
begin
    if vcbConcesionarias.ItemIndex > -1 then begin
        Result := vcbConcesionarias.Items[vcbConcesionarias.ItemIndex].Caption;
    end
    else Result := '';
end;

function TRevalidarTicketVentaManualForm.ObtenerNombreCortoConcesionaria(CodigoConcesionaria : Integer): string;
begin
    Result:= FNombresConcesionarias.ObtenerValor(IntToStr(CodigoConcesionaria));
end;

function TRevalidarTicketVentaManualForm.ObtenerEstadosTransitos: string;
var
    i       : Integer;
    Codigos : string;
begin
    Codigos := '';
    for i := 0 to chklstEstadosTtos.Items.Count - 1 do begin
        if chklstEstadosTtos.Checked[i] then begin
            if Codigos = '' then Codigos := FCodigosEstados[i]
            else Codigos := Codigos + ', '+ FCodigosEstados[i];
        end;
    end;
    Result := Codigos;
end;

function TRevalidarTicketVentaManualForm.ObtenerEstadoTicketVentaManual: EstadoTicketVentaManual;
begin
    case vcbEstado.ItemIndex of
      0: Result := NoAsignado;
      1: Result := Asignado;
      2: Result := Todos;
    end;    
end;

function TRevalidarTicketVentaManualForm.ObtenerFechaTVMDesde: TDateTime;
begin
    if deDesde.Date <> NullDate then begin
        Result := StrToDateTime(DateToStr(deDesde.Date) + ' ' + TimeToStr(edtHoraVentaDesde.Time));
    end
    else
        Result := NullDate;
end;

function TRevalidarTicketVentaManualForm.ObtenerFechaTVMHasta: TDateTime;
begin
    if deHasta.Date <> NullDate then begin
        Result := StrToDateTime(DateToStr(deHasta.Date) + ' ' + TimeToStr(edtHoraVentaHasta.Time));
    end
    else
        Result := NullDate;
end;

function TRevalidarTicketVentaManualForm.ObtenerPatente: String;
begin
    Result := edtPatente.Text;
end;

function TRevalidarTicketVentaManualForm.ObtenerRangoFechaTransito: Integer;
begin
    Result := edtRangoFecha.ValueInt;
end;
//INICIO BLOQUE SS_976_ALA_20110816
procedure TRevalidarTicketVentaManualForm.OcultarListaEstados(
  Sender: TObject);
begin
    peEstadosTtos.Text := ObtenerEstadosTransitos;
    chklstEstadosTtos.Visible := False;
end;
//TERMINO BLOQUE SS_976_ALA_20110816

procedure TRevalidarTicketVentaManualForm.peEstadosTtosButtonClick(
  Sender: TObject);
begin
    chklstEstadosTtos.Left  := peEstadosTtos.Left;
    chklstEstadosTtos.Top   := peEstadosTtos.Top + 19;

    chklstEstadosTtos.Visible := not chklstEstadosTtos.Visible;
    if not chklstEstadosTtos.Visible then peEstadosTtos.Text := ObtenerEstadosTransitos;
    if chklstEstadosTtos.Visible then chklstEstadosTtos.SetFocus;                                                   //SS_976_ALA_20110818
end;

procedure TRevalidarTicketVentaManualForm.spObtenerTicketsVentaManualAfterScroll(
  DataSet: TDataSet);
var
    FechaVentaTVM   : TDateTime;
begin
    if not spObtenerTicketsVentaManual.IsEmpty then begin
        FechaVentaTVM   := spObtenerTicketsVentaManual.FieldByName('FechaHoraVenta').AsDateTime;
        BuscarTransitos(FechaVentaTVM);
    end;
end;

procedure TRevalidarTicketVentaManualForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if FormStyle <> fsNormal then Action :=  caFree;
end;

end.
