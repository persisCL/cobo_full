{
Revision    : 1
Date        : 07-Junio-2011
Author      : Nelson Droguett Sierra
Description : (Ref.Fase2) SS-377
              Eliminar el trabajo con "imagenes padre" y trabajar las imagenes
              en forma individual
              Se agrega la unit UtilDB
              Se agrega la variable FImagePathNFI
              function Inicializar
                Se obtienen los valores de los parametrosgenerales de las nuevas rutas.
              function CargarImagen
                Se agrega variable nombrecorto que se obtiene de un QueryGetValue
                Se envia esta variable como parametro en las llamadas a la funcion ObtenerImagenTransito
              procedure FormCreate
                Se obtienen los valores de los parametrosgenerales de las nuevas rutas.
              function CargarImagenOverview
                Se agrega variable nombrecorto que se obtiene de un QueryGetValue
                Se envia esta variable como parametro en las llamadas a la funcion ObtenerImagenTransito
Firma       : SS-377-NDR-20110607

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.
              Agrega una sobrecarga al m�todo MostrarVentanaImagen

Author      :   Claudio Quezada Ib��ez
Date        :   28-Noviembre-2013
Firma       :   SS_1138_CQU_20131127
Descripcion :   Se quita la m�scara al cargar las im�genes.
                Se redimensionan las im�genes italianas
                Se agregan las funcionalidades de para los botones F1, F2, F3 y F4
                Se agregan las imagenes laterales.

Author      :   Claudio Quezada Ib��ez
Date        :   24-Julio-2014
Firma       :   SS_1203_CQU_20140724
Descripcion :   Se despliega la ventana, en el caso de los italianos, cuando exista almenos una imagen.

Author      :   Claudio Quezada Ib��ez
Date        :   15-Diciembre-2014
Firma		:   SS_1147W_CQU_20141215
Descripcion	:   Se agregan los bit  FEsKapschImagenActual, FEsKapschFrontal, FEsKapschFrontal2,
                FEsKapschPosterior, FEsKapschPosterior2 para indicar si la imagen es TJPEGPlusImage
                con ello se pretende mantener el comportamiento independiente de la imagen.
                Se hizo as� ya que el valor "EsItaliano" �no sirve en el caso de VS porque ellos
                usan otro proveedor de p�rticos el cual tambi�n trabaja con im�genes normales.

Author      :   Claudio Quezada Ib��ez
Date        :   19-Octubre-2015
Firma       :   SS_1382_CQU_20151019
Descripcion :   Se permite la habilitaci�n y deshabilitaci�n de la m�scara, independiente de si es Italiano.
                Se agrega la funcionalidad F1, F2, F3 y F4 tal cual en Infracciones. (AjustarTama�oTImagePlus)

}
unit frmVisorImagenes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, UtilImg, imgTypes, JpegPlus, ImgProcs,
  ConstParametrosGenerales, DMConnection, Filtros, UtilProc, DB, ADODB,
  StdCtrls, ImgList, ImagePlus, ToolWin, ActnList, XPStyleActnCtrls, ActnMan,
  Buttons,																			// SS_1138_CQU_20131127
  Math, printers, formImagenOverview, Menus, UtilDB;

type
  TfrmVisorImagenes = class(TForm)
    ImageList1: TImageList;
    ActionManager: TActionManager;
    actZoomIn: TAction;
    actZoomOut: TAction;
    actZoomReset: TAction;
    actZoomPlate: TAction;
    actBrightPlus: TAction;
    actBrigthLess: TAction;
    actBrigthReset: TAction;
    actImageOverview: TAction;
    actCenterToPlate: TAction;
    actResetImage: TAction;
    actUltimoValidado: TAction;
    actDobleValidacion: TAction;
    actAceptarValidacion: TAction;
    actNoIdentificable: TAction;
    actSalir: TAction;
    actIgnorarValidacion: TAction;
    actBuscarIgnorado: TAction;
    actAlmacenarImagenReferencia: TAction;
    actRemarcarBordes: TAction;
    actSuavizarBordes: TAction;
    actImagenOverview1: TAction;
    actImagenOverview2: TAction;
    actImagenOverview3: TAction;
    actImagenFrontal1: TAction;
    actImagenFrontal2: TAction;
    actImagenPosterior1: TAction;
    actImagenPosterior2: TAction;
    actZoom: TAction;
    actHerram: TAction;
    actFiltros: TAction;
    actLimpiarGreenFlag: TAction;
    actVerMascara: TAction;
    act_HabilitarCategoriaAValidar: TAction;
    CoolBar1: TCoolBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton12: TToolButton;
    ToolButton7: TToolButton;
    tbImagenOverview: TToolButton;
    ImagenAmpliar: TImagePlus;
    ToolButton6: TToolButton;
    acImprimirImagen: TAction;
    DialogoImpresora: TPrintDialog;
    pmImagenOverview: TPopupMenu;
    miImagenOverview1: TMenuItem;
    miImagenOverview2: TMenuItem;
    miImagenOverview3: TMenuItem;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lblPatente: TLabel;
    Label2: TLabel;
    LblTelevia: TLabel;
    Label4: TLabel;
    LblCategoria: TLabel;
    Label6: TLabel;
    LblEstado: TLabel;
    CheckBox1: TCheckBox;
    pnlThumb: TPanel;                                                           // SS_1138_CQU_20131127
    sbThumb0: TSpeedButton;                                                     // SS_1138_CQU_20131127
    sbThumb1: TSpeedButton;                                                     // SS_1138_CQU_20131127
    sbThumb2: TSpeedButton;                                                     // SS_1138_CQU_20131127
    sbThumb5: TSpeedButton;                                                     // SS_1138_CQU_20131127
    sbThumb4: TSpeedButton;                                                     // SS_1138_CQU_20131127
    sbThumb3: TSpeedButton;                                                     // SS_1138_CQU_20131127
    sbThumb6: TSpeedButton;                                                     // SS_1138_CQU_20131127
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actZoomInExecute(Sender: TObject);
    procedure actZoomOutExecute(Sender: TObject);
    procedure actZoomResetExecute(Sender: TObject);
    procedure actZoomPlateExecute(Sender: TObject);
    procedure actBrightPlusExecute(Sender: TObject);
    procedure actBrigthResetExecute(Sender: TObject);
	procedure actBrigthLessExecute(Sender: TObject);
    procedure actRemarcarBordesExecute(Sender: TObject);
    procedure actSuavizarBordesExecute(Sender: TObject);
  	procedure actMaskExecute(Sender: TObject);
    procedure actImageOverviewExecute(Sender: TObject);
    procedure actCenterToPlateExecute(Sender: TObject);
    procedure actResetImageExecute(Sender: TObject);
    procedure acImprimirImagenExecute(Sender: TObject);
    procedure actVerMascaraExecute(Sender: TObject);
    procedure sbThumb0Click(Sender: TObject);                                   // SS_1138_CQU_20131127
    procedure sbThumb1Click(Sender: TObject);                                   // SS_1138_CQU_20131127
    procedure sbThumb2Click(Sender: TObject);                                   // SS_1138_CQU_20131127
    procedure sbThumb3Click(Sender: TObject);                                   // SS_1138_CQU_20131127
    procedure sbThumb4Click(Sender: TObject);                                   // SS_1138_CQU_20131127
    procedure sbThumb5Click(Sender: TObject);                                   // SS_1138_CQU_20131127
    procedure sbThumb6Click(Sender: TObject);                                   // SS_1138_CQU_20131127
  private
    FNumCorrCo: int64;
    FPortico: string;
    FRegistrationAccessibility: integer;
    FImagenActual: TJPEGPlusImage;
    FImagenActualItaliana : TBitmap;    // SS_1091_CQU_20130516
    FJPGItalianoFrontal,                // SS_1138_CQU_20131127
	FJPGItalianoFrontal2,               // SS_1138_CQU_20131127
	FJPGItalianoPosterior,              // SS_1138_CQU_20131127
	FJPGItalianoPosterior2: TBitmap;    // SS_1138_CQU_20131127
    FJPG12Frontal,                      // SS_1138_CQU_20131127
	FJPG12Frontal2,                     // SS_1138_CQU_20131127
	FJPG12Posterior,                    // SS_1138_CQU_20131127
	FJPG12Posterior2: TJPEGPlusImage;   // SS_1138_CQU_20131127
    FFechaHora: TDateTime;
    //Posicion de la patente dentro de la imagen.
    FPositionPlate: TRect;
    FImagePath, FImagePathNFI: AnsiString;                                      //SS-377-NDR-20110607
    FShiftOriginal: Integer;
    FFrmImagenOverview: TFrmImageOverview;
    FImgOverview: TBitMap;
	FImgOverview2,                      // SS_1138_CQU_20131127
	FImgOverview3: TBitMap;             // SS_1138_CQU_20131127
    FFormOverviewCargado: Boolean;
    FTipoImagenOverview: TTipoImagen;
    FEsItaliano : Boolean;              // SS_1091_CQU_20130516
    FEsKapschImagenActual,              // SS_1147W_CQU_20141215
    FEsKapschFrontal,                   // SS_1147W_CQU_20141215
	FEsKapschFrontal2,                  // SS_1147W_CQU_20141215
	FEsKapschPosterior,                 // SS_1147W_CQU_20141215
	FEsKapschPosterior2 : Boolean;      // SS_1147W_CQU_20141215

    function CargarImagen(NumCorrCo: Int64; RegistrationAccessibility: integer; FechaHora: TDateTime): Boolean; overload;                           // SS_1091_CQU_20130516
    function CargarImagen(NumCorrCo: Int64; RegistrationAccessibility: integer; FechaHora: TDateTime; EsItaliano : Boolean) : Boolean; overload;    // SS_1091_CQU_20130516
    function Inicializar(Patente, Televia, Categoria, Estado, Portico: string): Boolean;
    function CargarImagenOverview: Boolean;
    procedure Redibujar(VerMascara: Boolean);
    procedure CentrarImageninicial;
    procedure CentrarImagen;
    procedure HabilitarHerramientasImagen;
    procedure PrintImage(Image: TImagePlus; ZoomPercent: Integer);
    Procedure MostrarImagenBoton(btn: TSpeedButton; aGraphic: TGraphic; bmp: TBitMap);  // SS_1138_CQU_20131127
    Procedure NoMostrarImagenBoton(btn: TSpeedButton);                                  // SS_1138_CQU_20131127
    procedure SeleccionarImagen(aGraphic: TGraphic; aTipoImagen: TTipoImagen);          // SS_1138_CQU_20131127
  public
  end;

//function MostrarVentanaImagen(AOwner: TObject; NumCorrCo: Int64; RegistrationAccessibility: integer;                  // SS_1091_CQU_20130516
//    FechaHora: TDateTime; Patente, Televia, Estado, Categoria, Portico: string): Boolean;                             // SS_1091_CQU_20130516
function MostrarVentanaImagen(AOwner: TObject; NumCorrCo: Int64; RegistrationAccessibility: integer;                    // SS_1091_CQU_20130516
    FechaHora: TDateTime; Patente, Televia, Estado, Categoria, Portico: string; EsItaliano : Boolean = False): Boolean; // SS_1091_CQU_20130516
function VentanaImagenVisible: Boolean;
procedure CerrarVentanaImagen(AOwner: TObject);

implementation

{$R *.dfm}

uses Util;

Var
    GVentana: TfrmVisorImagenes = nil;
    GOwner: TObject = nil;

//function MostrarVentanaImagen(AOwner: TObject; NumCorrCo: Int64; RegistrationAccessibility: integer;
//    FechaHora: TDateTime; Patente, Televia, Estado, Categoria, Portico: string): Boolean;
function MostrarVentanaImagen(AOwner: TObject; NumCorrCo: Int64; RegistrationAccessibility: integer;            // SS_1091_CQU_20130516
    FechaHora: TDateTime; Patente, Televia, Estado, Categoria, Portico: string; EsItaliano : Boolean): Boolean; // SS_1091_CQU_20130516
resourcestring
	MSG_NO_IMAGES = 'Im�genes del tr�nsito no disponibles';
begin
	  GOwner := AOwner;
	  if not Assigned(GVentana) then begin
        Application.CreateForm(TfrmVisorImagenes, GVentana);
        Result := GVentana.Inicializar(Patente, Televia, Categoria, Estado, Portico);
        //Result := Result and GVentana.CargarImagen(NumCorrCo, RegistrationAccessibility, FechaHora);          // SS_1091_CQU_20130516
        Result := Result and GVentana.CargarImagen(NumCorrCo, RegistrationAccessibility, FechaHora, EsItaliano);// SS_1091_CQU_20130516
    end
    else
	    //Result := GVentana.CargarImagen(NumCorrCo, RegistrationAccessibility, FechaHora);                     // SS_1091_CQU_20130516
        Result := GVentana.CargarImagen(NumCorrCo, RegistrationAccessibility, FechaHora, EsItaliano);           // SS_1091_CQU_20130516
	  if not Result then MsgBox(MSG_NO_IMAGES, 'Im�genes Tr�nsito', MB_ICONINFORMATION);
end;

function VentanaImagenVisible: Boolean;
begin
    Result := Assigned(GVentana);
end;

procedure CerrarVentanaImagen(AOwner: TObject);
begin
    if Assigned(GVentana) and (AOwner = GOwner) and
      not (csDestroying in GVentana.COmponentState)
      then begin
        GVentana.Release;
        //GVentana.Close;
      end;
end;

{ TfrmImagenesTransitos }

procedure TfrmVisorImagenes.HabilitarHerramientasImagen;
begin
    //if not FEsItaliano then begin                                   // SS_1147W_CQU_20141215  // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                               // SS_1147W_CQU_20141215

        actZoom.Enabled 	:= not FImagenActual.Empty;
        actHerram.Enabled 	:= not FImagenActual.Empty;
        actFiltros.Enabled 	:= not FImagenActual.Empty;
        //actVerMascara.Enabled 	:= not FImagenActual.Empty;
        actZoom.Visible     := not FImagenActual.Empty;
        actFiltros.Visible  := not FImagenActual.Empty;
        actHerram.Visible   := not FImagenActual.Empty;
        actVerMascara.Visible := not FImagenActual.Empty;
    end else begin                                                  // SS_1091_CQU_20130516
        actZoom.Enabled 	:= not FImagenActualItaliana.Empty;     // SS_1091_CQU_20130516
        actHerram.Enabled 	:= not FImagenActualItaliana.Empty;     // SS_1091_CQU_20130516
        actFiltros.Enabled 	:= not FImagenActualItaliana.Empty;     // SS_1091_CQU_20130516
        actZoom.Visible     := not FImagenActualItaliana.Empty;     // SS_1091_CQU_20130516
        actFiltros.Visible  := not FImagenActualItaliana.Empty;     // SS_1091_CQU_20130516
        actHerram.Visible   := not FImagenActualItaliana.Empty;     // SS_1091_CQU_20130516
        actVerMascara.Visible := not FImagenActualItaliana.Empty;   // SS_1091_CQU_20130516
    end;                                                            // SS_1091_CQU_20130516
    // Habilitamos el form de imagenes overview
    if actImageOverview.Checked then
        FFrmImagenOverview.Show;
    actImageOverviewExecute(nil);
end;

function TfrmVisorImagenes.Inicializar(Patente, Televia, Categoria, Estado, Portico: string): Boolean;
begin
    result := False;
    FFrmImagenOverview := TfrmImageOverview.create(self);
    FFrmImagenOverview.Parent := self;
    FImgOverview :=  TBitMap.Create;
    FImgOverview.PixelFormat := pf24bit;
    FFrmImagenOverview.Left := self.ScreenToClient(Self.ClientToScreen(Point(ImagenAmpliar.Left, 0))).X + (ImagenAmpliar.Width - FFrmImagenOverview.Width + 1) - 17;
    FFrmImagenOverview.top :=  self.ScreenToClient(Self.ClientToScreen(Point(0, ImagenAmpliar.top))).Y + 2;
    FFormOverviewCargado := False;
    //result := ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS, FImagePath);                               //SS-377-NDR-20110607
    result := ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN, FImagePathNFI);         //SS-377-NDR-20110607
    FImagenActual := TJPEGPlusImage.Create;
    FImagenActualItaliana := TBitMap.Create;    // SS_1091_CQU_20130516
    LblPatente.Caption := iif((Patente = ''), '[sin datos]', Patente);
    LblTelevia.Caption := iif((Televia = ''), '[sin datos]', Televia);
    LblCategoria.Caption := iif((Categoria = ''), '[sin datos]', Categoria);
    LblEstado.Caption := iif((Estado = ''), '[sin datos]', Estado);
    FPortico := Portico;
    // Inicializo las variables de im�genes                                     // SS_1138_CQU_20131127
	if Assigned(FJPG12Frontal)         then FreeAndNil(FJPG12Frontal);          // SS_1138_CQU_20131127
	if Assigned(FJPG12Frontal2)         then FreeAndNil(FJPG12Frontal2);        // SS_1138_CQU_20131127
	if Assigned(FJPG12Posterior)        then FreeAndNil(FJPG12Posterior);       // SS_1138_CQU_20131127
	if Assigned(FJPG12Posterior2)       then FreeAndNil(FJPG12Posterior2);      // SS_1138_CQU_20131127
	if Assigned(FImgOverview2)          then FreeAndNil(FImgOverview2);         // SS_1138_CQU_20131127
	if Assigned(FImgOverview3)          then FreeAndNil(FImgOverview3);         // SS_1138_CQU_20131127
    if Assigned(FImagenActual)          then FreeAndNil(FImagenActual);         // SS_1138_CQU_20131127
    if Assigned(FImagenActualItaliana)  then FreeAndNil(FImagenActualItaliana); // SS_1138_CQU_20131127
    if Assigned(FJPGItalianoFrontal)    then FreeAndNil(FJPGItalianoFrontal);   // SS_1138_CQU_20131127
	if Assigned(FJPGItalianoFrontal2)   then FreeAndNil(FJPGItalianoFrontal2);  // SS_1138_CQU_20131127
	if Assigned(FJPGItalianoPosterior)  then FreeAndNil(FJPGItalianoPosterior); // SS_1138_CQU_20131127
	if Assigned(FJPGItalianoPosterior2) then FreeAndNil(FJPGItalianoPosterior2);// SS_1138_CQU_20131127
    FImagenActual                       := TJPEGPlusImage.Create;               // SS_1138_CQU_20131127
    FJPG12Frontal                       := TJPEGPlusImage.Create;               // SS_1138_CQU_20131127
    FJPG12Frontal2                      := TJPEGPlusImage.Create;               // SS_1138_CQU_20131127
    FJPG12Posterior                     := TJPEGPlusImage.Create;               // SS_1138_CQU_20131127
    FJPG12Posterior2                    := TJPEGPlusImage.Create;               // SS_1138_CQU_20131127
    FImgOverview2                       := TBitMap.Create;                      // SS_1138_CQU_20131127
    FImgOverview2.PixelFormat           := pf24bit;                             // SS_1138_CQU_20131127
    FImgOverview3                       := TBitMap.Create;                      // SS_1138_CQU_20131127
    FImgOverview3.PixelFormat           := pf24bit;                             // SS_1138_CQU_20131127
    FImagenActualItaliana               := TBitMap.Create;                      // SS_1138_CQU_20131127
    FJPGItalianoFrontal                 := TBitMap.Create;                      // SS_1138_CQU_20131127
    FJPGItalianoFrontal2                := TBitMap.Create;                      // SS_1138_CQU_20131127
    FJPGItalianoPosterior               := TBitMap.Create;                      // SS_1138_CQU_20131127
    FJPGItalianoPosterior2              := TBitMap.Create;                      // SS_1138_CQU_20131127
    FEsKapschImagenActual               := True;                                // SS_1147W_CQU_20141215
    FEsKapschFrontal                    := True;                                // SS_1147W_CQU_20141215
    FEsKapschFrontal2                   := True;                                // SS_1147W_CQU_20141215
    FEsKapschPosterior                  := True;                                // SS_1147W_CQU_20141215
    FEsKapschPosterior2                 := True;                                // SS_1147W_CQU_20141215
end;

function TfrmVisorImagenes.CargarImagen(NumCorrCo: Int64; RegistrationAccessibility: integer; FechaHora: TDateTime): Boolean;
resourcestring
    CAPTION_IMAGEN_TRANSITO = 'Imagenes del tr�nsito (Patente: %s - Fecha y Hora: %s)';
    CAPTION_IMAGEN_TRANSITO_DEFAULT = 'Imagenes del tr�nsito';
var
  	Error: TTipoErrorImg;
    JPG12: TJPEGPlusImage;
    ImgOverview: TBitMap;
    DescriError, NombreCorto: AnsiString;                                       //SS-377-NDR-20110607
    DataImage: TDataImage;
    bmp: TBitMap;                                                               // SS_1138_CQU_20131127
    ImagenTMP: TObject;															// SS_1147W_CQU_20141215
begin
    Result := False;
    FNumCorrCo := NumCorrCo;
    FFechaHora := FechaHora;
    FRegistrationAccessibility := RegistrationAccessibility;
    bmp := TBitMap.Create;                                                      // SS_1138_CQU_20131127
    NombreCorto := QueryGetValue(DMConnections.BaseCAC,Format('SELECT Concesionarias.NombreCorto FROM Transitos (NOLOCK) INNER JOIN Concesionarias (NOLOCK) ON Concesionarias.CodigoConcesionaria=Transitos.CodigoConcesionaria WHERE NumCorrCA=%d', [NumCorrCo]));        //SS-377-NDR-20110607

    ImagenTMP := TObject.Create;                                                                                                                            // SS_1147W_CQU_20141215
    if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora, tifrontal, ImagenTMP, DataImage, DescriError, Error, FEsKapschFrontal) then// SS_1147W_CQU_20141215
    begin                                                                                                                                                   // SS_1147W_CQU_20141215
        FPositionPlate := Rect(                                                                                                                             // SS_1147W_CQU_20141215
                                Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                            // SS_1147W_CQU_20141215
                                Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                           // SS_1147W_CQU_20141215
                                Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                          // SS_1147W_CQU_20141215
                                Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                          // SS_1147W_CQU_20141215
        (* Guardamos la primer imagen VR cargada como la imagen actual y habilitamos los controles para inicilizar el visor en el TAB inicio. *)            // SS_1147W_CQU_20141215
        actImagenFrontal1.Enabled := true;                                                                                                                  // SS_1147W_CQU_20141215
        actImagenFrontal1.Checked := true;                                                                                                                  // SS_1147W_CQU_20141215
        actResetImage.Enabled := True;                                                                                                                      // SS_1147W_CQU_20141215
        actVerMascara.Enabled := True;                                                                                                                      // SS_1147W_CQU_20141215

        if FEsKapschFrontal then begin                                                                                                                      // SS_1147W_CQU_20141215
            FJPG12Frontal := (ImagenTMP as TJPEGPlusImage);                                                                                                 // SS_1147W_CQU_20141215
            MostrarImagenBoton(sbThumb0, FJPG12Frontal, bmp);                                                                                               // SS_1147W_CQU_20141215
        end else begin                                                                                                                                      // SS_1147W_CQU_20141215
            FJPGItalianoFrontal := (ImagenTMP as TBitmap);                                                                                                  // SS_1147W_CQU_20141215
            MostrarImagenBoton(sbThumb0, FJPGItalianoFrontal, bmp);                                                                                         // SS_1147W_CQU_20141215
        end;                                                                                                                                                // SS_1147W_CQU_20141215
        sbThumb0.Click;                                                                                                                                     // SS_1147W_CQU_20141215
        CentrarImagenInicial;                                                                                                                               // SS_1147W_CQU_20141215
        CargarImagenOverview;                                                                                                                               // SS_1147W_CQU_20141215
        HabilitarHerramientasImagen;                                                                                                                        // SS_1147W_CQU_20141215
        Result := True;                                                                                                                                     // SS_1147W_CQU_20141215
        if Result and not Visible then Show;                                                                                                                // SS_1147W_CQU_20141215
    end else                                                                                                                                                // SS_1147W_CQU_20141215
        NoMostrarImagenBoton(sbThumb0);                                                                                                                     // SS_1147W_CQU_20141215

    if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora, tiFrontal2, ImagenTMP, DataImage, DescriError, Error, FEsKapschFrontal2)   // SS_1147W_CQU_20141215
    then begin                                                                                                                                              // SS_1147W_CQU_20141215
        if FEsKapschFrontal2 then begin                                                                                                                     // SS_1147W_CQU_20141215
            FJPG12Frontal2 := (ImagenTMP as TJPEGPlusImage);                                                                                                // SS_1147W_CQU_20141215
            MostrarImagenBoton(sbThumb1, FJPG12Frontal2, bmp);                                                                                              // SS_1147W_CQU_20141215
        end else begin                                                                                                                                      // SS_1147W_CQU_20141215
            FJPGItalianoFrontal2 := (ImagenTMP as TBitmap);                                                                                                 // SS_1147W_CQU_20141215
            MostrarImagenBoton(sbThumb1, FJPGItalianoFrontal2, bmp);                                                                                        // SS_1147W_CQU_20141215
        end;                                                                                                                                                // SS_1147W_CQU_20141215
    end else NoMostrarImagenBoton(sbThumb1);                                                                                                                // SS_1147W_CQU_20141215

    //if FEsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora, tifrontal	                                            // SS_1147W_CQU_20141215    // SS_1091_CQU_20130516
	//	//, FImagenActualItaliana, DataImage, DescriError, Error) then begin									                                            // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //    , FJPGItalianoFrontal, DataImage, DescriError, Error) then begin										                                            // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //        FPositionPlate := Rect(                                                                                                                       // SS_1147W_CQU_20141215
    //                Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                      // SS_1147W_CQU_20141215
    //                Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                     // SS_1147W_CQU_20141215
    //                Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                    // SS_1147W_CQU_20141215
    //                Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                    // SS_1147W_CQU_20141215
    //            (* Guardamos la primer imagen VR cargada como la imagen actual                                                                            // SS_1147W_CQU_20141215
    //            y habilitamos los controles para inicilizar el visor en el TAB                                                                            // SS_1147W_CQU_20141215
    //            inicio. *)                                                                                                                                // SS_1147W_CQU_20141215
    //          actImagenFrontal1.Enabled := true;                                                                                                          // SS_1147W_CQU_20141215
    //          actImagenFrontal1.Checked := true;                                                                                                          // SS_1147W_CQU_20141215
    //          actResetImage.Enabled := True;                                                                                                              // SS_1147W_CQU_20141215
    //          actVerMascara.Enabled := True;                                                                                                              // SS_1147W_CQU_20141215
    //          //Redibujar(True);                                                                                                                          // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //          MostrarImagenBoton(sbThumb0, FJPGItalianoFrontal, bmp);                                                                                     // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //          sbThumb0.Click;                                                                                                                             // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //          CentrarImagenInicial;                                                                                                                       // SS_1147W_CQU_20141215
    //          CargarImagenOverview;                                                                                                                       // SS_1147W_CQU_20141215
    //          HabilitarHerramientasImagen;                                                                                                                // SS_1147W_CQU_20141215
    //          Result := True;                                                                                                                             // SS_1147W_CQU_20141215
    //          if Result and not Visible then Show;                                                                                                        // SS_1147W_CQU_20141215
    //end else																								                                                // SS_1147W_CQU_20141215    // SS_1091_CQU_20130516
	////if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora, tifrontal, FImagenActual, DataImage,//FJPG12Frontal, DataImage,	    // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127	//SS-377-NDR-20110607
    //if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora, tifrontal, FJPG12Frontal, DataImage,									    // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //            DescriError, Error) then begin                                                                                                            // SS_1147W_CQU_20141215
    //            // Almacenamos el Rect de la patente                                                                                                      // SS_1147W_CQU_20141215
    //            // Left Top Right Bottom                                                                                                                  // SS_1147W_CQU_20141215
    //FPositionPlate := Rect(                                                                                                                               // SS_1147W_CQU_20141215
    //                Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                      // SS_1147W_CQU_20141215
    //                Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                     // SS_1147W_CQU_20141215
    //                Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                    // SS_1147W_CQU_20141215
    //                Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                    // SS_1147W_CQU_20141215
    //            (* Guardamos la primer imagen VR cargada como la imagen actual                                                                            // SS_1147W_CQU_20141215
    //            y habilitamos los controles para inicilizar el visor en el TAB                                                                            // SS_1147W_CQU_20141215
    //            inicio. *)                                                                                                                                // SS_1147W_CQU_20141215
    //  actImagenFrontal1.Enabled := true;                                                                                                                  // SS_1147W_CQU_20141215
    //  actImagenFrontal1.Checked := true;                                                                                                                  // SS_1147W_CQU_20141215
    //  actResetImage.Enabled := True;                                                                                                                      // SS_1147W_CQU_20141215
    //  actVerMascara.Enabled := True;                                                                                                                      // SS_1147W_CQU_20141215
    //  //Redibujar(True);                                                                                                                                  // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //  MostrarImagenBoton(sbThumb0, FJPG12Frontal, bmp);                                                                                                   // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //  sbThumb0.Click;                                                                                                                                     // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //  CentrarImagenInicial;                                                                                                                               // SS_1147W_CQU_20141215
    //  CargarImagenOverview;                                                                                                                               // SS_1147W_CQU_20141215
    //  HabilitarHerramientasImagen;                                                                                                                        // SS_1147W_CQU_20141215
    //  Result := True;                                                                                                                                     // SS_1147W_CQU_20141215
    //  if Result and not Visible then Show;                                                                                                                // SS_1147W_CQU_20141215
    ////end;                                                                                                                                                // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //end else                                                                                                                                              // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //    NoMostrarImagenBoton(sbThumb0);                                                                                                                   // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127

    //// Intento obtener la imagen tiFrontal2                                                                                                               // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //if FEsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto,                                                                                  // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //    FNumCorrCO, FFechaHora, tiFrontal2, FJPGItalianoFrontal2, DataImage,                                                                              // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //    DescriError, Error)                                                                                                                               // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //then MostrarImagenBoton(sbThumb1, FJPGItalianoFrontal2, bmp)                                                                                          // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //else if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO,                                                                                 // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //    FFechaHora, tiFrontal2, FJPG12Frontal2, DataImage, DescriError, Error)                                                                            // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //then MostrarImagenBoton(sbThumb1, FJPG12Frontal2, bmp)                                                                                                // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //else NoMostrarImagenBoton(sbThumb1);                                                                                                                  // SS_1147W_CQU_20141215    // SS_1147W_CQU_20141215  // SS_1138_CQU_20131127

    // Intento obtener la imagen tiOverview1                                    // SS_1138_CQU_20131127
    if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora,// SS_1138_CQU_20131127
        tiOverview1, FImgOverview, DataImage, DescriError, Error)               // SS_1138_CQU_20131127
    then MostrarImagenBoton(sbThumb2, FImgOverview, bmp)                        // SS_1138_CQU_20131127
    else begin                                                                  // SS_1138_CQU_20131127
        NoMostrarImagenBoton(sbThumb2);                                         // SS_1138_CQU_20131127
        actImagenOverview1.Enabled := False;                                    // SS_1138_CQU_20131127
    end;                                                                        // SS_1138_CQU_20131127

    // Intento obtener la imagen tiOverview2                                    // SS_1138_CQU_20131127
    if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora,// SS_1138_CQU_20131127
        tiOverview2, FImgOverview2, DataImage, DescriError, Error)              // SS_1138_CQU_20131127
    then MostrarImagenBoton(sbThumb3, FImgOverview2, bmp)                       // SS_1138_CQU_20131127
    else begin                                                                  // SS_1138_CQU_20131127
        NoMostrarImagenBoton(sbThumb3);                                         // SS_1138_CQU_20131127
        actImagenOverview2.Enabled := False;                                    // SS_1138_CQU_20131127
    end;                                                                        // SS_1138_CQU_20131127

    // Intento obtener la imagen tiOverview3                                    // SS_1138_CQU_20131127
    if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora,// SS_1138_CQU_20131127
        tiOverview3, FImgOverview3, DataImage, DescriError, Error)              // SS_1138_CQU_20131127
    then MostrarImagenBoton(sbThumb4, FImgOverview3, bmp)                       // SS_1138_CQU_20131127
    else begin                                                                  // SS_1138_CQU_20131127
        NoMostrarImagenBoton(sbThumb4);                                         // SS_1138_CQU_20131127
        actImagenOverview3.Enabled := False;                                    // SS_1138_CQU_20131127
    end;                                                                        // SS_1138_CQU_20131127

    // Habilito la botonera s�lo si existe al menos un OverView Cargado         // SS_1203_CQU_20140724
    tbImagenOverview.Enabled := actImagenOverview1.Enabled or                   // SS_1203_CQU_20140724
                                actImagenOverview2.Enabled or                   // SS_1203_CQU_20140724
                                actImagenOverview3.Enabled;                     // SS_1203_CQU_20140724

    // Intento obtener la imagen tiPosterior
    if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora, tiPosterior, ImagenTMP, DataImage, DescriError, Error, FEsKapschPosterior) // SS_1147W_CQU_20141215
    then begin                                                                                                                                              // SS_1147W_CQU_20141215
        actImagenPosterior1.Enabled := true;                                                                                                                // SS_1147W_CQU_20141215
        actImagenPosterior1.Checked := true;                                                                                                                // SS_1147W_CQU_20141215
        actResetImage.Enabled := True;                                                                                                                      // SS_1147W_CQU_20141215
        //actVerMascara.Enabled := False;                                                                                                                   // SS_1382_CQU_20151019  // SS_1147W_CQU_20141215
        if FEsKapschPosterior then begin                                                                                                                    // SS_1147W_CQU_20141215
            FJPG12Posterior := (ImagenTMP as TJPEGPlusImage);                                                                                               // SS_1147W_CQU_20141215
            MostrarImagenBoton(sbThumb5, FJPG12Posterior, bmp);                                                                                             // SS_1147W_CQU_20141215
        end else begin                                                                                                                                      // SS_1147W_CQU_20141215
            FJPGItalianoPosterior := (ImagenTMP as TBitmap);                                                                                                // SS_1147W_CQU_20141215
            MostrarImagenBoton(sbThumb5, FJPGItalianoPosterior, bmp);                                                                                       // SS_1147W_CQU_20141215
        end;                                                                                                                                                // SS_1147W_CQU_20141215
        sbThumb5.Click;                                                                                                                                     // SS_1147W_CQU_20141215
        CentrarImagenInicial;                                                                                                                               // SS_1147W_CQU_20141215
        HabilitarHerramientasImagen;                                                                                                                        // SS_1147W_CQU_20141215
        Result := True;                                                                                                                                     // SS_1147W_CQU_20141215
        if Result and not Visible then Show;                                                                                                                // SS_1147W_CQU_20141215
    end else NoMostrarImagenBoton(sbThumb5);                                                                                                                // SS_1147W_CQU_20141215

    // Intento obtener la imagen tiPosterior2                                                                                                                   // SS_1147W_CQU_20141215
    if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora, tiPosterior2, ImagenTMP, DataImage, DescriError, Error, FEsKapschPosterior2)   // SS_1147W_CQU_20141215
    then begin                                                                                                                                                  // SS_1147W_CQU_20141215
        actImagenPosterior2.Enabled := true;                                                                                                                    // SS_1147W_CQU_20141215
        actImagenPosterior2.Checked := true;                                                                                                                    // SS_1147W_CQU_20141215
        actResetImage.Enabled := True;                                                                                                                          // SS_1147W_CQU_20141215
        //actVerMascara.Enabled := False;                                                                                                                       // SS_1382_CQU_20151019  // SS_1147W_CQU_20141215
        if FEsKapschPosterior2 then begin                                                                                                                       // SS_1147W_CQU_20141215
            FJPG12Posterior2 := (ImagenTMP as TJPEGPlusImage);                                                                                                  // SS_1147W_CQU_20141215
            MostrarImagenBoton(sbThumb6, FJPG12Posterior2, bmp);                                                                                                // SS_1147W_CQU_20141215
        end else begin                                                                                                                                          // SS_1147W_CQU_20141215
            FJPGItalianoPosterior2 := (ImagenTMP as TBitmap);                                                                                                   // SS_1147W_CQU_20141215
            MostrarImagenBoton(sbThumb6, FJPGItalianoPosterior2, bmp);                                                                                          // SS_1147W_CQU_20141215
        end;                                                                                                                                                    // SS_1147W_CQU_20141215
        sbThumb6.Click;                                                                                                                                         // SS_1147W_CQU_20141215
        CentrarImagenInicial;                                                                                                                                   // SS_1147W_CQU_20141215
        HabilitarHerramientasImagen;                                                                                                                            // SS_1147W_CQU_20141215
        Result := True;                                                                                                                                         // SS_1147W_CQU_20141215
        if Result and not Visible then Show;                                                                                                                    // SS_1147W_CQU_20141215
    end else NoMostrarImagenBoton(sbThumb6);                                                                                                                    // SS_1147W_CQU_20141215
    AjustarTama�oTImagePlus(ImagenAmpliar);                                     																				// SS_1138_CQU_20151019

    //// Intento obtener la imagen tiPosterior                                                                                                                  // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //if FEsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto,                                                                                      // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //    FNumCorrCO, FFechaHora, tiPosterior, FJPGItalianoPosterior, DataImage,                                                                                // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //    DescriError, Error)                                                                                                                                   // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    ////then MostrarImagenBoton(sbThumb5, FJPGItalianoPosterior, bmp)                                                                                           // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724 // SS_1138_CQU_20131127
    //then begin                                                                                                                                                // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    actImagenPosterior1.Enabled := true;                                                                                                                  // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    actImagenPosterior1.Checked := true;                                                                                                                  // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    actResetImage.Enabled := True;                                                                                                                        // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    actVerMascara.Enabled := False;                                                                                                                       // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    MostrarImagenBoton(sbThumb5, FJPGItalianoPosterior, bmp);                                                                                             // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    sbThumb5.Click;                                                                                                                                       // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    CentrarImagenInicial;                                                                                                                                 // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    HabilitarHerramientasImagen;                                                                                                                          // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    Result := True;                                                                                                                                       // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    if Result and not Visible then Show;                                                                                                                  // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //end                                                                                                                                                       // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //else if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO,                                                                                     // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //    FFechaHora, tiPosterior, FJPG12Posterior, DataImage, DescriError, Error)                                                                              // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //then MostrarImagenBoton(sbThumb5, FJPG12Posterior, bmp)                                                                                                   // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //else NoMostrarImagenBoton(sbThumb5);                                                                                                                      // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127

    //// Intento obtener la imagen tiPosterior2                                                                                                                 // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //if FEsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto,                                                                                      // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //    FNumCorrCO, FFechaHora, tiPosterior2, FJPGItalianoPosterior2, DataImage,                                                                              // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //    DescriError, Error)                                                                                                                                   // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    ////then MostrarImagenBoton(sbThumb6, FJPGItalianoPosterior2, bmp)                                                                                          // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724  // SS_1138_CQU_20131127
    //then begin                                                                                                                                                // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    actImagenPosterior2.Enabled := true;                                                                                                                  // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    actImagenPosterior2.Checked := true;                                                                                                                  // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    actResetImage.Enabled := True;                                                                                                                        // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    actVerMascara.Enabled := False;                                                                                                                       // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    MostrarImagenBoton(sbThumb6, FJPGItalianoPosterior2, bmp);                                                                                            // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    sbThumb6.Click;                                                                                                                                       // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    CentrarImagenInicial;                                                                                                                                 // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    HabilitarHerramientasImagen;                                                                                                                          // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    Result := True;                                                                                                                                       // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //    if Result and not Visible then Show;                                                                                                                  // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //end                                                                                                                                                       // SS_1147W_CQU_20141215    // SS_1203_CQU_20140724
    //else if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO,                                                                                     // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //  FFechaHora, tiPosterior2, FJPG12Posterior2, DataImage, DescriError, Error)                                                                              // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //then MostrarImagenBoton(sbThumb6, FJPG12Posterior2, bmp)                                                                                                  // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127
    //else NoMostrarImagenBoton(sbThumb6);                                                                                                                      // SS_1147W_CQU_20141215    // SS_1138_CQU_20131127

end;

function TfrmVisorImagenes.CargarImagen(NumCorrCo: Int64; RegistrationAccessibility: integer;   // SS_1091_CQU_20130516
    FechaHora: TDateTime; EsItaliano : Boolean): Boolean;                                       // SS_1091_CQU_20130516
begin                                                                                           // SS_1091_CQU_20130516
    FEsItaliano := EsItaliano;                                                                  // SS_1091_CQU_20130516
    result :=  CargarImagen(NumCorrCo, RegistrationAccessibility, FechaHora);                   // SS_1091_CQU_20130516
end;                                                                                            // SS_1091_CQU_20130516

procedure TFrmVisorImagenes.Redibujar(VerMascara: Boolean);
var
    bmp: TBitMap;
begin
	  Screen.Cursor := crHourGlass;
    bmp := TBitMap.create;
    bmp.PixelFormat := pf24bit;
	  try
        // Reseteamos los filtros aplicados
        //bmp.Assign(FImagenActual);            // SS_1091_CQU_20130516
        //if not FEsItaliano then               // SS_1147W_CQU_20141215  // SS_1091_CQU_20130516
        if FEsKapschImagenActual then           // SS_1147W_CQU_20141215
            bmp.Assign(FImagenActual)           // SS_1091_CQU_20130516
        else                                    // SS_1091_CQU_20130516
            bmp.Assign(FImagenActualItaliana);  // SS_1091_CQU_20130516

        // Aplicamos el Sharp
        if actRemarcarBordes.Checked then begin
          Sharp(bmp);
        end;
        // Aplicamos el Blurr
        if actSuavizarBordes.checked then begin
            Blurr(bmp);
        end;
        // Dibujamos la Mascara
        if VerMascara then begin
//            DrawMask(bmp, FPositionPlate.top - 4);
            DrawMask(bmp, (ImagenAmpliar.Picture.Height div 3) * 2);
        end;

        // Cargamos la imagen de la patente
        //GetImagePlate(FImagenActual, FImagenCargada);
        // Asignamos la nueva imagen
        ImagenAmpliar.Picture.Assign(bmp);
    finally
        Screen.Cursor := crDefault;
        bmp.Free;
    end;
end;

procedure TfrmVisorImagenes.CentrarImageninicial;
begin
    (* Cuando la imagen se carga inicialmente se centra en funcion de la
    posici�n de la patente.
    Si la patente es desconocida se centra la imagen en funci�n de la posici�n
    m�s probable de la patente. *)
    //if FPositionPlate.Left = 0 then                                               // SS_1091_CQU_20130516
    //    ImagenAmpliar.CenterToPoint(Point((FImagenActual.Width div 2),            // SS_1091_CQU_20130516
    //      (trunc((FImagenActual.Height) / 1.6))))                                 // SS_1091_CQU_20130516
    //else                                                                          // SS_1091_CQU_20130516
    if FPositionPlate.Left = 0 then begin                                           // SS_1091_CQU_20130516
        //if not FEsItaliano then                                                   // SS_1147W_CQU_20141215  // SS_1091_CQU_20130516
        if FEsKapschImagenActual then                                               // SS_1147W_CQU_20141215
            ImagenAmpliar.CenterToPoint(Point((FImagenActual.Width div 2),          // SS_1091_CQU_20130516
                (trunc((FImagenActual.Height) / 1.6))))                             // SS_1091_CQU_20130516
        else                                                                        // SS_1091_CQU_20130516
            ImagenAmpliar.CenterToPoint(Point((FImagenActualItaliana.Width div 2),  // SS_1091_CQU_20130516
                (trunc((FImagenActualItaliana.Height) / 1.6))));                    // SS_1091_CQU_20130516
    end else                                                                        // SS_1091_CQU_20130516
        ImagenAmpliar.Center;
        {
         CenterToPoint(Point(
        FPositionPlate.Left +
          ((FPositionPlate.right - FPositionPlate.Left) div 2),
        FPositionPlate.top +
          ((FPositionPlate.bottom - FPositionPlate.top) div 2)));}
end;

procedure TfrmVisorImagenes.FormCreate(Sender: TObject);
begin
    //ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS, FImagePath);                                            //SS-377-NDR-20110607
    ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN, FImagePathNFI);                      //SS-377-NDR-20110607
    if (Mouse.CursorPos.X > (Screen.Width div 2)) then
        Left := 10
    else
        Left := (Screen.Width - Width - 10);
    if (Mouse.CursorPos.Y > (Screen.Height div 2)) then
        top := 10
    else
        top := (Screen.Height - Height - 10);
end;

procedure TfrmVisorImagenes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //Action := caFree;                                                         // SS_1138_CQU_20131127
    //FImagenActual.Free;                                                       // SS_1138_CQU_20131127
    if Assigned(FImagenActualItaliana) then FImagenActualItaliana.Free; // SS_1091_CQU_20130516
    if assigned(FImgOverview) then FImgOverview.Free;
    if assigned(FFrmImagenOverview) then FFrmImagenOverview.Free;GVentana := nil;

    if Assigned(FImagenActual)          then FImagenActual.Free;                // SS_1138_CQU_20131127
	if Assigned(FJPG12Frontal2)         then FJPG12Frontal2.Free;               // SS_1138_CQU_20131127
	if Assigned(FJPG12Posterior)        then FJPG12Posterior.Free;              // SS_1138_CQU_20131127
	if Assigned(FJPG12Posterior2)       then FJPG12Posterior2.Free;             // SS_1138_CQU_20131127
	if Assigned(FImgOverview2)          then FImgOverview2.Free;                // SS_1138_CQU_20131127
	if Assigned(FImgOverview3)          then FImgOverview3.Free;                // SS_1138_CQU_20131127
    if Assigned(FJPGItalianoFrontal)    then FJPGItalianoFrontal.Free;          // SS_1138_CQU_20131127
	if Assigned(FJPGItalianoFrontal2)   then FJPGItalianoFrontal2.Free;         // SS_1138_CQU_20131127
	if Assigned(FJPGItalianoPosterior)  then FJPGItalianoPosterior.Free;        // SS_1138_CQU_20131127
	if Assigned(FJPGItalianoPosterior2) then FJPGItalianoPosterior2.Free;       // SS_1138_CQU_20131127
    Action := caFree;                                                           // SS_1138_CQU_20131127
end;

procedure TfrmVisorImagenes.actZoomInExecute(Sender: TObject);
begin
   	ImagenAmpliar.Scale := ImagenAmpliar.Scale + 0.2;
end;

procedure TfrmVisorImagenes.actZoomOutExecute(Sender: TObject);
begin
    if ImagenAmpliar.Scale < 0.5 then exit;
   	ImagenAmpliar.Scale := ImagenAmpliar.Scale - 0.2;
end;

procedure TfrmVisorImagenes.actZoomResetExecute(Sender: TObject);
begin
    //if ImagenAmpliar.Scale > 4.0 then exit;   // SS_1382_CQU_20151019
    //ImagenAmpliar.Scale := 1;                 // SS_1382_CQU_20151019
    AjustarTama�oTImagePlus(ImagenAmpliar);     // SS_1382_CQU_20151019
end;

procedure TfrmVisorImagenes.actZoomPlateExecute(Sender: TObject);
begin
    ImagenAmpliar.Scale := 1;
end;

procedure TfrmVisorImagenes.PrintImage(Image: TImagePlus; ZoomPercent: Integer);
  // if ZoomPercent=100, Image will be printed across the whole page
  procedure DrawImage(Canvas: TCanvas; DestRect: TRect; ABitmap: TBitmap);
  var
    Header, Bits: Pointer;
    HeaderSize: DWORD;
    BitsSize: DWORD;
  begin
    GetDIBSizes(ABitmap.Handle, HeaderSize, BitsSize);
    Header := AllocMem(HeaderSize);
    Bits := AllocMem(BitsSize);
    try
      GetDIB(ABitmap.Handle, ABitmap.Palette, Header^, Bits^);
      StretchDIBits(Canvas.Handle, DestRect.Left, DestRect.Top,
        DestRect.Right, DestRect.Bottom,
        0, 0, ABitmap.Width, ABitmap.Height, Bits, TBitmapInfo(Header^),
        DIB_RGB_COLORS, SRCCOPY);
    finally
      FreeMem(Header, HeaderSize);
      FreeMem(Bits, BitsSize);
    end;
  end;

  procedure EscribirTextoEnImagen(Image: TImagePlus);
  var
        AltoViejo: Integer;
  begin
        AltoViejo := Image.Picture.Bitmap.Height;
        Image.Picture.Bitmap.Height := AltoViejo + 170;
        Image.Picture.Bitmap.Canvas.Font.Size := 12;
        Image.Picture.Bitmap.Canvas.Font.Name := 'Arial';
        Image.Picture.Bitmap.Canvas.TextOut(10, Altoviejo + 10, 'ID Tr�nsito: ' + inttoStr(FNumCorrCo));
        Image.Picture.Bitmap.Canvas.TextOut(10, Altoviejo + 50, 'Fecha y hora: ' + DateTimeToStr(FFechaHora));
        Image.Picture.Bitmap.Canvas.TextOut(10, Altoviejo + 90, 'Telev�a ' + LblTelevia.Caption);
        Image.Picture.Bitmap.Canvas.TextOut(10, Altoviejo + 130, 'Portico: ' + FPortico);
  end;

var
    relHeight, relWidth: integer;
    Imagen: TImagePlus;
begin
      Screen.Cursor := crHourglass;
      Printer.BeginDoc;
      //Creo otra imagen porque a esta le voy a escribir un pie con las caracter�sticas
      //del tr�nsito.
      Imagen := TImagePlus.Create(Self);
      Imagen.Picture.Bitmap.Assign(Image.Picture.Bitmap);
      EscribirTextoEnImagen(Imagen);
      with Imagen.Picture.Bitmap do
      begin
        if ((Width / Height) > (Printer.PageWidth / Printer.PageHeight)) then
        begin
            // Stretch Bitmap to width of PrinterPage
            relWidth := Printer.PageWidth;
            relHeight := MulDiv(Height, Printer.PageWidth, Width);
        end
        else
        begin
            // Stretch Bitmap to height of PrinterPage
            relWidth  := MulDiv(Width, Printer.PageHeight, Height);
            relHeight := Printer.PageHeight;
        end;
        relWidth := Round(relWidth * ZoomPercent / 100);
        relHeight := Round(relHeight * ZoomPercent / 100);
        DrawImage(Printer.Canvas, Rect(0, 0, relWidth, relHeight), Imagen.Picture.Bitmap);
    end;
    Printer.EndDoc;
    Screen.cursor := crDefault;
    Imagen.Free;
end;


procedure TfrmVisorImagenes.acImprimirImagenExecute(Sender: TObject);
begin
    if DialogoImpresora.Execute then
    begin
        //Printer.SetPrinter(DialogoImpresora);
        PrintImage(ImagenAmpliar, 100);
    end;
end;

procedure TfrmVisorImagenes.actBrightPlusExecute(Sender: TObject);
begin
    //if not FEsItaliano then begin                                // SS_1147W_CQU_20141215 // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                            // SS_1147W_CQU_20141215
        // Si el Shift supero el limite nos vamos
        if FImagenActual.Shift = 0 then exit;
        // Si el FShift no esta inicializado lo asignamos
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;                                                     // SS_1091_CQU_20130516
        FImagenActual.Shift := FImagenActual.Shift - 1;
    end;
	  Redibujar(not actVerMascara.Checked);
end;

procedure TfrmVisorImagenes.actBrigthResetExecute(Sender: TObject);
begin
    //if not FEsItaliano then begin                                // SS_1147W_CQU_20141215 // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                            // SS_1147W_CQU_20141215
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
          end;
        if FImagenActual.Shift = FShiftOriginal then exit;
        FImagenActual.Shift := FShiftOriginal;
    end;                                                       // SS_1091_CQU_20130516
	  Redibujar(not actVerMascara.Checked);
end;

procedure TfrmVisorImagenes.actBrigthLessExecute(Sender: TObject);
begin
    //if not FEsItaliano then begin                                // SS_1147W_CQU_20141215 // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                            // SS_1147W_CQU_20141215
        if FImagenActual.Shift > 4 then exit;
        // Si el FShift no esta inicializado lo asignamos
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift := FImagenActual.Shift + 1;
    end;                                                        // SS_1091_CQU_20130516
	Redibujar(not actVerMascara.Checked);
end;

procedure TfrmVisorImagenes.actRemarcarBordesExecute(Sender: TObject);
begin
	Redibujar(not actVerMascara.Checked);
end;

procedure TfrmVisorImagenes.actSuavizarBordesExecute(Sender: TObject);
begin
	Redibujar(not actVerMascara.Checked);
end;

procedure TfrmVisorImagenes.actVerMascaraExecute(Sender: TObject);
begin
    Redibujar(not actVerMascara.Checked);
end;

procedure TfrmVisorImagenes.actMaskExecute(Sender: TObject);
begin
	Redibujar(not actVerMascara.Checked);
end;

procedure TfrmVisorImagenes.actImageOverviewExecute(Sender: TObject);
begin
    // Si el form de Imagenes Overview no esta cargado lo cargamos
    if not FFormOverviewCargado then begin
        (* Si la imagen seleccionada actualmente es una overview cargamos esa,
        de lo contrario cargamos la primera que tenemos. *)
        if FTipoImagenOverview = tiOverview1 then begin
            FFrmImagenOverview.ShowImageOverview(FImgOverview, FTipoImagenOverview);
            actImagenOverview1.Checked := true;
        end else if FTipoImagenOverview = tiOverview2 then begin
            FFrmImagenOverview.ShowImageOverview(FImgOverview, FTipoImagenOverview);
            actImagenOverview2.Checked := true;
        end else if FTipoImagenOverview = tiOverview3 then begin
			      FFrmImagenOverview.ShowImageOverview(FImgOverview, FTipoImagenOverview);
            actImagenOverview3.Checked := true;
        end else
        if not FImgOverview.Empty then begin
            FFrmImagenOverview.ShowImageOverview(FImgOverview, tiOverview1);
        end;
        
        FFormOverviewCargado := true;
    end;
    if FFrmImagenOverview.Visible then actImageOverview.Checked := False;       // SS_1138_CQU_20131127
    
    // Luego de cargarlo lo mostramos u ocultamos segun corresponda
    if actImageOverview.Checked then begin
        FFrmImagenOverview.Show;
        tbImagenOverview.Style := tbsDropDown;
        tbImagenOverview.DropDownMenu := pmImagenOverview;
    end else begin
        FFrmImagenOverview.Hide;
        tbImagenOverview.Style := tbsButton;
        tbImagenOverview.DropDownMenu := nil;
    end;
end;

procedure TFrmVisorImagenes.CentrarImagen;
begin
    if FPositionPlate.Left = 0 then begin
        //if FCentrarImag then
            ImagenAmpliar.Center
    end else
        ImagenAmpliar.CenterToPoint(Point(
            FPositionPlate.Left +
            ((FPositionPlate.right - FPositionPlate.Left) div 2),
            FPositionPlate.top +
            ((FPositionPlate.bottom - FPositionPlate.top) div 2)));
end;

procedure TfrmVisorImagenes.actCenterToPlateExecute(Sender: TObject);
begin
    CentrarImagen;
end;

procedure TfrmVisorImagenes.actResetImageExecute(Sender: TObject);
begin
    // Volvemos a False todos los filtros seleccionados
  	actRemarcarBordes.Checked := False;
    actSuavizarBordes.Checked := False;
    //if not FEsItaliano then begin                     // SS_1147W_CQU_20141215   // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                 // SS_1147W_CQU_20141215
        // Volvelos el brillo al original
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift    := FShiftOriginal;
    end;                                                // SS_1091_CQU_20130516
    // Volvemos el Zoom al original
    ImagenAmpliar.Scale     := 1;
    // Redibujamos
    Redibujar(not actVerMascara.Checked);
	// Centramos la imagen
    CentrarImagen;
    AjustarTama�oTImagePlus(ImagenAmpliar);            // SS_1382_CQU_20151019
end;

function TfrmVisorImagenes.CargarImagenOverview: Boolean;
resourceString
    ERROR_CARGANDO_IMAGEN = 'Ha ocurrido un error al intentar cargar la imagen overview';
var
    DataImage: TDataImage;
    DescriError, NombreCorto: AnsiString;                                       //SS-377-NDR-20110607
    Error: TTipoErrorImg;
begin
        Result := False;
    NombreCorto := QueryGetValue(DMConnections.BaseCAC,Format('SELECT Concesionarias.NombreCorto FROM Transitos (NOLOCK) INNER JOIN Concesionarias (NOLOCK) ON Concesionarias.CodigoConcesionaria=Transitos.CodigoConcesionaria WHERE NumCorrCA=%d', [FNumCorrCo]));
    if (FRegistrationAccessibility and RegistrationAccessibilityImagen[tiOverview1]) = RegistrationAccessibilityImagen[tiOverview1] then begin
        if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora, tiOverview1, FImgOverview,                       //SS-377-NDR-20110607
            DataImage, DescriError, Error) then begin
            FTipoImagenOverview := tiOverview1;
            actImagenOverview1.Enabled := true;
            Result := True;
        end else begin
            // Agrego IF y Tabulado                                             // SS_1138_CQU_20131127
            if not FEsItaliano then begin                                       // SS_1138_CQU_20131127
                MsgBoxErr(ERROR_CARGANDO_IMAGEN, descrierror, caption, MB_ICONSTOP);
                exit;
            end else                                                            // SS_1138_CQU_20131127
                actImageOverview.Enabled := False;                              // SS_1138_CQU_20131127
        end;                                                                    // SS_1138_CQU_20131127
    end
    else begin
        if (FRegistrationAccessibility and RegistrationAccessibilityImagen[tiOverview2]) = RegistrationAccessibilityImagen[tiOverview2] then begin
            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora, tiOverview2, FImgOverview,                   //SS-377-NDR-20110607
              DataImage, DescriError, Error) then begin
                FTipoImagenOverview := tiOverview2;
                actImagenOverview2.Enabled := true;
                Result := True;
            end else begin
                // Agrego IF y Tabulado                                         // SS_1138_CQU_20131127
                if not FEsItaliano then begin                                   // SS_1138_CQU_20131127
                    MsgBoxErr(ERROR_CARGANDO_IMAGEN, descrierror, caption, MB_ICONSTOP);
                    exit;
                end else                                                        // SS_1138_CQU_20131127
                    actImagenOverview2.Enabled := False;                        // SS_1138_CQU_20131127
            end;
        end
        else if (FRegistrationAccessibility and RegistrationAccessibilityImagen[tiOverview3]) = RegistrationAccessibilityImagen[tiOverview3] then begin
                if ObtenerImagenTransito(FImagePathNFI, NombreCorto, FNumCorrCO, FFechaHora, tiOverview3, FImgOverview,               //SS-377-NDR-20110607
                  DataImage, DescriError, Error) then begin
                    FTipoImagenOverview := tiOverview3;
                    actImagenOverview3.Enabled := true;
                    Result := True;
                end else begin
                    // Agrego IF y Tabulado                                     // SS_1138_CQU_20131127
                    if not FEsItaliano then begin                               // SS_1138_CQU_20131127
                        MsgBoxErr(ERROR_CARGANDO_IMAGEN, descrierror, caption, MB_ICONSTOP);
                        exit;
                    end else                                                    // SS_1138_CQU_20131127
                        actImagenOverview3.Enabled := False;                    // SS_1138_CQU_20131127
                end;
        end
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   SeleccionarImagen
Date Created    :   28-Noviembre-2013
Description     :   Carga la imagen seleccionada en el Panel central
Firma           :   SS_1138_CQU_20131127
-----------------------------------------------------------------------------}
procedure TfrmVisorImagenes.SeleccionarImagen(aGraphic: TGraphic;
    aTipoImagen: TTipoImagen);
begin
    if assigned(aGraphic) then begin
        if aGraphic.Empty then Exit;
		Screen.Cursor := crHourGlass;
        try
		  	if aTipoImagen in [tiFrontal, tiPosterior, tiFrontal2, tiPosterior2]
            then begin
                if      ((aTipoImagen = tiFrontal) AND FEsKapschFrontal) then FEsKapschImagenActual := True         // SS_1147W_CQU_20141215
                else if ((aTipoImagen = tiFrontal2) AND FEsKapschFrontal2) then FEsKapschImagenActual := True       // SS_1147W_CQU_20141215
                else if ((aTipoImagen = tiPosterior) AND FEsKapschPosterior) then FEsKapschImagenActual := True     // SS_1147W_CQU_20141215
                else if ((aTipoImagen = tiPosterior2) AND FEsKapschPosterior2) then FEsKapschImagenActual := True   // SS_1147W_CQU_20141215
                else FEsKapschImagenActual := false;                                                                // SS_1147W_CQU_20141215
                // if not FEsItaliano then                                                                          // SS_1147W_CQU_20141215
                if FEsKapschImagenActual then                                                                       // SS_1147W_CQU_20141215
				    FImagenActual.Assign(aGraphic)
                else
                    FImagenActualItaliana.Assign(aGraphic);
				FShiftOriginal := -1;
                Redibujar(CheckBox1.Checked);
                actResetImageExecute(nil);
                //if FEsItaliano then                   // SS_1147W_CQU_20141215
                if not FEsKapschImagenActual then       // SS_1147W_CQU_20141215
                begin
                    if aTipoImagen in [tiFrontal, tiPosterior] then
                        ImagenAmpliar.Scale := 0.4
                    else if aTipoImagen in [tiFrontal2, tiPosterior2] then
                        ImagenAmpliar.Scale := 0.1;
                end;
            end else begin
                FFormOverviewCargado := true;
                FFrmImagenOverview.ShowImageOverview(aGraphic, aTipoImagen);
                FFrmImagenOverview.Show;
            end;
            AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1382_CQU_20151019
        finally
            Screen.Cursor := crDefault;
		end;
	end;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   MostrarImagenBoton
Date Created    :   28-Noviembre-2013
Description     :   Habilita los botones de la derecha y carga la imagen
                    en la posici�n que le corresponda.
Firma           :   SS_1138_CQU_20131127
-----------------------------------------------------------------------------}
Procedure TfrmVisorImagenes.MostrarImagenBoton(
    btn: TSpeedButton; aGraphic: TGraphic; bmp: TBitMap);
var
    aRect: TRect;
begin
    bmp.FreeImage;
    bmp.Assign(aGraphic);
    bmp.Width := btn.Width;
    bmp.Height := btn.Height;
    bmp.TransparentColor := clRed;

    aRect := Rect(0, 0, btn.ClientRect.Right, btn.ClientRect.Bottom);
    bmp.Canvas.StretchDraw(aRect, aGraphic);
    btn.caption := EmptyStr;
    btn.Glyph.Assign(bmp);
    btn.Enabled := true;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   NoMostrarImagenBoton
Date Created    :   28-Noviembre-2013
Description     :   Coloca en los botones de la izquiera la leyenda (Sin Imagen)
                    y los deshabilita
Firma           :   SS_1138_CQU_20131127
-----------------------------------------------------------------------------}
Procedure TfrmVisorImagenes.NoMostrarImagenBoton(btn: TSpeedButton);
resourcestring
    SIN_IMAGEN = '(Sin Imagen)';
begin
    btn.Enabled := False;
    btn.Glyph.FreeImage;
    btn.Glyph.ReleaseHandle;
    btn.Glyph := nil;
    btn.Caption := SIN_IMAGEN;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   sbThumb0Click
Date Created    :   28-Noviembre-2013
Description     :   Despliega la imagen Frontal
Firma           :   SS_1138_CQU_20131127
-----------------------------------------------------------------------------}
procedure TfrmVisorImagenes.sbThumb0Click(Sender: TObject);
begin
    //if not FEsItaliano then   // SS_1147W_CQU_20141215
    if FEsKapschFrontal then    // SS_1147W_CQU_20141215
        SeleccionarImagen(FJPG12Frontal, tiFrontal)
    else
        SeleccionarImagen(FJPGItalianoFrontal, tiFrontal);
end;

{-----------------------------------------------------------------------------
Procedure Name  :   sbThumb1Click
Date Created    :   28-Noviembre-2013
Description     :   Despliega la imagen Frontal2
Firma           :   SS_1138_CQU_20131127
-----------------------------------------------------------------------------}
procedure TfrmVisorImagenes.sbThumb1Click(Sender: TObject);
begin
    //if not FEsItaliano then   // SS_1147W_CQU_20141215
    if FEsKapschFrontal2 then   // SS_1147W_CQU_20141215
        SeleccionarImagen(FJPG12Frontal2, tiFrontal2)
    else
        SeleccionarImagen(FJPGItalianoFrontal2, tiFrontal2);
end;

{-----------------------------------------------------------------------------
Procedure Name  :   sbThumb2Click
Date Created    :   28-Noviembre-2013
Description     :   Despliega la imagen Overview1
Firma           :   SS_1138_CQU_20131127
-----------------------------------------------------------------------------}
procedure TfrmVisorImagenes.sbThumb2Click(Sender: TObject);
begin
    SeleccionarImagen(FImgOverview, tiOverview1);
end;

{-----------------------------------------------------------------------------
Procedure Name  :   sbThumb3Click
Date Created    :   28-Noviembre-2013
Description     :   Despliega la iamgen Overview2
Firma           :   SS_1138_CQU_20131127
-----------------------------------------------------------------------------}
procedure TfrmVisorImagenes.sbThumb3Click(Sender: TObject);
begin
    SeleccionarImagen(FImgOverview2, tiOverview2);
end;

{-----------------------------------------------------------------------------
Procedure Name  :   sbThumb4Click
Date Created    :   28-Noviembre-2013
Description     :   Despliega la imagen Overview3
Firma           :   SS_1138_CQU_20131127
-----------------------------------------------------------------------------}
procedure TfrmVisorImagenes.sbThumb4Click(Sender: TObject);
begin
    SeleccionarImagen(FImgOverview3, tiOverview3);
end;

{-----------------------------------------------------------------------------
Procedure Name  :   sbThumb5Click
Date Created    :   28-Noviembre-2013
Description     :   Despliega la imagen Posterior1
Firma           :   SS_1138_CQU_20131127
-----------------------------------------------------------------------------}
procedure TfrmVisorImagenes.sbThumb5Click(Sender: TObject);
begin
    //if not FEsItaliano then   // SS_1147W_CQU_20141215
    if FEsKapschPosterior then  // SS_1147W_CQU_20141215
        SeleccionarImagen(FJPG12Posterior, tiPosterior)
    else
        SeleccionarImagen(FJPGItalianoPosterior, tiPosterior);
end;

{-----------------------------------------------------------------------------
Procedure Name  :   sbThumb6Click
Date Created    :   28-Noviembre-2013
Description     :   Despliega la imagen Posterior2
Firma           :   SS_1138_CQU_20131127
-----------------------------------------------------------------------------}
procedure TfrmVisorImagenes.sbThumb6Click(Sender: TObject);
begin
    //if not FEsItaliano then   // SS_1147W_CQU_20141215
    if FEsKapschPosterior2 then // SS_1147W_CQU_20141215
        SeleccionarImagen(FJPG12Posterior2, tiPosterior2)
    else
        SeleccionarImagen(FJPGItalianoPosterior2, tiPosterior2);
end;

end.
