object formSeleccionSolicitudDuplicada: TformSeleccionSolicitudDuplicada
  Left = 104
  Top = 218
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Atenci'#243'n'
  ClientHeight = 291
  ClientWidth = 873
  Color = clBtnFace
  Constraints.MaxHeight = 400
  Constraints.MaxWidth = 1000
  Constraints.MinHeight = 280
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 117
    Width = 873
    Height = 135
    Align = alBottom
    Caption = 'Solicitudes '
    TabOrder = 0
    object dbl_Solicitud: TDBListEx
      Left = 2
      Top = 15
      Width = 869
      Height = 118
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 115
          Header.Caption = 'Hora de Creaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHoraCreacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 280
          Header.Caption = 'Nombre'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Nombre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Tel. Principal'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TelefonoPrincipal'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Tel. Alternativo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TelefonoAlternativo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Calle'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripcionCalle'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripcionEstadoSolicitud'
        end>
      DataSource = DS_SolicitudContacto
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = dbl_SolicitudDblClick
    end
  end
  object PanelDeAbajo: TPanel
    Left = 0
    Top = 252
    Width = 873
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      873
      39)
    object BtnCancelar: TButton
      Left = 791
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Cancelar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtnCancelarClick
    end
    object btnAceptar: TButton
      Left = 709
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Para modificar la solicitud seleccionada'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Modificar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnAceptarClick
    end
    object btnNueva: TButton
      Left = 627
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Nueva, para ingresar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Nueva'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNuevaClick
    end
    object btnRegistrarLlamada: TButton
      Left = 520
      Top = 5
      Width = 103
      Height = 26
      Hint = 'Registrar el resultado de la llamada'
      Anchors = [akRight, akBottom]
      Caption = 'Registrar &Llamada'
      Enabled = False
      ModalResult = 6
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnRegistrarLlamadaClick
    end
  end
  object Pnl_LabelDescripcion: TPanel
    Left = 0
    Top = 0
    Width = 873
    Height = 91
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object lblDescripcionMensaje: TLabel
      Left = 20
      Top = 24
      Width = 493
      Height = 65
      AutoSize = False
      Caption = 
        'Se han encontrado una o mas solicitudes con el RUT/RUN  ingresad' +
        'o.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object lblRutBuscado: TLabel
      Left = 20
      Top = 5
      Width = 493
      Height = 16
      AutoSize = False
      Caption = 'RUT buscado: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
  end
  object ObtenerSolicitudContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterScroll = ObtenerSolicitudContactoAfterScroll
    ProcedureName = 'ObtenerSolicitudContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaHoraCreacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraFin'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoFuenteSolicitud'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadVehiculos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@RUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@OrderBy'
        Attributes = [paNullable]
        DataType = ftFixedChar
        Size = 100
        Value = Null
      end>
    Left = 120
    Top = 184
  end
  object DS_SolicitudContacto: TDataSource
    DataSet = ObtenerSolicitudContacto
    Left = 152
    Top = 184
  end
end
