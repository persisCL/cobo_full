unit DetalleInfraccion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Navigator, StdCtrls, ExtCtrls, Buttons,
   DB, ADODB, Grids, DBGrids, DPSGrid, Util,
  DBTables, DMConnection, Menus,
   PeaTypes, ImgList;

type
  TNavWindowDetalleInfraccion = class(TNavWindowFrm)
    ds_DetalleViaje: TDataSource;
	gpDetalle: TGroupBox;
	dbgComprobantes: TDPSGrid;
	dsTransitos: TDataSource;
	ObtenerComprobantesInfraccion: TADOStoredProc;
	ObtenerTransitosInfraccion: TADOStoredProc;
	GroupBox1: TGroupBox;
	dbgViajes: TDPSGrid;
	Label1: TLabel;
	Label2: TLabel;
	Label3: TLabel;
	Label4: TLabel;
	Label5: TLabel;
	Label7: TLabel;
//	Image: TImagesVehicle;
	lMotivoInfraccion: TLabel;
	lEstadoInfraccion: TLabel;
	lFechaConfirmacion: TLabel;
	lFechaNotificacion: TLabel;
	lFechaInicioTramiteJudicial: TLabel;
	LFechaRegularizacion: TLabel;
    ImageList: TImageList;
	procedure btn_cerrarClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure ActualizarBusqueda;
	procedure dsTransitosDataChange(Sender: TObject; Field: TField);
    procedure dbgComprobantesDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
  private
	FCodigoMotivoInfraccion, FCodigoCliente, FNumeroHistorialInfraccion: integer;
	FPatente: AnsiString;
    FFechaRegularizacionAsignada: Boolean;
  public
	{ Public declarations }
	Procedure RefreshData; override;
	Function GetBookmark(Var Bookmark, Description: AnsiString): Boolean; override;
	Function GotoBookmark(Bookmark: AnsiString): Boolean; override;
	class Function CreateBookmark(CodigoCliente,
		  CodigoMotivoInfraccion: Integer; Patente: AnsiString; NumeroHistorialInfraccion: Integer;
		  MotivoInfraccion, EstadoInfraccion: AnsiString;
		  FechaConfirmacion, FechaNotificacion,
		  FechaInicioTramiteJudicial, FechaRegularizacion: TDateTime;
		  TipoCOmprobante: Char; NumeroComprobante: Double;
		  CodigoConcesionaria, NumeroViaje, NumeroPuntoCobro: Integer): AnsiString;
	function Inicializa: Boolean; override;
	function FindFirst(SearchText: AnsiString): Boolean; override;
  end;

var
  NavWindowDetalleInfraccion: TNavWindowDetalleInfraccion;



implementation

{$R *.dfm}

{ TNavWindowDatosCliente }
class function TNavWindowDetalleInfraccion.CreateBookmark(CodigoCliente,
		  CodigoMotivoInfraccion: Integer; Patente: AnsiString; NumeroHistorialInfraccion: Integer;
		  MotivoInfraccion, EstadoInfraccion: AnsiString;
		  FechaConfirmacion, FechaNotificacion,
		  FechaInicioTramiteJudicial, FechaRegularizacion: TDateTime;
		  TipoCOmprobante: Char; NumeroComprobante: Double;
		  CodigoConcesionaria, NumeroViaje, NumeroPuntoCobro: Integer): AnsiString;
begin
	Result := format('%d;%d;%s;%d;%s;%s;%s;%s;%s;%s;%s;%.0f;%d;%d;%d',[CodigoCliente, CodigoMotivoInfraccion,
	  Patente, NumeroHistorialInfraccion, MotivoInfraccion, EstadoInfraccion,
	  DateToStr(FechaConfirmacion), DateToStr(FechaNotificacion),
	  DateToStr(FechaInicioTramiteJudicial), DateToStr(FechaRegularizacion),
	  TipoComprobante, NumeroComprobante,
	  CodigoConcesionaria, NumeroViaje, NumeroPuntoCobro]);
end;

function TNavWindowDetalleInfraccion.GetBookmark(var Bookmark, Description: AnsiString): Boolean;
resourcestring
    MSG_BOOKMARK = 'Detalle de la Infracción %s - %d';
var TipoCOmprobante: Char;
	NumeroComprobante: Double;
	CodigoConcesionaria, NumeroViaje, NumeroPuntoCobro: Integer;
begin
	if not ObtenerComprobantesInfraccion.isEmpty then begin
		TipoComprobante := ObtenerComprobantesInfraccion.FieldByName('TipoComprobante').asString[1];
		NumeroComprobante := ObtenerComprobantesInfraccion.FieldByName('NumeroComprobante').asFloat;
    end	else begin
		TipoComprobante := ' ';
		NumeroComprobante := 0;
	end;

	if not ObtenerTransitosInfraccion.IsEmpty then begin
		CodigoConcesionaria := ObtenerTransitosInfraccion.FieldByName('CodigoConcesionaria').asInteger;
		NumeroViaje := ObtenerTransitosInfraccion.FieldByName('NumeroViaje').asInteger;
		NumeroPuntoCobro := ObtenerTransitosInfraccion.FieldByName('NumeroPuntoCobro').asInteger;
	end else begin
		CodigoConcesionaria := 0;
		NumeroViaje := 0;
		NumeroPuntoCobro := 0;
	end;

	Bookmark := CreateBookMark(FCodigoCliente, FCodigoMotivoInfraccion,
	  FPatente, FNumeroHistorialInfraccion, lMotivoInfraccion.caption, lEstadoInfraccion.caption,
	  strToDate(lFechaConfirmacion.Caption), strToDate(lFechaNotificacion.caption),
	  strToDate(lFechaInicioTramiteJudicial.Caption),
	  iif(FFechaRegularizacionAsignada, lFechaRegularizacion.caption, 0),
	  TipoComprobante, NumeroComprobante,
	  CodigoConcesionaria, NumeroViaje, NumeroPuntoCobro);
	Description := Format(MSG_BOOKMARK, [Trim(FPatente), FNumeroHistorialInfraccion]);
	Result := True;
end;

function TNavWindowDetalleInfraccion.GotoBookmark(Bookmark: AnsiString): Boolean;
var d: TDateTime;
resourceString
    CAPTION_FECHA_NOASIGNADA = 'No Asignada';
begin
	try
        // Asignamos los valores del bookmark
		FCodigoCliente := IVal(ParseParamByNumber(Bookmark, 1, ';'));
		FCodigoMotivoInfraccion := IVal(ParseParamByNumber(Bookmark, 2, ';'));
		FPatente := trim(ParseParamByNumber(Bookmark, 3, ';'));
		FNumeroHistorialInfraccion := IVal(ParseParamByNumber(Bookmark, 4, ';'));
		lMotivoInfraccion.Caption := Trim(ParseParamByNumber(Bookmark, 5, ';'));
		lEstadoInfraccion.caption := Trim(ParseParamByNumber(Bookmark, 6, ';'));
		d := strToDate(ParseParamByNumber(Bookmark, 7, ';'));
		lFechaConfirmacion.Caption := iif(d = 0, CAPTION_FECHA_NOASIGNADA, dateToStr(d));
		d := strToDate(ParseParamByNumber(Bookmark, 8, ';'));
		lFechaNotificacion.caption := iif(d = 0, CAPTION_FECHA_NOASIGNADA, dateToStr(d));
		d := strToDate(ParseParamByNumber(Bookmark, 9, ';'));
		lFechaInicioTramiteJudicial.Caption := iif(d = 0, CAPTION_FECHA_NOASIGNADA, dateToStr(d));
		d := StrToDate(ParseParamByNumber(Bookmark, 10, ';'));
		lFechaRegularizacion.caption := iif(d = 0, CAPTION_FECHA_NOASIGNADA, dateToStr(d));

        // Verificamos si la fecha de regularizacion esta asignada
        FFechaRegularizacionAsignada := (d <> 0);

        // Obtenemos los datos
		Update;
		ActualizarBusqueda;
		if FCodigoMotivoInfraccion <> 4 then begin
			ObtenerTransitosInfraccion.Locate('CodigoConcesionaria;NumeroViaje;NumeroPuntoCobro',
			  VarArrayOf([IVal(ParseParamByNumber(Bookmark, 13, ';')), IVal(ParseParamByNumber(Bookmark, 14, ';')),
			  IVal(ParseParamByNumber(Bookmark, 15, ';'))]),[]);
			Result := not ObtenerTransitosInfraccion.IsEmpty;
		end else begin
			ObtenerComprobantesInfraccion.Locate('TipoComprobante;NumeroComprobante',
			  VarArrayOf([ParseParamByNumber(Bookmark, 11, ';'),
			  StrToFloat(Trim(ParseParamByNumber(Bookmark, 12, ';')))]),[]);
			Result := not ObtenerTransitosInfraccion.IsEmpty;
		end;
	except
		result := False;
	end;
end;

function TNavWindowDetalleInfraccion.Inicializa: Boolean;
begin
	Update;
	Result := Inherited Inicializa;
    with dbgComprobantes.columns do begin
		Items[0].Title.Caption  := MSG_COL_TITLE_TIPO;
		Items[1].Title.Caption  := MSG_COL_TITLE_COMPROBANTE;
		Items[2].Title.Caption  := MSG_COL_TITLE_FECHA;
		Items[3].Title.Caption  := MSG_COL_TITLE_FECHA_VENC;
		Items[4].Title.Caption  := MSG_COL_TITLE_IMPORTE;
		Items[5].Title.Caption  := MSG_COL_TITLE_COMPROBANTE_AJUSTADO;
		Items[6].Title.Caption  := MSG_COL_TITLE_DEBITO;
		Items[7].Title.Caption  := MSG_COL_TITLE_IMPORTE_INFRACCION;
  		Items[8].Title.Caption  := MSG_COL_TITLE_IMAGEN;
    end;
	if not Result then Exit;
end;

procedure TNavWindowDetalleInfraccion.btn_cerrarClick(Sender: TObject);
begin
	Close;
end;

procedure TNavWindowDetalleInfraccion.RefreshData;
begin
	inherited;
end;

procedure TNavWindowDetalleInfraccion.FormCreate(Sender: TObject);
begin
	Update;
end;


function TNavWindowDetalleInfraccion.FindFirst(SearchText: AnsiString): Boolean;
begin
	result := true;
end;

procedure TNavWindowDetalleInfraccion.ActualizarBusqueda();
begin
	screen.Cursor := crhourGlass;
	try
		ObtenerTransitosInfraccion.Close;
		ObtenerComprobantesInfraccion.Close;
		// Si es una infraccion por transacción actualizo el store ObtenerViajesInfracccion
		if FCodigoMotivoInfraccion <> 4 then begin
			With ObtenerTransitosInfraccion do begin
				Parameters.ParamByName('@Patente').value := FPatente;
				Parameters.ParamByName('@NumeroHistorialInfraccion').value := FNumeroHistorialInfraccion;
				Open;
			end;
			dbgComprobantes.visible := False;
//			Image.Visible := True;
		end else begin
		// Si es una infraccion por morosidad actualizo el store ObtenerComprobantesInfracccion
			With ObtenerComprobantesInfraccion do begin
				Parameters.ParamByName('@Patente').value := FPatente;
				Parameters.ParamByName('@NumeroHistorialInfraccion').value := FNumeroHistorialInfraccion;
				Open;
			end;
			dbgComprobantes.visible := True;
//			Image.Visible := False;
		end;
	finally
		screen.Cursor := crDefault;
	end;
end;

procedure TNavWindowDetalleInfraccion.dsTransitosDataChange(
  Sender: TObject; Field: TField);
begin
(*	image.LoadImages((Sender as TDataSource).DataSet.FieldByName('CodigoConcesionaria').AsInteger,
	  (Sender as TDataSource).DataSet.FieldByName('NumeroPuntoCobro').AsInteger,
	  (Sender as TDataSource).DataSet.FieldByName('NumeroViaje').AsInteger,
	  etValidado, (Sender as TDataSource).DataSet.FieldByName('FechaHora').AsDateTime);*)
end;

procedure TNavWindowDetalleInfraccion.dbgComprobantesDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var bmp: TBitMap;
begin
	with (Sender as TDBGrid) do begin
        if Column.FieldName = '' then begin
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
(*            if image.ExistImage then
                ImageList.GetBitmap(0, bmp)
            else
                ImageList.GetBitmap(1, bmp);*)
            if Rect.Right <= bmp.Width then
                canvas.StretchDraw(Rect, bmp)
            else
                canvas.Draw(Rect.left + ((Rect.Right - Rect.left) - bmp.Width) div 2, Rect.top, bmp);
            bmp.Free;
       end;
   end;
end;

end.
