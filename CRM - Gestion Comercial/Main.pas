{-------------------------------------------------------------------------------
 File Name: Main.pas
 Author:
 Date Created:
 Language: ES-AR
 Description: CAC

 Revision 1:
    Author : ggomez
    Date : 31/05/2006
    Description :
        - Agregu� la funci�n adicional EDICION_ACTIONLIST_TAG_TERCEROS.
        - Modifiqu� el nombre de la funci�n adicional EDICION_ACTIONLIST_TAG,
        ahora es EDICION_ACTIONLIST_TAG_PROPIOS.

 Revision 2:
    Author : jconcheyro
    Date : 17/11/2006
    Description : En inicializar se agrega VerificarExisteCotizacionUFDelDia
    Que muestra un mensaje de aviso si no existe la Cotizacion de UF del d�a
    que se usa en Arriendos de Telev�a.



  Revision 4:
    Author : lcanteros
    Date : 26-03-2008
    Description : se agrega el permiso para modificar el tipo de cliente

  Revisi�n 5:
    Author : mbecerra
    Date: 03-04-2008
    Description: Se agregan dos formularios nuevos para Revalidaci�n de Tr�nsitos:
            	- ConsultaTransitosARevalidarForm  y
                - RevalidarTransitosForm


            	Se modifica el men� principal con dos nuevas opciones en el submen�
                Infractores: Revalidar Transitos  e Informe Revalidaci�n



 Revisi�n 7:
    Author : dAllegretti
    Date: 24/06/2008
    Description:
    		Se agrega el permiso para modificar la fecha vencimiento de la reemision de la Nota de Cobro.

  Revisi�n 8:
    Author : dAllegretti
    Date: 20/10/2008
    Description:
    		Se agregan los parametros en la llamada a la facturacion de infracciones

  Revisi�n 9:
    Author : dAllegretti
    Date: 29/10/2008
    Description:
    		Se agregan los permisos para el alta y modificaci�n de convenios sin cuentas
Revision 11
    Author: pdominguez
    Date: 18/02/2009
    Description: Se cre� el objeto tmrEstadoMigracion (TTimer) que actualizar�
    el estado de la Migraci�n.

Revisi�n 12
    Author: nefernandez
    Date: 16/04/2009
    Description: Ref SS 797: Se agrega el permiso para generar los tr�nsitos en Formato CSV, a partir
    de la pantalla de Consulta de Convenio (Cartola).

Revision 13
Author: mbecerra
Date: 17-Junio-2009
Description: 	(Ref. Facturaci�n Electr�nica)
        	Se agrega el permiso para editar la fecha de Emisi�n en el formulario
            de facturaci�n manual.

Revisi�n 14
    Author: pdominguez
    Date: 07/07/2009
    Description: SS 784
        - Se a�ade al men� principal dentro de la opci�n de Infractores el acceso
        al m�dulo nuevo "Emitir Certificado Infracciones".

Revision 15
Author: mbecerra
Date: 29-Julio-2009
Description:	(SS 784 Facturaci�n Electr�nica)
        Se agrega la opci�n de men� "Nota Cr�dito Infractores"

Revision 16
Author: mbecerra
Date: 14-Octubre-2009
Description:	Se agrega el men� de Lista Blanca y el submen� de ABM de Lista Blanca.

Revision 17
Author: Nelson Droguett Sierra
Date: 07-Agosto-2009
Description:	Se agrega la opcion de men� para Anular Movimientos no Facturados

Revision 18
Author: mbecerra
Date: 28-Septiembre-2009
Description:	(Ref. SS 833)
        	Se agrega el acceso (funci�n) para poder modificar el tipo de documento
            electr�nico a imprimir.

Revision 19
Author: Nelson Droguett Sierra
Date: 01-Diciembre-2009
Description:	(Ref. SS 833)
        	Se agrega el acceso (funci�n) para poder acceder al BIT de Habilitado
            Lista Blanca AMB

Revision : 20
Date: 29/01/2010
Author: mpiazza
Description:  Ref SS-510 se modifica mnu_sistemaClick(...
              le agrega una validacion por estacion de trabajo

Revision: 21
    Author : pdominguez
    Date   : 03/02/2010
    Description : SS 510
        - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
            actConfiguracionPuesto: TAction

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            actConfiguracionPuestoExecute
            HabilitarItemsTurno

Revision: 22
    Author : dcepeda
    Date   : 05/05/2010
    Description : SS 767
        - Se elimina acceso a las siguientes funcionalidades:
         Consultas / FrmRespuestaRNUT
         Convenio / FrmReemisionNotaCobro
Revision: 24
    Author : jjofre
    Date   : 01/08/2010
    Description : SS 469
        - Se crea acceso a Nuevo Permiso: InactivaDomicilio RNUT
                                          InactivaMedioDeContacto RNUT

Revision 25
    Author      : mbecerra
    Date        : 14-Septiembre-2010
    Description :       (Ref SS 921)
                    Se agrega un permiso nuevo para el nuevo bot�n de "Editar Fecha
                    de vencimiento del TAG" del formulario frmSCDatosVehiculo.

Description : Se agrega al men� el formulario de revalidaci�n de Ticket de Venta
             Manual.
Firma       : SS_976_SS_ALA_20110805

Firma       : SS-971-NDR-20110823, SS-971-PDO-20111102
Description : Se modifica posicion de opciones en el arreglo de funciones adicionales

Firma       : SS_628_ALA_20111201
Description : Se agerga funcion BIT_Convenio_Sin_Cuenta para permiso sobre
              checkbox SinCuenta.

Firma       : SS-1006-NDR-20120313
Description : Se agrega opcion de menu para la inhabilitacion de cuentas.

Firma       : SS_1046_CQU_20120530
Descripcion : Se agrega el permiso para el bot�n exportar a Excel en la pesta�a de Cta.Cte.


Firma       : SS_660_MBE_20121004
Description : Se agrega el permiso para poder colocar en Lista amarilla todos los
                TAGs activos de un convenio

Firma       : SS_1006_CQU_20121007
Description : Se agrega el formulario para consultar los estacionamientos y su respectivo men�

Firma       : SS_1117_CQU_20130802
Descripcion : Se agrega la validaci�n de Turno Abierto en el m�todo FormCloseQuery
              se basa en la misma validaci�n del mismo m�todo pero de Cobranzas.exe

Firma       : SS_1118 _MVI_20130805
Descripcion : Se agrega men� a la pantalla "Consulta Patente Arauco TAG".

Firma       :   SS_925_NDR_20131122
Description :   Genera nuevos menu segun campo GenerarMenues de tabla SistemasVersiones (no del .ini)

Author      :   Claudio Quezada Ib��ez
Date        :   28-Noviembre-2013
Firma       :   SS_1138_CQU_20131127
Descripcion :   Se oculta el men� mnuNoClientesClick

Firma       :   SS_925_MBE_20131223
Description :   Se cambia la ventana de Acerca de.

Firma       : SS_1156B_NDR_20140128
Description : Baja forzada masiva procesando un archivo .csv y dejando registro en operaciones interfaces

Firma       :
Author      :   Claudio Quezada Ib��ez
Date        :   29-Enero-2014
Firma       :   SS_1138_CQU_20140129
Descripcion :   Se solicita volver a desplegar el men� mnuNoClientesClick

Author      :   Claudio Quezada Ib��ez
Date        :   11-Marzo-2014
Firma       :   SS_660_CQU_20140311
Descripcion :   Se agrega permiso para facturar infracciones por rut

Firma		:	SS_1175_MCA_20140325
Description	:	se comenta el menu consulta de transitos por unificacion con la transitos no clientes

Author      :   Claudio Quezada Ib��ez
Date        :   06-Agosto-2014
Firma       :   SS_660E_CQU_20140806 SS_660D_CQU_20140806
Descripcion :   Se agregan permisos para la impresion del certificado
                Se agrega menu para la facturacion de infractores por morosidad

Firma       : SS_1147_NDR_20141216
Descripcion : Color de menues segun concesionaria nativa

Firma       : SS_1147ZZZ_NDR_20150413
Descripcion : Correccion a observaciones de la migracion

Firma       : SS_1147AAA_NDR_20150414
Descripcion : Correccion a observaciones de Refinanciaci�n (Caption y StatusBar)

Firma       :   SS_1408_MCA_20151027
Descripcion :   Se agregan menu para el ABM de Clientes a contactar con mensaje

Firma       : SS_1436_NDR_20151230
Descripcion : Dejar un registro en RegistroOperaciones cuando el usuario sale del sistema  o cuando trata de ingresar con password erronea

Firma       : SS_1437_NDR_20151230
Descripcion : Que los menus de los informes se carguen segun los permisos del usuario, y que dependan del GenerarMenues del Sistema

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

-------------------------------------------------------------------------------}
unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Navigator, Menus, ExtCtrls, ImgList, Buttons, StdCtrls, FrmSolicitud,
  UtilProc, ComCtrls, ToolWin, ActnList, UtilDB, DmConnection, util, peaprocs,
  login, CacProcs, DB, ADODB, jpeg,  DdeMan,  DPSAbout, CambioPassword, PeaTypes, FrmListadoConvenio,
  MostrarDocumentos,FrmModificacionConvenio, FrmDevolverTelevia,
  frmGestionComponentesObligatorios,  FrmListadoTurnos,
  ConstParametrosGenerales, frmFAQ, frmConfirmaContrasena, FrmAperturaCierrePuntoVenta, RStrings,
  DPSControls, FreDomicilio, ActnMan,
  FrmPaseDiarioCambioFecha, FrmRecibirTagsNominados, FrmGenerarOrdenPedidoTAGs, GeneradorDeIni,
  frmreclamos, FrmInicioConsultaConvenio,
  MenuesContextuales, FrmRptEstadisticaReclamos, FrmOrdenesServicioBloqueadas,
  FrmConsultaTransitosNoCliente, XPStyleActnCtrls, XPMan, FrmSolicitudContacto,
  DeclHard, DeclPrn, CustomAssistance, fFacturacionInfracciones,
  frmConsultaTransitos, frmConsultaTransitosARevalidar,
  frmAjusteCaja,                   //TASK_031_GLE_20170111

  ABMPersonas,

  frmAcercaDe,                                          //SS_925_MBE_20131223
  frmFacturacionManual,
  frmNotaCreditoInfractor,			//REV.15
  frmABMListaBlanca,				//REV.16
  frmMovimientosNofacturados,		// REV.17
  frmRevalidarTicketVentaManual,    //SS_974_SS_ALA_20110805
  SysUtilsCN,
  frmConsultaAltaOtraConsecionaria,       //TASK_002_ECA_20160304
  frmAdhesionPreInscripcionPAK,
  FrmConsultaEstacionamientos,      // SS_1006_CQU_20121007
  frmConsultaPatenteAraucoTAG,      // SS_1118 _MVI_20130805
  frmBajaForzada,					//SS_1156B_NDR_20140128
  FrmABMClientesAContactar, LMDPNGImage;         //SS_1408_MCA_20151027

type
  TMainForm = class(TForm)
	PageImagenes: TImageList;
    MainMenu: TMainMenu;
    mnu_salir: TMenuItem;
	StatusBar: TStatusBar;
	tmr_llamada: TTimer;
	HotLinkImagenes: TImageList;
	MenuImagenes: TImageList;
	ImagenFondo: TImage;
    mnu_Ventana: TMenuItem;
    Mnu_Cascada: TMenuItem;
    mnu_Mosaico: TMenuItem;
    N2: TMenuItem;
    mnu_Siguiente: TMenuItem;
	mnu_Anterior: TMenuItem;
    mnu_Ayuda: TMenuItem;
    mnu_AcercaDe: TMenuItem;
    mnu_Mantenimiento: TMenuItem;
    mnu_CambiodeContrasena: TMenuItem;
	mnu_Parmetros: TMenuItem;
    mnu_IconBar: TMenuItem;
    mnu_Ver: TMenuItem;
    qry_MaxComprobante: TADOQuery;
    mnu_informes: TMenuItem;
    mnu_solicitudContacto: TMenuItem;
    mnu_ConsultaSOlicitudes: TMenuItem;
	mnu_SolicitudesdeContacto: TMenuItem;
    mnu_PreguntasFrecuentes: TMenuItem;
    mnu_FAQs: TMenuItem;
    N4: TMenuItem;
    mnu_ModificarFaq: TMenuItem;
	mnu_ModificarSolicitudes: TMenuItem;
    mnu_GestionCamposObligatorios: TMenuItem;
    mnu_ImportarVehiculosSolicitudes: TMenuItem;
    mnu_ImportarVehiculosSolicitudesSA: TMenuItem;
    N3: TMenuItem;
    mnu_MostrarScriptSolicitudContacto: TMenuItem;
    mnu_Archivo: TMenuItem;
    mnu_Contrato: TMenuItem;
    mnu_IngresoContrato: TMenuItem;
    mnu_sistema: TMenuItem;
    mnu_abrir_turno: TMenuItem;
    mnu_cerrar_turno: TMenuItem;
    mnu_adicionar_tag_turno: TMenuItem;
	mnu_recibir_tags: TMenuItem;
    mnu_Listado_Convenio: TMenuItem;
    mnu_ver_Turnos: TMenuItem;
    mnu_pedir_reposicion: TMenuItem;
    mnu_ModificarConvenio: TMenuItem;
    ActionManager1: TActionManager;
    actAbrirTurno: TAction;
    actCerrarTurno: TAction;
    actAdicionarTagsTurno: TAction;
    mnu_CerrarTurnosTerceros: TMenuItem;
    mnu_PaseDiario: TMenuItem;
    mnu_PaseDiarioConsultaPorPatente: TMenuItem;
    DevolucindeTelevas1: TMenuItem;
    Reclamos1: TMenuItem;
    mnuReclamosGenerales: TMenuItem;
	N1: TMenuItem;
    VerReclamos1: TMenuItem;
    IniciarConsultaConvenio1: TMenuItem;
    Mnu_EstadisticaReclamos: TMenuItem;
    N5: TMenuItem;
    Mnu_areasBloqueadas: TMenuItem;
    mnuConsultas: TMenuItem;
    mnuNoClientes: TMenuItem;
    mnu_BarraDeNavegacion: TMenuItem;
    mnu_BarraDeHotLinks: TMenuItem;
    XPManifest1: TXPManifest;
    spearador: TMenuItem;
    mnu_IniCom: TMenuItem;
    mnu_FinCom: TMenuItem;
    mnu_ImpresoraFiscal: TMenuItem;
    mnu_IngresoComprobantes: TMenuItem;
    mnu_CierreDeCajero: TMenuItem;
    mnu_InformeZ: TMenuItem;
    mnu_Configuracion: TMenuItem;
    ImpresoraFiscal: TFiscalPrinter;
    Infractores1: TMenuItem;
    mnuExplorarInfracciones: TMenuItem;
    mnu_ReIngresoComprobantes: TMenuItem;
    actReIngresoComprobantes: TAction;
    //rnsitos1: TMenuItem;														//SS_1175_MCA_20140325
    mnuInformeRevalidacionTransito: TMenuItem;
    N6: TMenuItem;
    mnuRevalidarTransitos: TMenuItem;
    
    tmrEstadoMigracion: TTimer;
    N7: TMenuItem;
    mnuFacturacionManual: TMenuItem;
    mnuEmitirFacturaInfracciones: TMenuItem;
    EmitirCertificadodeInfracciones1: TMenuItem;
    mnuNotaCreditoInfractor: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    actConfiguracionPuesto: TAction;
    N10: TMenuItem;
    mnuTicketVentaManual: TMenuItem;								//SS-1006-GVI-20120619
    N11: TMenuItem;
    //mnu_PreInscripcionPAK: TMenuItem;                           // TASK_002_ECA_20160304
    mnuEstacionamientos: TMenuItem;                           // SS_1118 _MVI_20130805
    //ConsultaPatenteAraucoTAG1 : TMenuItem;                       // TASK_002_ECA_20160304
    mnuBajaForzadaMasiva: TMenuItem;                                //SS_1156B_NDR_20140128
    mnuInfraNormal: TMenuItem;									// SS_660E_CQU_20140806
    mnuABMClientesaContactar: TMenuItem;
    mnu_AdhesionTAGListaBlanca: TMenuItem;
    mnu_ABMClientes: TMenuItem;
    Image1: TImage;
    Image2: TImage;
    mnuAjustedeCaja: TMenuItem;
    mnuCampannas: TMenuItem;
    mnuCrearCampanna: TMenuItem;							           //TASK_031_GLE_20170111
    procedure mnuAjustedeCajaClick(Sender: TObject);               //TASK_031_GLE_20170111
    procedure tmrEstadoMigracionTimer(Sender: TObject);
    procedure mnuExplorarInfraccionesClick(Sender: TObject);
    procedure mnu_ProcesoFacturacionClick(Sender: TObject);
    procedure act_InformeZExecute(Sender: TObject);
    procedure act_ConfiguracionImpresoraFiscalExecute(Sender: TObject);
    procedure act_InformeCierreDeCajeroExecute(Sender: TObject);
    procedure actIngresoComprobantesExecute(Sender: TObject);
    procedure mnu_FinComClick(Sender: TObject);
    procedure mnu_IniComClick(Sender: TObject);
    procedure mnu_IngresoContratoClick(Sender: TObject);
	procedure mnu_salirClick(Sender: TObject);
	procedure Cerrar1Click(Sender: TObject);
	procedure tmr_llamadaTimer(Sender: TObject);
	procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	procedure FormPaint(Sender: TObject);
	procedure FormResize(Sender: TObject);
    procedure mnu_TerminarLlamadaencursoClick(Sender: TObject);
    procedure Mnu_CascadaClick(Sender: TObject);
	procedure mnu_AcercaDeClick(Sender: TObject);
	procedure mnu_CambiodeContrasenaClick(Sender: TObject);
    procedure mnu_VentanaClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
	procedure mnu_comunicacionesClick(Sender: TObject);
    procedure mnu_BackOfficeClick(Sender: TObject);
    procedure mnu_IconBarClick(Sender: TObject);
    procedure mnu_ConsultaSOlicitudesClick(Sender: TObject);
    procedure mnu_FAQsClick(Sender: TObject);
    procedure mnu_ModificarSolicitudesClick(Sender: TObject);
	procedure mnu_GestionCamposObligatoriosClick(Sender: TObject);
    procedure mnu_recibir_tagsClick(Sender: TObject);
    procedure mnu_MostrarScriptSolicitudContactoClick(Sender: TObject);
    procedure mnu_Listado_ConvenioClick(Sender: TObject);
    procedure mnu_ver_TurnosClick(Sender: TObject);
	procedure mnu_pedir_reposicionClick(Sender: TObject);
    procedure mnu_ModificarConvenioClick(Sender: TObject);
    procedure mnu_ModificarConvenio_AdminClick(Sender: TObject);
    procedure actAbrirTurnoExecute(Sender: TObject);
    procedure actCerrarTurnoExecute(Sender: TObject);
    procedure actAdicionarTagsTurnoExecute(Sender: TObject);
    procedure mnu_CerrarTurnosTercerosClick(Sender: TObject);
    procedure mnu_PaseDiarioConsultaPorPatenteClick(Sender: TObject);
    procedure DevolucindeTelevas1Click(Sender: TObject);
    procedure VerReclamos1Click(Sender: TObject);
    procedure IniciarConsultaConvenio1Click(Sender: TObject);
    procedure Mnu_EstadisticaReclamosClick(Sender: TObject);
    procedure Mnu_areasBloqueadasClick(Sender: TObject);
    procedure mnuNoClientesClick(Sender: TObject);
    procedure mnu_sistemaClick(Sender: TObject);
    procedure mnu_BarraDeNavegacionClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnu_BarraDeHotLinksClick(Sender: TObject);
    procedure actReIngresoComprobantesExecute(Sender: TObject);
    //procedure rnsitos1Click(Sender: TObject);									//SS_1175_MCA_20140325
    procedure mnuRevalidarTransitosClick(Sender: TObject);
    procedure mnuFacturacionManualClick(Sender: TObject);
    procedure mnuEmitirFacturaInfraccionesClick(Sender: TObject);
    procedure EmitirCertificadodeInfracciones1Click(Sender: TObject);
    procedure mnuNotaCreditoInfractorClick(Sender: TObject);
    //procedure mnuAnulacionMovimientosnoFacturadosClick(Sender: TObject);      //TASK_067_GLE_20170410
    procedure actConfiguracionPuestoExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mnuTicketVentaManualClick(Sender: TObject);                                         //SS_974_SS_ALA_20110805
    procedure mnu_ConsultaAltaPAraucoClick(Sender: TObject);
    procedure mni_PreInscripcionPAKNOClick(Sender: TObject);
    procedure mnuEstacionamientosClick(Sender: TObject);                        // SS_1006_CQU_20121007
    procedure mniConsultaPatenteAraucoTAG1NOClick(Sender: TObject);                  // SS_1118 _MVI_20130805
    procedure mnuBajaForzadaMasivaClick(Sender: TObject);                       //SS_1156B_NDR_20140128
	procedure mnuInfraMorosidadClick(Sender: TObject);                          // SS_660E_CQU_20140806
    function CambiarEventoMenu(Menu: TMenu): Boolean;                           //SS_1147_NDR_20141216
    procedure mnuABMClientesaContactarClick(Sender: TObject);
    procedure mnu_AdhesionTAGListaBlancaClick(Sender: TObject);
    procedure mnu_ABMClientesClick(Sender: TObject);
    procedure mnuCrearCampannaClick(Sender: TObject);
  private
	{ Private declarations }
	FDDEItem: TDDEServerItem;
	FDDETopic: TDDEServerConv;
	FFechaHoraInicioComunicacion: TDateTime;
  FPuntoVenta, FPuntoEntrega: Integer;
  FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                //SS_1147_NDR_20141216

	procedure InicializarVariablesCAC;
	procedure ComenzarNavegacion(Const NumeroRut: String = '');
	procedure DoPokeData(Sender: TObject);
    function CheckFonts: Boolean;
    procedure OnClickConsulta(NumeroConvenio: String; CodigoConvenio: Integer);
    procedure RefrescarBarraDeEstado;
	procedure SetPuntoEntrega(const Value: Integer);
    procedure SetPuntoVenta(const Value: Integer);
    procedure RefrescarEstadosIconBar;
    procedure InicializarIconBar;
    procedure GuardarUserSettings;
    procedure CheckMenuComunicaciones(Iniciada: boolean); // chequea enabled y desabled para menu e iconos de comunicaciones
                                                          // pasa el parametro true para la comunicaci�n iniciada y false para la cerrada
    function ValidarRUTCTI(CTIRUT: String): String;
  public
	{ Public declarations }
    procedure HabilitarItemsTurno(habilitar: boolean);
    procedure DeshabilitarTodosItemsTurnos;
	function Inicializa: Boolean;
    procedure OnClickModif(NumeroConvenio: String; CodigoConvenio: Integer; Fuente: Integer; Administrador: Boolean);
    property PuntoEntrega: Integer read FPuntoEntrega write SetPuntoEntrega;
    property PuntoVenta: Integer read FPuntoVenta write SetPuntoVenta;


    procedure DrawMenuItem(Sender: TObject; ACanvas: TCanvas;ARect: TRect; Selected: Boolean);             //SS_1147_NDR_20141216
  end;

var
    MainForm: TMainForm;
    ContactoComunicacion: TDatosContactoComunicacion;

implementation

uses frm_IngresarComprobantes, ExplorarInfracciones, Frm_ReIngresarComprobantes, frmEmitirCertificadoInfracciones,
  StartupSAU,
  //INICIO	: CFU 20170215 TASK_107_CFU_20170215-CRM_Crear_Campa�a
  frmCrearCampannas;	
  //TERMINO : CFU 20170215 TASK_107_CFU_20170215-CRM_Crear_Campa�a

resourcestring
    PAGINA_FAVORITOS            = 'Page_Favoritos';
    PAGINA_COMUNICACIONES       = 'Page_Comunicaciones';
	MSG_FAVORITOS	            = 'Favoritos';
 	HINT_FAVORITOS  	        = 'Favoritos';
    PAGINA_MANTENIMIENTO        = 'Page_Mantenimiento';
	MSG_MANTENIMIENTO	        = 'Mantenimiento';
 	HINT_MANTENIMIENTO	        = 'Configuraci�n del Sistema';
    // Captions y hints de las p�ginas del IconBar
	MSG_COMUNICACIONES 	        = 'Comunicaciones';
    HINT_COMUNICACIONES         = 'Gesti�n de Comunicaciones';
    MSG_BACKOFFICE              = 'BackOffice';
    HINT_BACKOFFICE             = 'Gesti�n de Cuentas';
	MSG_REPORTES  		        = 'Reportes';
	HINT_REPORTES  		        = 'Emitir Reportes';
	MSG_COBRANZAS		        = 'Cobranzas';
  	HINT_COBRANZAS		        = 'Realizar Cobranzas de Comprobantes';
	MSG_SOLICITUDES	            = 'Solicitudes';
 	HINT_SOLICITUDES	        = 'Solicitudes de contacto';

    // Captions y hints de los iconos de la p�gina Comunicaciones
	MSG_NUEVA_COMUNICACION 	    = 'Nueva Comunicaci�n';

    // Captions de los iconos de la p�gina BackOffice
	MSG_NUEVA_CUENTA		    = 'Nueva Cuenta';
	MSG_MODIFICAR_CUENTA		= 'Modificaci�n de Cuenta';

	// Captions de los iconos de la p�gina Reportes
    MSG_RTE_ORDENES_SERVICIO     = 'Ordenes de Servicio';
    MSG_RTE_COMUNICACIONES      = 'Estad�stica de Comunicaciones';
    MSG_RTE_DIARIO_CAJA         = 'Diario de Caja';
    MSG_RTE_LIQUIDACION         = 'Liquidaci�n';

    // Captions de los iconos de la p�gina Cobranzas
    MSG_PAGO_COMPROBANTES        = 'Pago de Comprobantes';
    MSG_AJUSTE_CAJA              = 'Ajuste de Caja';

    // Captions de los iconos de la p�gina Solicitudes
    MSG_INGRESO_SOLICITUDES       = 'Ingreso de solicitudes';
    MSG_FAQ                       = 'FAQ';
    MSG_CONSULTA_SOLICITUDES      = 'Consulta de solicitudes';
    MSG_SCRIPT_SOLICITUD_CONTACTO = 'Mostrar Script solicitud de contacto';
    // Captions de los iconos de la p�gina Mantenimiento
    MSG_CAMBIO_CONTRASENIA       = 'Cambio de contrase�a';

	MSG_USER		 	            = 'Usuario: %s';
    CAPTION_FIN_COMUNICACION        = 'Finalizar Comunicaci�n';
    MSG_TERMINAR_COMUNICACION       = 'Finalizar Comunicaci�n';
    MSG_NUEVA_ORDEN_SERVICIO        = 'Nueva Orden de Servicio';
    CAPTION_ALTA_SOLICITUD          = 'Ingresar Solicitud';
    CAPTION_ALTA_CONTRATO           = 'Ingresar Contrato';

{$R *.dfm}
{$R fonts.res}
{$I ..\comunes\Ambiente.inc}

function TMainForm.Inicializa: Boolean;
Var
	f: TLoginForm;
    ok: boolean;
    DatosTurno: TDatosTurno;

ResourceString
    MSG_SEGURIDAD_SEGURIDAD = 'Seguridad - Modificar Convenio';
    MSG_SEGURIDAD_BOTONES = 'Botones';
    MSG_AGREGAR = 'Agregar Veh�culo.';
    MSG_EDITAR = 'Editar Veh�culo.';
    MSG_ELIMINAR = 'Eliminar Cuenta';
    MSG_CAMBIAR_TAG = 'Cambiar Telev�a';
    MSG_CAMBIAR_VEH = 'Cambiar Vehiculo';
    MSG_SUSPENDER = 'Suspender Cuenta';
    MSG_PERDIDO = 'Perdida de Telev�a';
    MSG_BLANQUEAR_CLAVE = 'Blanquear Clave';
    MSG_REIMPRIMIR = 'Reimprimir Convenio';
    MSG_BAJA_CONVENIO = 'Baja de Convenio';
    MSG_SEGURIDAD_DATOS = 'Datos';
    MSG_CLIENTE = 'Datos del Cliente';
    MSG_MEDIO_COMUNICACION = 'Medios de Comuniaci�n';
    MSG_DOMICILIO = 'Domicilios';
    MSG_MEDIO_PAGO = 'Medio de Pago';
    MSG_OTROS = 'Observaciones / Ubicaci�n / Medio de Envio';
    MSG_INICIA_NOTIF_TAGS = 'Reimpresi�n Notif. TAGs Vencidos';     // SS_960_PDO_20110608
    MSG_FONTS_ERROR = 'No se han podido instalar los fonts';
    MSG_INITIALIZATION_ERROR = 'Error de Inicializaci�n';
    MSG_NO_FACTURAR = 'Colocar como No facturables los tr�nsitos de veh�culos';
    MSG_BONIFICAR = 'Bonificar los tr�nsitos de veh�culos';
    MSG_CONFIRMAR_TIPO_MEDIO_PAGO_AUTOMATICO = 'Confirma bajo permiso el Tipo Medio Pago Autom�tico (PAT o PAC)';
    MSG_NO_GENERAR_INTERESES = 'No generar intereses para la proxima facturaci�n';
    MSG_EDICION_ACTIONLIST_TAG_PROPIOS  = 'Permite editar los ActionList de Telev�as Propios';
    MSG_EDICION_ACTIONLIST_TAG_TERCEROS = 'Permite editar los ActionList de Telev�as de Terceros';
    MSG_REALIZAR_RECLAMO_TRANSITOS = 'Habilita a Realizar Reclamos de Transitos';
    MSG_REALIZAR_RECLAMO_ESTACIONAMIENTOS = 'Habilita a Realizar Reclamos de Estacionamientos';     // SS_1006_PDO_20120106
    MSG_VER_PAT_PAC = 'Habilita a ver n�mero completo de Tarjeta de Cr�dito.';
    MSG_ENVIAR_MAIL_NK = 'Habilita a enviar Comprobantes al cliente v�a mail.';
    MSG_MODIFICAR_TIPO_CLIENTE = 'Modificar Tipo de Cliente (sem�foro)';
    MSG_EXPORTARXML = 'Permite generar el XML de Novedades (de Alta o de Baja) de un convenio existente';
    MSG_MODIFICARFECHAALTA = 'Editar Fecha Alta';
    MSG_RECUPERAR_VEHICULO_ROBADO = 'Habilita el boton de recuperaci�n de Veh�culos Robados en Alta de Cuenta';
    //Revision 7
    MSG_VER_INFORMES_DE_TERCEROS        	= 'Ver informes de terceros';
    MSG_MODIF_FECHA_VENCIMIENTO_REEMISION	= 'Fecha de Vencimiento Reemisi�n';
    //Revision 9
    MSG_ALTA_CONVENIOS_SIN_CUENTAS	        = 'Alta de convenios sin cuentas';
    MSG_ALTA_CONVENIOS_CON_CUENTAS	        = 'Alta de convenios con cuentas';
    MSG_ALTA_CONVENIO_SEGURIDAD = 'Seguridad - Alta Convenio';
    //Revision 13
    MSG_CONSULTA_CONVENIO_SEGURIDAD             = 'Seguridad - Consulta Convenio';
    MSG_PERMISOS_VER_TRANSITOS_FORMATO_CSV      = 'Permite ver los tr�nsitos en Formato CSV';
    //REV.14
    MSG_EDICION_FACTURACION_FECHA_EMISION		= 'Habilita el cambio de la fecha emisi�n en facturaci�n manual';
    //REV.18
    MSG_EDICION_DOCUMENTO_ELECTRONICO			= 'Habilita el cambio del tipo de documento electr�nico asociado al convenio';
    //REV.19
    //BIT_HABILITADO_LISTA_BLANCA_AMB 			= 'Habilitacion autom�tica de Lista Blanca en AMB';    //SS-1006-NDR-20111105
    BIT_ADHERIDO_LISTA_ACCESO_PA 			    = 'Habilitacion autom�tica de Lista de Acceso en PA';      //SS-1006-NDR-20111105
    BIT_ADHERIDO_LISTA_ACCESO_PA_INHABILITAR	= 'Inhabilitacion Lista de Acceso en PA';      //SS-1006-NDR-20111105
    //REV. 24
    INACTIVA_ACTUALIZACION_RNUT                 = 'Permite Inactivar las Actualizaciones de domicilios/Medios de Contacto desde RNUT';
    //REV.25
    MSG_EDICION_FECHA_VENCIMIENTO_TAG           = 'Permite Modificar la fecha de vencimiento de la garant�a del TAG';

    MSG_FACTURAR_CONVENIO_SEGURIDAD             = 'Seguridad - Facturar Convenio';  // SS-971-PDO-20111102

    MSG_CONVENIO_SIN_CUENTA                     = 'Permite designar un Convenio Sin Cuenta';                  //SS_628_ALA_20111201
    MSG_FORMATO_ARCHIVO_DETALLE                 = 'Permite indicar el formato del archivo con el detalle (PDF o TXT)';  // SS_1033_CQU_20120420
    MSG_EXPORTAR_CTACTE_EXCEL                   = 'Permite exportar el detalle de cuenta corriente a excel';    // SS_1046_CQU_20120530
    MSG_SEGURIDAD_ACTIONLIST                    = 'Seguridad - Action List';                     //SS_660_MBE_20121004
    MSG_ACTIONLIST_YELLOW_LIST                  = 'Permite colocar los TAGs en lista Amarilla';  //SS_660_MBE_20121004
    MSG_FACTURAR_INFRACTOR_POR_RUT              = 'Permite facturar infracciones por RUT';       // SS_660_CQU_20140311
    MSG_FACTURAR_INFRACTOR_MOROSIDAD            = 'Permite facturar infracciones por Morosidad';                    // SS_660E_CQU_20140806
    MSG_CERTIFICADO_INFRACTOR                   = 'Permite emitir el certificado de infracciones por Morosidad';    // SS_660D_CQU_20140806

    MSG_A_CLIENTES                              = 'Permite incorporar clientes para desplegar mensajes informativos';   //SS_1408_MCA_20151027
    MSG_PERMISO_EDITAR_FECHA_PD                 = 'Permite cambiar fechas de pases diarios'; //TASK_012_AUN_20170420-CU.COBO.INT.660

    //INICIO: TASK_007_ECA_20160428
     MSG_TIPO_CONVENIO_TODOS                    = 'Permite Consultar todos los Convenios';
     MSG_TIPO_CONVENIO_RNUT                     = 'Permite Consultar todos los Convenios tipo RNUT';
     MSG_TIPO_CONVENIO_CTA_COMERCIAL            = 'Permite Consultar todos los Convenios tipo CUENTA COMERCIAL';
     MSG_TIPO_CONVENIO_INFRACTOR                = 'Permite Consultar todos los Convenios tipo INFRACTOR';
    //FIN: TASK_007_ECA_20160428
Const

    FuncionesAdicionales: Array[1..58] of TPermisoAdicional = (	//TASK_012_AUN_20170420-CU.COBO.CRM.PDS.302

      (Funcion: 'seguridad_Facturar_convenio'; Descripcion: MSG_FACTURAR_CONVENIO_SEGURIDAD; Nivel: 1),             // SS-971-PDO-20111102
      (funcion: 'facturar_infractor_por_rut'; Descripcion: MSG_FACTURAR_INFRACTOR_POR_RUT; Nivel: 2),      // SS_660_CQU_20140311
      (funcion: 'facturar_infractor_moroso_por_rut'; Descripcion: MSG_FACTURAR_INFRACTOR_MOROSIDAD; Nivel: 2),      // SS_660E_CQU_20140806
      (funcion: 'certificado_infractor_moroso'; Descripcion: MSG_CERTIFICADO_INFRACTOR; Nivel: 2),                  // SS_660D_CQU_20140806
      //REV: 14                                                                                                     // SS-971-PDO-20111102
      (Funcion: 'FECHA_EMISION_FACTURACION_MANUAL'; Descripcion: MSG_EDICION_FACTURACION_FECHA_EMISION; Nivel: 2),  // SS-971-PDO-20111102
      //Revision 9
      (Funcion: 'seguridad_alta_convenio'; Descripcion: MSG_ALTA_CONVENIO_SEGURIDAD; Nivel: 1),
      (Funcion: 'seguridad_botones_sin_cuentas'; Descripcion: MSG_SEGURIDAD_BOTONES; Nivel: 2),
      (Funcion: 'alta_convenios_sin_cuentas';  Descripcion: MSG_ALTA_CONVENIOS_SIN_CUENTAS; Nivel: 3),
      //Fin Revision 9
      (Funcion: 'seguridad_modif_convenio'; Descripcion: MSG_SEGURIDAD_SEGURIDAD; Nivel: 1),
      (Funcion: 'seguridad_botones'; Descripcion: MSG_SEGURIDAD_BOTONES; Nivel: 2),
      (Funcion: 'boton_agregar'; Descripcion: MSG_AGREGAR; Nivel: 3),
      (Funcion: 'boton_editar'; Descripcion: MSG_EDITAR; Nivel: 3),
      (Funcion: 'boton_eliminar'; Descripcion: MSG_ELIMINAR; Nivel: 3),
      (Funcion: 'boton_cambiar_tag'; Descripcion: MSG_CAMBIAR_TAG; Nivel: 3),
      (Funcion: 'boton_cambiar_vehiculo'; Descripcion: MSG_CAMBIAR_VEH; Nivel: 3),
      (Funcion: 'boton_suspender'; Descripcion: MSG_SUSPENDER; Nivel: 3),
      (Funcion: 'boton_perdido'; Descripcion: MSG_PERDIDO; Nivel: 3),
      (Funcion: 'boton_blanquear_clave'; Descripcion: MSG_BLANQUEAR_CLAVE; Nivel: 3),
      (Funcion: 'boton_reimprimir'; Descripcion: MSG_REIMPRIMIR; Nivel: 3),
      (Funcion: 'boton_baja_convenio'; Descripcion: MSG_BAJA_CONVENIO; Nivel: 3),
      (Funcion: 'boton_EximirFacturacion'; Descripcion: MSG_NO_FACTURAR; Nivel: 3),
      (Funcion: 'boton_BonificarFacturacion'; Descripcion: MSG_BONIFICAR; Nivel: 3),
      (Funcion: 'boton_ExportarXML'; Descripcion: MSG_EXPORTARXML; Nivel: 3),
      (Funcion: 'Enviar_NK_Via_Mail'; Descripcion: MSG_ENVIAR_MAIL_NK; Nivel: 3),
      (Funcion: 'combo_CambiarTipoCliente'; Descripcion: MSG_MODIFICAR_TIPO_CLIENTE; Nivel: 3),
      (Funcion: 'seguridad_datos'; Descripcion: MSG_SEGURIDAD_DATOS; Nivel: 2),
      (Funcion: 'datos_cliente'; Descripcion: MSG_CLIENTE; Nivel: 3),
      (Funcion: 'datos_medio_comunicacion'; Descripcion: MSG_MEDIO_COMUNICACION; Nivel: 3),
      (Funcion: 'datos_domicilios'; Descripcion: MSG_DOMICILIO; Nivel: 3),
      (Funcion: 'datos_medio_pago'; Descripcion: MSG_MEDIO_PAGO; Nivel: 3),
      (Funcion: 'datos_otros'; Descripcion: MSG_OTROS; Nivel: 3),
      (Funcion: 'CONFIRMAR_TIPO_MEDIO_PAGO_AUTOMATICO'; Descripcion: MSG_CONFIRMAR_TIPO_MEDIO_PAGO_AUTOMATICO; Nivel: 3),
      //REV.18
      (Funcion: 'Combo_Modificar_Documento_Electronico'; Descripcion: MSG_EDICION_DOCUMENTO_ELECTRONICO; Nivel: 3),     // SS-971-PDO-20111102
      (Funcion: 'NO_GENERAR_INTERESES'; Descripcion: MSG_NO_GENERAR_INTERESES; Nivel: 3),
      (Funcion: 'EDICION_ACTIONLIST_TAG_PROPIOS'; Descripcion: MSG_EDICION_ACTIONLIST_TAG_PROPIOS; Nivel: 3),
      (Funcion: 'EDICION_ACTIONLIST_TAG_TERCEROS'; Descripcion: MSG_EDICION_ACTIONLIST_TAG_TERCEROS; Nivel: 3),
      (Funcion: 'REALIZAR_RECLAMO_TRANSITOS'; Descripcion: MSG_REALIZAR_RECLAMO_TRANSITOS; Nivel: 3),
      (Funcion: 'REALIZAR_RECLAMO_ESTACIONAMIENTOS'; Descripcion: MSG_REALIZAR_RECLAMO_ESTACIONAMIENTOS; Nivel: 3),     // SS_1006_PDO_20120106
      (Funcion: 'Ver_Numero_PAT_PAC'; Descripcion: MSG_VER_PAT_PAC; Nivel: 3),
      (Funcion: 'Boton_ModificarFechaAlta'; Descripcion: MSG_MODIFICARFECHAALTA; Nivel: 3),
      (Funcion: 'Boton_Recuperar_Vehiculo_Robado'; Descripcion: MSG_RECUPERAR_VEHICULO_ROBADO; Nivel: 3),


      //REV.24                                                                                                      //SS-971-NDR-20110823
      (Funcion: 'BIT_Inactiva_RNUT'; Descripcion: INACTIVA_ACTUALIZACION_RNUT; Nivel: 3),                           //SS-971-NDR-20110823
      //REV.25                                                                                                      //SS-971-NDR-20110823
      (Funcion: 'Modif_Fecha_Vencimiento_TAG'; Descripcion: MSG_EDICION_FECHA_VENCIMIENTO_TAG; Nivel: 3),           //SS-971-NDR-20110823
      //REV.19                       
      //(Funcion: 'BIT_Habilitado_Lista_Blanca_AMB'; Descripcion: BIT_HABILITADO_LISTA_BLANCA_AMB; Nivel: 3),     //SS-1006-NDR-20111105
      (Funcion: 'BIT_Adherido_Lista_Acceso_PA'; Descripcion: BIT_ADHERIDO_LISTA_ACCESO_PA; Nivel: 3),         //SS-1006-NDR-20111105
      (Funcion: 'BIT_Adherido_Lista_Acceso_PA_Inhabilitar'; Descripcion: BIT_ADHERIDO_LISTA_ACCESO_PA_INHABILITAR; Nivel: 3),         //SS-1006-NDR-20111105
      (Funcion: 'BIT_Convenio_Sin_Cuenta'; Descripcion: MSG_CONVENIO_SIN_CUENTA; Nivel: 3),                       //SS_628_ALA_20111201
	  (Funcion: 'Boton_Exportar_CtaCte_Excel'; Descripcion: MSG_EXPORTAR_CTACTE_EXCEL; Nivel: 3),                 // SS_1046_CQU_20120530

      // SS_660_MBE_20121004
      (Funcion: 'Seguridad_action_list'; Descripcion: MSG_SEGURIDAD_ACTIONLIST; Nivel: 1),        // SS_660_MBE_20121004
      (Funcion: 'ActionList_Amarillo';  Descripcion: MSG_ACTIONLIST_YELLOW_LIST; Nivel: 2),       // SS_660_MBE_20121004
      
      //Revision 13
      (Funcion: 'seguridad_consulta_convenio'; Descripcion: MSG_CONSULTA_CONVENIO_SEGURIDAD;      	Nivel: 1),
      (Funcion: 'inicia_notif_tags'; Descripcion: MSG_INICIA_NOTIF_TAGS; Nivel: 2),     // SS_960_PDO_20110608
      (Funcion: 'ver_transitos_formato_csv'; Descripcion: MSG_PERMISOS_VER_TRANSITOS_FORMATO_CSV; Nivel: 2),// SS_1047_CQU_20120528
      (Funcion: 'MENSAJES_A_CLIENTES'; Descripcion: MSG_A_CLIENTES; Nivel: 2), //; TASK_007_ECA_20160428	//SS_1408_MCA_20151027// SS_1047_CQU_20120528

      (Funcion: 'Permiso_CambiarFechasPasesDiarios'; Descripcion: MSG_PERMISO_EDITAR_FECHA_PD; Nivel: 1), //TASK_012_AUN_20170420-CU.COBO.CRM.PDS.302
      
       //INNICIO: TASK_007_ECA_20160428
       //Seguridad consulta tipo de convenio
      (Funcion: 'TIPO_CONVENIO_TODOS'; Descripcion: MSG_TIPO_CONVENIO_TODOS; Nivel: 2),
      (Funcion: 'TIPO_CONVENIO_RNUT'; Descripcion: MSG_TIPO_CONVENIO_RNUT; Nivel: 2),
      (Funcion: 'TIPO_CONVENIO_CTA_COMERCIAL'; Descripcion: MSG_TIPO_CONVENIO_CTA_COMERCIAL; Nivel: 2),
      (Funcion: 'TIPO_CONVENIO_INFRACTOR'; Descripcion: MSG_TIPO_CONVENIO_INFRACTOR; Nivel: 2));
       //FIN:TASK_007_ECA_20160428

      //Revision 7
//      (Funcion: 'ver_informes_de_terceros';       	Descripcion: MSG_VER_INFORMES_DE_TERCEROS;      	Nivel: 1),  // SS-971-PDO-20111102
//      (Funcion: 'Modif_Fecha_Vencimiento_Reemision'; Descripcion: MSG_MODIF_FECHA_VENCIMIENTO_REEMISION; Nivel: 2));  // SS-971-PDO-20111102


begin
    Result := False;
    Application.UpdateFormatSettings:= False;
    ShortDateFormat:='yyyy/mm/dd';
    ShortTimeFormat:='hh:nn:ss';

	Application.Title := SYS_CAC_TITLE;
    SistemaActual := SYS_CAC;
    Width   := Screen.WorkAreaWidth;
    Height  := Screen.WorkAreaHeight;
    Left    := Screen.WorkAreaLeft;
    Top     := Screen.WorkAreaTop;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    CambiarEventoMenu(MainMenu);                                                                            //SS_1147_NDR_20141216

    try
        // Login
        Application.CreateForm(TLoginForm, f);
        if f.Inicializar(DMConnections.BaseCAC, SYS_CAC) and (f.ShowModal = mrOk) then begin
            UsuarioSistema := f.CodigoUsuario;

            //BEGIN : SS_925_NDR_20131122-----------------------------------------------------------------------
            //if not ValidarVersion(DMConnections.BaseCAC, SistemaActual) then begin
            //    MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
            //    Exit;
            //end;

            // StatusBar
            StatusBar.Panels.Items[0].Text := Format(MSG_USER, ['']);

            // Par�metros de DDE
            FDDETopic := TDDEServerConv.Create(Self);
            FDDETopic.Name := InstallIni.ReadString('DDE', 'Topic', 'CTI');
            FDDEItem  := TDDEServerItem.Create(Self);
            FDDEItem.Name  := InstallIni.ReadString('DDE', 'Item', 'CTI_Data');
            FDDEItem.ServerConv := FDDETopic;
            FDDEItem.OnPokeData := DoPokeData;
            if not CheckFonts then MsgBoxErr(MSG_INITIALIZATION_ERROR, MSG_FONTS_ERROR, STR_ERROR, MB_ICONERROR);
            InicializarVariablesCac;

            //Ok := true;
            // Men�es
            //if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) then begin
            //    ok := GenerateMenuFile(MainMenu, SYS_CAC, DMConnections.BaseCAC, FuncionesAdicionales);
            //end;

            {INICIO:	20160831 CFU
            CargarInformes(DMConnections.BaseCAC, SYS_CAC, MainMenu);                 //SS_1437_NDR_20151230
            TERMINO:	20160831 CFU}
            if not ValidarVersion(DMConnections.BaseCAC, SistemaActual, MainMenu, FuncionesAdicionales) then begin
                MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
                Exit;
            end;
            ok := True;
            //END : SS_925_NDR_20131122-----------------------------------------------------------------------


            if ok then begin
                mnu_PaseDiarioConsultaPorPatente.Enabled := False; //TASK_012_AUN_20170420-CU.COBO.CRM.PDS.302
                RefrescarBarraDeEstado;
                CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_CAC);
                HabilitarPermisosMenu(MainMenu);

            	//INICIO:	20160831 CFU
                CargarInformes(DMConnections.BaseCAC, SYS_CAC, MainMenu);
                //TERMINO:	20160831 CFU

                //CargarInformes(DMConnections.BaseCAC, SYS_CAC, MainMenu, DMConnections.cnInformes);             //SS_1437_NDR_20151230

                //Genero el Menu de Reclamos Generales
                GenerarMenuReclamosGenerales(mnuReclamosGenerales);
                //Punto de Entrega y Venta
                { INICIO : 20160315 MGO
                PuntoEntrega:= ApplicationIni.ReadInteger('General', 'PuntoEntrega', -1);
                PuntoVenta:= ApplicationIni.ReadInteger('General', 'PuntoVenta', -1);
                }
                PuntoEntrega:= InstallIni.ReadInteger('General', 'PuntoEntrega', -1);
                PuntoVenta:= InstallIni.ReadInteger('General', 'PuntoVenta', -1);
                // FIN : 20160315 MGO
                if not VerificarPuntoEntregaVenta(DMConnections.BaseCAC, PuntoEntrega, PuntoVenta) then begin
                    MsgBox(MSG_ERROR_PUNTO_ENTREGA_VENTA, self.Caption, MB_ICONWARNING);
                    DeshabilitarTodosItemsTurnos;
                    PuntoEntrega := -1;
                    PuntoVenta := -1;
                end;

                CambiarEventoMenu(MainMenu);                                    //SS_1147_NDR_20141216

                if not mnu_abrir_turno.Visible then begin //Usuario sin permiso para abrir turno
                    HabilitarItemsTurno(False);
                    Result := True;
                end else begin
                    if mnu_abrir_turno.Enabled then begin
                        if VerTurnoAbierto(DMConnections.BaseCAC, PuntoEntrega, PuntoVenta, UsuarioSistema, DatosTurno) then begin
                            HabilitarItemsTurno(True);
                            GNumeroTurno := DatosTurno.NumeroTurno;
                        end else begin
                            if (DatosTurno.NumeroTurno < 1) or (DatosTurno.Estado = TURNO_ESTADO_CERRADO) then GNumeroTurno := -1;
                            if (DatosTurno.NumeroTurno <> -1)
                                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                                    and (Trim(DatosTurno.CodigoUsuario) <> (uppercase(UsuarioSistema)))) then begin//Si es distinto es que en el mismo POS y PV hay un turno abierto por otro usuario
                                DeshabilitarTodosItemsTurnos;
                                MsgBox(Format(MSG_CAPTION_TURNO_ABIERTO_OTRO_USUARIO,[DatosTurno.CodigoUsuario]), self.Caption, MB_ICONWARNING);
                            end else begin
                                HabilitarItemsTurno(False);
                            end;
                        end;
                    end;
                    if mnu_CerrarTurnosTerceros.Visible then mnu_ver_Turnos.Visible := False;
                    Result := True;
                end;
                //mnuInfraMorosidad.Enabled := ExisteAcceso('facturar_infractor_moroso_por_rut'); // TASK_144_MGO_20170301
            end;

            if Result then InicializarIconBar;
            if Result then VerificarExistenCotizacionesDelDia;

            tmrEstadoMigracionTimer(tmrEstadoMigracion);
            //mnuNoClientes.Visible := False; // SS_1138_CQU_20140129   // SS_1138_CQU_20131127

            //INICIO: TASK_017_ECA_20160528
            mnu_SolicitudesdeContacto.Visible:=False;
            mnuBajaForzadaMasiva.Visible:=False;
            //mnu_PaseDiario.Visible:=False;                        //TASK_034_GLE_20170119 
            //FIN: TASK_017_ECA_20160528

            //INICIO: TASK_035_ECA_20160620 - Revalidaci�n
            //mnu_ProcesoFacturacion.Visible:=False;
            mnuEmitirFacturaInfracciones.Visible:=False;
            EmitirCertificadodeInfracciones1.Visible:=False;
            mnuTicketVentaManual.Visible:=False;
            mnuNotaCreditoInfractor.Visible:=False;
            mnuTicketVentaManual.Visible:=False;
            //FIN: TASK_035_ECA_20160620 - Revalidaci�n
        end
        else                                                                    //SS_1436_NDR_20151230
        begin                                                                   //SS_1436_NDR_20151230
            UsuarioSistema := f.CodigoUsuario;                                  //SS_1436_NDR_20151230
        end;                                                                    //SS_1436_NDR_20151230
    finally
        f.Release;
        if Assigned(FormStartup) then FreeAndNil(FormStartup);
    end;
end;

procedure TMainForm.RefrescarBarraDeEstado;
var
	UltimoPanel: Integer;
begin
  Caption := Caption +  ' >> ' + ObtenerNombreConcesionariaNativa();                                             //SS_1147AAA_NDR_20150414
	StatusBar.Panels.Items[0].Text  := Format(MSG_USER, [UsuarioSistema]);
    StatusBar.Panels[0].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[0].text));

	StatusBar.Panels[1].Text := Trim(Format(MSG_CONECTADO_A, ['Provider ' + DMConnections.BaseCAC.Provider + ' - ' + InstallIni.ReadString('Database CAC', 'Server', ''), InstallIni.ReadString('Database CAC', 'DatabaseName', '')])) + ' >> ' + ObtenerNombreConcesionariaNativa(); //SS_1147AAA_NDR_20150414
	StatusBar.Panels[1].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[1].text));

    UltimoPanel := StatusBar.Panels.Count-1;
    if VerificarPuntoEntregaVenta(DMConnections.BaseCAC,
      //{ INICIO : 20160315 MGO
      ApplicationIni.ReadInteger('GENERAL', 'PuntoEntrega', 0), Applicationini.ReadInteger('GENERAL', 'PUNTOVENTA', 0)) then
        StatusBar.Panels[UltimoPanel].Text := format(' %s (%s)',
          [ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,
            ApplicationIni.ReadInteger('GENERAL', 'PuntoEntrega', 0)),
              IntToStr(ApplicationIni.ReadInteger('GENERAL', 'PuntoEntrega', 0))])
      {
      InstallIni.ReadInteger('GENERAL', 'PuntoEntrega', 0), InstallIni.ReadInteger('GENERAL', 'PUNTOVENTA', 0)) then
        StatusBar.Panels[UltimoPanel].Text := format(' %s (%s)',
          [ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,
            InstallIni.ReadInteger('GENERAL', 'PuntoEntrega', 0)),
              IntToStr(InstallIni.ReadInteger('GENERAL', 'PuntoEntrega', 0))])
      }
      // FIN : 20160315 MGO
    else
        StatusBar.Panels[UltimoPanel].Text := MSG_PUNTO_ENTREGA_VENTA;

    StatusBar.Panels[UltimoPanel].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[UltimoPanel].text));
end;

(*
function TMainForm.TerminarLLamada(Finalizar, Observaciones: Boolean): Boolean;
resourcestring
	MSG_TERMINAR_LLAMADA = '�Desea terminar la llamada en curso?';
//var
//	f : TFormObservaciones;
//    MsgError: TMsgError;
begin

	Result := False;
	if gCodigoComunicacion <> 0 then begin
        try
			Application.CreateForm(TFormObservaciones, f);
            if f.Inicializa(gCodigoComunicacion) then begin
                f.ShowModal;
                if f.ModalResult = mrOk then begin
                    // Actualizar la Fecha de Finalizacion de la Llamada.
                    if not FinalizarComunicacion(gCodigoComunicacion, MsgError) then exit;
                    if ContactoComunicacion.Persona.CodigoPersona = 0 then begin
                        //Si no hay persona relacionada, me fijo si cargo los datos del contacto
                        //(mientras no sea an�nima la guardo).
                        if (trim(ContactoComunicacion.Persona.Apellido) <> '') then begin
                            //No tiene codigo e ingreso el apellido, grabo el contacto!!!


                        end;
                    end;
                    Tmr_llamada.Enabled := False;
					Navegador.ClearHotLinks;
                    // Debo verificar si queda alguna navwindow abierta y cerrarla
                    if Assigned(Navegador.CurrentNavWindow) then Navegador.CurrentNavWindow.Close;
                    Result := True;
                end;
            end;
        finally
            f.Release;
        end;
    end;
    if Result then InicializarVariablesCAC;
end;
*)

procedure TMainForm.mnu_salirClick(Sender: TObject);
begin
	Close;
end;

procedure TMainForm.Cerrar1Click(Sender: TObject);
begin
	Close;
end;
{
****************************** Cabecera Procedure ******************************
 Nombre Procedure: tmrEstadoMigracionTimer
 Autor           : Pedro Segu� Dom�nguez
 Fecha Creaci�n  :
 Descripci�n     :
---------------------------------- Par�metros ----------------------------------
 Sender: TObject
---------------------------------- Comentarios ---------------------------------
 Se cre� para actualizar el estado de las variables de la Migraci�n al Nuevo
 Formato de Im�genes y visualizarlo en el objeto StatusBar.Panels[3] del Form
 MainForm.
---------------------------- Historial de Revisiones ---------------------------
 Sin Revisiones.
********************************************************************************
}
procedure TMainForm.tmrEstadoMigracionTimer(Sender: TObject);
ResourceString
    MSG_MIGRACION_INFO_STATUSBAR = ' Estado Migraci�n NFI: %s';
//Var																	//SS_1408_MCA_20151027
//    EstadoMigracion: AnsiString;										//SS_1408_MCA_20151027
begin
 {   try
        tmrEstadoMigracion.Enabled := False;
        CargarParametrosMigracionNFI(DMConnections.BaseCAC);

        if NuevoFormatoActivo then begin
            if NuevoFormatoFinMigracion then
                EstadoMigracion := Format(MSG_MIGRACION_INFO_STATUSBAR,[Format('Finalizada el %s.',[FormatDateTime('dd/mm/yyyy',FechaFinMigracionNuevoFormatoImagen)])])
            else
                EstadoMigracion := Format(MSG_MIGRACION_INFO_STATUSBAR,[Format('Iniciada el %s.',[FormatDateTime('dd/mm/yyyy',FechaInicioMigracionNuevoFormatoImagen)])]);
        end
        else EstadoMigracion := Format(MSG_MIGRACION_INFO_STATUSBAR,['NO Iniciada.']);

        StatusBar.Panels[3].Text := EstadoMigracion;
    finally
        tmrEstadoMigracion.Enabled := Not NuevoFormatoFinMigracion;
    end;
    }end;

procedure TMainForm.tmr_llamadaTimer(Sender: TObject);
resourcestring
	MSG_DURACIONLLAMADA	= 'Duraci�n de la Llamada: %s';
begin
	StatusBar.Panels.Items[2].Text :=
	  Format(MSG_DURACIONLLAMADA, [FormatDateTime('hh:mm:ss', Now - FFechaHoraInicioComunicacion)])
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
ResourceString                                                                                      // SS_1117_CQU_20130802
    MSG_TURNO_ABIERTO = '�Desea cerrar el Turno antes de salir del sistema?';                       // SS_1117_CQU_20130802
    MSG_AVISO         = 'Aviso';                                                                    // SS_1117_CQU_20130802
var                                                                                                 // SS_1117_CQU_20130802
    Respuesta: integer;                                                                             // SS_1117_CQU_20130802
begin
	if gCodigoComunicacion = 0 then
		CanClose := True
    else begin
//       	Canclose := TerminarLLamada(False, True);
    end;
    if CanClose and (GNumeroTurno > 0) then begin                                                   // SS_1117_CQU_20130802
        Respuesta := MsgBox(MSG_TURNO_ABIERTO, MSG_AVISO, MB_ICONINFORMATION or MB_YESNOCANCEL);    // SS_1117_CQU_20130802
        if Respuesta = IDYES then begin                                                             // SS_1117_CQU_20130802
            actCerrarTurnoExecute(self);                                                            // SS_1117_CQU_20130802
	        CanClose := False;                                                                      // SS_1117_CQU_20130802
        end;                                                                                        // SS_1117_CQU_20130802
        if Respuesta = IDNO then CanClose := True;                                                  // SS_1117_CQU_20130802
        if Respuesta = IDCANCEL then CanClose := False;                                             // SS_1117_CQU_20130802
    end;                                                                                            // SS_1117_CQU_20130802
end;

 procedure TMainForm.FormCreate(Sender: TObject);
begin
    {$IFNDEF PRODUCCION}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
    {$ELSE}
//        Caption := Caption + Format(' - Versi�n: %s', [GetFileVersionString(GetExeName, True)]);
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
    {$ENDIF}
end;

{
 ****************************** Cabecera Procedure ******************************
  Nombre Procedure: InicializarVariablesCAC
  Autor           :
  Fecha Creaci�n  :
  Descripci�n     : 
 ---------------------------------- Par�metros ----------------------------------
  Ninguno.
 ---------------------------------- Comentarios ---------------------------------
  Ninguno.
 ---------------------------- Historial de Revisiones ---------------------------
  Revisi�n   : 1
  Autor      : pdominguez
  Fecha      : 18/02/2009
  Descripci�n: Se agreg� la inicializaci�n del panel a�adido al objeto StatusBar.
 ********************************************************************************
 }
procedure TMainForm.InicializarVariablesCAC;
begin
	gCodigoComunicacion				:= 0;
    fillChar(ContactoComunicacion, sizeof(ContactoComunicacion), #0);
    FFechaHoraInicioComunicacion	:= NullDate;
    StatusBar.Panels.Items[1].Text	:= '';
    StatusBar.Panels.Items[2].Text	:= '';
    StatusBar.Panels.Items[3].Text	:= '';
    StatusBar.Panels.Items[4].Text	:= '';
end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
//	DrawMDIBackground(ImagenFondo.Picture.Graphic);
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
//	DrawMDIBackground(ImagenFondo.Picture.Graphic);
end;

procedure TMainForm.mnu_TerminarLlamadaencursoClick(Sender: TObject);
resourcestring
    MSG_NO_EXISTE_LLAMADA = 'No hay ninguna llamada en curso.';
begin
	if gCodigoComunicacion = 0 then begin
		MsgBox(MSG_NO_EXISTE_LLAMADA, CAPTION_FIN_COMUNICACION, MB_ICONINFORMATION);
	end else begin
//		TerminarLLamada(False, True);
	end;
end;

procedure TMainForm.Mnu_CascadaClick(Sender: TObject);
begin
	if sender = mnu_cascada then cascade
	  else if sender = mnu_mosaico then tile
	  else if sender = mnu_siguiente then next
	  else if sender = mnu_anterior then previous;
end;

{******************************** Function Header ******************************
Function Name: mnu_AcercaDeClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision : 1
    Author : ggomez
    Date : 29/01/2007
    Description : En la llamada f.Inicializa, agregu� el segundo par�metro ''
        debido a que cambi� el perfil de la funci�n en las Utiles.
*******************************************************************************}
procedure TMainForm.mnu_AcercaDeClick(Sender: TObject);
{                                                       SS_925_MBE_20131223
resourcestring
    CAPTION_ACERCADE = 'Atenci�n al Usuario';
var
    f : TAboutFrm;
    a: TfrmLocalAssistance;
}
begin
{                                                       SS_925_MBE_20131223
    Application.CreateForm(TfrmLocalAssistance, a);
    try
        if a.Inicializar then begin
            Application.CreateForm(TAboutFrm, f);
            if f.Inicializa(CAPTION_ACERCADE, '', '', a) then f.ShowModal;
            f.Release;
        end;
    finally
        a.Release;
    end; // finally
}
    Application.CreateForm(TAcercaDeForm, AcercaDeForm);
    AcercaDeForm.ShowModal;
    AcercaDeForm.Release;
end;

procedure TMainForm.mnu_CambiodeContrasenaClick(Sender: TObject);
Var
	f: TFormCambioPassword;
begin
	if not mnu_CambiodeContrasena.Enabled then Exit;
	Application.CreateForm(TFormCambioPassword, f);
	if f.Inicializa(DmConnections.BaseCAC, UsuarioSistema) then f.ShowModal;
	f.Release;
end;

procedure TMainForm.mnu_VentanaClick(Sender: TObject);
begin
	mnu_cascada.Enabled 	:= GetMenuItemCount(mnu_ventana.handle) > 5;
	mnu_mosaico.Enabled		:= mnu_cascada.Enabled;
	mnu_anterior.Enabled	:= GetMenuItemCount(mnu_ventana.handle) > 7;
	mnu_siguiente.Enabled 	:= mnu_anterior.Enabled;
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Shift = []) and (Key = VK_ESCAPE) then close;
end;


procedure TMainForm.mnu_comunicacionesClick(Sender: TObject);
begin
(*
	mnu_NuevaComunicacion.Enabled		:= (gCodigoComunicacion = 0);
	mnu_TerminarLLamadaEnCurso.Enabled	:= not mnu_NuevaComunicacion.Enabled;
*)
end;

procedure TMainForm.ComenzarNavegacion(Const NumeroRut: String = '');
var
    f: TFormInicioConsultaConvenio;
begin
    //Nos pasaron un RUT que est� en el tel�fono
    //Debemos comenzar una comunicacion en el CAC
    //registrando los datos de la llamada
    //y finalmente mostrar la cartola..
   if FindFormOrCreate(TFormInicioConsultaConvenio, f) then begin
        f.Show;
        f.IrARUT(NumeroRut);
   end else begin
        if f.Inicializar(0, 0, 0) then begin
            f.Show;
            f.IrARUT(NumeroRut);
        end
        else f.Release;
   end;
end;



procedure TMainForm.mniConsultaPatenteAraucoTAG1NOClick(Sender: TObject);                  //SS_1118 _MVI_20130805
//var                                                                                   //SS_1118 _MVI_20130805
    //f: TConsultaPatenteAraucoTAGForm;                                                 //SS_1118 _MVI_20130805
begin                                                                                 //SS_1118 _MVI_20130805
                                                                                      //SS_1118 _MVI_20130805
//       if FindFormOrCreate(TConsultaPatenteAraucoTAGForm, f) then f.Show              //SS_1118 _MVI_20130805   //TASK_002_ECA_20160304
//        else if not f.Inicializar() then f.Release;                                   //SS_1118 _MVI_20130805   //TASK_002_ECA_20160304
end;                                                                                  //SS_1118 _MVI_20130805


{******************************** Function Header ******************************
Function Name: DoPokeData
Author : ddiaz
Date Created : none
Description : Atiende mensajes recibidos de la central telefonica
Parameters : Sender: TObject
Return Value : None
*******************************************************************************
Revision 1
Author : lgisuk
Date Created : 21/11/06
Description : Ahora si se produce una excepcion se maneja muestrando un cartel
informando que el comando DDE no pudo ser procesado y el detalle del mensaje
recibido.
*******************************************************************************
Revision 2
Author : lgisuk
Date Created : 21/11/06
Description : Ahora si el rut informado por la central telefonica viene vacio
mostramos un cartel informando la situacion.
*******************************************************************************}
procedure TMainForm.DoPokeData(Sender: TObject);
resourcestring
    MSG_CTI             = 'Mensaje: %s';
    CAPTION_MENSAJE_CTI = 'Datos Recibidos';
    MSG_INICIAR_COMUNICACION = 'No se pudo iniciar la comunicaci�n.';
    MSG_FINALIZAR_COMUNICACION = 'No se pudo finalizar la comunicaci�n.';
    CAPTION_INICIAR_COMUNICACION = 'Comenzar Comunicaci�n';
    CAPTION_FINALIZAR_COMUNICACION = 'Finalizar Comunicaci�n';
    FAIL_INSERT_LOG_COMUNICATION = 'Fallo agregar log comunicaciones';
    MSG_OPEN_COMMUNICATION = 'Existe una Comunicaci�n Abierta.' + CRLF + 'Desea Finalizarla ?';
    MSG_COMM = 'Comunicaci�n';
    MSG_UNKNOWN_DDE_CMD = 'Comando DDE Desconocido';
    MSG_ERROR_EMPTY_RUT = 'Se recibio una llamada entrante pero no hay informaci�n de rut disponible';
    MSG_ERROR_PROCESS_CMD = 'No se puede procesar el comando DDE Recibido';
    MSG_ERROR = 'Error';
var
	ParamValue, Str: AnsiString;
    TextoError: String;
	Comando: Integer;
const
    CMD_ATTACH_DATA = 120;
    STR_CTI = 'CTI';
    STR_CMD = 'Comando = ';
begin
    // Esta activa la integraci�n con el CTI?
    if InstallIni.ReadInteger('DDE', 'Active', 0) <> 1 then begin
        FDDEItem.Lines.Text := '';
        Exit;
    end;
	// Lleg� un mensaje CTI, lo leemos y s� corresponde, lo mostramos
    // esto es t�picamente para debug
	Str := FDDEItem.Lines.Text;
	if Installini.ReadInteger('DDE', 'ShowMsg', 0) <> 0 then begin
	    MsgBox(format(MSG_CTI, [trim(Str)]), CAPTION_MENSAJE_CTI, MB_ICONINFORMATION);
    end;
	// Parseamos el mensaje que es lo que est� a cotinuaci�n del primer Pipe
	Comando := IVal(ParseParamByNumber(Str, 1, '|'));
    // Si el comando es alguno de los que esperamos entonces lo procesamos
    case Comando of
        CMD_ATTACH_DATA:
            begin
                ParamValue := Trim(ParseParamByNumber(Str, 2, '|'));

                //Verifico si el rut esta vacio
                if ParamValue = '' then begin
                  MsgBox(MSG_ERROR_EMPTY_RUT, STR_CTI, MB_ICONERROR);
                  Exit;
                end;
                
                //Valido el Rut
                try
                    ParamValue := ValidarRUTCTI(ParamValue);
                Except
                    on E : Exception do begin
                        MsgBoxErr(MSG_ERROR_PROCESS_CMD + CRLF + STR_CMD + STR, e.Message + CRLF + STR_CMD + STR, STR_CTI, MB_ICONERROR); //Agregado en revision 1
                        Exit;
                    end;
                end;

                if (ParamValue <> '') and (not EnComunicacion) then begin
                    // Recib� algo que puede ser un RUT y no estoy en
                    // en una comunicaci�n por lo tanto intento empezar una
                    // y si lo consigo muestro la pantalla inicial
                    if IniciarComunicacion(ParamValue, GCodigoComunicacion, TextoError) then begin
                        ComenzarNavegacion(ParamValue);
                        CheckMenuComunicaciones(true);
                    end else begin
                        MsgBoxErr(MSG_INICIAR_COMUNICACION, TextoError, CAPTION_INICIAR_COMUNICACION, MB_ICONERROR);
                    end;
                end else if (ParamValue <> '') and EnComunicacion then begin
                    // Recib� algo que puede ser un rut pero estoy en comunicaci�n
                    // esto no deber�a ocurrir, pero si ocurre alerto al operador.
                    if MsgBox(MSG_OPEN_COMMUNICATION, CAPTION_INICIAR_COMUNICACION, MB_YESNO) = mrYes then begin
                        if TerminarComunicacion(gCodigoComunicacion, TextoError) then begin
                            if IniciarComunicacion(ParamValue, GCodigoComunicacion, TextoError) then begin
                                ComenzarNavegacion(ParamValue);
                                CheckMenuComunicaciones(true);
                            end else begin
                                MsgBoxErr(MSG_INICIAR_COMUNICACION, TextoError, CAPTION_INICIAR_COMUNICACION, MB_ICONERROR);
                            end;
                        end else begin
                            MsgBoxErr(MSG_FINALIZAR_COMUNICACION, TextoError, CAPTION_FINALIZAR_COMUNICACION, MB_ICONERROR);
                        end;
                    end;
                end;
            end;
        else begin
            MsgBox(MSG_UNKNOWN_DDE_CMD, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

procedure TMainForm.mnu_BackOfficeClick(Sender: TObject);
begin
//--
end;

//******************************************************************************
//                                       Reporte
//******************************************************************************
procedure TMainForm.mnu_IconBarClick(Sender: TObject);
begin
	GNavigator.IconBarVisible	:= not GNavigator.IconBarVisible;
    mnu_IconBar.Checked			:= GNavigator.IconBarVisible;
end;

procedure TMainForm.mnu_ConsultaAltaPAraucoClick(Sender: TObject); //  SS-1006-GVI-20120619
begin
    try
	    CambiarEstadoCursor(CURSOR_RELOJ);
 //INICIO : TASK_002_ECA_20160304                                                                                                              //TASK_002_ECA_20160304
//        if Assigned(FormConsultaAltaPArauco) then begin                                                                  //TASK_002_ECA_20160304
//            if FormConsultaAltaPArauco.WindowState = wsMinimized then FormConsultaAltaPArauco.WindowState := wsNormal;   //TASK_002_ECA_20160304
//            FormConsultaAltaPArauco.BringToFront;                                                                        //TASK_002_ECA_20160304
//        end                                                                                                              //TASK_002_ECA_20160304
//        else begin                                                                                                       //TASK_002_ECA_20160304
//            Application.CreateForm(TFormConsultaAltaPArauco, FormConsultaAltaPArauco);                                   //TASK_002_ECA_20160304
//            if FormConsultaAltaPArauco.Inicializar then                                                                  //TASK_002_ECA_20160304
//                FormConsultaAltaPArauco.Show                                                                             //TASK_002_ECA_20160304
//            else FormConsultaAltaPArauco.Free;                                                                           //TASK_002_ECA_20160304
//        end;                                                                                                             //TASK_002_ECA_20160304

        if Assigned(FormConsultaAltaOtraConsecionaria) then begin                                                                    //TASK_002_ECA_20160304
            if FormConsultaAltaOtraConsecionaria.WindowState = wsMinimized then FormConsultaAltaOtraConsecionaria.WindowState := wsNormal; //TASK_002_ECA_20160304
            FormConsultaAltaOtraConsecionaria.BringToFront;                                                                          //TASK_002_ECA_20160304
        end                                                                                                                    //TASK_002_ECA_20160304
        else begin                                                                                                             //TASK_002_ECA_20160304
            Application.CreateForm(TFormConsultaAltaOtraConsecionaria, FormConsultaAltaOtraConsecionaria);                                  //TASK_002_ECA_20160304
            if FormConsultaAltaOtraConsecionaria.Inicializar then                                                                    //TASK_002_ECA_20160304
                FormConsultaAltaOtraConsecionaria.Show                                                                               //TASK_002_ECA_20160304
            else FormConsultaAltaOtraConsecionaria.Free;                                                                             //TASK_002_ECA_20160304
        end;                                                                                                                   //TASK_002_ECA_20160304
//TERMINADO: TASK_002_ECA_20160304
    finally
	    CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TMainForm.mnu_ConsultaSOlicitudesClick(Sender: TObject);
var
    f:TFormSolicitudConsulta;
begin
	if FindFormOrCreate(TFormSolicitudConsulta, f) then
		f.Show
	else begin
		if not f.Inicializar(True, mnu_ConsultaSOlicitudes.Caption, PuntoVenta, PuntoEntrega) then f.Release;
	end;
end;

procedure TMainForm.mnu_FAQsClick(Sender: TObject);
var
	f: TfrmFAQs;
begin
	if FindFormOrCreate(TfrmFAQs, f) then
		f.Show
	else begin
		if not f.Inicializar(True) then f.Release;
	end;
end;

procedure TMainForm.mnu_ModificarSolicitudesClick(Sender: TObject);
var
    f:TFormSolicitudModifcacion;
begin
	if FindFormOrCreate(TFormSolicitudModifcacion, f) then
		f.Show
	else begin
		if not f.Inicializar(True, mnu_ModificarSolicitudes.Caption, PuntoVenta, PuntoEntrega) then f.Release;
	end;
end;

procedure TMainForm.mnu_GestionCamposObligatoriosClick(Sender: TObject);
var
    f:TFormGestionComponentesObligatorios;
begin
	if FindFormOrCreate(TFormGestionComponentesObligatorios, f) then
		f.Show
    else begin
		if not f.inicializa(True, SistemaActual) then f.Release;
	end;

end;


procedure TMainForm.mnu_recibir_tagsClick(Sender: TObject);
var
    f: TFormRecibirTagsNominados;
begin
    Application.CreateForm(TFormRecibirTagsNominados, f);
    if (f.Inicializar(LimpiarCaracteresLabel(mnu_recibir_tags.Caption),UsuarioSistema, PuntoEntrega,True)) then
        f.ShowModal;
    f.Release;
end;

procedure TMainForm.mnu_MostrarScriptSolicitudContactoClick(
  Sender: TObject);
var
    F: TFormMostrarDocumentos;
    archivo: AnsiString;
begin
    archivo := '';
	if FindFormOrCreate(TFormMostrarDocumentos, f) then
		f.Show
	else begin
        ObtenerParametroGeneral(DMConnections.BaseCAC, SCRIPT_SOLICITUD_CONTACTO, archivo);
		if (not f.Inicializa(True,LimpiarCaracteresLabel(mnu_MostrarScriptSolicitudContacto.Caption), archivo)) then f.Release;
	end;
end;



procedure TMainForm.mnu_Listado_ConvenioClick(Sender: TObject);
var
    f: TFormListadoConveniosConsulta;
begin
	if FindFormOrCreate(TFormListadoConveniosConsulta, f) then f.Show
	else begin
		if not f.Inicializar(LimpiarCaracteresLabel(mnu_Listado_Convenio.Caption), True) then
            f.Release;

        f.OnClickRegSeleccionadoConsulta := OnClickConsulta;
	end;
end;

procedure TMainForm.mnu_ver_TurnosClick(Sender: TObject);
var
    f: TFormListadoTurnos;
begin
	if FindFormOrCreate(TFormListadoTurnos, f) then begin
        f.Show;
	end else begin
        if f.Inicializar(True, PuntoEntrega, PuntoVenta) then
            f.Show
        else f.Release;
    end;
end;

procedure TMainForm.mnu_CerrarTurnosTercerosClick(Sender: TObject);
var
    f: TFormListadoTurnos;
begin
	if FindFormOrCreate(TFormListadoTurnos, f) then begin
        f.Show;
	end else begin
        if f.Inicializar(True, PuntoEntrega, PuntoVenta) then
            f.show
        else f.Release;
    end;
end;

{******************************** Function Header ******************************
Procedure Name: HabilitarItemsTurno
Author :
Date Created :
Parameters : habilitar: boolean
Return Value : None
Description :

Revision: 1
    Author : pdominguez
    Date   : 03/02/2010
    Description : SS 510
            - Se incluye la opci�n "Configuraci�n del Puesto" de la opci�n de
            menu "Mantenimiento" dentro de la gesti�n del procedimiento, para
            que no est� disponible cuando el turno est� ya abierto.
*******************************************************************************}
procedure TMainForm.HabilitarItemsTurno(habilitar: boolean);
begin
    actAbrirTurno.Enabled := not Habilitar;
    actConfiguracionPuesto.Enabled := not habilitar; // Rev. 1 SS 510
    actCerrarTurno.Enabled := Habilitar;

	mnu_adicionar_tag_turno.Enabled := habilitar;
    mnuAjustedeCaja.Enabled := habilitar;                   //TASK_031_GLE_20170111

    mnu_recibir_tags.Enabled := True;
    mnu_pedir_reposicion.Enabled := True;

    mnu_PaseDiarioConsultaPorPatente.Enabled := Habilitar; //TASK_012_AUN_20170420-CU.COBO.CRM.PDS.302

	if habilitar then StatusBar.Panels.Items[2].Text := MSG_CAPTION_ESTADO_TURNO_ABIERTO
	else begin
        GNumeroTurno := -1;
        StatusBar.Panels.Items[2].Text := MSG_CAPTION_ESTADO_TURNO_CERRADO;
    end;
end;

procedure TMainForm.mnu_pedir_reposicionClick(Sender: TObject);
var
	f: TFormGenerarOrdenPedidoTAGs;
begin
	if FindFormOrCreate(TFormGenerarOrdenPedidoTAGs, f) then
		f.Show
	else begin
		if not (f.Inicializar(mnu_pedir_reposicion.Caption, true, PuntoEntrega)) then f.Release;
	end;
end;

procedure TMainForm.mnu_ModificarConvenioClick(Sender: TObject);
var
	f : TFormListadoConveniosOperador;
    FuenteSolicitud: integer;
begin
    //{ INICIO : 20160315 MGO
   	FuenteSolicitud := ApplicationIni.ReadInteger('General', 'FuenteSolicitud', FUENTE_CALLCENTER);
    {
    FuenteSolicitud := InstallIni.ReadInteger('General', 'FuenteSolicitud', FUENTE_CALLCENTER);
	}
    // FIN : 20160315 MGO
	if FindFormOrCreate(TFormListadoConveniosOperador, f) then
		f.Show
	else begin
		if not f.Inicializar(LimpiarCaracteresLabel(mnu_ModificarConvenio.Caption), True, False, PuntoEntrega, PuntoVenta, FuenteSolicitud) then
            f.Release;
        f.OnClickRegSeleccionadoModif := OnClickModif;
	end;

end;

procedure TMainForm.mnu_ModificarConvenio_AdminClick(Sender: TObject);
(*
var
	f : TFormListadoConveniosAdministrador;
    FuenteSolicitud: integer;
*)
begin
(*
  	FuenteSolicitud := applicationini.ReadInteger('General', 'FuenteSolicitud', FUENTE_CALLCENTER);
	if FindFormOrCreate(TFormListadoConveniosAdministrador, f) then
		f.Show
	else begin
		if not f.Inicializar(LimpiarCaracteresLabel(mnu_ModificarConvenio_Admin.Caption), True, False, PuntoEntrega, PuntoVenta, FuenteSolicitud) then f.Release;
*)
end;


{******************************** Function Header ******************************
Function Name: TMainForm.CheckFonts
Author       : gcasais
Date Created : 20-Ago-2004
Description  :
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TMainForm.CheckFonts: Boolean;
    procedure ExtractRes(ResType, ResName, ResNewName : String);
    var
        Res : TResourceStream;
    begin
        Res:= nil;
        try
        	try
	            Res := TResourceStream.Create(Hinstance, Resname, Pchar(ResType));
    	        Res.SavetoFile(ResNewName);
            except
            	// capturo excepci�n para que entregue msg., de lo contrario no aborta aplicaci�n
            end
        finally
            Res.Free;
        end;
    end;
CONST
    CODE_BAR_FONT_NAME = 'FREE3OF9.TTF';
    SCANNER_FONT_NAME =  'WINGDING.TTF';
var
    FontsDir: AnsiString;
begin
    Result := True;
    FontsDir:= GoodDir(Windir + 'FONTS');
    // Font de c�digo de barras
    if not FileExists(FontsDir + CODE_BAR_FONT_NAME) then begin
        RemoveFontResource(PChar(FontsDir + CODE_BAR_FONT_NAME));
        ExtractRes('EXEFILE', 'CODEBAR', FontsDir + CODE_BAR_FONT_NAME);
        if AddFontResource(PChar(FontsDir + CODE_BAR_FONT_NAME)) > 0 then begin
            SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0 );
            Result := True;
        end else Result := False;
    end;
    //Font para el scanner
    if not FileExists(FontsDir + SCANNER_FONT_NAME) then begin
        RemoveFontResource(PChar(FontsDir + SCANNER_FONT_NAME));
        ExtractRes('EXEFILE', 'SCANNER', FontsDir + SCANNER_FONT_NAME);
        if AddFontResource(PChar(FontsDir + SCANNER_FONT_NAME)) > 0 then begin
            SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0 );
            Result := True;
        end else Result := False;
    end;
end;

procedure TMainForm.actAbrirTurnoExecute(Sender: TObject);
resourcestring
    CAPTION_OPEN_SHIFT = 'Abrir Turno';
var
    fTurno: TFormAperturaCierrePuntoVenta;
begin

    if ExisteTurnoAbiertoPorUsuario(DMConnections.BaseCAC, UsuarioSistema) then
    begin
        MSgBox(MSG_ERROR_OPEN_SHIFT, SELF.Caption, MB_ICONSTOP);
        Exit;
    end;

    Application.CreateForm(TFormAperturaCierrePuntoVenta, fTurno);
    try

        if (fTurno.Inicializar(CAPTION_OPEN_SHIFT, UsuarioSistema, PuntoVenta, Abrir, PuntoEntrega, GNumeroTurno)) then begin
            if ( fTurno.ShowModal = mrOK ) then begin
            	GNumeroTurno := fTurno.FNumeroTurno;
    			HabilitarItemsTurno(True);
            end;
        end;
	finally;
        fTurno.Release;
    end;
end;

procedure TMainForm.actCerrarTurnoExecute(Sender: TObject);
resourcestring
	CAPTION_CLOSE_SHIFT = 'Cerrar Turno';
var
    fCierre: TFormAperturaCierrePuntoVenta;
    bTurnoCerrado: boolean;
begin
	bTurnoCerrado := False;
	try
        // Antes de cerrar hago que ingrese las cantidades finales
        Application.CreateForm(TFormAperturaCierrePuntoVenta, fCierre);
        if (fCierre.Inicializar(CAPTION_CLOSE_SHIFT, UsuarioSistema, PuntoVenta, Cerrar, PuntoEntrega, GNumeroTurno)) then
            bTurnoCerrado := (fCierre.ShowModal = mrOk);
        fCierre.Release;
    finally
    	fCierre.Release;
    end;

    if bTurnoCerrado then begin
    	StatusBar.Panels.Items[2].Text := MSG_CAPTION_ESTADO_TURNO_CERRADO;
        HabilitarItemsTurno(False);
        GNumeroTurno := -1;
    end;
end;

procedure TMainForm.actAdicionarTagsTurnoExecute(Sender: TObject);
resourcestring
    CAPTION_ADICIONAR_TELEVIAS = 'Adicionar Telev�as al turno';
var
    fTurno: TFormAperturaCierrePuntoVenta;
begin
    try
    	Application.CreateForm(TFormAperturaCierrePuntoVenta, fTurno);
        if (fTurno.Inicializar(CAPTION_ADICIONAR_TELEVIAS, UsuarioSistema, PuntoVenta, AdicionarTelevia, PuntoEntrega, GNumeroTurno)) then begin
            fTurno.ShowModal;
        end;
	finally;
        fTurno.Release;
    end;
end;

procedure TMainForm.OnClickConsulta(NumeroConvenio: String; CodigoConvenio: Integer);
var
    f: TConsultaConvenio;
begin
	try
    Application.CreateForm(TConsultaConvenio,f);
     if not f.Inicializa(True,Caption,NumeroConvenio, PuntoVenta, PuntoEntrega)  then begin
        MSgBox(MSG_ERROR_CARGA_SOLICITUD, SELF.Caption, MB_ICONSTOP);
        end
        else f.ShowModal;
    finally
    	f.Free;
     end;

end;

procedure TMainForm.OnClickModif(NumeroConvenio: String; CodigoConvenio: Integer; Fuente: Integer; Administrador: Boolean);
var
	f : TFormModificacionConvenio;
begin
    try
        Application.CreateForm(TFormModificacionConvenio, f);
        if f.Inicializa(False,MSG_CAPTION_MODIFICAR_CONVENIO,NumeroConvenio, PuntoVenta, PuntoEntrega, scModi,Fuente, Administrador)  then
        f.ShowModal;
    finally
        f.free;
    end;

end;

procedure TMainForm.DeshabilitarTodosItemsTurnos;
begin
    actAbrirTurno.Enabled := False;
    actCerrarTurno.Enabled := False;

    mnu_adicionar_tag_turno.Enabled := false;

    mnu_recibir_tags.Enabled := False;
    mnu_pedir_reposicion.Enabled := False;

    mnuAjustedeCaja.Enabled := False;       //TASK_031_GLE_20170111

    StatusBar.Panels[2].Text := '';
    GNumeroTurno := -1;
end;


procedure TMainForm.SetPuntoEntrega(const Value: Integer);
begin
  FPuntoEntrega := Value;
  if Assigned(GNavigator) then GNavigator.PuntoEntrega := Value;
end;

procedure TMainForm.SetPuntoVenta(const Value: Integer);
begin
  FPuntoVenta := Value;
  if Assigned(GNavigator) then GNavigator.PuntoVenta := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_PaseDiarioCambioFechaClick
  Author:    Flamas
  Date Created: 17/02/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_PaseDiarioConsultaPorPatenteClick(Sender: TObject);
var
	f: TPaseDiarioConsultaEdicionForm;
begin
	if FindFormOrCreate(TPaseDiarioConsultaEdicionForm, f) then begin
		f.Show;
	end else begin
		if not f.Inicializar(True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: DevolucindeTelevas1Click
  Author:    lgisuk
  Date Created: 17/05/2005
  Description: Devoluci�n de televia
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.DevolucindeTelevas1Click(Sender: TObject);
var
	f: TFDevolverTelevia;
begin
	if FindFormOrCreate(TFDevolverTelevia, f) then begin
        f.Show;
	end else begin
        if not f.Inicializar(PuntoEntrega) then f.Release;
    end;
end;


procedure TMainForm.VerReclamos1Click(Sender: TObject);
var
	f: TFReclamos;
begin
	if FindFormOrCreate(TFReclamos, f) then
		f.Show
	else begin
		if not f.Inicializar then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: IniciarConsultaConvenio1Click
  Author: gcasais
  Date Created: 21/04/2005
  Description: Cartola
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.IniciarConsultaConvenio1Click(Sender: TObject);
var
	f: TFormInicioConsultaConvenio;
begin
	if FindFormOrCreate(TFormInicioConsultaConvenio, f) then
		f.Show
	else begin
		if f.Inicializar(0, PuntoVenta, PuntoEntrega) then begin
            f.Show;
        end
        else f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_EstadisticaReclamosClick
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: Reporte de Estadistica de Reclamos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.Mnu_EstadisticaReclamosClick(Sender: TObject);
var
    f:TFRptEstadisticaReclamos;
begin
    //muestro el reporte
    Application.createForm(TFRptEstadisticaReclamos, f);
    if not f.Inicializar('Estad�stica de Reclamos') then f.Release;
end;

{-----------------------------------------------------------------------------
  Function Name: Mnu_OrdenesServicioBloquadasClick
  Author:    lgisuk
  Date Created: 06/04/2005
  Description: Ordenes de Servicio Bloqueadas
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.Mnu_areasBloqueadasClick(Sender: TObject);
Var
	f:  TFOrdenesServicioBloquedas;
begin
	if FindFormOrCreate( TFOrdenesServicioBloquedas, f) then
		f.Show
	else begin
		if not f.Inicializar then f.Release;
	end;
end;

//Inicio Bloque SS_974_SS_ALA_20110805
procedure TMainForm.mnuTicketVentaManualClick(Sender: TObject);
var
    f : TRevalidarTicketVentaManualForm;
begin
    if ExisteAcceso('mnuTicketVentaManual') then begin
        if FindFormOrCreate( TRevalidarTicketVentaManualForm, f) then
            f.Show
        else begin
            if not f.Inicializar then f.Release;
        end;
    end;
end;
//Termino Bloque SS_974_SS_ALA_20110805

procedure TMainForm.mnuABMClientesaContactarClick(Sender: TObject);             //SS_1408_MCA_20151027
resourcestring                                                                  //SS_1408_MCA_20151027
    MSG_TITULO = 'Clientes a Contactar';                                        //SS_1408_MCA_20151027
var                                                                             //SS_1408_MCA_20151027
	f : TFrmClientesAContactar;                                                 //SS_1408_MCA_20151027
begin                                                                           //SS_1408_MCA_20151027
    if FindFormOrCreate(TFrmClientesAContactar, f) then f.Show                  //SS_1408_MCA_20151027
    else begin                                                                  //SS_1408_MCA_20151027
    	if not f.Inicializar(MSG_TITULO) then f.Release                         //SS_1408_MCA_20151027
        else f.Show;                                                            //SS_1408_MCA_20151027
    end;                                                                        //SS_1408_MCA_20151027
end;                                                                            //SS_1408_MCA_20151027

//INICIA: TASK_067_GLE_20170410
{
procedure TMainForm.mnuAnulacionMovimientosnoFacturadosClick(Sender: TObject);
resourcestring
    MSG_TITULO = 'Movimientos No Facturados';

var
	f : TMovimientosNoFacturadosForm;
begin
    if FindFormOrCreate(TMovimientosNoFacturadosForm, f) then f.Show
    else begin
    	if not f.Inicializar(MSG_TITULO) then f.Release
        else f.Show;
    end;

end;
}
//TERMINA: TASK_067_GLE_20170410

//INICIO	: CFU 20170215 TASK_107_CFU_20170215-CRM_Crear_Campa�a
procedure TMainForm.mnuCrearCampannaClick(Sender: TObject);
var
	f: TFormCrearCampannas;
begin
	if FindFormOrCreate(TFormCrearCampannas, f) then begin
		f.Show;
	end else begin
		if not f.Inicializar(True) then 
        	f.Release;
	end;
end;
//TERMINO : CFU 20170215 TASK_107_CFU_20170215-CRM_Crear_Campa�a

{-----------------------------------------------------------------------------
  Function Name: mnuNoClientesClick
  Author:    rcastro
  Date Created: 11/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnuNoClientesClick(Sender: TObject);
var
	f: TFormConsultaTransitosNoCliente;
begin
	if FindFormOrCreate(TFormConsultaTransitosNoCliente, f) then begin
		f.Show;
	end else begin
		//if not f.Inicializar(mnuNoClientes.Caption, True) then f.Release;		//SS_1175_MCA_20140325
		if not f.Inicializar(f.Caption, True) then f.Release;					//SS_1175_MCA_20140325
	end;
end;

{------------------------------------------------------------------------
                	mnuNotaCreditoInfractorClick

Author: mbecerra
Date: 31-Julio-2009
Description:		(Ref SS 784)
        	Invoca el men� de emisi�n de Notas de Cr�dito para Infracciones.
--------------------------------------------------------------------------}
procedure TMainForm.mnuNotaCreditoInfractorClick(Sender: TObject);
var
	f : TNotaCreditoInfractorForm;
begin
    if FindFormOrCreate(TNotaCreditoInfractorForm, f) then f.Show
    else if not f.Inicializar() then f.Release;
end;

procedure TMainForm.mnuRevalidarTransitosClick(Sender: TObject);
var
	f: TConsultaTransitosARevalidarForm;
begin
	if FindFormOrCreate(TConsultaTransitosARevalidarForm, f) then begin
		f.Show;
	end else begin
		if not f.Inicializar(mnuRevalidarTransitos.Caption, True) then f.Release;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: mnu_sistemaClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 29/01/2010
    Author: mpiazza
    Description:  Ref SS-510 se le agrega una validacion por estacion de trabajo

-----------------------------------------------------------------------------}
procedure TMainForm.mnu_sistemaClick(Sender: TObject);
var
    DatosTurno: TDatosTurno;
begin
    if VerTurnoAbierto(DMConnections.BaseCAC, PuntoEntrega, PuntoVenta, UsuarioSistema, DatosTurno) then begin
        HabilitarItemsTurno(True);
        GNumeroTurno := DatosTurno.NumeroTurno;
    end else begin
        if (DatosTurno.NumeroTurno < 1) or (DatosTurno.Estado = TURNO_ESTADO_CERRADO) then GNumeroTurno := -1;

        if (DatosTurno.NumeroTurno <> -1)
                and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema))))
                and ( ResolveHostNameStr(GetMachineName) <> DatosTurno.IdEstacion )
                then begin//Si es distinto es que en el mismo POS y PV hay un turno abierto por otro usuario
            DeshabilitarTodosItemsTurnos;
        end else begin
            HabilitarItemsTurno(False);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TMainForm.InicializarIconBar
  Author:    ggomez
  Date:      21-Jun-2005
  Arguments: None
  Result:    None
  Description: Inicializa la barra de �conos.
-----------------------------------------------------------------------------}
procedure TMainForm.InicializarIconBar;
var
    Bmp: TBitmap;
    NavigatorHasPages : Boolean;
begin
    //------------------------------------------------------------------------
	// Navegador
    //------------------------------------------------------------------------
	Bmp                         := TBitmap.Create;
	GNavigator                  := TNavigator.Create(Self);
    GNavigator.ShowBrowserBar       := GetUserSettingShowBrowserBar;
    mnu_BarraDeNavegacion.Checked   := GNavigator.ShowBrowserBar;

    GNavigator.ShowHotLinkBar   := GetUserSettingShowHotLinksBar;
    mnu_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;

    GNavigator.IconBarVisible   := False;
    mnu_IconBar.Checked         := GNavigator.IconBarVisible;
    NavigatorHasPages           := False;

    if ExisteAcceso('mnu_sistema') then begin
        GNavigator.AddPage(PAGINA_COMUNICACIONES, MSG_COMUNICACIONES, HINT_COMUNICACIONES);
        NavigatorHasPages := True;
        if ExisteAcceso('mnu_IniCom') then begin
            PageImagenes.GetBitmap(0, Bmp);
            GNavigator.AddIcon(PAGINA_COMUNICACIONES,mnu_IniCom.Caption, EmptyStr, Bmp, mnu_IniCom.OnClick);
        end;
        if ExisteAcceso('mnu_FinCom') then begin
            PageImagenes.GetBitmap(1, Bmp);
            GNavigator.AddIcon(PAGINA_COMUNICACIONES, mnu_FinCom.Caption, EmptyStr, Bmp, mnu_FinCom.OnClick);
            GNavigator.SetIconEnabled(PAGINA_COMUNICACIONES, mnu_FinCom.Caption, false);
        end;
    end;

	// SOLICITUDES DE CONTACTO
    if  ExisteAcceso('mnu_solicitudcontacto') or ExisteAcceso('mnu_IngresoContrato')
            or ExisteAcceso('mnu_consultaSolicitudes') or ExisteAcceso('mnu_FAQs') then begin

        GNavigator.AddPage(PAGINA_FAVORITOS, MSG_FAVORITOS, HINT_FAVORITOS);
    	NavigatorHasPages := True;

        if ExisteAcceso('mnu_solicitudcontacto') then begin
            PageImagenes.GetBitmap(8, Bmp);
            GNavigator.AddIcon(PAGINA_FAVORITOS, mnu_solicitudContacto.Caption, EmptyStr, Bmp, mnu_solicitudContacto.OnClick);
            Bmp.Assign(nil);
        end;

        if ExisteAcceso('mnu_IngresoContrato') then begin
            PageImagenes.GetBitmap(8, Bmp);
            GNavigator.AddIcon(PAGINA_FAVORITOS, mnu_IngresoContrato.Caption, EmptyStr, Bmp, mnu_IngresoContrato.OnClick);
            Bmp.Assign(nil);
        end;

        if ExisteAcceso('mnu_FAQs') then begin
            PageImagenes.GetBitmap(9, Bmp);
            GNavigator.AddIcon(PAGINA_FAVORITOS, mnu_FAQs.Caption, EmptyStr, Bmp, mnu_FAQs.OnClick);
            Bmp.Assign(nil);
        end;

        if ExisteAcceso('mnuNoClientes') then begin                                                               // SS_1138_CQU_20140129  // SS_1138_CQU_20131127
            PageImagenes.GetBitmap(10, Bmp);                                                                      // SS_1138_CQU_20140129  // SS_1138_CQU_20131127
            GNavigator.AddIcon(PAGINA_FAVORITOS, mnuNoClientes.Caption, EmptyStr, Bmp, mnuNoClientes.OnClick);    // SS_1138_CQU_20140129  // SS_1138_CQU_20131127
            Bmp.Assign(nil);                                                                                      // SS_1138_CQU_20140129  // SS_1138_CQU_20131127
        end;                                                                                                      // SS_1138_CQU_20140129  // SS_1138_CQU_20131127

        if ExisteAcceso('mnu_MostrarScriptSolicitudContacto') then begin
            PageImagenes.GetBitmap(2, Bmp);
            GNavigator.AddIcon(PAGINA_FAVORITOS, mnu_MostrarScriptSolicitudContacto.Caption, EmptyStr, Bmp, mnu_MostrarScriptSolicitudContacto.OnClick);
            Bmp.Assign(nil);
        end;
	end;

	//MANTENIMIENTO
    if ExisteAcceso('mnu_CambiodeContrasena') then begin
    	GNavigator.AddPage(PAGINA_MANTENIMIENTO, MSG_MANTENIMIENTO, HINT_MANTENIMIENTO);
    	NavigatorHasPages := True;

        PageImagenes.GetBitmap(5, Bmp);
	    GNavigator.AddIcon(PAGINA_MANTENIMIENTO, mnu_CambiodeContrasena.Caption, EmptyStr, Bmp, mnu_CambiodeContrasena.OnClick);
        Bmp.Assign(nil);
	end;
	if NavigatorHasPages then GNavigator.IconBarActivePage := 0;

	Bmp.Free;
	Update;

    RefrescarEstadosIconBar;

    GNavigator.IconBarVisible   := GetUserSettingShowIconBar;
    mnu_IconBar.Checked		    := GNavigator.IconBarVisible;
    GNavigator.ConnectionCOP    := nil;
    GNavigator.ConnectionCAC    := DMConnections.BaseCAC;

end;

{-----------------------------------------------------------------------------
  Function Name: RefrescarEstadosIconBar
  Author:    ggomez
  Date Created: 21/06/2005
  Description: Refresca los Estados de la IconBar
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.RefrescarEstadosIconBar;
begin
    if (GNavigator = Nil) then Exit;

    GNavigator.SetIconEnabled(PAGINA_FAVORITOS, mnu_solicitudContacto.Caption, mnu_solicitudContacto.Enabled);
    GNavigator.SetIconEnabled(PAGINA_FAVORITOS, mnu_IngresoContrato.Caption, mnu_IngresoContrato.Enabled);
    GNavigator.SetIconEnabled(PAGINA_FAVORITOS, mnu_FAQs.Caption, mnu_FAQs.Enabled);
    GNavigator.SetIconEnabled(PAGINA_FAVORITOS, mnuNoClientes.Caption, mnuNoClientes.Enabled);
    GNavigator.SetIconEnabled(PAGINA_FAVORITOS, mnu_MostrarScriptSolicitudContacto.Caption, mnu_MostrarScriptSolicitudContacto.Enabled);
    GNavigator.SetIconEnabled(PAGINA_MANTENIMIENTO, MSG_MANTENIMIENTO, mnu_CambiodeContrasena.Enabled);
end;

//procedure TMainForm.rnsitos1Click(Sender: TObject);							//SS_1175_MCA_20140325
//var																			//SS_1175_MCA_20140325
//  f: TFormConsultaTransitos;													//SS_1175_MCA_20140325
//begin																			//SS_1175_MCA_20140325
    //if FindFormOrCreate(TFormConsultaTransitos, f) then begin					//SS_1175_MCA_20140325
      //f.Show;																	//SS_1175_MCA_20140325
    //end else begin															//SS_1175_MCA_20140325
      //if not f.Inicializar(f.Caption, True) then	 f.Release;					//SS_1175_MCA_20140325
    //end;																		//SS_1175_MCA_20140325
//end;																			//SS_1175_MCA_20140325

procedure TMainForm.mnu_BarraDeNavegacionClick(Sender: TObject);
begin
	GNavigator.ShowBrowserBar       := not GNavigator.ShowBrowserBar;
    mnu_BarraDeNavegacion.Checked   := GNavigator.ShowBrowserBar;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
resourcestring                                                                                    //SS_1436_NDR_20151230
	MSG_REGISTRO_CAPTION	= 'Error al Grabar Registro de Operaciones';                          //SS_1436_NDR_20151230
var                                                                                               //SS_1436_NDR_20151230
  DescriError: array[0..255] of char;                                                             //SS_1436_NDR_20151230
begin
    // Desactivar la Impresora Fiscal
    if ImpresoraFiscal.Active then ImpresoraFiscal.Active := False;

    (* Guardar las opciones de configuraci�n del usuario. *)
    GuardarUserSettings;

    if CrearRegistroOperaciones(                                                                  //SS_1436_NDR_20151230
            DMCOnnections.BaseCAC,                                                                //SS_1436_NDR_20151230
            SYS_CAC,                                                                              //SS_1436_NDR_20151230
            RO_MOD_LOGOUT,                                                                        //SS_1436_NDR_20151230
            RO_AC_LOGOUT,                                                                         //SS_1436_NDR_20151230
            0,                                                                                    //SS_1436_NDR_20151230
            GetMachineName,                                                                       //SS_1436_NDR_20151230
            UsuarioSistema,                                                                       //SS_1436_NDR_20151230
            'LOGOUT DEL SISTEMA',                                                                 //SS_1436_NDR_20151230
            0,0,0,0,                                                                              //SS_1436_NDR_20151230
            DescriError) = -1 then                                                              //SS_1436_NDR_20151230
    begin                                                                                         //SS_1436_NDR_20151230
      MsgBoxErr(MSG_ERROR_ACCESO, StrPas(DescriError), MSG_REGISTRO_CAPTION, MB_ICONSTOP);        //SS_1436_NDR_20151230
    end;                                                                                          //SS_1436_NDR_20151230

end;

procedure TMainForm.mnu_BarraDeHotLinksClick(Sender: TObject);
begin
	GNavigator.ShowHotLinkBar   := not GNavigator.ShowHotLinkBar;
    mnu_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;
end;

{-----------------------------------------------------------------------------
  Procedure: TMainForm.GuardarUserSettings
  Author:    ggomez
  Date:      30-Jun-2005
  Arguments: None
  Result:    None
  Description: Guarda las opciones de configuraci�n para el usuario logueado
    en el Sistema Operativo.
-----------------------------------------------------------------------------}
procedure TMainForm.GuardarUserSettings;
begin
    SetUserSettingShowIconBar(mnu_IconBar.Checked);
    SetUserSettingShowBrowserBar(mnu_BarraDeNavegacion.Checked);
    SetUserSettingShowHotLinksBar(mnu_BarraDeHotLinks.Checked);
end;

procedure TMainForm.mnu_IngresoContratoClick(Sender: TObject);
var
    f: TFormSolicitudContacto;
    FuenteSolicitud: Integer;
    DatosTurno: TDatosTurno;
begin
    if not VerTurnoAbierto(DMConnections.BaseCAC, PuntoEntrega, PuntoVenta, UsuarioSistema, DatosTurno) then begin
        if (DatosTurno.NumeroTurno < 1) or (DatosTurno.Estado = TURNO_ESTADO_CERRADO) then GNumeroTurno := -1;
        if (DatosTurno.NumeroTurno <> -1)
                and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then //Si es distinto es que en el mismo POS y PV hay un turno abierto por otro usuario
            MsgBox(Format(MSG_CAPTION_TURNO_ABIERTO_OTRO_USUARIO,[DatosTurno.CodigoUsuario]), self.Caption, MB_ICONWARNING)
        else
            MsgBox(MSG_ERROR_TURNO_ABRIR, CAPTION_ALTA_CONTRATO, MB_ICONSTOP);
        Exit;
    end;

	if FindFormOrCreate(TFormSolicitudContacto, f) then
		f.Show
	else begin
        //{ INICIO : 20160315 MGO
        FuenteSolicitud := applicationini.ReadInteger('General', 'FuenteSolicitud', FUENTE_CALLCENTER);
		{
        FuenteSolicitud := InstallIni.ReadInteger('General', 'FuenteSolicitud', FUENTE_CALLCENTER);
        }// FIN : 20160315 MGO
		if not f.Inicializa(True, CAPTION_ALTA_CONTRATO, PuntoEntrega, PuntoVenta, -1, scAlta, -1, INTERMEDIO, FuenteSolicitud) then f.Release;
	end;

end;

procedure TMainForm.mnu_IniComClick(Sender: TObject);
resourcestring
    MSG_COMM_ERROR = 'No Se Ha Podido Iniciar Una Comunicaci�n';
    MSG_TITLE = 'Error en la Comunicaci�n';
var
    TextoError: String;
begin
    if IniciarComunicacion('0', GCodigoComunicacion, TextoError) then begin
        ComenzarNavegacion;
    end else begin
        MsgBoxErr(MSG_COMM_ERROR, TextoError, MSG_TITLE, MB_ICONERROR);
    end;
    CheckMenuComunicaciones(true);
end;

procedure TMainForm.mnu_FinComClick(Sender: TObject);
resourcestring
    MSG_COMM_ERROR = 'No Se Ha Podido Terminar Una Comunicaci�n';
    MSG_TITLE = 'Error en la Comunicaci�n';
var
    TextoError: String;
begin
    CheckMenuComunicaciones(false);
    if not TerminarComunicacion(GCodigoComunicacion, TextoError) then begin
        MsgBoxErr(MSG_COMM_ERROR, TextoError, MSG_TITLE, MB_ICONERROR);
    end;
end;


{-----------------------------------------------------------------------------
  Procedure: TMainForm.CheckMenuComunicaciones
  Author:    ddiaz
  Date:      20-Sep-2005
  Arguments: Iniciada (indica si la comunicaci�n esta iniciada o se finaliza
  Result:    None
  Description:  Chequea y setea enabled y desabled para menu Sistema e iconos en GNavigator de de comunicaciones.
                Pasa el parametro true para la comunicaci�n iniciada y false para la cerrada
-----------------------------------------------------------------------------}
procedure TMainForm.CheckMenuComunicaciones(Iniciada: boolean);
var strIni, strFin: string; // variables necesarias para utilizar la funci�n Delete
begin
    // Quita el & del caption, poniendo el valor del mismo en strIni/strFin. La funci�n SetIconEnabled no funciona
    // correctamente si el segundo parametro tiene el caracter &...
    strIni:=mnu_IniCom.Caption;
    Delete(strIni, pos('&',mnu_IniCom.Caption), 1);
    strFin:=mnu_FinCom.Caption;
    Delete(strFin,pos('&',mnu_FinCom.Caption),1);
    if Iniciada then begin
        GNavigator.SetIconEnabled(PAGINA_COMUNICACIONES, strIni,false);
        GNavigator.SetIconEnabled(PAGINA_COMUNICACIONES, strFin,true);
        mnu_IniCom.Enabled:=false;
        mnu_FinCom.Enabled:=true; end
    else begin
        GNavigator.SetIconEnabled(PAGINA_COMUNICACIONES, strIni,true);
        GNavigator.SetIconEnabled(PAGINA_COMUNICACIONES, strFin,false);
        mnu_IniCom.Enabled:=true;
        mnu_FinCom.Enabled:=false;
    end;
end;



procedure TMainForm.mnu_ABMClientesClick(Sender: TObject);
var
	f: TfrmABMPersonas;
begin
	if FindFormOrCreate(TfrmABMPersonas, f) then
		f.Show
	else begin
		if not f.Inicializar(True) then f.Release;
	end;
end;


procedure TMainForm.actIngresoComprobantesExecute(Sender: TObject);
var
	f: TfrmIngresarComprobantes;
begin
	if FindFormOrCreate(TfrmIngresarComprobantes, f) then
		f.Show
	else begin
		if not f.Inicializar(True) then f.Release;
	end;
end;

procedure TMainForm.actReIngresoComprobantesExecute(Sender: TObject);
var
	f: TfrmReIngresarComprobantes;
begin
	if FindFormOrCreate(TfrmReIngresarComprobantes, f) then
		f.Show
	else begin
		if not f.Inicializar(True) then f.Release;
	end;
end;

procedure TMainForm.act_InformeCierreDeCajeroExecute(Sender: TObject);
resourcestring
    MSG_DESEA_IMPRIMIR  = '�Desea imprimir el Informe de Cierre de Cajero?';
    MSG_ERROR_IMPRIMIR  = 'Ha ocurrido un error al imprimir el Informe de Cierre de Cajero.';
begin
    if MsgBox(MSG_DESEA_IMPRIMIR, Caption, MB_YESNO + MB_ICONQUESTION) = mrYes then begin

//        if ImpresoraFiscalLista then begin
            if not ImpresoraFiscal.Active then ImpresoraFiscal.Active := True;
            if not ImpresoraFiscal.CerrarTurnoParcial then begin
                MsgBox(MSG_ERROR_IMPRIMIR + CRLF +
                    ImpresoraFiscal.GetLastError, Caption, MB_ICONSTOP);
            end;
  //      end;

    end;
end;

procedure TMainForm.act_InformeZExecute(Sender: TObject);
resourcestring
    MSG_DESEA_IMPRIMIR  = '�Desea imprimir el Informe Z?';
    MSG_ERROR_IMPRIMIR  = 'Ha ocurrido un error al imprimir el Informe Z.';
begin
    if MsgBox(MSG_DESEA_IMPRIMIR, Caption, MB_YESNO + MB_ICONQUESTION) = mrYes then begin

//        if ImpresoraFiscalLista then begin

            if not ImpresoraFiscal.Active then ImpresoraFiscal.Active := True;
            if not ImpresoraFiscal.CerrarTurno then begin
                MsgBox(MSG_ERROR_IMPRIMIR + CRLF + ImpresoraFiscal.GetLastError,
                Caption, MB_ICONSTOP);
            end;
//        end;
    end;
end;

procedure TMainForm.act_ConfiguracionImpresoraFiscalExecute(
  Sender: TObject);
resourcestring
    MSG_CONFIGURACION_NO_POSIBLE = 'No se puede realizar la configuraci�n de la impresora.'
                                    + CRLF + 'Contacte al administrador del sistema.';
    MSG_CONFIGURACION_EXITOSA = 'La impresora se ha configurado con exito.';
begin
    if not ImpresoraFiscal.CanSetup then begin
        MsgBox(MSG_CONFIGURACION_NO_POSIBLE, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if ImpresoraFiscal.Setup then begin
        MsgBox(MSG_CONFIGURACION_EXITOSA, Caption, MB_ICONINFORMATION);
    end;
end;


function TMainForm.ValidarRUTCTI(CTIRUT: String): String;
var
    KRUT: String;
begin
    if CTIRUT[Length(CTIRUT)] = '1' then begin
        KRUT := Copy(CTIRUT, 1, Length(CTIRUT) - 1) + 'K';
    end;
    //Verificamos si el ultimo d�gito es un 1 � una K
    if ValidarRut(DMConnections.BaseCAC, CTIRUT) then begin
        Result := CTIRUT;
    end else if ValidarRUT(DMConnections.BaseCAC, KRUT) then begin
        Result := KRUT;
    end else Result := CTIRUT;
end;

procedure TMainForm.mni_PreInscripcionPAKNOClick(Sender: TObject);
begin
//INICIO: TASK_009_ECA_20160506
//    try
//	    CambiarEstadoCursor(CURSOR_RELOJ);
//
//        if Assigned(AdhesionPreInscripcionPAKForm) then begin
//            if AdhesionPreInscripcionPAKForm.WindowState = wsMinimized then AdhesionPreInscripcionPAKForm.WindowState := wsNormal;
//            AdhesionPreInscripcionPAKForm.BringToFront;
//        end
//        else begin
//            Application.CreateForm(TAdhesionPreInscripcionPAKForm, AdhesionPreInscripcionPAKForm);
//            if AdhesionPreInscripcionPAKForm.Inicializar then
//                AdhesionPreInscripcionPAKForm.Show
//            else AdhesionPreInscripcionPAKForm.Free;
//        end;
//    finally
//	    CambiarEstadoCursor(CURSOR_DEFECTO);
//    end;
//FIN: TASK_009_ECA_20160506
end;

procedure TMainForm.mnu_ProcesoFacturacionClick(Sender: TObject);
var
    f: TFormFacturacionInfracciones;
begin
    //if FindFormOrCreate(TFormFacturacionInfracciones, f) then f.Show  // SS_660E_CQU_20140806
    //else if not f.Inicializar then f.Release;                         // SS_660E_CQU_20140806
    Application.CreateForm(TFormFacturacionInfracciones, f);            // SS_660E_CQU_20140806
    if f.Inicializar(False) then f.ShowModal;                           // SS_660E_CQU_20140806
    f.Release;                                                          // SS_660E_CQU_20140806
end;

procedure TMainForm.mnuEmitirFacturaInfraccionesClick(Sender: TObject);
var
    f: TFacturacionManualForm;
begin
	if ExisteAcceso('mnuFacturacionManual') then begin
    	if FindFormOrCreate(TFacturacionManualForm, f) then f.Show
    	else if not f.Inicializar(TC_NOTA_COBRO_INFRACCIONES) then f.Release;
    end;
end;

procedure TMainForm.mnuExplorarInfraccionesClick(Sender: TObject);
var
    f: TfrmExplorarInfraciones;
begin
    if FindFormOrCreate(TfrmExplorarInfraciones, f) then begin
        f.Show;
    end else begin
        if not f.Inicializar(SYS_CAC) then f.Release;
    end;
end;

procedure TMainForm.mnuFacturacionManualClick(Sender: TObject);
var
    f: TFacturacionManualForm;
begin
	if ExisteAcceso('mnuFacturacionManual') then begin
    	if FindFormOrCreate(TFacturacionManualForm, f) then f.Show
    	else if not f.Inicializar(TC_NOTA_COBRO) then f.Release;
    end;

end;

procedure TMainForm.EmitirCertificadodeInfracciones1Click(Sender: TObject);
var
    f: TEmitirCertificadoInfraccionesForm;
begin
    if FindFormOrCreate(TEmitirCertificadoInfraccionesForm, f) then f.Show
    else if not f.Inicializar then f.Release;
end;

{******************************* Procedure Header ******************************
Procedure Name: actConfiguracionPuestoExecute
Author : pdominguez
Date Created : 03/02/2010
Parameters : Sender: TObject
Description : SS 510
        - Se mueve el evento OnClick del item "Configuraci�n del Puesto" a una
        acci�n dentro del ActionManager1.
*******************************************************************************}
procedure TMainForm.actConfiguracionPuestoExecute(Sender: TObject);
    var
	    f : TFormGeneradorIni;
        DatosTurno: TDatosTurno;
begin
	Application.CreateForm(TFormGeneradorIni, f);
	if f.Inicializa then begin
        if f.ShowModal = mrOK then begin
            //{ INICIO : 20160315 MGO
            PuntoEntrega:= ApplicationIni.ReadInteger('General', 'PuntoEntrega', -1);
            PuntoVenta:= ApplicationIni.ReadInteger('General', 'PuntoVenta', -1);
            {
            PuntoEntrega:= InstallIni.ReadInteger('General', 'PuntoEntrega', -1);
            PuntoVenta:= InstallIni.ReadInteger('General', 'PuntoVenta', -1);
            }// FIN : 20160315 MGO

            RefrescarBarraDeEstado;

            if VerTurnoAbierto(DMConnections.BaseCAC, PuntoEntrega, PuntoVenta, UsuarioSistema, DatosTurno) then begin
                HabilitarItemsTurno(True);
                GNumeroTurno := DatosTurno.NumeroTurno;
            end else begin
                if (DatosTurno.NumeroTurno < 1) or (DatosTurno.Estado = TURNO_ESTADO_CERRADO) then GNumeroTurno := -1;

                if (DatosTurno.NumeroTurno <> -1)
                        and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                        and (Trim(DatosTurno.CodigoUsuario) <> (uppercase(UsuarioSistema)))) then begin//Si es distinto es que en el mismo POS y PV hay un turno abierto por otro usuario
                    DeshabilitarTodosItemsTurnos;
                    MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO,[DatosTurno.CodigoUsuario]), self.Caption, MB_ICONWARNING);
                end else HabilitarItemsTurno(False);
            end;

        end;
    end;
    f.Release;
end;

{-----------------------------------------------------------------------------
  Procedure     : TMainForm.mnuEstacionamientosClick
  Author        : CQuezadaI
  Date          : 07/10/2012
  Arguments     : Sender: TObject
  Result        : None
  Description   : Carga el formulario para consultar Estacionamientos
  Firma         : SS_1006_CQU_20121007
-----------------------------------------------------------------------------}
procedure TMainForm.mnuEstacionamientosClick(Sender: TObject);
var
    f: TFormConsultaEstacionamientos;
begin


    if ObtenerCodigoConcesionariaNativa = CODIGO_CN then                        //SS_1147ZZZ_NDR_20150413
    begin                                                                       //SS_1147ZZZ_NDR_20150413
      if FindFormOrCreate(TFormConsultaEstacionamientos, f) then begin
        f.Show;
      end else begin
        if not f.Inicializar(f.Caption, True) then f.Release;
      end;
    end                                                                         //SS_1147ZZZ_NDR_20150413
    else                                                                        //SS_1147ZZZ_NDR_20150413
    begin                                                                       //SS_1147ZZZ_NDR_20150413
      ShowMessage('Esta implementaci�n no tiene habilitada esta opci�n');       //SS_1147ZZZ_NDR_20150413
    end;                                                                        //SS_1147ZZZ_NDR_20150413
end;




//BEGIN : SS_1156B_NDR_20140128 ------------------------------------------------
procedure TMainForm.mnuBajaForzadaMasivaClick(Sender: TObject);
var
    frmBajaForzadaMasiva: TfrmBajaForzadaMasiva;
begin
	if FindFormOrCreate(TfrmBajaForzadaMasiva, frmBajaForzadaMasiva) then
  begin
		frmBajaForzadaMasiva.ShowModal
  end
	else
  begin
		if not frmBajaForzadaMasiva.Inicializar(mnuBajaForzadaMasiva.Caption,True) then
      frmBajaForzadaMasiva.Release
    else
      frmBajaForzadaMasiva.ShowModal ;
	end;
end;
//END : SS_1156B_NDR_20140128 ------------------------------------------------

// BEGIN: SS_660E_CQU_20140806 -------------------------------------------------
procedure TMainForm.mnuInfraMorosidadClick(Sender: TObject);
var
    f: TFormFacturacionInfracciones;
begin
    if ExisteAcceso('facturar_infractor_moroso_por_rut') then
    begin
            Application.CreateForm(TFormFacturacionInfracciones, f);
            if f.Inicializar(True) then
            begin
                f.ShowModal;
            end;
            f.Release;
    end else
        MsgBox(MSG_ERROR_SIN_PERMISO, self.Caption, MB_ICONWARNING);
end;
// END: SS_660E_CQU_20140806 ---------------------------------------------------





//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
procedure TMainForm.DrawMenuItem(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
var
  S: String;
begin
  with ACanvas do
  begin
    S := TMenuItem(Sender).Caption;
    if Selected then
    begin
        Brush.Color := FColorMenuSel;
        Font.Color := FColorFontSel;
    end
    else
    begin
        if TMenuItem(Sender).Enabled then
        begin
            Brush.Color := FColorMenu;
            Font.Color := FColorFont;
            Font.Style:=[fsBold];
        end
        else
        begin
            Brush.Color := FColorMenu;
            Font.Color := clGrayText;
            Font.Style:=[];
        end;
    end;


    if (Parent = nil) and (TMenuItem(Sender).MenuIndex = 14 ) and not Selected then
        ARect.Right := Width;

    FillRect(ARect);
    DrawText(ACanvas.Handle, PChar(S), Length(S), ARect, DT_SINGLELINE or DT_VCENTER);

    if S='-' then
    begin
      with ACanvas do
      begin
        ACanvas.MoveTo(ARect.Left,ARect.top + 4);
        ACanvas.Pen.Color := FColorFont;
        ACanvas.LineTo(ARect.Right, ARect.top + 4 );
      end;
    end;

  end;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
function TMainForm.CambiarEventoMenu(Menu: TMenu): Boolean;

	Procedure CambiarEventoItem(Nivel: Integer; Item: TmenuItem);
	Var
		i: Integer;
	begin
    if (Nivel <> 0) then
       Item.OnDrawItem := DrawMenuItem;

		for i := 0 to Item.Count - 1 do begin
			CambiarEventoItem(Nivel + 1, Item.Items[i]);
		end;
	end;

begin
	CambiarEventoItem(0, Menu.Items);
	Result := True;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------

//INICIO: TASK_013_ECA_20160520
procedure TMainForm.mnu_AdhesionTAGListaBlancaClick(Sender: TObject);
begin
  try
	    CambiarEstadoCursor(CURSOR_RELOJ);
        if Assigned(FormConsultaAltaOtraConsecionaria) then begin
            if FormConsultaAltaOtraConsecionaria.WindowState = wsMinimized then FormConsultaAltaOtraConsecionaria.WindowState := wsNormal;
            FormConsultaAltaOtraConsecionaria.BringToFront;
        end
        else begin
            Application.CreateForm(TFormConsultaAltaOtraConsecionaria, FormConsultaAltaOtraConsecionaria);
            if FormConsultaAltaOtraConsecionaria.Inicializar then
                FormConsultaAltaOtraConsecionaria.Show
            else FormConsultaAltaOtraConsecionaria.Free;
        end;
    finally
	    CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;
//FIN: TASK_013_ECA_20160520

{-----------------------------------------------------------------------------
  Function Name : TMainForm.mnuAjustedeCajaClick
  Author        : gleon
  Date Created  : 18/01/2017
  Description   : abre formulario para ajuste de caja
  Parameters    : Sender: TObject
  Return Value  : None
  firma         : TASK_031_GLE_20170111
-----------------------------------------------------------------------------}
procedure TMainForm.mnuAjustedeCajaClick(Sender: TObject);
resourcestring
    MSG_ERROR_TURNO_CERRADO     = 'Debe abrir un turno para Ajustar el Importe en Caja de un turno.';
    MSG_ERRORPERMISOS           = 'No posee permisos para registrar Ajustes en Importes Efectivos de Turnos';
    CAPTION_AJUSTE_CAJA     	= 'Ajuste de Caja - Cambio Efectivo';
var
    f: TFormAjusteCaja;
	DatosTurno: TDatosTurno;
begin
    if ExisteAcceso('mnuAjustedeCaja') then begin
        (* Verificar que el turno que se est� por utilizar est� abierto y sea del
        usuario logueado. *)
        if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
        end else begin
            if (DatosTurno.NumeroTurno <> -1)
                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                    and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
                MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                    CAPTION_AJUSTE_CAJA, MB_ICONSTOP);
            end else begin
                MsgBox(MSG_ERROR_TURNO_CERRADO, CAPTION_AJUSTE_CAJA, MB_ICONSTOP);

            end;
            Exit;
        end;
    end else begin
        MsgBox(MSG_ERRORPERMISOS, CAPTION_AJUSTE_CAJA, MB_ICONSTOP);
        Exit;
    end;

    try
        f := TFormAjusteCaja.Create(Self);
        if f.Inicializar(GNumeroTurno, UsuarioSistema, FPuntoVenta, ExisteAcceso('mnuAjustedeCaja')) then begin
            f.ShowModal;
        end;

    finally
        if Assigned(f) then FreeAndNil(f);
    end;
end;

end.
