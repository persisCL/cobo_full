object FormFacturacionInfraccionesDetalleCargo: TFormFacturacionInfraccionesDetalleCargo
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Realizar cargos al cliente para la proxima facturaci'#243'n'
  ClientHeight = 275
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object btnFacturar: TButton
    Left = 391
    Top = 241
    Width = 121
    Height = 25
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = btnFacturarClick
  end
  object BtnSalir: TButton
    Left = 519
    Top = 241
    Width = 105
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
    OnClick = BtnSalirClick
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 8
    Width = 617
    Height = 221
    Caption = ' Datos del Cargo'
    TabOrder = 0
    object lblInfracciones: TLabel
      Left = 14
      Top = 20
      Width = 376
      Height = 13
      Alignment = taRightJustify
      Caption = 
        'Se han seleccionado N Infracciones a facturar de un importe por ' +
        'infracci'#243'n de:'
    end
    object lblTotalInfracciones: TLabel
      Left = 98
      Top = 46
      Width = 292
      Height = 13
      Alignment = taRightJustify
      Caption = 'Importe total en concepto de Peaje por Boleto de Infracci'#243'n:'
    end
    object lblGastosAdm: TLabel
      Left = 65
      Top = 96
      Width = 325
      Height = 13
      Alignment = taRightJustify
      Caption = 
        'Importe total en concepto de Gastos Administrativos de Cobranzas' +
        ':'
    end
    object lblPorcentaje: TLabel
      Left = 268
      Top = 70
      Width = 122
      Height = 13
      Alignment = taRightJustify
      Caption = 'Porcentaje sobre el total:'
    end
    object lblTotalComprobante: TLabel
      Left = 185
      Top = 153
      Width = 207
      Height = 13
      Alignment = taRightJustify
      Caption = 'Importe Total del Comprobante a generar: '
    end
    object lblFechaEmision: TLabel
      Left = 223
      Top = 192
      Width = 65
      Height = 13
      Caption = 'Fecha Cargo:'
    end
    object lblIMporteTotalInfraccion: TLabel
      Left = 416
      Top = 46
      Width = 105
      Height = 13
      AutoSize = False
      Caption = '$ 2500'
    end
    object lblImporteTotal: TLabel
      Left = 416
      Top = 153
      Width = 105
      Height = 13
      AutoSize = False
      Caption = '$ 2500'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Bevel1: TBevel
      Left = 8
      Top = 144
      Width = 601
      Height = 9
      Shape = bsTopLine
    end
    object Bevel2: TBevel
      Left = 8
      Top = 175
      Width = 601
      Height = 9
      Shape = bsTopLine
    end
    object Label1: TLabel
      Left = 224
      Top = 119
      Width = 166
      Height = 13
      Alignment = taRightJustify
      Caption = 'Importe en concepto de intereses:'
    end
    object nePrecioInfraccion: TNumericEdit
      Left = 416
      Top = 16
      Width = 105
      Height = 21
      TabOrder = 0
      OnChange = nePrecioInfraccionChange
      Decimals = 0
    end
    object nePorcentaje: TNumericEdit
      Left = 416
      Top = 66
      Width = 105
      Height = 21
      TabOrder = 1
      OnChange = nePorcentajeChange
      Decimals = 2
    end
    object deFechaEmision: TDateEdit
      Left = 296
      Top = 188
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 4
      Date = -693594.000000000000000000
    end
    object neImporteIntereses: TNumericEdit
      Left = 416
      Top = 115
      Width = 105
      Height = 21
      TabOrder = 3
      OnChange = neImporteInteresesChange
      Decimals = 0
    end
    object neImporteTotalGastosAdministrativos: TNumericEdit
      Left = 416
      Top = 90
      Width = 105
      Height = 21
      TabOrder = 2
      OnChange = neImporteTotalGastosAdministrativosChange
      Decimals = 0
    end
  end
end
