unit frmSituacionTag;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DPSControls, Convenios, Peaprocs, DMConnection;

type
  TformSituacionTag = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Panel1: TPanel;
    cb_situacion: TComboBox;
    lbl_Patente: TLabel;
    lbl_tag: TLabel;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function inicializar(Caption : String; Cuenta : TCuentasConvenio):Boolean;
  end;

var
  formSituacionTag: TformSituacionTag;

implementation

{$R *.dfm}

procedure TformSituacionTag.btn_CancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TformSituacionTag.btn_AceptarClick(Sender: TObject);
begin
    ModalResult := mrOk;
end;

function TformSituacionTag.inicializar(Caption: String;
  Cuenta: TCuentasConvenio): Boolean;
begin
    CargarCombioSituacionTag(DMConnections.BaseCAC, cb_Situacion);
    Result := True;
end;

end.
