{-----------------------------------------------------------------------------
 Unit Name: frmReporteFacturacionDetallada
 Author:    ndonadio
 Purpose:   Generar el reporte de FACTURACION DETALLADA!!!
 History:

 Revision 1:
    Author: ggomez
    Date: 29/11/2005
    Description: Cambi� la forma de obtener los tr�nsitos de la facturaci�n
        detallada de cada comprobante. Ahora se hace por pasos, pues para
        comprobantes con muchos tr�nsitos daba Timeout.

 Revision 2:
    Author: ggomez
    Date: 27/04/2006
    Description: Sub� el TimeOut de los SP a 500.

 Revision 3:
    Author: nefernandez
    Date: 21/03/2007
    Description: Si la impresion ya fue cobrada (ej: llamada desde carpeta legales),
    cambi� para que se pueda imprimir el reporte con la posibilidad de evitar que se
    muestre el mensaje de impresi�n exitosa.

Revision 4:
    Author: dAllegretti
    Date: 22/05/2008
    Description: Se le agreg� el parametro del Stored spAgregarMovimientoCuenta
                    y la correspondiente asignaci�n en el llamado al stored

Revision 5:
    Author: Nelson Droguett Sierra
    Date: 28/04/2009
    Description: Se pone el nombre del reporte segun el tipo de comprobante fiscal

Firma       : SS_1006_HUR_20120213
Descripcion :  ObtenerDetalleTransitos

Firma       : SS_1045_CQU_20120604
Descripcion : Agrega la l�gica para la impresi�n autom�tica desde el m�dulo de infracciones.

Firma       : SS-1006-NDR-20120702
Description : Se agrega un reporte con 2 subreportes para mostrar fusionados los detalles de transitos
            y de estacionamientos, con un total unificado por patente. Para esto se incrust� una variable
            a la que al momento de imprimirla se le asigna la suma de los 2 subtotales de los subreportes.

Fecha       :   24/08/2015
Firma       :   SS_1246_CQU_20150824
Descripcion :   Se pide: En la ventana de comprobantes emitidos, en el CAC, Eliminar el bot�n de �Imprimir con Cobro�
                        y cambiar la etiqueta del bot�n "Imprimir gratis" por "imprimir".
                        A partir de ahora todas las impresiones son gratis.
                Se hizo: Se elimina la llamada a TfSeleccFactDetallada, ahora imprime directo, sin cobrar.

Firma       : SS_1397_MCA_20150930
Descripcion : se agrega el logo concesionaria
             se quitan las leyendas Pto. Cobro Entrada y Pto. Cobro salida.
             se eliminan completamente las columnas Lugar, Fecha y Hora bajo la leyenda �Pto. Cobro salida.
             se reemplaza la leyenda lugar por p�rtico
             se agrega pie de pagina la leyenda
                    TBPF : Tarifa base fuera de punta
                    TBP   : Tarifa base punta
                    TS     : Tarifa de saturaci�n
             se agrega margen a la impresion y exportacion del detalle del comprobante
Firma       : SS_1390_MGO_20150928
Descripcion : Se actualiza la funci�n ReporteAPDF para poder ser llamada desde la Web

Firma       : SS_1397_MGO_20151015
Descripcion : Se agrega el par�metro esWeb en el detalle de facturaci�n para usar una ruta
              de logo distinto en caso de que se llame desde la Web. Se actualiza funci�n ReporteAPDF.

Firma       : SS_1397_MGO_20151027
Descripcion : La leyenda de PAK no se muestra para VS.
              El total de concesionaria solo se muestra si hay m�s de una concesionaria.

Firma       : SS_1397_MGO_20151028
Descripcion : Se corrige margen de columnas y env�a conexi�n a la obtenci�n de c�digos de concesionaria

Firma       : SS_1332_CQU_20151229
Descripcion : Se quita el margen de columnas en el proceso ReporteAPDF ya que en la WEB solapaba las columnas.

Firma       : SS_1397_MCA_20160105
Descripcion : se cambia parametro LOGO_REPORTES_WEB por LOGO_REPORTES_WS dado que son distintos servidores y no tienen las mismas rutas
por lo que el WEB no ve la imagen del WS
-----------------------------------------------------------------------------}



unit frmReporteFacturacionDetallada;

interface

uses
 Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppBands, ppReport, ppSubRpt, ppCtrls,
  ppStrtch, ppMemo, ppParameter, ppBarCod, jpeg, ppPrnabl, ppClass, UtilRB,
  ppCache, ppComm, ppRelatv, ppProd, ppModule, DB,
  ADODB, ppDB, ppDBPipe,util,utilproc, ppVar, StdCtrls, daDataModule,
  ppRegion, ppCTMain, ppTxPipe, rbSetup, TeEngine, Series, ExtCtrls,
  TeeProcs, Chart, DbChart, TXComp, ppDevice, ConstParametrosGenerales, Printers, PeaProcs, PeaTypes, UtilDb,
  frmSeleccFactDetallada, ppPDFDevice, raCodMod;

type
  TFormReporteFacturacionDetallada = class(TForm)
    Label3: TLabel;
    dsDetalleTransitos: TDataSource;
    spDetalleTransitos: TADOStoredProc;
    rptReporteFacturacionDetallada: TppReport;
    ppDetallada: TppDBPipeline;
	rbiDetallada: TRBInterface;
    spDescontarFacturacionDetalladaGratis: TADOStoredProc;
    spAgregarMovimientoCuenta: TADOStoredProc;
    qryComprobantes: TADOQuery;
    ppdbComprobantes: TppDBPipeline;
    dsComprobantes: TDataSource;
    qryComprobantesTipoComprobante: TStringField;
    qryComprobantesNumeroComprobante: TLargeintField;
    qryComprobantesFechaVencimiento: TDateTimeField;
    qryComprobantesPeriodoInicial: TDateTimeField;
    qryComprobantesPeriodoFinal: TDateTimeField;
    qryComprobantesFechaEmision: TDateTimeField;
    qryComprobantesCodigoConvenio: TIntegerField;
    ppParameterList1: TppParameterList;
    qryComprobantesTipoComprobanteFiscal: TStringField;
    qryComprobantesNumeroComprobanteFiscal: TLargeintField;
    qryComprobantesDescripcion: TStringField;
    spDetalleTransitosPatente: TStringField;
    spDetalleTransitosConcesionaria: TStringField;
    spDetalleTransitosDescriPtoCobroEntrada: TStringField;
    spDetalleTransitosFechaHoraEntrada: TDateTimeField;
    spDetalleTransitosDescriPtoCobroSalida: TStringField;
    spDetalleTransitosFechaHoraSalida: TDateTimeField;
    spDetalleTransitosDescriTipoHorario: TStringField;
    spDetalleTransitosImporte: TBCDField;
    spDetalleTransitosDescriImporte: TStringField;
    ppHeaderBand3: TppHeaderBand;
    ppShape28: TppShape;
    ppShape26: TppShape;
    ppShape27: TppShape;
    ppShape22: TppShape;
    ppShape21: TppShape;
    ppShape24: TppShape;
    ppShape23: TppShape;
    ppLabel38: TppLabel;
    ppDBText22: TppDBText;
    ppDBText20: TppDBText;
    ppLabel35: TppLabel;
    ppLine9: TppLine;
    ppLabel34: TppLabel;
    ppShape25: TppShape;
    ppDBText21: TppDBText;
    ppLabel32: TppLabel;
    ppDBText24: TppDBText;
    ppDBText23: TppDBText;
    ppDBText1: TppDBText;
    ppDetailBand6: TppDetailBand;
    ppNumeroComprobante: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppShape7: TppShape;
    ppShape8: TppShape;
    ppLabel16: TppLabel;					//SS_1397_MCA_20150930
    ppLogo: TppImage;
    Footer: TppFooterBand;
    spObtenerDetalleTransitos: TADOStoredProc;
    pfldCategoria: TppField;
    pfldFechaHora: TppField;
    pfldFecha: TppField;
    pfldHora: TppField;
    pfldVia: TppField;
    pfldPlaza: TppField;
    pfldSentido: TppField;
    pfldImporte: TppField;
    pfldImporteDesc: TppField;
    pfldTAG: TppField;
    pfldFechaEmision: TppField;
    pfldFechaVencimiento: TppField;
    pfldPeriodoInicial: TppField;
    pfldPeriodoFinal: TppField;
    pfldNombre: TppField;
    pfldRUT: TppField;
    pfldNumeroConvenio: TppField;
    pfldTipoComprobanteFiscal: TppField;
    pfldNumeroComprobanteFiscal: TppField;
    pfldDetalladappField2: TppField;
    plbl1: TppLabel;
    psystmvrbl1: TppSystemVariable;
    plbl2: TppLabel;
    psystmvrbl2: TppSystemVariable;
    ppSummaryBand1: TppSummaryBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    pln1: TppLine;
    pln2: TppLine;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    pshp1: TppShape;
    ppShape1: TppShape;
    pdbclc1: TppDBCalc;
    plbl3: TppLabel;
    plbl4: TppLabel;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    pdbclcImporte: TppDBCalc;
    ppShape2: TppShape;
    ppShape3: TppShape;
    plbl5: TppLabel;
    ppDBText12: TppDBText;
    ppShape4: TppShape;
    ppShape5: TppShape;
    plbl6: TppLabel;
    ppDBText13: TppDBText;
    ppShape6: TppShape;
    ppShape9: TppShape;
    plbl7: TppLabel;
    ppDBText14: TppDBText;
    ppTitleBand1: TppTitleBand;						  //SS_1397_MGO_20151027
    procedure rptReporteFacturacionDetalladaBeforePrint(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure rbiDetalladaExecute(Sender: TObject; var Cancelled: Boolean);
   private
    { Private declarations }
    FTipoComprobante: String;
    FNumeroComprobante: Int64;
    FCodigoConvenio: integer;
    FImpresionYaCobrada : boolean;
//    FDirLogo, FNombreImagenLogo: String;
    FConexion: TADOConnection;
    FCostoFacturacionDetallada: integer;
    FMostrarMensajeImpresionExistosa: boolean;
    FMostrarConfirmacion : Boolean; 			// SS_1045_CQU_20120604
    procedure Preparar;
    procedure CobrarFacturacionDetallada(CodigoConvenio : integer);

  public
    { Public declarations }
        property MostrarMensajeImpresionExistosa : boolean read FMostrarMensajeImpresionExistosa write FMostrarMensajeImpresionExistosa;
        function Inicializar(Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: int64; var Error: string; bImpresionYaCobrada : boolean = False; esWeb: Boolean = False): Boolean;      //SS_1397_MGO_20151015
        function Ejecutar: Boolean;
        function EjecutarPDF(var Resultado: string; var Error: String):Boolean;
    procedure ObtenerDetalleTransitos(TipoCompro: AnsiString; NumeroCompro: Int64);
    property MostrarConfirmacion : Boolean read FMostrarConfirmacion write FMostrarConfirmacion; 	// SS_1045_CQU_20120604
  	function ReporteAPDF(Reporte: TppReport; var Resultado: string; var Error: string): boolean;    //SS_1397_MGO_20151015
end;

    function ExportarReporte(Reporte: TppReport; Dispositivo: TppDevice; var Error: string): boolean;

var
  FormReporteFacturacionDetallada: TFormReporteFacturacionDetallada;

implementation

{$R *.dfm}
uses dmConnection, RStrings;

resourcestring
    ERROR_INIT                        = 'Error de Inicializaci�n';
	ERROR_PRINTING                    = 'Error de Impresi�n';
	ERROR_INIT_DESCRIPTION            = 'No Se Encuentra El Comprobante';

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    ndonadio
  Date Created: 20/12/2004
  Description: Inicializa y adquiere los datos para imprimir el comprobante
  Parameters: ninguno
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReporteFacturacionDetallada.Inicializar(Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: int64; var Error: string; bImpresionYaCobrada : boolean = False; esWeb: Boolean = False): Boolean;        //SS_1397_MGO_20151015
resourcestring
	MSG_MISSED_IMAGES_FILES = 'No existen todas las im�genes requeridas para '
		+ CRLF + 'poder imprimir una Nota de Cobro en el sistema';
	MSG_COMPROBANTE_NO_EXISTE = 'El Comprobante %s %d no existe';
    LOGO_FACTURACION = 'LOGO_FACTURACION';
var
    RutaLogo: AnsiString;                                                       //SS_1397_MCA_20150930
begin
    try
        if not Conexion.Connected then Conexion.Open;
        FConexion := Conexion;
        qryComprobantes.Connection := FConexion;
        //spDetalleTransitos.Connection := FConexion;                           // TASK_059_MGO_20160811
        spObtenerDetalleTransitos.Connection := FConexion;                      // TASK_059_MGO_20160811
        spAgregarMovimientoCuenta.Connection := FConexion;
        spDescontarFacturacionDetalladaGratis.Connection := FConexion;

        qryComprobantes.Close;
        qryComprobantes.Parameters.ParamByName('TipoComprobante').Value := tipoComprobante;
        qryComprobantes.Parameters.ParamByName('NumeroComprobante').Value := numeroComprobante;
        qryComprobantes.Open;

        if qryComprobantes.IsEmpty then begin
            Result := False;
			MsgBox(format(MSG_COMPROBANTE_NO_EXISTE,[tipoComprobante, NumeroComprobante]), STR_ERROR,MB_ICONSTOP);
            Exit;
        end;

        FTipoComprobante := TipoComprobante;
        FNumeroComprobante := NumeroComprobante;
        FCodigoConvenio := qryComprobantes.FieldByName('CodigoConvenio').AsInteger;
        FImpresionYaCobrada := bImpresionYaCobrada;
        FMostrarMensajeImpresionExistosa := True;

        FMostrarConfirmacion := True;   		// SS_1045_CQU_20120604

        ObtenerParametroGeneral(Conexion, 'PRECIO_IMPR_FACTURACION_DETALLADA', FCostoFacturacionDetallada);

        //ObtenerParametroGeneral(FConexion, iif(esWeb, LOGO_REPORTES_WS, LOGO_REPORTES), RutaLogo);       //TASK_059_MGO_20160811
        ObtenerParametroGeneral(FConexion, iif(esWeb, LOGO_REPORTES_WS, LOGO_FACTURACION), RutaLogo);      //TASK_059_MGO_20160811
        try                                                                         
            ppLogo.Picture.LoadFromFile(Trim(RutaLogo));                            
        except                                                                      
            On E: Exception do begin                                                
                Screen.Cursor := crDefault;                                         
                MsgBox(E.message, Self.Caption, MB_ICONSTOP);                       
                Screen.Cursor := crHourGlass;                                       
            end;                                                                    
        end;

        //if ObtenerCodigoConcesionariaNativa(FConexion) = ObtenerCodigoConcesionariaVS(FConexion) then     //TASK_059_MGO_20160811
        //    ppLabelPAK.Visible := False;                                                                  //TASK_059_MGO_20160811

        Result := True;
    except
        on e:Exception do begin
            result := false;
            Error := e.message;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: Ejecutar
  Author:    ndonadio
  Date Created: 28/12/2004
  Description: Abre los origenes de datos con los parametros pasados, genera la informacion a
            mostrar y muestra el reporte.
  Parameters: TipoComprobante: string; NumeroComprobante: int64; M:Integer; EPS, Servicio: string
  Return Value: Boolean
       Revisi�n 1: ddiaz  24/11/2005
                    Se modific� la metodolog�a implementada en la interface de usuario
                    concerniente a la reimpresi�n y impresi�n detallada.
                    Asignaci�n Kanav: 1505
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  15/03/07
  Solo cobro el detalle de transacciones si corresponde cobrarlo.

  Firma         : SS_1045_CQU_20120604
  Descripcion   : Se agrega el despliegue o no del Dialog de impresi�n
                  Usado principalmente en el proceso de facturaci�n de infracciones
-----------------------------------------------------------------------------}
function TFormReporteFacturacionDetallada.Ejecutar: Boolean;
resourcestring
    MSG_DETAIL_FREE_UNTIL_DATE  = 'La facturacion detallada es gratuita hasta el d�a %s';
	MSG_DETAIL_FREE_UNTIL_ZERO  = 'Le restan %d impresiones de la facturacion detallada gratuitas';
	MSG_DETAIL_CHARGED          = 'La facturaci�n detallada tiene un costo de $ %d';
	MSG_DETAIL_FREE_ALWAYS      = 'La facturacion detallada es gratuita.';
	MSG_WANT_DETAIL             = 'Desea imprimir la Facturaci�n Detallada?';
	TITLE_DETAIL                = 'Facturaci�n Detallada';
	PRINTING_COMPLETE           = 'La impresi�n ha sido satisfactoria?';
	PRINTING_TITLE              = 'Facturaci�n Detallada';
var
	DfltConfig: TRBConfig;
	PrintingOk: Boolean;
    f : TfSeleccFactDetallada;
    RutaLogo: AnsiString;                                                       //SS_1397_MCA_20150930
begin
	DfltConfig := rbidetallada.GetConfig;

    rptReporteFacturacionDetallada.ColumnPositions.Add('0,9');                  //SS_1397_MGO_20151028
    rptReporteFacturacionDetallada.ColumnPositions.Add('4,3');                  //SS_1397_MGO_20151028

    ObtenerParametroGeneral(FConexion, LOGO_REPORTES, RutaLogo);                //SS_1397_MCA_20150930
	try                                                                         //SS_1397_MCA_20150930
		ppLogo.Picture.LoadFromFile(Trim(RutaLogo));                            //SS_1397_MCA_20150930
	except                                                                      //SS_1397_MCA_20150930
		On E: Exception do begin                                                //SS_1397_MCA_20150930
			Screen.Cursor := crDefault;                                         //SS_1397_MCA_20150930
			MsgBox(E.message, Self.Caption, MB_ICONSTOP);                       //SS_1397_MCA_20150930
			Screen.Cursor := crHourGlass;                                       //SS_1397_MCA_20150930
		end;                                                                    //SS_1397_MCA_20150930
	end;                                                                        //SS_1397_MCA_20150930
	// Si la impresi�n no fue cobrada en la Facturaci�n la cobra por ac�
    {   // INICIO ELIMINACION CODIGO    // SS_1246_CQU_20150824
    if not FImpresionYaCobrada then begin
    	try
            // Verifica si tiene o quiere facturaci�n detallada
            // y si es gratuita o es paga
            Application.CreateForm( TfSeleccFactDetallada, f);
            if DfltConfig.BinName = '' then f.cbOpciones.Checked := True;
            if f.Inicializar(FTipoComprobante, FNumeroComprobante) then begin
                if (f.ShowModal = mrOK) then begin
                    PrintingOk := False;
                    while  NOT PrintingOK do begin
                        if f.Opciones then begin
                            rbiDetallada.Report.DeviceType := 'Screen';
                            DfltConfig.DeviceType := 'Screen';
                            rbiDetallada.SetConfig(DfltConfig);
                        end;
                        if rbiDetallada.Execute(f.Opciones) then begin
                            if MsgBox(PRINTING_COMPLETE,PRINTING_TITLE,MB_ICONQUESTION+MB_YESNO) = mrYes then begin
                                if  f.Cobrar then
                                    CobrarFacturacionDetallada(FCodigoConvenio)
                                else if f.DescontarGratis then begin
                                    spDescontarFacturacionDetalladaGratis.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
                                    spDescontarFacturacionDetalladaGratis.ExecProc;
                                end;
                                PrintingOk := True;
                                end;
                            end else Break;
                        end;
                    end;
              end;
         finally
            f.Release;
         end;
    end else begin
    }   // FIN ELIMINACION CODIGO       // SS_1246_CQU_20150824
        // Si la impresi�n ya fue cobrada en la Facturaci�n no la vuelve a cobrar
        PrintingOk := False;
        // Revision 3
        while not PrintingOK do begin
            //if rbiDetallada.Execute(True) then begin										// SS_1045_CQU_20120604
            if rbiDetallada.Execute(MostrarConfirmacion) then begin							// SS_1045_CQU_20120604
                if not MostrarConfirmacion then FMostrarMensajeImpresionExistosa := False;	// SS_1045_CQU_20120604
                if FMostrarMensajeImpresionExistosa then begin
                    if MsgBox(PRINTING_COMPLETE,PRINTING_TITLE,MB_ICONQUESTION+MB_YESNO) = mrYes then PrintingOk := True;
                end else PrintingOk := True;
            end else PrintingOk := True;
        end;
    //end;                              // SS_1246_CQU_20150824
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    ndonadio
  Date Created: 28/12/2004
  Description: Cierra los origenes de datos al cerrar el Form.
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReporteFacturacionDetallada.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    qryComprobantes.Close;
end;


procedure TFormReporteFacturacionDetallada.Preparar;
var
	OldDecimalSeparator : char;
    OldThousandSeparator : char;
begin

    // Guarda los separadores decimales y los cambia
    // por "," para decimales y "." para miles
    OldDecimalSeparator	:= DecimalSeparator;
    OldThousandSeparator:= ThousandSeparator;
    DecimalSeparator	:= ',';
    ThousandSeparator	:= '.';

    qryComprobantes.Close;
    qryComprobantes.Parameters.ParamByName('TipoComprobante').Value := FTipoComprobante;
    qrycomprobantes.Parameters.ParamByName('NumeroComprobante').Value := FNumeroComprobante;
    qryComprobantes.Open;

//	spDetalleTransitos.Close;
//    spDetalleTransitos.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
//    spDetalleTransitos.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
//    spDetalleTransitos.Open;
    ObtenerDetalleTransitos(FTipoComprobante, FNumeroComprobante);

    //Determinamos cuales de todas las im�genes de pago tenemos que mostrar
    //ppImgLogoFactDet.Picture.LoadFromFile(FNombreImagenLogo);

    // Vuelve a dejar los separadores decimales como estaban
    DecimalSeparator  := OldDecimalSeparator;
    ThousandSeparator := OldThousandSeparator;
end;


function TFormReporteFacturacionDetallada.EjecutarPDF(var Resultado: string; var Error: String):Boolean;
begin
    //Ejecuto el report...
    try
        Preparar;
		Result := reporteAPDF(rptReporteFacturacionDetallada, Resultado, Error);
	except
        on e:Exception do begin
            Error := e.Message;
            Result := False;
        end;
    end;
end;

function ExportarReporte(Reporte: TppReport; Dispositivo: TppDevice; var Error: string): boolean;
begin
    try
        Dispositivo.Publisher := Reporte.Publisher;
        Dispositivo.Reset;
        Reporte.PrintToDevices;
        result := true;
    except
        on e: exception do begin
            result := false;
            Error := e.message;
        end;
    end;
end;

function TFormReporteFacturacionDetallada.ReporteAPDF(Reporte: TppReport; var Resultado: string; var Error: string): boolean;             //SS_1397_MGO_20151015
var
    Exportador: TppPDFDevice;
    Salida: TStringStream;
    Salida2: TMemoryStream;                                       //SS_1397_MGO_20151015
begin
  try                                                             //SS_1397_MGO_20151015
    Salida := TStringStream.Create('');
    Salida2 := TMemoryStream.Create;                              //SS_1397_MGO_20151015
    Error := '';                                                  //SS_1397_MGO_20151015

    Exportador := TppPDFDevice.Create(nil);
//    Exportador.PrintToStream := true;
//    Exportador.ReportStream := Salida;
//    Exportador.OutputStream := Salida;                                                                                                  //SS_1397_MGO_20151015
    Exportador.PDFSettings.OptimizeImageExport := False;                                                                                  //SS_1397_MGO_20151015
    Exportador.PDFSettings.ScaleImages := False;                                                                                          //SS_1397_MGO_20151015
    Exportador.FileName := GetTempDir + FormatDateTime('yyyymmddhhnnsszzz_', Now) + IntToStr(FNumeroComprobante) + '.pdf';                //SS_1397_MGO_20151015

    //Reporte.ColumnPositions.Add('0,1');                         // SS_1332_CQU_20151229   //SS_1397_MGO_20151028
    //Reporte.ColumnPositions.Add('4,3');                         // SS_1332_CQU_20151229   //SS_1397_MGO_20151028

    Result := ExportarReporte(Reporte, Exportador, Error);

    Salida2.LoadFromFile(Exportador.FileName);                    //SS_1397_MGO_20151015
    Salida2.Position := 0;                                        //SS_1397_MGO_20151015
    Salida2.SaveToStream(Salida);                                 //SS_1397_MGO_20151015

    //Salida.Position := 0;                                       //SS_1397_MGO_20151015
    Resultado := Salida.DataString;
    //Exportador.Free;                                            //SS_1397_MGO_20151015
    //Salida.Free;                                                //SS_1397_MGO_20151015
  finally                                                         //SS_1397_MGO_20151015
    if Assigned(Exportador) then FreeAndNil(Exportador);          //SS_1397_MGO_20151015
    if Assigned(Salida) then FreeAndNil(Salida);                  //SS_1397_MGO_20151015
    if Assigned(Salida2) then FreeAndNil(Salida2);                //SS_1397_MGO_20151015
  end;                                                            //SS_1397_MGO_20151015
end;

procedure TFormReporteFacturacionDetallada.rbiDetalladaExecute(Sender: TObject;
  var Cancelled: Boolean);
begin
   try
        rbiDetallada.Report.PrinterSetup.BinName := rbiDetallada.GetConfig.BinName ;
        Preparar;
    except
        on e: Exception do begin
			MsgBoxErr(ERROR_INIT, e.Message, STR_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: CobrarFacturacionDetallada
  Author:    flamas
  Date Created: 24/02/2005
  Description: Cobra la impresi�n de la facturaci�n detallada
  Parameters: CodigoConvenio : integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReporteFacturacionDetallada.CobrarFacturacionDetallada(CodigoConvenio : integer);
resourcestring
	MSG_DETAILED = 'Solicitada por el Cliente';
begin
	// No trapea la excepci�n para que la
    // capture EscribirFacturacionDetallada
	with spAgregarMovimientoCuenta, Parameters do begin
    	ParamByName('@CodigoConvenio').Value 		:= CodigoConvenio;
        ParamByName('@IndiceVehiculo').Value 		:= NULL;
        ParamByName('@FechaHora').Value				:= NowBase(FConexion);
        ParamByName('@CodigoConcepto').Value		:= CONCEPTO_IMPR_FACT_DETALLADA;
        ParamByName('@Importe' ).Value				:= FCostoFacturacionDetallada;
        ParamByName('@LoteFacturacion' ).Value		:= NULL;
        ParamByName('@EsPago' ).Value				:= False;
        ParamByName('@Observaciones' ).Value		:= MSG_DETAILED;
        ParamByName('@NumeroPromocion' ).Value		:= NULL;
        ParamByName('@NumeroFinanciamiento' ).Value	:= NULL;
        ParamByName('@UsuarioCreacion' ).Value	    := UsuarioSistema;
        ExecProc;
    end;
end;               

{******************************** Function Header ******************************
Function Name: ObtenerDetalleTransitos
Author : ggomez
Date Created : 29/11/2005
Description : Obtiene los registros de los tr�nsitos de la Nota de Cobro.
Parameters : TipoCompro: AnsiString; NumeroCompro: Int64
Return Value : None


Revision 1:
    Author : ggomez
    Date : 28/03/2006
    Description : Agregu� el seteo de los par�metros TipoComprobante y
        NumeroComprobante en el llamado al SP ImprimirFactDetallada_CargarTransitosDeNotaCobro.

Revision 2:
    Author : ggomez
    Date : 27/04/2006
    Description : Sub� el timeout del sp a 500.

*******************************************************************************}
procedure TFormReporteFacturacionDetallada.ObtenerDetalleTransitos(
  TipoCompro: AnsiString; NumeroCompro: Int64);
resourcestring                                                                  // TASK_059_MGO_20160811
    STR_ERROR_SP = 'Hubo un error al obtener el detalle de tr�nsitos';          // TASK_059_MGO_20160811
var
    sp: TADOStoredProc;
  	NumCorrCADesde: Int64;
    NumCorrCAHasta: Int64;
    NumCorrEstacionamientoDesde: Int64;   // SS_1006_HUR_20120213
    NumCorrEstacionamientoHasta: Int64;   // SS_1006_HUR_20120213
    vConcesionarias: TStringList;         //SS_1397_MGO_20151027
begin
    { INICIO : TASK_059_MGO_20160811
    spDetalleTransitos.Close;

    sp := TADOStoredProc.Create(nil);
    try
        sp.Connection       := spDetalleTransitos.Connection;
        // Colocar 500 segundos, pues hay convenios para los que puede demorar algo m�s que 30 seg.
        sp.CommandTimeout   := 500;

        // Cargar la Tabla Temporal Global con Los NumCorrCA de los tr�nsitos de la NK.
        sp.ProcedureName := 'ImprimirFactDetallada_CargarNumCorrCADeNotaCobro';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@TipoComprobante').Value     := TipoCompro;
        sp.Parameters.ParamByName('@NumeroComprobante').Value   := NumeroCompro;
        sp.ExecProc;
        sp.Close;

        // Cargar la Tabla Temporal Global con el resto de los datos de
        // tr�nsitos de la NK.
        sp.ProcedureName := 'ImprimirFactDetallada_CargarTransitosDeNotaCobro';
        sp.Parameters.Refresh;

        // Inicializar variables
        NumCorrCADesde  := -1;
        NumCorrCAHasta  := 0;
        NumCorrEstacionamientoDesde  := -1;     // SS_1006_HUR_20120213
        NumCorrEstacionamientoHasta  := 0;      // SS_1006_HUR_20120213
        // Mientras haya tr�nsitos para cargar.
        while ((NumCorrCADesde <> NumCorrCAHasta) and (NumCorrEstacionamientoDesde <> NumCorrEstacionamientoHasta)) do begin // SS_1006_HUR_20120213
            NumCorrCADesde := NumCorrCaHasta + 1;
            NumCorrEstacionamientoDesde := NumCorrEstacionamientoHasta + 1;         // SS_1006_HUR_20120213

            sp.Parameters.ParamByName('@TipoComprobante').Value     := TipoCompro;
            sp.Parameters.ParamByName('@NumeroComprobante').Value   := NumeroCompro;
            sp.Parameters.ParamByName('@NumCorrCADesde').Value  := NumCorrCADesde;
            sp.Parameters.ParamByName('@NumCorrEstacionamientoDesde').Value  := NumCorrEstacionamientoDesde; // SS_1006_HUR_20120213
            sp.Parameters.ParamByName('@NumCorrCAHasta').Value  := 0;
            sp.Parameters.ParamByName('@NumCorrEstacionamientoHasta').Value  := 0;    // SS_1006_HUR_20120213
            sp.ExecProc;

            // Obtener el �ltimo n�mero de tr�nsito procesado
            NumCorrCAHasta := sp.Parameters.ParamByName('@NumCorrCAHasta').Value;
            NumCorrEstacionamientoHasta := sp.Parameters.ParamByName('@NumCorrEstacionamientoHasta').Value; // SS_1006_HUR_20120213

            sp.Close;
        end; // while
    finally
        sp.Close;
        sp.Free;
    end; // finally

    // Obtener los datos de los consumos.
    spDetalleTransitos.Open;

    try                                                                                                         //SS_1397_MGO_20151027
        vConcesionarias := TStringList.Create;                                                                  //SS_1397_MGO_20151027
        // Se obtienen las Concesionarias que aparecen en el detalle                                            //SS_1397_MGO_20151027
        while not spDetalleTransitos.Eof do begin                                                               //SS_1397_MGO_20151027
            if vConcesionarias.IndexOf(spDetalleTransitos.FieldByName('Concesionaria').AsString) = -1 then      //SS_1397_MGO_20151027
                vConcesionarias.Add(spDetalleTransitos.FieldByName('Concesionaria').AsString);                  //SS_1397_MGO_20151027
            spDetalleTransitos.Next;                                                                            //SS_1397_MGO_20151027
        end;                                                                                                    //SS_1397_MGO_20151027
                                                                                                                //SS_1397_MGO_20151027
        // Si solo hay una, no se muestra el total de la concesionaria                                          //SS_1397_MGO_20151027
        if vConcesionarias.Count < 2 then                                                                       //SS_1397_MGO_20151027
            ppGroupFooterBand7.Visible := False;                                                                //SS_1397_MGO_20151027
    finally                                                                                                     //SS_1397_MGO_20151027
        spDetalleTransitos.First;                                                                               //SS_1397_MGO_20151027
        vConcesionarias.Free;                                                                                   //SS_1397_MGO_20151027
    end;                                                                                                        //SS_1397_MGO_20151027
    }
    try
        spObtenerDetalleTransitos.Close;
        spObtenerDetalleTransitos.Parameters.Refresh;
        spObtenerDetalleTransitos.Parameters.ParamByName('@TipoComprobante').Value := TipoCompro;
        spObtenerDetalleTransitos.Parameters.ParamByName('@NumeroComprobante').Value := NumeroCompro;
        spObtenerDetalleTransitos.Open;

        if spObtenerDetalleTransitos.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
            raise Exception.Create(spObtenerDetalleTransitos.Parameters.ParamByName('@ErrorDescription').Value);
    except
        on e: Exception do begin
            MsgBoxErr(STR_ERROR_SP, e.Message, STR_ERROR, MB_ICONERROR);
        end;
    end;
    // FIN : TASK_059_MGO_20160811
end;

procedure TFormReporteFacturacionDetallada.rptReporteFacturacionDetalladaBeforePrint(
  Sender: TObject);
begin
    try
        rbiDetallada.Report.PrinterSetup.BinName := rbiDetallada.GetConfig.BinName ;
        Preparar;
    except
        on e: Exception do begin
			MsgBoxErr(ERROR_INIT, e.Message, STR_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

end.

