object FormDenunciasConvenio: TFormDenunciasConvenio
  Left = 38
  Top = 84
  Width = 936
  Height = 542
  Caption = 'Denuncias'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GBPersona: TGroupBox
    Left = 0
    Top = 0
    Width = 928
    Height = 96
    Align = alTop
    Caption = 'Datos personales'
    TabOrder = 0
    inline FrameDatosPersonaConsulta1: TFrameDatosPersonaConsulta
      Left = 8
      Top = 16
      Width = 544
      Height = 68
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 96
    Width = 928
    Height = 89
    Align = alTop
    Caption = 'Convenio'
    TabOrder = 1
    inline FrameDatosConvenioConsulta1: TFrameDatosConvenioConsulta
      Left = 7
      Top = 13
      Width = 662
      Height = 67
      TabOrder = 0
    end
  end
  object PanelDeAbajo: TPanel
    Left = 0
    Top = 469
    Width = 928
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      928
      39)
    object BtnCancelar: TDPSButton
      Left = 846
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Cancelar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object btnAceptar: TDPSButton
      Left = 764
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Aceptar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Aceptar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 185
    Width = 613
    Height = 284
    Align = alClient
    Caption = 'Seleccione el veh'#237'culo'
    TabOrder = 3
    DesignSize = (
      613
      284)
    object dblVehiculos: TDBListEx
      Left = 8
      Top = 24
      Width = 596
      Height = 247
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 65
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 60
          Header.Caption = 'Dig. Verif.'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DigitoVerificadorPatente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Marca'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Arial'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripcionMarca'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Modelo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Modelo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Tipo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripcionTipoVehiculo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 35
          Header.Caption = 'A'#241'o'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'AnioVehiculo'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 90
          Header.Caption = 'Nro. de Tag'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'SerialNumberaMostrar'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 140
          Header.Caption = 'Nro. Contrato del  MOPTT'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Tiene Acoplado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TieneAcoplado'
        end>
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object GroupBox3: TGroupBox
    Left = 613
    Top = 185
    Width = 315
    Height = 284
    Align = alRight
    Caption = 'Denuncia'
    TabOrder = 4
    object lblOperacionGestion: TLabel
      Left = 12
      Top = 25
      Width = 46
      Height = 13
      Caption = 'Denuncia'
    end
    object Label2: TLabel
      Left = 10
      Top = 50
      Width = 46
      Height = 13
      Caption = 'Veh'#237'culo:'
    end
    object Label3: TLabel
      Left = 79
      Top = 51
      Width = 226
      Height = 13
      Caption = 'AB1234 FORD TAUNUS 1998'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 11
      Top = 96
      Width = 71
      Height = 13
      Caption = 'Observaciones'
    end
    object Label5: TLabel
      Left = 11
      Top = 70
      Width = 60
      Height = 13
      Caption = 'Nro. de Tag:'
    end
    object Label6: TLabel
      Left = 77
      Top = 70
      Width = 92
      Height = 13
      Caption = '1846468468464'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboBox1: TComboBox
      Left = 79
      Top = 21
      Width = 226
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 0
      Text = 'Robo de telev'#237'a'
      Items.Strings = (
        'Robo de telev'#237'a')
    end
    object Memo1: TMemo
      Left = 9
      Top = 112
      Width = 296
      Height = 41
      Lines.Strings = (
        '')
      TabOrder = 1
    end
    object btn_DocRequerido: TDPSButton
      Left = 224
      Top = 164
      Width = 82
      Height = 26
      Caption = '&Documentaci'#243'n'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object CollapsablePanel1: TCollapsablePanel
      Left = 2
      Top = 259
      Width = 311
      Height = 23
      Open = False
      Caption = 'Instrucciones'
      BarColorStart = 14071199
      BarColorEnd = 10646097
      RightMargin = 0
      LeftMargin = 0
      ArrowLocation = cpaRightTop
      InternalSize = 67
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      object Label1: TLabel
        Left = 0
        Top = 23
        Width = 311
        Height = 67
        Align = alClient
        Caption = 
          'Aca hay un espacio de ayuda al usuario que describe las instrucc' +
          'iones de la operaci'#243'n seg'#250'n la denuncia que haya seleccionado.'
        WordWrap = True
      end
    end
  end
end
