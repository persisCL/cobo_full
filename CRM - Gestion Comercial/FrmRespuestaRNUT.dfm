object FormRespuestaRNUT: TFormRespuestaRNUT
  Left = 209
  Top = 112
  Width = 812
  Height = 555
  Caption = 'Administraci'#243'n de respuestas del RNUT'
  Color = clBtnFace
  Constraints.MinHeight = 483
  Constraints.MinWidth = 812
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnl_Botones: TPanel
    Left = 0
    Top = 486
    Width = 804
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      804
      35)
    object btn_salir: TButton
      Left = 716
      Top = 8
      Width = 75
      Height = 20
      Anchors = [akTop, akRight, akBottom]
      Cancel = True
      Caption = '&Salir'
      TabOrder = 0
      OnClick = btn_salirClick
    end
  end
  object PControl: TPageControl
    Left = 0
    Top = 0
    Width = 804
    Height = 486
    ActivePage = P_Recha_Salida
    Align = alClient
    Style = tsFlatButtons
    TabIndex = 0
    TabOrder = 1
    OnChange = PControlChange
    object P_Recha_Salida: TTabSheet
      Caption = 'Envios (XML)'
      DesignSize = (
        796
        455)
      object Label1: TLabel
        Left = 573
        Top = 15
        Width = 163
        Height = 13
        Caption = 'Cantidad de Convenios afectados:'
      end
      object Label2: TLabel
        Left = 573
        Top = 29
        Width = 162
        Height = 13
        Caption = 'Cantidad de Registros  Mostrados:'
      end
      object lbl_CantConvAfectados: TLabel
        Left = 743
        Top = 15
        Width = 139
        Height = 13
        AutoSize = False
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_CantMostrar: TLabel
        Left = 743
        Top = 29
        Width = 8
        Height = 13
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 8
        Top = 2
        Width = 33
        Height = 13
        Caption = 'Estado'
      end
      object lbl_TipoError: TLabel
        Left = 155
        Top = 2
        Width = 61
        Height = 13
        Caption = 'Tipo de Error'
      end
      object Label7: TLabel
        Left = 384
        Top = 2
        Width = 100
        Height = 13
        Caption = 'Numero de Convenio'
      end
      object Label6: TLabel
        Left = 515
        Top = 2
        Width = 152
        Height = 13
        Caption = 'Cant. Max. de Conv. a Devolver'
      end
      object cb_Estados: TComboBox
        Left = 4
        Top = 17
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = cb_EstadosChange
      end
      object cb_Codigo_Error: TComboBox
        Left = 152
        Top = 17
        Width = 225
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        OnChange = cb_Codigo_ErrorChange
      end
      object dbl_Convenio: TDBListEx
        Left = 3
        Top = 45
        Width = 670
        Height = 225
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 25
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Marcado'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Numero Convenio CN'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dbl_ConvenioColumns0HeaderClick
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Numero Convenio RNUT'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dbl_ConvenioColumns0HeaderClick
            FieldName = 'NumeroConvenioRNUT'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Ultima Fecha de Envio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dbl_ConvenioColumns0HeaderClick
            FieldName = 'FechaEnvio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 400
            Header.Caption = 'Motivo Probable del Rechazo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'COMPLEMENTO'
          end>
        DataSource = ds_ObtenerConvenioRespuestaRNUT
        DragReorder = True
        ParentColor = False
        TabOrder = 5
        TabStop = True
        OnClick = dbl_ConvenioClick
        OnDblClick = dbl_ConvenioDblClick
        OnDrawText = dbl_ConvenioDrawText
      end
      object txt_NumConvenio: TEdit
        Left = 382
        Top = 17
        Width = 121
        Height = 21
        Hint = 'Numero de Convenio'
        MaxLength = 17
        TabOrder = 2
        OnChange = txt_NumConvenioChange
        OnKeyPress = txt_NumConvenioKeyPress
      end
      object pnl_BotonesConvenio: TPanel
        Left = 685
        Top = 45
        Width = 103
        Height = 228
        Anchors = [akTop, akRight, akBottom]
        BevelOuter = bvNone
        TabOrder = 4
        DesignSize = (
          103
          228)
        object btn_Reenviar: TButton
          Left = 2
          Top = 202
          Width = 100
          Height = 20
          Anchors = [akRight, akBottom]
          Caption = '&ReEnviar'
          TabOrder = 0
          OnClick = btn_ReenviarClick
        end
        object btn_Invertir: TButton
          Left = 2
          Top = 32
          Width = 100
          Height = 20
          Anchors = [akTop, akRight]
          Caption = '&Invertir Selecci'#243'n'
          TabOrder = 1
          OnClick = btn_InvertirClick
        end
        object btn_Marcar: TButton
          Left = 2
          Top = 56
          Width = 100
          Height = 20
          Anchors = [akTop, akRight]
          Caption = '&Marcar/Desmarcar'
          TabOrder = 2
          OnClick = btn_MarcarClick
        end
        object btnMarcarTodos: TButton
          Left = 2
          Top = 8
          Width = 100
          Height = 20
          Anchors = [akTop, akRight]
          Caption = 'Marcar &Todos'
          TabOrder = 3
          OnClick = btnMarcarTodosClick
        end
      end
      object txt_TOP: TNumericEdit
        Left = 512
        Top = 17
        Width = 57
        Height = 21
        MaxLength = 3
        TabOrder = 3
        OnChange = txt_TOPChange
        OnExit = txt_TOPExit
        Decimals = 0
        Value = 500
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 278
        Width = 792
        Height = 183
        Anchors = [akLeft, akRight, akBottom]
        Caption = 'Historico de Errores'
        TabOrder = 6
        DesignSize = (
          792
          183)
        object dbl_DetalleRespuesta: TDBListEx
          Left = 10
          Top = 15
          Width = 775
          Height = 96
          Anchors = [akLeft, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 400
              Header.Caption = 'Detalle Error'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Complemento'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Cantidad de Re Intentos'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'CantReIntentos'
            end>
          DataSource = ds_ObtenerDetalleRespuesta
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
        end
        object dbl_DetalleArchivos: TDBListEx
          Left = 9
          Top = 114
          Width = 776
          Height = 64
          Anchors = [akLeft, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Fecha Envio a RNUT'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'FechaEnvioRNUT'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 400
              Header.Caption = 'Nombre Archivo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NombreArchivo'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'Usuario'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'usuario'
            end>
          DataSource = ds_ObtenerEnviosConvenioRNUT
          DragReorder = True
          ParentColor = False
          TabOrder = 1
          TabStop = True
        end
      end
    end
    object P_Recha_Entrada: TTabSheet
      Caption = 'Entradas'
      ImageIndex = 1
      DesignSize = (
        796
        455)
      object lbl_CantConvAfectadosEntrada: TLabel
        Left = 562
        Top = 10
        Width = 8
        Height = 13
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 389
        Top = 10
        Width = 163
        Height = 13
        Caption = 'Cantidad de Convenios afectados:'
      end
      object Label4: TLabel
        Left = 390
        Top = 27
        Width = 162
        Height = 13
        Caption = 'Cantidad de Registros  Mostrados:'
      end
      object lbl_CantMostrarEntrada: TLabel
        Left = 562
        Top = 27
        Width = 8
        Height = 13
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 8
        Top = 2
        Width = 162
        Height = 13
        Caption = 'Estado de los Convenio a Importar'
      end
      object cb_CodigosErrorEntradasRNUT: TComboBox
        Left = 4
        Top = 17
        Width = 373
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = cb_CodigosErrorEntradasRNUTChange
      end
      object dbl_RechazosImportacionRNUT: TDBListEx
        Left = 3
        Top = 46
        Width = 790
        Height = 413
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 70
            Header.Caption = 'ID RNUT'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'ID'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 120
            Header.Caption = 'Codigo Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'codigo_concesionaria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 130
            Header.Caption = 'Fecha Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Fecha'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 200
            Header.Caption = 'Descripcion'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 200
            Header.Caption = 'Detalle'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Detalle'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 110
            Header.Caption = 'Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Concesionaria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 130
            Header.Caption = 'Fecha Actualizacion RNUT'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'fecha_actualizacion'
          end>
        DataSource = DS_ObtenerRechazosImportacionRNUT
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
      end
    end
    object P_Recha_Lista: TTabSheet
      Caption = 'Envios de Listas'
      ImageIndex = 2
      DesignSize = (
        796
        455)
      object Label9: TLabel
        Left = 573
        Top = 15
        Width = 163
        Height = 13
        Caption = 'Cantidad de Convenios afectados:'
      end
      object Label10: TLabel
        Left = 573
        Top = 29
        Width = 162
        Height = 13
        Caption = 'Cantidad de Registros  Mostrados:'
      end
      object lbl_CantConvAfectados_Lista: TLabel
        Left = 743
        Top = 15
        Width = 139
        Height = 13
        AutoSize = False
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_CantMostrar_Lista: TLabel
        Left = 743
        Top = 29
        Width = 8
        Height = 13
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 8
        Top = 2
        Width = 33
        Height = 13
        Caption = 'Estado'
      end
      object lbl_TipoError_Lista: TLabel
        Left = 155
        Top = 2
        Width = 61
        Height = 13
        Caption = 'Tipo de Error'
      end
      object Label15: TLabel
        Left = 384
        Top = 2
        Width = 92
        Height = 13
        Caption = 'Numero de Telev'#237'a'
      end
      object Label16: TLabel
        Left = 515
        Top = 2
        Width = 152
        Height = 13
        Caption = 'Cant. Max. de Conv. a Devolver'
      end
      object cb_Estados_Lista: TComboBox
        Left = 4
        Top = 17
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = cb_Estados_ListaChange
      end
      object cb_Codigo_Error_Lista: TComboBox
        Left = 152
        Top = 17
        Width = 225
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        OnChange = cb_Codigo_Error_ListaChange
      end
      object txt_NumTelevia: TEdit
        Left = 382
        Top = 17
        Width = 121
        Height = 21
        Hint = 'Numero de Convenio'
        TabOrder = 2
        OnChange = txt_NumTeleviaChange
        OnKeyPress = txt_NumTeleviaKeyPress
      end
      object txt_TOP_Lista: TNumericEdit
        Left = 512
        Top = 17
        Width = 57
        Height = 21
        MaxLength = 3
        TabOrder = 3
        OnChange = txt_TOPChange
        OnExit = txt_TOP_ListaExit
        Decimals = 0
        Value = 500
      end
      object dbl_Convenio_Lista: TDBListEx
        Left = 3
        Top = 45
        Width = 784
        Height = 225
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 90
            Header.Caption = 'Telev'#237'a'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dbl_Convenio_ListaColumns0HeaderClick
            FieldName = 'Televia'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 80
            Header.Caption = 'Context Mark'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dbl_Convenio_ListaColumns0HeaderClick
            FieldName = 'ContextMark'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 125
            Header.Caption = 'Contract Serial Number'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dbl_Convenio_ListaColumns0HeaderClick
            FieldName = 'ContractSerialNumber'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 200
            Header.Caption = 'Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dbl_Convenio_ListaColumns0HeaderClick
            FieldName = 'Concesionaria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 140
            Header.Caption = 'Fecha de Activaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dbl_Convenio_ListaColumns0HeaderClick
            FieldName = 'FechaActivacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 80
            Header.Caption = 'Color'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dbl_Convenio_ListaColumns0HeaderClick
            FieldName = 'Color'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 80
            Header.Caption = 'Accion'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Accion'
          end>
        DataSource = ds_ObtenerListasRespuestaRNUT
        DragReorder = True
        ParentColor = False
        TabOrder = 4
        TabStop = True
        OnClick = dbl_Convenio_ListaClick
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 272
        Width = 792
        Height = 183
        Anchors = [akLeft, akRight, akBottom]
        Caption = 'Historico de Mensajes'
        TabOrder = 5
        DesignSize = (
          792
          183)
        object dbl_DetalleRespuesta_Lista: TDBListEx
          Left = 10
          Top = 15
          Width = 775
          Height = 96
          Anchors = [akLeft, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 120
              Header.Caption = 'C'#243'dgio de Respuesta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'CodigoRespuesta'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 500
              Header.Caption = 'Complemento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Complemento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Fecha de Recepci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'FechaRecepcion'
            end>
          DataSource = ds_ObtenerListaDetallleRespuestaRNUT
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
        end
        object dbl_DetalleArchivos_Lista: TDBListEx
          Left = 9
          Top = 114
          Width = 776
          Height = 64
          Anchors = [akLeft, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Fecha Envio a RNUT'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'FechaEnvioRNUT'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 400
              Header.Caption = 'Nombre Archivo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NombreArchivo'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'Usuario'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'usuario'
            end>
          DataSource = ds_ObtenerListaEnviosRNUT
          DragReorder = True
          ParentColor = False
          TabOrder = 1
          TabStop = True
        end
      end
    end
  end
  object ObtenerRechazosImportacionRNUT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = ObtenerRechazosImportacionRNUTAfterOpen
    ProcedureName = 'ObtenerRechazosImportacionRNUT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoEstado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = '1'
      end>
    Left = 616
    Top = 96
  end
  object DS_ObtenerRechazosImportacionRNUT: TDataSource
    DataSet = ObtenerRechazosImportacionRNUT
    Left = 584
    Top = 96
  end
  object ds_ObtenerConvenioRespuestaRNUT: TDataSource
    DataSet = cdsObtenerConvenioRespuestaRNUT
    Left = 34
    Top = 114
  end
  object ObtenerConvenioRespuestaRNUT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = ObtenerConvenioRespuestaRNUTAfterOpen
    ProcedureName = 'ObtenerConvenioRespuestaRNUT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoRespuesta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Top'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 98
    Top = 114
    object ObtenerConvenioRespuestaRNUTNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 19
    end
    object ObtenerConvenioRespuestaRNUTCantConveniosAfectados: TIntegerField
      FieldName = 'CantConveniosAfectados'
      ReadOnly = True
    end
    object ObtenerConvenioRespuestaRNUTNumeroConvenioRNUT: TStringField
      FieldName = 'NumeroConvenioRNUT'
      FixedChar = True
      Size = 30
    end
    object ObtenerConvenioRespuestaRNUTEstado: TStringField
      FieldName = 'Estado'
      FixedChar = True
      Size = 1
    end
    object ObtenerConvenioRespuestaRNUTFechaEnvio: TDateTimeField
      FieldName = 'FechaEnvio'
    end
    object ObtenerConvenioRespuestaRNUTMarcado: TIntegerField
      FieldName = 'Marcado'
      ReadOnly = True
    end
    object ObtenerConvenioRespuestaRNUTCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 255
    end
  end
  object ds_ObtenerDetalleRespuesta: TDataSource
    DataSet = ObtenerDetalleRespuesta
    Left = 50
    Top = 226
  end
  object ObtenerDetalleRespuesta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = ObtenerDetalleRespuestaAfterScroll
    AfterScroll = ObtenerDetalleRespuestaAfterScroll
    ProcedureName = 'ObtenerDetalleRespuesta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroConvenioRNUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end>
    Left = 82
    Top = 226
  end
  object ds_ObtenerEnviosConvenioRNUT: TDataSource
    DataSet = ObtenerEnviosConvenioRNUT
    Left = 42
    Top = 338
  end
  object ObtenerEnviosConvenioRNUT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerEnviosConvenioRNUT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroConvenioRNUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoEnvioConvenioRNUT'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 82
    Top = 338
  end
  object ReenviarSalidaRNUT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ReenviarSalidaRNUT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroConvenioRNUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end>
    Left = 616
    Top = 128
  end
  object Imagenes: TImageList
    Left = 141
    Top = 115
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008C8C9400B5B5B500F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7
      E7003939420018212100ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600182121000000000031313900F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF003942
      4200181821003942420018182100ADB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0063636B001818
      210031313900E7E7E7003131390029293100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6003939
      4200FFFFFF00FFFFFF00A5A5A500212129004A525200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006B6B7300212929007B848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007373730021212900C6C6C600F7F7
      F700FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7005A5A5A0031313900D6DE
      DE00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042424A001821
      2100949C9C00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9C
      9C002129290094949C00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7E7E70042424A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object cdsObtenerConvenioRespuestaRNUT: TClientDataSet
    Active = True
    Aggregates = <>
    AutoCalcFields = False
    FieldDefs = <
      item
        Name = 'NumeroConvenio'
        DataType = ftString
        Size = 19
      end
      item
        Name = 'CantConveniosAfectados'
        DataType = ftInteger
      end
      item
        Name = 'NumeroConvenioRNUT'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Estado'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FechaEnvio'
        DataType = ftDateTime
      end
      item
        Name = 'Marcado'
        DataType = ftBoolean
      end
      item
        Name = 'COMPLEMENTO'
        DataType = ftString
        Size = 255
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterScroll = cdsObtenerConvenioRespuestaRNUTAfterScroll
    Left = 64
    Top = 112
    Data = {
      E10000009619E0BD010000001800000007000000000003000000E1000E4E756D
      65726F436F6E76656E696F010049000000010005574944544802000200130016
      43616E74436F6E76656E696F73416665637461646F730400010000000000124E
      756D65726F436F6E76656E696F524E5554010049000000010005574944544802
      0002001E000645737461646F0100490000000100055749445448020002000100
      0A4665636861456E76696F0800080000000000074D61726361646F0200030000
      0000000B434F4D504C454D454E544F0200490000000100055749445448020002
      00FF000000}
    object cdsObtenerConvenioRespuestaRNUTNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      FixedChar = True
      Size = 19
    end
    object cdsObtenerConvenioRespuestaRNUTCantConveniosAfectados: TIntegerField
      FieldName = 'CantConveniosAfectados'
    end
    object cdsObtenerConvenioRespuestaRNUTNumeroConvenioRNUT: TStringField
      FieldName = 'NumeroConvenioRNUT'
      FixedChar = True
      Size = 30
    end
    object cdsObtenerConvenioRespuestaRNUTEstado: TStringField
      FieldName = 'Estado'
      FixedChar = True
      Size = 1
    end
    object cdsObtenerConvenioRespuestaRNUTFechaEnvio: TDateTimeField
      FieldName = 'FechaEnvio'
    end
    object cdsObtenerConvenioRespuestaRNUTMarcado: TBooleanField
      FieldName = 'Marcado'
    end
    object cdsObtenerConvenioRespuestaRNUTCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 255
    end
  end
  object ObtenerListasRespuestaRNUT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = ObtenerListasRespuestaRNUTAfterOpen
    ProcedureName = 'ObtenerListasRespuestaRNUT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = 'e'
      end
      item
        Name = '@CodigoRespuesta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end
      item
        Name = '@NumeroTag'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = '1'
      end
      item
        Name = '@Top'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 616
    Top = 184
  end
  object ds_ObtenerListasRespuestaRNUT: TDataSource
    DataSet = ObtenerListasRespuestaRNUT
    Left = 584
    Top = 184
  end
  object ds_ObtenerListaDetallleRespuestaRNUT: TDataSource
    DataSet = ObtenerListaDetallleRespuestaRNUT
    Left = 584
    Top = 218
  end
  object ObtenerListaDetallleRespuestaRNUT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = ObtenerListaDetallleRespuestaRNUTAfterScroll
    AfterScroll = ObtenerListaDetallleRespuestaRNUTAfterScroll
    ProcedureName = 'ObtenerListaDetallleRespuestaRNUT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroEtiqueta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = '1'
      end
      item
        Name = '@CodigoColor'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = 1
      end>
    Left = 618
    Top = 218
  end
  object ds_ObtenerListaEnviosRNUT: TDataSource
    DataSet = ObtenerListaEnviosRNUT
    Left = 584
    Top = 250
  end
  object ObtenerListaEnviosRNUT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerListaEnviosRNUT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoEnvioConvenioRNUT'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 618
    Top = 250
  end
end
