{********************************** Unit Header ********************************
File Name : frmEmitirCertificadoInfracciones.pas
Author : pdominguez
Date Created: 07/07/2009
Language : ES-AR
Description : SS 784
    - Se crea la Unit por el requerimiento de la SS 784 para la emisi�n de
    Certificados de Infracciones.
    - Unit creada a partir de la fFacturacionInfracciones.pas

Revision 1
Author: mbecerra
Date: 09-Julio-2009
Description:	(Ref. SS 784)
			1.-	Se corrige el funcionamiento, pues estaba buscando mal el cliente
            	(Desplegaba los datos de otro cliente)
            2.-	Se agrega el bot�n "btnBuscarCliente"
            3.-	La Impresi�n del certificado s�lo se habilita si tiene ingresada
            	la patente y ambas fechas, adem�s de los datos del cliente.
            4.- Dado que el usuario puede ingresar una fecha (para que se habilite el
            	bot�n Imprimir) y luego puede modificarla para Imprimir (sin que
                se refresque la ventana), se valida que las fecha de la consulta
                no hayan sido modificadas al momento de imprimir.
            5.- Se agregan las columnas de Tipo y Numerocomprobante Fiscal para las
            	infracciones facturadas.
            6.-	Tambi�n se cambia la llamada a la funci�n InicializaConvenioConValores
            	por la llamada a Inicializar, pues dado que la ventana se despliega s�lo
                si el Rut no tiene convenios, y al hacerlo no estaba mostrando
                las regiones ni las comunas.
            7.-	Se quita la llamada a la funcion TieneConvenioActivo pues lo que se desea
            	es que el cliente exista y al menos posea un convenio, no importa
                si est� dado de baja.

Revision : 2
    Author : pdominguez
    Date   : 21/09/2009
    Description : SS 828
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            peRUTClienteButtonClick
            btnImprimirCertificadoClick

Revision : 3
    Author : pdominguez
    Date   : 30/09/2009
    Description : SS 719
        - Se modificaron/a�adieron/eliminaron los siguientes objetos/constantes/variables:
            - Se elimin� el objeto spAgregarAHistoricoCertificadosInfraccionesEmitidos: TADOStoredProc.
            - Se cambi� en el objeto cldInfracciones: TClientDataSet, el nombre del campo Fecha por
            FechaInfraccion.

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            btnImprimirCertificadoClick,
            CargarInfracciones
            btnBuscarClick

Revision : 4
    Author : pdominguez
    Date   : 27/11/2009
    Description : SS 719 - Defects 13718, 13719 y 13741
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            btnBuscarClick
            MuestraDatosClienteCertificado

Revision : 5
    Author : pdominguez
    Date   : 11/02/2010
    Description : SS 719
            - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
                FEstadoPagoPago,
                Se a�adio el campo EstadoPago al objeto cldInfracciones: TClientDataSet,
                Se a�adi� la columna EstadoPago al objeto dblInfracciones: TDBListEx.

            - Se modificaron/a�adieron los siguientes procedimientos/funciones:
                dblInfraccionesDrawText,
                Inicializar,
                CargarInfracciones

 Revision : 6
    Author : pdominguez
    Date   : 04/07/2010
    Description : Infractores Fase 2
            - Se elimina la Funci�n InfraccionEsFacturable.
            - Se eliminaron las variables FInfraccionFacturada, FInfraccionEnBHTU y
            FInfraccionEnPDU.

            - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
                Se a�adieron las variables FInfraccionPagada, FInfraccionAnulada.
                Se elimina el campo Descripcion del objeto dblInfracciones: TDBListEx.
                Se a�aden los campos EstadoCN y EstadoIF al objeto dblInfracciones: TDBListEx.
                Se elimina los campos Descripcion y ExisteTransitoNoAnulado del objeto cldInfracciones: TClientDataSet.
                Se a�aden los campos CodigoEstadoInterno, EstadoCN y EstadoIF al objeto
                cldInfracciones: TClientDataSet.

            - Se modificaron/a�adieron los siguientes procedimientos/funciones:
                Inicializar,
                CargarInfracciones
                dblInfraccionesDrawText,
                btnImprimirCertificadoClick

 Firma          : PAR00114-NDR-20110901
 Description    : Comentar inhabilitacion del Combo de Concesionarias

Author      :   Claudio Quezada Ib��ez
Date        :   06-Agosto-2014
Firma       :   SS_660D_CQU_20140806
Descripcion :   Se agrega permiso para emitir certificado de infracciones de tipo 2 (ListaAmarilla).
                Se agrega RadioButon para elegir el tipo de infraccion a obtener (con persimso)

 Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
 
Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

*******************************************************************************}
unit frmEmitirCertificadoInfracciones;

interface

uses
  DMConnection,
  Util,
  UtilDB,
  UtilProc,
  PeaProcs,
  PeaTypes,
  BuscaClientes,
  FrmSolicitudContacto,
  Main,
  RStrings,
  frmReportCertificadoInfracciones,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, DbList, StdCtrls, Validate, DateEdit, ExtCtrls, ListBoxEx,
  DBListEx,  DmiCtrls, DBClient, ImgList, VariantComboBox, Grids, DBGrids;

type
  TEmitirCertificadoInfraccionesForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    edPatente: TEdit;
    deDesdeFecha: TDateEdit;
    deHastaFecha: TDateEdit;
    lblFechaHasta: TLabel;
    lblFechaDesde: TLabel;
    lblPatente: TLabel;
    btnSalir: TButton;
    btnBuscar: TButton;
    dblInfracciones: TDBListEx;
    spObtenerInfraccionesAFacturar: TADOStoredProc;
    cldInfracciones: TClientDataSet;
    ds_Infracciones: TDataSource;
    gbDatosCertificado: TGroupBox;
    lblRUT: TLabel;
    peRUTCliente: TPickEdit;
    gbDatosCliente: TGroupBox;
    lblApellido: TLabel;
    lblDatoApellidoNombre: TLabel;
    btnImprimirCertificado: TButton;
    btnBuscarCliente: TButton;
    spObtenerConveniosCliente: TADOStoredProc;
    cbConcesionarias: TVariantComboBox;
    Label1: TLabel;
    rgMorosidad: TRadioGroup;   // SS_660D_CQU_20140806
    procedure dblInfraccionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure edPatenteChange(Sender: TObject);
    procedure peRUTClienteChange(Sender: TObject);
    procedure edPatenteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnImprimirCertificadoClick(Sender: TObject);
    procedure btnBuscarClienteClick(Sender: TObject);
    procedure cbConcesionariasChange(Sender: TObject);
    procedure rgMorosidadClick(Sender: TObject);    // SS_660D_CQU_20140806
  private
    { Private declarations }
	FUltimaBusqueda : TBusquedaCliente;

    FInfraccionPagada,
    FInfraccionAnulada: integer;

    FEstadoPagoPago: String; // Rev. 5 (SS 719)

    SePuedeEmitirCertificado: Boolean;
    DatosClienteCertificado: TDatosPersonales;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    //REV.1
    FFechaDesde, FFechaHasta : TDateTime;
    FTraerListaAmarilla : Boolean; // SS_660D_CQU_20140806
  public
    { Public declarations }
    function Inicializar: Boolean;

    procedure LimpiarDatosCertificado;
    procedure MuestraDatosClienteCertificado;
    function  ObtenerNombreCompletoClienteCertificado: String;

    function CargarInfracciones : boolean;
  end;

var
  EmitirCertificadoInfraccionesForm: TEmitirCertificadoInfraccionesForm;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Procedure Name: Inicializar
Author :
Date Created :
Parameters : None
Return Value : Boolean
Description :

Revision : 1
    Author : pdominguez
    Date   : 12/02/2010
    Description : SS 719
        - Se asigna el valor a la nueva variable FEstadoPago.

Revision : 6
    Author : pdominguez
    Date   : 04/07/2010
    Description : Infractores Fase 2
        - Se carga el combo de Concesionarias.
        - Se eliminan las variables FInfraccionFacturada, FInfraccionEnBHTU y FInfraccionEnPDU
*******************************************************************************}
function TEmitirCertificadoInfraccionesForm.Inicializar: Boolean;
begin
	if not cldInfracciones.Active then cldInfracciones.Active := True;
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;
    //Abro el dataset
    cldInfracciones.Close;
    cldInfracciones.CreateDataSet;
    cldInfracciones.Open;
    //Centro el formulario
    CenterForm(Self);
    //cargo estas constantes
    FEstadoPagoPago := QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.CONST_ESTADO_PAGO_PAGO()'); // Rev. 1 (SS 719)
    FInfraccionPagada := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_ESTADO_INFRACCION_INTERNO_PAGADA()');
    FInfraccionAnulada := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_ESTADO_INFRACCION_INTERNO_ANULADA()');

    SePuedeEmitirCertificado := False;
    LimpiarDatosCertificado;

   //CargarComboConcesionarias(DMConnections.BaseCAC, cbConcesionarias, False); // Rev. 6 (Infractores Fase 2)
    //cbConcesionarias.Items.Add('Costanera Norte', 1);                         							//SS_1147_MCA_20140408
    cbConcesionarias.Items.Add(ObtenerNombreConcesionariaNativa, ObtenerCodigoConcesionariaNativa);			//SS_1147_MCA_20140408
    cbConcesionarias.Items.Add('Acceso Vial AMB', 5);

    // Temporal a eliminar cuando se habiliten el resto de concesionarias para emitir los certificados      //PAR00114-NDR-20110901
    //cbConcesionarias.ItemIndex := cbConcesionarias.Items.IndexOfValue(1);                             	//SS_1147_MCA_20140408 //PAR00114-NDR-20110901
    cbConcesionarias.ItemIndex := cbConcesionarias.Items.IndexOfValue(ObtenerCodigoConcesionariaNativa);	//SS_1147_MCA_20140408
    //cbConcesionarias.Enabled   := False;                                                                  //PAR00114-NDR-20110901
    FTraerListaAmarilla := False;                                           // SS_660D_CQU_20140806
    rgMorosidad.Buttons[0].Checked := True;                                 // SS_660D_CQU_20140806
    rgMorosidad.Enabled := ExisteAcceso('certificado_infractor_moroso');    // SS_660D_CQU_20140806

    Result := True;
end;

{******************************* Procedure Header ******************************
Procedure Name: btnBuscarClick
Author : pdominguez
Date Created : 07/07/2009
Parameters : Sender: TObject
Description : SS 784

Revision : 1
    Author : pdominguez
    Date   : 05/11/2009
    Description : SS 719
        - Se implementa la validaci�n del formato de la Patente.

Revision : 2
    Author : pdominguez
    Date   : 27/11/2009
    Description : SS 719 - Defect 13741
        - Defect 13741:
            Se elimina la comprobaci�n del formato para patentes no chilenas (m�s
        de 6 posiciones)
*******************************************************************************}
procedure TEmitirCertificadoInfraccionesForm.btnBuscarClick(Sender: TObject);
resourcestring
    MESSAGE_PATENTE_VACIA 			= 'Patente vac�a';
    MESSAGE_PATENTE_NO_VALIDA       = 'El formato de la Patente ingresada NO es v�lido.'; // Rev. 1 (SS 719)
    MESSAGE_FECHA_DESDE_VACIA 		= 'Fecha desde vac�a';
    MESSAGE_FECHA_HASTA_VACIA 		= 'Fecha hasta vac�a';
    MESSAGE_HASTA_MENOR_A_DESDE 	= 'Fecha hasta es menor a fecha desde ';
    MSG_INFRACCIONES_PENDIENTES     = 'Existen infracciones pendientes o asociadas a comprobantes impagos. No se puede emitir el Certificado.';
    MSG_INFRACCIONES_NO_ENCONTRADAS = 'No se han encontrado infracciones con este criterio de b�squeda';

begin
    //Verifica si son validos los datos para la busqueda

    {----------------REV.1
  	if not ValidateControls(	[edPatente,
                                 deDesdeFecha,
                                 deHastaFecha,
                                 deHastaFecha],
    							[edPatente.Text <> '',
                                 deDesdeFecha.Text <> '  /  /    ',
                                 deHastaFecha.Text <> '  /  /    ',
                                 deDesdeFecha.Date <= deHastaFecha.Date],
                                Caption,
                                [MESSAGE_PATENTE_VACIA,
                                 MESSAGE_FECHA_DESDE_VACIA,
                                 MESSAGE_FECHA_HASTA_VACIA,
                                 MESSAGE_HASTA_MENOR_A_DESDE ]) then Exit;
    }

    cldInfracciones.EmptyDataSet;
    dblInfracciones.Invalidate;
    LimpiarDatosCertificado();
    // Rev. 1 (SS 719)
    if not ValidateControls(
                [edPatente,
                 edPatente],
                [edPatente.Text <> '',
                 ((Length(edPatente.Text) > 6) or VerificarFormatoPatenteChilena(DMConnections.BaseCAC, edPatente.Text))], //Rev. 2 (SS 719 - Defect 13741)
                Caption,
                [MESSAGE_PATENTE_VACIA,
                 MESSAGE_PATENTE_NO_VALIDA]) then Exit;
    // Fin Rev. 1 (SS 719)
    
    if		(deDesdeFecha.Date <> NullDate) and
    		(deHastaFecha.Date <> NullDate) and
        	(deDesdeFecha.Date > deHastaFecha.Date) then begin
    	MsgBoxBalloon(MESSAGE_HASTA_MENOR_A_DESDE, Caption, MB_ICONSTOP, deHastaFecha);
        Exit;
    end;

    FFechaDesde := deDesdeFecha.Date;
    FFechaHasta := deHastaFecha.Date;
    //-----------------------------------FIN REV.1

    if CargarInfracciones() then begin
        LimpiarDatosCertificado;
    	if (spObtenerInfraccionesAFacturar.RecordCount > 0) then begin
            if SePuedeEmitirCertificado then begin
            	peRUTCliente.SetFocus;
            end
            else MsgBox(MSG_INFRACCIONES_PENDIENTES, Self.Caption, MB_OK + MB_ICONINFORMATION);
    	end else MsgBox(MSG_INFRACCIONES_NO_ENCONTRADAS, Self.Caption, MB_OK + MB_ICONINFORMATION);
    end;
end;

{******************************* Procedure Header ******************************
Procedure Name: peRUTClienteButtonClick
Author :
Date Created :
Parameters : Sender: TObject
Description :

Revision : 1
    Author : pdominguez
    Date   : 18/09/2009
    Description : SS 828
        - Se invierte el orden de las l�neas para evitar conflictos con el
        evento OnChange del objeto.
*******************************************************************************}
procedure TEmitirCertificadoInfraccionesForm.peRUTClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
	FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);

    Application.createForm(TFormBuscaClientes, f);
	if f.Inicializa(FUltimaBusqueda) then begin
        f.ShowModal;
        if f.ModalResult = idOk then begin
            fUltimaBusqueda := f.UltimaBusqueda;
            peRUTCliente.Text :=  f.Persona.NumeroDocumento; // Rev. 1 (SS 828)
            DatosClienteCertificado := f.Persona; // Rev. 1 (SS 828)
            MuestraDatosClienteCertificado;
            peRUTCliente.setFocus;
        end;
    end;
    f.free;
end;

procedure TEmitirCertificadoInfraccionesForm.peRUTClienteChange(Sender: TObject);
begin
    if ActiveControl = Sender then begin
        lblDatoApellidoNombre.Caption := '';
        btnImprimirCertificado.Enabled := False;
        //REV.1
    	DatosClienteCertificado.CodigoPersona := -1;
    end;
end;

{---------------------------------------------------------------
            btnBuscarClienteClick

Author: mbecerra
Date: 09-Julio-2009
Decription:	Busca los datos del cliente (Rut) ingresado.
----------------------------------------------------------------}
procedure TEmitirCertificadoInfraccionesForm.btnBuscarClienteClick(Sender: TObject);
resourcestring
	MSG_FALTA_RUT		= 'No ha ingresado el Rut del cliente a buscar';
    MSG_ERROR 			= 'Ocurri� un error al intentar obtener los datos del Cliente';
    MSG_RUT_INVALIDO	= 'El Rut es inv�lido';
    
begin
    if not ValidateControls(	[peRUTCliente, peRUTCliente],
    							[peRUTCliente.Text <> '', ValidarRUT(DMConnections.BaseCAC, peRUTCliente.Text)],
                                Caption,
                                [MSG_FALTA_RUT, MSG_RUT_INVALIDO]) then Exit;
    try
        DatosClienteCertificado := ObtenerDatosPersonales(DMConnections.BaseCAC, 'RUT', peRUTCliente.Text);
        MuestraDatosClienteCertificado();
    except on e:exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;
end;

{******************************** Function Header ******************************
Procedure Name: CargarInfracciones
Author :
Date Created :
Parameters : None
Return Value : None
Description :

Revision : 1
    Author : pdominguez
    Date   : 02/10/2009
    Description : SS 719
        - Se cambia el nombre del Campo Fecha por FechaInfraccion por compatibilizaci�n
        con la impresi�n del certificado en la WEB.

Revision : 2
    Author : pdominguez
    Date   : 15/02/2010
    Description : SS 719
        - Se a�ade el control sobre la Fecha de Anulaci�n, esta no puede ser nula
        ya que debe ser almacenada en el detalle de la auditor�a.
        - Se a�ade el campo EstadoPago para el control sobre los comprobantes,
        estos deben encontrarse Pago para que se pueda emitir el Certificado.

Revisi�n : 6
    Author : pdominguez
    Date   : 04/07/2010
    Description : Infractores Fase 2
        - Se a�ade la asignaci�n del par�metro @CodigoConcesionaria al objeto
        spObtenerInfraccionesAFacturar.
        - Se elimina la Asignaci�n de los Campos Descripcion y ExisteTransitoNoAnulado.
        - Se agrega la Asignaci�n de los campos CodigoEstadoInterno, EstadoCN y EstadoIF.
*******************************************************************************}
function TEmitirCertificadoInfraccionesForm.CargarInfracciones;
resourcestring
    ERROR_OBT_INFRACCIONES 	=  'Error obteniendo infracciones';
    MSG_INFRACCIONES_NO_ENCONTRADAS = 'No se han encontrado infracciones con este criterio de b�squeda';
begin
    try
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    Result := True;
    try
    	spObtenerInfraccionesAFacturar.Close;
        cldInfracciones.EmptyDataSet;
        cldInfracciones.DisableControls;

        with spObtenerInfraccionesAFacturar do begin
        	//Cargo los parametros
            if deDesdeFecha.Date = NullDate then Parameters.ParamByName('@FechaInicial').Value := NULL
            else Parameters.ParamByName('@FechaInicial').Value := deDesdeFecha.Date;

            if deHastaFecha.Date = NullDate then Parameters.ParamByName('@FechaFinal').Value := NULL
            else Parameters.ParamByName('@FechaFinal').Value := deHastaFecha.Date;

            Parameters.ParamByName('@Patente').Value := edPatente.Text;
            Parameters.ParamByName('@ValidarInfraccionesImpagas').Value := False;   // Rev. 3 (SS 719)  // SS_INFRA_FASE2_20110624
            Parameters.ParamByName('@CodigoConcesionaria').Value := cbConcesionarias.Value; // Rev. 6 (Infractores Fase 2)
            Parameters.ParamByName('@TraerListaAmarilla').Value := FTraerListaAmarilla;     // SS_660D_CQU_20140806
            Open;

            if not Eof then begin

                SePuedeEmitirCertificado := Parameters.ParamByName('@SePuedeEmitirCertificado').Value;  // Rev. 3 (SS 719)

                while not Eof do begin
                	cldInfracciones.Append;
                    cldInfracciones.FieldByName('CodigoInfraccion').AsInteger	:= FieldByName('CodigoInfraccion').Value;
                    cldInfracciones.FieldByName('Patente').AsString				:= FieldByName('Patente').Value;
                    cldInfracciones.FieldByName('FechaInfraccion').AsDateTime	:= FieldByName('FechaInfraccion').Value; // Rev. 1 (SS 719)
                    // Rev. 6 (Infractores Fase 2)
                    cldInfracciones.FieldByName('EstadoCN').AsString			:= FieldByName('EstadoCN').Value;
                    cldInfracciones.FieldByName('EstadoIF').AsString			:= FieldByName('EstadoIF').Value;
                    cldInfracciones.FieldByName('CodigoEstadoInterno').AsString := FieldByName('CodigoEstadoInterno').Value;
                    // Rev. 6 (Infractores Fase 2)
                    cldInfracciones.FieldByName('MotivoAnulacion').AsString		:= FieldByName('MotivoAnulacion').Value;
                    cldInfracciones.FieldByName('Nombre').AsString				:= IIF(FieldByName('Nombre').Value = null, '',FieldByName('Nombre').Value );
                    cldInfracciones.FieldByName('NumeroDocumento').AsString		:= IIF(FieldByName('NumeroDocumento').Value = null, '', FieldByName('NumeroDocumento').Value);
                    cldInfracciones.FieldByName('PrecioInfraccion').AsInteger	:= IIF(FieldByName('PrecioInfraccion').Value = null, 0,FieldByName('PrecioInfraccion').Value);
                    cldInfracciones.FieldByName('Numeroconvenio').Asstring		:= IIF(FieldByName('NumeroConvenio').Value = null, '', FieldByName('NumeroConvenio').Value);
                    cldInfracciones.FieldByName('Codigoconvenio').AsInteger		:= IIF(FieldByName('CodigoConvenio').Value = null , 0 , FieldByName('CodigoConvenio').Value);
                    if FieldByName('FechaAnulacion').Value <> NULL then begin
                    	cldInfracciones.FieldByName('FechaAnulacion').AsDateTime := FieldByName('FechaAnulacion').AsDateTime;
                    end; // else SePuedeEmitirCertificado := False; // Rev. 2 (SS 719) // Rev. 3 (SS 719)

                    // Rev. 2 (SS 719)
                    cldInfracciones.FieldByName('EstadoPago').AsString := FieldByName('EstadoPago').AsString;
                    // Fin Rev. 2 (SS 719)

                    cldInfracciones.FieldByName('CodigoMotivoAnulacion').AsInteger := IIF(FieldByName('CodigoMotivoAnulacion').Value = null , 0 , FieldByName('CodigoMotivoAnulacion').Value);
                    //Agregado Revisi�n 4
                    cldInfracciones.FieldByName('CodigoTarifaDayPassBALI').AsInteger := FieldByName('CodigoTarifaDayPassBALI').AsInteger;
                    //REV.1
                    cldInfracciones.FieldByName('TipoComprobanteFiscal').AsString := FieldByName('TipoComprobanteFiscalDesc').AsString;
                    cldInfracciones.FieldByName('NumeroComprobanteFiscal').AsInteger := FieldByName('NumeroComprobanteFiscal').AsInteger;
                    cldInfracciones.Post;
                    {
                    // Rev. 2 (SS 719)
                    if (FieldByName('CodigoMotivoAnulacion').AsInteger = FInfraccionFacturada) and
                        (FieldByName('EstadoPago').AsString <> FEstadoPagoPago) then SePuedeEmitirCertificado := False;
                    // Fin Rev. 2 (SS 719)

                    SePuedeEmitirCertificado := (Not InfraccionEsFacturable) And SePuedeEmitirCertificado;
                    }
                    Next;
                end;

                cldInfracciones.First;
            end;
          end; {with}

      except
        on e: exception do begin
              Result := False;
              MsgBoxErr(ERROR_OBT_INFRACCIONES, e.message, caption, MB_ICONSTOP);
          end;
      end;

    finally


    cldInfracciones.EnableControls;
    dblInfracciones.Invalidate;
	Screen.Cursor := crDefault;
    Application.ProcessMessages;
    end;
end;

{******************************* Procedure Header *****************************
Procedure Name: dblInfraccionesDrawText
Author :
Date Created :
Parameters : Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
    State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
    var ItemWidth: Integer; var DefaultDraw: Boolean
Description :

Revision : 1
    Author : pdominguez
    Date   : 11/02/2010
    Description : SS 719
        - Tambi�n se ponen en color rojo adem�s de las lineas que son facturables,
        las que no poseen fecha de anulaci�n y aquellos comprobantes emitidos que
        no esten pagados.

Revision : 1
    Author : pdominguez
    Date   : 04/07/2010
    Description : Infractores Fase 2
        - Se cambia la l�gica para determinar las infraccines NO regularizadas.
*******************************************************************************}
procedure TEmitirCertificadoInfraccionesForm.dblInfraccionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: string; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if not (cldInfracciones.FieldByName('CodigoEstadoInterno').AsInteger in [FInfraccionPagada, FInfraccionAnulada]) then
    begin
        Sender.Canvas.Font.Style := Sender.Canvas.Font.Style + [fsBold];
        if Not (odSelected in State) then
            Sender.Canvas.Font.Color := clRed;
    end;
end;

procedure TEmitirCertificadoInfraccionesForm.edPatenteChange(Sender: TObject);
begin
    if ActiveControl = Sender then begin
		cldInfracciones.EmptyDataSet;
        dblInfracciones.Invalidate;
        SePuedeEmitirCertificado := False;
        LimpiarDatosCertificado;
    end;
end;

procedure TEmitirCertificadoInfraccionesForm.edPatenteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if Key = VK_RETURN then btnBuscarClick(Nil);
end;

{******************************* Procedure Header ******************************
Procedure Name: btnImprimirCertificadoClick
Author :
Date Created :
Parameters : Sender: TObject
Description :

Revision : 1
    Author : pdominguez
    Date   : 21/09/2009
    Description : SS 828
        - Se a�ade la asignaci�n del nuevo par�metro en la llamada a la funci�n
        ImprimirCertificado.

Revision : 2
    Author : pdominguez
    Date   : 30/09/2009
    Description : SS 719
        - Se elimina el formateo del N�mero de Documento (RUT) y se incluye dentro
        de la impresi�n del reporte para compatibilizar la impresi�n con la WEB.
        - Se pasa la auditoria de registro del Certificado dentro de la unit
        frmReportCertificadoInfracciones, para compatibilizar la impresi�n desde la WEB.
        - Se modifican las llamadas a las funciones ImprimirCertificado e Inicializar por
        cambio de sus par�metros.

 Revision : 6
    Author : pdominguez
    Date   : 04/07/2010
    Description : Infractores Fase 2
        - Se a�ade el C�digo de la Concesionaria a la llamada del reporte del
        Certificado.
*******************************************************************************}
procedure TEmitirCertificadoInfraccionesForm.btnImprimirCertificadoClick(Sender: TObject);
    resourcestring
        DESC_TRATAMIENTO_DON = 'Don';
        DESC_TRATAMIENTO_DONA = 'Do�a';
        DESC_TRATAMIENTO_EMPRESA = 'La Empresa';
        DESC_RELACION_DON = 'due�o';
        DESC_RELACION_DONA = 'due�a';
        DESC_RELACION_EMPRESA = 'due�a';

        MSG_ERROR_CREACION_REPORTE = 'Se produjo un Error al Crear el Reporte.';
        //REV.1
        MSG_FECHAS = 'Debe ingresar ambas Fechas (Desde - Hasta) para imprimir el certificado';
        MSG_FECHAS_2 = 'Ha modificado las fechas, pues Fecha Desde y/o Hasta no coincide con las fechas de la consulta de infracciones';

    Var
        f: TReportCertificadoInfraccionesForm;
        DescError: String;
begin
	//REV.1
    if not ValidateControls(	[deDesdeFecha, deHastaFecha, deDesdeFecha, deHastaFecha],
    							[deDesdeFecha.Date <> NullDate, deHastaFecha.Date <> NullDate,
                                 deDesdeFecha.Date = FFechaDesde, deHastaFecha.Date = FFechaHasta],
                                Caption,
                                [MSG_FECHAS, MSG_FECHAS, MSG_FECHAS_2, MSG_FECHAS_2]) then Exit;

    //----------- FIN REV.1
    Try
        Application.CreateForm(TReportCertificadoInfraccionesForm, f);
        if f.Inicializar(
                DMConnections.BaseCAC,
                DescError,
                NowBase(DMConnections.BaseCAC),
                deDesdeFecha.Date,
                deHastaFecha.Date,
                iif(DatosClienteCertificado.Personeria = PERSONERIA_JURIDICA,DESC_TRATAMIENTO_EMPRESA,iif(DatosClienteCertificado.Sexo = SEXO_FEMENINO,DESC_TRATAMIENTO_DONA,DESC_TRATAMIENTO_DON)),
                iif(DatosClienteCertificado.Personeria = PERSONERIA_JURIDICA,DatosClienteCertificado.RazonSocial,ObtenerNombreCompletoClienteCertificado),
                DatosClienteCertificado.NumeroDocumento, // Rev. 2 (SS 719)
                iif(DatosClienteCertificado.Personeria = PERSONERIA_JURIDICA,DESC_RELACION_EMPRESA,iif(DatosClienteCertificado.Sexo = SEXO_FEMENINO,DESC_RELACION_DONA,DESC_RELACION_DON)),
                edPatente.Text,
                cldInfracciones, // Rev. 1 (SS 828)
                UsuarioSistema,
                DatosClienteCertificado.Personeria,
                DatosClienteCertificado.sexo,
                //cbConcesionarias.Value) then begin    // SS_660D_CQU_20140806
                cbConcesionarias.Value,                 // SS_660D_CQU_20140806
                FTraerListaAmarilla) then begin         // SS_660D_CQU_20140806
            try
                cldInfracciones.DisableControls;

                f.ImprimirCertificado;
            finally
                cldInfracciones.EnableControls;
                cldInfracciones.First;
            end;
        end
        else MsgBox(DescError, Self.Caption, MB_ICONSTOP);

    finally
        if assigned(f) then f.Release;
    End;
end;

procedure TEmitirCertificadoInfraccionesForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TEmitirCertificadoInfraccionesForm.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TEmitirCertificadoInfraccionesForm.LimpiarDatosCertificado;
begin
    peRUTCliente.Text				:= '';
    lblDatoApellidoNombre.Caption	:= '';
    peRUTCliente.Enabled			:= SePuedeEmitirCertificado;
    btnBuscarCliente.Enabled		:= SePuedeEmitirCertificado;
    btnImprimirCertificado.Enabled	:= False;
    //REV.1
    DatosClienteCertificado.CodigoPersona := -1;
end;

{******************************* Procedure Header ******************************
Procedure Name: MuestraDatosClienteCertificado
Author :
Date Created :
Parameters : None
Description :

Revision : 1
    Author : pdominguez
    Date   : 26/11/2009
    Description : SS 719 - Defects 13718 y 13719
        - Defect 13718:
            Se limpia el RUT y datos asociados, y se pasa el foco al
        objeto peRUTCliente, cuando se responde que no se desea dar de alta un
        convenio para el RUT introducido.
        - Defect 13719:
            Se limpia el RUT y datos asociados, y se pasa el foco al
        objeto peRUTCliente, cuando se responde que si se desea dar de alta un
        convenio para el RUT introducido, pero no se llega a dar el alta.
            Se habilita y pasa el foco al bot�n de Imprimir Certificado, cuando
        se responde que si se desea dar de alta un convenio para el RUT introducido,
        y se efect�a el alta.
*******************************************************************************}
procedure TEmitirCertificadoInfraccionesForm.MuestraDatosClienteCertificado;
resourcestring
	MSG_NO_TIENE_CONVENIO_ASOCIADO = 'El Cliente seleccionado NO tiene convenio asociado.' + CRLF + CRLF + '� Desea crear un convenio para este cliente ?';
    CAPTION_ALTA_CONTRATO          = 'Ingresar Contrato';

var
    f: TFormSolicitudContacto;
    FuenteSolicitud: Integer;
    DatosTurno: TDatosTurno;
    //DatosDomicilio: TDatosDomicilio;                                          //SS_1147_MCA_20140408
    Preguntar : boolean;
begin
	//REV.1
    Preguntar := False;
    lblDatoApellidoNombre.Caption := '';
    btnImprimirCertificado.Enabled := False;

	if DatosClienteCertificado.CodigoPersona > 0 then begin
    	if DatosClienteCertificado.Personeria = PERSONERIA_FISICA then begin
			lblDatoApellidoNombre.Caption :=
					ArmaNombreCompleto(
							Trim(DatosClienteCertificado.Apellido),
							Trim(DatosClienteCertificado.ApellidoMaterno),
                			Trim(DatosClienteCertificado.Nombre));
        end
    	else lblDatoApellidoNombre.Caption := DatosClienteCertificado.RazonSocial;

        //REV.1 if QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT DBO.TieneConvenioActivo(''%s'',''%s'')',[DatosClienteCertificado.NumeroDocumento,DatosClienteCertificado.TipoDocumento])) = 1 then begin
        spObtenerConveniosCliente.Close;
        with spObtenerConveniosCliente do begin
            Parameters.ParamByName('@CodigoCliente').Value := DatosClienteCertificado.CodigoPersona;
            Parameters.ParamByName('@IncluirConveniosDeBaja').Value := True;
            Open;
        	if not IsEmpty then begin
                // Rev. 1 (SS 719 - Defect 13719)
                btnImprimirCertificado.Enabled := True;
                btnImprimirCertificado.SetFocus;
                // Fin Rev. 1 (SS 719 - Defect 13719)
            end
        	else Preguntar := True;
        end;
        spObtenerConveniosCliente.Close;
        
    end
    else Preguntar := True;

    if Preguntar then
        if (MsgBox(MSG_NO_TIENE_CONVENIO_ASOCIADO, Self.Caption, MB_YESNO + MB_ICONQUESTION) = ID_YES) then begin
        	if not VerTurnoAbierto(DMConnections.BaseCAC, MainForm.PuntoEntrega, MainForm.PuntoVenta, UsuarioSistema, DatosTurno) then begin
            	if (DatosTurno.NumeroTurno < 1) or (DatosTurno.Estado = TURNO_ESTADO_CERRADO) then begin
                	GNumeroTurno := -1;
                end;

                if (DatosTurno.NumeroTurno <> -1) and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                		and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then //Si es distinto es que en el mismo POS y PV hay un turno abierto por otro usuario
                    MsgBox(Format(MSG_CAPTION_TURNO_ABIERTO_OTRO_USUARIO,[DatosTurno.CodigoUsuario]), Caption, MB_ICONWARNING)
                else MsgBox(MSG_ERROR_TURNO_ABRIR, CAPTION_ALTA_CONTRATO, MB_ICONSTOP);

                Exit;
            end;

            if FindFormOrCreate(TFormSolicitudContacto, f) then	f.Show
            else begin
                //{ INICIO : 20160315 MGO
            	FuenteSolicitud := ApplicationIni.ReadInteger('General', 'FuenteSolicitud', FUENTE_CALLCENTER);
                {
                FuenteSolicitud := InstallIni.ReadInteger('General', 'FuenteSolicitud', FUENTE_CALLCENTER);
                }// FIN : 20160315 MGO
                //REV.1 if not f.InicializaConvenioConValores(DatosClienteCertificado,DatosDomicilio,True, CAPTION_ALTA_CONTRATO, MainForm.PuntoEntrega, MainForm.PuntoVenta, -1, scAlta, -1, INTERMEDIO, FuenteSolicitud) then f.Release;
                if not f.Inicializa(False, 'Ingresar Contrato',
                						MainForm.PuntoEntrega,
                                        MainForm.PuntoVenta, -1, scAlta, -1, INTERMEDIO,
                                        FuenteSolicitud, True, True) then begin
                	f.Release;
                end
                else begin
                	f.FreDatoPersona.txtDocumento.Text := peRUTCliente.Text;
                    f.IniciarCargaConRut;
                    f.ShowModal;
                end;
            end;

            // Rev. 1 (SS 719 - Defect 13719)
            with spObtenerConveniosCliente do begin
                if Active then Close;
                Parameters.ParamByName('@CodigoCliente').Value := DatosClienteCertificado.CodigoPersona;
                Parameters.ParamByName('@IncluirConveniosDeBaja').Value := True;
                Open;
        	    if not IsEmpty then begin
                    btnImprimirCertificado.Enabled := True;
                    btnImprimirCertificado.SetFocus;
                end
                else begin
                    peRUTCliente.Text := '';
                    lblDatoApellidoNombre.Caption := '';
                    peRUTCliente.SetFocus;
                end;
            end;
            // Fin Rev. 1 (SS 719 - Defect 13719)

        end
        else begin
            // Rev. 1 (SS 719 - Defect 13718)
            peRUTCliente.Text := '';
            lblDatoApellidoNombre.Caption := '';
            peRUTCliente.SetFocus;
            // Fin Rev. 1 (SS 719 - Defect 13718)
        end;
end;

function TEmitirCertificadoInfraccionesForm.ObtenerNombreCompletoClienteCertificado: String;
begin
    Result :=
        Trim(
            Trim(DatosClienteCertificado.Nombre) + ' ' +
            Trim(DatosClienteCertificado.Apellido) + ' ' +
            Trim(DatosClienteCertificado.ApellidoMaterno));
end;

procedure TEmitirCertificadoInfraccionesForm.cbConcesionariasChange(
  Sender: TObject);
begin
    if ActiveControl = Sender then begin
		cldInfracciones.EmptyDataSet;
        dblInfracciones.Invalidate;
        SePuedeEmitirCertificado := False;
        LimpiarDatosCertificado;
    end;
end;

procedure TEmitirCertificadoInfraccionesForm.rgMorosidadClick(Sender: TObject); // SS_660D_CQU_20140806
begin                                                                           // SS_660D_CQU_20140806
    FTraerListaAmarilla := rgMorosidad.Buttons[1].Checked;                      // SS_660D_CQU_20140806
end;                                                                            // SS_660D_CQU_20140806

end.
