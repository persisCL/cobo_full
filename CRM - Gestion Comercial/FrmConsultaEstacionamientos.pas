{********************************** Unit Header ********************************
File Name       : FrmConsultaEstacionamientos.pas
Author          : CQuezadaI
Date Created    : 07/10/2012
Language        : ES-CL
Firma           : SS_1006_CQU_20121007
Description     : El objetivo es mostrar una serie de estacionamientos pendientes y poder,
                  adem�s, ver sus datos.
                  Se utiliza como base el formulario FrmConsultaTransitos.pas
                  Se realizan las correcciones indicadas en el proceso de testing.
*******************************************************************************}
unit FrmConsultaEstacionamientos;

interface

uses
  DmConnection, UtilProc, peaprocs, util, UtilDB, Peatypes, Windows, Messages,
  SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  DPSControls, ExtCtrls, ListBoxEx, DBListEx, DB, ADODB, DmiCtrls, VariantComboBox,
  Validate, DateEdit,Abm_obj,RStrings,StrUtils, ImgList, TimeEdit;

type
  TFormConsultaEstacionamientos = class(TForm)
    pnlFiltro: TPanel;
    grpFiltros: TGroupBox;
    pnlGrilla: TPanel;
    dblEstacionamientos: TDBListEx;
    ObtenerEstacionamientosFiltro: TADOStoredProc;
    Panel3: TPanel;
    pnlBotonera: TPanel;
    lblFechaDesde: TLabel;
	txt_FechaDesde: TDateEdit;
    txt_FechaHasta: TDateEdit;
    lblFechaHasta: TLabel;
    lblPatente: TLabel;
	edPatente: TEdit;
    dsEstacionamientos: TDataSource;
    lblTotales: TLabel;
    lnCheck: TImageList;
    btn_Filtrar: TButton;
    btn_Limpiar: TButton;
    BtnSalir: TButton;
    lblTelevia: TLabel;
    txtTelevia: TEdit;
    lblEstado: TLabel;
    HoraDesde: TTimeEdit;
    HoraHasta: TTimeEdit;
    lblEntrada: TLabel;
    cbEstado: TVariantComboBox;
    cbEntrada: TVariantComboBox;
    cbConcesionaria: TVariantComboBox;
    lblConcesionaria: TLabel;
    cbSalida: TVariantComboBox;
    lblSalida: TLabel;

    procedure FormShow(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure dblEstacionamientosColumns0HeaderClick(Sender: TObject);
    procedure dblEstacionamientosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure BtnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtTeleviaKeyPress(Sender: TObject; var Key: Char);
    procedure cbConcesionariaChange(Sender: TObject);
  private
    { Private declarations }
    CodigoConcesionaria, CodigoPAK : Integer;
    Cargando : Boolean;
    procedure CargarComboEstados(EstadoActual: Integer = 0);
    procedure CargarConcesionariasCombo(ConcesionariaActual: Integer = 0);
    procedure CargarComboEntrada(EntradaSeleccionada: Integer = 0);
    procedure CargarComboSalida(SalidaSeleccionada: Integer = 0);
  public
	  { Public declarations }
  	function Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
  end;

var
  FormConsultaEstacionamientos: TFormConsultaEstacionamientos;

implementation

uses frmVisorImagenes;

{$R *.dfm}

{******************************** Function Header ******************************
Function Name   : CargarComboEstados
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     : Carga los posibles estados de un Estacionamiento en un combo.
Parameters      : EstadoActual: Integer = 0
Return Value    : None
*******************************************************************************}
procedure TFormConsultaEstacionamientos.CargarComboEstados(EstadoActual: Integer = 0);
resourceString
    MSG_CUALQUIER_ESTADO = 'Todos';
var
    Qry: TADOQuery;
begin
    Qry := TAdoQuery.Create(nil);
    try
        Qry.Connection := DmConnections.BaseCAC;
        Qry.SQL.Text := 'SELECT Estado, Descripcion FROM EstadosEstacionamientos WITH (NOLOCK) ORDER BY Descripcion';
        Qry.Open;
        CbEstado.Clear;
        CbEstado.Items.Add(MSG_CUALQUIER_ESTADO, 0);
        While not Qry.Eof do begin
            CbEstado.Items.Add(Qry.FieldByName('Descripcion').AsString,
                Istr(Qry.FieldByName('Estado').AsInteger));
            Qry.Next;
		end;
		CbEstado.ItemIndex := 0;
    finally
        Qry.Close;
        Qry.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name   : CargarConcesionariasCombo
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     : Carga las Concesionarias en un combo para poder seleccionarlas
Parameters      : ConcesionariaActual: Integer = 0
Return Value    : None

*******************************************************************************}
procedure TFormConsultaEstacionamientos.CargarConcesionariasCombo(ConcesionariaActual: Integer = 0);
resourceString
    MSG_CUALQUIER_CATEGORIA = 'Todas';
begin
    CargarComboConcesionarias(DMConnections.BaseCAC, cbConcesionaria, False);
    if cbConcesionaria.Items.Count > 0 then begin
        cbConcesionaria.Items.Add('Todas las Concesionarias', -1);
        // Por defecto es Costanera, luego que se agreguen m�s se deber�a cambiar las l�neas.
        //cbConcesionaria.ItemIndex := IIf(ConcesionariaActual > 0, ConcesionariaActual, cbConcesionaria.Items.IndexOfValue(-1));
        cbConcesionaria.ItemIndex := cbConcesionaria.Items.IndexOfValue(CodigoPAK);
    end else cbConcesionaria.ItemIndex := cbConcesionaria.Items.IndexOfValue(-1);
end;

{-----------------------------------------------------------------------------
  Function Name : CargarComboEntrada
  Author        : CQuezadaI
  Date Created  : 07/10/2012
  Description   : Obtiene las Entradas de Estacionamiento
  Parameters    : EntradaSeleccionada: Integer = 0
  Return Value  : N/A
-----------------------------------------------------------------------------}
procedure TFormConsultaEstacionamientos.CargarComboEntrada(EntradaSeleccionada: Integer = 0);
resourceString
    MSG_CUALQUIER_PORTICO   = 'Todas';
    ERROR_OBT_ENTRADAS      = 'Error al cargar las entradas de estacionamientos';
var
    spObtenerEntradasEstacionamiento : TADOStoredProc;
begin
    spObtenerEntradasEstacionamiento := TADOStoredProc.Create(nil);
	try
        try
            cbEntrada.Clear;
            cbEntrada.Items.Add(MSG_CUALQUIER_PORTICO, 0);
            with spObtenerEntradasEstacionamiento do begin
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'ObtenerEntradasEstacionamientos';
                CommandTimeout := 30;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;
                Open;
                While not Eof do begin
                    cbEntrada.Items.Add(FieldByName('NombreCorto').AsString,
                        FieldByName('CodigoEntradaEstacionamiento').AsInteger);
                    Next;
                end;
            end;
        except
            on e: exception do begin
                MsgBoxErr(ERROR_OBT_ENTRADAS, e.message, caption, MB_ICONSTOP);
            end;
        end;
  	finally
	  	spObtenerEntradasEstacionamiento.Close;
        spObtenerEntradasEstacionamiento.Free;

        if (cbEntrada.Items.Count > 0) and (EntradaSeleccionada > 0) then
            cbEntrada.ItemIndex := EntradaSeleccionada
        else cbEntrada.ItemIndex := 0;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : CargarComboSalida
  Author        : CQuezadaI
  Date Created  : 07/10/2012
  Description   : Obtiene las Salidas de Estacionamiento
  Parameters    : SalidaSeleccionada: Integer = 0
  Return Value  : N/A
-----------------------------------------------------------------------------}
procedure TFormConsultaEstacionamientos.CargarComboSalida(SalidaSeleccionada: Integer = 0);
resourceString
    MSG_CUALQUIER_PORTICO = 'Todas';
    ERROR_OBT_SALIDAS     = 'Error al cargar las salidas de estacionamientos';
var
    spObtenerSalidasEstacionamiento : TADOStoredProc;
begin
    spObtenerSalidasEstacionamiento := TADOStoredProc.Create(nil);
	try
        try
            spObtenerSalidasEstacionamiento.Connection := DMConnections.BaseCAC;
            spObtenerSalidasEstacionamiento.ProcedureName := 'ObtenerSalidasEstacionamientos';
            spObtenerSalidasEstacionamiento.CommandTimeout := 30;
            spObtenerSalidasEstacionamiento.Parameters.Refresh;
            spObtenerSalidasEstacionamiento.Parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;
            spObtenerSalidasEstacionamiento.Open;

            cbSalida.Clear;
            cbSalida.Items.Add(MSG_CUALQUIER_PORTICO, 0);
            with spObtenerSalidasEstacionamiento do begin
                While not Eof do begin
                    cbSalida.Items.Add(FieldByName('NombreCorto').AsString,
                        FieldByName('CodigoSalidaEstacionamiento').AsInteger);
                    spObtenerSalidasEstacionamiento.Next;
                end;
            end;
        except
            on e: exception do begin
                MsgBoxErr(ERROR_OBT_SALIDAS, e.message, caption, MB_ICONSTOP);
            end;
        end;
  	finally
	  	spObtenerSalidasEstacionamiento.Close;
        spObtenerSalidasEstacionamiento.Free;

        if (cbSalida.Items.Count > 0) and (SalidaSeleccionada > 0) then
            cbSalida.ItemIndex := SalidaSeleccionada
        else cbSalida.ItemIndex := 0;
    end;
end;

{******************************** Function Header ******************************
Function Name   : Inicializar
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     : Inicializar el formulario.
Parameters      : txtCaption: String; MDIChild: Boolean
Return Value    : Boolean
*******************************************************************************}
function TFormConsultaEstacionamientos.Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
    var S: TSize;
begin
    Result := True;
    Cargando := True;
    try
        if MDIChild then begin
            S := GetFormClientSize(Application.MainForm);
            SetBounds(0, 0, S.cx, S.cy);
        end else begin
            FormStyle := fsNormal;
            Visible := False;
        end;
        CenterForm(Self);
        Caption := AnsiReplaceStr(txtCaption, '&', '');
        txt_FechaHasta.Date := Now;
        // Por ahora la concesionaria ser� por defecto PAK
        CodigoPAK := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO()');
        btn_Limpiar.Click;
        CargarComboEstados(0);
        CargarConcesionariasCombo(0);
        CargarComboEntrada(0);
        CargarComboSalida(0);
        cbConcesionaria.Enabled := False;
    except
        result := False;
    end;
    lblTotales.Caption := '';
    Cargando := False;
    // Coloco la Concesionaria Parque Arauco ya que por ahora ser� s�lo esa,
    // esta linea deber�a desaparecer cuando se habilite para todas las concesionarias
    cbConcesionariaChange(nil);
end;

{******************************** Function Header ******************************
Function Name   : FormShow
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     :
Parameters      : Sender: TObject
Return Value    : None
*******************************************************************************}
procedure TFormConsultaEstacionamientos.FormShow(Sender: TObject);
begin
    edPatente.SetFocus;
end;

{******************************** Function Header ******************************
Function Name   : btn_FiltrarClick
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     :
Parameters      : Sender: TObject
Return Value    : None
*******************************************************************************}
procedure TFormConsultaEstacionamientos.btn_FiltrarClick(Sender: TObject);
resourcestring
    MSG_LICENSE_PLATE		    = 'Debe ingresar un valor de patente';
    MSG_ERROR_LOADING_TRANSITS	= 'Error cargando Estacionamientos';
    MSG_FILTER                  = 'Debe ingresar o la patente o el telev�a o una fecha desde y hasta que permita filtrar los Estacionamientos';
    MSG_TELEVIA                 = 'El n�mero de Telev�a no es correcto';
    MSG_VALIDAR_RANGO_DIAS      = 'Si no filtra por patente o telev�a, la diferencia entre fechas puede ser de %d d�as/s';
    MSG_BUSQUEDA_VACIA          = 'No se encontr� ning�n registro con esos par�metros de b�squeda';
    MSG_TOTAL_VISUALIZADOS      = 'Visualizando %d estacionamientos';
    MSG_PATENTE                 = 'La Patente ingresada no es correcta';
    MSG_FECHA_INVALIDA          = 'La fecha ingresada no es v�lida para el sistema';
begin
    ObtenerEstacionamientosFiltro.Close;
    
    if not validatecontrols([edPatente],
        [(Trim(edPatente.Text) <> '')
            or (Trim(txtTelevia.Text) <> '')
            or((txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate))
        ],
        caption,
        [MSG_FILTER]) then exit;

    // Verifico que la Patente cumpla con el estandar nacional
    if not (edPatente.Text = '')
        and not validatecontrols([edPatente],
                [VerificarFormatoPatenteChilena(DMConnections.BaseCAC, Trim(edPatente.Text))],
                Caption,
                [MSG_PATENTE]) then Exit;

    if not ValidateControls(
        [txt_FechaDesde, txt_FechaDesde, txt_FechaHasta],
        [not ((txt_FechaDesde.Date <> nulldate) and (txt_FechaHasta.Date <> nulldate) and (txt_FechaDesde.Date > txt_FechaHasta.Date)),
        not ((txt_FechaDesde.Date <> nulldate) and (txt_FechaDesde.Date <= 2)),
        not ((txt_FechaHasta.Date <> nulldate) and (txt_FechaHasta.Date <= 2))],
        Caption,
        [MSG_VALIDAR_ORDEN_FECHA, MSG_FECHA_INVALIDA, MSG_FECHA_INVALIDA]
    ) then Exit;

    // Verifica que el n�mero de televia este compuesto solamente de n�meros y tenga mas de 10 digitos.
    if (txtTelevia.Text <> '') and (
            (Length(Trim(TxtTelevia.Text)) < 10) or (not PermitirSoloDigitos(txtTelevia.Text)) or
            (txtTelevia.Text <> SerialNumberToEtiqueta(EtiquetaToSerialNumber(txtTelevia.Text)))
        ) then  begin

        MsgBoxBalloon(MSG_TELEVIA, Caption, MB_ICONSTOP, txtTelevia);
        Exit;
    end;

//    ObtenerEstacionamientosFiltro.DisableControls;
    try
        try
            if ObtenerEstacionamientosFiltro.Active then ObtenerEstacionamientosFiltro.close;
            ObtenerEstacionamientosFiltro.CommandTimeout := 500;
            ObtenerEstacionamientosFiltro.Parameters.Refresh;

            with ObtenerEstacionamientosFiltro do begin
                Parameters.ParamByName('@Patente').Value                 := IIf(edPatente.Text = EmptyStr, Null, edpatente.text);
                Parameters.ParamByName('@ContractSerialNumber').Value    := IIf(txtTelevia.Text = EmptyStr, Null, EtiquetaToSerialNumber(txtTelevia.Text));
                Parameters.ParamByName('@FechaDesde').Value              := IIf(txt_FechaDesde.Date = nulldate, Null, FormatDateTime('yyyymmdd hh:nn',(txt_FechaDesde.Date + HoraDesde.Time)));
                Parameters.ParamByName('@FechaHasta').Value              := IIf(txt_FechaHasta.Date = nulldate, Null, FormatDateTime('yyyymmdd hh:nn',(txt_FechaHasta.Date + HoraHasta.Time)));
                Parameters.ParamByName('@CodigoConcesionaria').Value     := IIf(CodigoConcesionaria = 0, Null, CodigoConcesionaria);
                Parameters.ParamByName('@Estado').Value                  := IIf(CbEstado.Value = 0, Null, CbEstado.Value);
                Parameters.ParamByName('@Entrada').Value                 := IIf(cbEntrada.Value = 0, Null, cbEntrada.Value);
                Parameters.ParamByName('@Salida').Value                  := IIf(cbSalida.Value = 0, null, cbSalida.Value);
//                Parameters.ParamByName('@CantidadTransitos').Value       := 0;
                Open;
            end;

            if ObtenerEstacionamientosFiltro.IsEmpty then begin
                MsgBox(MSG_BUSQUEDA_VACIA, Caption, MB_ICONINFORMATION);
                lblTotales.Caption := EmptyStr;
            end else
                lblTotales.Caption := Format(MSG_TOTAL_VISUALIZADOS, [ObtenerEstacionamientosFiltro.RecordCount]);
        except
            on e: exception do begin
                MsgBoxErr('Error', e.message, caption, MB_ICONSTOP);
            end;
        end;
    finally
//	    ObtenerEstacionamientosFiltro.EnableControls;
    end;
    dblEstacionamientos.SetFocus;
end;

{******************************** Function Header ******************************
Function Name   : btn_LimpiarClick
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     : Limpia los datos del formulario
Parameters      : Sender: TObject
Return Value    : None
*******************************************************************************}
procedure TFormConsultaEstacionamientos.btn_LimpiarClick(Sender: TObject);
begin
    if not Cargando then begin
        edPatente.Clear;
        txtTelevia.Clear;
        txt_FechaDesde.Clear;
        txt_FechaHasta.Clear;
        HoraDesde.Time := 0;
        HoraHasta.Time := 0;
        //Por ahora la concesionaria es ParqueArauco por lo cual no se limpia
        //cbConcesionaria.ItemIndex := cbConcesionaria.Items.IndexOfValue(-1);
        CargarComboEstados;
        CargarComboEntrada;
        CargarComboSalida;
        ObtenerEstacionamientosFiltro.close;
        edPatente.SetFocus;
    end;
end;

{******************************** Function Header ******************************
Function Name   : dblEstacionamientosColumns0HeaderClick
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     :
Parameters      : Sender: TObject
Return Value    : None
*******************************************************************************}
procedure TFormConsultaEstacionamientos.dblEstacionamientosColumns0HeaderClick(Sender: TObject);
begin
	  if ObtenerEstacionamientosFiltro.IsEmpty then Exit;

	  if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   	  else TDBListExColumn(sender).Sorting := csAscending;

	  ObtenerEstacionamientosFiltro.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

{******************************** Function Header ******************************
Function Name   : txtTeleviaKeyPress
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     :
Parameters      : Sender: TObject; var Key: Char
Return Value    : None
*******************************************************************************}
procedure TFormConsultaEstacionamientos.txtTeleviaKeyPress(Sender: TObject; var Key: Char);
begin
    // #08: Backspace.
    if (not (Key in ['0'..'9', #08])) then Exit;
end;

{******************************** Function Header ******************************
Function Name   : dblEstacionamientosDrawText
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     :
Parameters      : Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
                  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
                  var ItemWidth: Integer; var DefaultDraw: Boolean
Return Value    : None
*******************************************************************************}
procedure TFormConsultaEstacionamientos.dblEstacionamientosDrawText(Sender: TCustomDBListEx;
Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
var ItemWidth: Integer; var DefaultDraw: Boolean);
var
	i: Integer;
begin
    // Revision 1
    // Armo las imagenes en las celdas de la grilla donde se muestra un campo boolean

    //BlackList
    if Column.FieldName = 'BlackList' then begin
          if (ObtenerEstacionamientosFiltro.FieldByName('BlackList').Value = 0) then i := 0
          else i := 1;
      lnCheck.Draw(dblEstacionamientos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
      DefaultDraw := False;
      ItemWidth   := lnCheck.Width + 2;
    end;
    // YellowList
    if Column.FieldName = 'YellowList' then begin
        if (ObtenerEstacionamientosFiltro.FieldByName('YellowList').Value = 0) then i := 0
        else i := 1;
      lnCheck.Draw(dblEstacionamientos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
      DefaultDraw := False;
      ItemWidth   := lnCheck.Width + 2;
    end;
    //GreenList
    if Column.FieldName = 'GreenList' then begin
        if (ObtenerEstacionamientosFiltro.FieldByName('GreenList').Value = 0) then i := 0
        else i := 1;
      lnCheck.Draw(dblEstacionamientos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
      DefaultDraw := False;
      ItemWidth   := lnCheck.Width + 2;
    end;
    //GrayList
    if Column.FieldName = 'GrayList' then begin
        if(ObtenerEstacionamientosFiltro.FieldByName('GrayList').value = 0) then i := 0
        else i := 1;
      lnCheck.Draw(dblEstacionamientos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
      DefaultDraw := False;
      ItemWidth   := lnCheck.Width + 2;
    end;
    //ExceptionHandling
    if Column.FieldName = 'ExceptionHandling' then begin
        if(ObtenerEstacionamientosFiltro.FieldByName('ExceptionHandling').value = 0) then i := 0
        else i := 1;
        lnCheck.Draw(dblEstacionamientos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
        DefaultDraw := False;
        ItemWidth   := lnCheck.Width + 2;
    end;
    //ExceptionHandling
    if Column.FieldName = 'MMIContactOperator' then begin
        if(ObtenerEstacionamientosFiltro.FieldByName('MMIContactOperator').value = 0) then i := 0
        else i := 1;
        lnCheck.Draw(dblEstacionamientos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
        DefaultDraw := False;
        ItemWidth   := lnCheck.Width + 2;
    end;

    if column.FieldName = 'FechaHoraEntrada' then begin
        Text := FormatDateTime('dd/mm/yyyy hh:mm', ObtenerEstacionamientosFiltro.FieldByName('FechaHoraEntrada').AsDateTime)
    end;

     if column.FieldName = 'FechaHoraSalida' then begin
        Text := FormatDateTime('dd/mm/yyyy hh:mm', ObtenerEstacionamientosFiltro.FieldByName('FechaHoraSalida').AsDateTime)
    end;
end;

{******************************** Function Header ******************************
Function Name   : BtnSalirClick
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     :
Parameters      : Sender: TObject
Return Value    : None
*******************************************************************************}
procedure TFormConsultaEstacionamientos.BtnSalirClick(Sender: TObject);
begin
	ObtenerEstacionamientosFiltro.Close;
	Close;
end;

{******************************** Function Header ******************************
Function Name   : FormClose
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     :
Parameters      : Sender: TObject; var Action: TCloseAction
Return Value    : None
*******************************************************************************}
procedure TFormConsultaEstacionamientos.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
    CerrarVentanaImagen(Self);
    action := caFree;
end;

{******************************** Function Header ******************************
Function Name   : cbConcesionariaChange
Author          : CQuezadaI
Date Created    : 07/10/2012
Description     : Obtiene el Codigo de la Concesionaria Seleccionada
Parameters      : Sender: TObject;
Return Value    : None
*******************************************************************************}
procedure TFormConsultaEstacionamientos.cbConcesionariaChange(Sender: TObject);
begin
    if not Cargando and (cbConcesionaria.ItemIndex > 0) then begin
        CodigoConcesionaria := cbConcesionaria.Items[cbConcesionaria.ItemIndex].Value;
        CargarComboEntrada(0);
        CargarComboSalida(0);
    end;
end;

end.
