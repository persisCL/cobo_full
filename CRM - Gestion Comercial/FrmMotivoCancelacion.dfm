object FormMotivoCancelacion: TFormMotivoCancelacion
  Left = 186
  Top = 181
  BorderStyle = bsDialog
  Caption = 'Motivo de Cancelaci'#243'n'
  ClientHeight = 197
  ClientWidth = 568
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    568
    197)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 6
    Top = 8
    Width = 556
    Height = 153
  end
  object Label1: TLabel
    Left = 16
    Top = 45
    Width = 74
    Height = 13
    Caption = '&Observaciones:'
    FocusControl = txtObservaciones
  end
  object Label2: TLabel
    Left = 16
    Top = 22
    Width = 35
    Height = 13
    Caption = '&Motivo:'
    FocusControl = cbMotivosCancelacion
  end
  object txtObservaciones: TMemo
    Left = 16
    Top = 61
    Width = 537
    Height = 88
    MaxLength = 255
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object cbMotivosCancelacion: TVariantComboBox
    Left = 67
    Top = 18
    Width = 305
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items = <>
  end
  object btn_Aceptar: TButton
    Left = 470
    Top = 167
    Width = 86
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Terminar'
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = btn_AceptarClick
  end
end
