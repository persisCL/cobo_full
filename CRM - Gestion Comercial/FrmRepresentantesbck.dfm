object FormRepresentantes: TFormRepresentantes
  Left = 101
  Top = 159
  BorderStyle = bsDialog
  Caption = 'Representantes'
  ClientHeight = 464
  ClientWidth = 861
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbRepresentanteUno: TGroupBox
    Left = 3
    Top = 0
    Width = 855
    Height = 208
    Caption = 'Representante:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    inline FreRepresentanteUno: TFreDatoPresona
      Left = 15
      Top = 16
      Width = 803
      Height = 89
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      inherited Pc_Datos: TPageControl
        Width = 803
        Height = 63
        inherited Tab_Fisica: TTabSheet
          inherited Label4: TLabel
            Top = 6
          end
          inherited Label6: TLabel
            Top = 6
          end
          inherited Label7: TLabel
            Top = 34
          end
          inherited Label1: TLabel
            Left = 258
            Top = 34
            ParentFont = False
          end
          inherited Label3: TLabel
            Top = 6
          end
          inherited txt_ApellidoMaterno: TEdit
            Top = 1
            ParentFont = False
          end
          inherited txt_Apellido: TEdit
            Top = 1
            ParentFont = False
          end
          inherited cbSexo: TComboBox
            Top = 29
            ParentFont = False
          end
          inherited txtFechaNacimiento: TDateEdit
            Left = 333
            Top = 29
            ParentFont = False
          end
          inherited txt_Nombre: TEdit
            Top = 1
            ParentFont = False
          end
        end
      end
      inherited Panel1: TPanel
        Width = 803
        inherited Label53: TLabel
          Left = 232
          Visible = False
        end
        inherited lblRutRun: TLabel
          Left = 7
        end
        inherited lblRazonSocial: TLabel
          Visible = False
        end
        inherited cbPersoneria: TComboBox
          Left = 340
          ParentFont = False
          Visible = False
        end
        inherited txtDocumento: TEdit
          Left = 112
          ParentFont = False
          OnChange = FreRepresentanteUnotxtDocumentoChange
          OnExit = FreRepresentanteUnotxtDocumentoExit
        end
        inherited txtRazonSocial: TEdit
          Visible = False
        end
      end
    end
    object GBDomicilios: TGroupBox
      Left = 6
      Top = 102
      Width = 844
      Height = 100
      Caption = 'Domicilio '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      inline freDomicilioRepresentanteUno: TFrameDomicilio
        Left = 2
        Top = 11
        Width = 836
        Height = 75
        HorzScrollBar.Visible = False
        VertScrollBar.Visible = False
        TabOrder = 0
        inherited Pnl_Arriba: TPanel
          Width = 836
          inherited txt_DescripcionCiudad: TEdit
            MaxLength = 50
          end
        end
        inherited Pnl_Abajo: TPanel
          Width = 836
        end
        inherited Pnl_Medio: TPanel
          Width = 836
        end
      end
    end
  end
  object dbRepresentanteDos: TGroupBox
    Left = 3
    Top = 208
    Width = 854
    Height = 209
    Caption = 'Segundo Representante (Opcional)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object btnBorrar: TSpeedButton
      Left = 819
      Top = 18
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
        3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
        03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
        33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
        0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
        3333333337FFF7F3333333333000003333333333377777333333}
      NumGlyphs = 2
      OnClick = btnBorrarClick
    end
    object GroupBox1: TGroupBox
      Left = 6
      Top = 103
      Width = 844
      Height = 100
      Caption = 'Domicilio '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      inline freDomicilioRepresentanteDos: TFrameDomicilio
        Left = 2
        Top = 11
        Width = 836
        Height = 75
        HorzScrollBar.Visible = False
        VertScrollBar.Visible = False
        TabOrder = 0
        inherited Pnl_Arriba: TPanel
          Width = 836
          inherited txt_DescripcionCiudad: TEdit
            MaxLength = 50
          end
        end
        inherited Pnl_Abajo: TPanel
          Width = 836
        end
        inherited Pnl_Medio: TPanel
          Width = 836
        end
      end
    end
    inline FreRepresentanteDos: TFreDatoPresona
      Left = 15
      Top = 16
      Width = 803
      Height = 89
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      inherited Pc_Datos: TPageControl
        Width = 803
        Height = 63
        inherited Tab_Fisica: TTabSheet
          inherited Label4: TLabel
            Top = 6
          end
          inherited Label6: TLabel
            Top = 6
          end
          inherited Label7: TLabel
            Top = 34
          end
          inherited Label1: TLabel
            Left = 258
            Top = 34
            ParentFont = False
          end
          inherited Label3: TLabel
            Top = 6
          end
          inherited txt_ApellidoMaterno: TEdit
            Top = 1
            ParentFont = False
          end
          inherited txt_Apellido: TEdit
            Top = 1
            ParentFont = False
          end
          inherited cbSexo: TComboBox
            Top = 29
            ParentFont = False
          end
          inherited txtFechaNacimiento: TDateEdit
            Left = 333
            Top = 29
            ParentFont = False
          end
          inherited txt_Nombre: TEdit
            Top = 1
            ParentFont = False
          end
        end
      end
      inherited Panel1: TPanel
        Width = 803
        inherited Label53: TLabel
          Left = 232
          Visible = False
        end
        inherited lblRutRun: TLabel
          Left = 7
        end
        inherited lblRazonSocial: TLabel
          Visible = False
        end
        inherited cbPersoneria: TComboBox
          Left = 340
          ParentFont = False
          Visible = False
        end
        inherited txtDocumento: TEdit
          Left = 112
          ParentFont = False
          OnChange = FreRepresentanteDostxtDocumentoChange
          OnExit = FreRepresentanteDostxtDocumentoExit
        end
        inherited txtRazonSocial: TEdit
          Visible = False
        end
      end
    end
  end
  object Panel: TPanel
    Left = 0
    Top = 416
    Width = 861
    Height = 48
    TabOrder = 2
    object btnAceptar: TDPSButton
      Left = 639
      Top = 12
      Width = 95
      Hint = 'Guardar Datos'
      Caption = '&Aceptar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnSalir: TDPSButton
      Left = 744
      Top = 12
      Width = 92
      Hint = 'Salir de la Carga'
      Cancel = True
      Caption = '&Salir'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSalirClick
    end
  end
  object ObtenerDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 808
    Top = 24
  end
  object ObtenerDomiciliosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomicilioPrincipalPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 808
    Top = 56
  end
end
