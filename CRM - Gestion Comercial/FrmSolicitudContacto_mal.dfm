object FormSolicitudContacto: TFormSolicitudContacto
  Left = 116
  Top = 70
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  Caption = 'Alta de Solicitud'
  ClientHeight = 550
  ClientWidth = 1016
  Color = 16776699
  Constraints.MinHeight = 577
  Constraints.MinWidth = 950
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    1016
    550)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = -1
    Width = 1016
    Height = 545
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      1016
      545)
    object pnlDatosContacto: TPanel
      Left = 0
      Top = -6
      Width = 1016
      Height = 423
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        1016
        423)
      object GBMediosComunicacion: TGroupBox
        Left = 6
        Top = 100
        Width = 1008
        Height = 70
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Medios de comunicaci'#243'n'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        DesignSize = (
          1008
          70)
        object lblEmail: TLabel
          Left = 426
          Top = 46
          Width = 28
          Height = 13
          Caption = 'Email:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lblEnviar: TLabel
          Left = 968
          Top = 46
          Width = 30
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Enviar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          OnClick = lblEnviarClick
          OnMouseEnter = lblEnviarMouseEnter
          OnMouseLeave = lblEnviarMouseLeave
        end
        inline FreTelefonoPrincipal: TFrameTelefono
          Left = 9
          Top = 14
          Width = 607
          Height = 26
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 9
          ExplicitTop = 14
          ExplicitWidth = 607
          ExplicitHeight = 26
        end
        inline FreTelefonoSecundario: TFrameTelefono
          Left = 9
          Top = 39
          Width = 416
          Height = 26
          HorzScrollBar.Visible = False
          VertScrollBar.Visible = False
          TabOrder = 1
          TabStop = True
          ExplicitLeft = 9
          ExplicitTop = 39
          ExplicitWidth = 416
          ExplicitHeight = 26
          inherited txtTelefono: TEdit
            OnExit = FreTelefonoSecundariotxtTelefonoExit
          end
        end
        object NBook_Email: TNotebook
          Left = 460
          Top = 41
          Width = 419
          Height = 24
          Anchors = [akLeft, akTop, akRight]
          PageIndex = 1
          TabOrder = 2
          object TPage
            Left = 0
            Top = 0
            Caption = 'PageSalir'
            object txtEMailContacto: TEdit
              Left = 3
              Top = 1
              Width = 350
              Height = 21
              Hint = 'Direcci'#243'n de e-mail'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 60
              ParentFont = False
              ParentShowHint = False
              PopupMenu = PopupMenu
              ShowHint = False
              TabOrder = 0
              Text = 'txtEMailContacto'
              OnChange = txtEMailContactoChange
            end
          end
          object TPage
            Left = 0
            Top = 0
            HelpContext = 1
            Caption = 'PageGuardar'
            object txtEmailParticular: TEdit
              Left = 3
              Top = 1
              Width = 494
              Height = 21
              Hint = 'Direcci'#243'n de e-mail'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 60
              ParentFont = False
              ParentShowHint = False
              PopupMenu = PopupMenu
              ShowHint = True
              TabOrder = 0
              OnChange = txtEMailContactoChange
            end
          end
        end
      end
      object GBDomicilios: TGroupBox
        Left = 6
        Top = 177
        Width = 924
        Height = 119
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Domicilio '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        DesignSize = (
          924
          119)
        object Label8: TLabel
          Left = 14
          Top = 96
          Width = 279
          Height = 13
          Caption = #191'Desea recibir la factura mensual en este mismo domicilio? '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbl_OtroDomicilio: TLabel
          Left = 558
          Top = 97
          Width = 360
          Height = 13
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'aca va la descripci'#243'n del Otro domicilio...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        inline FMDomicilioPrincipal: TFrameDomicilio
          Left = 2
          Top = 11
          Width = 919
          Height = 75
          HorzScrollBar.Visible = False
          VertScrollBar.Visible = False
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 2
          ExplicitTop = 11
          ExplicitWidth = 919
          ExplicitHeight = 75
          inherited Pnl_Arriba: TPanel
            Width = 919
            ExplicitWidth = 919
            DesignSize = (
              919
              52)
            inherited txt_DescripcionCiudad: TEdit
              MaxLength = 50
            end
            inherited txt_numeroCalle: TEdit
              OnExit = FMDomicilioPrincipaltxt_numeroCalleExit
            end
            inherited txt_CodigoPostal: TEdit
              Width = 170
              Anchors = [akLeft, akTop, akRight]
              OnKeyPress = FMDomicilioPrincipaltxt_CodigoPostalKeyPress
              ExplicitWidth = 170
            end
          end
          inherited Pnl_Abajo: TPanel
            Top = 47
            Width = 919
            Align = alBottom
            ExplicitTop = 47
            ExplicitWidth = 919
            DesignSize = (
              919
              28)
            inherited cb_Comunas: TComboBox
              Width = 345
              Anchors = [akLeft, akTop, akRight]
              ExplicitWidth = 345
            end
          end
          inherited Pnl_Medio: TPanel
            Top = 52
            Width = 13
            Align = alNone
            ExplicitTop = 52
            ExplicitWidth = 13
          end
        end
        object RBDomicilioEntrega: TRadioButton
          Left = 303
          Top = 95
          Width = 31
          Height = 17
          Hint = 
            'El domicilio ingresado es el de entrega de correspondencia (fact' +
            'uraci'#243'n)'
          Caption = 'Si'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          TabStop = True
          OnClick = RBDomicilioEntregaOtroClick
        end
        object RBDomicilioEntregaOtro: TRadioButton
          Left = 347
          Top = 95
          Width = 49
          Height = 17
          Hint = 
            'Indica que otro es el de entrega de correspondencia (ver o ingre' +
            'sar los datos con el bot'#243'n Editar Domicilio)'
          Caption = '&Otro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = RBDomicilioEntregaOtroClick
        end
        object BtnEditarDomicilioEntrega: TButton
          Left = 395
          Top = 93
          Width = 155
          Height = 20
          Hint = 'Editar domicilio de facturaci'#243'n'
          Caption = 'Editar &domicilio de facturaci'#243'n'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtnEditarDomicilioEntregaClick
        end
      end
      object GBOtrosDatos: TGroupBox
        Left = 6
        Top = 296
        Width = 924
        Height = 113
        Anchors = [akLeft, akTop, akRight]
        BiDiMode = bdLeftToRight
        Caption = 'Otros'
        Color = clBtnFace
        Ctl3D = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentBiDiMode = False
        ParentColor = False
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
        DesignSize = (
          924
          113)
        object lbl_PagoAutomatico: TLabel
          Left = 10
          Top = 18
          Width = 126
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = 'Forma de pago autom'#225'tico'
          Color = clBtnFace
          FocusControl = cb_PagoAutomatico
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
        end
        object lbl_DescripcionMedioPago: TLabel
          Left = 378
          Top = 17
          Width = 528
          Height = 13
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'aca va la descripci'#242'n del m.pago automatico'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object lblPreguntaFuenteOrigenContacto: TLabel
          Left = 9
          Top = 43
          Width = 275
          Height = 13
          Caption = 'Aca va la pregunta de la promo del Santander (de la base)'
          Color = clBtnFace
          FocusControl = cbFuentesOrigenContacto
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object lbl_POS: TLabel
          Left = 491
          Top = 67
          Width = 264
          Height = 13
          AutoSize = False
          Caption = 'Debe seleccionar un POS'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object lbl_Pregunta: TLabel
          Left = 6
          Top = 67
          Width = 251
          Height = 13
          AutoSize = False
          BiDiMode = bdLeftToRight
          Caption = #191'D'#243'nde quiere pasar a buscar su Telev'#237'a?'
          Color = clBtnFace
          FocusControl = cb_POS
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
        end
        object lbl_Ubicacion: TLabel
          Left = 8
          Top = 90
          Width = 145
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = 'Ubicaci'#243'n f'#237'sica de la carpeta:'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
        end
        object cb_PagoAutomatico: TVariantComboBox
          Left = 170
          Top = 12
          Width = 105
          Height = 21
          Hint = 'Indique si utiliza una forma de pago autom'#225'tica'
          Style = vcsDropDownList
          Color = 16444382
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnChange = cb_PagoAutomaticoChange
          Items = <>
        end
        object cbFuentesOrigenContacto: TComboBox
          Left = 295
          Top = 39
          Width = 57
          Height = 21
          Hint = 'Fuente de Solicitud'
          Style = csDropDownList
          BiDiMode = bdLeftToRight
          Color = 16444382
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentBiDiMode = False
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Items.Strings = (
            'F'#237'sica                          F'
            'Jur'#237'dica                       J')
        end
        object cb_POS: TVariantComboBox
          Left = 255
          Top = 63
          Width = 231
          Height = 21
          Hint = 'C'#243'digo de Area'
          Style = vcsDropDownList
          Color = 16444382
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnChange = cb_POSChange
          Items = <>
        end
        object txt_UbicacionFisicaCarpeta: TEdit
          Left = 169
          Top = 87
          Width = 192
          Height = 21
          BiDiMode = bdLeftToRight
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 15
          ParentBiDiMode = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
        end
        object cb_Observaciones: TCheckBox
          Left = 504
          Top = 90
          Width = 169
          Height = 17
          BiDiMode = bdLeftToRight
          Caption = '&Convenio con Observaciones'
          Color = clBtnFace
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          TabOrder = 6
          OnClick = cb_ObservacionesClick
        end
        object cb_RecInfoMail: TCheckBox
          Left = 375
          Top = 90
          Width = 117
          Height = 17
          Hint = #191'Quiere recibir Informaci'#243'n por email?'
          BiDiMode = bdLeftToRight
          Caption = 'Recibir Info por Mail'
          Color = clBtnFace
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
        end
        object btn_EditarPagoAutomatico: TButton
          Left = 281
          Top = 12
          Width = 80
          Height = 20
          Hint = 'Editar medio de pago autom'#225'tico'
          Caption = 'Editar &pago'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btn_EditarPagoAutomaticoClick
        end
        object btn_DocRequerido: TButton
          Left = 838
          Top = 65
          Width = 82
          Height = 20
          Anchors = [akRight, akBottom]
          Caption = '&Documentaci'#243'n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          OnClick = btn_DocRequeridoClick
        end
        object btn_MedioDoc: TButton
          Left = 838
          Top = 40
          Width = 82
          Height = 20
          Hint = 'Medio de env'#237'o de los documentos de cobro'
          Anchors = [akRight, akBottom]
          Caption = '&Medio Env'#237'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          OnClick = btn_MedioDocClick
        end
        object btn_VerObservaciones: TButton
          Left = 671
          Top = 88
          Width = 97
          Height = 20
          Caption = '&Ver Observaciones'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          OnClick = btn_VerObservacionesClick
        end
      end
      inline FreDatoPersona: TFreDatoPresona
        Left = 0
        Top = 19
        Width = 1008
        Height = 85
        Anchors = [akLeft, akTop, akRight]
        Constraints.MinWidth = 900
        TabOrder = 0
        TabStop = True
        ExplicitTop = 19
        ExplicitWidth = 1008
        ExplicitHeight = 85
        inherited Pc_Datos: TPageControl
          inherited Tab_Juridica: TTabSheet
            inherited cbSexo_Contacto: TComboBox
              Top = 25
              ExplicitTop = 25
            end
          end
          inherited Tab_Fisica: TTabSheet
            inherited txt_ApellidoMaterno: TEdit
              Width = 164
              ExplicitWidth = 164
            end
          end
        end
        inherited Panel1: TPanel
          Width = 1008
          ExplicitWidth = 1008
          DesignSize = (
            1008
            25)
          inherited lblRazonSocial: TLabel
            Left = 455
            ExplicitLeft = 455
          end
          inherited lblGiro: TLabel
            Top = 8
            ExplicitTop = 8
          end
          inherited cbPersoneria: TComboBox
            OnChange = FreDatoPresona1cbPersoneriaChange
          end
          inherited txtDocumento: TEdit
            OnExit = FreDatoPersonatxtDocumentoExit
            OnKeyDown = FreDatoPersonatxtDocumentoKeyDown
            OnKeyPress = FreDatoPersonatxtDocumentoKeyPress
            OnKeyUp = FreDatoPersonatxtDocumentoKeyUp
          end
          inherited txtGiro: TEdit [6]
            Width = 288
            ExplicitWidth = 288
          end
          inherited txtRazonSocial: TEdit [7]
          end
        end
        inherited cboTipoCliente: TDBLookupComboBox
          Font.Name = 'MS Sans Serif'
          ParentFont = False
        end
      end
      object Panel6: TPanel
        Left = 7
        Top = 6
        Width = 131
        Height = 16
        BevelOuter = bvNone
        Caption = 'Datos Personales'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
      end
      object pnl_RepLegal: TPanel
        Left = 453
        Top = 83
        Width = 475
        Height = 26
        Anchors = [akLeft, akTop, akRight]
        BevelOuter = bvNone
        TabOrder = 5
        DesignSize = (
          475
          26)
        object lbl_RepresentanteLegal: TLabel
          Left = 126
          Top = 8
          Width = 345
          Height = 13
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'Representante legal'
        end
        object btn_RepresentanteLegal: TButton
          Left = 2
          Top = 5
          Width = 115
          Height = 20
          Caption = '&Representante Legal'
          TabOrder = 0
          OnClick = btn_RepresentanteLegalClick
        end
      end
    end
    object pnlVehiculos: TPanel
      Left = 5
      Top = 416
      Width = 1004
      Height = 97
      Alignment = taLeftJustify
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      DesignSize = (
        1004
        97)
      object Label2: TLabel
        Left = 11
        Top = -4
        Width = 70
        Height = 16
        Caption = 'Veh'#237'culos'
        FocusControl = dblVehiculos
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dblVehiculos: TDBListEx
        Left = 2
        Top = 34
        Width = 924
        Height = 62
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 65
            Header.Caption = 'Patente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Arial'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'Patente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 25
            Header.Caption = 'DV'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'DigitoVerificadorPatente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 200
            Header.Caption = 'Marca'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Arial'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'DescripcionMarca'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 200
            Header.Caption = 'Modelo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'Modelo'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Tipo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'DescripcionTipoVehiculo'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 35
            Header.Caption = 'A'#241'o'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'AnioVehiculo'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Nro. de Tag'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'SerialNumberaMostrar'
          end>
        DataSource = dsVehiculosContacto
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDblClick = dblVehiculosDblClick
        OnDrawText = dblVehiculosDrawText
      end
      object pnl_BotonesVehiculos: TPanel
        Left = 3
        Top = 12
        Width = 615
        Height = 22
        Anchors = [akLeft, akTop, akRight]
        BevelOuter = bvNone
        TabOrder = 1
        object btnAgregarVehiculo: TButton
          Left = 2
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Agregar Veh'#237'culo'
          Caption = '&Agregar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnAgregarVehiculoClick
        end
        object btnEditarVehiculo: TButton
          Left = 77
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Editar Veh'#237'culo'
          Caption = '&Editar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnEditarVehiculoClick
        end
        object btnEliminarVehiculo: TButton
          Left = 152
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Eliminar Veh'#237'culo'
          Caption = 'E&liminar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnEliminarVehiculoClick
        end
        object btn_AsignarTag: TButton
          Left = 227
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Asignar Telev'#237'a'
          Caption = '&Telev'#237'a'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btn_AsignarTagClick
        end
        object btn_EximirFacturacion: TButton
          Left = 302
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Eximir de facturaci'#243'n a los tr'#225'nsitos del veh'#237'culo'
          Caption = 'E&ximir'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnClick = btn_EximirFacturacionClick
        end
        object btn_BonificarFacturacion: TButton
          Left = 377
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Colocar una bonificaci'#243'n para los tr'#225'nsitos del veh'#237'culo'
          Caption = 'Bo&nificar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          OnClick = btn_BonificarFacturacionClick
        end
      end
    end
    object pnl_Botones: TPanel
      Left = 3
      Top = 515
      Width = 1016
      Height = 28
      Anchors = [akLeft, akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 2
      DesignSize = (
        1016
        28)
      object lbl_EstadoSolicitud: TLabel
        Left = 16
        Top = 10
        Width = 467
        Height = 13
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'lbl_EstadoSolicitud'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NB_Botones: TNotebook
        Left = 676
        Top = 0
        Width = 340
        Height = 28
        Align = alRight
        PageIndex = 2
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            340
            28)
          object btnSalir: TButton
            Left = 240
            Top = 1
            Width = 92
            Height = 25
            Hint = 'Salir de la Carga'
            Anchors = [akRight, akBottom]
            Caption = '&Salir'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = btnSalirClick
          end
          object btnNuevaSolicitud: TButton
            Left = 38
            Top = 1
            Width = 95
            Height = 25
            Hint = 'Nueva Solicitud'
            Anchors = [akRight, akBottom]
            Caption = 'Nueva &Solicitud'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Visible = False
            OnClick = btnNuevaSolicitudClick
          end
          object btn_NuevoContrato: TButton
            Left = 140
            Top = 1
            Width = 95
            Height = 25
            Hint = 'Nuevo Contrato'
            Anchors = [akRight, akBottom]
            Caption = 'Nuevo &Contrato'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            Visible = False
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageSolicitud'
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            340
            28)
          object btnGuardarDatos: TButton
            Left = 140
            Top = 1
            Width = 95
            Height = 25
            Hint = 'Guardar Datos'
            Anchors = [akRight, akBottom]
            Caption = '&Guardar Datos'
            Default = True
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btnGuardarDatosClick
          end
          object btn_Cancelar: TButton
            Left = 240
            Top = 1
            Width = 92
            Height = 25
            Hint = 'Cancelar carga Solicitud'
            Anchors = [akRight, akBottom]
            Cancel = True
            Caption = '&Cancelar'
            ModalResult = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = btn_CancelarClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 3
          Caption = 'PageContrato'
          DesignSize = (
            340
            28)
          object btn_Imprimir: TButton
            Left = 139
            Top = 1
            Width = 95
            Height = 25
            Hint = 'Imprimir Contrato'
            Anchors = [akRight, akBottom]
            Caption = '&Imprimir'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btn_ImprimirClick
          end
          object btn_CancelarContrato: TButton
            Left = 243
            Top = 1
            Width = 92
            Height = 25
            Hint = 'Cancelar Carga de Contrato'
            Anchors = [akRight, akBottom]
            Cancel = True
            Caption = '&Cancelar'
            ModalResult = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = btn_CancelarContratoClick
          end
          object btn_GuardarSolicitud: TButton
            Left = 35
            Top = 1
            Width = 96
            Height = 25
            Hint = 'Guardar Datos como Solicitud'
            Anchors = [akRight, akBottom]
            Caption = '&Guardar Solicitud'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            Visible = False
            OnClick = btn_GuardarSolicitudClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 4
          Caption = 'PageCancelar'
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            340
            28)
          object btn_SalirSolo: TButton
            Left = 243
            Top = 1
            Width = 92
            Height = 25
            Hint = 'Salir'
            Anchors = [akRight, akBottom]
            Cancel = True
            Caption = '&Salir'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btn_SalirSoloClick
          end
          object btn_BlanquearClave: TButton
            Left = 139
            Top = 1
            Width = 92
            Height = 25
            Caption = '&Blanquear Clave'
            TabOrder = 1
            OnClick = btn_BlanquearClaveClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 5
          Caption = 'PageComvenioPuro'
          ExplicitWidth = 0
          ExplicitHeight = 0
          object btn_CancelarConvenioPuro: TButton
            Left = 344
            Top = 1
            Width = 92
            Height = 25
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 0
          end
          object btn_ImprimirConvenioPuro: TButton
            Left = 243
            Top = 1
            Width = 92
            Height = 25
            Caption = '&Imprimir'
            TabOrder = 1
          end
        end
      end
    end
  end
  object ActualizarMedioComunicacionContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioComunicacionContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@IndiceMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Direction = pdInputOutput
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Anexo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@HorarioDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@IndiceDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 300
    Top = 65532
  end
  object ActualizarDatosContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 250
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 1
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@LugarNacimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@FechaNacimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoActividad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ApellidoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaternoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@NombreContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@SexoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@EmailContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoSituacionIVA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = 'password?'
      end
      item
        Name = '@Pregunta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Respuesta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 208
    Top = 65532
  end
  object ActualizarDomicilioContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TipoDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Dpto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@DescripcionCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@IndiceDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 237
    Top = 65532
  end
  object ActualizarDomicilioRelacionadoContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioRelacionadoContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Orden'
        Attributes = [paNullable]
        DataType = ftWord
        Direction = pdInputOutput
        Precision = 3
        Value = Null
      end>
    Left = 268
    Top = 65532
  end
  object dspVehiculosContacto: TDataSetProvider
    DataSet = ObtenerVehiculosContacto
    ResolveToDataSet = True
    Left = 320
    Top = 469
  end
  object cdsVehiculosContacto: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoSolicitudContacto'
        DataType = ftInteger
      end
      item
        Name = 'IndiceVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'TipoPatente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoMarca'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionMarca'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Modelo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoColor'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionColor'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AnioVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'CodigoTipoVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionTipoVehiculo'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'ContextMark'
        DataType = ftSmallint
      end
      item
        Name = 'ContractSerialNumber'
        DataType = ftLargeint
      end
      item
        Name = 'TieneAcoplado'
        DataType = ftInteger
      end
      item
        Name = 'DigitoVerificadorPatente'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DigitoVerificadorValido'
        DataType = ftInteger
      end
      item
        Name = 'PatenteRVM'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'DigitoVerificadorPatenteRVM'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SerialNumberaMostrar'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'TeleviaNuevo'
        DataType = ftBoolean
      end
      item
        Name = 'FechaInicioOriginalEximicion'
        DataType = ftDateTime
      end
      item
        Name = 'FechaInicioEximicion'
        DataType = ftDateTime
      end
      item
        Name = 'FechaFinalizacionEximicion'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoMotivoEximicion'
        DataType = ftSmallint
      end
      item
        Name = 'FechaInicioOriginalBonificacion'
        DataType = ftDateTime
      end
      item
        Name = 'FechaInicioBonificacion'
        DataType = ftDateTime
      end
      item
        Name = 'FechaFinalizacionBonificacion'
        DataType = ftDateTime
      end
      item
        Name = 'PorcentajeBonificacion'
        DataType = ftBCD
        Precision = 5
        Size = 2
      end
      item
        Name = 'ImporteBonificacion'
        DataType = ftInteger
      end
      item
        Name = 'FechaInicioEximicionEditable'
        DataType = ftBoolean
      end
      item
        Name = 'FechaFinEximicionEditable'
        DataType = ftBoolean
      end
      item
        Name = 'FechaInicioBonificacionEditable'
        DataType = ftBoolean
      end
      item
        Name = 'FechaFinBonificacionEditable'
        DataType = ftBoolean
      end
      item
        Name = 'Almacen'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaAltaCuenta'
        DataType = ftDateTime
      end
      item
        Name = 'Robado'
        DataType = ftBoolean
      end
      item
        Name = 'Recuperado'
        DataType = ftBoolean
      end>
    IndexDefs = <
      item
        Name = 'DEFAULT_ORDER'
      end
      item
        Name = 'CHANGEINDEX'
      end>
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@CodigoSolicitudContacto'
        ParamType = ptInput
      end>
    ProviderName = 'dspVehiculosContacto'
    StoreDefs = True
    AfterScroll = cdsVehiculosContactoAfterScroll
    Left = 280
    Top = 469
  end
  object dsVehiculosContacto: TDataSource
    DataSet = cdsVehiculosContacto
    Left = 224
    Top = 469
  end
  object ObtenerVehiculosContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterCancel = cdsVehiculosContactoAfterScroll
    ProcedureName = 'ObtenerVehiculosContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Size = -1
        Value = 0
      end
      item
        Name = '@CodigoSolicitudContacto'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Left = 356
    Top = 469
    object ObtenerVehiculosContactoCodigoSolicitudContacto: TIntegerField
      FieldName = 'CodigoSolicitudContacto'
    end
    object ObtenerVehiculosContactoIndiceVehiculo: TSmallintField
      FieldName = 'IndiceVehiculo'
    end
    object ObtenerVehiculosContactoTipoPatente: TStringField
      FieldName = 'TipoPatente'
      Size = 4
    end
    object ObtenerVehiculosContactoPatente: TStringField
      FieldName = 'Patente'
      FixedChar = True
      Size = 10
    end
    object ObtenerVehiculosContactoCodigoMarca: TSmallintField
      FieldName = 'CodigoMarca'
    end
    object ObtenerVehiculosContactoDescripcionMarca: TStringField
      FieldName = 'DescripcionMarca'
      Size = 30
    end
    object ObtenerVehiculosContactoModelo: TStringField
      FieldName = 'Modelo'
      Size = 50
    end
    object ObtenerVehiculosContactoCodigoColor: TIntegerField
      FieldName = 'CodigoColor'
    end
    object ObtenerVehiculosContactoDescripcionColor: TStringField
      FieldName = 'DescripcionColor'
      Size = 1
    end
    object ObtenerVehiculosContactoAnioVehiculo: TSmallintField
      FieldName = 'AnioVehiculo'
    end
    object ObtenerVehiculosContactoObservaciones: TStringField
      FieldName = 'Observaciones'
      Size = 255
    end
    object ObtenerVehiculosContactoCodigoTipoVehiculo: TSmallintField
      FieldName = 'CodigoTipoVehiculo'
    end
    object ObtenerVehiculosContactoDescripcionTipoVehiculo: TStringField
      FieldName = 'DescripcionTipoVehiculo'
      Size = 40
    end
    object ObtenerVehiculosContactoContextMark: TWordField
      FieldName = 'ContextMark'
    end
    object ObtenerVehiculosContactoContractSerialNumber: TLargeintField
      FieldName = 'ContractSerialNumber'
    end
    object ObtenerVehiculosContactoTieneAcoplado: TIntegerField
      FieldName = 'TieneAcoplado'
    end
    object ObtenerVehiculosContactoDigitoVerificadorPatente: TStringField
      FieldName = 'DigitoVerificadorPatente'
      Size = 1
    end
    object ObtenerVehiculosContactoDigitoVerificadorValido: TIntegerField
      FieldName = 'DigitoVerificadorValido'
    end
    object ObtenerVehiculosContactoPatenteRVM: TStringField
      DisplayWidth = 10
      FieldName = 'PatenteRVM'
      Size = 10
    end
    object ObtenerVehiculosContactoDigitoVerificadorPatenteRVM: TStringField
      FieldName = 'DigitoVerificadorPatenteRVM'
      Size = 1
    end
    object ObtenerVehiculosContactoSerialNumberaMostrar: TStringField
      FieldName = 'SerialNumberaMostrar'
      Size = 12
    end
    object ObtenerVehiculosContactoTeleviaNuevo: TBooleanField
      FieldName = 'TeleviaNuevo'
    end
    object ObtenerVehiculosContactoFechaInicioOriginalEximicion: TDateTimeField
      FieldName = 'FechaInicioOriginalEximicion'
    end
    object ObtenerVehiculosContactoFechaInicioEximicion: TDateTimeField
      FieldName = 'FechaInicioEximicion'
    end
    object ObtenerVehiculosContactoFechaFinalizacionEximicion: TDateTimeField
      FieldName = 'FechaFinalizacionEximicion'
    end
    object ObtenerVehiculosContactoCodigoMotivoEximicion: TWordField
      FieldName = 'CodigoMotivoEximicion'
    end
    object ObtenerVehiculosContactoFechaInicioOriginalBonificacion: TDateTimeField
      FieldName = 'FechaInicioOriginalBonificacion'
    end
    object ObtenerVehiculosContactoFechaInicioBonificacion: TDateTimeField
      FieldName = 'FechaInicioBonificacion'
    end
    object ObtenerVehiculosContactoFechaFinalizacionBonificacion: TDateTimeField
      FieldName = 'FechaFinalizacionBonificacion'
    end
    object ObtenerVehiculosContactoPorcentajeBonificacion: TBCDField
      FieldName = 'PorcentajeBonificacion'
      Precision = 5
      Size = 2
    end
    object ObtenerVehiculosContactoImporteBonificacion: TIntegerField
      FieldName = 'ImporteBonificacion'
    end
    object ObtenerVehiculosContactoFechaInicioEximicionEditable: TBooleanField
      FieldName = 'FechaInicioEximicionEditable'
    end
    object ObtenerVehiculosContactoFechaFinEximicionEditable: TBooleanField
      FieldName = 'FechaFinEximicionEditable'
    end
    object ObtenerVehiculosContactoFechaInicioBonificacionEditable: TBooleanField
      FieldName = 'FechaInicioBonificacionEditable'
    end
    object ObtenerVehiculosContactoFechaFinBonificacionEditable: TBooleanField
      FieldName = 'FechaFinBonificacionEditable'
    end
    object ObtenerVehiculosContactoAlmacen: TStringField
      FieldName = 'Almacen'
    end
    object ObtenerVehiculosContactoFechaAltaCuenta: TDateTimeField
      FieldName = 'FechaAltaCuenta'
    end
    object ObtenerVehiculosContactoRobado: TBooleanField
      FieldName = 'Robado'
    end
    object ObtenerVehiculosContactoRecuperado: TBooleanField
      FieldName = 'Recuperado'
    end
  end
  object ActualizarVehiculosContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarVehiculosContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoMarca'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Modelo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@AnioVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoTipoVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoColor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 333
    Top = 65532
  end
  object ObtenerDatosSCContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosSContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 400
    Top = 65534
  end
  object ObtenerDomiciliosContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomiciliosContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 431
    Top = 65534
  end
  object ObtenerMediosComunicacionContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMediosComunicacionContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 462
    Top = 65534
  end
  object ActualizarOperacionesFuenteSC: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarOperacionesFuenteSC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraInicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Operador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 177
    Top = 65532
  end
  object ActualizarSolicitudContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarSolicitudContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraCreacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@CodigoFuenteSolicitud'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCitaPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoEstadoSolicitud'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFuenteOrigenContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoEstadoResultado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 146
    Top = 65532
  end
  object QryTraerMediosComunicacion: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'codigosolicitudcontacto'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'indicepersona'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT s.indicemediocontacto, s.codigotipomediocontacto,'
      's.valor, s.codigoarea, s.anexo, s.horariodesde, s.horariohasta,'
      't.descripcion, t.formato as FormatoMedioContacto'
      'FROM solicitudcontactomedioscomunicacion s, tiposmediocontacto t'
      'WHERE codigosolicitudcontacto = :codigosolicitudcontacto'
      'AND indicepersona = :indicepersona'
      'and s.codigotipomediocontacto = t.codigotipomediocontacto'
      'order by indicemediocontacto')
    Left = 368
    Top = 65534
  end
  object ActualizarDomicilioEntregaContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioEntregaContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@IndiceDomicilioEntrega'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 496
    Top = 65534
  end
  object ObtenerDatosMedioPagoAutomaticoSolicitud: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosMedioPagoAutomaticoSolicitud;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 592
    Top = 224
  end
  object ActualizarEmailContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEmailContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmailAlta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 496
    Top = 30
  end
  object ActualizarConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEstadoConvenio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TipoTotalizacionDocumentosCobro'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TipoMedioPagoAutomatico'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoDomicilioFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CicloFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaInicioVigencia'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@UbicacionFisicaCarpeta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@CodigoPersonaMedioPagoAutomatico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCampania'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@NumeroConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoUsuarioActualizacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@AccionRNUT'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PlanComercial'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4096
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@RecibirInfoPorMail'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NumeroConvenioRNUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@FechaBajaRNUT'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaActualizacionMedioPago'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@ForzarConfirmado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 32
    Top = 520
  end
  object ActualizarDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Giro'
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@LugarNacimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@FechaNacimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoActividad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ApellidoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaternoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@NombreContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@SexoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@EmailContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoSituacionIVA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Pregunta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Respuesta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 64
    Top = 520
  end
  object ActualizarConvenioMediosEnvioDocumentos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarConvenioMediosEnvioDocumentos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMedioEnvioDocumento'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioEnvio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 96
    Top = 520
  end
  object ActualizarDatosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 128
    Top = 520
  end
  object ActualizarCuenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'ActualizarCuenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DetallePasadas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoCategoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CentroCosto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaAltaCuenta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaBajaCuenta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoEstadoCuenta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaVencimientoTAG'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaAltaTAG'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaBajaTAG'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ResponsableInstalacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PatenteRNUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DigitoVerificadorPatenteRNUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@AccionRNUT'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Suspendida'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TagEnCliente'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@FechaInicioOriginalEximicion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaInicioEximicion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinalizacionEximicion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoMotivoEximicion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaInicioOriginalBonificacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaInicioBonificacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinalizacionBonificacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PorcentajeBonificacion'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@ImporteBonificacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 160
    Top = 520
  end
  object ActualizarConvenioMedioPagoAutomaticoPAT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarConvenioMedioPagoAutomaticoPAT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoEmisorTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 192
    Top = 520
  end
  object ActualizarConvenioMedioPagoAutomaticoPAC: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarConvenioMedioPagoAutomaticoPAC'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 224
    Top = 520
  end
  object ActualizarMaestroVehiculos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMaestroVehiculos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DigitoVerificadorPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoMarca'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Modelo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@AnioVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoColor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@TieneAcoplado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoCategoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@DigitoVerificadorValido'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@PatenteRVM'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DigitoVerificadorPatenteRVM'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Robado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 256
    Top = 520
  end
  object ActualizarTurnoMovimientoEntrega: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTurnoMovimientoEntrega'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoVehiculo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TieneAcoplado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 288
    Top = 520
  end
  object ActualizarDocumentacionPresentadaConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDocumentacionPresentadaConvenios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumentacionRespaldo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@NoCorresponde'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@PathArchivo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FechaPresentacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaBaja'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TipoPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumentacionPresentada'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 320
    Top = 520
  end
  object ActualizarPersonasDomicilios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarPersonasDomicilios'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Principal'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 360
    Top = 520
  end
  object ActualizarMedioComunicacionPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioComunicacionPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Anexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@HorarioDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Principal'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@EstadoVerificacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoMedioComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 392
    Top = 520
  end
  object ObtenerVehiculo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerVehiculo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end>
    Left = 456
    Top = 520
  end
  object ActualizarDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@DescripcionCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSegmento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Depto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 488
    Top = 520
  end
  object spActualizarRepresentantesConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRepresentantesConvenios'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoClienteConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRepLegal1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRepLegal2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRepLegal3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 520
    Top = 520
  end
  object ActualizarMedioPagoAutomaticoPAC: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioPagoAutomaticoPAC'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Telefono'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 624
    Top = 224
  end
  object ActualizarMedioPagoAutomaticoPAT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioPagoAutomaticoPAT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Telefono'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoEmisorTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 656
    Top = 224
  end
  object CrearOrdenServicioAltaCuenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearOrdenServicioAltaCuenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 532
    Top = 65532
  end
  object BlanquearPasswordPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'BlanquearPasswordPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = Null
      end>
    Left = 160
    Top = 472
  end
  object PopupMenu: TPopupMenu
    OnPopup = PopupMenuPopup
    Left = 753
    Top = 118
    object mnu_EnviarMail: TMenuItem
      Caption = 'Enviar &Mail'
      OnClick = mnu_EnviarMailClick
    end
  end
  object ADOQuery1: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 768
    Top = 8
  end
  object spObtenerRepresentantesUltimoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRepresentantesUltimoConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersonaConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 566
    Top = 65533
  end
  object cdsIndemnizaciones: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'codigoconcepto'
        DataType = ftInteger
      end
      item
        Name = 'descripcionMotivo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ContractSerialNumber'
        DataType = ftLargeint
      end
      item
        Name = 'ContextMark'
        DataType = ftInteger
      end
      item
        Name = 'CodigoAlmacen'
        DataType = ftInteger
      end
      item
        Name = 'TipoasignacionTag'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Patente'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 680
    Top = 472
    Data = {
      020100009619E0BD01000000180000000800000000000300000002010E636F64
      69676F636F6E636570746F0400010000000000116465736372697063696F6E4D
      6F7469766F010049000000010005574944544802000200140014436F6E747261
      637453657269616C4E756D62657204000100000000000B436F6E746578744D61
      726B04000100000000000D436F6469676F416C6D6163656E0400010000000000
      115469706F617369676E6163696F6E5461670100490000000100055749445448
      0200020014000D4F62736572766163696F6E6573010049000000010005574944
      544802000200140007506174656E746501004900000001000557494454480200
      020014000000}
  end
  object spGenerarMovimientoCuentaConTelevia: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GenerarMovimientoCuentaConTelevia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@DescripcionMotivo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@TipoAsignacionTag'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 712
    Top = 472
  end
  object SPAgregarAuditoriaConveniosCAC: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarAuditoriaConveniosCAC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Operacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 12
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Etiqueta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@HostName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@Accion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end>
    Left = 824
    Top = 416
  end
end
