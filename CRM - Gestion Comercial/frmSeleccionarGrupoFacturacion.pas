unit frmSeleccionarGrupoFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, Util, UtilProc, UtilDB, DMConnection,
  DB, ADODB, DBClient, VariantComboBox;

type
  TfrmSeleccionarGrupoFacturacionForm = class(TForm)
    lbl1: TLabel;
    vcbCodigoGrupoFacturacion: TVariantComboBox;
    btnAceptar: TButton;
    btnCancelar: TButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigoGrupoFacturacion: Integer;
    function Inicializar(CodigoGrupoFacturacion: Integer = 0): Boolean;
  end;

var
  frmSeleccionarGrupoFacturacionForm: TfrmSeleccionarGrupoFacturacionForm;

implementation

{$R *.dfm}

function TfrmSeleccionarGrupoFacturacionForm.Inicializar(CodigoGrupoFacturacion: Integer = 0): Boolean;
resourcestring
    MSG_CAPTION = 'Error al obtener los grupos de facturación.';
var
  spObtenerGruposFacturacionDetallado: TADOStoredProc;
begin
    Result := True;
    FCodigoGrupoFacturacion := CodigoGrupoFacturacion;

    spObtenerGruposFacturacionDetallado := TADOStoredProc.Create(Self);
    try
        try
            spObtenerGruposFacturacionDetallado.Connection := DMConnections.BaseCAC;
            spObtenerGruposFacturacionDetallado.CommandTimeout := 30;
            spObtenerGruposFacturacionDetallado.ProcedureName := 'ObtenerGruposFacturacionDetallado';
            spObtenerGruposFacturacionDetallado.Parameters.Refresh;
            spObtenerGruposFacturacionDetallado.Open;

            if spObtenerGruposFacturacionDetallado.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spObtenerGruposFacturacionDetallado.Parameters.ParamByName('@ErrorDescription').Value);

            vcbCodigoGrupoFacturacion.Clear;
            vcbCodigoGrupoFacturacion.Items.Add('(Seleccione)', 0);
            while not spObtenerGruposFacturacionDetallado.Eof do begin
                vcbCodigoGrupoFacturacion.Items.Add(
                    spObtenerGruposFacturacionDetallado.FieldByName('DescripcionCompleta').AsString,
                    spObtenerGruposFacturacionDetallado.FieldByName('CodigoGrupoFacturacion').AsInteger);

                spObtenerGruposFacturacionDetallado.Next;
            end;
            
            vcbCodigoGrupoFacturacion.Value := FCodigoGrupoFacturacion;
        except
            on e: Exception do begin
                Result := False;
                MsgBoxErr(MSG_CAPTION, e.Message, 'Error', MB_ICONERROR);
            end;
        end;
    finally
        spObtenerGruposFacturacionDetallado.Free;
    end;
end;

procedure TfrmSeleccionarGrupoFacturacionForm.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_DEBE_SELECCIONAR = 'Debe seleccionar un grupo de facturación.';
begin
    if vcbCodigoGrupoFacturacion.Value = 0 then begin
        MsgBoxBalloon(MSG_DEBE_SELECCIONAR, 'Selección', MB_ICONSTOP, vcbCodigoGrupoFacturacion);
        Exit;
    end;

    FCodigoGrupoFacturacion := vcbCodigoGrupoFacturacion.Value;

    ModalResult := mrOk;
end;

procedure TfrmSeleccionarGrupoFacturacionForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
    Close;
end;

procedure TfrmSeleccionarGrupoFacturacionForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
