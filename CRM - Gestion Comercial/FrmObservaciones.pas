unit FrmObservaciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, PeaProcs, BuscaTab, StdCtrls, DmiCtrls, UtilDb, Util,
  ExtCtrls, DBTables, UtilProc, VariantComboBox, DMConnection,
  DPSControls;

type
  TFormObservaciones = class(TForm)
    Label1: TLabel;
    MeObservaciones: TMemo;
    Bevel1: TBevel;
    cbMotivosContacto: TVariantComboBox;
    Label2: TLabel;
    MotivosContactoPosibles: TADOQuery;
    Comunicaciones: TADOTable;
    btn_Aceptar: TDPSButton;
    btnCancelar: TDPSButton;
    procedure MeObservacionesChange(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    function bt_MotivosContactoProcess(Tabla: TDataSet;
      var Texto: String): Boolean;
    procedure btn_AceptarClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoComunicacion: Integer;
    function GetObservaciones: TStrings;
  public
    { Public declarations }
    Function Inicializa(CodigoComunicacion : integer): Boolean;
    property Observaciones: TStrings read GetObservaciones;
  end;

var
  FormObservaciones: TFormObservaciones;

implementation

{$R *.dfm}

function TFormObservaciones.Inicializa(CodigoComunicacion : integer): Boolean;
resourcestring
    MSG_NINGUN_MOTIVO_CONTACTO = '<Ninguno>';
var
    i : Integer;
begin
	FCodigoComunicacion := CodigoComunicacion;
    // Cargamos todos los Motivos de Contacto que existen
    CargarMotivosContacto(DMConnections.BaseCAC, cbMotivosContacto);

    // Obtenemos los Motivos de Contacto asociados a las ordenes de servicio generadas a partir de la comunicación
(*    MotivosContactoPosibles.Close;
    MotivosContactoPosibles.Parameters.ParamByName('CodigoComunicacion').Value := FCodigoComunicacion;
    MotivosContactoPosibles.Open;
    // Si el Motivo de Contacto es único es el propuesto, de lo contrario no se propone ninguno
    if MotivosContactoPosibles.RecordCount = 1 then begin
        i := 0;
        while (i < cbMotivosContacto.Items.Count) and
    	  (cbMotivosContacto.Items[i].Value <> MotivosContactoPosibles.FieldByName('MotivoContacto').asInteger) do
            inc(i);
        cbMotivosContacto.itemIndex := iif(i = cbMotivosContacto.Items.count, 0, i);
    end else
        cbMotivosContacto.ItemIndex := 0;
  *)
	Result := True;
end;


procedure TFormObservaciones.MeObservacionesChange(Sender: TObject);
//var i: integer;
begin
	(*
    btn_Aceptar.Enabled := False;
    if Observaciones.Lines.Count > 0 then begin
        for i:= 0 to Observaciones.Lines.Count - 1 do begin
            if Trim(Observaciones.Lines[i]) <> '' then
				btn_Aceptar.Enabled := True;
        end;
    end;
    *)
end;

procedure TFormObservaciones.btn_CancelarClick(Sender: TObject);
begin
	Close;
    ModalResult := mrCancel;
end;

procedure TFormObservaciones.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Shift = []) and (Key = VK_ESCAPE) then close;
end;

function TFormObservaciones.bt_MotivosContactoProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Texto := PadR(Tabla.fieldByName('Descripcion').AsString, 200, ' ') + Istr(Tabla.fieldByName('CodigoMotivo').AsInteger);
    Result := Trim(Texto) <> '';
end;

procedure TFormObservaciones.btn_AceptarClick(Sender: TObject);
resourcestring
    MSG_OBSERVACIONES = 'No se pudieron ingresar las Observaciones.';
    CAPTION_OBSERVACIONES = 'Ingresar Observaciones';
    CAPTION_VALIDAR_OBSERVACIONES = 'Validar Observaciones';
    MSG_MOTIVO_CONTACTO = 'Debe especificarse un Motivo de Contacto para la comunicación.';

begin
    if not ValidateControls([cbMotivosContacto],
      [cbMotivosContacto.itemIndex > 0],
      CAPTION_VALIDAR_OBSERVACIONES,
      [MSG_MOTIVO_CONTACTO]) then exit;

	try
		Comunicaciones.Active := True;
    	if Comunicaciones.Locate('CodigoComunicacion', FCodigoComunicacion, []) then begin
			Comunicaciones.Edit;
    	    TMemo(Comunicaciones.FieldByName('Observaciones')).Assign(MEObservaciones.Lines);
            Comunicaciones.FieldByName('MotivoContacto').asInteger :=
              cbMotivosContacto.Items[cbMotivosContacto.ItemIndex].Value;
			Comunicaciones.Post;
        end;
        Comunicaciones.Close;
        close;
        ModalResult := mrOk;
    except
    	on E: Exception do
        	MsgBoxErr(MSG_OBSERVACIONES, e.Message, CAPTION_OBSERVACIONES, MB_ICONSTOP);
    end;
end;

function TFormObservaciones.GetObservaciones: TStrings;
begin
    Result := MeObservaciones.Lines;
end;

end.
