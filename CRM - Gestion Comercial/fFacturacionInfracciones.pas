{-------------------------------------------------------------------------------
 File Name: fFacturacionInfracciones.pas
 Author:  Fmalisia
 Date Created: 08/12/2005
 Language: ES-AR                                                   
 Description: Modulo para Facturar a Infractores

 Revision :1
 Author:  jconcheyro
 Date Created: 30/05/2006
 Description: Se crea un transaccion aqui para todo el proceso de generacion de
 la NI y el marcado de las infracciones

 Revision: 2
 Author: nefernandez
 Date: 16/03/2007
 Description: Se pasa el parametro CodigoUsuario al SP spCrearNotaCobroNI
(para Auditoria)

Revision : 3
    Author : vpaszkowicz
    Date : 31/03/2008
    Description : Agrego el total de la infracci�n como par�metro al formulario
    de detalles.
    El precio en la grilla x la infracci�n.

Revision : 4
    Author : dAllegretti
    Date : 13/05/2008
    Description : Agrego el campo CodigoTarifaDayPassBALI en cldInfracciones.
    Con este campo luego ser� actualizada la tabla Infracciones.
Revision : 8
    Author : vpaszkowicz
    Date : 10/02/2009
    Description : Agrego que solo pueda facturar una infracci�n si existe al menos
    un tr�nsito no anulado en la misma.

Revision 9
	Author: mbecerra
    Date: 15-Abril-2009
    Description:	(Ref. Facturaci�n Electr�nica)
                	Se cambian las instrucciones del tipo
                    		DMConnections.BaseCAC.BeginsTrans
                    por
                        QueryExecute (DMConnections.BaseCAC, 'BEGIN TRAN Infractores')

                    para evitar el error de 'no active transaction' cuando se gatilla
                    un SAVE TRAN

Revision 10
	Author: nefernandez
    Date: 28-Abril-2009
    Description: SS 784 - A ra�z del inicio del proyecto de Factura Electr�nica en CN y
    su posterior implementaci�n, se necesita realizar la modificaci�n del actual proceso de
    Facturaci�n y de los sistemas asociados a este.

Revision 11
	Author: mbecerra
    Date: 18-Mayo-2009
    Description:		(Ref. Facturaci�n Electr�nica)
                El c�lculo de intereses se hace de manera autom�tica, permitiendo
                al operador modificar el valor.

                Una vez que el operador acepta los cargos, inmediatamente se
                generan los MovimientosCuentas y se despliega la ventana de
                facturaci�n. Por esto, el bot�n "btnFacturarConvenio" desaparece.

    	30-Junio-2009
                * Se desea que los convenios dados de baja aparezcan en rojo

        01-Julio-2009
                * Se cambia gran parte de la ventana, pues tiene un comportamiento
                  err�tico despu�s de la primera facturaci�n.

                * Se quitan todas las l�neas de c�digo comentadas y el c�digo
                  no utilizado para hacer m�s legible el programa.

Revision: 12
    Author: pdominguez
    Date  : 01/06/2010
    Description: Infractores Fase 2
        - Se a�adieron/modificaron los siguientes objetos/variables/constantes:
            cbConcesionaria: TVariantComboBox,
            cldInfracciones: TClientDataSet - Se a�aden los campos: CodigoEstadoInterno,
                CodigoEstadoExterno, EstadoCN, EstadoIF y EsFacturable.
            dblInfracciones: TDBListEx - se a�aden los campos EstadoCN y EstadoIF a la
                rejilla.

        - Se eliminaron los siguientes objetos:
            cldInfracciones: TClientDataSet - se elimina el campo Descripcion.
            dblInfracciones: TDBListEx - se elimina los campos Descripcion, ExisteTransitoNoAnulado.
            SpAnularInfraccionCargo: TADOStoredProc.
            spAnularInfraccionFacturar: TADOStoredProc.

        - Se eliminaron las sigiuentes variables/Constantes:
            FInfraccionFacturada, FInfraccionEnBHTU y FInfraccionEnPDU.

        - Se a�adieron/modificaron los siguientes procedimientos/funciones:
            Inicializar,
            CargarInfracciones,
            InfraccionEsFacturable,
            dblInfraccionesDrawText

Revision 13
Author      : mbecerra
Date        : 05-Enero-2011
Description :       (Ref SS 945)
                -   Se agrega la validaci�n de que el convenio est� vigente para
                    poder facturar infracciones

Firma		: 	SS990_MBE_20110921
Description	: 	Al seleccionar un Rut para facturar, desplegar un mensaje
				indicando si tiene deuda vencida e impaga, y/o est� en legales.

Firma       :   SS_1045_CQU_20120604
Description :   Permite facturar infracciones por rut.
                Se agrega un PageControl para separar la funcionalidad antigua de la nueva
                y se agregan los m�todos y funciones necesarias para realizar este proceso.
                Existen funciones a los que se le agerg� un "Case of" para determinar
                la hoja (tsSheet) activa y determinar los pasos a seguir.
                Se agrega la l�gica para que facture por Patente y los movimientos cuenta
                por concesionaria.
                Se modifica el formato en el mensaje antes de facturar por rut.

Firma       :   SS_660_CQU_20121010
Description :   Agrega dos par�metros a spGenerarMovimientosInfraccionPorConvenio,
                �stos se usan en la facturaci�n de Infractores Morosos solamente.
                Adem�s coloca la l�gica para impedir que se facturen los dos tipos
                de infracciones juntas.
                Se aumenta el TimeOut para la consulta de infracciones por rut.
                Se arregla un error detectado en las pruebas de 660,
                si se cambia a la facturaci�n por rut y son cargados datos en la dblInfraccionesPorRut
                y luego se vuelve a la facturaci�n por patente, la dblInfracciones queda habilitada
                para seleccionar, este comportamiento es independiente de que est� o no el bot�n
                btnSeleccionarInfraccionesClick habilitado o visible. Se corrigi�.
                Se agrega un Try-Finally en el m�todo CargarInfraccionesPorRut por lo tanto
                se aument� la tabulaci�n.

Firma       :   SS_660_CQU_20130604
Descripcion :   Se corrige un error detectado en el �ndice de un stringlist (Categoria)
                Se modifica la funcion que selecciona y deselecciona infracciones, cambiando de posici�n la consulta.
                Se modifica la carga del StringList de Categor�as, ahora se cargan las categer�as seg�n Patentes Seleccionadas.
                Se oculta o muestra el panel de mensaje seg�n corresponda.
                Se agrega variable para indicar que se est� realizando la facturacion por rut

Firma       :   SS_1120_NDR_20130827
Descripcion :   En el combo de convenios indicar si es convenio sin cuenta y/o de baja.

Firma       : SS_1120_MVI_20130930
Descripcion : Se cambia la forma en la que se llena el combo cbConveniosCliente, para utilizar el procedimeinto generico CargarConveniosRUT.
              Se modifica el procedimiento cbConveniosClienteDrawItem para que llame al procedimiento generico ItemComboBoxColor.
              Se comenta el procedimiento ItemComboBoxColor, ya que con el cambio no se utilizara.

Firma       :   SS_1045_CQU_20131108
Descripcion :   Se modifica el requerimiento y ahora se solicita que facture por rut y no por patente, es decir, generar� una factura para todas
                las infracciones seleccionadas.
                Se agrega un refresh a la grilla en el bot�n Buscar de la facturaci�n por rut y se agranda el TimeOut al obtener.

Author      :   Claudio Quezada Ib��ez
Date        :   11-Marzo-2014
Firma       :   SS_660_CQU_20140311
Descripcion :   Se agrega permiso para facturar infracciones por rut, si no existe el permiso se oculta el TAB.

Author      :   Claudio Quezada Ib��ez
Date        :   06-Agosto-2014
Firma       :   SS_660E_CQU_20140806
Descripcion :   Se agrega permiso para facturar infracciones por rut de tipo 2 (ListaAmarilla), si no existe el permiso No abre la ventana.

Firma       : SS_1147_NDR_20140813
Descripcion : Si el boton "Buscar" no encuentra datos, el boton Facturar/Valorizar queda deshabilitado

Author      :   Claudio Quezada Ib��ez
Date        :   10-Septiembre-2014
Firma       :   SS_660E_CQU_20140910
Descripcion :   Se realizan correcciones indicadas por QA
                En el dfm se deja el formulario Visible = False y FormStyle = fsNormal

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

-------------------------------------------------------------------------------}
unit fFacturacionInfracciones;

interface

uses
  //Facturacion a Infractores
  DMConnection,
  Util,
  UtilDB,
  UtilProc,
  PeaProcs,
  PeaTypes,
  frmRptNotaCobroInfractor,
  fFacturacionInfraccionesDetalle,
  fFacturacionInfraccionesDetalleCargo,
  frmRptNotaCobroDetalleInfracciones,
  BuscaClientes,
  frmFacturacionManual,
  FrmSolicitudContacto,
  //Otros
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, DbList, StdCtrls, Validate, DateEdit, ExtCtrls, ListBoxEx,
  DBListEx,  DmiCtrls, DBClient, ImgList, VariantComboBox, Grids, DBGrids,
  ComCtrls, RStrings, ImprimirWO,frmMuestraMensaje, Provider, SysUtilsCN, StrUtils; 

type
  TFormFacturacionInfracciones = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    edPatente: TEdit;
    deDesdeFecha: TDateEdit;
    deHastaFecha: TDateEdit;
    lblFechaHasta: TLabel;
    lblFechaDesde: TLabel;
    lblPatente: TLabel;
    btnSalir: TButton;
    btnValorizar: TButton;
    btnBuscar: TButton;
    dblInfracciones: TDBListEx;
    spObtenerInfraccionesAFacturar: TADOStoredProc;
    ds_Infracciones: TDataSource;
    Img_Tilde: TImageList;
    ObtenerDatosPersonaNotaCobroInfraccion: TADOStoredProc;
    spCrearNotaCobroNI: TADOStoredProc;
    spObtenerCliente: TADOStoredProc;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    spGenerarMovimientosInfraccionPorConvenio: TADOStoredProc;
    spObtenerConveniosCliente: TADOStoredProc;
    spCalcularIntereses: TADOStoredProc;
    cldInfracciones: TClientDataSet;
    cbConcesionaria: TVariantComboBox;
    lblConcesionaria: TLabel;
    spActualizarInfraccionValorizada: TADOStoredProc;
    pgCentral: TPageControl;
    tsFacturacionPatente: TTabSheet;
    tsFacturacionRut: TTabSheet;
    dblInfraccionesPorRut: TDBListEx;
    spObtenerInfraccionesAFacturarPorRut: TADOStoredProc;
    ds_InfraccionesPorRut: TDataSource;
    cldInfraccionesPorRut: TClientDataSet;
    pnlBotonesRut: TPanel;
    lblRutInput: TLabel;
    lblDesdeFechaPorRut: TLabel;
    lblHastaFechaPorRut: TLabel;
    txtRutInput: TEdit;
    deDesdeFechaRut: TDateEdit;
    deHastaFechaRut: TDateEdit;
    btnBuscarRut: TButton;
    dspInfraccionesPorRUT: TDataSetProvider;
    stbInfraccionesPorRUT: TStatusBar;
    stbInfraccionesPorPatente: TStatusBar;
    Panel4: TPanel;
    gbDatosComprobante: TGroupBox;
    lblRUT: TLabel;
    lblConvenio: TLabel;
    lblNumeroConvenio: TLabel;
    peRUTCliente: TPickEdit;
    txtNumeroConvenio: TEdit;
    btnBuscarConvenio: TButton;
    cbConveniosCliente: TVariantComboBox;
    gbDatosCliente: TGroupBox;
    lblApellido: TLabel;
    lblDomicilio: TLabel;
    lblComuna: TLabel;
    lblRegion: TLabel;
    lblDatoApellidoNombre: TLabel;
    lblDatoDomicilio: TLabel;
    lblDatoComuna: TLabel;
    lblDatoRegion: TLabel;
    lblDomicilioFact: TLabel;
    lblComunaFact: TLabel;
    Label2: TLabel;
    lblDatoDomicilioFact: TLabel;
    lblDatoComunaFact: TLabel;
    lblDatoRegionFact: TLabel;
    lblDomicilioComprobante: TLabel;
    btnSeleccionarInfracciones: TButton;
    cldInfraccionesPorRutCodigoInfraccion: TIntegerField;
    cldInfraccionesPorRutPatente: TStringField;
    cldInfraccionesPorRutFechaInfraccion: TDateTimeField;
    cldInfraccionesPorRutCodigoEstadoInterno: TSmallintField;
    cldInfraccionesPorRutEstadoCN: TStringField;
    cldInfraccionesPorRutCodigoEstadoExterno: TSmallintField;
    cldInfraccionesPorRutEstadoIF: TStringField;
    cldInfraccionesPorRutFechaAnulacion: TDateTimeField;
    cldInfraccionesPorRutCodigoMotivoAnulacion: TSmallintField;
    cldInfraccionesPorRutMotivoAnulacion: TStringField;
    cldInfraccionesPorRutNombre: TStringField;
    cldInfraccionesPorRutNumeroDocumento: TStringField;
    cldInfraccionesPorRutPrecioInfraccion: TIntegerField;
    cldInfraccionesPorRutCodigoTarifaDayPassBALI: TIntegerField;
    cldInfraccionesPorRutCodigoConvenio: TIntegerField;
    cldInfraccionesPorRutNumeroConvenio: TStringField;
    cldInfraccionesPorRutTipoComprobanteFiscal: TStringField;
    cldInfraccionesPorRutTipoComprobanteFiscalDesc: TStringField;
    cldInfraccionesPorRutNumeroComprobanteFiscal: TLargeintField;
    cldInfraccionesPorRutEstadoPago: TStringField;
    cldInfraccionesPorRutCodigoConcesionaria: TSmallintField;
    cldInfraccionesPorRutNombreConcesionaria: TStringField;
    cldInfraccionesPorRutEsFacturable: TBooleanField;
    cldInfraccionesPorRutDescripcionConcesionaria: TStringField;
    cldInfraccionesPorRutFacturar: TSmallintField;
    cbConcesionariaRut: TVariantComboBox;
    lblConcesionariaRut: TLabel;
    cldInfraccionesPorRutCodigoTipoInfraccion: TSmallintField; 
    cldInfraccionesPorRutCategoria_RNVM: TSmallintField;       
    cldInfraccionesPorRutConceptoMoroso: TIntegerField;        
    procedure btnValorizarClick(Sender: TObject);
    procedure dblInfraccionesKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure dblInfraccionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure dblInfraccionesDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure cbConveniosClienteChange(Sender: TObject);
    procedure btnBuscarConvenioClick(Sender: TObject);
    procedure btnSeleccionarInfraccionesClick(Sender: TObject);
    procedure cbConveniosClienteDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure edPatenteChange(Sender: TObject);
    procedure peRUTClienteChange(Sender: TObject);
    procedure cbConcesionariaChange(Sender: TObject);
    procedure dblInfraccionesPorRutDblClick(Sender: TObject);
    procedure dblInfraccionesPorRutDrawText(Sender: TCustomDBListEx;
        Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
        var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
        var DefaultDraw: Boolean);
    procedure dblInfraccionesPorRutKeyDown(Sender: TObject; var Key: Word;
        Shift: TShiftState);
    procedure pgCentralDrawTab(Control: TCustomTabControl; TabIndex: Integer;
        const Rect: TRect; Active: Boolean);
    procedure pgCentralChange(Sender: TObject);
    procedure txtRutInputChange(Sender: TObject);
    procedure btnBuscarRutClick(Sender: TObject);
    procedure ClientDataSetAfterOpenGenerico(DataSet: TDataSet);
    procedure dblInfraccionesPorRutColumns2HeaderClick(Sender: TObject);
  private
    { Private declarations }
    FListaConveniosBaja : TStringList;
	FUltimaBusqueda : TBusquedaCliente;
    FPuedeSeleccionarInfracciones,
    FGrillaDesmarcada : boolean;
    NumeroMovimientoInfracciones    : int64;
    NumeroMovimientoGastosCobranzas : int64;
    NumeroMovimientoIntereses       : int64;
    NumeroMovimientoMorosos         : int64;
    FechaEmisionComprobante         : TDateTime;
    FechaVencimientoComprobante     : TDateTime;
    FListaPatentes, FListaPatentesPorRut , FListaConcesionarias: TStringList;
    FHecho, FOpcion1, FOpcion2 : Boolean;
    FRespuesta : TRespuesta;
    FInfraccionesSeleccionadasPorRut, FTotalFacturar : Integer;
    FInfraccionesSeleccionadas: Integer;
    FCodigoInfraccionNormal : SmallInt;
    FInfraccionesNormales, FInfraccionesMorosas,                        
    FInfraccionesNormalesPorRut, FInfraccionesMorosasPorRut : Integer;  
    FListaCategorias : TStringList;                                     
    FFacturandoPorRut : Boolean;                                        
    FTraerListaAmarilla : Boolean;                                      
    FCodigoConcesionariaNativa: Integer;								
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;      
    function InfraccionEsFacturable:boolean;
    procedure MostrarDomicilioFacturacion;
  public
    { Public declarations }
    function Inicializar: Boolean; overload;
    function Inicializar(Morosidad : Boolean): Boolean; overload;

    procedure ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor);
    procedure InicializarData;
    procedure LimpiarPatente;
    procedure LimpiarDatosComprobante(Habilitar : boolean);
    procedure LimpiarDatosCliente;
    function CargarInfracciones : boolean;
    procedure DesmarcarGrilla;
    function HayInfraccionesSeleccionadas : Boolean;
    procedure PoneColorGrilla(Color : TColor; var dblGrilla : TDBListEx);
    procedure LimpiarRut;
    procedure Valorizar;
    procedure Facturar;
    function CargarInfraccionesPorRut : boolean;
  end;

var
  FormFacturacionInfracciones: TFormFacturacionInfracciones;

implementation

{$R *.dfm}


{
    Function Name: Inicializar
    Parameters : None
    Return Value: Boolean
    Author : fmalisia
    Date Created : 08/12/2005

    Description : Inicializaci�n de Este Formulario.

    Revision : 12
        Author: pdominguez
        Date: 01/06/2010
        Description: Infractores Fase 2
            - Se inicializa el combo de Concesionarias.
            - Se eliminan las variables FInfraccionFacturada, FInfraccionEnBHTU
            y FInfraccionEnPDU.
}
function TFormFacturacionInfracciones.Inicializar: Boolean;
resourcestring                                                                     
    CONSULTA_CONSTANTE  =   'SELECT dbo.CONST_CODIGO_TIPO_INFRACCION_NORMAL()';    
    MSG_ERROR_CODIGO    =   'No se pudo obtener el C�digo de las Infracciones';
begin
    tsFacturacionPatente.TabVisible := False;   // TASK_144_MGO_20170301

    if not cldInfracciones.Active then cldInfracciones.Active := True;
    if not cldInfraccionesPorRut.Active then cldInfraccionesPorRut.Active := True;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         
    Color := FColorMenu;                                                                                    

    //Abro el dataset
    cldInfracciones.Close;
    //cldInfracciones.CreateDataSet;
    cldInfracciones.Open;
    //Centro el formulario
    CenterForm(Self);

    CargarComboConcesionarias(DMConnections.BaseCAC, cbConcesionaria, False);
    FCodigoConcesionariaNativa := ObtenerCodigoConcesionariaNativa;
    cbConcesionaria.ItemIndex := cbConcesionaria.Items.IndexOfValue(FCodigoConcesionariaNativa);

    CargarComboConcesionarias(DMConnections.BaseCAC, cbConcesionariaRut, False);
    if cbConcesionariaRut.Items.Count > 0 then begin                                            
        cbConcesionariaRut.Items.Add('Todas las Concesionarias', -1);                           
        cbConcesionariaRut.ItemIndex := cbConcesionariaRut.Items.IndexOfValue(-1);              
    end else
             cbConcesionariaRut.ItemIndex := cbConcesionariaRut.Items.IndexOfValue(FCodigoConcesionariaNativa);
    try                                                                                                         
        FCodigoInfraccionNormal := SysUtilsCN.QueryGetSmallIntValue(DMConnections.BaseCAC, CONSULTA_CONSTANTE); 
    except                                                                                                      
        MsgBoxErr(Format(MSG_ERROR_INICIALIZACION, [caption]), MSG_ERROR_CODIGO, caption, MB_ICONERROR);        
        Result := False;                                                                                        
        Exit;                                                                                                   
    end;                                                                                                        

    InicializarData();

    FListaConveniosBaja := TStringList.Create;
    cldInfraccionesPorRut.Close;
    cldInfraccionesPorRut.Open;
    //pgCentral.ActivePage := tsFacturacionPatente;                             // TASK_144_MGO_20170301
    pgCentral.ActivePage := tsFacturacionRUT;                                   // TASK_144_MGO_20170301
    FListaPatentes := TStringList.Create;

    FListaPatentesPorRut := TStringList.Create;
    FListaConcesionarias := TStringList.Create;         
    FListaCategorias          := TStringList.Create;    
    DBListExResaltaCamposIndex(dblInfraccionesPorRut);  
    DBListExResaltaCamposIndex(dblInfracciones);        

    if FTraerListaAmarilla then begin
        tsFacturacionRut.TabVisible := ExisteAcceso('facturar_infractor_moroso_por_rut');   
        Caption := 'Facturaci�n de Infracciones por Morosidad';                            
        tsFacturacionPatente.Caption := 'Facturacion por Morosidad';                        
        tsFacturacionPatente.TabVisible := False;                                           
        tsFacturacionPatente.Enabled := False;                                              
        tsFacturacionRut.Caption := 'Facturacion por Morosidad';                            
        pgCentral.ActivePage := tsFacturacionRut;                                           
        pgCentralChange(tsFacturacionRut);                                                  
    end else                                                                                
        tsFacturacionRut.TabVisible := ExisteAcceso('facturar_infractor_por_rut');          

    Result := True;
end;

function TFormFacturacionInfracciones.Inicializar(Morosidad : Boolean): Boolean;
var                                                                                     
    Resultado : Boolean;                                                                
begin                                                                                   
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         
    Color := FColorMenu;

    FTraerListaAmarilla := Morosidad;
    Resultado := Inicializar;                                                          
    Result := Resultado;                                                               
end;                                                                                   
{-----------------------------------------------------------------------------
                    	InicializarData
  Author: mbecerra
  Date: 02-Julio-2009
  Description:		Inicializa todos los datos para el siguiente ingreso
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.InicializarData;
begin

    LimpiarPatente();

    cldInfracciones.EmptyDataSet;
    cldInfracciones.EnableControls;
    dblInfracciones.Invalidate;
    LimpiarDatosComprobante(False);
    LimpiarDatosCliente();

	btnSeleccionarInfracciones.Enabled := False;
    FPuedeSeleccionarInfracciones := False;
    gbDatosComprobante.Enabled := False;
    spObtenerInfraccionesAFacturar.Close;

    LimpiarRut();
    if cldInfraccionesPorRut.Active then cldInfraccionesPorRut.Close;
    stbInfraccionesPorPatente.SimpleText := EmptyStr;
    stbInfraccionesPorRUT.SimpleText := EmptyStr;

    FHecho := False;
    FOpcion1 := False;
    FOpcion2 := False;
    FInfraccionesMorosasPorRut  :=  0;
    FInfraccionesNormalesPorRut :=  0;  
    FInfraccionesMorosas        :=  0;  
    FInfraccionesNormales       :=  0;  
end;

{-----------------------------------------------------------------------------
                    	LimpiarPatente
  Author: mbecerra
  Date: 02-Julio-2009
  Description:		Limpia los datos de la patente ingresada
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.LimpiarPatente;
begin
    edPatente.Text := '';
    deDesdeFecha.Date := 0;
    deHastaFecha.Date := 0;
end;

{-----------------------------------------------------------------------------
                    	LimpiarDatosCliente
  Author:
  Date:
  Description:		Limpia los datos del cliente
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.LimpiarDatosCliente;
begin
    lblDatoApellidoNombre.Caption := '';
    lblDatoDomicilio.Caption := '';
    lblDatoComuna.Caption := '';
    lblDatoRegion.Caption := '';

    lblDatoDomicilioFact.Caption := '';
    lblDatoComunaFact.Caption := '';
    lblDatoRegionFact.Caption := '';
end;

{-----------------------------------------------------------------------------
                    	LimpiarDatosComprobante
  Author:
  Date:
  Description:		Limpia los datos del Comprobante
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.LimpiarDatosComprobante;
begin
    peRUTCliente.Text := '';
    txtNumeroConvenio.Text := '';
    cbConveniosCliente.Clear;
	gbDatosComprobante.Enabled := Habilitar;
end;

{-----------------------------------------------------------------------------
                    	LimpiarDatosComprobante
  Author:
  Date:
  Description:		Limpia los datos del Comprobante

  Firma         :   SS_1045_CQU_20120604
  Description   :   Se agrega evaluaci�n del TAB activo
-----------------------------------------------------------------------------}
function TFormFacturacionInfracciones.HayInfraccionesSeleccionadas;
var
    Book : TBookMark;
begin
    Result := False;

    if pgCentral.ActivePage = tsFacturacionPatente then begin
        Book := cldInfracciones.GetBookmark;
        cldInfracciones.DisableControls;
        cldInfracciones.First;
        while not (cldInfracciones.Eof or Result) do begin
            Result := cldInfracciones.FieldByName('Facturar').AsBoolean;
            cldInfracciones.Next;
        end;
        cldInfracciones.GotoBookmark(Book);
        cldInfracciones.EnableControls;
    end else begin
        if cldInfraccionesPorRut.Active then begin
            Book := cldInfraccionesPorRut.GetBookmark;
            cldInfraccionesPorRut.DisableControls;
            cldInfraccionesPorRut.First;
            while not (cldInfraccionesPorRut.Eof or Result) do begin
                Result := cldInfraccionesPorRut.FieldByName('Facturar').AsInteger = 1;
                cldInfraccionesPorRut.Next;
            end;
            cldInfraccionesPorRut.GotoBookmark(Book);
            cldInfraccionesPorRut.EnableControls;
        end else
            Result := False;                        
    end;
end;

{-----------------------------------------------------------------------------
  					PoneColorGrilla
  Author: mbecerra
  Date: 03-Julio-2009
  Description:		Pone el color de la columnas de la grilla seg�n el color
                    indicado como par�metro.

  Firma         :   SS_1045_CQU_20120604
  Description   :   Se agrega par�metro de tipo DBListEx para recibir la grilla
                    que se desea colorear, esto es por que ahora hay 2 grillas
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.PoneColorGrilla(Color : TColor; var dblGrilla : TDBListEx);  
var
	i : integer;
begin

    dblGrilla.Color := Color;
    for i := 0 to dblGrilla.Columns.Count - 1 do begin
    	dblGrilla.Columns[i].Color := Color;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnBuscarClick
  Author:    fmalisia
  Date Created: 08/12/2005
  Description:  Permito a buscar las infracciones a facturar
  Parameters: None
  Return Value: Boolean

  Firma         : SS_1045_CQU_20120604
  Description   : Cambia la llamada al m�todo PoneColorGrilla ya que  ahora es gen�rico.
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.btnBuscarClick(Sender: TObject);
resourcestring
    MESSAGE_PATENTE_VACIA 			= 'Patente vac�a';
    MESSAGE_FECHA_DESDE_VACIA 		= 'Fecha desde vac�a';
    MESSAGE_HASTAA_DESDE_VACIA 		= 'Fecha hasta vac�a';
    MESSAGE_HASTA_MENOR_A_DESDE 	= 'Fecha hasta es menor a fecha desde ';
var
    i: integer;
begin
    //Verifica si son validos los datos para la busqueda
  	if not ValidateControls(	[edPatente, deHastaFecha],
    							[edPatente.Text <> '', deDesdeFecha.Date <= deHastaFecha.Date],
                                Caption,
                                [MESSAGE_PATENTE_VACIA, MESSAGE_HASTA_MENOR_A_DESDE ]) then Exit;

    cldInfracciones.EmptyDataSet;
    dblInfracciones.Invalidate;
    LimpiarDatosComprobante(False);
    LimpiarDatosCliente();
    btnSeleccionarInfracciones.Enabled := False;
    if CargarInfracciones() then begin
    	//Poner en gris el color del DBList
        if (spObtenerInfraccionesAFacturar.RecordCount > 0) then begin
            PoneColorGrilla(clBtnFace, dblInfracciones);
            gbDatosComprobante.Enabled := True;
            peRUTCliente.SetFocus;
            btnBuscar.Default := False;                     
            btnBuscarConvenio.Default := True;              
        end;
    end;
end;

{-----------------------------------------------------------------------------
                    	peRUTClienteButtonClick
  Author:
  Date:
  Description:		Abre la ventana de B�squeda de Clientes
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.peRUTClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
	FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);

    Application.createForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda) then begin
        f.ShowModal;
        if f.ModalResult = idOk then begin
            fUltimaBusqueda := f.UltimaBusqueda;
            peRUTCliente.Text :=  f.Persona.NumeroDocumento;
            peRUTCliente.setFocus;
            if not btnBuscarConvenio.Default then begin
                btnBuscarConvenio.Default := True;                 
                if pgCentral.ActivePage = tsFacturacionPatente then
                    btnBuscar.Default := False                     
                else                                               
                    btnBuscarRut.Default := False;                 
            end;                                                   

            cbConveniosCliente.Clear;
        end;
    end;
    f.free;
end;


procedure TFormFacturacionInfracciones.DesmarcarGrilla;
var
    Book : TBookMark;
begin
    if not FGrillaDesmarcada then begin
        if pgCentral.ActivePage = tsFacturacionPatente then begin
            with cldInfracciones do begin
                Book := GetBookmark;
                DisableControls;
                First;
                while not EOF  do begin
                    Edit;
                    FieldByName('Facturar').AsBoolean := False;
                    Post;

                    Next;
                end;

                GotoBookmark(Book);
                EnableControls;
                FGrillaDesmarcada := True;
            end;
        end;
    end;
end;

procedure TFormFacturacionInfracciones.peRUTClienteChange(Sender: TObject);
begin
    if ActiveControl = Sender then begin
        LimpiarDatosCliente();
        btnSeleccionarInfracciones.Enabled := False;
        DesmarcarGrilla();
        FPuedeSeleccionarInfracciones := False;
        cbConveniosCliente.Clear;
        txtNumeroConvenio.Text := '';

        if pgCentral.ActivePage = tsFacturacionPatente then begin
            PoneColorGrilla(clBtnFace, dblInfracciones);
            btnBuscar.Default := False;                             
            FInfraccionesSeleccionadas := 0;                        
            FListaPatentes.Clear;                                   
            ClientDataSetAfterOpenGenerico(ds_Infracciones.DataSet);
        end else begin                                              
            PoneColorGrilla(clBtnFace, dblInfraccionesPorRut);      
            FPuedeSeleccionarInfracciones := True;                  
            btnBuscarRut.Default := False;                          
        end;                                                        
        btnBuscarConvenio.Default := True;                          
    end;

end;

{
    Function Name: CargarInfracciones
    Parameters : None
    Return Value: Boolean
    Author : fmalisia
    Date Created : 08/12/2005

    Description : Cargo las infracciones en un DataSet

    Revision : 12
        Author: pdominguez
        Date: 01/06/2010
        Description: Infractores Fase 2
            - Se a�aden los campo EstadoCN, EstadoIF, CodigoEstadoInterno y CodigoEstadoExterno
            en la carga de datos al ClientDataSet.
            - se eliminan los campos Descripcion, EstadoTransitoNoAnulado.
            - Se a�ade la asignaci�n del valor al par�metro @CodigoConcesionaria en el objeto
            spObtenerInfraccionesAFacturar: TADOStoredProc.
}
function TFormFacturacionInfracciones.CargarInfracciones;
resourcestring
    ERROR_OBT_INFRACCIONES 	=  'Error obteniendo infracciones';
    MSG_INFRACCIONES_NO_ENCONTRADAS = 'No se han encontrado infracciones con este criterio de b�squeda';
var
    Patente     : string;          
    PosPatente  : Integer;         
begin
    Screen.Cursor := crHourGlass;
    Result := True;
    try
        FListaPatentes.Clear;
        FInfraccionesSeleccionadas := 0;   
        FInfraccionesMorosas := 0;         
        FInfraccionesNormales := 0;        
    	spObtenerInfraccionesAFacturar.Close;
        cldInfracciones.EmptyDataSet;
        cldInfracciones.DisableControls;

        cldInfracciones.AfterOpen   := nil;
        cldInfracciones.AfterClose  := nil; 
        cldInfracciones.AfterScroll := nil; 

        with spObtenerInfraccionesAFacturar do begin
        	//Cargo los parametros
            if deDesdeFecha.Date = NullDate then Parameters.ParamByName('@FechaInicial').Value := NULL
            else Parameters.ParamByName('@FechaInicial').Value := deDesdeFecha.Date;

            if deHastaFecha.Date = NullDate then Parameters.ParamByName('@FechaFinal').Value := NULL
            else Parameters.ParamByName('@FechaFinal').Value := deHastaFecha.Date;

            Parameters.ParamByName('@Patente').Value := edPatente.Text;
            Parameters.ParamByName('@CodigoConcesionaria').Value := AsignarValorComboEstado(cbConcesionaria);
            Open;

            if Eof then ShowMessage(MSG_INFRACCIONES_NO_ENCONTRADAS);
            while not Eof do begin
            	cldInfracciones.Append;
                cldInfracciones.FieldByName('Facturar').AsBoolean			:= False;
                cldInfracciones.FieldByName('CodigoInfraccion').AsInteger	:= FieldByName('CodigoInfraccion').Value;
                cldInfracciones.FieldByName('Patente').AsString				:= FieldByName('Patente').Value;
                cldInfracciones.FieldByName('Fecha').AsDateTime	            := FieldByName('FechaInfraccion').Value;
                cldInfracciones.FieldByName('MotivoAnulacion').AsString		:= FieldByName('MotivoAnulacion').Value;
                cldInfracciones.FieldByName('Nombre').AsString				:= IIF(FieldByName('Nombre').Value = null, '',FieldByName('Nombre').Value );
                cldInfracciones.FieldByName('NumeroDocumento').AsString		:= IIF(FieldByName('NumeroDocumento').Value = null, '', FieldByName('NumeroDocumento').Value);
                cldInfracciones.FieldByName('PrecioInfraccion').AsInteger	:= IIF(FieldByName('PrecioInfraccion').Value = null, 0,FieldByName('PrecioInfraccion').Value);
                cldInfracciones.FieldByName('Numeroconvenio').Asstring		:= IIF(FieldByName('NumeroConvenio').Value = null, '', FieldByName('NumeroConvenio').Value);
                cldInfracciones.FieldByName('Codigoconvenio').AsInteger		:= IIF(FieldByName('CodigoConvenio').Value = null , 0 , FieldByName('CodigoConvenio').Value);
                if FieldByName('FechaAnulacion').Value <> NULL then begin
                	cldInfracciones.FieldByName('FechaAnulacion').AsDateTime := FieldByName('FechaAnulacion').AsDateTime;
                end;

                cldInfracciones.FieldByName('CodigoMotivoAnulacion').AsInteger := IIF(FieldByName('CodigoMotivoAnulacion').Value = null , 0 , FieldByName('CodigoMotivoAnulacion').Value);
                cldInfracciones.FieldByName('CodigoTarifaDayPassBALI').AsInteger := FieldByName('CodigoTarifaDayPassBALI').AsInteger;

                cldInfracciones.FieldByName('TipoComprobanteFiscal').AsString := FieldByName('TipoComprobanteFiscalDesc').AsString;
                cldInfracciones.FieldByName('NumeroComprobanteFiscal').AsInteger := FieldByName('NumeroComprobanteFiscal').AsInteger;

                cldInfracciones.FieldByName('CodigoEstadoInterno').AsInteger := FieldByName('CodigoEstadoInterno').AsInteger;
                cldInfracciones.FieldByName('CodigoEstadoExterno').AsInteger := FieldByName('CodigoEstadoExterno').AsInteger;
                cldInfracciones.FieldByName('EstadoCN').AsString             := FieldByName('EstadoCN').AsString;
                cldInfracciones.FieldByName('EstadoIF').AsString             := FieldByName('EstadoIF').AsString;
                cldInfracciones.FieldByName('EsFacturable').AsVariant        := FieldByName('EsFacturable').AsVariant;
                cldInfracciones.FieldByName('TipoInfraccion').AsString       := FieldByName('TipoInfraccion').AsString;
                cldInfracciones.FieldByName('CodigoTipoInfraccion').AsInteger:= FieldByName('CodigoTipoInfraccion').AsInteger;  
                cldInfracciones.FieldByName('Categoria').AsInteger           := FieldByName('Categoria_RNVM').AsInteger;        
                cldInfracciones.FieldByName('ConceptoMoroso').AsInteger      := FieldByName('ConceptoMoroso').AsInteger;        
                cldInfracciones.Post;
                Next;
            end;

            cldInfracciones.AfterOpen   := ClientDataSetAfterOpenGenerico;
            cldInfracciones.AfterClose  := ClientDataSetAfterOpenGenerico; 
            cldInfracciones.AfterScroll := ClientDataSetAfterOpenGenerico; 
            cldInfracciones.First;
            dblInfracciones.Repaint;                                       

            FGrillaDesmarcada := False;
          end; {with}

    except on e: exception do begin
          Result := False;
          MsgBoxErr(ERROR_OBT_INFRACCIONES, e.message, caption, MB_ICONSTOP);
      end;
    end;

    cldInfracciones.EnableControls;
    dblInfracciones.Invalidate;
	Screen.Cursor := crDefault;
end;

{
    Procedure Name: dblInfraccionesDrawText
    Parameters : fmalisia
    Author : fmalisia
    Date Created : 08/12/2005

    Description : Muestro las infracciones en la grilla

    Revision : 12
        Author: pdominguez
        Date: 01/06/2010
        Description: Infractores Fase 2
            - Se mejora la visualizaci�n de los registros en la rejilla.
}
procedure TFormFacturacionInfracciones.dblInfraccionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: string; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin

        if not InfraccionEsFacturable then begin
            Sender.Canvas.Font.Style := Sender.Canvas.Font.Style + [fsBold];
            if Not (odSelected in State) then begin
                if cldInfracciones.FieldByName('CodigoEstadoInterno').AsInteger <> CONST_ESTADO_INFRACCION_INTERNO_PENDIENTE then
                    Sender.Canvas.Font.Color := clRed
                else Sender.Canvas.Font.Color := clMaroon;
            end
            else begin
                if (odFocused in State) then begin
                    if cldInfracciones.FieldByName('CodigoEstadoInterno').AsInteger <> CONST_ESTADO_INFRACCION_INTERNO_PENDIENTE then
                        Sender.Canvas.Brush.Color := clRed
                    else Sender.Canvas.Brush.Color := clMaroon;
                end else Sender.Canvas.Brush.Color := clGray;
            end;
        end;

        if (Column.FieldName = 'Facturar') then begin
	            Text := '';
	            Canvas.FillRect(Rect);
	            bmp := TBitMap.Create;
                try
                    DefaultDraw := False;
                    if (cldInfracciones.FieldByName('Facturar').AsBoolean ) then begin
                        Img_Tilde.GetBitmap(0, Bmp);
                    end else begin
                        Img_Tilde.GetBitmap(1, Bmp);
                    end;

                    Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
                finally
                    bmp.Free;
                end;
        end;        
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: dblInfraccionesKeyDown
  Author:    fmalisia
  Date Created: 08/12/2005
  Description:  Marco la infraccion para facturar
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.dblInfraccionesKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    if Key = VK_SPACE then begin
        dblInfraccionesDblClick(Sender);
    end;
end;

procedure TFormFacturacionInfracciones.edPatenteChange(Sender: TObject);
begin
    if ActiveControl = Sender then begin
		FPuedeSeleccionarInfracciones := False;
        btnSeleccionarInfracciones.Enabled := False;
        FGrillaDesmarcada := False;
		cldInfracciones.EmptyDataSet;
        dblInfracciones.Invalidate;
    	LimpiarDatosComprobante(False);
    	LimpiarDatosCliente();
        FInfraccionesMorosas := 0;
        FInfraccionesNormales := 0;             
        if not btnBuscar.Default then begin     
            btnBuscar.Default := True;          
            btnBuscarConvenio.Default := False; 
        end;                                    
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: dblInfraccionesDblClick
  Author:    fmalisia
  Date Created: 08/12/2005
  Description:  Marco la infraccion para facturar
  Parameters: None
  Return Value: Boolean

  Revision : 1
      Author : mpiazza
      Date : 20/05/2009
      Description : ss-784 se le agrega validacion de controles

-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.dblInfraccionesDblClick(Sender: TObject);
var
    PosPatente: Integer;
    Puntero: TBookmark;   
begin
    if cldInfracciones.IsEmpty then Exit;
    if not InfraccionEsFacturable then Exit;
    if not FPuedeSeleccionarInfracciones then Exit;
    if FPuedeSeleccionarInfracciones and
       not btnSeleccionarInfracciones.Enabled then Exit;                              

    try
        Puntero := cldInfracciones.GetBookmark;
        //Marco la infraccion para facturar
        with cldInfracciones do begin
            Edit;
            FieldByName('Facturar').AsBoolean := not FieldByName( 'Facturar' ).AsBoolean;
            Post;
        end;

        if not cldInfracciones.FieldByName('Facturar').AsBoolean then begin
            Dec(FInfraccionesSeleccionadas);

            if cldInfracciones.FieldByName('CodigoTipoInfraccion').AsInteger = FCodigoInfraccionNormal
            then Dec(FInfraccionesNormales)                                                              
            else Dec(FInfraccionesMorosas);                                                              

            if not cldInfracciones.Locate('Patente;Facturar', VarArrayOf([cldInfracciones.FieldByName('Patente').AsString,1]),[]) then begin
                PosPatente := FListaPatentes.IndexOf(cldInfracciones.FieldByName('Patente').AsString);
                if PosPatente <> -1 then begin
                    FListaPatentes.Delete(PosPatente);
                end;
            end;

        end
        else begin
            Inc(FInfraccionesSeleccionadas);
            PosPatente := FListaPatentes.IndexOf(cldInfracciones.FieldByName('Patente').AsString);

            if PosPatente = -1 then begin
                FListaPatentes.Add(cldInfracciones.FieldByName('Patente').AsString)
            end;
            
            if cldInfracciones.FieldByName('CodigoTipoInfraccion').AsInteger = FCodigoInfraccionNormal
            then Inc(FInfraccionesNormales)                                                               
            else Inc(FInfraccionesMorosas);                                                               
            
        end;
    finally
        if cldInfracciones.BookmarkValid(Puntero) then begin
            cldInfracciones.GotoBookmark(Puntero);
            cldInfracciones.FreeBookmark(Puntero);
        end;
    end;
end;

procedure TFormFacturacionInfracciones.btnValorizarClick(Sender: TObject);
resourcestring
    MSG_SELECCION_INFRA_INVALIDA  = 'Se deben seleccionar infracciones de un mismo tipo por vez';  
begin
    if pgCentral.ActivePage = tsFacturacionPatente then begin                                      
        if ((FInfraccionesMorosas > 0) and (FInfraccionesNormales > 0)) then begin                 
            MsgBox(MSG_SELECCION_INFRA_INVALIDA, Caption, MB_ICONINFORMATION + MB_OK);             
            Exit;                                                                                  
        end;                                                                                       
        if not cldInfracciones.Active then Exit;                                                   
        Valorizar;                                                                                 
    end else begin                                                                                 
        if ((FInfraccionesMorosasPorRut > 0) and (FInfraccionesNormalesPorRut > 0)) then begin     
            MsgBox(MSG_SELECCION_INFRA_INVALIDA, Caption, MB_ICONINFORMATION + MB_OK);             
            Exit;                                                                                  
        end;                                                                                       
        if not cldInfraccionesPorRut.Active then Exit;                                             
        Facturar;                                                                                  
    end;                                                                                           
    
end;
// Fin Bloque SS_1045_CQU_20120604 (btnValorizarClick)

{
    Function Name: InfraccionEsFacturable
    Parameters : None
    Return Value: Boolean
    Author :
    Date Created :

    Description : Devuelve si es posible o no facturar la infracci�n.

    Revision : 1
        Author: pdominguez
        Date: 01/06/2010
        Description: Infractores Fase 2
            - Se actualiza la l�gica a trav�s del Estado Interno y el campo nuevo
            EsFacturable.

    Firma       :   SS_1045_CQU_20120604
    Description :   Verifica el TAb activo para usar el objeto correcto
}
function TFormFacturacionInfracciones.InfraccionEsFacturable: boolean;
begin
    if pgCentral.ActivePage = tsFacturacionPatente then begin
        Result :=
            (cldInfracciones.FieldByName('CodigoEstadoInterno').AsInteger = CONST_ESTADO_INFRACCION_INTERNO_PENDIENTE) and
            cldInfracciones.FieldByName('EsFacturable').AsBoolean;
    end else begin
        Result :=
            (cldInfraccionesPorRut.FieldByName('CodigoEstadoInterno').AsInteger = CONST_ESTADO_INFRACCION_INTERNO_PENDIENTE) and
            cldInfraccionesPorRut.FieldByName('EsFacturable').AsBoolean;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnBuscarConvenioClick
  Author:
  Date:
  Description	Busca los datos asociados a un convenio

  Firma         :   SS_1045_CQU_20120604
  Description   :   Agrega llamada a btnBuscarRut cuando se agrega un convenio nuevo
                    Habilita o deshabilita btnSeleccionarInfracciones dependiendo de la p�gina activa
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.btnBuscarConvenioClick(Sender: TObject);
resourcestring
    MSG_RESULTADO_BUSQUEDA = 'No se han encontrado datos para el convenio solicitado';
    MSG_VALIDAR_RUT        = 'Se debe ingresar un n�mero de documento (RUT) para seleccionar las infracciones a valorizar';
    TXT_ATENCION           = 'Atenci�n';
    MSG_CAPTION_VALIDAR		 = 'Validar RUT %S';
    MSG_ALERT_RUT_SIN_CONVENIO  = 'El RUT ingresado %S no posee un convenio activo. �Desea ingresar un convenio para valorizar las infracciones?';     //SS_945    20110107
    MSG_SQL 	= 'SELECT ISNULL (NumeroDocumento, '''') FROM Personas WHERE CodigoPersona = (SELECT CodigoCliente FROM Convenio WHERE NumeroConvenio = ''%s'')';
    MSG_CLIENTE_CON_DEUDA	= 'Atenci�n !!' + CRLF + 'El cliente tiene deuda vencida e impaga';
    MSG_CLIENTE_EN_LEGALES	= 'Atenci�n !!' + CRLF + 'El cliente est� en Legales';					 	  
    MSG_SQL_TIENE_DEUDA		= 	'IF EXISTS( ' +                                                        		  
                                '			SELECT	1 ' +                                              		  
                                '			FROM	Convenio c (NOLOCK) ' +                                   
                                '						INNER JOIN Comprobantes co (NOLOCK) ON ' +            
                                '								c.CodigoConvenio = co.CodigoConvenio ' +      
                                '			WHERE	c.CodigoCliente = %d ' +                                  
                                '			AND		c.SaldoConvenio > 0 ' +                                   
                                '			AND		co.TipoComprobante = ''NK'' ' +                           
                                '			AND		co.Saldo > 0 ' +                                          
                                '			AND		co.FechaVencimiento < GETDATE() ' +                       
                                '			AND		co.EstadoPago <> ''A'' ' +                                
                                '		  ) ' +                                                               
                                '	SELECT 1 ' +                                                              
                                ' ELSE ' +                                                                    
                                '	SELECT 0 ';                                                               
    MSG_SQL_ESTA_LEGALES	=	'IF EXISTS( ' +                                                               
    							'			SELECT	1 ' +                                                     
                                '			FROM	ConveniosCarpetaLegal (NOLOCK) ' +                        
                                '			WHERE	NumeroDocumento = ''%s'' ' +                              
                                '			AND		FechaHoraEnvioLegales IS NOT NULL ' +                     
                                '			AND		FechaHoraSalidaLegales IS NULL ' +                        
								'			AND		Eliminado IS NULL ' +                                     
                                '		  ) ' +                                                               
                                '	SELECT 1 ' +                                                              
                                ' ELSE ' +                                                                    
                                '	SELECT 0 ';                                                               

    MSG_CONTINUAR			= '�Desea continuar el proceso de facturaci�n ?';
var
	Documento: String;
    CodigoConvenio: Variant;
    DescripcionCalle: AnsiString;
    f: TFormSolicitudContacto;
    FuenteSolicitud: Integer;
    PuntoEntrega: integer;
    ClienteTieneDeuda, ClienteEstaEnLegales,
    PuntoVenta: integer;
    mKey: Word;
    Aux : string;
    val : Integer;
begin
    LimpiarDatosCliente();
    FGrillaDesmarcada := False;
    
    // Me fijo si tengo que buscar por convenio: Hace este filtro primero - Para enviar al procedure el RUT
    if Trim(peRUTCliente.Text) = EmptyStr then begin
        if Trim(txtNumeroConvenio.Text) <> EmptyStr then begin
            // voy buscando el cliente que tenga este convenio, si existe pongo su RUT en el buscador
            Documento := Trim( QueryGetValue (DMConnections.BaseCAC, Format(MSG_SQL, [txtNumeroConvenio.Text])));
            if Documento <> '' then begin
                peRUTCliente.Text := Documento;
                peRUTCliente.setFocus;
            end else begin
                MsgBox(MSG_RESULTADO_BUSQUEDA, TXT_ATENCION, MB_ICONINFORMATION);
                Aux := txtNumeroConvenio.Text;
                LimpiarDatosComprobante(True);
                LimpiarDatosCliente();
                txtNumeroConvenio.Text := Aux;
                txtNumeroConvenio.SetFocus;
                Exit;
            end;
        end
        else begin
            MsgBox(MSG_VALIDAR_RUT, Caption, MB_ICONEXCLAMATION);
            Exit;
        end;
    end;

    // Ahora busco los datos del cliente - Se ejecuta si no se ingreso un Convenio, o se ingres� un Convenio correcto
	Screen.Cursor := crHourglass;
    try
        LimpiarDatosCliente();
        cbConveniosCliente.Items.Clear;
        // Obtenemos el cliente seleccionado
        with spObtenerCliente do begin
            Close;
            Parameters.ParamByName('@CodigoCliente').Value   := NULL;
            Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
            Parameters.ParamByName('@NumeroDocumento').Value := iif(peRUTCLiente.Text <> EmptyStr,PadL(Trim(peRUTCLiente.Text), 9, '0'), EmptyStr);
            Open;
        end;

        ClienteTieneDeuda		:= QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_TIENE_DEUDA, [spObtenerCliente.FieldByName('CodigoCliente').AsInteger]));
        ClienteEstaEnLegales	:= QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_ESTA_LEGALES, [spObtenerCliente.FieldByName('NumeroDocumento').AsString]));
        if ClienteTieneDeuda = 1 then begin
            if MsgBox(MSG_CLIENTE_CON_DEUDA + CRLF + CRLF + MSG_CONTINUAR,Caption, MB_ICONWARNING + MB_YESNO) = IDNO then Exit;
        end;
        if ClienteEstaEnLegales = 1 then begin
            if MsgBox(MSG_CLIENTE_EN_LEGALES + CRLF + CRLF + MSG_CONTINUAR, Caption, MB_ICONWARNING + MB_YESNO) = IDNO then Exit;
        end;

        CargarConveniosRUT(DMCOnnections.BaseCAC,cbConveniosCliente, 'RUT',
        PadL(peRUTCLiente.Text, 9, '0' ), False, 0 );
        FListaConveniosBaja.Clear;
        for  val := 0 to cbConveniosCliente.Items.Count - 1 do begin
            if Pos( TXT_CONVENIO_DE_BAJA, cbConveniosCliente.Items[val].Caption ) > 0 then  begin
                FListaConveniosBaja.Add(Trim(IntToStr(cbConveniosCliente.Items[val].value)));
            end
        end;

        if cbConveniosCliente.Items.Count < 1 then begin
            if trim(txtNumeroConvenio.Text) <> '' then begin
                cbConveniosCliente.Clear;
                LimpiarDatosCliente();
                Exit;
            end
            else cbConveniosCliente.ItemIndex := 0;
        end;

        cbConveniosClienteChange(cbConveniosCliente);
        // Mostramos el nombre completo del cliente
        if (spObtenerCliente.FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
        	lblDatoApellidoNombre.Caption :=  ArmaNombreCompleto(	Trim(spObtenerCliente.FieldByName('Apellido').asString),
              														Trim(spObtenerCliente.FieldByName('ApellidoMaterno').asString),
                                                                    Trim(spObtenerCliente.FieldByName('Nombre').asString))
        else
            lblDatoApellidoNombre.Caption :=  ArmaRazonSocial(		Trim(spObtenerCliente.FieldByName('Apellido').asString),
              														Trim(spObtenerCliente.FieldByName('ApellidoMaterno').asString));

        // Mostramos el domicilio Personal
        DescripcionCalle := ObtenerDescripCalle(	DMConnections.BaseCAC,
          											PAIS_CHILE,
              										Trim(spObtenerCliente.FieldByName('CodigoRegion').asString),
                                                    Trim(spObtenerCliente.FieldByName('CodigoComuna').asString),
              										spObtenerCliente.FieldByName('CodigoCalle').asInteger,
              										Trim(spObtenerCliente.FieldByName('CalleDesnormalizada').asString));

        lblDatoDomicilio.Caption := ArmarDomicilioSimple(	DMConnections.BaseCAC,
        													DescripcionCalle,
          													Trim(spObtenerCliente.FieldByName('Numero').asString),
                                                            0,
                                                            '',
          													Trim(spObtenerCliente.FieldByName('detalle').asString));

        lblDatoComuna.Caption := BuscarDescripcionComuna(	DMConnections.BaseCAC,
          													PAIS_CHILE,
                                                            Trim(spObtenerCliente.FieldByName('Codigoregion').asString),
          													Trim(spObtenerCliente.FieldByName('CodigoComuna').asString));
                                                            
        lblDatoRegion.Caption := BuscarDescripcionRegion(	DMConnections.BaseCAC,
        													PAIS_CHILE,
          													Trim(spObtenerCliente.FieldByName('CodigoRegion').asString));

        if  (cbConveniosCliente.Items.Count = 0) or
            (cbConveniosCliente.Items.Count = FListaConveniosBaja.Count) then
        begin
        	Aux := QueryGetValue(	DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
            						StrLeft(peRUTCliente.Text, (Length( peRUTCliente.Text) -1)) +
            						''',''' +  StrRight(peRUTCliente.Text, 1) + '''');
            if ( Aux = '0') then begin
                MsgBoxBalloon(MSG_VALIDAR_RUT ,
                Format(MSG_CAPTION_VALIDAR,[peRUTCliente.text]), MB_ICONSTOP, peRUTCliente);
                exit;
            end;

            if Trim(peRUTCliente.Text) <> '' then begin
                IF MsgBox(format(MSG_ALERT_RUT_SIN_CONVENIO, [StrLeft(peRUTCliente.Text, (Length( peRUTCliente.Text) -1)) + '-' + StrRight(peRUTCliente.Text, 1)]), TXT_ATENCION , MB_YESNO) = IDYES then begin

                    PuntoEntrega:= ApplicationIni.ReadInteger('General', 'PuntoEntrega', -1);
                    PuntoVenta:= ApplicationIni.ReadInteger('General', 'PuntoVenta', -1);
                    FuenteSolicitud := applicationini.ReadInteger('General', 'FuenteSolicitud', FUENTE_CALLCENTER);
                    if not VerificarPuntoEntregaVenta(DMConnections.BaseCAC, PuntoEntrega, PuntoVenta) then begin
                        MsgBox(MSG_ERROR_PUNTO_ENTREGA_VENTA, self.Caption, MB_ICONWARNING);
                        Exit;
                    end;

                    Application.CreateForm(TFormSolicitudContacto,f);
                    f.BorderStyle := bsDialog;
                    f.FormStyle := fsNormal;
                    f.Visible := true;
                    f.Enabled := true;
                    if not f.Inicializa(false, 'Ingresar Contrato', PuntoEntrega, PuntoVenta, -1, scAlta, -1, INTERMEDIO, FuenteSolicitud, True, True) then begin
                        f.Release;
                    end else begin
                        f.FreDatoPersona.txtDocumento.Text := peRUTCliente.Text;
                        mKey := 13;
                        f.FreDatoPersonatxtDocumentoKeyUp(self, mKey, []);
                        if f.ShowModal = mrok then begin
                            mKey := 13;
                        end;

                        if pgCentral.ActivePage = tsFacturacionRut then
                            btnBuscarRut.Click;
                    end;
                end;
            end;
        end else begin
            if pgCentral.ActivePage = tsFacturacionPatente then
                btnSeleccionarInfracciones.Enabled := True
            else
                btnSeleccionarInfracciones.Enabled := False;
        end;

    finally
		Screen.Cursor := crDefault;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    fmalisia
  Date Created: 08/12/2005
  Description:  Permito Cerrar el formulario
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
                    	btnSeleccionarInfraccionesClick
  Author: mbecerra
  Date: 02-Julio-2009
  Description:		Habilita la grilla para selecci�n de infracciones

  Firma         :   SS_1045_CQU_20120604
  Description   :   Modifica la llamada a un procedure el cual pas� a ser gen�rico
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.btnSeleccionarInfraccionesClick(Sender: TObject);
var
    HabilitarObjeto : Boolean;
begin
    if not FPuedeSeleccionarInfracciones then begin
        FPuedeSeleccionarInfracciones := True;
        dblInfracciones.Enabled := True;
        if pgCentral.ActivePage = tsFacturacionPatente then begin
            PoneColorGrilla(clWhite, dblInfracciones);
            HabilitarObjeto := True;
        end else begin
            PoneColorGrilla(clWhite, dblInfraccionesPorRut);
            HabilitarObjeto := False;
        end;
        btnSeleccionarInfracciones.Enabled := HabilitarObjeto;
    end;
end;



{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    fmalisia
  Date Created: 08/12/2005
  Description:  Lo libero de memoria
  Parameters: None
  Return Value: None

  Firma         :   SS_1045_CQU_20120604
  Description   :   Libera las variables con la lista de patentes
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
    FListaConveniosBaja.Free;  
    FListaPatentes.Free;       
    FListaPatentesPorRut.Free; 
    FListaConcesionarias.Free; 
    FListaCategorias.Free;
end;

procedure TFormFacturacionInfracciones.cbConcesionariaChange(Sender: TObject);
begin
    if ActiveControl = Sender then begin
        FPuedeSeleccionarInfracciones := False;
        FGrillaDesmarcada := False;
        // Verifico qu� p�gina est� activa
        if (pgCentral.ActivePage = tsFacturacionPatente) then  begin
            cldInfracciones.EmptyDataSet;
            dblInfracciones.Invalidate;
        end else begin
            if cldInfraccionesPorRut.Active then cldInfraccionesPorRut.Close;
            dblInfraccionesPorRut.Invalidate;
        end;
        LimpiarDatosComprobante(False);
        LimpiarDatosCliente();
    end;
end;

procedure TFormFacturacionInfracciones.cbConveniosClienteChange(
  Sender: TObject);
begin
	MostrarDomicilioFacturacion;
end;

{------------------------------------------------------------------
        	cbConveniosClienteDrawItem

Author: mbecerra
Date: 30-Junio-2009
Description: 	(SS 784)	Se pide colorear los convenios de baja en ROJO.
-------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.cbConveniosClienteDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConveniosCliente.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;

{-----------------------------------------------------------------------------
  Procedure Name: ItemCBColor
  Author:    ddiaz
  Date Created: 18/09/2006
  Description: Pinta en el combobox los convenios con distinto solor seg�n el estado (Vigrnte o Baja)
                SS 0341
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.ItemCBColor;
begin
    with (cmbBox as TVariantComboBox) do begin
        Canvas.Brush.color := ColorFondo;
        Canvas.FillRect(R);
        Canvas.Font.Color := ColorTexto;
        Canvas.TextOut(R.Left, R.Top, Items[pos].Caption);
   end;
end;

procedure TFormFacturacionInfracciones.MostrarDomicilioFacturacion;
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
begin
    with spObtenerDomicilioFacturacion, Parameters do begin
        // Mostramos el domicilio de Facturaci�n
        Close;
        ParamByName('@CodigoConvenio').Value := cbConveniosCliente.Value;
        try
        	ExecProc;
            lblDatoDomicilioFact.Caption    := ParamByName('@Domicilio').Value;
            lblDatoComunaFact.Caption		:= ParamByName('@Comuna').Value;
            lblDatoRegionFact.Caption       := ParamByName('@Region').Value
        except
            on e: exception do begin
                MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : LimpiarRut
  Author        : CQuezadaI
  Date Created  : 07/06/2012
  Description   : Se limpian los campos del filtro por Rut
  Parameters    : None
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.LimpiarRut;
begin
    txtRutInput.Text := '';
    deDesdeFechaRut.Date := 0;
    deHastaFechaRut.Date := 0;
    FTotalFacturar := 0;
end;

{-----------------------------------------------------------------------------
  Function Name : dblInfraccionesPorRutColumns2HeaderClick
  Author        : PSeguiD
  Date Created  : 05/07/2012
  Description   : M�todo Gen�rico para ordenar las columnas de la grilla dblInfraccionesPorRut
  Parameters    : Sender : TObject
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.dblInfraccionesPorRutColumns2HeaderClick(Sender: TObject);
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        DBListExColumnHeaderClickGenerico(Sender);
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : dblInfraccionesPorRutDblClick
  Author        : CQuezadaI
  Date Created  : 07/06/2012
  Description   : Se marca o desmarca el registro para Facturar o no.
  Parameters    : Sender : TObject
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.dblInfraccionesPorRutDblClick(Sender: TObject);
    var
        PosPatente, PosConcesionaria: Integer;
        PosCategoria : Integer;
        Puntero: TBookmark;
begin
    if cldInfraccionesPorRut.IsEmpty then Exit;
    if not InfraccionEsFacturable then Exit;
    if not FPuedeSeleccionarInfracciones then Exit;

    try
        Puntero := cldInfraccionesPorRut.GetBookmark;
        //Marco la infraccion para facturar
        with cldInfraccionesPorRut do begin
            Edit;
            FieldByName('Facturar').AsInteger := iif(FieldByName('Facturar').AsInteger = 0, 1, 0);
            Post;
        end;

        if cldInfraccionesPorRutFacturar.AsInteger = 0 then begin
            Dec(FInfraccionesSeleccionadasPorRut);
            FTotalFacturar := FTotalFacturar - cldInfraccionesPorRutPrecioInfraccion.AsInteger;
            if (FTotalFacturar < 0) then FTotalFacturar := 0;

            if cldInfraccionesPorRut.FieldByName('CodigoTipoInfraccion').AsInteger = FCodigoInfraccionNormal    
            then Dec(FInfraccionesNormalesPorRut)                                                               
            else Dec(FInfraccionesMorosasPorRut);

            if not cldInfraccionesPorRut.Locate('Patente;Facturar', VarArrayOf([cldInfraccionesPorRutPatente.AsString, 1]),[]) then begin
                PosPatente := FListaPatentesPorRut.IndexOf(cldInfraccionesPorRutPatente.AsString);
                if PosPatente <> -1 then begin
                    FListaPatentesPorRut.Delete(PosPatente);
                end;
            end;

            if not cldInfraccionesPorRut.Locate('Facturar;CodigoConcesionaria', VarArrayOf([1, cldInfraccionesPorRutCodigoConcesionaria.AsString]),[]) then begin
                PosConcesionaria := FListaConcesionarias.IndexOf(cldInfraccionesPorRutCodigoConcesionaria.AsString);
                if PosConcesionaria <> -1 then begin
                    FListaConcesionarias.Delete(PosConcesionaria)
                end;
            end;

            if not cldInfraccionesPorRut.Locate('Categoria_RNVM;Facturar', VarArrayOf([cldInfraccionesPorRutCategoria_RNVM.AsString, 1]),[]) then begin // SS_660_CQU_20121010
                PosCategoria := FListaCategorias.IndexOf(cldInfraccionesPorRutCategoria_RNVM.AsString);                                                 // SS_660_CQU_20121010
                if PosCategoria <> -1 then begin                                                                                                        // SS_660_CQU_20121010
                    FListaCategorias.Delete(PosCategoria);                                                                                              // SS_660_CQU_20121010
                end;                                                                                                                                    // SS_660_CQU_20121010
            end;                                                                                                                                        // SS_660_CQU_20121010

        end
        else begin
            Inc(FInfraccionesSeleccionadasPorRut);
            PosPatente := FListaPatentesPorRut.IndexOf(cldInfraccionesPorRutPatente.AsString);
            FTotalFacturar := FTotalFacturar + cldInfraccionesPorRutPrecioInfraccion.AsInteger;

            if PosPatente = -1 then begin
                FListaPatentesPorRut.Add(cldInfraccionesPorRutPatente.AsString)
            end;

            PosConcesionaria := FListaConcesionarias.IndexOf(cldInfraccionesPorRutCodigoConcesionaria.AsString);
            if PosConcesionaria = -1 then begin
                FListaConcesionarias.Add(cldInfraccionesPorRutCodigoConcesionaria.AsString)
            end;

            if PosPatente = -1 then begin                                                                       
                FListaCategorias.Add(cldInfraccionesPorRutCategoria_RNVM.AsString)                              
            end;                                                                                                

            if cldInfraccionesPorRut.FieldByName('CodigoTipoInfraccion').AsInteger = FCodigoInfraccionNormal    
            then Inc(FInfraccionesNormalesPorRut)                                                               
            else Inc(FInfraccionesMorosasPorRut);
            
        end;
    finally
        if cldInfraccionesPorRut.BookmarkValid(Puntero) then begin
            cldInfraccionesPorRut.GotoBookmark(Puntero);
            cldInfraccionesPorRut.FreeBookmark(Puntero);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : dblInfraccionesPorRutDrawText
  Author        : CQuezadaI
  Date Created  : 07/06/2012
  Description   : Colorea la grilla y celdas seg�n los datos que el registro tiene.
  Parameters    : Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
                  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
                  var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.dblInfraccionesPorRutDrawText(
    Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
    State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
    var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    if not FFacturandoPorRut then begin

        with Sender do begin

            if not InfraccionEsFacturable then begin
                Sender.Canvas.Font.Style := Sender.Canvas.Font.Style + [fsBold];
                if Not (odSelected in State) then begin
                    if cldInfraccionesPorRut.FieldByName('CodigoEstadoInterno').AsInteger <> CONST_ESTADO_INFRACCION_INTERNO_PENDIENTE then
                        Sender.Canvas.Font.Color := clRed
                    else Sender.Canvas.Font.Color := clMaroon;
                end
                else begin
                    if (odFocused in State) then begin
                        if cldInfraccionesPorRut.FieldByName('CodigoEstadoInterno').AsInteger <> CONST_ESTADO_INFRACCION_INTERNO_PENDIENTE then
                            Sender.Canvas.Brush.Color := clRed
                        else Sender.Canvas.Brush.Color := clMaroon;
                    end else Sender.Canvas.Brush.Color := clGray;
                end;
            end;

            if (Column.FieldName = 'Facturar') then begin
                    Text := '';
                    Canvas.FillRect(Rect);
                    bmp := TBitMap.Create;
                    try
                        DefaultDraw := False;
                        if (cldInfraccionesPorRut.FieldByName('Facturar').AsInteger = 1) then begin
                            Img_Tilde.GetBitmap(0, Bmp);
                        end else begin
                            Img_Tilde.GetBitmap(1, Bmp);
                        end;

                        Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
                    finally
                        bmp.Free;
                    end;
            end;

            if (Column.FieldName = 'FechaInfraccion') then begin

                Text := FormatDateTime('dd-mm-yyyy hh:nn', cldInfraccionesPorRutFechaInfraccion.AsDateTime);
            end;

            if (Column.FieldName = 'FechaAnulacion') then begin

                Text := FormatDateTime('dd-mm-yyyy', cldInfraccionesPorRutFechaInfraccion.AsDateTime);
            end;

            if (Column.FieldName = 'PrecioInfraccion') then begin

                Text := FormatFloat(FORMATO_IMPORTE, cldInfraccionesPorRutPrecioInfraccion.AsInteger);
            end;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : dblInfraccionesPorRutKeyDown
  Author        : CQuezadaI
  Date Created  : 07/06/2012
  Description   : Verifica que la tecla presionada no se el "espacio" y ejecuta
                  el procedure dblInfraccionesPorRutDblClick()
  Parameters    : Sender: TObject; var Key: Word; Shift: TShiftState;
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.dblInfraccionesPorRutKeyDown(
    Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if Key = VK_SPACE then begin
        dblInfraccionesPorRutDblClick(Sender);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : pgCentralChange
  Author        : CQuezadaI
  Date Created  : 07/06/2012
  Description   : Al cambiar de TAB modifica el nombre de un bot�n y oculta un label
  Parameters    : Sender: TObject;
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.pgCentralChange(Sender: TObject);
var
    TextoBoton : string;
    Mostrar : Boolean;
begin
    // Verifico el TAB seleccionado
    if pgCentral.ActivePage = tsFacturacionPatente then begin
        TextoBoton  := 'Valorizar';
        Mostrar     := True;
    end else begin
        TextoBoton  := 'Facturar';
        Mostrar     := False;
    end;
    // Cambio el texto del bot�n
    btnValorizar.Caption := TextoBoton;
    // Muestro u Oculto el label
    btnSeleccionarInfracciones.Visible := Mostrar;
end;

{-----------------------------------------------------------------------------
  Function Name : pgCentralDrawTab
  Author        : CQuezadaI
  Date Created  : 07/06/2012
  Description   : Pinta el TAB de color plomo ya que al ejecutar aparece de color Blanco
  Parameters    : Control: TCustomTabControl; TabIndex: Integer;
                  const Rect: TRect; Active: Boolean
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.pgCentralDrawTab(
    Control: TCustomTabControl; TabIndex: Integer; const Rect: TRect;
    Active: Boolean);
begin
    case TabIndex of
        0: Control.Canvas.Brush.Color := clBtnFace;
        1: Control.Canvas.Brush.Color := clBtnFace;
    end;
    Control.Canvas.TextOut(Rect.left + 5, Rect.top + 3, pgCentral.Pages[tabindex].Caption);
    pgCentral.Pages[TabIndex].Brush.Color:=Control.Canvas.Brush.Color;
end;

{-----------------------------------------------------------------------------
  Function Name : CargarInfraccionesPorRut
  Author        : CQuezadaI
  Date Created  : 07/06/2012
  Description   : Busca las infracciones asociadas al Rut indicado
  Parameters    : None
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
function TFormFacturacionInfracciones.CargarInfraccionesPorRut;
resourcestring
    ERROR_OBT_INFRACCIONES 	=  'Ha ocurrido un error al obtener las infracciones';
    MSG_INFRACCIONES_NO_ENCONTRADAS = 'No se han encontrado infracciones con este criterio de b�squeda';
    MSG_MOROSOS                   = 'ATENCION: Tiene infracciones por morosidad (lista amarilla),'  
                                    + ' estas no contemplan Intereses y Gastos de cobranza.';
var
    Patente : string;
    PosPatente, PosConcesionaria : Integer;
    PosCategoria : Integer;
begin
    Screen.Cursor := crHourGlass;
    Result := True;
    try
        try
            FListaPatentesPorRut.Clear;
            FListaConcesionarias.Clear;
            FInfraccionesSeleccionadasPorRut := 0;
            if cldInfraccionesPorRut.Active then cldInfraccionesPorRut.Close;
            FTotalFacturar := 0;
            FInfraccionesNormalesPorRut := 0;   
            FInfraccionesMorosasPorRut  := 0;   
            FListaCategorias.Clear;

            TPanelMensajesForm.MuestraMensaje('Cargando Infracciones...', True);

            with spObtenerInfraccionesAFacturarPorRut do begin
                // Aumento el TimeOut ya que se cae en aquellos Rut con varios convenios
                CommandTimeout := 180;
                //Cargo los parametros
                if deDesdeFechaRut.Date = NullDate then Parameters.ParamByName('@FechaInicial').Value := NULL
                else Parameters.ParamByName('@FechaInicial').Value := deDesdeFechaRut.Date;

                if deHastaFechaRut.Date = NullDate then Parameters.ParamByName('@FechaFinal').Value := NULL
                else Parameters.ParamByName('@FechaFinal').Value := deHastaFechaRut.Date;

                if cbConcesionariaRut.ItemIndex < 0 then Parameters.ParamByName('@CodigoConcesionaria').Value := NULL
                else Parameters.ParamByName('@CodigoConcesionaria').Value := AsignarValorComboEstado(cbConcesionariaRut);

                Parameters.ParamByName('@Rut').Value := txtRutInput.Text;
                Parameters.ParamByName('@TraerListaAmarilla').Value := FTraerListaAmarilla;
            end;

            cldInfraccionesPorRut.DisableControls;
            cldInfraccionesPorRut.AfterOpen   := nil;
            cldInfraccionesPorRut.AfterClose  := nil;
            cldInfraccionesPorRut.AfterScroll := nil;

            cldInfraccionesPorRut.open;

            TPanelMensajesForm.MuestraMensaje('Infracciones Cargadas. Filtrando Infracciones ...');

            cldInfraccionesPorRut.Filter := 'Facturar <> 0';
            cldInfraccionesPorRut.Filtered := True;

            TPanelMensajesForm.MuestraMensaje('Infracciones Filtradas. Generando Lista Patentes ...');

            cldInfraccionesPorRut.First;

            while not cldInfraccionesPorRut.Eof do begin
                if cldInfraccionesPorRutFacturar.AsInteger = 1 then begin
                    Inc(FInfraccionesSeleccionadasPorRut);
                    // Sumo el valor de la infracci�n
                    FTotalFacturar := FTotalFacturar + cldInfraccionesPorRutPrecioInfraccion.AsInteger;
                    // Busco que la patente no exista en el StringList
                    PosPatente := FListaPatentesPorRut.IndexOf(cldInfraccionesPorRutPatente.AsString);
                    // Si la patente no existe, la agrego.
                    if PosPatente = -1 then FListaPatentesPorRut.Add(cldInfraccionesPorRutPatente.AsString);
                    // Busco la Concesionaria
                    PosConcesionaria := FListaConcesionarias.IndexOf(cldInfraccionesPorRutCodigoConcesionaria.AsString);
                    // Si la concesionaria no existe, la agrego.
                    if PosConcesionaria = -1 then FListaConcesionarias.Add(cldInfraccionesPorRutCodigoConcesionaria.AsString);
                    // Busco la Categoria
                    if PosPatente = -1 then FListaCategorias.Add(cldInfraccionesPorRutCategoria_RNVM.AsString);
                    // Cuento las infracciones por Tipo
                    if cldInfraccionesPorRutCodigoTipoInfraccion.AsInteger = FCodigoInfraccionNormal    
                    then Inc(FInfraccionesNormalesPorRut)                                               
                    else Inc(FInfraccionesMorosasPorRut);
                end;

                cldInfraccionesPorRut.Next;
            end;

            cldInfraccionesPorRut.Filtered := False;
            cldInfraccionesPorRut.Filter := '';

            cldInfraccionesPorRut.AfterOpen   := ClientDataSetAfterOpenGenerico;
            cldInfraccionesPorRut.AfterClose  := ClientDataSetAfterOpenGenerico;
            cldInfraccionesPorRut.AfterScroll := ClientDataSetAfterOpenGenerico;
            cldInfraccionesPorRut.EnableControls;
            cldInfraccionesPorRut.First;

            FPuedeSeleccionarInfracciones := True;

        except
            on e: exception do begin
                Result := False;
                TPanelMensajesForm.OcultaPanel;
                MsgBoxErr(ERROR_OBT_INFRACCIONES, e.message, caption, MB_ICONSTOP);
            end;
        end;
    finally                                                                                                                                
        TPanelMensajesForm.OcultaPanel;

        if (FInfraccionesMorosasPorRut > 0) then MsgBox(MSG_MOROSOS, Caption, MB_ICONINFORMATION + MB_OK);                                 

        if cldInfraccionesPorRut.Eof and Result then ShowMessage(MSG_INFRACCIONES_NO_ENCONTRADAS);                                         

        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : txtRutInputChange
  Author        : CQuezadaI
  Date Created  : 07/06/2012
  Description   : Limpia los datos ingresados cuando se ha cambiado el Rut
  Parameters    : Sender: TObject
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.txtRutInputChange(Sender: TObject);
begin
    if ActiveControl = Sender then begin
		FPuedeSeleccionarInfracciones := False;
        FGrillaDesmarcada := False;
        if cldInfraccionesPorRut.Active then cldInfraccionesPorRut.Close;
        dblInfraccionesPorRut.Invalidate;
        LimpiarDatosComprobante(False);
    	LimpiarDatosCliente();
        FInfraccionesMorosasPorRut  := 0;   
        FInfraccionesNormalesPorRut := 0;
        if not btnBuscarRut.Default then begin
            btnBuscarRut.Default := True;
            btnBuscarConvenio.Default := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : btnBuscarRutClick
  Author        : CQuezadaI
  Date Created  : 07/06/2012
  Description   : Realiza la b�squeda de registros por el Rut indicado
  Parameters    : Sender: TObject
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.btnBuscarRutClick(Sender: TObject);
resourcestring
    MESSAGE_RUT_VACIO 			    = 'Rut vac�o';
    MESSAGE_FECHA_DESDE_VACIA 		= 'Fecha desde vac�a';
    MESSAGE_HASTAA_DESDE_VACIA 		= 'Fecha hasta vac�a';
    MESSAGE_HASTA_MENOR_A_DESDE 	= 'Fecha hasta es menor a fecha desde ';
    MESSAGE_SIN_CONVENIO            = 'El rut ingresado no tiene convenio asociado' + CRLF + '�Desea crearle uno?';
    MSG_VALIDAR_RUT                 = 'El Rut ingresado no es v�lido';
begin
    // Valido que el Rut ingresado sea correcto
    if Not ValidarRUT(DMConnections.BaseCAC, Trim(txtRutInput.Text)) then begin
        MsgBox(MSG_VALIDAR_RUT, Caption, MB_ICONEXCLAMATION);
        Exit;
    end;

    //Verifica si son validos los datos para la busqueda
  	if not ValidateControls(	[txtRutInput,deHastaFechaRut],
    							[txtRutInput.Text <> '', deDesdeFechaRut.Date <= deHastaFechaRut.Date],
                                Caption,
                                [MESSAGE_RUT_VACIO, MESSAGE_HASTA_MENOR_A_DESDE ]) then Exit;

    if cldInfraccionesPorRut.Active then cldInfraccionesPorRut.Close;
    dblInfraccionesPorRut.Invalidate;
    LimpiarDatosComprobante(False);
    LimpiarDatosCliente();
    btnSeleccionarInfracciones.Enabled := False;

    if CargarInfraccionesPorRut() then begin
        //Poner en gris el color del DBList
        if (cldInfraccionesPorRut.RecordCount > 0) then begin
            PoneColorGrilla(clBtnFace, dblInfraccionesPorRut);
            gbDatosComprobante.Enabled := True;
            peRUTCliente.SetFocus;
        end;
        btnValorizar.Enabled := (cldInfraccionesPorRut.RecordCount>0);
    end;
    dblInfraccionesPorRut.Refresh;
    Application.ProcessMessages;
end;

{-----------------------------------------------------------------------------
  Function Name : Valorizar
  Author        : CQuezadaI
  Date Created  : 07/06/2012
  Description   : M�todo originalmente llamado btnValorizarClick (comentado m�s arriba),
                  dado que el bot�n Valorizar es usado con otros fines (en el TAB de facturaci�n por rut),
                  se decide eliminar la l�gica desde el bot�n y mover a un m�todo externo,
                  la l�gica no se ha modificado, es decir, est� tal cual estaba en el bot�n.
  Parameters    : None
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.Valorizar;
resourcestring
    MSG_MOVIMIENTOSCUENTAS	= 'Se han cargado correctamente los Movimientos Cuentas al Cliente';
    MSG_FACTURAR			= 'A continuaci�n se presentar� la p�gina del proceso de facturaci�n';
    MSG_NO_HAY_PATENTE		= 'No ha ingresado la patente';
    MSG_NO_HAY_CLIENTE		= 'No ha ingresado el Rut del Cliente';
    MSG_NO_HAY_CONVENIO		= 'No ha seleccionado el Convenio del Cliente';
    MSG_SELECCION_INVALIDA  = 'Se deben seleccionar infracciones de un �nico documento por vez' ;
	MSG_INFRA_CLIENTE		= 'Las Infracciones pertenecen a un cliente con convenio!';
	MSG_SELECCION_VACIA     = 'No se han seleccionado infracciones a valorizar' ;
	MSG_ERROR_ANULANDO	    = 'Se ha producido un error anulando las infracciones a facturar' ;
	MSG_NUEVO_PROCESO       = 'Desea facturar nuevamente?' ;
    MSG_CONVENIO_BAJA       = 'S�lo puede facturar a un convenio Vigente';
    MSG_SELECCION_INFRA_INVALIDA  = 'Se deben seleccionar infracciones de un mismo tipo por vez' ;                  
    MSG_MOROSOS                   = 'ATENCION: Las infracciones seleccionadas son por morosidad (lista amarilla),'  
                                    + ' debe revisar los Intereses y Gastos de cobranza.';                          
    MSG_SELECCION_CATEGORIA = 'S�lo se puede seleccionar infracciones de una �nica categor�a por vez';
var
    FFacturacionManual : TFacturacionManualForm;
	Aux,
	SumaPrecioInfraccion : int64;
	CodigoInfraccion, CodigoLineaComprobante,
    CantidadInfraccionesSeleccionadas: integer;
	PrecioInfraccion , ImporteGastosAdministrativos, ImporteIntereses: Extended;
	PrimerCaso: boolean;
	Nombre, Documento, Calle, CodigoRegion, CodigoComuna, Numero, Detalle: AnsiString;
	FechaEmision, FechaVencimiento: TDateTime;
    Puntero: TBookmark;
    CodigoTipoInfraccion, Categoria : Integer;
begin
    {ver si hay datos seleccionados}
    if not ValidateControls([edPatente, Panel3, Panel3, peRUTCliente, cbConveniosCliente, cbConveniosCliente],
                        	[	edPatente.Text <> '', cldInfracciones.RecordCount > 0,
                            	HayInfraccionesSeleccionadas(),
                            	peRUTCliente.Text <> '', cbConveniosCliente.Text <> '',
                                FListaConveniosBaja.IndexOf(cbConveniosCliente.Value) < 0 ],
                            Caption,
                            [	MSG_NO_HAY_PATENTE, MSG_SELECCION_VACIA,
                            	MSG_SELECCION_VACIA, MSG_NO_HAY_CLIENTE,
                            	MSG_NO_HAY_CONVENIO,
                                MSG_CONVENIO_BAJA] ) then Exit;

    try
        Puntero := cldInfracciones.GetBookmark;
        //Valorizar las infracciones

        // Contolar si hay algo seleccionado y si la selecci�n es v�lida
        cldInfracciones.DisableControls ;
        cldInfracciones.First;

        PrimerCaso 				:= True;
        SumaPrecioInfraccion 	:= 0;
        CantidadInfraccionesSeleccionadas := 0;
        PrecioInfraccion    := 0;
        Nombre              := '';
        CodigoInfraccion    := 0;
        ImporteIntereses	:= 0;
        CodigoTipoInfraccion:= 0;
        Categoria           := 0;
        TPanelMensajesForm.OcultaPanel;
        // Agergo el mensaje de morosidad                                                               
        if (FInfraccionesMorosas > 0) then MsgBox(MSG_MOROSOS, Caption, MB_ICONINFORMATION + MB_OK);

        while not cldInfracciones.Eof do begin
            if cldInfracciones.FieldByName('Facturar').AsBoolean then begin
                if Primercaso then begin
                    PrecioInfraccion := cldInfracciones.FieldByName('PrecioInfraccion').AsInteger;
                    Nombre 			 := cldInfracciones.FieldByName('Nombre').AsString;
                    Documento		 := cldInfracciones.FieldByName('Numerodocumento').AsString;
                    CodigoInfraccion := cldInfracciones.FieldByName('CodigoInfraccion').AsInteger;
                    PrimerCaso       := False;
                    CodigoTipoInfraccion := cldInfracciones.FieldByName('CodigoTipoInfraccion').AsInteger;  
                    Categoria        := cldInfracciones.FieldByName('Categoria').AsInteger;
                end
                else begin
                    if (Documento <> cldInfracciones.FieldByName('Numerodocumento').AsString) then begin
                          MsgBox(MSG_SELECCION_INVALIDA, Caption, MB_ICONINFORMATION + MB_OK);
                          cldInfracciones.EnableControls;
                          Exit;
                    end;

                    if (CodigoTipoInfraccion <> cldInfracciones.FieldByName('CodigoTipoInfraccion').AsInteger) then begin   
                          MsgBox(MSG_SELECCION_INFRA_INVALIDA, Caption, MB_ICONINFORMATION + MB_OK);                        
                          cldInfraccionesPorRut.EnableControls;                                                             
                          Exit;                                                                                             
                    end;                                                                                                    

                    if (Categoria <> cldInfracciones.FieldByName('Categoria').AsInteger) then begin                         
                          MsgBox(MSG_SELECCION_CATEGORIA, Caption, MB_ICONINFORMATION + MB_OK);                             
                          cldInfraccionesPorRut.EnableControls;                                                             
                          Exit;                                                                                             
                    end;

                end;


                if (CodigoTipoInfraccion = FCodigoInfraccionNormal) then begin                                                      
                    // Calcular los intereses                                                                                       
                    spCalcularIntereses.Close;                                                                                      
                    with spCalcularIntereses do begin                                                                               
                        Parameters.Refresh;                                                                                         
                        Parameters.ParamByName('@MontoBase').Value	:= cldInfracciones.FieldByName('PrecioInfraccion').AsInteger;   
                        Parameters.ParamByName('@FechaBase').Value	:= cldInfracciones.FieldByName('Fecha').AsDateTime;             
                        Parameters.ParamByName('@FechaDeCalculo').Value := NULL;		//NULL = tomar la fecha de hoy              
                        Parameters.ParamByName('@Intereses').Value	:= NULL;                                                        
                        ExecProc;                                                                                                   
                        Aux := Parameters.ParamByName('@Intereses').Value;                                                          
                        ImporteIntereses	:= ImporteIntereses + Aux;                                                              
                    end;

                    //Verifico que las infracciones no pertenecen a un cliente
                    //Informo que no pertenecen a un cliente
                    if (cldInfracciones.FieldByName('CodigoConvenio').AsInteger <> cbConveniosCliente.Value) //and	// TASK_144_MGO_20170301
                        //(cldInfracciones.FieldByName('CodigoConvenio').AsInteger <> 0)							// TASK_144_MGO_20170301
                    then begin
                        MsgBox(MSG_INFRA_CLIENTE, Self.Caption, MB_ICONINFORMATION + MB_OK);
                        Exit;                                                                                                       
                    end;

                end;                                                                                                                

                if (cldInfracciones.FieldByName('PrecioInfraccion').Value <> null) then begin
                    SumaPrecioInfraccion := SumaPrecioInfraccion + cldInfracciones.FieldByName('PrecioInfraccion').AsInteger;
                end;

                CantidadInfraccionesSeleccionadas := CantidadInfraccionesSeleccionadas + 1 ;
            end;

            cldInfracciones.Next;
        end;
    finally
        if cldInfracciones.BookmarkValid(Puntero) then cldInfracciones.GotoBookmark(Puntero);
        cldInfracciones.FreeBookmark(Puntero);
    end;

    //si no selecciono ninguna infraccion
    if CantidadInfraccionesSeleccionadas = 0 then begin
        //informo que no se selecciono ningua infraccion al operador
        MsgBox(MSG_SELECCION_VACIA, Caption, MB_ICONINFORMATION + MB_OK);
            cldInfracciones.EnableControls;
        Exit;
    end;


    // Obtenemos los datos de la persona y del domicilio
    try
        with ObtenerDatosPersonaNotaCobroInfraccion do begin
            Close;
            Parameters.ParamByName('@CodigoInfraccion').Value := CodigoInfraccion;
            Open;
            Nombre			:= FieldByName('Nombre').AsString;
            Documento		:= FieldByName('NumeroDocumento').AsString;
            Calle			:= FieldByName('Calle').AsString;
            CodigoRegion	:= FieldByName('CodigoRegion').AsString;
            CodigoComuna	:= FieldByName('CodigoComuna').AsString;
            Numero			:= FieldByName('Numero').AsString;
            Detalle			:= FieldByName('Detalle').AsString;
        end;
    except
        // No hacemos nada por que sin estos datos la nota de cobro infraccion se puede editar igual
    end;

    // se abre la pantalla donde se solicitan los datos para generar el comprobante
    if ObtenerDatosNotaCobroInfraccion(	peRUTCliente.Text,
    									cbConveniosCliente.Text,
                                        lblDatoDomicilioFact.Caption,
                                        lblDatoComunaFact.Caption,
                                        lblDatoRegionFact.Caption,
                                        lblDatoApellidoNombre.Caption,
                                        lblDatoDomicilio.Caption,
                                        lblDatoComuna.Caption,
                                        lblDatoRegion.Caption,
                                        SumaPrecioInfraccion,
                                        ImporteGastosAdministrativos,
                                        ImporteIntereses,
                                        FechaEmision,
                                        FechaVencimiento,
                                        CantidadInfraccionesSeleccionadas,  
                                        True,                               
                                        CodigoTipoInfraccion) then
    begin //facturar
        Screen.Cursor := crHourGlass;
        try
            FechaEmisionComprobante := FechaEmision;
            FechaVencimientoComprobante := FechaVencimiento;
            QueryExecute(DMConnections.BaseCAC, 'BEGIN TRAN spInfractores');

            with spGenerarMovimientosInfraccionPorConvenio , Parameters do begin
                Parameters.Refresh;
                ParamByName('@CodigoConvenio').Value		  := strtoint(cbConveniosCliente.value) ;
                ParamByName('@FechaFacturacion').value        := FechaEmision ;
                if (FInfraccionesNormales > 0) and (FInfraccionesMorosas = 0) then begin                
                    ParamByName('@TotalInfracciones').Value       := SumaPrecioInfraccion *  100;       
                    ParamByName('@TotalGastosCobranza').Value     := ImporteGastosAdministrativos * 100;
                    ParamByName('@TotalIntereses').Value          := ImporteIntereses * 100 ;           
                    ParamByName('@TotalInfraccionesMorosos').Value:= 0;                                 
                    ParamByName('@CodigoCategoria').Value         := 0;                                 
                    ParamByName('@CantInfraccionCategoria').Value := 0;                                 
                end else begin                                                                          
                    ParamByName('@TotalInfracciones').Value       := 0;                                 
                    ParamByName('@TotalGastosCobranza').Value     := 0;                                 
                    ParamByName('@TotalIntereses').Value          := 0;                                 
                    ParamByName('@TotalInfraccionesMorosos').Value:= SumaPrecioInfraccion *  100;       
                    ParamByName('@CodigoCategoria').Value         := Categoria;                         
                    ParamByName('@CantInfraccionCategoria').Value := CantidadInfraccionesSeleccionadas;
                end;
                ParamByName('@Usuario').Value                 := UsuarioSistema;
                ParamByName('@CodigoConcesionaria').Value     := AsignarValorComboEstado(cbConcesionaria);
                ParamByName('@NumeroMovimientoInfracciones').Value    := NULL;
                ParamByName('@NumeroMovimientoGastosCobranzas').Value := NULL;
                ParamByName('@NumeroMovimientoIntereses').Value       := NULL;
                ParamByName('@NumeroMovimientoInfraccionesMorosos').Value   := NULL;

                ExecProc ;
                if VarIsNull(ParamByName('@NumeroMovimientoInfracciones').Value) then
                    NumeroMovimientoInfracciones := 0
                else
                    NumeroMovimientoInfracciones    := ParamByName('@NumeroMovimientoInfracciones').Value  ;
                if VarIsNull(ParamByName('@NumeroMovimientoGastosCobranzas').Value) then
                    NumeroMovimientoGastosCobranzas := 0
                else
                    NumeroMovimientoGastosCobranzas := ParamByName('@NumeroMovimientoGastosCobranzas').Value;
                if VarIsNull(ParamByName('@NumeroMovimientoIntereses').Value) then
                    NumeroMovimientoIntereses       := 0
                else
                    NumeroMovimientoIntereses       := ParamByName('@NumeroMovimientoIntereses').Value ;

                if VarIsNull(ParamByName('@NumeroMovimientoInfraccionesMorosos').Value) then                        
                    NumeroMovimientoMorosos       := 0                                                              
                else                                                                                                
                    NumeroMovimientoMorosos       := ParamByName('@NumeroMovimientoInfraccionesMorosos').Value ;
            end;

            //Anular las infracciones
            cldInfracciones.First ;
            while not cldInfracciones.Eof do begin
                if cldInfracciones.FieldByName('Facturar').AsBoolean then begin
                  //Anulo la infraccion
                    with spActualizarInfraccionValorizada, Parameters do begin
                        ParamByName('@CodigoInfraccion').Value := cldInfracciones.FieldByName('CodigoInfraccion').AsInteger;
                        ParamByName('@CodigoLineaComprobante').Value := CodigoLineaComprobante;
                        ParamByName('@CodigoTarifaDayPassBALI').Value := cldInfracciones.FieldByName('CodigoTarifaDayPassBALI').AsInteger;
                        if NumeroMovimientoInfracciones > 0 then ParamByName('@NumeroMovimiento').Value := NumeroMovimientoInfracciones 
                        else ParamByName('@NumeroMovimiento').Value := NumeroMovimientoMorosos;
						ExecProc;
                    end;
                end;
                cldInfracciones.Next;
            end;
            QueryExecute(DMConnections.BaseCAC, 'COMMIT TRAN spInfractores');
        except
            on e: exception do begin
                QueryExecute(DMConnections.BaseCAC, 'ROLLBACK TRAN spInfractores');
                Screen.Cursor := crDefault;
                cldInfracciones.EnableControls;
                MsgBoxErr(MSG_ERROR_ANULANDO, e.message, caption, MB_ICONERROR);
                Exit;
            end;
        end;

        Screen.Cursor := crDefault;
        MsgBox(MSG_MOVIMIENTOSCUENTAS + CRLF + MSG_FACTURAR, Caption, MB_ICONINFORMATION);
        Application.CreateForm(TFacturacionManualForm, FFacturacionManual);
        FFacturacionManual.FormStyle := fsNormal;
        FFacturacionManual.Visible := False;
        if FFacturacionManual.Inicializar(TC_NOTA_COBRO_INFRACCIONES, False ) then begin
        	FFacturacionManual.peRUTCliente.Text := peRUTCliente.Text;
            FFacturacionManual.peRUTCliente.OnChange(nil);
            FFacturacionManual.btnBuscar.Click;
            FFacturacionManual.cbConveniosCliente.ItemIndex := FFacturacionManual.cbConveniosCliente.Items.IndexOfValue(cbConveniosCliente.Value);
            FFacturacionManual.cbConveniosCliente.OnChange(nil);
            FFacturacionManual.ShowModal;
        end;

        FFacturacionManual.Release;
        InicializarData();
        cldInfracciones.EnableControls;
    end
    else
        cldInfracciones.EnableControls;
end;

{-----------------------------------------------------------------------------
  Function Name : Facturar
  Author        : CQuezadaI
  Date Created  : 07/06/2012
  Description   : Realiza el proceso de Facturaci�n autom�ticamente cuando se est�
                  en la secci�n de Facturaci�n por Rut.
  Parameters    : None
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.Facturar;
resourcestring
    MSG_NO_HAY_CLIENTE		= 'No ha ingresado el Rut del Cliente';
    MSG_NO_HAY_CONVENIO		= 'No ha seleccionado el Convenio del Cliente';
    MSG_SELECCION_INVALIDA  = 'Se deben seleccionar infracciones de un �nico documento por vez' ;
	MSG_INFRA_CLIENTE		= 'Las Infracciones pertenecen a un cliente con convenio!';
	MSG_SELECCION_VACIA     = 'No se han seleccionado infracciones a valorizar' ;
	MSG_ERROR_ANULANDO	    = 'Se ha producido un error anulando las infracciones a facturar' ;
	MSG_NUEVO_PROCESO       = '�Desea facturar nuevamente?' ;
    MSG_CONVENIO_BAJA       = 'S�lo puede facturar a un convenio Vigente';
    FILTRO                  = 'Trim(Patente) = ''%s'' AND Facturar = True';
    FILTRO_CONCE_PATENTE    = 'Trim(Patente) = ''%s'' AND Facturar = True AND CodigoConcesionaria = %s ';
    MSG_CONFIRMACION        = 'Se van a Facturar %s Infracciones por un importe neto de %s' + CRLF + '�Confirma el proceso de Facturaci�n?';
    MSG_ERROR_PROCESO       = 'Ocurri� un error en el proceso';
    MSG_SELECCION_INFRA_INVALIDA  = 'Se deben seleccionar infracciones de un mismo tipo por vez' ;
    FILTRO_CATEGORIA        = 'Trim(Patente) = ''%s'' AND Facturar = True AND CodigoConcesionaria = %s AND Categoria_RNVM = %s';
var
    FFacturacionManual : TFacturacionManualForm;
	Aux, I, J,
	SumaPrecioInfraccion : int64;
	CodigoInfraccion, CodigoLineaComprobante,
    CantidadInfraccionesSeleccionadas, CodigoConcesionaria: integer;
	PrecioInfraccion , ImporteGastosAdministrativos, ImporteIntereses: Extended;
	PrimerCaso: boolean;
	Nombre, Documento, Calle, CodigoRegion, CodigoComuna, Numero, Detalle: AnsiString;
	FechaEmision, FechaVencimiento: TDateTime;
    Puntero: TBookmark;
    Patente : string;
    ano, mes, dia : Word;
    CodigoTipoInfraccion, Categoria : Integer;
    bError : Boolean;               
    Confirmacion : string;          
    bCargoFacturacion : Boolean;
begin

    {ver si hay datos seleccionados}
    if not ValidateControls([txtRutInput, pnlBotonesRut, pnlBotonesRut, peRUTCliente, cbConveniosCliente, cbConveniosCliente],
                        	[	txtRutInput.Text <> '',
                                cldInfraccionesPorRut.RecordCount > 0,                                                                          
                                HayInfraccionesSeleccionadas(),                                                                                 
                            	peRUTCliente.Text <> '',
                                cbConveniosCliente.Text <> '',
                                FListaConveniosBaja.IndexOf(cbConveniosCliente.Value) < 0],
                            Caption,
                            [	MSG_NO_HAY_CLIENTE,
                                MSG_SELECCION_VACIA,                                                                                            
                                MSG_SELECCION_VACIA,                                                                                            
                                MSG_NO_HAY_CLIENTE,
                            	MSG_NO_HAY_CONVENIO,
                                MSG_CONVENIO_BAJA]) then Exit;

    if cldInfraccionesPorRut.Active then begin
        if not ValidateControls([dblInfraccionesPorRut, dblInfraccionesPorRut, dblInfraccionesPorRut],
                                [HayInfraccionesSeleccionadas(),
                                cldInfraccionesPorRut.Active = True,
                                cldInfraccionesPorRut.RecordCount > 0 ],
                                Caption,
                                [MSG_SELECCION_VACIA, MSG_SELECCION_VACIA, MSG_SELECCION_VACIA])
        then Exit;
    end;

    // Despliego mensaje de confirmaci�n
    if (MsgBox(Format(MSG_CONFIRMACION, [FloatToStrF(FInfraccionesSeleccionadasPorRut, ffNumber, 9,0), FormatFloat(FORMATO_IMPORTE, FTotalFacturar)])
            , 'Atenci�n'
            , MB_YESNO + MB_ICONINFORMATION) = IDNO) then Exit
    else begin
        try
            Puntero := cldInfraccionesPorRut.GetBookmark;
            cldInfraccionesPorRut.DisableControls;
            try
                Screen.Cursor := crHourGlass;
                Enabled := False;
                TPanelMensajesForm.MuestraMensaje('Inicializando Proceso de Facturaci�n ...', True);
                FFacturandoPorRut := True;
                // Inicio la transaccci�n por si falla la facturaci�n
                DMConnections.BaseCAC.BeginTrans;
                // Ac� hacer la agrupaci�n por patente
                While I < FListaPatentesPorRut.Count do begin
                    // Limpio la patente porque se inicia un nuevo ciclo
                    Patente := '';

                    J := 0;
                    // Ciclo por Concesionarias
                    while J < FListaConcesionarias.Count do begin
                        // Limpio los totalizadores
                        SumaPrecioInfraccion 	            := 0;
                        CantidadInfraccionesSeleccionadas   := 0;
                        PrecioInfraccion                    := 0;
                        Nombre                              := '';
                        CodigoInfraccion                    := 0;
                        ImporteIntereses	                := 0;
                        CodigoTipoInfraccion                := 0;   
                        Categoria                           := 0;
                        // Realizo el filtro
                        cldInfraccionesPorRut.Filtered := False;
                        cldInfraccionesPorRut.Filter := Format(FILTRO_CONCE_PATENTE, [Trim(FListaPatentesPorRut[I]), FListaConcesionarias[J]]);
                        cldInfraccionesPorRut.Filtered := True;

                        PrimerCaso := True;

                        if not cldInfraccionesPorRut.Eof then begin
                            try
                                // Contolar si hay algo seleccionado y si la selecci�n es v�lida
                                cldInfraccionesPorRut.First;

                                TPanelMensajesForm.MuestraMensaje('Calculando Intereses...');
                                while not cldInfraccionesPorRut.Eof do begin
                                    if cldInfraccionesPorRut.FieldByName('Facturar').AsInteger = 1 then begin
                                        if Primercaso then begin
                                            PrecioInfraccion    := cldInfraccionesPorRut.FieldByName('PrecioInfraccion').AsInteger;
                                            Nombre 			    := cldInfraccionesPorRut.FieldByName('Nombre').AsString;
                                            Documento		    := cldInfraccionesPorRut.FieldByName('Numerodocumento').AsString;
                                            CodigoInfraccion    := cldInfraccionesPorRut.FieldByName('CodigoInfraccion').AsInteger;
                                            PrimerCaso          := False;
                                            Patente             := cldInfraccionesPorRut.FieldByName('Patente').AsString;
                                            CodigoTipoInfraccion:= cldInfraccionesPorRut.FieldByName('CodigoTipoInfraccion').AsInteger; 
                                            Categoria           := cldInfraccionesPorRut.FieldByName('Categoria_RNVM').AsInteger;
                                        end
                                        else begin

                                            if (CodigoTipoInfraccion <> cldInfraccionesPorRut.FieldByName('CodigoTipoInfraccion').AsInteger) then begin 
                                                PanelMensajesForm.OcultaPanel;
                                                MsgBox(MSG_SELECCION_INFRA_INVALIDA, Caption, MB_ICONINFORMATION + MB_OK);                              
                                                DMConnections.BaseCAC.RollbackTrans;                                 
                                                bError := True;                                                                                         
                                                Exit;                                                                                                   
                                            end;                                                                                                        

                                        end;
                                        if (CodigoTipoInfraccion = FCodigoInfraccionNormal) then begin
                                            //Calcular los intereses
                                            spCalcularIntereses.Close;
                                            with spCalcularIntereses do begin
                                                Parameters.Refresh;
                                                Parameters.ParamByName('@MontoBase').Value	:= cldInfraccionesPorRut.FieldByName('PrecioInfraccion').AsInteger;
                                                Parameters.ParamByName('@FechaBase').Value	:= cldInfraccionesPorRut.FieldByName('FechaInfraccion').AsDateTime;
                                                Parameters.ParamByName('@FechaDeCalculo').Value := NULL;		//NULL = tomar la fecha de hoy
                                                Parameters.ParamByName('@Intereses').Value	:= NULL;
                                                ExecProc;
                                                Aux := Parameters.ParamByName('@Intereses').Value;
                                                ImporteIntereses	:= ImporteIntereses + Aux;
                                            end;

                                            if cldInfraccionesPorRut.FieldByName('CodigoConvenio').AsInteger <> cbConveniosCliente.Value then begin	// TASK_144_MGO_20170301
                                                TPanelMensajesForm.OcultaPanel;
                                                //Verifico que las infracciones no pertenecen a un cliente
                                                //Informo que no pertenecen a un cliente
                                                MsgBox(MSG_INFRA_CLIENTE, Self.Caption, MB_ICONINFORMATION + MB_OK);
                                                Exit;
                                            end;
                                        end;

                                        if (cldInfraccionesPorRut.FieldByName('PrecioInfraccion').Value <> null) then begin
                                            SumaPrecioInfraccion := SumaPrecioInfraccion + cldInfraccionesPorRut.FieldByName('PrecioInfraccion').AsInteger;
                                        end;

                                        CantidadInfraccionesSeleccionadas := CantidadInfraccionesSeleccionadas + 1 ;
                                    end;

                                    cldInfraccionesPorRut.Next;
                                end; // End While
                            finally

                            end;

                            //si no selecciono ninguna infraccion
                            if CantidadInfraccionesSeleccionadas = 0 then begin
                                //informo que no se selecciono ningua infraccion al operador
                                MsgBox(MSG_SELECCION_VACIA, Caption, MB_ICONINFORMATION + MB_OK);
                                Break;
                            end;

                            // Obtenemos los datos de la persona y del domicilio
                            TPanelMensajesForm.MuestraMensaje('Obteniendo datos de la cuenta...');
                            try
                                with ObtenerDatosPersonaNotaCobroInfraccion do begin
                                    Close;
                                    Parameters.ParamByName('@CodigoInfraccion').Value := CodigoInfraccion;
                                    Open;
                                    Nombre			:= FieldByName('Nombre').AsString;
                                    Documento		:= FieldByName('NumeroDocumento').AsString;
                                    Calle			:= FieldByName('Calle').AsString;
                                    CodigoRegion	:= FieldByName('CodigoRegion').AsString;
                                    CodigoComuna	:= FieldByName('CodigoComuna').AsString;
                                    Numero			:= FieldByName('Numero').AsString;
                                    Detalle			:= FieldByName('Detalle').AsString;
                                end;
                            except
                                // No hacemos nada por que sin estos datos la nota de cobro infraccion se puede editar igual
                            end;

                            TPanelMensajesForm.MuestraMensaje('Generando datos para el comprobante...');
                            // se abre la pantalla donde se solicitan los datos para generar el comprobante
                            if ObtenerDatosNotaCobroInfraccion(	peRUTCliente.Text,
                                                                cbConveniosCliente.Text,
                                                                lblDatoDomicilioFact.Caption,
                                                                lblDatoComunaFact.Caption,
                                                                lblDatoRegionFact.Caption,
                                                                lblDatoApellidoNombre.Caption,
                                                                lblDatoDomicilio.Caption,
                                                                lblDatoComuna.Caption,
                                                                lblDatoRegion.Caption,
                                                                SumaPrecioInfraccion,
                                                                ImporteGastosAdministrativos,
                                                                ImporteIntereses,
                                                                FechaEmision,
                                                                FechaVencimiento,
                                                                CantidadInfraccionesSeleccionadas,
                                                                False,                      
                                                                CodigoTipoInfraccion) then
                            begin //facturar
                                try
                                    FechaEmisionComprobante := FechaEmision;
                                    FechaVencimientoComprobante := FechaVencimiento;

                                    if cbConcesionariaRut.Value > 0 then
                                        CodigoConcesionaria := AsignarValorComboEstado(cbConcesionariaRut)
                                    else
                                        CodigoConcesionaria := cldInfraccionesPorRutCodigoConcesionaria.AsInteger;

                                    TPanelMensajesForm.MuestraMensaje('Generando Movimientos...');

                                    with spGenerarMovimientosInfraccionPorConvenio , Parameters do begin
                                        Parameters.Refresh;
                                        ParamByName('@CodigoConvenio').Value		  := strtoint(cbConveniosCliente.value) ;
                                        ParamByName('@FechaFacturacion').value        := FechaEmision ;
                                        if (FInfraccionesNormalesPorRut > 0) and (FInfraccionesMorosasPorRut = 0) then begin    
                                            ParamByName('@TotalInfracciones').Value       := SumaPrecioInfraccion *  100;       
                                            ParamByName('@TotalGastosCobranza').Value     := ImporteGastosAdministrativos * 100;
                                            ParamByName('@TotalIntereses').Value          := ImporteIntereses * 100 ;
                                            ParamByName('@TotalInfraccionesMorosos').Value:= 0;                                 
                                            ParamByName('@CodigoCategoria').Value         := 0;
                                            ParamByName('@CantInfraccionCategoria').Value := 0;                                 
                                        end else begin
                                            ParamByName('@TotalInfracciones').Value       := 0;
                                            ParamByName('@TotalGastosCobranza').Value     := 0;                                 
                                            ParamByName('@TotalIntereses').Value          := 0;
                                            ParamByName('@TotalInfraccionesMorosos').Value:= SumaPrecioInfraccion *  100;       
                                            ParamByName('@CodigoCategoria').Value         := Categoria;                         
                                            ParamByName('@CantInfraccionCategoria').Value := CantidadInfraccionesSeleccionadas; 
                                        end;
                                        ParamByName('@Usuario').Value                 := UsuarioSistema;
                                        ParamByName('@CodigoConcesionaria').Value     := CodigoConcesionaria;
                                        ParamByName('@NumeroMovimientoInfracciones').Value    := NULL;
                                        ParamByName('@NumeroMovimientoGastosCobranzas').Value := NULL;
                                        ParamByName('@NumeroMovimientoIntereses').Value       := NULL;
                                        ParamByName('@NumeroMovimientoInfraccionesMorosos').Value   := NULL;

                                        ExecProc ;
                                        if VarIsNull(ParamByName('@NumeroMovimientoInfracciones').Value) then
                                            NumeroMovimientoInfracciones := 0
                                        else
                                            NumeroMovimientoInfracciones    := ParamByName('@NumeroMovimientoInfracciones').Value  ;
                                        if VarIsNull(ParamByName('@NumeroMovimientoGastosCobranzas').Value) then
                                            NumeroMovimientoGastosCobranzas := 0
                                        else
                                            NumeroMovimientoGastosCobranzas := ParamByName('@NumeroMovimientoGastosCobranzas').Value;
                                        if VarIsNull(ParamByName('@NumeroMovimientoIntereses').Value) then
                                            NumeroMovimientoIntereses       := 0
                                        else
                                            NumeroMovimientoIntereses       := ParamByName('@NumeroMovimientoIntereses').Value ;

                                        if VarIsNull(ParamByName('@NumeroMovimientoInfraccionesMorosos').Value) then                        
                                            NumeroMovimientoMorosos       := 0                                                              
                                        else                                                                                                
                                            NumeroMovimientoMorosos       := ParamByName('@NumeroMovimientoInfraccionesMorosos').Value ;

                                    end;
                                    TPanelMensajesForm.MuestraMensaje('Cambiando estado a las infracciones...');
                                    //Anular las infracciones
                                    cldInfraccionesPorRut.First ;
                                    while not cldInfraccionesPorRut.Eof do begin
                                        if cldInfraccionesPorRut.FieldByName('Facturar').AsInteger = 1 then begin
                                          //Anulo la infraccion
                                            with spActualizarInfraccionValorizada, Parameters do begin
                                                ParamByName('@CodigoInfraccion').Value := cldInfraccionesPorRut.FieldByName('CodigoInfraccion').AsInteger;
                                                ParamByName('@CodigoLineaComprobante').Value := CodigoLineaComprobante;
                                                ParamByName('@CodigoTarifaDayPassBALI').Value := cldInfraccionesPorRut.FieldByName('CodigoTarifaDayPassBALI').AsInteger;
                                                if NumeroMovimientoInfracciones > 0 then ParamByName('@NumeroMovimiento').Value := NumeroMovimientoInfracciones 
                                                else ParamByName('@NumeroMovimiento').Value := NumeroMovimientoMorosos;
                                                ExecProc;
                                            end;
                                        end;
                                        cldInfraccionesPorRut.Next;
                                    end;
                                except
                                    on e: exception do begin
                                        TPanelMensajesForm.OcultaPanel;
                                        MsgBoxErr(MSG_ERROR_ANULANDO, e.message, caption, MB_ICONERROR);
                                    end;
                                end;
                            end;
                        end; // Fin If EOF
                        Inc(J);
                    end; // Fin While Concesionarias

                    Inc(I);																																	// SS_1045_CQU_20131108
                end; // Fin String List
                // Filtro S�lo por Patente																																	 // SS_1045_CQU_20131108
                cldInfraccionesPorRut.Filtered := False;																													// SS_1045_CQU_20131108
																																											// SS_1045_CQU_20131108
                if not cldInfraccionesPorRut.Eof then begin																													// SS_1045_CQU_20131108
                    // Facturacion por patente																																// SS_1045_CQU_20131108
                    TPanelMensajesForm.MuestraMensaje('Facturando las Infracciones...');																					// SS_1045_CQU_20131108
                    try																																						// SS_1045_CQU_20131108
                        FFacturacionManual := TFacturacionManualForm.Create(nil);																							// SS_1045_CQU_20131108
                        FFacturacionManual.FormStyle := fsNormal;																											// SS_1045_CQU_20131108
                        FFacturacionManual.Visible := False;																												// SS_1045_CQU_20131108
                        TPanelMensajesForm.MuestraMensaje('Iniciando generaci�n de comprobantes...');																		 // SS_1045_CQU_20131108
                        if FTraerListaAmarilla then bCargoFacturacion := FFacturacionManual.Inicializar(False, TC_NOTA_COBRO_INFRACCIONES, 1)                            	// SS_660E_CQU_20140806
                        else bCargoFacturacion := FFacturacionManual.Inicializar(TC_NOTA_COBRO_INFRACCIONES, False );                                                       // SS_660E_CQU_20140806

                        if bCargoFacturacion then begin                                                                                                                     // SS_660E_CQU_20140806
                                                                                                                                                                            // SS_660E_CQU_20140806
                            FFacturacionManual.peRUTCliente.Text := peRUTCliente.Text;																						// SS_1045_CQU_20131108
                            FFacturacionManual.peRUTCliente.OnChange(nil);																									// SS_1045_CQU_20131108
                            FFacturacionManual.btnBuscar.Click;																												// SS_1045_CQU_20131108
                            FFacturacionManual.cbConveniosCliente.ItemIndex := FFacturacionManual.cbConveniosCliente.Items.IndexOfValue(cbConveniosCliente.Value);			// SS_1045_CQU_20131108
                            FFacturacionManual.cbConveniosCliente.OnChange(nil);																							// SS_1045_CQU_20131108
                            // Valido que se haya ejecutado antes																											// SS_1045_CQU_20131108
                            // para colocar los valores elegidos por el usuario																								// SS_1045_CQU_20131108
                            if FHecho then begin																															// SS_1045_CQU_20131108
                                FFacturacionManual.Hecho := FHecho;																											// SS_1045_CQU_20131108
                                FFacturacionManual.Opcion1 := FOpcion1;																										// SS_1045_CQU_20131108
                                FFacturacionManual.Opcion2 := FOpcion2;																										// SS_1045_CQU_20131108
                                FFacturacionManual.Respuesta := FRespuesta;																									// SS_1045_CQU_20131108
                            end;																																			// SS_1045_CQU_20131108
                            FFacturacionManual.Hide;																														// SS_1045_CQU_20131108
                            // Ejecuto la facturaci�n																														 // SS_1045_CQU_20131108
                            FFacturacionManual.btnFacturar.Click;																											// SS_1045_CQU_20131108
                            // Valido que no se haya ejecutado antes																										// SS_1045_CQU_20131108
                            // para obtener los valores elegidos por el usuario																								// SS_1045_CQU_20131108
                            if not FHecho then begin																														// SS_1045_CQU_20131108
                                FHecho := FFacturacionManual.Hecho;																											// SS_1045_CQU_20131108
                                FOpcion1 := FFacturacionManual.Opcion1;																										// SS_1045_CQU_20131108
                                FOpcion2 := FFacturacionManual.Opcion2;																										// SS_1045_CQU_20131108
                                FRespuesta := FFacturacionManual.Respuesta;																									// SS_1045_CQU_20131108
                            end;																																			// SS_1045_CQU_20131108
                        end;																																				// SS_1045_CQU_20131108
                    finally																																					// SS_1045_CQU_20131108
                        if Assigned(FFacturacionManual) then FreeAndNil(FFacturacionManual);																				// SS_1045_CQU_20131108
                    end;																																					// SS_1045_CQU_20131108
                end else begin																																				// SS_1045_CQU_20131108
                    Raise Exception.Create ('Error al realizar la facturaci�n, no se encuentran los datos para facturar');													 // SS_1045_CQU_20131108
                end;																																						// SS_1045_CQU_20131108
																																											// SS_1045_CQU_20131108
                // Finalizo la Transacci�n																																	 // SS_1045_CQU_20131108
                if not FHecho then begin																																	// SS_1045_CQU_20131108
                    TPanelMensajesForm.MuestraMensaje('Problema al generar el comprobante...');																				// SS_1045_CQU_20131108
                    if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;																		// SS_1045_CQU_20131108
                end else begin																																				// SS_1045_CQU_20131108
                    TPanelMensajesForm.MuestraMensaje('Comprobante generado...');																							// SS_1045_CQU_20131108
                    if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;																			// SS_1045_CQU_20131108
                end;																																						// SS_1045_CQU_20131108
            except
                on e: exception do begin
                    //Hago Rollback de la Transacci�n
                    if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                    TPanelMensajesForm.OcultaPanel;
                    MsgBoxErr(MSG_ERROR_PROCESO, e.message, caption, MB_ICONERROR);
                    FFacturandoPorRut := False;
                end;
            end;
        finally
            TPanelMensajesForm.MuestraMensaje('Proceso finalizado...');
            Screen.Cursor := crDefault;
            Enabled := True;
            FFacturandoPorRut := False;
            // Quito el filtro al ClientDataSet
            if cldInfraccionesPorRut.Filtered then begin
                cldInfraccionesPorRut.Filter := EmptyStr;
                cldInfraccionesPorRut.Filtered := False;
            end;
            if cldInfraccionesPorRut.BookmarkValid(Puntero) then cldInfraccionesPorRut.GotoBookmark(Puntero);
            cldInfraccionesPorRut.FreeBookmark(Puntero);
            // Reinicio el formulario
            cldInfraccionesPorRut.EnableControls;

            if not bError then begin
                pgCentral.ActivePage := tsFacturacionRut;
                InicializarData();
            end;
            TPanelMensajesForm.OcultaPanel;
            Application.ProcessMessages;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : ClientDataSetAfterOpenGenerico
  Author        : PSegu�D
  Date Created  : 05/07/2012
  Description   : Despliega en la barra de estado la cantidad de Infracciones,
                  las seleccionadas y las patentes a facturar.
  Parameters    : DataSet: TDataSet
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfracciones.ClientDataSetAfterOpenGenerico(DataSet: TDataSet);
    resourcestring
        cPosicionEnRecordset = 'Infracci�n %s de %s - [%s Infracciones a Facturar][%s Patentes Seleccionadas]';
        cRecordsetVacio = 'Sin Infracciones en la B�squeda';
begin
    if pgCentral.ActivePage = tsFacturacionPatente then begin
        if cldInfracciones.Active then begin
            case cldInfracciones.RecordCount of
                0: begin
                    stbInfraccionesPorPatente.SimpleText := cRecordsetVacio;
                end;
            else
                stbInfraccionesPorPatente.SimpleText :=
                    Format(
                            cPosicionEnRecordset,
                            [
                                FloatToStrF(cldInfracciones.RecNo, ffNumber, 9,0),
                                FloatToStrF(cldInfracciones.RecordCount, ffNumber, 9,0),
                                FloatToStrF(FInfraccionesSeleccionadas, ffNumber, 9,0),
                                FloatToStrF(FListaPatentes.Count, ffNumber, 9,0)
                            ]
                    );
            end;
        end
        else stbInfraccionesPorPatente.SimpleText := cRecordsetVacio;
    end else begin
      if not FFacturandoPorRut then begin
        if cldInfraccionesPorRut.Active then begin
            case cldInfraccionesPorRut.RecordCount of
                0: begin
                    stbInfraccionesPorRUT.SimpleText := cRecordsetVacio;
                end;
            else
                stbInfraccionesPorRUT.SimpleText :=
                    Format(
                            cPosicionEnRecordset,
                            [
                                FloatToStrF(cldInfraccionesPorRut.RecNo, ffNumber, 9,0),
                                FloatToStrF(cldInfraccionesPorRut.RecordCount, ffNumber, 9,0),
                                FloatToStrF(FInfraccionesSeleccionadasPorRut, ffNumber, 9,0),
                                FloatToStrF(FListaPatentesPorRut.Count, ffNumber, 9,0)
                            ]
                    );
            end;
        end
        else stbInfraccionesPorRUT.SimpleText := cRecordsetVacio;
      end;
    end;
end;


end.
