object FormDenunciaCliente: TFormDenunciaCliente
  Left = 228
  Top = 144
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Denuncia'
  ClientHeight = 308
  ClientWidth = 414
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object groupbox: TGroupBox
    Left = 0
    Top = 0
    Width = 414
    Height = 92
    Align = alTop
    TabOrder = 0
    object lb_CodigoCliente: TLabel
      Left = 8
      Top = 17
      Width = 44
      Height = 13
      Caption = 'Cliente:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 8
      Top = 43
      Width = 45
      Height = 13
      Caption = 'Cuenta:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_TagDescripcion: TLabel
      Left = 62
      Top = 66
      Width = 319
      Height = 15
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object lbl_tag: TLabel
      Left = 8
      Top = 67
      Width = 51
      Height = 13
      Caption = 'Nro. tag:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lApellidoNombre: TLabel
      Left = 146
      Top = 17
      Width = 251
      Height = 13
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object peCodigoCliente: TPickEdit
      Left = 55
      Top = 11
      Width = 89
      Height = 21
      Color = 16444382
      Enabled = True
      TabOrder = 0
      OnChange = peCodigoClienteChange
      Decimals = 0
      EditorStyle = bteNumericEdit
      OnButtonClick = peCodigoClienteButtonClick
    end
    object cb_Cuentas: TComboBox
      Left = 55
      Top = 38
      Width = 346
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cb_CuentasChange
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 273
    Width = 414
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object btn_Aceptar: TDPSButton
      Left = 235
      Top = 5
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btn_AceptarClick
    end
    object btn_Cancelar: TDPSButton
      Left = 319
      Top = 5
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 92
    Width = 414
    Height = 181
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      414
      181)
    object Label2: TLabel
      Left = 8
      Top = 11
      Width = 43
      Height = 13
      Caption = 'Motivo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 36
      Width = 74
      Height = 13
      Caption = 'Observaciones:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txt_observaciones: TMemo
      Left = 8
      Top = 51
      Width = 396
      Height = 122
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
    end
    object cb_denuncia: TComboBox
      Left = 55
      Top = 6
      Width = 346
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
    end
  end
  object dsCuentasCliente: TDataSource
    DataSet = ObtenerCuentasCliente
    Left = 264
    Top = 39
  end
  object ObtenerCuentasCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCuentasCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 296
    Top = 39
  end
  object RegistrarDenuncia: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'RegistrarDenuncia'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDenuncia'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@MotivoDenuncia'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 11
        Value = Null
      end
      item
        Name = '@CodigoComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 200
    Top = 277
  end
end
