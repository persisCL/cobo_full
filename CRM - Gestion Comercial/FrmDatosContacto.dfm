object NavWindowDatosContacto: TNavWindowDatosContacto
  Left = 156
  Top = 83
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  BorderStyle = bsDialog
  Caption = 'Datos del Contacto'
  ClientHeight = 555
  ClientWidth = 705
  Color = 16776699
  Constraints.MinHeight = 500
  Constraints.MinWidth = 705
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 705
    Height = 15
    Align = alTop
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 705
      Height = 15
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 705
        Height = 2
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 15
    Width = 705
    Height = 540
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      705
      540)
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 705
      Height = 328
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        705
        328)
      object lblNombre: TLabel
        Left = 10
        Top = 99
        Width = 62
        Height = 13
        Caption = 'Nombre(s):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblApellido: TLabel
        Left = 10
        Top = 73
        Width = 50
        Height = 13
        Caption = 'Apellido:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblFechaNacCreacion: TLabel
        Left = 366
        Top = 100
        Width = 89
        Height = 13
        Caption = 'Fecha Nacimiento:'
      end
      object lblSexo: TLabel
        Left = 366
        Top = 122
        Width = 27
        Height = 13
        Caption = 'Sexo:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Label4: TLabel
        Left = 10
        Top = 49
        Width = 69
        Height = 13
        Caption = 'Documento:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 12
        Top = 122
        Width = 72
        Height = 13
        Caption = 'Pa'#237's de origen:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblApellidoMaterno: TLabel
        Left = 366
        Top = 72
        Width = 81
        Height = 13
        Caption = 'Apellido materno:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label53: TLabel
        Left = 10
        Top = 27
        Width = 67
        Height = 13
        Caption = 'Personer'#237'a:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_nombre: TEdit
        Left = 129
        Top = 94
        Width = 229
        Height = 21
        Color = 16444382
        TabOrder = 4
      end
      object txt_apellido: TEdit
        Left = 129
        Top = 69
        Width = 229
        Height = 21
        Color = 16444382
        TabOrder = 2
      end
      object txt_fechanacimiento: TDateEdit
        Left = 469
        Top = 94
        Width = 96
        Height = 21
        AutoSelect = False
        TabOrder = 5
        Date = -693594
      end
      object cb_sexo: TComboBox
        Left = 469
        Top = 118
        Width = 103
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 7
        Text = 'Masculino'
        Items.Strings = (
          'Masculino'
          'Femenino')
      end
      object mcTipoNumeroDocumento: TMaskCombo
        Left = 129
        Top = 45
        Width = 228
        Height = 21
        Items = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Color = 16444382
        ComboWidth = 107
        ItemIndex = -1
        OmitCharacterRequired = False
        TabOrder = 1
      end
      object cb_pais: TComboBox
        Left = 129
        Top = 118
        Width = 229
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        MaxLength = 4
        TabOrder = 6
      end
      object txt_apellidoMaterno: TEdit
        Left = 469
        Top = 69
        Width = 215
        Height = 21
        TabOrder = 3
      end
      object cbPersoneria: TComboBox
        Left = 129
        Top = 22
        Width = 107
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbPersoneriaChange
        Items.Strings = (
          'F'#237'sica                          F'
          'Jur'#237'dica                       J')
      end
      object GBMediosComunicacion: TGroupBox
        Left = 5
        Top = 254
        Width = 698
        Height = 74
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Medios de comunicaci'#243'n'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 9
        object Label12: TLabel
          Left = 11
          Top = 25
          Width = 91
          Height = 13
          Caption = 'Tel'#233'fono particular:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label18: TLabel
          Left = 253
          Top = 25
          Width = 93
          Height = 13
          Caption = 'Tel'#233'fono comercial:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label20: TLabel
          Left = 11
          Top = 49
          Width = 72
          Height = 13
          Caption = 'Tel'#233'fono movil:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label21: TLabel
          Left = 254
          Top = 51
          Width = 28
          Height = 13
          Caption = 'Email:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object txtAreaTelefonoParticular: TEdit
          Left = 111
          Top = 19
          Width = 46
          Height = 21
          Hint = 'C'#243'digo de '#225'rea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnKeyPress = txtTelefonoParticularKeyPress
        end
        object txtTelefonoParticular: TEdit
          Left = 160
          Top = 19
          Width = 86
          Height = 21
          Hint = 'N'#250'mero de tel'#233'fono'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnKeyPress = txtTelefonoParticularKeyPress
        end
        object txtTelefonoComercial: TEdit
          Left = 403
          Top = 19
          Width = 86
          Height = 21
          Hint = 'N'#250'mero de tel'#233'fono'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnKeyPress = txtTelefonoComercialKeyPress
        end
        object txtTelefonoMovil: TEdit
          Left = 160
          Top = 43
          Width = 86
          Height = 21
          Hint = 'N'#250'mero de tel'#233'fono'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnKeyPress = txtTelefonoMovilKeyPress
        end
        object txtEmailParticular: TEdit
          Left = 353
          Top = 43
          Width = 228
          Height = 21
          Hint = 'direcci'#243'n de e-mail'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
        end
        object txtAreaTelefonoMovil: TEdit
          Left = 111
          Top = 43
          Width = 46
          Height = 21
          Hint = 'C'#243'digo de '#225'rea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnKeyPress = txtTelefonoParticularKeyPress
        end
        object txtAreaTelefonoComercial: TEdit
          Left = 353
          Top = 19
          Width = 46
          Height = 21
          Hint = 'C'#243'digo de '#225'rea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnKeyPress = txtTelefonoParticularKeyPress
        end
      end
      object GBDomicilios: TGroupBox
        Left = 5
        Top = 144
        Width = 698
        Height = 109
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Domicilios '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
        object lblnada: TLabel
          Left = 15
          Top = 18
          Width = 63
          Height = 13
          Cursor = crHandPoint
          Hint = 'Domicilio particular'
          Caption = 'Particular: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold, fsUnderline]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label2: TLabel
          Left = 15
          Top = 46
          Width = 60
          Height = 13
          Cursor = crHandPoint
          Hint = 'Domicilio particular'
          Caption = 'Comercial:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold, fsUnderline]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object LblDomicilioComercial: TLabel
          Left = 77
          Top = 46
          Width = 476
          Height = 26
          AutoSize = False
          Caption = 'Ingrese el domicilio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsItalic]
          ParentFont = False
          WordWrap = True
        end
        object LblDomicilioParticular: TLabel
          Left = 78
          Top = 18
          Width = 475
          Height = 26
          AutoSize = False
          Caption = 'Ingrese el domicilio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsItalic]
          ParentFont = False
          WordWrap = True
        end
        object Label16: TLabel
          Left = 16
          Top = 83
          Width = 96
          Height = 13
          Caption = 'Domicilio de entrega'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object BtnDomicilioComercialAccion: TDPSButton
          Left = 561
          Top = 47
          Width = 61
          Hint = 'Domicilio comercial'
          Caption = 'Alta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = BtnDomicilioComercialAccionClick
        end
        object BtnDomicilioParticularAccion: TDPSButton
          Left = 561
          Top = 16
          Width = 61
          Hint = 'Domicilio particular'
          Caption = 'Alta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = BtnDomicilioParticularAccionClick
        end
        object RGDomicilioEntrega: TRadioGroup
          Left = 127
          Top = 72
          Width = 235
          Height = 32
          Columns = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            '&Particular'
            '&Comercial')
          ParentFont = False
          TabOrder = 2
        end
        object ChkEliminarDomicilioParticular: TCheckBox
          Left = 626
          Top = 22
          Width = 67
          Height = 17
          Caption = 'Eliminar'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          OnClick = ChkEliminarDomicilioParticularClick
        end
        object ChkEliminarDomicilioComercial: TCheckBox
          Left = 627
          Top = 51
          Width = 67
          Height = 17
          Caption = 'Eliminar'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnClick = ChkEliminarDomicilioComercialClick
        end
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 348
      Width = 705
      Height = 3
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
    end
    object Panel12: TPanel
      Left = 0
      Top = 328
      Width = 705
      Height = 20
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = '   Comunicaciones'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object Panel10: TPanel
      Left = 0
      Top = 499
      Width = 705
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object Notebook1: TNotebook
        Left = 492
        Top = 0
        Width = 213
        Height = 41
        Align = alRight
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'FormatoMDI'
          ExplicitWidth = 0
          ExplicitHeight = 0
          object btn_GuardarDatos: TDPSButton
            Left = 11
            Top = 8
            Width = 95
            Caption = '&Guardar Datos'
            Default = True
            TabOrder = 0
            OnClick = btn_GuardarDatosClick
          end
          object btn_Salir: TDPSButton
            Left = 111
            Top = 8
            Width = 92
            Cancel = True
            Caption = '&Salir'
            Default = True
            ModalResult = 2
            TabOrder = 1
            OnClick = btn_SalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'FormatoModal'
          object btnGuardarDatos: TDPSButton
            Left = 10
            Top = 8
            Width = 97
            Caption = '&Guardar Datos'
            Default = True
            TabOrder = 0
            OnClick = btn_GuardarDatosClick
          end
          object btnSalir: TDPSButton
            Left = 111
            Top = 8
            Width = 92
            Cancel = True
            Caption = '&Salir'
            Default = True
            ModalResult = 2
            TabOrder = 1
            OnClick = btn_SalirClick
          end
        end
      end
    end
    object dg_OrdenesServicio: TDPSGrid
      Left = 5
      Top = 352
      Width = 695
      Height = 133
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = dsReclamos
      FixedColor = 16444382
      Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 4
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = dg_OrdenesServicioDrawColumnCell
      OnLinkClick = dg_OrdenesServicioLinkClick
      Columns = <
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'CodigoComunicacion'
          Title.Caption = 'Comunicaci'#243'n'
          Width = 103
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ver'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          Title.Caption = 'Ord. Srv.'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FechaHoraInicio'
          Title.Caption = 'Inicio Comunicaci'#243'n'
          Width = 105
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NombreCompleto'
          Title.Caption = 'Usuario'
          Width = 89
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 'Tiempo Restante'
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DescripcionEstado'
          Title.Caption = 'Estado'
          Width = 71
          Visible = True
        end>
    end
  end
  object Panel6: TPanel
    Left = 14
    Top = 15
    Width = 146
    Height = 16
    BevelOuter = bvNone
    Caption = 'Datos Personales'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
  end
  object ObtenerComunicaciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerComunicaciones;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DesdeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 232
    Top = 344
  end
  object dsReclamos: TDataSource
    DataSet = ObtenerComunicaciones
    Left = 200
    Top = 344
  end
  object Timer: TTimer
    Interval = 10000
    OnTimer = TimerTimer
    Left = 263
    Top = 344
  end
  object ObtenerPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 597
    Top = 104
  end
  object ObtenerDatosDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosDomicilio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 626
    Top = 104
  end
  object ActualizarMedioComunicacionPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioComunicacionPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@HorarioDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 422
    Top = 36
  end
  object ActualizarDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@LugarNacimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@FechaNacimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoActividad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoSituacionIVA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoDomicilioEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PIN'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 4
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 392
    Top = 36
  end
  object ActualizarDomicilioEntregaPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioEntregaPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilioEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 515
    Top = 36
  end
  object ActualizarDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoEdificacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Depto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 453
    Top = 36
  end
  object EliminarDomicilioPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarDomicilioPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 547
    Top = 36
  end
  object ActualizarDomicilioRelacionado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioRelacionado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 484
    Top = 36
  end
end
