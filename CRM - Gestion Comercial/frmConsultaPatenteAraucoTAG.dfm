object ConsultaPatenteAraucoTAGForm: TConsultaPatenteAraucoTAGForm
  Left = 0
  Top = 0
  Anchors = []
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Consulta Patente Arauco TAG'
  ClientHeight = 410
  ClientWidth = 926
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 209
    Width = 926
    Height = 201
    Align = alClient
    TabOrder = 0
    object pnl2: TPanel
      Left = 1
      Top = 1
      Width = 924
      Height = 199
      Align = alClient
      TabOrder = 0
      object spl1: TSplitter
        Left = 491
        Top = 1
        Height = 197
        Color = clGrayText
        ParentColor = False
        ExplicitLeft = 504
        ExplicitTop = 4
        ExplicitHeight = 495
      end
      object pnl3: TPanel
        Left = 1
        Top = 1
        Width = 490
        Height = 197
        Align = alLeft
        Caption = 'pnl3'
        TabOrder = 0
        object grp1: TGroupBox
          Left = 1
          Top = 1
          Width = 488
          Height = 195
          Align = alClient
          Caption = 'Adhesi'#243'n a Parque Arauco'
          TabOrder = 0
          object lstDBLEAdhesionPA: TDBListEx
            Left = 2
            Top = 15
            Width = 484
            Height = 178
            Align = alClient
            BorderStyle = bsSingle
            Columns = <
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 60
                Header.Caption = 'Patente'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'Patente'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 80
                Header.Caption = 'Etiqueta'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'Etiqueta'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 125
                Header.Caption = 'Alta Parking'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'FechaHoraAltaConcesionaria'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 125
                Header.Caption = 'Baja Parking'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'FechaHoraBajaConcesionaria'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 125
                Header.Caption = 'Parking'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'Descripcion'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 125
                Header.Caption = 'Alta CAC'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'FechaHoraAltaListaAcceso'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 150
                Header.Caption = 'Usuario Alta'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'UsuarioAlta'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 125
                Header.Caption = 'Baja CAC'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'FechaHoraBajaListaAcceso'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 125
                Header.Caption = 'Usuario Baja'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'UsuarioBaja'
              end>
            DataSource = dsObtenerHistoricoListaDeAccesoConvenio
            DragReorder = True
            ParentColor = False
            TabOrder = 0
            TabStop = True
          end
        end
      end
      object pnl4: TPanel
        Left = 494
        Top = 1
        Width = 429
        Height = 197
        Align = alClient
        Caption = 'pnl4'
        TabOrder = 1
        object grp2: TGroupBox
          Left = 1
          Top = 1
          Width = 427
          Height = 195
          Align = alClient
          Caption = 'Inhabilitaciones'
          TabOrder = 0
          object lstDBLEInhabilitaciones: TDBListEx
            Left = 2
            Top = 15
            Width = 423
            Height = 178
            Align = alClient
            BorderStyle = bsSingle
            Columns = <
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 125
                Header.Caption = 'Desde'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'FechaInicio'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 125
                Header.Caption = 'Hasta'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'FechaFinalizacion'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 130
                Header.Caption = 'Concesionaria'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'DescripcionConcesionaria'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 150
                Header.Caption = 'Motivo'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                OnHeaderClick = DBLEAdhesionPAColumnsHeaderClick
                FieldName = 'DescripcionMotivo'
              end>
            DataSource = dsObtenerHistoricoConveniosInhabilitados
            DragReorder = True
            ParentColor = False
            TabOrder = 0
            TabStop = True
          end
        end
      end
    end
  end
  object pnl5: TPanel
    Left = 0
    Top = 0
    Width = 926
    Height = 209
    Align = alTop
    Caption = 'pnl5'
    TabOrder = 1
    object grp3: TGroupBox
      Left = 1
      Top = 137
      Width = 924
      Height = 71
      Align = alClient
      Caption = 'Datos del Cliente'
      Color = clBtnFace
      ParentColor = False
      TabOrder = 1
      DesignSize = (
        924
        71)
      object lblApellidoNombre: TLabel
        Left = 66
        Top = 18
        Width = 407
        Height = 15
        Anchors = [akLeft, akTop, akBottom]
        AutoSize = False
      end
      object lbl5: TLabel
        Left = 12
        Top = 19
        Width = 48
        Height = 13
        Caption = 'Nombre:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblLDomicilio: TLabel
        Left = 74
        Top = 39
        Width = 407
        Height = 11
        Anchors = [akLeft, akTop, akBottom]
        AutoSize = False
      end
      object lbl7: TLabel
        Left = 12
        Top = 39
        Width = 56
        Height = 13
        Caption = 'Domicilio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object grpFiltros: TGroupBox
      Left = 1
      Top = 1
      Width = 924
      Height = 136
      Align = alTop
      Caption = '  Filtros de Consulta  '
      ParentBackground = False
      TabOrder = 0
      object lbl1: TLabel
        Left = 43
        Top = 29
        Width = 26
        Height = 13
        Caption = 'RUT:'
      end
      object lbl2: TLabel
        Left = 26
        Top = 53
        Width = 43
        Height = 13
        Caption = 'Patente :'
      end
      object lbl3: TLabel
        Left = 263
        Top = 29
        Width = 48
        Height = 13
        Caption = 'Convenio:'
        Color = 16776699
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object lbl4: TLabel
        Left = 32
        Top = 80
        Width = 37
        Height = 13
        Caption = 'Desde :'
      end
      object lbl6: TLabel
        Left = 277
        Top = 80
        Width = 34
        Height = 13
        Caption = 'Hasta :'
      end
      object btn_Filtrar: TButton
        Left = 785
        Top = 17
        Width = 75
        Height = 25
        Hint = 'Consultar '
        Caption = '&Consultar'
        TabOrder = 6
        OnClick = btn_FiltrarClick
      end
      object btn_Limpiar: TButton
        Left = 785
        Top = 54
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Caption = '&Limpiar'
        TabOrder = 7
        OnClick = btn_LimpiarClick
      end
      object edtPatente: TEdit
        Left = 75
        Top = 53
        Width = 139
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 6
        TabOrder = 3
        OnChange = edtPatenteChange
        OnKeyUp = edtPatenteKeyUp
      end
      object edtRUTCliente: TPickEdit
        Left = 75
        Top = 26
        Width = 139
        Height = 21
        CharCase = ecUpperCase
        Color = 16444382
        Enabled = True
        TabOrder = 0
        OnChange = edtRUTClienteChange
        OnKeyPress = edtRUTClienteKeyPress
        OnKeyUp = edtRUTClienteKeyUp
        EditorStyle = bteTextEdit
        OnButtonClick = edtRUTClienteOnClick
      end
      object cbbPatente: TVariantComboBox
        Left = 75
        Top = 53
        Width = 145
        Height = 19
        Style = vcsOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 2
        Visible = False
        Items = <>
      end
      object edtHistListaAccesoDesde: TDateEdit
        Left = 75
        Top = 80
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 4
        Date = -693594.000000000000000000
      end
      object edtHistListaAccesoHasta: TDateEdit
        Left = 317
        Top = 77
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 5
        Date = -693594.000000000000000000
      end
      object cbbConvenio: TVariantComboBox
        Left = 317
        Top = 26
        Width = 267
        Height = 21
        Style = vcsOwnerDrawFixed
        Color = clWhite
        ItemHeight = 15
        TabOrder = 1
        OnChange = cbbConvenioChange
        OnDrawItem = cbbConvenioDrawItem
        Items = <>
      end
      object btnSalir: TButton
        Left = 785
        Top = 92
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Caption = '&Salir'
        TabOrder = 8
        OnClick = btnSalirClick
      end
    end
  end
  object spObtenerPatentesConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerPatentesConvenio;1'
    Parameters = <>
    Left = 224
    Top = 48
  end
  object spObtenerHistoricoListaDeAccesoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerHistoricoListaDeAccesoConvenio;1'
    Parameters = <>
    Left = 392
    Top = 280
  end
  object dsObtenerHistoricoListaDeAccesoConvenio: TDataSource
    DataSet = spObtenerHistoricoListaDeAccesoConvenio
    Left = 432
    Top = 280
  end
  object spObtenerHistoricoConveniosInhabilitados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerHistoricoConveniosInhabilitados;1'
    Parameters = <>
    Left = 696
    Top = 280
  end
  object dsObtenerHistoricoConveniosInhabilitados: TDataSource
    DataSet = spObtenerHistoricoConveniosInhabilitados
    Left = 728
    Top = 280
  end
  object spObtenerCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 712
    Top = 151
  end
  object spObtenerConvenioPorPatente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConvenioPorPatente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 264
    Top = 48
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomicilioFacturacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 712
    Top = 184
  end
end
