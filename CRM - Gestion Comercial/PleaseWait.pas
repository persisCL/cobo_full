unit PleaseWait;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, LMDPNGImage, ExtCtrls;

type
{INICIO: TASK_106_JMA_20170206
  TfrmPleaseWait = class(TForm)
    img1: TImage;
    lbl1: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure Animacion;
  private
	Private declarations 
    H,W, L, T : Integer;
  protected
     procedure CreateParams(var Params: TCreateParams); override;
  public
	Public declarations 
  end;

  TAnimacion = class(TThread)
  private
    UnMillon,
    Frecuencia,
    X,
    Y: Int64;

  public
    constructor Create();
    procedure Execute(); override;
    procedure DoTerminate; override;
    destructor Destroy; override;
  end;
}
 TAnimacion = class(TThread)
  private
    UnMillon,
    Frecuencia,
    X,
    Y: Int64;
  public
    stopAnimation: Boolean;
    constructor Create();
    procedure Execute(); override;
    procedure DoTerminate; override;
    destructor Destroy; override;
  end;

  TfrmPleaseWait = class(TForm)
    img1: TImage;
    lbl1: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure Animacion;
  private
    H,W, L, T : Integer;
    oAnimacion: TAnimacion;
  protected
     procedure CreateParams(var Params: TCreateParams); override;
  public
  end;
{TERMINO: TASK_106_JMA_20170206   }
var
  frmPleaseWait: TfrmPleaseWait;

implementation

{$R *.dfm}

destructor TAnimacion.Destroy;
begin
  inherited Destroy;
end;

procedure TAnimacion.DoTerminate;
begin

  inherited;
end;


constructor TAnimacion.Create();
begin
  inherited Create(False);
  Self.FreeOnTerminate  := True;
  UnMillon              := 1000000;
  QueryPerformanceFrequency(Frecuencia);
  QueryPerformanceCounter(X);
end;

procedure TAnimacion.Execute();
var
  Msg: TMsg;
begin
  inherited;
  try
   repeat
     QueryPerformanceCounter(Y);
     if (Y - X) * UnMillon div Frecuencia >= 750000 then begin
       X := Y;
       Self.Synchronize(frmPleaseWait.Animacion);
     end;
   until stopAnimation;                         //TASK_106_JMA_20170206
  except
  end;
end;

procedure TfrmPLeaseWait.Animacion;
begin
  if Assigned(img1) then                        //TASK_106_JMA_20170206
    img1.Visible := not img1.Visible;           //TASK_106_JMA_20170206
  Width  := W;
  Height := H;
  Left   := L;
  Top    := T;
end;

procedure TfrmPLeaseWait.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.Style := Params.Style or WS_BORDER or WS_THICKFRAME;
end;

procedure TfrmPleaseWait.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    oAnimacion.stopAnimation:= True;          //TASK_106_JMA_20170206
  Action := caFree;
end;

procedure TfrmPleaseWait.FormCreate(Sender: TObject);
begin
  Visible := True;
  oAnimacion := TAnimacion.Create;            //TASK_106_JMA_20170206
  oAnimacion.stopAnimation := False;          //TASK_106_JMA_20170206
  H := Height;
  W := Width;
  L := Left;
  T := Top;
  Application.ProcessMessages;
end;


end.
