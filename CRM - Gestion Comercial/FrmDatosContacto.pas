unit FrmDatosContacto;
{
Firma       : SS_1419_NDR_20151130
Descripcion : Se envia el parametro @CodigoUSuario al SP ActualizarDatosPersona
            Para grabarlo en la auditoria de la tabla Personas y en en HistoricoPersonas
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Navigator, StdCtrls, ExtCtrls, Buttons, DmiCtrls, validate,
  Dateedit, DB, ADODB, UtilDB, Util, DBTables, UtilProc, PeaProcs,
  CACProcs, Grids, DBGrids, DPSGrid, math, WorkFlowComp,
  frmWrkDisplay, Peatypes, MaskCombo, FrmDatosOrdenServicio, ComCtrls,
  DPSPageControl, DPSControls, FrmEditarDomicilio;

type
  TNavWindowDatosContacto = class(TNavWindowFrm)
    ObtenerComunicaciones: TADOStoredProc;
    Panel2: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel1: TPanel;
    Panel3: TPanel;
    lblNombre: TLabel;
    lblApellido: TLabel;
    lblFechaNacCreacion: TLabel;
    lblSexo: TLabel;
    Label4: TLabel;
    txt_nombre: TEdit;
    txt_apellido: TEdit;
    txt_fechanacimiento: TDateEdit;
    cb_sexo: TComboBox;
    Panel9: TPanel;
    Panel6: TPanel;
    Panel12: TPanel;
    Panel10: TPanel;
    dsReclamos: TDataSource;
    dg_OrdenesServicio: TDPSGrid;
    Notebook1: TNotebook;
    mcTipoNumeroDocumento: TMaskCombo;
    Timer: TTimer;
    Label8: TLabel;
    cb_pais: TComboBox;
    lblApellidoMaterno: TLabel;
    txt_apellidoMaterno: TEdit;
    btn_GuardarDatos: TDPSButton;
    btnGuardarDatos: TDPSButton;
    ObtenerPersona: TADOStoredProc;
    ObtenerDatosDomicilio: TADOStoredProc;
    Label53: TLabel;
    cbPersoneria: TComboBox;
    ActualizarMedioComunicacionPersona: TADOStoredProc;
    ActualizarDatosPersona: TADOStoredProc;
    ActualizarDomicilioEntregaPersona: TADOStoredProc;
    ActualizarDomicilio: TADOStoredProc;
    EliminarDomicilioPersona: TADOStoredProc;
    btn_Salir: TDPSButton;
    btnSalir: TDPSButton;
    GBMediosComunicacion: TGroupBox;
    GBDomicilios: TGroupBox;
    lblnada: TLabel;
    Label2: TLabel;
    LblDomicilioComercial: TLabel;
    LblDomicilioParticular: TLabel;
    Label16: TLabel;
    BtnDomicilioComercialAccion: TDPSButton;
    BtnDomicilioParticularAccion: TDPSButton;
    Label12: TLabel;
    txtAreaTelefonoParticular: TEdit;
    txtTelefonoParticular: TEdit;
    Label18: TLabel;
    txtTelefonoComercial: TEdit;
    Label20: TLabel;
    txtTelefonoMovil: TEdit;
    Label21: TLabel;
    txtEmailParticular: TEdit;
    RGDomicilioEntrega: TRadioGroup;
    ChkEliminarDomicilioParticular: TCheckBox;
    ChkEliminarDomicilioComercial: TCheckBox;
    txtAreaTelefonoMovil: TEdit;
    txtAreaTelefonoComercial: TEdit;
    ActualizarDomicilioRelacionado: TADOStoredProc;
    procedure btn_editarClick(Sender: TObject);
    procedure btn_GuardarDatosClick(Sender: TObject);
    procedure dg_OrdenesServicioDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dg_OrdenesServicioLinkClick(Sender: TDPSGrid;
      Column: TColumn);
    procedure TimerTimer(Sender: TObject);
    procedure cbPersoneriaChange(Sender: TObject);
    procedure txtTelefonoParticularKeyPress(Sender: TObject;
      var Key: Char);
    procedure txtTelefonoComercialKeyPress(Sender: TObject; var Key: Char);
    procedure txtTelefonoMovilKeyPress(Sender: TObject; var Key: Char);
    procedure btn_SalirClick(Sender: TObject);
    procedure BtnDomicilioParticularAccionClick(Sender: TObject);
    procedure BtnDomicilioComercialAccionClick(Sender: TObject);
    procedure ChkEliminarDomicilioParticularClick(Sender: TObject);
    procedure ChkEliminarDomicilioComercialClick(Sender: TObject);
  private
	FCodigoContacto: Integer;
    FTipoDocumento: AnsiString;
    EsPersonaFisica: boolean;
    FCerrarAlGrabar: boolean;
    CodigoDomicilioParticular, CodigoDomicilioComercial, CodigoDomicilioEntrega: integer;
    DomicilioParticular: TDatosDomicilio;
    DomicilioComercial: TDatosDomicilio; 
    //cerrarAlGrabar: boolean;
	function CargarContacto(CodigoContacto: Integer): Boolean;
	function GetApellido: AnsiString;
    function GetApellidoMaterno: AnsiString;
	function GetNombre: AnsiString;
    function GetNumeroDocumento: AnsiString;
    procedure SetNumeroDocumento(const Value: AnsiString);
    procedure setApellido(const Value: AnsiString);
    procedure setApellidoMaterno(const Value: AnsiString);
    procedure setNombre(const Value: AnsiString);
    procedure HabilitarCamposPersoneria(Personeria: Char);
    function  PuedeGuardar: boolean;
    procedure GuardarMediosComunicacionContacto (persona: integer);
    procedure GuardarMedioComunicacion(persona, mediocontacto, tipoorigen: integer; valor: AnsiString);
    procedure PrepararMostrarDomicilio (descripcion: AnsiString; tipoOrigen: integer);
    procedure MostrarDomicilio (var L: TLabel; descripcion: AnsiString);
    procedure AltaDomicilio(tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
    procedure ModificarDomicilio(tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
    function  EstaElegidoComoDomicilioEntrega(tipoOrigenDatos: integer): boolean;
    procedure MarcarComoDomicilioEntrega(tipoOrigenDatos: integer);

  protected
	//**FEsCliente: Boolean;
    FTipoContacto: AnsiString;
	FCodigoCliente: integer;
    FCodigoPersona: integer;
	FActualizarBusqueda: Boolean;
	FCodigoComunicacion: Integer;
    FPIN: AnsiString;
    FFechaHoraInicioComunicacion: TDateTime;
	procedure Loaded; override;
  public
	{ Public declarations }
	//**property EsCliente: Boolean Read FEsCliente;
    property TipoContacto: AnsiString Read FTipoContacto;
	property CodigoCliente: Integer Read FCodigoCliente;
    property CodigoPersona: Integer Read FCodigoPersona;
	property Apellido: AnsiString Read GetApellido write setApellido;
    property ApellidoMaterno: AnsiString Read GetApellidoMaterno write setApellidoMaterno;
	property Nombre: AnsiString Read GetNombre write setNombre;
    property TipoDocumento: AnsiString read FTipoDocumento write FTipoDocumento;
    property NumeroDocumento: AnsiString read GetNumeroDocumento write SetNumeroDocumento;
    property PINCliente: AnsiString Read FPIN;
    property CerrarAlGrabar: boolean Read FCerrarAlGrabar write FCerrarAlGrabar;
    //
    Procedure RefreshData; override;
	Function GetBookmark(Var Bookmark, Description: AnsiString): Boolean; override;
	Function GotoBookmark(Bookmark: AnsiString): Boolean; override;
	class Function CreateBookmark(CodigoContacto: Integer): AnsiString;
    function Inicializa(MDIChild: Boolean): Boolean; override;
  end;


resourcestring
    CAPTION_BOTON_DOMICILIO_ALTA = 'Alta';
    CAPTION_BOTON_DOMICILIO_MODIFICAR = 'Modificar';
    SIN_ESPECIFICAR = '< Sin especificar >';
var
  NavWindowDatosContacto: TNavWindowDatosContacto;

implementation

uses DMConnection;

{$R *.dfm}


{ TNavWindowDatosCliente }

class function TNavWindowDatosContacto.CreateBookmark(CodigoContacto: Integer): AnsiString;
begin
	Result := IntToStr(CodigoContacto);
end;

function TNavWindowDatosContacto.GetBookmark(var Bookmark, Description: AnsiString): Boolean;
resourcestring
    MSG_BOOKMARK = 'Datos del Contacto %s';
begin
	Result := (FCodigoContacto > 0);
	Bookmark := CreateBookmark(FCodigoContacto);
	Description := Format(MSG_BOOKMARK, [Trim(BuscarApellidoNombreCliente(DMConnections.BaseCAC, FCodigoContacto))]);
end;

function TNavWindowDatosContacto.GotoBookmark(Bookmark: AnsiString): Boolean;
begin
	Result := CargarContacto(IVal(Bookmark));
end;

function TNavWindowDatosContacto.Inicializa(MDIChild: Boolean): Boolean;
begin
	result := inherited inicializa(MDIChild);
    if not Result then exit;

    CodigoDomicilioParticular := -1;
    CodigoDomicilioComercial := -1;
    
	if MDIChild then begin
        Notebook1.PageIndex := 0;
	end else begin
        Notebook1.PageIndex := 1;
    end;
    //scerrarAlGrabar := cerrarAlGrabarAux;
    with dg_OrdenesServicio.Columns do begin
		Items[0].Title.Caption := MSG_COL_TITLE_COMUNICACION;
		Items[1].Title.Caption := MSG_COL_TITLE_ORDEN_DE_SERVICIO;
		Items[2].Title.Caption := MSG_COL_TITLE_INICIO_DE_COMUNICACION;
		Items[3].Title.Caption := MSG_COL_TITLE_USUARIO;
		Items[4].Title.Caption := MSG_COL_TITLE_TIEMPO_RESTANTE;
		Items[5].Title.Caption := MSG_COL_TITLE_ESTADO;
	end;

    CargarPersoneria(cbPersoneria);
    cbPersoneria.ItemIndex := 0;
    cbPersoneria.OnChange(cbPersoneria);
    CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento, FTipoDocumento);
    CargarPaises(DMConnections.BaseCAC, cb_pais, PAIS_CHILE);
    FPIN := '';
    //cbMotivosContacto.Items.InsertItem(MSG_NINGUN_MOTIVO_CONTACTO, null).Index := 0;
    //cbRegionesParticular.Items.InsertItem(SIN_ESPECIFICAR, null).Index := 0;
    ModalResult := mrCancel;
	Result := True;
end;

procedure TNavWindowDatosContacto.RefreshData;
begin
	inherited;
	CargarContacto(FCodigoContacto);
end;

procedure TNavWindowDatosContacto.btn_editarClick(Sender: TObject);
begin
    Close;
end;

function TNavWindowDatosContacto.CargarContacto(CodigoContacto: Integer): Boolean;
begin
    CodigoDomicilioParticular := -1;
    CodigoDomicilioComercial  := -1;
    codigodomicilioentrega    := -1;

	ObtenerPersona.Parameters.ParamByName('@CodigoPersona').Value := CodigoContacto;
	ObtenerComunicaciones.Parameters.ParamByName('@CodigoPersona').Value := CodigoContacto;

	try
		ObtenerPersona.Open;
		ObtenerComunicaciones.Open;
		Result := ObtenerPersona.RecordCount > 0;
	except
		Result := False;
	end;
	if not Result then Exit;
	//
	FCodigoContacto := CodigoContacto;
	With ObtenerPersona do begin
        CargarPersoneria(cbPersoneria, FieldByName('Personeria').AsString);
        cbPersoneriaChange(cbPersoneria);
		txt_nombre.Text := TrimRight(FieldByName('Nombre').AsString);
		txt_Apellido.Text := TrimRight(FieldByName('Apellido').AsString);
        txt_ApellidoMaterno.Text := TrimRight(FieldByName('ApellidoMaterno').AsString);
		CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento, FieldByName('CodigoDocumento').AsString);
		mcTipoNumeroDocumento.MaskText := FieldByName('NumeroDocumento').AsString;
		CargarSexos(cb_Sexo, FieldByName('Sexo').AsString);
		txt_fechanacimiento.Date := FieldByName('FechaNacimiento').AsDateTime;
		CargarPaises(DMConnections.BaseCAC, cb_Pais, FieldByName('LugarNacimiento').AsString);
        if not FieldByName('codigodomicilioentrega').IsNull then
            codigodomicilioentrega := FieldByName('codigodomicilioentrega').AsInteger;
        FPIN := FieldByName('PIN').AsString;
	end;

    //***** DOMICILIO PARTICULAR ******//
    codigodomicilioparticular := ObtenerCodigoDomicilioPersona(FCodigoContacto, TIPO_ORIGEN_DATO_PARTICULAR);
    if (codigodomicilioparticular <> -1) then begin
        with ObtenerDatosDomicilio do
        begin
            Close;
            Parameters.ParamByName('@CodigoDomicilio').Value := codigodomicilioparticular;
            Open;
            if IsEmpty then begin
                LimpiarRegistroDomicilio(DomicilioParticular, TIPO_ORIGEN_DATO_PARTICULAR);
            end
            else begin
                DomicilioParticular.CodigoDomicilio := codigodomicilioparticular;
                {
                //IndiceDomicilio Lo utilizo para indicar que esta ingresado. Si tiene -1 es que no hay datos!!!
                DomicilioParticular.IndiceDomicilio := 1;
                DomicilioParticular.Descripcion := '';
//                DomicilioParticular.CodigoTipoOrigenDato := TIPO_ORIGEN_DATO_PARTICULAR;
                DomicilioParticular.CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
                DomicilioParticular.CodigoTipoCalle := iif(FieldByName('CodigoTipoCalle').IsNull, -1,FieldByName('CodigoTipoCalle').AsInteger);
                DomicilioParticular.NumeroCalle := FieldByName('Numero').AsInteger;
                DomicilioParticular.Piso := FieldByName('Piso').AsInteger;
                DomicilioParticular.Dpto := FieldByName('Dpto').AsString;
                DomicilioParticular.Detalle := FieldByName('Detalle').AsString;
                DomicilioParticular.CodigoPostal := FieldByName('CodigoPostal').AsString;
                DomicilioParticular.CodigoPais := FieldByName('CodigoPais').AsString;
                DomicilioParticular.CodigoRegion := FieldByName('CodigoRegion').AsString;
                DomicilioParticular.CodigoComuna := FieldByName('CodigoComuna').AsString;
                DomicilioParticular.CodigoCiudad := FieldByName('CodigoCiudad').AsString;
                DomicilioParticular.CalleDesnormalizada := FieldByName('CalleDesnormalizada').AsString;
                DomicilioParticular.CodigoTipoEdificacion := FieldByName('CodigoTipoEdificacion').AsInteger;
                DomicilioParticular.DomicilioEntrega := false;
                DomicilioParticular.CodigoCalleRelacionadaUno := iif(FieldByName('CodigoCalleRelacionadaUno').IsNull, -1,FieldByName('CodigoCalleRelacionadaUno').AsInteger);
                DomicilioParticular.NumeroCalleRelacionadaUno := iif(FieldByName('NumeroCalleRelacionadaUno').IsNull, -1,FieldByName('NumeroCalleRelacionadaUno').AsInteger);
                DomicilioParticular.CodigoCalleRelacionadaDos := iif(FieldByName('CodigoCalleRelacionadaDos').IsNull, -1,FieldByName('CodigoCalleRelacionadaDos').AsInteger);
                DomicilioParticular.NumeroCalleRelacionadaDos := iif(FieldByName('NumeroCalleRelacionadaDos').IsNull, -1,FieldByName('NumeroCalleRelacionadaDos').AsInteger);
                DomicilioParticular.Activo := FieldByName('Activo').AsBoolean;
                }
            end;
        end;
        if (codigodomicilioparticular = codigodomicilioentrega) then
            MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR);
    end
    else LimpiarRegistroDomicilio(DomicilioParticular, TIPO_ORIGEN_DATO_PARTICULAR);

    //***** DOMICILIO COMERCIAL ******//
    codigodomiciliocomercial := ObtenerCodigoDomicilioPersona(FCodigoContacto, TIPO_ORIGEN_DATO_COMERCIAL);
    if (codigodomiciliocomercial <> -1) then begin
        with ObtenerDatosDomicilio do
        begin
            Close;
            Parameters.ParamByName('@CodigoDomicilio').Value := codigodomiciliocomercial;
            Open;
            if IsEmpty then begin
                LimpiarRegistroDomicilio(DomicilioComercial, TIPO_ORIGEN_DATO_COMERCIAL);
            end
            else begin
                Domiciliocomercial.CodigoDomicilio := codigodomiciliocomercial;
                {
                Domiciliocomercial.IndiceDomicilio := 1;
                Domiciliocomercial.Descripcion := '';
//                Domiciliocomercial.CodigoTipoOrigenDato := TIPO_ORIGEN_DATO_COMERCIAL;
                Domiciliocomercial.CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
                DomicilioComercial.CodigoTipoCalle := iif(FieldByName('CodigoTipoCalle').IsNull, -1,FieldByName('CodigoTipoCalle').AsInteger);
                DomicilioComercial.NumeroCalle := FieldByName('Numero').AsInteger;
                DomicilioComercial.Piso := FieldByName('Piso').AsInteger;
                DomicilioComercial.Dpto := FieldByName('Dpto').AsString;
                DomicilioComercial.Detalle := FieldByName('Detalle').AsString;
                DomicilioComercial.CodigoPostal := FieldByName('CodigoPostal').AsString;
                DomicilioComercial.CodigoPais := FieldByName('CodigoPais').AsString;
                DomicilioComercial.CodigoRegion := FieldByName('CodigoRegion').AsString;
                DomicilioComercial.CodigoComuna := FieldByName('CodigoComuna').AsString;
                DomicilioComercial.CodigoCiudad := FieldByName('CodigoCiudad').AsString;
                DomicilioComercial.CalleDesnormalizada := FieldByName('CalleDesnormalizada').AsString;
                DomicilioComercial.CodigoTipoEdificacion := FieldByName('CodigoTipoEdificacion').AsInteger;
                DomicilioComercial.DomicilioEntrega := false;
                DomicilioComercial.CodigoCalleRelacionadaUno := iif(FieldByName('CodigoCalleRelacionadaUno').IsNull, -1,FieldByName('CodigoCalleRelacionadaUno').AsInteger);
                DomicilioComercial.NumeroCalleRelacionadaUno := iif(FieldByName('NumeroCalleRelacionadaUno').IsNull, -1,FieldByName('NumeroCalleRelacionadaUno').AsInteger);
                DomicilioComercial.CodigoCalleRelacionadaDos := iif(FieldByName('CodigoCalleRelacionadaDos').IsNull, -1,FieldByName('CodigoCalleRelacionadaDos').AsInteger);
                DomicilioComercial.NumeroCalleRelacionadaDos := iif(FieldByName('NumeroCalleRelacionadaDos').IsNull, -1,FieldByName('NumeroCalleRelacionadaDos').AsInteger);
                DomicilioComercial.Activo := FieldByName('Activo').AsBoolean;
                }
            end;
        end;
        if (codigodomiciliocomercial = codigodomicilioentrega) then
            MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL);
    end
    else LimpiarRegistroDomicilio(DomicilioComercial, TIPO_ORIGEN_DATO_COMERCIAL);


    //*** COMPLETAR  ***//
    (* FIXME
    PrepararMostrarDomicilio (iif(codigodomicilioparticular < 1, '', ArmarDomicilioCompletoPorCodigo(DMConnections.BaseCAC, codigodomicilioparticular)),
                                TIPO_ORIGEN_DATO_PARTICULAR);
    PrepararMostrarDomicilio (iif(codigodomiciliocomercial < 1, '', ArmarDomicilioCompletoPorCodigo(DMConnections.BaseCAC, codigodomiciliocomercial)),
                                TIPO_ORIGEN_DATO_COMERCIAL);
    *)
    //Traigo el telefono particular y comercial y el mail
    txtTelefonoParticular.Text := '';
    txtTelefonoComercial.Text := '';
    txtTelefonoMovil.Text := '';
    txtEmailParticular.Text := '';

    txtTelefonoParticular.Text := ObtenerValorMediosComunicacion(FCodigoContacto, MC_TELEPHONE, TIPO_ORIGEN_DATO_PARTICULAR);
    txtTelefonoComercial.Text := ObtenerValorMediosComunicacion(FCodigoContacto, MC_TELEPHONE, TIPO_ORIGEN_DATO_COMERCIAL);
    txtTelefonoMovil.Text := ObtenerValorMediosComunicacion(FCodigoContacto, MC_MOVIL_PHONE, TIPO_ORIGEN_DATO_PARTICULAR);
    txtEMailParticular.Text := ObtenerValorMediosComunicacion(FCodigoContacto, MC_EMAIL, TIPO_ORIGEN_DATO_PARTICULAR);
	ObtenerPersona.Close;
end;

procedure TNavWindowDatosContacto.Loaded;
begin
	inherited;
	Color := $00FFFDFB;
end;

procedure TNavWindowDatosContacto.btn_GuardarDatosClick(Sender: TObject);
resourcestring
    // Mensajes de confirmaci�n
    MSG_DATOS_CONTACTO          = '�Est� seguro de que desea cambiar los datos del Contacto?';
    MSG_DOCUMENTO               = 'El n�mero de documento especificado es incorrecto.';
    CAPTION_ACTUALIZAR_CONTACTO = 'Actualizar Contacto';
    MSG_ACTUALIZAR_ERROR		= 'No se pudieron actualizar los datos del contacto.';
    MSG_ACTUALIZAR_DOMICILIO_ERROR = 'No se pudo actualizar el domicilio del contacto.';
    // Mensajes de resultado
    MSG_ACTUALIZACION           = 'Los datos se actualizaron correctamente.';
    MSG_SU_NRO_DE_PIN           = 'Su n�mero de PIN es: ';
    MSG_ELIMINAR_DOMICILIO      =   'No se puede eliminar el domicilio. Puede que est� indicado como domicilio de entrega.';
var
    CodigoPersonaActual: integer;
    msgFinal: AnsiString;
    TodoOk: boolean;
begin
    CodigoPersonaActual := FCodigoContacto;
    if PuedeGuardar then begin
        //****** INICIO GRABAR DATOS DE LA PERSONA (CONTACTO) ******// 
        Screen.Cursor   := crHourGlass;
        DMConnections.BaseCAC.BeginTrans;
        TodoOk := false;
        try
            try
                //if alMaestroPersonal.Estado = Alta then CodigoPersonaActual := -1;

                ActualizarDatosPersona.Close;
                With ActualizarDatosPersona.Parameters do begin

                    ParamByName('@Personeria').Value        := iif( cbPersoneria.ItemIndex < 0, null,  Trim(StrRight( cbPersoneria.Text, 1)));
                    ParamByName('@Nombre').Value			:= iif(Trim(txt_Nombre.Text)= '', null, Trim(txt_Nombre.Text));
                    ParamByName('@Apellido').Value			:= iif(Trim(txt_Apellido.Text)= '', null, Trim(txt_Apellido.Text));

                    if StrRight( Trim(cbPersoneria.Text), 1) = PERSONERIA_FISICA then begin
                        ParamByName('@ApellidoMaterno').Value	    := iif(Trim(txt_ApellidoMaterno.Text)= '', null, Trim(txt_ApellidoMaterno.Text));
                        ParamByName('@Sexo').Value 				    := iif( cb_Sexo.ItemIndex < 0, null, Trim(cb_Sexo.Text)[1]);
                        ParamByName('@ContactoComercial').Value	    := null;
                    end else if StrRight( Trim(cbPersoneria.Text), 1) = PERSONERIA_JURIDICA then begin
                        ParamByName('@ApellidoMaterno').Value	    := null;
                        ParamByName('@Sexo').Value 				    := null;
                        ParamByName('@ContactoComercial').Value	    := iif(Trim(txt_ApellidoMaterno.Text)= '', null, Trim(txt_ApellidoMaterno.Text));
                    end;

                    ParamByName('@CodigoDocumento').Value	    := Trim(StrRight(mcTipoNumeroDocumento.ComboText, 10));
                    ParamByName('@NumeroDocumento').Value	    := iif(Trim(mcTipoNumeroDocumento.MaskText)= '', null, Trim(mcTipoNumeroDocumento.MaskText));

                    ParamByName('@LugarNacimiento').Value 	    := iif( cb_pais.ItemIndex < 0, null,  Trim( StrRight(cb_pais.Text, 10)));
                    ParamByName('@FechaNacimiento').Value 	    := iif( txt_fechanacimiento.date < 0, null, txt_fechanacimiento.Date);
                    //ParamByName('@CodigoSituacionIVA').Value    := iif( cbSituacionIVA.ItemIndex < 0, null,  Trim(StrRight( cbSituacionIVA.Text, 2)));
                    //ParamByName('@CodigoActividad').Value 	    := iif( cbActividades.ItemIndex < 0, null,  Ival(StrRight( Trim(cbActividades.Text), 10)));
                    //***chequear
                    ParamByName('@PIN').Value                   := trim(FPIN);
                    ParamByName('@CodigoPersona').Value 	    := CodigoPersonaActual;
                    ParamByName('@CodigoUsuario').Value 	    := UsuarioSistema;            //SS_1419_NDR_20151130
                end;
                ActualizarDatosPersona.ExecProc;
                CodigoPersonaActual :=  ActualizarDatosPersona.Parameters.ParamByName('@CodigoPersona').Value;
                FPIN := ActualizarDatosPersona.Parameters.ParamByName('@PIN').Value;
                ActualizarDatosPersona.Close;
            except
                On E: Exception do begin
                    ActualizarDatosPersona.Close;
                    MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONSTOP);
                    Screen.Cursor := crDefault;
                    Exit;
                end;
            end;

            //Ahora grabo los domicilios
            //Borrar todos los domicilios que figuran en la base que ya no pertenecen al cliente.
            //Grabo cada uno individualmente
            if CodigoPersonaActual > 0 then begin
                //Si cargo el domicilio particular, lo guardamos

                if (DomicilioParticular.IndiceDomicilio = 1) then begin
                    with ActualizarDomicilio.Parameters do begin
                        ParamByName('@CodigoPersona').Value         := CodigoPersonaActual;
                        ParamByName('@CodigoTipoOrigenDato').Value  := TIPO_ORIGEN_DATO_PARTICULAR;
                        ParamByName('@CodigoCalle').Value           := iif( DomicilioParticular.CodigoCalle < 1, null, DomicilioParticular.CodigoCalle);
                        ParamByName('@Numero').Value                := iif( DomicilioParticular.NumeroCalle < 1, null, DomicilioParticular.NumeroCalle);
                        ParamByName('@Piso').Value                  := iif( DomicilioParticular.Piso < 1, null, DomicilioParticular.Piso);
                        ParamByName('@Depto').Value                  := iif(DomicilioParticular.Dpto = '', null,  Trim(DomicilioParticular.Dpto));
                        ParamByName('@Detalle').Value               := iif(DomicilioParticular.Detalle = '', null,  Trim(DomicilioParticular.Detalle));
                        ParamByName('@CalleDesnormalizada').Value   := iif(DomicilioParticular.CodigoCalle >= 1, null,  Trim(DomicilioParticular.CalleDesnormalizada));
                        ParamByName('@CodigoPostal').Value          := iif(DomicilioParticular.CodigoPostal = '', null,  Trim(DomicilioParticular.CodigoPostal));
                        ParamByName('@Activo').Value                := DomicilioParticular.Activo;
                        ParamByName('@CodigoDomicilio').Value       := iif(CodigoDomicilioParticular < 1, null, CodigoDomicilioParticular);
                    end;
                    ActualizarDomicilio.ExecProc;
                    CodigoDomicilioParticular :=  ActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value;
                    if (DomicilioParticular.CodigoCalle < 1) then begin
                        with ActualizarDomicilioRelacionado do
                        begin
                            Parameters.Refresh;
                            if (DomicilioParticular.CodigoCalleRelacionadaUno > 0) then begin
                                Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioParticular;
                                Parameters.ParamByName('@CodigoCalle').Value := DomicilioParticular.CodigoCalleRelacionadaUno;
                                Parameters.ParamByName('@Numero').Value := DomicilioParticular.NumeroCalleRelacionadaUno;
                                ExecProc;
                            end;
                            Close;
                            if (DomicilioParticular.CodigoCalleRelacionadaDos > 0) then begin
                                Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioParticular;
                                Parameters.ParamByName('@CodigoCalle').Value := DomicilioParticular.CodigoCalleRelacionadaDos;
                                Parameters.ParamByName('@Numero').Value := DomicilioParticular.NumeroCalleRelacionadaDos;
                                ExecProc;
                            end;
                        end;
                    end;
                    //Si no marco ninguno de los dos domicilios y carga uno, lo marco como entrega
                    if EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR) then begin
                        CodigoDomicilioEntrega := CodigoDomicilioParticular;
                    end
                    else
                        if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) and
                           (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) then begin
                            MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR);
                            CodigoDomicilioEntrega := CodigoDomicilioParticular;
                        end;
                end;
                if (DomicilioComercial.IndiceDomicilio = 1) then begin
                    with ActualizarDomicilio.Parameters do begin
                        ParamByName('@CodigoPersona').Value         := CodigoPersonaActual;
                        ParamByName('@CodigoTipoOrigenDato').Value  := TIPO_ORIGEN_DATO_COMERCIAL;
                        ParamByName('@CodigoCalle').Value           := iif( DomicilioComercial.CodigoCalle < 1, null, DomicilioComercial.CodigoCalle);
                        ParamByName('@Numero').Value                := iif( DomicilioComercial.NumeroCalle < 1, null, DomicilioComercial.NumeroCalle);
                        ParamByName('@Piso').Value                  := iif( DomicilioComercial.Piso < 1, null, DomicilioComercial.Piso);
                        ParamByName('@Depto').Value                  := iif(DomicilioComercial.Dpto = '', null,  Trim(DomicilioComercial.Dpto));
                        ParamByName('@Detalle').Value               := iif(DomicilioComercial.Detalle = '', null,  Trim(DomicilioComercial.Detalle));
                        ParamByName('@CalleDesnormalizada').Value   := iif(DomicilioParticular.CodigoCalle >= 1, null,  Trim(DomicilioParticular.CalleDesnormalizada));
                        ParamByName('@CodigoPostal').Value          := iif(DomicilioComercial.CodigoPostal = '', null,  Trim(DomicilioComercial.CodigoPostal));
                        ParamByName('@Activo').Value                := DomicilioComercial.Activo;
                        ParamByName('@CodigoDomicilio').Value       := iif(CodigoDomicilioComercial < 1, null, CodigoDomicilioComercial);
                    end;
                    ActualizarDomicilio.ExecProc;
                    CodigoDomicilioComercial :=  ActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value;
                    if (DomicilioComercial.CodigoCalle < 1) then begin
                        with ActualizarDomicilioRelacionado do
                        begin
                            Parameters.Refresh;
                            if (DomicilioComercial.CodigoCalleRelacionadaUno > 0) then begin
                                Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioComercial;
                                Parameters.ParamByName('@CodigoCalle').Value := DomicilioComercial.CodigoCalleRelacionadaUno;
                                Parameters.ParamByName('@Numero').Value := DomicilioComercial.NumeroCalleRelacionadaUno;
                                ExecProc;
                            end;
                            Close;
                            if (DomicilioComercial.CodigoCalleRelacionadaDos > 0) then begin
                                Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioComercial;
                                Parameters.ParamByName('@CodigoCalle').Value := DomicilioComercial.CodigoCalleRelacionadaDos;
                                Parameters.ParamByName('@Numero').Value := DomicilioComercial.NumeroCalleRelacionadaDos;
                                ExecProc;
                            end;
                        end;
                    end;

                    //Si no marco ninguno de los dos domicilios y carga uno, lo marco como entrega
                    if EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL) then begin
                        CodigoDomicilioEntrega := CodigoDomicilioComercial;
                    end
                    else
                        if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) and
                           (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) then begin
                            MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL);
                            CodigoDomicilioEntrega := CodigoDomicilioComercial;
                        end;
                end;
            end;
            //Fin Domicilios.

            if (CodigoDomicilioEntrega > 0) then begin
                //Actualizo en los datos de la persona el Codigo de Domicilio de Entrega.
                try
                    ActualizarDomicilioEntregaPersona.Close;
                    with ActualizarDomicilioEntregaPersona.Parameters do begin
                        ParamByName('@CodigoPersona').Value             := CodigoPersonaActual;
                        ParamByName('@CodigoDomicilioEntrega').Value    := CodigoDomicilioEntrega;
                    end;
                    ActualizarDomicilioEntregaPersona.ExecProc;
                except
                    On E: Exception do begin
                        ActualizarDomicilioEntregaPersona.Close;
                        MsgBoxErr(MSG_ACTUALIZAR_DOMICILIO_ERROR, e.message,
                                  CAPTION_ACTUALIZAR_CONTACTO, MB_ICONSTOP);
                        Screen.Cursor := crDefault;
                        Exit;
                    end;
                end;
            end;

            //Eliminar los domicilios
            if (chkEliminarDomicilioParticular.Checked) and
               (CodigoDomicilioParticular > 0) then begin
                with EliminarDomicilioPersona do
                begin
                    Close;
                    Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioParticular;
                    try
                        ExecProc;
                    except
                        on E: exception do
                            raise exception.Create(MSG_ELIMINAR_DOMICILIO);
                    end;
                end;
            end;
            if (chkEliminarDomicilioComercial.Checked) and
               (CodigoDomicilioComercial > 0) then begin
                with EliminarDomicilioPersona do
                begin
                    Close;
                    Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioComercial;
                    try
                        ExecProc;
                    except
                        on E: exception do
                            raise exception.Create(MSG_ELIMINAR_DOMICILIO);
                    end;
                end;
            end;
            
            //Ahora grabo los Medios de Contacto
            GuardarMediosComunicacionContacto(CodigoPersonaActual);
            TodoOK := True;
        finally
            if TodoOK then DMConnections.BaseCAC.CommitTrans
            else DMConnections.BaseCAC.RollbackTrans;
            Screen.Cursor   := crDefault;
        end;

        //****** FIN GRABAR DATOS DE LA PERSONA (CONTACTO) ******/

        //Muestro un solo mensaje, para ser menos molesto en el Alta que sino muestra 2
        // Verificamos si estamos dando de alta un nuevo ontacto, si es asi informamos del pin.
        if (CodigoPersonaActual <> FCodigoContacto) then
            msgFinal := MSG_ACTUALIZACION + CRLF + CRLF + MSG_SU_NRO_DE_PIN + Trim(FPIN)
        else
            msgFinal := MSG_ACTUALIZACION;
            
        MsgBox(msgFinal, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONINFORMATION);
        ModalResult := mrOk;
        if cerrarAlGrabar then
            close;
        CargarContacto(CodigoPersonaActual);
        Screen.Cursor   := crDefault;
    end;
end;

procedure TNavWindowDatosContacto.dg_OrdenesServicioDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var Porc, tot, hecho, X0, X1: Integer;
	Texto: AnsiString;
begin
	if (Column.FieldName = '') and
	  not (Sender as TDPSGrid).Datasource.dataset.FieldByName('CodigoOrdenServicio').IsNull then begin
		with (Sender as TDPSGrid).Canvas, (Sender as TDPSGrid).Datasource.dataset do begin
			FillRect(Rect);
			if FieldByName('DuracionMaxima').AsInteger = 0 then begin
				Porc := 100;
				Texto := '0:00';
			end else begin
				Tot := FieldByName('DuracionMaxima').AsInteger;
				Hecho := Round((Now - FieldByName('FechaHoraInicio').AsDateTime) * 24 * 60);
				Porc := Min(Round(Hecho / Tot * 100), 100);
				Porc := Max(0, Porc);
				if Hecho > Tot then Texto := '0:00' else Texto := TimeAsText((Tot - Hecho) / 24 / 60);
				end;
			x0 := Rect.Left + 4;
			TextOut(x0, Rect.Top + 1, Texto);
			Inc(x0, TextWidth(Texto) + 4);
			if Porc = 0 then begin
				Brush.Color := clWindow;
				Rectangle(x0, Rect.Top + 2, Rect.Right - 4, Rect.Bottom - 2);
			end else if Porc = 100 then begin
				Brush.Color := RGB(255, 120, 120);
				Rectangle(x0, Rect.Top + 2, Rect.Right - 4, Rect.Bottom - 2);
			end else begin
				x1 := x0 + Round((Rect.Right - 4 - x0) * Porc / 100);
				Brush.Color := RGB(255, 120, 120);
				Rectangle(x0, Rect.Top + 2, x1, Rect.Bottom - 2);
				Brush.Color := clWindow;
				Rectangle(x1 - 1, Rect.Top + 2, Rect.Right - 4, Rect.Bottom - 2);
			end;
		end;
	end;
end;

procedure TNavWindowDatosContacto.dg_OrdenesServicioLinkClick(
  Sender: TDPSGrid; Column: TColumn);
resourcestring
    MSG_ORDENSERVICIO = 'El contacto no tiene una orden de servicio asociada.';
    CAPTION_ORDENSERVICIO = 'Ordenes de Servicio';

var f: TFormWorkflowDisplay;
begin
	if (Column.FieldName = 'Ver') and (not ObtenerComunicaciones.isEmpty) then begin
		if trim(ObtenerComunicaciones.FieldByName('Descripcion').AsString) = '' then begin
			msgBox(MSG_ORDENSERVICIO, CAPTION_ORDENSERVICIO, MB_OK)
		end else begin
			Application.CreateForm(TFormWorkflowDisplay, f);
			if f.Inicializa(ObtenerComunicaciones.FieldByName('CodigoOrdenServicio').AsInteger,
            	ObtenerComunicaciones.FieldByName('CodigoWorkflow').AsInteger) then f.ShowModal;
			f.Release;
		end;
	end;
end;
function TNavWindowDatosContacto.GetApellido: AnsiString;
begin
	Result := Trim(txt_Apellido.Text);
end;


function TNavWindowDatosContacto.GetNombre: AnsiString;
begin
	Result := Trim(txt_Nombre.Text);
end;

function TNavWindowDatosContacto.GetNumeroDocumento: AnsiString;
begin
    result := mcTipoNumeroDocumento.MaskText;
end;

procedure TNavWindowDatosContacto.SetNumeroDocumento(
  const Value: AnsiString);
begin
    if mcTipoNumeroDocumento.MaskText = value then exit;
    mcTipoNumeroDocumento.MaskText := value;
end;

procedure TNavWindowDatosContacto.setApellido(const Value: AnsiString);
begin
    if txt_Apellido.Text = Value then exit;
    txt_Apellido.Text := Value;
end;

procedure TNavWindowDatosContacto.setNombre(const Value: AnsiString);
begin
    if txt_Nombre.Text = Value then exit;
    txt_Nombre.Text := Value;
end;

procedure TNavWindowDatosContacto.TimerTimer(Sender: TObject);
begin
    Timer.enabled := False;
    try
        inherited;
        ObtenerComunicaciones.close;
    	ObtenerComunicaciones.Parameters.ParamByName('@CodigoPersona').Value := FCodigoContacto;
	    try
		    ObtenerComunicaciones.Open;
	    except
	    end;
    finally
        Timer.enabled := true;
    end;
end;

function TNavWindowDatosContacto.GetApellidoMaterno: AnsiString;
begin
   	Result := Trim(txt_ApellidoMaterno.Text);
end;

procedure TNavWindowDatosContacto.setApellidoMaterno(
  const Value: AnsiString);
begin
    if txt_ApellidoMaterno.Text = Value then exit;
    txt_ApellidoMaterno.Text := Value;
end;

procedure TNavWindowDatosContacto.HabilitarCamposPersoneria(
  Personeria: Char);
ResourceString
    LBL_PERSONA_RAZON_SOCIAL        = 'Raz�n Social';
    LBL_PERSONA_NOMBRE_FANTASIA     = 'Nombre de Fantas�a:';
    LBL_PERSONA_SITUACION_IVA       = 'Situaci�n de IVA:';
    LBL_PERSONA_FECHA_CREACION      = 'Fecha de Creaci�n:';
    LBL_PERSONA_APELLIDO            = 'Apellido:';
    LBL_PERSONA_NOMBRE              = 'Nombre:';
    LBL_PERSONA_APELLIDO_MATERNO    = 'Apellido Materno:';
    LBL_PERSONA_CONTACTO_COMERCIAL  = 'Contacto Comercial:';
    LBL_PERSONA_SEXO                = 'Sexo:';
    LBL_PERSONA_FECHA_NACIMIENTO    = 'Fecha de Nacimiento:';

begin
    Case Personeria of
        PERSONERIA_JURIDICA:
            begin
                lblApellido.Caption         := LBL_PERSONA_RAZON_SOCIAL;
                lblNombre.Caption           := LBL_PERSONA_NOMBRE_FANTASIA;
                lblApellidoMaterno.Caption  := LBL_PERSONA_CONTACTO_COMERCIAL;
                lblSexo.Visible             := False;
                cb_Sexo.Visible             := False;
                lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_CREACION;
                EsPersonaFisica             := false;
            end;
        PERSONERIA_FISICA:
            begin
                lblApellido.Caption         := LBL_PERSONA_APELLIDO;
                lblNombre.Caption           := LBL_PERSONA_NOMBRE;
                lblApellidoMaterno.Caption  := LBL_PERSONA_APELLIDO_MATERNO;
                lblSexo.Caption             := LBL_PERSONA_SEXO;
                lblSexo.Visible             := True;
                cb_Sexo.Visible             := True;
                lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_NACIMIENTO;
                EsPersonaFisica             := true;
            end;
    end;
end;

procedure TNavWindowDatosContacto.cbPersoneriaChange(Sender: TObject);
begin
    HabilitarCamposPersoneria( StrRight(Trim(cbPersoneria.Text), 1)[1]);
end;

function TNavWindowDatosContacto.PuedeGuardar: boolean;
resourcestring
    // Mensajes de confirmaci�n
    MSG_DOCUMENTO               = 'El n�mero de documento especificado es incorrecto.';
    MSG_DATOS_CONTACTO          = '�Est� seguro de que desea cambiar los datos del Contacto?';
    // Mensajes de Validaci�n
    CAPTION_ACTUALIZAR_CONTACTO = 'Actualizar contacto';
    CAPTION_VALIDAR_CONTACTO    = 'Validar datos del Contacto';
    MSG_NOMBRE                  = 'Debe indicar un nombre.';
    MSG_APELLIDO                = 'Debe indicar un apellido.';
    MSG_PAIS                    = 'Debe indicar el pa�s de residencia.';
    MSG_ERROR_SEXO              = 'Debe indicar el Sexo del Contacto.';
    MSG_ERROR_DIRECCION         = 'La direcci�n de mail ingresada es incorrecta.';
    MSG_ERROR_NOMBRE_APELLIDO   = 'Se deben ingresar al menos nombre y apellido.';
    MSG_MAIL                    = 'La direcci�n de mail es inv�lida.';
    MSG_FECHA                   = 'La fecha es inv�lida.';
    MSG_DOMICILIO_INCOMPLETO    = 'Para el domicilio debe indicar la calle, el n�mero y la comuna.';
    MSG_FALTA_MEDIO_CONTACTO    = 'Debe ingresar por lo menos un medio de comunicaci�n.';
    MSG_FALTA_APELLIDO_MATERNO  = 'No ingres� el Apellido Materno.';
    MSG_FALTA_DOMICILIO_ENTREGA = 'Debe seleccionar el domicilio de entrega.';
    MSG_ERROR_DOMICILIO_ENTREGA = 'Eligi� un domicilio de entrega que no esta ingresado.';
    MSG_ERROR_DOMICILIO_ENTREGA_BORRAR = 'Eligi� un domicilio de entrega que est� marcado para ser borrado.';
var
    FaltaPreguntar, ok: boolean;

begin
    ok := true;
    FaltaPreguntar := true;
    if (not mcTipoNumeroDocumento.ValidateMask) then begin
        MsgBox(MSG_DOCUMENTO, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONSTOP);
        ok := false;
    end
    else begin
        // Validamos los datos
        if not ValidateControls(
          [txt_nombre, txt_apellido, txt_fechanacimiento],
          [Trim(txt_nombre.text) <> '', Trim(txt_apellido.text) <> '',
          (txt_fechanacimiento.Text = '  /  /    ') or FechaNacimientoValida(txt_fechanacimiento.Date)],
          CAPTION_VALIDAR_CONTACTO, [MSG_NOMBRE, MSG_APELLIDO, MSG_FECHA]) then
            ok := false
        else begin
            // Validamos los datos de la persona jur�dica
            {
            if EsPersonaFisica then begin
                if not ValidateControls(
                  [cb_sexo],
                  [cb_sexo.ItemIndex >= 0],
                  CAPTION_VALIDAR_CONTACTO,
                  [MSG_ERROR_SEXO]) then ok := false;
            end;
            }
            if ((trim(txtTelefonoParticular.Text) = '') and
                (trim(txtTelefonoComercial.Text) = '') and
                (trim(txtTelefonoMovil.Text) = ''))  then begin
                MsgBox(MSG_FALTA_MEDIO_CONTACTO, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONSTOP);
                txtTelefonoParticular.SetFocus;
                ok := false;
            end
            else begin
                {Si no marc� ninguno para entrega entonces
                    Si ingres� los 2,
                        Avisar porque no sabe cual tomar
                    sino
                        nada (si no hay ninguno no pasa nada, pero si hay uno se marcara posteriormente)
                sino  (en este caso marc� alguno)
                    valido que el que esta marcado, este ingresado!!
                }

                if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) and
                   (not EstaElegidoComoDomicilioEntrega( TIPO_ORIGEN_DATO_COMERCIAL)) then begin
                    //si no marco ninguno pero ingres� los 2, avisar!!!
                    if (DomicilioParticular.IndiceDomicilio = 1) and
                       (DomicilioComercial.IndiceDomicilio = 1) then begin
                        MsgBox(MSG_FALTA_DOMICILIO_ENTREGA, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONSTOP);
                        RGDomicilioEntrega.SetFocus;
                        ok := false;
                    end

                end
                else begin
                    if (EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) then begin
                        if (DomicilioParticular.IndiceDomicilio <> 1) then begin
                            MsgBox(MSG_ERROR_DOMICILIO_ENTREGA, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONSTOP);
                            RGDomicilioEntrega.SetFocus;
                            ok := false;
                        end
                        else begin
                            //esta ingresado, pero no tiene que estar marcado para borrar
                            if ChkEliminarDomicilioParticular.Checked then begin
                                MsgBox(MSG_ERROR_DOMICILIO_ENTREGA_BORRAR, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONSTOP);
                                RGDomicilioEntrega.SetFocus;
                                ok := false;
                            end;
                        end;
                    end
                    else begin
                        //en este caso esta elegido el domicilio Comercial
                        if (DomicilioComercial.IndiceDomicilio <> 1) then begin
                            MsgBox(MSG_ERROR_DOMICILIO_ENTREGA, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONSTOP);
                            RGDomicilioEntrega.SetFocus;
                            ok := false;
                        end
                        else begin
                            //esta ingresado, pero no tiene que estar marcado para borrar
                            if ChkEliminarDomicilioComercial.Checked then begin
                                MsgBox(MSG_ERROR_DOMICILIO_ENTREGA_BORRAR, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONSTOP);
                                RGDomicilioEntrega.SetFocus;
                                ok := false;
                            end;
                        end;
                    end;
                    if ok then begin
                        //Valido que si marco un domicilio de entrega que no este indicado para ser borrado
                        if (EsPersonaFisica) and (trim(txt_apellidoMaterno.Text) = '') and
                           ((iif( cb_pais.ItemIndex < 0, null,  Trim( StrRight(cb_pais.Text, 10)))) = PAIS_CHILE) then begin
                            if (MsgBox(MSG_FALTA_APELLIDO_MATERNO + #13#10 +
                                       MSG_DATOS_CONTACTO, CAPTION_ACTUALIZAR_CONTACTO,
                                       MB_YESNO or MB_ICONWARNING) <> IDYES) then begin
                                txt_apellidoMaterno.SetFocus;
                                ok := false;
                            end;
                            FaltaPreguntar := false;
                        end;
                    end;
                end;
            end;
        end;
    end;
    if (FaltaPreguntar) and (Ok) then begin
        result := (MsgBox(MSG_DATOS_CONTACTO, CAPTION_ACTUALIZAR_CONTACTO,
                    MB_YESNO or MB_DEFBUTTON2 or MB_ICONQUESTION) = IDYES);
    end
    else result := ok;
end;

procedure TNavWindowDatosContacto.GuardarMediosComunicacionContacto (persona: integer);
resourcestring
    MSG_ACTUALIZAR_MEDIO_CONTACTO_ERROR = 'No se pudo actualizar el medio de comunicaci�n del contacto';
    MSG_ACTUALIZAR_MEDIO_CONTACTO_CAPTION = 'Actualizar contacto';
begin
    //Guardo los datos en la tabla MediosComunicacion
    try
        //Telefono Particular
        if txtTelefonoParticular.Text <> '' then begin
            GuardarMedioComunicacion(persona, MC_TELEPHONE,
                    TIPO_ORIGEN_DATO_PARTICULAR, txtTelefonoParticular.Text);
        end
        else
        begin
            QueryExecute( DMConnections.BaseCAC,
            'DELETE FROM MediosComunicacion WHERE CodigoPersona = ' + IntToStr(persona) +
            ' AND CodigoMedioContacto = ' + IntToStr(MC_TELEPHONE) +
            ' AND CodigoTipoOrigenDato = ' + IntToStr(TIPO_ORIGEN_DATO_PARTICULAR));
        end;
        //Telefono Comercial
        if txtTelefonoComercial.Text <> '' then begin
            GuardarMedioComunicacion(persona, MC_TELEPHONE,
                    TIPO_ORIGEN_DATO_COMERCIAL, txtTelefonoComercial.Text);
        end
        else begin
            QueryExecute( DMConnections.BaseCAC,
            'DELETE FROM MediosComunicacion WHERE CodigoPersona = ' + IntToStr(persona)+
            ' AND CodigoMedioContacto = ' + IntToStr(MC_TELEPHONE) +
            ' AND CodigoTipoOrigenDato = ' + IntToStr(TIPO_ORIGEN_DATO_COMERCIAL));
        end;
        //Telefono Movil
        if txtTelefonoMovil.Text <> '' then begin
            GuardarMedioComunicacion(persona, MC_MOVIL_PHONE,
                    TIPO_ORIGEN_DATO_PARTICULAR, txtTelefonoMovil.Text);
        end
        else
        begin
            QueryExecute( DMConnections.BaseCAC,
            'DELETE FROM MediosComunicacion WHERE CodigoPersona = ' + IntToStr(persona)+
            ' AND CodigoMedioContacto = ' + IntToStr(MC_MOVIL_PHONE) +
            ' AND CodigoTipoOrigenDato = ' + IntToStr(TIPO_ORIGEN_DATO_PARTICULAR));
        end;
        //Email Particular
        if txtEmailParticular.Text <> '' then begin
            GuardarMedioComunicacion(persona, MC_EMAIL,
                    TIPO_ORIGEN_DATO_COMERCIAL, txtEmailParticular.Text);
        end
        else
        begin
            QueryExecute( DMConnections.BaseCAC,
            'DELETE FROM MediosComunicacion WHERE CodigoPersona = ' + IntToStr(persona)+
            ' AND CodigoMedioContacto = ' + IntToStr(MC_EMAIL) +
            ' AND CodigoTipoOrigenDato = ' + IntToStr(TIPO_ORIGEN_DATO_PARTICULAR));
        end;
    except
        on E: exception do begin
            MsgBoxErr( MSG_ACTUALIZAR_MEDIO_CONTACTO_ERROR, e.message, MSG_ACTUALIZAR_MEDIO_CONTACTO_CAPTION, MB_ICONSTOP);
            ActualizarMedioComunicacionPersona.Close;
            Screen.Cursor := crDefault;
            Exit;
        end;
    end;
end;

procedure TNavWindowDatosContacto.GuardarMedioComunicacion(persona,
  mediocontacto, tipoorigen: integer; valor: AnsiString);
begin
    ActualizarMedioComunicacionPersona.Close;
    with ActualizarMedioComunicacionPersona.Parameters do begin
        ParamByName('@CodigoPersona').Value         := persona;
        ParamByName('@CodigoMedioContacto').Value   := mediocontacto;
        ParamByName('@CodigoTipoOrigenDato').Value  := tipoorigen;
        ParamByName('@Valor').Value                 := valor;
        ParamByName('@CodigoDomicilio').Value       := null;
        ParamByName('@HorarioDesde').Value          := null;
        ParamByName('@HorarioHasta').Value          := null;
        ParamByName('@Observaciones').Value         := null;
        ParamByName('@Activo').Value                := 1;
        ActualizarMedioComunicacionPersona.ExecProc;
    end;
end;

procedure TNavWindowDatosContacto.txtTelefonoParticularKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9','-', #8]) then
        Key := #0;
end;

procedure TNavWindowDatosContacto.txtTelefonoComercialKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9','-', #8]) then
        Key := #0;
end;

procedure TNavWindowDatosContacto.txtTelefonoMovilKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9','-', #8]) then
        Key := #0;
end;

procedure TNavWindowDatosContacto.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

procedure TNavWindowDatosContacto.PrepararMostrarDomicilio(descripcion: AnsiString;
  tipoOrigen: integer);
begin
    if tipoOrigen = TIPO_ORIGEN_DATO_PARTICULAR then begin
        if (descripcion = '') then begin
            BtnDomicilioParticularAccion.Caption := CAPTION_BOTON_DOMICILIO_ALTA;
            ChkEliminarDomicilioParticular.Checked := false;
        end
        else
            BtnDomicilioParticularAccion.Caption := CAPTION_BOTON_DOMICILIO_MODIFICAR;
        ChkEliminarDomicilioParticular.Enabled := (descripcion <> '') and (codigodomicilioparticular > 0);
        MostrarDomicilio (lblDomicilioParticular, descripcion);
    end
    else begin
        if (descripcion = '') then begin
            BtnDomicilioComercialAccion.Caption := CAPTION_BOTON_DOMICILIO_ALTA;
            ChkEliminarDomicilioComercial.Checked := false;
        end
        else
            BtnDomicilioComercialAccion.Caption := CAPTION_BOTON_DOMICILIO_MODIFICAR;
        ChkEliminarDomicilioComercial.Enabled := (descripcion <> '') and (codigodomiciliocomercial > 0);
        MostrarDomicilio (lblDomicilioComercial, descripcion);
    end;
end;

procedure TNavWindowDatosContacto.MostrarDomicilio(var L: TLabel;
  descripcion: AnsiString);
begin
    if descripcion = '' then begin
        l.Font.Style := [fsItalic];
        l.Caption := FALTA_INGRESAR_DOMICILIO;
    end
    else begin
        l.Font.Style := [];
        l.Caption := descripcion;
    end;
end;


procedure TNavWindowDatosContacto.AltaDomicilio(tipoOrigenDatos: integer;
  var datosDomicilio: TDatosDomicilio);
ResourceString
    MSG_AGREGAR_CAPTION = 'Agregar Domicilio';
    MSG_ERROR_AGREGAR   = 'No se ha podido agregar el domicilio';
var
    f: TFormEditarDomicilio;
begin
  {  LimpiarRegistroDomicilio(datosDomicilio, tipoOrigenDatos);
    Application.CreateForm(TFormEditarDomicilio, f);
    if f.Inicializar(tipoOrigenDatos, true) and (f.ShowModal = mrOK) then begin
        try
            with datosDomicilio do begin
                CodigoTipoDomicilio := f.TipoDomicilio;
                //lo utilizo para indicar que hay datos. Si tiene -1 es porque esta vac�o
                IndiceDomicilio := 1;
                CodigoCalle := f.CodigoCalle;
                CodigoTipoCalle := f.TipoCalle;
                NumeroCalle := f.Numero;
                Piso := f.Piso;
                Dpto := f.Depto;
                Detalle := f.Detalle;
                CodigoPostal := f.CodigoPostal;
                CodigoPais := f.Pais;
                CodigoRegion := f.Region;
                CodigoComuna := f.Comuna;
                CodigoCiudad := f.Ciudad;
                CalleDesnormalizada := f.DescripcionCalle;
                CodigoTipoEdificacion := f.TipoEdificacion;
                DomicilioEntrega := f.DomicilioEntrega;
                Normalizado := f.CodigoCalle > 0;
                CodigoCalleRelacionadaUno := f.CalleRelacionadaUno;
                NumeroCalleRelacionadaUno := f.NumeroCalleRelacionadaUno;
                CodigoCalleRelacionadaDos := f.CalleRelacionadaDos;
                NumeroCalleRelacionadaDos := f.NumeroCalleRelacionadaDos;
                Descripcion := f.DomicilioCompleto;
                if f.DomicilioEntrega then MarcarComoDomicilioEntrega(tipoOrigenDatos);
                PrepararMostrarDomicilio(Descripcion,tipoOrigenDatos);
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_AGREGAR, E.message, MSG_AGREGAR_CAPTION, MB_ICONSTOP);
                //dsDomicilios.DataSet.Cancel;
            end;
        end;
    end;
    f.Release;}
end;

procedure TNavWindowDatosContacto.BtnDomicilioParticularAccionClick(
  Sender: TObject);
begin
    if (BtnDomicilioParticularAccion.Caption = CAPTION_BOTON_DOMICILIO_ALTA) then begin
        //Alta
        AltaDomicilio(TIPO_ORIGEN_DATO_PARTICULAR, DomicilioParticular);
        //Si todav�a no eligi� ning�n domicilio para entrega, lo marco
        if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) and
           (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) then
            MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR);
    end
    else begin
        //Modificacion
        ModificarDomicilio(TIPO_ORIGEN_DATO_PARTICULAR, DomicilioParticular);
    end;
end;

procedure TNavWindowDatosContacto.BtnDomicilioComercialAccionClick(
  Sender: TObject);
begin
    if (BtnDomicilioComercialAccion.Caption = CAPTION_BOTON_DOMICILIO_ALTA) then begin
        //Alta
        AltaDomicilio(TIPO_ORIGEN_DATO_COMERCIAL, DomicilioComercial);
        //Si todav�a no eligi� ning�n domicilio para entrega, lo marco
        if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) and
           (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) then
            MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL);
    end
    else begin
        //Modificacion
        ModificarDomicilio(TIPO_ORIGEN_DATO_COMERCIAL, DomicilioComercial);
    end;
end;

procedure TNavWindowDatosContacto.ModificarDomicilio(
  tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
ResourceString
    MSG_EDITAR_CAPTION = 'Modificar Domicilio';
    MSG_ERROR_EDITAR   = 'No se ha podido modificar el domicilio';
var
	f: TFormEditarDomicilio;
begin
    Application.CreateForm(TFormEditarDomicilio, f);
    {
    if f.Inicializar(
        datosDomicilio.CodigoDomicilio,
        tipoOrigenDatos,
        datosDomicilio.CodigoPais,
        datosDomicilio.CodigoRegion,
        datosDomicilio.CodigoComuna,
        datosDomicilio.CodigoCiudad,
        datosDomicilio.CodigoTipoCalle,
        datosDomicilio.CodigoCalle,
        datosDomicilio.CalleDesnormalizada,
        datosDomicilio.NumeroCalle,
        datosDomicilio.Piso,
        datosDomicilio.Dpto,
        datosDomicilio.Detalle,
        datosDomicilio.CodigoPostal,
        datosDomicilio.CodigoTipoEdificacion,
        EstaElegidoComoDomicilioEntrega(tipoOrigenDatos),
        datosDomicilio.CodigoCalleRelacionadaUno,
        datosDomicilio.NumeroCalleRelacionadaUno,
        datosDomicilio.CodigoCalleRelacionadaDos,
        datosDomicilio.NumeroCalleRelacionadaDos
        ) and (f.ShowModal = mrOK) then begin
        try
            with datosDomicilio do begin
                CodigoTipoDomicilio := f.TipoDomicilio;
                IndiceDomicilio := 1; //significa que hay datos
                CodigoCalle := f.CodigoCalle;
                CodigoTipoCalle := f.TipoCalle;
                NumeroCalle := f.Numero;
                Piso := f.Piso;
                Dpto := f.Depto;
                Detalle := f.Detalle;
                CodigoPostal := f.CodigoPostal;
                CodigoPais := f.Pais;
                CodigoRegion := f.Region;
                CodigoComuna := f.Comuna;
                CodigoCiudad := f.Ciudad;
                CalleDesnormalizada := f.DescripcionCalle;
                CodigoTipoEdificacion := f.TipoEdificacion;
                DomicilioEntrega := f.DomicilioEntrega;
                Normalizado := f.CodigoCalle > 0;
                CodigoCalleRelacionadaUno := f.CalleRelacionadaUno;
                NumeroCalleRelacionadaUno := f.NumeroCalleRelacionadaUno;
                CodigoCalleRelacionadaDos := f.CalleRelacionadaDos;
                NumeroCalleRelacionadaDos := f.NumeroCalleRelacionadaDos;
                Descripcion := f.DomicilioCompleto;
                if f.DomicilioEntrega then MarcarComoDomicilioEntrega(tipoOrigenDatos);
                PrepararMostrarDomicilio(Descripcion, tipoOrigenDatos);
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_EDITAR, E.message, MSG_EDITAR_CAPTION, MB_ICONSTOP);
            end;
        end;
    end;
    }
    f.Release;
end;

function TNavWindowDatosContacto.EstaElegidoComoDomicilioEntrega(
  tipoOrigenDatos: integer): boolean;
begin
    if tipoOrigenDatos = TIPO_ORIGEN_DATO_PARTICULAR then
        result := RGDomicilioEntrega.ItemIndex = 0
    else
        if tipoOrigenDatos = TIPO_ORIGEN_DATO_COMERCIAL then
            result := RGDomicilioEntrega.ItemIndex = 1
        else
            result := false;
end;

procedure TNavWindowDatosContacto.MarcarComoDomicilioEntrega(
  tipoOrigenDatos: integer);
begin
    if tipoOrigenDatos = TIPO_ORIGEN_DATO_PARTICULAR then
        RGDomicilioEntrega.ItemIndex := 0
    else
        if tipoOrigenDatos = TIPO_ORIGEN_DATO_COMERCIAL then
            RGDomicilioEntrega.ItemIndex := 1
        else
            RGDomicilioEntrega.ItemIndex := -1;
end;

procedure TNavWindowDatosContacto.ChkEliminarDomicilioParticularClick(
  Sender: TObject);
begin
    ClickCheckEliminar(chkEliminarDomicilioParticular,
                    EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR));
end;

procedure TNavWindowDatosContacto.ChkEliminarDomicilioComercialClick(
  Sender: TObject);
begin
    ClickCheckEliminar(chkEliminarDomicilioComercial,
                    EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL));
end;

end.

