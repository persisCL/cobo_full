{********************************** Unit Header ********************************
File Name : frmEliminarTag.pas
Author : jconcheyro
Date Created: 15/12/2006
Language : ES-AR
Description :

Revision : 1
Author : jconcheyro
Date : 15/12/2006
Description : Se agrega columna Motivo, para abrir el combo que determina el concepto
a crear el movimiento cuenta. Indemnizacion de telev�as

Revision : 2
Author : FSandi
Date : 23-08-2007
Description : Se corrigen labels, posiciones y tama�os
*******************************************************************************}
unit frmEliminarTag;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, PeaProcs, DB, ADODB, DMConnection, UtilDB, Peatypes,
  Util, RStrings, Grids, DBGrids, DBClient, UtilProc, Convenios,
  ImgList, VariantComboBox, Menus, frmSeleccionarMovimientosTelevia, ListBoxEx,
  DBListEx ;

const
    ColumnaPatente          = 0;
    ColumnaTelevia          = 1;
    ColumnaEstadoTAG        = 2;
    ColumnaMotivoTAG        = 3;
    ColumnaRobado           = 5;
type
  TFormEliminarTag = class(TForm)
    Panel1: TPanel;
    dbgEliminarTags: TDBGrid;
    cdsEliminarTelevia: TClientDataSet;
    dsEliminarTelevia: TDataSource;
    Imagenes: TImageList;
    ObtenerEstadosConservacionTAGs: TADOStoredProc;
    btnAceptar: TButton;
    btnSalir: TButton;
    lbl_Patente: TLabel;
    Label1: TLabel;
    Panel2: TPanel;
    Bevel1: TBevel;
    Label9: TLabel;
    Label10: TLabel;
    lblSeleccionar: TLabel;
    cdConceptos: TClientDataSet;
    dsConceptos: TDataSource;
    mnuGrillaConceptos: TPopupMenu;
    mnuEliminarConcepto: TMenuItem;
    AgregarConceptos1: TMenuItem;
    dblConceptos: TDBListEx;
    lblTotalMovimientos: TLabel;
    lblTotalMovimientosTxt: TLabel;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure dbgEliminarTagsExit(Sender: TObject);
    procedure dbgEliminarTagsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cdsEliminarTeleviaBeforePost(DataSet: TDataSet);
    procedure cdsEliminarTeleviaAfterInsert(DataSet: TDataSet);
    procedure dbgEliminarTagsKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure dbgEliminarTagsDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dbgEliminarTagsDblClick(Sender: TObject);
    procedure dbgEliminarTagsColEnter(Sender: TObject);
    procedure dbgEliminarTagsColExit(Sender: TObject);
    procedure cdsEliminarTeleviaAfterScroll(DataSet: TDataSet);
    procedure cdsEliminarTeleviaBeforeScroll(DataSet: TDataSet);
    procedure lblSeleccionarClick(Sender: TObject);
    procedure mnuEliminarConceptoClick(Sender: TObject);
    procedure dblConceptosContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure dblConceptosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AgregarConceptos1Click(Sender: TObject);
    procedure dbgEliminarTagsCellClick(Column: TColumn);

   private
    FCuentaVehiculo: TVehiculosConvenio;
    FCuentasConvenio: TCuentasConvenio;
    EstaIniciando: boolean;
    FDescripcionNODevuelto: String;
    FCantidadTagACobrar: integer;
    FImporteTotalMovimientos: int64;
    function GetEstadoConservacion: integer;
    function GetRobado: Boolean;
    function EsTagVendido: Boolean;
    function EsTagPerdido: Boolean;
    procedure FiltrarGrillaBoxMotivosPorPatente(Patente: string);
    function CargarCuentasMotivosBaja: TADOStoredProc;   //TASK_011_ECA_20160514
    function ValidarEstadoTAGvsMotivoBaja(EstadoTAG, Motivo: string):Boolean;     //TASK_011_ECA_20160514
    function GetCodigoMotivoBaja: integer;                                       //TASK_011_ECA_20160514
    function GetDescripcionMotivoBaja: string;                                     //   TASK_011_JMA_20160505
    function GetDescripcionEstadoTAG: string;                                     //   TASK_011_JMA_20160505
  public
    function Inicializar(Caption: TCaption; CuentaVehiculo: TVehiculosConvenio): Boolean; overload;
    function Inicializar(Caption: TCaption; CuentasConvenio: TCuentasConvenio): Boolean; overload;
    property Robado: Boolean read GetRobado default false;
    property TagVendido: Boolean read EsTagVendido;
    property EstadoConservacion: integer read GetEstadoConservacion default 1;
    property MotivodeBaja: integer read GetCodigoMotivoBaja;
    property DescripcionMotivodeBaja: string read GetDescripcionMotivoBaja;       //   TASK_011_JMA_20160505
    property DescripcionEstadoTAG: string read GetDescripcionEstadoTAG;           //   TASK_011_JMA_20160505
  end;


var
  FormEliminarTag: TFormEliminarTag;
  MotivosBajaDes :  string;

resourcestring
  MSG_ERROR_TELEVIA_VEHICULO = 'El n�mero del telev�a no est� asignado a este veh�culo';
  MSG_ERROR_INGRESE_TELEVIA = 'Ingrese el n�mero de telev�a devuelto';

implementation

{$R *.dfm}

{ TFormAsignarTag }
{**********************************************************************************************
    Revision : 2
    Author : vpaszkowicz
    Date : 31/07/2009
    Description: Cambio el seteo de si est� suspendido usando ahora la propiedad booleana
    EstaSuspendida que se cre� dentro de los datos del veh�culo.
**********************************************************************************************}
function TFormEliminarTag.Inicializar(Caption: TCaption; CuentaVehiculo: TVehiculosConvenio): Boolean;
var
    spAux: TADOStoredProc;
begin
    self.Caption := Caption;
    FCuentaVehiculo := CuentaVehiculo;
    FCuentasConvenio := nil;

    EstaIniciando := True;
    if not cdsEliminarTelevia.Active then begin
        cdsEliminarTelevia.CreateDataSet;
        cdsEliminarTelevia.Open;

        ObtenerEstadosConservacionTAGs.Close;
        ObtenerEstadosConservacionTAGs.Parameters.ParamByName('@CodigoEstadoConservacion').Value := ESTADO_CONSERVACION_NO_DEVUELTO;
        ObtenerEstadosConservacionTAGs.Open;
        FDescripcionNODevuelto := ObtenerEstadosConservacionTAGs.fieldbyname('Descripcion').AsString;

        ObtenerEstadosConservacionTAGs.Close;
        ObtenerEstadosConservacionTAGs.Parameters.ParamByName('@CodigoEstadoConservacion').Value := Null;
        ObtenerEstadosConservacionTAGs.Open;

        dbgEliminarTags.Columns[2].PickList.Add(SIN_ESPECIFICAR);
        while not ObtenerEstadosConservacionTAGs.Eof do begin
            dbgEliminarTags.Columns[2].PickList.Add(Trim(ObtenerEstadosConservacionTAGs.fieldbyname('Descripcion').AsString));
            ObtenerEstadosConservacionTAGs.Next;
        end;

        dbgEliminarTags.Columns[3].PickList.Add(SIN_ESPECIFICAR);
        MotivosBajaDes:='';
        spAux:= TADOStoredProc.Create(nil);
        spAux:=CargarCuentasMotivosBaja;
        while not spAux.Eof do begin
            dbgEliminarTags.Columns.Items[3].PickList.AddObject(Trim(spAux.fieldbyname('DescripcionMotivoBaja').AsString),TObject(spAux.fieldbyname('CodigoMotivoBaja').AsInteger));
            MotivosBajaDes:=MotivosBajaDes + spAux.fieldbyname('CodigoMotivoBaja').AsString + ';' + spAux.fieldbyname('DescripcionMotivoBaja').AsString + '.';
            spAux.Next;
        end;
        spAux.Close;

    end;

    cdsEliminarTelevia.Append;
    cdsEliminarTelevia.FieldByName('Patente').AsString := CuentaVehiculo.Cuenta.Vehiculo.Patente;
    cdsEliminarTelevia.FieldByName('Televia').AsString := SerialNumberToEtiqueta(IntToStr(CuentaVehiculo.Cuenta.ContactSerialNumber));
    cdsEliminarTelevia.FieldByName('EstadoTag').AsString := SIN_ESPECIFICAR;
    cdsEliminarTelevia.FieldByName('Motivo').AsString := SIN_ESPECIFICAR;
    cdsEliminarTelevia.FieldByName('Robado').Value := False;

    cdsEliminarTelevia.FieldByName('CuentaSuspendida').Value := (CuentaVehiculo.Cuenta.EstaSuspendida);

    if CuentaVehiculo.Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_EN_COMODATO then
        cdsEliminarTelevia.FieldByName('Almacen').Value := CONST_STR_ALMACEN_COMODATO;

    if CuentaVehiculo.Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_ARRIENDO then
        cdsEliminarTelevia.FieldByName('Almacen').Value := CONST_STR_ALMACEN_ARRIENDO;

    //SS 769
	if CuentaVehiculo.Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_ENTREGADO_EN_CUOTAS then
        cdsEliminarTelevia.FieldByName('Almacen').Value := CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS;


    if CuentaVehiculo.EsTagVendido or CuentaVehiculo.EsTagPerdido then begin
        cdsEliminarTelevia.FieldByName('EstadoTag').Value := FDescripcionNODevuelto;
        cdsEliminarTelevia.FieldByName('Televia').Value := SerialNumberToEtiqueta(CuentaVehiculo.Cuenta.ContactSerialNumber);
    end;

    cdsEliminarTelevia.Post;
    EstaIniciando := False;
    Result := true;
end;

function TFormEliminarTag.Inicializar(Caption: TCaption;
  CuentasConvenio: TCuentasConvenio): Boolean;
var
    i: integer;
begin
    for i:=0 to CuentasConvenio.CantidadVehiculos - 1 do
        //if not CuentasConvenio.Vehiculos[i].EsTagVendido then
            Inicializar(Caption, CuentasConvenio.Vehiculos[i]);
    FCuentasConvenio := CuentasConvenio;
    self.Caption := Caption;

    result := true;
end;

procedure TFormEliminarTag.lblSeleccionarClick(Sender: TObject);
var
    f: TfrmSeleccionarMovimientosTeleviaFORM;
    IndiceVehiculo: integer;
    AlmacenDestino: Integer; //TASK_016_ECA_20160526
begin
    Application.CreateForm(TfrmSeleccionarMovimientosTeleviaFORM, f);

    //TASK_016_ECA_20160526
    if cdsEliminarTelevia.RecordCount=1 then
       AlmacenDestino:= FCuentaVehiculo.Cuenta.CodigoAlmacenDestino
    else
    begin
        IndiceVehiculo := FCuentasConvenio.BuscarIndice(cdsEliminarTelevia.FieldByName('Patente').AsString);
        AlmacenDestino := FCuentasConvenio.Vehiculos[IndiceVehiculo].Cuenta.CodigoAlmacenDestino
    end;
    //TASK_016_ECA_20160526

    //if f.Inicializar(CONST_OPERACION_TAG_ELIMINAR, FCuentasConvenio.Vehiculos[IndiceVehiculo].Cuenta.CodigoAlmacenDestino, '') and (f.ShowModal = mrOk) then begin //TASK_016_ECA_20160526
    if f.Inicializar(CONST_OPERACION_TAG_ELIMINAR, AlmacenDestino, '') and (f.ShowModal = mrOk) then begin                                                           //TASK_016_ECA_20160526
      // Cargamos el dataset con lo que se selecciono
        f.ListaConceptos.First;
        while not f.ListaConceptos.Eof do begin
            if f.ListaConceptos.FieldByName('Seleccionado').AsBoolean then begin
              //Agregamos el concepto a la lista
                cdConceptos.AppendRecord([
                    cdsEliminarTelevia.FieldByName('Patente').AsString,
                      f.ListaConceptos.FieldByName('Codigoconcepto'   ).Value,
                      f.ListaConceptos.FieldByName('DescripcionMotivo').Value,
                      f.ListaConceptos.FieldByName('PrecioOrigen'     ).Value,
                      f.ListaConceptos.FieldByName('Moneda'           ).Value,
                      f.ListaConceptos.FieldByName('Cotizacion'       ).Value,
                      f.ListaConceptos.FieldByName('PrecioEnPesos'    ).Value,
                      f.ListaConceptos.FieldByName('Comentario'       ).Value ]);
                FImporteTotalMovimientos := FImporteTotalMovimientos + f.ListaConceptos.FieldByName('PrecioEnPesos').AsVariant;
            end;
            f.ListaConceptos.Next;
        end;
        f.Release;
    end;
    lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);
end;

procedure TFormEliminarTag.AgregarConceptos1Click(Sender: TObject);
begin
    lblSeleccionarClick(Sender);
end;

procedure TFormEliminarTag.mnuEliminarConceptoClick(Sender: TObject);
begin
    if not cdConceptos.eof then begin
        FImporteTotalMovimientos := FImporteTotalMovimientos - cdConceptos.FieldByName('PrecioEnPesos').AsVariant ;
        lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);
        cdConceptos.Delete;
    end;
end;

procedure TFormEliminarTag.btnAceptarClick(Sender: TObject);
ResourceString
    MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION = 'Se debe ingresar al menos un motivo de indemnizaci�n';
    MSG_ERROR_MOTIVO_BAJA ='Debe seleccionar un motivo de baja';
    MSG_ERROR_ESTADOTAG_MOTIVO_BAJA='Valide el motivo de baja para el estado del televia: ';
var
    Patente: string;
begin
    cdsEliminarTelevia.DisableControls;
    cdConceptos.Filtered := False;
    FCantidadTagACobrar := 0;
    cdsEliminarTelevia.First;
    cdConceptos.First;
    //Primero validamos
    while not cdsEliminarTelevia.Eof do begin
        if not ValidarEtiqueta('', '', cdsEliminarTelevia.FieldByName('Televia').AsString , False) then begin
             MsgBox(MSG_ERROR_TELEVIA,STR_ERROR,MB_ICONSTOP);
             dbgEliminarTags.SetFocus;
             cdsEliminarTelevia.EnableControls;
             dbgEliminarTags.Fields[1].FocusControl;
             Exit;
        end;

        if (Trim(cdsEliminarTelevia.FieldByName('Televia').AsString) = '') then begin
        // se elimino esta validacion             and (EstadoConservacion <> ESTADO_CONSERVACION_NO_DEVUELTO)
            dbgEliminarTags.SetFocus;
            cdsEliminarTelevia.EnableControls;
            MsgBox(MSG_ERROR_INGRESE_TELEVIA, STR_ERROR, MB_ICONSTOP);
            dbgEliminarTags.Fields[1].FocusControl;
            Exit;
        end;

        //INICIO: TASK_011_ECA_20160514
        if FCuentasConvenio <> nil then begin
            if (trim(cdsEliminarTelevia.FieldByName('Televia').AsString) <> '') and
               (trim(cdsEliminarTelevia.FieldByName('Patente').AsString) <> '') and
               (FCuentasConvenio.BuscarIndice(trim(cdsEliminarTelevia.FieldByName('Patente').AsString)) <> FCuentasConvenio.BuscarIndice(EtiquetaToSerialNumber(PadL(trim(cdsEliminarTelevia.FieldByName('Televia').AsString),11,'0')))) then begin
                dbgEliminarTags.SetFocus;
                cdsEliminarTelevia.EnableControls;
                MsgBox(MSG_ERROR_TELEVIA_VEHICULO,STR_ERROR,MB_ICONSTOP);
                Exit;
            end;
        end else if (trim(cdsEliminarTelevia.FieldByName('Televia').AsString) <> '') and
          (EtiquetaToSerialNumber(PadL(trim(cdsEliminarTelevia.FieldByName('Televia').AsString), 11, '0')) <> FCuentaVehiculo.Cuenta.ContactSerialNumber) then begin
                dbgEliminarTags.SetFocus;
                cdsEliminarTelevia.EnableControls;
                MsgBox(MSG_ERROR_TELEVIA_VEHICULO,STR_ERROR,MB_ICONSTOP);
                Exit;
            end;

        if (Trim(cdsEliminarTelevia.FieldByName('EstadoTag').AsString) = '') or
            (Trim(cdsEliminarTelevia.FieldByName('EstadoTag').AsString) = SIN_ESPECIFICAR) then begin
            dbgEliminarTags.SetFocus;
            cdsEliminarTelevia.EnableControls;
            MsgBox(MSG_ERROR_ESTADO_TELEVIA,STR_ERROR,MB_ICONSTOP);
            dbgEliminarTags.Fields[2].FocusControl;
            Exit;
        end;

         if (Trim(cdsEliminarTelevia.FieldByName('Motivo').AsString) = '') or
            (Trim(cdsEliminarTelevia.FieldByName('Motivo').AsString) = SIN_ESPECIFICAR) then begin
            dbgEliminarTags.SetFocus;
            cdsEliminarTelevia.EnableControls;
            MsgBox(MSG_ERROR_MOTIVO_BAJA, STR_ERROR, MB_ICONSTOP);
            dbgEliminarTags.Fields[3].FocusControl;
            Exit;
        end;

        if NOT ValidarEstadoTAGvsMotivoBaja(cdsEliminarTelevia.FieldByName('EstadoTag').AsString,cdsEliminarTelevia.FieldByName('Motivo').AsString) then
        begin
            dbgEliminarTags.SetFocus;
            cdsEliminarTelevia.EnableControls;
            MsgBox(MSG_ERROR_ESTADOTAG_MOTIVO_BAJA + cdsEliminarTelevia.FieldByName('EstadoTag').AsString, STR_ERROR, MB_ICONSTOP);
            cdsEliminarTelevia.Edit;
            cdsEliminarTelevia.FieldByName('Motivo').AsString:=SIN_ESPECIFICAR;
            cdsEliminarTelevia.post;
            dbgEliminarTags.Fields[3].FocusControl;
            Exit;
        end;

        cdsEliminarTelevia.Edit;
        if Ansipos('ROBO',UpperCase(cdsEliminarTelevia.FieldByName('Motivo').AsString))<>0 then
           cdsEliminarTelevia.FieldByName('Robado').Value:= True;

        cdsEliminarTelevia.FieldByName('CodigoMotivo').AsInteger := Integer(dbgEliminarTags.Columns[3].PickList.Objects[dbgEliminarTags.Columns[3].PickList.IndexOf(cdsEliminarTelevia.FieldByName('Motivo').AsString)]);
{INICIO: TASK_011_JMA_20160505}
        cdsEliminarTelevia.FieldByName('DescripcionMotivo').Value := cdsEliminarTelevia.FieldByName('Motivo').AsString;
        cdsEliminarTelevia.FieldByName('DescripcionEstadoTAG').Value := cdsEliminarTelevia.FieldByName('EstadoTag').AsString;
{TERMINO: TASK_011_JMA_20160505}
        cdsEliminarTelevia.post;

        //verfico que cada patente tenga al menos un Motivo de indemnizacion cargado
        cdConceptos.Filtered := False;
        Patente := trim(cdsEliminarTelevia.FieldByName('Patente').AsString) ;
        cdConceptos.Filter := 'Patente =' + QuotedStr(Patente) ;
        cdConceptos.Filtered := True;
        if cdConceptos.IsEmpty then begin
             MsgBoxBalloon(MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION,'Error en Patente '+ Patente ,MB_ICONSTOP, dblConceptos);
             dblConceptos.SetFocus;
             Exit;
        end;

        cdsEliminarTelevia.Next;
        //FIN: TASK_011_ECA_20160514
    end;

    cdsEliminarTelevia.EnableControls;
    ModalResult := mrOK;
end;

procedure TFormEliminarTag.btnSalirClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TFormEliminarTag.dbgEliminarTagsExit(Sender: TObject);
begin
    if not btnSalir.Focused and (cdsEliminarTelevia.State in [dsEdit, dsInsert]) then cdsEliminarTelevia.Post;
end;

procedure TFormEliminarTag.dbgEliminarTagsKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (ssCtrl in Shift) and (key = VK_DELETE) then key := 0;
    if (key = VK_INSERT) then key := 0;
    if (((Sender as TDBGrid).SelectedIndex = ColumnaEstadoTAG) or ((Sender as TDBGrid).SelectedIndex = ColumnaMotivoTAG)) then Key := 0;
//    if ((Sender as TDBGrid).SelectedIndex = ColumnaMotivo) then Key := 0;
end;

procedure TFormEliminarTag.cdsEliminarTeleviaBeforePost(DataSet: TDataSet);
begin
    dbgEliminarTags.Options := dbgEliminarTags.Options - [dgEditing];
end;

procedure TFormEliminarTag.cdsEliminarTeleviaAfterInsert(
  DataSet: TDataSet);
begin
    if (not EstaIniciando) and (Trim(DataSet.FieldByName('Patente').AsString) = '') then begin
        DataSet.Delete;
        btnAceptar.SetFocus;
    end;
end;


procedure TFormEliminarTag.dbgEliminarTagsKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (((Sender as TDBGrid).SelectedIndex = ColumnaEstadoTAG) or ((Sender as TDBGrid).SelectedIndex = ColumnaMotivoTAG))then begin
        Key := #0;
    end
    else begin
        if not (Key  in ['0'..'9', #8]) then
            Key := #0;
    end;
end;

procedure TFormEliminarTag.dblConceptosContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
    mnuEliminarConcepto.Enabled := not cdConceptos.IsEmpty;
end;

procedure TFormEliminarTag.dblConceptosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = VK_DELETE) or (Key = VK_BACK) then mnuEliminarConceptoClick(Sender);
end;

procedure TFormEliminarTag.FormShow(Sender: TObject);
begin
    cdsEliminarTelevia.First;
    dbgEliminarTags.Fields[1].FocusControl;
end;

procedure TFormEliminarTag.dbgEliminarTagsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var bmp: TBitMap;
begin
//INICIO: TASK_011_ECA_20160514
    with (Sender as TDBGrid) do begin
        if (DataCol in [ColumnaRobado]) then begin
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            if (Columns[DataCol].Field.asBoolean) then
          	    Imagenes.GetBitmap(0, Bmp)
            else
                Imagenes.GetBitmap(1, Bmp);
            canvas.Draw(rect.Left, rect.Top,bmp);
            bmp.Free;
        end;
    end;
//FIN: TASK_011_ECA_20160514
end;

procedure TFormEliminarTag.dbgEliminarTagsDblClick(Sender: TObject);
begin
    if (Sender as TDBGrid).SelectedIndex = ColumnaRobado then begin
        cdsEliminarTelevia.Edit;
        cdsEliminarTelevia.FieldByName('Robado').AsBoolean := not cdsEliminarTelevia.FieldByName('Robado').AsBoolean;
        cdsEliminarTelevia.post;
    end;
end;

procedure TFormEliminarTag.dbgEliminarTagsCellClick(Column: TColumn);
begin
    if Column.PickList.Count > 0 then
  begin
    keybd_event(VK_F2,0,0,0);
    keybd_event(VK_F2,0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_MENU,0,0,0);
    keybd_event(VK_DOWN,0,0,0);
    keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_MENU,0,KEYEVENTF_KEYUP,0);
  end;
end;

{*******************************************************************************
Revision: 1
Author: nefernandez
Date: 09/04/2008
Description: SS 681: Se quita la verificaci�n de si el Tag esta vendido o
perdido para habilitar la seleccion de las opciones del estado de devolucion
*******************************************************************************}
procedure TFormEliminarTag.dbgEliminarTagsColEnter(Sender: TObject);
begin
    if (((Sender as TDBGrid).SelectedIndex = ColumnaTelevia)
            or ((Sender as TDBGrid).SelectedIndex = ColumnaEstadoTAG) or ((Sender as TDBGrid).SelectedIndex = ColumnaMotivoTAG))
            //and not (EsTagVendido)  // Revision 1
            //and not (EsTagPerdido) then  // Revision 1
            then
        (Sender as TDBGrid).Options := (Sender as TDBGrid).Options + [dgEditing]
    else
        (Sender as TDBGrid).Options := (Sender as TDBGrid).Options - [dgEditing];
end;



function TFormEliminarTag.GetEstadoConservacion: integer;
begin
    Result := 0;
    if (cdsEliminarTelevia.Active) and (cdsEliminarTelevia.RecordCount > 0) then begin
        ObtenerEstadosConservacionTAGs.First;
        while (not ObtenerEstadosConservacionTAGs.Eof) do begin
            if trim(ObtenerEstadosConservacionTAGs.FieldByName('Descripcion').AsString) = Trim(cdsEliminarTelevia.FieldByName('EstadoTag').AsString) then
                result := ObtenerEstadosConservacionTAGs.FieldByName('CodigoEstadoConservacionTAG').AsInteger;

            if Trim(cdsEliminarTelevia.FieldByName('EstadoTag').AsString) = SIN_ESPECIFICAR then
                Result := 0;

            ObtenerEstadosConservacionTAGs.Next;
        end;
    end;
end;

procedure TFormEliminarTag.dbgEliminarTagsColExit(Sender: TObject);
begin
    if not EstaIniciando and ((Sender as TDBGrid).SelectedIndex = ColumnaTelevia) and not ValidarEtiqueta('', '', cdsEliminarTelevia.FieldByName('Televia').AsString , False) then begin
         MsgBox(MSG_ERROR_TELEVIA, Caption, MB_ICONSTOP);
    end;

end;

function TFormEliminarTag.GetRobado: Boolean;
begin
    Result := False;
    if (cdsEliminarTelevia.Active) and (cdsEliminarTelevia.RecordCount > 0) then
        result := cdsEliminarTelevia.FieldByName('Robado').AsBoolean;
end;


procedure TFormEliminarTag.cdsEliminarTeleviaAfterScroll(
  DataSet: TDataSet);
begin
    // Llamo al entrar a columna...
    dbgEliminarTagsColEnter(dbgEliminarTAGS);
    lbl_Patente.Caption := DataSet.FieldByName('Patente').AsString;
    //cargar la lista de cargos
    if not EstaIniciando then
        FiltrarGrillaBoxMotivosPorPatente(DataSet.FieldByName('Patente').AsString);

end;
procedure TFormEliminarTag.cdsEliminarTeleviaBeforeScroll(
  DataSet: TDataSet);
begin
    // Llamo al salir de columna...
    dbgEliminarTagsColExit(dbgEliminarTAGS);
end;

function TFormEliminarTag.EsTagVendido: Boolean;
begin
    Result := cdsEliminarTelevia.FieldByName('TagVendido').AsBoolean;
end;

function TFormEliminarTag.EsTagPerdido: Boolean;
begin
    Result := cdsEliminarTelevia.FieldByName('TagPerdido').AsBoolean;
end;


procedure TFormEliminarTag.FiltrarGrillaBoxMotivosPorPatente(Patente: string);
begin
    cdConceptos.DisableControls;
    cdConceptos.Filtered := False;
    cdConceptos.Filter := 'Patente =' + QuotedStr(Patente) ;
    cdConceptos.Filtered := True;
    cdConceptos.EnableControls;
    cdConceptos.First;
end;
//INICIO: TASK_011_ECA_20160514

function TFormEliminarTag.GetCodigoMotivoBaja: Integer;
begin
    Result := cdsEliminarTelevia.FieldByName('CodigoMotivo').AsInteger;
end;
{NICIO: TASK_011_JMA_201605}
function TFormEliminarTag.GetDescripcionMotivoBaja: string;
begin
    Result := cdsEliminarTelevia.FieldByName('DescripcionMotivo').AsString;
end;
function TFormEliminarTag.GetDescripcionEstadoTAG: string;
begin
    Result := cdsEliminarTelevia.FieldByName('DescripcionEstadoTAG').AsString;
end;
{TERMINO: TASK_011_JMA_201605}
function TFormEliminarTag.CargarCuentasMotivosBaja: TADOStoredProc;
begin
    Result:= TADOStoredProc.Create(nil);
    Result.Connection:=DMConnections.BaseCAC;
    Result.ProcedureName := 'CRM_CuentasMotivosBaja_SELECT';
    Result.Parameters.Refresh;
    Result.Open;
end;

function TFormEliminarTag.ValidarEstadoTAGvsMotivoBaja(EstadoTAG, Motivo: string):Boolean;
var
  Pos: Integer;
begin
   Result:=True;
//INICIO: TASK_016_ECA_20160526
//   Pos:= Ansipos('ROBO',UpperCase(Motivo));
//   if (UpperCase(EstadoTAG)<>'NO DEVUELTO') then
//   begin
//      if Pos<>0 then
//           Result:=False;
//   end
//   else
//   begin
//        if Pos=0 then
//           Result:=False;
//   end;
//FIN: TASK_016_ECA_20160526
end;
//FIN: TASK_011_ECA_20160514

end.
