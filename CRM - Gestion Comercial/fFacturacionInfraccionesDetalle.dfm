object FormFacturacionInfraccionesDetalle: TFormFacturacionInfraccionesDetalle
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Valorizacion de Infracciones'
  ClientHeight = 456
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 633
    Height = 77
    Align = alTop
    Caption = 'Domicilio del Comprobante '
    TabOrder = 0
    object lblNombre: TLabel
      Left = 8
      Top = 20
      Width = 44
      Height = 13
      Caption = 'Domicilio:'
    end
    object lblApellido: TLabel
      Left = 8
      Top = 53
      Width = 37
      Height = 13
      Caption = 'Regi'#243'n:'
    end
    object Label2: TLabel
      Left = 8
      Top = 36
      Width = 43
      Height = 13
      Caption = 'Comuna:'
    end
    object lblDatoDomicilioFact: TLabel
      Left = 80
      Top = 20
      Width = 73
      Height = 13
      Caption = 'lblDatoDomicilio'
    end
    object lblDatoComunaFact: TLabel
      Left = 80
      Top = 36
      Width = 72
      Height = 13
      Caption = 'lblDatoComuna'
    end
    object lblDatoRegionFact: TLabel
      Left = 80
      Top = 53
      Width = 66
      Height = 13
      Caption = 'lblDatoRegion'
    end
    object Label3: TLabel
      Left = 431
      Top = 20
      Width = 24
      Height = 13
      Caption = 'RUT:'
    end
    object Label5: TLabel
      Left = 431
      Top = 36
      Width = 49
      Height = 13
      Caption = 'Convenio:'
    end
    object lblDatoRUT: TLabel
      Left = 493
      Top = 20
      Width = 53
      Height = 13
      Caption = 'lblDatoRUT'
    end
    object lblDatoConvenio: TLabel
      Left = 493
      Top = 36
      Width = 78
      Height = 13
      Caption = 'lblDatoConvenio'
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 77
    Width = 633
    Height = 111
    Align = alTop
    Caption = 'Datos del Cliente'
    TabOrder = 1
    object lblDomicilio: TLabel
      Left = 10
      Top = 20
      Width = 41
      Height = 13
      Caption = 'Apellido:'
    end
    object Label4: TLabel
      Left = 10
      Top = 41
      Width = 44
      Height = 13
      Caption = 'Domicilio:'
    end
    object Label6: TLabel
      Left = 10
      Top = 84
      Width = 37
      Height = 13
      Caption = 'Regi'#243'n:'
    end
    object Label7: TLabel
      Left = 10
      Top = 62
      Width = 43
      Height = 13
      Caption = 'Comuna:'
    end
    object lblDatoApellidoNombre: TLabel
      Left = 80
      Top = 20
      Width = 107
      Height = 13
      Caption = 'lblDatoApellidoNombre'
    end
    object lblDatoDomicilio: TLabel
      Left = 80
      Top = 41
      Width = 73
      Height = 13
      Caption = 'lblDatoDomicilio'
    end
    object lblDatoComuna: TLabel
      Left = 80
      Top = 62
      Width = 72
      Height = 13
      Caption = 'lblDatoComuna'
    end
    object lblDatoRegion: TLabel
      Left = 80
      Top = 84
      Width = 66
      Height = 13
      Caption = 'lblDatoRegion'
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 188
    Width = 633
    Height = 222
    Align = alClient
    Caption = ' Datos del Comprobante '
    TabOrder = 2
    object lblInfracciones: TLabel
      Left = 14
      Top = 20
      Width = 376
      Height = 13
      Alignment = taRightJustify
      Caption = 
        'Se han seleccionado N Infracciones a facturar de un importe por ' +
        'infracci'#243'n de:'
    end
    object lblTotalInfracciones: TLabel
      Left = 98
      Top = 46
      Width = 292
      Height = 13
      Alignment = taRightJustify
      Caption = 'Importe total en concepto de Peaje por Boleto de Infracci'#243'n:'
      Visible = False
    end
    object lblGastosAdm: TLabel
      Left = 144
      Top = 76
      Width = 190
      Height = 29
      Alignment = taRightJustify
      AutoSize = False
      Caption = 
        'Importe Total en Concepto de Gastos Administrativos de Cobranzas' +
        ':'
      WordWrap = True
    end
    object lblPorcentaje: TLabel
      Left = 369
      Top = 65
      Width = 11
      Height = 13
      Alignment = taRightJustify
      Caption = '%'
    end
    object lblTotalComprobante: TLabel
      Left = 185
      Top = 153
      Width = 207
      Height = 13
      Alignment = taRightJustify
      Caption = 'Importe Total del Comprobante a generar: '
    end
    object lblFechaEmision: TLabel
      Left = 47
      Top = 192
      Width = 86
      Height = 13
      Caption = 'Fecha de Emisi'#243'n:'
    end
    object lblFechaVencimiento: TLabel
      Left = 281
      Top = 192
      Width = 108
      Height = 13
      Caption = 'Fecha de Vencimiento:'
    end
    object lblIMporteTotalInfraccion: TLabel
      Left = 416
      Top = 46
      Width = 105
      Height = 13
      AutoSize = False
      Caption = '$ 2500'
      Visible = False
    end
    object lblImporteTotal: TLabel
      Left = 416
      Top = 153
      Width = 105
      Height = 13
      AutoSize = False
      Caption = '$ 2500'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Bevel1: TBevel
      Left = 8
      Top = 144
      Width = 601
      Height = 9
      Shape = bsTopLine
    end
    object Bevel2: TBevel
      Left = 8
      Top = 175
      Width = 601
      Height = 9
      Shape = bsTopLine
    end
    object Label1: TLabel
      Left = 224
      Top = 119
      Width = 166
      Height = 13
      Alignment = taRightJustify
      Caption = 'Importe en concepto de intereses:'
    end
    object nePrecioInfraccion: TNumericEdit
      Left = 416
      Top = 16
      Width = 105
      Height = 21
      Enabled = False
      MaxLength = 7
      TabOrder = 0
    end
    object nePorcentaje: TNumericEdit
      Left = 347
      Top = 80
      Width = 54
      Height = 21
      TabOrder = 1
      OnChange = nePorcentajeChange
      Decimals = 2
    end
    object deFechaEmision: TDateEdit
      Left = 144
      Top = 188
      Width = 90
      Height = 21
      AutoSelect = False
      ReadOnly = True
      TabOrder = 4
      OnChange = deFechaEmisionChange
      Date = -693594.000000000000000000
    end
    object deFechaVencimiento: TDateEdit
      Left = 400
      Top = 188
      Width = 90
      Height = 21
      AutoSelect = False
      ReadOnly = True
      TabOrder = 5
      Date = -693594.000000000000000000
    end
    object neImporteIntereses: TNumericEdit
      Left = 416
      Top = 115
      Width = 105
      Height = 21
      TabOrder = 3
      OnChange = neImporteInteresesChange
    end
    object neImporteTotalGastosAdministrativos: TNumericEdit
      Left = 416
      Top = 80
      Width = 105
      Height = 21
      TabOrder = 2
      OnChange = neImporteTotalGastosAdministrativosChange
    end
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 410
    Width = 633
    Height = 46
    Align = alBottom
    TabOrder = 3
    object btnFacturar: TButton
      Left = 369
      Top = 13
      Width = 121
      Height = 25
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btnFacturarClick
    end
    object BtnSalir: TButton
      Left = 514
      Top = 13
      Width = 105
      Height = 25
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
      OnClick = BtnSalirClick
    end
  end
end
