unit frmResultadoSuscripcionCuentas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, StdCtrls, ListBoxEx, DBListEx, ExtCtrls, DB, PeaProcs,
  DPSControls, UtilProc,DmConnection,Peatypes,
  ADODB, DmiCtrls, Validate, DateEdit,util,UtilDB,RStrings,
  VariantComboBox, frmVisorDocumentos, Convenios, frmImprimirConvenio,
  ConstParametrosGenerales, DBClient, Math, Provider, PeaProcsCN, SysUtilsCN;

type
  TformResultadoSuscripcionCuentas = class(TForm)
    pnlGridsPanel: TPanel;
    dbl_Cuentas: TDBListEx;
    Panel1: TPanel;
    lblCuentas: TLabel;
    pnlBottomPanel: TPanel;
    BtnSalir: TButton;
    dsObtenerCuentas: TDataSource;
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(var cdsCuentasPersona:TClientDataSet): Boolean;
  end;

var
  formResultadoSuscripcionCuentas: TformResultadoSuscripcionCuentas;

implementation

{$R *.dfm}

procedure TformResultadoSuscripcionCuentas.BtnSalirClick(Sender: TObject);
begin
   close;
end;

function TformResultadoSuscripcionCuentas.Inicializar(var cdsCuentasPersona:TClientDataSet): Boolean;
begin
    try
        dsObtenerCuentas.DataSet := cdsCuentasPersona;
        Result := True;
    except
        on e: Exception do begin
    		Result := false;
        end;
    end;
end;

end.
