object NavWindowDatosReclamos: TNavWindowDatosReclamos
  Left = 177
  Top = 165
  Width = 783
  Height = 504
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  Caption = 'Ordenes de Servicio'
  Color = clBtnFace
  Constraints.MinHeight = 504
  Constraints.MinWidth = 750
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 249
    Width = 775
    Height = 8
    Cursor = crVSplit
    Align = alTop
  end
  object Display: TWorkflowDisplay
    Left = 0
    Top = 257
    Width = 775
    Height = 220
    Align = alClient
    WorkflowID = 4
    ServiceOrder = 21
    WorkflowVersion = 0
  end
  object Grilla: TDBListEx
    Left = 0
    Top = 0
    Width = 775
    Height = 249
    Align = alTop
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        Width = 50
        Header.Caption = 'Cliente'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'VerCliente'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        Width = 82
        Header.Caption = 'Orden Servicio'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'CodigoOrdenServicio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 201
        Header.Caption = 'Descripci'#243'n'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Descripcion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 63
        Header.Caption = 'Estado'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'EstadoOrden'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 87
        Header.Caption = 'Fecha creaci'#243'n'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaHoraCreacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 97
        Header.Caption = 'Fecha finalizaci'#243'n'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaHoraFin'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 57
        Header.Caption = 'Prioridad'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescripcionPrioridad'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 133
        Header.Caption = 'Compromiso'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaCompromiso'
      end>
    DataSource = ds_qry_OS
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnDblClick = GrillaDblClick
    OnLinkClick = GrillaLinkClick
  end
  object ds_qry_OS: TDataSource
    DataSet = qry_OSTodas
    OnDataChange = ds_qry_OSDataChange
    Left = 120
    Top = 376
  end
  object qry_OSTodas: TADOQuery
    Parameters = <
      item
        Name = 'CLIENTE'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'TipoCliente'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'Estado1'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Estado2'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'select'
      #9#39'Ver ...'#39'  As VerCliente,'
      #9'Comunicaciones.CodigoContacto AS CodigoCliente,'
      #9'OrdenesServicio.CodigoOrdenServicio,'
      #9'TiposOrdenServicio.Descripcion,'
      #9'OrdenesServicio.Estado,'
      #9'OrdenesServicio.FechaHoraCreacion,'
      #9'OrdenesServicio.FechaHoraFin,'
      #9'OrdenesServicio.Prioridad,'
      #9'OrdenesServicio.FechaCompromiso,'
      #9'OrdenesServicioWorkFlows.CodigoWorkflow,'
      #9'OrdenesServicioWorkFlows.Version,'
      
        '    dbo.PrioridadToString(OrdenesServicio.Prioridad) AS Descripc' +
        'ionPrioridad,'
      '    dbo.EstadoOSToString(OrdenesServicio.Estado) AS EstadoOrden'
      'From'
      #9'Comunicaciones,'
      #9'OrdenesServicio,'
      #9'TiposOrdenServicio,'
      #9'OrdenesServicioWorkflows,'
      #9'WorkFlows'
      'Where'
      #9'Comunicaciones.CodigoContacto = :CLIENTE and'
      #9'Comunicaciones.TipoContacto'#9'= :TipoContacto and'
      
        #9'OrdenesServicio.CodigoComunicacion = Comunicaciones.CodigoComun' +
        'icacion and'
      
        '    ((:Estado1 Is null) or (Rtrim(OrdenesServicio.Estado) = RTri' +
        'm(:Estado2))) and    '
      
        #9'OrdenesServicio.TipoOrdenServicio = TiposOrdenServicio.TipoOrde' +
        'nServicio  and'
      
        #9'OrdenesServicio.CodigoOrdenServicio = OrdenesServicioWorkflows.' +
        'CodigoOrdenServicio and'
      
        #9'OrdenesServicioWorkflows.CodigoWorkFlow = WorkFlows.CodigoWorkf' +
        'low and'
      #9'OrdenesServicioWorkflows.Version = WorkFlows.VersionActual'
      'Order by OrdenesServicio.FechaHoraCreacion')
    Left = 160
    Top = 376
  end
end
