object FrmClientesAContactar: TFrmClientesAContactar
  Left = 0
  Top = 0
  Caption = 'Clientes A Contactar'
  ClientHeight = 352
  ClientWidth = 744
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 744
    Height = 41
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 0
    object abmtlbr1: TAbmToolbar
      Left = 1
      Top = 1
      Width = 742
      Height = 35
      Habilitados = [btAlta, btBaja, btSalir, btBuscar]
      OnClose = abmtlbr1Close
    end
    object chkOcultarEliminados: TCheckBox
      Left = 577
      Top = 13
      Width = 147
      Height = 17
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Ocultar/ Mostrar Eliminados'
      TabOrder = 1
      OnClick = chkOcultarEliminadosClick
    end
  end
  object dblClientes: TAbmList
    Left = 0
    Top = 41
    Width = 744
    Height = 206
    TabStop = True
    TabOrder = 1
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'50'#0'Id           '
      #0'109'#0'Numero Documento  '
      #0'119'#0'Fecha Alta                    '
      #0'122'#0'Fecha Baja                    '
      #0'119'#0'Usuario Creaci'#243'n          '
      #0'119'#0'Fecha Creaci'#243'n            '
      #0'125'#0'Usuario Modificaci'#243'n      '
      #0'125'#0'Fecha Modificaci'#243'n        ')
    HScrollBar = True
    Table = tblClientesAContactar
    Style = lbOwnerDrawVariable
    OnClick = dblClientesClick
    OnDrawItem = dblClientesDrawItem
    OnInsert = dblClientesInsert
    OnDelete = dblClientesDelete
    Access = [accAlta, accBaja]
    Estado = Normal
    ToolBar = abmtlbr1
  end
  object pnl2: TPanel
    Left = 0
    Top = 247
    Width = 744
    Height = 105
    Align = alBottom
    TabOrder = 2
    object lblRut: TLabel
      Left = 45
      Top = 24
      Width = 26
      Height = 13
      Caption = 'RUT:'
      Color = 16776699
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 269
      Top = 23
      Width = 380
      Height = 13
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object peNumeroDocumento: TPickEdit
      Left = 77
      Top = 20
      Width = 159
      Height = 21
      Enabled = False
      TabOrder = 0
      OnChange = peNumeroDocumentoChange
      OnKeyPress = peNumeroDocumentoKeyPress
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object Notebook: TNotebook
      Left = 534
      Top = 58
      Width = 197
      Height = 37
      PageIndex = 1
      TabOrder = 1
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btnBtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btnBtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object btnBtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          TabOrder = 0
          OnClick = btnBtnAceptarClick
        end
        object btnBtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = btnBtnCancelarClick
        end
      end
    end
  end
  object tblClientesAContactar: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'ClientesAContactar'
    Left = 504
    Top = 128
  end
  object spObtenerDatosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 504
    Top = 160
  end
end
