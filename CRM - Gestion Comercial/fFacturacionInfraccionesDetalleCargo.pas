{----------------------------------------------------------------------------------------
 File Name: fFacturacionInfraccionesDetalleCargo.pas
 Author:  lgisuk
 Date Created: 15/12/2005
 Language: ES-AR
 Description: Permite realizar cargos al cliente por infractor en la proxima facturaci�n
-----------------------------------------------------------------------------------------}
unit fFacturacionInfraccionesDetalleCargo;

interface

uses
  //Detalle de los cargos
  DMConnection,
  util,
  UtilDB,
  UtilProc,
  peaProcs,
  PeaTypes,
  ConstParametrosGenerales,
  //Otros
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, Validate, DateEdit, ExtCtrls;

type
  TFormFacturacionInfraccionesDetalleCargo = class(TForm)
	btnFacturar: TButton;
	BtnSalir: TButton;
	GroupBox3: TGroupBox;
	lblInfracciones: TLabel;
	lblTotalInfracciones: TLabel;
	lblGastosAdm: TLabel;
	lblPorcentaje: TLabel;
	lblTotalComprobante: TLabel;
	nePrecioInfraccion: TNumericEdit;
	nePorcentaje: TNumericEdit;
	deFechaEmision: TDateEdit;
	lblFechaEmision: TLabel;
	lblIMporteTotalInfraccion: TLabel;
	lblImporteTotal: TLabel;
	Bevel1: TBevel;
	Bevel2: TBevel;
	neImporteIntereses: TNumericEdit;
	Label1: TLabel;
	neImporteTotalGastosAdministrativos: TNumericEdit;
	procedure neImporteTotalGastosAdministrativosChange(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure btnFacturarClick(Sender: TObject);
	procedure neImporteInteresesChange(Sender: TObject);
	procedure nePorcentajeChange(Sender: TObject);
	procedure nePrecioInfraccionChange(Sender: TObject);
  private
	{ Private declarations }
  public
	{ Public declarations }
	FCantidadInfracciones: Integer;
	FImportePorInfraccion: extended;
	FImporteTotalPorInfraccion: extended;
	FImporteTotalGastosAdministrativos: Int64;
	function Inicializar(aImportePorInfraccion, aImporteGastosAdministrativos, aImporteIntereses: extended; aFechaEmision: TDateTime; aCantidadInfracciones: Integer): boolean;
  end;

  function ObtenerDatosCargoInfraccion(var ImportePorInfraccion, ImporteGastosAdministrativos, ImporteIntereses: extended; var fechaEmision: TDateTime; CantidadInfracciones: Integer): Boolean;


implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  lgisuk
  Date Created: 15/12/2005
  Description:  Inicializaci�n de Este Formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormFacturacionInfraccionesDetalleCargo.Inicializar(aImportePorInfraccion, aImporteGastosAdministrativos, aImporteIntereses: extended; aFechaEmision: TDateTime; aCantidadInfracciones: Integer): boolean;
resourcestring
	MSG_ERROR_LOAD_PARAMETERS_PERCENT = 'Error cargando par�metro de porcentaje de gastos de cobranzas.' ;
var
	PorcentajeGastosCobranzas: integer;
begin
	result := false;
	if not ObtenerParametroGeneral(DMCOnnections.BaseCAC, PORCENTAJE_GASTOS_COBRANZAS_BI, PorcentajeGastosCobranzas) then begin
		MsgBoxErr(MSG_ERROR_LOAD_PARAMETERS_PERCENT, '', Caption, MB_ICONSTOP);
		exit;
	end;
	// Cargamos los valores del comprobante
	FCantidadInfracciones 		:= aCantidadInfracciones;
	nePrecioInfraccion.Value 	:= aImportePorInfraccion / 100;
	nePorcentaje.Value 			:= PorcentajeGastosCobranzas;
	FImportePorInfraccion 		:= aImportePorInfraccion / 100;
	FImporteTotalPorInfraccion 	:=  FImportePorInfraccion * FCantidadInfracciones;
	FImporteTotalGastosAdministrativos := trunc((FImporteTotalPorInfraccion * nePorcentaje.Value) / 100);
	// Mostramos los valores en los labels
	lblInfracciones.Caption 		    := format('Se han seleccionado (%d) Infracciones a facturar, de un importe unitario de :', [FCantidadInfracciones]);
	lblImporteTotalInfraccion.Caption   := formatFloat(FORMATO_IMPORTE, FImporteTotalPorInfraccion);
	neImporteTotalGastosAdministrativos.ValueInt := FImporteTotalGastosAdministrativos;
	lblImporteTotal.Caption             := formatFloat(FORMATO_IMPORTE, FImporteTotalPorInfraccion + FImporteTotalGastosAdministrativos);
	// Inicializamos los valores de fecha
	deFechaEmision.Date := NowBase(DMConnections.BaseCAC);
	result := true;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerDatosCargoInfraccion
  Author:  lgisuk
  Date Created: 15/12/2005
  Description:  obtengo el detalle del cargo
  Parameters: None
  Return Value: Boolean

Revision 1:
    Author : ggomez
    Date : 24/05/2006
    Description :
        - Cambi� el uso de Free por Release.
        - Agregu� un try finally para liberar el recurso si es que ocurre un
        error.
-----------------------------------------------------------------------------}
function ObtenerDatosCargoInfraccion(var ImportePorInfraccion, ImporteGastosAdministrativos, ImporteIntereses: extended; var fechaEmision: TDateTime; CantidadInfracciones: Integer): Boolean;
var
	F: TFormFacturacionInfraccionesDetalleCargo;
begin
	Application.CreateForm(TFormFacturacionInfraccionesDetalleCargo, f);
    try
        if f.Inicializar(ImportePorInfraccion, ImporteGastosAdministrativos, ImporteIntereses, FechaEmision, CantidadInfracciones) then f.showmodal;
        if f.ModalResult = mrOk then begin
            ImportePorInfraccion := f.nePrecioInfraccion.Value;
            ImporteGastosAdministrativos := f.FImporteTotalGastosAdministrativos;
            ImporteIntereses := f.neImporteIntereses.Value;
            FechaEmision := f.deFechaEmision.date;
            Result := true;
        end else begin
            Result := False;
        end;
    finally
//    f.Free;
        f.Release;
    end; // finally
end;

{-----------------------------------------------------------------------------
  Function Name: nePrecioInfraccionChange
  Author:  lgisuk
  Date Created: 15/12/2005
  Description:  Calculo el importe a pagar por infracciones
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfraccionesDetalleCargo.nePrecioInfraccionChange(Sender: TObject);
begin
	FImporteTotalPorInfraccion := nePrecioInfraccion.ValueInt * FCantidadInfracciones;
	FImporteTotalGastosAdministrativos := trunc((FImporteTotalPorInfraccion * nePorcentaje.Value) / 100);
	lblImporteTotalInfraccion.Caption := formatFloat(FORMATO_IMPORTE, FImporteTotalPorInfraccion);
	neImporteTotalGastosAdministrativos.ValueInt := FImporteTotalGastosAdministrativos;
	lblImporteTotal.Caption := formatFloat(FORMATO_IMPORTE, FImporteTotalPorInfraccion + FImporteTotalGastosAdministrativos + neImporteIntereses.Value);
end;

{-----------------------------------------------------------------------------
  Function Name: nePorcentajeChange
  Author:  lgisuk
  Date Created: 15/12/2005
  Description:  Calculo el importe a pagar por Gastos administrativos
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfraccionesDetalleCargo.nePorcentajeChange(Sender: TObject);
begin
	FImporteTotalGastosAdministrativos := trunc((FImporteTotalPorInfraccion * nePorcentaje.Value) / 100);
	neImporteTotalGastosAdministrativos.ValueInt := FImporteTotalGastosAdministrativos;
	lblImporteTotal.Caption := formatFloat(FORMATO_IMPORTE,FImporteTotalPorInfraccion + FImporteTotalGastosAdministrativos + neImporteIntereses.Value);
end;

{-----------------------------------------------------------------------------
  Function Name: neImporteTotalGastosAdministrativosChange
  Author:  lgisuk
  Date Created: 19/12/2005
  Description:  Permito cambiar el importe total de gastos administrativos
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfraccionesDetalleCargo.neImporteTotalGastosAdministrativosChange(Sender: TObject);
begin
	FImporteTotalGastosAdministrativos := neImporteTotalGastosAdministrativos.ValueInt;
	lblImporteTotal.Caption := formatFloat(FORMATO_IMPORTE,FImporteTotalPorInfraccion + FImporteTotalGastosAdministrativos + neImporteIntereses.Value);
end;

{-----------------------------------------------------------------------------
  Function Name: neImporteInteresesChange
  Author:  lgisuk
  Date Created: 15/12/2005
  Description:  Obtengo el importe total
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfraccionesDetalleCargo.neImporteInteresesChange(Sender: TObject);
begin
	lblImporteTotal.Caption := formatFloat(FORMATO_IMPORTE,FImporteTotalPorInfraccion + FImporteTotalGastosAdministrativos + neImporteIntereses.Value);
end;

{-----------------------------------------------------------------------------
  Function Name: btnFacturarClick
  Author:  lgisuk
  Date Created: 15/12/2005
  Description:  Valido que los datos ingresados sean correctos
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfraccionesDetalleCargo.btnFacturarClick(Sender: TObject);
resourcestring
	MSG_ERROR_UNITARY_OBLIGATORY_PRICE = 'Se debe ingresar el precio de la infracci�n para generar el comprobante.';
	MSG_ERROR_INVALID_DATE = 'Se debe ingresar como fecha de emisi�n una fecha mayor o igual a %s (Fecha �ltima Nota de Cobro Infracci�n emitida)';
const
	STR_DO_YOU_WANT_TO_CREATE_CHARGUES_OF_COLLECTION = '�Desea Realizar los Cargos al Cliente?';
var
	FechaUltimoNotaCobroInfraccion: TDateTime;
begin
	FechaUltimoNotaCobroInfraccion := QueryGetValueDateTime(DMConnections.BaseCAC, 'SELECT ISNULL((SELECT MAX(FechaEmision) FROM Comprobantes (NOLOCK) WHERE TipoComprobante = ''NI''), CAST(''19000101'' as DATETIME))');

	if not validateControls(
		[nePrecioInfraccion, deFechaEmision],
		[nePrecioInfraccion.value <> 0,
		((deFechaEmision.date >= FechaUltimoNotaCobroInfraccion) )],
		caption,
		[MSG_ERROR_UNITARY_OBLIGATORY_PRICE,
		format(MSG_ERROR_INVALID_DATE, [DateTimeToStr(FechaUltimoNotaCobroInfraccion)])]) then
		exit;


	If MsgBox(STR_DO_YOU_WANT_TO_CREATE_CHARGUES_OF_COLLECTION, self.Caption, MB_YESNO + MB_ICONQUESTION) <> IDYES then begin
		Exit;
	end;

	ModalResult := mrOk;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author:  lgisuk
  Date Created: 15/12/2005
  Description:  Permito Cerrar la Ventana
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFormFacturacionInfraccionesDetalleCargo.BtnSalirClick(Sender: TObject);
begin
	ModalResult := mrCancel;
end;


end.
