program CRM;



uses
  Forms,
  OpenOnce,
  Controls,
  SysUtils,
  Windows,
  MidasLib,
  Navigator in '..\Comunes\Navigator.pas',
  DMConnection in '..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  MaskCombo in '..\Componentes\MaskCombo\MaskCombo.pas',
  Login in '..\Comunes\Login.pas' {LoginForm},
  Util,
  UtilProc,
  StartupSAU in 'StartupSAU.pas' {FormStartup},
  CacProcs in '..\Comunes\CacProcs.pas',
  CambioPassword in '..\Comunes\CambioPassword.pas' {FormCambioPassword},
  PeaTypes in '..\Comunes\PeaTypes.pas',
  DPSPageControl in '..\Componentes\PageControl\DPSPageControl.pas',
  FrmRptInformeUsuarioConfig in '..\Comunes\FrmRptInformeUsuarioConfig.pas' {FormRptInformeUsuarioConfig},
  FrmRptInformeUsuario in '..\Comunes\FrmRptInformeUsuario.pas' {FormRptInformeUsuario},
  Mensajes in '..\Comunes\Mensajes.pas' {FormMensajes},
  FrmEditarDomicilio in '..\Comunes\FrmEditarDomicilio.pas' {formEditarDomicilio},
  RStrings in '..\Comunes\RStrings.pas',
  FrmSCDatosVehiculo in 'FrmSCDatosVehiculo.pas' {FormSCDatosVehiculo},
  frmSeleccionSolicitudDuplicada in 'frmSeleccionSolicitudDuplicada.pas' {formSeleccionSolicitudDuplicada},
  FreDomicilio in '..\Comunes\FreDomicilio.pas' {FrameDomicilio: TFrame},
  FrmHacerEncuesta in '..\Comunes\FrmHacerEncuesta.pas' {FormHacerEncuesta},
  ConstParametrosGenerales in '..\Comunes\ConstParametrosGenerales.pas',
  frePersonas in '..\Comunes\frePersonas.pas' {FramePersona: TFrame},
  FreTelefono in '..\Comunes\FreTelefono.pas' {FrameTelefono: TFrame},
  frmGestionComponentesObligatorios in '..\Comunes\frmGestionComponentesObligatorios.pas' {FormGestionComponentesObligatorios},
  frmDiferenciaSolicitudContacto in 'frmDiferenciaSolicitudContacto.pas' {formDiferenciaSolicitudContacto},
  FrmMotivoCancelacion in 'FrmMotivoCancelacion.pas' {FormMotivoCancelacion},
  frmMediosPagos in '..\Comunes\frmMediosPagos.pas' {FormMediosPagos},
  UtilMails in '..\Comunes\UtilMails.pas',
  frmFAQ in '..\Comunes\frmFAQ.pas' {frmFAQs},
  FrmObservacionesGeneral in '..\Comunes\FrmObservacionesGeneral.pas' {FormObservacionesGeneral},
  FrmTerminarTarea in '..\Comunes\FrmTerminarTarea.pas' {FormTerminarTarea},
  FrmCancelarTarea in '..\Comunes\FrmCancelarTarea.pas' {FormCancelarTarea},
  DMComunicacion in '..\Comunes\DMComunicacion.pas' {DMComunicaciones: TDataModule},
  DMOperacionesComunicacion in '..\Comunes\DMOperacionesComunicacion.pas' {DMOperacionesComunicaciones: TDataModule},
  OPScripts in '..\Comunes\OPScripts.pas',
  BuscaClientes in '..\Comunes\BuscaClientes.pas' {FormBuscaClientes},
  frmDatoPesona in '..\Comunes\frmDatoPesona.pas' {FreDatoPresona: TFrame},
  frmMediosPagosConvenio in '..\Comunes\frmMediosPagosConvenio.pas' {FormMediosPagosConvenio},
  frmDocPresentada in 'frmDocPresentada.pas' {formDocPresentada},
  frmFirmarConvenio in 'frmFirmarConvenio.pas' {formFirmarConvenio},
  frmMedioEnvioDocumentosCobro in 'frmMedioEnvioDocumentosCobro.pas' {formMedioEnvioDocumentosCobro},
  FrmRepresentantes in 'FrmRepresentantes.pas' {FormRepresentantes},
  frmConfirmaContrasena in '..\Comunes\frmConfirmaContrasena.pas' {FormConfirmaContrasena},
  FrmAperturaCierrePuntoVenta in '..\Comunes\FrmAperturaCierrePuntoVenta.pas' {FormAperturaCierrePuntoVenta},
  ComboBuscarCalle in '..\Componentes\ComboBuscarCalle\ComboBuscarCalle.pas',
  frmImprimirConvenio in 'frmImprimirConvenio.pas' {FormImprimirConvenio},
  MostrarDocumentos in '..\Comunes\MostrarDocumentos.pas' {FormMostrarDocumentos},
  frmRegistrarLlamada in 'frmRegistrarLlamada.pas' {FormRegistrarLlamada},
  FrmListadoTurnos in '..\Comunes\FrmListadoTurnos.pas' {FormListadoTurnos},
  FrmRecibirTagsNominados in '..\Comunes\FrmRecibirTagsNominados.pas' {FormRecibirTagsNominados},
  GeneradorDeIni in 'GeneradorDeIni.pas' {FormGeneradorIni},
  FrmGenerarOrdenPedidoTAGs in 'FrmGenerarOrdenPedidoTAGs.pas' {FormGenerarOrdenPedidoTAGs},
  FrmModificacionConvenio in 'FrmModificacionConvenio.pas' {FormModificacionConvenio},
  UtilReportes in '..\Comunes\UtilReportes.pas',
  Convenios in 'Convenios.pas',
  frmDocPresentadaModificacionConvenio in 'frmDocPresentadaModificacionConvenio.pas' {formDocPresentadaModificacionConvenio},
  FrmSeleccionarReimpresionPlantillas in 'FrmSeleccionarReimpresionPlantillas.pas' {FrmSeleccionarReimpresion},
  frmEliminarTag in 'frmEliminarTag.pas' {FormEliminarTag},
  frmSituacionTag in 'frmSituacionTag.pas' {formSituacionTag},
  FrameSCDatosVehiculo in 'FrameSCDatosVehiculo.pas' {FmeSCDatosVehiculo: TFrame},
  FrmRespuestaRNUT in 'FrmRespuestaRNUT.pas' {FormRespuestaRNUT},
  Filtros in '..\Componentes\Imagenes\Filtros.pas',
  UtilFacturacion in '..\Comunes\UtilFacturacion.pas',
  adminImg in '..\Comunes\adminImg.pas',
  utilImg in '..\Comunes\utilImg.pas',
  ImagePlus in '..\Componentes\Imagenes\ImagePlus.pas',
  formSuspensionCuenta in 'formSuspensionCuenta.pas' {frmSuspensionCuenta},
  FrmListadoConvenio in '..\Comunes\FrmListadoConvenio.pas' {FormListadoConvenios},
  FrmCheckListConvenio in 'FrmCheckListConvenio.pas' {FormCheckListConvenio},
  OrdenesServicio in '..\Comunes\OrdenesServicio.pas',
  FreContactoReclamo in '..\Comunes\FreContactoReclamo.pas' {FrameContactoReclamo: TFrame},
  FreCompromisoOrdenServicio in '..\Comunes\FreCompromisoOrdenServicio.pas' {FrameCompromisoOrdenServicio: TFrame},
  FreSolucionOrdenServicio in '..\Comunes\FreSolucionOrdenServicio.pas' {FrameSolucionOrdenServicio: TFrame},
  FrmReclamoGeneral in '..\Comunes\FrmReclamoGeneral.pas' {FormReclamoGeneral},
  MenuesContextuales in '..\Comunes\MenuesContextuales.pas',
  FrmReclamoFactura in '..\Comunes\FrmReclamoFactura.pas' {FormReclamoFactura},
  FrmReclamoCuenta in '..\Comunes\FrmReclamoCuenta.pas' {FormReclamoCuenta},
  ImagenesTransitos in '..\Comunes\ImagenesTransitos.pas' {frmImagenesTransitos},
  ImgTypes in '..\Comunes\ImgTypes.pas',
  ImgProcs in '..\Comunes\ImgProcs.pas',
  FrmChecklist in '..\Comunes\FrmChecklist.pas' {FormChecklist},
  frmVisorDocumentos in '..\Comunes\frmVisorDocumentos.pas' {formVisorDocumentos},
  frmMultimpleMedioPago in 'frmMultimpleMedioPago.pas' {formMultimpleMedioPago},
  ComprobantesEmitidos in '..\FAC - Facturacion\ComprobantesEmitidos.pas' {NavWindowComprobantesEmitidos},
  FrmPaseDiarioCambioFecha in 'FrmPaseDiarioCambioFecha.pas' {PaseDiarioConsultaEdicionForm},
  Frm_CambiarFechaUsoPaseDiario in 'Frm_CambiarFechaUsoPaseDiario.pas' {FrmCambiarFechaUsoPaseDiario},
  FormActivarCuenta in 'FormActivarCuenta.pas' {frmActivarCuenta},
  formEliminarCuenta in 'formEliminarCuenta.pas' {frmEliminarCuenta},
  frmReporteFacturacionDetallada in '..\FAC - Facturacion\frmReporteFacturacionDetallada.pas' {FormReporteFacturacionDetallada},
  formReacuperacionTag in 'formReacuperacionTag.pas' {frmReacuperacionTag},
  FrmUltimosConsumos in '..\FAC - Facturacion\FrmUltimosConsumos.pas' {NavWindowUltimosConsumos},
  FormConfirmarBajaModifConvenio in 'FormConfirmarBajaModifConvenio.pas' {frmConfirmarBajaModifConvenio},
  FrmReclamos in '..\Comunes\FrmReclamos.pas' {FReclamos},
  frmSeleccFactDetallada in '..\FAC - Facturacion\frmSeleccFactDetallada.pas' {fSeleccFactDetallada},
  FreHistoriaOrdenServicio in '..\Comunes\FreHistoriaOrdenServicio.pas' {FrameHistoriaOrdenServicio: TFrame},
  FrmNumeroReclamoAsignado in '..\Comunes\FrmNumeroReclamoAsignado.pas' {FNumeroReclamoAsignado},
  FrmRptEstadisticaReclamos in '..\Comunes\FrmRptEstadisticaReclamos.pas' {FRptEstadisticaReclamos},
  FrmOrdenesServicioBloqueadas in '..\Comunes\FrmOrdenesServicioBloqueadas.pas' {FOrdenesServicioBloquedas},
  FrmReclamoTransito in '..\Comunes\FrmReclamoTransito.pas' {FormReclamoTransito},
  ImprimirWO in '..\Comunes\ImprimirWO.pas' {frmImprimirWO},
  RVMClient in '..\Comunes\RVMClient.pas',
  RVMTypes in '..\Comunes\RVMTypes.pas',
  RVMBind in '..\Comunes\RVMBind.pas',
  FrmReclamoContactarCliente in '..\Comunes\FrmReclamoContactarCliente.pas' {FormReclamoContactarCliente},
  FrmReclamoFaltaVehiculo in '..\Comunes\FrmReclamoFaltaVehiculo.pas' {FormReclamoFaltaVehiculo},
  SeleccReimpresionComprobante in '..\FAC - Facturacion\SeleccReimpresionComprobante.pas' {frmSeleccReimpresion},
  FrmBonificacionFacturacionDeCuenta in 'FrmBonificacionFacturacionDeCuenta.pas' {FormBonificacionFacturacionDeCuenta},
  FrmNoFacturarTransitosDeCuenta in '..\Comunes\FrmNoFacturarTransitosDeCuenta.pas' {FormNoFacturarTransitosDeCuenta},
  FrmReclamoTransitoDesconoceInfraccion in '..\Comunes\FrmReclamoTransitoDesconoceInfraccion.pas' {FormReclamoTransitoDesconoceInfraccion},
  FrmConsultaTransitosNoCliente in 'FrmConsultaTransitosNoCliente.pas' {FormConsultaTransitosNoCliente},
  FrmTransitosReclamados in '..\Comunes\FrmTransitosReclamados.pas' {FormTransitosReclamados},
  FreFuenteReclamoOrdenServicio in '..\Comunes\FreFuenteReclamoOrdenServicio.pas' {FrameFuenteReclamoOrdenServicio: TFrame},
  FrmIncluirTransitosInfractoresAlReclamo in '..\Comunes\FrmIncluirTransitosInfractoresAlReclamo.pas' {FormIncluirTransitosInfractoresAlReclamo},
  FreNumeroOrdenServicio in '..\Comunes\FreNumeroOrdenServicio.pas' {FrameNumeroOrdenServicio: TFrame},
  FreRespuestaOrdenServicio in '..\Comunes\FreRespuestaOrdenServicio.pas' {FrameRespuestaOrdenServicio: TFrame},
  FrmTransitosReclamadosAcciones in '..\Comunes\FrmTransitosReclamadosAcciones.pas' {FormTransitosReclamadosAcciones},
  frmCambiarTag in 'frmCambiarTag.pas' {formCambiarTag},
  FrmMsgNormalizar in 'FrmMsgNormalizar.pas' {FormMsgNormalizar},
  FrmAnulacionRecibos in '..\Comunes\FrmAnulacionRecibos.pas' {frm_AnulacionRecibos},
  CSVUtils in '..\Comunes\CSVUtils.pas',
  FrmSolicitudContacto in 'FrmSolicitudContacto.pas' {FormSolicitudContacto},
  dmMensaje in '..\Comunes\dmMensaje.pas' {dmMensajes: TDataModule},
  ComponerEMail in '..\Comunes\ComponerEMail.pas' {frmComponerMail},
  MensajesEmail in '..\Comunes\MensajesEmail.pas',
  LoginSetup in '..\Comunes\LoginSetup.pas' {frmLoginSetup},
  LoginValuesEditor in '..\Comunes\LoginValuesEditor.pas' {frmLoginValuesEditor},
  Main in 'Main.pas' {MainForm},
  FrmSolicitud in 'FrmSolicitud.pas' {FormSolicitud},
  FrmDevolverTelevia in 'FrmDevolverTelevia.pas' {FDevolverTelevia},
  frm_IngresarComprobantes in '..\Comunes\frm_IngresarComprobantes.pas' {frmIngresarComprobantes},
  Frm_AgregarItemFacturacion in '..\Comunes\Frm_AgregarItemFacturacion.pas' {frmAgregarItemFacturacion},
  PeaProcs in '..\Comunes\PeaProcs.pas',
  CustomAssistance in '..\Comunes\CustomAssistance.pas' {frmLocalAssistance},
  fFacturacionInfraccionesDetalle in 'fFacturacionInfraccionesDetalle.pas' {FormFacturacionInfraccionesDetalle},
  fFacturacionInfracciones in 'fFacturacionInfracciones.pas' {FormFacturacionInfracciones},
  FrmRptNotaCobroDetalleInfracciones in 'FrmRptNotaCobroDetalleInfracciones.pas' {fRptNotaCobroDetalleInfracciones},
  FrmRptNotaCobroInfractor in 'FrmRptNotaCobroInfractor.pas' {fRptNotaCobroInfractor},
  fFacturacionInfraccionesDetalleCargo in 'fFacturacionInfraccionesDetalleCargo.pas' {FormFacturacionInfraccionesDetalleCargo},
  DeclHard,
  Declprn,
  ExplorarInfracciones in '..\Comunes\ExplorarInfracciones.pas' {frmExplorarInfraciones},
  frmEdicionInfraccion in '..\INF - Infractores\frmEdicionInfraccion.pas' {formEdicionInfraccion},
  formImagenOverview in '..\Comunes\formImagenOverview.pas' {frmImageOverview},
  ActualizarDatosInfractor in '..\Comunes\ActualizarDatosInfractor.pas' {DMActualizarDatosInfractor: TDataModule},
  frmListadoInfracciones in '..\INF - Infractores\frmListadoInfracciones.pas' {FormListadoInfracciones},
  frmValidarInfraccion in '..\INF - Infractores\frmValidarInfraccion.pas' {formValidarInfraccion},
  RVMLogin in '..\Comunes\RVMLogin.pas' {frmLogin},
  OPImageOverview in '..\Componentes\Imagenes\OPImageOverview.pas',
  ProcsOverview in '..\Componentes\Imagenes\ProcsOverview.pas',
  FrmRptReclamo in '..\Comunes\FrmRptReclamo.pas' {FRptReclamo},
  frmReemisionNotaCobro in '..\Comunes\frmReemisionNotaCobro.pas' {formReemisionNotaCobro},
  ReporteRecibo in '..\Comunes\ReporteRecibo.pas' {formRecibo},
  FrmImprimir in '..\Comunes\FrmImprimir.pas' {FormImprimir},
  PagoVentanilla in '..\Comunes\PagoVentanilla.pas' {frmPagoVentanilla},
  ReporteFactura in '..\Comunes\ReporteFactura.pas' {frmReporteFactura},
  frmCambiarEstadoTag in 'frmCambiarEstadoTag.pas' {FormCambiarEstadoTag},
  ReporteCK in '..\FAC - Facturacion\ReporteCK.pas' {frmReporteCK},
  ReporteNC in '..\Comunes\ReporteNC.pas' {frmReporteNC},
  FrmHistoricoListTag in '..\Comunes\FrmHistoricoListTag.pas' {frmHistoricoListTagForm},
  FrmAgregarCheque in '..\Comunes\FrmAgregarCheque.pas' {frmAgregarCheque},
  Frm_CargoNotaCredito in '..\FAC - Facturacion\Frm_CargoNotaCredito.pas' {frmCargoNotaCredito},
  ABMConceptosMovimiento in '..\FAC - Facturacion\ABMConceptosMovimiento.pas',
  XMLCAC in '..\Comunes\XMLCAC.pas',
  ConvenioToXML in '..\Comunes\ConvenioToXML.pas',
  ModeloEntrada in '..\Comunes\ModeloEntrada.pas',
  frm_ReIngresarComprobantes in '..\Comunes\frm_ReIngresarComprobantes.pas' {frmReIngresarComprobantes},
  frmSeleccionarMovimientosTelevia in 'frmSeleccionarMovimientosTelevia.pas' {frmSeleccionarMovimientosTeleviaFORM},
  frmPerdidaTelevia in 'frmPerdidaTelevia.pas' {frm_PerdidaTelevia},
  FormConfirmarImpresion in '..\Comunes\FormConfirmarImpresion.pas' {frmConfirmarImpresion},
  FrmConsultaTransitos in 'FrmConsultaTransitos.pas' {FormConsultaTransitos},
  ModificarFechaAlta in '..\Comunes\ModificarFechaAlta.pas' {frmModificarFechaAlta},
  frmConsultaTransitosARevalidar in 'frmConsultaTransitosARevalidar.pas' {ConsultaTransitosARevalidarForm},
  frmRevalidarTransito in 'frmRevalidarTransito.pas' {RevalidarTransitoForm},
  frmRptDetInfraccionesAnuladas in '..\Comunes\frmRptDetInfraccionesAnuladas.pas' {RptDetInfraccionesAnuladas},
  frmReporteNotaDebitoElectronica in '..\Comunes\frmReporteNotaDebitoElectronica.pas' {ReporteNotaDebitoElectronicaForm},
  frmReporteBoletaFacturaElectronica in '..\Comunes\frmReporteBoletaFacturaElectronica.pas' {ReporteBoletaFacturaElectronicaForm},
  frmReporteNotaCreditoElectronica in '..\Comunes\frmReporteNotaCreditoElectronica.pas' {ReporteNotaCreditoElectronicaForm},
  DTEControlDLL in '..\FAC - Facturacion\DBNet\DTEControlDLL.pas',
  frmFacturacionManual in '..\Comunes\frmFacturacionManual.pas' {FacturacionManualForm},
  frmEmitirCertificadoInfracciones in 'frmEmitirCertificadoInfracciones.pas' {EmitirCertificadoInfraccionesForm},
  frmABMListaBlanca in 'frmABMListaBlanca.pas' {ABMListaBlancaForm},
  frmNotaCreditoInfractor in 'frmNotaCreditoInfractor.pas' {NotaCreditoInfractorForm},
  frmReportCertificadoInfracciones in '..\Comunes\frmReportCertificadoInfracciones.pas' {ReportCertificadoInfraccionesForm},
  EncriptaRijandel in '..\Comunes\EncriptaRijandel.pas',
  AES in '..\Comunes\AES.pas',
  base64 in '..\Comunes\base64.pas',
  MsgBoxCN in '..\Comunes\MsgBoxCN.pas' {frmMsgBoxCN},
  PeaProcsCN in '..\Comunes\PeaProcsCN.pas',
  SysUtilsCN in '..\Comunes\SysUtilsCN.pas',
  frmBloqueosSistema in '..\Comunes\frmBloqueosSistema.pas' {BloqueosSistemaForm},
  frmMuestraMensaje in '..\Comunes\frmMuestraMensaje.pas' {PanelMensajesForm},
  CobranzasRoutines in '..\Comunes\CobranzasRoutines.pas',
  CobranzasClasses in '..\Comunes\CobranzasClasses.pas',
  CobranzasResources in '..\Comunes\CobranzasResources.pas',
  FreConcesionariaReclamoOrdenServicio in '..\Comunes\FreConcesionariaReclamoOrdenServicio.pas' {FrameConcesionariaReclamoOrdenServicio: TFrame},
  FrmReclamoEstacionamiento in '..\Comunes\FrmReclamoEstacionamiento.pas' {FormReclamoEstacionamiento},
  FrmEstacionamientosReclamadosAcciones in '..\Comunes\FrmEstacionamientosReclamadosAcciones.pas' {FormEstacionamientosReclamadosAcciones},
  FrmEstacionamientosReclamados in '..\Comunes\FrmEstacionamientosReclamados.pas' {FormEstacionamientosReclamados},
  FrmInicioConsultaConvenio in '..\Comunes\FrmInicioConsultaConvenio.pas' {FormInicioConsultaConvenio},
  frmImagenesTicketVentaManual in 'frmImagenesTicketVentaManual.pas' {ImagenesTicketVentaManuallForm},
  frmRevalidarTicketVentaManual in 'frmRevalidarTicketVentaManual.pas' {RevalidarTicketVentaManualForm},
  Diccionario in '..\Comunes\Diccionario.pas',
  ClaveValor in '..\Comunes\ClaveValor.pas',
  frmVentanaAviso in '..\Comunes\Avisos\frmVentanaAviso.pas' {VentanaAvisoForm},
  Aviso in '..\Comunes\Avisos\Aviso.pas',
  TipoFormaAtencion in '..\Comunes\Avisos\TipoFormaAtencion.pas',
  Notificacion in '..\Comunes\Avisos\Notificacion.pas',
  AvisoTagVencidos in '..\Comunes\Avisos\AvisoTagVencidos.pas',
  NotificacionImpresionDoc in '..\Comunes\Avisos\NotificacionImpresionDoc.pas',
  NotificacionImpresionReporte in '..\Comunes\Avisos\NotificacionImpresionReporte.pas' {FormNotificacionImpresionReporte},
  frmMuestraPDF in '..\Comunes\frmMuestraPDF.pas' {MuestraPDFForm},
  FactoryINotificacion in '..\Comunes\Avisos\FactoryINotificacion.pas',
  frmDMGuardarConvenios in '..\Comunes\frmDMGuardarConvenios.pas' {DMGuardarConvenios: TDataModule},
  frmMedioEnvioMail in '..\Comunes\frmMedioEnvioMail.pas' {FormMedioEnvioMail},
  frmReporteContratoAdhesionPA in '..\Comunes\frmReporteContratoAdhesionPA.pas' {ReporteContratoAdhesionPAForm},
  FrmHabilitarCuentasListasAcceso in 'FrmHabilitarCuentasListasAcceso.pas' {FormHabilitarCuentasListasAcceso},
  FrmHabilitarCuentasVentanaEnrolamiento in 'FrmHabilitarCuentasVentanaEnrolamiento.pas' {FormHabilitarCuentasVentanaEnrolamiento},
  frmAdhesionPreInscripcionPAK in 'frmAdhesionPreInscripcionPAK.pas' {AdhesionPreInscripcionPAKForm},
  frmReporteCompDevDinero in '..\Comunes\frmReporteCompDevDinero.pas' {ReporteCompDevDineroForm},
  frmConsultaPatenteAraucoTAG in 'frmConsultaPatenteAraucoTAG.pas' {ConsultaPatenteAraucoTAGForm},
  frmAcercaDe in '..\Comunes\frmAcercaDe.pas' {AcercaDeForm},
  EBEditors in '..\Componentes\Editor\EBEditors.pas',
  FrmRptBajasForzadas in '..\INT - Interfaces\FrmRptBajasForzadas.pas' {FRptBajasForzadas},
  ComunInterfaces in '..\Comunes\ComunInterfaces.pas',
  ComunesInterfaces in '..\INT - Interfaces\ComunesInterfaces.pas',
  MOPInfBind in '..\INT - Interfaces\MOPInfBind.pas',
  FrmRptErroresSintaxis in '..\INT - Interfaces\FrmRptErroresSintaxis.pas' {FRptErroresSintaxis},
  frmMensajeACliente in '..\Comunes\frmMensajeACliente.pas' {FormMensajeACliente},
  Crypto in '..\Comunes\Crypto.pas',
  frmConsultaAltaOtraConsecionaria in 'frmConsultaAltaOtraConsecionaria.pas' {FormConsultaAltaOtraConsecionaria},
  frmConsultaAltaPArauco in 'frmConsultaAltaPArauco.pas' {FormConsultaAltaPArauco},
  frmCodigoConvenioFacturacion in 'frmCodigoConvenioFacturacion.pas' {formCodigoConvenioFacturacion},
  frmConvenioCuentasRUT in 'frmConvenioCuentasRUT.pas' {formConvenioCuentasRUT},
  DetalleTurnoCierre in '..\REC - Recaudacion\DetalleTurnoCierre.pas' {DetalleCierreTurno},
  LoginSup in '..\REC - Recaudacion\LoginSup.pas' {FrmConfirmacionSupervisor},
  ABMPersonas in 'ABMPersonas.pas' {frmABMPersonas},
  PatronBusqueda in 'PatronBusqueda.pas' {frmPatronBusqueda},
  frmResultadoSuscripcionCuentas in 'frmResultadoSuscripcionCuentas.pas' {formResultadoSuscripcionCuentas},
  frmImagenesSPC in 'frmImagenesSPC.pas' {ImagenesSPCForm},
  ImagenesTransitosSPC in '..\Comunes\ImagenesTransitosSPC.pas' {frmImagenesTransitosSPC},
  frmRevalidarTransitoSPC in 'frmRevalidarTransitoSPC.pas' {RevalidarTransitoSPCForm},
  ApplicationUpdateRequest in '..\ComunesNew\ApplicationUpdateRequest.pas',
  ApplicationGlobalVariables in '..\ComunesNew\ApplicationGlobalVariables.pas',
  WritingToLog in '..\ComunesNew\WritingToLog.pas',
  ApplicationControl in '..\ComunesNew\ApplicationControl.pas',
  frmAltaForzada in 'frmAltaForzada.pas' {FormAltaForzada},
  frmSeleccionarGrupoFacturacion in 'frmSeleccionarGrupoFacturacion.pas' {frmSeleccionarGrupoFacturacionForm},
  PleaseWait in 'PleaseWait.pas' {frmPleaseWait},
  frmAjusteCaja in '..\Comunes\frmAjusteCaja.pas' {FormAjusteCaja},
  frmAjusteEfectivoCaja in '..\Comunes\frmAjusteEfectivoCaja.pas',
  Cuentas in '..\Comunes\Clases\Cuentas.pas',
  Categoria in '..\Comunes\Clases\Categoria.pas',
  ClaseBase in '..\Comunes\Clases\ClaseBase.pas',
  frmRptCierreTurno in '..\Comunes\frmRptCierreTurno.pas' {FormRptCierreTurno},
  TurnosCategoriasTags in '..\Comunes\Clases\TurnosCategoriasTags.pas',
  FrameDatosVehiculos in '..\Comunes\Clases\FrameDatosVehiculos.pas',
  frmAdministrarDescuentos in '..\Comunes\frmAdministrarDescuentos.pas' {FormAdministrarDescuentos},
  frmPatentesConvenio in '..\Comunes\frmPatentesConvenio.pas' {FormPatentesConvenio},
  frmCrearCampannas in 'frmCrearCampannas.pas' {FormCrearCampannas},
  ExportarToExcel in '..\Comunes\ExportarToExcel.pas',
  FreSubTipoReclamoOrdenServicio in '..\Comunes\FreSubTipoReclamoOrdenServicio.pas' {FrameSubTipoReclamoOrdenServicio: TFrame},
  FrmAsignacionCasos in '..\Comunes\FrmAsignacionCasos.pas' {FrAsignacionCasos},
  FrmSeleccionarPlantillaEMail in '..\Comunes\FrmSeleccionarPlantillaEMail.pas' {FormSeleccionarPlantillaEMail},
  DB_CRUDCommonProcs in '..\Comunes\DB_CRUDCommonProcs.pas';

{$R *.res}

begin
    Application.Initialize;
    Application.Title	:= 'Atención a Usuarios';

    if not ActualizadorUpdateNeeded then begin
        if not MidasUpdateNeeded then begin
            if not ApplicationINIUPdateNeeded then begin
                if not ApplicationUpdateNeeded then begin

                    GetOtherFilesCheckUpdate;


                    if not AlreadyOpen then begin

                        Screen.Cursor := crAppStart;
                        Application.ProcessMessages;
                        FormStartup := TFormStartup.Create(nil);
                        FormStartup.Show;
                        FormStartup.Update;
                        Application.ProcessMessages;
                        Sleep(2000);


                        Application.CreateForm(TDMConnections, DMConnections);
  Application.CreateForm(TDMComunicaciones, DMComunicaciones);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TFrAsignacionCasos, FrAsignacionCasos);
  Mainform.SetBounds(Screen.DesktopLeft, Screen.DesktopTop, Screen.DesktopWidth, Screen.DesktopHeight);
                        Mainform.Show;

                        if ApplicationLocalExecution then begin
                            if not MainForm.Inicializa then begin
                                Application.Terminate;
                            end;
                        end
                        else begin
                            ShowMsgBoxCN('CRM - Validación de Ejecución','La Aplicación NO puede ejecutarse en Red. Copie la Aplicación en su Computador para poder ejecutarla.', MB_ICONERROR, nil);
                            Application.Terminate;
                        end;

                        Screen.Cursor := crDefault;
                        Application.ProcessMessages;

                        ShortDateFormat := 'dd/mm/yyyy';
                        LongDateFormat  := 'dd/mm/yyyy';
                        Application.Run;
                    end
                    else Application.Terminate;
                end
                else begin
                    ApplicationUpdateExecute;
                    Application.Terminate;
                end;
            end
            else begin
                ApplicationINIUpdateExecute;
                Application.Terminate;
            end;
        end
        else begin
            MidasUpdateExecute;
            Application.Terminate;
        end;
    end
    else begin
        ActualizadorUpdateExecute;
        Application.Terminate;
    end;

end.
