object FormAnaliticoCierreDeTurno: TFormAnaliticoCierreDeTurno
  Left = 192
  Top = 114
  BorderStyle = bsDialog
  Caption = 'Informe Anal'#237'tico de Cierre de Turno'
  ClientHeight = 170
  ClientWidth = 333
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object bv_Datos: TBevel
    Left = 5
    Top = 8
    Width = 322
    Height = 124
  end
  object lbl_NumeroTurno: TLabel
    Left = 86
    Top = 57
    Width = 58
    Height = 13
    Caption = 'N'#176' de Turno'
  end
  object pnl_Botones: TPanel
    Left = 0
    Top = 139
    Width = 333
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      333
      31)
    object btn_Aceptar: TDPSButton
      Left = 169
      Top = 3
      Anchors = [akTop, akRight]
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btn_AceptarClick
    end
    object btn_Cancelar: TDPSButton
      Left = 249
      Top = 3
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btn_CancelarClick
    end
  end
  object txt_NumeroTurno: TNumericEdit
    Left = 152
    Top = 53
    Width = 95
    Height = 21
    Hint = 'N'#250'mero del Turno para el cual obtener el informe'
    Color = 16444382
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Decimals = 0
  end
  object sp_EsTurnoCerrado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EsTurnoCerrado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EsCerrado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@UsuarioTurno'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end>
    Left = 6
    Top = 140
  end
end
