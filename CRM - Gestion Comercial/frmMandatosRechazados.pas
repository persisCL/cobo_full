{-------------------------------------------------------------------------------
File Name   : RendicionPAC.pas
Author      : ndonadio
Date Created:
Language    : ES-AR
Description :
--------------------------------------------------------------------------------
Revision History
--------------------------------------------------------------------------------
Author: rcastro
Date Created: 17/12/2004
Description: revisión general
-------------------------------------------------------------------------------}
unit frmMandatosRechazados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ListBoxEx, DBListEx, ComCtrls, StrUtils,
  PeaTypes, UtilProc, DMConnection, util, DB, ADODB,
  DmiCtrls,DBCtrls, ExtCtrls, Mask, VariantComboBox, BuscaClientes;

type
	TMandatosRechazados = class(TForm)
    dsMandatosPendientes: TDataSource;
    spMandatos: TADOStoredProc;
	spMandatosCodigoConvenio: TIntegerField;
    spMandatosNumeroConvenio: TStringField;
	spMandatosNumeroConvenioFormateado: TStringField;
    spMandatosFechaAccion: TDateTimeField;
    spMandatosCodigoLote: TIntegerField;
    spMandatosCodigoAccionSantander: TSmallintField;
	spMandatosCodigoTipoCuentaBancaria: TWordField;
	spMandatosCodigoBanco: TIntegerField;
    spMandatosNombreBanco: TStringField;
    spMandatosNroCuentaBancaria: TStringField;
    spMandatosCodigoRespuestaSantander: TSmallintField;
    spMandatosFechaRespuesta: TDateTimeField;
    spMandatosObservaciones: TStringField;
    spMandatosUsuario: TStringField;
    spMandatosEnviadoAlCAC: TBooleanField;
    dblPendientes: TDBListEx;
    spMandatosDescriRespuesta: TStringField;
    Panel1: TPanel;
    DBEdit1: TDBEdit;
    lObservaciones: TLabel;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
	peNumeroDocumento: TPickEdit;
    cbConvenio: TVariantComboBox;
    spCliente: TADOStoredProc;
    grbDatosCliente: TGroupBox;
    Label2: TLabel;
	Label4: TLabel;
    lNombre: TLabel;
    lApellido: TLabel;
    btnSalir: TButton;
    btnMarcarRechazado: TButton;
	btnMarcarResuelto: TButton;
    btnMarcarNoResuelto: TButton;
    spClienteCodigoDocumento: TStringField;
    spClienteNumeroDocumento: TStringField;
    spClienteCodigoCliente: TAutoIncField;
    spClienteApellido: TStringField;
    spClienteApellidoMaterno: TStringField;
    spClienteNombre: TStringField;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnMarcarRechazadoClick(Sender: TObject);
    procedure btnMarcarResueltoClick(Sender: TObject);
    procedure btnMarcarNoResueltoClick(Sender: TObject);
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure cbConvenioChange(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure spMandatosAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FUltimaBusqueda: TBusquedaCliente;
	FPersona: TDatosPersonales;
	function Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
  end;

resourcestring
    CONST_ITEM_TODOS = 'Todos Los Convenios';

var
  MandatosRechazados: TMandatosRechazados;

implementation

{$R *.dfm}

procedure CargarConveniosCliente(Conn: TADOConnection; Combo: TVariantComboBox; CodigoCliente: LongInt );
Var
	Qry		: TADOQuery;
    FOldSel : Variant;
    nSelIdx : integer;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT dbo.FormatearNumeroConvenio(NumeroConvenio) as NumeroConvenio, CodigoConvenio ' +
        				'FROM Convenio WITH (NOLOCK) WHERE Convenio.CodigoCliente = ' + inttostr(CodigoCliente);
		Qry.Open;

    	FOldSel := Combo.Value;
		Combo.Clear;
		nSelIdx := 0;
		while not Qry.Eof do begin
            Combo.Items.Add(Qry.fieldbyname('NumeroConvenio').AsString,
              trim(Qry.fieldbyname('CodigoConvenio').AsString));
            if ( Qry.fieldbyname('CodigoConvenio').Value = FOldSel ) then nSelIdx := Combo.Items.Count - 1;
			Qry.Next;
        end;
        Combo.ItemIndex := nSelIdx;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure TMandatosRechazados.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TMandatosRechazados.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TMandatosRechazados.btnMarcarRechazadoClick(Sender: TObject);
var
    Qry: TADOQuery;
    CodCon : Integer;
    StrSQL: string;
	RazonRechazo: string;
begin
    CodCon := spMandatos.FieldByName('CodigoConvenio').AsInteger;
    Qry := TADOQuery.Create(Self);
    DMConnections.BaseCAC.BeginTrans;
    RazonRechazo := InputBox('Observaciones','Ingrese Razón de Rechazo','Rechazo No Especificado');
	try
        spMandatos.Edit;
        spMandatos.FieldByName('Observaciones').Value := RazonRechazo;
        spMandatos.FieldByName('FechaRespuesta').Value := Date;
        spMandatos.FieldByName('CodigoRespuestaSantander').Value := CONST_RESPUESTA_MANDATO_RECHAZADA;
        spMandatos.Post;

        StrSQL :=   'UPDATE ConvenioMedioPagoAutomaticoPAC ' + crlf +
                ' SET Activo = 0 ' + crlf +
                ' WHERE CodigoConvenio = ' + IntToStr(COdCon);

        Qry.SQL.Add(strSQL);
        Qry.ExecuteOptions := [eoExecuteNoRecords];
		Qry.Connection := DMConnections.BaseCAC ;
		Qry.ExecSQL;
        If  Qry.Active then Qry.Close;
		StrSQL :=   'UPDATE Convenio ' + crlf +
				' SET TipoMedioPagoAutomatico = dbo.CONST_TIPO_MEDIO_PAGO_AUTOMATICO_NINGUNO() ' + crlf +
                ' WHERE CodigoConvenio = ' + IntToStr(COdCon);
        Qry := TADOQuery.Create(Self);
		Qry.SQL.Add(strSQL);
		Qry.ExecuteOptions := [eoExecuteNoRecords];
        Qry.Connection := DMConnections.BaseCAC;
        Qry.ExecSQL;



        If  Qry.Active then Qry.Close;
        Qry.Free;

        spMandatos.Close;
        spMandatos.Open;
        DMConnections.BaseCAC.CommitTrans;
    except
        DMConnections.BaseCAC.RollbackTrans;
        if Qry <> nil then Qry.Destroy;
    end;

end;

procedure TMandatosRechazados.btnMarcarResueltoClick(Sender: TObject);
var
	ObsResuelto: string;
    Fecha: TDateTime;
    FechaAccion: TDateTime;
    CodigoConvenio: Integer;
	Qry: TADOQuery;
	strSQL : string;
begin
    Qry := TADOQuery.Create(Self) ;
try
    ObsResuelto := InputBox('Ingrese Observacion de '+ crlf +'Resolución proviniente del CAC:','Resolucion de Mandato Rechazado','')  ;
	Fecha := Now;
    FechaAccion := spMandatos.FieldByName('FechaAccion').AsDateTime ;
    CodigoConvenio := spMandatos.FieldByName('CodigoConvenio').asInteger;
    spMandatos.Edit;
    spMandatos.FieldByName('Observaciones').Value := spMandatos.FieldByName('Observaciones').AsString + '-- RESUELTO POR EL CAC';
    spMandatos.FieldByName('FechaRespuesta').Value := Fecha;
    spMandatos.FieldByName('CodigoRespuestaSantander').Value := CONST_RESPUESTA_MANDATO_RESUELTA;
    spMandatos.Post;
    spMandatos.Close;
	spMandatos.Open;

    // Asiento el Rechazo Resuelto en el Historico
    If  Qry.Active then Qry.Close;
	StrSQL :=   'INSERT INTO HistoricoMandatosRechazadosResueltos ' + crlf +
			' VALUES('+IntToStr(CodigoConvenio)+','+DateToStr(FechaAccion)+','+DateToStr(Fecha)+','+ QuotedStr(ObsResuelto)+')';

	Qry := TADOQuery.Create(Self);
    Qry.SQL.Add(strSQL);
    Qry.ExecuteOptions := [eoExecuteNoRecords];
    Qry.Connection := DMConnections.BaseCAC;
	Qry.ExecSQL;
finally
    Qry.Free;
end;
end;

procedure TMandatosRechazados.btnMarcarNoResueltoClick(Sender: TObject);
begin
    spMandatos.Edit;
    spMandatos.FieldByName('Observaciones').Value := spMandatos.FieldByName('Observaciones').AsString + 'NO RESUELTO POR EL CAC';
    spMandatos.FieldByName('FechaRespuesta').Value := Date;
	spMandatos.FieldByName('CodigoRespuestaSantander').Value := CONST_RESPUESTA_MANDATO_NO_RESUELTA;
    spMandatos.Post;
    spMandatos.Close;
    spMandatos.Open;
end;

procedure TMandatosRechazados.peNumeroDocumentoChange(Sender: TObject);
var
	CodigoCliente: Integer;
begin
	if (ActiveControl <> Sender) OR (LENGTH(peNumeroDocumento.Text) < 7 )then begin
            //blanqueo todo... quiere decir que ya no son validos los
            //datos que tengo a la vista...
            lNombre.Caption := '';
            lApellido.Caption := '';
            if spMandatos.Active then spMandatos.Close ;
            if spCliente.Active then spCliente.Close;
            cbConvenio.Clear;
            cbConvenio.Items.Add(CONST_ITEM_TODOS,0);
            Exit;
    end;

	if spCliente.Active then spCliente.Close;
	spCliente.Parameters.ParamByName('@NumeroDocumento').Value :=peNumeroDocumento.Text;
	spCliente.Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';
	spCliente.Parameters.ParamByName('@CodigoCliente').Value := 0;

	try
		spCliente.Open;
		CodigoCliente := spcliente.fieldbyname('CodigoCliente').asInteger;
		if CodigoCliente > 0 then begin
			CargarConveniosCliente(DMConnections.BaseCAC,cbConvenio, CodigoCliente);
			lNombre.Caption := spCliente.FieldByName('Nombre').asString;
			lApellido.Caption := Trim(spCliente.FieldByName('Apellido').asString) + ' ' +
								Trim(spCliente.FieldByName('ApellidoMaterno').asString);
		end
		else cbConvenio.Clear;

		if cbConvenio.Items.Count > 0 then begin
			cbConvenio.ItemIndex := 0;
			spMandatos.Close;
			spMandatos.Parameters.ParamByName('@CodConvenio').Value := IVal(cbConvenio.Items[cbConvenio.ItemIndex].Value);
			spMandatos.Open;
		end;

	finally
		if spCliente.Active then spCliente.Close;
	end;
end;

procedure TMandatosRechazados.cbConvenioChange(Sender: TObject);
begin
	if ActiveControl <> Sender then Exit;

	Screen.Cursor := crHourglass;
	spMandatos.Close;
	spMandatos.Parameters.ParamByName('@CodConvenio').Value := cbConvenio.Items[cbConvenio.ItemIndex].Value ;
	spMandatos.Open;
	Screen.Cursor := crDefault;
end;

procedure TMandatosRechazados.peNumeroDocumentoButtonClick(
  Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
			FUltimaBusqueda := F.UltimaBusqueda;
			FPersona :=  f.Persona ;
			peNumeroDocumento.Text := f.Persona.NumeroDocumento;
		end;
	end;
	F.free;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 03/12/2004
  Description:  Inicialización de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TMandatosRechazados.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
var
	S: TSize;
begin
	Result := False;
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

	btnMarcarRechazado.Enabled := False;
	btnMarcarResuelto.Enabled := False;
	btnMarcarNOResuelto.Enabled := False;

	lNombre.Caption := '';
	lApellido.Caption := '';

	try
		Result := DMConnections.BaseCAC.Connected;
        cbConvenio.Clear;
        cbConvenio.Items.Add(CONST_ITEM_TODOS,0);
	except
		on e: Exception do begin
			MsgBoxErr('Error al Inicializar', e.Message,'Error', MB_ICONERROR);
		end;
	end;
end;

procedure TMandatosRechazados.spMandatosAfterScroll(DataSet: TDataSet);
var
    Qry: TADOQuery;
begin
	Case DataSet.FieldByName('CodigoRespuestaSantander').Value of
		CONST_RESPUESTA_MANDATO_PENDIENTE: begin
			btnMarcarRechazado.Enabled := True;
			btnMarcarResuelto.Enabled := False;
			btnMarcarNOResuelto.Enabled := False;
		end;

		CONST_RESPUESTA_MANDATO_RECHAZADA: begin
			btnMarcarRechazado.Enabled := False;
			btnMarcarResuelto.Enabled := True;
			btnMarcarNOResuelto.Enabled := True;
		end;

		CONST_RESPUESTA_MANDATO_NO_RESUELTA: begin
			btnMarcarRechazado.Enabled := False;
			btnMarcarResuelto.Enabled := True;
			btnMarcarNOResuelto.Enabled := False;
		end;
	end;

    // Busco los datos del cliente...
    if spCliente.Active then spCliente.Close;

    try
      try
          Qry := TADOQuery.Create(Self);
          Qry.Connection := DMConnections.BaseCAC;
          Qry.SQL.Text := 'SELECT dbo.ObtenerCodigoCliente(' +  spMandatos.FieldByName('CodigoConvenio').AsString + ') as ElCliente' ;
          Qry.OPen;
          spCliente.Parameters.ParamByName('@CodigoCliente').Value := Qry.FieldByName('elCliente').AsInteger;
          Qry.Close;
          spCliente.Parameters.ParamByName('@NumeroDocumento').Value :=0;
          spCliente.Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';

          spCliente.Open;


          lNombre.Caption := trim(spCliente.FieldByName('Nombre').asString);
          lApellido.Caption := trim(spCliente.FieldByName('Apellido').asString )+ ' ' +
                              trim( spCliente.FieldByName('ApellidoMaterno').asString);

      except
          //Cliente no encontrado...
          On e:Exception do begin
              lNombre.Caption := 'Cliente No Encontrado';
              lApellido.Caption := e.Message;
          end;
      end;
    finally
        Qry.Free;
    end;
end;

end.
