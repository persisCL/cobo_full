{-------------------------------------------------------------------------------
Firma       : SS-1006-NDR-20120803
Description : Poder excluir veh�culos antes de dar de alta las cuentas.

Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Etiqueta	:	20160525 MGO
Descripci�n	:	Se reemplaza spEMAIL_AgregarMensaje por spEMAIL_Correspondencia_Agregar

Firma          : TASK_034_ECA_20160611
Descripci�n	: Se agrega suscripcion por RUT, Convenio y Cuenta 
--------------------------------------------------------------------------------}
unit frmConsultaAltaOtraConsecionaria;

interface

uses
  DMConnection,
  Util,
  UtilDB,
  UtilProc,
  PeaProcs,
  PeaTypes,
  BuscaClientes,
  //Otros
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, DbList, StdCtrls, Validate, DateEdit, ExtCtrls, ListBoxEx,
  DBListEx,  DmiCtrls, DBClient, ImgList, VariantComboBox, Grids, DBGrids,
  ComCtrls, RStrings, ImprimirWO,frmMuestraMensaje, ppCtrls, ppPrnabl, ppClass,
  ppBands, ppCache, ppDB, ppParameter, ppProd, ppReport, ppComm, ppRelatv,
  ppDBPipe, UtilRB, ppStrtch, ppSubRpt,
  frmMedioEnvioMail,
  frmReporteContratoAdhesionPA,
  ConstParametrosGenerales,
  PeaProcsCN, SysutilsCN,  ppMemo, ppVar, ppTypes,                              //SS-1006-NDR-20120803
  frmHabilitarCuentasVentanaEnrolamiento, Provider,                             //SS-1006-NDR-20120803
  RBSetup, PRinters,frmResultadoSuscripcionCuentas,                             //TASK_034_ECA_20160611
  frmImprimirConvenio;                                                          //TASK_036_ECA_20160620

type
  TFormConsultaAltaOtraConsecionaria = class(TForm)
    dblInfracciones: TDBListEx;
    Panel1: TPanel;
    btnImprimirImpedimentos: TButton;
    btnSalir: TButton;
    ppdbImpedimentos: TppDBPipeline;
    ttprImpedimentos: TppReport;
    ppParameterList2: TppParameterList;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand3: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppImage2: TppImage;
    ppLabel6: TppLabel;
    ppLabel1: TppLabel;
    ppLabel20: TppLabel;
    RBInterface: TRBInterface;
    pnlFiltros: TPanel;
    grpFiltros: TGroupBox;
    Label1: TLabel;
    lblNombreCli: TLabel;
    lblPersoneria: TLabel;
    lblPatente: TLabel;
    btn_Limpiar: TButton;
    btn_Filtrar: TButton;
    peNumeroDocumento: TPickEdit;
    edtPatente: TEdit;
    fnObtenerCodigoPersonaPorCuentaActiva: TADOStoredProc;
    spValidarRestriccionesAdhesionListaBlanca: TADOStoredProc;
    dsValidarRestriccionesAdhesionListaBlanca: TDataSource;
    lblRUT: TLabel;
    ppLine1: TppLine;
    ppLabel2: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    pplblRUT: TppLabel;
    pplblNombreCliente: TppLabel;
    ppDBMemDescripcion: TppDBMemo;
    ppLine2: TppLine;
    btnExclusionVehiculos: TButton;														//SS-1006-NDR-20120803
    spObtenerCuentasPersona: TADOStoredProc;											//SS-1006-NDR-20120803
    dspCuentasPersona: TDataSetProvider;												//SS-1006-NDR-20120803
    cdsCuentasPersona: TClientDataSet;													//SS-1006-NDR-20120803
    cdsCuentasPersonaPatente: TStringField;												//SS-1006-NDR-20120803
    cdsCuentasPersonaMarcaModeloColor: TStringField;									//SS-1006-NDR-20120803
    cdsCuentasPersonaConvenio: TStringField;											//SS-1006-NDR-20120803
    cdsCuentasPersonaCodigoEstadoCuenta: TSmallintField;								//SS-1006-NDR-20120803
    cdsCuentasPersonaAdheridoPA: TBooleanField;											//SS-1006-NDR-20120803
    cdsCuentasPersonaChequeado: TBooleanField;											//SS-1006-NDR-20120803
    cdsCuentasPersonaCodigoConvenio: TIntegerField;										//SS-1006-NDR-20120803
    cdsCuentasPersonaIndiceVehiculo: TIntegerField;
    spCRM_CuentasPersonaNoNativa_SELECT: TADOStoredProc;
    btnSuscribir: TButton;
    cdsVehiculos: TClientDataSet;
    dsVehiculos: TDataSource;
    chkDesuscribir: TCheckBox;										//SS-1006-NDR-20120803
    procedure BtnSalirClick(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnImprimirImpedimentosClick(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure peNumeroDocumentoKeyPress(Sender: TObject; var Key: Char);
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure edtPatenteChange(Sender: TObject);
    procedure dblInfraccionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure ppDBMemDescripcionPrint(Sender: TObject);
    procedure ttprImpedimentosBeforePrint(Sender: TObject);
    procedure btnImprimirContratoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnExclusionVehiculosClick(Sender: TObject);
    procedure cdsCuentasPersonaAfterOpen(DataSet: TDataSet);
    procedure cdsCuentasPersonaAfterPost(DataSet: TDataSet);
    procedure btnSuscribirClick(Sender: TObject);
    procedure chkDesuscribirClick(Sender: TObject);								//SS-1006-NDR-20120803
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
    FCodigoPersona,
    FCodigoAuditoria: Integer;
    FPersoneria,
    FRUTPersona: string;
    FLimpiando,
    FSoloConsulta: Boolean;
    FFechaHoy: TDate;
    procedure MostrarDatosCliente(CodigoCliente: Integer; RUT, Personeria: string);
    procedure BuscarCliente;
    procedure Limpiar(ObjetoALimpiar: TObject);
    function ValidarFiltros: Boolean;
    function ValidarCliente: Boolean;
    procedure ValidarRestriccionesAdhesionListaBlanca;
    procedure ActualizaEMailPersona(pEMail: string; pCodigoPersona: Integer);
    procedure ActualizarConveniosPersonaEnvioPorEMail(pCodigoPersona: Integer);
    procedure AdherirClienteTAGListaBlanca(pCodigoPersona: Integer; pEMail: string);
    function AgregarAuditoria_Adhesion(pNumeroDocumento, pPatente, pEMail: string; pCodigoPersona, pCumpleRequisitos, pAdheridoEnvioEMail, pImpresionContrato, pFirmaContrato, pCodigoAuditoria: Integer): Integer;
    procedure AdherirPorCuenta();														//SS-1006-NDR-20120803
    procedure ValidarCuentasNoNativa(CodigoPersonas: Integer);
    //INICIO: TASK_034_ECA_20160611
    function ValidarPersonaSuscrita(CodigoPersonas: Integer): Boolean;
    function ValidarConvenioSuscrito(CodigoConvenio: Integer): Boolean;
    function ValidarCuentaSuscrita(CodigoConvenio, IndiceVehiculo: Integer): Boolean;
    procedure BurcarDatosVehiculo(Patente: string; out Marca:string; out Modelo: string; out Anio:string; out TipoVehiculo: string); //TASK_049_ECA_20160706
    procedure CargarCuentas(CodigoPersonas, CodigoConvenio: Integer; Patente: string);
    procedure CargarCuentasSuscritas(CodigoPersonas, CodigoConvenio: Integer; Patente: string);
    procedure SuscribirPersona(CodigoPersonas: Integer);
    procedure SuscribirConvenio(CodigoConvenio: Integer);
    procedure SuscribirCuenta(CodigoConvenio,IndiceVehiculo: Integer);
    procedure DeSuscribirPersona(CodigoPersonas: Integer);
    procedure DeSuscribirConvenio(CodigoConvenio: Integer);
    procedure DeSuscribirCuenta(CodigoConvenio,IndiceVehiculo: Integer);
    
    procedure ImprimirDocumentos(DireccionConvenio: Boolean);
    //FIN: TASK_034_ECA_20160611
    procedure ObtenerDatosVehiculo(NumeroDocumento, Patente: string; out CodigoConvenio: Integer; out IndiceVehiculo: Integer); //TASK_037_ECA_20160622
  public
    { Public declarations }
    constructor ModalCreate(AOwner: TComponent);
    function Inicializar(): Boolean; overload;
    function Inicializar(pNumeroDocumento: string): boolean; overload;
    function Inicializar(CodigoConvenio: Integer; Suscribir: Boolean): boolean;  overload;
    function Inicializar(CodigoConvenio: Integer; Patente: string; Suscribir: Boolean): boolean;  overload;
  end;


var
  FormConsultaAltaOtraConsecionaria: TFormConsultaAltaOtraConsecionaria;
  FormConsultaAltaOtraConsecionariaSoloConsulta: TFormConsultaAltaOtraConsecionaria;

  FCodigoConvenio : Integer;                               //TASK_034_ECA_20160611
  FIndiceVehiculo : Integer;                               //TASK_034_ECA_20160611
  TipoSuscripcion : Integer; //1:RUT 2:Convenio 3:Cuenta   //TASK_034_ECA_20160611
  SalirPorMensaje : Boolean;                               //TASK_034_ECA_20160611
  PatentesSuscritas: string;                               //TASK_036_ECA_20160621

  EMailPersona: string;                                    //TASK_044_ECA_20160630
  ClienteAdheridoEnvioEMail: Boolean;                      //TASK_044_ECA_20160630

implementation

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}

constructor TFormConsultaAltaOtraConsecionaria.ModalCreate(AOwner: TComponent);
begin
	inherited Create(AOwner);

    FormStyle   := fsNormal;
    BorderIcons := [biSystemMenu];
end;

function TFormConsultaAltaOtraConsecionaria.Inicializar(): Boolean;
begin
    try
    	Result := False;

        SalirPorMensaje:= False; //TASK_034_ECA_20160611

        FSoloConsulta    := False;
        FCodigoPersona   := -1;
        FCodigoAuditoria := -1;
        FPersoneria      := EmptyStr;
        FRUTPersona      := EmptyStr;
        FLimpiando       := False;

        lblRUT.Caption        := EmptyStr;
        lblNombreCli.Caption  := EmptyStr;
        lblPersoneria.Caption := EmptyStr;

        FFechaHoy := SysutilsCN.NowBaseCN(DMConnections.BaseCAC);

        TipoSuscripcion := 1; //TASK_034_ECA_20160611

        Result     := True;
    except
    	on e: Exception do begin
        	ShowMsgBoxCN(e, Self);
        end;
    end;
end;

function TFormConsultaAltaOtraConsecionaria.Inicializar(pNumeroDocumento: string): Boolean;
begin
    try
    	Result := False;

        SalirPorMensaje:= False; //TASK_034_ECA_20160611

        FSoloConsulta    := True;
        FCodigoPersona   := -1;
        FCodigoAuditoria := -1;
        FPersoneria      := EmptyStr;
        FRUTPersona      := EmptyStr;
        FLimpiando       := True;

        pnlFiltros.Visible              := False;
        btnImprimirImpedimentos.Visible := False;
        //btnImprimirContrato.Visible     := False;                             //TASK_034_ECA_20160611
        btnExclusionVehiculos.Visible   := False;                               //SS-1006-NDR-20120803
        btnSuscribir.Visible            := true;  //False;                      //TASK_034_ECA_20160611
        peNumeroDocumento.Text := pNumeroDocumento;

        TipoSuscripcion := 1; //TASK_034_ECA_20160611

        btn_FiltrarClick(nil);

        Caption := Caption + ' - [ ' + lblNombreCli.Caption + lblPersoneria.Caption + ' ]';

        FLimpiando       := False;



        Result := True;
    except
    	on e: Exception do begin
        	ShowMsgBoxCN(e, Self);
        end;
    end;
end;

//BEGIN : SS-1006-NDR-20120803-------------------------------------------------
procedure TFormConsultaAltaOtraConsecionaria.btnExclusionVehiculosClick(Sender: TObject);
var
    f: TFormHabilitarCuentasVentanaEnrolamiento;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        try
//INICIO: TASK_034_ECA_20160611        
           //INICIO: TASK_013_ECA_20160520
//           if not cdsCuentasPersona.Active then
//            begin
//                spCRM_CuentasPersonaNoNativa_SELECT.Close;
//                spCRM_CuentasPersonaNoNativa_SELECT.Prepared := False;
//                spCRM_CuentasPersonaNoNativa_SELECT.Parameters.Refresh;
//                spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@CodigoPersona').Value      :=  FCodigoPersona;
//                spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@ErrorDescription').Value   :=  null;
//                spCRM_CuentasPersonaNoNativa_SELECT.Prepared := True;
//                with cdsCuentasPersona do begin
//                    Params.Clear;
//                    Params.Add;
//                    Params[0].Name      := '@CodigoPersona';
//                    Params[0].ParamType := ptInput;
//                    Params[0].DataType  := ftInteger;
//                    Params[0].Value     := FCodigoPersona;
//                    Open;
//                end;
//            end;
//FIN: TASK_034_ECA_20160611
           //FIN: TASK_013_ECA_20160520

            f := TFormHabilitarCuentasVentanaEnrolamiento.Create(nil);
            if f.Inicializar(cdsCuentasPersona) then
            begin
                if chkDesuscribir.Checked then
                    f.Caption:='Desuscripci�n Lista Blanca'
                else
                    f.Caption:='Suscripci�n Lista Blanca';

                f.ShowModal;
                //INICIO: TASK_034_ECA_20160611
                if (cdsCuentasPersona.RecordCount>0) and (btnSuscribir.Enabled=False) then
                begin
                    cdsCuentasPersona.First;
                    while not cdsCuentasPersona.Eof do
                    begin
                        if cdsCuentasPersona.FieldByName('Chequeado').AsBoolean then
                        begin
                            btnSuscribir.Enabled:=True;
                            Break;
                        end;
                        cdsCuentasPersona.Next;
                    end;
                    cdsCuentasPersona.First;
                end;
                //FIN: //TASK_034_ECA_20160611
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(f) then FreeAndNil(f);
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;
//END : SS-1006-NDR-20120803-------------------------------------------------

procedure TFormConsultaAltaOtraConsecionaria.btnImprimirContratoClick(Sender: TObject);
	resourcestring
    	rsTituloActivacion  	 = 'Activaci�n Servicio TAG Lista Blanca';
        rsMensajeActivacion 	 = '� Firm� el Cliente todos los documentos del contrato del Servicio TAG Lista Blanca ?';
        rsMensajeActivado   	 = 'El Servicio TAG Lista Blanca, se activ� correctamente para el Cliente.';
    	rsTituloImpresion   	 = 'Impresi�n Contrato Servicio TAG Lista Blanca';
        rsMensajeImpresion  	 = '� Se imprimi� el contrato del Servicio TAG Lista Blanca correctamente ?';
    	rsTituloImpresionAnexo   = 'Impresi�n Anexo Contrato Servicio TAG Lista Blanca';
    	rsAvisoImpresionAnexo    = 'La impresi�n del contrato Servicio TAG Lista Blanca, lleva Anexo.';
        rsMensajeImpresionAnexo  = '� Se imprimi� el Anexo del Contrato del Servicio TAG Lista Blanca correctamente ?';
	var
        FormMedioEnvioMail: TFormMedioEnvioMail;
        EMailPersona: string;
        ClienteAdheridoEnvioEMail: Boolean;
        ReporteContratoAdhesionPAForm: TReporteContratoAdhesionPAForm;
        RBInterfaceConfig: TRBConfig;
        spValidarRestriccionesAdhesionAnexo: TADOStoredProc;			// SS_1006_PDO_20121122
        SituacionesARegularizar: AnsiString;							// SS_1006_PDO_20121122
        DocumentoImpreso: Boolean;
        RutaLogo: AnsiString;                                                   //SS_1147_NDR_20140710
        AuxRut: string;
    const
        cSQLObtenerEmailPersona                  = 'SELECT dbo.ObtenerEmailPersona(%d)';
        cSQLValidarConveniosPersonaEnvioPorEMail = 'SELECT dbo.ValidarConveniosPersonaEnvioPorEMail(%d)';
begin
    try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
          ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
          try                                                                                                 //SS_1147_NDR_20140710
            ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
          except                                                                                              //SS_1147_NDR_20140710
            On E: Exception do begin                                                                          //SS_1147_NDR_20140710
              Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
              MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
              Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
            end;                                                                                              //SS_1147_NDR_20140710
          end;                                                                                                //SS_1147_NDR_20140710

        	// SS_1006_PDO_20121122 Inicio Bloque
          SituacionesARegularizar := EmptyStr;

        	spValidarRestriccionesAdhesionAnexo := TADOStoredProc.Create(nil);
            with spValidarRestriccionesAdhesionAnexo do begin
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'ValidarRestriccionesAdhesionListaBlancaAnexo';
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
                Parameters.ParamByName('@DevolverValidaciones').Value := 1;
                Open;

                if RecordCount > 0 then begin
                	while not Eof do begin

                        if SituacionesARegularizar <> EmptyStr then begin
                        	SituacionesARegularizar := SituacionesARegularizar + '\par \par ';
                        end;

                        SituacionesARegularizar := SituacionesARegularizar + ' \tab ' + '- ' + AnsiUpperCase(FieldByName('Descripcion').AsString);

                        Next;
                    end;
                end;
            end;
            // SS_1006_PDO_20121122 Fin Bloque

            EMailPersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerEmailPersona, [FCodigoPersona]));
            ClienteAdheridoEnvioEMail := QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarConveniosPersonaEnvioPorEMail, [FCodigoPersona]));

	        FormMedioEnvioMail := TFormMedioEnvioMail.Create(nil);

            with FormMedioEnvioMail do begin
            	Inicializar(EMailPersona, ClienteAdheridoEnvioEMail);

	        	if ShowModal = mrOk then begin
	                if EMailPersona <> Trim(txtEmail.Text) then begin
                    	ActualizaEMailPersona(Trim(txtEmail.Text), FCodigoPersona);
                        EMailPersona := Trim(txtEmail.Text);
                    end;
                    if (not ClienteAdheridoEnvioEMail) and chkNotaCobroPorEMail.Checked then begin
                    	ActualizarConveniosPersonaEnvioPorEMail(FCodigoPersona);
                        ClienteAdheridoEnvioEMail := True;
                    end;

                    //INICIO: TASK_034_ECA_20160611
            		{*AgregarAuditoria_Adhesion(
                    	Trim(peNumeroDocumento.Text),
                        Trim(edtPatente.Text),
                        EMailPersona,
                        FCodigoPersona,
                        1,
                        IIf(ClienteAdheridoEnvioEMail, 1, 0),
                        -1,
                        -1,
                        FCodigoAuditoria); *}
                    //FIN: TASK_034_ECA_20160611
            	end;
            end;

            if SituacionesARegularizar <> EmptyStr then begin
                ShowMsgBoxCN(rsTituloImpresion, rsAvisoImpresionAnexo, MB_ICONINFORMATION, Self);
            end;

            //INICIO: TASK_013_ECA_20160520
            AuxRut:=StringReplace(lblRUT.Caption, '.', '',[rfReplaceAll, rfIgnoreCase]);
            AuxRut:=Trim(StringReplace(AuxRut, '-', '',[rfReplaceAll, rfIgnoreCase]));
            //FIN: TASK_013_ECA_20160520

            ReporteContratoAdhesionPAForm := TReporteContratoAdhesionPAForm.Create(nil);

            with ReporteContratoAdhesionPAForm do  begin
                Inicializar(
                	lblNombreCli.Caption,
                    lblNombreCli.Caption,
                    IIf(peNumeroDocumento.Text='',AuxRut,peNumeroDocumento.Text),
                    EMailPersona,
                    FFechaHoy,
                    ClienteAdheridoEnvioEMail,
                    True,
                    False,							// SS_1006_PDO_20121122
                    SituacionesARegularizar);		// SS_1006_PDO_20121122

                RBInterfaceConfig := RBInterface.GetConfig;

                with RBInterfaceConfig do begin
	                Copies       := 2;
                    MarginTop    := 0;
                    MarginLeft   := 0;
                    MarginRight  := 0;
                    MarginBottom := 0;
                    Orientation  := poPortrait;
	                {$IFDEF DESARROLLO or TESTKTC}
                    	PaperName    := 'Letter';
                    {$ELSE}
                    	PaperName    := 'Carta';
                    {$ENDIF}
                    ShowUser     := False;
                    ShowDateTime := False;
                end;

                RBInterface.SetConfig(RBInterfaceConfig);

                ReporteContratoAdhesionPAForm.ConfigurarImpresionContrato(True, False);

                DocumentoImpreso := RBInterface.Execute(True);

                if DocumentoImpreso then begin
                    //INICIO: TASK_034_ECA_20160611
            		{*AgregarAuditoria_Adhesion(
                    	Trim(peNumeroDocumento.Text),
                        Trim(edtPatente.Text),
                        EmptyStr,
                        FCodigoPersona,
                        1,
                        -1,
                        1,
                        -1,
                        FCodigoAuditoria);  *}
                    //FIN: TASK_034_ECA_20160611
                    while DocumentoImpreso and (ShowMsgBoxCN(rsTituloImpresion, rsMensajeImpresion, MB_ICONQUESTION + MB_YESNO, Self) <> mrOk) do begin
	                   DocumentoImpreso := RBInterface.Execute(True);
                    end;

                    if DocumentoImpreso then begin

                        if SituacionesARegularizar <> EmptyStr then begin
                            ReporteContratoAdhesionPAForm.ConfigurarImpresionContrato(False, True);
                            DocumentoImpreso := RBInterface.Execute(True);

                            while DocumentoImpreso and (ShowMsgBoxCN(rsTituloImpresionAnexo, rsMensajeImpresionAnexo, MB_ICONQUESTION + MB_YESNO, Self) <> mrOk) do begin
                               DocumentoImpreso := RBInterface.Execute(True);
                            end;
                        end;

                        if DocumentoImpreso then begin

                            if ShowMsgBoxCN(rsTituloActivacion, rsMensajeActivacion, MB_ICONQUESTION + MB_YESNO, Self) = mrOk then begin

                                AdherirClienteTAGListaBlanca(FCodigoPersona, EMailPersona);
                                //INICIO: TASK_034_ECA_20160611
                                {*AgregarAuditoria_Adhesion(
                                    Trim(peNumeroDocumento.Text),
                                    Trim(edtPatente.Text),
                                    EmptyStr,
                                    FCodigoPersona,
                                    1,
                                    -1,
                                    1,
                                    1,
                                    FCodigoAuditoria);  *}
                                //FIN: TASK_034_ECA_20160611
                                ShowMsgBoxCN(rsTituloActivacion, rsMensajeActivado, MB_ICONINFORMATION, Self);
                                Limpiar(nil);
                            end
                            //INICIO: TASK_034_ECA_20160611
                            {*else begin
                                AgregarAuditoria_Adhesion(
                                    Trim(peNumeroDocumento.Text),
                                    Trim(edtPatente.Text),
                                    EmptyStr,
                                    FCodigoPersona,
                                    1,
                                    -1,
                                    1,
                                    0,
                                    FCodigoAuditoria);
                            end; *}
                            //FIN: TASK_034_ECA_20160611 
                        end;
                    end;
                end;
                //INICIO: TASK_034_ECA_20160611
                {*if not DocumentoImpreso then begin
            	   	AgregarAuditoria_Adhesion(
                    	Trim(peNumeroDocumento.Text),
                        Trim(edtPatente.Text),
                        EmptyStr,
                        FCodigoPersona,
                        1,
                        -1,
                        0,
                        -1,
                        FCodigoAuditoria);
                end; *}
                //FIN: TASK_034_ECA_20160611
            end;
        except
        	on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	if Assigned(FormMedioEnvioMail) then FreeAndNil(FormMedioEnvioMail);
    	if Assigned(ReporteContratoAdhesionPAForm) then FreeAndNil(ReporteContratoAdhesionPAForm);
        // SS_1006_PDO_20121122 Inicio Bloque
        if Assigned(spValidarRestriccionesAdhesionAnexo) then begin
        	if spValidarRestriccionesAdhesionAnexo.Active then spValidarRestriccionesAdhesionAnexo.Close;
            FreeAndNil(spValidarRestriccionesAdhesionAnexo);
        end;
        // SS_1006_PDO_20121122 Fin BLoque
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConsultaAltaOtraConsecionaria.btnImprimirImpedimentosClick(Sender: TObject);
begin
	RBInterface.Execute(True);
end;

procedure TFormConsultaAltaOtraConsecionaria.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

//INICIO: TASK_034_ECA_20160611
procedure TFormConsultaAltaOtraConsecionaria.btn_FiltrarClick(Sender: TObject);
var
    Validar: Boolean;
begin
    btnExclusionVehiculos.Enabled := False;
    btnSuscribir.Enabled          := False;

    Validar:= True;
    if TipoSuscripcion<>2 then
       Validar:= ValidarFiltros;

    if Validar then begin
        if FCodigoPersona <= 0 then begin
            BuscarCliente;
        end;

        if FCodigoPersona > 0 then begin
            if ValidarCliente then begin

                if not chkDesuscribir.Checked then
                    ValidarRestriccionesAdhesionListaBlanca
                else
                begin
                   //btnExclusionVehiculos.Enabled := True;                     //TASK_037_ECA_20160622
                   if cdsCuentasPersona.RecordCount>0  then
                   begin
                      btnSuscribir.Enabled := True;
                      btnExclusionVehiculos.Enabled := True;                     //TASK_037_ECA_20160622
                   end;
                end;
            end;
        end;
    end;
end;
//FIN: TASK_034_ECA_20160611

procedure TFormConsultaAltaOtraConsecionaria.edtPatenteChange(Sender: TObject);
begin
	if not FLimpiando then Limpiar(peNumeroDocumento);
end;

procedure TFormConsultaAltaOtraConsecionaria.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action:= caFree;
end;

procedure TFormConsultaAltaOtraConsecionaria.FormCreate(Sender: TObject);
begin
    FFormState := FFormState - [fsVisible];
end;

procedure TFormConsultaAltaOtraConsecionaria.FormDestroy(Sender: TObject);
begin
	FormConsultaAltaOtraConsecionaria := nil;
end;

procedure TFormConsultaAltaOtraConsecionaria.Limpiar(ObjetoALimpiar: TObject);
begin
	try
    	FLimpiando := True;

        FCodigoPersona   := -1;
        FCodigoAuditoria := -1;
        FPersoneria      := EmptyStr;
        FRUTPersona      := EmptyStr;

        lblRUT.Caption        := EmptyStr;
        lblNombreCli.Caption  := EmptyStr;
        lblPersoneria.Caption := EmptyStr;

        if not Assigned(ObjetoALimpiar) then begin
            peNumeroDocumento.Text := EmptyStr;
            edtPatente.Text        := EmptyStr;

            peNumeroDocumento.SetFocus;
        end
        else begin
            (ObjetoALimpiar as TCustomEdit).Text := EmptyStr;
        end;

        btnImprimirImpedimentos.Enabled := False;
        //btnImprimirContrato.Enabled     := False;                             //TASK_034_ECA_20160611
        btnExclusionVehiculos.Enabled   := False;                               //SS-1006-NDR-20120803
        btnSuscribir.Enabled            := False;                               //TASK_034_ECA_20160611

        if spValidarRestriccionesAdhesionListaBlanca.Active then spValidarRestriccionesAdhesionListaBlanca.Close;
        if cdsCuentasPersona.Active then cdsCuentasPersona.Close;				//SS-1006-NDR-20120803
    finally
    	FLimpiando := False;
    end;
end;

procedure TFormConsultaAltaOtraConsecionaria.peNumeroDocumentoButtonClick(Sender: TObject);
    var
        formBuscaClientes: TFormBuscaClientes;
begin
    try
        formBuscaClientes := TFormBuscaClientes.Create(nil);

        if formBuscaClientes.Inicializa(FUltimaBusqueda)then begin
            if (formBuscaClientes.ShowModal = mrok) then begin

                FUltimaBusqueda        := formBuscaClientes.UltimaBusqueda;
                peNumeroDocumento.Text := Trim(formBuscaClientes.Persona.NumeroDocumento);
                FCodigoPersona         := formBuscaClientes.Persona.CodigoPersona;
                FRUTPersona            := Trim(formBuscaClientes.Persona.NumeroDocumento);
                FPersoneria            := formBuscaClientes.Persona.Personeria;

                if FCodigoPersona <> 0 then begin
                	MostrarDatosCliente(FCodigoPersona, FRUTPersona, FPersoneria);
                    btn_FiltrarClick(nil);
                end;
            end;
        end;
    finally
    	if Assigned(formBuscaClientes) then FreeAndNil(formBuscaClientes);
    end;
end;

procedure TFormConsultaAltaOtraConsecionaria.peNumeroDocumentoChange(Sender: TObject);
begin
	if not FLimpiando then Limpiar(edtPatente);
end;

procedure TFormConsultaAltaOtraConsecionaria.peNumeroDocumentoKeyPress(Sender: TObject;var Key: Char);
begin
    if Key = #13 then btn_FiltrarClick(nil);
end;

procedure TFormConsultaAltaOtraConsecionaria.ppDBMemDescripcionPrint(Sender: TObject);
begin
  {  if spValidarRestriccionesAdhesionListaBlanca.FieldByName('Codigo').AsInteger = 0 then begin
    	ppDBMemDescripcion.Font.Style    := ppDBMemDescripcion.Font.Style + [fsBold];
        ppDBMemDescripcion.Transparent   := False;
    end
    else begin
    	ppDBMemDescripcion.Font.Style    := ppDBMemDescripcion.Font.Style - [fsBold];
        ppDBMemDescripcion.Transparent   := True;
    end;    }
end;

procedure TFormConsultaAltaOtraConsecionaria.ttprImpedimentosBeforePrint(Sender: TObject);
begin
    pplblRUT.Caption           := lblRUT.Caption;
    pplblNombreCliente.Caption := lblNombreCli.Caption;
end;

procedure TFormConsultaAltaOtraConsecionaria.btn_LimpiarClick(Sender: TObject);
begin
	Limpiar(nil);
end;

procedure TFormConsultaAltaOtraConsecionaria.MostrarDatosCliente(CodigoCliente: Integer; RUT, Personeria: string);
	const
      	cSQLObtenerNombrePersona  = 'SELECT dbo.ObtenerNombrePersona(%d)';
    	cSQLFormatearRutConPuntos = 'SELECT dbo.FormatearRutConPuntos(''%s'')';
begin
	lblRUT.Caption       := QueryGetValue(DMConnections.BaseCAC, Format(cSQLFormatearRutConPuntos, [RUT])) + '   ';
    lblNombreCli.Caption := QueryGetValue(DMConnections.BaseCAC, Format(cSQLObtenerNombrePersona, [CodigoCliente]));

    if Personeria = PERSONERIA_FISICA then begin
        lblPersoneria.Caption := ' - (Persona Natural)';
    end

    else begin
        if Personeria = PERSONERIA_JURIDICA then lblPersoneria.Caption := ' - (Persona Jur�dica)';
        if Personeria = PERSONERIA_AMBAS then lblPersoneria.Caption := ' - (Persona Natural y Jur�dica)';
    end;

    lblNombreCli.Left  := lblRUT.Left + lblRUT.Width;
    lblPersoneria.Left := lblNombreCli.Left + lblNombreCli.Width;
end;

function TFormConsultaAltaOtraConsecionaria.ValidarFiltros: Boolean;
	resourcestring
    	rsTituloValidacion = 'Validaci�n Filtros Consulta';
        rsErrorDatosConsulta = 'No ha ingresado datos para la consulta.';
        rsRUTNoValido = 'El RUT ingresado NO es v�lido.';
        rsPatenteNoValida = 'La Patente ingresada NO es v�lida.';
    const
        MaxArray = 3;
    var
        vControles  : Array [1..MaxArray] of TControl;
        vCondiciones: Array [1..MaxArray] of Boolean;
        vMensajes   : Array [1..MaxArray] of String;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);

        vControles[01] := peNumeroDocumento;
        vControles[02] := peNumeroDocumento;
        vControles[03] := edtPatente;

        vCondiciones[01] := (edtPatente.Text <> EmptyStr) or ((peNumeroDocumento.Text) <> EmptyStr);
        vCondiciones[02] := (peNumeroDocumento.Text = EmptyStr) or ValidarRUT(DMConnections.BaseCAC, peNumeroDocumento.Text);
        vCondiciones[03] := (edtPatente.Text = EmptyStr) or VerificarFormatoPatenteChilena(DMConnections.BaseCAC, edtPatente.Text);

        vMensajes[01] := rsErrorDatosConsulta;
        vMensajes[02] := rsRUTNoValido;
        vMensajes[03] := rsPatenteNoValida;

    	try
        	Result := ValidateControls(vControles, vCondiciones, rsTituloValidacion, vMensajes);
        except
            on e:exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConsultaAltaOtraConsecionaria.BuscarCliente;
	resourcestring
    	rsTituloMensaje = 'Busqueda Cliente';
        rsClienteNoencontrado = 'No se encontr� el Cliente por los filtros de b�squeda indicados.';
    const
    	cSQLObtenerRUTPorCuentaActiva = 'SELECT dbo.ObtenerRUTPorCuentaActiva(''%s'')';
    var
    	Persona: TDatosPersonales;
begin
    try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
        //INICIO: TASK_034_ECA_20160611
            if Trim(edtPatente.Text) <> EmptyStr then begin
                    FRUTPersona := Trim(QueryGetValue(DMConnections.BaseCAC, Format(cSQLObtenerRUTPorCuentaActiva, [Trim(edtPatente.Text)])));
                    TipoSuscripcion:= 3;
                end
            else begin
                if Trim(peNumeroDocumento.Text) <> EmptyStr then
                begin
                    FRUTPersona := Trim(peNumeroDocumento.Text);
                    TipoSuscripcion:= 1;
                end
                else
                    if FCodigoConvenio<>0 then
                    begin
                       FRUTPersona := Trim(QueryGetValue(DMConnections.BaseCAC,'SELECT dbo.ObtenerNumeroDocumentoPorConvenio('+ IntToStr(FCodigoConvenio) + ')'));
                       TipoSuscripcion:= 2;
                    end
            end;

            if FRUTPersona = EmptyStr then begin
                raise EWarningExceptionCN.Create(rsTituloMensaje, rsClienteNoencontrado);
            end;
            
        //FIN:TASK_034_ECA_20160611
            Persona := ObtenerDatosPersonales(DMConnections.BaseCAC, 'RUT', FRUTPersona);

            if Persona.CodigoPersona <> -1 then begin
            	FCodigoPersona := Persona.CodigoPersona;
                FPersoneria    := Persona.Personeria;

                MostrarDatosCliente(FCodigoPersona, FRUTPersona, FPersoneria);
            end
            else begin
            	raise EWarningExceptionCN.Create(rsTituloMensaje, rsClienteNoencontrado);
            end;
        except
        	on e: Exception do begin
                if not FSoloConsulta then begin
		            ShowMsgBoxCN(e, Self);
                end
                else raise;
            end;
        end;
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

//INICIO:  TASK_034_ECA_20160611
function TFormConsultaAltaOtraConsecionaria.ValidarCliente: Boolean;
	resourcestring
    	rsTituloMensaje          = 'Validaci�n Cliente';
//        rsClientePersonaJuridica = 'El Cliente NO puede ser Persona Jur�dica para adherise al servicio Arauco TAG.';
        rsClienteYaAdherido      = 'El Cliente ya est� suscrito a la Lista Blanca.'; //TASK_034_ECA_20160611
//        rsClienteYaInscrito      = 'El Cliente ya se inscribi� v�a WEB, para solicitar la adhesi�n al servicio Arauco TAG.'; //TASK_034_ECA_20160611
        rsConvenioSuscrito =  'El Convenio ya est� suscrito a la Lista Blanca.';
        rsCuentaSuscrita   =  'La Cuenta ya est� suscrita a la Lista Blanca.';
//	const                                                                                            //TASK_034_ECA_20160611
//    	cSQLValidarPersonaAdheridaListaBlanca = 'SELECT dbo.ValidarPersonaAdheridaListaBlanca(%d)'; //TASK_034_ECA_20160611  TASK_034_ECA_20160611
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
        Result := False;
    	try
//INICIO: TASK_034_ECA_20160611
            if TipoSuscripcion=1 then
            begin
                if not chkDesuscribir.Checked then
                begin
                    if ValidarPersonaSuscrita(FCodigoPersona) then
                       raise EInfoExceptionCN.Create(rsTituloMensaje, rsClienteYaAdherido);
                    CargarCuentas(FCodigoPersona,0,EmptyStr)
                end
                else
                begin
                    if not ValidarPersonaSuscrita(FCodigoPersona) then
                        raise EInfoExceptionCN.Create(rsTituloMensaje, 'El Cliente no est� Suscrito a la lista Blanca');
                    CargarCuentasSuscritas(FCodigoPersona,0,EmptyStr);
                end;
            end
            else
            begin
                if TipoSuscripcion=2 then
                begin
                    if not chkDesuscribir.Checked then
                    begin
                      if ValidarConvenioSuscrito(FCodigoConvenio) then
                        raise EInfoExceptionCN.Create(rsTituloMensaje, rsConvenioSuscrito);
                      CargarCuentas(FCodigoPersona,FCodigoConvenio,EmptyStr)
                    end
                    else
                    begin
                        if not ValidarConvenioSuscrito(FCodigoConvenio) then
                            raise EInfoExceptionCN.Create(rsTituloMensaje, 'El Convenio no est� Suscrito a la lista Blanca');
                        CargarCuentasSuscritas(FCodigoPersona,FCodigoConvenio,EmptyStr);
                    end;
                end
                else
                begin
                    if not chkDesuscribir.Checked then
                    begin
                        ObtenerDatosVehiculo(FRUTPersona,edtPatente.Text,FCodigoConvenio,FIndiceVehiculo);

                        If (FCodigoConvenio <= 0) OR (FIndiceVehiculo <= 0) then
                                raise EInfoExceptionCN.Create(rsTituloMensaje, 'La Cuenta no existe');

                        if ValidarCuentaSuscrita(FCodigoConvenio,FIndiceVehiculo) then
                                raise EInfoExceptionCN.Create(rsTituloMensaje, rsCuentaSuscrita);

                        CargarCuentas(FCodigoPersona,FCodigoConvenio,edtPatente.Text);
                    end
                    else
                    begin
                       ObtenerDatosVehiculo(FRUTPersona,edtPatente.Text,FCodigoConvenio,FIndiceVehiculo);

                       If (FCodigoConvenio <= 0) OR (FIndiceVehiculo <= 0) then
                                raise EInfoExceptionCN.Create(rsTituloMensaje, 'La Cuenta no existe');

                       if not ValidarCuentaSuscrita(FCodigoConvenio,FIndiceVehiculo) then
                            raise EInfoExceptionCN.Create(rsTituloMensaje, 'La Cuenta no est� Suscrita a la lista Blanca');

                       CargarCuentasSuscritas(FCodigoPersona,0,edtPatente.Text);
                    end;
                end;
            end;
//            if QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarPersonaAdheridaListaBlanca, [FCodigoPersona])) then begin //TASK_034_ECA_20160611
//            	if not FSoloConsulta then begin
//                    AgregarAuditoria_Adhesion(
//                        Trim(peNumeroDocumento.Text),
//                        Trim(edtPatente.Text),
//                        EmptyStr,
//                        FCodigoPersona,
//                        2,
//                        -1,
//                        -1,
//                        -1,
//                        FCodigoAuditoria);
//                end;
//
//            	raise EInfoExceptionCN.Create(rsTituloMensaje, rsClienteYaAdherido);
//            end;
//FIN: TASK_034_ECA_20160611
            Result := True;
        except
        	on e: Exception do begin
            	if not FSoloConsulta then begin
		            ShowMsgBoxCN(e, Self);
                end
                else raise;
            end;
        end;
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
        //nada
    end;
end;
//FIN: TASK_034_ECA_20160611

procedure TFormConsultaAltaOtraConsecionaria.ValidarRestriccionesAdhesionListaBlanca;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
//INICIO: TASK_049_ECA_20160706
//INICIO : TASK_030_JMA_20160704
//INICIO: TASK_034_ECA_20160611
    	try
            with spValidarRestriccionesAdhesionListaBlanca do begin
            	if Active then Close;

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
                Parameters.ParamByName('@ConDetalle').Value := 1;
                Open;

                if Parameters.ParamByName('@PuedeAdherirse').Value = 0 then begin
                	if not FSoloConsulta then begin

                       { AgregarAuditoria_Adhesion(
                            Trim(peNumeroDocumento.Text),
                            Trim(edtPatente.Text),
                            EmptyStr,
                            FCodigoPersona,
                            0,
                            -1,
                            -1,
                            -1,
                            FCodigoAuditoria); }
	                    btnImprimirImpedimentos.Enabled := True;
                    end;
                end
                else begin
                    btnExclusionVehiculos.Enabled := True; //TASK_034_ECA_20160611

                    if cdsCuentasPersona.RecordCount>0  then
                        btnSuscribir.Enabled := True; //TASK_034_ECA_20160611
//					if not FSoloConsulta then begin
//                        AgregarAuditoria_Adhesion(
//                            Trim(peNumeroDocumento.Text),
//                            Trim(edtPatente.Text),
//                            EmptyStr,
//                            FCodigoPersona,
//                            1,
//                            -1,
//                            -1,
//                            -1,
//                            FCodigoAuditoria);
//
//                        //btnExclusionVehiculos.Enabled := True;                  //SS-1006-NDR-20120803
//	                	//btnImprimirContrato.Enabled := True;                  //TASK_034_ECA_20160611
//                    end;
                end;
            end;
        except
        	on e: Exception do begin
            	if not FSoloConsulta then begin
		            ShowMsgBoxCN(e, Self);
                end
                else raise;
            end;
        end;
//FIN: TASK_034_ECA_20160611

{        try

            if spValidarRestriccionesAdhesionListaBlanca.Active then
                spValidarRestriccionesAdhesionListaBlanca.Close;

            spValidarRestriccionesAdhesionListaBlanca.Parameters.Refresh;
            spValidarRestriccionesAdhesionListaBlanca.Parameters.ParamByName('@CodigoConvenio').Value := null;
            spValidarRestriccionesAdhesionListaBlanca.Parameters.ParamByName('@RUT').Value := peNumeroDocumento.Text;
            spValidarRestriccionesAdhesionListaBlanca.ExecProc;

            if spValidarRestriccionesAdhesionListaBlanca.Parameters.ParamByName('@RETURN_VALUE').Value = 1 then begin
                if not FSoloConsulta then begin
                    btnImprimirImpedimentos.Enabled := True;
                end;
            end
            else begin
                btnExclusionVehiculos.Enabled := True;
                if cdsCuentasPersona.RecordCount>0  then
                begin
                  //  btnSuscribir.Enabled := True;           //TASK_049_ECA_20160706
                    btnExclusionVehiculos.Enabled := True;
                end;
            end;
            btnExclusionVehiculos.Enabled := True;
        except
        	on e: Exception do begin
            	if not FSoloConsulta then begin
		            ShowMsgBoxCN(e, Self);
                end
                else raise;
            end;
        end;    }
{TERMINO: TASK_030_JMA_20160704}
//FIN: TASK_049_ECA_20160706
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConsultaAltaOtraConsecionaria.dblInfraccionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
   { if spValidarRestriccionesAdhesionListaBlanca.FieldByName('Codigo').AsInteger = 0 then begin
        Sender.Canvas.Font.Style := Sender.Canvas.Font.Style + [fsBold];
    end;}
end;

procedure TFormConsultaAltaOtraConsecionaria.ActualizaEMailPersona(pEMail: string; pCodigoPersona: Integer);
	var
        CodigoMedioComunicacionEmailPersona: Integer;
        spActualizarMedioComunicacionPersona: TADOStoredProc;
	const
        cSQLObtenerCodigoMedioComunicacionEmailPersona = 'SELECT dbo.ObtenerCodigoMedioComunicacionEmailPersona(%d)';
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
            CodigoMedioComunicacionEmailPersona := QueryGetIntegerValue(DMConnections.BaseCAC, Format(cSQLObtenerCodigoMedioComunicacionEmailPersona, [FCodigoPersona]));

            spActualizarMedioComunicacionPersona := TADOStoredProc.Create(nil);

            with spActualizarMedioComunicacionPersona do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'ActualizarMedioComunicacionPersona';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@CodigoTipoMedioContacto').Value := 1;
                	ParamByName('@CodigoArea').Value              := null;
                	ParamByName('@Valor').Value                   := pEMail;
                	ParamByName('@Anexo').Value                   := null;
                	ParamByName('@CodigoDomicilio').Value         := null;
                	ParamByName('@HorarioDesde').Value            := null;
                	ParamByName('@HorarioHasta').Value            := null;
                	ParamByName('@Observaciones').Value           := null;
                	ParamByName('@CodigoPersona').Value           := pCodigoPersona;
                	ParamByName('@Principal').Value               := 0;
                	ParamByName('@EstadoVerificacion').Value      := null;
                	ParamByName('@CodigoMedioComunicacion').Value := IIf(CodigoMedioComunicacionEmailPersona = 0, null, CodigoMedioComunicacionEmailPersona);
                end;
            end;

            DMConnections.BaseCAC.BeginTrans;
            spActualizarMedioComunicacionPersona.ExecProc;
            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
        except
        	on e: Exception do begin
            	if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
            	raise EErrorExceptionCN.Create('procedure TFormConsultaAltaOtraConsecionaria.ActualizaEMailPersona',e.Message);
            end;
        end;
    finally
    	if Assigned(spActualizarMedioComunicacionPersona) then FreeAndNil(spActualizarMedioComunicacionPersona);
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConsultaAltaOtraConsecionaria.ActualizarConveniosPersonaEnvioPorEMail(pCodigoPersona: Integer);
	var
        spActualizarConveniosPersonaEnvioPorEMail: TADOStoredProc;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
            spActualizarConveniosPersonaEnvioPorEMail := TADOStoredProc.Create(nil);

            with spActualizarConveniosPersonaEnvioPorEMail do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'ActualizarConveniosPersonaEnvioPorEMail';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@CodigoPersona').Value := pCodigoPersona;
                	ParamByName('@Usuario').Value       := UsuarioSistema;
                end;
            end;

            DMConnections.BaseCAC.BeginTrans;
            spActualizarConveniosPersonaEnvioPorEMail.ExecProc;
            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
        except
        	on e: Exception do begin
            	if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
            	raise EErrorExceptionCN.Create('procedure TFormConsultaAltaOtraConsecionaria.ActualizarConveniosPersonaEnvioPorEMail',e.Message);
            end;
        end;
    finally
    	if Assigned(spActualizarConveniosPersonaEnvioPorEMail) then FreeAndNil(spActualizarConveniosPersonaEnvioPorEMail);
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConsultaAltaOtraConsecionaria.AdherirClienteTAGListaBlanca(pCodigoPersona: Integer; pEMail: string);
	resourcestring
    	rsParametroGeneralNOExiste = 'El Par�metro General "%s" NO Existe, o contiene un valor inv�lido.';
        rsErrorspActualizarConveniosClienteEnListaBlanca = 'Error al ejecutar el procedimiento spActualizarConveniosClienteEnListaBlanca. Error: %s.';
        // INICIO : 20160525 MGO
        QRY_TIPO_CORRESPONDENCIA_BIENVENIDA_FORANEO = 'SELECT dbo.CONST_TIPO_CORRESPONDENCIA_BIENVENIDA_FORANEO()';
        // TERMINO : 20160525 MGO
	const
        cSQLTemporal = 'UPDATE Personas SET AdheridoPA = 1 WHERE CodigoPersona = %d';
        cSQLObtenerPrimerCodigoConvenioPersona = 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona(%d, 1)';
        cFormatoParametros = '"%s"';
        { INICIO : 20160525 MGO
        COD_PLANTILLA_EMAIL_PAK = 'COD_PLANTILLA_EMAIL_PAK';
        COD_FIRMA_EMAIL_PAK = 'COD_FIRMA_EMAIL_PAK';
        } // TERMINO : 20160525 MGO
	var
        spActualizarConveniosClienteEnListaBlanca,
        { INICIO : 20160525 MGO
        spEMAIL_AgregarMensaje: TADOStoredProc;
        CodigoConvenio,
        CodigoPlantillaEMail,
        CodigoFirmaEMail: Integer;
        }
        spEMAIL_Correspondencia_Agregar: TADOStoredProc;
        CodigoConvenio,
        CodigoTipoCorrespondencia: Integer;
        // TERMINO : 20160525 MGO
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
        try
            spActualizarConveniosClienteEnListaBlanca := TADOStoredProc.Create(nil);
            { INICIO : 20160525 MGO
            spEMAIL_AgregarMensaje := TADOStoredProc.Create(nil);
            if not ObtenerParametroGeneral(DMConnections.BaseCAC, COD_PLANTILLA_EMAIL_PAK, CodigoPlantillaEMail) then begin
            	raise Exception.Create(Format(rsParametroGeneralNOExiste, [COD_PLANTILLA_EMAIL_PAK]));
            end;

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, COD_FIRMA_EMAIL_PAK, CodigoFirmaEMail) then begin
            	raise Exception.Create(Format(rsParametroGeneralNOExiste, [COD_FIRMA_EMAIL_PAK]));
            end;
            } // TERMINO : 20160525 MGO

        	CodigoConvenio := QueryGetIntegerValue(DMConnections.BaseCAC, Format(cSQLObtenerPrimerCodigoConvenioPersona, [pCodigoPersona]));

            with spActualizarConveniosClienteEnListaBlanca do begin
                Connection    := DMConnections.BaseCAC;
  //              ProcedureName := 'ActualizarConveniosClienteEnListaDeAcceso';
                ProcedureName := 'ActualizarConveniosClienteEnListaBlanca';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@CodigoPersona').Value := pCodigoPersona;
                	ParamByName('@Usuario').Value       := UsuarioSistema;
                	ParamByName('@MensajeError').Value  := EmptyStr;
                end;
            end;

            { INICIO : 20160525 MGO
            with spEMAIL_AgregarMensaje do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'EMAIL_AgregarMensaje';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@Email').Value 				   := pEMail;
                	ParamByName('@CodigoPlantilla').Value          := CodigoPlantillaEMail;
                	ParamByName('@CodigoFirma').Value  			   := CodigoFirmaEMail;
                	ParamByName('@CodigoConvenio').Value  		   := CodigoConvenio;
                	ParamByName('@Parametros').Value               := Format(cFormatoParametros, [lblNombreCli.Caption]);
                	ParamByName('@NumeroProcesoFacturacion').Value := null;
                end;
            end;
            }
            CodigoTipoCorrespondencia := QueryGetValueInt(DMConnections.BaseCAC, QRY_TIPO_CORRESPONDENCIA_BIENVENIDA_FORANEO);

			spEMAIL_Correspondencia_Agregar := TADOStoredProc.Create(nil);
            try
                with spEMAIL_Correspondencia_Agregar do begin
                    Connection := DMConnections.BaseCAC;
                    ProcedureName := 'EMAIL_Correspondencia_Agregar';
                    Parameters.Refresh;

                    with Parameters do begin
                        ParamByName('@CodigoTipoCorrespondencia').Value := CodigoTipoCorrespondencia;
                        ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                        ParamByName('@Parametros').Value := Null;
                        ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                        ParamByName('@ErrorDescription').Value := Null;
                    end;

                    ExecProc;
                end;
            finally
                spEMAIL_Correspondencia_Agregar.Free;
            end;
            // TERMINO : 20160525 MGO

          //  DMConnections.BaseCAC.BeginTrans;
			//BEGIN : SS-1006-NDR-20120803-------------------------------------------------
            if cdsCuentasPersona.Active then
            begin
                AdherirPorCuenta();
            end
            else
            begin
                with spActualizarConveniosClienteEnListaBlanca do begin
                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                        raise Exception.Create(Format(rsErrorspActualizarConveniosClienteEnListaBlanca, [Parameters.ParamByName('@MensajeError').Value]));
                    end;
                end;
			//END : SS-1006-NDR-20120803-------------------------------------------------

            end;

            { INICIO : 20160525 MGO
            if (pEMail <> EmptyStr) then begin
	            spEMAIL_AgregarMensaje.ExecProc;
            end;
            } // TERMINO : 20160525 MGO
            
           // if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
        except
        	on e: Exception do begin
            	if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
            	raise EErrorExceptionCN.Create('procedure TFormConsultaAltaOtraConsecionaria.AdherirClienteTAGListaBLanca',e.Message);
            end;
        end;
    finally
    	if Assigned(spActualizarConveniosClienteEnListaBlanca) then begin
        	FreeAndNil(spActualizarConveniosClienteEnListaBlanca);
        end;
    	{ INICIO : 20160525 MGO
    	if Assigned(spEMAIL_AgregarMensaje) then begin
        	FreeAndNil(spEMAIL_AgregarMensaje);
        end;
        } // TERMINO : 20160525 MGO
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

function TFormConsultaAltaOtraConsecionaria.AgregarAuditoria_Adhesion(
	pNumeroDocumento, pPatente, pEMail: string;
    pCodigoPersona, pCumpleRequisitos, pAdheridoEnvioEMail, pImpresionContrato, pFirmaContrato, pCodigoAuditoria: Integer): Integer;
	var
        spAgregarAuditoria_Adhesion: TADOStoredProc;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
            spAgregarAuditoria_Adhesion := TADOStoredProc.Create(nil);

            with spAgregarAuditoria_Adhesion do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'AgregarAuditoria_AdhesionListaBlanca';

                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@Usuario').Value                := UsuarioSistema;
                	ParamByName('@NumeroDocumento').Value        := IIf(pNumeroDocumento = EmptyStr, null, pNumeroDocumento);
                	ParamByName('@Patente').Value                := IIf(pPatente = EmptyStr, null, pPatente);
                	ParamByName('@CodigoPersona').Value          := pCodigoPersona;
                	ParamByName('@CumpleRequisitos').Value       := pCumpleRequisitos;
                	ParamByName('@EMail').Value        	         := IIf(pEMail = EmptyStr, null, pEMail);
                	ParamByName('@AdheridoEnvioEMail').Value     := IIf(pAdheridoEnvioEMail = -1, null, pAdheridoEnvioEMail);
                	ParamByName('@ImpresionContrato').Value      := IIf(pImpresionContrato = -1, null, pImpresionContrato);
                	ParamByName('@FirmaContrato').Value          := IIf(pFirmaContrato = -1, null, pFirmaContrato);
                	ParamByName('@AdhesionPreInscripcion').Value := null;
                	ParamByName('@IDCodigoAuditoria').Value      := IIf(pCodigoAuditoria = -1, null, pCodigoAuditoria);

	                ExecProc;

                    FCodigoAuditoria := ParamByName('@IDCodigoAuditoria').Value;
                end;
            end;
        except
        	on e: Exception do begin
            	raise EErrorExceptionCN.Create('procedure TFormConsultaAltaOtraConsecionaria.AgregarAuditoria_Adhesion', e.Message);
            end;
        end;
    finally
    	if Assigned(spAgregarAuditoria_Adhesion) then FreeAndNil(spAgregarAuditoria_Adhesion);
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

//BEGIN : SS-1006-NDR-20120803--------------------------------------------------
procedure TFormConsultaAltaOtraConsecionaria.AdherirPorCuenta;
resourcestring
    rsErrorspActualizarCuentaEnListaDeAcceso = 'Error al ejecutar el procedimiento spActualizarCuentaEnListaDeAcceso. Error: %s.';
    rsErrorspActualizarAdheridoPAConvenioPersona = 'Error al ejecutar el procedimiento spActualizarAdheridoPAConvenioPersona. Error: %s.';    //SS-1006-NDR-20120814
var
    CodigoConcesionaria: Integer;
    FechaAltaListaDeAcceso: TDateTime;
    spActualizarCuentaEnListaDeAcceso: TADOStoredProc;
    spActualizarAdheridoPAConvenioPersona : TADOStoredProc;                     //SS-1006-NDR-20120814
    CodigoConvenioActual: Integer;
begin
    FechaAltaListaDeAcceso  := NowBaseCN(DMConnections.BaseCAC);
    CodigoConcesionaria     := 0; //QueryGetTinyintValue(DMConnections.BaseCAC,'SELECT dbo.CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO()');

    try
        try
            spActualizarAdheridoPAConvenioPersona := TADOStoredProc.Create(nil);                            //SS-1006-NDR-20120814
            spActualizarAdheridoPAConvenioPersona.Connection := DMConnections.BaseCAC;                      //SS-1006-NDR-20120814
            spActualizarAdheridoPAConvenioPersona.ProcedureName := 'ActualizarAdheridoPAConvenioPersona';   //SS-1006-NDR-20120814

            spActualizarCuentaEnListaDeAcceso := TADOStoredProc.Create(nil);
            with spActualizarCuentaEnListaDeAcceso do
            begin
            //INICIO: TASK_013_ECA_20160520
                Connection := DMConnections.BaseCAC;
//                ProcedureName := 'ActualizarCuentaEnListaDeAcceso';    //TASK_001_ECA_20160304
                ProcedureName := 'ActualizarCuentaEnListaBlanca';        //TASK_001_ECA_20160304
                Parameters.Refresh;
                with Parameters do
                begin
                    ParamByName('@CodigoConcesionaria').Value   := CodigoConcesionaria;
                    ParamByName('@Usuario').Value               := UsuarioSistema;
                    ParamByName('@Fecha').Value                 := FechaAltaListaDeAcceso;
                    ParamByName('@TipoMovimiento').Value        := 'A';
                    ParamByName('@ActualizarBITAdherido').Value := 1;
                    with cdsCuentasPersona do
                    begin
                        First;
                        CodigoConvenioActual:=-1;
                        while not eof do
                        begin

                            if cdsCuentasPersona.FieldByName('Chequeado').AsBoolean then
                               ParamByName('@TipoMovimiento').Value := 'A'
                            else
                                ParamByName('@TipoMovimiento').Value:= 'B';

                            ParamByName('@CodigoConvenio').Value := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;
                            ParamByName('@IndiceVehiculo').Value := cdsCuentasPersona.FieldByName('IndiceVehiculo').AsInteger;
                            ParamByName('@MensajeError').Value :='';
                            ExecProc;
                            if Parameters.ParamByName('@MensajeError').Value <> '' then begin
                                raise Exception.Create(Format(rsErrorspActualizarCuentaEnListaDeAcceso, [Parameters.ParamByName('@MensajeError').Value]));
                            end;

                        //FIN: TASK_013_ECA_20160520
//BEGIN TASK_001_ECA_20160304
//                            else
//                            begin
//                                with spActualizarAdheridoPAConvenioPersona do                                                                       //SS-1006-NDR-20120814
//                                begin                                                                                                               //SS-1006-NDR-20120814
//                                    Close;                                                                                                          //SS-1006-NDR-20120814
//                                    Parameters.Refresh;                                                                                             //SS-1006-NDR-20120814
//                                    Parameters.ParamByName('@CodigoConvenio').Value := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;   //SS-1006-NDR-20120814
//                                    Parameters.ParamByName('@Usuario').Value := UsuarioSistema;                                                     //SS-1006-NDR-20120814
//                                    ExecProc;                                                                                                       //SS-1006-NDR-20120814
//                                    if Parameters.ParamByName('@MensajeError').Value <> '' then                                                     //SS-1006-NDR-20120814
//                                    begin                                                                                                           //SS-1006-NDR-20120814
//                                        raise Exception.Create( Format(rsErrorspActualizarAdheridoPAConvenioPersona,                                //SS-1006-NDR-20120814
//                                                                [Parameters.ParamByName('@MensajeError').Value])                                    //SS-1006-NDR-20120814
//                                                              );                                                                                    //SS-1006-NDR-20120814
//                                    end;                                                                                                            //SS-1006-NDR-20120814
//                                end;                                                                                                                //SS-1006-NDR-20120814
//                            end;
//END TASK_001_ECA_20160304
                            Next;
                        end;
                    end;
                end;
            end;
        except
            on e: Exception do begin
            	raise EErrorExceptionCN.Create('Error al dar de alta los veh�culos del cliente', e.Message);
            end;
        end;
    finally
    	if Assigned(spActualizarCuentaEnListaDeAcceso) then FreeAndNil(spActualizarCuentaEnListaDeAcceso);
    end;
end;
//END : SS-1006-NDR-20120803--------------------------------------------------

procedure TFormConsultaAltaOtraConsecionaria.cdsCuentasPersonaAfterOpen(DataSet: TDataSet);
begin
    cdsCuentasPersona.Tag := cdsCuentasPersona.RecordCount;
end;

procedure TFormConsultaAltaOtraConsecionaria.cdsCuentasPersonaAfterPost(DataSet: TDataSet);
begin
    if cdsCuentasPersonaChequeado.AsBoolean then begin
        cdsCuentasPersona.Tag := cdsCuentasPersona.Tag + 1;
    end
    else begin
        cdsCuentasPersona.Tag := cdsCuentasPersona.Tag - 1;
    end;

    //btnImprimirContrato.Enabled := cdsCuentasPersona.Tag > 0; //TASK_034_ECA_20160611
    btnSuscribir.Enabled := cdsCuentasPersona.Tag > 0; //TASK_034_ECA_20160611
end;

procedure TFormConsultaAltaOtraConsecionaria.chkDesuscribirClick(
  Sender: TObject);
begin
    if chkDesuscribir.Checked then
    begin
       btnSuscribir.Caption:='Desuscribir Lista Blanca';
       Self.Caption := 'Desuscripci�n Lista Blanca';
    end
    else
    begin
       btnSuscribir.Caption:='Suscribir Lista Blanca';
       Self.Caption := 'Suscripci�n Lista Blanca';
    end;

    if (Trim(peNumeroDocumento.Text) <> EmptyStr) or (Trim(edtPatente.Text) <> EmptyStr) then
        btn_FiltrarClick(Sender);

end;

//INICIO: TASK_013_ECA_20160520
procedure TFormConsultaAltaOtraConsecionaria.ValidarCuentasNoNativa(CodigoPersonas: Integer);
var
    sp: TADOStoredProc;
begin
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'CRM_CuentasPersonaNoNativa_SELECT';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersonas;
    sp.Open;
    if not sp.IsEmpty then
       btnExclusionVehiculos.Enabled := True;

    sp.Close;
    FreeAndNil(sp);
end;
//FIN: TASK_013_ECA_20160520

//INICIO: TASK_034_ECA_20160611
function TFormConsultaAltaOtraConsecionaria.Inicializar(CodigoConvenio: Integer; Suscribir: Boolean): Boolean;
begin
    try
    	Result := False;

        SalirPorMensaje:= True; //TASK_034_ECA_20160611

        FCodigoConvenio := CodigoConvenio;  //TASK_034_ECA_20160611

        FSoloConsulta    := False;
        FCodigoPersona   := -1;
        FCodigoAuditoria := -1;
        FPersoneria      := EmptyStr;
        FRUTPersona      := EmptyStr;
        FLimpiando       := False;

        lblRUT.Caption        := EmptyStr;
        lblNombreCli.Caption  := EmptyStr;
        lblPersoneria.Caption := EmptyStr;

        FFechaHoy := SysutilsCN.NowBaseCN(DMConnections.BaseCAC);

        TipoSuscripcion := 2;

        chkDesuscribir.Checked:= True;
        if Suscribir then
            chkDesuscribir.Checked:= False;
        chkDesuscribir.Enabled:= False;
        chkDesuscribirClick(nil);

        btn_FiltrarClick(nil);

        peNumeroDocumento.Enabled:= False;
        edtPatente.Enabled:= False;
        btn_Limpiar.Enabled:= False;
        btn_Filtrar.Enabled:= False;

        Result     := True;
    except
    	on e: Exception do begin
        	ShowMsgBoxCN(e, Self);
        end;
    end;
end;

function TFormConsultaAltaOtraConsecionaria.Inicializar(CodigoConvenio: Integer; Patente: string; Suscribir: Boolean): boolean;
begin
    try
    	Result := False;

        SalirPorMensaje:= True; //TASK_034_ECA_20160611

        FCodigoConvenio := CodigoConvenio;  //TASK_034_ECA_20160611

        FSoloConsulta    := False;
        FCodigoPersona   := -1;
        FCodigoAuditoria := -1;
        FPersoneria      := EmptyStr;
        FRUTPersona      := EmptyStr;
        FLimpiando       := False;

        lblRUT.Caption        := EmptyStr;
        lblNombreCli.Caption  := EmptyStr;
        lblPersoneria.Caption := EmptyStr;

        FFechaHoy := SysutilsCN.NowBaseCN(DMConnections.BaseCAC);

        TipoSuscripcion := 3;

        chkDesuscribir.Checked:= True;
        if Suscribir then
            chkDesuscribir.Checked:= False;
        chkDesuscribir.Enabled:= False;
        chkDesuscribirClick(nil);

        edtPatente.Text:= Patente;

        btn_FiltrarClick(nil);

        peNumeroDocumento.Enabled:= False;
        edtPatente.Enabled:= False;
        btn_Limpiar.Enabled:= False;
        btn_Filtrar.Enabled:= False;

        Result     := True;
    except
    	on e: Exception do begin
        	ShowMsgBoxCN(e, Self);
        end;
    end;
end;


procedure TFormConsultaAltaOtraConsecionaria.btnSuscribirClick(Sender: TObject);
resourcestring
    rsTituloActivacion  	 = 'Activaci�n Servicio TAG Lista Blanca';
    rsMensajeActivado   	 = 'El Servicio TAG Lista Blanca, se activ� correctamente para el Cliente.';
var
    FormMedioEnvioMail: TFormMedioEnvioMail;

const
    cSQLObtenerEmailPersona                  = 'SELECT dbo.ObtenerEmailPersona(%d)';
    cSQLValidarConveniosPersonaEnvioPorEMail = 'SELECT dbo.ValidarConveniosPersonaEnvioPorEMail(%d)';
begin

    if not chkDesuscribir.Checked then
    begin
        if MsgBox('Confirmar Suscripci�n', 'Suscripci�n Lista Blanca', MB_YESNO or MB_ICONWARNING) = IDYES then
        begin
            //INICIO: TASK_044_ECA_20160630
            EMailPersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerEmailPersona, [FCodigoPersona]));
            ClienteAdheridoEnvioEMail := QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarConveniosPersonaEnvioPorEMail, [FCodigoPersona]));

            FormMedioEnvioMail := TFormMedioEnvioMail.Create(nil);

            with FormMedioEnvioMail do begin
                Inicializar(EMailPersona, ClienteAdheridoEnvioEMail);

                if ShowModal = mrOk then begin
                  if EMailPersona <> Trim(txtEmail.Text) then begin
                       ActualizaEMailPersona(Trim(txtEmail.Text), FCodigoPersona);
                       EMailPersona := Trim(txtEmail.Text);
                  end;

                  if (not ClienteAdheridoEnvioEMail) and chkNotaCobroPorEMail.Checked then begin
                      ActualizarConveniosPersonaEnvioPorEMail(FCodigoPersona);
                      ClienteAdheridoEnvioEMail := True;
                  end;
                end;
            end;

            if Assigned(FormMedioEnvioMail) then FreeAndNil(FormMedioEnvioMail);
            //FIN: TASK_044_ECA_20160630

            if TipoSuscripcion=1 then
            begin
                SuscribirPersona(FCodigoPersona);
            end
            else
            begin
                if TipoSuscripcion=2 then
                    SuscribirConvenio(FCodigoConvenio)
                else
                    SuscribirCuenta(FCodigoConvenio,FIndiceVehiculo);
            end;
        end;
    end
    else
    begin
        if MsgBox('Confirmar Desuscripci�n', 'Desuscripci�n Lista Blanca', MB_YESNO or MB_ICONWARNING) = IDYES then
           if TipoSuscripcion=1 then
            begin
                DesuscribirPersona(FCodigoPersona);
            end
            else
            begin
                if TipoSuscripcion=2 then
                    DesuscribirConvenio(FCodigoConvenio)
                else
                    DesuscribirCuenta(FCodigoConvenio,FIndiceVehiculo);
            end;
    end;
    CambiarEstadoCursor(CURSOR_DEFECTO);
end;

function TFormConsultaAltaOtraConsecionaria.ValidarPersonaSuscrita(CodigoPersonas: Integer): Boolean;
resourcestring
    SQL_VALIDAR = 'SELECT dbo.ValidarPersonaSuscritaListaBlanca(%d)';
begin
   Result:=QueryGetBooleanValue(DMConnections.BaseCAC, Format(SQL_VALIDAR, [CodigoPersonas]));
end;

function TFormConsultaAltaOtraConsecionaria.ValidarConvenioSuscrito(CodigoConvenio: Integer): Boolean;
begin
   Result:=QueryGetBooleanValue(DMConnections.BaseCAC, 'SELECT dbo.ValidarConvenioSuscritoListaBlanca('+ IntToStr(CodigoConvenio) +')');
end;
function TFormConsultaAltaOtraConsecionaria.ValidarCuentaSuscrita(CodigoConvenio, IndiceVehiculo: Integer): Boolean;
begin
   Result:=QueryGetBooleanValue(DMConnections.BaseCAC, 'SELECT dbo.ValidarCuentaSuscritaListaBlanca('+ IntToStr(CodigoConvenio) + ',' + IntToStr(IndiceVehiculo) + ')');
end;

//ININIO: TASK_049_ECA_20160706
procedure TFormConsultaAltaOtraConsecionaria.BurcarDatosVehiculo(Patente: string; out Marca:string; out Modelo: string; out Anio:string; out TipoVehiculo: string);
var
    sp: TADOStoredProc;
begin
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'ObtenerDatosVehiculo';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@Patente').Value  := Patente;
    sp.Parameters.ParamByName('@CodigoTipoPatente').Value  := 'CHL';
    sp.Open;

    if not sp.Eof then
    begin
        Marca:=sp.FieldByName('DescripcionMarca').AsString;
        Modelo:=sp.FieldByName('Modelo').AsString;
        Anio:=sp.FieldByName('AnioVehiculo').AsString;
        TipoVehiculo:=sp.FieldByName('DescripcionTipoVehiculo').AsString;
    end;

    sp.Close;
    FreeAndNil(sp)
end;
//FIN: TASK_049_ECA_20160706

procedure TFormConsultaAltaOtraConsecionaria.CargarCuentas(CodigoPersonas, CodigoConvenio: Integer; Patente: string);
begin
  if cdsCuentasPersona.Active then
  begin
     cdsCuentasPersona.Close
  end;

    spCRM_CuentasPersonaNoNativa_SELECT.Close;
    spCRM_CuentasPersonaNoNativa_SELECT.Prepared := False;
    spCRM_CuentasPersonaNoNativa_SELECT.Parameters.Refresh;
    spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@CodigoPersona').Value:= CodigoPersonas;

    if CodigoConvenio>0 then
        spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@CodigoConvenio').Value   := CodigoConvenio
    else
        spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@CodigoConvenio').Value:= null;
    if Trim(Patente)<>EmptyStr then
        spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@Patente').Value   := Patente
    else
        spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@Patente').Value:= null;

    spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@Suscrito').Value:= 0;
    spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@ErrorDescription').Value   :=  null;
    spCRM_CuentasPersonaNoNativa_SELECT.Prepared := True;
    with cdsCuentasPersona do begin
        Params.Clear;
        Params.Add;
        Params[0].Name      := '@CodigoPersona';
        Params[0].ParamType := ptInput;
        Params[0].DataType  := ftInteger;
        Params[0].Value     := FCodigoPersona;

        Params.Add;
        Params[1].Name      := '@CodigoConvenio';
        Params[1].ParamType := ptInput;
        Params[1].DataType  := ftInteger;
        if CodigoConvenio>0 then
            Params[1].Value     := CodigoConvenio
        else
            Params[1].Value     := null;
        Params.Add;

        Params[2].Name      := '@Patente';
        Params[2].ParamType := ptInput;
        Params[2].DataType  := ftInteger;
        if Trim(Patente)<>EmptyStr then
            Params[2].Value     := Patente
        else
            Params[2].Value     := null;

        Params.Add;
        Params[3].Name      := '@Suscrito';
        Params[3].ParamType := ptInput;
        Params[3].DataType  := ftBoolean;
        Params[3].Value     := 0;

        Open;
    end;

  //end;
end;

procedure TFormConsultaAltaOtraConsecionaria.CargarCuentasSuscritas(CodigoPersonas, CodigoConvenio: Integer; Patente: string);
begin
    if cdsCuentasPersona.Active then
    begin
        cdsCuentasPersona.Close
    end;
    
    spCRM_CuentasPersonaNoNativa_SELECT.Close;
    spCRM_CuentasPersonaNoNativa_SELECT.Prepared := False;
    spCRM_CuentasPersonaNoNativa_SELECT.Parameters.Refresh;
    spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@CodigoPersona').Value:= CodigoPersonas;

    if CodigoConvenio>0 then
        spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@CodigoConvenio').Value   := CodigoConvenio
    else
        spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@CodigoConvenio').Value:= null;
    if Trim(Patente)<>EmptyStr then
        spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@Patente').Value   := Patente
    else
        spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@Patente').Value:= null;

    spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@Suscrito').Value:= 1;
    spCRM_CuentasPersonaNoNativa_SELECT.Parameters.ParamByName('@ErrorDescription').Value   :=  null;
    spCRM_CuentasPersonaNoNativa_SELECT.Prepared := True;
    with cdsCuentasPersona do begin
        Params.Clear;
        Params.Add;
        Params[0].Name      := '@CodigoPersona';
        Params[0].ParamType := ptInput;
        Params[0].DataType  := ftInteger;
        Params[0].Value     := FCodigoPersona;

        Params.Add;
        Params[1].Name      := '@CodigoConvenio';
        Params[1].ParamType := ptInput;
        Params[1].DataType  := ftInteger;
        if CodigoConvenio>0 then
            Params[1].Value     := CodigoConvenio
        else
            Params[1].Value     := null;
        Params.Add;

        Params[2].Name      := '@Patente';
        Params[2].ParamType := ptInput;
        Params[2].DataType  := ftInteger;
        if Trim(Patente)<>EmptyStr then
            Params[2].Value     := Patente
        else
            Params[2].Value     := null;

        Params.Add;
        Params[3].Name      := '@Suscrito';
        Params[3].ParamType := ptInput;
        Params[3].DataType  := ftBoolean;
        Params[3].Value     := 1;

        Open;
    end;

 // end;
end;

//Permite la suscripcion de los convenios de una persona
procedure TFormConsultaAltaOtraConsecionaria.SuscribirPersona(CodigoPersonas: Integer);
 function BuscarNumeroConvenio(CodigoConvenio: Integer): String;
 begin
    Result := QueryGetStringValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerNumeroConvenio(' + IntToStr(CodigoConvenio) +')' );
 end;

resourcestring
    Titulo = 'Suscripci�n de Persona a Lista Blanca';
    ErrorSuscribirPersona = 'Error al ejecutar el procedimiento CRM_PersonasAltaSuscripcion. Error: %s.';
var
    sp: TADOStoredProc;
    i,Resultado: Integer;
    frm :TformResultadoSuscripcionCuentas;
    Patente, MarcaModelo :string;
    Suscrito : Boolean;

begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        sp:= TADOStoredProc.Create(nil);
        sp.Connection:=DMConnections.BaseCAC;
        sp.ProcedureName := 'CRM_CuentasAltaSuscripcion';
        if cdsCuentasPersona.RecordCount>0 then
            cdsCuentasPersona.First;

        if cdsVehiculos.Active then
            cdsVehiculos.Close;

        cdsVehiculos.CreateDataSet;
        cdsVehiculos.Open;

        if cdsCuentasPersona.RecordCount >0 then
        begin
           cdsCuentasPersona.First;

        end;
        Suscrito:=False;

        PatentesSuscritas:=''; //TASK_036_ECA_20160621
        while not cdsCuentasPersona.eof do
        begin
            if cdsCuentasPersona.FieldByName('Chequeado').AsBoolean then
            Begin
                if FCodigoConvenio=0 then
                   FCodigoConvenio := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;

                sp.Parameters.Refresh;
                sp.Parameters.ParamByName('@CodigoConvenio').Value  := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;
                sp.Parameters.ParamByName('@IndiceVehiculo').Value  := cdsCuentasPersona.FieldByName('IndiceVehiculo').AsInteger;
                sp.Parameters.ParamByName('@Usuario').Value         := UsuarioSistema;
                sp.Parameters.ParamByName('@Mensaje').Value         := null;
                sp.ExecProc;

                Resultado:= Integer(sp.Parameters.ParamByName('@RETURN_VALUE').Value);
                with cdsVehiculos do begin
                    Append;
                    FieldByName('NumeroConvenio').AsString := BuscarNumeroConvenio(cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger);
                    FieldByName('CodigoConvenio').Value    := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;
                    FieldByName('IndiceVehiculo').Value    := cdsCuentasPersona.FieldByName('IndiceVehiculo').AsInteger;
                    FieldByName('Patente').Value           := cdsCuentasPersona.FieldByName('Patente').AsString;
                    FieldByName('MarcaModeloColor').Value  := cdsCuentasPersona.FieldByName('MarcaModeloColor').AsString;

                    if Resultado=0 then
                    begin
                        Suscrito:=True;
                        FieldByName('Observacion').Value    := 'Suscrito a la Lista Blanca';
                        PatentesSuscritas:= PatentesSuscritas + Trim(cdsCuentasPersona.FieldByName('Patente').AsString) + ';';    //TASK_036_ECA_20160621
                    end
                    else
                    begin
                        if Resultado<0 then
                            FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@ErrorDescription').Value
                        else
                        begin
                            FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@Mensaje').Value
                        end;
                    end;
                    Post;
                end;

            End;
            cdsCuentasPersona.Next;
        end;

        FreeAndNil(sp);

        CambiarEstadoCursor(CURSOR_DEFECTO);

        frm := TformResultadoSuscripcionCuentas.Create(nil);
        if frm.Inicializar(cdsVehiculos) then
        begin
            frm.ShowModal;
        end;

        cdsVehiculos.Close;

        if Suscrito then
        begin
            ImprimirDocumentos(False);
            Close;
        end;
    except
        on e: Exception do begin
                ShowMsgBoxCN(e, Self);
        end;
    end
end;

procedure TFormConsultaAltaOtraConsecionaria.SuscribirConvenio(CodigoConvenio: Integer);
 function BuscarNumeroConvenio(CodigoConvenio: Integer): String;
 begin
    Result := QueryGetStringValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerNumeroConvenio(' + IntToStr(CodigoConvenio) +')' );
 end;
resourcestring
    Titulo = 'Suscripci�n de Convenio a Lista Blanca';
    ErrorSuscribirPersona = 'Error al ejecutar el procedimiento CRM_CuentasAltaSuscripcion. Error: %s.';
var
    sp: TADOStoredProc;
    Resultado: Integer;
    frm :TformResultadoSuscripcionCuentas;
    Patente, MarcaModelo :string;
    Suscrito : Boolean;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        sp:= TADOStoredProc.Create(nil);
        sp.Connection:=DMConnections.BaseCAC;
        sp.ProcedureName := 'CRM_CuentasAltaSuscripcion';

        if cdsCuentasPersona.RecordCount>0 then
            cdsCuentasPersona.First;

        if cdsVehiculos.Active then
            cdsVehiculos.Close;

        cdsVehiculos.CreateDataSet;
        cdsVehiculos.Open;

        if cdsCuentasPersona.RecordCount >0 then
           cdsCuentasPersona.First;

        Suscrito:=False;

        PatentesSuscritas:=''; //TASK_036_ECA_20160621
        while not cdsCuentasPersona.eof do
        begin
            if cdsCuentasPersona.FieldByName('Chequeado').AsBoolean then
            Begin
                sp.Parameters.Refresh;
                sp.Parameters.ParamByName('@CodigoConvenio').Value  := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;
                sp.Parameters.ParamByName('@IndiceVehiculo').Value  := cdsCuentasPersona.FieldByName('IndiceVehiculo').AsInteger;
                sp.Parameters.ParamByName('@Usuario').Value         := UsuarioSistema;
                sp.Parameters.ParamByName('@Mensaje').Value         := null;
                sp.ExecProc;

                Resultado:= Integer(sp.Parameters.ParamByName('@RETURN_VALUE').Value);
                with cdsVehiculos do begin
                    Append;
                    FieldByName('NumeroConvenio').AsString := BuscarNumeroConvenio(CodigoConvenio);
                    FieldByName('CodigoConvenio').Value    := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;
                    FieldByName('IndiceVehiculo').Value    := cdsCuentasPersona.FieldByName('IndiceVehiculo').AsInteger;
                    FieldByName('Patente').Value           := cdsCuentasPersona.FieldByName('Patente').AsString;
                    FieldByName('MarcaModeloColor').Value  := cdsCuentasPersona.FieldByName('MarcaModeloColor').AsString;
                     
                    if Resultado=0 then
                    begin
                        Suscrito:=True;
                        FieldByName('Observacion').Value    := 'Suscrito a la Lista Blanca';
                        PatentesSuscritas:= PatentesSuscritas + Trim(cdsCuentasPersona.FieldByName('Patente').AsString) + ';';    //TASK_036_ECA_20160621
                    end
                    else
                    begin
                        if Resultado<0 then
                            FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@ErrorDescription').Value
                        else
                        begin
                            FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@Mensaje').Value
                        end;
                    end;
                    Post;
                end;
            End;
            cdsCuentasPersona.Next;
        end;

        FreeAndNil(sp);

        CambiarEstadoCursor(CURSOR_DEFECTO);

        frm := TformResultadoSuscripcionCuentas.Create(nil);
        if frm.Inicializar(cdsVehiculos) then
        begin
            frm.ShowModal;
        end;

        cdsVehiculos.Close;

        if Suscrito then
        begin
            ImprimirDocumentos(True);
            Close;
        end;
    except
        on e: Exception do begin
                ShowMsgBoxCN(e, Self);
        end;
    end
end;

procedure TFormConsultaAltaOtraConsecionaria.SuscribirCuenta(CodigoConvenio,IndiceVehiculo: Integer);
 function BuscarNumeroConvenio(CodigoConvenio: Integer): String;
 begin
    Result := QueryGetStringValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerNumeroConvenio(' + IntToStr(CodigoConvenio) +')' );
 end;
resourcestring
    Titulo = 'Suscripci�n de Cuenta a Lista Blanca';
    ErrorSuscribirPersona = 'Error al ejecutar el procedimiento CRM_CuentasAltaSuscripcion. Error: %s.';
var
    sp: TADOStoredProc;
    Resultado: Integer;
    frm :TformResultadoSuscripcionCuentas;
    Patente, MarcaModelo :string;
    Suscrito : Boolean;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        sp:= TADOStoredProc.Create(nil);
        sp.Connection:=DMConnections.BaseCAC;
        sp.ProcedureName := 'CRM_CuentasAltaSuscripcion';

        if cdsCuentasPersona.RecordCount>0 then
            cdsCuentasPersona.First;

        if cdsVehiculos.Active then
            cdsVehiculos.Close;

        cdsVehiculos.CreateDataSet;
        cdsVehiculos.Open;

        if cdsCuentasPersona.RecordCount >0 then
           cdsCuentasPersona.First;

        Suscrito:=False;

        PatentesSuscritas:=''; //TASK_036_ECA_20160621
        while not cdsCuentasPersona.eof do
        begin
            if cdsCuentasPersona.FieldByName('Chequeado').AsBoolean then
            Begin
                if FCodigoConvenio=0 then
                   FCodigoConvenio := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;

                sp.Parameters.Refresh;
                sp.Parameters.ParamByName('@CodigoConvenio').Value  := CodigoConvenio;
                sp.Parameters.ParamByName('@IndiceVehiculo').Value  := IndiceVehiculo;
                sp.Parameters.ParamByName('@Usuario').Value         := UsuarioSistema;
                sp.Parameters.ParamByName('@Mensaje').Value         := null;
                sp.ExecProc;

                Resultado:= Integer(sp.Parameters.ParamByName('@RETURN_VALUE').Value);
                with cdsVehiculos do begin
                    Append;
                    FieldByName('NumeroConvenio').AsString := BuscarNumeroConvenio(CodigoConvenio);
                    FieldByName('CodigoConvenio').Value    := CodigoConvenio;
                    FieldByName('IndiceVehiculo').Value    := IndiceVehiculo;
                    FieldByName('Patente').Value           := cdsCuentasPersona.FieldByName('Patente').AsString;
                    FieldByName('MarcaModeloColor').Value  := cdsCuentasPersona.FieldByName('MarcaModeloColor').AsString;
                     
                    if Resultado=0 then
                    begin
                        Suscrito:=True;
                        FieldByName('Observacion').Value    := 'Suscrito a la Lista Blanca';
                        PatentesSuscritas:= PatentesSuscritas + Trim(cdsCuentasPersona.FieldByName('Patente').AsString) + ';';  //TASK_036_ECA_20160621
                    end
                    else
                    begin
                        if Resultado<0 then
                            FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@ErrorDescription').Value
                        else
                        begin
                            FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@Mensaje').Value
                        end;
                    end;
                    Post;
                end;

            End;
            cdsCuentasPersona.Next;
        end;

        FreeAndNil(sp);

        CambiarEstadoCursor(CURSOR_DEFECTO);

        frm := TformResultadoSuscripcionCuentas.Create(nil);
        if frm.Inicializar(cdsVehiculos) then
        begin
            frm.ShowModal;
        end;

        cdsVehiculos.Close;

        if Suscrito then
        begin
            ImprimirDocumentos(False);
            Close;
        end;
    except
        on e: Exception do begin
                ShowMsgBoxCN(e, Self);
        end;
    end
end;

procedure TFormConsultaAltaOtraConsecionaria.DeSuscribirPersona(CodigoPersonas: Integer);
 function BuscarNumeroConvenio(CodigoConvenio: Integer): String;
 begin
    Result := QueryGetStringValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerNumeroConvenio(' + IntToStr(CodigoConvenio) +')' );
 end;

resourcestring
    Titulo = 'Desuscripci�n de Persona de la Lista Blanca';
    ErrorSuscribirPersona = 'Error al ejecutar el procedimiento CRM_CuentasBajaSuscripcion. Error: %s.';
var
    sp: TADOStoredProc;
    Resultado: Integer;
    frm :TformResultadoSuscripcionCuentas;
    Patente, MarcaModelo :string;
    DeSuscrito : Boolean;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        sp:= TADOStoredProc.Create(nil);
        sp.Connection:=DMConnections.BaseCAC;
        sp.ProcedureName := 'CRM_CuentasBajaSuscripcion';
        if cdsCuentasPersona.RecordCount>0 then
            cdsCuentasPersona.First;

        if cdsVehiculos.Active then
            cdsVehiculos.Close;

        cdsVehiculos.CreateDataSet;
        cdsVehiculos.Open;

        if cdsCuentasPersona.RecordCount >0 then
           cdsCuentasPersona.First;

        DeSuscrito:=False;

        while not cdsCuentasPersona.eof do
        begin
            if cdsCuentasPersona.FieldByName('Chequeado').AsBoolean then
            Begin
                sp.Parameters.Refresh;
                sp.Parameters.ParamByName('@CodigoConvenio').Value  := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;
                sp.Parameters.ParamByName('@IndiceVehiculo').Value  := cdsCuentasPersona.FieldByName('IndiceVehiculo').AsInteger;
                sp.Parameters.ParamByName('@Usuario').Value         := UsuarioSistema;
                sp.Parameters.ParamByName('@Mensaje').Value         := null;
                sp.ExecProc;

                Resultado:= Integer(sp.Parameters.ParamByName('@RETURN_VALUE').Value);
                with cdsVehiculos do begin
                    Append;
                    FieldByName('NumeroConvenio').AsString := BuscarNumeroConvenio(cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger);
                    FieldByName('CodigoConvenio').Value    := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;
                    FieldByName('IndiceVehiculo').Value    := cdsCuentasPersona.FieldByName('IndiceVehiculo').AsInteger;
                    FieldByName('Patente').Value           := cdsCuentasPersona.FieldByName('Patente').AsString;
                    FieldByName('MarcaModeloColor').Value  := cdsCuentasPersona.FieldByName('MarcaModeloColor').AsString;
                     
                    if Resultado=0 then
                    begin
                        DeSuscrito:=True;
                        FieldByName('Observacion').Value    := 'Desuscrito de la Lista Blanca';
                    end
                    else
                    begin
                        if Resultado<0 then
                            FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@ErrorDescription').Value
                        else
                        begin
                            FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@Mensaje').Value
                        end;
                    end;
                    Post;
                end;

            End;
            cdsCuentasPersona.Next;
        end;

        FreeAndNil(sp);

        CambiarEstadoCursor(CURSOR_DEFECTO);

        frm := TformResultadoSuscripcionCuentas.Create(nil);
        if frm.Inicializar(cdsVehiculos) then
        begin
            frm.ShowModal;
        end;

        cdsVehiculos.Close;

        if DeSuscrito then
        begin
            //ImprimirDocumentos;
            Close;
        end;
    except
        on e: Exception do begin
                ShowMsgBoxCN(e, Self);
        end;
    end
end;

procedure TFormConsultaAltaOtraConsecionaria.DeSuscribirConvenio(CodigoConvenio: Integer);
 function BuscarNumeroConvenio(CodigoConvenio: Integer): String;
 begin
    Result := QueryGetStringValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerNumeroConvenio(' + IntToStr(CodigoConvenio) +')' );
 end;
resourcestring
    Titulo = 'Desuscripci�n de Convenio de la Lista Blanca';
    ErrorSuscribirPersona = 'Error al ejecutar el procedimiento CRM_CuentasBajaSuscripcion. Error: %s.';
var
    sp: TADOStoredProc;
    Resultado: Integer;
    frm :TformResultadoSuscripcionCuentas;
    Patente, MarcaModelo :string;
    Desuscrito: Boolean;
begin
    try                                                                         // TASK_089_MGO_20161209
        try
            CambiarEstadoCursor(CURSOR_RELOJ);
            sp:= TADOStoredProc.Create(nil);
            sp.Connection:=DMConnections.BaseCAC;
            //sp.ProcedureName := 'CRM_CuentasBajaSuscripcion';                 // TASK_089_MGO_20161209
            sp.ProcedureName := 'LB_ConvenioBajaSuscripcion';                   // TASK_089_MGO_20161209

            if cdsCuentasPersona.RecordCount>0 then
                cdsCuentasPersona.First;

            if cdsVehiculos.Active then
                cdsVehiculos.Close;

            cdsVehiculos.CreateDataSet;
            cdsVehiculos.Open;

            if cdsCuentasPersona.RecordCount >0 then
               cdsCuentasPersona.First;

            Desuscrito:=False;

            // INICIO : TASK_089_MGO_20161209
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@CodigoConvenio').Value  := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;
            sp.Parameters.ParamByName('@Usuario').Value         := UsuarioSistema;
            sp.Parameters.ParamByName('@Mensaje').Value         := null;
            sp.ExecProc;
            Resultado := Integer(sp.Parameters.ParamByName('@RETURN_VALUE').Value);
            // FIN : TASK_089_MGO_20161209

            while not cdsCuentasPersona.eof do
            begin
                if cdsCuentasPersona.FieldByName('Chequeado').AsBoolean then
                Begin
                    { INICIO : TASK_089_MGO_20161209
                    sp.Parameters.Refresh;
                    sp.Parameters.ParamByName('@CodigoConvenio').Value  := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;
                    sp.Parameters.ParamByName('@IndiceVehiculo').Value  := cdsCuentasPersona.FieldByName('IndiceVehiculo').AsInteger;
                    sp.Parameters.ParamByName('@Usuario').Value         := UsuarioSistema;
                    sp.Parameters.ParamByName('@Mensaje').Value         := null;
                    sp.ExecProc;

                    Resultado:= Integer(sp.Parameters.ParamByName('@RETURN_VALUE').Value);
                    } // FIN : TASK_089_MGO_20161209
                    with cdsVehiculos do begin
                        Append;
                        FieldByName('NumeroConvenio').AsString := BuscarNumeroConvenio(CodigoConvenio);
                        FieldByName('CodigoConvenio').Value    := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;
                        FieldByName('IndiceVehiculo').Value    := cdsCuentasPersona.FieldByName('IndiceVehiculo').AsInteger;
                        FieldByName('Patente').Value           := cdsCuentasPersona.FieldByName('Patente').AsString;
                        FieldByName('MarcaModeloColor').Value  := cdsCuentasPersona.FieldByName('MarcaModeloColor').AsString;
                     
                        if Resultado=0 then
                        begin
                            Desuscrito:=True;
                            FieldByName('Observacion').Value    := 'Desuscrito de la Lista Blanca';
                        end
                        else
                        begin
                            if Resultado<0 then
                                FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@ErrorDescription').Value
                            else
                            begin
                                FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@Mensaje').Value
                            end;
                        end;
                        Post;
                    end;
                End;
                cdsCuentasPersona.Next;
            end;

            FreeAndNil(sp);

            CambiarEstadoCursor(CURSOR_DEFECTO);

            frm := TformResultadoSuscripcionCuentas.Create(nil);
            if frm.Inicializar(cdsVehiculos) then
            begin
                frm.ShowModal;
            end;

            cdsVehiculos.Close;

            if Desuscrito then
            begin
                //ImprimirDocumentos;
                Close;
            end;
        except
            on e: Exception do begin
                    ShowMsgBoxCN(e, Self);
            end;
        end     
    finally                                                                     // TASK_089_MGO_20161209
        sp.Free;                                                                // TASK_089_MGO_20161209
    end;                                                                        // TASK_089_MGO_20161209
end;

procedure TFormConsultaAltaOtraConsecionaria.DeSuscribirCuenta(CodigoConvenio,IndiceVehiculo: Integer);
 function BuscarNumeroConvenio(CodigoConvenio: Integer): String;
 begin
    Result := QueryGetStringValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerNumeroConvenio(' + IntToStr(CodigoConvenio) +')' );
 end;
resourcestring
    Titulo = 'Suscripcion de Cuenta de la Lista Blanca';
    ErrorSuscribirPersona = 'Error al ejecutar el procedimiento CRM_CuentasBajaSuscripcion. Error: %s.';
var
    sp: TADOStoredProc;
    Resultado: Integer;
    frm :TformResultadoSuscripcionCuentas;
    Patente, MarcaModelo :string;
    Desuscrito: Boolean;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        sp:= TADOStoredProc.Create(nil);
        sp.Connection:=DMConnections.BaseCAC;
        sp.ProcedureName := 'CRM_CuentasBajaSuscripcion';

        if cdsCuentasPersona.RecordCount>0 then
            cdsCuentasPersona.First;

        if cdsVehiculos.Active then
            cdsVehiculos.Close;

        cdsVehiculos.CreateDataSet;
        cdsVehiculos.Open;

        if cdsCuentasPersona.RecordCount >0 then
           cdsCuentasPersona.First;

        Desuscrito:=False;

        while not cdsCuentasPersona.eof do
        begin
            if cdsCuentasPersona.FieldByName('Chequeado').AsBoolean then
            Begin
                sp.Parameters.Refresh;
                sp.Parameters.ParamByName('@CodigoConvenio').Value  := CodigoConvenio;
                sp.Parameters.ParamByName('@IndiceVehiculo').Value  := IndiceVehiculo;
                sp.Parameters.ParamByName('@Usuario').Value         := UsuarioSistema;
                sp.Parameters.ParamByName('@Mensaje').Value         := null;
                sp.ExecProc;

                Resultado:= Integer(sp.Parameters.ParamByName('@RETURN_VALUE').Value);
                with cdsVehiculos do begin
                    Append;
                    FieldByName('NumeroConvenio').AsString := BuscarNumeroConvenio(CodigoConvenio);
                    FieldByName('CodigoConvenio').Value    := CodigoConvenio;
                    FieldByName('IndiceVehiculo').Value    := IndiceVehiculo;
                    FieldByName('Patente').Value           := cdsCuentasPersona.FieldByName('Patente').AsString;
                    FieldByName('MarcaModeloColor').Value  := cdsCuentasPersona.FieldByName('MarcaModeloColor').AsString;
                     
                    if Resultado=0 then
                    begin
                        Desuscrito:=True;
                        FieldByName('Observacion').Value    := 'Desuscrito de la Lista Blanca';
                    end
                    else
                    begin
                        if Resultado<0 then
                            FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@ErrorDescription').Value
                        else
                        begin
                            FieldByName('Observacion').Value:= sp.Parameters.ParamByName('@Mensaje').Value
                        end;
                    end;
                    Post;
                end;
            End;
            cdsCuentasPersona.Next;
        end;

        FreeAndNil(sp);

        CambiarEstadoCursor(CURSOR_DEFECTO);

        frm := TformResultadoSuscripcionCuentas.Create(nil);
        if frm.Inicializar(cdsVehiculos) then
        begin
            frm.ShowModal;
        end;

        cdsVehiculos.Close;

        if Desuscrito then
        begin
            //ImprimirDocumentos;
            Close;
        end;
    except
        on e: Exception do begin
                ShowMsgBoxCN(e, Self);
        end;
    end
end;

procedure TFormConsultaAltaOtraConsecionaria.ImprimirDocumentos(DireccionConvenio: Boolean);
resourcestring
    rsTituloActivacion  	 = 'Activaci�n Servicio TAG Lista Blanca';
    rsMensajeActivacion 	 = '� Firm� el Cliente todos los documentos del contrato del Servicio TAG Lista Blanca ?';
    rsMensajeActivado   	 = 'El Servicio TAG Lista Blanca, se activ� correctamente para el Cliente.';
    rsTituloImpresion   	 = 'Impresi�n Contrato Servicio TAG Lista Blanca';
    rsMensajeImpresion  	 = '� Se imprimi� el contrato del Servicio TAG Lista Blanca correctamente ?';
    rsTituloImpresionAnexo   = 'Impresi�n Anexo Contrato Servicio TAG Lista Blanca';
    rsAvisoImpresionAnexo    = 'La impresi�n del contrato Servicio TAG Lista Blanca, lleva Anexo.';
    rsMensajeImpresionAnexo  = '� Se imprimi� el Anexo del Contrato del Servicio TAG Lista Blanca correctamente ?';
var
    //FormMedioEnvioMail: TFormMedioEnvioMail; //TASK_044_ECA_20160630
    //EMailPersona: string;                    //TASK_044_ECA_20160630
    //ClienteAdheridoEnvioEMail: Boolean;      //TASK_044_ECA_20160630
    frm : TFormImprimirConvenio;
    FechaImpresion : TDateTime;
    i: Integer;
    ListaPatentes: TStringList;
    Cuentas: array of TCuentaVehiculo;
    Marca, Modelo, Anio, TipoVehiculo: string;
const
    cSQLObtenerEmailPersona                  = 'SELECT dbo.ObtenerEmailPersona(%d)';
    cSQLValidarConveniosPersonaEnvioPorEMail = 'SELECT dbo.ValidarConveniosPersonaEnvioPorEMail(%d)';
begin
    try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
            ListaPatentes:= TStringList.Create;
            ListaPatentes.Clear;
            ListaPatentes.Delimiter       := ';';
            ListaPatentes.StrictDelimiter := True;
            ListaPatentes.DelimitedText   := PatentesSuscritas;

            SetLength(Cuentas, ListaPatentes.Count - 1);
            for i := 0 to ListaPatentes.Count - 2 do
            begin
                Cuentas[i].Vehiculo.Patente:= ListaPatentes[i];

                BurcarDatosVehiculo(ListaPatentes[i],Marca, Modelo, Anio, TipoVehiculo);

                Cuentas[i].Vehiculo.Marca:= Marca;
                Cuentas[i].Vehiculo.Modelo:= Modelo;
                //Cuentas[i].Vehiculo.Tipo:= TipoVehiculo;  //TASK_106_JMA_20170206
            end;

            FechaImpresion := NowBase(DMConnections.BaseCAC);
            Application.CreateForm(TFormImprimirConvenio,frm);
            if not frm.ImprimirAltaSuscripcionListaBlanca(FCodigoConvenio,FCodigoPersona, lblRUT.Caption,lblNombreCli.Caption,
                                                        EMailPersona,Cuentas,1,ClienteAdheridoEnvioEMail,
                                                        DireccionConvenio,FechaImpresion)  then
            begin
                FreeAndNil(frm);
                Exit;
            end;
        except
        	on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
//    	if Assigned(FormMedioEnvioMail) then FreeAndNil(FormMedioEnvioMail);     //TASK_044_ECA_20160630

    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;
//FIN: TASK_034_ECA_20160611

//INICIO: TASK_037_ECA_20160622
procedure TFormConsultaAltaOtraConsecionaria.ObtenerDatosVehiculo(NumeroDocumento, Patente: string; out CodigoConvenio: Integer; out IndiceVehiculo: Integer);
 function ObtenerConvenioporPatente_Rut (NumeroDocumento, Patente: string): Integer;
 begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerConvenioporPatente_Rut(''' + Trim(Patente) +''',''' + Trim(NumeroDocumento) + ''')');
 end;

 function ObtenerIndiceVehiculoConvenio(CodigoConvenio: Integer; Patente: string): Integer;
 begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerIndiceVehiculoConvenio(' + IntToStr(CodigoConvenio) +',''CHL'',''' + Trim(Patente) + ''')');
 end;

begin
    CodigoConvenio:= ObtenerConvenioporPatente_Rut(NumeroDocumento,Patente);
    IndiceVehiculo:= ObtenerIndiceVehiculoConvenio(CodigoConvenio,Patente);
end;
//FIN: TASK_037_ECA_20160622
end.


