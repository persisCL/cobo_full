unit frmRegistrarLlamada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, VariantComboBox, ExtCtrls, Peaprocs, UtilProc, RStrings;

type
  TFormRegistrarLlamada = class(TForm)
    pnlBottom: TPanel;
    pnlMain: TPanel;
    cbEstadosResultados: TVariantComboBox;
    lblResultado: TLabel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    function GetCodigoEstadoResultado: integer;
  public
    property CodigoEstadoResultado: integer read GetCodigoEstadoResultado;
    function Inicializar(CodigoEstadoSolicitud: integer = -1; CodigoEstadoResultado: integer = -1): boolean;
  end;

var
  FormRegistrarLlamada: TFormRegistrarLlamada;

implementation

uses DMConnection;

{$R *.dfm}

{ TFormRegistrarLlamada }


{ TFormRegistrarLlamada }

function TFormRegistrarLlamada.GetCodigoEstadoResultado: integer;
begin
    result := cbEstadosResultados.Items[cbEstadosResultados.ItemIndex].Value;
end;

function TFormRegistrarLlamada.Inicializar(CodigoEstadoSolicitud: integer = -1;
CodigoEstadoResultado: integer = -1): boolean;
begin
    CargarResultadosEstados(DMConnections.BaseCAC, cbEstadosResultados, CodigoEstadoSolicitud, CodigoEstadoResultado, True);
    result:=True;
end;

procedure TFormRegistrarLlamada.btnAceptarClick(Sender: TObject);
begin
    if cbEstadosResultados.ItemIndex < 1 then begin
		MsgBoxBalloon(MSG_ERROR_RESULTADO, self.Caption, MB_ICONSTOP, cbEstadosResultados);
        Exit;
    end;
    ModalResult := mrOk;
end;

procedure TFormRegistrarLlamada.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

end.
