object FormGeneradorIni: TFormGeneradorIni
  Left = 350
  Top = 280
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Configurar Punto de Entrega'
  ClientHeight = 152
  ClientWidth = 361
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poNone
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 8
    Width = 411
    Height = 104
  end
  object Label1: TLabel
    Left = 7
    Top = 27
    Width = 104
    Height = 13
    Caption = 'Punto de &Entrega:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 7
    Top = 52
    Width = 93
    Height = 13
    Caption = 'Punto de &Venta:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 7
    Top = 76
    Width = 93
    Height = 13
    Caption = '&Fuente Solicitud'
    FocusControl = txt_FuenteSolicitud
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnAceptar: TDPSButton
    Left = 118
    Top = 119
    Height = 28
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TDPSButton
    Left = 198
    Top = 119
    Height = 28
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object txt_FuenteSolicitud: TNumericEdit
    Left = 146
    Top = 72
    Width = 121
    Height = 21
    Color = 16444382
    TabOrder = 0
    Decimals = 0
  end
  object cbPuntosEntrega: TVariantComboBox
    Left = 146
    Top = 22
    Width = 205
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 3
    Items = <>
  end
  object cpPuntosVentas: TVariantComboBox
    Left = 146
    Top = 47
    Width = 205
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 4
    Items = <>
  end
  object qryPuntosEntrega: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT CodigoPuntoEntrega, Descripcion'
      'FROM PUNTOSENTREGA')
    Left = 112
    Top = 16
  end
  object qryPuntosVentas: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoPuntoEntrega'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT CodigoPuntoVenta, Descripcion'
      'FROM PUNTOSVENTA'
      'WHERE CodigoPuntoEntrega = :CodigoPuntoEntrega')
    Left = 112
    Top = 47
  end
end
