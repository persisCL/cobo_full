object FormGenerarOrdenPedidoTAGs: TFormGenerarOrdenPedidoTAGs
  Left = 340
  Top = 217
  BorderIcons = [biSystemMenu, biHelp]
  BorderStyle = bsSingle
  Caption = 'Pedido de Reposici'#243'n de Telev'#237'as'
  ClientHeight = 273
  ClientWidth = 442
  Color = clBtnFace
  Constraints.MaxHeight = 307
  Constraints.MaxWidth = 450
  Constraints.MinHeight = 300
  Constraints.MinWidth = 450
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dbgPedidos: TDBGrid
    Left = 8
    Top = 8
    Width = 425
    Height = 233
    DataSource = dsPedido
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnKeyDown = dbgPedidosKeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'Descripcion'
        ReadOnly = True
        Title.Caption = 'Categor'#237'a'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = [fsBold]
        Width = 300
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Cantidad'
        Title.Alignment = taRightJustify
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = [fsBold]
        Width = 99
        Visible = True
      end>
  end
  object btnAceptar: TButton
    Left = 276
    Top = 246
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    TabOrder = 0
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 357
    Top = 246
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object btnReiniciar: TButton
    Left = 195
    Top = 246
    Width = 75
    Height = 25
    Caption = '&Reiniciar'
    TabOrder = 3
    OnClick = btnReiniciarClick
  end
  object spObtenerCantidadesPedidoTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCantidadesPedidoTAGs'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 158
      end>
    Left = 136
    Top = 104
  end
  object dsPedido: TDataSource
    DataSet = cdsPedido
    Left = 104
    Top = 72
  end
  object cdsPedido: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Categoria'
        DataType = ftSmallint
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Cantidad'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    BeforePost = cdsPedidoBeforePost
    Left = 136
    Top = 72
  end
  object spRegistrarPedidoTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarPedidoTAGs'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 136
    Top = 136
  end
  object spRegistrarDetallePedidoTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarDetallePedidoTAGs'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 136
    Top = 168
  end
end
