object FInfoPlanesComerciales: TFInfoPlanesComerciales
  Left = 299
  Top = 110
  BorderStyle = bsToolWindow
  Caption = 'Informaci'#243'n'
  ClientHeight = 521
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel25: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 37
    Align = alTop
    Color = 16776699
    TabOrder = 0
    object Label4: TLabel
      Left = 24
      Top = 8
      Width = 138
      Height = 16
      Caption = 'Planes comerciales'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cb_PlanesComerciales: TComboBox
      Left = 171
      Top = 5
      Width = 260
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cb_PlanesComercialesChange
    end
    object Salir: TDPSButton
      Left = 603
      Top = 6
      Caption = 'Salir'
      TabOrder = 1
      OnClick = SalirClick
    end
  end
  object REditPlanComercial: TRichEdit
    Left = 0
    Top = 37
    Width = 688
    Height = 484
    Align = alClient
    Lines.Strings = (
      '')
    ReadOnly = True
    TabOrder = 1
  end
  object qry_PlanComercial: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'PlanCom'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM PlanesComerciales'
      'WHERE PlanComercial = :PlanCom'
      ''
      '')
    Left = 384
    Top = 15
  end
  object qry_CategoriaPlan: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'PlanCom'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT'
      #9'A.*, B.Descripcion AS DescCategoria'
      'FROM'
      #9'PlanesComercialesCategorias A, Categorias B'
      'WHERE'
      #9'PlanComercial = :PlanCom AND'
      #9'A.Categoria = B.Categoria'
      '')
    Left = 414
    Top = 15
  end
  object qry_TipoPago: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'PlanCom'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT'
      #9'A.*, B.Descripcion AS DescTipoPago'
      'FROM'
      #9'PlanesComercialesTipoPago A, TipoPago B'
      'WHERE'
      #9'A.PlanComercial = :PlanCom AND'
      #9'A.TipoPago = B.TipoPago AND'
      #9'B.Habilitado = 1'
      '')
    Left = 444
    Top = 15
  end
  object qry_PlanesComPosibles: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'PlanCom'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'A.*, rtrim(B.Descripcion) as DescAdhesion, rtrim(C.Descripcion)' +
        ' AS DescCliente'
      'FROM'
      #9'PlanesComercialesPosibles A,TiposAdhesion B, TiposCliente C'
      'WHERE'
      #9'A.PlanComercial = :PlanCom AND'
      #9'A.TipoCliente   = C.TipoCliente AND'
      #9'A.TipoAdhesion  = B.TipoAdhesion'
      '')
    Left = 474
    Top = 15
  end
end
