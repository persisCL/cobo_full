unit DenunciaCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DeclTag, DeclHard, DeclCtl, Util, UtilProc, Peaprocs, DMConnection,
  DmiCtrls, ExtCtrls, DB, ADODB, UtilDB, BuscaClientes, Variants, FrmCliente,
  DPSControls, peatypes;

type
  TFormDenunciaCliente = class(TForm)
    groupbox: TGroupBox;
    lb_CodigoCliente: TLabel;
    peCodigoCliente: TPickEdit;
    cb_Cuentas: TComboBox;
    Label1: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    txt_observaciones: TMemo;
    cb_denuncia: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    lbl_TagDescripcion: TLabel;
    lbl_tag: TLabel;
    lApellidoNombre: TLabel;
    dsCuentasCliente: TDataSource;
    ObtenerCuentasCliente: TADOStoredProc;
    RegistrarDenuncia: TADOStoredProc;
    btn_Aceptar: TDPSButton;
    btn_Cancelar: TDPSButton;
	procedure btn_AceptarClick(Sender: TObject);
    procedure peCodigoClienteChange(Sender: TObject);
    procedure peCodigoClienteButtonClick(Sender: TObject);
    procedure cb_CuentasChange(Sender: TObject);
  private
	{ Private declarations }
    TipoDenuncia,
    Comunicacion: integer;
    Function CargarCliente: Boolean;
    procedure Limpiar;
    function PuedeGrabar: boolean;
    procedure Grabar;
  public
	{ Public declarations }
	Function Inicializa(Tipo: integer = -1;
                        Codigocliente: integer = 0;
                        CodigoComunicacion: integer = 0): Boolean;
  end;

var
  FormDenunciaCliente: TFormDenunciaCliente;

implementation

{$R *.DFM}

resourcestring
    CAPTION_DENUNCIA = 'Denuncia del cliente';

{ TForm1 }

function TFormDenunciaCliente.Inicializa(Tipo: integer = -1;
                                         Codigocliente: integer = 0;
                                         CodigoComunicacion: integer = 0): Boolean;
begin
	Screen.Cursor := crHourGlass;
    Screen.Cursor := crDefault;
    Limpiar;
    TipoDenuncia := Tipo;
    if Codigocliente <> 0 then
    begin
        peCodigoCliente.Value := Codigocliente;
        peCodigoCliente.OnChange(peCodigoCliente);
        peCodigoCliente.Enabled := false;
        Comunicacion := CodigoComunicacion;
        //if FindFormOrCreate(TNavWindowCliente, FCli) then
        ///begin
            //FCli.dg_cuentas.FieldByName
        //    FCli.ObtenerDetallesCuentasCliente.FieldByName('CodigoPersona').AsInteger;
        //end;
    end else peCodigoCliente.Enabled := true;
    CargarMotivosDenuncia(DMConnections.BaseCAC, cb_denuncia, TipoDenuncia);
	Result := True;
end;

procedure TFormDenunciaCliente.btn_AceptarClick(Sender: TObject);
begin
    if PuedeGrabar then Grabar;
end;

procedure TFormDenunciaCliente.peCodigoClienteChange(Sender: TObject);
begin
    Limpiar;
    CargarCliente;
    CargarCuentas(DMConnections.BaseCAC, cb_cuentas, Round(peCodigoCliente.Value));
                cb_cuentas.OnChange(self);
    //try
        //si esta vacio no hace nada
    cb_cuentas.ItemIndex := 0; cb_cuentas.OnChange(self);
    //except
    //end;


end;

function TFormDenunciaCliente.CargarCliente: Boolean;
begin
    lApellidoNombre.caption := '';
	lApellidoNombre.caption := QueryGetValue(DMConnections.BaseCAC,
	                            'Select dbo.ObtenerApellidoNombreCliente(' +
                                FloatToStr(peCodigoCliente.Value) + ')' );
    result := true;
end;

procedure TFormDenunciaCliente.peCodigoClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
   	Application.CreateForm(TFormBuscaClientes, f);
	if f.Inicializa('', '', '', '', '') and (f.ShowModal = mrOk) then
    begin
        peCodigoCliente.Cursor  := crHourGlass;
        peCodigoCliente.value   := f.Persona.CodigoPersona;
        peCodigoCliente.Cursor  := crDefault;
	end;
	f.Release;
end;

procedure TFormDenunciaCliente.Limpiar;
begin
    lApellidoNombre.Caption := '';
    lbl_TagDescripcion.Caption := '';
    txt_observaciones.Clear;
    cb_cuentas.Items.Clear;
end;

procedure TFormDenunciaCliente.cb_CuentasChange(Sender: TObject);
resourcestring
    MSG_ERROR_TAG = 'Ha ocurrido un error al traer el tag.';
 	MSG_NUEVA_UBICACION = 'Ubicación de TAG: %s';
begin
    try
        Screen.Cursor := crHourGlass;
        try
            if cb_cuentas.Items.Count = 0 then begin
                exit;
            end else begin
                cb_cuentas.Enabled   := true;
                if cb_Cuentas.Text <> '' then
                    lbl_tagDescripcion.caption := QueryGetValue(DMConnections.BaseCAC,
                            'select dbo.ObtenerNumeroTagAsString('+ Trim(StrRight(cb_cuentas.Text, 10)) + ')');
            end;
        except
            on e: Exception do begin
                Screen.Cursor := crDefault;
                MsgBoxErr(MSG_ERROR_TAG, e.Message, CAPTION_DENUNCIA, MB_ICONSTOP);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TFormDenunciaCliente.Grabar;
resourcestring
    MSG_ERROR_GRABAR = 'No se pudo registrar la denuncia.';
    MSG_OK_GRABAR = 'Datos de la denuncia registrados.';

var
    CodigoDenuncia, ContextMark: integer;
    ContractSerialNumber: AnsiString;
begin
    Screen.Cursor := crHourGlass;
    try
        CodigoDenuncia := 0; //Se de de alta y es devuelto por parámetro.
        ContextMark := QueryGetValueInt(DMConnections.BaseCAC, 'Select dbo.ObtenerContextMark(' +
        Trim(StrRight(cb_cuentas.Text, 10)) + ')');
        ContractSerialNumber := QueryGetValue(DMConnections.BaseCAC,'Select dbo.ObtenerContractSerialNumber(' +
        Trim(StrRight(cb_cuentas.Text, 10)) + ')');
        //Comunicacion
        DMConnections.BaseCAC.BeginTrans;
        with RegistrarDenuncia do begin
            //Parameters.ParamByName('@CodigoDenuncia').Value := CodigoDenuncia;
            Parameters.ParamByName('@MotivoDenuncia').value := Ival(Trim(Copy(cb_denuncia.Text,201,100)));
            Parameters.ParamByName('@CodigoCuenta').value := Ival(Trim(StrRight(cb_cuentas.Text, 10)));
            Parameters.ParamByName('@Observaciones').value := txt_observaciones.Text;
            Parameters.ParamByName('@ContextMark').value := ContextMark;
            Parameters.ParamByName('@ContractSerialNumber').value := QueryGetValue(DMConnections.BaseCAC,'Select dbo.ObtenerContractSerialNumber(' +
                                    Trim(StrRight(cb_cuentas.Text, 10)) + ')');
            Parameters.ParamByName('@CodigoComunicacion').Value := iif((Comunicacion = 0),null, Comunicacion);
            
            ExecProc;

            CodigoDenuncia := Parameters.ParamByName('@CodigoDenuncia').Value;
            
            MsgBox(MSG_OK_GRABAR, CAPTION_DENUNCIA, MB_ICONINFORMATION);
            if (CodigoDenuncia > 0) then ModalResult := mrOk;
            Close;
        end;
        DMConnections.BaseCAC.CommitTrans;
    except
        on e: exception do
        begin
            DMConnections.BaseCAC.RollbackTrans;
            MsgBoxErr(MSG_ERROR_GRABAR, e.Message, CAPTION_DENUNCIA, MB_ICONSTOP);
        end;
    end;
    Screen.Cursor := crDefault;
end;

function TFormDenunciaCliente.PuedeGrabar: boolean;
resourcestring
    MSG_OBSERVACIONES = 'No se pudieron ingresar las Observaciones.';
    CAPTION_OBSERVACIONES = 'Ingresar Observaciones';
    CAPTION_VALIDACION = 'Validación';
    MSG_CLIENTE = 'Debe ingresar el cliente.';
    MSG_CUENTAS = 'Debe seleccionar una cuenta.';
    MSG_DENUNCIA = 'Debe seleccionar el motivo de la denuncia.';

begin
    {Que haya ingresado cliente, cuenta y motivo}
    if not ValidateControls([peCodigoCliente, cb_Cuentas, cb_denuncia],
      [Trim(peCodigoCliente.text) <> '',
      cb_Cuentas.ItemIndex >= 0,
      cb_denuncia.ItemIndex >= 0],
      CAPTION_VALIDACION,
      [MSG_CLIENTE,
      MSG_CUENTAS,
      MSG_DENUNCIA]) then
    begin
        result := false;
        exit;
    end;
    result := true;
end;


end.
