object FormAsignarTag: TFormAsignarTag
  Left = 337
  Top = 283
  BorderStyle = bsDialog
  Caption = 'Asignar Telev'#237'a'
  ClientHeight = 137
  ClientWidth = 339
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 339
    Height = 137
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 21
      Top = 59
      Width = 114
      Height = 13
      Caption = 'N'#250'mero de Telev'#237'a:'
      FocusControl = txtNumeroTelevia
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 21
      Top = 23
      Width = 49
      Height = 13
      Caption = 'Patente:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblPatente: TLabel
      Left = 147
      Top = 23
      Width = 36
      Height = 13
      Caption = 'FF1234'
    end
    object txtNumeroTelevia: TEdit
      Left = 147
      Top = 55
      Width = 145
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 15
      TabOrder = 0
    end
    object btnAceptar: TDPSButton
      Left = 121
      Top = 99
      Width = 95
      Hint = 'Guardar Datos'
      Caption = '&Aceptar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnAceptarClick
    end
    object btnSalir: TDPSButton
      Left = 228
      Top = 99
      Width = 92
      Hint = 'Salir de la Carga'
      Cancel = True
      Caption = '&Salir'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnSalirClick
    end
  end
  object ObtenerUbicacionTag: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerUbicacionTag'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end>
    Left = 296
    Top = 8
  end
end
