object NavWindowProximoResumen: TNavWindowProximoResumen
  Left = 239
  Top = 73
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  Anchors = []
  AutoScroll = False
  Caption = 'Pr'#243'xima Facturaci'#243'n'
  ClientHeight = 559
  ClientWidth = 712
  Color = 14732467
  Constraints.MinHeight = 504
  Constraints.MinWidth = 720
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    712
    559)
  PixelsPerInch = 96
  TextHeight = 13
  object gb_MovDisponibles: TGroupBox
    Left = 4
    Top = 342
    Width = 704
    Height = 185
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'Detalle de viajes'
    Color = 14732467
    ParentColor = False
    TabOrder = 0
    DesignSize = (
      704
      185)
    object dbgDetalles: TDBListEx
      Left = 5
      Top = 15
      Width = 693
      Height = 163
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Concesionaria'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Descripcion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 110
          Header.Caption = 'N'#250'mero de Viaje'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroViaje'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          Header.Caption = 'Momento de Inicio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHoraInicio'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Puntos de recorridos'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'PuntosCobro'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriImporte'
        end>
      DataSource = dsFacturacionDetallada
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object gb_MovSeleccionados: TGroupBox
    Left = 4
    Top = 127
    Width = 704
    Height = 215
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Factura resultado'
    Color = 14732467
    ParentColor = False
    TabOrder = 1
    DesignSize = (
      704
      215)
    object label9: TLabel
      Left = 540
      Top = 194
      Width = 61
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Importe total:'
    end
    object lTotalProximosMovimientos: TLabel
      Left = 604
      Top = 194
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      AutoSize = False
      Caption = 'Importe Total'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object dbgFacturas: TDBListEx
      Left = 5
      Top = 16
      Width = 693
      Height = 175
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Concesionaria'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Concesionaria'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 84
          Header.Caption = 'Fecha'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriFechaCorte'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 250
          Header.Caption = 'Descripci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Observaciones'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriImporte'
        end>
      DataSource = dsInformeFactura
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object gb_Parametros: TGroupBox
    Left = 4
    Top = 0
    Width = 704
    Height = 60
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Par'#225'metros de b'#250'squeda'
    Color = 14732467
    ParentColor = False
    TabOrder = 2
    object Label5: TLabel
      Left = 105
      Top = 18
      Width = 80
      Height = 13
      Caption = 'Pr'#243'ximas fechas:'
    end
    object Label2: TLabel
      Left = 492
      Top = 18
      Width = 42
      Height = 13
      Caption = 'Cuentas:'
    end
    object Label3: TLabel
      Left = 209
      Top = 18
      Width = 80
      Height = 13
      Caption = 'Formas de Pago:'
    end
    object lb_CodigoCliente: TLabel
      Left = 9
      Top = 18
      Width = 71
      Height = 13
      Caption = 'C'#243'digo Cliente:'
    end
    object cb_Cuentas: TComboBox
      Left = 492
      Top = 33
      Width = 205
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = ActualizarCombos
    end
    object cb_FormaPago: TComboBox
      Left = 209
      Top = 33
      Width = 278
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = ActualizarCombos
    end
    object cb_FechasFacturacion: TComboBox
      Left = 105
      Top = 33
      Width = 99
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = ActualizarCombos
    end
    object peCodigoCliente: TPickEdit
      Left = 9
      Top = 33
      Width = 92
      Height = 21
      Enabled = True
      TabOrder = 3
      OnChange = peCodigoClienteChange
      Decimals = 0
      EditorStyle = bteNumericEdit
      OnButtonClick = peCodigoClienteButtonClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 62
    Width = 704
    Height = 63
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Datos del Cliente'
    TabOrder = 3
    DesignSize = (
      704
      63)
    object lApellidoNombre: TLabel
      Left = 253
      Top = 19
      Width = 444
      Height = 16
      Anchors = [akLeft, akTop, akBottom]
      AutoSize = False
      Caption = 'Apellido Cliente'
    end
    object LNumeroDocumento: TLabel
      Left = 112
      Top = 19
      Width = 75
      Height = 16
      Anchors = [akLeft, akTop, akBottom]
      AutoSize = False
      Caption = 'LNumeroDocumento'
    end
    object Label1: TLabel
      Left = 12
      Top = 19
      Width = 96
      Height = 13
      Caption = 'N'#250'mero de RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 202
      Top = 19
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LDomicilio: TLabel
      Left = 72
      Top = 39
      Width = 620
      Height = 16
      Anchors = [akLeft, akTop, akBottom]
      AutoSize = False
      Caption = 'DomicilioCliente'
    end
    object Label10: TLabel
      Left = 12
      Top = 39
      Width = 56
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object btnImprimirDetalles: TDPSButton
    Left = 322
    Top = 529
    Width = 157
    Anchors = [akRight, akBottom]
    Caption = '&Imprimir Detalle de Viajes'
    Enabled = False
    TabOrder = 4
    Visible = False
    OnClick = btnImprimirDetallesClick
  end
  object btnImprimir: TDPSButton
    Left = 484
    Top = 529
    Width = 141
    Anchors = [akRight, akBottom]
    Caption = '&Imprimir Movimientos'
    Enabled = False
    TabOrder = 5
    Visible = False
    OnClick = btnImprimirClick
  end
  object btnSalir: TDPSButton
    Left = 629
    Top = 529
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 6
    OnClick = btnSalirClick
  end
  object ObtenerCliente: TADOStoredProc
    Active = True
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 528
    Top = 327
  end
  object dsInformeFactura: TDataSource
    DataSet = ObtenerInformeFactura
    OnDataChange = dsInformeFacturaDataChange
    Left = 128
    Top = 175
  end
  object ObtenerInformeFactura: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = ObtenerInformeFacturaAfterOpen
    AfterClose = ObtenerInformeFacturaAfterClose
    ProcedureName = 'ObtenerInformeFactura;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigosConvenios'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = '-1'
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigosCuentas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CantidadLineas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 255
      end>
    Left = 160
    Top = 175
  end
  object dsFacturacionDetallada: TDataSource
    DataSet = ObtenerDetalleFacturacion
    Left = 147
    Top = 406
  end
  object ObtenerCuentasProximaFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCuentasProximaFacturacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaFacturacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 332
    Top = 151
  end
  object ObtenerFechasProximaFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerFechasProximasFacturacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoMedioPagoAutomatico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 255
      end>
    Left = 268
    Top = 151
  end
  object ObtenerTipoMedioPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTipoMedioPago;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 300
    Top = 151
  end
  object rp_ProximoResumen: TppReport
    AutoStop = False
    DataPipeline = plDetalle
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 0
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Template.FileName = 'C:\TEMP\aaa.rtm'
    DeviceType = 'Screen'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    Left = 402
    Top = 326
    Version = '7.04'
    mmColumnWidth = 0
    DataPipelineName = 'plDetalle'
    object ppHeaderBand2: TppHeaderBand
      mmBottomOffset = 0
      mmHeight = 26458
      mmPrintPosition = 0
      object ppLabel21: TppLabel
        UserName = 'Label4'
        Caption = 'Fecha:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 179652
        mmTop = 5027
        mmWidth = 10848
        BandType = 0
      end
      object ppLabel34: TppLabel
        UserName = 'Label1'
        Caption = 'Movimientos a Facturar en el Pr'#243'ximo Resumen'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 16
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 6773
        mmLeft = 45566
        mmTop = 0
        mmWidth = 129794
        BandType = 0
      end
      object ppSystemVariable1: TppSystemVariable
        UserName = 'SystemVariable1'
        AutoSize = False
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 179652
        mmTop = 794
        mmWidth = 29898
        BandType = 0
      end
      object ppLabel17: TppLabel
        UserName = 'Label17'
        Caption = 'C'#243'digo de Cliente:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1058
        mmTop = 12171
        mmWidth = 29104
        BandType = 0
      end
      object ppLabel19: TppLabel
        UserName = 'Label2'
        Caption = 'Apellido y Nombres o Raz'#243'n Social:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1058
        mmTop = 17463
        mmWidth = 56356
        BandType = 0
      end
      object ppLabel20: TppLabel
        UserName = 'Label20'
        Caption = 'Domicilio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1058
        mmTop = 22225
        mmWidth = 15346
        BandType = 0
      end
      object ppLine2: TppLine
        UserName = 'Line2'
        ParentWidth = True
        Weight = 0.75
        mmHeight = 3969
        mmLeft = 0
        mmTop = 10583
        mmWidth = 210000
        BandType = 0
      end
      object ppSystemVariable2: TppSystemVariable
        UserName = 'SystemVariable2'
        DisplayFormat = 'dd/mm/yyyy'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 191823
        mmTop = 5027
        mmWidth = 17727
        BandType = 0
      end
      object ppDBText1: TppDBText
        UserName = 'DBText5'
        DataField = 'CodigoCliente'
        DataPipeline = ppdbClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppdbClientes'
        mmHeight = 4233
        mmLeft = 30163
        mmTop = 12171
        mmWidth = 55298
        BandType = 0
      end
      object ppLApellidoNombre: TppLabel
        UserName = 'Label3'
        Caption = 'Label3'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 57415
        mmTop = 17463
        mmWidth = 71702
        BandType = 0
      end
      object ppLDomicilio: TppLabel
        UserName = 'Label12'
        Caption = 'Label12'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 16404
        mmTop = 22225
        mmWidth = 112713
        BandType = 0
      end
    end
    object ppDetailBand2: TppDetailBand
      mmBottomOffset = 0
      mmHeight = 4763
      mmPrintPosition = 0
      object ppdbFechaMovimiento: TppDBText
        UserName = 'DBText1'
        AutoSize = True
        DataField = 'FechaHora'
        DataPipeline = plDetalle
        DisplayFormat = 'dd/mm/yyyy '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'plDetalle'
        mmHeight = 4233
        mmLeft = 1323
        mmTop = 265
        mmWidth = 27263
        BandType = 4
      end
      object ppDBText6: TppDBText
        UserName = 'DBText3'
        DataField = 'Importe'
        DataPipeline = plDetalle
        DisplayFormat = '#######0.00;(#######0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'plDetalle'
        mmHeight = 4233
        mmLeft = 167217
        mmTop = 265
        mmWidth = 17198
        BandType = 4
      end
      object ppDBText7: TppDBText
        UserName = 'DBText4'
        DataField = 'Observaciones'
        DataPipeline = plDetalle
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'plDetalle'
        mmHeight = 4233
        mmLeft = 61119
        mmTop = 265
        mmWidth = 100806
        BandType = 4
      end
      object ppDBText3: TppDBText
        UserName = 'DBText2'
        DataField = 'IndiceVehiculo'
        DataPipeline = plDetalle
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'plDetalle'
        mmHeight = 4233
        mmLeft = 30692
        mmTop = 265
        mmWidth = 28046
        BandType = 4
      end
    end
    object ppFooterBand2: TppFooterBand
      mmBottomOffset = 0
      mmHeight = 17463
      mmPrintPosition = 0
      object ppLine6: TppLine
        UserName = 'Line3'
        Weight = 0.75
        mmHeight = 3175
        mmLeft = 0
        mmTop = 265
        mmWidth = 210080
        BandType = 8
      end
      object ppLabel35: TppLabel
        UserName = 'Label9'
        Caption = 'Notas aclaratorias generales:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 0
        mmTop = 794
        mmWidth = 49213
        BandType = 8
      end
      object ppLabel36: TppLabel
        UserName = 'Label11'
        AutoSize = False
        Caption = 'El presente es un documento de demostraci'#243'n.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        WordWrap = True
        mmHeight = 10583
        mmLeft = 0
        mmTop = 5292
        mmWidth = 205052
        BandType = 8
      end
    end
    object ppGroup2: TppGroup
      BreakName = 'ppLFormaDePago'
      BreakType = btCustomField
      KeepTogether = True
      OnGetBreakValue = ppGroup2GetBreakValue
      OutlineSettings.CreateNode = True
      UserName = 'Group1'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = ''
      object ppGroupHeaderBand2: TppGroupHeaderBand
        mmBottomOffset = 0
        mmHeight = 21167
        mmPrintPosition = 0
        object ppRegion3: TppRegion
          UserName = 'Region1'
          Brush.Color = 15263976
          ParentWidth = True
          mmHeight = 21167
          mmLeft = 0
          mmTop = 0
          mmWidth = 210000
          BandType = 3
          GroupNo = 0
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          object ppLabel37: TppLabel
            UserName = 'Label5'
            Caption = 'Fecha '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 3704
            mmTop = 15611
            mmWidth = 11377
            BandType = 3
            GroupNo = 0
          end
          object ppLabel38: TppLabel
            UserName = 'Label6'
            Caption = 'Cuenta'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4191
            mmLeft = 30427
            mmTop = 15610
            mmWidth = 11938
            BandType = 3
            GroupNo = 0
          end
          object ppLabel39: TppLabel
            UserName = 'Label7'
            Caption = 'Descripci'#243'n'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 61119
            mmTop = 15610
            mmWidth = 20373
            BandType = 3
            GroupNo = 0
          end
          object ppLabel40: TppLabel
            UserName = 'Label8'
            AutoSize = False
            Caption = 'Importe'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            TextAlignment = taRightJustified
            Transparent = True
            mmHeight = 4233
            mmLeft = 161925
            mmTop = 15610
            mmWidth = 22225
            BandType = 3
            GroupNo = 0
          end
          object ppLFormaDePago: TppLabel
            UserName = 'FormaDePago'
            OnGetText = ppLFormaDePagoGetText
            Caption = 'FormaDePago'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 3969
            mmTop = 9790
            mmWidth = 24077
            BandType = 3
            GroupNo = 0
          end
          object ppImage3: TppImage
            UserName = 'Image2'
            MaintainAspectRatio = False
            Stretch = True
            Picture.Data = {
              07544269746D61701E530000424D1E530000000000003604000028000000C800
              0000650000000100080000000000E84E0000C30E0000C30E0000000100000000
              0000171A1F0028353400323C3D002F423F00384846003C584F003F615500424E
              4E00495755004C5556004B635C005768640054766A004E75660067787400636E
              6C00398123003981240039812A0039812D00398131003981360039813A003981
              3D003A8145003A8142003A814A003A814D003A8152003B8255003B8259003B81
              63003D8366003F856800478A330051904C0041876A00458A6E0042886B00498A
              6E0041876300478C71004D8D7200488E73004C9176004E937900518F7500508E
              73005A8273005888750053907600529075005491770057937900569279005296
              7C0053987F0058947A005A957B005A957C005C967D005D977F005D987F005085
              6F004DAB6E0054AC74005AAE790058AD77005DB07B0065877A0060927D0061B1
              7E00559A8100569C8300599E86005F9880005B9C84005DA28A005DA38B005AA0
              8700619A8200639B830060998000649B8300649C8400679D8600679E8600689E
              8700659C85006A9F880069938200759589007793890061A58E0063A28B006BA0
              8A006CA18A006EA28B006FA38D006EA28C006AA48E0069A9930066B4820069B5
              85006BB09B0072A48E0071A48E0070A38D0073A5900075A6910076A7920077A8
              930078A9940079A995007AAA95007BAA96007CAB970078A893007EAC98007FAD
              9A007DAC980076AA960079B49A0072B78D007BB6A300879A9300808B890082AF
              9B0080AE9A0082AA990089A59A0083B09D0082AF9C0085B19E0085B69E0084BB
              9C0092A09C0087B2A00083B5A30089B3A1008AB4A20089B4A2008DB6A4008EB7
              A5008FB7A6008CB5A3008BBDAD0086BBA70090B8A60090BCA50092B9A80093BA
              AA0092BAA90095BBAB0097BDAD0097BCAC0095BCAB0099BEAE009ABFAF0098BD
              AD009ABAAD009BBFB00098AEA600A7BBB3008DC2B20097C8AB009DC0B1009EC1
              B2009EC2B2009AC3B30097C6B7008EC5A300A1C2B400A2C3B500A4C4B600A1C3
              B400A6C6B800A4C6B800A8C7B900A8C7BA00A9C8BB00ABC9BD00ADCABE00ADCB
              BE00A4CBBE00A2D1B300AFCCC000ABCBBD00A4CEC100ACD1C500B2CEC200B1CD
              C100B4CFC300B5CFC400B6CBC200B6D0C500B6D1C600B2D2C600B8D1C600BAD3
              C800BDD4CA00BFD6CC00B9D5CB00B4D6CB00BDDBD100C1D7CE00C2D8CE00C3D9
              D000C6DAD200C4DED500C8DCD300C9DDD500CBDED600CDDFD700CDDFD800CFE0
              D900CCE3DC00D1E2DA00D3E3DC00D4E3DC00D6E5DE00D2E5DE00D7E6E000D8E6
              E000DAE7E100DBE8E300DCE9E300DEEAE400DEEAE500DEEAE500DBEBE500D6E9
              E200E1ECE700E1ECE800E3EDE900E4EEEA00E5EEEA00E7F0EC00E6F2EE00E2EF
              EB00E8F1ED00E8F0EC00EAF2EE00ECF3F000EEF4F200EBF3F000F2F7F400F1F6
              F400F0F6F300F3F8F600F4F8F600F6FAF800F8FBF900F9FCFB00FDFEFE00FBFD
              FC00232222222222222222222222222222222222222222222222222222222222
              2222222222222222222222222222222222222222222222222222222222222222
              2222222222222222222222222222222222222222222222222222222222222222
              2222222222222222222222222222222222222222222222222222222222222222
              2222222222222222222222222222222222222222222222222222222222222222
              2222222222222222222222222222222222222222222222222222222222222222
              2222222222222222222322101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101012121314151516171918
              1A1A1B1C1C1E1D1C1C1B1A1A1819171615151413121210101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              101010101010101010101010101010101010101010101010121417181B1F1F1F
              1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1B
              1817141210101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010121417181C1F
              1F1F1F1F1F1F20212625292B2C2D3738494A4F4D4E5D654E4D4F4A4938372D2C
              2B29252621201F1F1F1F1F1F1F1C181714121010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              101010101010101010101010101010101010101010101010101010101011141A
              1C1F1F1F1F1F1F1F21252D484F5D686868686868686868686868686868686868
              68686868686868686868686868685D4F482D25211F1F1F1F1F1F1F1C1A141110
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              101012161B1F1F1F1F1F21292D494D6868686868686868686868686868686868
              68686868686868686868686868686868686868686868686868686868684D492D
              29211F1F1F1F1F1B161210101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101014191C1F1F1F1F25384D686868686868686868686868686868
              6865656565656464646464646464646464656565656565686868686868686868
              68686868686868686868684D38251F1F1F1F1C19141010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              101010101010101010101012161A1F1F1F1F212C4A6868686868686868686868
              68655D5E54503D3D3E50505053545454555657595F606162626269696C6C6D6F
              6F707070717479797968686868686868686868686868684A2C211F1F1F1F1A16
              1210101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010151C1F1F1F1F29385D686868686868
              686868655D4D4C3E3B3A3C3C3C3D3E4B50505153545455555659595F60616262
              62696C6C6D6D6F7070707272747676777F7F838480777A7A6868686868686868
              68685D38291F1F1F1F1C15101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101011171D1F1F202D4F6868
              686868686868685D3B3934363535393A3C3C3C3D3D3E50505053545455555657
              595F606063626269696C6D6D6F6F7070707274747677777F838383838589898B
              8C8D8D8A7A6868686868686868684F2D201F1F1D171110101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101012181F1F
              1F2B4D68686868686868685D4C392E333434363539393C3C3C3C3D3E4B505051
              53545455565759595F6061636262696C6C6D6F6F7070707272747676777F7F83
              83838585898B8B918E8E8F8F8F948D8A7A68686868686868684D2B1F1F1F1812
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010141B1F1F202D5D6868686868685D4D482E2E2E333234363535393A3C3C3C
              3D3D3E50505053545455555659595F60606362626A696C6D6D6F707070707274
              747677777F838383858589898B8C8C8E8F8F8F94949696979998937C7A686868
              6868685D2D201F1F1B1410101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010161C1F1F26486868686868685D4C342A2F2E2E2E33343436
              3539393B3C3C3D3D3E4B505051535454555657595F6060616362626A696C6D6F
              6F7070707272747676777F7F8383838589898B8B8C8E8E8F8F8F94969697979C
              9A9A9A9D9D97937C68686868686848261F1F1C16101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010161F1F1F2B4F6868686868684A2E2A2A2F2F
              2E2E333234363535393A3B3C3C3D3E4B50505053545455555659595F60616362
              626A69696C6D6F707070727274747677777F8383838585898B8B8C8C8E8F8F8F
              9494969797999C9A9A9A9D9EA1A7A7A7A1936868686868684F2B1F1F1F161010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010141C1F252D5D68686868685D
              48272A2A2A2F2E2E33323434363539393B3C3C3D3D3E50505051535454555657
              595F6060636262626A696C6C6E6F7070707274747676777F7F8383838589898B
              8C8C8E8E8F8F8F94969697999C9A9A9A9D9DA1A6A7A7ACADADADAD927C686868
              68685D2D251F1C14101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101A1F1F2C68
              68686868684D2D272A2A2A2F2F2E2E2E3434363535393A3B3C3C3D3E4B505051
              53545455555659595F60616362626A69696C6D6E757070727274767677777F83
              83838585898B8B8C8E8E8F8F8F94949697979C9A9A9A9A9D9EA1A6A7A7ADADAD
              AEAEAEB0B0A98A6868686868682C1F1F1A101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              10161F1F254E68686868684A2A27272A2A2A2F2E2E2E323236353539393B3C3C
              3D3D3E52505053545454555657595F60606362626A69696C6C6E6F7570707274
              747677777F7F8383838589898B8C8C8E8F8F8F8F94969697999C9A9A9A9D9DA1
              A6A6A7ACADADADAEAEB0B0B2B3B4B5B19268686868684E251F1F161010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              10101010101010111B1F2B4A686868684D382727272A2A2A2F2F2E2E2E323436
              3539393A3B3C3C3D3E4B52505153545455555659595F60616362626A696C6C6D
              6E7570707172747676777F7F8383838585898B8B8C8E8E8F8F8F94949697979C
              9A9A9A9D9D9EA1A6A7A7ACADADAEAEAEB0B2B3B4B4B5B5B5B7B69E9368686868
              4A2B1F1B11101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010101010101010101010141F1F2C6868686868482727272A2A2A2A2F
              2E2E2E3232363535393A3B3C3C3D3D3E52505053545455555657595F60606362
              626A69696C6D6E6F7570707174747677777F838383838589898B8C8C8E8F8F8F
              949496969799999A9A9A9D9EA1A6A6A7ACACADADAEAEB0B0B3B4B4B5B5B5B6B7
              BABFBFBEA468686868682C1F1F14101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101A1F1F4A686868684F2727
              27272A2A2A2F2F2E2E2E3234363539393B3C3C3C3D3E4B525051535454555657
              59595F60616362626A696C6C6E6F7570707172747876777F7F8383838585898B
              8B8C8E8E8F8F8F9496969797999A9A9A9D9DA1A6A6A7A7ACADADADAEAEB0B2B3
              B4B5B5B5B5B7B7BABFBEBEC3C3BE92686868684A1F1F1A101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              101010101010101010101010101010101010101010101010101010101B1F2568
              6868685D382727272A2A2A2F2F2E2E2E3232363535393A3B3C3C3D3D3E525050
              53545458555659595F60606362626A69696C6D6E757070707174747877777F83
              8383858589898B8C8C8E8F8F8F949496979799999A9A9A9D9EA1A6A7A7ACACAD
              ADADAEB0B0B3B4B4B5B5B5B6B7BABFBFBEC0C3C3C3C4C6A97C68686868251F1B
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101C1F2D686868684C272727272A2A2A2F2E2E2E2E3234363539393B3C3C
              3D3D3E4B525051535454585657595F6060616362626A696C6C6E6F7570707172
              747876777F7F8383838589898B8B8C8E8E8F8F8F9496969799999A9A9A9D9DA1
              A6A6A7ACACADADADAEAEB0B2B3B4B5B5B5B6B7B7BABFBEBEC1C3C3C4C6C6C7C8
              C8A4686868682D1F1C1010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              101010101010101010131F1F48686868682D2727272A2A2A2F2F2E2E2E323236
              3535393A3B3C3C3C3E4B52505053545458555659595F60616362626A69696C6D
              6E757070717274747877777F8483838585898B8B8C8C8E8F8F8F949496979799
              9A9A9A9A9D9E9EA6A7A7ACADADADADAEB0B0B2B4B4B5B5B5B7B7BABFBFBEC0C1
              C3C3C4C6C7C7C8C8C8C9BA68686868481F1F1310101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              10101010101010101010101010101010101F1F4F68686837272727272A2A2A2F
              2E2E2E323234363539393B3C3C3C3D3D52505051535454585657595F60606362
              62626A696C6C6E6F7570707174747876777F7F8483838589898B8C8C8E8E8F8F
              8F9496969799999A9A9A9D9D9EA6A6A7ACACADADADAEAEB0B2B2B4B5B5B5B6B7
              BABFBFBEBEC1C3C3C4C6C6C7C8C8C8C9C9C9CDCDB86868684F1F1F1010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101F1F4D6868684C27
              27272A2A2A2F2F2E2E2E3234363535393A3B3C3C3C3D4B525051535454585556
              59595F60616362626A69696C6D6E757070717274787677777F8483838385898B
              8B8C8E8E8F8F8F9494969797999A9A9A9D9D9E9EA6A7A7ACADADADAEAEB0B0B2
              B4B4B5B5B5B7B7BABFBEBEC0C1C3C3C4C6C7C7C8C8C8C9C9CDCDCECFCFAA6868
              684D1F1F10101010101010101010101010101010101010101010101010101010
              102222101010101010101010101010101010101010101010101010101010101C
              1F4D686868382727272A2A2A2F2E2E2E323234353539393B3C3C3C3D3D525050
              53545454585630010001303001000130696C006E0C00705A0100014577000000
              000A0000000A8B8C005A0100015A949600970E00077D9A7D079EA600A7AC0882
              000782B08207B2B4B5B5B5B6B7BABFBFBEC0C1C3C3C4C6C6C7C8C8C8C9C9CDCD
              CECECFD0D0D2B86868684D1F1C10101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010121B1F4F6868685D27272A2A2A2F2F2E2E2E3234343539393A3B3C
              3C3C3D4B52505151545458555659003160460D00316A45006C6D005A00007100
              5A785B0C7F008483838385765A048C8E00005B8F5B009697008100007D000000
              82A6A700AC5C0EAD008200000082B4B5B5B5B6B7BABFBEBEC1C3C3C3C4C6C7C7
              C8C8C8C9C9CDCECECFCFD0D2D2D2D37C6868684F1F1B12101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              10101010101010101010101010191F496868682D272A2A2A2A2F2E2E2E323234
              3535393A3B3C3C3C3D3D52505051545458555657595F003163550D0045694500
              6E6F00005A0074005A776E0C840000000008455B8D8C8E8F00005B945B009799
              00007D009D070E07A7A7AC00000007AE00B0070E07B5B5B5B6B6BABFBFBEC0C1
              C3C3C4C6C6C7C8C8C8C9C9CDCDCECFCFD0D0D2D2D3D3D4D4C5686868491F1910
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010141F2468686838272A2A2A2F
              2F2E2E2E3234343539393A3C3C3C3C3D4B52505151545458565759595F603002
              00000C30010001307570000C720078450200003083000000000A0000000B8F8F
              005B0100015B999A00459D009E820082ACACAD00000004B000B2820082B5B5B6
              B7BABFBEBEC1C3C3C4C6C6C7C7C8C8C8C9C9CDCECECFD0D0D2D2D2D3D3D4D5D6
              D6BD686868241F14101010101010101010101010101010101010101010101010
              102222101010101010101010101010101010101010101010101010101D266868
              68482A2A2A2F2F2E2E2E3232343535393A3A3C3C3C3D3D525050515454585555
              59595F60606362626B69696C6D6E757070707174747877777F84838383858989
              898D8C918E8F8F949496979799999A9A9A9D9E9EA6A7A7ACADADADADAEB0B0B2
              B4B4B5B5B5B6B6BABFBFBEC0C1C3C3C4C6C7C7C8C8C8C9C9C9CDCECFCFD0D2D2
              D2D3D3D4D4D5D6D7D9D9B8686868261D10101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101C1F5D68685D2A2A2A2F2E2E2E2E3234343539393A3C3C3C3D3D4B5250
              51515454585557575F6060616362626B696C6C6E6F7570707172747876777F7F
              848383838989898B8D918E8E8F8F9496969799999A9A9A9D9D9EA6A6A7ACACAD
              ADADAEAEB0B2B2B4B5B5B5B6B6B7BABFBEBEC1C3C3C4C6C6C7C8C8C8C8C9C9C9
              CECECFD0D0D2D2D3D3D3D4D5D6D6D9D9D9DADA9268685D1F1C10101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              10101010101010101010131F4E6868682A2A2F2F2E2E2E3232343535393A3A3C
              3C3C3D4B52505051545458555557595F60616362626B69696C6D6E7570707172
              74747877777F848383838589898B8D8C918E8F8F9094969797999A9A9A9A9D9E
              9EA6A7A7ACADADADAEAEB0B0B2B4B4B5B5B5B6B7BABFBFBEC0C1C3C3C4C6C7C7
              C8C8C8C9C9C9CDCECFCFD0D2D2D2D3D3D4D4D5D6D7D9D9DADADBDBD96868684E
              1F13101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101A246868682A2A2F2E2E2E323234
              343539393A3C3C3C3D3D4B505051515454585557575F6060636262626B696C6C
              6E6F7570707174747876777F7F848383838989898D8C918E8E8F8F9096969799
              999A9A9A9D9D9EA6A6A7ACACADADADAEB0B0B2B2B4B5B5B5B6B6BABFBFBEBEC1
              C3C3C3C6C6C7C8C8C8C9C9C9C9CECECFD0D0D2D2D3D3D4D4D5D6D6D9D9D9DADA
              DBDCDCDEDC686868241A10101010101010101010101010101010101010101010
              10222210101010101010101010101010101010101010101010121F4968685D2F
              2F2E2E2E3234343535393A3A3C3C3C3D4B4B505151545458555557595F606161
              62626B69696C6D6E757070717274787677777F848383838589898B8D918E8E8F
              8F9094969797999A9A9A9D9D9E9EA6A7A7ACADADADAEAEB0B2B2B4B4B5B5B5B6
              B7BABFBEBEC0C1C3C3C3C6C7C7C8C8C8C9C9C9CDCECFCFD0D2D2D2D3D3D4D5D6
              D6D7D9D9DADADBDBDCDEDFE0E1A46868491F1210101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              101C24686868372E2E2E32323435352E0401000000033F4B5050510500000005
              57575F600A0100000230696C6C0400007070710C010000044500848300838989
              898D00918E8F8F0A000000000A999A9A009D9E9EA6A6A745020000077D00B0B2
              B4B4B5B5B500B6BABFBFBEC00B00C3C3C6C6C70F0000000FC9CDCECE00D0D0D2
              D2D3090000D5D6D70F000000000EDCDCDEE0E0E1E2D1686868241C1010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010111F4D68685D2E2E2E32343435393F01063F3C3C3D0D010D
              5151030546554B06035F60616162626B30006C6D6E007070717273000C5A5A75
              300083830089898B8D91008E8F8F040E9697976D9B9A9A9D009EA6A6A7A7AC00
              458282A05C00B2B4B5B5B5B5B600B7BFBEBEC1047D00C3C6C7C7090EC2C9C20E
              09CECFCF00D2D2D2D3D300D5D6D6D9097DDADADBA3DCDEDFE0E1E2E2E3E3A468
              684D1F1110101010101010101010101010101010101010101022221010101010
              10101010101010101010101010101010141F686868372E3232343535393A013F
              3C3C3D3D4B505006545400555557575F0060616230030000000A6E6F75007071
              7374786D0B0404040000838900898D8C918E008F9094000000000000009A9D9E
              00A6A7A7ACACADA00E0707070000B4B5B5B5B6B6B700BFBEC0C108A2C300C6C7
              C8C800C9C9C9CDCE00CFD0D000D2D3D3D3D400D6D7D9D900000000000000E0E0
              E1E2E3E3E4E4D46868681F141010101010101010101010101010101010101010
              10222210101010101010101010101010101010101010101018296868682E3234
              343539393A3C003C3C3D4B4B50515154545403065259530603616262000D456C
              6E6F757070007273787677755A84838345008989000A818E8E0E00909696040E
              979B9A45089D9EA6000EACACADADADA082B0B2B27D00B5B5B6B6B7B7BF00BEC1
              A302A3C6C600C7C8C8C8090EC2CEC20E09D0D2D2007ED3D3D5D600D9D9D9DA09
              7DDBDCDE7D0BE1E2E2E3E3E4E8E9E96868682918101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1C4868685E32343535393A3A3C3C013F3D4B50505154543F5555570500000005
              62626B690C010000010A7070000000007777808408000000025A898D000C0100
              00035B969797970A0000000A9D9EA6A7005C00000BADAEB00B000000047DB5B6
              B7B7BFBFBE00C0A208C3C6C7C700C8C8C9C9C90F0000000FD2D2D2D300880000
              0F00000000DADBDB0E0000000EE1E2E3E3E4E4E8E9EAEBAA6868481C10101010
              1010101010101010101010101010101010222210101010101010101010101010
              10101010101010111F5D68684C343539393A3C3C3C3D31010631515154310405
              5757596060616162626B696C6C6E6F6F7070717273007677807F848383838989
              898B8D918E8E8F8F9096969797999B9A9A9F9D9DA1A6A7ACACADADADAEAEB0B2
              B2B4B5B5B5B6B6B7BFBFBEBEC0007D04C6C6C7C8C800C8C9C9C9CDCECFD0D0D2
              D2D3D3D3D3D5D6D6D7D900DADADBDCDCDEDFE0E1E2E2E3E3E4E8E9E9EBEBECCC
              68685D1F11101010101010101010101010101010101010101022221010101010
              10101010101010101010101010101012206868683B35393A3A3C3C3C3D4B4B3B
              0501000000030D57595960616162626B69696C6D6E6F707071727374780B7780
              848383838589898B8D8C918E8F8F9094969797979B9A9A9A9F9D9EA1A7A7ACAD
              ADADAEAEB0B0B2B4B4B5B5B5B6B7B7BFBEBEC0C0C3000BC6C7C7C8C8C800C9C9
              CDCDCECFD0D2D2D2D3D3D3D4D5D6D7D7D9DA7EDBDBDCDEDFE0E0E1E2E3E3E4E4
              E8E9EAEBECEDF1E1686868201210101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010142168686839393A3C
              3C3C3D3D4B50505151545454555757596060616262626B696C6C6E6F6F707070
              7374787677807F848383838589898D8C918E8E8F8F9096969797999B9A9A9F9D
              9DA1A6A7ACACADADADAEB0B0B2B2B4B5B5B5B6B6B7BFBFBEC0C0C3C3C3C6C6C7
              C8C8C8C9C9C9C9CDCECED0D0D2D2D3D3D3D4D5D6D6D7D9D9DADADBDCDCDEE0E0
              E1E2E2E3E3E4E8E9E9EBEBECF1F0F0EB68686821141010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101015
              256868653A3A3C3C3C3D4B4B505151545454555557595960616162626B69696C
              6D6E6F70707072737876777780848383838585898B8D918E8E8F8F9094969797
              979B9A9A9F9D9D9EA1A7A7ACADADADAEAEB0B2B2B4B4B5B5B5B6B7B7BFBEBEC0
              C3C3C3C3C6C7C7C8C8C8C9C9C9CDCDCECFD0D0D2D2D3D3D3D5D6D6D7D7D9DADA
              DBDBDCDEDFE0E1E2E2E3E3E4E4E8E9EAEBECEDF1F0F0F0F29268682515101010
              1010101010101010101010101010101010222210101010101010101010101010
              101010101010101E2D6868653C3C3C3D3D4B5050515454545455575759606061
              62626269696C6C6E6F6F7070707374747777807F848383838589898D8C918E8F
              8F8F9096969797999B9A9A9F9D9EA1A6A7ACACADADADAEB0B0B2B4B4B5B5B5B6
              B6B7BFBFBEC0C0C3C3C3C6C6C7C8C8C8C9C9C9CDCDCECED0D0D0D2D3D3D3D4D5
              D6D7D7D9D9DADADBDCDCDEE0E0E1E2E3E3E3E4E8E9E9EBEBECF1F0F0F0F2F5F3
              936868381E101010101010101010101010101010101010101022221010101010
              10101010101010101010101010101015496868683C3C3D4B4B50515154545455
              55575959606161626262696C6C6D6E6F7070707273747676807F848383424141
              41414141414141419094969797979B9A9A9F9D9DA1A6A7A7ACADADADAEAEB0B2
              B2B4B5B5B5B5B6B7B5ADA5AB877B7B664744444243434141434342444767ABD0
              D2D2D3D3D3D5D6D6D7D9D9DADADBDBDCDEDFE0E1E2E2E3E3E4E4E8E9EAEBECED
              F1F0F0F0F5F3F4F4936868371710101010101010101010101010101010101010
              102222101010101010101010101010101010101010101014256868653D3D4B50
              5051545454555557575960606162626269696C6D6E6F6F707070737474767780
              8483838383414040404040404040408696969797999B9A9A9F9D9EA1A7A7ACAC
              ADADADAEB0B0B2B4B4B095877B44404040404040404040404040404040404040
              40404040404067D2D3D3D3D4D5D6D7D7D9DADADADBDCDCDEE0E0E1E2E3E3E4E4
              E8E9E9EBEBECF1F0F0F0F2F5F4F4F4F892686825141010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101012
              246868684B4B5051515454545557575959606161626262696C6C6D6F6F707070
              7273747676807F7F838383858541404040404040404096969797979B9A9A9F9D
              9DA1A6A7ACACADADADAEAEB09A7A664341414040404040404040404040404040
              404040404040404040404040404067D3D3D3D4D6D6D7D9D9DADADBDBDCDEDFE0
              E0E2E2E3E3E4E8E9E9EAEBECEDF1F0F0F0F5F3F4F4F8F7F46868682412101010
              1010101010101010101010101010101010222210101010101010101010101010
              101010101010101120686868515051545454555557595960606162626269696C
              6D6D6F707070707374747677807F838383858589894140404040404040869797
              97999B9A9A9F9D9EA1A7A7ACADADADAD87664241404040404040404040404040
              4040404040404040404040404040404040404040404067D3D4D4D6D7D7D9DADA
              DBDBDCDCDEE0E0E0E2E3E3E4E4E8E9EAEBEBECF1F0F0F0F2F5F4F4F4F7F6F6EB
              6868682011101010101010101010101010101010101010101022221010101010
              101010101010101010101010101010101D6868685E5154545455575759606061
              61626262696C6C6D6F6F7070707273747676807F7F8383838589898B8D414040
              40404040439797999B9A9A9F9D9DA1A6A7ACACAD9D6643404040404040404040
              40404040404040404040404040404040404040404040404040404040404067D4
              D6D6D7D9D9DADADBDCDCDEDFE0E0E2E2E3E3E4E8E9E9EBEBECEDF1F0F0F0F5F3
              F4F4F8F7F6F6F6DD6868681C1010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101849686864545455
              5557595960616162626269696C6D6D6F707070727374747677807F8383838585
              898B8D8C914340404040404097979B9A9A9A9F9D9EA1A7A7AC95474040404040
              4040404040404040404040404040404040404040404040404040404040404040
              40404040404067D6D7D7D9DADADBDBDCDEDFE0E0E0E2E3E3E4E4E8E9EAEBECED
              F1F0F0F0F2F5F4F4F4F7F6F6F6F9FABD68684918101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              142B686865545557575960606162626262696C6C6D6F6F707070727474767680
              7F7F8383838589898D8C918E8E43404040404067999B9A9A9F9D9DA1A6A7AC87
              4241404040404040404040404040404040404040404040404040404040404040
              404040404040404040404040404067D7D9D9DADADBDCDCDEE0E0E0E2E2E3E3E4
              E8E9E9EBEBECF1F0F0F0F0F5F3F4F4F8F7F6F6F6FAFAFBA468682B1410101010
              1010101010101010101010101010101010222210101010101010101010101010
              101010101010101011206868685757595960616162626269696C6D6D6F707070
              727274767677807F8383838585898B8B918E8E8F8F434040404040959A9A9F9D
              9D9EA1A7A7874140404040404040404040404040404040404040404040404040
              4040404040404040404040404040404040404040404067D9DADADADBDCDEDFE0
              E0E2E2E3E3E4E4E8E9EAEBECEDF1F0F0F0F2F5F4F4F4F7F6F6F6F9FAFBFBE468
              6868201110101010101010101010101010101010101010101022221010101010
              10101010101010101010101010101010101C4D6868655960606162626269696C
              6C6D6F6F7070707274747677807F7F8383838589898B8C918E8F8F8F90434040
              4040409A9A9F9D9EA1A6A7AC4140404040404040404040404040404040404040
              40404040404040404040404040404040404040404040404040404040404067DA
              DADADCDCDEE0E0E0E2E3E3E3E4E8E9E9EBEBECF1F0F0F0F2F5F3F4F4F8F7F6F6
              F6FAFAFBFCFCBC68684D1C101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101012256868686061
              61626262696C6C6D6D6F7070707272747676777F7F8383838585898B8B918E8E
              8F8F9094944340404040409F9D9DA1A6A7A78740404040404040404040404040
              4040404040404040404040404040404040404040404040404040404040404040
              40404040404067DADBDCDCDFE0E0E2E2E3E3E4E4E8E9EAEBECEDF1F0F0F0F5F3
              F4F4F4F7F6F6F6F9FAFBFBFCFDEE686868251210101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              10101A4D68686562626269696C6D6D6F6F7070707274747677777F8383838385
              89898B8C918E8F8F90949496974340404040409C9EA1A7A7AC95404040404040
              4040404040404040404040404040404040404040404040404040404040404040
              404040404040404040404040404066DCDCDCDFE0E0E2E3E3E4E4E8E9E9EBEBEC
              F1F0F0F0F2F5F4F4F4F8F7F6F6F6FAFAFBFCFCFDFFBC68684D1A101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101013256868686262696C6C6D6F6F707070727274767677
              7F7F8383838585898B8B918E8E8F8F8F94969797974340404040407AA6A7ACAC
              AD41404040404040404040404040404040404040404040404040404040404040
              4040404040404040404040404040404040404040404066DCDFDFE0E2E2E3E3E4
              E8E9E9EAEBECEDF1F0F0F0F5F3F4F4F8F7F6F6F6F9FAFBFBFCFDFDFFFC686868
              2513101010101010101010101010101010101010101010101022221010101010
              101010101010101010101010101010101010101C4E686868696C6D6D6F707070
              707274747677777F838383858589898B8C918E8F8F8F9494979797999C434040
              40404042A7ACADADA64040404040404040404040404040404040404040404040
              40404040404040404040404040404040404040404040404040404040404066DF
              E0E0E2E3E3E4E4E8E9EAEBEBECF1F0F0F0F2F5F4F4F4F7F6F6F6F6FAFAFBFCFC
              FDFFFEFEA468684E1C1010101010101010101010101010101010101010101010
              102222101010101010101010101010101010101010101010101010101D686868
              656D6F6F7070707272747676777F7F8383838589898B8B918E8E8F8F8F949696
              97999C9A9A4340404040404095ADADAD9C404040404040404040404040404040
              4040404040404040404040404040404040404040404040404040404040404040
              40404040404066E0E2E2E3E3E4E8E9E9EBEBECEDF1F0F0F0F2F3F4F4F8F7F6F6
              F6F9FAFBFBFCFDFDFFFEFEBD6868681D10101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              10101010142B68686879707070727274747677777F8383838585898B8B8C918E
              8F8F8F94949697979C9A9A9A9F4340404040404040ADAEAEB041404040404040
              4040404040404040404040404040404040404040404040404040404040404040
              404040404040404040404040404066E2E3E3E3E4E8E9EAEBECEDF1F0F0F0F2F2
              F4F4F4F7F6F6F6F9FAFAFBFCFCFDFFFEFEFECC6868682B141010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              10101010101010101010101010194968686879707274747676777F7F83838385
              89898B8C918E8E8F8F8F94969697999C9A9A9F9D9D4340404040404040409CB0
              B07A404040404040404040404040404040404040404040404040404040404040
              4040404040404040404040404040404040404040404066E3E3E3E8E9E9EBEBEC
              F1F0F0F0F0F2F3F4F4F8F7F6F6F6FAFAFBFBFCFDFDFFFEFEFEEE686868491910
              1010101010101010101010101010101010101010101010101022221010101010
              101010101010101010101010101010101010101010121D4A6868687074767677
              777F8383838585898B8B8C8E8E8F8F8F94949697979C9A9A9F9D9D9EA1424040
              4040404040404095B3B443404040404040404040404040404040404040404040
              40404040404040404040404040404040404040404040404040404040404066E3
              E4E8E9EAEBECEDF1F0F0F0F2F2F4F4F4F8F6F6F6F9FAFBFBFCFCFDFFFEFEFEFE
              F86868684A1D1210101010101010101010101010101010101010101010101010
              102222101010101010101010101010101010101010101010101010101010101C
              5D6868687977777F7F8383838589898B8C8C8E8F8F8F8F94969697999C9A9A9A
              9D9EA1A6A742404040404040404040407BB5B587404040404040404040404040
              4040404040404040404040404040404040404040404040404040404040404040
              40404040404066E8E9E9EAEBECF1F0F0F0F2F2F3F4F4F8F8F6F6F6FAFAFBFCFC
              FDFDFFFEFEFEFED86868685D1C10101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              10101010101010101F68686868777F8383838585898B8B8C8E8E8F8F8F949496
              97979C9A9A9A9D9DA1A6A7A7A742404040404040404040404042A8B787414040
              4040404040404040404040404040404040404040404040404040404040404040
              404040404040404040404040404066E9EAEAECEDF1F0F0F0F2F3F4F4F4F8F6F6
              F6F9FAFBFBFCFDFDFFFEFEFEFEFEEF686868681F101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              10101010101010101010101010101010101F686868687A83838589898B8C8C8E
              8F8F8F9494969697999C9A9A9A9D9EA1A7A7A7ACAD4240404040404040404040
              4040404787AF4440404040404040404040404040404040404040404040404040
              4040404040404040404040404040404040404040404066EAEBECEDF0F0F0F2F2
              F4F4F4F8F8F6F6F6FAFAFBFCFCFDFFFEFEFEFEFEFEDD686868681F1010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101013215D68686884
              85898B8B8C8E8E8F8F8F94969697979C9A9A9A9D9DA1A6A6A7ACADADAD424040
              404040404040404040404040404041A540404040404040404040404040404040
              40404040404040404040404040404040404040404040404040404040404066EC
              EDEDF0F0F0F2F3F4F4F8F8F6F6F6F9FAFBFBFCFDFDFFFEFEFEFEFEFEEE686868
              5D21131010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101C4D6868687A898C8C8E8F8F8F9494969797999C9A9A9A9D9EA1A6A7A7
              ADADADADAE9EA8A8AFAFADAEAEAEB0B0B2B2B4B5BBBBB7C3C4A9A5A9A5A5A5A5
              A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5AB
              ABABABABABABB9EDF0F0F0F2F2F4F4F4F8F6F6F6F6FAFAFBFCFCFDFFFEFEFEFE
              FEFEFBAA6868684D1C1010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010101010101010101B49686868688A8E8F8F8F94969697999C9A9A9A
              9D9DA1A6A6A7ACACADADAEAEAEB0B2B3B4B5B5B5B6B7BABFBFBEBEC3C3C3C4C6
              C6C7C8C8C8C9C9C9CDCECECFD0D0D2D3D3D3D4D4D6D6D7D9D9DADADADBDCDCDF
              E0E0E2E2E3E3E3E8E9E9EAEBECEDEDF0F0F0F2F3F4F4F8F8F6F6F6F9FAFBFBFC
              FDFDFFFEFEFEFEFEFEDD68686868491B10101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101A2D686868687A8D949496
              9797999A9A9A9A9D9EA1A6A7A7ACADADAEAEAEB0B0B3B4B4B5B5B5B7B7BABFBE
              BEC0C3C3C3C4C6C7C7C8C8C8C9C9CDCDCECFCFD0D2D2D3D3D3D4D6D6D7D7D9DA
              DADADBDBDCDFDFE0E2E2E3E3E3E4E4E9EAEAECEDEDF0F0F0F2F2F3F4F4F8F6F6
              F6F9FAFAFBFCFCFDFFFEFEFEFEFEFEEFBC686868682D1A101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101424
              5D686868688A969799999A9A9A9D9DA1A6A6A7ACACADADADAEB0B0B2B3B4B5B5
              B5B6B7BABFBFBEC0C3C3C3C4C6C6C7C8C8C8C9C9CDCDCECECFD0D0D2D2D3D3D4
              D4D6D7D7D9D9DADADADBDCDCDFE0E0E2E3E3E3E3E4E9E9EAEBECEDF0F0F0F0F2
              F3F3F4F8F8F6F6F6FAFAFBFBFCFDFDFFFEFEFEFEFEFED1686868685D24141010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              10101010101010111B38686868687A8E9A9A9A9D9D9EA1A6A7A7ACADADADAEAE
              B0B2B3B4B4B5B5B5B7B7BABFBEBEC3C3C3C3C4C6C7C7C8C8C8C9C9CDCECECFCF
              D0D2D2D2D3D3D4D6D6D7D9D9DADADADBDBDCDFDFE0E2E2E3E3E3E4E4E9EAEAEC
              EDEDF0F0F0F2F2F3F4F4F8F6F6F6F9FAFBFBFCFCFDFFFEFEFEFEFEFEE7A46868
              6868381B11101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              101010101010101010101010101010101016385D686868688A989D9EA1A6A6A7
              ACACADADADAEB0B0B3B4B4B5B5B5B6B7BABFBFBEC0C3C3C3C4C6C6C7C8C8C8C9
              C9CDCDCECFCFD0D0D2D2D3D3D4D4D6D7D7D9DADADADADBDCDCDFE0E0E2E3E3E3
              E4E4E9E9EAEBEBEDF0F0F0F2F2F3F3F4F8F8F6F6F6FAFAFBFCFCFDFDFFFEFEFE
              FEFEEFCC686868685D3816101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101A2D686868
              68687C92A6A7A7ACADADADAEAEB0B2B2B4B5B5B5B5B7B7BABFBEBEC1C3C3C4C6
              C6C7C7C8C8C8C9C9CDCECECFD0D0D2D2D2D3D3D4D5D6D7D9D9DADADADBDBDCDF
              DFE0E2E2E3E3E3E4E9E9EAEAEBEDEDF0F0F0F2F3F3F4F4F8F6F6F6F9FAFBFBFC
              FDFDFFFEFEFEFEFED8AA68686868682D1A101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              10101010141D4868686868686893A6ADADADAEB0B0B2B4B4B5B5B5B6B7BABFBF
              BEC0C1C3C3C4C6C7C7C8C8C8C9C9CDCDCECFCFD0D2D2D2D3D3D4D4D5D6D7D9DA
              DADADBDBDCDCDFE0E0E2E3E3E3E4E4E9EAEAEBEBEDF0F0F0F2F2F3F4F4F8F8F6
              F6F6F9FAFBFCFCFDFFFEFEFEFEF9CA686868686868481D141010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010162C496868686868688A9EAEB0B2B2B4B5B5
              B5B6B7B7BABFBEBEC1C3C3C4C6C6C7C8C8C8C8C9C9CDCECECFD0D0D2D2D3D3D3
              D4D5D6D6D9D9DADADADBDCDCDFDFE0E2E2E3E3E3E4E9E9EAEBEBEDEDF0F0F0F2
              F3F3F4F8F8F6F6F6F9F9FBFBFCFDFDFFFEFEFEEFBC686868686868492C161010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              101010101010101010101010101010101010101010101010161D376868686868
              687C9AB4B4B5B5B5B7B7BABFBFBEC0C1C3C3C4C6C7C7C8C8C8C9C9CDCDCECFCF
              D0D2D2D2D3D3D4D4D5D6D7D9D9DADADBDBDCDFDFE0E0E2E3E3E3E4E4E9EAEAEB
              EDEDF0F0F0F2F2F3F4F4F8F6F6F6F6F9FAFBFBFCFDFFFEFEFED8AA6868686868
              68371D1610101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010141B2C4D6868686868688AA9B5B6BABFBFBEBEC1C3C3C4C6C6C7C8C8C8C9
              C9C9CDCECECFD0D0D2D2D3D3D4D4D5D6D6D9D9D9DADADBDCDCDFE0E0E2E2E3E3
              E3E4E9E9EAEBEBEDF0F0F0F0F2F3F3F4F8F8F6F6F6F9F9FBFBFBFCFDFFFCE7B8
              6868686868684D2C1B1410101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101218254A68686868686868939EB4BEC0C1C3C3C4
              C6C7C7C8C8C8C9C9CDCDCECFCFD0D2D2D2D3D3D4D5D6D6D7D9D9DADADBDBDCDF
              DFE0E2E2E3E3E3E4E4E9EAEAEBEDEDF0F0F0F2F2F3F4F4F8F6F6F6F9F9FAFBFB
              FCFCEED1AA686868686868684A25181210101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101011171E37686868686868
              68687C92B1C0C6C6C7C8C8C8C9C9CDCDCECECFD0D0D2D2D3D3D4D4D5D6D7D9D9
              D9DADADBDCDCDFE0E0E2E3E3E3E3E4E9E9EAEBEBEDF0F0F0F2F2F3F3F4F8F8F6
              F6F6F9FAFBFBF6DDBDA46868686868686868371E171110101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              10151C29495D686868686868686893A9C1C8C8C9C9CDCECECFCFD0D2D2D2D3D3
              D4D5D6D6D9D9D9DADADBDBDCDEDFE0E2E2E3E3E3E4E4E9EAEAEBEDEDF0F0F0F2
              F3F3F4F4F8F6F6F6F9F9F9F5CCAA68686868686868685D49291C151010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              101010101010101010101012161A262D4D6868686868686868689392AAB8C4CD
              D0D0D2D2D3D3D4D4D5D6D7D9D9DADADADBDCDCDEE0E0E2E3E3E3E4E4E9E9EAEB
              EBEDF0F0F0F2F2F3F4F4F8F8F4E4D6CBB8A46868686868686868684D2D261A16
              1210101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              101010101010101010101010101010101010101010101014191D2B485D686868
              68686868686868689392AABBCAD0D5D6D6D9D9D9DADADBDBDCDEDFE0E1E2E3E3
              E3E4E9E9EAEAEBEDEDF0F0F0F2F3ECE7D1BDAAA468686868686868686868685D
              482B1D1914101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              101012161B242D4F6868686868686868686868686868939292A4AAAAB8BCBDC5
              CACCD1D6DAD3DBDCD9D8D3D1CCCBC5BDBCB8AAAAA4A468686868686868686868
              686868684F2D241B161210101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101011141A1C242B37494D68686868686868686868
              6868686868686868686868686868686868686868686868686868686868686868
              6868686868684D49372B241C1A14111010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010121417181C21
              292D494D68686868686868686868686868686868686868686868686868686868
              68686868686868684D492D29211C181714121010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010121417181B21292B2C2D373848494F4D4E5D5D6868686868
              685D5D4E4D4F494838372D2C2B29211B18171412101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101012121314151516171918
              1A1A1B1C1D28281D1C1B1A1A1819171615151413121210101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022221010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010102222101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010222210101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101022221010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1010101010101010101010101010101010101010101010101010101010101010
              1022232222222222222222222222222222222222222222222222222222222222
              2222222222222222222222222222222222222222222222222222222222222222
              2222222222222222222222222222222222222222222222222222222222222222
              2222222222222222222222222222222222222222222222222222222222222222
              2222222222222222222222222222222222222222222222222222222222222222
              2222222222222222222222222222222222222222222222222222222222222222
              22222222222222222223}
            mmHeight = 6615
            mmLeft = 3969
            mmTop = 1588
            mmWidth = 13229
            BandType = 3
            GroupNo = 0
          end
          object ppLabel24: TppLabel
            UserName = 'Label24'
            Caption = 'Facturaci'#243'n por Concesionaria Costanera Norte'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 18785
            mmTop = 2911
            mmWidth = 79640
            BandType = 3
            GroupNo = 0
          end
        end
      end
      object ppGroupFooterBand2: TppGroupFooterBand
        mmBottomOffset = 0
        mmHeight = 11642
        mmPrintPosition = 0
        object ppRegion4: TppRegion
          UserName = 'Region2'
          Brush.Color = 15329769
          ParentWidth = True
          mmHeight = 11642
          mmLeft = 0
          mmTop = 0
          mmWidth = 210000
          BandType = 5
          GroupNo = 0
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          object ppLabel41: TppLabel
            UserName = 'Label10'
            Caption = 'Subtotal Concesionaria:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 120915
            mmTop = 1323
            mmWidth = 40217
            BandType = 5
            GroupNo = 0
          end
          object ppDBCalc2: TppDBCalc
            UserName = 'DBCalc1'
            DataField = 'Importe'
            DataPipeline = plDetalle
            DisplayFormat = '#######0.00;(#######0.00)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            ResetGroup = ppGroup2
            TextAlignment = taRightJustified
            Transparent = True
            DataPipelineName = 'plDetalle'
            mmHeight = 4233
            mmLeft = 162984
            mmTop = 1058
            mmWidth = 22225
            BandType = 5
            GroupNo = 0
          end
          object ppLabel2: TppLabel
            UserName = 'Label101'
            Caption = 'Total:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 151607
            mmTop = 6085
            mmWidth = 9525
            BandType = 5
            GroupNo = 0
          end
          object ppDBCalc3: TppDBCalc
            UserName = 'DBCalc3'
            DataField = 'Importe'
            DataPipeline = plDetalle
            DisplayFormat = '#######0.00;(#######0.00)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            ResetGroup = ppGroup2
            TextAlignment = taRightJustified
            Transparent = True
            DataPipelineName = 'plDetalle'
            mmHeight = 4233
            mmLeft = 162984
            mmTop = 6085
            mmWidth = 22225
            BandType = 5
            GroupNo = 0
          end
        end
      end
    end
  end
  object ppdbClientes: TppDBPipeline
    DataSource = dsCliente
    UserName = 'dbClientes'
    Left = 464
    Top = 327
    object ppdbClientesppField1: TppField
      FieldAlias = 'Personeria'
      FieldName = 'Personeria'
      FieldLength = 0
      DisplayWidth = 0
      Position = 0
    end
    object ppdbClientesppField2: TppField
      FieldAlias = 'CodigoDocumento'
      FieldName = 'CodigoDocumento'
      FieldLength = 4
      DisplayWidth = 4
      Position = 1
    end
    object ppdbClientesppField3: TppField
      FieldAlias = 'NumeroDocumento'
      FieldName = 'NumeroDocumento'
      FieldLength = 11
      DisplayWidth = 11
      Position = 2
    end
    object ppdbClientesppField4: TppField
      FieldAlias = 'NumeroDocumentoFormateado'
      FieldName = 'NumeroDocumentoFormateado'
      FieldLength = 12
      DisplayWidth = 12
      Position = 3
    end
    object ppdbClientesppField5: TppField
      Alignment = taRightJustify
      FieldAlias = 'CodigoCliente'
      FieldName = 'CodigoCliente'
      FieldLength = 0
      DataType = dtLongint
      DisplayWidth = 10
      Position = 4
    end
    object ppdbClientesppField6: TppField
      FieldAlias = 'Apellido'
      FieldName = 'Apellido'
      FieldLength = 60
      DisplayWidth = 60
      Position = 5
    end
    object ppdbClientesppField7: TppField
      FieldAlias = 'ApellidoMaterno'
      FieldName = 'ApellidoMaterno'
      FieldLength = 30
      DisplayWidth = 30
      Position = 6
    end
    object ppdbClientesppField8: TppField
      FieldAlias = 'Nombre'
      FieldName = 'Nombre'
      FieldLength = 60
      DisplayWidth = 60
      Position = 7
    end
    object ppdbClientesppField9: TppField
      Alignment = taRightJustify
      FieldAlias = 'CodigoDomicilio'
      FieldName = 'CodigoDomicilio'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 8
    end
    object ppdbClientesppField10: TppField
      Alignment = taRightJustify
      FieldAlias = 'CodigoCalle'
      FieldName = 'CodigoCalle'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 9
    end
    object ppdbClientesppField11: TppField
      FieldAlias = 'CalleDesnormalizada'
      FieldName = 'CalleDesnormalizada'
      FieldLength = 50
      DisplayWidth = 50
      Position = 10
    end
    object ppdbClientesppField12: TppField
      FieldAlias = 'Numero'
      FieldName = 'Numero'
      FieldLength = 10
      DisplayWidth = 10
      Position = 11
    end
    object ppdbClientesppField13: TppField
      Alignment = taRightJustify
      FieldAlias = 'Piso'
      FieldName = 'Piso'
      FieldLength = 0
      DataType = dtLongint
      DisplayWidth = 10
      Position = 12
    end
    object ppdbClientesppField14: TppField
      FieldAlias = 'Dpto'
      FieldName = 'Dpto'
      FieldLength = 10
      DisplayWidth = 10
      Position = 13
    end
    object ppdbClientesppField15: TppField
      FieldAlias = 'Detalle'
      FieldName = 'Detalle'
      FieldLength = 100
      DisplayWidth = 100
      Position = 14
    end
    object ppdbClientesppField16: TppField
      FieldAlias = 'CodigoPostal'
      FieldName = 'CodigoPostal'
      FieldLength = 10
      DisplayWidth = 10
      Position = 15
    end
  end
  object dsCliente: TDataSource
    DataSet = ObtenerCliente
    Left = 496
    Top = 327
  end
  object rp_DetalleViajes: TppReport
    AutoStop = False
    DataPipeline = ppDBPipeline1
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 0
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 297127
    PrinterSetup.mmPaperWidth = 210079
    PrinterSetup.PaperSize = 9
    Template.FileName = 'C:\TEMP\aaa.rtm'
    DeviceType = 'Screen'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    Left = 185
    Top = 326
    Version = '7.04'
    mmColumnWidth = 210000
    DataPipelineName = 'ppDBPipeline1'
    object ppHeaderBand1: TppHeaderBand
      mmBottomOffset = 0
      mmHeight = 37042
      mmPrintPosition = 0
      object ppLine4: TppLine
        UserName = 'Line4'
        ParentWidth = True
        Weight = 0.75
        mmHeight = 3969
        mmLeft = 0
        mmTop = 30163
        mmWidth = 210079
        BandType = 0
      end
      object ppLabel1: TppLabel
        UserName = 'Label4'
        Caption = 'Fecha:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 180182
        mmTop = 5556
        mmWidth = 10848
        BandType = 0
      end
      object ppLabel3: TppLabel
        UserName = 'Label1'
        Caption = 'Detalle de Viajes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 16
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 6773
        mmLeft = 79239
        mmTop = 529
        mmWidth = 44704
        BandType = 0
      end
      object ppSystemVariable3: TppSystemVariable
        UserName = 'SystemVariable1'
        AutoSize = False
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 180182
        mmTop = 1323
        mmWidth = 29898
        BandType = 0
      end
      object ppLabel4: TppLabel
        UserName = 'Label17'
        Caption = 'C'#243'digo de Cliente:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1058
        mmTop = 15346
        mmWidth = 29104
        BandType = 0
      end
      object ppLabel5: TppLabel
        UserName = 'Label2'
        Caption = 'Apellido y Nombres o Raz'#243'n Social:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1058
        mmTop = 20108
        mmWidth = 56356
        BandType = 0
      end
      object ppLabel6: TppLabel
        UserName = 'Label20'
        Caption = 'Domicilio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1058
        mmTop = 24871
        mmWidth = 15346
        BandType = 0
      end
      object ppLine1: TppLine
        UserName = 'Line2'
        ParentWidth = True
        Weight = 0.75
        mmHeight = 3969
        mmLeft = 0
        mmTop = 11113
        mmWidth = 210079
        BandType = 0
      end
      object ppSystemVariable4: TppSystemVariable
        UserName = 'SystemVariable2'
        DisplayFormat = 'dd/mm/yyyy'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4064
        mmLeft = 192352
        mmTop = 5556
        mmWidth = 17611
        BandType = 0
      end
      object ppDBText9: TppDBText
        UserName = 'DBText5'
        DataField = 'CodigoCliente'
        DataPipeline = ppdbClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppdbClientes'
        mmHeight = 4233
        mmLeft = 30163
        mmTop = 15346
        mmWidth = 55298
        BandType = 0
      end
      object pplApellidoNombreViajes: TppLabel
        UserName = 'Label3'
        Caption = 'Label3'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 57415
        mmTop = 20108
        mmWidth = 71702
        BandType = 0
      end
      object ppLabel7: TppLabel
        UserName = 'Label12'
        Caption = 'C'#243'digo Cuenta:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 51858
        mmTop = 32544
        mmWidth = 24606
        BandType = 0
      end
      object ppLDomicilioViajes: TppLabel
        UserName = 'Label14'
        Caption = 'Label14'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 16404
        mmTop = 24871
        mmWidth = 112977
        BandType = 0
      end
      object ppLabel8: TppLabel
        UserName = 'Label16'
        Caption = 'Fecha de Corte:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 2117
        mmTop = 32544
        mmWidth = 25665
        BandType = 0
      end
      object ppLFechaCorteViajes: TppLabel
        UserName = 'Label18'
        AutoSize = False
        Caption = 'Label18'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 27781
        mmTop = 32544
        mmWidth = 21960
        BandType = 0
      end
      object ppLCuenta: TppLabel
        UserName = 'Label7'
        AutoSize = False
        Caption = 'Label7'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 76200
        mmTop = 32544
        mmWidth = 28575
        BandType = 0
      end
    end
    object ppDetailBand1: TppDetailBand
      mmBottomOffset = 0
      mmHeight = 4763
      mmPrintPosition = 0
      object ppdFechaViaje: TppDBText
        UserName = 'DBText2'
        DataField = 'NumeroViaje'
        DataPipeline = ppDBPipeline1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 3969
        mmLeft = 3175
        mmTop = 265
        mmWidth = 20108
        BandType = 4
      end
      object ppdbFechaViaje: TppDBText
        UserName = 'DBText1'
        AutoSize = True
        DataField = 'FechaHoraInicio'
        DataPipeline = ppDBPipeline1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 4233
        mmLeft = 26458
        mmTop = 265
        mmWidth = 27252
        BandType = 4
      end
      object ppDBText13: TppDBText
        UserName = 'DBText3'
        DataField = 'Importe'
        DataPipeline = ppDBPipeline1
        DisplayFormat = '#######0.00;(#######0.00)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 3969
        mmLeft = 191823
        mmTop = 529
        mmWidth = 17198
        BandType = 4
      end
      object ppDBText16: TppDBText
        UserName = 'DBText9'
        DataField = 'PuntosCobro'
        DataPipeline = ppDBPipeline1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 4233
        mmLeft = 121179
        mmTop = 265
        mmWidth = 64558
        BandType = 4
      end
      object ppDBText14: TppDBText
        UserName = 'DBText4'
        DataField = 'Patente'
        DataPipeline = ppDBPipeline1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 3969
        mmLeft = 59267
        mmTop = 265
        mmWidth = 14288
        BandType = 4
      end
      object ppDBText17: TppDBText
        UserName = 'DBText8'
        DataField = 'Categoria'
        DataPipeline = ppDBPipeline1
        DisplayFormat = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 4064
        mmLeft = 74083
        mmTop = 265
        mmWidth = 17198
        BandType = 4
      end
      object ppLabel26: TppLabel
        UserName = 'Label26'
        Caption = 'Costanera Norte'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 93398
        mmTop = 265
        mmWidth = 24871
        BandType = 4
      end
    end
    object ppFooterBand1: TppFooterBand
      mmBottomOffset = 0
      mmHeight = 20902
      mmPrintPosition = 0
      object ppLine3: TppLine
        UserName = 'Line3'
        Weight = 0.75
        mmHeight = 3175
        mmLeft = 0
        mmTop = 265
        mmWidth = 210080
        BandType = 8
      end
      object ppLabel10: TppLabel
        UserName = 'Label9'
        Caption = 'Notas aclaratorias generales:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 0
        mmTop = 794
        mmWidth = 49213
        BandType = 8
      end
      object ppLabel11: TppLabel
        UserName = 'Label11'
        AutoSize = False
        Caption = 'El presente es un documento de demostraci'#243'n.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        WordWrap = True
        mmHeight = 10583
        mmLeft = 0
        mmTop = 5027
        mmWidth = 205052
        BandType = 8
      end
    end
    object ppGroup1: TppGroup
      BreakType = btCustomField
      KeepTogether = True
      OutlineSettings.CreateNode = True
      UserName = 'Group1'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = ''
      object ppGroupHeaderBand1: TppGroupHeaderBand
        mmBottomOffset = 0
        mmHeight = 6350
        mmPrintPosition = 0
        object ppRegion1: TppRegion
          UserName = 'Region1'
          Brush.Color = 15263976
          ParentHeight = True
          ParentWidth = True
          mmHeight = 6350
          mmLeft = 0
          mmTop = 0
          mmWidth = 210079
          BandType = 3
          GroupNo = 0
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          object ppLabel12: TppLabel
            UserName = 'Label5'
            Caption = 'Nro. de Viaje'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 1058
            mmTop = 1323
            mmWidth = 21960
            BandType = 3
            GroupNo = 0
          end
          object ppLabel13: TppLabel
            UserName = 'Label6'
            Caption = 'Fecha / Hora Inicio'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 26458
            mmTop = 1323
            mmWidth = 31750
            BandType = 3
            GroupNo = 0
          end
          object ppLabel15: TppLabel
            UserName = 'Label8'
            AutoSize = False
            Caption = 'Importe'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            TextAlignment = taRightJustified
            Transparent = True
            mmHeight = 4233
            mmLeft = 187061
            mmTop = 1323
            mmWidth = 22225
            BandType = 3
            GroupNo = 0
          end
          object ppLabel22: TppLabel
            UserName = 'Label22'
            Caption = 'Puntos de Cobro Recorridos'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 121179
            mmTop = 1323
            mmWidth = 48419
            BandType = 3
            GroupNo = 0
          end
          object ppLabel16: TppLabel
            UserName = 'Label13'
            Caption = 'Categor'#237'a'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 74877
            mmTop = 1323
            mmWidth = 16404
            BandType = 3
            GroupNo = 0
          end
          object ppLabel9: TppLabel
            UserName = 'Label19'
            Caption = 'Patente'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 59267
            mmTop = 1323
            mmWidth = 12785
            BandType = 3
            GroupNo = 0
          end
          object ppLabel14: TppLabel
            UserName = 'Label21'
            AutoSize = False
            Caption = 'Concesionaria'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            TextAlignment = taCentered
            Transparent = True
            mmHeight = 4233
            mmLeft = 92604
            mmTop = 1323
            mmWidth = 26988
            BandType = 3
            GroupNo = 0
          end
        end
      end
      object ppGroupFooterBand1: TppGroupFooterBand
        mmBottomOffset = 0
        mmHeight = 8731
        mmPrintPosition = 0
        object ppRegion2: TppRegion
          UserName = 'Region2'
          Brush.Color = 15329769
          ParentWidth = True
          mmHeight = 6350
          mmLeft = 0
          mmTop = 0
          mmWidth = 210079
          BandType = 5
          GroupNo = 0
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          object ppLabel18: TppLabel
            UserName = 'Label10'
            Caption = 'Total:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 148167
            mmTop = 1058
            mmWidth = 9525
            BandType = 5
            GroupNo = 0
          end
          object ppDBCalc1: TppDBCalc
            UserName = 'DBCalc1'
            DataField = 'Importe'
            DataPipeline = ppDBPipeline1
            DisplayFormat = '#######0.00;(#######0.00)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            TextAlignment = taRightJustified
            Transparent = True
            DataPipelineName = 'ppDBPipeline1'
            mmHeight = 4233
            mmLeft = 162984
            mmTop = 1058
            mmWidth = 22225
            BandType = 5
            GroupNo = 0
          end
        end
      end
    end
  end
  object ObtenerCuentaPorFormaPago: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 616
    Top = 295
    object ObtenerCuentaPorFormaPagotipopago: TStringField
      FieldName = 'tipopago'
      FixedChar = True
      Size = 2
    end
    object ObtenerCuentaPorFormaPagotipodebito: TStringField
      FieldName = 'tipodebito'
      FixedChar = True
      Size = 2
    end
    object ObtenerCuentaPorFormaPagocodigoentidad: TWordField
      FieldName = 'codigoentidad'
    end
    object ObtenerCuentaPorFormaPagocuentadebito: TStringField
      FieldName = 'cuentadebito'
      FixedChar = True
      Size = 16
    end
    object ObtenerCuentaPorFormaPagoagrupacioncuentas: TWordField
      FieldName = 'agrupacioncuentas'
    end
    object ObtenerCuentaPorFormaPagoFecha: TDateTimeField
      FieldName = 'Fecha'
      ReadOnly = True
    end
  end
  object CuentasDebitoTC: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoCuenta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select distinct TarjetasCredito.Descripcion, dbo.OcultarTarjeta(' +
        'DebitoAutomatico.CuentaDebito) as CuentaDebito'
      '  from DebitoAutomatico, TarjetasCredito, cuentas'
      
        ' where DebitoAutomatico.CodigoEntidad = TarjetasCredito.CodigoTa' +
        'rjeta'
      '   and DebitoAutomatico.TipoDebito = '#39'TC'#39
      '   and DebitoAutomatico.CodigoCliente = Cuentas.CodigoCliente'
      
        '   and DebitoAutomatico.CodigoDebitoAutomatico = Cuentas.CodigoD' +
        'ebitoAutomatico'
      '   and Cuentas.CodigoCuenta = :CodigoCuenta')
    Left = 648
    Top = 295
  end
  object CuentasDebitoCB: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoCuenta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select distinct Bancos.Descripcion, DebitoAutomatico.CuentaDebit' +
        'o '
      '  from DebitoAutomatico, Bancos, cuentas'
      ' where DebitoAutomatico.CodigoEntidad = Bancos.CodigoBanco'
      '   and DebitoAutomatico.TipoDebito = '#39'CB'#39
      '   and DebitoAutomatico.CodigoCliente = Cuentas.CodigoCliente'
      
        '   and DebitoAutomatico.CodigoDebitoAutomatico = Cuentas.CodigoD' +
        'ebitoAutomatico'
      '   and Cuentas.CodigoCuenta = :CodigoCuenta')
    Left = 648
    Top = 327
  end
  object plDetalle: TppDBPipeline
    DataSource = dsInformeFactura
    UserName = 'plDetalle'
    Left = 432
    Top = 327
  end
  object ppDBPipeline1: TppDBPipeline
    DataSource = dsFacturacionDetallada
    UserName = 'DBPipeline1'
    Left = 216
    Top = 326
  end
  object rbi_DetalleViajes: TRBInterface
    Report = rp_DetalleViajes
    OrderIndex = 0
    Left = 368
    Top = 328
  end
  object rbi_ProximoResumen: TRBInterface
    Report = rp_ProximoResumen
    OrderIndex = 0
    Left = 152
    Top = 328
  end
  object ObtenerDetalleFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDetalleFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaHoraInicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 36872d
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 176
    Top = 408
  end
end
