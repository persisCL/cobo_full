unit frmBajaForzada;


{
Firma       : SS_1156B_NDR_20140128
Description : Baja forzada masiva procesando un archivo .csv y dejando registro en operaciones interfaces


Firma       : SS_1156B_NDR_20140409
Description : Si la etiqueta COMPLETA (incluyendo el digito verificador) no corresponde a un ContractSerialNumber se informa como error

}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls,StrUtils, DB, ADODB,UtilDB,
  Math, DateUtils;                                                              //SS_1156B_NDR_20140409

type
  TfrmBajaForzadaMasiva = class(TForm)
    pnlOrigen: TPanel;
    btnBuscarArchivo: TSpeedButton;
    lblOrigen: TLabel;
    txtOrigen: TEdit;
    btnProcesar: TButton;
    btnSalir: TButton;
    pnlAvance: TPanel;
    OpenDialog: TOpenDialog;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    pnlBotones: TPanel;
    lblMensaje: TLabel;
    spSuspenderCuenta: TADOStoredProc;
    spActualizarLogOperacionesRegistrosProcesados: TADOStoredProc;
    spInsertarAuditoriaBajasForzadas: TADOStoredProc;
    InsertarHistoricoConvenio: TADOStoredProc;
    procedure btnBuscarArchivoClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
    DirectorioOrigen:AnsiString;
    FErrorGrave: Boolean;
    FCancelar: Boolean;         //Indica si ha sido cancelado
    FTotalRegistros: Integer;
    FCantidadErrores:integer;
    FRegistrosProcesados:integer;
    FCodigoOperacion : Integer;
    FPathNovedadXML: string;

  	Function  AbrirArchivo : Boolean;
    Function  GenerarRegistros(var Respuesta:integer):Boolean;
    Function  RegistrarOperacion : Boolean;
    function  AgregarErroresValidacion( ListaErrores: TStringList;
                                        CodigoOperacion: Integer;
                                        var Error: AnsiString): boolean;
    Function  GenerarReporteFinalizacionProceso(   CodigoOperacionInterfase,
                                                   CantidadRegistros,
                                                   CantidadErrores: Integer): Boolean;


  public
    { Public declarations }
  	Function  Inicializar(txtCaption:ANSIString; MDIChild:Boolean) : Boolean;
  end;

var
  frmBajaForzadaMasiva: TfrmBajaForzadaMasiva;
  FLista, FErrores: TStringList;

implementation
uses
  DMConnection,
  Util,
  UtilProc,
  PeaProcs,
  PeaTypes,
  XMLCAC,
  ConvenioToXML,
  ConstParametrosGenerales,
  ComunesInterfaces,
  frmRptBajasForzadas;
{$R *.dfm}

{ TfrmBajaForzadaMasiva }

function TfrmBajaForzadaMasiva.AbrirArchivo: Boolean;
resourcestring
	MSG_OPEN_FILE_ERROR = 'Error al abrir el archivo';
begin
    Result          := False;
    FLista.text     := FileToString(txtOrigen.text);
    FTotalRegistros := FLista.count;
    if FLista.text <> '' then  begin
        Result := True;
    end else begin
        FErrorGrave := True;
        MsgBox(MSG_OPEN_FILE_ERROR);
        Exit;
    end;
end;



function TfrmBajaForzadaMasiva.AgregarErroresValidacion(  ListaErrores: TStringList;
                                                          CodigoOperacion: Integer;
                                                          var Error: AnsiString): boolean;
var
    i: integer;
begin
    Result := False;
    i := 0;
    try
        if ListaErrores.Count > 0 then begin
            while (i < ListaErrores.Count) and (not FCancelar) do begin
                AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacion, ListaErrores.Strings[i]);
                inc(i);
            end;
        end;
        FCantidadErrores:=i;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
            Exit;
        end;
    end;
end;




procedure TfrmBajaForzadaMasiva.btnBuscarArchivoClick(Sender: TObject);
begin
	OpenDialog.InitialDir := DirectorioOrigen;
	if OpenDialog.execute then begin
     //btnBuscarArchivo.Enabled := False;
     txtOrigen.text := OpenDialog.FileName;
	end;
end;



procedure TfrmBajaForzadaMasiva.btnProcesarClick(Sender: TObject);
resourcestring
    MSG_PROCESS_CANCEL = 'Proceso cancelado';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
  	MSG_PROCESS_FINALLY_OK = 'Proceso finalizado con �xito';
    ERROR_INSERTING_VALIDATION_ERRORS   = 'No se pueden almacenar los errores. El proceso es cancelado.';
Const
    STR_OPEN_FILE               = 'Abrir Archivo...';
    STR_IMPORT                  = 'Importar Registros...';
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';

var
    Respuesta         : Integer;
    CantidadAEliminar : Integer;
    descError         : AnsiString;



    Function ObtenerCantidadErroresInterfaz(CodigoOperacion : Integer;var Cantidad : Integer) : Boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;



    Function ActualizarLog(CantidadErrores : Integer) : Boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
    var
         DescError : String;
    begin
         try
             Result := ActualizarLogOperacionesInterfaseAlFinal( DMConnections.BaseCAC,
                                                                 FCodigoOperacion,
                                                                 STR_ERRORES+ IntToStr(CantidadErrores),
                                                                 DescError);
         except
             on e: Exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
                Result := False;
             end;
         end;
    end;

    Function ValidarNombreArchivo(sArchivo:string):boolean;
    resourcestring
      STR_FILE_BAD_NAME='El nombre del archivo no es v�lido, debe cumplir con el formato BF_AAAAMMDD';
    begin
        Result:=True;
        if  (LeftStr(ExtractFileName(sArchivo),2)<>'BF') or
            (Copy(ExtractFileName(sArchivo),3,1)<>'_') or
            (StrToInt(Copy(ExtractFileName(sArchivo),4,8))=0) or
            (Copy(ExtractFileName(sArchivo),12,1)<>'.') 
             then
        begin
          ShowMessage(STR_FILE_BAD_NAME);
          Result:=False;
          txtOrigen.SelectAll;
        end;
    end;

    Function ValidarArchivo(sArchivo:string):boolean;
    resourcestring
      STR_FILE_NOT_EXISTS='Archivo ingresado no existe';
    begin
        Result:=True;
        if (not FileExists( sArchivo )) then
        begin
          Result:=False;
          ShowMessage(STR_FILE_NOT_EXISTS);
          txtOrigen.SelectAll;
        end
        else
        begin
          Result := ValidarNombreArchivo(sArchivo);
        end;
    end;




begin

     if not ValidarArchivo(txtOrigen.Text) then
     begin
       Exit;
     end;

  	btnProcesar.Enabled := False;
    PnlAvance.visible := True;       //El Panel de Avance es visible durante el procesamiento
    Respuesta := 0;
    KeyPreview := True;
  	try

      Lblmensaje.Caption := STR_REGISTER_OPERATION;
      Application.ProcessMessages;
      FCantidadErrores    := 0;
      FTotalRegistros     :=0;
      FRegistrosProcesados:=0;

      //Registra la Operacion de Interface
      if not RegistrarOperacion then begin
          FErrorGrave := True;
          Exit;
      end;


      //Abro el archivo
      Lblmensaje.Caption := STR_OPEN_FILE;
    	if not AbrirArchivo then
      begin
        FErrorGrave := True;
        Exit;
      end;


      //Procesa las bajas
      Lblmensaje.Caption := STR_IMPORT;
    	IF ((NOT GenerarRegistros(Respuesta)) OR (Respuesta <> 0)) then begin
          FErrorGrave := True;
          Exit;
      end;


      // Registro los errores de las infracciones
      if not AgregarErroresValidacion(FErrores, FCodigoOperacion, descError) then begin
          MsgBoxErr(ERROR_INSERTING_VALIDATION_ERRORS, descError, Caption, MB_ICONERROR);
          Exit;
      end;


      //Obtengo la cantidad de errores
      ObtenerCantidadErroresInterfaz(FCodigoOperacion, FCantidadErrores);


      //Actualizo el log al Final
      ActualizarLog(FCantidadErrores);


      //Muestro el Reporte de Finalizacion del Proceso
      GenerarReporteFinalizacionProceso(FCodigoOperacion,FTotalRegistros,FCantidadErrores);


	finally
      pbProgreso.Position := 0;     //Inicio la Barra de progreso
      PnlAvance.visible := False;   //Oculto el Panel de Avance
    	Lblmensaje.Caption := '';

      if FCancelar then
      begin
          //muestro mensaje que el proceso fue cancelado
          MsgBox(MSG_PROCESS_CANCEL, self.Caption, MB_OK + MB_ICONINFORMATION);
      end
      else if FErrorGrave then
      begin
          //muestro mensaje que el proceso finalizo con error
          MsgBox(MSG_PROCESS_FINALLY_WITH_ERROR, self.Caption, MB_OK + MB_ICONINFORMATION);
          if FErrores.Count <> 0 then
          begin
            //Muestro el Reporte de Finalizacion del Proceso
            GenerarReporteFinalizacionProceso(FCodigoOperacion,FTotalRegistros,FCantidadErrores);
          end;
      end
      else
      begin
        //informo que el proceso finalizo con exito
        MsgBox(MSG_PROCESS_FINALLY_OK, self.Caption, MB_OK + MB_ICONINFORMATION);
      end;
      KeyPreview := False;
	end
end;



function TfrmBajaForzadaMasiva.GenerarRegistros(var Respuesta: integer): Boolean;
resourcestring
  	MSG_PROCESS_ERROR = 'Error al Procesar Archivo de Bajas Forzadas';
    MSG_ERROR         = 'Error';
    MSG_ERROR_INSERT_LOG_SOR= 'Error al intentar insertar evento en la tabla Eventos.';
    MSG_CONFIRMATION        = 'Confirmar Cambios Convenio';
    MSG_XMLFILE_ERROR       = 'No se gener� archivo de novedades XML';

Const
    STR_OBSERVACION = 'Procesar archivo de bajas forzadas';
    COD_MODULO      = 109; //Archivo bajas forzadas
var
    DescError : AnsiString;


    Function MovimientosTxtToTable(var Respuesta : Integer) : Boolean;
    resourcestring
        MSG_RECORD_ERROR            = 'Error en importaci�n Registro';
        MSG_AUDIT_ERROR             = 'Error Registrando Auditoria Baja Forzada';
        MSG_RECORD_NOT_EXISTS       = 'No existe el convenio';
        MSG_COMBINATION_NOT_EXISTS  = 'Inexistente';
        MSG_COMBINATION_INACTIVE    = 'Inactiva';
        MSG_ETIQUETA_NO_EXISTE      = 'Etiqueta no est� asociada a un TAG';     //SS_1156B_NDR_20140409
        MSG_RECORD_INVALID_LENGTH   = 'Longitud Numero Convenio ';
        MSG_ERROR                   = 'Error';
    Const
        STR_CONVENIO = 'Convenio: ';
        STR_PATENTE  = 'Patente : ';
        STR_ETIQUETA = 'Etiqueta: ';
    var
        I : Integer;
        NumeroConvenio, Etiqueta, Patente : String;
        CodigoConvenio, IndiceVehiculo : integer;
        ExisteConvenio : Boolean;
        ExisteConvenioEtiquetaPatente:integer;
        CodigoConcesionaria:Integer;
        EsConvenioSinCuenta:Boolean;
        FRNUTUTF8: Integer;
        DescriError : string;
        spEventosSOR: TADOStoredProc;
        sQueryText:String;
        ContractSerialNumber:Integer;


    begin
        Result := False;
        Respuesta := 0;                    //Comienza importacion correctamente
        FCancelar := False;                //Permito que la importacion sea cancelada
        pbProgreso.Position := 0;          //Inicio la Barra de progreso
        pbProgreso.Max := FLista.Count-1;  //Establezco como maximo la cantidad de registros del memo
        i:=0;

      	while i < FLista.count do
        begin
        		Application.ProcessMessages;

        		if FCancelar then begin
                Respuesta := 2;
                pbProgreso.Position := 0;
                Exit;
            end;

            NumeroConvenio  := AnsiReplaceText(ParseParamByNumber(FLista.strings[i],1,','),chr(39),'');
            Etiqueta        := AnsiReplaceText(ParseParamByNumber(FLista.strings[i],2,','),chr(39),'');
            Patente         := AnsiReplaceText(ParseParamByNumber(FLista.strings[i],3,','),chr(39),'');

            CodigoConvenio := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT ISNULL(CodigoConvenio,0) FROM Convenio WITH (NOLOCK) WHERE NumeroConvenio=%s',[QuotedStr(numeroconvenio)]));
            if CodigoConvenio>0 then
            begin
              ContractSerialNumber := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.EtiquetaToSerialNumber('+''''+Etiqueta+''''+')');
              IndiceVehiculo := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT Cuentas.IndiceVehiculo FROM MaestroVehiculos WITH (NOLOCK) '+
                                                                               'INNER JOIN Cuentas ON ' +
                                                                               '    Cuentas.CodigoConvenio=%s ' +
                                                                               'AND Cuentas.CodigoVehiculo=MaestroVehiculos.CodigoVehiculo ' +
                                                                               'WHERE MaestroVehiculos.Patente=%s '+
                                                                               'AND ISNULL(ContractSerialNumber,ContractSerialNumberOtraConsecionaria)=%s '+
                                                                               'AND CodigoEstadoCuenta=dbo.CONST_ESTADO_CUENTA_ALTA()',[IntToStr(CodigoConvenio),QuotedStr(Patente),IntToStr(ContractSerialNumber)]));
              {verifico que la longitud del numero de convenio sea de 17 caracteres}
              //if  Length(Numeroconvenio) = 17 then                                        //SS_1147Q_NDR_20141202
              if  Length(Numeroconvenio) = LargoNumeroConvenioConcesionariaNativa() then      //SS_1147Q_NDR_20141202
              begin

                {verifico que le convenio exista}
                Existeconvenio := trim(QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerCodigoConvenio (''%s'')',[numeroconvenio]))) <> '';
                if Existeconvenio = true then
                begin
                  ExisteConvenioEtiquetaPatente := QueryGetValueInt(DMConnections.BaseCAC,
                                                                    Format( 'SELECT dbo.ExisteConvenioEtiquetaPatente (''%s'',''%s'',''%s'')',
                                                                            [numeroconvenio,Etiqueta,Patente]
                                                                          )
                                                                    );
                  //Existe convenio / patente / etiqueta en cuenta activa
                  if ExisteConvenioEtiquetaPatente=1 then
                  begin
                    try

                      InsertarHistoricoConvenio.Close;
                      InsertarHistoricoConvenio.Parameters.Refresh;
                      InsertarHistoricoConvenio.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                      InsertarHistoricoConvenio.ExecProc;


                      with spSuspenderCuenta do
                      begin
                        Parameters.Refresh;
                        Parameters.ParamByName('@CodigoConvenio').Value     := CodigoConvenio;
                        Parameters.ParamByName('@IndiceVehiculo').Value     := IndiceVehiculo;
                        Parameters.ParamByName('@Suspendida').Value         := NowBase(DMConnections.BaseCAC);
                        //Parameters.ParamByName('@DeadlineSuspension').Value := NowBase(DMConnections.BaseCAC);				//SS_1156B_NDR_20140409
                        Parameters.ParamByName('@DeadlineSuspension').Value := IncDay(NowBase(DMConnections.BaseCAC) , 30);   //SS_1156B_NDR_20140409
                        Parameters.ParamByName('@SuspensionForzada').Value  := True;
                        Parameters.ParamByName('@Usuario').Value            := UsuarioSistema;
                        ExecProc;
                        FRegistrosProcesados:= FRegistrosProcesados + 1;
                      end;

                      try
                        with spInsertarAuditoriaBajasForzadas do
                        begin
                          Parameters.Refresh;
                          Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                          Parameters.ParamByName('@NumeroConvenio').Value           := Numeroconvenio;
                          Parameters.ParamByName('@Etiqueta').Value                 := Etiqueta;
                          Parameters.ParamByName('@Patente').Value                  := Patente;
                          Parameters.ParamByName('@Usuario').Value                  := UsuarioSistema;
                          ExecProc;
                        end;


                        //Generar XML para el RNUT si aplica
                        try
                           CodigoConcesionaria := QueryGetValueInt( DMConnections.BaseCAC,
                                                                    Format('SELECT CodigoConcesionaria FROM Convenio WITH (NOLOCK) WHERE CodigoConvenio=%d',
                                                                    [CodigoConvenio])
                                                                   );

                           if (CodigoConcesionaria = CODIGO_CN) then
                           begin                                                                                                                //SS_628_ALA_20111202
                               if not GenerarNovedadXML(DMConnections.BaseCAC,
                                                        CodigoConvenio, CodigoConcesionaria, FPathNovedadXML,iif(FRNUTUTF8 = 1, True, False), DescriError
                                                       ) then
                               begin
                                 if DescriError <> EmptyStr then
                                 begin
                                   try
                                       spEventosSOR :=  TADOStoredProc.Create(nil);
                                       spEventosSOR.Connection := DMConnections.BaseSOR;
                                       spEventosSOR.ProcedureName := 'CrearEvento';
                                       spEventosSOR.Parameters.Refresh;
                                       spEventosSOR.Parameters.ParamByName('@CodigoTipoEvento').Value := 213;
                                       spEventosSOR.Parameters.ParamByName('@CodigoSistema').Value := SistemaActual;
                                       spEventosSOR.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                                       spEventosSOR.Parameters.ParamByName('@NombreArchivo').Value := Application.Title;
                                       spEventosSOR.Parameters.ParamByName('@Detalle').Value := DescriError + ' ' +
                                       'CONVENIO: ' + NumeroConvenio;
                                       spEventosSOR.ExecProc;
                                   except
                                       on e: Exception do
                                       begin
                                           MsgBoxErr(MSG_ERROR_INSERT_LOG_SOR, e.Message, Self.Caption, MB_ICONSTOP);
                                       end;
                                   end;
                                 end;
                               end;
                           end;
                        except
                            on e: exception do begin
                                FErrorGrave := True;
                                MsgBoxErr(STR_CONVENIO + NumeroConvenio + STR_PATENTE + Patente + STR_ETIQUETA + Etiqueta + ' - ' + MSG_AUDIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                      except
                          on e: exception do begin
                              FErrorGrave := True;
                              MsgBoxErr(STR_CONVENIO + NumeroConvenio + STR_PATENTE + Patente + STR_ETIQUETA + Etiqueta + ' - ' + MSG_AUDIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                              Exit;
                          end;
                      end;
                    except
                        on e: exception do begin
                            FErrorGrave := True;
                            MsgBoxErr(STR_CONVENIO + NumeroConvenio + ' - ' + MSG_RECORD_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                            Exit;
                        end;
                    end;
                  end
                  else
                  begin
                    //Existe convenio / patente / etiqueta en cuenta que no esta activa, no se puede dar la baja, se informa como error
                    if ExisteConvenioEtiquetaPatente=2 then
                    begin
                      FErrores.Add(STR_CONVENIO + NumeroConvenio + STR_PATENTE + Patente + STR_ETIQUETA + Etiqueta + ' : ' + MSG_COMBINATION_INACTIVE);
                    end
                    //NO EXISTE ContractSerialNumber para esa Etiqueta en los Maestros de Tags                                                              //SS_1156B_NDR_20140409
                    else if ExisteConvenioEtiquetaPatente=3 then                                                                                            //SS_1156B_NDR_20140409
                    begin                                                                                                                                   //SS_1156B_NDR_20140409
                      FErrores.Add(STR_CONVENIO + NumeroConvenio + STR_PATENTE + Patente + STR_ETIQUETA + Etiqueta + ' : ' + MSG_ETIQUETA_NO_EXISTE);       //SS_1156B_NDR_20140409
                    end                                                                                                                                     //SS_1156B_NDR_20140409
                    else                                                                                                                                    //SS_1156B_NDR_20140409
                    //NO EXISTE Existe convenio / patente / etiqueta                                                                                        //SS_1156B_NDR_20140409
                    begin
                      FErrores.Add(STR_CONVENIO + NumeroConvenio + STR_PATENTE + Patente + STR_ETIQUETA + Etiqueta + ' : ' + MSG_COMBINATION_NOT_EXISTS);
                    end;
                  end;
                end else begin
                    //NO EXISTE EL CONVENIO
                    FErrores.Add(STR_CONVENIO + NumeroConvenio + ' - ' + MSG_RECORD_NOT_EXISTS);
                end;
              end
              else
              begin
                //EL NUMERO DE CONVENIO NO CORRESPONDE AL FORMATO
                FErrores.Add(STR_CONVENIO + NumeroConvenio + ' - ' + MSG_RECORD_INVALID_LENGTH);
              end;
            end;
            pbProgreso.Position := pbProgreso.Position + 1;  //Muestro el progreso
            Application.ProcessMessages;                    //Refresco la pantalla
            i := i + 1;
        end;

        spActualizarLogOperacionesRegistrosProcesados.Close;
        with  spActualizarLogOperacionesRegistrosProcesados do
        begin
              Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
              Parameters.ParamByName('@RegistrosAProcesar').Value       := FRegistrosProcesados;
              Parameters.ParamByName('@LineasArchivo').Value            := FTotalRegistros;
              ExecProc;
        end;

        Result := True;
    end;

begin
    Result := False;
    try
        Result := MovimientosTxtToTable(Respuesta);
    except
        on e: Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_PROCESS_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;





function TfrmBajaForzadaMasiva.GenerarReporteFinalizacionProceso( CodigoOperacionInterfase,
                                                                  CantidadRegistros,
                                                                  CantidadErrores: Integer): Boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de finalizaci�n de proceso';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Proceso de Bajas Forzadas';
var
    F : TFRptBajasForzadas;
begin
    Result := False;
    try
        Application.CreateForm(TFRptBajasForzadas, F);
        if not F.Inicializar(CodigoOperacionInterfase, CantidadRegistros, CantidadErrores, REPORT_TITLE) then f.Release;
        Result := True;
    except
       on e: Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;




function TfrmBajaForzadaMasiva.Inicializar(txtCaption:AnsiString; MDIChild: Boolean): Boolean;
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF;
        MSG_PARAM_XML_NOT_FOUND			      = 'Ha ocurrido un error intentando obtener el par�metro %s';
        MSG_PARAM_XML_TITLE					      = 'Error obteniendo un par�metro general de la interfaz RNUT-XML';

    const
        BAJA_FORZADA_DIR_ORIGEN            = 'BAJA_FORZADA_DIR_ORIGEN';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BAJA_FORZADA_DIR_ORIGEN , DirectorioOrigen) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BAJA_FORZADA_DIR_ORIGEN;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                DirectorioOrigen := GoodDir(DirectorioOrigen);
                if  not DirectoryExists(DirectorioOrigen) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + DirectorioOrigen;
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

           try
                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'PATH_ARCHIVO_SALIDA_XML' , FPathNovedadXML) then begin
                    DescError := Format(MSG_PARAM_XML_NOT_FOUND, ['PATH_ARCHIVO_SALIDA_XML']);
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FPathNovedadXML := GoodDir(FPathNovedadXML);
                if  not DirectoryExists(FPathNovedadXML) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FPathNovedadXML;
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;



        finally
            if (Result = False) then begin
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
  	MSG_ERROR = 'Error';

begin
  	Result := False;
  	CenterForm(Self);
    try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and VerificarParametrosGenerales;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Caption             := AnsiReplaceStr(txtCaption, '&', '');  //titulo
  	//btnProcesar.enabled := False;   //Procesar
  	lblmensaje.Caption  := '';      //limpio el indicador
    PnlAvance.visible   := False;   //Oculto el Panel de Avance
    FErrores.Clear;                 //Inicio la lista de errores
    FLista.Clear;
    FCodigoOperacion    := 0;       //inicializo codigo de operacion
end;



function TfrmBajaForzadaMasiva.RegistrarOperacion: Boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Procesar archivo de bajas forzadas';
    COD_MODULO = 109; //Archivo bajas forzadas
var
	DescError : String;
begin
   Result := RegistrarOperacionEnLogInterface(  DMConnections.BaseCAC,
                                                COD_MODULO,
                                                ExtractFileName(txtOrigen.text),
                                                UsuarioSistema,
                                                STR_OBSERVACION,
                                                False,
                                                False ,
                                                NowBase(DMConnections.BaseCAC),
                                                0,
                                                FCodigoOperacion,
                                                DescError);
   if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;




procedure TfrmBajaForzadaMasiva.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;



procedure TfrmBajaForzadaMasiva.btnSalirClick(Sender: TObject);
begin
    close;
end;



initialization
    FLista   := TStringList.Create;
    FErrores := TStringList.Create;
finalization
    FreeAndNil(Flista);
    FreeAndNil(FErrores);
end.

