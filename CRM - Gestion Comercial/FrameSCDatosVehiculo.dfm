object FmeSCDatosVehiculo: TFmeSCDatosVehiculo
  Left = 0
  Top = 0
  Width = 423
  Height = 249
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  TabOrder = 0
  TabStop = True
  object Label13: TLabel
    Left = 48
    Top = 84
    Width = 49
    Height = 13
    Caption = 'Patente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label14: TLabel
    Left = 57
    Top = 109
    Width = 40
    Height = 13
    Caption = 'Marca:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label20: TLabel
    Left = 59
    Top = 135
    Width = 38
    Height = 13
    Caption = 'Modelo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label47: TLabel
    Left = 70
    Top = 182
    Width = 27
    Height = 13
    Caption = 'A'#241'o:'
    FocusControl = txt_anio
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblTelevia: TLabel
    Left = 48
    Top = 8
    Width = 49
    Height = 13
    Caption = 'Telev'#237'a:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label21: TLabel
    Left = 70
    Top = 159
    Width = 27
    Height = 13
    Caption = 'Color:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lblVehiculoRobado: TLabel
    Left = 243
    Top = 84
    Width = 109
    Height = 13
    AutoSize = False
    Caption = 'lblVehiculoRobado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object lblCategoriaInterurbana: TLabel
    Left = 0
    Top = 54
    Width = 97
    Height = 13
    Caption = 'Cat. Interurbana:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblCategoriaUrbana: TLabel
    Left = 24
    Top = 31
    Width = 73
    Height = 13
    Caption = 'Cat. Urbana:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object cb_marca: TComboBox
    Left = 101
    Top = 105
    Width = 236
    Height = 21
    Style = csDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 6
  end
  object txt_anio: TNumericEdit
    Left = 101
    Top = 178
    Width = 47
    Height = 21
    Color = 16444382
    MaxLength = 4
    TabOrder = 8
    OnExit = txt_anioExit
  end
  object txtDescripcionModelo: TEdit
    Left = 101
    Top = 131
    Width = 236
    Height = 21
    Hint = 'Modelo'
    CharCase = ecUpperCase
    ParentShowHint = False
    ShowHint = True
    TabOrder = 7
    OnKeyPress = txtDescripcionModeloKeyPress
  end
  object txtPatente: TEdit
    Left = 101
    Top = 80
    Width = 108
    Height = 21
    Hint = 'Patente'
    CharCase = ecUpperCase
    Color = 16444382
    MaxLength = 8
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnChange = txtPatenteChange
    OnEnter = txtPatenteEnter
    OnKeyPress = PatentesKeyPress
  end
  object txtDigitoVerificador: TEdit
    Left = 214
    Top = 80
    Width = 26
    Height = 21
    Hint = 'D'#237'gito Verificador'
    CharCase = ecUpperCase
    Color = 16444382
    MaxLength = 1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    Visible = False
    OnChange = txtPatenteChange
    OnKeyPress = DigitosVerificadoresKeyPress
  end
  object cb_color: TComboBox
    Left = 101
    Top = 155
    Width = 126
    Height = 21
    Style = csDropDownList
    Enabled = False
    ItemHeight = 13
    TabOrder = 9
    TabStop = False
  end
  object GBListas: TGroupBox
    Left = 0
    Top = 202
    Width = 422
    Height = 46
    Align = alCustom
    Caption = '  Status de Listas en el Tag  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    object ledVerde: TLed
      Left = 18
      Top = 20
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clGreen
      ColorOff = clBtnFace
    end
    object ledGris: TLed
      Left = 130
      Top = 20
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clGray
      ColorOff = clBtnFace
    end
    object ledAmarilla: TLed
      Left = 233
      Top = 20
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clYellow
      ColorOff = clBtnFace
    end
    object ledNegra: TLed
      Left = 357
      Top = 20
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clBlack
      ColorOff = clBtnFace
    end
    object Label5: TLabel
      Left = 39
      Top = 20
      Width = 28
      Height = 13
      Caption = 'Verde'
    end
    object Label6: TLabel
      Left = 151
      Top = 20
      Width = 18
      Height = 13
      Caption = 'Gris'
    end
    object Label7: TLabel
      Left = 254
      Top = 20
      Width = 36
      Height = 13
      Caption = 'Amarilla'
    end
    object Label8: TLabel
      Left = 378
      Top = 20
      Width = 29
      Height = 13
      Caption = 'Negra'
    end
  end
  object BtnRecuperar: TButton
    Left = 358
    Top = 80
    Width = 61
    Height = 21
    Caption = 'Recuperar'
    Enabled = False
    TabOrder = 5
    Visible = False
    OnClick = BtnRecuperarClick
  end
  object cbbCategoriaInterurbana: TVariantComboBox
    Left = 101
    Top = 50
    Width = 236
    Height = 21
    Style = vcsDropDownList
    Enabled = False
    ItemHeight = 13
    TabOrder = 2
    Items = <>
  end
  object cbbCategoriaUrbana: TVariantComboBox
    Left = 101
    Top = 27
    Width = 236
    Height = 21
    Style = vcsDropDownList
    Enabled = False
    ItemHeight = 13
    TabOrder = 1
    Items = <>
  end
  object txtNumeroTelevia: TEdit
    Left = 101
    Top = 3
    Width = 126
    Height = 21
    CharCase = ecUpperCase
    Color = 16444382
    MaxLength = 11
    TabOrder = 0
    OnChange = txtNumeroTeleviaChange
    OnExit = txtNumeroTeleviaExit
    OnKeyPress = txtNumeroTeleviaKeyPress
  end
end
