object NavWindowDetalleComprobante: TNavWindowDetalleComprobante
  Left = 229
  Top = 183
  Anchors = []
  AutoScroll = False
  Caption = 'Detalle de Comprobante'
  ClientHeight = 477
  ClientWidth = 742
  Color = 14732467
  Constraints.MinHeight = 504
  Constraints.MinWidth = 720
  ParentFont = True
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  DesignSize = (
    742
    477)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 5
    Top = 1
    Width = 733
    Height = 230
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Detalle del Comprobante'
    TabOrder = 0
    DesignSize = (
      733
      230)
    object dbgDetalleComprobante: TDBListEx
      Left = 5
      Top = 15
      Width = 722
      Height = 209
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 93
          Header.Caption = 'Concesionaria'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Concesionaria'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 60
          Header.Caption = 'Fecha'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Fecha'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 94
          Header.Caption = 'C'#243'digo Cuenta'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'CodigoCuenta'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 293
          Header.Caption = 'Descripci'#243'n'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Descripcion'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 89
          Header.Caption = 'Importe'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriImporte'
        end>
      DataSource = dsDatosComprobante
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object gbViajesCuenta: TGroupBox
    Left = 5
    Top = 234
    Width = 732
    Height = 210
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Viajes facturados a la cuenta '
    TabOrder = 1
    DesignSize = (
      732
      210)
    object dbgViajesFacturados: TDBListEx
      Left = 5
      Top = 15
      Width = 721
      Height = 189
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 92
          Header.Caption = 'Concesionaria'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Concesionaria'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          Width = 63
          Header.Caption = 'Viaje'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroViaje'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 52
          Header.Caption = 'Patente'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Categor'#237'a'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Categoria'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 140
          Header.Caption = 'Fecha/Hora de Inicio'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHoraInicio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 125
          Header.Caption = 'Puntos de Cobro'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'PuntosCobro'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 72
          Header.Caption = 'Importe'
          Header.Font.Charset = ANSI_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriImporte'
        end>
      DataSource = dsFacturacionDetallada
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnLinkClick = dbgViajesFacturadosLinkClick
    end
  end
  object btnIMprimirComprobante: TDPSButton
    Left = 600
    Top = 449
    Width = 134
    Anchors = [akRight, akBottom]
    Caption = '&Reimprimir Comprobante'
    TabOrder = 3
    OnClick = btnIMprimirComprobanteClick
  end
  object btnImprimirDetalleViajes: TDPSButton
    Left = 458
    Top = 449
    Width = 136
    Anchors = [akRight, akBottom]
    Caption = 'Imprimir &Detalle de Viajes'
    TabOrder = 2
    OnClick = btnImprimirDetalleViajesClick
  end
  object dsDatosComprobante: TDataSource
    DataSet = ObtenerDatosComprobante
    OnDataChange = dsDatosComprobanteDataChange
    Left = 168
    Top = 64
  end
  object ObtenerDatosComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosComprobante'
    Parameters = <
      item
        Name = '@TipoComprobante'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        DataType = ftFloat
        Value = Null
      end>
    Left = 200
    Top = 64
  end
  object dsFacturacionDetallada: TDataSource
    DataSet = ObtenerFacturacionDetallada
    Left = 168
    Top = 311
  end
  object ObtenerFacturacionDetallada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerFacturacionDetallada'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end>
    Left = 200
    Top = 311
  end
end
