{--------------------------------------------------------------------------------
Revision : 2
    Date: 03/03/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
                  
Etiqueta    : 20160315 MGO
Descripción : Se utiliza InstallIni en vez de ApplicationIni

--------------------------------------------------------------------------------}
unit GeneradorDeIni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  StdCtrls, ExtCtrls, UtilProc, Variants, Util, UtilDB, Peatypes,
  DPSControls, DmiCtrls, PeaProcs, DB, ADODB, DMConnection,
  Dialogs;

type
  TFormGeneradorIni = class(TForm)
	Label1: TLabel;
	Label2: TLabel;
	Bevel1: TBevel;
    edtPuntoEntrega: TNumericEdit;
    edtPuntoVenta: TNumericEdit;
    lbl_FuenteSolicitud: TLabel;
    txt_FuenteSolicitud: TNumericEdit;
    VerificarPuntoEntregaVenta2: TADOStoredProc;
	lblPOS: TLabel;
	Label4: TLabel;
    btnAceptar: TButton;
    btnCancelar: TButton;
	procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure edtPuntoEntregaChange(Sender: TObject);
  private
	{ Private declarations }
  public
	{ Public declarations }
	Function Inicializa(MostrarFuenteSolicitud : Boolean = True; MostrarNumeroPOS: Boolean = False): Boolean;
  end;

var
  FormGeneradorIni: TFormGeneradorIni;

implementation



{$R *.DFM}

resourcestring
  MSG_DEBE_COMPLETAR_PE =  'Debe completar Punto Entrega';
  MSG_DEBE_COMPLETAR_PV =  'Debe completar Punto Venta';
  MSG_DEBE_COMPLETAR_FUENTE = 'Debe completar la Fuente de Solicitud.';
  MSG_NO_EXISTE_PUNTO_ENTREGA_VENTA = 'No existe el punto de entrega o de venta.';
  CAPTION_NO_HAY_POS_ASOCIADO = 'No hay POSNET asociado al Punto de Entrega';
{-----------------------------------------------------------------------------
  Function Name: 
  Author:
  Date Created:  /  /
  Description:
  Parameters: MostrarFuenteSolicitud : Boolean = True; MostrarNumeroPOS: Boolean = False
  Return Value: Boolean

  Revision : 1
    Date: 03/03/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
Function TFormGeneradorIni.Inicializa(MostrarFuenteSolicitud : Boolean = True; MostrarNumeroPOS: Boolean = False): Boolean;
resourcestring
	MSG_YA_CONFIGURADO = 'Archivo de Inicio ya se encuentra configurado.';
	CAPTION_PUESTO_CONFIGURADO_PARA_POS = 'Puesto Configurado para utilizar POSNET';
var
 APuntoEntrega, APuntoVenta, AFuenteSolicitud : integer;
 UsaPos: Boolean;
 NroPos: Integer;
begin
	result:=True;
    //{ INICIO : 20160315
	APuntoEntrega:= ApplicationIni.ReadInteger('General', 'PuntoEntrega', -1);
	APuntoVenta:= ApplicationIni.ReadInteger('General', 'PuntoVenta', -1);
	AFuenteSolicitud:= ApplicationIni.ReadInteger('General','FuenteSolicitud',-1);
	UsaPos := ApplicationIni.ReadInteger('General', 'UsaPOSNET', -1) = 1;
    {
    APuntoEntrega:= InstallIni.ReadInteger('General', 'PuntoEntrega', -1);
	APuntoVenta:= InstallIni.ReadInteger('General', 'PuntoVenta', -1);
	AFuenteSolicitud:= InstallIni.ReadInteger('General','FuenteSolicitud',-1);
	UsaPos := InstallIni.ReadInteger('General', 'UsaPOSNET', -1) = 1;
    }
    // FIN : 20160315

	if APuntoEntrega > -1 then edtPuntoEntrega.ValueInt := APuntoEntrega;
	if APuntoVenta > -1 then edtPuntoVenta.ValueInt := APuntoVenta;
	if AFuenteSolicitud > -1 then txt_FuenteSolicitud.Value:=AFuenteSolicitud;

	if MostrarNumeroPOS then begin
		NroPos := QueryGetValueInt( DMConnections.BaseCAC,
		  format('select isnull((select isNull(cast(NumeroPOS as integer),0) from PuntosVenta WITH (NOLOCK) '
		  +' where CodigoPuntoEntrega = %d and CodigoPuntoVenta = %d),-1)',[APuntoEntrega, APuntoVenta]));

		lblPos.Caption := QueryGetValue( DMConnections.BaseCAC,Format(
		  'Select (LTRIM(RTRIM(CodigoComercio))+'' - ''+LTRIM(RTRIM(Descripcion))) from POS  WITH (NOLOCK) '
		  +' where NumeroPOS = %d ' ,[NroPOS]));
		if UsaPos then begin
		   lblPos.Caption := lblPos.Caption + CrLf + CAPTION_PUESTO_CONFIGURADO_PARA_POS;
		end;
   end;

	txt_FuenteSolicitud.Visible := MostrarFuenteSolicitud;
	lbl_FuenteSolicitud.Visible := MostrarFuenteSolicitud;
end;

procedure TFormGeneradorIni.btnAceptarClick(Sender: TObject);
begin
	if not ValidateControls([edtPuntoVenta,
        edtPuntoEntrega,
        txt_FuenteSolicitud],
	      [(Trim(edtPuntoVenta.Text) <> ''),
        (Trim(edtPuntoEntrega.Text) <> ''),
        not(txt_FuenteSolicitud.Visible) or (trim(txt_FuenteSolicitud.Text)<>'')],
        self.Caption,
	      [MSG_DEBE_COMPLETAR_PV,
        MSG_DEBE_COMPLETAR_PE,
        MSG_DEBE_COMPLETAR_FUENTE]) then begin

    // Colocar ModalResult en None para que en caso de error en los datos igresados, el form no se cierre.
    ModalResult := mrNone;
		Exit;
	end;

    if not VerificarPuntoEntregaVenta(DMConnections.BaseCAC, edtPuntoEntrega.ValueInt, edtPuntoVenta.ValueInt) then begin
       MsgBoxBalloon(MSG_NO_EXISTE_PUNTO_ENTREGA_VENTA, self.Caption, MB_ICONERROR, edtPuntoEntrega);
       Exit;
    end;

    //{ INICIO : 20160315 MGO
    ApplicationIni.WriteInteger('General', 'PuntoEntrega', edtPuntoEntrega.ValueInt);
    ApplicationIni.WriteInteger('General', 'PuntoVenta', edtPuntoVenta.ValueInt);
    if txt_FuenteSolicitud.Visible then ApplicationIni.WriteInteger('General', 'FuenteSolicitud', txt_FuenteSolicitud.ValueInt);
    {
    InstallIni.WriteInteger('General', 'PuntoEntrega', edtPuntoEntrega.ValueInt);
    InstallIni.WriteInteger('General', 'PuntoVenta', edtPuntoVenta.ValueInt);
    if txt_FuenteSolicitud.Visible then InstallIni.WriteInteger('General', 'FuenteSolicitud', txt_FuenteSolicitud.ValueInt);
    }
    // FIN : 20160315 MGO

    ModalResult := mrOk;
end;

procedure TFormGeneradorIni.btnCancelarClick(Sender: TObject);
begin
	close;
end;
{-----------------------------------------------------------------------------
  Function Name: 
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 03/03/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormGeneradorIni.edtPuntoEntregaChange(Sender: TObject);
var
    PtoEntrega: Integer;
    NroPOS: Integer;
begin
	if ActiveControl <> Sender then Exit;

	try
		if edtPuntoEntrega.ValueInt > 0 then begin
			PtoEntrega := edtPuntoEntrega.ValueInt;
			NroPos := QueryGetValueInt( DMConnections.BaseCAC,
						format('select isnull((select isNull(cast(NumeroPOS as integer),0) from PuntosEntrega with (noLock)'
							  +' where CodigoPuntoEntrega = %d ),-1)',[PtoEntrega] ));
			if NroPos = 0 then begin
				lblPOS.Caption := CAPTION_NO_HAY_POS_ASOCIADO;
				Exit;
			end;
			if NroPos = -1 then begin
				MsgBoxBalloon(MSG_NO_EXISTE_PUNTO_ENTREGA_VENTA,self.Caption,MB_ICONERROR,edtPuntoEntrega);
				lblPOS.Caption := '';
				Exit;
			end;
			lblPos.Caption :=  QueryGetValue( DMConnections.BaseCAC,Format(
			'Select (LTRIM(RTRIM(CodigoComercio))+'' - ''+LTRIM(RTRIM(Descripcion))) from POS with (noLock)'
			+' where NumeroPOS = %d '
			,[NroPOS]));
		end
		else begin
			lblPOS.Caption := '';
		end;
	except
		lblPOS.Caption := '';
	end;
end;

end.
