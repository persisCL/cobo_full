{-------------------------------------------------------------------------------
Firma       : SS_660_MVI_20130909
Description : Se agrega validaci�n para detectar si alguno de los convenios del cliente,
              est� en Lista Amarilla, para deshabilitar la opci�n de modificar.


Firma		: SS_660_MCA_20131114
Description : Se muestra un mensaje si el convenio esta en LA y no permite agregar nuevo convenio

Firma		: SS_660_MCA_20140114
Descripcion	: Se comenta codigo ya que la validaci�n se realiza en el formulario de contacto
-------------------------------------------------------------------------------}
unit frmMultimpleMedioPago;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx, DB, ADODB,
  DMConnection, UtilProc, PeaProcs,
  SysUtilsCN,                                                                    //SS_660_MVI_20130909
  UtilDB;																		//SS_660_MCA_20131114

type
  TformMultimpleMedioPago = class(TForm)
    pnl_Botones: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbl_RUT: TLabel;
	Label2: TLabel;
    lbl_Nombre: TLabel;
    Label3: TLabel;
    dbl_convenio: TDBListEx;
    ObtenerConvenios: TADOStoredProc;
    DataSource: TDataSource;
    ObtenerDatosPersona: TADOStoredProc;
    btn_Cancelar: TButton;
    btn_Nuevo: TButton;
    btn_Modificar: TButton;
    procedure btn_NuevoClick(Sender: TObject);
    procedure btn_ModificarClick(Sender: TObject);
    procedure ObtenerConveniosAfterOpen(DataSet: TDataSet);
 //   procedure FormShow(Sender: TObject);                                             //SS_660_MVI_20130909
    procedure dbl_convenioClick(Sender: TObject);                                      //SS_660_MVI_20130909
    procedure FormActivate(Sender: TObject);                                           //SS_660_MVI_20130909
  private
    FNuevo: Boolean;
    FModificar: Boolean;
    FCodigoConvenioModif: Integer;
    FNumeroConvenio: String;
    procedure SetNuevo(const Value: Boolean);
    procedure SetModificar(const Value: Boolean);
    procedure SetCodigoConvenioModif(const Value: Integer);
	procedure SetNumeroConvenio(const Value: String);
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(CodigoDocumento, NumeroDocumento: String): Boolean;
    property Nuevo: Boolean read FNuevo write SetNuevo;
    property Modificar: Boolean read FModificar write SetModificar;
    property CodigoConvenioModif: Integer read FCodigoConvenioModif write SetCodigoConvenioModif;
    property NumeroConvenio: String read FNumeroConvenio write SetNumeroConvenio;
  end;

resourcestring                                                                                                          //SS_660_MVI_20130909
    MSG_CONVENIO_EN_LA = 'No se puede seleccionar el convenio %s '#13#10'debido a que se encuentra en Lista Amarilla';  //SS_660_MVI_20130909
var
  formMultimpleMedioPago: TformMultimpleMedioPago;

implementation

uses RStrings;

{$R *.dfm}

procedure TformMultimpleMedioPago.btn_NuevoClick(Sender: TObject);
begin
    Nuevo := True;
    ModalResult := mrOk;
end;


{--------------------------------------------------------------------
Procedure Name  : TformMultimpleMedioPago.dbl_convenioClick(
Date Created    : 23/09/2013
Description     : Al pinchar sobre uno de los item de la grilla, se validar� si
                  este est� en Lista Amarilla.
Firma           : SS_660_MVI_20130909
--------------------------------------------------------------------}
procedure TformMultimpleMedioPago.dbl_convenioClick(Sender: TObject);
begin
    //if dbl_convenio.DataSource.DataSet.FieldByName('ListaAmarilla').AsInteger > 0 then begin //SS_660_MCA_20131114
    if dbl_convenio.DataSource.DataSet.FieldByName('ListaAmarilla').AsBoolean then begin       //SS_660_MCA_20131114

        DBListExMostrarMensajeBalloon(  (Sender as TDBListEx),
                                        'NumeroConvenioFormateado',
                                        Caption,
                                        Format(MSG_CONVENIO_EN_LA, [dbl_convenio.DataSource.DataSet.FieldByName('NumeroConvenioFormateado').AsString]));

        btn_Modificar.Enabled := False;

    end
    else
        btn_Modificar.Enabled := True;
end;



procedure TformMultimpleMedioPago.btn_ModificarClick(Sender: TObject);
begin
    Modificar := True;
    CodigoConvenioModif := ObtenerConvenios.fieldbyName('CodigoConvenio').AsInteger;
    NumeroConvenio := ObtenerConvenios.fieldbyName('NumeroConvenio').AsString;
    ModalResult := mrOk;

end;

function TformMultimpleMedioPago.Inicializar(
  CodigoDocumento, NumeroDocumento: String): Boolean;
resourcestring
	MSG_ERROR_INICIALIZACION_FORMULARIO_CONVENIOS = 'Error al inicializar formulario de multiples convenios';
    //MSG_INHABILITADO_LISTA_AMARILLA = 'RUT inhabilitado por Lista Amarilla, No es posible ingresar un nuevo convenio.'; //SS_660_MCA_20140114 //SS_660_MCA_20131114
    //MSG_PROCESO_LISTA_AMARILLA = 'El RUT a lo menos tiene un convenio en proceso de Lista Amarilla';					  //SS_660_MCA_20140114 //SS_660_MCA_20131114
begin
	Result := False;
	try
		with ObtenerConvenios do begin
            Parameters.ParamByName('@CodigoDocumento').Value := CodigoDocumento;
            Parameters.ParamByName('@NumeroDocumento').Value := NumeroDocumento;
			Parameters.ParamByName('@SoloActivos').Value     := True;
			Parameters.ParamByName('@SoloContanera').Value   := True;
            Open;
        end;

        IF ObtenerConvenios.FieldByName('ListaAmarilla').AsBoolean then             			//SS_660_MCA_20131114
        begin                                                                       			//SS_660_MCA_20131114
            //MsgBox(MSG_INHABILITADO_LISTA_AMARILLA, self.Caption, MB_ICONWARNING);  			//SS_660_MCA_20140114 //SS_660_MCA_20131114
            btn_Nuevo.Enabled := False;                                             			//SS_660_MCA_20131114
        //end                                                                         			//SS_660_MCA_20140114 //SS_660_MCA_20131114
            //else begin                                                                		//SS_660_MCA_20140114 //SS_660_MCA_20131114
        	//if ObtenerConvenios.FieldByName('EstadoConvenioLA').AsString <> EmptyStr then		//SS_660_MCA_20140114 //SS_660_MCA_20131114
             	//MsgBox(MSG_PROCESO_LISTA_AMARILLA, self.Caption, MB_ICONWARNING);  			//SS_660_MCA_20140114 //SS_660_MCA_20131114
        end;                                                                       				//SS_660_MCA_20131114

        lbl_RUT.Caption := FormatearNumeroRut(NumeroDocumento);
        with ObtenerDatosPersona do begin
            Parameters.ParamByName('@CodigoPersona').Value := null;
            Parameters.ParamByName('@CodigoDocumento').Value := CodigoDocumento;
            Parameters.ParamByName('@NumeroDocumento').Value := NumeroDocumento;
            open;

            if recordCount > 0 then
				lbl_Nombre.Caption := ArmarNombreGenerico(FieldByName('Personeria').AsString,
												FieldByName('Nombre').AsString, FieldByName('Apellido').AsString,
                                                FieldByName('ApellidoMaterno').AsString, FieldByName('NombreContactoComercial').AsString,
												FieldByName('ApellidoContactoComercial').AsString, FieldByName('ApellidoMaternoContactoComercial').AsString)
			else lbl_Nombre.Caption := 'S/D';
		end;
    	btn_Modificar.Enabled := False;                                         //SS_660_MVI_20130909
		Result := True;
	except
		on e: Exception do MsgBoxErr(MSG_ERROR_INICIALIZACION_FORMULARIO_CONVENIOS, e.Message, MSG_ERROR_INICIALIZACION_FORMULARIO, MB_ICONSTOP);
    end;

end;

procedure TformMultimpleMedioPago.ObtenerConveniosAfterOpen(
  DataSet: TDataSet);
begin
    btn_Modificar.Enabled := ObtenerConvenios.RecordCount > 0;
end;

procedure TformMultimpleMedioPago.SetNuevo(const Value: Boolean);
begin
  FNuevo := Value;
  if Value then Modificar := False;
end;

procedure TformMultimpleMedioPago.SetModificar(const Value: Boolean);
begin
  FModificar := Value;
  if Value then Nuevo := false;
end;

procedure TformMultimpleMedioPago.SetCodigoConvenioModif(
  const Value: Integer);
begin
  FCodigoConvenioModif := Value;
end;

{--------------------------------------------------------------------
Procedure Name  : TformMultimpleMedioPago.FormActivate(
Date Created    : 23/09/2013
Description     : Una vez que el formulario est� activo, se dar� foco en la grilla y
                  se seleccionar� el primer item, para verificar si est� en Lista Amarilla.
Firma           : SS_660_MVI_20130909
--------------------------------------------------------------------}
procedure TformMultimpleMedioPago.FormActivate(Sender: TObject);
begin
  dbl_convenio.SetFocus;
  dbl_convenio.OnClick(dbl_convenio);
end;

//procedure TformMultimpleMedioPago.FormShow(Sender: TObject);                      //SS_660_MVI_20130909
//begin                                                                             //SS_660_MVI_20130909
    //dbl_convenio.SetFocus;                                                        //SS_660_MVI_20130909
//end;                                                                              //SS_660_MVI_20130909

procedure TformMultimpleMedioPago.SetNumeroConvenio(const Value: String);
begin
  FNumeroConvenio := Value;
end;

end.
