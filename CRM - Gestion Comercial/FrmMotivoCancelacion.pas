unit FrmMotivoCancelacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, BuscaTab, StdCtrls, DmiCtrls, UtilDb, Util,
  ExtCtrls, DBTables, UtilProc, VariantComboBox, PeaProcs, DMConnection,
  DPSControls;

type
  TFormMotivoCancelacion = class(TForm)
    Label1: TLabel;
    txtObservaciones: TMemo;
    Bevel1: TBevel;
    cbMotivosCancelacion: TVariantComboBox;
    Label2: TLabel;
    btn_Aceptar: TButton;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
  private
    { Private declarations }
    function GetCodigoCancelacion: integer;
    function GetObservaciones: string;
  public
    { Public declarations }
    Function Inicializar: Boolean;
    property CodigoCancelacion: integer read GetCodigoCancelacion;
    property Observaciones: string read GetObservaciones;
  end;

var
  FormMotivoCancelacion: TFormMotivoCancelacion;

implementation

{$R *.dfm}

function TFormMotivoCancelacion.Inicializar: Boolean;
begin
    CargarMotivosCancelacion(DMConnections.BaseCAC, cbMotivosCancelacion,False, 1);
    if cbMotivosCancelacion.Items.Count > 0 then cbMotivosCancelacion.ItemIndex := 0;
	Result := True;
end;


procedure TFormMotivoCancelacion.btn_CancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TFormMotivoCancelacion.btn_AceptarClick(Sender: TObject);
begin
    ModalResult := mrOk;
end;

function TFormMotivoCancelacion.GetCodigoCancelacion: integer;
begin
    Result := cbMotivosCancelacion.Value;
end;

function TFormMotivoCancelacion.GetObservaciones: string;
begin
    result := Trim(txtObservaciones.Text);
end;

end.
