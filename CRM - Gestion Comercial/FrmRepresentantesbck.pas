unit FrmRepresentantes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, FreDomicilio, frmDatoPesona, PeaTypes, Abm_obj,
  DPSControls, ExtCtrls, Util, DB, ADODB, DmConnection, Buttons, UtilProc, RStrings;

type
  TFormRepresentantes = class(TForm)
    gbRepresentanteUno: TGroupBox;
    dbRepresentanteDos: TGroupBox;
    FreRepresentanteUno: TFreDatoPresona;
    GBDomicilios: TGroupBox;
    freDomicilioRepresentanteUno: TFrameDomicilio;
    Panel: TPanel;
    btnAceptar: TDPSButton;
    btnSalir: TDPSButton;
    GroupBox1: TGroupBox;
    freDomicilioRepresentanteDos: TFrameDomicilio;
    FreRepresentanteDos: TFreDatoPresona;
    ObtenerDatosPersona: TADOStoredProc;
    ObtenerDomiciliosPersona: TADOStoredProc;
    btnBorrar: TSpeedButton;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    function DebeValidarDomicilioDos: boolean;
    function DebeValidarRepresentanteDos: boolean;
    procedure FreRepresentanteDostxtDocumentoChange(Sender: TObject);
    procedure FreRepresentanteUnotxtDocumentoChange(Sender: TObject);
    procedure btnBorrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FreRepresentanteUnotxtDocumentoExit(Sender: TObject);
    procedure FreRepresentanteDostxtDocumentoExit(Sender: TObject);

  private
    EstaInicializando: boolean;
    FDocumentoConvenio: string;
    FDomicilioConvenio: TDatosDomicilio;
    function DarRepresentanteUno:TDatosPersonales;
    function DarRepresentanteDos:TDatosPersonales;
    function DarDomicilioRepresentanteUno:TDatosDomicilio;
    function DarDomicilioRepresentanteDos:TDatosDomicilio;
    procedure VerificarPersona(FrePersona: TFreDatoPresona; FreDomicilio: TFrameDomicilio);
    function GenerarRegistro(FrameDomicilio: TFrameDomicilio): TDatosDomicilio;
    function CargarDomicilio(sp: TADOStoredProc): TDatosDomicilio;
    procedure Limpiar(var Representante: TDatosPersonales);

  public
    property RepresentanteUno:TDatosPersonales read DarRepresentanteUno;
    property RepresentanteDos:TDatosPersonales read DarRepresentanteDos;
    property DomicilioRepresentanteUno:TDatosDomicilio read DarDomicilioRepresentanteUno;
    property DomicilioRepresentanteDos:TDatosDomicilio read DarDomicilioRepresentanteDos;

    function Inicializar(Caption: TCaption;
                         RepresentanteUno:TDatosPersonales; //Datos del RepresentanteUno
                         DomicilioRepresentanteUno: TDatosDomicilio;
                         RepresentanteDos:TDatosPersonales; //Datos del RepresentanteDos
                         DomicilioRepresentanteDos: TDatosDomicilio;
                         DomicilioConvenio: TDatosDomicilio;
                         DocumentoConvenio: String): Boolean;
  end;

var
  FormRepresentantes: TFormRepresentantes;

implementation

{$R *.dfm}

{ TFormRepresentantes }


{ TFormRepresentantes }

function TFormRepresentantes.Inicializar(Caption: TCaption;
  RepresentanteUno: TDatosPersonales;
  DomicilioRepresentanteUno: TDatosDomicilio;
  RepresentanteDos: TDatosPersonales;
  DomicilioRepresentanteDos: TDatosDomicilio;
  DomicilioConvenio: TDatosDomicilio;
  DocumentoConvenio: String): Boolean;
begin
    result := false;
    try

        EstaInicializando:= true;

        FDocumentoConvenio:= DocumentoConvenio;

        FDomicilioConvenio:= DomicilioConvenio;

        self.Caption := Caption;
        //Cargo los Datos del Primero
        RepresentanteUno.Personeria := PERSONERIA_FISICA;
        FreRepresentanteUno.Limpiar;
        FreRepresentanteUno.RegistrodeDatos := RepresentanteUno;
        freDomicilioRepresentanteUno.Limpiar;
        freDomicilioRepresentanteUno.Inicializa;
        if (DomicilioRepresentanteUno.CodigoCalle > 0) or (trim(DomicilioRepresentanteUno.CalleDesnormalizada) <> '') then
            freDomicilioRepresentanteUno.CargarDatosDomicilio(DomicilioRepresentanteUno);

        //Cargo los Datos del Segundo
        RepresentanteDos.Personeria := PERSONERIA_FISICA;
        FreRepresentanteDos.Limpiar;
        FreRepresentanteDos.RegistrodeDatos := RepresentanteDos;
        freDomicilioRepresentanteDos.Limpiar;
        freDomicilioRepresentanteDos.Inicializa;
        if (DomicilioRepresentanteDos.CodigoCalle > 0) or (trim(DomicilioRepresentanteDos.CalleDesnormalizada) <> '') then
            freDomicilioRepresentanteDos.CargarDatosDomicilio(DomicilioRepresentanteDos);

        EstaInicializando:= false;

        result := true;

    except
        Exit;
    end;
end;

procedure TFormRepresentantes.btnAceptarClick(Sender: TObject);
begin

    if Trim(FreRepresentanteUno.txtDocumento.Text) = '' then begin
        MsgBoxBalloon(Format(MSG_VALIDAR_DEBE_EL,[STR_NUMERO_DOCUMENTO]), MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteUno.txtDocumento);
        Exit;
    end;

     if (FreRepresentanteDos.RegistrodeDatos.TipoDocumento = FreRepresentanteUno.RegistrodeDatos.TipoDocumento) and
       (FreRepresentanteDos.RegistrodeDatos.NumeroDocumento =  FreRepresentanteUno.RegistrodeDatos.NumeroDocumento) then begin
        MsgBoxBalloon(MSG_ERROR_DOCUMENTOS_IGUALES, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteDos.txtDocumento);
        Exit;
    end;

    if not FreRepresentanteUno.txt_Apellido.Enabled then begin
        if Trim(FreRepresentanteUno.txtDocumento.Text) = Trim(FDocumentoConvenio) then begin
            MsgBoxBalloon(MSG_ERROR_MISMA_PERSONA_CONVENIO, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FreRepresentanteUno.txtDocumento);
            Exit;
        end
        else begin
            MsgBoxBalloon(MSG_ERROR_ES_JURIDICA, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FreRepresentanteUno.txtDocumento);
            Exit;
        end;
    end;

    if not FreRepresentanteDos.txt_Apellido.Enabled then begin
        if Trim(FreRepresentanteDos.txtDocumento.Text) = Trim(FDocumentoConvenio) then begin
            MsgBoxBalloon(MSG_ERROR_MISMA_PERSONA_CONVENIO, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FreRepresentanteDos.txtDocumento);
            Exit;
        end
        else begin
            MsgBoxBalloon(MSG_ERROR_ES_JURIDICA, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FreRepresentanteDos.txtDocumento);
            Exit;
        end;
    end;

    if not FreRepresentanteUno.ValidarDatos then Exit;
    if not freDomicilioRepresentanteUno.ValidarDomicilio then Exit;

    if (DebeValidarRepresentanteDos or DebeValidarDomicilioDos) and not FreRepresentanteDos.ValidarDatos then Exit;
    if (DebeValidarRepresentanteDos or DebeValidarDomicilioDos) and not freDomicilioRepresentanteDos.ValidarDomicilio then Exit;

    if ((FreRepresentanteDos.RegistrodeDatos.CodigoPersona <> -1) or (FreRepresentanteUno.RegistrodeDatos.CodigoPersona <> -1)) and
        (MsgBox(MSG_CAPTION_PERSONA_EXISTE + #13#10 +  MSG_QUESTION_DESEA_CONTINUAR, self.Caption,
                        MB_ICONWARNING or MB_YESNO) <> mrYes) then  Exit;
    ModalResult:=mrOk;
end;

procedure TFormRepresentantes.btnSalirClick(Sender: TObject);
begin
    ModalResult:=mrCancel;
end;

function TFormRepresentantes.DebeValidarDomicilioDos: boolean;
begin
    result := (freDomicilioRepresentanteDos.cb_BuscarCalle.CodigoCalle > 0) or
    (Trim(freDomicilioRepresentanteDos.cb_BuscarCalle.NombreCalle) <> '') or
    (Trim(freDomicilioRepresentanteDos.txt_numeroCalle.Text) <> '') or
    (Trim(freDomicilioRepresentanteDos.txt_CodigoPostal.Text) <> '') or
    (Trim(freDomicilioRepresentanteDos.txt_Detalle.Text) <> '');
//    (freDomicilioRepresentanteDos.cb_Comunas.ItemIndex > 1);
end;

function TFormRepresentantes.DebeValidarRepresentanteDos: boolean;
begin
     result := (Trim(FreRepresentanteDos.txtDocumento.text) <> '') or
     (Trim(FreRepresentanteDos.txt_Nombre.Text) <> '') or
     (Trim(FreRepresentanteDos.txt_Apellido.Text) <> '') or
     (Trim(FreRepresentanteDos.txt_ApellidoMaterno.Text) <> '') or
     (FreRepresentanteDos.cbSexo.ItemIndex > 1) or
     (FreRepresentanteDos.txtFechaNacimiento.Date <> nulldate);
end;


procedure TFormRepresentantes.VerificarPersona(FrePersona: TFreDatoPresona; FreDomicilio: TFrameDomicilio);

procedure Habilitar(Control: TWinControl);
begin
    if Control <> nil then Begin
        Control.Enabled := True;
        Habilitar(Control.Parent);
    end;
end;

var
    RegistroPersona: TDatosPersonales;
begin
    //Verifico si ya es una persona

    if EstaInicializando then Exit;

    RegistroPersona:= FrePersona.RegistrodeDatos;

    if (Length(FrePersona.txtDocumento.Text) <= 5) then begin
        if RegistroPersona.CodigoPersona <> -1 then begin
                RegistroPersona.CodigoPersona := -1;
                FreDomicilio.Limpiar;
                Limpiar(RegistroPersona);
                FrePersona.RegistrodeDatos := RegistroPersona;
                FrePersona.Limpiar;
                EnableControlsInContainer(FrePersona.Parent, True);
            end;
        RegistroPersona.CodigoPersona := -1;
        FreDomicilio.Limpiar;
        FrePersona.RegistrodeDatos := RegistroPersona;
        FrePersona.txtDocumento.SelStart := Length(Trim(RegistroPersona.NumeroDocumento));
        exit;
    end;

    if Trim(FrePersona.txtDocumento.Text) = Trim(FDocumentoConvenio) then begin
        MsgBoxBalloon(MSG_ERROR_MISMA_PERSONA_CONVENIO, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FrePersona.txtDocumento);
        EnableControlsInContainer(FrePersona.Parent, False);
        Habilitar(FrePersona.txtDocumento);
        btnBorrar.Enabled := True;
        RegistroPersona.CodigoPersona := -1;
        FrePersona.lblRutRun.Enabled := true;
        FrePersona.RegistrodeDatos := RegistroPersona;
        FrePersona.txtDocumento.SelStart := Length(Trim(RegistroPersona.NumeroDocumento));
        Exit;
    end;

    ObtenerDatosPersona.Close;
    ObtenerDatosPersona.Parameters.Refresh;
    ObtenerDatosPersona.Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
    ObtenerDatosPersona.Parameters.ParamByName('@NumeroDocumento').Value := Trim(FrePersona.txtDocumento.Text);
    ObtenerDatosPersona.Open;

    if ObtenerDatosPersona.RecordCount > 0 then begin
       if Trim(ObtenerDatosPersona.FieldByName('Personeria').AsString) = PERSONERIA_FISICA then begin
            RegistroPersona.Nombre := Trim(ObtenerDatosPersona.FieldByName('Nombre').AsString);
            RegistroPersona.Apellido := Trim(ObtenerDatosPersona.FieldByName('Apellido').AsString);
            RegistroPersona.ApellidoMaterno := Trim(ObtenerDatosPersona.FieldByName('ApellidoMaterno').AsString);
            RegistroPersona.FechaNacimiento := ObtenerDatosPersona.FieldByName('FechaNacimiento').AsDateTime;
            RegistroPersona.Sexo := Trim(ObtenerDatosPersona.FieldByName('Sexo').AsString)[1];
            RegistroPersona.LugarNacimiento := Trim(ObtenerDatosPersona.FieldByName('LugarNacimiento').AsString);
            RegistroPersona.CodigoActividad := ObtenerDatosPersona.FieldByName('CodigoActividad').AsInteger;
            RegistroPersona.Password := Trim(ObtenerDatosPersona.FieldByName('Password').AsString);
            RegistroPersona.Pregunta := Trim(ObtenerDatosPersona.FieldByName('Pregunta').AsString);
            RegistroPersona.Respuesta := Trim(ObtenerDatosPersona.FieldByName('Respuesta').AsString);
            RegistroPersona.TipoDocumento := Trim(ObtenerDatosPersona.FieldByName('CodigoDocumento').AsString);
            EnableControlsInContainer(FrePersona.Parent, True);

       end
       else begin
            MsgBoxBalloon(MSG_ERROR_ES_JURIDICA, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FrePersona.txtDocumento);
            Limpiar(RegistroPersona);
            FrePersona.RegistrodeDatos := RegistroPersona;
            FrePersona.txtDocumento.SelStart := Length(Trim(RegistroPersona.NumeroDocumento));
            EnableControlsInContainer(FrePersona.Parent, False);
            Habilitar(FrePersona.txtDocumento);
            btnBorrar.Enabled := True;
            FrePersona.lblRutRun.Enabled := true;
       end;

       RegistroPersona.CodigoPersona := ObtenerDatosPersona.FieldByName('CodigoPersona').AsInteger;

       FrePersona.RegistrodeDatos := RegistroPersona;

       ObtenerDomiciliosPersona.Close;
       ObtenerDomiciliosPersona.Parameters.Refresh;
       ObtenerDomiciliosPersona.Parameters.ParamByName('@CodigoPersona').Value := RegistroPersona.CodigoPersona;
       ObtenerDomiciliosPersona.Open;

       if ObtenerDomiciliosPersona.RecordCount > 0 then begin
            FreDomicilio.CargarDatosDomicilio(CargarDomicilio(ObtenerDomiciliosPersona));
       end else
            if FreDomicilio.CodigoDomicilio <> -1 then begin
                FreDomicilio.Limpiar;
            end;
    end
    else begin
        if (RegistroPersona.CodigoPersona <> -1) then begin
                FrePersona.Limpiar;
                FreDomicilio.Limpiar;
                Limpiar(RegistroPersona);
                FrePersona.RegistrodeDatos := RegistroPersona;
                FrePersona.txtDocumento.SelStart := Length(Trim(RegistroPersona.NumeroDocumento));
                EnableControlsInContainer(FrePersona.Parent, True);
            end
        else begin
            if (not FrePersona.txt_Apellido.Enabled) and (Trim(FrePersona.txtDocumento.Text) <> Trim(FDocumentoConvenio)) then begin
                EnableControlsInContainer(FrePersona.Parent, True);
                Limpiar(RegistroPersona);
                FreDomicilio.Limpiar;
                FrePersona.Limpiar;
                FrePersona.RegistrodeDatos := RegistroPersona;
                FrePersona.txtDocumento.SelStart := Length(Trim(RegistroPersona.NumeroDocumento));
            end else
                FreDomicilio.CargarDatosDomicilio(FDomicilioConvenio);
        end;
    end;
end;


procedure TFormRepresentantes.FreRepresentanteDostxtDocumentoChange(
  Sender: TObject);
begin
    VerificarPersona(FreRepresentanteDos, freDomicilioRepresentanteDos);
end;

procedure TFormRepresentantes.FreRepresentanteUnotxtDocumentoChange(
  Sender: TObject);
begin
    VerificarPersona(FreRepresentanteUno, freDomicilioRepresentanteUno);
end;

function TFormRepresentantes.DarDomicilioRepresentanteDos: TDatosDomicilio;
begin
    result := GenerarRegistro(freDomicilioRepresentanteDos);
end;

function TFormRepresentantes.DarDomicilioRepresentanteUno: TDatosDomicilio;
begin
    result := GenerarRegistro(freDomicilioRepresentanteUno);
end;

function TFormRepresentantes.DarRepresentanteDos: TDatosPersonales;
begin
    result := FreRepresentanteDos.RegistrodeDatos;
end;

function TFormRepresentantes.DarRepresentanteUno: TDatosPersonales;
begin
    result := FreRepresentanteUno.RegistrodeDatos;
end;

function TFormRepresentantes.GenerarRegistro(
  FrameDomicilio: TFrameDomicilio): TDatosDomicilio;
var
    auxDomicilio: TDatosDomicilio;
begin
    auxDomicilio.CodigoDomicilio := FrameDomicilio.CodigoDomicilio;
    auxDomicilio.CodigoCalle := FrameDomicilio.CodigoCalle;
    auxDomicilio.CodigoSegmento := FrameDomicilio.CodigoSegmento;
    auxDomicilio.NumeroCalleSTR := Trim(FrameDomicilio.Numero);
    auxDomicilio.Detalle := Trim(FrameDomicilio.Detalle);
    auxDomicilio.CodigoPostal := Trim(FrameDomicilio.CodigoPostal);
    auxDomicilio.CodigoPais := Trim(FrameDomicilio.Pais);
    auxDomicilio.CodigoRegion :=  Trim(FrameDomicilio.Region);
    auxDomicilio.CodigoComuna :=  Trim(FrameDomicilio.Comuna);
    auxDomicilio.CalleDesnormalizada := Trim(FrameDomicilio.DescripcionCalle);
    auxDomicilio.Normalizado :=  FrameDomicilio.EsCalleNormalizada;
    auxDomicilio.Descripcion:=FrameDomicilio.DescripcionCalle;
    result := auxDomicilio;
end;

function TFormRepresentantes.CargarDomicilio(
  sp: TADOStoredProc): TDatosDomicilio;
var
    auxDomicilio: TDatosDomicilio;
begin
    auxDomicilio.CodigoDomicilio := sp.FieldByName('CodigoDomicilio').AsInteger;
    auxDomicilio.CodigoCalle := iif(sp.FieldByName('CodigoCalle').AsInteger>0, sp.FieldByName('CodigoCalle').AsInteger, -1);
    auxDomicilio.CodigoSegmento :=  sp.FieldByName('CodigoSegmento').AsInteger;
    auxDomicilio.NumeroCalleSTR :=  sp.FieldByName('Numero').AsString;
    auxDomicilio.Detalle :=  sp.FieldByName('Detalle').AsString;
    auxDomicilio.CodigoPostal :=  sp.FieldByName('CodigoPostal').AsString;
    auxDomicilio.CodigoPais := sp.FieldByName('CodigoPais').AsString;
    auxDomicilio.CodigoRegion :=   sp.FieldByName('CodigoRegion').AsString;
    auxDomicilio.CodigoComuna :=   sp.FieldByName('CodigoComuna').AsString;
    auxDomicilio.CalleDesnormalizada := sp.FieldByName('CalleDesnormalizada').AsString;
    auxDomicilio.Normalizado :=  iif(sp.FieldByName('Normalizado').Asinteger = 1, true, false);
    result := auxDomicilio;
end;

procedure TFormRepresentantes.Limpiar(var Representante: TDatosPersonales);
begin
    Representante.CodigoPersona := -1;
    Representante.RazonSocial := '';
    Representante.Apellido := '';
    Representante.ApellidoMaterno := '';
    Representante.Nombre := '';
    Representante.Sexo := #0;
    Representante.LugarNacimiento := '';
    Representante.FechaNacimiento := NullDate;
    Representante.CodigoActividad := -1;
    Representante.Password := '';
    Representante.Pregunta := '';
    Representante.Respuesta := '';
    Representante.Pregunta := '';
end;

procedure TFormRepresentantes.btnBorrarClick(Sender: TObject);
begin
    FreRepresentanteDos.Limpiar;
    freDomicilioRepresentanteDos.Limpiar;
end;

procedure TFormRepresentantes.FormShow(Sender: TObject);
begin
    FreRepresentanteUno.txtDocumento.SetFocus;
end;

procedure TFormRepresentantes.FreRepresentanteUnotxtDocumentoExit(
  Sender: TObject);
begin
    if (not((btnAceptar.Focused) or (btnSalir.Focused))) and (FreRepresentanteUno.RegistrodeDatos.CodigoPersona <> -1) and (FreRepresentanteUno.txt_Apellido.Enabled)then
        MsgBoxBalloon(MSG_CAPTION_PERSONA_EXISTE, STR_REPRESENTANTE, MB_ICONWARNING, FreRepresentanteUno.txtDocumento);
end;

procedure TFormRepresentantes.FreRepresentanteDostxtDocumentoExit(
  Sender: TObject);
begin
    if (((not btnAceptar.Focused) or (btnSalir.Focused))) and (FreRepresentanteDos.RegistrodeDatos.CodigoPersona <> -1) and (FreRepresentanteDos.txt_Apellido.Enabled) then
        MsgBoxBalloon(MSG_CAPTION_PERSONA_EXISTE, STR_REPRESENTANTE, MB_ICONWARNING, FreRepresentanteDos.txtDocumento);
end;

end.
