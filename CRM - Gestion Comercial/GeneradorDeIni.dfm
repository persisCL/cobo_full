object FormGeneradorIni: TFormGeneradorIni
  Left = 325
  Top = 201
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Configuraci'#243'n del Puesto'
  ClientHeight = 187
  ClientWidth = 279
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poNone
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 8
    Width = 273
    Height = 137
  end
  object Label1: TLabel
    Left = 7
    Top = 27
    Width = 104
    Height = 13
    Caption = 'Punto de &Entrega:'
    FocusControl = edtPuntoEntrega
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 7
    Top = 52
    Width = 93
    Height = 13
    Caption = 'Punto de &Venta:'
    FocusControl = edtPuntoVenta
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl_FuenteSolicitud: TLabel
    Left = 7
    Top = 76
    Width = 93
    Height = 13
    Caption = '&Fuente Solicitud'
    FocusControl = txt_FuenteSolicitud
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblPOS: TLabel
    Left = 72
    Top = 99
    Width = 193
    Height = 41
    AutoSize = False
  end
  object Label4: TLabel
    Left = 7
    Top = 105
    Width = 55
    Height = 13
    Caption = 'POSNET:'
    FocusControl = txt_FuenteSolicitud
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edtPuntoEntrega: TNumericEdit
    Left = 146
    Top = 24
    Width = 121
    Height = 21
    Color = 16444382
    TabOrder = 0
    OnChange = edtPuntoEntregaChange
  end
  object edtPuntoVenta: TNumericEdit
    Left = 146
    Top = 48
    Width = 121
    Height = 21
    Color = 16444382
    TabOrder = 1
  end
  object txt_FuenteSolicitud: TNumericEdit
    Left = 146
    Top = 72
    Width = 121
    Height = 21
    Color = 16444382
    TabOrder = 2
  end
  object btnAceptar: TButton
    Left = 118
    Top = 151
    Width = 75
    Height = 28
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 3
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 198
    Top = 151
    Width = 75
    Height = 28
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
    OnClick = btnCancelarClick
  end
  object VerificarPuntoEntregaVenta2: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarPuntoEntregaVenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Valido'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 8
    Top = 152
  end
end
