{--------------------------------------------------------------------------------------

Revision		:	2
Author			:	Miguel Villarroel
Date			:	12-Junio-2013
Description		:	Valida que cliente no est� en Lista Amarilla.
Firma           :   SS-660-MVI-20130610

Autor       :   CQuezadaI
Fecha       :   23-07-2013
Firma       :   SS_660_CQU_20130711
Desripcion  :   Se agrega el par�metro CodigoConcesionaria en la funci�n EstaConvenioEnListaAmarilla.

Firma       : SS_660_MVI_20130729
Descripcion : Se cambia texto y se agrega estado del convenio en lista amarilla

--------------------------------------------------------------------------------------}
unit frmAdhesionPreInscripcionPAK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls,

  BuscaClientes, DMConnection, PeaTypes, SysUtilsCN, PeaProcsCN, PeaProcs,
  ADODb, Util, ConstParametrosGenerales, frmMedioEnvioMail, DB;

type
  TAdhesionPreInscripcionPAKForm = class(TForm)
    grpFiltros: TGroupBox;
    Label1: TLabel;
    lblNombreCli: TLabel;
    lblPersoneria: TLabel;
    lblRUT: TLabel;
    btn_Filtrar: TButton;
    peNumeroDocumento: TPickEdit;
    btn_Limpiar: TButton;
    btnAdherir: TButton;
    spConveniosEnListaAmarillaPorRut: TADOStoredProc;
    dsConveniosEnListaAmarilla: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure peNumeroDocumentoKeyPress(Sender: TObject; var Key: Char);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure btnAdherirClick(Sender: TObject);

  private
    { Private declarations }
    { Variables }
    FUltimaBusqueda: TBusquedaCliente;
    FCodigoPersona,
    FCodigoAuditoria: Integer;
    FPersoneria,
    FRUTPersona: string;
    FFechaHoy: TDate;
    { Procedures }
    procedure MostrarDatosCliente(CodigoCliente: Integer; RUT, Personeria: string);
    procedure Limpiar(Sender: TObject);
    procedure BuscarCliente;
    procedure AgregarAuditoria_AdhesionPAK(pNumeroDocumento, pEmail: string; pCodigoPersona, pCumpleRequisitos, pAdheridoEnvioEMail: Integer; pAdherido: Byte; pCodigoAuditoria: Integer);
    procedure AdherirClienteAraucoTAG(pCodigoPersona: Integer; pEMail: string);
	procedure ActualizaEMailPersona(pEMail: string; pCodigoPersona: Integer);
	procedure ActualizarConveniosPersonaEnvioPorEMail(pCodigoPersona: Integer);
    { Functions }
    function ValidarCliente: Boolean;
    function ValidarFiltros: Boolean;
  public
    { Public declarations }
    { Variables }
    { Procedures }
    { Functions }
    function Inicializar: Boolean;
  end;

var
  AdhesionPreInscripcionPAKForm: TAdhesionPreInscripcionPAKForm;

implementation

{$R *.dfm}

function TAdhesionPreInscripcionPAKForm.Inicializar: Boolean;
begin
    try
    	Result := False;

        FCodigoPersona   := -1;
        FCodigoAuditoria := -1;
        FPersoneria      := EmptyStr;
        FRUTPersona      := EmptyStr;

        lblRUT.Caption        := EmptyStr;
        lblNombreCli.Caption  := EmptyStr;
        lblPersoneria.Caption := EmptyStr;

        btnAdherir.Enabled	  := False;

        FFechaHoy := SysutilsCN.NowBaseCN(DMConnections.BaseCAC);

        Result     := True;
    except
    	on e: Exception do begin
        	ShowMsgBoxCN(e, Self);
        end;
    end;
end;

procedure TAdhesionPreInscripcionPAKForm.peNumeroDocumentoButtonClick(Sender: TObject);
    var
        formBuscaClientes: TFormBuscaClientes;
begin
    try
        formBuscaClientes := TFormBuscaClientes.Create(nil);

        if formBuscaClientes.Inicializa(FUltimaBusqueda)then begin
            if (formBuscaClientes.ShowModal = mrok) then begin

                FUltimaBusqueda        := formBuscaClientes.UltimaBusqueda;
                peNumeroDocumento.Text := Trim(formBuscaClientes.Persona.NumeroDocumento);
                FCodigoPersona         := formBuscaClientes.Persona.CodigoPersona;
                FRUTPersona            := Trim(formBuscaClientes.Persona.NumeroDocumento);
                FPersoneria            := formBuscaClientes.Persona.Personeria;

                if FCodigoPersona <> 0 then begin
                	MostrarDatosCliente(FCodigoPersona, FRUTPersona, FPersoneria);
                    btn_FiltrarClick(nil);
                end;
            end;
        end;
    finally
    	if Assigned(formBuscaClientes) then FreeAndNil(formBuscaClientes);
    end;
end;

procedure TAdhesionPreInscripcionPAKForm.peNumeroDocumentoChange(Sender: TObject);
begin
	Limpiar(Sender);
end;

procedure TAdhesionPreInscripcionPAKForm.peNumeroDocumentoKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then btn_FiltrarClick(nil);
end;

procedure TAdhesionPreInscripcionPAKForm.btnAdherirClick(Sender: TObject);
	resourcestring
    	rsTituloActivacion  = 'Activaci�n Servicio Arauco TAG';
        rsMensajeActivacion = '� Desea activar el Servicio Arauco TAG para el Cliente seleccionado ?';
        rsMensajeActivado   = 'El Servicio Arauco TAG, se activ� correctamente para el Cliente.';
        rsMensajeErrorEMail = 'El cliente no posee E-Mail. No es posible activar el servicio a trav�s de este medio.';
	var
        FormMedioEnvioMail: TFormMedioEnvioMail;
    	EMailPersona: string;
        ClienteAdheridoEnvioEMail: Boolean;
    const
    	cSQLObtenerEmailPersona = 'SELECT dbo.ObtenerEmailPersona(%d)';
        cSQLValidarConveniosPersonaEnvioPorEMail = 'SELECT dbo.ValidarConveniosPersonaEnvioPorEMail(%d)';

begin
    try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
            EMailPersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerEmailPersona, [FCodigoPersona]));
            ClienteAdheridoEnvioEMail := QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarConveniosPersonaEnvioPorEMail, [FCodigoPersona]));

	        FormMedioEnvioMail := TFormMedioEnvioMail.Create(nil);

            with FormMedioEnvioMail do begin
            	Inicializar(EMailPersona, ClienteAdheridoEnvioEMail);

	        	if ShowModal = mrOk then begin
	                if EMailPersona <> Trim(txtEmail.Text) then begin
                    	ActualizaEMailPersona(Trim(txtEmail.Text), FCodigoPersona);
                        EMailPersona := Trim(txtEmail.Text);

                        AgregarAuditoria_AdhesionPAK(
                            FRUTPersona,
                            EMailPersona,
                            FCodigoPersona,
                            -1,
                            -1,
                            0,
                            FCodigoAuditoria);

                    end;
                    if (not ClienteAdheridoEnvioEMail) and chkNotaCobroPorEMail.Checked then begin
                    	ActualizarConveniosPersonaEnvioPorEMail(FCodigoPersona);
                        ClienteAdheridoEnvioEMail := True;

                        AgregarAuditoria_AdhesionPAK(
                            FRUTPersona,
                            EmptyStr,
                            FCodigoPersona,
                            -1,
                            IIf(ClienteAdheridoEnvioEMail, 1, 0),
                            0,
                            FCodigoAuditoria);
                    end;
            	end;
            end;


            if ShowMsgBoxCN(rsTituloActivacion, rsMensajeActivacion, MB_ICONQUESTION + MB_YESNO, Self) = mrOk then begin

                AdherirClienteAraucoTAG(FCodigoPersona, EMailPersona);

                AgregarAuditoria_AdhesionPAK(
                    FRUTPersona,
                    EmptyStr,
                    FCodigoPersona,
                    -1,
                    -1,
                    1,
                    FCodigoAuditoria);

                ShowMsgBoxCN(rsTituloActivacion, rsMensajeActivado, MB_ICONINFORMATION, Self);
                Limpiar(Sender);
            end
        except
        	on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	if Assigned(FormMedioEnvioMail) then FreeAndNil(FormMedioEnvioMail);    
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;

end;

procedure TAdhesionPreInscripcionPAKForm.btn_FiltrarClick(Sender: TObject);
begin
    if ValidarFiltros then begin
        if FCodigoPersona <= 0 then begin
        	BuscarCliente;
        end;

        if FCodigoPersona > 0 then begin
            btnAdherir.Enabled := ValidarCliente;
        end;
    end;
end;

procedure TAdhesionPreInscripcionPAKForm.btn_LimpiarClick(Sender: TObject);
begin
	Limpiar(Sender);
end;

procedure TAdhesionPreInscripcionPAKForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action:= caFree;
end;

procedure TAdhesionPreInscripcionPAKForm.FormCreate(Sender: TObject);
begin
    FFormState := FFormState - [fsVisible];
end;

procedure TAdhesionPreInscripcionPAKForm.FormDestroy(Sender: TObject);
begin
	AdhesionPreInscripcionPAKForm := nil;
end;

procedure TAdhesionPreInscripcionPAKForm.MostrarDatosCliente(CodigoCliente: Integer; RUT, Personeria: string);
	const
      	cSQLObtenerNombrePersona  = 'SELECT dbo.ObtenerNombrePersona(%d)';
    	cSQLFormatearRutConPuntos = 'SELECT dbo.FormatearRutConPuntos(''%s'')';
begin
	lblRUT.Caption       := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLFormatearRutConPuntos, [RUT])) + '   ';
    lblNombreCli.Caption := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerNombrePersona, [CodigoCliente]));

    if Personeria = PERSONERIA_FISICA then begin
        lblPersoneria.Caption := ' - (Persona Natural)';
    end

    else begin
        if Personeria = PERSONERIA_JURIDICA then lblPersoneria.Caption := ' - (Persona Jur�dica)';
        if Personeria = PERSONERIA_AMBAS then lblPersoneria.Caption := ' - (Persona Natural y Jur�dica)';
    end;

    lblNombreCli.Left  := lblRUT.Left + lblRUT.Width;
    lblPersoneria.Left := lblNombreCli.Left + lblNombreCli.Width;
end;

procedure TAdhesionPreInscripcionPAKForm.Limpiar(Sender: TObject);
begin
    FCodigoPersona   := -1;
    FCodigoAuditoria := -1;
    FPersoneria      := EmptyStr;
    FRUTPersona      := EmptyStr;

    lblRUT.Caption        := EmptyStr;
    lblNombreCli.Caption  := EmptyStr;
    lblPersoneria.Caption := EmptyStr;

    if not (Sender is TPickEdit) then begin
        peNumeroDocumento.Clear;
    end;

    btnAdherir.Enabled    := False;
end;

function TAdhesionPreInscripcionPAKForm.ValidarFiltros: Boolean;
	resourcestring
    	rsTituloValidacion   = 'Validaci�n Filtros Consulta';
        rsErrorDatosConsulta = 'No ha ingresado datos para la consulta.';
        rsRUTNoValido        = 'El RUT ingresado NO es v�lido.';
    const
        MaxArray = 2;
    var
        vControles  : Array [1..MaxArray] of TControl;
        vCondiciones: Array [1..MaxArray] of Boolean;
        vMensajes   : Array [1..MaxArray] of String;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);

        vControles[01] := peNumeroDocumento;
        vControles[02] := peNumeroDocumento;

        vCondiciones[01] := ((peNumeroDocumento.Text) <> EmptyStr);
        vCondiciones[02] := (peNumeroDocumento.Text = EmptyStr) or ValidarRUT(DMConnections.BaseCAC, peNumeroDocumento.Text);

        vMensajes[01] := rsErrorDatosConsulta;
        vMensajes[02] := rsRUTNoValido;

    	try
        	Result := ValidateControls(vControles, vCondiciones, rsTituloValidacion, vMensajes);
        except
            on e:exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TAdhesionPreInscripcionPAKForm.BuscarCliente;
	resourcestring
    	rsTituloMensaje = 'Busqueda Cliente';
        rsClienteNoencontrado = 'No se encontr� el Cliente por los filtros de b�squeda indicados.';
    var
    	Persona: TDatosPersonales;
begin
    try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
          	FRUTPersona := Trim(peNumeroDocumento.Text);

            if FRUTPersona = EmptyStr then begin
            	raise EWarningExceptionCN.Create(rsTituloMensaje, rsClienteNoencontrado);
            end;

            Persona := ObtenerDatosPersonales(DMConnections.BaseCAC, 'RUT', FRUTPersona);

            if Persona.CodigoPersona <> -1 then begin
            	FCodigoPersona := Persona.CodigoPersona;
                FPersoneria    := Persona.Personeria;

                MostrarDatosCliente(FCodigoPersona, FRUTPersona, FPersoneria);
            end
            else begin
            	raise EWarningExceptionCN.Create(rsTituloMensaje, rsClienteNoencontrado);
            end;
        except
        	on e: Exception do begin
	            ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

function TAdhesionPreInscripcionPAKForm.ValidarCliente: Boolean;
	resourcestring
    	rsTituloMensaje          = 'Validaci�n Cliente';
        rsClientePersonaJuridica = 'El Cliente NO puede ser Persona Jur�dica para adherise al servicio Arauco TAG.';
        rsClienteYaAdherido      = 'El Cliente ya est� adherido al servicio Arauco TAG.';
        rsClienteYaInscrito      = 'El Cliente ya se inscribi� v�a WEB, para solicitar la adhesi�n al servicio Arauco TAG.';
        rsClienteSinCuentas		 = 'El Cliente NO posee ninguna Cuenta Activa. No es posible adherir al Cliente al servicio Arauco TAG';
        rsClienteEnListaAmarilla = 'El cliente posee cuentas en Lista Amarilla';						 //SS-660-MVI-20130610
	const
    	cSQLValidarPersonaAdheridaPA  				= 'SELECT dbo.ValidarPersonaAdheridaPAK(%d)';
        cSQLValidarPersonaInscritaWebPA 			= 'SELECT dbo.ValidarPersonaInscritaWebPAK(%d)';
        cSQLObtenerCantidadCuentasActivasPorPersona = 'SELECT dbo.ObtenerCantidadCuentasActivasPorPersona(%d)';
        //cSQLValidarClienteListaAmarilla = 'SELECT 1 FROM Convenio WITH(NOLOCK) WHERE CodigoCliente = %d AND dbo.EstaConvenioEnListaAmarilla(Convenio.CodigoConvenio, GETDATE()) = 1';		// SS_660_CQU_20130711	 //SS-660-MVI-20130610
        cSQLValidarClienteListaAmarilla = 'SELECT 1 FROM Convenio WITH(NOLOCK) WHERE CodigoCliente = %d AND dbo.EstaConvenioEnListaAmarilla(Convenio.CodigoConvenio, GETDATE(), NULL) = 1'; // SS_660_CQU_20130711


  var
       Convenios: String;                                                                               //SS-660-MVI-20130610
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
        Result := False;

    	try
//            if (StrToInt64(Copy(FRUTPersona, 1, Length(FRUTPersona) - 1)) >= 50000000) or (FPersoneria = PERSONERIA_JURIDICA) then begin
//
//                AgregarAuditoria_AdhesionPAK(
//                    FRUTPersona,
//                    EmptyStr,
//                    FCodigoPersona,
//                    0,
//                    -1,
//                    0,
//                    FCodigoAuditoria);
//
//            	raise EWarningExceptionCN.Create(rsTituloMensaje, rsClientePersonaJuridica);
//            end;

		   // BEGIN SS-660-MVI-20130610
		   if (QueryGetIntegerValue(DMConnections.BaseCAC, Format(cSQLValidarClienteListaAmarilla, [FCodigoPersona])) = 1 )then begin
		
		                AgregarAuditoria_AdhesionPAK(
		                    FRUTPersona,
		                    EmptyStr,
		                    FCodigoPersona,
		                    6,
		                    -1,
							0,
		                    FCodigoAuditoria);
		
		        Convenios := ' ' + #13;
		        with spConveniosEnListaAmarillaPorRut do begin
		            Parameters.Refresh;
		            Parameters.ParamByName('@Rut').Value := FRUTPersona;
		
		            Open;
		           while not dsConveniosEnListaAmarilla.dataset.Eof  do begin
		               Convenios:= Convenios + ' ' + dsConveniosEnListaAmarilla.DataSet.FieldByName('NumeroConvenio').AsString + ' ' + dsConveniosEnListaAmarilla.DataSet.FieldByName('EstadoListaAmarilla').AsString + #13;            //SS_660_MVI_20130729
		               dsConveniosEnListaAmarilla.dataset.Next;
		           end;
		
		           Close
		
		        end;
		            	raise EInfoExceptionCN.Create(rsTituloMensaje, rsClienteEnListaAmarilla + Convenios );
		    end;
		    // END SS-660-MVI-20130610
            if QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarPersonaAdheridaPA, [FCodigoPersona])) then begin

                AgregarAuditoria_AdhesionPAK(
                    FRUTPersona,
                    EmptyStr,
                    FCodigoPersona,
                    2,
                    -1,
					0,
                    FCodigoAuditoria);

            	raise EInfoExceptionCN.Create(rsTituloMensaje, rsClienteYaAdherido);
            end;

            if QueryGetIntegerValue(DMConnections.BaseCAC, Format(cSQLObtenerCantidadCuentasActivasPorPersona, [FCodigoPersona])) = 0 then begin

                AgregarAuditoria_AdhesionPAK(
                    FRUTPersona,
                    EmptyStr,
                    FCodigoPersona,
                    0,
                    -1,
					0,
                    FCodigoAuditoria);

            	raise EInfoExceptionCN.Create(rsTituloMensaje, rsClienteSinCuentas);
            end;

            if QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarPersonaInscritaWebPA, [FCodigoPersona])) then begin

                AgregarAuditoria_AdhesionPAK(
                    FRUTPersona,
                    EmptyStr,
                    FCodigoPersona,
                    4,
                    -1,
                    0,
                    FCodigoAuditoria);
            end
            else begin

                AgregarAuditoria_AdhesionPAK(
                    FRUTPersona,
                    EmptyStr,
                    FCodigoPersona,
                    5,
                    -1,
                    0,
                    FCodigoAuditoria);
            end;

            Result := True;
        except
        	on e: Exception do begin
	            ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TAdhesionPreInscripcionPAKForm.AgregarAuditoria_AdhesionPAK(pNumeroDocumento, pEMail: string; pCodigoPersona, pCumpleRequisitos, pAdheridoEnvioEMail: Integer; pAdherido: Byte; pCodigoAuditoria: Integer);
	var
        spAgregarAuditoria_AdhesionPAK: TADOStoredProc;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
            spAgregarAuditoria_AdhesionPAK := TADOStoredProc.Create(nil);

            with spAgregarAuditoria_AdhesionPAK do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'AgregarAuditoria_AdhesionPAK';

                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@Usuario').Value                := UsuarioSistema;
                	ParamByName('@NumeroDocumento').Value        := IIf(pNumeroDocumento = EmptyStr, null, pNumeroDocumento);
                	ParamByName('@Patente').Value                := null;
                	ParamByName('@CodigoPersona').Value          := pCodigoPersona;
                	ParamByName('@CumpleRequisitos').Value       := IIf(pCumpleRequisitos = -1, null, pCumpleRequisitos);
                	ParamByName('@EMail').Value        	         := IIf(pEMail = EmptyStr, null, pEMail);
                	ParamByName('@AdheridoEnvioEMail').Value     := IIf(pAdheridoEnvioEMail = -1, null, pAdheridoEnvioEMail);
                	ParamByName('@ImpresionContrato').Value      := null;
                	ParamByName('@FirmaContrato').Value          := null;
                	ParamByName('@AdhesionPreInscripcion').Value := pAdherido;
                	ParamByName('@IDCodigoAuditoria').Value      := IIf(pCodigoAuditoria = -1, null, pCodigoAuditoria);

	                ExecProc;

                    FCodigoAuditoria := ParamByName('@IDCodigoAuditoria').Value;
                end;
            end;
        except
        	on e: Exception do begin
            	raise EErrorExceptionCN.Create('procedure TAdhesionPreInscripcionPAKForm.AgregarAuditoria_AdhesionPAK', e.Message);
            end;
        end;
    finally
    	if Assigned(spAgregarAuditoria_AdhesionPAK) then FreeAndNil(spAgregarAuditoria_AdhesionPAK);
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TAdhesionPreInscripcionPAKForm.AdherirClienteAraucoTAG(pCodigoPersona: Integer; pEMail: string);
	resourcestring
    	rsParametroGeneralNOExiste = 'El Par�metro General "%s" NO Existe, o contiene un valor inv�lido.';
        rsErrorEjecutarProcedimiento = 'Error al ejecutar el procedimiento %s. Error: %s.';
        rsErrorEjecutarProcedimiento2 = 'Error al ejecutar el procedimiento %s.';
	const
        cSQLTemporal = 'UPDATE Personas SET AdheridoPA = 1 WHERE CodigoPersona = %d';
        cSQLObtenerPrimerCodigoConvenioPersona = 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona(%d, 1)';
        cFormatoParametros = '"%s"';

        COD_PLANTILLA_EMAIL_PAK = 'COD_PLANTILLA_EMAIL_PAK';
        COD_FIRMA_EMAIL_PAK = 'COD_FIRMA_EMAIL_PAK';

	var
        spActualizarConveniosClienteEnListaDeAcceso,
        spEMAIL_AgregarMensaje,
        spActualizarInscripcionWebAdhesionPAK: TADOStoredProc;
    	CodigoConvenio,
        CodigoPlantillaEMail,
        CodigoFirmaEMail: Integer;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
        try
            spActualizarConveniosClienteEnListaDeAcceso := TADOStoredProc.Create(nil);
            spEMAIL_AgregarMensaje := TADOStoredProc.Create(nil);
            spActualizarInscripcionWebAdhesionPAK := TADOStoredProc.Create(nil);

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, COD_PLANTILLA_EMAIL_PAK, CodigoPlantillaEMail) then begin
            	raise Exception.Create(Format(rsParametroGeneralNOExiste, [COD_PLANTILLA_EMAIL_PAK]));
            end;

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, COD_FIRMA_EMAIL_PAK, CodigoFirmaEMail) then begin
            	raise Exception.Create(Format(rsParametroGeneralNOExiste, [COD_FIRMA_EMAIL_PAK]));
            end;

        	CodigoConvenio := QueryGetIntegerValue(DMConnections.BaseCAC, Format(cSQLObtenerPrimerCodigoConvenioPersona, [pCodigoPersona]));

            with spActualizarConveniosClienteEnListaDeAcceso do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'ActualizarConveniosClienteEnListaDeAcceso';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@CodigoPersona').Value := pCodigoPersona;
                	ParamByName('@Usuario').Value       := UsuarioSistema;
                	ParamByName('@MensajeError').Value  := EmptyStr;
                end;
            end;

            with spEMAIL_AgregarMensaje do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'EMAIL_AgregarMensaje';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@Email').Value 				   := pEMail;
                	ParamByName('@CodigoPlantilla').Value          := CodigoPlantillaEMail;
                	ParamByName('@CodigoFirma').Value  			   := CodigoFirmaEMail;
                	ParamByName('@CodigoConvenio').Value  		   := CodigoConvenio;
                	ParamByName('@Parametros').Value               := Format(cFormatoParametros, [lblNombreCli.Caption]);
                	ParamByName('@NumeroProcesoFacturacion').Value := null;
                end;
            end;

            with spActualizarInscripcionWebAdhesionPAK do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'ActualizarInscripcionWebAdhesionPAK';
                Parameters.Refresh;

                with Parameters do begin
                	ParamByName('@CodigoPersona').Value := pCodigoPersona;
                	ParamByName('@Usuario').Value       := UsuarioSistema;
                end;
            end;

            DMConnections.BaseCAC.BeginTrans;

            with spActualizarConveniosClienteEnListaDeAcceso do begin
                ExecProc;

                if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                    raise Exception.Create(Format(rsErrorEjecutarProcedimiento, [ProcedureName, Parameters.ParamByName('@MensajeError').Value]));
                end;
            end;

            with spActualizarInscripcionWebAdhesionPAK do begin
                ExecProc;

                if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                    raise Exception.Create(Format(rsErrorEjecutarProcedimiento2, [ProcedureName]));
                end;
            end;

            if pEMail <> EmptyStr then begin
	            spEMAIL_AgregarMensaje.ExecProc;
            end;

            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
        except
        	on e: Exception do begin
            	if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
            	raise EErrorExceptionCN.Create('procedure TAdhesionPreInscripcionPAKForm.AdherirClienteAraucoTAG',e.Message);
            end;
        end;
    finally
    	if Assigned(spActualizarConveniosClienteEnListaDeAcceso) then begin
        	FreeAndNil(spActualizarConveniosClienteEnListaDeAcceso);
        end;
    	if Assigned(spEMAIL_AgregarMensaje) then begin
        	FreeAndNil(spEMAIL_AgregarMensaje);
        end;
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TAdhesionPreInscripcionPAKForm.ActualizaEMailPersona(pEMail: string; pCodigoPersona: Integer);
	var
        CodigoMedioComunicacionEmailPersona: Integer;
        spActualizarMedioComunicacionPersona: TADOStoredProc;
	const
        cSQLObtenerCodigoMedioComunicacionEmailPersona = 'SELECT dbo.ObtenerCodigoMedioComunicacionEmailPersona(%d)';
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
            CodigoMedioComunicacionEmailPersona := QueryGetIntegerValue(DMConnections.BaseCAC, Format(cSQLObtenerCodigoMedioComunicacionEmailPersona, [FCodigoPersona]));

            spActualizarMedioComunicacionPersona := TADOStoredProc.Create(nil);

            with spActualizarMedioComunicacionPersona do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'ActualizarMedioComunicacionPersona';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@CodigoTipoMedioContacto').Value := 1;
                	ParamByName('@CodigoArea').Value              := null;
                	ParamByName('@Valor').Value                   := pEMail;
                	ParamByName('@Anexo').Value                   := null;
                	ParamByName('@CodigoDomicilio').Value         := null;
                	ParamByName('@HorarioDesde').Value            := null;
                	ParamByName('@HorarioHasta').Value            := null;
                	ParamByName('@Observaciones').Value           := null;
                	ParamByName('@CodigoPersona').Value           := pCodigoPersona;
                	ParamByName('@Principal').Value               := 0;
                	ParamByName('@EstadoVerificacion').Value      := null;
                	ParamByName('@CodigoMedioComunicacion').Value := IIf(CodigoMedioComunicacionEmailPersona = 0, null, CodigoMedioComunicacionEmailPersona);
                end;
            end;

            DMConnections.BaseCAC.BeginTrans;
            spActualizarMedioComunicacionPersona.ExecProc;
            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
        except
        	on e: Exception do begin
            	if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
            	raise EErrorExceptionCN.Create('procedure TAdhesionPreInscripcionPAKForm.ActualizaEMailPersona',e.Message);
            end;
        end;
    finally
    	if Assigned(spActualizarMedioComunicacionPersona) then FreeAndNil(spActualizarMedioComunicacionPersona);
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TAdhesionPreInscripcionPAKForm.ActualizarConveniosPersonaEnvioPorEMail(pCodigoPersona: Integer);
	var
        spActualizarConveniosPersonaEnvioPorEMail: TADOStoredProc;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
            spActualizarConveniosPersonaEnvioPorEMail := TADOStoredProc.Create(nil);

            with spActualizarConveniosPersonaEnvioPorEMail do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'ActualizarConveniosPersonaEnvioPorEMail';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@CodigoPersona').Value := pCodigoPersona;
                	ParamByName('@Usuario').Value       := UsuarioSistema;
                end;
            end;

            DMConnections.BaseCAC.BeginTrans;
            spActualizarConveniosPersonaEnvioPorEMail.ExecProc;
            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
        except
        	on e: Exception do begin
            	if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
            	raise EErrorExceptionCN.Create('procedure TAdhesionPreInscripcionPAKForm.ActualizarConveniosPersonaEnvioPorEMail',e.Message);
            end;
        end;
    finally
    	if Assigned(spActualizarConveniosPersonaEnvioPorEMail) then FreeAndNil(spActualizarConveniosPersonaEnvioPorEMail);
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;



end.
