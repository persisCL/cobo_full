{********************************** File Header ********************************
File Name   : frmRevalidarTransitoSPC
Author      : mbecerra / cfuentesc
Date Created: 07-04-2008 / 21/01/2016
Language    : ES-CL
Revisi�n 1  : Recibe un listado de tr�nsitos a revalidar y despliega,
				para cada uno de ellos, algunos datos y da al operador
                la alternativa de modificar la Patente.

                Al presionar el bot�n Aceptar se invoca al SP RevalidarTransito

--------------------------------------------------------------------------------}

unit frmRevalidarTransitoSPC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Grids, DBGrids, ComCtrls, DMConnection, DB, ADODB,
  UtilProc, RStrings, ImgList, OPImageOverview, DPSControls, DBCtrls, ImgProcs,
  ImagePlus, ToolWin, Buttons, ActnList, ActnMan, ConstParametrosGenerales, ImgTypes,
  JPEGPlus, Filtros, Math, formImagenOverview, UtilDB, Util, PeaProcs, RVMLogin,
  Provider, DBClient, ListBoxEx, DBListEx, DmiCtrls, ActualizarDatosInfractor,
  VariantComboBox, PeaTypes, XPStyleActnCtrls, JPeg, PeaProcsCN, SysUtilsCN;

type
  TRevalidarTransitoSPCForm = class(TForm)
    pnlInfraccion: TGroupBox;
    Label5: TLabel;
    lblEstado: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    lblPatente: TLabel;
    lblCategoria: TLabel;
    lblAnularInfraccion: TLabel;
    Label9: TLabel;
    txtPatente: TEdit;
    gbImagenAmpliar: TGroupBox;
    cbCategoria: TVariantComboBox;
    btnCancelar: TButton;
    btnAceptar: TButton;
    Label1: TLabel;
    lblNumCorrCA: TLabel;
    tmerVisualizar: TTimer;
    spRevalidarTransito: TADOStoredProc;
    btnSalir: TButton;
    Label2: TLabel;
    lblFechaTTo: TLabel;
    Label3: TLabel;
    lblPortico: TLabel;
    grpRNVM: TGroupBox;
    Label4: TLabel;
    lblRNVMModelo: TLabel;
    Label11: TLabel;
    lblRNVMMarca: TLabel;
    Label13: TLabel;
    lblRNVMCategoria: TLabel;
    spObtenerUltimaConsultaRNVM: TADOStoredProc;
    Label10: TLabel;
    lblRNVMPatente: TLabel;
    Label12: TLabel;
    lblRNVMFecha: TLabel;
    pgcImages: TPageControl;
    tsFrontal1: TTabSheet;
    imgFrontal1: TImage;
    tsFrontal2: TTabSheet;
    imgFrontal2: TImage;
    tsContexto1: TTabSheet;
    imgContexto1: TImage;
    tsContexto2: TTabSheet;
    imgContexto2: TImage;
    tsContexto3: TTabSheet;
    imgContexto3: TImage;
    tsContexto4: TTabSheet;
    imgContexto4: TImage;
    tsContexto5: TTabSheet;
    imgContexto5: TImage;
    tsContexto6: TTabSheet;
    imgContexto6: TImage;
    spObtenerTransitosCliente: TADOStoredProc;
    procedure FormShow(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure VerSiHabilita(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure actBrightPlusExecute(Sender: TObject);
    procedure actBrightLessExecute(Sender: TObject);
    procedure actBrightResetExecute(Sender: TObject);
    procedure actResetImageExecute(Sender: TObject);
    procedure actImageOverviewExecute(Sender: TObject);
    procedure tmerVisualizarTimer(Sender: TObject);
    procedure txtPatenteKeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    FImagePathNFI: AnsiString;

	FImagenActual: TJPEGPlusImage;
    FShiftOriginal: Integer;

    FPositionPlate: Array[tiFrontal..tiPosterior2] of TRect;
    FFrmImagenOverview: TFrmImageOverview;
    FFormOverviewCargado: Boolean;

	function fGetCodEstadoTransito(strEstado: string): Integer;
	function fGetCodCategoria(strCategoria: string): Integer;

  public
    { Public declarations }
    procedure Inicializar(txtCaption : string);
    procedure VisualizaDatos;
    procedure VerImagenTransito(NumCorrCA: Int64; RegistrationAccessibility: Int64);
    procedure AsignarRect(Indice : TTipoImagen; DataImage : TDataImage);
    procedure SiguienteTransito;
    procedure DespliegaMensaje(CodigoError : integer);
    procedure ConsultaRNVM;
  end;

var
  RevalidarTransitoSPCForm: TRevalidarTransitoSPCForm;
  lstListaTransitosARevalidar,                          {NumCorrCA de los Transitos a revalidar}
  lstListaPatenteyCategoriaARevalidar : TStringList;	{otros datos, como Patente y categoria de ese Transito}

implementation

{$R *.dfm}
ResourceString
    CAPTION_POS_PATENTE = 'Posici�n de Patente Desconocida';
var
    IndiceCategoria : integer;
    EstadoTransito, CategoriaTransito, NumCorrCA : string;
    ImagenNumCorrCA : Int64;
    FechaTransito : TDateTime;
    PrimeraVezTimer : boolean;
    EsItaliano : Boolean;

function StrToFechaHora(fecha : string) : TDateTime;
var
    dia, mes, anno, hora, min, seg, mseg : word;
begin
  dia  := StrToInt(Copy(fecha,1,2));
  mes  := StrToInt(Copy(fecha,3,2));
  anno := StrToInt(Copy(fecha,5,4));
  hora := StrToInt(Copy(fecha,9,2));
  min  := StrToInt(Copy(fecha,11,2));
  seg  := StrToInt(Copy(fecha,13,2));
  mseg := StrToInt(Copy(fecha,15,2));
  Result := EncodeDate(anno, mes, dia) + EncodeTime(hora,min,seg,mseg);
end;

function GetItem(Texto : string; NumeroItem : integer; Separador : Char) : string;
var
    Elemento : string;
    i, Cual : integer;
begin
    i := 1;
    Elemento := '';
    Cual := 0;
    Texto := Texto + Separador;  {para asegurarnos de encontrar el separador en el �ltimo elemento}
    while i <= Length(Texto) do begin
        if Texto[i] = Separador then begin
            Inc(Cual);
            if Cual = NumeroItem then   {es el elemento que se debe retornar}
            	i := Length(Texto) + 1
            else
                Elemento := '';         {no es el elemento que se debe retornar}

        end
        else
          Elemento := Elemento + Texto[i];

    	Inc(i);
    end;

    Result := Elemento;
end;

procedure TRevalidarTransitoSPCForm.ConsultaRNVM;
resourcestring
    SIN_DATOS = 'Sin Datos';
begin
	spObtenerUltimaConsultaRNVM.Close;
    spObtenerUltimaConsultaRNVM.CommandTimeout := 500;
    spObtenerUltimaConsultaRNVM.Parameters.ParamByName('@Patente').Value := txtPatente.Text;
    spObtenerUltimaConsultaRNVM.Open;
    grpRNVM.Visible := True;
    if spObtenerUltimaConsultaRNVM.IsEmpty then begin
        lblRNVMModelo.Caption    := SIN_DATOS;
        lblRNVMMarca.Caption     := '';
        lblRNVMCategoria.Caption := '';
        lblRNVMPatente.Caption   := txtPatente.Text;
        lblRNVMFecha.Caption     := '';
    end
    else begin
        lblRNVMModelo.Caption    := spObtenerUltimaConsultaRNVM.FieldByName('Model').AsString;
        lblRNVMMarca.Caption     := spObtenerUltimaConsultaRNVM.FieldByName('Manufacturer').AsString;
        lblRNVMCategoria.Caption := spObtenerUltimaConsultaRNVM.FieldByName('Category').AsString;
        lblRNVMPatente.Caption   := spObtenerUltimaConsultaRNVM.FieldByName('LicensePlate').AsString;
        lblRNVMFecha.Caption     := DateToStr(spObtenerUltimaConsultaRNVM.FieldByName('UltimaFechaModificacion').AsDateTime);
    end;

    spObtenerUltimaConsultaRNVM.Close;
end;

procedure TRevalidarTransitoSPCForm.DespliegaMensaje;
resourcestring
        MENSAJE_DE_ERROR = 'C�digo %d' + #13#13 + 'Descripci�n: %s';
var
  Descripcion : string;
begin
    case CodigoError of
       -1: Descripcion := 'Estado = 1, Falta Relaci�n TransitosItemsPeajeCuenta';
       -2: Descripcion := 'Estado = 1, No se pudo borrar la relaci�n en TransitosItemsPeajeCuenta';
       -3: Descripcion := 'Estado = 1, No se pudo actualizar la tabla ItemsPeajeCuenta, con la resta del Importe';
       -4: Descripcion := 'Estado = 1, No se pudo borrar desde ItemsPeajeCuenta el registro cuyo importe es cero';
       -5: Descripcion := 'Estado = 10 � 14, Fall� el Store AnularTransitoInfraccion';
       -6: Descripcion := 'Estado = 12 � 13, Fall� la b�squeda de un DayPass';
       -7: Descripcion := 'Estado = 12 � 13, Fall� la eliminaci�n del DayPas';
       -8: Descripcion := 'Estado = 12 � 13, No se pudo dejar "sin usar" el DayPass';
       -9: Descripcion := 'No se pudo calcular el nuevo importe, para la nueva categor�a';
      -10: Descripcion := 'No se pudo insertar un detalle tr�nsito para la nueva patente y nueva categor�a';
      -11: Descripcion := 'No se pudo actualizar el tr�nsito con el nuevoID, Importe y TipoHorario';
      -12: Descripcion := 'Fall� el Store CambiarEstadoTransito';
      -13: Descripcion := 'Fall� el Store InsertarAuditoriaRevalidacionTransitos';
      -14: Descripcion := 'Fall� el Store ChequearTransitosLimbo2';
      -15: Descripcion := 'El Tr�nsito est� facturado (campo LoteFacturacion de MovimientosCuentas NO es NULL)';
      -16: Descripcion := 'No se eliminaron registros desde la tabla DetalleConsumoMovimientos';
      -17: Descripcion := 'No se pudo poner en NULL el campo NumeroMovimiento en la Tabla ItemsPeajeCuenta';
      -18: Descripcion := 'No se eliminaron registros desde la tabla MovimientosCuentas';
      -19: Descripcion := 'No se puede revalidar el transito pues tiene Daypass asociado y facturado';            // SS_1222_MBE_20141022
      -20: Descripcion := 'No puede cambiar la categor�a a un tr�nsito de otra Concesionaria';                    // SS_1222_MBE_20141022
    end;

	MsgBoxErr('No se pudo revalidar Tr�nsito ', Format(MENSAJE_DE_ERROR, [CodigoError, Descripcion]), Caption, MB_ICONERROR );
end;

procedure TRevalidarTransitoSPCForm.SiguienteTransito;
begin
	lstListaTransitosARevalidar.Delete(0);
    lstListaPatenteyCategoriaARevalidar.Delete(0);
    tmerVisualizar.Enabled := True;
end;

procedure TRevalidarTransitoSPCForm.AsignarRect;
begin
    FPositionPlate[Indice] := Rect(
    		Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),
            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),
            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),
            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));
end;

procedure TRevalidarTransitoSPCForm.VerImagenTransito(NumCorrCA: Int64; RegistrationAccessibility: Int64);
var
    lImagen: string;
begin

    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiFrontalSPC1]) = RegistrationAccessibilityImagen[tiFrontalSPC1] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiFrontalSPC1]]));

        imgFrontal1.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiFrontalSPC2]) = RegistrationAccessibilityImagen[tiFrontalSPC2] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiFrontalSPC2]]));

        imgFrontal2.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC1]) = RegistrationAccessibilityImagen[tiContextoSPC1] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC1]]));

        imgContexto1.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC2]) = RegistrationAccessibilityImagen[tiContextoSPC2] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC2]]));

        imgContexto2.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC3]) = RegistrationAccessibilityImagen[tiContextoSPC3] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC3]]));

        imgContexto3.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC4]) = RegistrationAccessibilityImagen[tiContextoSPC4] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC4]]));

        imgContexto4.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC5]) = RegistrationAccessibilityImagen[tiContextoSPC5] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC5]]));

        imgContexto5.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC6]) = RegistrationAccessibilityImagen[tiContextoSPC6] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC6]]));

        imgContexto6.Picture.LoadFromFile(lImagen);
    end;

end;

procedure TRevalidarTransitoSPCForm.VerSiHabilita(Sender: TObject);
begin
	btnAceptar.Enabled := Length(txtPatente.Text) >= 6; // (lblPatente.Caption <> txtPatente.Text) or (IndiceCategoria <> cbCategoria.ItemIndex);     TASK_080_JMA_20161202
    if (lblPatente.Caption <> txtPatente.Text)and (Length(txtPatente.Text)>= 6) then  begin
    	ConsultaRNVM();
    end
    else grpRNVM.Visible := False;
end;

procedure TRevalidarTransitoSPCForm.Visualizadatos;
resourcestring
	MSG_ERROR_LOADING_TRANSITS	= 'Error cargando Tr�nsitos';
    MSG_BUSQUEDA_VACIA          = 'No se encontr� ning�n registro con esos par�metros de b�squeda';
var
	Item : string;
	Numcorrca: string;
begin
    grpRNVM.Visible := False;
	Item := lstListaPatenteyCategoriaARevalidar[0];
    NumCorrCA            := GetItem(Item, 1, #9);
	lblPatente.Caption   := GetItem(Item, 2, #9);
    lblEstado.Caption    := GetItem(Item, 3, #9);
    lblCategoria.Caption := GetItem(Item, 4, #9);
    CategoriaTransito    := GetItem(Item, 5, #9);  {c�digo categor�a}
    EstadoTransito       := GetItem(Item, 6, #9);  {c�digo estado}
    FechaTransito        := StrToFechaHora(GetItem(Item, 7, #9));
    lblPortico.Caption   := GetItem(Item,8,#9);
    EsItaliano           := StrToBool(GetItem(Item, 9, #9)); // SS_1091_CQU_20130516
    lblFechaTTo.Caption  := DateTimeToStr(FechaTransito);
    lblNumCorrCA.Caption := NumCorrCA;
    txtPatente.Text := lblPatente.Caption;
    btnAceptar.Enabled := True;
    IndiceCategoria := cbCategoria.Items.IndexOfValue(CategoriaTransito);
    cbCategoria.ItemIndex := IndiceCategoria;
    btnAceptar.Enabled := False;
    ImagenNumCorrCA := StrToInt64(NumCorrCA);

    spObtenerTransitosCliente.DisableControls;
    try
        with spObtenerTransitosCliente do begin
		    Close;
            CommandTimeout := 500;
		    Parameters.ParamByName('@Patente').Value 		 		:= iif(txtPatente.Text = '',null,txtPatente.Text);
			Parameters.ParamByName('@FechaDesde').Value		 		:= FormatDateTime('yyyymmdd hh:nn', FechaTransito);
            Parameters.ParamByName('@FechaHasta').Value		 		:= FormatDateTime('yyyymmdd hh:nn', FechaTransito);
            Parameters.ParamByName('@Estado').Value					:= fGetCodEstadoTransito(lblEstado.Caption);
            Parameters.ParamByName('@Categoria').Value				:= fGetCodCategoria(lblCategoria.Caption);
        	Parameters.ParamByName('@Portico').Value				:= Null;
		    Parameters.ParamByName('@CantidadAMostrar').Value		:= 20;
		    Parameters.ParamByName('@CantidadTransitos').Value		:= 0;
            Parameters.ParamByName('@ContractSerialNumber').Value 	:= Null;
        end;

		if not(OpenTables([spObtenerTransitosCliente])) then
			  MsgBox(MSG_ERROR_LOADING_TRANSITS,Caption, MB_ICONERROR);

        if spObtenerTransitosCliente.IsEmpty then
            MsgBox(MSG_BUSQUEDA_VACIA, Caption, MB_ICONINFORMATION);

    finally
	    spObtenerTransitosCliente.EnableControls;
    end;

    Numcorrca := spObtenerTransitosCliente.FieldByName('numcorrca').AsString;
    VerImagenTransito(spObtenerTransitosCliente['numcorrca'], spObtenerTransitosCliente['RegistrationAccessibility']);
end;

function TRevalidarTransitoSPCForm.fGetCodEstadoTransito(strEstado: string): Integer;
var
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
    try
    	Qry.Connection := DmConnections.BaseCAC;
        Qry.SQL.Text := 'select	Estado from EstadoTransitos  WITH (NOLOCK) where Descripcion = ''' + strEstado + '''';
        Qry.Open;
        Result := Qry.Fields[0].AsInteger;
    finally
    	Qry.Close;
        Qry.Free;
    end;
end;

function TRevalidarTransitoSPCForm.fGetCodCategoria(strCategoria: string): Integer;
var
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
    try
    	Qry.Connection := DmConnections.BaseCAC;
        Qry.SQL.Text := 'select	Categoria from Categorias  WITH (NOLOCK) where Descripcion = ''' + strCategoria + '''';
        Qry.Open;
        Result := Qry.Fields[0].AsInteger;
    finally
    	Qry.Close;
        Qry.Free;
    end;
end;

procedure TRevalidarTransitoSPCForm.actBrightLessExecute(Sender: TObject);
begin
    if not EsItaliano then begin
        if FImagenActual.Shift > 4 then exit;
        // Si el FShift no esta inicializado lo asignamos
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift := FImagenActual.Shift + 1;
    end;
end;

procedure TRevalidarTransitoSPCForm.actBrightPlusExecute(Sender: TObject);
begin
    if not EsItaliano then begin
        // Si el Shift supero el limite nos vamos
        if FImagenActual.Shift = 0 then exit;
        // Si el FShift no esta inicializado lo asignamos
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift := FImagenActual.Shift - 1;
    end;
end;

procedure TRevalidarTransitoSPCForm.actBrightResetExecute(Sender: TObject);
begin
    if not EsItaliano then begin
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        if FImagenActual.Shift = FShiftOriginal then exit;
        FImagenActual.Shift := FShiftOriginal;
    end;
end;

procedure TRevalidarTransitoSPCForm.actResetImageExecute(Sender: TObject);
begin
    if not EsItaliano then begin
        // Volvemos el brillo al original
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift    := FShiftOriginal;
    end;
end;

procedure TRevalidarTransitoSPCForm.btnAceptarClick(Sender: TObject);
ResourceString
    LARGO_PATENTE		= 'No ha ingresado la patente o est� incompleta';
    PATENTE_INCORECTA	= 'La patente ingresada es incorrecta';
    CATEGORIA_NOSELEC	= 'No ha seleccionado la Categor�a';
var
  Respuesta, CodigoRetorno :  integer;
begin
    if Length(txtPatente.Text) < 5 then begin
    	MsgBox(LARGO_PATENTE, Caption, MB_OK + MB_ICONEXCLAMATION);
        exit;
    end;

    if (Length(txtPatente.Text) = 5) and (Copy(txtPatente.Text,1,2) <> 'PR') then begin
    	MsgBox(PATENTE_INCORECTA, Caption, MB_OK + MB_ICONEXCLAMATION);
        exit;
    end;

    if cbCategoria.ItemIndex < 0 then begin
        MsgBox(CATEGORIA_NOSELEC, Caption, MB_OK + MB_ICONEXCLAMATION);
        exit;
    end;

    spRevalidarTransito.Close;
    spRevalidarTransito.CommandTimeout := 500;
    spRevalidarTransito.Parameters.ParamByName('@NumCorrCA').Value := ImagenNumCorrCA;
    spRevalidarTransito.Parameters.ParamByName('@NuevaPatente').Value := txtPatente.Text;
    spRevalidarTransito.Parameters.ParamByName('@NuevaCategoria').Value := cbCategoria.Value;
    spRevalidarTransito.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
    //Rev 1 DMConnections.BaseCAC.BeginTrans;
    try
    	spRevalidarTransito.ExecProc;
        CodigoRetorno := spRevalidarTransito.Parameters.ParamByName('@RETURN_VALUE').Value;
        if CodigoRetorno < 0 then begin
          DespliegaMensaje(CodigoRetorno);
        end
        else begin
        	MsgBox('Tr�nsito Revalidado exitosamente', Caption, MB_OK + MB_ICONEXCLAMATION);
    		SiguienteTransito();
        end;
    except on e:exception do begin
            MsgBoxErr('Ocurri� un Error en la Revalidaci�n del Tr�nsito', e.message, Caption, MB_ICONERROR );
	        Respuesta := MsgBox(' �Desea reintentarlo?' +#13#13 +
                            'Si = Reintentar' + #13 + 'No = continuar con el siguiente Tr�nsito' + #13 +
                            'Cancelar = Cancelar todo el proceso', Caption, MB_YESNOCANCEL + MB_ICONEXCLAMATION);
    	    if Respuesta = IDNO then begin {siguiente}
                SiguienteTransito()
	        end
	        else if Respuesta = IDCANCEL then begin {cancelar todo}
	        	Close;
            end;

	    end;                          	
    end; {try}

end;

procedure TRevalidarTransitoSPCForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TRevalidarTransitoSPCForm.FormShow(Sender: TObject);
begin
	PrimeraVezTimer := True;
    CargarCategoriasVehiculos(DMConnections.BaseCAC, cbCategoria);
	tmerVisualizar.Enabled := True;
    txtPatente.Text := '';
    cbCategoria.ItemIndex := -1;
    lblPatente.Caption := '';
    lblCategoria.Caption := '';
    lblEstado.Caption := '';
    lblNumCorrCA.Caption := '';
    grpRNVM.Visible := False;
end;

procedure TRevalidarTransitoSPCForm.Inicializar;
var
	S : TSize;
begin
    Caption := txtCaption;
    Position := poScreenCenter;
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);

    ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN, FImagePathNFI);
    if Trim(FImagePathNFI) = '' then begin
    	raise Exception.Create('No se encontro el Parametro ' + DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN);
    end;

    Screen.Cursor := crHourGlass;

    Screen.Cursor := crDefault;
end;

procedure TRevalidarTransitoSPCForm.tmerVisualizarTimer(Sender: TObject);
begin
	{s�lo usado para invocar al procedimiento VisualizarDatos,
      ya que desde el OnShow no funciona}
    tmerVisualizar.Enabled := False;
    if PrimeraVezTimer then begin
    	PrimeraVezTimer := False;
        tmerVisualizar.Interval := 200;  {aceleramos el despliegue}
    end;

    if lstListaTransitosARevalidar.Count > 0 then begin
    	VisualizaDatos();
    end
    else begin
    	Close;
    end;
end;

procedure TRevalidarTransitoSPCForm.txtPatenteKeyPress(Sender: TObject;
  var Key: Char);
begin
	if not (Key  in [#3, #22, #24, #26, '0'..'9','a'..'z','A'..'Z', Char(VK_BACK)]) then
        Key := #0;
end;

procedure TRevalidarTransitoSPCForm.actImageOverviewExecute(Sender: TObject);
begin
	if FFrmImagenOverview.Showing then FFrmImagenOverview.Hide
    else if FFormOverviewCargado then FFrmImagenOverview.Show;
end;

procedure TRevalidarTransitoSPCForm.btnCancelarClick(Sender: TObject);
begin
	SiguienteTransito();
end;

end.
