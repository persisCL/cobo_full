object formMedioEnvioDocumentosCobro: TformMedioEnvioDocumentosCobro
  Left = 175
  Top = 231
  BorderStyle = bsDialog
  Caption = 'Medio de env'#237'o de los documentos de cobro'
  ClientHeight = 202
  ClientWidth = 537
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  DesignSize = (
    537
    202)
  PixelsPerInch = 96
  TextHeight = 13
  object gb_Datos: TGroupBox
    Left = 6
    Top = 44
    Width = 526
    Height = 123
    TabOrder = 2
    object rbDetalleImpresoConNotaCobro: TRadioButton
      Left = 8
      Top = 94
      Width = 500
      Height = 17
      Alignment = taLeftJustify
      Caption = 
        #191'Desea recibir con costo por emisi'#243'n el Detalle de Transacciones' +
        ' Impreso junto a su Nota de Cobro?'
      TabOrder = 4
    end
    object rbNotaCobroYDetalleEmail: TRadioButton
      Left = 8
      Top = 34
      Width = 500
      Height = 17
      Alignment = taLeftJustify
      Caption = 
        #191'Desea recibir Gratuitamente su Nota de Cobro y el Detalle de Tr' +
        'ansacciones en Mail separados?'
      TabOrder = 1
    end
    object rbNotaCobroEmail: TRadioButton
      Left = 8
      Top = 54
      Width = 500
      Height = 17
      Alignment = taLeftJustify
      Caption = #191'Desea recibir Gratuitamente solo su Nota de Cobro v'#237'a E-Mail?'
      TabOrder = 2
    end
    object rbDetalleEmail: TRadioButton
      Left = 8
      Top = 74
      Width = 500
      Height = 17
      Alignment = taLeftJustify
      Caption = 
        #191'Desea recibir Gratuitamente solo el Detalle de Transacciones v'#237 +
        'a E-Mail?'
      TabOrder = 3
    end
    object rbNinguno: TRadioButton
      Left = 8
      Top = 15
      Width = 500
      Height = 17
      Alignment = taLeftJustify
      Caption = #191'Desea recibir su Nota de Cobro por Correo postal?'
      TabOrder = 0
    end
  end
  object pnl_ConvenioNoTieneEmail: TPanel
    Left = 6
    Top = 4
    Width = 526
    Height = 40
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Caption = 'El convenio no tiene E-Mail'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
  end
  object btnAceptar: TButton
    Left = 379
    Top = 172
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 0
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 457
    Top = 172
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 1
  end
end
