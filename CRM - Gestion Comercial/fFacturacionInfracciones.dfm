object FormFacturacionInfracciones: TFormFacturacionInfracciones
  Left = 0
  Top = 0
  Caption = 'Facturaci'#243'n de Infracciones'
  ClientHeight = 592
  ClientWidth = 874
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 700
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 551
    Width = 874
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      874
      41)
    object btnSalir: TButton
      Left = 772
      Top = 6
      Width = 97
      Height = 27
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnSalirClick
    end
    object btnValorizar: TButton
      Left = 629
      Top = 6
      Width = 137
      Height = 27
      Anchors = [akRight, akBottom]
      Caption = 'Valorizar'
      TabOrder = 1
      OnClick = btnValorizarClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 874
    Height = 551
    Align = alClient
    TabOrder = 0
    object pgCentral: TPageControl
      Left = 1
      Top = 1
      Width = 872
      Height = 327
      ActivePage = tsFacturacionRut
      Align = alClient
      OwnerDraw = True
      TabOrder = 0
      OnChange = pgCentralChange
      OnDrawTab = pgCentralDrawTab
      object tsFacturacionRut: TTabSheet
        Caption = 'Facturaci'#243'n por Rut'
        ImageIndex = 1
        object dblInfraccionesPorRut: TDBListEx
          Left = 0
          Top = 41
          Width = 864
          Height = 239
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 75
              MinWidth = 60
              Header.Caption = 'Facturar'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblInfraccionesPorRutColumns2HeaderClick
              FieldName = 'Facturar'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 105
              MinWidth = 75
              Header.Caption = 'Fecha'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Sorting = csAscending
              IsLink = False
              OnHeaderClick = dblInfraccionesPorRutColumns2HeaderClick
              FieldName = 'FechaInfraccion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              MinWidth = 125
              Header.Caption = 'Concesionaria'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblInfraccionesPorRutColumns2HeaderClick
              FieldName = 'DescripcionConcesionaria'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 75
              MinWidth = 75
              Header.Caption = 'Patente'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblInfraccionesPorRutColumns2HeaderClick
              FieldName = 'Patente'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              MinWidth = 100
              Header.Caption = 'Tipo Infracci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'TipoInfraccion'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 60
              MinWidth = 60
              Header.Caption = 'Precio'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'PrecioInfraccion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              MinWidth = 125
              Header.Caption = 'Estado IF'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblInfraccionesPorRutColumns2HeaderClick
              FieldName = 'EstadoIF'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              MinWidth = 125
              Header.Caption = 'Estado CN'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblInfraccionesPorRutColumns2HeaderClick
              FieldName = 'EstadoCN'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 75
              MinWidth = 75
              Header.Caption = 'Fec. Anula.'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaAnulacion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              MinWidth = 130
              Header.Caption = 'Motivo Anulaci'#243'n'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'MotivoAnulacion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              MinWidth = 130
              Header.Caption = 'Nombre'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Nombre'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              MinWidth = 80
              Header.Caption = 'Documento'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroDocumento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              MinWidth = 130
              Header.Caption = 'NumeroConvenio'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroConvenio'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              MinWidth = 130
              Header.Caption = 'Tipo Comprobante'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'TipoComprobanteFiscal'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              MinWidth = 125
              Header.Caption = 'Nro Comprobante'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroComprobanteFiscal'
            end>
          DataSource = ds_InfraccionesPorRut
          DragReorder = False
          ParentColor = False
          TabOrder = 1
          TabStop = True
          OnDblClick = dblInfraccionesPorRutDblClick
          OnDrawText = dblInfraccionesPorRutDrawText
          OnKeyDown = dblInfraccionesPorRutKeyDown
        end
        object pnlBotonesRut: TPanel
          Left = 0
          Top = 0
          Width = 864
          Height = 41
          Align = alTop
          ParentColor = True
          TabOrder = 0
          DesignSize = (
            864
            41)
          object lblRutInput: TLabel
            Left = 14
            Top = 12
            Width = 21
            Height = 13
            Caption = 'Rut:'
          end
          object lblDesdeFechaPorRut: TLabel
            Left = 426
            Top = 12
            Width = 66
            Height = 13
            Caption = 'Desde Fecha:'
          end
          object lblHastaFechaPorRut: TLabel
            Left = 600
            Top = 12
            Width = 64
            Height = 13
            Caption = 'Hasta Fecha:'
          end
          object lblConcesionariaRut: TLabel
            Left = 151
            Top = 12
            Width = 71
            Height = 13
            Caption = 'Concesionaria:'
          end
          object txtRutInput: TEdit
            Left = 66
            Top = 9
            Width = 81
            Height = 21
            CharCase = ecUpperCase
            MaxLength = 9
            TabOrder = 0
            OnChange = txtRutInputChange
          end
          object deDesdeFechaRut: TDateEdit
            Left = 499
            Top = 9
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 3
            Date = -693594.000000000000000000
          end
          object deHastaFechaRut: TDateEdit
            Left = 682
            Top = 9
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 4
            Date = -693594.000000000000000000
          end
          object btnBuscarRut: TButton
            Left = 783
            Top = 7
            Width = 75
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            Default = True
            TabOrder = 1
            OnClick = btnBuscarRutClick
          end
          object cbConcesionariaRut: TVariantComboBox
            Left = 228
            Top = 9
            Width = 190
            Height = 21
            Style = vcsDropDownList
            ItemHeight = 13
            TabOrder = 2
            OnChange = cbConcesionariaChange
            Items = <>
          end
        end
        object stbInfraccionesPorRUT: TStatusBar
          Left = 0
          Top = 280
          Width = 864
          Height = 19
          Panels = <>
          SimplePanel = True
          SimpleText = 'Sin Infracciones en la B'#250'squeda.'
        end
      end
      object tsFacturacionPatente: TTabSheet
        Caption = 'Facturaci'#243'n por Patente'
        object dblInfracciones: TDBListEx
          Left = 0
          Top = 41
          Width = 864
          Height = 239
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 75
              MinWidth = 60
              Header.Caption = 'Facturar'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = dblInfraccionesPorRutColumns2HeaderClick
              FieldName = 'Facturar'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 105
              MinWidth = 75
              Header.Caption = 'Fecha'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = dblInfraccionesPorRutColumns2HeaderClick
              FieldName = 'Fecha'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              MinWidth = 100
              Header.Caption = 'Tipo Infraccion'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'TipoInfraccion'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 60
              MinWidth = 60
              Header.Caption = 'Precio'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'PrecioInfraccion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              MinWidth = 125
              Header.Caption = 'Estado IF'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblInfraccionesPorRutColumns2HeaderClick
              FieldName = 'EstadoIF'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              MinWidth = 125
              Header.Caption = 'Estado CN'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblInfraccionesPorRutColumns2HeaderClick
              FieldName = 'EstadoCN'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 75
              MinWidth = 75
              Header.Caption = 'Fec. Anula.'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaAnulacion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              MinWidth = 130
              Header.Caption = 'Motivo Anulaci'#243'n'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'MotivoAnulacion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              MinWidth = 130
              Header.Caption = 'Nombre'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Nombre'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              MinWidth = 80
              Header.Caption = 'Documento'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroDocumento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              MinWidth = 130
              Header.Caption = 'NumeroConvenio'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroConvenio'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              MinWidth = 130
              Header.Caption = 'Tipo Comprobante'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'TipoComprobanteFiscal'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              MinWidth = 125
              Header.Caption = 'Nro Comprobante'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Arial'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroComprobanteFiscal'
            end>
          DataSource = ds_Infracciones
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnDblClick = dblInfraccionesDblClick
          OnDrawText = dblInfraccionesDrawText
          OnKeyDown = dblInfraccionesKeyDown
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 864
          Height = 41
          Align = alTop
          ParentBackground = False
          ParentColor = True
          TabOrder = 1
          DesignSize = (
            864
            41)
          object lblPatente: TLabel
            Left = 14
            Top = 12
            Width = 42
            Height = 13
            Caption = 'Patente:'
          end
          object lblConcesionaria: TLabel
            Left = 151
            Top = 12
            Width = 71
            Height = 13
            Caption = 'Concesionaria:'
          end
          object lblFechaHasta: TLabel
            Left = 600
            Top = 12
            Width = 64
            Height = 13
            Caption = 'Hasta Fecha:'
          end
          object lblFechaDesde: TLabel
            Left = 426
            Top = 12
            Width = 66
            Height = 13
            Caption = 'Desde Fecha:'
          end
          object edPatente: TEdit
            Left = 66
            Top = 9
            Width = 81
            Height = 21
            CharCase = ecUpperCase
            MaxLength = 6
            TabOrder = 0
            OnChange = edPatenteChange
          end
          object btnBuscar: TButton
            Left = 783
            Top = 7
            Width = 75
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            Default = True
            TabOrder = 1
            OnClick = btnBuscarClick
          end
          object cbConcesionaria: TVariantComboBox
            Left = 228
            Top = 9
            Width = 190
            Height = 21
            Style = vcsDropDownList
            ItemHeight = 13
            TabOrder = 2
            OnChange = cbConcesionariaChange
            Items = <>
          end
          object deDesdeFecha: TDateEdit
            Left = 499
            Top = 9
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 3
            Date = -693594.000000000000000000
          end
          object deHastaFecha: TDateEdit
            Left = 682
            Top = 9
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 4
            Date = -693594.000000000000000000
          end
        end
        object stbInfraccionesPorPatente: TStatusBar
          Left = 0
          Top = 280
          Width = 864
          Height = 19
          Panels = <>
          SimplePanel = True
          SimpleText = 'Sin Infracciones en la B'#250'squeda.'
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 328
      Width = 872
      Height = 222
      Align = alBottom
      TabOrder = 1
      DesignSize = (
        872
        222)
      object gbDatosComprobante: TGroupBox
        Left = 6
        Top = 2
        Width = 860
        Height = 78
        Caption = '  Datos del Comprobante  '
        TabOrder = 0
        object lblRUT: TLabel
          Left = 14
          Top = 21
          Width = 77
          Height = 13
          Caption = 'RUT del Cliente:'
        end
        object lblConvenio: TLabel
          Left = 13
          Top = 48
          Width = 107
          Height = 13
          Caption = 'Convenios del Cliente:'
        end
        object lblNumeroConvenio: TLabel
          Left = 308
          Top = 21
          Width = 104
          Height = 13
          Caption = 'N'#250'mero de Convenio:'
        end
        object peRUTCliente: TPickEdit
          Left = 126
          Top = 19
          Width = 139
          Height = 21
          Enabled = True
          TabOrder = 0
          OnChange = peRUTClienteChange
          EditorStyle = bteTextEdit
          OnButtonClick = peRUTClienteButtonClick
        end
        object txtNumeroConvenio: TEdit
          Left = 418
          Top = 17
          Width = 121
          Height = 21
          TabOrder = 1
        end
        object btnBuscarConvenio: TButton
          Left = 573
          Top = 15
          Width = 75
          Height = 25
          Caption = 'Buscar'
          TabOrder = 2
          OnClick = btnBuscarConvenioClick
        end
        object cbConveniosCliente: TVariantComboBox
          Left = 126
          Top = 46
          Width = 286
          Height = 19
          Style = vcsOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 3
          OnChange = cbConveniosClienteChange
          OnDrawItem = cbConveniosClienteDrawItem
          Items = <>
        end
      end
      object gbDatosCliente: TGroupBox
        Left = 6
        Top = 86
        Width = 860
        Height = 130
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = '  Datos del Cliente  '
        TabOrder = 1
        object lblApellido: TLabel
          Left = 14
          Top = 21
          Width = 41
          Height = 13
          Caption = 'Apellido:'
        end
        object lblDomicilio: TLabel
          Left = 14
          Top = 40
          Width = 44
          Height = 13
          Caption = 'Domicilio:'
        end
        object lblComuna: TLabel
          Left = 14
          Top = 60
          Width = 43
          Height = 13
          Caption = 'Comuna:'
        end
        object lblRegion: TLabel
          Left = 14
          Top = 80
          Width = 37
          Height = 13
          Caption = 'Regi'#243'n:'
        end
        object lblDatoApellidoNombre: TLabel
          Left = 80
          Top = 21
          Width = 319
          Height = 13
          AutoSize = False
          Caption = 'lblDatoApellidoNombre'
        end
        object lblDatoDomicilio: TLabel
          Left = 80
          Top = 40
          Width = 319
          Height = 13
          AutoSize = False
          Caption = 'lblDatoDomicilio'
        end
        object lblDatoComuna: TLabel
          Left = 80
          Top = 60
          Width = 319
          Height = 13
          AutoSize = False
          Caption = 'lblDatoComuna'
        end
        object lblDatoRegion: TLabel
          Left = 80
          Top = 80
          Width = 319
          Height = 13
          AutoSize = False
          Caption = 'lblDatoRegion'
        end
        object lblDomicilioFact: TLabel
          Left = 405
          Top = 40
          Width = 44
          Height = 13
          Caption = 'Domicilio:'
        end
        object lblComunaFact: TLabel
          Left = 405
          Top = 60
          Width = 43
          Height = 13
          Caption = 'Comuna:'
        end
        object Label2: TLabel
          Left = 405
          Top = 80
          Width = 37
          Height = 13
          Caption = 'Regi'#243'n:'
        end
        object lblDatoDomicilioFact: TLabel
          Left = 472
          Top = 40
          Width = 369
          Height = 13
          AutoSize = False
          Caption = 'lblDatoDomicilioFact'
        end
        object lblDatoComunaFact: TLabel
          Left = 472
          Top = 60
          Width = 369
          Height = 13
          AutoSize = False
          Caption = 'lblDatoComunaFact'
        end
        object lblDatoRegionFact: TLabel
          Left = 472
          Top = 80
          Width = 369
          Height = 13
          AutoSize = False
          Caption = 'lblDatoRegionFact'
        end
        object lblDomicilioComprobante: TLabel
          Left = 405
          Top = 21
          Width = 125
          Height = 13
          Caption = 'Domicilio del Comprobante'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object btnSeleccionarInfracciones: TButton
          Left = 13
          Top = 99
          Width = 163
          Height = 25
          Caption = 'Seleccionar Infracciones'
          TabOrder = 0
          OnClick = btnSeleccionarInfraccionesClick
        end
      end
    end
  end
  object spObtenerInfraccionesAFacturar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerInfraccionesAFacturar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@SePuedeEmitirCertificado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = True
      end
      item
        Name = '@ValidarInfraccionesImpagas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Prepared = True
    Left = 113
    Top = 541
  end
  object ds_Infracciones: TDataSource
    DataSet = cldInfracciones
    Left = 48
    Top = 541
  end
  object Img_Tilde: TImageList
    Left = 209
    Top = 541
    Bitmap = {
      494C010102000400800010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0084848400C6C6C600F7FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00F7FF
      FF0000008400000000009C4A0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600000000000000000000000000F7FFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000084
      8400000000000084840000000000C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00848484000000
      000000000000F7FFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6000000
      8400FFFFFF00FFFFFF009C4A00000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00848484000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484840000000000C6C6C600F7FF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7FFFF0084848400000000009CAD
      AD00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484000000
      00009C4A0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C4A
      0000000000009C4A0000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7FFFF0084848400FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object ObtenerDatosPersonaNotaCobroInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosPersonaNotaCobroInfraccion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 145
    Top = 541
  end
  object spCrearNotaCobroNI: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearNotaCobroNI;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImporteInfracciones'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@ImporteGastosAdministrativos'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@ImporteIntereses'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@PeriodoInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PeriodoFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@TipoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Calle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DetalleDomicilio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@LineaComprobanteInfracciones'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 241
    Top = 541
  end
  object spObtenerCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 273
    Top = 541
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomicilioFacturacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 369
    Top = 541
  end
  object spGenerarMovimientosInfraccionPorConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GenerarMovimientosInfraccionPorConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaFacturacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TotalInfracciones'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TotalGastosCobranza'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TotalIntereses'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroMovimientoInfracciones'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimientoGastosCobranzas'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimientoIntereses'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 177
    Top = 541
  end
  object spObtenerConveniosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IncluirConveniosDeBaja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 305
    Top = 541
  end
  object spCalcularIntereses: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 300
    ProcedureName = 'CalcularIntereses'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MontoBase'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaBase'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaDeCalculo'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Intereses'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 337
    Top = 541
  end
  object cldInfracciones: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'NumeroDocumento'
        Attributes = [faFixed]
        DataType = ftString
        Size = 11
      end
      item
        Name = 'Nombre'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'NumeroConvenio'
        Attributes = [faFixed]
        DataType = ftString
        Size = 19
      end
      item
        Name = 'Facturar'
        Attributes = [faUnNamed]
        DataType = ftBoolean
      end
      item
        Name = 'CodigoInfraccion'
        DataType = ftInteger
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Fecha'
        DataType = ftDateTime
      end
      item
        Name = 'MotivoAnulacion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'PrecioInfraccion'
        Attributes = [faUnNamed]
        DataType = ftInteger
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'CodigoMotivoAnulacion'
        DataType = ftSmallint
      end
      item
        Name = 'FechaAnulacion'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoTarifaDayPassBALI'
        Attributes = [faUnNamed]
        DataType = ftInteger
      end
      item
        Name = 'TipoComprobanteFiscal'
        Attributes = [faFixed]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'NumeroComprobanteFiscal'
        DataType = ftLargeint
      end
      item
        Name = 'CodigoEstadoInterno'
        DataType = ftSmallint
      end
      item
        Name = 'CodigoEstadoExterno'
        DataType = ftSmallint
      end
      item
        Name = 'EstadoCN'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'EstadoIF'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'EstadoPago'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CodigoConcesionaria'
        DataType = ftSmallint
      end
      item
        Name = 'NombreConcesionaria'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'EsFacturable'
        Attributes = [faUnNamed]
        DataType = ftBoolean
      end
      item
        Name = 'TipoComprobanteFiscalDesc'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TipoInfraccion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CodigoTipoInfraccion'
        DataType = ftSmallint
      end
      item
        Name = 'Categoria'
        DataType = ftSmallint
      end
      item
        Name = 'ConceptoMoroso'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'Facturar'
        Fields = 'Facturar'
      end
      item
        Name = 'FacturarDESC'
        Fields = 'Facturar'
        Options = [ixDescending]
      end
      item
        Name = 'Fecha'
        Fields = 'Fecha'
      end
      item
        Name = 'FechaDESC'
        Fields = 'Fecha'
        Options = [ixDescending]
      end
      item
        Name = 'EstadoIF'
        Fields = 'EstadoIF;Fecha'
      end
      item
        Name = 'EstadoIFDESC'
        Fields = 'EstadoIF;Fecha'
        Options = [ixDescending]
      end
      item
        Name = 'EstadoCN'
        Fields = 'EstadoCN;Fecha'
      end
      item
        Name = 'EstadoCNDESC'
        Fields = 'EstadoCN;Fecha'
        Options = [ixDescending]
      end>
    IndexName = 'Fecha'
    Params = <
      item
        DataType = ftBoolean
        Name = '@SePuedeEmitirCertificado'
        ParamType = ptInputOutput
        Value = True
      end>
    StoreDefs = True
    AfterOpen = ClientDataSetAfterOpenGenerico
    AfterClose = ClientDataSetAfterOpenGenerico
    AfterPost = ClientDataSetAfterOpenGenerico
    AfterScroll = ClientDataSetAfterOpenGenerico
    Left = 17
    Top = 541
    Data = {
      440300009619E0BD01000000180000001C00000000000300000044030F4E756D
      65726F446F63756D656E746F0100490020000100055749445448020002000B00
      064E6F6D62726501004900000001000557494454480200020032000E4E756D65
      726F436F6E76656E696F01004900200001000557494454480200020013000846
      61637475726172020003001000000010436F6469676F496E6672616363696F6E
      040001000000000007506174656E746501004900200001000557494454480200
      02000A0005466563686108000800000000000F4D6F7469766F416E756C616369
      6F6E01004900000001000557494454480200020032001050726563696F496E66
      72616363696F6E04000100100000000E436F6469676F436F6E76656E696F0400
      01000000000015436F6469676F4D6F7469766F416E756C6163696F6E02000100
      000000000E4665636861416E756C6163696F6E080008000000000017436F6469
      676F5461726966614461795061737342414C490400010010000000155469706F
      436F6D70726F62616E746546697363616C010049002000010005574944544802
      0002000200174E756D65726F436F6D70726F62616E746546697363616C080001
      000000000013436F6469676F45737461646F496E7465726E6F02000100000000
      0013436F6469676F45737461646F45787465726E6F0200010000000000084573
      7461646F434E0100490000000100055749445448020002003200084573746164
      6F494601004900000001000557494454480200020032000A45737461646F5061
      676F010049002000010005574944544802000200010013436F6469676F436F6E
      636573696F6E617269610200010000000000134E6F6D627265436F6E63657369
      6F6E6172696101004900200001000557494454480200020003000C4573466163
      74757261626C650200030010000000195469706F436F6D70726F62616E746546
      697363616C4465736301004900000001000557494454480200020064000E5469
      706F496E6672616363696F6E0100490000000100055749445448020002001400
      14436F6469676F5469706F496E6672616363696F6E0200010000000000094361
      7465676F72696102000100000000000E436F6E636570746F4D6F726F736F0400
      0100000000000000}
  end
  object spActualizarInfraccionValorizada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarInfraccionValorizada'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLineaComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarifaDayPassBALI'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 80
    Top = 541
  end
  object spObtenerInfraccionesAFacturarPorRut: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerInfraccionesAFacturarPorRut;1'
    Parameters = <
      item
        Name = '@Rut'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TraerListaAmarilla'
        DataType = ftBoolean
        Value = False
      end>
    Prepared = True
    Left = 496
    Top = 541
  end
  object ds_InfraccionesPorRut: TDataSource
    DataSet = cldInfraccionesPorRut
    Left = 401
    Top = 541
  end
  object cldInfraccionesPorRut: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoInfraccion'
        DataType = ftInteger
      end
      item
        Name = 'Patente'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'FechaInfraccion'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoEstadoInterno'
        DataType = ftSmallint
      end
      item
        Name = 'EstadoCN'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoEstadoExterno'
        DataType = ftSmallint
      end
      item
        Name = 'EstadoIF'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FechaAnulacion'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoMotivoAnulacion'
        DataType = ftSmallint
      end
      item
        Name = 'MotivoAnulacion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Nombre'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'NumeroDocumento'
        Attributes = [faFixed]
        DataType = ftString
        Size = 11
      end
      item
        Name = 'PrecioInfraccion'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTarifaDayPassBALI'
        DataType = ftInteger
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'NumeroConvenio'
        Attributes = [faFixed]
        DataType = ftString
        Size = 19
      end
      item
        Name = 'TipoComprobanteFiscal'
        Attributes = [faFixed]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'TipoComprobanteFiscalDesc'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'NumeroComprobanteFiscal'
        DataType = ftLargeint
      end
      item
        Name = 'EstadoPago'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CodigoConcesionaria'
        DataType = ftSmallint
      end
      item
        Name = 'NombreConcesionaria'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'EsFacturable'
        DataType = ftBoolean
      end
      item
        Name = 'DescripcionConcesionaria'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Facturar'
        DataType = ftSmallint
      end
      item
        Name = 'TipoInfraccion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoTipoInfraccion'
        DataType = ftSmallint
      end
      item
        Name = 'Categoria_RNVM'
        DataType = ftSmallint
      end
      item
        Name = 'ConceptoMoroso'
        Attributes = [faReadonly]
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'FechaInfraccion'
        Fields = 'FechaInfraccion'
      end
      item
        Name = 'FechaInfraccionDESC'
        Fields = 'FechaInfraccion'
        Options = [ixDescending]
      end
      item
        Name = 'Patente'
        Fields = 'Patente;FechaInfraccion'
      end
      item
        Name = 'PatenteDESC'
        Fields = 'Patente;FechaInfraccion'
        Options = [ixDescending]
      end
      item
        Name = 'DescripcionConcesionaria'
        Fields = 'DescripcionConcesionaria;FechaInfraccion'
      end
      item
        Name = 'DescripcionConcesionariaDESC'
        Fields = 'DescripcionConcesionaria;FechaInfraccion'
        Options = [ixDescending]
      end
      item
        Name = 'Facturar'
        Fields = 'Facturar;FechaInfraccion'
      end
      item
        Name = 'FacturarDESC'
        Fields = 'Facturar;FechaInfraccion'
        Options = [ixDescending]
      end
      item
        Name = 'EstadoCN'
        Fields = 'EstadoCN;FechaInfraccion'
      end
      item
        Name = 'EstadoCNDESC'
        Fields = 'EstadoCN;FechaInfraccion'
        Options = [ixDescending]
      end
      item
        Name = 'EstadoIF'
        Fields = 'EstadoIF;FechaInfraccion'
      end
      item
        Name = 'EstadoIFDESC'
        Fields = 'EstadoIF;FechaInfraccion'
        Options = [ixCaseInsensitive]
      end>
    IndexName = 'FechaInfraccion'
    Params = <>
    ProviderName = 'dspInfraccionesPorRUT'
    StoreDefs = True
    AfterOpen = ClientDataSetAfterOpenGenerico
    AfterClose = ClientDataSetAfterOpenGenerico
    AfterPost = ClientDataSetAfterOpenGenerico
    AfterScroll = ClientDataSetAfterOpenGenerico
    Left = 433
    Top = 541
    object cldInfraccionesPorRutCodigoInfraccion: TIntegerField
      FieldName = 'CodigoInfraccion'
    end
    object cldInfraccionesPorRutPatente: TStringField
      FieldName = 'Patente'
      ReadOnly = True
      Size = 10
    end
    object cldInfraccionesPorRutFechaInfraccion: TDateTimeField
      FieldName = 'FechaInfraccion'
    end
    object cldInfraccionesPorRutCodigoEstadoInterno: TSmallintField
      FieldName = 'CodigoEstadoInterno'
    end
    object cldInfraccionesPorRutEstadoCN: TStringField
      FieldName = 'EstadoCN'
      Size = 50
    end
    object cldInfraccionesPorRutCodigoEstadoExterno: TSmallintField
      FieldName = 'CodigoEstadoExterno'
    end
    object cldInfraccionesPorRutEstadoIF: TStringField
      FieldName = 'EstadoIF'
      Size = 50
    end
    object cldInfraccionesPorRutFechaAnulacion: TDateTimeField
      FieldName = 'FechaAnulacion'
    end
    object cldInfraccionesPorRutCodigoMotivoAnulacion: TSmallintField
      FieldName = 'CodigoMotivoAnulacion'
    end
    object cldInfraccionesPorRutMotivoAnulacion: TStringField
      FieldName = 'MotivoAnulacion'
      Size = 50
    end
    object cldInfraccionesPorRutNombre: TStringField
      FieldName = 'Nombre'
      Size = 50
    end
    object cldInfraccionesPorRutNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
    object cldInfraccionesPorRutPrecioInfraccion: TIntegerField
      FieldName = 'PrecioInfraccion'
    end
    object cldInfraccionesPorRutCodigoTarifaDayPassBALI: TIntegerField
      FieldName = 'CodigoTarifaDayPassBALI'
    end
    object cldInfraccionesPorRutCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object cldInfraccionesPorRutNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      FixedChar = True
      Size = 19
    end
    object cldInfraccionesPorRutTipoComprobanteFiscal: TStringField
      FieldName = 'TipoComprobanteFiscal'
      FixedChar = True
      Size = 2
    end
    object cldInfraccionesPorRutTipoComprobanteFiscalDesc: TStringField
      FieldName = 'TipoComprobanteFiscalDesc'
      Size = 100
    end
    object cldInfraccionesPorRutNumeroComprobanteFiscal: TLargeintField
      FieldName = 'NumeroComprobanteFiscal'
    end
    object cldInfraccionesPorRutEstadoPago: TStringField
      FieldName = 'EstadoPago'
      FixedChar = True
      Size = 1
    end
    object cldInfraccionesPorRutCodigoConcesionaria: TSmallintField
      FieldName = 'CodigoConcesionaria'
    end
    object cldInfraccionesPorRutNombreConcesionaria: TStringField
      FieldName = 'NombreConcesionaria'
      FixedChar = True
      Size = 3
    end
    object cldInfraccionesPorRutEsFacturable: TBooleanField
      FieldName = 'EsFacturable'
    end
    object cldInfraccionesPorRutDescripcionConcesionaria: TStringField
      FieldName = 'DescripcionConcesionaria'
      FixedChar = True
      Size = 50
    end
    object cldInfraccionesPorRutFacturar: TSmallintField
      FieldName = 'Facturar'
    end
    object cldInfraccionesPorRutTipoInfraccion: TStringField
      FieldName = 'TipoInfraccion'
      Size = 9
    end
    object cldInfraccionesPorRutCodigoTipoInfraccion: TSmallintField
      FieldName = 'CodigoTipoInfraccion'
    end
    object cldInfraccionesPorRutCategoria_RNVM: TSmallintField
      FieldName = 'Categoria_RNVM'
    end
    object cldInfraccionesPorRutConceptoMoroso: TIntegerField
      FieldName = 'ConceptoMoroso'
      ReadOnly = True
    end
  end
  object dspInfraccionesPorRUT: TDataSetProvider
    DataSet = spObtenerInfraccionesAFacturarPorRut
    Left = 464
    Top = 541
  end
end
