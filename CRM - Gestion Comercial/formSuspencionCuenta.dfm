object frmSuspencionCuenta: TfrmSuspencionCuenta
  Left = 479
  Top = 221
  BorderStyle = bsDialog
  Caption = 'Suspensi'#243'n de Cuenta'
  ClientHeight = 109
  ClientWidth = 290
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 40
    Width = 136
    Height = 13
    Caption = 'Fecha Limite de Suspensi'#243'n:'
  end
  object btn_Aceptar: TButton
    Left = 124
    Top = 76
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    TabOrder = 2
    OnClick = btn_AceptarClick
  end
  object btn_Cancelar: TButton
    Left = 208
    Top = 76
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    TabOrder = 3
    OnClick = btn_CancelarClick
  end
  object ch_Notificado: TCheckBox
    Left = 16
    Top = 16
    Width = 165
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Cliente notificado'
    TabOrder = 0
  end
  object txt_FechaDeadline: TDateEdit
    Left = 168
    Top = 36
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 1
    Date = -693594
  end
end
