unit frmCambiarTag;

{*******************************************************************************
Revision : 1
Author	 : jconcheyro
Date     : 19/12/2006
Description : Se agrega el combo de Motivos para manejar la generacion de 
MovimientosCuentas por indemnizaciones de Televias

Revision : 2
Author	 : nefernandez
Date     : 25/07/2007
Description : Cambi� el tipo de datos del par�metro ContractSerialNumber
(de Integer a Int64)

Revision : 3
Author :   Fsandi
Date Created: 10-08-2007
Language : ES-AR
Description : Se agrega una validacion al televia para confirmar que solo se permiten arriendos
            para tags identificados como propiedad CN en MaestroTags

Revision 4
Author: mbecerra
Date: 30-Septiembre-2009
Description:		(Ref SS 835)
        	Se agrega la validaci�n que el telev�a est� en garant�a.

	10-Noviembre-2009
        	1.-		Se despliega un mensaje en caso de que el telev�a est� en comodato

Revision	: 5
Author		: Nelson Droguett Sierra
Date		: 26-Julio-2010
Description	: Se incluye ArriendoEnCuotas en validacion del almacen del  TAG a entregar
*******************************************************************************}


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, VariantComboBox, DmiCtrls, Convenios, DB,
  ADODB, PeaProcs, util, RStrings, UtilProc, PeaTypes, DMConnection, UtilDB,
  Menus, frmSeleccionarMovimientosTelevia, ListBoxEx, DBListEx, DBClient;

type
  TformCambiarTag = class(TForm)
    Panel1: TPanel;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    Label1: TLabel;
    lbl_Patente: TLabel;
    GroupBox1: TGroupBox;
    txt_TagDevuelto: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    cb_EstadoTagDevuelto: TVariantComboBox;
    ck_VehiculoRobado: TCheckBox;
    gb_NuevoTag: TGroupBox;
    Label4: TLabel;
    txt_TagEntregar: TEdit;
    Label5: TLabel;
    ledVerde: TLed;
    ledGris: TLed;
    Label6: TLabel;
    ledAmarilla: TLed;
    Label7: TLabel;
    Label8: TLabel;
    ledNegra: TLed;
    ObtenerEstadosConservacionTAGs: TADOStoredProc;
    spLeerDatosTAG: TADOStoredProc;
    spObtenerEstadoConservacionTAG: TADOStoredProc;
    dblConceptos: TDBListEx;
    Bevel1: TBevel;
    Label9: TLabel;
    Label10: TLabel;
    lblSeleccionar: TLabel;
    cdConceptos: TClientDataSet;
    dsConceptos: TDataSource;
    mnuGrillaConceptos: TPopupMenu;
    mnuEliminarConcepto: TMenuItem;
    AgregarConceptos1: TMenuItem;
    lblTotalMovimientos: TLabel;
    lblTotalMovimientosTxt: TLabel;
    gbModalidadEntregaTelevia: TGroupBox;
    lblModalidadEntregaTelevia: TLabel;
    spValidarSiTagTieneGarantia: TADOStoredProc;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure cb_EstadoTagDevueltoChange(Sender: TObject);
    procedure txt_TagEntregarChange(Sender: TObject);
    procedure txt_TagDevueltoKeyPress(Sender: TObject; var Key: Char);
    procedure lblSeleccionarClick(Sender: TObject);
    procedure mnuEliminarConceptoClick(Sender: TObject);
    procedure dblConceptosContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure dblConceptosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AgregarConceptos1Click(Sender: TObject);
  private
    { Private declarations }
    FCodigoEstadoConservacionTagNuevo: Integer;
    FTagOriginal: String;
    FVehiculo: TVehiculosConvenio;
    FContractSerialNumberNuevo: DWORD;
    FContextMarkNuevo: Integer;
    FPuntoEntrega: Integer;
    FCodigoUbicacionTag: Integer;
    FImporteTotalMovimientos: Int64;
    FCodigoAlmacen: integer;
    function GetRobado: Boolean;
    function GetTagNuevo: String;
    function GetEstadoConservacion: integer;
    function GetVehiculosConvenio: TVehiculosConvenio;
    function GetAlmacenDestino: integer;
	//function ValidarCategoriaTelevia(Conn :TADOConnection; CodigoTipoVehiculo: integer; ContextMark: integer; SerialNumber: DWORD): boolean;      //TASK_073_JMA_20161111
    function ValidarCategoriaTeleviaCuenta(Conn :TADOConnection; CodigoVehiculo: integer; ContextMark: integer; SerialNumber: DWORD): boolean;        //TASK_073_JMA_20161111
    function ObtenerEstadoConservacionTAG(ContractSerialNumber: Int64): Integer; // Revision 2
    function ValidarArriendoTelevia (SerialNumber:DWORD; ContextMark:Integer):Boolean;  //Revision 3
  public
    { Public declarations }
    property Robado: Boolean read GetRobado default false;
    property EstadoConservacion: integer read GetEstadoConservacion;

    property CodigoEstadoConservacionTagNuevo: Integer read FCodigoEstadoConservacionTagNuevo;
    property TagNuevo: String read GetTagNuevo;
    property Vehiculo: TVehiculosConvenio read GetVehiculosConvenio;
    property AlmacenDestino: Integer read GetAlmacenDestino;
    function Inicializar(Vehiculo: TVehiculosConvenio; CambiarTag: Boolean; PuntoEntrega: Integer): Boolean;
  end;

var
  formCambiarTag: TformCambiarTag;

const
    ALTO_CON_CAMBIO = 502;
    ALTO_SIN_CAMBIO = 392;


implementation

{$R *.dfm}

function TformCambiarTag.Inicializar(Vehiculo: TVehiculosConvenio; CambiarTag: Boolean; PuntoEntrega: Integer): Boolean;
resourcestring
	MSG_TAG_VENDIDO_NO_SE_PUEDE_CAMBIAR = 'El tag ha sido vendido, no se puede cambiar';
begin
    Result := False;
    lbl_Patente.Caption := Trim(Vehiculo.Cuenta.Vehiculo.Patente);
    FTagOriginal := SerialNumberToEtiqueta(IntToStr(Vehiculo.Cuenta.ContactSerialNumber));
    FVehiculo := Vehiculo;
    FPuntoEntrega := PuntoEntrega;
    FCodigoAlmacen := FVehiculo.Cuenta.CodigoAlmacenDestino ;


    if FCodigoAlmacen = CONST_ALMACEN_EN_COMODATO then
        lblModalidadEntregaTelevia.Caption := CONST_STR_ALMACEN_COMODATO;

    if FCodigoAlmacen = CONST_ALMACEN_ARRIENDO then
        lblModalidadEntregaTelevia.Caption := CONST_STR_ALMACEN_ARRIENDO;

    //SS 769
	if FCodigoAlmacen = CONST_ALMACEN_ENTREGADO_EN_CUOTAS then
        lblModalidadEntregaTelevia.Caption := CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS;

    cb_EstadoTagDevuelto.Items.Add(SIN_ESPECIFICAR, 0);
    ObtenerEstadosConservacionTAGs.Open;
    while not ObtenerEstadosConservacionTAGs.Eof do begin
        if not CambiarTag or
            (ObtenerEstadosConservacionTAGs.Fieldbyname('CodigoEstadoConservacionTAG').AsInteger <> 4) or
            (CambiarTag and Vehiculo.EsTagPerdido) then // 4 -> No devuelto
            cb_EstadoTagDevuelto.Items.Add(Trim(ObtenerEstadosConservacionTAGs.Fieldbyname('Descripcion').AsString), ObtenerEstadosConservacionTAGs.fieldbyname('CodigoEstadoConservacionTAG').AsInteger );
        ObtenerEstadosConservacionTAGs.Next;
    end;
    cb_EstadoTagDevuelto.ItemIndex := 0;

    Height := iif(CambiarTag, ALTO_CON_CAMBIO, ALTO_SIN_CAMBIO);
    gb_NuevoTag.Visible := CambiarTag;
    Caption := iif(CambiarTag, MSG_CAPTION_CAMBIO_TAG_CAMBIO, MSG_CAPTION_CAMBIO_TAG_DEVOLUCION);
    ck_VehiculoRobado.Visible := not CambiarTag;


    if Vehiculo.EsTagVendido then begin
    	MsgBox(MSG_TAG_VENDIDO_NO_SE_PUEDE_CAMBIAR, Caption, MB_ICONERROR);
        Exit;
    end;

    if CambiarTag and Vehiculo.EsTagPerdido then begin
        txt_TagDevuelto.Text := SerialNumberToEtiqueta(Vehiculo.Cuenta.ContactSerialNumber);
        txt_TagDevuelto.Enabled := False;
        cb_EstadoTagDevuelto.ItemIndex := ObtenerEstadoConservacionTAG(Vehiculo.Cuenta.ContactSerialNumber);
        cb_EstadoTagDevuelto.Enabled := False;
        cb_EstadoTagDevuelto.OnChange(cb_EstadoTagDevuelto);
    end;
    txt_TagDevuelto.Text := SerialNumberToEtiqueta(Vehiculo.Cuenta.ContactSerialNumber); //TASK_106_JMA_20170206
    Result := True;
end;

//Revision 3
function TformCambiarTag.ValidarArriendoTelevia(SerialNumber: DWORD;
  ContextMark: Integer): Boolean;
var
    Propiedad : string;
    NombreCortoConcesionariaNativa: string;                                     //SS_1147_MCA_20140408
begin
    Result := False;
    Propiedad := EmptyStr;
    Propiedad := QueryGetValue(DMConnections.BaseCAC,format('select dbo.ObtenerPropiedadTAG (%d,%d)', [SerialNumber, ContextMark]));

	NombreCortoConcesionariaNativa := QueryGetValue(DMConnections.BaseCAC, ('SELECT dbo.ObtenerNombreCortoConcesionaria(dbo.ObtenerConcesionariaNativa())'));	//SS_1147_MCA_20140408
    // Valida que el Telev�a solo puede darse en arriendo si es tipo nativo --'CN'
    //if Trim(Propiedad) = 'CN' then begin										//SS_1147_MCA_20140408
    if Trim(Propiedad) = NombreCortoConcesionariaNativa then begin              //SS_1147_MCA_20140408
        Result := True;
    end;
end;
//Fin de Revision 3


procedure TformCambiarTag.cb_EstadoTagDevueltoChange(Sender: TObject);
begin
    txt_TagEntregar.Enabled := (cb_EstadoTagDevuelto.Value > 0);
end;

procedure TformCambiarTag.dblConceptosContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
    mnuEliminarConcepto.Enabled := not cdConceptos.IsEmpty;
end;


procedure TformCambiarTag.dblConceptosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = VK_DELETE) or (Key = VK_BACK) then mnuEliminarConceptoClick(Sender);
end;

procedure TformCambiarTag.txt_TagEntregarChange(Sender: TObject);
resourcestring
    MSG_ADVERTENCIA_TAG_EN_COMODATO		= 'El telev�a est� en COMODATO. No entregar';		//REV.4

begin
    if gb_NuevoTag.Visible then begin
        ledVerde.Enabled 			:= False;
        ledAmarilla.Enabled 		:= False;
        ledGris.Enabled 			:= False;
        ledNegra.Enabled 			:= False;

        if (Trim(txt_TagEntregar.Text) <> '') and (length(Trim(txt_TagEntregar.Text)) > 7) and (EtiquetaToSerialNumber(PadL(Trim(txt_TagEntregar.Text),11,'0')) <> 0) then begin

            spLeerDatosTAG.Parameters.ParamByName('@ContractSerialNumber').Value := EtiquetaToSerialNumber(PadL(Trim(txt_TagEntregar.Text),11,'0'));
            spLeerDatosTAG.Open;

			btn_Aceptar.Enabled := (spLeerDatosTAG.RecordCount = 1);
            if spLeerDatosTAG.RecordCount = 1 then begin
                ledVerde.Enabled 			:= spLeerDatosTAG.FieldByName ('IndicadorListaVerde').AsBoolean;
                ledAmarilla.Enabled 		:= spLeerDatosTAG.FieldByName ('IndicadorListaAmarilla').AsBoolean;
                ledGris.Enabled 			:= spLeerDatosTAG.FieldByName ('IndicadorListaGris').AsBoolean;
                ledNegra.Enabled 			:= spLeerDatosTAG.FieldByName ('IndicadorListaNegra').AsBoolean;
                FCodigoEstadoConservacionTagNuevo := spLeerDatosTAG.FieldByName ('CodigoEstadoConservacion').AsInteger;
                FContractSerialNumberNuevo  := spLeerDatosTAG.FieldByName ('ContractSerialNumber').AsVariant;
                FContextMarkNuevo           := spLeerDatosTAG.FieldByName ('ContextMark').AsInteger;
                FCodigoUbicacionTag         := spLeerDatosTAG.FieldByName ('CodigoUbicacionTag').AsInteger;
            end;
            spLeerDatosTAG.Close;

            //REV.4
            if FCodigoUbicacionTag = CONST_ALMACEN_EN_COMODATO then begin
            	MsgBoxBalloon(MSG_ADVERTENCIA_TAG_EN_COMODATO, MSG_CAPTION_ASIGNAR_TAG, MB_ICONWARNING, txt_TagEntregar);
            end;
            //FIN REV.4
        end;
    end;
end;

procedure TformCambiarTag.AgregarConceptos1Click(Sender: TObject);
begin
    lblSeleccionarClick(Sender);
end;

procedure TformCambiarTag.btn_AceptarClick(Sender: TObject);
ResourceString
    MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION = 'Se debe ingresar al menos un motivo de indemnizaci�n';
    MSG_ERROR_ARRIENDO_INVALIDO       = 'El Televia tiene propiedad MOP, no puede ser arrendado'; //Revision 3
    MSG_ERROR_TELEVIA_O_NO_HAY_TELEVIA= 'El n�mero del telev�a es incorrecto o se encuentra en blanco';

var
	Mensaje : string;
    CodigoRetorno : integer;
begin

    if cdConceptos.IsEmpty then begin
         MsgBoxBalloon(MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION,'Error',MB_ICONSTOP, dblConceptos);
         dblConceptos.SetFocus;
         Exit;
    end;

    if not ValidarEtiqueta('', '', txt_TagDevuelto.Text , False) or (Trim(txt_TagDevuelto.Text) = '') then begin
         MsgBoxBalloon(MSG_ERROR_TELEVIA_O_NO_HAY_TELEVIA,'Error',MB_ICONSTOP, txt_TagDevuelto);
         txt_TagDevuelto.SetFocus;
         Exit;
    end;

    if StrToFloat(FTagOriginal) <> StrToFloat(txt_TagDevuelto.Text) then begin
         MsgBoxBalloon(MSG_ERROR_NUMERO_TELEVIA_BORRAR,'Error',MB_ICONSTOP, txt_TagDevuelto);
         txt_TagDevuelto.SetFocus;
         Exit;
    end;

    if cb_EstadoTagDevuelto.Value = 0 then begin
        MsgBoxBalloon(MSG_ERROR_ESTADO_TELEVIA, 'Error', MB_ICONSTOP, cb_EstadoTagDevuelto);
        cb_EstadoTagDevuelto.SetFocus;
        exit;
    end;

    // Valido que el Tag no este entregado al cliente
    if gb_NuevoTag.Visible and (not ValidarEtiqueta('', '', txt_TagEntregar.Text , False)  or (Trim(txt_TagEntregar.Text) = '')) then begin
         MsgBoxBalloon(MSG_ERROR_TELEVIA_O_NO_HAY_TELEVIA,'Error',MB_ICONSTOP, txt_TagEntregar);
         txt_TagEntregar.SetFocus;
         Exit;
    end;

    // Valida que el Tag corresponda a la Categor�a del Veh�culo
{INICIO: TERMINO: TASK_073_JMA_20161111
	if not ValidarCategoriaTelevia(DMConnections.BaseCAC, FVehiculo.Cuenta.Vehiculo.CodigoTipo,
}
        if not ValidarCategoriaTeleviaCuenta(DMConnections.BaseCAC, FVehiculo.Cuenta.Vehiculo.CodigoVehiculo,
{TERMINO: TERMINO: TASK_073_JMA_20161111}
    								FContextMarkNuevo, FContractSerialNumberNuevo) then begin
        MsgBoxBalloon(MSG_ERROR_CATEGORIA_TAG, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txt_TagEntregar );
        txt_TagEntregar.SetFocus;
        Exit;
    end;


    // Valida el estado del Telev�a
	if not(FCodigoEstadoConservacionTagNuevo In [ESTADO_CONSERVACION_BUENO, ESTADO_CONSERVACION_NO_DEVUELTO]) then begin
        MsgBoxBalloon(MSG_ERROR_TELEVIA_MALO, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txt_TagEntregar );
        txt_TagEntregar.SetFocus;
        txt_TagEntregar.SelStart := 1000;
        Exit;
    end;

    if FCodigoUbicacionTag in [CONST_ALMACEN_VENDIDO,
                               CONST_ALMACEN_EN_COMODATO,
                               CONST_ALMACEN_ARRIENDO,
                               //Rev.5 / 26-Julio-2010 / Nelson Droguett Sierra -------------------------------------------
                               CONST_ALMACEN_ENTREGADO_EN_CUOTAS] then begin
        MsgBoxBalloon(MSG_ERROR_TAN_EN_CLIENTE, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txt_TagEntregar );
        txt_TagEntregar.SetFocus;
        txt_TagEntregar.SelStart := 1000;
        Exit;
    end;

    // Valida que el Telev�a est� en el Almac�n correspondiente
    if not ValidarUbicacionTelevia(DMConnections.BaseCAC, FContextMarkNuevo, FContractSerialNumberNuevo, FPuntoEntrega) then begin
        MsgBoxBalloon(MSG_ERROR_UBICACION_TAG, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txt_TagEntregar );
        txt_TagEntregar.SetFocus;
        txt_TagEntregar.SelStart := 1000;
        Exit;
    end;

//Revision 3
    // Valida que el Telev�a pueda darse en arriendo, si eso es lo que se pretende
    if  ((AlmacenDestino = CONST_ALMACEN_ARRIENDO) OR
         //Rev.5 / 26-Julio-2010 / Nelson Droguett Sierra -----------------------------------------------------------------
         (AlmacenDestino = CONST_ALMACEN_ENTREGADO_EN_CUOTAS)) then begin //Revision 3
        if not ValidarArriendoTelevia(FContractSerialNumberNuevo, FContextMarkNuevo) then begin
            MsgBoxBalloon(MSG_ERROR_ARRIENDO_INVALIDO, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txt_TagEntregar);
            txt_TagEntregar.SetFocus;
            txt_TagEntregar.SelStart := 1000;
            Exit;
        end;
    end;
//Fin de Revision 3

//------------------REV.4
	with spValidarSiTagTieneGarantia do begin
    	Parameters.Refresh;
        Parameters.ParamByName('@ContextMark').Value := FContextMarkNuevo;
        Parameters.ParamByName('@ContractSerialNumber').Value := FContractSerialNumberNuevo;
        Parameters.ParamByName('@DescripcionError').Value := '';
        ExecProc;
        Mensaje := Parameters.ParamByName('@DescripcionError').Value;
        CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
        if Mensaje <> '' then begin
        	if CodigoRetorno = 0 then begin
            	MsgBox(Mensaje, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR);
                txt_TagEntregar.SetFocus;
            	txt_TagEntregar.SelStart := 1000;
                Exit;
            end
            else MsgBox(Mensaje, MSG_CAPTION_ASIGNAR_TAG, MB_ICONEXCLAMATION);

        end;
    end;

//--------------FIN REV.4

    ModalResult := mrOK;
end;

procedure TformCambiarTag.btn_CancelarClick(Sender: TObject);
begin
    close;
end;


function TformCambiarTag.GetRobado: Boolean;
begin
    Result := ck_VehiculoRobado.Checked;
end;

function TformCambiarTag.GetTagNuevo: String;
begin
    Result := Trim(txt_TagEntregar.Text);
end;

function TformCambiarTag.GetAlmacenDestino: integer;
begin
    Result := FCodigoAlmacen ;
end;

function TformCambiarTag.GetEstadoConservacion: integer;
begin
    Result := cb_EstadoTagDevuelto.Value;
end;

procedure TformCambiarTag.txt_TagDevueltoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9', #8]) then Key := #0;

end;

function TformCambiarTag.GetVehiculosConvenio: TVehiculosConvenio;
begin
    if gb_NuevoTag.Visible then begin
        FVehiculo.Cuenta.ContactSerialNumber := FContractSerialNumberNuevo;
        FVehiculo.Cuenta.ContextMark := FContextMarkNuevo;
        FVehiculo.Cuenta.EstadoConservacionTAG := FCodigoEstadoConservacionTagNuevo;
        FVehiculo.EsCambioTag := True;
    end;

    Result := FVehiculo;
end;



{INICIO: TASK_073_JMA_20161111
function TformCambiarTag.ValidarCategoriaTelevia(Conn :TADOConnection; CodigoTipoVehiculo: integer; ContextMark: integer; SerialNumber: DWORD): boolean;
//    Result := QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.ValidarCategoriaTAGporTipoVehiculo(' + inttostr(CodigoTipoVehiculo) + ',' + //TASK_016_FSI
    Result := QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.ValidarCategoriaTAGporCategoriaCuentas(' + inttostr(CodigoTipoVehiculo) + ',' +   //TASK_016_FSI
}
function TformCambiarTag.ValidarCategoriaTeleviaCuenta(Conn :TADOConnection; CodigoVehiculo: integer; ContextMark: integer; SerialNumber: DWORD): boolean;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.ValidarCategoriaTAGporCategoriaCuentas(' + inttostr(CodigoVehiculo) + ',' +   
{TERMINO: TASK_073_JMA_20161111}
     '0 ,' + inttostr(ContextMark) + ',' + intToStr(SerialNumber) + ')') = 1;
end;

function TformCambiarTag.ObtenerEstadoConservacionTAG(ContractSerialNumber: Int64): Integer; //Revision 2
resourcestring
    MSG_ERROR = 'Error';
begin
    Result := -1;
    spObtenerEstadoConservacionTAG.Parameters.ParamByName('@ContractSerialNumber').Value := ContractSerialNumber;
    try
        try
            spObtenerEstadoConservacionTAG.Open;
            Result := spObtenerEstadoConservacionTAG.FieldByName('CodigoEstadoConservacion').AsInteger;
        except
            on e: exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        spObtenerEstadoConservacionTAG.Close;
    end;
end;

procedure TformCambiarTag.lblSeleccionarClick(Sender: TObject);
var
    f: TfrmSeleccionarMovimientosTeleviaFORM;
begin
    Application.CreateForm(TfrmSeleccionarMovimientosTeleviaFORM, f);
    if f.Inicializar(CONST_OPERACION_TAG_CAMBIAR, FVehiculo.Cuenta.CodigoAlmacenDestino, '') and
            (f.ShowModal = mrOk) then begin
      // Cargamos el dataset con lo que se selecciono
        f.ListaConceptos.First;
        while not f.ListaConceptos.Eof do begin
            if f.ListaConceptos.FieldByName('Seleccionado').AsBoolean then begin
             //INICIO: TASK_028_ECA_20160608
             if cdConceptos.RecordCount>0 then
                cdConceptos.First;
              while not cdConceptos.eof do
              begin
                if cdConceptos.FieldByName('CodigoConcepto').Value= f.ListaConceptos.FieldByName('Codigoconcepto'   ).Value then
                begin
                   cdConceptos.Delete;

                   Break;
                end;
                cdConceptos.Next;
              end;
              //FIN: TASK_028_ECA_20160608

              //Agregamos el concepto a la lista
                cdConceptos.AppendRecord([
                    f.ListaConceptos.FieldByName('Seleccionado'     ).Value,
                    f.ListaConceptos.FieldByName('Codigoconcepto'   ).Value,
                    f.ListaConceptos.FieldByName('DescripcionMotivo').Value,
                    f.ListaConceptos.FieldByName('PrecioOrigen'     ).Value,
                    f.ListaConceptos.FieldByName('Moneda'           ).Value,
                    f.ListaConceptos.FieldByName('Cotizacion'       ).Value,
                    f.ListaConceptos.FieldByName('PrecioEnPesos'    ).Value,
                    f.ListaConceptos.FieldByName('Comentario'       ).Value
                ]);
                FImporteTotalMovimientos := FImporteTotalMovimientos + f.ListaConceptos.FieldByName('PrecioEnPesos').AsVariant;
          end;
          f.ListaConceptos.Next;
      end;
      f.Release;
    end else begin
        f.Release;
    end;
    lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);
end;

procedure TformCambiarTag.mnuEliminarConceptoClick(Sender: TObject);
begin
    if not cdConceptos.eof then begin
        FImporteTotalMovimientos := FImporteTotalMovimientos - cdConceptos.FieldByName('PrecioEnPesos').AsVariant ;
        lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);
        cdConceptos.Delete;
    end;
end;

end.
