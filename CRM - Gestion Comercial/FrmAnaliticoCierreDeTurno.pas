{-----------------------------------------------------------------------------
 Unit Name: FrmAnaliticoCierreDeTurno
 Author:    ggomez
 Purpose:   Form para ingresar un n�mero de turno y obtener el informe del
 	Anal�tico de Cierre de Turno.
 History:
-----------------------------------------------------------------------------}


unit FrmAnaliticoCierreDeTurno;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DPSControls, DmiCtrls, DB, ADODB,
  DMConnection, PeaProcs, UtilProc, Util, FrmReporteAnaliticoCierreDeTurno;

type
  TFormAnaliticoCierreDeTurno = class(TForm)
    pnl_Botones: TPanel;
    btn_Aceptar: TDPSButton;
    btn_Cancelar: TDPSButton;
    lbl_NumeroTurno: TLabel;
    bv_Datos: TBevel;
    txt_NumeroTurno: TNumericEdit;
    sp_EsTurnoCerrado: TADOStoredProc;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(txt_Caption: AnsiString): Boolean;
  end;

var
  FormAnaliticoCierreDeTurno: TFormAnaliticoCierreDeTurno;

implementation

{$R *.dfm}

procedure TFormAnaliticoCierreDeTurno.btn_CancelarClick(Sender: TObject);
begin
	Close;
end;

function TFormAnaliticoCierreDeTurno.Inicializar(
  txt_Caption: AnsiString): Boolean;
begin
    try
        Self.Caption := Caption;
        Result := True;
    except
        Result := False;
    end;
end;

procedure TFormAnaliticoCierreDeTurno.btn_AceptarClick(Sender: TObject);
resourcestring
	MSG_ERROR_NUMERO_VACIO = 'Debe ingresar un N�mero de Turno';
	MSG_ERROR_NUMERO_NEGATIVO = 'El N�mero de Turno debe ser un n�mero positivo';
    MSG_TURNO_CERRADO = 'El Turno ingresado no est� cerrado.' + CRLF + 'No se permite emitir el informe.';
    MSG_TURNO_DE_OTRO_USUARIO = 'El Turno ingresado le pertenece a otro usuario.' + CRLF + 'No se permite emitir el informe.';
var
	EsTurnoCerrado: Boolean;
    UsuarioTurno: AnsiString;
    r: TFormReporteAnaliticoCierreDeTurno;

begin
    UsuarioTurno := EmptyStr;

	(* Controlar que el turno ingresado no sea vac�o, ni negativo ni cero. *)
    if not ValidateControls([txt_NumeroTurno,
    	txt_NumeroTurno],
    	[txt_NumeroTurno.Text <> EmptyStr,
        txt_NumeroTurno.ValueInt > 0],
        Self.Caption,
        [MSG_ERROR_NUMERO_VACIO,
        MSG_ERROR_NUMERO_NEGATIVO]) then Exit;

	(* Controlar que el turno ingresado est� cerrado. *)
    with sp_EsTurnoCerrado, Parameters do begin
    	ParamByName('@NumeroTurno').Value := txt_NumeroTurno.ValueInt;
        ExecProc;
        EsTurnoCerrado := ParamByName('@EsCerrado').Value;
        UsuarioTurno := ParamByName('@UsuarioTurno').Value;
    end; // with

    if not EsTurnoCerrado then begin
    	(* Turno NO Cerrado, NO emitir el informe. *)

    	MsgBox(MSG_TURNO_CERRADO, Self.Caption, MB_ICONSTOP);
    end else begin
    	(* Turno Cerrado. *)

    	(* Controlar que el usuario del turno sea el usuario logueado. *)
        if UpperCase(Trim(UsuarioTurno)) <> UpperCase(Trim(UsuarioSistema)) then begin
           	MsgBox(MSG_TURNO_DE_OTRO_USUARIO, Self.Caption, MB_ICONSTOP);
        end else begin
        	(* Turno Cerrado y del usuario logueado, Mostrar el informe. *)

			Application.CreateForm(TFormReporteAnaliticoCierreDeTurno, r);
			try
				r.Inicializar(txt_NumeroTurno.ValueInt, UsuarioTurno);
			finally
				r.Release;
			end;

        end; // else if

    end; // else if

    (* Cierro el form porque si se hace un preview, queda este form sobre el
    preview. *)
	Close;
end;

end.
