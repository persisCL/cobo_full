object FormSeleccionArchivoImportar: TFormSeleccionArchivoImportar
  Left = 192
  Top = 145
  Width = 730
  Height = 426
  Caption = 'Importar Archivos'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 722
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlImportar: TPanel
    Left = 0
    Top = 0
    Width = 722
    Height = 392
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object dblArchivos: TDBListEx
      Left = 0
      Top = 0
      Width = 722
      Height = 351
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 250
          Header.Caption = 'Archivo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Sorting = csAscending
          IsLink = False
          OnHeaderClick = dblArchivosColumns4HeaderClick
          FieldName = 'Archivo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 90
          Header.Caption = 'Tipo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dblArchivosColumns4HeaderClick
          FieldName = 'Tipo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 170
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dblArchivosColumns4HeaderClick
          FieldName = 'Estado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 235
          Header.Caption = 'Resultado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dblArchivosColumns4HeaderClick
          FieldName = 'Resultado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 115
          Header.Caption = 'Fecha Proceso'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dblArchivosColumns4HeaderClick
          FieldName = 'FechaHora'
        end>
      DataSource = dsArchivos
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnClick = dblArchivosClick
      OnDblClick = dblArchivosDblClick
    end
    object Panel1: TPanel
      Left = 0
      Top = 351
      Width = 722
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        722
        41)
      object btnImportar: TButton
        Left = 557
        Top = 8
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Importar'
        Default = True
        Enabled = False
        TabOrder = 0
        OnClick = btnImportarClick
      end
      object btnSalir: TButton
        Left = 637
        Top = 8
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Salir'
        TabOrder = 1
        OnClick = btnSalirClick
      end
    end
  end
  object ExcelApplication: TExcelApplication
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    AutoQuit = False
    Left = 384
    Top = 362
  end
  object ExcelWorkbook: TExcelWorkbook
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    Left = 424
    Top = 362
  end
  object ExcelWorksheetVehiculos: TExcelWorksheet
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    Left = 464
    Top = 362
  end
  object dsArchivos: TDataSource
    DataSet = spObtenerArchivosImportarVehiculos
    Left = 16
    Top = 360
  end
  object spActualizarArchivoImportarVehiculos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarArchivoImportarVehiculos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Archivo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 119
    Top = 360
  end
  object spObtenerArchivosImportarVehiculos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = spObtenerArchivosImportarVehiculosAfterOpen
    ProcedureName = 'ObtenerArchivosImportarVehiculos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Orden'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 56
    Top = 360
  end
  object spActualizarEstadoArchivoImportarVehiculos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEstadoArchivoImportarVehiculos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Archivo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoResultado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@VehiculosAgregados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@VehiculosRechazados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 160
    Top = 360
  end
  object spActualizarVehiculosExcel: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarVehiculosExcel;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMarca'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Modelo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@AnioVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoTipoVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoColor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 240
    Top = 360
  end
end
