object FormConvenioExterno: TFormConvenioExterno
  Left = 375
  Top = 151
  BorderStyle = bsDialog
  Caption = 'FormConvenioExterno'
  ClientHeight = 453
  ClientWidth = 284
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 134
    Height = 13
    Caption = 'Numero de Documento:'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 48
    Width = 123
    Height = 13
    Caption = 'Numero de Convenio:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object DBListEx1: TDBListEx
    Left = 16
    Top = 80
    Width = 249
    Height = 313
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 240
        Header.Caption = 'Patente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Patente'
      end>
    DataSource = DataSource
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
  end
  object txt_NumeroDocumento: TEdit
    Left = 144
    Top = 8
    Width = 121
    Height = 21
    Color = 16444382
    ReadOnly = True
    TabOrder = 1
  end
  object txt_NumeroConvenio: TEdit
    Left = 144
    Top = 40
    Width = 121
    Height = 21
    Color = 16444382
    ReadOnly = True
    TabOrder = 2
  end
  object Panel1: TPanel
    Left = 0
    Top = 412
    Width = 284
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    DesignSize = (
      284
      41)
    object btn_ImprimirPAT: TDPSButton
      Left = 8
      Top = 8
      Width = 79
      Caption = 'Mandato PA&T'
      TabOrder = 0
      OnClick = btn_ImprimirPATClick
    end
    object BtnSalir: TDPSButton
      Left = 197
      Top = 7
      Width = 79
      Height = 26
      Hint = 'Salir del visualizador de Solicitudes'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Salir'
      TabOrder = 1
      OnClick = BtnSalirClick
    end
    object btn_ImprimirPAC: TDPSButton
      Left = 104
      Top = 8
      Width = 79
      Caption = 'Mandato PA&C'
      TabOrder = 2
      OnClick = btn_ImprimirPATClick
    end
  end
  object ObtenerVehiculosContratados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = ObtenerVehiculosContratadosAfterOpen
    ProcedureName = 'ObtenerVehiculosContratados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Top = 120
  end
  object DataSource: TDataSource
    DataSet = ObtenerVehiculosContratados
    Left = 32
    Top = 120
  end
end
