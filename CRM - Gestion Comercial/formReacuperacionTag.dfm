object frmReacuperacionTag: TfrmReacuperacionTag
  Left = 353
  Top = 198
  BorderStyle = bsDialog
  Caption = 'Recuperaci'#243'n de Televia'
  ClientHeight = 114
  ClientWidth = 254
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 24
    Width = 98
    Height = 13
    Caption = 'Televia recuperado: '
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 73
    Width = 254
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object btnAceptar: TButton
      Left = 80
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnCancelar: TButton
      Left = 168
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
  object txt_Tag: TEdit
    Left = 112
    Top = 19
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'txt_Tag'
    OnKeyPress = txt_TagKeyPress
  end
  object cb_VehiculoRecuperado: TCheckBox
    Left = 8
    Top = 48
    Width = 129
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Veh'#237'culo Recuperado'
    TabOrder = 1
  end
end
