{********************************** Unit Header ********************************
File Name : frmReportCertificadoInfracciones.pas
Author : pdominguez
Date Created: 08/07/2009
Language : ES-AR
Description : SS 784
    - Imprime el Certificado de Inrfacciones.

Revision : 1
    Author : pdominguez
    Date   : 21/09/2009
    Description : SS
        - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
            dsInfracciones: TDataSource,
            ppDBPlInfracciones: TppDBPipeline,
            ppSubReport1: TppSubReport,
            ArchivoPlantillaAnexo: String,
            LineasDetalleCertificado: Integer,
            TAG_PATENTE_N,
            TAG_FECHA_N,
            TAG_FEC_ANUL_N,
            PLANTILLA_CERTIFICADO_INFRACCIONES_ANEXO,
            CANTIDAD_LINEAS_DETALLE_CERTIFICADO

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            Inicializar,
            ImprimirCertificado,
            ppSummaryBand1BeforePrint


    
Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

*******************************************************************************}
unit frmReportCertificadoInfracciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppPrnabl, ppClass, ppStrtch, ppRichTx, ppBands, ppCache, ppComm,
  ppRelatv, ppProd, ppReport, UtilRB, jpeg, ppCtrls, ADODb, ppRegion, DBClient, ppDB, ppDBPipe, ppSubRpt, DB,
  ppParameter;

type
  TReportCertificadoInfraccionesForm = class(TForm)
    RBInterface1: TRBInterface;
    ppCertificadoInfracciones: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppRichText1: TppRichText;
    ppImage1: TppImage;
    ppLabel1: TppLabel;
    ppSummaryBand1: TppSummaryBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppHeaderBand2: TppHeaderBand;
    ppImage2: TppImage;
    ppLabel2: TppLabel;
    ppDBText1: TppDBText;
    ppDBPlInfracciones: TppDBPipeline;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    dsInfracciones: TDataSource;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppShape4: TppShape;
    ppShape5: TppShape;
    ppShape6: TppShape;
    ppRichText2: TppRichText;
    ppShape1: TppShape;
    ppShape2: TppShape;
    ppShape3: TppShape;
    procedure ppSummaryBand1BeforePrint(Sender: TObject);
  private
    { Private declarations }
    ArchivoPlantilla,
    ArchivoPlantillaAnexo: String;  // Rev. 1 (SS 828)
    LineasDetalleCertificado: Integer; // Rev. 1 (SS 828)
  public
    { Public declarations }
    function Inicializar(Conexion: TADOConnection; var DescError: String): Boolean;
    procedure ImprimirCertificado(Fecha, FechaDesde, FechaHasta: TDateTime; Tratamiento, ClienteCertificado, NumeroRut, Relacion, Patente: String; cdsInfracciones: TClientDataSet);
  end;

var
  ReportCertificadoInfraccionesForm: TReportCertificadoInfraccionesForm;

const
    TAG_DIA  = 'DIA';
    TAG_MES  = 'MES';
    TAG_ANIO = 'ANIO';
    TAG_PATENTE = 'PATENTE';
    TAG_TRATAMIENTO = 'TRATAMIENTO';
    TAG_CLIENTE_CERTIFICADO = 'CLIENTE_CERTIFICADO';
    TAG_NUMERO_RUT = 'NUMERO_RUT';
    TAG_RELACION   = 'RELACION';
    TAG_DIA_DESDE  = 'DIA_DESDE';
    TAG_MES_DESDE  = 'MES_DESDE';
    TAG_ANIO_DESDE = 'ANIO_DESDE';
    TAG_DIA_HASTA  = 'DIA_HASTA';
    TAG_MES_HASTA  = 'MES_HASTA';
    TAG_ANIO_HASTA = 'ANIO_HASTA';
    // Rev. 1 (SS 828)
    TAG_PATENTE_N  = 'PAT_%d';
    TAG_FECHA_N    = 'FEC_%d';
    TAG_FEC_ANUL_N = 'FEC2_%d';
    // Fin Rev. 1 (SS 828)
    PLANTILLA_CERTIFICADO_INFRACCIONES = 'PLANTILLA_CERTIFICADO_INFRACCIONES';
    PLANTILLA_CERTIFICADO_INFRACCIONES_ANEXO = 'PLANTILLA_CERTIFICADO_INFRACC_ANEXO';   // Rev. 1 (SS 828)
    CANTIDAD_LINEAS_DETALLE_CERTIFICADO = 'CANT_LINEAS_CERTIFICADO';    // Rev. 1 (SS 828)

implementation

{$R *.dfm}

Uses ConstParametrosGenerales, Util, UtilHTTP, UtilProc;

{******************************* Procedure Header ******************************
Procedure Name: Inicializar
Author : pdominguez
Date Created : 08/07/2009
Parameters : Conexion: TADOConnection; var DescError: String
Description : SS 784
    - Inicializa el Reporte, cargando los par�metros necesarios.

Revision : 1
    Author : pdominguez
    Date   : 21/09/2009
    Description : SS 828
        - Se a�ade la carga de los par�metros PLANTILLA_CERTIFICADO_INFRACCIONES_ANEXO y
        CANTIDAD_LINEAS_DETALLE_CERTIFICADO.
*******************************************************************************}
function TReportCertificadoInfraccionesForm.Inicializar(Conexion: TADOConnection; var DescError: String): Boolean;
    resourcestring
        MSG_NO_EXISTE_PLANTILLA = 'La plantilla del Certificado de Inrfacciones NO Existe. '  + CRLF + CRLF + 'Archivo: %s';
        MSG_PARAMETRO_NO_DEFINIDO = 'El par�metro General "%s" para la plantilla del Certificado de Infracciones NO est� Definido.';
        MSG_PARAMETRO_NO_VALIDO = 'El par�metro General "%s" para la plantilla del Certificado de Infracciones NO puede ser Cero.';
        MSG_ERROR_INESPERADO = 'Se produjo un Error al inicializar el Reporte. Error: %s';
    var                                                                             //SS_1147_NDR_20140710
      RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    Try
        ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
        try                                                                                                 //SS_1147_NDR_20140710
          ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
        except                                                                                              //SS_1147_NDR_20140710
          On E: Exception do begin                                                                          //SS_1147_NDR_20140710
            Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
            MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
            Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
          end;                                                                                              //SS_1147_NDR_20140710
        end;                                                                                                //SS_1147_NDR_20140710
        ppLabel1.Caption := UpperCase(QueryGetValue('SELECT dbo.ObtenerNombreConcesionariaNativa()'));      //SS_1147_NDR_20140710


        DescError := '';
        ArchivoPlantilla := '';
        ArchivoPlantillaAnexo := '';    // Rev. 1 (SS 828)
        LineasDetalleCertificado := 0;  // Rev. 1 (SS 828)

        ObtenerParametroGeneral(Conexion,PLANTILLA_CERTIFICADO_INFRACCIONES,ArchivoPlantilla);

        ObtenerParametroGeneral(Conexion,PLANTILLA_CERTIFICADO_INFRACCIONES_ANEXO,ArchivoPlantillaAnexo);   // Rev. 1 (SS 828)
        ObtenerParametroGeneral(Conexion,CANTIDAD_LINEAS_DETALLE_CERTIFICADO, LineasDetalleCertificado);    // Rev. 1 (SS 828)

        if Trim(ArchivoPlantilla) <> '' then begin
            if Not FileExists(ArchivoPlantilla) then DescError := Format(MSG_NO_EXISTE_PLANTILLA,[ArchivoPlantilla])
        end else DescError := Format(MSG_PARAMETRO_NO_DEFINIDO,[PLANTILLA_CERTIFICADO_INFRACCIONES]);

        // Rev. 1 (SS 828)
        if Trim(ArchivoPlantillaAnexo) <> '' then begin
            if Not FileExists(ArchivoPlantillaAnexo) then DescError := Format(MSG_NO_EXISTE_PLANTILLA,[ArchivoPlantillaAnexo])
        end else begin
            if DescError <> '' then DescError := DescError + #13#10;
            DescError := DescError + Format(MSG_PARAMETRO_NO_DEFINIDO,[PLANTILLA_CERTIFICADO_INFRACCIONES_ANEXO]);
        end;

        if LineasDetalleCertificado = 0 then begin
            if DescError <> '' then DescError := DescError + #13#10;
            DescError := DescError + Format(MSG_PARAMETRO_NO_VALIDO,[CANTIDAD_LINEAS_DETALLE_CERTIFICADO]);
        end;
        // Fin Rev. 1 (SS 828)
        Result := DescError = '';
    Except
        On E:Exception do begin
            DescError := Format(MSG_ERROR_INESPERADO,[E.Message]);
            Result := False;
        end;
    End;
end;

{******************************* Procedure Header ******************************
Procedure Name: ImprimirCertificado
Author : pdominguez
Date Created : 08/07/2009
Parameters : Fecha, FechaDesde, FechaHasta: TDateTime; Tratamiento, ClienteCertificado,
    NumeroRut, Relacion, Patente: String; cdsInfracciones: TClientDataSet
Description : SS 784
    - Prepara y lanza la impresi�n del Certificado de infracciones.

Revision : 1
    Author : pdominguez
    Date   : 21/09/2009
    Description : SS 828
        - Se a�ade el par�metro cdsInfracciones: TClientDataSet.
        - Se a�ade la impresi�n de los campos Patente, Fecha y FechaAnulacion,
        almacenados en el objeto cdsInfracciones.
*******************************************************************************}
procedure TReportCertificadoInfraccionesForm.ImprimirCertificado(Fecha, FechaDesde, FechaHasta: TDateTime; Tratamiento, ClienteCertificado, NumeroRut, Relacion, Patente: String; cdsInfracciones: TClientDataSet);
    resourcestring
        MSG_ERROR_INESPERADO = 'Se produjo un Error al imprimir el Reporte. Error: %s';
        MSG_ERROR_CAPTION = ' Impresi�n Certificado Infracciones';
    var
        Veces: Integer;
begin
    try
        dsInfracciones.DataSet := cdsInfracciones;  // Rev. 1 (SS 828)

        with ppRichText1 do begin
            LoadFromFile(ArchivoPlantilla);
            RichText := ReplaceTag(RichText, TAG_DIA,FormatDateTime('dd',Fecha));
            RichText := ReplaceTag(RichText, TAG_MES,FormatDateTime('mmmm',Fecha));
            RichText := ReplaceTag(RichText, TAG_ANIO,FormatDateTime('yyyy',Fecha));
            RichText := ReplaceTag(RichText, TAG_DIA_DESDE,FormatDateTime('dd',FechaDesde));
            RichText := ReplaceTag(RichText, TAG_MES_DESDE,FormatDateTime('mmmm',FechaDesde));
            RichText := ReplaceTag(RichText, TAG_ANIO_DESDE,FormatDateTime('yyyy',FechaDesde));
            RichText := ReplaceTag(RichText, TAG_DIA_HASTA,FormatDateTime('dd',FechaHasta));
            RichText := ReplaceTag(RichText, TAG_MES_HASTA,FormatDateTime('mmmm',FechaHasta));
            RichText := ReplaceTag(RichText, TAG_ANIO_HASTA,FormatDateTime('yyyy',FechaHasta));
            RichText := ReplaceTag(RichText, TAG_TRATAMIENTO,Tratamiento);
            RichText := ReplaceTag(RichText, TAG_CLIENTE_CERTIFICADO,ClienteCertificado);
            RichText := ReplaceTag(RichText, TAG_NUMERO_RUT,NumeroRut);
            RichText := ReplaceTag(RichText, TAG_RELACION,Relacion);
            RichText := ReplaceTag(RichText, TAG_PATENTE,Patente);

            // Rev. 1 (SS 828)
            cdsInfracciones.First;
            // Asignamos los datos a la rejilla del certificado
            for Veces := 1 to LineasDetalleCertificado do begin
                RichText := ReplaceTag(RichText, Format(TAG_PATENTE_N,[Veces]),iif(not cdsInfracciones.Eof, cdsInfracciones.FieldByName('Patente').AsString, ''));
                RichText := ReplaceTag(RichText, Format(TAG_FECHA_N,[Veces]),iif(not cdsInfracciones.Eof, cdsInfracciones.FieldByName('Fecha').AsString, ''));
                RichText := ReplaceTag(RichText, Format(TAG_FEC_ANUL_N,[Veces]),iif(not cdsInfracciones.Eof, cdsInfracciones.FieldByName('FechaAnulacion').AsString, ''));

                if not cdsInfracciones.Eof then cdsInfracciones.Next;
            end;
            // Rev. 1 (SS 828)
        end;

        // Rev. 1 (SS 828)
        // Si tenemos m�s registros que los previstos en la plantilla del certificado, asignamos los
        // datos en el Anexo, pues lo imprimiremos.
        if cdsInfracciones.RecordCount > LineasDetalleCertificado then
            with ppRichText2 do begin
                LoadFromFile(ArchivoPlantillaAnexo);
                RichText := ReplaceTag(RichText, TAG_DIA,FormatDateTime('dd',Fecha));
                RichText := ReplaceTag(RichText, TAG_MES,FormatDateTime('mmmm',Fecha));
                RichText := ReplaceTag(RichText, TAG_ANIO,FormatDateTime('yyyy',Fecha));
                RichText := ReplaceTag(RichText, TAG_TRATAMIENTO,Tratamiento);
                RichText := ReplaceTag(RichText, TAG_CLIENTE_CERTIFICADO,ClienteCertificado);
                RichText := ReplaceTag(RichText, TAG_NUMERO_RUT,NumeroRut);
                RichText := ReplaceTag(RichText, TAG_RELACION,Relacion);
                RichText := ReplaceTag(RichText, TAG_PATENTE,Patente);
            end;
        // Fin Rev. 1 (SS 828)
        RBInterface1.Execute(True);
    except
        on E:Exception do begin
            MsgBox(Format(MSG_ERROR_INESPERADO,[E.Message]), MSG_ERROR_CAPTION, MB_ICONSTOP);
        end;
    end;
end;

{******************************* Procedure Header ******************************
Procedure Name: ppSummaryBand1BeforePrint
Author : pdominguez
Date Created : 21/09/2009
Parameters : Sender: TObject
Description : SS 828
    - Habilitamos o deshabilitamos la banda Summary, la cual contiene el reporte
    del anexo. Imprimiremos el anexo siempre que los registros de detalle superen
    las l�neas de detalle de la hoja principal del certificado.
*******************************************************************************}
procedure TReportCertificadoInfraccionesForm.ppSummaryBand1BeforePrint(Sender: TObject);
begin
    ppSummaryBand1.Visible := (dsInfracciones.DataSet As TClientDataSet).RecordCount > LineasDetalleCertificado;
end;

end.
