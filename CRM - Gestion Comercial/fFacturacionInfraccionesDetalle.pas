{-------------------------------------------------------------------------------
Revision 1
	Author: nefernandez
    Date: 03-Mayo-2009
    Description: SS 784 - A ra�z del inicio del proyecto de Factura Electr�nica en CN y
    su posterior implementaci�n, se necesita realizar la modificaci�n del actual proceso de
    Facturaci�n y de los sistemas asociados a este.

Revision 2
	Author: mbecerra
    Date: 15-Mayo-2009
    Description:		(Ref. Facturacion Electronica)
                Los intereses de las infracciones se calculan de manera autom�tica,
                permitiendo al operador modificar dicho valor.

    30-Junio-2009:
            	Los ConceptosAdministrativos e Intereses deben redondearse a la decena
                de 10 m�s cercana.

Revision 3
	Author: mpiazza
    Date: 26/05/2009
    Description:		(ss-784) la validacion de controles solo se debe hacer
                    sobre los que estan hablitados, por este motivo se modifica la calidacion

Revision 4
Author: mbecerra
Date: 01-Julio-2009
Description:		el cliente solicita que la fecha de vencimiento desplegada
                	sea en base al par�metro general

Firma       :   SS_1045_CQU_20120604
Description :   Crea sobrecarga a la funci�n ObtenerDatosNotaCobroInfraccion
                agregando una variable booleana la cual indicar� si se muestra
                o no la ventana, esto con la finalidad de hacer autom�tico
                el proceso de facturaci�n.

Firma       :   SS_660_CQU_20130604
Description :   Modifica sobrecarga de la funci�n ObtenerDatosNotaCobroInfraccion,
                Se agrega el CodigoTipoInfraccion para deshabilitar los intereses
                y gastos de cobranza cuando sea infracci�n por morosidad.

Firma       :   SS_660_CQU_20140108
Description :   Se elimina una consulta SQL que est� de sobra ya que no se usa.
-------------------------------------------------------------------------------}
unit fFacturacionInfraccionesDetalle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, peaProcs, DMConnection, PeaTypes, Validate,
  DateEdit, ConstParametrosGenerales, UtilProc, ExtCtrls, UtilDB, util, SysUtilsCN; // SS_660_CQU_20130604

type
  TFormFacturacionInfraccionesDetalle = class(TForm)
    GroupBox1: TGroupBox;
    lblNombre: TLabel;
    GroupBox2: TGroupBox;
    lblDomicilio: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    GroupBox3: TGroupBox;
    lblInfracciones: TLabel;
    lblTotalInfracciones: TLabel;
    lblGastosAdm: TLabel;
    lblPorcentaje: TLabel;
    lblTotalComprobante: TLabel;
    nePrecioInfraccion: TNumericEdit;
    nePorcentaje: TNumericEdit;
    lblApellido: TLabel;
    deFechaEmision: TDateEdit;
    deFechaVencimiento: TDateEdit;
    lblFechaEmision: TLabel;
    lblFechaVencimiento: TLabel;
    lblIMporteTotalInfraccion: TLabel;
    lblImporteTotal: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    neImporteIntereses: TNumericEdit;
    Label1: TLabel;
    neImporteTotalGastosAdministrativos: TNumericEdit;
    lblDatoApellidoNombre: TLabel;
    lblDatoDomicilio: TLabel;
    lblDatoComuna: TLabel;
    lblDatoRegion: TLabel;
    Label2: TLabel;
    lblDatoDomicilioFact: TLabel;
    lblDatoComunaFact: TLabel;
    lblDatoRegionFact: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    lblDatoRUT: TLabel;
    lblDatoConvenio: TLabel;
    GroupBox4: TGroupBox;
    btnFacturar: TButton;
    BtnSalir: TButton;
    procedure neImporteTotalGastosAdministrativosChange(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure btnFacturarClick(Sender: TObject);
    procedure neImporteInteresesChange(Sender: TObject);
    procedure nePorcentajeChange(Sender: TObject);
    procedure nePrecioInfraccionChange(Sender: TObject);
    procedure deFechaEmisionChange(Sender: TObject);
  private
    { Private declarations }
    procedure LimpiarDatosCliente;
  public
    { Public declarations }
    FNombre: string;
    FCodigoComuna: AnsiString;
    FCodigoRegion: AnsiString;
    FNumeroDocumento: string;
    FCantidadInfracciones: Integer;
    FImportePorInfraccion: extended;
    FImporteTotalPorInfraccion: extended;
    FImporteTotalGastosAdministrativos: Int64;
    //REV.2
    FImporteIntereses : extended;
    FDiasVencimiento: integer;
    FCalle: string;
    FNumero: string;
    FDetalle: string;
    FCodigoTipoInfraccion,                // SS_660_CQU_20130604
    FCodigoInfraccionNormal : Integer;    // SS_660_CQU_20130604
    //function Inicializar(aNombre, aNumeroDocumento, aCalle, aNumero, aCodigoComuna, aCodigoRegion, aDetalle: AnsiString; aImportePorInfraccion, aImporteGastosAdministrativos, aImporteIntereses: extended; aFechaEmision, aFechaVencimiento: TDateTime; aCantidadInfracciones: Integer): boolean;
    //function Inicializar(aNombre, aNumeroDocumento, aCalle, aNumero, aCodigoComuna, aCodigoRegion, aDetalle: AnsiString; ImporteTotal: int64; aImporteGastosAdministrativos, aImporteIntereses: extended; aFechaEmision, aFechaVencimiento: TDateTime; aCantidadInfracciones: Integer): boolean;
    function Inicializar(	aNumeroDocumento, aNumeroConvenio, aDomicilioFact,
    						aComunaFact, aRegionFact, aApellidoNombre, aDomicilio,
                            aComuna, aRegion: AnsiString;
                            ImporteTotal: int64;
                            aImporteGastosAdministrativos, aImporteIntereses: extended;
                            aFechaEmision, aFechaVencimiento: TDateTime;
                            aCantidadInfracciones: Integer): boolean;

  end;

  //function ObtenerDatosNotaCobroInfraccion(var Nombre, Documento, Calle, Numero, CodigoComuna, CodigoRegion, Detalle: ANsiString; var ImportePorInfraccion: int64; var ImporteGastosAdministrativos, ImporteIntereses: extended; var fechaEmision, fechavencimiento: TDateTime; CantidadInfracciones: Integer): Boolean;
  function ObtenerDatosNotaCobroInfraccion(	NumeroDocumento, NumeroConvenio, DomicilioFact,
  											ComunaFact, RegionFact, ApellidoNombre, Domicilio,
                                            Comuna, Region: AnsiString;
                                            var ImportePorInfraccion: int64;
                                            var ImporteGastosAdministrativos, ImporteIntereses: extended;
                                            var fechaEmision, fechavencimiento: TDateTime;
                                            CantidadInfracciones: Integer): Boolean; overload; // SS_1045_CQU_20120604
  // Inicio Bloque SS_1045_CQU_20120604
  function ObtenerDatosNotaCobroInfraccion(	NumeroDocumento, NumeroConvenio, DomicilioFact,
  											ComunaFact, RegionFact, ApellidoNombre, Domicilio,
                                            Comuna, Region: AnsiString;
                                            var ImportePorInfraccion: int64;
                                            var ImporteGastosAdministrativos, ImporteIntereses: extended;
                                            var fechaEmision, fechavencimiento: TDateTime;
                                            CantidadInfracciones: Integer;
                                            //MostrarVentana : Boolean                              // SS_660_CQU_20130604
                                            //): Boolean; overload;                                 // SS_660_CQU_20130604
                                            MostrarVentana : Boolean;                               // SS_660_CQU_20130604
                                            CodigoTipoInfraccion : Integer = 1): Boolean; overload; // SS_660_CQU_20130604
  // Fin Bloque SS_1045_CQU_20120604
implementation

{$R *.dfm}

 //REV.2
{----------------------------------------------------------
            	Redondear

 Author: mbecerra
 Date: 30-Junio-2009
 Description:		(Ref.Facturaci�n Electr�nica)
                Se crea esta funci�n que redondea, ya que el Round de Delphi
                no opera correctamente en el 100% de los casos
 -------------------------------------------------------------------}
function Redondear(Monto : integer) : integer;
var
    ValorModulo : integer;
begin
    ValorModulo := Monto mod 10;
    if ValorModulo >= 5 then Result := Monto + (10 - ValorModulo)
    else Result := Monto - ValorModulo;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author :
Date Created :
Description :
Parameters : aNombre, aNumeroDocumento, aCalle, aNumero,  aCodigoComuna, aCodigoRegion, aDetalle: AnsiString; aImportePorInfraccion, aImporteGastosAdministrativos, aImporteIntereses: extended; aFechaEmision, aFechaVencimiento: TDateTime; aCantidadInfracciones: Integer
Return Value : boolean
*******************************************************************************}
//function TFormFacturacionInfraccionesDetalle.Inicializar(aNombre, aNumeroDocumento, aCalle, aNumero,aCodigoComuna, aCodigoRegion, aDetalle: AnsiString; aImportePorInfraccion, aImporteGastosAdministrativos, aImporteIntereses: extended; aFechaEmision, aFechaVencimiento: TDateTime; aCantidadInfracciones: Integer): boolean;
//function TFormFacturacionInfraccionesDetalle.Inicializar(aNombre, aNumeroDocumento, aCalle, aNumero, aCodigoComuna, aCodigoRegion, aDetalle: AnsiString; ImporteTotal: int64; aImporteGastosAdministrativos, aImporteIntereses: extended; aFechaEmision, aFechaVencimiento: TDateTime; aCantidadInfracciones: Integer): boolean;
function TFormFacturacionInfraccionesDetalle.Inicializar(aNumeroDocumento, aNumeroConvenio, aDomicilioFact, aComunaFact, aRegionFact, aApellidoNombre, aDomicilio, aComuna, aRegion: AnsiString; ImporteTotal: int64; aImporteGastosAdministrativos, aImporteIntereses: extended; aFechaEmision, aFechaVencimiento: TDateTime; aCantidadInfracciones: Integer): boolean;
resourcestring
    MSG_ERROR_CARGA_PARAMETRO_PORCENTAJE = 'Error cargando par�metro de porcentaje de gastos de cobranzas.' ;
    CONSULTA_CONSTANTE  =   'SELECT dbo.CONST_CODIGO_TIPO_INFRACCION_NORMAL()';    // SS_660_CQU_20130604
    MSG_ERROR_CODIGO    =   'No se pudo obtener el C�digo de las Infracciones';    // SS_660_CQU_20130604
var
    PorcentajeGastosCobranzas: integer;
    FechaHora: TDateTime;

    //REV.2  30-Junio-2009
    ValorModulo : integer;
begin
    result := false;

    LimpiarDatosCliente;

    //Dias de Vencimiento
    FDiasVencimiento := 0;

    // Cargamos los par�metros generales
    if not ObtenerParametroGeneral(DMCOnnections.BaseCAC, PORCENTAJE_GASTOS_COBRANZAS_BI, PorcentajeGastosCobranzas) then begin
        MsgBoxErr(MSG_ERROR_CARGA_PARAMETRO_PORCENTAJE, '', Caption, MB_ICONSTOP);
        exit;
    end;

    // Cargo la Constante para el tipo de infraccion normal
    try                                                                                                         // SS_660_CQU_20130604
        FCodigoInfraccionNormal := SysUtilsCN.QueryGetSmallIntValue(DMConnections.BaseCAC, CONSULTA_CONSTANTE); // SS_660_CQU_20130604
    except                                                                                                      // SS_660_CQU_20130604
        MsgBoxErr(MSG_ERROR_CODIGO, '', caption, MB_ICONERROR);                                                 // SS_660_CQU_20130604
        Exit;                                                                                                   // SS_660_CQU_20130604
    end;                                                                                                        // SS_660_CQU_20130604
    if FCodigoTipoInfraccion = 0 then FCodigoTipoInfraccion := FCodigoInfraccionNormal;                         // SS_660_CQU_20130604
    
    // Cargamos los datos Domicilio Comprobante
    lblDatoRUT.Caption            := aNumeroDocumento;
    lblDatoConvenio.Caption       := aNumeroConvenio;
    lblDatoDomicilioFact.Caption  := aDomicilioFact;
    lblDatoComunaFact.Caption     := aComunaFact;
    lblDatoRegionFact.Caption     := aRegionFact;

    // Cargamos los combos de regi�n y comuna
    ///CargarRegiones(DMConnections.BaseCAC, cbRegion, PAIS_CHILE, aCodigoRegion);
    ///CargarComunas(DMConnections.BaseCAC, cbComuna, PAIS_CHILE, aCodigoRegion, aCodigoComuna);

    // Cargamos los datos del domicilio
    (*
    eCalle.text     := aCalle;
    eNumero.text    := aNumero;
    eDetalle.text   := aDetalle;
    FCodigoRegion   := trim(copy(cbRegion.text, length(cbRegion.text) - 3, 4));
    FCodigoComuna   := trim(copy(cbComuna.text, length(cbComuna.text) - 3, 4));
    *)
    lblDatoApellidoNombre.Caption   := aApellidoNombre;
    lblDatoDomicilio.Caption    	:= aDomicilio;
    lblDatoComuna.Caption           := aComuna;
    lblDatoRegion.Caption       	:= aRegion;

	// Cargamos los valores del comprobante
	FCantidadInfracciones 		:= aCantidadInfracciones;
	//nePrecioInfraccion.Value 	:= ImporteTotal / 100;
    //Le quito los decimales porque no lo puedo mostrar as�.
    nePrecioInfraccion.Value 	:= ImporteTotal;
	//nePorcentaje.Value 			:= PorcentajeGastosCobranzas;                                               // SS_660_CQU_20130604
    if (FCodigoTipoInfraccion = FCodigoInfraccionNormal) then nePorcentaje.Value := PorcentajeGastosCobranzas   // SS_660_CQU_20130604
    else begin                                                                                                  // SS_660_CQU_20130604
        PorcentajeGastosCobranzas := 0;                                                                         // SS_660_CQU_20130604
        nePorcentaje.Value := PorcentajeGastosCobranzas;                                                        // SS_660_CQU_20130604
        nePorcentaje.Enabled := false;                                                                          // SS_660_CQU_20130604
    end;                                                                                                        // SS_660_CQU_20130604

	//FImportePorInfraccion 		:= ImporteTotal / 100;
	//FImporteTotalPorInfraccion 	:=  FImportePorInfraccion * FCantidadInfracciones;
    FImporteTotalPorInfraccion := ImporteTotal;
	FImporteTotalGastosAdministrativos := trunc((FImporteTotalPorInfraccion * nePorcentaje.Value) / 100);
    // Mostramos los valores en los labels
    lblInfracciones.Caption 		    := format('Se han seleccionado (%d) Infracciones a facturar, de un importe Total de :', [FCantidadInfracciones]);
    lblImporteTotalInfraccion.Caption   := formatFloat(FORMATO_IMPORTE, FImporteTotalPorInfraccion);
    //lblImporteTotalGastosAdministrativos.Caption := formatFloat(FORMATO_IMPORTE, FImporteTotalGastosAdministrativos);
    {REV.2 neImporteTotalGastosAdministrativos.ValueInt := FImporteTotalGastosAdministrativos;
    lblImporteTotal.Caption             := formatFloat(FORMATO_IMPORTE, FImporteTotalPorInfraccion + FImporteTotalGastosAdministrativos);
    }

    // Inicializamos los valores de fecha
    ObtenerParametroGeneral(DMCOnnections.BaseCAC, DIAS_VENCIMIENTO_COMPROBANTE, FDiasVencimiento);
    FechaHora := NowBase(DMConnections.BaseCAC);
    deFechaEmision.Date := FechaHora;
    deFechaVencimiento.Date := FechaHora + FDiasVencimiento;

    deFechaEmision.Enabled := False;
    deFechaVencimiento.Enabled := False;


    //REV.2
    //Redondear
    aImporteIntereses := Redondear(Trunc(aImporteIntereses));
    FImporteTotalGastosAdministrativos := Redondear(FImporteTotalGastosAdministrativos);

    //desplegar
    FImporteIntereses := aImporteIntereses;
    //neImporteIntereses.Value := FImporteIntereses;            // SS_660_CQU_20130604
    if (FCodigoTipoInfraccion = FCodigoInfraccionNormal) then   // SS_660_CQU_20130604
        neImporteIntereses.Value := FImporteIntereses           // SS_660_CQU_20130604
    else begin                                                  // SS_660_CQU_20130604
        FImporteIntereses := 0;                                 // SS_660_CQU_20130604
        neImporteIntereses.Value := FImporteIntereses;          // SS_660_CQU_20130604
        neImporteIntereses.Enabled := False;                    // SS_660_CQU_20130604
    end;                                                        // SS_660_CQU_20130604

    //neImporteTotalGastosAdministrativos.ValueInt := FImporteTotalGastosAdministrativos;   // SS_660_CQU_20130604
    if (FCodigoTipoInfraccion = FCodigoInfraccionNormal) then                               // SS_660_CQU_20130604
        neImporteTotalGastosAdministrativos.ValueInt := FImporteTotalGastosAdministrativos  // SS_660_CQU_20130604
    else begin                                                                              // SS_660_CQU_20130604
        FImporteTotalGastosAdministrativos := 0;                                            // SS_660_CQU_20130604
        neImporteTotalGastosAdministrativos.ValueInt := FImporteTotalGastosAdministrativos; // SS_660_CQU_20130604
        neImporteTotalGastosAdministrativos.Enabled := False;                               // SS_660_CQU_20130604
    end;                                                                                    // SS_660_CQU_20130604
    lblImporteTotal.Caption := FormatFloat(FORMATO_IMPORTE,	FImporteTotalPorInfraccion +
    														FImporteTotalGastosAdministrativos +
                                                            FImporteIntereses);
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: ObtenerDatosNotaCobroInfraccion
Author :
Date Created :
Description :
Parameters : var Nombre, Documento, Calle, Numero, CodigoComuna, CodigoRegion, Detalle: ANsiString; var ImportePorInfraccion, ImporteGastosAdministrativos, ImporteIntereses: extended; var fechaEmision, fechavencimiento: TDateTime; CantidadInfracciones: Integer
Return Value : Boolean

Revision 1:
    Author : ggomez
    Date : 24/05/2006
    Description :
        - Cambi� el uso de Free por Release.
        - Agregu� un try finally para liberar el recurso si es que ocurre un
        error.

*******************************************************************************}
//function ObtenerDatosNotaCobroInfraccion(var Nombre, Documento, Calle, Numero, CodigoComuna, CodigoRegion, Detalle: ANsiString; var ImportePorInfraccion, ImporteGastosAdministrativos, ImporteIntereses: extended; var fechaEmision, fechavencimiento: TDateTime; CantidadInfracciones: Integer): Boolean;
function ObtenerDatosNotaCobroInfraccion(NumeroDocumento, NumeroConvenio, DomicilioFact, ComunaFact, RegionFact, ApellidoNombre, Domicilio, Comuna, Region: ANsiString; var ImportePorInfraccion: int64; var ImporteGastosAdministrativos, ImporteIntereses: extended; var fechaEmision, fechavencimiento: TDateTime; CantidadInfracciones: Integer): Boolean;
var
  F: TFormFacturacionInfraccionesDetalle;
begin
    Application.CreateForm(TFormFacturacionInfraccionesDetalle, f);
    try
        //if f.Inicializar(Nombre, Documento, Calle, Numero, CodigoComuna, CodigoRegion, Detalle, ImportePorInfraccion, ImporteGastosAdministrativos, ImporteIntereses, FechaEmision, FechaVencimiento, CantidadInfracciones) then f.showmodal;
        if f.Inicializar(NumeroDocumento, NumeroConvenio, DomicilioFact, ComunaFact, RegionFact, ApellidoNombre, Domicilio, Comuna, Region, ImportePorInfraccion, ImporteGastosAdministrativos, ImporteIntereses, FechaEmision, FechaVencimiento, CantidadInfracciones) then f.showmodal;

        if f.ModalResult = mrOk then begin
            ///Nombre := f.eNombreCompleto.text;
            ///Documento := f.eDocumento.text;
            ///Calle := f.eCalle.text;
            ///CodigoComuna := f.FCodigoComuna;
            ///CodigoRegion := f.FCodigoRegion;
            ///Detalle := f.eDetalle.text;
            //ImportePorInfraccion := f.nePrecioInfraccion.Value;
            ImportePorInfraccion := ImportePorInfraccion;
            ImporteGastosAdministrativos := f.FImporteTotalGastosAdministrativos;
            ImporteIntereses := f.neImporteIntereses.Value;
            FechaEmision := f.deFechaEmision.date;
            FechaVencimiento := f.deFechaVencimiento.Date;
            Result := true;
        end else begin
            Result := False;
        end;
    finally
//    f.Free;
        f.Release;
    end; // finally
end;

procedure TFormFacturacionInfraccionesDetalle.deFechaEmisionChange(Sender: TObject);
begin
    if deFechaEmision.Date = nullDate then begin
        deFechaVencimiento.Date := nulldate;
    end else begin
        deFechaVencimiento.Date := deFechaEmision.Date + FDiasVencimiento;
    end;
end;

{******************************** Function Header ******************************
Function Name: nePrecioInfraccionChange
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None
Revision :1
    Author : vpaszkowicz
    Date : 31/03/2008
    Description : Quito esta posibilidad
*******************************************************************************}
procedure TFormFacturacionInfraccionesDetalle.nePrecioInfraccionChange(Sender: TObject);
begin
    {FImporteTotalPorInfraccion := nePrecioInfraccion.ValueInt * FCantidadInfracciones;
    FImporteTotalGastosAdministrativos := trunc((FImporteTotalPorInfraccion * nePorcentaje.Value) / 100);
    lblImporteTotalInfraccion.Caption := formatFloat(FORMATO_IMPORTE, FImporteTotalPorInfraccion);
    neImporteTotalGastosAdministrativos.ValueInt := FImporteTotalGastosAdministrativos;
    lblImporteTotal.Caption := formatFloat(FORMATO_IMPORTE, FImporteTotalPorInfraccion + FImporteTotalGastosAdministrativos + neImporteIntereses.Value);}
end;

procedure TFormFacturacionInfraccionesDetalle.nePorcentajeChange(Sender: TObject);
begin
    FImporteTotalGastosAdministrativos := trunc((FImporteTotalPorInfraccion * nePorcentaje.Value) / 100);
    //REV2
    FImporteTotalGastosAdministrativos := Redondear(FImporteTotalGastosAdministrativos);
    
    neImporteTotalGastosAdministrativos.ValueInt 	:= FImporteTotalGastosAdministrativos;
    lblImporteTotal.Caption := formatFloat(FORMATO_IMPORTE,FImporteTotalPorInfraccion + FImporteTotalGastosAdministrativos + neImporteIntereses.Value);
end;

procedure TFormFacturacionInfraccionesDetalle.neImporteTotalGastosAdministrativosChange(Sender: TObject);
begin
    FImporteTotalGastosAdministrativos := neImporteTotalGastosAdministrativos.ValueInt;
    lblImporteTotal.Caption := formatFloat(FORMATO_IMPORTE,FImporteTotalPorInfraccion + FImporteTotalGastosAdministrativos + neImporteIntereses.Value);
end;

procedure TFormFacturacionInfraccionesDetalle.neImporteInteresesChange(Sender: TObject);
begin
    lblImporteTotal.Caption := formatFloat(FORMATO_IMPORTE,FImporteTotalPorInfraccion + FImporteTotalGastosAdministrativos + neImporteIntereses.Value);
end;
{-------------------------------------------------------------------------------
Revision 1
	Author: mpiazza
    Date: 26/05/2009
    Description:		(ss-784) la validacion de controles solo se debe hacer
                    sobre los que estan hablitados, por este motivo se
                    modifica la Validacion (validateControls)
-------------------------------------------------------------------------------}
procedure TFormFacturacionInfraccionesDetalle.btnFacturarClick(Sender: TObject);
resourcestring
    MSG_DOCUMENTO_OBLIGATORIO = 'Se debe ingresar un n�mero de documento (RUT).';
    MSG_RUT_INCORRECTO        = 'El RUT ingresado es incorrecto.';
    MSG_NOMBRE_OBLIGATORIO    = 'Se debe ingresar un nombre.';
    MSG_CALLE_OBLIGATORIA     = 'Se debe ingresar una calle para completar el domicilio.';
	MSG_NUMERO_OBLIGATORIO    = 'Se debe ingresar el n�mero para completar el domicilio.';
	MSG_PRECIO_UNITARIO_OBLIGATORIO = 'Debe ingresar un valor mayor a cero para generar el comprobante.';
	MSG_FECHA_EMISION_INCORRECTA = 'Se debe ingresar como fecha de emisi�n una fecha mayor o igual a %s (Fecha �ltima Nota de Cobro Infracci�n emitida) ';
	MSG_FECHA_VENCIMIENTO_INCORRECTA = 'Se debe ingresar como fecha de vencimiento una fecha mayor a la fecha de emisi�n.';
    MSG_PORCENTAJE_INVALIDO             = 'Debe ingresar un porcentaje entre 0 y 200';
    MSG_TOTAL_GASTOS_ADMIN_INVALIDO     = 'Debe ingresar un porcentaje entre 0 y 9.999.999';
    MSG_INTERES_INCORRECTO              = 'El importe para el inter�s debe ser mayor que cero o no debe completarse';
Const
    STR_DO_YOU_WANT_TO_EMIT_NOTE_OF_COLLECTION = '�Desea Emitir la Nota de Cobro Infractor?';
//var                                                                                                                                                                                                                           // SS_660_CQU_20140108
//    FechaUltimoNotaCobroInfraccion: TDateTime;                                                                                                                                                                                // SS_660_CQU_20140108
begin
    //FechaUltimoNotaCobroInfraccion := QueryGetValueDateTime(DMConnections.BaseCAC, 'SELECT ISNULL((SELECT MAX(FechaEmision) FROM Comprobantes (NOLOCK) WHERE TipoComprobante = ''NI''), CAST(''19000101'' as DATETIME))');    // SS_660_CQU_20140108

	if not validateControls(
	  [nePrecioInfraccion, nePorcentaje, neImporteTotalGastosAdministrativos,
      neImporteIntereses],
	  [nePrecioInfraccion.value > 0,
       (nePorcentaje.Value >= 0)  and (nePorcentaje.Value <= 200),
       (neImporteTotalGastosAdministrativos.Value >= 0) and (neImporteTotalGastosAdministrativos.Value <= 9999999),
	     ((Trim(neImporteIntereses.Text) = '')or((Trim(neImporteIntereses.Text) <> '')and (StrToInt(neImporteIntereses.Text)> 0)))],
       caption,
      [MSG_PRECIO_UNITARIO_OBLIGATORIO,
       MSG_PORCENTAJE_INVALIDO,
       MSG_TOTAL_GASTOS_ADMIN_INVALIDO,
       MSG_INTERES_INCORRECTO]) then
        exit;

    (*
	If MsgBox(STR_DO_YOU_WANT_TO_EMIT_NOTE_OF_COLLECTION, self.Caption, MB_YESNO + MB_ICONQUESTION) <> IDYES then begin
        Exit;
    end;
    *)

    ModalResult := mrOk;
end;

procedure TFormFacturacionInfraccionesDetalle.BtnSalirClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TFormFacturacionInfraccionesDetalle.LimpiarDatosCliente;
begin
    lblDatoApellidoNombre.Caption := '';
    lblDatoDomicilio.Caption := '';
    lblDatoComuna.Caption := '';
    lblDatoRegion.Caption := '';

    lblDatoDomicilioFact.Caption := '';
    lblDatoComunaFact.Caption := '';
    lblDatoRegionFact.Caption := '';
    lblDatoRUT.Caption := '';
    lblDatoConvenio.Caption := '';
end;

// Inicio Bloque SS_1045_CQU_20120604
{******************************** Function Header ******************************
Function Name   : ObtenerDatosNotaCobroInfraccion
Author          :
Date Created    :
Description     : Esta Sobre carga realiza lo mismo que la funcion original,
                  la diferencia es que a esta se le debe indicar mediante el par�metro [MostrarVentana]
                  si se despliega la pantalla o no.
Parameters      : NumeroDocumento, NumeroConvenio, DomicilioFact, ComunaFact, RegionFact
                  , ApellidoNombre, Domicilio, Comuna, Region: ANsiString; var ImportePorInfraccion: int64;
                  var ImporteGastosAdministrativos, ImporteIntereses: extended; var fechaEmision, fechavencimiento: TDateTime;
                  CantidadInfracciones: Integer; MostrarVentana : Boolean
Return Value    : Boolean
Firma           : SS_1045_CQU_20120604
*******************************************************************************}
function ObtenerDatosNotaCobroInfraccion(NumeroDocumento, NumeroConvenio, DomicilioFact, ComunaFact, RegionFact, ApellidoNombre,
    Domicilio, Comuna, Region: ANsiString; var ImportePorInfraccion: int64; var ImporteGastosAdministrativos, ImporteIntereses: extended;
//    var fechaEmision, fechavencimiento: TDateTime; CantidadInfracciones: Integer; MostrarVentana : Boolean): Boolean;                                     // SS_660_CQU_20130604
    var fechaEmision, fechavencimiento: TDateTime; CantidadInfracciones: Integer; MostrarVentana : Boolean; CodigoTipoInfraccion : Integer = 1): Boolean;   // SS_660_CQU_20130604
var
  F: TFormFacturacionInfraccionesDetalle;
begin
    Application.CreateForm(TFormFacturacionInfraccionesDetalle, f);
    try
        f.FCodigoTipoInfraccion := CodigoTipoInfraccion;    // SS_660_CQU_20130604
        if f.Inicializar(NumeroDocumento, NumeroConvenio, DomicilioFact, ComunaFact, RegionFact
                , ApellidoNombre, Domicilio, Comuna, Region, ImportePorInfraccion, ImporteGastosAdministrativos
                , ImporteIntereses, FechaEmision, FechaVencimiento, CantidadInfracciones) then
        begin
            if (MostrarVentana) then
                f.ShowModal
            else begin
                f.Hide;
                f.btnFacturar.Click;
            end;
        end;
        if f.ModalResult = mrOk then begin
            ImportePorInfraccion := ImportePorInfraccion;
            ImporteGastosAdministrativos := f.FImporteTotalGastosAdministrativos;
            ImporteIntereses := f.neImporteIntereses.Value;
            FechaEmision := f.deFechaEmision.date;
            FechaVencimiento := f.deFechaVencimiento.Date;
            Result := true;
        end else begin
            Result := False;
        end;
    finally
        f.Release;
    end;
end;
// Fin Bloque SS_1045_CQU_20120604

end.
