object formCodigoConvenioFacturacion: TformCodigoConvenioFacturacion
  Left = 0
  Top = 0
  Hint = 'Convenios de Facturaci'#243'n'
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Convenios de Facturaci'#243'n'
  ClientHeight = 391
  ClientWidth = 631
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  DesignSize = (
    631
    391)
  PixelsPerInch = 96
  TextHeight = 13
  object grp_PACgrp1: TGroupBox
    Left = 8
    Top = 1
    Width = 601
    Height = 136
    TabOrder = 0
    object lbl_Personeria: TLabel
      Left = 7
      Top = 19
      Width = 67
      Height = 13
      Caption = 'Personer'#237'a:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl1: TLabel
      Left = 7
      Top = 47
      Width = 31
      Height = 13
      Caption = 'RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl3: TLabel
      Left = 7
      Top = 106
      Width = 50
      Height = 13
      Caption = 'Apellido:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl4: TLabel
      Left = 241
      Top = 106
      Width = 100
      Height = 13
      Caption = 'Apellido Materno:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_Nombre: TLabel
      Left = 7
      Top = 77
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRazonSocial: TLabel
      Left = 247
      Top = 19
      Width = 80
      Height = 13
      Caption = 'Raz'#243'n Social:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object txt_RUT: TEdit
      Left = 80
      Top = 47
      Width = 121
      Height = 21
      Hint = 'N'#250'mero de documento'
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = False
      MaxLength = 11
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object txt_Nombre: TEdit
      Left = 80
      Top = 74
      Width = 258
      Height = 21
      Hint = 'Nombre'
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = False
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object txt_Apellido: TEdit
      Left = 80
      Top = 103
      Width = 155
      Height = 21
      Hint = 'Apellido paterno'
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = False
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
    object txt_ApellidoMaterno: TEdit
      Left = 344
      Top = 103
      Width = 193
      Height = 21
      Hint = 'Apellido materno'
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = False
      MaxLength = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
    end
    object txt_RazonSocial: TEdit
      Left = 333
      Top = 19
      Width = 260
      Height = 21
      Hint = 'Nombre'
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = False
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      Visible = False
    end
    object txt_Personeria: TEdit
      Left = 80
      Top = 20
      Width = 161
      Height = 21
      Hint = 'Personeria'
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = False
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
  end
  object lstConvenios: TDBListEx
    Left = 8
    Top = 143
    Width = 601
    Height = 208
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 0
        Header.Caption = 'C'#243'digo Convenio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'CodigoConvenio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Numero Convenio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroConvenio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Concesionaria'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Descripcion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Tipo Convenio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'TipoConvenio'
      end>
    DataSource = ds_ConveniosRUT
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnDblClick = lstConveniosDblClick
  end
  object BtnSalir: TButton
    Left = 479
    Top = 357
    Width = 130
    Height = 26
    Hint = 'Salir del visualizador de Convenios de Facturaci'#243'n'
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 2
    OnClick = BtnSalirClick
  end
  object btn_Seleccionar: TButton
    Left = 341
    Top = 357
    Width = 130
    Height = 26
    Hint = 'Seleccionar convenio de Facturaci'#243'n'
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'S&eleccionar'
    TabOrder = 3
    OnClick = btn_SeleccionarClick
  end
  object ds_ConveniosRUT: TDataSource
    DataSet = spObtenerConveniosRUT
    Left = 264
    Top = 176
  end
  object spObtenerConveniosRUT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosRUT;1'
    Parameters = <
      item
        Name = '@NumeroDocumento'
        Size = -1
        Value = Null
      end>
    Left = 152
    Top = 176
  end
end
