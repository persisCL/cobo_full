object frmPatronBusqueda: TfrmPatronBusqueda
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Buscar...'
  ClientHeight = 135
  ClientWidth = 373
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 32
    Top = 16
    Width = 277
    Height = 13
    Caption = 'Ingrese RUT, Apellidos o Nombre para filtrar resultados...'
  end
  object edBuscar: TEdit
    Left = 29
    Top = 48
    Width = 305
    Height = 21
    TabOrder = 0
    OnKeyPress = edBuscarKeyPress
  end
  object btnBuscar: TButton
    Left = 144
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Buscar'
    TabOrder = 1
    OnClick = btnBuscarClick
  end
end
