object FormConsultaEstacionamientos: TFormConsultaEstacionamientos
  Left = 103
  Top = 157
  Caption = 'Consulta de Estacionamientos'
  ClientHeight = 486
  ClientWidth = 969
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 985
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlFiltro: TPanel
    Left = 0
    Top = 0
    Width = 969
    Height = 73
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object grpFiltros: TGroupBox
      Left = 0
      Top = 0
      Width = 969
      Height = 73
      Align = alTop
      Caption = 'Filtros'
      TabOrder = 0
      DesignSize = (
        969
        73)
      object lblFechaDesde: TLabel
        Left = 197
        Top = 19
        Width = 84
        Height = 13
        Caption = 'Fecha  &Desde:'
        FocusControl = txt_FechaDesde
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblFechaHasta: TLabel
        Left = 197
        Top = 45
        Width = 77
        Height = 13
        Caption = '&Fecha Hasta:'
        FocusControl = txt_FechaHasta
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblPatente: TLabel
        Left = 12
        Top = 19
        Width = 49
        Height = 13
        Caption = '&Patente:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTelevia: TLabel
        Left = 12
        Top = 45
        Width = 43
        Height = 13
        Caption = '&Televia'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblEstado: TLabel
        Left = 451
        Top = 45
        Width = 40
        Height = 13
        Caption = 'Estado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblEntrada: TLabel
        Left = 718
        Top = 19
        Width = 45
        Height = 13
        Caption = 'Entrada'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblConcesionaria: TLabel
        Left = 451
        Top = 19
        Width = 81
        Height = 13
        Caption = 'Concesionaria'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object lblSalida: TLabel
        Left = 718
        Top = 45
        Width = 36
        Height = 13
        Caption = 'Salida'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_FechaDesde: TDateEdit
        Left = 285
        Top = 15
        Width = 99
        Height = 21
        AutoSelect = False
        TabOrder = 2
        Date = -693594.000000000000000000
      end
      object txt_FechaHasta: TDateEdit
        Left = 284
        Top = 40
        Width = 99
        Height = 21
        AutoSelect = False
        TabOrder = 4
        Date = -693594.000000000000000000
      end
      object edPatente: TEdit
        Left = 65
        Top = 15
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 0
      end
      object btn_Filtrar: TButton
        Left = 882
        Top = 9
        Width = 75
        Height = 25
        Hint = 'Filtrar Pases Diarios'
        Anchors = [akTop, akRight]
        Caption = '&Filtrar'
        Default = True
        TabOrder = 10
        OnClick = btn_FiltrarClick
      end
      object btn_Limpiar: TButton
        Left = 882
        Top = 40
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '&Limpiar'
        TabOrder = 11
        OnClick = btn_LimpiarClick
      end
      object txtTelevia: TEdit
        Left = 65
        Top = 43
        Width = 122
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 11
        TabOrder = 1
        OnKeyPress = txtTeleviaKeyPress
      end
      object HoraDesde: TTimeEdit
        Left = 386
        Top = 15
        Width = 63
        Height = 21
        Hint = 'Hora desde'
        AutoSelect = False
        Color = 16444382
        TabOrder = 3
        AllowEmpty = False
        ShowSeconds = False
      end
      object HoraHasta: TTimeEdit
        Left = 386
        Top = 40
        Width = 63
        Height = 21
        Hint = 'Hora hasta'
        AutoSelect = False
        Color = 16444382
        TabOrder = 5
        AllowEmpty = False
        ShowSeconds = False
      end
      object cbEstado: TVariantComboBox
        Left = 534
        Top = 40
        Width = 180
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        Items = <>
      end
      object cbEntrada: TVariantComboBox
        Left = 765
        Top = 15
        Width = 110
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        Items = <>
      end
      object cbConcesionaria: TVariantComboBox
        Left = 534
        Top = 15
        Width = 180
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 6
        Visible = False
        OnChange = cbConcesionariaChange
        Items = <>
      end
      object cbSalida: TVariantComboBox
        Left = 765
        Top = 40
        Width = 110
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        Items = <>
      end
    end
  end
  object pnlGrilla: TPanel
    Left = 0
    Top = 73
    Width = 969
    Height = 375
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pnlGrilla'
    TabOrder = 1
    object dblEstacionamientos: TDBListEx
      Left = 0
      Top = 0
      Width = 969
      Height = 375
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MaxWidth = 200
          Header.Caption = 'Identificador Transaccion'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'NumCorrEstacionamiento'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 70
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Sorting = csAscending
          IsLink = False
          OnHeaderClick = dblEstacionamientosColumns0HeaderClick
          FieldName = 'Patente'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 85
          Header.Caption = 'Telev'#237'a'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dblEstacionamientosColumns0HeaderClick
          FieldName = 'Televia'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          MaxWidth = 200
          Header.Caption = 'Fecha Entrada'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dblEstacionamientosColumns0HeaderClick
          FieldName = 'FechaHoraEntrada'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 60
          MaxWidth = 100
          Header.Caption = 'Entrada'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Entrada'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          MaxWidth = 200
          Header.Caption = 'Fecha Salida'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dblEstacionamientosColumns0HeaderClick
          FieldName = 'FechaHoraSalida'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 60
          MaxWidth = 100
          Header.Caption = 'Salida'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Salida'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 30
          Header.Caption = 'LN'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'BlackList'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 30
          Header.Caption = 'LA'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'YellowList'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 30
          Header.Caption = 'LV'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'GreenList'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 30
          Header.Caption = 'LG'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'GrayList'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 30
          Header.Caption = 'EH'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'ExceptionHandling'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 30
          Header.Caption = 'MMI'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'MMIContactOperator'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 200
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dblEstacionamientosColumns0HeaderClick
          FieldName = 'Estado'
        end>
      DataSource = dsEstacionamientos
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDrawText = dblEstacionamientosDrawText
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 448
    Width = 969
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object pnlBotonera: TPanel
      Left = 0
      Top = -3
      Width = 969
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        969
        41)
      object lblTotales: TLabel
        Left = 15
        Top = 12
        Width = 43
        Height = 13
        Caption = 'Totales'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BtnSalir: TButton
        Left = 866
        Top = 7
        Width = 79
        Height = 26
        Hint = 'Salir del visualizador de Solicitudes'
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Salir'
        TabOrder = 0
        OnClick = BtnSalirClick
      end
    end
  end
  object ObtenerEstacionamientosFiltro: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 500
    ProcedureName = 'ObtenerEstacionamientosFiltro'
    Parameters = <>
    Left = 301
    Top = 275
  end
  object dsEstacionamientos: TDataSource
    DataSet = ObtenerEstacionamientosFiltro
    Left = 300
    Top = 237
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 351
    Top = 243
    Bitmap = {
      494C01010200040028000F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFC000000008003000400000000BFFB7FF400000000BFFB77F400000000
      BFFB63F400000000BFFB41F400000000BFFB48F400000000BFFB5C7400000000
      BFFB7E3400000000BFFB7F1400000000BFFB7F9400000000BFFB7FD400000000
      BFFB7FF4000000008003000400000000FFFFFFFC000000000000000000000000
      0000000000000000000000000000}
  end
end
