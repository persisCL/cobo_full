unit frmImagenesTicketVentaManual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ImgProcs, ConstParametrosGenerales, Util, UtilProc,
  DMConnection, DB, ADODB, Jpeg;

type
  TImagenesTicketVentaManuallForm = class(TForm)
    imgTicketVentaManual: TImage;
    pnlInformacion: TPanel;
    lblFechaTVM: TLabel;
    lblPatente: TLabel;
    lblImporte: TLabel;
    lblCategoria: TLabel;
    lblNumCorrCA: TLabel;
    lblConcesionaria: TLabel;
    btnCerrar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    pnlEdicionPatente: TPanel;
    edtPatente: TEdit;
    lblEdicionPatente: TLabel;
    btnGuardar: TButton;
    spActualizar_Transacciones_TVM: TADOStoredProc;
    procedure btnCerrarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
  private
    FIdTransaccionTVM : Integer;
  public
    function Inicializar(IdTransaccionTVM : Integer; nombreImagen : AnsiString; importe : Integer;
            numCorrCA : Int64; nombreCortoConcesionaria: string; nombreConcesionaria : string;
            categoria : Integer; patente : string; fechaVenta : TDateTime;
            EdicionPatente: Boolean) : Boolean;
    Destructor  Destroy; override;
  end;

var
  ImagenesTicketVentaManuallForm: TImagenesTicketVentaManuallForm;

implementation

{$R *.dfm}

{ TImagenesTicketVentaManuallForm }

procedure TImagenesTicketVentaManuallForm.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

procedure TImagenesTicketVentaManuallForm.btnGuardarClick(Sender: TObject);
resourcestring
    ERROR_TICKET_NO_ENCONTRADO          = 'No existe el ticket de venta manual, por favor actualice el formulario y vuelva a intentar';
    DETALLE_ERROR_TICKET_NO_ENCONTRADO  = 'Detalle del error: ID_Transacciones_TVM %d, Patente: %s';
    ERROR_UPDATE_TICKET                 = 'No se puedo actualizar la patente del ticket de venta manual, por favor contactese con el administrador';
    DETALLE_ERROR_UPDATE_TICKET         = 'Detalle del error: ID_Transacciones_TVM %d, Patente: %s';
    DETALLE_ERROR                       = 'Detalle del error: ID_Transacciones_TVM %d, Patente: %s, Exception: %s';
var
    CodigoRetorno : Integer;
begin
    try
        spActualizar_Transacciones_TVM.Close;
        with spActualizar_Transacciones_TVM do begin
            Parameters.Refresh;
            Parameters.ParamByName('@Patente').Value            := Trim(edtPatente.Text);
            Parameters.ParamByName('@IDRegistro').Value         := FIdTransaccionTVM;
            Parameters.ParamByName('@UsuarioModificador').Value := UsuarioSistema;
            ExecProc;
            CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
        end;
        
        case CodigoRetorno of
            0 :
                begin
                    MsgBox('Se ha actualizado correctamente la Patente', Self.Caption, MB_ICONINFORMATION);
                    Close;
                end;
            -1 :
                begin
                    MsgBoxErr(ERROR_TICKET_NO_ENCONTRADO,
                        Format(DETALLE_ERROR_TICKET_NO_ENCONTRADO, [Trim(edtPatente.Text),FIdTransaccionTVM]),
                        Self.Caption, MB_ICONERROR);
                end;
            -2 :
                begin
                    MsgBoxErr(ERROR_UPDATE_TICKET,
                            Format(DETALLE_ERROR_TICKET_NO_ENCONTRADO, [Trim(edtPatente.Text),FIdTransaccionTVM]),
                            Self.Caption, MB_ICONERROR);
                end;
        end;
    except on e:exception do
        begin
            MsgBoxErr(ERROR_UPDATE_TICKET,
                    Format(DETALLE_ERROR, [Trim(edtPatente.Text),FIdTransaccionTVM, e.Message]),
                    Self.Caption, MB_ICONERROR);
        end;
    end;
end;

destructor TImagenesTicketVentaManuallForm.Destroy;
begin
    inherited;
end;

function TImagenesTicketVentaManuallForm.Inicializar(IdTransaccionTVM : Integer; nombreImagen: AnsiString;
  importe: Integer; numCorrCA: Int64; nombreCortoConcesionaria: string; nombreConcesionaria : string;
  categoria: Integer; patente: string; fechaVenta : TDateTime; EdicionPatente: Boolean): Boolean;
resourcestring
    ERROR_DIR_IMG_TRANSITOS = 'No se encuentra la carpeta de las im�genes de los Transitos, %s';
    ERROR_IMG_NO_ENCONTRADA = 'No se ha encontrado la im�gen del ticket de Venta Manual. Ruta: %s';
var
    RutaImg     : AnsiString;
    CarpetaImg  : AnsiString;
    imgJpg      : TJPegImage;
    Rectangulo   : TRect;

    EscalaX,
    EscalaY,
    Escala       : Single;
begin
    try
        FormStyle   := fsStayOnTop;
        Position    := poScreenCenter;
        BorderStyle := bsDialog;
        BorderIcons := [];

        if not EdicionPatente then begin
            pnlEdicionPatente.Visible := False;
        end
        else begin
            pnlEdicionPatente.Visible := True;
            edtPatente.Text := patente;
        end;
        FIdTransaccionTVM := IdTransaccionTVM;

        // SS_377_PDO_20120605 Inicio Bloque
        {
        ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN, CarpetaImg);
        if not DirectoryExists(GoodDir(CarpetaImg + FormatDateTime('mm', Now))) then begin
            MsgBoxErr('Carpeta Imagenes Transitos no Encontrada', Format(ERROR_DIR_IMG_TRANSITOS, [CarpetaImg]),
                        Self.Caption, MB_ICONERROR);
            Result := false;
            Exit;
        end;
        }
        // SS_377_PDO_20120605 Fin Bloque

        ArmarPathImagenTVM(CarpetaImg, nombreImagen, nombreCortoConcesionaria, RutaImg);

        lblFechaTVM.Caption         := FormatDateTime('dd-mm-yyyy hh:nn:ss', fechaVenta);
        lblPatente.Caption          := patente;
        lblConcesionaria.Caption    := nombreConcesionaria;
        lblImporte.Caption          := IIf(importe >0, IntToStr(importe), ' - ');
        lblCategoria.Caption        := IIf(categoria >0, IntToStr(categoria), ' - ');
        lblNumCorrCA.Caption        := IIf(numCorrCA>0, IntToStr(numCorrCA), ' - ');


        if FileExists(GoodDir(RutaImg) + nombreImagen) then begin
            imgJpg := TJPegImage.Create;
            imgJpg.LoadFromFile(GoodDir(RutaImg) + nombreImagen);
            //Imagen.Assign(imgJpg);
            //Por defecto, escala 1:1
            EscalaX := 1.0;
            EscalaY := 1.0;

            //Hallamos la escala de reducci�n Horizontal
            if imgJpg.Width <> imgTicketVentaManual.Width then
            EscalaX := imgTicketVentaManual.Width / imgJpg.Width;

            //La escala vertical
            if imgJpg.Height <> imgTicketVentaManual.Height then
            EscalaY := imgTicketVentaManual.Height / imgJpg.Height;

            //Escogemos la menor de las 2
            if EscalaY < EscalaX then Escala:=EscalaY else Escala:=EscalaX;


            //Y la usamos para reducir el rectangulo destino
            with Rectangulo do begin
                Right:=Trunc(imgJpg.Width * Escala);
                Bottom:=Trunc(imgJpg.Height * Escala);
                Left:=0;
                Top:=0;
            end;

            //Dibujamos el bitmap con el nuevo tama�o en el TImage destino
            with imgTicketVentaManual.Picture.Bitmap do begin
                Width  := Rectangulo.Right;
                Height := Rectangulo.Bottom;
                Canvas.StretchDraw(Rectangulo, imgJpg);
            end;
            Result := True;
        end
        else begin
            MsgBoxErr('Im�gen Ticket Venta Manual no encontrada', Format(ERROR_IMG_NO_ENCONTRADA, [GoodDir(RutaImg) + nombreImagen]),
                        Self.Caption, MB_ICONINFORMATION);
            Result := false;
            Exit;
        end;
    finally
        FreeAndNil(imgJpg);
    end;
end;

end.
