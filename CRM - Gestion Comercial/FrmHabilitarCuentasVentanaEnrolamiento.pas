{
Firma       : SS-1006-NDR-20120614
Description : Ventana para ADHERIR cuentas en lista de acceso en forma individual
              LA ADHESION es por convenio
}
unit FrmHabilitarCuentasVentanaEnrolamiento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx,UtilProc,DmConnection,Peatypes,
  DB, ADODB, DmiCtrls, Validate, DateEdit,peaprocs,util,UtilDB,RStrings,
  VariantComboBox, frmVisorDocumentos, Convenios, frmImprimirConvenio,
  ConstParametrosGenerales, DBClient, ImgList,Math, Provider, PeaProcsCN, SysUtilsCN;

type
  TFormHabilitarCuentasVentanaEnrolamiento = class(TForm)
    pnlGridsPanel: TPanel;
    dbl_Cuentas: TDBListEx;
    pnlBottomPanel: TPanel;
    BtnSalir: TButton;
    lnCheck: TImageList;
    btnTodos: TButton;
    btnNinguno: TButton;
    dsObtenerCuentas: TDataSource;
    Panel1: TPanel;
    lblCuentas: TLabel;
    procedure BtnSalirClick(Sender: TObject);
    procedure dbl_CuentasDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dbl_CuentasLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnTodosClick(Sender: TObject);
    procedure btnNingunoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(var cdsCuentasPersona:TClientDataSet): Boolean;  overload;
  end;

var
  FormHabilitarCuentasVentanaEnrolamiento: TFormHabilitarCuentasVentanaEnrolamiento;

implementation

{$R *.dfm}

function TFormHabilitarCuentasVentanaEnrolamiento.Inicializar(var cdsCuentasPersona:TClientDataSet): Boolean;
resourcestring
    STR_LISTADO_CONVENIO = 'Habilitar Cuentas en Lista Blanca';
//var S: TSize;       //TASK_013_ECA_20160520
    //CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO : Integer; //TASK_013_ECA_20160520
  //  i:Integer;  //TASK_013_ECA_20160520
begin
    try
        //CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO := QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO()'); //TASK_013_ECA_20160520
        dsObtenerCuentas.DataSet := cdsCuentasPersona;

        Result := True;
    except
        on e: Exception do begin
            MsgBoxErr(Format(MSG_ERROR_INICIALIZACION,[STR_LISTADO_CONVENIO]),e.Message, Caption, MB_ICONSTOP);
    		Result := false;
        end;
    end;
end;

procedure TFormHabilitarCuentasVentanaEnrolamiento.btnNingunoClick(Sender: TObject);
resourcestring
    rsTitulo    ='Suscripción de Cuentas a la Lista Blanca';
    rsMensaje   ='Debe seleccionar al menos una cuenta para Suscribir a la Lista Blanca';
begin
    CambiarEstadoCursor(CURSOR_RELOJ);

    with dsObtenerCuentas.DataSet do
        try
            First;
            while not Eof do begin
                if FieldByName('Chequeado').AsBoolean then begin
                    Edit;
                    FieldByName('Chequeado').AsBoolean := False;
                    Post;
                end;

                Next;
            end;
            First;
        finally
		   	CambiarEstadoCursor(CURSOR_DEFECTO);

            if dsObtenerCuentas.DataSet.Tag = 0 then begin
                ShowMsgBoxCN(rsTitulo, rsMensaje, MB_ICONWARNING, Self)
            end;
        end;

end;

procedure TFormHabilitarCuentasVentanaEnrolamiento.BtnSalirClick(Sender: TObject);
begin
    close;
end;

procedure TFormHabilitarCuentasVentanaEnrolamiento.btnTodosClick(Sender: TObject);
begin
    CambiarEstadoCursor(CURSOR_RELOJ);
    with dsObtenerCuentas.DataSet do
        try
            DisableControls;
            First;
            while not Eof do begin
                if not FieldByName('Chequeado').AsBoolean then begin
                    Edit;
                    FieldByName('Chequeado').AsBoolean := True;
                    Post;
                end;

                Next;
            end;

            First;
        finally
            EnableControls;
            CambiarEstadoCursor(CURSOR_DEFECTO);
        end;
end;


procedure TFormHabilitarCuentasVentanaEnrolamiento.dbl_CuentasDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
	if Column = dbl_Cuentas.Columns[2] then begin
		lnCheck.Draw(dbl_Cuentas.Canvas, Rect.Left + 3, Rect.Top + 1, Iif(dsObtenerCuentas.DataSet.FieldByName('Chequeado').AsBoolean,1,0));
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;
end;

procedure TFormHabilitarCuentasVentanaEnrolamiento.dbl_CuentasLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
resourcestring
    rsTitulo    ='Suscripción de Cuentas en Lista Blanca';
    rsMensaje   ='Debe seleccionar al menos una cuenta para Suscribir a la Lista Blanca';
Var
	i: Integer;
begin
    dsObtenerCuentas.DataSet.Edit;
    dsObtenerCuentas.DataSet.FieldByName('Chequeado').AsBoolean:= not dsObtenerCuentas.DataSet.FieldByName('Chequeado').AsBoolean;
    dsObtenerCuentas.DataSet.Post;

    if dsObtenerCuentas.DataSet.Tag = 0 then begin
        ShowMsgBoxCN(rsTitulo, rsMensaje, MB_ICONWARNING, Self)
    end;
end;
procedure TFormHabilitarCuentasVentanaEnrolamiento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
