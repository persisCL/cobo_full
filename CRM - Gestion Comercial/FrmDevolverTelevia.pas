{-------------------------------------------------------------------------------
 File Name: FrmDevolverTelevia.pas
 Author:    lgisuk
 Date Created: 01/03/2005
 Language: ES-AR
 Description:  Devolucion de Televia

 Revision : 1
 Author	 : nefernandez
 Date     : 25/07/2007
 Description : Cambi� la conversi�n del par�metro ContractSerialNumber
 (de asInteger a AsFloat/AsVariant)

Revision 2:
    Author: dAllegretti
    Date: 22/05/2008
    Description:    Se le agreg� el parametro @UsuarioCreacion del Stored spAgregarMovimientoCuenta
                    y la correspondiente asignaci�n en el llamado al stored.

Firma       : SS-1006-NDR-20120715
Description : Enviar el parametro @Usuario al SP DevolverTAGExtraviado
-------------------------------------------------------------------------------}
unit FrmDevolverTelevia;

interface

uses
  //Devolucion de Televia
  DMConnection,              //coneccion a base de datos OP_CAC
  UtilProc,                  //getformclient, mensajes
  PeaProcs,                  //sin especificar
  Util,                      //padl
  Utildb,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DmiCtrls, VariantComboBox, DB, ADODB, PeaTypes;

type
  TFDevolverTelevia = class(TForm)
    gb_DevolverTelevia: TGroupBox;
    Label3: TLabel;
    cb_EstadoTagDevuelto: TVariantComboBox;
    gb_BuscarTelevia: TGroupBox;
    Label4: TLabel;
    LVerde: TLabel;
    ledVerde: TLed;
    ledRojo: TLed;          //TASK_050_GLE_20170208
    lblLroja: TLabel;       //TASK_050_GLE_20170208
    ledAmarilla: TLed;
    Lamarilla: TLabel;
    Lnegra: TLabel;
    ledNegra: TLed;
    EEtiqueta: TEdit;
    Panel1: TPanel;
    btn_Devolver: TButton;
    btn_Salir: TButton;
    Lestado: TLabel;
    ObtenerEstadosConservacionTAGs: TADOStoredProc;
    spLeerDatosTAG: TADOStoredProc;
    Label2: TLabel;
    //lVendido: TLabel;     //TASK_050_GLE_20170208
    Label5: TLabel;
    Label7: TLabel;
    lDuenio: TLabel;
    lRUTDuenio: TLabel;
    ck_DevueltoPorDuenio: TCheckBox;
    spObtenerUltimoDuenioTelevia: TADOStoredProc;
    spAgregarMovimientoCuenta: TADOStoredProc;
    spDevolverTAGExtraviado: TADOStoredProc;
    //lEstaPago: TLabel;   //TASK_050_GLE_20170208
    procedure btn_SalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EEtiquetaChange(Sender: TObject);
    procedure btn_DevolverClick(Sender: TObject);
    procedure cb_EstadoTagDevueltoChange(Sender: TObject);
    procedure EEtiquetaKeyPress(Sender: TObject; var Key: Char);
  private
   	nPuntoEntrega : integer;
    bTeleviaEstaPago : boolean;
	Function  Limpiar:boolean;
  	Function  HabilitarDevolucion(Habilita:boolean):boolean;
    function  AcreditarTelevia: boolean;
    function  CambiarAlmacenYEstadoTelevia(nCodigoAlmacen: integer):boolean;
    { Private declarations }
  public
    function Inicializar(PuntoEntrega : integer):boolean;
    { Public declarations }
  end;

var
  FDevolverTelevia: TFDevolverTelevia;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 01/03/2005
  Description: Inicializacion de Este Formulario
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFDevolverTelevia.Inicializar(PuntoEntrega : integer):boolean;

    Function CargarCombo:boolean;
    begin
        result:=false;
        try
        	nPuntoEntrega := PuntoEntrega; 
            cb_EstadoTagDevuelto.Items.Add(SIN_ESPECIFICAR, 0);
            ObtenerEstadosConservacionTAGs.Open;
            while not ObtenerEstadosConservacionTAGs.Eof do begin
            if ObtenerEstadosConservacionTAGs.fieldbyname('CodigoEstadoConservacionTAG').AsInteger <> 4 then // 4 -> No devuelto
                cb_EstadoTagDevuelto.Items.Add(Trim(ObtenerEstadosConservacionTAGs.fieldbyname('Descripcion').AsString), ObtenerEstadosConservacionTAGs.fieldbyname('CodigoEstadoConservacionTAG').AsInteger );
                ObtenerEstadosConservacionTAGs.Next;
            end;
            cb_EstadoTagDevuelto.ItemIndex := 0;
            Result:=true;
        except
        end;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
var
	S: TSize;
begin
	Result := False;
	try

        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and
                        CargarCombo and
                            HabilitarDevolucion(False);
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;

    S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	CenterForm(Self);

	Limpiar;
end;

{-----------------------------------------------------------------------------
  Function Name: HabilitarDevolucion
  Author:    lgisuk
  Date Created: 01/03/2005
  Description: Habilita o Deshabitia la devolucion
  Parameters: Habilita:boolean
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFDevolverTelevia.HabilitarDevolucion(Habilita:boolean):boolean;
begin
    result:=false;
    try
        cb_EstadoTagDevuelto.Enabled:=Habilita;
        btn_Devolver.Enabled:=Habilita; //and (cb_EstadoTagDevuelto.Value <> 0); //TASK_050_GLE_20170208
        ck_DevueltoPorDuenio.Enabled := Habilita;
        if not Habilita then ck_DevueltoPorDuenio.Checked := False;
        
        Result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EEtiquetaChange
  Author:    lgisuk
  Date Created: 01/03/2005
  Description: Limpia los Campos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
function TFDevolverTelevia.Limpiar:boolean;
begin
	lDuenio.Caption			:= '';
    lRUTDuenio.Caption		:= '';
    lestado.Caption			:= '';
    //lconcesionaria.Caption	:= '';  //TASK_050_GLE_20170208
    //lVendido.Caption		:= '';      //TASK_050_GLE_20170208
    //lEstaPago.Caption		:= '';      //TASK_050_GLE_20170208
    ledVerde.Enabled 		:= False;
    ledAmarilla.Enabled 	:= False;
    //ledGris.Enabled 		:= False;   //TASK_050_GLE_20170208
    ledRojo.Enabled         := False;   //TASK_050_GLE_20170208
    ledNegra.Enabled 		:= False;
    cb_EstadoTagDevuelto.Value := 0;
    result:=true;
end;


{-----------------------------------------------------------------------------
  Function Name: EEtiquetaChange
  Author:    lgisuk
  Date Created: 01/03/2005
  Description: Busco al Televia por su Etiqueta
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFDevolverTelevia.EEtiquetaChange(Sender: TObject);

    //Busco el televia por su etiqueta
    function Buscar(Etiqueta:String):boolean;
    resourcestring
        MSG_ERROR_READING_TAG_DATA 	= 'Error leyendo los datos del telev�a';
		MSG_ERROR 					= 'Error';
    	MSG_YES						= 'Si';
        MSG_NO						= 'No';
        MSG_EXTRAVIADO				= 'Extraviado';
        MSG_NO_EXTRAVIADO			= 'No Extraviado';
    var
        OtraConcesionaria:integer;
        CodigoUbicacionTag:integer;
        CodigoPersona:integer;
        ContextMark:integer;
        ContractSerialNumber:int64;
        CODIGO_ALMACEN_EXTRAVIO : integer;
        TAGesNativo : integer;  //TASK_050_GLE_20170208
    begin
        TAGesNativo := 0;       //TASK_050_GLE_20170208
        HabilitarDevolucion(False);

        ledVerde.Enabled 			:= False;
        ledAmarilla.Enabled 		:= False;
        //ledGris.Enabled 			:= False;   //TASK_050_GLE_20170208
        ledRojo.Enabled             := False;   //TASK_050_GLE_20170208
        ledNegra.Enabled 			:= False;

        if (Trim(Eetiqueta.Text) <> '') and (length(Trim(Eetiqueta.Text)) > 7) and (EtiquetaToSerialNumber(PadL(Trim(Eetiqueta.Text),11,'0')) <> 0) then begin
            try
                spLeerDatosTAG.Close;
                spLeerDatosTAG.Parameters.ParamByName('@ContractSerialNumber').Value := EtiquetaToSerialNumber(PadL(Trim(Eetiqueta.Text),11,'0'));
                spLeerDatosTAG.Open;

                if spLeerDatosTAG.RecordCount = 1 then begin

                    //Obtengo el Context Mark y COntract Serial Number
                    ContextMark 				:= spLeerDatosTAG.FieldByName ('ContextMark').AsInteger;
                    ContractSerialNumber		:= spLeerDatosTAG.FieldByName ('ContractSerialNumber').AsVariant; //Revision 1

                    //Obtengo los indicadores
                    ledVerde.Enabled 			:= spLeerDatosTAG.FieldByName ('IndicadorListaVerde').AsBoolean;
                    ledAmarilla.Enabled 		:= spLeerDatosTAG.FieldByName ('IndicadorListaAmarilla').AsBoolean;
                    //ledGris.Enabled 			:= spLeerDatosTAG.FieldByName ('IndicadorListaGris').AsBoolean;    //TASK_050_GLE_20170208
                    ledRojo.Enabled             := False;                                                          //TASK_050_GLE_20170208 A�n no se define de d�nde se obtendr� este valor
                    ledNegra.Enabled 			:= spLeerDatosTAG.FieldByName ('IndicadorListaNegra').AsBoolean;

                    //Aca Averiguo si es de otra concesionaria
                    //OtraConcesionaria := spLeerDatosTAG.FieldByName ('OtraConcesionaria').AsInteger;             //TASK_050_GLE_20170208

                    //Aca  averiguo quien es la concesionaria
                    //Lconcesionaria.caption:= Trim(QueryGetValue(DMConnections.BaseCAC,                                                        //TASK_050_GLE_20170208
                    //                            Format('Select dbo.ObtenerConcesionariaTAG(%d,%d)', [ContextMark, ContractSerialNumber])));   //TASK_050_GLE_20170208

                    //Ac� obtengo el c�digo de Almac�n de los Extrav�os
                    //CODIGO_ALMACEN_EXTRAVIO := QueryGetValueInt(DMConnections.BaseCAC, 'Select dbo.CONST_ALMACEN_EXTRAVIADO()');              //TASK_050_GLE_20170208

                    //Aca obtengo el estado
                    //CodigoUbicacionTag:= spLeerDatosTAG.FieldByName ('CodigoUbicacionTAG').AsInteger;                                         //TASK_050_GLE_20170208
                    //Lestado.caption := iif(CodigoUbicacionTag = CODIGO_ALMACEN_EXTRAVIO, MSG_EXTRAVIADO, MSG_NO_EXTRAVIADO);                  //TASK_050_GLE_20170208
                    Lestado.Caption :=  spLeerDatosTAG.FieldByName ('EstadoSituacion').AsString;                                                //TASK_050_GLE_20170208

                    //Averiguo si fue vendido
                    //CodigoPersona := QueryGetValueInt(DMConnections.BaseCAC,                                                                  //TASK_050_GLE_20170208
                    //                            Format('Select dbo.ObtenerDuenioTagVendido(%d,%d)', [ContextMark, EtiquetaToSerialNumber(PadL(Trim(Eetiqueta.Text),11,'0'))]));     //TASK_050_GLE_20170208
                    //lVendido.Caption := iif(CodigoPersona = -1, MSG_NO, MSG_YES);                                                             //TASK_050_GLE_20170208

                    // Obtengo los Dtos del Ultimo Due�o
                    spObtenerUltimoDuenioTelevia.Close;
                    spObtenerUltimoDuenioTelevia.Parameters.ParamByName('@ContextMark').Value := ContextMark;
                    spObtenerUltimoDuenioTelevia.Parameters.ParamByName('@ContractSerialNumber').Value := ContractSerialNumber;
                    spObtenerUltimoDuenioTelevia.Open;

                    lDuenio.Caption 	:= spObtenerUltimoDuenioTelevia.FieldByName( 'Cliente' ).AsString;
                    lRUTDuenio.Caption 	:= spObtenerUltimoDuenioTelevia.FieldByName( 'RUT' ).AsString;

                    bTeleviaEstaPago := (QueryGetValueInt(DMConnections.BaseCAC,
    										Format('Select dbo.EstaPagoTelevia(%d, %d)', [
                                            spObtenerUltimoDuenioTelevia.FieldByName( 'CodigoConvenio' ).AsInteger,
        									spObtenerUltimoDuenioTelevia.FieldByName( 'IndiceVehiculo' ).AsInteger])) = 1);
                    //lEstaPago.Caption := iif( bTeleviaEstaPago, MSG_YES, MSG_NO);                                                 //TASK_050_GLE_20170208


                    //INICIO: TASK_050_GLE_20170208
                    //Verifico si se cumplen las tres condiciones
                    //if (Otraconcesionaria = 0) and
                    //   (CodigoUbicacionTag = CODIGO_ALMACEN_EXTRAVIO) then begin

                        //Habilito la Devolucion
                    //    HabilitarDevolucion(True);
                    //end;

                    //Verifica si patente est� en maestro tag; si es as�, es TAG nativo y se habilita combo
                    TAGesNativo := QueryGetValueInt(DMConnections.BaseCAC,
    										Format('SELECT COUNT(*) FROM MaestroTAGS (NOLOCK) WHERE Etiqueta = ''%s''',
                                                    [Trim(EEtiqueta.Text)]),0);

                    if TAGesNativo > 0 then   HabilitarDevolucion(True);

                    //TERMINO: TASK_050_GLE_20170208
                end;
            except
                on e: exception do begin
                    MsgBoxErr(MSG_ERROR_READING_TAG_DATA, e.Message, MSG_ERROR, MB_ICONERROR);
                    Limpiar;
                end;
            end;
         end;
        result:=true;
    end;

begin
    Limpiar;
    Buscar(EEtiqueta.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: btn_SalirClick
  Author:    lgisuk
  Date Created: 01/03/2005
  Description: Salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFDevolverTelevia.btn_SalirClick(Sender: TObject);
begin
    close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 01/03/2005
  Description: Lo elimino de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFDevolverTelevia.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;


{-----------------------------------------------------------------------------
  Function Name: btn_DevolverClick
  Author:    flamas
  Date Created: 03/03/2005
  Description: Devuelve el Telev�a, actializa los Flags y devuelve
  				la plata si corresponde
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFDevolverTelevia.btn_DevolverClick(Sender: TObject);
resourcestring
	MSG_REACTIVAR_CUENTA_MODIF_CONVENIO 	= 'Debe reactivar la cuenta en modificaci�n de convenio';
	MSG_INFORMAR_CLIENTE_TELEVIA_DEVUELTO 	= 'Informar al Cliente: %s'+crlf+'que su telev�a ha sido devuelto.';
	MSG_TELEVIA_DEVUELTO_CON_EXITO 			= 'El telev�a ha sido devuelto con �xito.';
var
	bError : boolean;
begin
    // Determino si el telev�a est� Pago
	if bTeleviaEstaPago then begin

    	bError := False;
        // Si el telev�a est� le acredita el monto del telev�a (si est� en buen estado)
        if (cb_EstadoTagDevuelto.Value = ESTADO_CONSERVACION_BUENO) then bError := not AcreditarTelevia;

        // Si el telev�a est� Pago Vuelve al Almacen de Devoluci�n y cambia el estado
        if (not bError) and
        	CambiarAlmacenYEstadoTelevia(QueryGetValueInt(DMConnections.BaseCAC,
            	Format('Select dbo.ObtenerCodigoAlmacenDevolucion(%d)', [nPuntoEntrega]))) then
        		MsgBox(MSG_TELEVIA_DEVUELTO_CON_EXITO, Caption, MB_ICONINFORMATION);

    end else begin
    	if ck_DevueltoPorDuenio.Checked then
        	// Si est� devuelto por el Due�o, debe reactivar la cuenta en modificaci�n de Convenio
        	MsgBox(MSG_REACTIVAR_CUENTA_MODIF_CONVENIO, Caption, MB_ICONINFORMATION)
        else begin
        	// Si no pasa a almacen de Devueltos
        	if CambiarAlmacenYEstadoTelevia(QueryGetValueInt(DMConnections.BaseCAC, 'Select dbo.CONST_ALMACEN_DEVUELTO()')) then
            	MsgBox(Format(MSG_TELEVIA_DEVUELTO_CON_EXITO + crlf + crlf + MSG_INFORMAR_CLIENTE_TELEVIA_DEVUELTO,
                		[spObtenerUltimoDuenioTelevia.FieldByName( 'Cliente' ).AsString]), Caption, MB_ICONINFORMATION);
        end;
    end;

	Limpiar;
    EEtiqueta.Text := '';
	EEtiqueta.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: AcreditarTelevia
  Author:    flamas
  Date Created: 03/03/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TFDevolverTelevia.AcreditarTelevia : boolean;
resourcestring
	MSG_ERROR							= 'Error';
	MSG_DEVOLUCION_PAGO_TELEVIA 		= 'Devoluci�n Pago de Telev�a n�mero : %s';
    MSG_NO_SE_PUDO_ACREDITAR_TELEVIA 	= 'No se pudo acreditar el telev�a';
begin
	try
		result := True;
        with spAgregarMovimientoCuenta.Parameters  do begin
            ParamByName('@CodigoConvenio').Value := spObtenerUltimoDuenioTelevia.FieldByName( 'CodigoConvenio' ).AsInteger;
            ParamByName('@IndiceVehiculo').Value := spObtenerUltimoDuenioTelevia.FieldByName( 'IndiceVehiculo' ).AsInteger;
            ParamByName('@FechaHora').Value := NowBase(DMConnections.BaseCAC);
{INICIO: TASK_005_JMA_20160418
            ParamByName('@CodigoConcepto').Value := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_CONCEPTO_DEVOLUCION_PAGO_TELEVIA()');
}
			ParamByName('@CodigoConcepto').Value := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_CONCEPTO_DEVOLUCION_PAGO_TELEVIA(dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}			
			// Devuelve el importe de un telev�a en buen estado
            ParamByName('@Importe').Value := -RVal(QueryGetValue(DMConnections.BaseCAC, 'SELECT DBO.ValorDanio(DBO.CONST_TAG_ESTADO_CONSERVACION_NO_DEVUELTO())'));
            ParamByName('@LoteFacturacion').Value := Null;
            ParamByName('@EsPago').Value := False;
            ParamByName('@Observaciones').Value := Format(MSG_DEVOLUCION_PAGO_TELEVIA, [EEtiqueta.Text]);
            ParamByName('@NumeroPromocion').Value := Null;
            ParamByName('@NumeroFinanciamiento').Value := Null;
            ParamByName('@NumeroMovimiento').Value := Null;
            ParamByName('@UsuarioCreacion').Value := UsuarioSistema;
        end;
        spAgregarMovimientoCuenta.ExecProc;
    except
    	on e: exception do begin
        	MsgBoxErr(MSG_NO_SE_PUDO_ACREDITAR_TELEVIA, e.Message, MSG_ERROR, MB_ICONERROR);
			result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CambiarAlmacenYEstadoTelevia
  Author:    flamas
  Date Created: 03/03/2005
  Description:
  Parameters: nCodigoAlmacen: integer
  Return Value: boolean
-----------------------------------------------------------------------------}
function  TFDevolverTelevia.CambiarAlmacenYEstadoTelevia(nCodigoAlmacen: integer):boolean;
resourcestring
	MSG_ERROR	 			= 'Error';
    MSG_DEVOLVER_TELEVIA 	= 'No se pudo devolver el telev�a';
begin
	try
		result := True;
        with spDevolverTAGExtraviado.Parameters  do begin
            Refresh;                                                            //SS-1006-NDR-20120715
            ParamByName('@ContextMark').Value 				:= spLeerDatosTAG.FieldByName ('ContextMark').AsInteger;
            ParamByName('@ContractSerialNumber').Value 		:= spLeerDatosTAG.FieldByName ('ContractSerialNumber').AsFloat; //Revision 1
            ParamByName('@CodigoEstadoConservacion').Value 	:= cb_EstadoTagDevuelto.Value;
            ParamByName('@CodigoAlmacen').Value 			:= nCodigoAlmacen;
            ParamByName('@Usuario').Value 			:= UsuarioSistema;              //SS-1006-NDR-20120715
        end;
        spDevolverTAGExtraviado.ExecProc;
    except
    	on e: exception do begin
        	MsgBoxErr(MSG_DEVOLVER_TELEVIA, e.Message, MSG_ERROR, MB_ICONERROR);
			result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: cb_EstadoTagDevueltoChange
  Author:    flamas
  Date Created: 03/03/2005
  Description:	Habilita el bot�n de devolver
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFDevolverTelevia.cb_EstadoTagDevueltoChange(Sender: TObject);
begin
	btn_Devolver.Enabled := (cb_EstadoTagDevuelto.Value <> 0);
end;

{-----------------------------------------------------------------------------
  Function Name: EEtiquetaKeyPress
  Author:    flamas
  Date Created: 07/03/2005
  Description: Permite ingrasar s�lo n�meros
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFDevolverTelevia.EEtiquetaKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9', #8]) then Key := #0;
end;

end.
