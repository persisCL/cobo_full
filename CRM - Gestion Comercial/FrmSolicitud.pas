unit FrmSolicitud;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, PeaTypes,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx,UtilProc,DmConnection,
  DB, ADODB, DmiCtrls, Validate, DateEdit,peaprocs,util,UtilDB,RStrings;

type
  TFormSolicitud = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    dbl_Solicitud: TDBListEx;
    ObtenerSolicitudContacto: TADOStoredProc;
    DS_SolicitudContacto: TDataSource;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    txt_FechaDesde: TDateEdit;
    txt_FechaHasta: TDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    cb_TipoSolicitud: TComboBox;
    Label4: TLabel;
    txt_CantVehiculos: TNumericEdit;
    Label5: TLabel;
    cb_Personeria: TComboBox;
    Label6: TLabel;
    Label7: TLabel;
    txt_Apellido: TEdit;
    txt_rut: TEdit;
    EliminarSolicitudContacto: TADOStoredProc;
    Label8: TLabel;
    txt_Patente: TEdit;
    lbl_Mostar: TLabel;
    Label9: TLabel;
    cb_EstadoSolicitud: TComboBox;
    Label11: TLabel;
    txt_Top: TNumericEdit;
    btn_Filtrar: TButton;
    btn_Limpiar: TButton;
    BtnSalir: TButton;
    btn_Ver: TButton;
    btn_Editar: TButton;
    btn_Eliminar: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure ObtenerSolicitudContactoAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure dbl_SolicitudColumns0HeaderClick(Sender: TObject);
    procedure btn_VerClick(Sender: TObject);
    procedure dbl_SolicitudDblClick(Sender: TObject);
    procedure btn_EditarClick(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure txt_rutKeyPress(Sender: TObject; var Key: Char);
    procedure btn_EliminarClick(Sender: TObject);
    procedure txt_ApellidoKeyPress(Sender: TObject; var Key: Char);
    procedure ObtenerSolicitudContactoAfterScroll(DataSet: TDataSet);
    procedure txt_TopExit(Sender: TObject);
  private
    { Private declarations }
    SoloConsulta: boolean;
    FPuntoVenta: Integer;
    FPuntoEntrega: Integer;
    function BuscarColumna(Nombre:String;IniciarDesde:integer):Integer;
    function DarOrderBy:String;
    procedure EliminarSolicitud;
    function PuedeEditarSolicitud:Boolean;
  public
    { Public declarations }
    function Inicializar(MDIChild: Boolean; EsSoloConsulta: boolean; Caption: String; PuntoVenta, PuntoEntrega: Integer): Boolean;
    property PuntoEntrega: Integer read FPuntoEntrega;
    property PuntoVenta: Integer read FPuntoVenta;

  end;

  TFormSolicitudConsulta = class(TFormSolicitud)
  public
    function Inicializar(MDIChild: Boolean; Caption: String; PuntoVenta, PuntoEntrega: Integer): Boolean;
  end;

  TFormSolicitudModifcacion = class(TFormSolicitud)
  public
    function Inicializar(MDIChild: Boolean; Caption: String; PuntoVenta, PuntoEntrega: Integer): Boolean;
  end;

var
  FormSolicitud: TFormSolicitud;

implementation

resourceString
    CAPTION_EDITAR_SOLICITUD = 'Modificar Solicitud';
    CAPTION_VER_SOLICITUD = 'Ver Solicitud';


{$R *.dfm}

procedure TFormSolicitud.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     action := caFree;
end;

function TFormSolicitud.Inicializar(MDIChild: Boolean; EsSoloConsulta: boolean; Caption: String; PuntoVenta, PuntoEntrega: Integer): Boolean;
resourcestring
	MSG_DOBLE_CLICK_SOLICITUD_CONSULTA = '<Doble click para ver el detalle de la solicitud>';
	MSG_DOBLE_CLICK_SOLICITUD_MODIFICAR = '<Doble click para modificar la solicitud>';
	MSG_LISTADO_SOLICITUD = 'Listado de Solicitud';
Var S: TSize;
begin
    try
        if MDIChild then begin
            S := GetFormClientSize(Application.MainForm);
            SetBounds(0, 0, S.cx, S.cy);
        end else begin
            FormStyle := fsNormal;
            Visible := False;
        end;
        SoloConsulta := EsSoloConsulta;
        if SoloConsulta then
            dbl_Solicitud.Hint := MSG_DOBLE_CLICK_SOLICITUD_CONSULTA
        else
            dbl_Solicitud.Hint := MSG_DOBLE_CLICK_SOLICITUD_MODIFICAR;

		FPuntoVenta := PuntoVenta;
        FPuntoEntrega := PuntoEntrega;
        btn_Limpiar.Click;
        Self.Caption := LimpiarCaracteresLabel(Caption);
        //*** 27-04-2004: Guillermo. No mostrar solicitudes al inicio y tampoco la fecha de hoy en el filtro desde
        //txt_FechaDesde.Date:=Now;
        txt_FechaHasta.Date:=Now;
        {with ObtenerSolicitudContacto do begin
            close;
            Parameters.ParamByName('@FechaHoraCreacion').Value:=now;
            Parameters.ParamByName('@FechaHoraFin').Value:=now;
            Parameters.ParamByName('@OrderBy').Value:=DarOrderBy;
            open;
        end;
        }

        Result := True;
    except
        on e: Exception do begin
			MsgBoxErr(Format(MSG_ERROR_INICIALIZACION,[MSG_LISTADO_SOLICITUD]),e.Message, Caption, MB_ICONSTOP);
    		Result := false;
        end;

    end;
end;

procedure TFormSolicitud.BtnSalirClick(Sender: TObject);
begin
    close;
end;

procedure TFormSolicitud.btn_FiltrarClick(Sender: TObject);
resourcestring
	MSG_ERROR_NO_PUDO_HACERSE_CONSULTA = 'No se pudo realizar la consulta.';

    function DarTipoSolicitud:Integer;
    begin
        result:=strtoint(iif(trim(cb_TipoSolicitud.Text)='','0',StrRight(cb_TipoSolicitud.Text,10)));
    end;

    function DarPersoneria:String;
    begin
        result:=StrRight(iif(trim(cb_Personeria.Text) = '','0',trim(cb_Personeria.Text)),1);
    end;

    function DarEstadoSolicitud:Integer;
    begin
        result:=strtoint(iif(trim(cb_EstadoSolicitud.Text)='','0',StrRight(cb_EstadoSolicitud.Text,10)));
    end;
begin
    if (txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate) and (txt_FechaDesde.Date>txt_FechaHasta.Date) then begin
        MsgBoxBalloon(MSG_VALIDAR_ORDEN_FECHA,MSG_CAPTION_LISTSOLICITUD,MB_ICONSTOP,txt_FechaDesde);
        exit;
    end;

    ObtenerSolicitudContacto.DisableControls;

    with ObtenerSolicitudContacto do begin
        close;
        Parameters.Refresh;
        Parameters.ParamByName('@FechaHoraCreacion').Value:=iif(txt_FechaDesde.Date=nulldate,null,txt_FechaDesde.Date);
        Parameters.ParamByName('@FechaHoraFin').Value:=iif(txt_FechaHasta.Date=nulldate,null,txt_FechaHasta.Date);
        Parameters.ParamByName('@CodigoFuenteSolicitud').Value:=iif(DarTipoSolicitud=0,NULL,DarTipoSolicitud);
        Parameters.ParamByName('@CodigoEstadoSolicitud').Value:=iif(DarEstadoSolicitud=0,NULL,DarEstadoSolicitud);
        Parameters.ParamByName('@CantidadVehiculos').Value:=iif(trim(txt_CantVehiculos.Text)='',0,txt_CantVehiculos.ValueInt);
        Parameters.ParamByName('@Personeria').Value:=iif(DarPersoneria='0',null,DarPersoneria);
        Parameters.ParamByName('@RUT').Value:=iif(trim(txt_rut.Text)='',NULL,trim(txt_rut.Text));
        Parameters.ParamByName('@Apellido').Value:=iif(Trim(txt_Apellido.Text)='',NULL,trim(txt_Apellido.Text)+'%');
        Parameters.ParamByName('@Patente').Value:=iif(Trim(txt_Patente.Text)='',NULL,trim(txt_Patente.Text)+'%');
        Parameters.ParamByName('@OrderBy').Value:=DarOrderBy;
        Parameters.ParamByName('@Top').Value := txt_Top.Value;
    end;

//    if not(OpenTables([ObtenerSolicitudContacto])) then MsgBox(FORMAT(MSG_ERROR_INICIALIZACION,[STR_SOLICITUD]),MSG_CAPTION_LISTSOLICITUD,MB_ICONSTOP);
    try
        ObtenerSolicitudContacto.Open;
    except
		on E: exception do begin
            ObtenerSolicitudContacto.Close;
			MsgBoxErr(MSG_ERROR_NO_PUDO_HACERSE_CONSULTA, e.message, MSG_CAPTION_LISTSOLICITUD, MB_ICONSTOP);
		end;
    end;

    ObtenerSolicitudContacto.EnableControls;

    dbl_Solicitud.SetFocus;
end;

procedure TFormSolicitud.ObtenerSolicitudContactoAfterOpen(
  DataSet: TDataSet);
resourcestring
	STR_CANTIDAD_SOLICITUDES = 'Cantidad de Solicitudes: %d.';
var
	CantRegistros:Integer;
begin

    CantRegistros:=0;
    if ObtenerSolicitudContacto.Active then begin
        CantRegistros := ObtenerSolicitudContacto.RecordCount;
        ObtenerSolicitudContacto.First;
    end;
	lbl_Mostar.Caption      := format(STR_CANTIDAD_SOLICITUDES,[CantRegistros]);
end;

procedure TFormSolicitud.FormShow(Sender: TObject);
begin
    txt_FechaDesde.SetFocus;
    btn_Editar.Enabled:=False;
    btn_Ver.Enabled:=False;
    btn_Eliminar.Enabled:=False;
end;

procedure TFormSolicitud.dbl_SolicitudColumns0HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting=csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;
    if ObtenerSolicitudContacto.Active then ObtenerSolicitudContacto.Sort := TDBListExColumn(sender).FieldName  + ' ' + iif(TDBListExColumn(sender).Sorting=csAscending ,'ASC','DESC');
//    if ObtenerSolicitudContacto.Active then btn_Filtrar.Click;
end;


function TFormSolicitud.BuscarColumna(Nombre:String;IniciarDesde:integer):Integer;
begin
    if IniciarDesde>dbl_Solicitud.Columns.Count-1 then begin result:=-1; exit; end;
    if dbl_Solicitud.Columns[IniciarDesde].FieldName=Nombre then result:=IniciarDesde
    else Result:=BuscarColumna(Nombre,IniciarDesde+1);
end;


function TFormSolicitud.DarOrderBy:String;
var
    i:Integer;
begin
    result:='';
    for i:=0 to  dbl_Solicitud.Columns.Count-1 do
        if dbl_Solicitud.Columns[i].Sorting <> csNone then result:=dbl_Solicitud.Columns[i].FieldName+iif(dbl_Solicitud.Columns[i].Sorting=csDescending,' DESC','');

    if result='' then begin
        result:='Nombre';
        dbl_Solicitud.Columns[BuscarColumna('Nombre',0)].Sorting:=csAscending;
    end;
end;


procedure TFormSolicitud.btn_VerClick(Sender: TObject);
{var
    f:TFormSolicitudContacto;}
begin
{    Application.CreateForm(TFormSolicitudContacto,f);
    if not (f.Inicializa(false, CAPTION_VER_SOLICITUD, FPuntoEntrega, FPuntoVenta, ObtenerSolicitudContacto.FieldByName('CodigoSolicitudContacto').AsInteger)) then MsgBox(format(MSG_ERROR_OPENTABLE,[STR_SOLICITUD]), MSG_CAPTION_LISTSOLICITUD, MB_ICONSTOP)
    else f.ShowModal;
    f.Release;}
end;

procedure TFormSolicitud.dbl_SolicitudDblClick(Sender: TObject);
begin
    //si entro en modo consulta
    if btn_Editar.Enabled then
        btn_Editar.Click
    else
        btn_Ver.Click;
end;

procedure TFormSolicitud.btn_EditarClick(Sender: TObject);
{var
    f:TFormSolicitudContacto;}
begin
{    Application.CreateForm(TFormSolicitudContacto,f);
    if not (f.Inicializa(false, CAPTION_EDITAR_SOLICITUD, FPuntoEntrega, FPuntoVenta, ObtenerSolicitudContacto.FieldByName('CodigoSolicitudContacto').AsInteger,scModi,ObtenerSolicitudContacto.FieldByName('IndicePersona').AsInteger)) then MsgBox(format(MSG_ERROR_OPENTABLE,[STR_SOLICITUD]),MSG_CAPTION_LISTSOLICITUD,MB_ICONSTOP)
    else f.ShowModal;
    f.Release;
    btn_Filtrar.Click;}
end;

procedure TFormSolicitud.btn_LimpiarClick(Sender: TObject);
begin
    CargarFuentesSolicitud(DMConnections.BaseCAC,cb_TipoSolicitud,0,true);
    CargarPersoneria(cb_Personeria,SIN_ESPECIFICAR,false,true);
    CargarEstadosSolicitud(DMConnections.BaseCAC,cb_EstadoSolicitud,0, true);
    txt_FechaDesde.Clear;
    txt_FechaHasta.Clear;
    txt_CantVehiculos.Clear;
    txt_Apellido.Clear;
    txt_rut.Clear;
    txt_Patente.clear;
    ObtenerSolicitudContacto.close;
    lbl_Mostar.Caption:='';
    txt_Top.Value := 100;
    txt_FechaDesde.SetFocus;
end;

procedure TFormSolicitud.txt_rutKeyPress(Sender: TObject;
  var Key: Char);
begin
    Key := UpCase(KEY);
    if not (Key  in ['0'..'9','K','k', #8,'%']) then
       Key := #0
end;

procedure TFormSolicitud.btn_EliminarClick(Sender: TObject);
resourcestring
    ESTA_SEGURO_ELIMINAR_SOLICITUD = 'Se eliminar� la solicitud completa. �Desea continuar?';
begin
    if MsgBox(ESTA_SEGURO_ELIMINAR_SOLICITUD, STR_SOLICITUD_NRO + ObtenerSolicitudContacto.FieldByName('CodigoSolicitudContacto').AsString,
    MB_ICONWARNING or MB_YESNO) = mrYes then begin
        EliminarSolicitud;
    end;
end;

procedure TFormSolicitud.EliminarSolicitud;
var
    nroSolic: AnsiString;
begin
    nroSolic := ObtenerSolicitudContacto.FieldByName('CodigoSolicitudContacto').AsString;
    with EliminarSolicitudContacto do
    begin
        try
            DMConnections.BaseCAC.BeginTrans;
            Parameters.ParamByName('@CodigoSolicitudContacto').Value := nroSolic;
            ExecProc;
            DMConnections.BaseCAC.CommitTrans;
            btn_Filtrar.Click;
        except
            on E: Exception do begin
                if DMConnections.BaseCAC.InTransaction then
                    DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_ERROR_ELIMINAR_SOLICITUD, E.Message, STR_SOLICITUD_NRO + nroSolic, MB_ICONSTOP);
            end;
        end;
    end;
end;

procedure TFormSolicitud.txt_ApellidoKeyPress(Sender: TObject;
  var Key: Char);
begin
    key:=UpCase(Key);
end;

function TFormSolicitud.PuedeEditarSolicitud: boolean;
begin
    result:=ObtenerSolicitudContacto.FieldByName('PermiteEdicion').AsBoolean;
end;

procedure TFormSolicitud.ObtenerSolicitudContactoAfterScroll(
  DataSet: TDataSet);
begin
    btn_Ver.Enabled:=(ObtenerSolicitudContacto.Active) and (ObtenerSolicitudContacto.RecordCount>0);
    btn_Editar.Enabled      := (btn_Ver.Enabled) and (not SoloConsulta) and (PuedeEditarSolicitud);
    btn_Eliminar.Enabled    := btn_Editar.Enabled;
end;

procedure TFormSolicitud.txt_TopExit(Sender: TObject);
resourcestring
	MSG_NUMERO_INVALIDO = 'N�mero no V�lido.';
begin
	if txt_Top.Value < 1 then begin
		MsgBoxBalloon(MSG_NUMERO_INVALIDO,Caption,MB_ICONSTOP,txt_Top);
		txt_Top.SetFocus;
    end;
end;

{ TFormSolicitudConsulta }

function TFormSolicitudConsulta.Inicializar(MDIChild: Boolean; Caption: String; PuntoVenta, PuntoEntrega: Integer): Boolean;
begin
    Result := inherited Inicializar(MDIChild, true, Caption, FPuntoVenta, FPuntoEntrega);
end;

{ TFormSolicitudModifcacion }

function TFormSolicitudModifcacion.Inicializar(MDIChild: Boolean; Caption: String; PuntoVenta, PuntoEntrega: Integer): Boolean;
begin
    Result := inherited Inicializar(MDIChild, False, Caption, FPuntoVenta, FPuntoEntrega);
end;

end.



