{$A+,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
unit FrmInformacionGeneral;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Navigator, StdCtrls, ExtCtrls, Buttons, DmiCtrls, validate,
  DB, UtilProc, PeaProcs, ADODB, ComCtrls, Grids, DBGrids,
  Util, PeaTypes, ListBoxEx, DBListEx;

type
  TNavWindowInformacionGeneral = class(TNavWindowFrm)
    pnl_BordeSupIzq: TPanel;
    ImagenBordeSupIzq: TImage;
    ImagenBordeSupDer: TImage;
    pnl_Superior: TPanel;
    pnl_BordeSup: TPanel;
    pnl_General: TPanel;
    pnl_Filtros: TPanel;
    pnl_BordeIzq: TPanel;
    pnl_BordeDer: TPanel;
    pnl_Separacion1: TPanel;
    Label2: TLabel;
    cb_TipoInformacion: TComboBox;
    ds_Tarifas: TDataSource;
    ObtenerInformacionTarifas: TADOStoredProc;
    nb_Informacion: TNotebook;
    Panel5: TPanel;
    Panel6: TPanel;
    ScrollBox4: TScrollBox;
    Panel7: TPanel;
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    cb_Mapas: TComboBox;
    REditGeneral: TRichEdit;
    REditFormaPago: TRichEdit;
    grillaTarifas: TDBListEx;
    Panel2: TPanel;
    Panel4: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Panel19: TPanel;
    Panel17: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Panel18: TPanel;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel23: TPanel;
    Panel24: TPanel;
    sb_ImagenMapa: TScrollBox;
    ImageMapa: TImage;
    qry_Aux: TADOQuery;
    qry_PlanComercial: TADOQuery;
    Panel25: TPanel;
    Label4: TLabel;
    cb_PlanesComerciales: TComboBox;
    REditPlanComercial: TRichEdit;
    PanelPromociones: TPanel;
    Label5: TLabel;
    cb_Promociones: TComboBox;
    REditPromociones: TRichEdit;
    GBFiltroPromociones: TGroupBox;
    RBVigentes: TRadioButton;
    RBFuturas: TRadioButton;
    qry_promociones: TADOQuery;
    procedure cb_TipoInformacionChange(Sender: TObject);
    procedure cb_PlanesComercialesChange(Sender: TObject);
    procedure cb_PromocionesChange(Sender: TObject);
    procedure RBVigentesClick(Sender: TObject);
    procedure RBFuturasClick(Sender: TObject);
  private
	procedure AddText(re: TRichEdit; const Text: AnsiString;
	  FontName: TFontName = 'Arial'; FontSize: Integer = 10; FontStyle: TFontStyles = [];
	  FontColor: TColor = clBlack);
    procedure CargarInfoTiposPago;
    procedure CargarInfoCategorias;
    procedure CargarInfoPlanesComerciales;
    procedure CargarInfoTiposHorario;
    procedure CargarInfoTiposCliente;
    procedure CargarInfoPromociones;
    procedure CargarInfoTarifas;
    function ValoresParametro(TipoParametro: AnsiString; Valor: integer): ansistring;
  protected
	procedure Loaded; override;
  public
	{ Public declarations }
	Function GetBookmark(Var Bookmark, Description: AnsiString): Boolean; override;
	Function GotoBookmark(Bookmark: AnsiString): Boolean; override;
	class Function CreateBookmark(TipoInformacion: AnsiString): AnsiString;
	function Inicializa: Boolean; override;
  end;

var
  NavWindowInformacionGeneral: TNavWindowInformacionGeneral;

implementation

uses DMConnection;

{$R *.dfm}

{ TNavWindowDatosCliente }

class function TNavWindowInformacionGeneral.CreateBookmark(TipoInformacion: AnsiString): AnsiString;
begin
	if TipoInformacion = 'Mapas' then
    	Result := Istr(0)
    else if TipoInformacion = 'Categorias' then
    	Result := Istr(1)
	else if TipoInformacion = 'TipoPago' then
    	Result := Istr(2)
	else if TipoInformacion = 'TipoTarifas' then
    	Result := Istr(3)
	else if TipoInformacion = 'PlanesComerciales' then
    	Result := Istr(4)
	else if TipoInformacion = 'TiposHorario' then
    	Result := Istr(5)
	else if TipoInformacion = 'TiposCliente' then
    	Result := Istr(6)
    else
    	Result := Istr(-1);
end;


function TNavWindowInformacionGeneral.GetBookmark(var Bookmark, Description: AnsiString): Boolean;
resourcestring
    MSG_INFO_MAPAS              = 'Información General de Mapas';
    MSG_INFO_CATEGORIAS         = 'Información General de Categorías';
    MSG_INFO_TIPOS_PAGO         = 'Información General de Tipos de Pago';
    MSG_INFO_TIPOS_TARIFAS      = 'Información General de Tipos de Tarifa';
    MSG_INFO_PLANES_COMERCIALES = 'Información General de Planes Comerciales';
    MSG_INFO_TIPOS_HORARIO      = 'Información General de los Tipos de Horario';
    MSG_INFO_TIPOS_CLIENTE      = 'Información General de los Tipos de Cliente';
    MSG_INFO_PROMOCIONES        = 'Información General de las Promociones';    
begin
    Case cb_TipoInformacion.ItemIndex of
    	0:	begin
        		Bookmark	:= CreateBookmark('Mapas');
	            Description	:= MSG_INFO_MAPAS;
        	end;
        1:	begin
        		Bookmark	:= CreateBookmark('Categorias');
           		Description	:= MSG_INFO_CATEGORIAS;
        	end;
        2:	begin
        		Bookmark	:= CreateBookmark('TipoPago');
           		Description	:= MSG_INFO_TIPOS_PAGO;
        	end;
        3:	begin
        		Bookmark	:= CreateBookmark('TipoTarifas');
                Description	:= MSG_INFO_TIPOS_TARIFAS;
        	end;
        4:  begin
        		Bookmark	:= CreateBookmark('PlanesComerciales');
                Description	:= MSG_INFO_PLANES_COMERCIALES;
        	end;
        5:  begin
        		Bookmark	:= CreateBookmark('TiposHorario');
           		Description	:= MSG_INFO_TIPOS_HORARIO;
        	end;
        6:  begin
        		Bookmark	:= CreateBookmark('TiposCliente');
           		Description	:= MSG_INFO_TIPOS_CLIENTE;
        	end;
        7:  begin
        		Bookmark	:= CreateBookmark('Promociones');
           		Description	:= MSG_INFO_PROMOCIONES;
        	end;
    end;
    Result := True;
end;

function TNavWindowInformacionGeneral.GotoBookmark(Bookmark: AnsiString): Boolean;
begin
	cb_TipoInformacion.ItemIndex := IVal(ParseParamByNumber(Bookmark, 1));
    cb_TipoInformacion.OnChange(cb_TipoInformacion);
	Result := cb_TipoInformacion.ItemIndex <> -1;
end;

function TNavWindowInformacionGeneral.Inicializa: Boolean;
resourcestring
    MSG_COMBO_MAPA        = 'Mapa general de la Concesionaria %s';
    MSG_CONCESIONARIA     = 'Mapas de la Concesionaria';
    MSG_CATEGORIAS        = 'Tipos de Categorías';
    MSG_TIPOS_PAGO        = 'Tipos de Pago';
    MSG_TIPOS_TARIFAS     = 'Tipos de Tarifas';
    MSG_PLANES_COMERCILES = 'Planes Comerciales';
    MSG_TIPOS_HORARIO     = 'Tipos de horario';
    MSG_TIPOS_CLIENTE     = 'Tipos de cliente';
    MSG_PROMOCIONES       = 'Promociones';
Var
	Sz: TSize;
begin
	SZ := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, SZ.cx, sz.cy);
    Result := Inherited Inicializa ;

	with grillaTarifas do begin
		Columns[0].Header.Caption := MSG_COL_TITLE_CONCESIONARIA;
		Columns[1].Header.Caption := MSG_COL_TITLE_TIPO_DE_CLIENTE;
		Columns[2].Header.Caption := MSG_COL_TITLE_TIPO_DE_HORARIO;
		Columns[3].Header.Caption := MSG_COL_TITLE_PRECIO_POR_KM;
	end;

    nb_Informacion.PageIndex := 0;
    // Si se utiliza un esquema de abm para los mapas..
    // cargar la info de una tabla.
    cb_Mapas.Items.Add(Format(MSG_COMBO_MAPA, [PadL('Foto1.bmp', 300, ' ')]));
	cb_Mapas.ItemIndex := 0;

    //Tipos de Informacion.
    cb_TipoInformacion.Items.Add(MSG_CONCESIONARIA);    //0
    cb_TipoInformacion.Items.Add(MSG_CATEGORIAS);       //1
    cb_TipoInformacion.Items.Add(MSG_TIPOS_PAGO);       //2
	cb_TipoInformacion.Items.Add(MSG_TIPOS_TARIFAS);    //3
	cb_TipoInformacion.Items.Add(MSG_PLANES_COMERCILES);//4
    cb_TipoInformacion.Items.Add(MSG_TIPOS_HORARIO);    //5
    cb_TipoInformacion.Items.Add(MSG_TIPOS_CLIENTE);    //6
    cb_TipoInformacion.Items.Add(MSG_PROMOCIONES);      //7    
	cb_TipoInformacion.ItemIndex := 0;

    CargarPlanesComerciales(DMConnections.BaseCAC, cb_PlanesComerciales);
    cb_PlanesComerciales.ItemIndex := 0;
    cb_PlanesComercialesChange(cb_PlanesComerciales);
    
    RBVigentes.Checked := true;
    CargarPromociones(DMConnections.BaseCAC, cb_Promociones,
        RBVigentes.Checked, RBFuturas.Checked, '');

    CargarInfoTarifas;
end;



procedure TNavWindowInformacionGeneral.Loaded;
begin
	inherited;
	Color := $00FFFDFB;
end;

procedure TNavWindowInformacionGeneral.cb_TipoInformacionChange(
  Sender: TObject);
begin
	case cb_TipoInformacion.ItemIndex of
		0: nb_Informacion.PageIndex := 0;
		1: begin
                nb_Informacion.PageIndex := 1;
                CargarInfoCategorias;
           end;
		2: begin
               nb_Informacion.PageIndex := 2;
               CargarInfoTiposPago;
           end;
		3: begin
		       nb_Informacion.PageIndex := 3;
			   CargarInfoTarifas;
		   end;
        4: begin
               nb_Informacion.PageIndex := 4;
               cb_PlanesComerciales.ItemIndex := 0;
               CargarInfoPlanesComerciales;
           end;
        5: begin
               nb_Informacion.PageIndex := 1;
               CargarInfoTiposHorario;
           end;
        6: begin
               nb_Informacion.PageIndex := 1;
               CargarInfoTiposCliente;
           end;
        7: begin
               nb_Informacion.PageIndex := 5;
               cb_Promociones.ItemIndex := 0;
               CargarInfoPromociones;
           end;
	end;
end;

procedure TNavWindowInformacionGeneral.AddText(re: TRichEdit;
  const Text: AnsiString; FontName: TFontName; FontSize: Integer;
  FontStyle: TFontStyles; FontColor: TColor);
begin
	re.SelAttributes.Name := FontName;
	re.SelAttributes.Size := FontSize;
	re.SelAttributes.Style := FontStyle;
	re.SelAttributes.Color := FontColor;
	re.SelText := Text;
end;

procedure TNavWindowInformacionGeneral.CargarInfoCategorias;
resourcestring
    TITULO = 'Los distintos tipo de categorias habilitados son:';
begin
    REditGeneral.Clear;
    AddText(REditGeneral, CRLF + TITULO, 'Arial', 11, [fsBold, fsUnderline], clGreen);
    with qry_Aux do
    begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT * FROM Categorias ';
        Open;
        while not EOF do begin
            AddText(REditGeneral, CRLF+ CRLF + FieldByName('Categoria').asstring + '-  ' + FieldByName('Descripcion').asstring, 'Arial', 11,[fsBold]);
            if not FieldByName('Comentarios').IsNull then AddText(REditGeneral, CRLF + FieldByName('Comentarios').asstring);
            Next;
        end;
        Close;
    end;

end;

procedure TNavWindowInformacionGeneral.CargarInfoPlanesComerciales;
resourcestring
    TITULO              = 'Datos del Plan comercial: ';
    TITULO_CATEGORIAS   = 'Categorías: ';
    TITULO_TIPO_PAGO    = 'Formas de pago: ';
    TITULO_POSIBILIDAD  = 'Posibilidades de adhesión: ';
var
    select: AnsiString;
    plan: integer;
begin
    REditPlanComercial.Clear;

    //si no hay ninguno elegido, nos vamos...
    if not (cb_PlanesComerciales.ItemIndex >= 0) then exit;

    plan := IVal(StrRight(cb_PlanesComerciales.Text, 20));

    with qry_PlanComercial do
    begin
        Close;
        Parameters.ParamByName('xPlanComercial').Value := plan;
        Open;
        //TITULO
        AddText(REditPlanComercial, CRLF + trim(FieldByName('Descripcion').AsString), 'Arial', 11, [fsBold, fsUnderline], clGreen);
        if not FieldByName('Observaciones').IsNull then
            AddText(REditPlanComercial, CRLF+ CRLF + FieldByName('Observaciones').asstring, 'Arial', 10);
        //Aca vendrían los importes, ver si se tienen que mostrar.
        Close;
    end;

    //Categorías del plan
    with qry_Aux do
    begin
        Close;
        SQL.Clear;
        select := 'SELECT A.*, B.Descripcion as DescCategoria FROM PlanesComercialesCategorias A, Categorias B '+
                  'where PlanComercial = '+ IntToStr(plan)+
                  'AND A.Categoria = B.Categoria';

        SQL.Text := select;

        Open;
        if not EOF then begin
            AddText(REditPlanComercial,CRLF +
                    CRLF + TITULO_CATEGORIAS, 'Arial', 10, [fsBold]);
            first;
            while not EOF do
            begin
                AddText(REditPlanComercial,
                  CRLF + '   - ' + FieldByName('DescCategoria').asstring, 'Arial', 10);
                Next;
            end;
        end;
        Close;
    end;

    //Tipos de pago del plan
    with qry_Aux do
    begin
        Close;
        SQL.Clear;
        select := 'SELECT A.*, B.Descripcion as DescTipoPago FROM PlanesComercialesTipoPago A, TipoPago B '+
                  'where A.PlanComercial = '+ IntToStr(plan)+
                  'AND A.TipoPago = B.TipoPago ' +
                  'AND B.Habilitado = 1';  //muestro los tipos de pago habilitados
        SQL.Text := select;

        Open;
        if not EOF then begin
            AddText(REditPlanComercial,CRLF +
                    CRLF + TITULO_TIPO_PAGO, 'Arial', 10, [fsBold]);
            first;
            while not EOF do
            begin
                AddText(REditPlanComercial,
                  CRLF + '   - ' + FieldByName('DescTipoPago').asstring, 'Arial', 10);
                Next;
            end;
        end;
        Close;
    end;

    //Posibilidades de adhesion (PLANESCOMERCIALESPOSIBLES)
    with qry_Aux do
    begin
        Close;
        SQL.Clear;
        select := 'SELECT A.*, rtrim(B.Descripcion) as DescAdhesion, rtrim(C.Descripcion) as DescCliente ' +
                  'FROM PlanesComercialesPosibles A,TiposAdhesion B, TiposCliente C ' +
                  'WHERE A.PlanComercial = '+ IntToStr(plan)+
                  'AND A.TipoCliente   = C.TipoCliente ' +
                  'AND A.TipoAdhesion  = B.TipoAdhesion ';
        SQL.Text := select;
        Open;
        if not EOF then begin
            AddText(REditPlanComercial,CRLF +
                    CRLF + TITULO_POSIBILIDAD, 'Arial', 10, [fsBold]);
            first;
            while not EOF do
            begin
                AddText(REditPlanComercial,
                  CRLF + '   - ' + FieldByName('DescCliente').asstring + ', ' +
                  FieldByName('DescAdhesion').asstring, 'Arial', 10);
                Next;
            end;
        end;
        Close;
    end;
end;

procedure TNavWindowInformacionGeneral.CargarInfoTiposPago;
resourcestring
    TITULO = 'Las distintas Formas de pago son:';
begin
(*
Las distintas Formas de pago son

Pago Manual: con esta modalidad de pago, el cliente debe presentarse
personalmente en la concesionaria
para abonar sus comprobantes.
Debito Automatico: con esta modalidad de pago, el cliente determina la
cuenta bancaria o Tarjeta de Crédito de la cual se debitarán los cargos
correspondientes
*)
    REditFormaPago.Clear;
    AddText(REditFormaPago, CRLF + TITULO + CRLF, 'Arial', 11, [fsBold, fsUnderline], clGreen);
    //AddText(REditFormaPago, '', 'Arial', 12, [fsUnderline], clGreen);

    with qry_Aux do
    begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT * FROM TipoPago WHERE Habilitado = 1';
        Open;
        while not EOF do
        begin
            AddText(REditFormaPago,
                    CRLF + FieldByName('TipoPago').asstring + '   ' +
                    FieldByName('Descripcion').asstring, 'Arial', 10, [fsBold]);
            AddText(REditFormaPago, CRLF);
            AddText(REditFormaPago, FieldByName('Explicacion').asstring, 'Arial', 10);
            AddText(REditFormaPago, CRLF);
            Next;
        end;
        Close;
    end;


end;

procedure TNavWindowInformacionGeneral.CargarInfoTiposCliente;
resourcestring
    TITULO = 'Los distintos tipos de clientes son:';
begin
    REditGeneral.Clear;
    AddText(REditGeneral, CRLF + TITULO, 'Arial', 11, [fsBold, fsUnderline], clGreen);
    with qry_Aux do
    begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT * FROM TiposCliente ';
        Open;
        while not EOF do
        begin
            AddText(REditGeneral,
                    CRLF+ CRLF + FieldByName('TipoCliente').asstring + '-  ' +
                    FieldByName('Descripcion').asstring, 'Arial', 11,[fsBold]);
            if not FieldByName('Comentarios').IsNull then
                AddText(REditGeneral, CRLF + FieldByName('Comentarios').asstring)
            else
                AddText(REditGeneral, CRLF);


            Next;
        end;
        Close;
    end;
end;

procedure TNavWindowInformacionGeneral.CargarInfoTiposHorario;
resourcestring
    TITULO = 'Los distintos tipos de horarios son:';
begin
    REditGeneral.Clear;
    AddText(REditGeneral, CRLF + TITULO, 'Arial', 11, [fsBold, fsUnderline], clGreen);
    with qry_Aux do
    begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT * FROM TiposHorario ';
        Open;
        while not EOF do
        begin
            AddText(REditGeneral,
                    CRLF+ CRLF + FieldByName('TipoHorario').asstring + '-  ' +
                    FieldByName('Descripcion').asstring, 'Arial', 11,[fsBold]);
            if not FieldByName('Comentarios').IsNull then
                AddText(REditGeneral, CRLF + FieldByName('Comentarios').asstring)
            else
                AddText(REditGeneral, CRLF);
            Next;
        end;
        Close;
    end;
end;

procedure TNavWindowInformacionGeneral.cb_PlanesComercialesChange(
  Sender: TObject);
begin
    CargarInfoPlanesComerciales;
end;

procedure TNavWindowInformacionGeneral.CargarInfoPromociones;
resourcestring
    TITULO       = 'Datos de la Promoción: ';
    TITULO_PLAN  = 'Planes comerciales: ';
    VIGENCIA     = 'Vigencia: ';
    DESDE        = ' desde ';
    HASTA        = ' hasta ';
    ENTRE        = ' entre ';
    Y            = ' y ';
    DESCUENTO    = 'Descuento ';
var
    select: AnsiString;
    promo: integer;
begin
    REditPromociones.Clear;

    //si no hay ninguno elegido, nos vamos...
    if not (cb_Promociones.ItemIndex >= 0) then exit;

    promo := IVal(StrRight(cb_Promociones.Text, 20));

    with qry_Promociones do
    begin
        Close;
        Parameters.ParamByName('xNumeroPromocion').Value := promo;
        Open;
        //TITULO
        AddText(REditPromociones, CRLF + trim(FieldByName('Descripcion').AsString),
                'Arial', 11, [fsBold, fsUnderline], clGreen);
        if not FieldByName('Observaciones').IsNull then
            AddText(REditPromociones, CRLF+ CRLF + FieldByName('Observaciones').asstring)
        else
            AddText(REditPromociones, CRLF);

        //Aca vendrían los importes, ver si se tienen que mostrar.
        //VIGENCIA
        AddText(REditPromociones, CRLF + VIGENCIA);

        AddText(REditPromociones, DESDE + FieldByName('FechaHoraActivacion').AsString +
                HASTA + FieldByName('FechaHoraBaja').AsString, 'Arial', 10, [fsBold]);

        //DESCRIPCION PARAMETRO
        AddText(REditPromociones, CRLF +
                DescripcionParametroPromocion(FieldByName('TipoParametro').AsString),
                'Arial', 10);
        AddText(REditPromociones, ENTRE, 'Arial', 10);

        (*Si es por rango de importe hay que dividir por 100,
        estan guardados en la base * 100... *)
        AddText(REditPromociones, ValoresParametro(FieldByName('TipoParametro').AsString,
                                  FieldByName('ValorInicial').AsInteger), 'Arial', 10, [fsBold]);
        //AddText(REditPromociones, Y, 'Arial', 10);
        AddText(REditPromociones, Y + ValoresParametro(FieldByName('TipoParametro').AsString,
                                    FieldByName('ValorFinal').AsInteger), 'Arial', 10, [fsBold]);

        //DESCUENTO
        AddText(REditPromociones, CRLF + CRLF +
                DESCUENTO +
                DescripcionDescuento(FieldByName('ValorDescuento').AsInteger,
                                     FieldByName('TipoDescuento').asString),
                'Arial', 10, [fsBold, fsItalic]);
        Close;
    end;

    //Planes comerciales de la Promocion
    with qry_Aux do
    begin
        Close;
        SQL.Clear;
        select := 'SELECT A.*, rtrim(B.Descripcion) as DescPromo FROM PromocionPlanComercial A, PlanesComerciales B '+
                  'WHERE A.NumeroPromocion = '+ IntToStr(promo)+
                  '  AND A.PlanComercial = B.PlanComercial';
        SQL.Text := select;
        Open;
        if not EOF then begin
            AddText(REditPromociones, CRLF +
                    CRLF + TITULO_PLAN, 'Arial', 10, [fsBold]);
            first;
            while not EOF do
            begin
                AddText(REditPromociones,
                  CRLF + '   - ' + FieldByName('DescPromo').asstring, 'Arial', 10);
                Next;
            end;
        end;
        Close;
    end;
end;

procedure TNavWindowInformacionGeneral.cb_PromocionesChange(
  Sender: TObject);
begin
    CargarInfoPromociones;
end;

procedure TNavWindowInformacionGeneral.RBVigentesClick(Sender: TObject);
begin
    CargarPromociones(DMConnections.BaseCAC,cb_Promociones,
                        RBVigentes.Checked, RBFuturas.Checked);
    cb_Promociones.ItemIndex := 0;
    cb_PromocionesChange(cb_Promociones);    
end;

procedure TNavWindowInformacionGeneral.RBFuturasClick(Sender: TObject);
begin
    CargarPromociones(DMConnections.BaseCAC,cb_Promociones,
                        RBVigentes.Checked, RBFuturas.Checked);
    cb_Promociones.ItemIndex := 0;
    cb_PromocionesChange(cb_Promociones);
end;


function TNavWindowInformacionGeneral.ValoresParametro(
  TipoParametro: AnsiString; Valor: integer): ansistring;
var
    importe: integer;
begin
    if (TipoParametro = 'IV') or (TipoParametro = 'IT') then begin
        importe := trunc(valor / 100);
        result := formatFloat(FORMATO_IMPORTE, importe);
    end
    else
        result := IntToStr(Valor);
end;

procedure TNavWindowInformacionGeneral.CargarInfoTarifas;
resourcestring
    MSG_ERRORTARIFAS = 'No se pudieron obtener los datos de Tarifas.';
    CAPTION_TARIFAS  = 'Tarifas';
begin
	try
		ObtenerInformacionTarifas.Close;
    	ObtenerInformacionTarifas.Parameters.ParamByName('@CodigoConcesionaria').Value := COD_CONCESIONARIA;
        ObtenerInformacionTarifas.Open;
    except
		On E: Exception do begin
        	MsgBoxErr(MSG_ERRORTARIFAS, e.Message, CAPTION_TARIFAS, MB_ICONSTOP);
        end;
    end;
end;

end.
