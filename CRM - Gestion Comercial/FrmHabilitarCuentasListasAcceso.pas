{
Firma       : SS-1006-NDR-20120614
Description : Ventana para ADHERIR cuentas en lista de acceso en forma individual
              LA ADHESION es por convenio

Firma       : SS_1006_1015_MCO_20121003
Descripcion : Se valida que en la adhesion de cuentas en lista de acceso, permita seleccionar todas las cuentas.

}
unit FrmHabilitarCuentasListasAcceso;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx,UtilProc,DmConnection,Peatypes,
  DB, ADODB, DmiCtrls, Validate, DateEdit,peaprocs,util,UtilDB,RStrings,
  VariantComboBox, frmVisorDocumentos, Convenios, frmImprimirConvenio,
  ConstParametrosGenerales, DBClient, ImgList,Math, Provider;

type
  TFormHabilitarCuentasListasAcceso = class(TForm)
    pnlGridsPanel: TPanel;
    dbl_Cuentas: TDBListEx;
    pnlBottomPanel: TPanel;
    BtnSalir: TButton;
    lnCheck: TImageList;
    btnTodos: TButton;
    btnNinguno: TButton;
    spObtenerCuentas: TADOStoredProc;
    dsObtenerCuentas: TDataSource;
    btnProcesar: TButton;
    spActualizarCuentaInhabilitada: TADOStoredProc;
    Panel1: TPanel;
    lblCuentas: TLabel;
    Label1: TLabel;
    cbConcesionariaAplicar: TComboBox;
    dspObtenerCuentas: TDataSetProvider;
    cdsObtenerCuentas: TClientDataSet;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure dbl_CuentasDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dbl_CuentasLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnTodosClick(Sender: TObject);
    procedure btnNingunoClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
  private
    { Private declarations }
    procedure CargarConcesionarias(Conn: TADOConnection; Combo: TComboBox; CodigoConcesionarias:Integer = 0; TieneItemNinguno: boolean = false);
  public
    { Public declarations }
  	//FCuentasElegidas: set of Byte;                                             //SS_1006_1015_MCO_20121003
    function Inicializar(CodigoConvenio: Integer): Boolean;
  end;

var
  FormHabilitarCuentasListasAcceso: TFormHabilitarCuentasListasAcceso;

implementation

{$R *.dfm}

procedure TFormHabilitarCuentasListasAcceso.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     action := caFree;
end;

function TFormHabilitarCuentasListasAcceso.Inicializar(CodigoConvenio: Integer): Boolean;
resourcestring
    STR_LISTADO_CONVENIO = 'Habilitar Cuentas en Listas de Acceso';
var
//S: TSize;
    CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO : Integer;
    i:Integer;
begin
    CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO := QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO()');
   //	FCuentasElegidas := [];                                                   //SS_1006_1015_MCO_20121003
    try
        CargarConcesionarias(DMConnections.BaseCAC, cbConcesionariaAplicar, CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO, false);
        with spObtenerCuentas do
        begin
          Close;
          Parameters.Refresh;
          Parameters.ParamByName('@CodigoConvenio').Value:= CodigoConvenio;
          Parameters.ParamByName('@TraerBaja').Value:=0;
          cdsObtenerCuentas.Open;                                                //SS_1006_1015_MCO_20121003
          dsObtenerCuentas.DataSet := cdsObtenerCuentas;                         //SS_1006_1015_MCO_20121003
        end;
        Result := True;
    except
        on e: Exception do begin
            MsgBoxErr(Format(MSG_ERROR_INICIALIZACION,[STR_LISTADO_CONVENIO]),e.Message, Caption, MB_ICONSTOP);
    		Result := false;
        end;
    end;
end;

procedure TFormHabilitarCuentasListasAcceso.btnNingunoClick(Sender: TObject);
begin
 //	FCuentasElegidas := [];                                                      //SS_1006_1015_MCO_20121003
	Screen.Cursor := crHourGlass ;
 { dbl_Cuentas.Invalidate;                                                       //SS_1006_1015_MCO_20121003
	dbl_Cuentas.Repaint;    }                                                      //SS_1006_1015_MCO_20121003

    with dsObtenerCuentas.DataSet do                                             //SS_1006_1015_MCO_20121003
        try                                                                      //SS_1006_1015_MCO_20121003
            First;                                                               //SS_1006_1015_MCO_20121003
            while not Eof do begin                                               //SS_1006_1015_MCO_20121003
                if FieldByName('Chequeado').AsBoolean then begin                 //SS_1006_1015_MCO_20121003
                    Edit;                                                        //SS_1006_1015_MCO_20121003
                    FieldByName('Chequeado').AsBoolean := False;                 //SS_1006_1015_MCO_20121003
                    Post;                                                        //SS_1006_1015_MCO_20121003
                end;                                                             //SS_1006_1015_MCO_20121003
                                                                                 //SS_1006_1015_MCO_20121003
                Next;                                                            //SS_1006_1015_MCO_20121003
            end;                                                                 //SS_1006_1015_MCO_20121003
            First;                                                               //SS_1006_1015_MCO_20121003
        finally                                                                  //SS_1006_1015_MCO_20121003
        	Screen.Cursor := crDefault ;                                          //SS_1006_1015_MCO_20121003
        end;                                                                     //SS_1006_1015_MCO_20121003
end;

procedure TFormHabilitarCuentasListasAcceso.btnProcesarClick(Sender: TObject);
var
  lstErrores:TStringList;
ResourceString
    MSG_RESULT     = 'Se han producido los siguientes errores';
    MSG_ATENTION   = 'Atenci�n';
begin
  ModalResult:=mrOk;
end;

procedure TFormHabilitarCuentasListasAcceso.BtnSalirClick(Sender: TObject);
begin
    close;
end;

procedure TFormHabilitarCuentasListasAcceso.btnTodosClick(Sender: TObject);
begin
 	//FCuentasElegidas := [];                                                       //SS_1006_1015_MCO_20121003
	Screen.Cursor := crHourGlass ;
{	if spObtenerCuentas.Active then begin                                           //SS_1006_1015_MCO_20121003
		spObtenerCuentas.DisableControls;                                             //SS_1006_1015_MCO_20121003
		spObtenerCuentas.First;                                                       //SS_1006_1015_MCO_20121003
		while not spObtenerCuentas.eof do begin                                       //SS_1006_1015_MCO_20121003
    //  Include(FCuentasElegidas, spObtenerCuentas.RecNo);                        //SS_1006_1015_MCO_20121003
			spObtenerCuentas.Next;                                                      //SS_1006_1015_MCO_20121003
		end;                                                                          //SS_1006_1015_MCO_20121003
		spObtenerCuentas.EnableControls;                                              //SS_1006_1015_MCO_20121003
		spObtenerCuentas.First;                                                       //SS_1006_1015_MCO_20121003
		dbl_Cuentas.Invalidate;                                                       //SS_1006_1015_MCO_20121003
		dbl_Cuentas.Repaint;                                                          //SS_1006_1015_MCO_20121003
	end;}                                                                           //SS_1006_1015_MCO_20121003
  with dsObtenerCuentas.DataSet do                                                //SS_1006_1015_MCO_20121003
        try                                                                       //SS_1006_1015_MCO_20121003
            DisableControls;                                                      //SS_1006_1015_MCO_20121003
            First;                                                                //SS_1006_1015_MCO_20121003
            while not Eof do begin                                                //SS_1006_1015_MCO_20121003
                if not FieldByName('Chequeado').AsBoolean then begin              //SS_1006_1015_MCO_20121003
                    Edit;                                                         //SS_1006_1015_MCO_20121003
                    FieldByName('Chequeado').AsBoolean := True;                   //SS_1006_1015_MCO_20121003
                    Post;                                                         //SS_1006_1015_MCO_20121003
                end;                                                              //SS_1006_1015_MCO_20121003
                                                                                  //SS_1006_1015_MCO_20121003
                Next;                                                             //SS_1006_1015_MCO_20121003
            end;                                                                  //SS_1006_1015_MCO_20121003
                                                                                  //SS_1006_1015_MCO_20121003
            First;                                                                //SS_1006_1015_MCO_20121003
        finally                                                                   //SS_1006_1015_MCO_20121003
             EnableControls;                                                      //SS_1006_1015_MCO_20121003
             Screen.Cursor := crDefault ;                                         //SS_1006_1015_MCO_20121003
        end;                                                                      //SS_1006_1015_MCO_20121003

end;

procedure TFormHabilitarCuentasListasAcceso.CargarConcesionarias(Conn: TADOConnection; Combo: TComboBox; CodigoConcesionarias:Integer = 0; TieneItemNinguno: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
    s: AnsiString;
begin
	Qry := TAdoQuery.Create(nil);
	try
        try
            s := '';
            Qry.Connection := Conn;
            s := 'SELECT * FROM Concesionarias WITH (NOLOCK) ORDER BY Descripcion';
            Qry.SQL.Text := s;
            Qry.Open;
            Combo.Clear;
            i := -1;
            if TieneItemNinguno then begin
                Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + ' ');
                i := 0;
            end;
            While not Qry.Eof do begin
                Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
                  Space(200) + Trim(Qry.FieldByName('CodigoConcesionaria').AsString));
                if Qry.FieldByName('CodigoConcesionaria').AsInteger = CodigoConcesionarias then
                  i := Combo.Items.Count - 1;
                Qry.Next;
            end;
            if (Combo.Items.Count > 0) and (i = -1) then i := 0;
            Combo.ItemIndex := i;

        except
             on E: exception do MsgBoxErr( caption, e.message, caption, MB_ICONSTOP);
        end;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure TFormHabilitarCuentasListasAcceso.dbl_CuentasDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
	if Column = dbl_Cuentas.Columns[0] then begin
//		lnCheck.Draw(dbl_Cuentas.Canvas, Rect.Left + 3, Rect.Top + 1, iif(spObtenerCuentas.RecNo in FCuentasElegidas, 1, 0));   //SS_1006_1015_MCO_20121003
      lnCheck.Draw(dbl_Cuentas.Canvas, Rect.Left + 3, Rect.Top + 1, iif(cdsObtenerCuentas.FieldByName('Chequeado').AsBoolean, 1, 0)); //SS_1006_1015_MCO_20121003
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;
end;

procedure TFormHabilitarCuentasListasAcceso.dbl_CuentasLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
Var
	i: Integer;
begin
	//i := spObtenerCuentas.RecNo;                                                 //SS_1006_1015_MCO_20121003
	//if i in FCuentasElegidas then Exclude(FCuentasElegidas, i)                   //SS_1006_1015_MCO_20121003
	//  else Include(FCuentasElegidas, i);                                         //SS_1006_1015_MCO_20121003
  //	Sender.Invalidate;                                                         //SS_1006_1015_MCO_20121003

    dsObtenerCuentas.DataSet.Edit;                                               //SS_1006_1015_MCO_20121003
    dsObtenerCuentas.DataSet.FieldByName('Chequeado').AsBoolean:= not dsObtenerCuentas.DataSet.FieldByName('Chequeado').AsBoolean; //SS_1006_1015_MCO_20121003
    dsObtenerCuentas.DataSet.Post;                                               //SS_1006_1015_MCO_20121003
end;

end.
