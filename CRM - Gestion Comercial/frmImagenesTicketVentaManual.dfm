object ImagenesTicketVentaManuallForm: TImagenesTicketVentaManuallForm
  Left = 411
  Top = 50
  Caption = 'Imagen Ticket Venta Manual'
  ClientHeight = 483
  ClientWidth = 759
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object imgTicketVentaManual: TImage
    Left = 0
    Top = 65
    Width = 759
    Height = 361
    Align = alClient
    ExplicitWidth = 769
    ExplicitHeight = 385
  end
  object pnlInformacion: TPanel
    Left = 0
    Top = 0
    Width = 759
    Height = 65
    Align = alTop
    TabOrder = 0
    object lblFechaTVM: TLabel
      Left = 128
      Top = 6
      Width = 27
      Height = 13
      Caption = 'fecha'
    end
    object lblPatente: TLabel
      Left = 128
      Top = 24
      Width = 38
      Height = 13
      Caption = 'patente'
    end
    object lblImporte: TLabel
      Left = 448
      Top = 6
      Width = 36
      Height = 13
      Caption = 'importe'
    end
    object lblCategoria: TLabel
      Left = 448
      Top = 24
      Width = 45
      Height = 13
      Caption = 'categoria'
    end
    object lblNumCorrCA: TLabel
      Left = 448
      Top = 40
      Width = 50
      Height = 13
      Caption = 'numcorrca'
    end
    object lblConcesionaria: TLabel
      Left = 128
      Top = 40
      Width = 65
      Height = 13
      Caption = 'concesionaria'
    end
    object Label1: TLabel
      Left = 17
      Top = 6
      Width = 111
      Height = 13
      Caption = 'Fecha Hora de Venta : '
    end
    object Label2: TLabel
      Left = 17
      Top = 24
      Width = 48
      Height = 13
      Caption = 'Patente : '
    end
    object Label3: TLabel
      Left = 17
      Top = 40
      Width = 77
      Height = 13
      Caption = 'Concesionaria : '
    end
    object Label4: TLabel
      Left = 376
      Top = 40
      Width = 66
      Height = 13
      Caption = 'NumCorrCA : '
    end
    object Label5: TLabel
      Left = 376
      Top = 24
      Width = 57
      Height = 13
      Caption = 'Categor'#237'a : '
    end
    object Label6: TLabel
      Left = 376
      Top = 6
      Width = 48
      Height = 13
      Caption = 'Importe : '
    end
    object btnCerrar: TButton
      Left = 680
      Top = 18
      Width = 75
      Height = 25
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnCerrarClick
    end
  end
  object pnlEdicionPatente: TPanel
    Left = 0
    Top = 426
    Width = 759
    Height = 57
    Align = alBottom
    TabOrder = 1
    object lblEdicionPatente: TLabel
      Left = 17
      Top = 22
      Width = 48
      Height = 13
      Caption = 'Patente : '
    end
    object edtPatente: TEdit
      Left = 71
      Top = 18
      Width = 130
      Height = 21
      Hint = '&& letras, # n'#250'meros, *  letras '#243' n'#250'meros indistintamente '
      CharCase = ecUpperCase
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = 'UV 53 15'
    end
    object btnGuardar: TButton
      Left = 224
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Guardar'
      TabOrder = 1
      OnClick = btnGuardarClick
    end
  end
  object spActualizar_Transacciones_TVM: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'Actualizar_Transacciones_TVM'
    Parameters = <>
    Left = 312
    Top = 440
  end
end
