unit FrmSeleccionArchivoImportar;

interface

uses
  Windows, Messages, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Excel2000, OleServer, StdCtrls, PeaTypes, Grids, DBGrids,
  DPSGrid, ExtCtrls, DPSControls, DB, ADODB, SysUtils, ListBoxEx, DBListEx, 
  ExcelXP;

type

  TFormSeleccionArchivoImportar = class(TForm)
    pnlImportar: TPanel;
    ExcelApplication: TExcelApplication;
    ExcelWorkbook: TExcelWorkbook;
    ExcelWorksheetVehiculos: TExcelWorksheet;
    dsArchivos: TDataSource;
    spActualizarArchivoImportarVehiculos: TADOStoredProc;
    spObtenerArchivosImportarVehiculos: TADOStoredProc;
    spActualizarEstadoArchivoImportarVehiculos: TADOStoredProc;
    dblArchivos: TDBListEx;
    spActualizarVehiculosExcel: TADOStoredProc;
    Panel1: TPanel;
    btnImportar: TButton;
    btnSalir: TButton;
    procedure btnImportarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spObtenerArchivosImportarVehiculosAfterOpen(
      DataSet: TDataSet);
    procedure dblArchivos1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dblArchivosClick(Sender: TObject);
    procedure dblArchivosColumns4HeaderClick(Sender: TObject);
    procedure dblArchivosDblClick(Sender: TObject);
  private
    FCodigoSolicitud: integer;
    FEvento: string;
    FArchivo: string;
    FVehiculos: TVehiculos;
    FDatosPersona: TDatosPersona;
    FDirectorio: string;
    FOrdenActual: string;
    procedure FinalizarImportacion;
    procedure LeerArchivos(Directorio: string);
    procedure ActualizarEstadoArchivo(Archivo: string; Estado: integer; CodigoResultado: integer = 0; Observaciones: string = ''; ContadorVehiculosAgregados: integer = 0; ContadorVehiculosRechazados: integer = 0; CodigoSolicitud: integer = 0);
    procedure ObtenerArchivosImportacion(Orden: string);
    function AgregarVehiculoExcel(CodigoSolicitud: integer; Vehiculo: TVehiculos):boolean;
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(CaptionAMostrar: string): boolean;
  end;

resourcestring
    NO_CUMPLE_DIGITO_VERIFICADOR = 'ERROR: PATENTE, NO CUMPLE CON EL DIGITO VERIFICADOR';
    ERROR_ANIO_NO_ENCONTRADO = 'ERROR: A�O NO ENCONTRADO';
    ERROR_MARCA_NO_ENCONTRADA = 'ERROR: LA MARCA NO ESTA EN LA BASE DE DATOS';
    ERROR_TIPO_NO_ENCONTRADO = 'ERROR: EL TIPO NO ESTA EN LA BASE DE DATOS';
    ERROR_ANIO_INCORRECTO = 'ERROR: EL A�O NO ES CORRECTO';
    ERROR_GUARDAR_VEHICULO = 'ERROR: NO SE PUDO GUARDAR ESTE VEHICULO EN LA BASE';
    IMPORTACION_EXISTOSA = 'Importaci�n Exitosa';
    IMPORTACION_ERRONEA  = 'Importaci�n con Errores';
    IMPORTACION_CON_RECHAZOS = 'Importaci�n con Rechazos';
    IMPORTACION_CANCELADA = 'Importaci�n Cancelada';
var
  FormSeleccionArchivoImportar: TFormSeleccionArchivoImportar;

implementation

uses DMConnection, frmDiferenciaSolicitudContacto, RStrings, UtilProc, UtilDB, Util, PeaProcs,
ConstParametrosGenerales, Abm_obj, FrmObservacionesGeneral;

{$R *.dfm}

procedure TFormSeleccionArchivoImportar.btnImportarClick(Sender: TObject);
var
  i, j, Cantidad, ContadorVehiculosAgregados,
  ContadorVehiculosRechazados: integer;
  Documento, ArchivoRechazo: string;
  f: TformDiferenciaSolicitudContacto;
  fm: TFormObservacionesGeneral;
  Importar, GeneroRechazos, Protegido: boolean;
  dondeEstaba: TBookmark;

begin
        Screen.Cursor   := crHourGlass;


        dondeEstaba := spObtenerArchivosImportarVehiculos.GetBookmark;

        try
            ExcelApplication.Connect;
        except
            On E: Exception do begin
                MsgBoxErr(MSG_EXCEL_ERROR, e.message, MSG_CAPTION_IMPORTAR_VEHICULOS, MB_ICONSTOP);
                Screen.Cursor := crDefault;
                Exit;
            end;
        end;

        ExcelApplication.Visible[0]  :=  False;

        ExcelWorkbook.ConnectTo(ExcelApplication.Workbooks.Open(
            FDirectorio + '\' +
            spObtenerArchivosImportarVehiculos.FieldByName('Archivo').AsString,
            emptyParam,
            emptyParam,
            emptyParam,
            emptyParam,
            emptyParam,
            emptyParam,
            emptyParam,
            emptyParam,
            emptyParam,
            emptyParam,
            emptyParam,
            emptyParam, 0));

        ExcelWorksheetVehiculos.ConnectTo(ExcelWorkbook.Sheets.Item[1] as ExcelWorkSheet);

        FCodigoSolicitud := 0;
        FEvento := IMPORTACION_EXISTOSA;
        FArchivo := spObtenerArchivosImportarVehiculos.FieldByName('Archivo').AsString;

        i:= 1;
        //Busco el RUT en la planilla
        while ((UpperCase(Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_CAPTION_RUT].Value))) <> COLUMNA_RUT) and
              (i <= 20)) do
            i:= i + 1;

        //Verifico si esta el RUT en la Planilla
        if ((Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_CAPTION_RUT].Value)) <> COLUMNA_RUT)) then begin
            MsgBox(MSG_ERROR_FALTA_RUT_ARCHIVO, MSG_CAPTION_IMPORTAR_VEHICULOS, MB_ICONSTOP);
            ActualizarEstadoArchivo(spObtenerArchivosImportarVehiculos.FieldByName('Archivo').AsString, ESTADO_CON_ERRORES,FALTA_RUT_EXCEL,'');
            FEvento := IMPORTACION_ERRONEA;
            FinalizarImportacion;
            Screen.Cursor   := crDefault;
            Exit;
        end;

        if (Trim(String(ExcelWorksheetVehiculos.Cells.Item[NUMERO_FILA_CLIENTE,NUMERO_COLUMNA_PERSONERIA].Value)) = COLUMNA_PERSONERIA_CLIENTE) then
            FDatosPersona.Personeria := PERSONERIA_FISICA_DESC
        else if (Trim(String(ExcelWorksheetVehiculos.Cells.Item[NUMERO_FILA_EMPRESA,NUMERO_COLUMNA_PERSONERIA].Value)) = COLUMNA_PERSONERIA_EMPRESA) then
                FDatosPersona.Personeria := PERSONERIA_JURIDICA_DESC;

        //Saco el guion al RUT
        Documento := Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_RUT].Value));
        Delete(Documento,Pos('-',Documento),1);

        //Verifico si esta el RUT
        if Documento = '' then begin
            MsgBox(MSG_ERROR_FALTA_RUT_ARCHIVO, MSG_CAPTION_IMPORTAR_VEHICULOS, MB_ICONSTOP);
            ActualizarEstadoArchivo(spObtenerArchivosImportarVehiculos.FieldByName('Archivo').AsString, ESTADO_CON_ERRORES,FALTA_RUT_EXCEL,'');
            FEvento := IMPORTACION_ERRONEA;
            FinalizarImportacion;
            Screen.Cursor   := crDefault;
		    Exit;
        end;

        //Verifico si el RUT es correcto
        if (QueryGetValue(DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
          StrLeft(Documento, (Length(Documento) -1)) +
          ''',''' +
            Documento[Length(Documento)] + '''') = '0') then begin
            MsgBox(MSG_ERROR_DOCUMENTO, MSG_CAPTION_IMPORTAR_VEHICULOS, MB_ICONSTOP);
            ActualizarEstadoArchivo(spObtenerArchivosImportarVehiculos.FieldByName('Archivo').AsString, ESTADO_CON_ERRORES,RUT_INCORRECTO,'');
            FEvento := IMPORTACION_ERRONEA;
            FinalizarImportacion;
            Screen.Cursor   := crDefault;
		    Exit;
        end;

        //Verifico si el RUT Existe y la cantidad de veces que se repite
        Cantidad := StrtoInt(QueryGetValue(DMConnections.BaseCAC, 'select dbo.ContarContactos(''RUT'','''
                      + Documento + ''')'));

        //Solo sigo si hay m�s de una solicitud
        if Cantidad = 0 then begin
            MsgBox(MSG_ERROR_FALTA_SOLICITUD, MSG_CAPTION_IMPORTAR_VEHICULOS, MB_ICONSTOP);
            ActualizarEstadoArchivo(spObtenerArchivosImportarVehiculos.FieldByName('Archivo').AsString, ESTADO_CON_ERRORES,FALTA_SOLICITUD,'');
            FEvento := IMPORTACION_ERRONEA;
            FinalizarImportacion;
            Screen.Cursor   := crDefault;
            Exit;
        end else begin
            if FDatosPersona.Personeria = PERSONERIA_FISICA_DESC then begin
                FDatosPersona.RazonSocial := '';
                FDatosPersona.Nombre := Trim(String(ExcelWorksheetVehiculos.Cells.Item[6,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Apellido := Trim(String(ExcelWorksheetVehiculos.Cells.Item[7,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.ApellidoMaterno := Trim(String(ExcelWorksheetVehiculos.Cells.Item[8,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Sexo := Trim(String(ExcelWorksheetVehiculos.Cells.Item[10,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.FechaNacimiento := Trim(String(ExcelWorksheetVehiculos.Cells.Item[9,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.TelefonoPrincipal := Trim(String(ExcelWorksheetVehiculos.Cells.Item[12,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.TelefonoAlternativo := Trim(String(ExcelWorksheetVehiculos.Cells.Item[13,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Domicilio := Trim(String(ExcelWorksheetVehiculos.Cells.Item[18,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Domicilio := FDatosPersona.Domicilio + ' ' + Trim(String(ExcelWorksheetVehiculos.Cells.Item[19,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Domicilio := FDatosPersona.Domicilio + ' - ' + Trim(String(ExcelWorksheetVehiculos.Cells.Item[17,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Domicilio := FDatosPersona.Domicilio + ' - ' + Trim(String(ExcelWorksheetVehiculos.Cells.Item[16,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Email:= Trim(String(ExcelWorksheetVehiculos.Cells.Item[11,NUMERO_COLUMNA_RUT].Value));
            end else begin
                FDatosPersona.RazonSocial := Trim(String(ExcelWorksheetVehiculos.Cells.Item[6,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Nombre := Trim(String(ExcelWorksheetVehiculos.Cells.Item[9,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Apellido := Trim(String(ExcelWorksheetVehiculos.Cells.Item[10,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.ApellidoMaterno := Trim(String(ExcelWorksheetVehiculos.Cells.Item[11,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Sexo := Trim(String(ExcelWorksheetVehiculos.Cells.Item[12,NUMERO_COLUMNA_RUT].Value));
                //FDatosPersona.FechaNacimiento := Trim(String(ExcelWorksheetVehiculos.Cells.Item[8,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.TelefonoPrincipal := Trim(String(ExcelWorksheetVehiculos.Cells.Item[14,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.TelefonoAlternativo := Trim(String(ExcelWorksheetVehiculos.Cells.Item[15,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Domicilio := Trim(String(ExcelWorksheetVehiculos.Cells.Item[20,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Domicilio := FDatosPersona.Domicilio + ' ' + Trim(String(ExcelWorksheetVehiculos.Cells.Item[21,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Domicilio := FDatosPersona.Domicilio + ' - ' + Trim(String(ExcelWorksheetVehiculos.Cells.Item[19,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Domicilio := FDatosPersona.Domicilio + ' - ' + Trim(String(ExcelWorksheetVehiculos.Cells.Item[18,NUMERO_COLUMNA_RUT].Value));
                FDatosPersona.Email:=  Trim(String(ExcelWorksheetVehiculos.Cells.Item[13,NUMERO_COLUMNA_RUT].Value));
            end;

            Application.CreateForm(TformDiferenciaSolicitudContacto, f);
            if (f.inicializar('', Documento, FDatosPersona)) and (f.ShowModal = mrOk) then begin
                FCodigoSolicitud := f.CodigoSolicitud;
                //Busco los vehiculos en la Planilla
                while (UpperCase(Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_PATENTE].Value))) <> COLUMNA_PATENTE) and
                    (i<=50)  do
                    i:= i + 1;

                if (UpperCase(Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_PATENTE].Value))) <> COLUMNA_PATENTE) then begin
                    MsgBox(MSG_ERROR_PLANILLA_EXCEL, MSG_CAPTION_IMPORTAR_VEHICULOS, MB_ICONSTOP);
                    FEvento := IMPORTACION_ERRONEA;
                    ActualizarEstadoArchivo(spObtenerArchivosImportarVehiculos.FieldByName('Archivo').AsString, ESTADO_CON_ERRORES,FALTAN_VEHICULOS,'');
                    FinalizarImportacion;
                    Screen.Cursor   := crDefault;
                    Exit;
                end;

                i:= i + 1;

                GeneroRechazos := False;
                ContadorVehiculosAgregados := 0;
                ContadorVehiculosRechazados := 0;
                Protegido := true;

                while String(ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_PATENTE].Value) <> '' do begin
                    for j := NUMERO_COLUMNA_PATENTE to 9 do begin
                        case j of
                            NUMERO_COLUMNA_PATENTE: FVehiculos.Patente := Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,j].Value));
                            NUMERO_COLUMNA_DIGITO_PATENTE: FVehiculos.DigitoPatente := Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,j].Value));
                            NUMERO_COLUMNA_MARCA: FVehiculos.Marca := Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,j].Value));
                            NUMERO_COLUMNA_MODELO: FVehiculos.Modelo := Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,j].Value));
                            NUMERO_COLUMNA_ANIO: FVehiculos.Anio := Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,j].Value));
                            NUMERO_COLUMNA_TIPO_VEHICULO: FVehiculos.Tipo := Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,j].Value));
                            NUMERO_COLUMNA_PROPIEDAD: FVehiculos.Propiedad := Trim(String(ExcelWorksheetVehiculos.Cells.Item[i,j].Value));
                        end;
                    end;

                    Importar:= true;
                    //Verifico solo si la patente es chilena
                    if (Length(FVehiculos.Patente) = 6) and
                        (UpperCase(FVehiculos.Patente)[1] in ['A'..'Z']) and
                        (UpperCase(FVehiculos.Patente)[2] in ['A'..'Z']) and
                        (FVehiculos.Patente[3] in ['0'..'9']) and
                        (FVehiculos.Patente[4] in ['0'..'9']) and
                        (FVehiculos.Patente[5] in ['0'..'9']) and
                        (FVehiculos.Patente[6] in ['0'..'9']) and
                        (FVehiculos.DigitoPatente <> '') then
                            if (QueryGetValue(DMConnections.BaseCAC, 'Exec VerificarPatente ''' + FVehiculos.Patente +
                                ''',''' + FVehiculos.DigitoPatente + '''') = '0') then begin
                                Importar:= false;
                                if Protegido then begin
                                    ExcelWorksheetVehiculos.Unprotect(PROTEGER_EXCEL);
                                    Protegido := false;
                                end;
                                ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_PROPIEDAD+1].Value := NO_CUMPLE_DIGITO_VERIFICADOR;
                            end;

                    //OJO que pasa si no excel 2000 y se escribe lo que se quiere? si no la encuentro se Descarta?
                    FVehiculos.CodigoMarca := strtoint(QueryGetValue(DMConnections.BaseCAC, 'Exec ObtenerCodigoMarca ''' + FVehiculos.Marca + ''''));

                    //Obtengo el codigo del Tipo del vehiculo
                    //OJO que pasa si no excel 2000 y se escribe lo que se quiere? si no la encuentro se Descarta?
                    FVehiculos.CodigoTipo := strtoint(QueryGetValue(DMConnections.BaseCAC, 'Exec ObtenerCodigoTipoVehiculo ''' + FVehiculos.Tipo + ''''));

                    //OJO LO TOMO DESDE LOS CAMPOS DE OBLIGATORIEDAD?
                    if  (FVehiculos.Anio = '') then begin
                        if Protegido then begin
                            ExcelWorksheetVehiculos.Unprotect(PROTEGER_EXCEL);
                            Protegido := false;
                        end;
                        ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_PROPIEDAD+1].Value := ERROR_ANIO_NO_ENCONTRADO;
                        Importar := False;
                    end;

                    if (FVehiculos.CodigoMarca = 0) then begin
                        if Protegido then begin
                            ExcelWorksheetVehiculos.Unprotect(PROTEGER_EXCEL);
                            Protegido := false;
                        end;
                        ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_PROPIEDAD+1].Value := ERROR_MARCA_NO_ENCONTRADA;
                        Importar := False;
                    end;

                    if (FVehiculos.CodigoTipo = 0) then begin
                        if Protegido then begin
                            ExcelWorksheetVehiculos.Unprotect(PROTEGER_EXCEL);
                            Protegido := false;
                        end;
                        ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_PROPIEDAD+1].Value := ERROR_TIPO_NO_ENCONTRADO;
                        Importar := False;
                    end;

                    //Verifico si el anio es correcto
                    if  (Length(FVehiculos.Anio) <> 4)
                        or (not (IsNumeric(FVehiculos.Anio[1]) and IsNumeric(FVehiculos.Anio[2]) and IsNumeric(FVehiculos.Anio[3]) and IsNumeric(FVehiculos.Anio[4]))
                        or (not EsAnioCorrecto(strtoint(FVehiculos.Anio)))) then begin
                        if Protegido then begin
                            ExcelWorksheetVehiculos.Unprotect(PROTEGER_EXCEL);
                            Protegido := false;
                        end;
                        ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_PROPIEDAD+1].Value := ERROR_ANIO_INCORRECTO;
                        Importar := False;
                    end;

                    if Importar then begin
                        if not AgregarVehiculoExcel(f.CodigoSolicitud, FVehiculos) then begin
                            if Protegido then begin
                                ExcelWorksheetVehiculos.Unprotect(PROTEGER_EXCEL);
                                Protegido := false;
                            end;
                            ExcelWorksheetVehiculos.Cells.Item[i,NUMERO_COLUMNA_PROPIEDAD+1].Value := ERROR_GUARDAR_VEHICULO;
                            ContadorVehiculosRechazados := ContadorVehiculosRechazados + 1;
                        end else begin
                            ContadorVehiculosAgregados := ContadorVehiculosAgregados + 1;
                        end;
                    end else begin
                        ContadorVehiculosRechazados := ContadorVehiculosRechazados + 1;
                        GeneroRechazos := True;
                    end;

                    i := i + 1;
                end;

                //si hubo vehiculos con errores genero un nuevo archivo con los rechazos
                if GeneroRechazos then begin
                    //Genero un nuevo nombre de Archivo que no Exista

                    FEvento := IMPORTACION_CON_RECHAZOS;

                    ArchivoRechazo := Trim(QueryGetValue(DMConnections.BaseCAC, 'Exec GenerarNombreArchivoRechazo ''' + spObtenerArchivosImportarVehiculos.FieldByName('Archivo').AsString + ''''));

                    if not Protegido then ExcelWorksheetVehiculos.Protect(PROTEGER_EXCEL);

                    if ArchivoRechazo = '' then begin
                        MsgBox(MSG_ERROR_PLANILLA_EXCEL, MSG_CAPTION_IMPORTAR_VEHICULOS, MB_ICONSTOP);
                        FEvento := IMPORTACION_ERRONEA;
                        FinalizarImportacion;
                        Screen.Cursor   := crDefault;
                        Exit;
                    end;

                    ExcelWorksheetVehiculos.SaveAs(FDirectorio + '\' + ArchivoRechazo);
                    spActualizarArchivoImportarVehiculos.Close;
                    spActualizarArchivoImportarVehiculos.Parameters.ParamByName('@Archivo').Value := ArchivoRechazo;
                    spActualizarArchivoImportarVehiculos.Parameters.ParamByName('@Tipo').Value    := ARCHIVO_RECHAZO;
                    spActualizarArchivoImportarVehiculos.Parameters.ParamByName('@FechaHora').Value := now;
                    spActualizarArchivoImportarVehiculos.Parameters.ParamByName('@Estado').Value  := ESTADO_A_CORREGIR;
                    spActualizarArchivoImportarVehiculos.ExecProc;
                    ActualizarEstadoArchivo(spObtenerArchivosImportarVehiculos.FieldByName('Archivo').AsString, ESTADO_IMPORTADO_CON_RECHAZOS, AGREGADOS_RECHAZADOS,'',ContadorVehiculosAgregados, ContadorVehiculosRechazados, f.CodigoSolicitud);
                end else
                    ActualizarEstadoArchivo(spObtenerArchivosImportarVehiculos.FieldByName('Archivo').AsString, ESTADO_IMPORTADO,TODO_INCORPORADO,'',ContadorVehiculosAgregados, f.CodigoSolicitud);
            end else begin if (f.ModalResult = mrCancel) then begin
                    Application.CreateForm(TFormObservacionesGeneral, fm);
                    fm.Inicializa;
                    //Application.CreateForm(TFormMotivoCancelacion, fm);
                    //fm.Inicializar;
                    FEvento := IMPORTACION_CANCELADA;
                    if (fm.ShowModal = mrOk) then
                        ActualizarEstadoArchivo(spObtenerArchivosImportarVehiculos.FieldByName('Archivo').AsString, ESTADO_CANCELADO, CANCELADO_USUARIO, fm.Observaciones);
                    fm.Release;
                end;
                f.Release;
            end;
        end;
        FinalizarImportacion;
        spObtenerArchivosImportarVehiculos.GotoBookmark(dondeEstaba);

        spObtenerArchivosImportarVehiculos.FreeBookmark(dondeEstaba);
        Screen.Cursor   := crDefault;
        dblArchivos.SetFocus;
end;

procedure TFormSeleccionArchivoImportar.FinalizarImportacion;
begin
    ExcelWorkbook.Close;
    ExcelWorksheetVehiculos.Disconnect;
    ExcelWorkbook.Disconnect;
    ExcelApplication.Disconnect;
    ObtenerArchivosImportacion(FOrdenActual);
    try
   	    CrearRegistroOperaciones(
        	    DMCOnnections.BaseCAC,
                SYS_CAC,
                RO_MOD_IMPORTACION_VEHICULOS,
    	  	    RO_IMPORTAR_VEHICULOS,
                SEV_INFO,
                GetMachineName,
                UsuarioSistema,
                FEvento,
    	  	    0,FCodigoSolicitud,0,FArchivo,
    	  	    '');
    except
        On E: Exception do begin
            MsgBoxErr(MSG_EXCEL_REGISTRO_OPERACIONES, e.message, MSG_CAPTION_IMPORTAR_VEHICULOS, MB_ICONSTOP);
            Screen.Cursor := crDefault;
            Exit;
        end;
    end;


end;

procedure TFormSeleccionArchivoImportar.LeerArchivos(Directorio: string);
resourcestring
    MSG_ERROR_GENERAR_TABLA_ARCHIVOS = 'No se pudo generar la tabla de archivos';
var
  I: Integer;
  MaskPtr: PChar;
  Ptr: PChar;
  AttrWord: Word;
  FileInfo: TSearchRec;
  SaveCursor: TCursor;

begin
    DMConnections.BaseCAC.BeginTrans;
    try
        AttrWord := DDL_READWRITE;

        ChDir(Directorio); //Voy al directorio especificado

        I := 0;
        SaveCursor := Screen.Cursor;
        try
          MaskPtr := PChar('*.xls');
          while MaskPtr <> nil do
          begin
            Ptr := StrScan (MaskPtr, ';');
            if Ptr <> nil then
              Ptr^ := #0;
            if FindFirst(MaskPtr, AttrWord, FileInfo) = 0 then
            begin
              repeat
                spActualizarArchivoImportarVehiculos.Close;
                spActualizarArchivoImportarVehiculos.Parameters.ParamByName('@Archivo').Value := FileInfo.Name;
                spActualizarArchivoImportarVehiculos.Parameters.ParamByName('@Estado').Value  := ESTADO_PENDIENTE;
                spActualizarArchivoImportarVehiculos.ExecProc;
                if I = 100 then
                  Screen.Cursor := crHourGlass;
              until FindNext(FileInfo) <> 0;
              FindClose(FileInfo);
            end;
            if Ptr <> nil then
            begin
              Ptr^ := ';';
              Inc (Ptr);
            end;
            MaskPtr := Ptr;
          end;
        finally
          Screen.Cursor := SaveCursor;
        end;
        QueryExecute(DMConnections.BaseCAC,'EXEC ActualizarArchivosImportacionBorrados');
        FOrdenActual := '1 ASC';
        ObtenerArchivosImportacion(FOrdenActual);
        except
        on E: exception do begin
            MsgBoxErr(MSG_ERROR_GENERAR_TABLA_ARCHIVOS, e.message, MSG_CAPTION_IMPORTAR_VEHICULOS, MB_ICONSTOP);
            DMConnections.BaseCAC.RollbackTrans;
            Exit;
        end;
    end;
    DMConnections.BaseCAC.CommitTrans;
end;

function TFormSeleccionArchivoImportar.Inicializar(CaptionAMostrar: string): boolean;
Var
	S: TSize;
begin
    Result := false;
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);
    self.Caption := CaptionAMostrar;
    ObtenerParametroGeneral(DMConnections.BaseCAC, Dir_Importacion_Vehiculos, FDirectorio);
    if not DirectoryExists(FDirectorio) then begin
        MsgBoxErr(MSG_ERROR_DIRECTORIO, MSG_ERROR_DIRECTORIO_ACCESO, MSG_CAPTION_IMPORTAR_VEHICULOS, MB_ICONSTOP);
        Exit;
    end;
    LeerArchivos(FDirectorio);
    dblArchivosClick(nil);
    Result := true;
end;

procedure TFormSeleccionArchivoImportar.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormSeleccionArchivoImportar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action:= caFree;
end;

procedure TFormSeleccionArchivoImportar.spObtenerArchivosImportarVehiculosAfterOpen(
  DataSet: TDataSet);
begin
    btnImportar.Enabled := (DataSet.Active) and (DataSet.RecordCount > 0) and (spObtenerArchivosImportarVehiculos.FieldByName('CodigoEstado').AsInteger = ESTADO_PENDIENTE);
end;

procedure TFormSeleccionArchivoImportar.dblArchivos1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    dblArchivosClick(nil);
end;

procedure TFormSeleccionArchivoImportar.ActualizarEstadoArchivo(Archivo: string; Estado: integer;
    CodigoResultado: integer = 0; Observaciones: string = '';ContadorVehiculosAgregados: integer = 0;
    ContadorVehiculosRechazados: integer = 0; CodigoSolicitud: integer = 0);

begin
    spActualizarEstadoArchivoImportarVehiculos.Close;
    spActualizarEstadoArchivoImportarVehiculos.Parameters.ParamByName('@Archivo').Value := Archivo;
    spActualizarEstadoArchivoImportarVehiculos.Parameters.ParamByName('@Observaciones').Value  := Observaciones;
    spActualizarEstadoArchivoImportarVehiculos.Parameters.ParamByName('@CodigoResultado').Value  := CodigoResultado;
    spActualizarEstadoArchivoImportarVehiculos.Parameters.ParamByName('@Estado').Value  := Estado;
    spActualizarEstadoArchivoImportarVehiculos.Parameters.ParamByName('@VehiculosAgregados').Value  := ContadorVehiculosAgregados;
    spActualizarEstadoArchivoImportarVehiculos.Parameters.ParamByName('@VehiculosRechazados').Value  := ContadorVehiculosRechazados;
    spActualizarEstadoArchivoImportarVehiculos.Parameters.ParamByName('@CodigoSolicitudContacto').Value  := CodigoSolicitud;
    spActualizarEstadoArchivoImportarVehiculos.ExecProc;
end;

procedure TFormSeleccionArchivoImportar.dblArchivosClick(Sender: TObject);
begin
    btnImportar.Enabled := spObtenerArchivosImportarVehiculos.FieldByName('CodigoEstado').AsInteger = ESTADO_PENDIENTE;
end;

procedure TFormSeleccionArchivoImportar.ObtenerArchivosImportacion(Orden: string);
begin
    spObtenerArchivosImportarVehiculos.Close;
    spObtenerArchivosImportarVehiculos.Parameters.ParamByName('@Orden').Value := Orden;
    spObtenerArchivosImportarVehiculos.Open;
end;

function TFormSeleccionArchivoImportar.AgregarVehiculoExcel(CodigoSolicitud: integer;
  Vehiculo: TVehiculos):boolean;
var
    TodoOk: Boolean;
begin
   try
        spActualizarVehiculosExcel.Close;
        spActualizarVehiculosExcel.Parameters.ParamByName('@CodigoMarca').Value := Vehiculo.CodigoMarca;
        spActualizarVehiculosExcel.Parameters.ParamByName('@Modelo').Value      := Vehiculo.Modelo;
        spActualizarVehiculosExcel.Parameters.ParamByName('@AnioVehiculo').Value:= strtoint(Vehiculo.Anio);
        spActualizarVehiculosExcel.Parameters.ParamByName('@CodigoTipoVehiculo').Value := Vehiculo.CodigoTipo;
        spActualizarVehiculosExcel.Parameters.ParamByName('@CodigoSolicitudContacto').Value := CodigoSolicitud;
        spActualizarVehiculosExcel.Parameters.ParamByName('@TipoPatente').Value := PATENTE_CHILE;
        spActualizarVehiculosExcel.Parameters.ParamByName('@Patente').Value := Vehiculo.Patente;
        spActualizarVehiculosExcel.ExecProc;
        TodoOk := True;
    except
        On E: Exception do begin
            MsgBoxErr(MSG_ERROR_AGREGAR_VEHICULO, E.message, MSG_CAPTION_IMPORTAR_VEHICULOS, MB_ICONSTOP);
            TodoOk := False;
            spActualizarVehiculosExcel.Close;
        end;
    end;
    result := TodoOk;
end;

procedure TFormSeleccionArchivoImportar.dblArchivosColumns4HeaderClick(
  Sender: TObject);
begin
    TDBListExColumn(sender).Sorting:=iif(TDBListExColumn(sender).Sorting=csAscending ,csDescending,csAscending);
    spObtenerArchivosImportarVehiculos.Sort := TDBListExColumn(sender).FieldName  + ' ' + iif(TDBListExColumn(sender).Sorting=csAscending ,'ASC','DESC');
end;

procedure TFormSeleccionArchivoImportar.dblArchivosDblClick(
  Sender: TObject);
begin
    if btnImportar.Enabled then btnImportar.Click;
end;

end.

