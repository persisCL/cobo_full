{---------------------------------------------------------------------------
            	frmNotaCreditoInfractor

Author: mbecerra
Date: 29-Julio-2009
Description:		(Ref. Facturaci�n Electr�nica)
        	Implementa la funcionalidad de anulaci�n de los aedocumentos
            electr�nicos con conceptos NI.

            Operatoria:
            1.-		La Nota de Cr�dito s�lo puede ser total
            2.-		El documento electr�nico queda anulado
            3.-		Las infraciones vuelven al estado anterior si el estado
            		actual es 4 (anulada) y si el motivo de la anulacion es
                    11 (facturado). Si alguna de las condiciones anteriores no
                    es cierta, entonces no se modifica el estado de la infracci�n.

Revision    : 2                                   
Author      : Nelson Droguett Sierra
Date        : 21-Septiembre-2009
Description : Se completa la generacion de la nota de credito.

Revision    : 3
Author      : Nelson Droguett Sierra
Date        : 24-Septiembre-2009
Description : 1.- En el formulario de Nota de credito Infractores mostrar Notas
                de Cobro en el dropdownlist (para consultas por numero de
                comprobantes internos).

----------------------------------------------------------------------------}
unit frmNotaCreditoInfractor;

interface

uses
	DMConnection,
    PeaTypes,
    PeaProcs,
    UtilProc,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, VariantComboBox, DB, ADODB, ComCtrls, ListBoxEx,
  DBListEx,
  // Rev.2 / 21-Septiembre-2009 / Nelson Droguett Sierra -----------------------
  UtilDB,
  Util,
  frmReporteNotaCreditoElectronica;
  //----------------------------------------------------------------------------

type
  TNotaCreditoInfractorForm = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    vcbTipoDocumento: TVariantComboBox;
    neNumeroDocumento: TNumericEdit;
    btnBuscar: TButton;
    btnSalir: TButton;
    Label6: TLabel;
    Label10: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
    lblFechaEmision: TLabel;
    lblFechaVencimiento: TLabel;
    lblTotalAPagar: TLabel;
    lblEstado: TLabel;
    lblRUT: TLabel;
    lblNumeroConvenio: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    btnGenerar: TButton;
    spComprobante: TADOStoredProc;
    DBListEx1: TDBListEx;
    DBListEx2: TDBListEx;
    spObtenerNumeroTipoInternoComprobanteFiscal: TADOStoredProc;
    StringField1: TStringField;
    LargeintField1: TLargeintField;
    StringField2: TStringField;
    WordField1: TWordField;
    spDetalle: TADOStoredProc;
    dsDetalle: TDataSource;
    spInfracciones: TADOStoredProc;
    dsInfracciones: TDataSource;
    spChequearUsuarioConcurrenteAcreditando: TADOStoredProc;
    spCrearProcesoFacturacion: TADOStoredProc;
    spAnularNotaCobro: TADOStoredProc;
    spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet: TADOStoredProc;
    StringField15: TStringField;
    StringField16: TStringField;
    IntegerField2: TIntegerField;
    StringField17: TStringField;
    LargeintField3: TLargeintField;
    DateTimeField5: TDateTimeField;
    DateTimeField6: TDateTimeField;
    DateTimeField7: TDateTimeField;
    DateTimeField8: TDateTimeField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    StringField26: TStringField;
    WordField2: TWordField;
    StringField27: TStringField;
    LargeintField4: TLargeintField;
    StringField28: TStringField;
    spCargarTransitoEnTablaTemporal: TADOStoredProc;
    spAgregarMovimientoCuentaTemporal: TADOStoredProc;
    spObtenerTransitosxInfraccion: TADOStoredProc;
    spReactivarInfraccionFacturar: TADOStoredProc;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBuscarClick(Sender: TObject);
    procedure vcbTipoDocumentoChange(Sender: TObject);
    procedure btnGenerarClick(Sender: TObject);
  private
    { Private declarations }
    // Rev.2 / 21-Septiembre-2009 / Nelson Droguett Sierra ---------------------
    FIdSesion : integer;
    FFechaProceso: TDateTime ;
    function  ChequearConcurrencia : boolean;
	procedure AnularNotaCobroInfractor;
    //--------------------------------------------------------------------------
  public
    { Public declarations }
    function Inicializar : boolean;
    procedure LimpiaDatos;
  end;

var
    NotaCreditoInfractorForm: TNotaCreditoInfractorForm;
    NumeroProcesoFacturacion : integer;
    TipoComprobante : string;
    NumeroComprobante : int64;



implementation

{$R *.dfm}

procedure TNotaCreditoInfractorForm.LimpiaDatos;
begin
    lblRUT.Caption := '';
    lblNombre.Caption := '';
    lblEstado.Caption := '';
    lblDomicilio.Caption := '';
    lblTotalAPagar.Caption := '';
    lblFechaEmision.Caption := '';
    lblNumeroConvenio.Caption := '';
    lblFechaVencimiento.Caption := '';
end;


procedure TNotaCreditoInfractorForm.vcbTipoDocumentoChange(Sender: TObject);
begin
	if ActiveControl = vcbTipoDocumento then LimpiaDatos();
end;

{-----------------------------------------------------------------------
        	btnBuscarClick

Author: mbecerra
Date: 04-Agosto-2009
Description:	Busca los datos del comprobante ingresado, inclu�das las infracciones
				facturadas.
------------------------------------------------------------------------}
procedure TNotaCreditoInfractorForm.AnularNotaCobroInfractor;
resourcestring
	MSG_ERROR 				  = 'Error';
	MSG_SUCCESS				  = 'El comprobante %d se proces� correctamente.'#10#13 + 'Se gener� la Nota de Cr�dito %d a Nota de Cobro';
	MSG_ERROR_VOIDING_INVOICE = 'Error generando la nota de cr�dito';
	MSG_CONFIRM				  = 'Desea generar la nota de cr�dito ?';
	MSG_CAPTION 			  = 'Anular Nota de Cobro';
	MSG_IMPRIMIR              = 'Desea imprimir la N�ta de Cr�dito ?';
    MSG_DBNET					= 'Error al insertar registro en la Cola de Env�o a DBNet';
var
	sDescError : string;
	nNotaCredito : Int64;
	index: integer;
    f: TReporteNotaCreditoElectronicaForm;
    NotaCreditoFiscal: int64;
begin
	//consulto al operador si esta seguro de realizar la operacion
	if MsgBox(MSG_CONFIRM, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Exit;
    if ChequearConcurrencia then Exit; //si otro usuario esta haciendo una CK de esta NK
    try
        DMConnections.BaseCAC.Execute('BEGIN TRAN spAnulacionNotaCobroInfractor');
		//ahora hay que crear los movimientos con los cargos
		spInfracciones.First;
		while not spInfracciones.Eof do begin
            // OBTIENE LOS TRANSITOS PARA ESTA INFRACCION
            spObtenerTransitosxInfraccion.Close;
            with spObtenerTransitosxInfraccion do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoInfraccion').Value := spInfracciones.FieldByName('CodigoInfraccion').AsInteger;
                Open;
            end;
            // CON ESTOS TRANSITOS DE LA INFRACCION, CARGA LA TEMPORAL DE TRANSITOS
            //Cargar tabla temporal de transitos, con los transitos asociados a la infraccion
            spObtenerTransitosXInfraccion.First;
            while not spObtenerTransitosxInfraccion.Eof do begin
                spCargarTransitoEnTablaTemporal.Parameters.Refresh;
                spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@IdSesion'          ).Value := FIdSesion ;
                spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@TipoComprobante'   ).Value := TipoComprobante;//cbTipoComprobante.Value;
                spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@NumeroComprobante' ).Value := NumeroComprobante;//edNumeroComprobante.Text;
                spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@FechaProcesamiento').Value := FFechaProceso;
                spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@Usuario'           ).Value := UsuarioSistema;
                spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@NumCorrCA'         ).Value := spObtenerTransitosxInfraccion.FieldByName('NumCorrCA').AsInteger;
                spCargarTransitoEnTablaTemporal.ExecProc;
                spObtenerTransitosxInfraccion.Next;
            end;
            // REACTIVO LAS INFRACCIONES, PARA QUE SE PUEDAN VOLVER A PROCESAR
            spReactivarInfraccionFacturar.Close;
            spReactivarInfraccionFacturar.Parameters.Refresh;
            spReactivarInfraccionFacturar.Parameters.ParamByName('@CodigoInfraccion').Value := spInfracciones.FieldByName('CodigoInfraccion').AsInteger;
            spReactivarInfraccionFacturar.ExecProc;
            //------------------------------------------------------------------------
    		spInfracciones.Next;
		end;

        spDetalle.First;
        while not spDetalle.Eof do begin
			spAgregarMovimientoCuentaTemporal.Parameters.Refresh;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConvenio'      ).Value := spComprobante.FieldByName('CodigoConvenio').AsInteger;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IndiceVehiculo'      ).Value := NULL;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaHora'           ).Value := NowBase(DMConnections.BaseCAC);
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConcepto'      ).Value := spDetalle.FieldByName('ConceptoAnula').AsInteger;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Importe'             ).Value := spDetalle.FieldByName('Importe').AsFloat * 100 ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@ImporteIVA'          ).Value := null ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@PorcentajeIVA'       ).Value := null ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@EsPago'              ).Value := False;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Observaciones'       ).Value := spDetalle.FieldByName('DescripcionConceptoAnula').AsString;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroPromocion'     ).Value := null;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroFinanciamiento').Value := null;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IdSesion'            ).Value := FIdSesion ;
 			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@TipoComprobante'     ).Value := TipoComprobante;//cbTipoComprobante.Value;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroComprobante'   ).Value := NumeroComprobante;//edNumeroComprobante.Text;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaProcesamiento'  ).Value := FFechaProceso;
            spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Usuario'             ).Value := UsuarioSistema;
            spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Seleccionado'        ).Value := False;
            spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroMovimiento'    ).Value := 0;
			spAgregarMovimientoCuentaTemporal.ExecProc;
            spDetalle.Next;
        end;

        // agrego generacion de proceso de facturacion
        with spCrearProcesoFacturacion do begin
        	Parameters.ParamByName('@Operador').Value := UsuarioSistema;
            Parameters.ParamByName('@FacturacionManual').Value := True;
            ExecProc;
        	NumeroProcesoFacturacion := Parameters.ParamByName('@NumeroProcesoFacturacion').Value;
        end;

		with spAnularNotaCobro do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
            Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
            Parameters.ParamByName('@PasarAEstadoEspecial').Value := False;
            Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
            Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
            Parameters.ParamByName('@NumeroNotaCredito').Value := NULL;
            Parameters.ParamByName('@DescripcionError').Value := NULL;
            Parameters.ParamByName('@NumeroNotaCreditoFiscal').Value := NULL;
            Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value := NULL;
            Parameters.ParamByName('@AnulaNotaCobroInfractora').Value := True;
            ExecProc;
            sDescError := Trim(Parameters.ParamByName('@DescripcionError').Value);
		end;
		//Verifico si hubo error al anular
		if (sDescError <> '') then begin
        	//informo que hubo un error al anular
			MsgBoxErr(MSG_ERROR, sDescError, MSG_ERROR, MB_ICONERROR);
        	// si hay un error en el SP hace rollback
            DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAnulacionNotaCobroInfractor');
		end
        else begin
        	//inserto en la cola si la Nota credito es fiscal (electronica)
            if spAnularNotaCobro.Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value = true then begin
		        with spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet do begin
                	Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
                	ExecProc;
                	Index := Parameters.ParamByName('@RETURN_VALUE').Value;
                    if Index < 0 then begin
        				MsgBox(MSG_DBNET, Caption, MB_ICONERROR);
                        DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAnulacionNotaCobroInfractor');
                        Exit;
					end;
            	end;
         	end;
            DMConnections.BaseCAC.Execute ('COMMIT TRAN spAnulacionNotaCobroInfractor');
        	nNotaCredito := spAnularNotaCobro.Parameters.ParamByName('@NumeroNotaCredito').Value;
        	NotaCreditoFiscal := spAnularNotaCobro.Parameters.ParamByName('@NumeroNotaCreditoFiscal').Value;
			 //informo que se anulo con exito
          	MsgBox( Format(MSG_SUCCESS, [neNumeroDocumento.ValueInt, NotaCreditoFiscal]), Caption, MB_ICONINFORMATION);

			if (MsgBox( Format(MSG_IMPRIMIR,[NumeroComprobante, nNotaCredito]), Caption, MB_YESNO + MB_ICONQUESTION) = ID_YES) then begin
            	//REV 11: si la nota de credito es fiscal (electronica), imprimo con el formulario para comprobantes electronicos
                f:= TReporteNotaCreditoElectronicaForm.Create(nil);
                try
                    if not f.Inicializar(DMConnections.BaseCAC,TC_NOTA_CREDITO_A_COBRO,nNotaCredito,sDescError) then
                        ShowMessage( sDescError )
                    else
                        f.Ejecutar;
                finally
                    if Assigned(f) then f.Release;

                end;
			end;
		end;
    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAnulacionNotaCobroInfractor');
        end;
    end;
    LimpiaDatos();
end;

procedure TNotaCreditoInfractorForm.btnBuscarClick(Sender: TObject);
resourcestring
	MSG_DOCUMENTO	= 'No ha seleccionado el Tipo de Documento electr�nico';
    MSG_NUMERO		= 'No ha ingresado el n�mero de documento electr�nico';

    MSG_ERROR = 'Ocurri� un error al buscar el comprobante';
    MSG_NODOC = 'No se encontr� el documento indicado';
    MSG_NODET = 'No se encontr� el detalle Comprobante';
    MSG_NOINF = 'No se encontraron Infracciones anuladas para el comprobante';

begin
	if not ValidateControls([vcbTipoDocumento, neNumeroDocumento],
    						[vcbTipoDocumento.ItemIndex >= 0, neNumeroDocumento.ValueInt > 0],
                            Caption,
                            [MSG_DOCUMENTO, MSG_NUMERO] ) then Exit;


    try
        // Rev.3 / 24-Septiembre-2009 / Nelson Droguett Sierra ------------------------
        if vcbTipoDocumento.Value=TC_NOTA_COBRO then
        begin
          TipoComprobante := vcbTipoDocumento.Value;
          NumeroComprobante := neNumeroDocumento.ValueInt;
        end
        else
        begin
          //Buscar el comprobante interno
          spObtenerNumeroTipoInternoComprobanteFiscal.Close;
          with spObtenerNumeroTipoInternoComprobanteFiscal do begin
              Parameters.Refresh;
              Parameters.ParamByName('@TipoComprobanteFiscal').Value := vcbTipoDocumento.Value;
              Parameters.ParamByName('@NumeroComprobanteFiscal').Value := neNumeroDocumento.ValueInt;
              Parameters.ParamByName('@TipoComprobante').Value := NULL;
              Parameters.ParamByName('@NumeroComprobante').Value := NULL;
              ExecProc;
              TipoComprobante := Parameters.ParamByName('@TipoComprobante').Value;
              NumeroComprobante := Parameters.ParamByName('@NumeroComprobante').Value;
          end;
        end;
        if (TipoComprobante = '') or (NumeroComprobante = 0) then begin
            MsgBox(MSG_NODOC, Caption, MB_ICONEXCLAMATION);
            Exit;
        end;
        //----------------------------------------------------------------------------- Rev.3

        spComprobante.Close;
        with spComprobante do begin
        	Parameters.Refresh;
            Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
            Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
            Open;
        	//Mostrar los datos del comprobante
            if not IsEmpty then begin
            	lblRUT.Caption				:= FieldByName('RutCliente').AsString;
                lblNombre.Caption			:= FieldByName('NombreCliente').AsString;
                lblDomicilio.Caption 		:= FieldByName('Domicilio').AsString + ' ' + FieldByName('Comuna').AsString;
                lblFechaEmision.Caption 	:= FormatDateTime('dd-mm-yyyy', FieldByName('FechaEmision').AsDateTime);
                lblFechaVencimiento.Caption := FormatDateTime('dd-mm-yyyy', FieldByName('FechaVencimiento').AsDateTime);
                lblNumeroConvenio.Caption 	:= FieldByName('NumeroConvenioFormateado').AsString;
                lblTotalAPagar.Caption		:= FieldByName('TotalComprobante').AsString;
                lblEstado.Caption			:= DescriEstadoComprobante(FieldByName('EstadoPago').AsString);
            end
            else MsgBox(MSG_NODOC, Caption, MB_ICONEXCLAMATION);
        end;

        if spComprobante.IsEmpty then spComprobante.Close
        else begin
        	//Desplegar los conceptos facturados
            spDetalle.Close;
            with spDetalle do begin
                Parameters.Refresh;
                Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
            	Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
                Open;
                if IsEmpty then MsgBox(MSG_NODET, Caption, MB_ICONEXCLAMATION);
            end;

            //Desplegar las infracciones
            spInfracciones.Close;
            with spInfracciones do begin
                Parameters.Refresh;
                Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
            	Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
                Parameters.ParamByName('@Total').Value := NULL;
                Parameters.ParamByName('@Cantidad').Value := NULL;
                Open;
                if IsEmpty then
                begin
                    MsgBox(MSG_NOINF, Caption, MB_ICONEXCLAMATION);
                    // Rev.2 / 23-Septiembre-2009 / Nelson Droguett Sierra ---------------------------------------------------------
                    btnGenerar.Enabled:=False;
                    // -------------------------------------------------------------------------------------------------------------
                end
                else
                begin
                  // Rev.2 / 23-Septiembre-2009 / Nelson Droguett Sierra ---------------------------------------------------------
                  btnGenerar.Enabled:=True;
                  // -------------------------------------------------------------------------------------------------------------
                end;
            end;
        end;
    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;
end;

{-----------------------------------------------------------------------
        	btnSalirClick

Author: mbecerra
Date: 04-Agosto-2009
Description:	Llama al procedure que cierra el formulario.
------------------------------------------------------------------------}
procedure TNotaCreditoInfractorForm.btnGenerarClick(Sender: TObject);
begin
	AnularNotaCobroInfractor;
    Inicializar;
end;

procedure TNotaCreditoInfractorForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

function TNotaCreditoInfractorForm.ChequearConcurrencia: boolean;
resourcestring
	MSG_NUMERO_INVALIDO = 'N�mero de comprobante inv�lido';
    ERROR_COMPROBANTE_SIENDO_ANULADO = 'El comprobante est� siendo anulado por el Usuario %s el d�a %s';
var
    OtroUsuario: string;
    FechaOtroUsuario: TDateTime;
begin
    //Chequeamos si otro usuario est� modificando este comprobante en la tabla de MovimientosTemporales

    spChequearUsuarioConcurrenteAcreditando.Parameters.Refresh;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@TipoComprobante').Value    := vcbTipoDocumento.Value ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@NumeroComprobante').Value  := neNumeroDocumento.ValueInt ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@NumeroDeSesion').Value     := FIdSesion ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@OtroUsuario').Value        := null ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@FechaOtroUsuario').Value   := null ;
    spChequearUsuarioConcurrenteAcreditando.ExecProc;


    if spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@OtroUsuario').Value <> null then
        OtroUsuario := spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@OtroUsuario').Value
    else OtroUsuario := '';

    if spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@FechaOtroUsuario').Value <> null then
        FechaOtroUsuario := spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@FechaOtroUsuario').Value
    else FechaOtroUsuario := 0;

    Result := False;
    if OtroUsuario <> '' then begin
        MsgBoxErr(Format(ERROR_COMPROBANTE_SIENDO_ANULADO,[OtroUsuario,DateTimeToStr(FechaOtroUsuario)] ), caption, caption, MB_ICONERROR);
        Result := True;
        Exit;
    end;

end;

{-----------------------------------------------------------------------
        	FormClose

Author: mbecerra
Date: 04-Agosto-2009
Description:	Cierra el formulario de Nota Cr�dito Infractor.
------------------------------------------------------------------------}
procedure TNotaCreditoInfractorForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

{-----------------------------------------------------------------------
        	Inicializar

Author: mbecerra
Date: 04-Agosto-2009
Description:	Inicializa el formulario de Nota Cr�dito Infractor.
------------------------------------------------------------------------}
function TNotaCreditoInfractorForm.Inicializar;
resourcestring
	MSG_CAPTION = 'Emisi�n de Nota de Cr�dito a Documentos con Infracci�n';
    
begin
    // Rev.2 / 21-Septiembre-2009 / Nelson Droguett Sierra ---------------------------------------------------------
    FIdSesion :=  QueryGetValueInt(DMConnections.BaseCAC,'SELECT @@SPID');
    FFechaProceso := NowBase( DMConnections.BaseCAC ) ;
    dsDetalle.DataSet.Close;
    dsInfracciones.DataSet.Close;
    btnGenerar.Enabled:=False;
    //--------------------------------------------------------------------------------------------------------------
	CenterForm(Self);
    LimpiaDatos();
    //Cargar la combobox con los documentos electr�nicos
    with vcbTipoDocumento do begin
        Clear;
        // Rev.3 / 24-Septiembre-2009 / Nelson Droguett Sierra -----------------------
        Items.Add(DescriTipoComprobante(TC_NOTA_COBRO), TC_NOTA_COBRO);
        //----------------------------------------------------------------------------
        Items.Add(DescriTipoComprobante(TC_BOLETA_AFECTA), TC_BOLETA_AFECTA);
        Items.Add(DescriTipoComprobante(TC_BOLETA_EXENTA), TC_BOLETA_EXENTA);
        Items.Add(DescriTipoComprobante(TC_FACTURA_AFECTA), TC_FACTURA_AFECTA);
        Items.Add(DescriTipoComprobante(TC_FACTURA_EXENTA), TC_FACTURA_EXENTA);
    end;

    Caption := MSG_CAPTION;
    neNumeroDocumento.Value := 0;
    vcbTipoDocumento.SetFocus;
end;

end.
