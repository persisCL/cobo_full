object MandatosRechazados: TMandatosRechazados
  Left = 86
  Top = 224
  BorderStyle = bsSingle
  Caption = 'Gestion de Mandatos Rechazados'
  ClientHeight = 466
  ClientWidth = 855
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  DesignSize = (
    855
    466)
  PixelsPerInch = 96
  TextHeight = 13
  object dblPendientes: TDBListEx
    Left = 1
    Top = 66
    Width = 853
    Height = 297
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 200
        Header.Caption = 'Banco del Debito'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NombreBanco'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 125
        Header.Caption = 'Nro. de Cuenta'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NroCuentaBancaria'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 110
        Header.Caption = 'Fecha Respuesta'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaRespuesta'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Estado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescriRespuesta'
      end>
    DataSource = dsMandatosPendientes
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
  end
  object Panel1: TPanel
    Left = 1
    Top = 366
    Width = 852
    Height = 64
    Anchors = [akLeft, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 2
    object lObservaciones: TLabel
      Left = 6
      Top = 4
      Width = 74
      Height = 13
      Caption = 'Observaciones:'
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 20
      Width = 673
      Height = 38
      AutoSize = False
      BorderStyle = bsNone
      Color = clBtnFace
      DataField = 'Observaciones'
      DataSource = dsMandatosPendientes
      ReadOnly = True
      TabOrder = 0
    end
  end
  object GroupBox4: TGroupBox
    Left = 6
    Top = 2
    Width = 433
    Height = 59
    Caption = ' Por Cliente / Cuenta '
    TabOrder = 6
    object Label1: TLabel
      Left = 9
      Top = 16
      Width = 78
      Height = 13
      Caption = 'N'#250'mero de RUT'
    end
    object Label3: TLabel
      Left = 217
      Top = 17
      Width = 45
      Height = 13
      Caption = 'Convenio'
    end
    object peNumeroDocumento: TPickEdit
      Left = 9
      Top = 30
      Width = 200
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 0
      OnChange = peNumeroDocumentoChange
      Decimals = 0
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object cbConvenio: TVariantComboBox
      Left = 216
      Top = 30
      Width = 203
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbConvenioChange
      Items = <>
    end
  end
  object grbDatosCliente: TGroupBox
    Left = 444
    Top = 2
    Width = 405
    Height = 61
    Caption = ' Datos del Cliente '
    TabOrder = 7
    object Label2: TLabel
      Left = 8
      Top = 16
      Width = 46
      Height = 13
      Caption = 'Nombre : '
    end
    object Label4: TLabel
      Left = 8
      Top = 35
      Width = 46
      Height = 13
      Caption = 'Apellido : '
    end
    object lNombre: TLabel
      Left = 56
      Top = 16
      Width = 37
      Height = 13
      Caption = 'Nombre'
    end
    object lApellido: TLabel
      Left = 56
      Top = 35
      Width = 37
      Height = 13
      Caption = 'Apellido'
    end
  end
  object btnSalir: TButton
    Left = 761
    Top = 434
    Width = 85
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = 'Salir'
    TabOrder = 0
    OnClick = btnSalirClick
  end
  object btnMarcarRechazado: TButton
    Left = 654
    Top = 435
    Width = 100
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Informar Rechazo'
    TabOrder = 3
    OnClick = btnMarcarRechazadoClick
  end
  object btnMarcarResuelto: TButton
    Left = 482
    Top = 435
    Width = 80
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Resuelto'
    TabOrder = 4
    OnClick = btnMarcarResueltoClick
  end
  object btnMarcarNoResuelto: TButton
    Left = 569
    Top = 435
    Width = 80
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'No Resuelto'
    TabOrder = 5
    OnClick = btnMarcarNoResueltoClick
  end
  object dsMandatosPendientes: TDataSource
    DataSet = spMandatos
    Left = 160
    Top = 120
  end
  object spMandatos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterScroll = spMandatosAfterScroll
    ProcedureName = 'ObtenerMandatosNoAceptados;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 200
    Top = 120
    object spMandatosCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object spMandatosNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      FixedChar = True
      Size = 30
    end
    object spMandatosNumeroConvenioFormateado: TStringField
      FieldName = 'NumeroConvenioFormateado'
      ReadOnly = True
      FixedChar = True
      Size = 19
    end
    object spMandatosFechaAccion: TDateTimeField
      FieldName = 'FechaAccion'
    end
    object spMandatosCodigoLote: TIntegerField
      FieldName = 'CodigoLote'
    end
    object spMandatosCodigoAccionSantander: TSmallintField
      FieldName = 'CodigoAccionSantander'
    end
    object spMandatosCodigoTipoCuentaBancaria: TWordField
      FieldName = 'CodigoTipoCuentaBancaria'
    end
    object spMandatosCodigoBanco: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object spMandatosNombreBanco: TStringField
      FieldName = 'NombreBanco'
      Size = 50
    end
    object spMandatosNroCuentaBancaria: TStringField
      FieldName = 'NroCuentaBancaria'
      FixedChar = True
      Size = 30
    end
    object spMandatosCodigoRespuestaSantander: TSmallintField
      FieldName = 'CodigoRespuestaSantander'
    end
    object spMandatosFechaRespuesta: TDateTimeField
      FieldName = 'FechaRespuesta'
    end
    object spMandatosObservaciones: TStringField
      FieldName = 'Observaciones'
      Size = 50
    end
    object spMandatosUsuario: TStringField
      FieldName = 'Usuario'
      FixedChar = True
    end
    object spMandatosEnviadoAlCAC: TBooleanField
      FieldName = 'EnviadoAlCAC'
    end
    object spMandatosDescriRespuesta: TStringField
      FieldName = 'DescriRespuesta'
      Size = 50
    end
  end
  object spCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = 'RUT'
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 112
    Top = 120
    object spClienteCodigoDocumento: TStringField
      FieldName = 'CodigoDocumento'
      FixedChar = True
      Size = 4
    end
    object spClienteNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
    object spClienteCodigoCliente: TAutoIncField
      FieldName = 'CodigoCliente'
      ReadOnly = True
    end
    object spClienteApellido: TStringField
      FieldName = 'Apellido'
      ReadOnly = True
      FixedChar = True
      Size = 60
    end
    object spClienteApellidoMaterno: TStringField
      FieldName = 'ApellidoMaterno'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object spClienteNombre: TStringField
      FieldName = 'Nombre'
      ReadOnly = True
      FixedChar = True
      Size = 60
    end
  end
end
