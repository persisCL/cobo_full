{
     
Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

}


unit ConfigurarPuesto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DeclTag, DeclHard,  Util, UtilProc,
  DPSControls;

type
  TFormConfigurarPuesto = class(TForm)
    gb_Antena: TGroupBox;
    Label1: TLabel;
    txt_bibLIC: TEdit;
    LIC: TTagReader;
    btn_setupLIC: TDPSButton;
    btn_Aceptar: TDPSButton;
    btn_Cancelar: TDPSButton;
    procedure btn_setupLICClick(Sender: TObject);
	procedure btn_AceptarClick(Sender: TObject);
  private
	{ Private declarations }
  public
	{ Public declarations }
	Function Inicializa: Boolean;
  end;

var
  FormConfigurarPuesto: TFormConfigurarPuesto;

implementation

{$R *.DFM}

resourcestring
    CAPTION_CONFIGURAR = 'Configuraci�n de Puesto de Trabajo';

{ TForm1 }

function TFormConfigurarPuesto.Inicializa: Boolean;
begin
    { INICIO : 20160315 MGO
	LIC.DLL 				:= ApplicationIni.ReadString('Bibliotecas', 'LIC', LIC.DLL);
	}
    LIC.DLL 				:= InstallIni.ReadString('Bibliotecas', 'LIC', LIC.DLL);
    // FIN : 20160315 MGO
    Screen.Cursor 		  	:= crHourGlass;
	btn_setupLIC.Enabled	:= False;//LIC.CanSetup;
    Screen.Cursor   		:= crDefault;
	txt_bibLIC.text 		:= LIC.DLL;
	Result := True;
end;

procedure TFormConfigurarPuesto.btn_setupLICClick(Sender: TObject);
resourcestring
    MSG_REINICIAR = 'Los cambios tendr�n efecto cuando reinicie el programa.';
begin
	if LIC.Setup then MsgBox(MSG_REINICIAR, CAPTION_CONFIGURAR, MB_ICONWARNING);
end;

procedure TFormConfigurarPuesto.btn_AceptarClick(Sender: TObject);
resourcestring
    MSG_CERRAR_TURNO = 'Deber� cerrar el turno y reingresar en el sistema.';
begin
    { INICIO : 20160315 MGO
	ApplicationIni.WriteString('Bibliotecas', 'LIC', txt_bibLIC.text);
    }
    InstallIni.WriteString('Bibliotecas', 'LIC', txt_bibLIC.text);
    // FIN : 20160315 MGO
    MsgBox(MSG_CERRAR_TURNO, CAPTION_CONFIGURAR, MB_ICONINFORMATION);
    ModalResult := mrOk;
end;

end.
