object frmActivarCuenta: TfrmActivarCuenta
  Left = 372
  Top = 157
  BorderStyle = bsDialog
  Caption = 'frmActivarCuenta'
  ClientHeight = 527
  ClientWidth = 430
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 496
    Width = 430
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object btn_Aceptar: TButton
      Left = 269
      Top = 3
      Width = 75
      Height = 25
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btn_AceptarClick
    end
    object btn_Cancelar: TButton
      Left = 349
      Top = 3
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btn_CancelarClick
    end
  end
  object cb_VehiculoRobado: TCheckBox
    Left = 16
    Top = 226
    Width = 114
    Height = 17
    Caption = '&Vehiculo Robado'
    TabOrder = 1
    Visible = False
  end
  object gbModalidadEntregaTelevia: TGroupBox
    Left = 0
    Top = 240
    Width = 430
    Height = 33
    Align = alTop
    TabOrder = 2
    Visible = False
    object Label1: TLabel
      Left = 28
      Top = 12
      Width = 160
      Height = 13
      Caption = 'Modalidad de entrega del Telev'#237'a'
    end
    object lblModalidadEntregaTelevia: TLabel
      Left = 204
      Top = 12
      Width = 158
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'lblModalidadEntregaTelevia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object gbIndemnizaciones: TGroupBox
    Left = 0
    Top = 273
    Width = 430
    Height = 209
    Align = alTop
    TabOrder = 3
    object Bevel1: TBevel
      Left = 12
      Top = 78
      Width = 407
      Height = 23
    end
    object lblSeleccionar: TLabel
      Left = 349
      Top = 82
      Width = 22
      Height = 13
      Cursor = crHandPoint
      Caption = 'ac'#225
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      OnClick = lblSeleccionarClick
    end
    object Label10: TLabel
      Left = 89
      Top = 82
      Width = 254
      Height = 13
      Caption = 'Para seleccionar un motivo de facturaci'#243'n haga click '
    end
    object lblTotalMovimientosTxt: TLabel
      Left = 250
      Top = 182
      Width = 98
      Height = 13
      AutoSize = False
      Caption = 'Total Movimientos :'
    end
    object lblTotalMovimientos: TLabel
      Left = 341
      Top = 182
      Width = 78
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
    end
    object dblConceptos: TDBListEx
      Left = 12
      Top = 107
      Width = 409
      Height = 77
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 60
          Header.Caption = 'Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'CodigoConcepto'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 250
          Header.Caption = 'Descripci'#243'n de Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescripcionMotivo'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'Precio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'PrecioEnPesos'
        end>
      DataSource = dsConceptos
      DragReorder = True
      ParentColor = False
      PopupMenu = mnuGrillaConceptos
      TabOrder = 1
      TabStop = True
      OnContextPopup = dblConceptosContextPopup
      OnKeyDown = dblConceptosKeyDown
    end
    object rgGarantiaTag: TRadioGroup
      Left = 243
      Top = 14
      Width = 184
      Height = 58
      Caption = '  Modalidad C'#225'lculo de Garant'#237'a '
      Columns = 2
      Enabled = False
      ItemIndex = 1
      Items.Strings = (
        'Nuevo'
        'Transferencia')
      TabOrder = 0
      OnClick = LimpiarDataSet
    end
    object rgModalidadEntregaTag: TRadioGroup
      Left = 3
      Top = 14
      Width = 234
      Height = 58
      Caption = '  Modalidad de Entrega del Telev'#237'a '
      Columns = 2
      Enabled = False
      ItemIndex = 2
      Items.Strings = (
        'Comodato'
        'Arriendo'
        'Arriendo en Cuotas')
      TabOrder = 2
      OnClick = LimpiarDataSet
    end
  end
  object GroupBox: TGroupBox
    Left = 0
    Top = 0
    Width = 430
    Height = 240
    Align = alTop
    Caption = ' Datos del Veh'#237'culo '
    TabOrder = 4
    inline freDatosVehiculo: TFmeSCDatosVehiculo
      Left = 5
      Top = 18
      Width = 419
      Height = 215
      HorzScrollBar.Visible = False
      VertScrollBar.Visible = False
      TabOrder = 0
      TabStop = True
      ExplicitLeft = 5
      ExplicitTop = 18
      ExplicitWidth = 419
      ExplicitHeight = 215
      inherited lblVehiculoRobado: TLabel
        Left = 234
        Width = 181
        ExplicitLeft = 234
        ExplicitWidth = 98
      end
      inherited txt_anio: TNumericEdit
        OnExit = frmVehiculotxt_anioExit
      end
      inherited txtDescripcionModelo: TEdit
        OnKeyPress = frmVehiculotxtDescripcionModeloKeyPress
      end
      inherited txtPatente: TEdit
        OnChange = frmVehiculotxtPatenteChange
        OnEnter = frmVehiculotxtPatenteEnter
        OnExit = frmVehiculotxtPatenteExit
        OnKeyPress = frmVehiculotxtPatenteKeyPress
      end
      inherited txtDigitoVerificador: TEdit
        OnKeyPress = frmVehiculotxtDigitoVerificadorKeyPress
      end
      inherited txtNumeroTelevia: TEdit
        OnChange = frmVehiculotxtNumeroTeleviaChange
        OnExit = frmVehiculotxtNumeroTeleviaExit
        OnKeyPress = frmVehiculotxtNumeroTeleviaKeyPress
      end
    end
  end
  object cdConceptos: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Seleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionMotivo'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'PrecioOrigen'
        DataType = ftInteger
      end
      item
        Name = 'Moneda'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Cotizacion'
        DataType = ftFloat
      end
      item
        Name = 'PrecioEnPesos'
        DataType = ftInteger
      end
      item
        Name = 'Comentario'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'IDMotivoMovCuentaTelevia'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 96
    Top = 408
    Data = {
      050100009619E0BD01000000180000000900000000000300000005010C53656C
      656363696F6E61646F02000300000000000E436F6469676F436F6E636570746F
      0400010000000000114465736372697063696F6E4D6F7469766F020049000000
      010005574944544802000200FF000C50726563696F4F726967656E0400010000
      000000064D6F6E656461020049000000010005574944544802000200FF000A43
      6F74697A6163696F6E08000400000000000D50726563696F456E5065736F7304
      000100000000000A436F6D656E746172696F0200490000000100055749445448
      02000200FF001849444D6F7469766F4D6F764375656E746154656C6576696104
      000100000000000000}
  end
  object dsConceptos: TDataSource
    DataSet = cdConceptos
    Left = 192
    Top = 408
  end
  object mnuGrillaConceptos: TPopupMenu
    Left = 280
    Top = 412
    object mnuEliminarConcepto: TMenuItem
      Caption = 'Eliminar Concepto'
      OnClick = mnuEliminarConceptoClick
    end
    object AgregarConceptos1: TMenuItem
      Caption = 'Agregar Conceptos'
      OnClick = AgregarConceptos1Click
    end
  end
end
