unit FrmMsgNormalizar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFormMsgNormalizar = class(TForm)
    lblMensaje: TLabel;
    btnNormalizar: TButton;
    btnContinuar: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializa(Caption: TCaption): Boolean;
  end;

var
  FormMsgNormalizar: TFormMsgNormalizar;

implementation

{$R *.dfm}

{ TFormMsgNormalizar }

function TFormMsgNormalizar.Inicializa(Caption: TCaption): Boolean;
begin
    Self.Caption := Caption;
    Result := True;
end;

end.
