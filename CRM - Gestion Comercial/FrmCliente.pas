unit FrmCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Navigator, StdCtrls, ExtCtrls, Buttons, DmiCtrls, validate,
  Dateedit, DB, ADODB, UtilDB, Util, DBTables,
  UtilProc, PeaProcs, FrmDatosReclamos, ComCtrls, Grids,
  DBGrids, DPSGrid, DPSPageControl, peaTypes, math, WorkFlowComp, FrmMantenimientoCuenta,
  DetalleComprobante, BuscaClientes,  DetalleInfraccion,
  DetalleViaje, detallePago, frmWrkDisplay, Gauges, MaskCombo, frmUltimaFactura,
  DPSControls, ListBoxEx, DBListEx;

type
  TNavWindowCliente = class(TNavWindowFrm)
    ObtenerComunicaciones: TADOStoredProc;
    Panel2: TPanel;
    Image1: TImage;
    Image2: TImage;
    Panel1: TPanel;
    Panel8: TPanel;
	Panel6: TPanel;
    PageControl: TDPSPageControl;
    ts_Inicio: TTabSheet;
	ts_cartola: TTabSheet;
	ds_Cuentas: TDataSource;
    ObtenerDetallesCuentasCliente: TADOStoredProc;
	ds_Comunicaciones: TDataSource;
    ts_Contactos: TTabSheet;
    ts_Viajes: TTabSheet;
    ts_Facturas: TTabSheet;
    ts_Pagos: TTabSheet;
    ts_refacturacion: TTabSheet;
    ts_financiamiento: TTabSheet;
	ts_infracciones: TTabSheet;
    ts_tag: TTabSheet;
    ObtenerFinanciamientos: TADOStoredProc;
	dsFinanciamientos: TDataSource;
    ObtenerListaComprobantes: TADOStoredProc;
	dsFacturas: TDataSource;
    ObtenerRefacturacion: TADOStoredProc;
    dsRefacturacion: TDataSource;
	ObtenerResumenInfracciones: TADOStoredProc;
	dsInfracciones: TDataSource;
    dsViajes: TDataSource;
    ObtenerViajesCliente: TADOStoredProc;
    ObtenerResumenMovimientosCliente: TADOStoredProc;
    dsResumenMovimientosCliente: TDataSource;
    dsPagosComprobantes: TDataSource;
    ObtenerPagosComprobantes: TADOStoredProc;
	dsResumenCliente: TDataSource;
	ObtenerResumenCliente: TADOStoredProc;
    dsTags: TDataSource;
	ObtenerTags: TADOStoredProc;
    Panel13: TPanel;
    Panel7: TPanel;
    Panel10: TPanel;
	Panel4: TPanel;
    Panel5: TPanel;
    Panel11: TPanel;
    ObtenerPersona: TADOStoredProc;
    Timer: TTimer;
    ObtenerDeudaCliente: TADOStoredProc;
    Panel3: TPanel;
    lblNombrePersona: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    Label6: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label15: TLabel;
    label9: TLabel;
    label16: TLabel;
    Label3: TLabel;
    label17: TLabel;
    txt_Viajes: TLabel;
    txt_Intereses: TLabel;
    txt_Impuestos: TLabel;
    txt_OtrosConceptos: TLabel;
    txt_DeudaSINFinanciar: TLabel;
    txt_DeudaFinanciada: TLabel;
    txt_Descuentos: TLabel;
    txt_TotalDeuda_2: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    label1: TLabel;
    txt_TotalDeuda_1: TLabel;
    Label4: TLabel;
    Bevel5: TBevel;
    lApellidoNombre: TLabel;
    lDomicilio: TLabel;
    lEMail: TLabel;
    lTEParticular: TLabel;
    lTEMovil: TLabel;
    lTEComercial: TLabel;
    Bevel6: TBevel;
    ObtenerEstadoCliente: TADOStoredProc;
    Label18: TLabel;
    GauMorosidad: TGauge;
    Panel12: TPanel;
    Bevel7: TBevel;
    Label19: TLabel;
    GauContactos: TGauge;
    Label20: TLabel;
    GauEfectividad: TGauge;
	Label21: TLabel;
	GauInfracciones: TGauge;
    Label22: TLabel;
    GauViajes: TGauge;
    Label23: TLabel;
    GauRegularizacion: TGauge;
    pnlCuentas: TPanel;
    mcTipoNumeroDocumento: TMaskCombo;
    Label24: TLabel;
    gauConsumo: TGauge;
    btnBuscar: TDPSButton;
    btnEditar: TDPSButton;
    DPSGridInicio: TDBListEx;
    bdgResumenMovimientos: TDBListEx;
    dbgViajes: TDBListEx;
    dbgListaFacturas: TDBListEx;
    dbgPagos: TDBListEx;
    dbgFinanciamientos: TDBListEx;
    dbgInfracciones: TDBListEx;
	dpgTag: TDBListEx;
    dbgRefacturacion: TDBListEx;
    dg_Cuentas: TDBListEx;
    dg_OrdenesServicio: TDBListEx;
	procedure btn_cerrarClick(Sender: TObject);
	procedure btn_editarClick(Sender: TObject);
	procedure PageControlChange(Sender: TObject);
	procedure cb_TiposDocumentoChange(Sender: TObject);
	procedure btnBuscarClick(Sender: TObject);
	procedure btnEditarClick(Sender: TObject);
	procedure TimerTimer(Sender: TObject);
    procedure PageControlDrawTab(Control: TCustomTabControl;
      TabIndex: Integer; const Rect: TRect; Active: Boolean);
    procedure mcTipoNumeroDocumentoChange(Sender: TObject);
    procedure dg_CuentasLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure bdgResumenMovimientosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
	procedure dbgViajesLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure dbgListaFacturasLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure dbgPagosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure dbgRefacturacionLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure dbgInfraccionesLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure dg_OrdenesServicioDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dg_OrdenesServicioCheckLink(Sender: TCustomDBListEx;
      Column: TDBListExColumn; var IsLink: Boolean);
    procedure dg_OrdenesServicioLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
  private
	FApellido, FApellidoMaterno, FNombre: AnsiString;
	FHabilitarBusqueda, FComprobantesVencidos, FOSPendientes, FFinanACaducar,
    FInfNOConfirmadas, FTAGsInhabilitados: Boolean;

	function CargarCliente(CodigoCliente: Integer; TipoDocumento, NumeroDocumento: AnsiString): Boolean;
	procedure BuscarItemGrilla(TabActual: Integer; Descripcion, Patente: AnsiString; FechaHora: TDateTime;
	  TipoComprobante: Char; NumeroComprobante, ContractSerialNumber: Double;
	  Codigo, Numero, ContextMark: Integer);
	procedure ObtenerItemGrilla(TabActual: Integer; var Descripcion, Patente: AnsiString;
	  var FechaHora: TDateTime; var TipoComprobante: Char; var NumeroComprobante, ContractSerialNumber: Double;
	  var Codigo, Numero, ContextMark: Integer);

  protected
	procedure Loaded; override;

  public
	{ Public declarations }
	FCodigoCliente: Integer;
    FCodigoPersona: integer;
	Procedure RefreshData; override;
	Function GetBookmark(Var Bookmark, Description: AnsiString): Boolean; override;
	Function GotoBookmark(Bookmark: AnsiString): Boolean; override;
	class Function CreateBookmark(HabilitarBusqueda: Boolean; CodigoCliente, CodigoCuenta: Integer;
	  NombrePagina, CodigoDocumento, NumeroDocumento: AnsiString;
	  Descripcion, Patente: AnsiString; FechaHora: TDateTime; TipoComprobante: Char; NumeroComprobante: Double;
	  Codigo, Numero, ContextMark: Integer; ContractSerialNumber: Double): AnsiString;
	function Inicializa: Boolean; override;
	procedure ActualizarCuentas;
  end;

var
  NavWindowCliente: TNavWindowCliente;

implementation

uses DMConnection;


{ TNavWindowDatosCliente }
{$R *.dfm}


class function TNavWindowCliente.CreateBookmark(HabilitarBusqueda: Boolean; CodigoCliente, CodigoCuenta: Integer;
	  NombrePagina, CodigoDocumento, NumeroDocumento: AnsiString;
	  Descripcion, Patente: AnsiString; FechaHora: TDateTime; TipoComprobante: Char; NumeroComprobante: Double;
	  Codigo, Numero, ContextMark: Integer; ContractSerialNumber: Double): AnsiString;
begin
	{Mapa de bookmark
	1  - Tipo de acceso (Por comunicacion o por menu (BackOffice))
	2  - CodigoCliente
	3  - CodigoCuenta
	4  - Nombre Pagina
	5  - CodigoDocumento
	6  - Numero Documento
	7  - Descripcion
	8  - Patente
	9  - FechaHora
	10 - TipoComprobante
	11 - NumeroComprobante
	12 - Codigo
	13 - Numero
	14 - Context Mark
	15 - ContractSerialNumber
	}

	Result := format('%s;%d;%d;%s;%s;%s;%s;%s;%s;%s;%.0f;%d;%d;%d;%.0f',[
	  iif(HabilitarBusqueda, 'TRUE', 'FALSE'),
	  CodigoCliente,
	  CodigoCuenta,
	  trim(NombrePagina),
	  CodigoDocumento,
	  NumeroDocumento,
	  Descripcion,
	  Patente,
	  DateTimeToStr(FechaHora),
	  TipoComprobante, NumeroComprobante,
	  Codigo, Numero, ContextMark, ContractSerialNumber]);
end;


procedure TNavWindowCliente.ObtenerItemGrilla(TabActual: Integer;
  var Descripcion, Patente: AnsiString; var FechaHora: TDateTime;
  var TipoComprobante: Char; var NumeroComprobante, ContractSerialNumber: Double;
  var Codigo, Numero, ContextMark: Integer);
begin
	case TabActual of
		0: Begin
			   if not ObtenerResumenCliente.isEmpty then begin
				   Descripcion := ObtenerResumenCliente.FieldByName('Evento').asString;
				   FechaHora := ObtenerResumenCliente.FieldByName('Fecha').asDateTime;
			   end;
		   end;
		1: Begin
			   if not ObtenerResumenMovimientosCliente.IsEMpty then begin
				   Descripcion := ObtenerResumenMovimientosCliente.FieldByName('Movimiento').asString;
				   TipoComprobante := ObtenerResumenMovimientosCliente.FieldByName('TipoComprobante').asString[1];
				   NumeroComprobante := ObtenerResumenMovimientosCliente.FieldByName('NumeroComprobante').asFloat;
			   end;
		   end;
		2: begin
			   if not ObtenerComunicaciones.isEmpty then
				   Codigo := ObtenerComunicaciones.FieldByName('CodigoComunicacion').asInteger;
		   end;
		3: begin
			   if not ObtenerViajesCliente.IsEmpty then begin
				   Codigo := ObtenerViajesCliente.FieldByName('CodigoConcesionaria').asInteger;
				   Numero := ObtenerViajesCliente.FieldByName('NumeroViaje').asInteger;
			   end;
		   end;
		4: begin
			   if not ObtenerListaComprobantes.IsEmpty then begin
				   TipoComprobante := ObtenerListaComprobantes.FieldByName('TipoComprobante').asString[1];
				   NumeroComprobante := ObtenerListaComprobantes.FieldByName('NumeroComprobante').asFloat;
			   end;
		   end;
		5: begin
			   if not ObtenerPagosComprobantes.isEMpty then begin
				   TipoComprobante := ObtenerPagosComprobantes.FieldByName('TipoComprobante').asString[1];
				   NumeroComprobante := ObtenerPagosComprobantes.FieldByName('NumeroComprobante').asFloat;
			   end;
		   end;
		6: begin
			   if not ObtenerReFacturacion.isEMpty then begin
				  TipoComprobante := ObtenerReFacturacion.FieldByName('TipoComprobante').asString[1];
				  NumeroComprobante := ObtenerReFacturacion.FieldByName('NumeroComprobante').asFloat;
			   end;
		   end;
		7: begin
			   if not ObtenerFinanciamientos.isEMpty then begin
				   Codigo := ObtenerFinanciamientos.FieldByName('CodigoCuenta').asInteger;
				   Numero := ObtenerFinanciamientos.FieldByName('NumeroFinanciamiento').asInteger;
			   end;
		   end;
		8: begin
			   if not ObtenerResumenInfracciones.isEmpty then begin
				   Patente := ObtenerResumenInfracciones.FieldByName('Patente').asString;
				   Codigo := ObtenerResumenInfracciones.FieldByName('NumeroHistorialInfraccion').asInteger;
				   Numero := ObtenerResumenInfracciones.FieldByName('NumeroViaje').asInteger;
			   end;
		   end;
		9: begin
               if not ObtenerTags.isEmpty then begin
				   ContextMark := ObtenerTags.FieldByName('ContextMark').asInteger;
				   ContractSerialNumber := ObtenerTags.FieldByName('ContractSerialNumber').asFloat;
			   end;
               
		   end
	end;
end;

procedure TNavWindowCliente.BuscarItemGrilla(TabActual: Integer;
  Descripcion, Patente: AnsiString; FechaHora: TDateTime;
  TipoComprobante: Char; NumeroComprobante, ContractSerialNumber: Double;
  Codigo, Numero, ContextMark: Integer);
begin

	// Posiciona el cursor de la consulta actual en la pocisi�n indicada
	case TabActual of
		0: if not ObtenerResumenCliente.isEmpty then
			   ObtenerResumenCliente.locate('Evento;Fecha', VarArrayOf([Descripcion, DateTimeToStr(FechaHora)]),[]);
		1: if not ObtenerResumenMovimientosCliente.isEmpty then
			   ObtenerResumenMovimientosCliente.locate('Movimiento;TipoComprobante;NumeroComprobante',
				 VarArrayOf([Descripcion, TipoComprobante, NumeroComprobante]),[]);
		2: if not ObtenerComunicaciones.isEmpty then
			   ObtenerComunicaciones.Locate('CodigoComunicacion',Codigo,[]);
		3: if not ObtenerViajesCliente.isEMpty then
			   ObtenerViajesCliente.locate('CodigoConcesionaria;NumeroViaje',VarArrayOf([Codigo,	Numero]),[]);
		4: if not ObtenerListaComprobantes.isEmpty then
			   ObtenerListaComprobantes.locate('TipoComprobante;NumeroComprobante',VarArrayOf([TipoComprobante,
				 NumeroComprobante]),[]);
		5: if not ObtenerPagosComprobantes.isEmpty then
			   ObtenerPagosComprobantes.Locate('TipoComprobante;NumeroComprobante',VarArrayOf([TipoComprobante,
				 NumeroComprobante]),[]);
		6: if not ObtenerRefacturacion.isEmpty then
			   ObtenerRefacturacion.Locate('TipoComprobante;NumeroComprobante',VarArrayOf([TipoComprobante,
				 NumeroComprobante]),[]);
		7: if not ObtenerFinanciamientos.isEMpty then
			   ObtenerFinanciamientos.Locate('CodigoCuenta;NumeroFinanciamiento',VarArrayOf([Codigo, Numero]),[]);
		8: if not ObtenerResumenInfracciones.isEmpty then
			   ObtenerResumenInfracciones.Locate('Patente;NumeroHistorialInfraccion;NumeroViaje',
				 VarArrayOf([Patente, iif(Codigo = 0, null, Codigo), iif(Numero = 0, null, numero)]), []);
		9: if not ObtenerTags.IsEmpty then
			   ObtenerTags.Locate('ContextMark;ContractSerialNumber',VarArrayOf([ContextMark, ContractSerialNumber]),[]);
	end;
end;

function TNavWindowCliente.GetBookmark(var Bookmark, Description: AnsiString): Boolean;
resourcestring
    MSG_INICIO_COMUNICACION  = 'Inicio de la comunicaci�n con el Cliente %s';
    MSG_PANTALLA_PRINCIPAL   = 'Pantalla principal del Cliente %s';
var Patente, Descripcion: AnsiString;
	FechaHora: TDateTime;
	TipoComprobante: Char;
	NumeroComprobante, ContractSerialNumber: Double;
	CodigoCuenta, Codigo, Numero, ContextMark: Integer;
begin
	Result := (FCodigoPersona > 0);
	// En funcion de la pagina asignamos los campos de estado de la pantalla
	Patente := '';
	Descripcion := '';
	FechaHora := nulldate;
	TipoComprobante := ' ';
	NumeroComprobante := 0.0;
	ContractSerialNumber := 0.0;
	Codigo := 0;
	CodigoCuenta := 0;
	Numero := 0;
	ContextMark := 0;

	if not ObtenerDetallesCuentasCliente.isEmpty then
		CodigoCuenta := ObtenerDetallesCuentasCliente.FieldByName('CodigoCuenta').asInteger;
	ObtenerItemGrilla(PageControl.TabIndex, Descripcion, Patente, FechaHora, TipoComprobante,
	  NumeroComprobante, ContractSerialNumber, Codigo, Numero, ContextMark);

	(*Bookmark := CreateBookmark(FHabilitarBusqueda, FCodigoPersona, CodigoCuenta, PageControl.ActivePage.Name,
	  trim(StrRight(cb_tiposdocumento.Text, 20)), txt_NumeroDocumento.text,
	  Descripcion, Patente, FechaHora, TipoComprobante, NumeroComprobante,
	  Codigo, Numero, ContextMark, ContractSerialNumber);*)

	Bookmark := CreateBookmark(FHabilitarBusqueda, FCodigoPersona, CodigoCuenta, PageControl.ActivePage.Name,
	  trim(StrRight(mcTipoNumeroDocumento.ComboText, 20)), mcTipoNumeroDocumento.MaskText,
	  Descripcion, Patente, FechaHora, TipoComprobante, NumeroComprobante,
	  Codigo, Numero, ContextMark, ContractSerialNumber);

	if not FHabilitarBusqueda then
		Description := Format(MSG_INICIO_COMUNICACION, [Trim(BuscarApellidoNombreCliente(DMConnections.BaseCAC, FCodigoPersona))])
	else
		Description := Format(MSG_PANTALLA_PRINCIPAL, [Trim(BuscarApellidoNombreCliente(DMConnections.BaseCAC, FCodigoPersona))])
end;

function TNavWindowCliente.GotoBookmark(Bookmark: AnsiString): Boolean;
var NombrePagina: AnsiString;
	CodigoCliente, i: Integer;
	FechaHora: TDateTime;
	TipoComprobante: Char;
begin
	Timer.Enabled := False;
	FHabilitarBusqueda := Trim(ParseParamByNumber(Bookmark, 1, ';')) = 'TRUE';
	// Habilitamos los campos de busqueda o no
//	cb_TiposDocumento.enabled := FHabilitarBusqueda;
//	txt_NumeroDocumento.enabled := FHabilitarBusqueda;
	mcTipoNumeroDocumento.Enabled := FHabilitarBusqueda;

	btnBuscar.enabled := FHabilitarBusqueda;
	// Buscamos la p�gina seteada
	NombrePagina := Trim(ParseParamByNumber(Bookmark, 4, ';'));
	i := 0;
	while (i < PageControl.PageCount) and (NombrePagina <> PageControl.Pages[i].name) do
		inc(i);
	PageControl.ActivePageIndex := iif(i = PageControl.PageCount, 0, i);

	// Cargamos la informaci�n del cliente
	CodigoCliente := IVal(ParseParamByNumber(Bookmark, 2, ';'));
(*	if FHabilitarBusqueda then begin
		i := 0;
		while (i < cb_TiposDocumento.Items.Count) and
		  (trim(StrRight(cb_tiposdocumento.Items[i], 20)) <> trim(ParseParamByNumber(Bookmark, 5, ';'))) do
			inc(i);
		cb_TiposDocumento.ItemIndex := iif(i = cb_TiposDocumento.Items.Count, 2, i);
		txt_numeroDocumento.value := IVal(ParseParamByNumber(Bookmark, 6, ';'));
	end;*)

	if FHabilitarBusqueda then begin
		i := 0;
		while (i < mcTipoNumeroDocumento.ItemsCount) and
		  (trim(StrRight( mcTipoNumeroDocumento.Items[i].Caption, 20)) <> trim(ParseParamByNumber(Bookmark, 5, ';'))) do
			inc(i);
		mcTipoNumeroDocumento.ItemIndex	:= iif(i = mcTipoNumeroDocumento.ItemsCount, 2, i);
		mcTipoNumeroDocumento.MaskText	:= ParseParamByNumber(Bookmark, 6, ';');
	end;

    if CodigoCliente <> 0 then begin
        Result := CargarCliente(CodigoCliente, '', '');

        // Seteamos la pantalla en el estado en que estaba
        if not ObtenerDetallesCuentasCliente.isEmpty then
            ObtenerDetallesCuentasCliente.locate('CodigoCuenta', IVal(ParseParamByNumber(Bookmark, 3, ';')), []);
    end
    else result := true;
    
	try
		FechaHora := strToDateTime(trim(ParseParamByNumber(Bookmark, 9, ';')));
	except
		FechaHora := nulldate;
	end;

	if trim(ParseParamByNumber(Bookmark, 10, ';')) = '' then
		TipoComprobante := ' '
	else
		tipoComprobante := trim(ParseParamByNumber(Bookmark, 10, ';'))[1];

	BuscarItemGrilla(PageControl.TabIndex, trim(ParseParamByNumber(Bookmark, 7, ';')),
	  Trim(ParseParamByNumber(Bookmark, 8, ';')), FechaHora, TipoComprobante,
	  strToFloat(trim(ParseParamByNumber(Bookmark, 11, ';'))),
	  strToFloat(ParseParamByNumber(Bookmark, 15, ';')),
	  IVal(ParseParamByNumber(Bookmark, 12, ';')),
	  IVal(ParseParamByNumber(Bookmark, 13, ';')),
	  IVal(ParseParamByNumber(Bookmark, 14, ';')));

//	if txt_NumeroDocumento.enabled then txt_NumeroDocumento.setFocus;
	if mcTipoNumeroDocumento.enabled then mcTipoNumeroDocumento.setFocus;
	Timer.Enabled := True;
end;

function TNavWindowCliente.Inicializa: Boolean;
resourcestring
    MSG_COL_TITLE_FECHA_DE_ALTA  = 'Fecha de Alta';
    MSG_COL_TITLE_SALDO			 = 'Saldo';
    MSG_COL_TITLE_PLAN_COMERCIAL = 'Plan Comercial';
Var
	Sz: TSize;
begin
	inherited Inicializa;

    
  	FComprobantesVencidos := False;
    FOSPendientes := False;
    FFinanACaducar := False;
    FInfNOConfirmadas := False;
    FTAGsInhabilitados := False;

    with dg_cuentas.Columns do begin
		Items[0].Header.Caption  := MSG_COL_TITLE_CUENTA;
		Items[1].Header.Caption  := MSG_COL_TITLE_SALDO;
        Items[2].Header.Caption  := MSG_COL_TITLE_FORMA_DE_PAGO;
        Items[3].Header.Caption  := MSG_COL_TITLE_PLAN_COMERCIAL;
  		Items[4].Header.Caption  := MSG_COL_TITLE_PATENTE;
        Items[5].Header.Caption  := MSG_COL_TITLE_VEHICULO;
        Items[6].Header.Caption  := MSG_COL_TITLE_FECHA_DE_ALTA;
		Items[7].Header.Caption  := MSG_COL_TITLE_ESTADO;
	end;

	with DPSGridInicio.Columns do begin
		Items[0].Header.Caption  := MSG_COL_TITLE_EVENTO;
		Items[1].Header.Caption  := MSG_COL_TITLE_FECHA;
		Items[2].Header.Caption  := MSG_COL_TITLE_ID_EVENTO;
		Items[3].Header.Caption  := MSG_COL_TITLE_ESTADO;
		Items[4].Header.Caption  := MSG_COL_TITLE_IMPORTE;
		Items[5].Header.Caption  := MSG_COL_TITLE_OBSERVACIONES;
    end;

	with bdgResumenMovimientos.Columns do begin
		Items[0].Header.Caption  := MSG_COL_TITLE_MOVIMIENTO;
		Items[1].Header.Caption  := MSG_COL_TITLE_TIPO;
		Items[2].Header.Caption  := MSG_COL_TITLE_NUMERO;
		Items[3].Header.Caption  := MSG_COL_TITLE_DET_MOV;
		Items[4].Header.Caption  := MSG_COL_TITLE_FECHA;
		Items[5].Header.Caption  := MSG_COL_TITLE_VENCIMIENTO;
		Items[6].Header.Caption  := MSG_COL_TITLE_DEUDA_ANTERIOR;
		Items[7].Header.Caption  := MSG_COL_TITLE_IMPORTE;
		Items[8].Header.Caption  := MSG_COL_TITLE_DEUDA_ACTUAL;
	end;

	with dbgViajes.Columns do begin
		Items[0].Header.Caption  := MSG_COL_TITLE_MOMENTO_DE_INICIO;
		Items[1].Header.Caption  := MSG_COL_TITLE_DET_VIAJE;
		Items[2].Header.Caption  := MSG_COL_TITLE_CONCESIONARIA;
		Items[3].Header.Caption  := MSG_COL_TITLE_PUNTOS_RECORRIDOS;
		Items[4].Header.Caption  := MSG_COL_TITLE_IMPORTE;
		Items[5].Header.Caption  := MSG_COL_TITLE_PATENTE;
		Items[6].Header.Caption  := MSG_COL_TITLE_CUENTA;
		Items[7].Header.Caption  := MSG_COL_TITLE_KMS;
		Items[8].Header.Caption  := MSG_COL_TITLE_FECHA_FACT;
		Items[9].Header.Caption  := MSG_COL_TITLE_TIPO_COMP;
		Items[10].Header.Caption  := MSG_COL_TITLE_NRO_COMP;
	end;

	with dbgListaFacturas.Columns do begin
		Items[0].Header.Caption  := MSG_COL_TITLE_ESTADO;
		Items[1].Header.Caption  := MSG_COL_TITLE_FECHA;
		Items[2].Header.Caption  := MSG_COL_TITLE_TIPO;
		Items[3].Header.Caption  := MSG_COL_TITLE_NUMERO;
		Items[4].Header.Caption  := MSG_COL_TITLE_VENCIMIENTO;
		Items[5].Header.Caption  := MSG_COL_TITLE_DET_CTE;
		Items[6].Header.Caption  := MSG_COL_TITLE_IMPRESION;
		Items[7].Header.Caption  := MSG_COL_TITLE_IMPORTE;
		Items[8].Header.Caption  := MSG_COL_TITLE_VIAJES;
		Items[9].Header.Caption  := MSG_COL_TITLE_SERVICIOS;
		Items[10].Header.Caption := MSG_COL_TITLE_INTERESES;
		Items[11].Header.Caption := MSG_COL_TITLE_IMPUESTOS;
		Items[12].Header.Caption := MSG_COL_TITLE_CONVENIOS;
		Items[13].Header.Caption := MSG_COL_TITLE_OTROS_CONCEPTOS;
		Items[14].Header.Caption := MSG_COL_TITLE_DESCUENTOS;
	end;

	with dbgPagos.Columns do begin
		Items[0].Header.Caption  := MSG_COL_TITLE_TIPO;
		Items[1].Header.Caption  := MSG_COL_TITLE_NUMERO;
		Items[2].Header.Caption  := MSG_COL_TITLE_DET_PAGO;
		Items[3].Header.Caption  := MSG_COL_TITLE_IMPORTE;
		Items[4].Header.Caption  := MSG_COL_TITLE_FECHA;
		Items[5].Header.Caption  := MSG_COL_TITLE_LUGAR_DE_PAGO;
		Items[6].Header.Caption  := MSG_COL_TITLE_DEUDA_ANTERIOR;
		Items[7].Header.Caption  := MSG_COL_TITLE_DEUDA_POSTERIOR;
	end;

	with dbgRefacturacion.Columns do begin
		Items[0].Header.Caption  := MSG_COL_TITLE_ESTADO;
		Items[1].Header.Caption  := MSG_COL_TITLE_FECHA;
		Items[2].Header.Caption  := MSG_COL_TITLE_TIPO;
		Items[3].Header.Caption  := MSG_COL_TITLE_NUMERO;
		Items[4].Header.Caption  := MSG_COL_TITLE_DET_CTE;
		Items[5].Header.Caption  := MSG_COL_TITLE_CTE_AJUSTADO;
		Items[6].Header.Caption  := MSG_COL_TITLE_VENCIMIENTO;
		Items[7].Header.Caption  := MSG_COL_TITLE_IMPORTE;
		Items[7].Header.Caption  := MSG_COL_TITLE_VIAJES;
		Items[8].Header.Caption  := MSG_COL_TITLE_SERVICIOS;
		Items[9].Header.Caption  := MSG_COL_TITLE_INTERESES;
		Items[10].Header.Caption	:= MSG_COL_TITLE_IMPUESTOS;
		Items[11].Header.Caption	:= MSG_COL_TITLE_CONVENIOS;
		Items[12].Header.Caption	:= MSG_COL_TITLE_OTROS_CONCEPTOS;
		Items[13].Header.Caption	:= MSG_COL_TITLE_DESCUENTOS;
	end;

	with dbgFinanciamientos.columns do begin
		Items[0].Header.Caption  := MSG_COL_TITLE_ESTADO;
		Items[1].Header.Caption  := MSG_COL_TITLE_FECHA;
		Items[2].Header.Caption  := MSG_COL_TITLE_CUENTA;
		Items[3].Header.Caption  := MSG_COL_TITLE_IMPORTE_FINANCIADO;
		Items[4].Header.Caption  := MSG_COL_TITLE_PIE_ENTREGADO;
		Items[5].Header.Caption  := MSG_COL_TITLE_INTERES;
		Items[6].Header.Caption  := MSG_COL_TITLE_VIGENCIA;
		Items[7].Header.Caption  := MSG_COL_TITLE_IMPORTE_CUOTA;
		Items[8].Header.Caption  := MSG_COL_TITLE_CUOTAS;
		Items[9].Header.Caption  := MSG_COL_TITLE_CUOTAS_IMPAGAS;
		Items[10].Header.Caption := MSG_COL_TITLE_CUOTAS_PAGAS;
		Items[11].Header.Caption := MSG_COL_TITLE_PROXIMA_FACTURACION;
		Items[12].Header.Caption := MSG_COL_TITLE_PROXIMO_VENCIMIENTO;
	end;

	with dbgInfracciones.Columns do begin
		Items[0].Header.Caption  := MSG_COL_TITLE_ESTADO;
		Items[1].Header.Caption  := MSG_COL_TITLE_FECHA;
		Items[2].Header.Caption  := MSG_COL_TITLE_DEF_INF;
		Items[3].Header.Caption  := MSG_COL_TITLE_CUENTA;
		Items[4].Header.Caption  := MSG_COL_TITLE_DESCRIPCION_INFRACCION;
		Items[5].Header.Caption  := MSG_COL_TITLE_EXPEDIENTE;
		Items[6].Header.Caption  := MSG_COL_TITLE_CONCESIONARIA;
		Items[7].Header.Caption  := MSG_COL_TITLE_TIPO_COMP;
		Items[8].Header.Caption  := MSG_COL_TITLE_NRO_COMP;
	end;

	with dpgTag.Columns do begin
		Items[0].Header.Caption  := MSG_COL_TITLE_NUMERO_DE_TAG;
		Items[1].Header.Caption  := MSG_COL_TITLE_PATENTE;
		Items[2].Header.Caption  := MSG_COL_TITLE_FECHA_ASIGNACION;
		Items[3].Header.Caption  := MSG_COL_TITLE_CATEGORIA;
		Items[4].Header.Caption  := MSG_COL_TITLE_UBICACION_ANTERIOR;
		Items[5].Header.Caption  := MSG_COL_TITLE_PROVEEDOR;
		Items[6].Header.Caption  := MSG_COL_TITLE_FECHA_INGRESO;
		Items[7].Header.Caption  := MSG_COL_TITLE_ESTADO;
	end;

	btnEditar.enabled := false;
    pnlCuentas.BringToFront;
//	CargarTiposDocumento(DMConnections.BaseCAC, cb_TiposDocumento);
	CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento);
	SZ := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, SZ.cx, sz.cy);
	Result := True;
end;

procedure TNavWindowCliente.btn_cerrarClick(Sender: TObject);
begin
	Close;
end;

procedure TNavWindowCliente.RefreshData;
begin
	inherited;
//	CargarCliente(FCodigoPersona, StrRight(cb_tiposdocumento.Text, 20), txt_NumeroDocumento.text);
	CargarCliente(FCodigoPersona, StrRight(mcTipoNumeroDocumento.ComboText, 20), mcTipoNumeroDocumento.MaskText);
end;

procedure TNavWindowCliente.btn_editarClick(Sender: TObject);
begin
	Close;
end;

Function TNavWindowCliente.CargarCliente(CodigoCliente: Integer; TipoDocumento, NumeroDocumento: AnsiString): Boolean;
resourcestring
    CAPTION_NOMBRE_FISICA = 'Nombre: ';
    CAPTION_NOMBRE_JURIDICA = 'Raz�n Social: ';
var i: Integer;
var
	selectComuna, selectRegion: AnsiString;
    CodigoDomicilioEntrega: integer;
begin
    selectComuna := '';
    selectRegion := '';

	FCodigoPersona := CodigoCliente;

	// Cerramos el
	ObtenerResumenCliente.close;
	ObtenerResumenMovimientosCliente.Close;
	ObtenerComunicaciones.Close;
	ObtenerViajesCliente.Close;
	ObtenerListaComprobantes.Close;
	ObtenerPagosComprobantes.Close;
	ObtenerRefacturacion.Close;
	ObtenerFinanciamientos.Close;
	ObtenerResumenInfracciones.Close;
	ObtenerTags.Close;

	// Cargamos los datos personales del cliente
	try
		With ObtenerPersona do begin
			close;
			Parameters.ParamByName('@CodigoPersona').Value := iif(FCodigoPersona = 0, null, FCodigoPersona);
			Parameters.ParamByName('@CodigoDocumento').Value := iif(TipoDocumento = '', null, TipoDocumento);
			Parameters.ParamByName('@NumeroDocumento').Value := iif(NumeroDocumento = '', null, NumeroDocumento);
			Open;
			FCodigoPersona := FieldByName('CodigoPersona').asInteger;

			if IsEmpty then begin
				lApellidoNombre.Caption := '';
                lDomicilio.Caption := '';
			end else begin
			    FApellido := TrimRight(FieldByName('Apellido').AsString);
                FApellidoMaterno := TrimRight(FieldByName('ApellidoMaterno').AsString);
				FNombre := TrimRight(FieldByName('Nombre').AsString);
                if FieldByName('Personeria').AsString = PERSONERIA_FISICA then begin
                    lblNombrePersona.Caption := CAPTION_NOMBRE_FISICA;
                    lApellidoNombre.Caption := ArmaNombreCompleto(FApellido, FApellidoMaterno, FNombre);
                end
                else begin
                    lblNombrePersona.Caption := CAPTION_NOMBRE_JURIDICA;
                    lApellidoNombre.Caption := ArmaRazonSocial(FApellido,FNombre);
                end;
            end;
            if not FHabilitarBusqueda then begin
				i := 0;
				while (i < mcTipoNumeroDocumento.ItemsCount) and
				  (trim(StrRight( mcTipoNumeroDocumento.Items[i].Caption, 20)) <> trim(FieldByName('CodigoDocumento').AsString)) do
					inc(i);
				mcTipoNumeroDocumento.ItemIndex	:= iif(i = mcTipoNumeroDocumento.ItemsCount, 2, i);
				mcTipoNumeroDocumento.MaskText	:= FieldByName('NumeroDocumento').AsString;
			end;

			btnEditar.enabled := not isEmpty;

            CodigoDomicilioEntrega := ObtenerCodigoDomicilioEntrega (FCodigoPersona);

			Result := (not isEmpty) or FHabilitarBusqueda;
			Close;
		end;

        lDomicilio.Caption := ArmarDomicilioCompletoPorCodigo(ObtenerPersona.Connection, CodigoCliente ,CodigoDomicilioEntrega);

        //Traigo el telefono particular y comercial y el mail
        lTEParticular.Caption := '';
        lTEMovil.Caption := '';
        lTEComercial.Caption := '';
        lEMail.Caption := '';

        lTEParticular.Caption := ObtenerValorMediosComunicacion(FCodigoPersona, MC_TELEPHONE, TIPO_ORIGEN_DATO_PARTICULAR);
		lTEMovil.Caption := ObtenerValorMediosComunicacion(FCodigoPersona, MC_MOVIL_PHONE, TIPO_ORIGEN_DATO_PARTICULAR);
        lTEComercial.Caption := ObtenerValorMediosComunicacion(FCodigoPersona, MC_TELEPHONE, TIPO_ORIGEN_DATO_COMERCIAL);
        lEMail.Caption := ObtenerValorMediosComunicacion(FCodigoPersona, MC_EMAIL, TIPO_ORIGEN_DATO_PARTICULAR);

	except
		Result := False;
	end;
	if not Result then Exit;

	// Cargamos la informacion de la deuda del cliente
	with ObtenerDeudaCliente, ObtenerDeudaCliente.parameters do begin
        close;
		ParamByName('@CodigoPersona').Value := FCodigoPersona;
		ExecProc;
		txt_Viajes.Caption := formatFloat(FORMATO_IMPORTE, ParamByName('@Viajes').value);
		txt_Intereses.Caption := formatFloat(FORMATO_IMPORTE, ParamByName('@Intereses').value);
		txt_Impuestos.Caption := formatFloat(FORMATO_IMPORTE, ParamByName('@Impuestos').value);
		txt_OtrosConceptos.Caption := formatFloat(FORMATO_IMPORTE, ParamByName('@OtrosConceptos').value +
		  ParamByName('@FinanCaducados').value);
		txt_Descuentos.Caption := formatFloat(FORMATO_IMPORTE, ParamByName('@Descuentos').value);
   		txt_DeudaFinanciada.Caption := formatFloat(FORMATO_IMPORTE, ParamByName('@DeudaFinanciada').value);
   		txt_DeudaSINFinanciar.Caption := formatFloat(FORMATO_IMPORTE, ParamByName('@DeudaSINFinanciar').value);
		txt_TotalDeuda_1.Caption := formatFloat(FORMATO_IMPORTE, ParamByName('@DeudaTotal').value);
		txt_TotalDeuda_2.Caption := formatFloat(FORMATO_IMPORTE, ParamByName('@DeudaTotal').value);
	end;

	// Cargamos la lista de cuentas del cliente
	ObtenerDetallesCuentasCliente.Close;
	ObtenerDetallesCuentasCliente.Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
	ObtenerDetallesCuentasCliente.Open;

	// Cargamos la informacion de movimientos de la cuenta
	PageControlChange(PageControl);
    with ObtenerEstadoCliente do begin
        Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
        Parameters.ParamByName('@CantidadDias').Value := 60;
		Parameters.ParamByName('@IndiceViaje').Value := 5;
        ExecProc;
        FComprobantesVencidos := Parameters.ParamByName('@ComprobantesVencidos').Value;
        FOSPendientes := Parameters.ParamByName('@OSPendientes').Value;
        FFinanACaducar := Parameters.ParamByName('@FinanACaducar').Value;
        FInfNOConfirmadas := Parameters.ParamByName('@InfNOConfirmadas').Value;
        FTAGsInhabilitados := Parameters.ParamByName('@TAGsInhabilitados').Value;
        GauMorosidad.Progress := Parameters.ParamByName('@Morosidad').Value;
        GauRegularizacion.Progress := Parameters.ParamByName('@Regularizacion').Value;
        GauInfracciones.Progress := Parameters.ParamByName('@Infracciones').Value;
        GauViajes.Progress := Parameters.ParamByName('@Viajes').Value;
        GauContactos.Progress := Parameters.ParamByName('@Contactos').Value;
        GauEfectividad.Progress := Parameters.ParamByName('@Efectividad').Value;
        //GauConsumo.Progress := Parameters.ParamByName('@Consumo').Value;
    end;
    PageControl.Repaint;
end;

procedure TNavWindowCliente.Loaded;
begin
	inherited;
	Color := $00FFFDFB;
end;

procedure TNavWindowCliente.PageControlChange(Sender: TObject);
begin
	inherited;
	Screen.Cursor := crHourGlass;
	try
		case PageControl.TabIndex of
			0:begin
				  with ObtenerResumenCliente do begin
					   if ((not Active) or (not Timer.enabled)) then begin
						   Close;
						   Parameters.ParamByName('@CodigoPersona').value := FCodigoPersona;
						   Parameters.ParamByName('@DesdeFecha').value := NowBase(DMConnections.BaseCAC) - 365;
						   Open;
					   end;
				   end;
			   end;
			1: begin
				  with ObtenerResumenMovimientosCliente do begin
					   if ((not Active) or (not Timer.enabled)) then begin
						   Close;
						   Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
						   Parameters.ParamByName('@DesdeFecha').value := NowBase(DMConnections.BaseCAC) - 365;
						   Open;
					   end;
				   end;
			   end;
			2: begin
				  with ObtenerComunicaciones do begin
					   if ((not Active) or (not Timer.enabled)) then begin
						   Close;
						   Parameters.ParamByName('@CodigoPersona').value := FCodigoPersona;
						   Parameters.ParamByName('@DesdeFecha').value := NowBase(DMConnections.BaseCAC) - 365;
						   Open;
					   end;
				   end;
			   end;
			3: begin
				   with ObtenerViajesCliente do begin
					   if ((not Active) or (not Timer.enabled)) then begin
						   Close;
						   Parameters.ParamByName('@CodigoPersona').value := FCodigoPersona;
						   Open;
					   end;
				   end;
			   end;
			4: begin
				   with ObtenerListaComprobantes do begin
					   if ((not Active) or (not Timer.enabled)) then begin
						   Close;
						   Parameters.ParamByName('@CodigoPersona').value := FCodigoPersona;
						   Parameters.ParamByName('@TipoComprobante').value := TC_FACTURA;
						   Open;
					   end;
				   end;
			   end;
			5: begin
				   with ObtenerPagosComprobantes do begin
					   if ((not Active) or (not Timer.enabled)) then begin
						   Close;
						   Parameters.ParamByName('@CodigoPersona').value := FCodigoPersona;
						   Parameters.ParamByName('@DesdeFecha').value := NowBase(DMConnections.BaseCAC) - 365;
						   Open;
					   end;
				   end;
			   end;
			6: begin
				   with ObtenerRefacturacion do begin
					   if ((not Active) or (not Timer.enabled)) then begin
						   Close;
						   Parameters.ParamByName('@CodigoPersona').value := FCodigoPersona;
						   Parameters.ParamByName('@TipoComprobante').value := TC_NOTA_CREDITO + TC_NOTA_DEBITO;
						   Open;
					   end;
				   end;
			   end;
			7: Begin
				   with ObtenerFinanciamientos do begin
					   if ((not Active) or (not Timer.enabled)) then begin
						   Close;
						   Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
						   Open;
					   end;
				   end;
			   end;
			8: Begin
				   with ObtenerResumenInfracciones do begin
					   if ((not Active) or (not Timer.enabled)) then begin
						   Close;
						   Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
						   Parameters.ParamByName('@TipoInfraccion').Value := 0;
						   Parameters.ParamByName('@SoloInfraccionesConfirmadas').Value := 0;
						   Parameters.ParamByName('@FechaInicial').Value := NowBase(DMConnections.BaseCAC) - 365;
						   Parameters.ParamByName('@FechaFinal').Value := NowBase(DMConnections.BaseCAC);
						   Open;
					   end;
				   end;
			   end;
			9: Begin
				   with ObtenerTags do begin
					   if ((not Active) or (not Timer.enabled)) then begin
						   Close;
						   Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
						   Open;
					   end;
				   end;
                   
			   end;
		end;
	finally
		Screen.Cursor := crDefault;
	end;
end;

procedure TNavWindowCliente.cb_TiposDocumentoChange(Sender: TObject);
begin
	if Sender = activeControl then begin
//		CargarCliente(0, trim(StrRight(cb_tiposdocumento.Text, 20)), txt_NumeroDocumento.text);
		CargarCliente(0, trim(StrRight( mcTipoNumeroDocumento.ComboText, 20)), mcTipoNumeroDocumento.MaskText);
	end;
end;

procedure TNavWindowCliente.btnBuscarClick(Sender: TObject);
var
	f: TFormBuscaClientes;
//    i: Integer;
begin

	Application.CreateForm(TFormBuscaClientes , f);
(*	if f.Inicializa( mcTipoNumeroDocumento.MaskText, Trim(StrRight( mcTipoNumeroDocumento.ComboText, 20)),
	  FApellido, FApellidoMaterno, FNombre, TBP_CLIENTES)then begin
		if (f.ShowModal = mrok) then begin
			i := 0;
			while (i < mcTipoNumeroDocumento.ItemsCount) and
			  (trim(StrRight(mcTipoNumeroDocumento.Items[i].Caption, 20)) <> f.Persona.TipoDocumento) do
				inc(i);
    		mcTipoNumeroDocumento.ItemIndex	:= iif(i = mcTipoNumeroDocumento.ItemsCount, 2, i);
			mcTipoNumeroDocumento.MaskText	:= f.Persona.NumeroDocumento;

			CargarCliente(f.Persona.CodigoPersona, trim(StrRight(mcTipoNumeroDocumento.ComboText, 20)), mcTipoNumeroDocumento.MaskText);
		end;
	end;
*)
	f.Release;
	ActiveControl := mcTipoNumeroDocumento;
end;



procedure TNavWindowCliente.btnEditarClick(Sender: TObject);
var F: TFormMantenimientoCuenta;
begin
	Application.CreateForm(TFormMantenimientoCuenta, F);
	if f.inicializa(0, ObtenerDetallesCuentasCliente.FieldByName('CodigoCuenta').AsInteger) then begin
		f.ShowModal;
		RefreshData;
	end;
	f.Release;
end;

procedure TNavWindowCliente.TimerTimer(Sender: TObject);
var Patente, Descripcion: AnsiString;
	FechaHora: TDateTime;
	TipoComprobante: Char;
	NumeroComprobante, ContractSerialNumber: Double;
	CodigoCuenta, Codigo, Numero, ContextMark: Integer;
begin
	// En funcion de la pagina obtenemos los campos de estado de la pantalla
	Patente := '';
	Descripcion := '';
	FechaHora := nulldate;
	TipoComprobante := ' ';
	NumeroComprobante := 0.0;
	ContractSerialNumber := 0.0;
	Codigo := 0;
	CodigoCuenta := 0;
	Numero := 0;
	ContextMark := 0;

	Timer.Enabled := False;

	ObtenerDetallesCuentasCliente.DisableControls;
	case PageControl.TabIndex of
		0:ObtenerResumenCliente.DisableControls;
		1: ObtenerResumenMovimientosCliente.DisableControls;
		2: ObtenerComunicaciones.DisableControls;
		3: ObtenerViajesCliente.DisableControls;
		4: ObtenerListaComprobantes.DisableControls;
		5: ObtenerPagosComprobantes.DisableControls;
		6: ObtenerRefacturacion.DisableControls;
		7: ObtenerFinanciamientos.DisableControls;
		8: ObtenerResumenInfracciones.DisableControls;
		9: ObtenerTags.DisableControls;
	end;

	try
		// Tomamos los datos actuales de la ventana
		ObtenerItemGrilla(PageControl.TabIndex, Descripcion, Patente, FechaHora, TipoComprobante,
		  NumeroComprobante, ContractSerialNumber, Codigo, Numero, ContextMark);
		if not ObtenerDetallesCuentasCliente.isEmpty then
			CodigoCuenta := ObtenerDetallesCuentasCliente.FieldByName('CodigoCuenta').asInteger;

		CargarCliente(FCodigoPersona, '', '');

		// Seteamos la pantalla en el estado en que estaba
		if not ObtenerDetallesCuentasCliente.isEmpty then
			ObtenerDetallesCuentasCliente.locate('CodigoCuenta', CodigoCuenta, []);
		BuscarItemGrilla(PageControl.TabIndex, Descripcion, Patente, FechaHora, TipoComprobante,
		  NumeroComprobante, ContractSerialNumber, Codigo, Numero, ContextMark);

	finally
		ObtenerDetallesCuentasCliente.EnableControls;
		case PageControl.TabIndex of
			0: ObtenerResumenCliente.EnableControls;
			1: ObtenerResumenMovimientosCliente.EnableControls;
			2: ObtenerComunicaciones.EnableControls;
			3: ObtenerViajesCliente.EnableControls;
			4: ObtenerListaComprobantes.EnableControls;
			5: ObtenerPagosComprobantes.EnableControls;
			6: ObtenerRefacturacion.EnableControls;
			7: ObtenerFinanciamientos.EnableControls;
			8: ObtenerResumenInfracciones.EnableControls;
			9: ObtenerTags.EnableControls;
		end;

		Timer.Enabled := True;
	end;
end;

procedure TNavWindowCliente.ActualizarCuentas;
var CodigoCuenta: Integer;
begin
	CodigoCuenta := 0;
	ObtenerDetallesCuentasCliente.DisableCOntrols;
	try
		if not ObtenerDetallesCuentasCliente.isEmpty then
			CodigoCuenta := ObtenerDetallesCuentasCliente.FieldByName('CodigoCuenta').asInteger;
		// Cargamos la lista de cuentas del cliente
		ObtenerDetallesCuentasCliente.Close;
		ObtenerDetallesCuentasCliente.Parameters.ParamByName('@CodigoCliente').Value := FCodigoPersona;
		ObtenerDetallesCuentasCliente.Open;

		if not ObtenerDetallesCuentasCliente.isEmpty then
			ObtenerDetallesCuentasCliente.locate('CodigoCuenta', CodigoCuenta, []);
	finally
		ObtenerDetallesCuentasCliente.EnableControls;
	end;
end;


procedure TNavWindowCliente.PageControlDrawTab(Control: TCustomTabControl;
  TabIndex: Integer; const Rect: TRect; Active: Boolean);
begin
	// Destacamos las comunicaciones (Contactos)
    if (TabIndex = 2) then begin
        if not FOSPendientes then
			Control.Canvas.Font.style := [fsBold]
        else
            Control.Canvas.Font.style := [];
    end;
    // Destacamos los comprobantes (Facturas)
	if (TabIndex = 4) then begin
    	if not FComprobantesVencidos then
            Control.Canvas.Font.style := [fsBold]
        else
            Control.Canvas.Font.style := [];
    end;
    // Destacamos los Financiamientos
    if (TabIndex = 7) then begin
		if not FFinanACaducar then
			Control.Canvas.Font.style := [fsBold]
		else
            Control.Canvas.Font.style := [];
    end;
	// Destacamos las Infracciones
    if (TabIndex = 8) then begin
        if not FInfNOConfirmadas then
            Control.Canvas.Font.style := [fsBold]
        else
			Control.Canvas.Font.style := [];
    end;
    // Destacamos los TAGs
    if (TabIndex = 9) then begin
        if not FTAGsInhabilitados then
            Control.Canvas.Font.style := [fsBold]
        else
            Control.Canvas.Font.style := [];
	end;
end;

procedure TNavWindowCliente.mcTipoNumeroDocumentoChange(Sender: TObject);
begin
	if (Sender as TMaskCombo).Focused then
      	CargarCliente(0, trim(StrRight(mcTipoNumeroDocumento.ComboText, 20)), trim(mcTipoNumeroDocumento.MaskText));
end;

procedure TNavWindowCliente.dg_CuentasLinkClick(Sender: TCustomDBListEx;
  Column: TDBListExColumn);
var F: TFormMantenimientoCuenta;
begin
	if Column.FieldName = 'CodigoCuenta' then begin
		Application.CreateForm(TFormMantenimientoCuenta, F);
		if f.inicializa(2, ObtenerDetallesCuentasCliente.FieldByName('CodigoCuenta').AsInteger) then begin
			f.ShowModal;
			if f.ModalResult = idOk then
				RefreshData;
			end;
		f.Release;
	end;
end;

procedure TNavWindowCliente.bdgResumenMovimientosLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
	if (Column.FieldName = 'Ver') and (not ObtenerResumenMovimientosCliente.isEmpty) then begin
		if (ObtenerResumenMovimientosCliente.FieldByName('Movimiento').asString = 'Facturaci�n') or
		  (ObtenerResumenMovimientosCliente.FieldByName('Movimiento').asString = 'Refacturaci�n') then begin
			if ObtenerResumenMovimientosCliente.FieldByName('TipoComprobante').asString[1] = TC_FACTURA then
				Navigator.JumpTo(TFormUltimaFactura,
				  TFormUltimaFactura.CreateBookmark(ObtenerNombreArchivoFactura(DMConnections.BaseCAC, TC_FACTURA,
				  ObtenerResumenMovimientosCliente.FieldByName('NumeroComprobante').asFloat),
				  'Factura','Factura', FCodigoPersona))
			else
				Navigator.JumpTo(TNavWindowDetalleComprobante,
				  TNavWindowDetalleComprobante.CreateBookmark(FCodigoPersona,
				  ObtenerResumenMovimientosCliente.FieldByName('TipoComprobante').asString[1],
				  ObtenerResumenMovimientosCliente.FieldByName('NumeroComprobante').asFloat,
				  iif(ObtenerResumenMovimientosCliente.FieldByName('FechaHoraImpreso').asDateTime = NullDate, 'FALSE', 'TRUE'),
				  0, 0, 0, 0));
		end else begin
			Navigator.JumpTo(TNavWindowDetallePago,
			  TNavWindowDetallePago.CreateBookMark(FCodigoPersona,
			  ObtenerResumenMovimientosCliente.FieldByName('TipoComprobante').asString[1],
			  ObtenerResumenMovimientosCliente.FieldByName('NumeroComprobante').asFloat,
			  0));
		end;
	end;
end;

procedure TNavWindowCliente.dbgViajesLinkClick(Sender: TCustomDBListEx;
  Column: TDBListExColumn);
begin
	if (Column.FieldName = 'Ver') and (not ObtenerViajesCliente.IsEmpty) then
		Navigator.JumpTo(TNavWindowDetalleViaje,
		  TNavWindowDetalleViaje.CreateBookMark(
		  ObtenerViajesCliente.FieldByName('InfraccionSaldoInsuficiente').asBoolean,
		  ObtenerViajesCliente.FieldByName('InfraccionTAGInhabilitado').asBoolean,
		  FCodigoPersona,
		  ObtenerViajesCliente.FieldByName('CodigoConcesionaria').asInteger,
		  ObtenerViajesCliente.FieldByName('NumeroViaje').asInteger,
		  ObtenerViajesCliente.FieldByName('Patente').asString,
		  ObtenerViajesCliente.FieldByName('DescriConcesionaria').asString,
		  ObtenerViajesCliente.FieldByName('ContractSerialNumber').asFloat,
		  ObtenerViajesCliente.FieldByName('FechaHoraInicio').asDateTime,
		  0,
		  ObtenerViajesCliente.FieldByName('PuntosCobro').asString,
		  ObtenerViajesCliente.FieldByName('Importe').asFloat));
end;

procedure TNavWindowCliente.dbgListaFacturasLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
	if (Column.FieldName = 'Ver') and (not ObtenerListaComprobantes.isEmpty) then begin
		if ObtenerListaComprobantes.FieldByName('TipoComprobante').asString[1] = TC_FACTURA then
			Navigator.JumpTo(TFormUltimaFactura,
			  TFormUltimaFactura.CreateBookmark(ObtenerNombreArchivoFactura(DMConnections.BaseCAC, TC_FACTURA,
			  ObtenerListaComprobantes.FieldByName('NumeroComprobante').asFloat),
			  'Factura','Factura', FCodigoPersona))
		else
			Navigator.JumpTo(TNavWindowDetalleComprobante,
			  TNavWindowDetalleComprobante.CreateBookmark(FCodigoPersona,
			  ObtenerListaComprobantes.FieldByName('TipoComprobante').asString[1],
			  ObtenerListaCOmprobantes.FieldByName('NumeroComprobante').asFloat,
			  iif(ObtenerListaComprobantes.FieldByName('FechaHoraImpreso').asDateTime = NullDate, 'FALSE', 'TRUE'),
			  0, 0, 0, 0));
	end
end;

procedure TNavWindowCliente.dbgPagosLinkClick(Sender: TCustomDBListEx;
  Column: TDBListExColumn);
begin
	if (Column.FieldName = 'Ver') and (not ObtenerPagosComprobantes.isEmpty) then
		Navigator.JumpTo(TNavWindowDetallePago,
		  TNavWindowDetallePago.CreateBookMark(FCodigoPersona,
		  ObtenerPagosComprobantes.FieldByName('TipoComprobante').asString[1],
		  ObtenerPagosComprobantes.FieldByName('NumeroComprobante').asFloat,
		  0));
end;

procedure TNavWindowCliente.dbgRefacturacionLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
	if (Column.FieldName = 'Ver') and (not ObtenerRefacturacion.isEmpty) then
		if ObtenerRefacturacion.FieldByName('TipoComprobante').asString[1] = TC_FACTURA then
			Navigator.JumpTo(TFormUltimaFactura,
			  TFormUltimaFactura.CreateBookmark(ObtenerNombreArchivoFactura(DMConnections.BaseCAC, TC_FACTURA,
			  ObtenerRefacturacion.FieldByName('NumeroComprobante').asFloat),
			  'Factura','Factura', FCodigoPersona))
		else
			Navigator.JumpTo(TNavWindowDetalleComprobante,
			  TNavWindowDetalleComprobante.CreateBookmark(FCodigoPersona,
			  ObtenerRefacturacion.FieldByName('TipoComprobante').asString[1],
			  ObtenerRefacturacion.FieldByName('NumeroComprobante').asFloat,
			  iif(ObtenerRefacturacion.FieldByName('FechaHoraImpreso').asDateTime = NullDate, 'FALSE', 'TRUE'),
			  0, 0, 0, 0));
end;

procedure TNavWindowCliente.dbgInfraccionesLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
	if (Column.FieldName = 'Ver') and (not ObtenerResumenInfracciones.isEmpty) then begin
		if ObtenerResumenInfracciones.FieldByName('Estado').asString = EI_SIN_CONFIRMAR then begin
			Navigator.JumpTo(TNavWindowDetalleViaje,
			  TNavWindowDetalleViaje.CreateBookMark(
			  ObtenerResumenInfracciones.FieldByName('InfraccionSaldoInsuficiente').asBoolean,
			  ObtenerResumenInfracciones.FieldByName('InfraccionTAGInhabilitado').asBoolean,
			  FCodigoPersona,
			  ObtenerResumenInfracciones.FieldByName('Concesionaria').asInteger,
			  ObtenerResumenInfracciones.FieldByName('NumeroViaje').asInteger,
			  ObtenerResumenInfracciones.FieldByName('Patente').asString,
			  ObtenerResumenInfracciones.FieldByName('DescripcionConcesionaria').asString,
			  ObtenerResumenInfracciones.FieldByName('ContractSerialNumber').asFloat,
			  ObtenerResumenInfracciones.FieldByName('FechaHoraInicio').asDateTime,
			  0,
			  ObtenerResumenInfracciones.FieldByName('PuntosCobro').asString,
			  ObtenerResumenInfracciones.FieldByName('ImporteViaje').asFloat));
		end else begin
			Navigator.JumpTo(TNavWindowDetalleInfraccion,
			  TNavWindowDetalleInfraccion.CreateBookMark(FCodigoPersona,
			  ObtenerResumenInfracciones.FieldByName('CodigoMotivoInfraccion').asInteger,
			  ObtenerResumenInfracciones.FieldByName('Patente').asString,
			  ObtenerResumenInfracciones.FieldByName('NumeroHistorialInfraccion').asInteger,
			  ObtenerResumenInfracciones.FieldByName('DescriMotivoInfraccion').asString,
			  ObtenerResumenInfracciones.FieldByName('Estado').asString,
			  ObtenerResumenInfracciones.FieldByName('FechaAlta').asDateTime,
			  ObtenerResumenInfracciones.FieldByName('FechaNotificacion').asDateTime,
			  ObtenerResumenInfracciones.FieldByName('FechaTramiteJudicial').asDateTime,
			  ObtenerResumenInfracciones.FieldByName('FechaRegularizacion').asDateTime,
			  ' ', 0.0, 0, 0, 0));
		end;
	end;
end;

procedure TNavWindowCliente.dg_OrdenesServicioDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
resourcestring
    VER = 'Ver...';
var
    Porc, tot, hecho, X0, X1: Integer;
begin
    if (Column = dg_OrdenesServicio.Columns[1]) then begin
        if (trim(ObtenerComunicaciones.FieldByName('Descripcion').AsString) <> '') then
            Text := VER
        else
            Text := '';
    end;

    if (Column = dg_OrdenesServicio.Columns[4]) then begin
//     not Sender.Datasource.dataset.FieldByName('CodigoOrdenServicio').IsNull then begin
        ItemWidth := 0;
        with Sender.Canvas, Sender.Datasource.dataset do begin
            if (FieldByName('DuracionMaxima').AsInteger = 0) or
               (FieldByName('Estado').AsString = ESTADO_TAREA_TERMINADA) then begin
                Porc := 100;
                Text := '0:00';
            end else begin
                Tot := FieldByName('DuracionMaxima').AsInteger;
                Hecho := Round((NowBase(DMConnections.BaseCAC) - FieldByName('FechaHoraInicio').AsDateTime) * 24 * 60);
                Porc := Min(Round(Hecho / Tot * 100), 100);
                Porc := Max(0, Porc);
                if Hecho > Tot then Text := '' else Text := TimeAsText((Tot - Hecho) / 24 / 60);
            end;
            x0 := TxtRect.Left + TextWidth(Text) + 4;
            if Porc <> 100 then begin
                if Porc = 0 then begin
                    Brush.Color := clWindow;
                    Rectangle(x0, Rect.Top + 2, Rect.Right - 4, Rect.Bottom - 2);
                end else begin
                    x1 := x0 + Round((Rect.Right - 4 - x0) * Porc / 100);
                    Brush.Color := RGB(255, 120, 120);
                    Rectangle(x0, Rect.Top + 2, x1, Rect.Bottom - 2);
                    Brush.Color := clWindow;
                    Rectangle(x1 - 1, Rect.Top + 2, Rect.Right - 4, Rect.Bottom - 2);
                end;
            end;
        end;
    end;
end;

procedure TNavWindowCliente.dg_OrdenesServicioCheckLink(Sender: TCustomDBListEx;
  Column: TDBListExColumn; var IsLink: Boolean);
begin
    if (Column = dg_OrdenesServicio.Columns[1]) then begin
        IsLink := (trim(ObtenerComunicaciones.FieldByName('Descripcion').AsString) <> '');
    end;

end;

procedure TNavWindowCliente.dg_OrdenesServicioLinkClick(Sender: TCustomDBListEx;
  Column: TDBListExColumn);
resourcestring
    MSG_ORDEN_SERVICIO     = 'El contacto no tiene una orden de servicio asociada.';
    CAPTION_ORDEN_SERVICIO = 'Ver Orden de Servicio';
var f: TFormWorkflowDisplay;
begin
	if (Column.FieldName = 'Ver') and (not ObtenerComunicaciones.isEmpty) then begin
		if trim(ObtenerComunicaciones.FieldByName('Descripcion').AsString) = '' then begin
			msgBox(MSG_ORDEN_SERVICIO, CAPTION_ORDEN_SERVICIO, MB_OK)
		end else begin
			Application.CreateForm(TFormWorkflowDisplay, f);
			if f.Inicializa(ObtenerComunicaciones.FieldByName('CodigoOrdenServicio').AsInteger,
			  ObtenerComunicaciones.FieldByName('CodigoWorkflow').AsInteger) then
				f.ShowModal;
			f.release;
		end;
	end;
end;

end.

