{********************************** File Header ********************************
File Name   : FrmRptNotaCobroInfractor.pas
Author      : lgisuk
Date Created: 07/12/05
Language    : ES-AR
Description : Nota de Cobro para Infractores
Revision :
    Author : vpaszkowicz
    Date : 20/03/2007
    Description : Agrego el dpto en la descripci�n del domicilio.
Revision 2:
    Author : nefernandez
    Date : 21/08/2007
    Description : SS 548: Cambio del atributo FNumeroComprobante y del par�metro
    NumeroComprobante (funci�n Inicializar) de Integer a Int64.

Revision 3
Author: mbecerra
Date: 22-Abril-2009
Description:	(Petici�n Cliente)
        	Se agrega una leyenda fija en la parte de "mensaje al Cliente"
            
*******************************************************************************}
unit frmRptNotaCobroInfractor;

interface

uses
   //Nota de Cobro para Infractores
   DMConnection,
   util,
   utilproc,
   UtilDb,
   PeaTypes,
   ConstParametrosGenerales,
   //Otros
   Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
   Dialogs, ppBands, ppReport, ppSubRpt, {ppChrt, ppChrtDP, }ppCtrls, ppStrtch,
   ppMemo, ppParameter, ppBarCod, jpeg, ppPrnabl, ppClass, UtilRB, ppCache,
   ppComm, ppRelatv, ppProd, ppModule, raCodMod, DB, ADODB, ppDB, ppDBPipe,
   ppVar, StdCtrls, daDataModule, ppRegion, ppCTMain, ppTxPipe, rbSetup,
   TeEngine, Series, ExtCtrls, TeeProcs, Chart, DbChart, TXComp, ppDevice,
   Printers, ImprimirWO, TXRB;

type
  TfRptNotaCobroInfractor = class(TForm)
    rpt_ReporteNotaCobroInfractor: TppReport;
    rb_ReporteNotaCobroInfractor: TRBInterface;
    ppParameterList1: TppParameterList;
    sp_ObtenerNotaCobroInfractor: TADOStoredProc;
    ppHeaderBand1: TppHeaderBand;
    ppRegion1: TppRegion;
    ppShape4: TppShape;
    pplblNumero: TppLabel;
    ppShape6: TppShape;
    ppShape5: TppShape;
    ppShape3: TppShape;
    ppLabel4: TppLabel;
    ppLabel15: TppLabel;
    ppShape7: TppShape;
    ppShape8: TppShape;
    ppShape9: TppShape;
    ppLine4: TppLine;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLine9: TppLine;
    pplblFechaEmision: TppLabel;
    pplblVencimiento: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppShape18: TppShape;
    ppShape17: TppShape;
    ppLabel25: TppLabel;
    ppLabel13: TppLabel;
    ppBoletoInfractor: TppLabel;
    ppLabel34: TppLabel;
    ppImporteBoletoInfractor: TppLabel;
    pplblTotalNotaCobro: TppLabel;
    pplblTotalAPagar: TppLabel;
    ppDetalleConsumo: TppLabel;
    ppsb_Lista: TppSubReport;
    ppChildReport1: TppChildReport;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLine1: TppLine;
    ppDetailBand2: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppSummaryBand3: TppSummaryBand;
    pplblNumero2: TppLabel;
    pplblFechaVencimiento2: TppLabel;
    pplblTotalAPagar2: TppLabel;
    ppLabel29: TppLabel;
    raCodeModule3: TraCodeModule;
    ppHeaderBand2: TppHeaderBand;
    raCodeModule1: TraCodeModule;
    pplblDatosLinea1: TppLabel;
    pplblDatosLinea2: TppLabel;
    pplblDatosLinea3: TppLabel;
    dbpl_ObtenerInfraccionesComprobante: TppDBPipeline;
    sp_ObtenerInfraccionesComprobante: TADOStoredProc;
    dsObtenerInfraccionesComprobante: TDataSource;
    ppImporteInteres: TppLabel;
    ppInteres: TppLabel;
    ppGastosAdministrativos: TppLabel;
    ppImporteGastosAdministrativos: TppLabel;
    ppImagenFondo: TppImage;
    ppDBText3: TppDBText;
    ppLabel1: TppLabel;
    ppMemo1: TppMemo;
    procedure rb_ReporteNotaCobroInfractorExecute(Sender: TObject;var Cancelled: Boolean);
   private
    { Private declarations }
    FNumeroComprobante : Int64;
  public
    { Public declarations }
    function Inicializar(NumeroComprobante: Int64): Boolean;
  end;

var
  fRptNotaCobroInfractor : TfRptNotaCobroInfractor;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 07/12/2005
  Description:  Inicializaci�n de Este Formulario
  Parameters: None
  Return Value: Boolean

  Revision 1:
      Author : ggomez
      Date : 22/06/2006
      Description :
        - Agregu� el c�digo para levantar la imagen de fondo para la Nota de
        Cobro a Infracciones.
        - Agregu� nolock al query que obtiene la Fecha de Creaci�n.

-----------------------------------------------------------------------------}
function TfRptNotaCobroInfractor.Inicializar(NumeroComprobante: Int64): Boolean;
resourcestring
    MSG_ERROR_OBTENER_DATOS_CONFIG_REPORTE = 'Error al obtener datos de configuraci�n del reporte.'
                                                + CRLF + 'Consulte al Administrador de Sistemas.';
    MSG_ERROR_OBTENER_FECHA_CORTE_RUT = 'Ha ocurrido un error al obtener la fecha desde la cual la concesionaria cambio el RUT.';
    MSG_ERROR_OBTENER_DATOS_IMG_FONDO = 'Ha ocurrido un error al obtener los datos de la imagen de fondo.';
    MSG_ERROR_FALTAN_IMAGENES = 'No existen todas las im�genes requeridas para'
        + CRLF + 'poder imprimir una Nota de Cobro a Infracciones en el sistema.';
    MSG_ERROR_OBTENER_IMG_FONDO = 'Ha ocurrido un error al obtener la imagen de fondo.';
var
  FechaCreacion, FechaDeCorte: TDateTime;
  DirImagenNotaCobroInfractor, NombreImagenNotaCobroInfractor: String;
begin
    FNumeroComprobante := NumeroComprobante;
    //revision 1:
    // Obtener la Fecha de Creaci�n del Recibo.
    try
        FechaCreacion := QueryGetValueDateTime(sp_ObtenerNotaCobroInfractor.Connection,
          ' select FechaCreacion '
          + ' from Comprobantes with (nolock)'
          + ' where (NumeroComprobante = ''' + IntToStr(FNumeroComprobante) + ''')'
            + ' and (TipoComprobante = ''' + TC_NOTA_COBRO_INFRACCIONES + ''')');

    except
        on E: Exception do begin
            // Mensja de error
            MsgBoxErr(MSG_ERROR_OBTENER_DATOS_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
            // Como ocurri� un error, salir con False.
            Result := False;
            Exit;
        end;
    end;

    try
        // Obtener la fecha desde cuando la concesionaria tiene un nuevo RUT.
        ObtenerParametroGeneral(sp_ObtenerNotaCobroInfractor.Connection, 'FECHA_DE_CORTE_RUT', FechaDeCorte);
    except
        on E: Exception do begin
            // Mensja de error
            MsgBoxErr(MSG_ERROR_OBTENER_FECHA_CORTE_RUT, E.Message, Self.Caption, MB_ICONSTOP);
            // Como ocurri� un error, salir con False.
            Result := False;
            Exit;
        end;
    end; // except


    // Si la Fecha de Creaci�n del Recibo es anterior a la Fecha desde cuando la
    // concesionaria tiene nuevo RUT, entonces imprimir el RUT Anterior, sino
    // imprimir el RUT Nuevo.
    try
        if ( FechaCreacion < FechaDeCorte ) then begin
           ObtenerParametroGeneral(sp_ObtenerNotaCobroInfractor.Connection, 'IMG_FONDO_NOTA_COBRO_INFRAC_ANT',
                                   NombreImagenNotaCobroInfractor);
        end else begin
           ObtenerParametroGeneral(sp_ObtenerNotaCobroInfractor.Connection, 'IMG_FONDO_NOTA_COBRO_INFRAC',
                                   NombreImagenNotaCobroInfractor);
        end;

        ObtenerParametroGeneral(sp_ObtenerNotaCobroInfractor.Connection, 'DIR_IMG_FONDO_NOTA_COBRO_INFRAC',
                                DirImagenNotaCobroInfractor);

    except
        on E: Exception do begin
            // Mensja de error
            MsgBoxErr(MSG_ERROR_OBTENER_DATOS_IMG_FONDO, E.Message, Self.Caption, MB_ICONSTOP);
            // Como ocurri� un error, salir con False.
            Result := False;
            Exit;
        end;
    end; // except

    DirImagenNotaCobroInfractor := GoodDir(DirImagenNotaCobroInfractor);
    DirImagenNotaCobroInfractor := DirImagenNotaCobroInfractor + NombreImagenNotaCobroInfractor;

    try
        if (FileExists(DirImagenNotaCobroInfractor)) then begin
            ppImagenFondo.Picture.LoadFromFile(DirImagenNotaCobroInfractor);
        end else begin
            raise Exception.Create(MSG_ERROR_OBTENER_IMG_FONDO);
        end;
    except
        on E: Exception do begin
            // Mensja de error
            MsgBoxErr(MSG_ERROR_OBTENER_IMG_FONDO, E.Message, Self.Caption, MB_ICONSTOP);
            // Como ocurri� un error, salir con False.
            Result := False;
            Exit;
        end;
    end; // except

    //fin revision 1:

    Result := rb_ReporteNotaCobroInfractor.Execute;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 07/12/2005
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRptNotaCobroInfractor.rb_ReporteNotaCobroInfractorExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_ERROR_INVOICE_NOT_FOUND = 'No se encuentra el Comprobante';
    MSG_ERROR_PRINTING_THE_COLLECTION_NOTE = 'Se ha producido un error al imprimir la Nota de Cobro';
const
    MAXIMUM_NUMBER_INFRACTIONS = 15;
begin
    try
        with sp_ObtenerNotaCobroInfractor, Parameters do begin
            Close;
            Refresh;
            ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
            CommandTimeOut := 500;
            Open;
            if not IsEmpty then begin
                //Comprobante
                pplblNumero.Caption := FieldByName('NumeroComprobante').AsString;
                pplblVencimiento.Caption := DateToStr(FieldByName('FechaVencimiento').AsDateTime);
                pplblFechaEmision.Caption := DateToStr(FieldByName('FechaEmision').AsDateTime);
                ppBoletoInfractor.Caption := FieldByName('DescriBoletoInfractor').AsString;
                ppImporteBoletoInfractor.Caption := FieldByName('ImporteBoletoInfractorStr').AsString;
                ppGastosAdministrativos.Caption := FieldByName('DescriGastosAdministrativos').AsString;
                ppImporteGastosAdministrativos.Caption := FieldByName('ImporteGastosAdministrativosStr').AsString;
                ppInteres.Caption := FieldByName('DescriIntereses').AsString;
                ppImporteInteres.Caption := FieldByName('ImporteInteresesStr').AsString;
                pplblTotalNotaCobro.Caption := FieldByName('DescriAPagar').AsString;
                pplblTotalAPagar.Caption := FieldByName('DescriAPagar').AsString;

                // Datos del Cliente
                pplblDatosLinea1.Caption := FieldByName('Apellido').AsString + ' ' + FieldByName('ApellidoMaterno').AsString + ' ' + FieldByName('Nombre').AsString;
                pplblDatosLinea2.Caption := FieldByName('Calle').AsString + ' ' + FieldByName('Numero').AsString + ' ' + FieldByName('DetalleDomicilio').AsString;
                pplblDatosLinea3.Caption := FieldByName('Comuna').AsString + ' - ' + FieldByName('Region').AsString;
                //Talon
                pplblNumero2.Caption := FieldByName('NumeroComprobante').AsString;
                pplblTotalAPagar2.Caption := FieldByName('DescriAPagar').AsString;
                pplblFechaVencimiento2.Caption := DateToStr(FieldByName('FechaVencimiento').AsDateTime);
                //Detalle Consumo
                ppDetalleConsumo.Visible := False;

                ppImporteGastosAdministrativos.Visible := FieldByName('ImporteGastosAdministrativos').AsFloat <> 0;
                ppImporteInteres.Visible := FieldByName('ImporteIntereses').AsFloat <> 0;
                ppGastosAdministrativos.Visible := FieldByName('ImporteGastosAdministrativos').AsFloat <> 0;
                ppInteres.Visible := FieldByName('ImporteIntereses').AsFloat <> 0;
            end else begin
                Cancelled := True;
                MsgBox(MSG_ERROR_INVOICE_NOT_FOUND, Self.Caption, MB_ICONERROR);
                Exit;
            end;
        end;

        //Obtengo las infracciones incluidas en el comprobante
        with sp_ObtenerInfraccionesComprobante, Parameters do begin
            Close;
            Refresh;
            ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
            ParamByName('@CantidadInfracciones').Value := 0;
            CommandTimeOut := 500;
            Open;
            //si la cantidad de infracciones es mayor que el maximo admitido a mostrar
            if ParamByName('@CantidadInfracciones').Value > MAXIMUM_NUMBER_INFRACTIONS then begin
                  //Muestro Mensaje "Detalle de Consumo en anexo"
                  ppDetalleConsumo.Visible := True;
                  //No muestro las infracciones
                  ppsb_lista.Visible := False;
            end;
        end;

    except
        on E : Exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR_PRINTING_THE_COLLECTION_NOTE, E.Message, Self.Caption, MB_ICONERROR);
        end;
    end;
end;

end.
