unit FrmGestionCuentas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, DB, ADODB, StdCtrls, ExtCtrls, DmiCtrls,
  validate, Dateedit, Grids, DBGrids, DMConnection, util, peaprocs,
  Utildb, utilproc, BuscaClientes, FrmMantenimientoCuenta, AltaOrdenServicio,
  Peatypes, MaskCombo, DPSControls, ListBoxEx, DBListEx;

type
  TFormGestionCuentas = class(TForm)
	Grilla: TDBListEx;
    DataSource1: TDataSource;
    pop_Cuentas: TPopupMenu;
    mnu_cierre: TMenuItem;
    mnu_inh_tag: TMenuItem;
    mnu_reh_tag: TMenuItem;
    Panel1: TPanel;
	txt_Apellido: TEdit;
    txt_Nombre: TEdit;
    Label5: TLabel;
    Label7: TLabel;
    Label3: TLabel;
    BuscarClienteContacto: TADOStoredProc;
    Panel2: TPanel;
    ObtenerDatosCuentasCliente: TADOStoredProc;
    cbDocumento: TMaskCombo;
    Label1: TLabel;
    txt_ApellidoMaterno: TEdit;
    btn_Buscar: TDPSButton;
    btn_editar: TDPSButton;
    btn_Close: TDPSButton;
    procedure pop_CuentasPopup(Sender: TObject);
    procedure mnu_cierreClick(Sender: TObject);
    procedure mnu_inh_tagClick(Sender: TObject);
    procedure mnu_reh_tagClick(Sender: TObject);
	procedure ActualizarGrilla;
    procedure btn_BuscarClick(Sender: TObject);
    procedure ActualizarBusqueda(Sender: TObject);
    procedure btn_editarClick(Sender: TObject);
    procedure btn_CloseClick(Sender: TObject);
    procedure GrillaDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbDocumentoChange(Sender: TObject);
  private
    { Private declarations }
   	FCodigoCliente 		: integer;
	FCodigoCuenta		: integer;
    FActualizarBusqueda : boolean;
  public
    { Public declarations }
    Function Inicializa(Cliente, Cuenta : integer): boolean;
  end;

var
  FormGestionCuentas: TFormGestionCuentas;

implementation

{$R *.dfm}

Function TFormGestionCuentas.Inicializa(Cliente, Cuenta: integer): Boolean;
Var
	Sz: TSize;
begin
	SZ := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, SZ.cx, sz.cy);

	with Grilla do begin
		Columns[0].Header.Caption := MSG_COL_TITLE_CUENTA;
		Columns[1].Header.Caption := MSG_COL_TITLE_TIPO_DE_PAGO;
		Columns[2].Header.Caption := MSG_COL_TITLE_VEHICULO;
		Columns[3].Header.Caption := MSG_COL_TITLE_ESTADO;
	end;

	FCodigoCliente 		:= Cliente;
	FCodigoCuenta		:= Cuenta;
	CargarTiposDocumento(DMConnections.BaseCAC, cbDocumento, 'RNU');
	FActualizarBusqueda	:= True;
	Result 				:= True;
end;

procedure TFormGestionCuentas.pop_CuentasPopup(Sender: TObject);
Var
	TagOk: Boolean;
    ContextMark: integer;
	ContractSerialNumber: Double;
begin
	mnu_cierre.Enabled := ObtenerDatosCuentasCliente.Active and not ObtenerDatosCuentasCliente.Eof
	  and ObtenerDatosCuentasCliente.FieldByName('FechaBaja').IsNull;
    ContextMark				:= ObtenerDatosCuentasCliente.FieldByName('ContextMark').AsInteger;
	ContractSerialNumber	:= ObtenerDatosCuentasCliente.FieldByName('ContractSerialNumber').AsFloat;
	TagOk := QueryGetValueInt(DMCOnnections.BaseCAC,
	  'SELECT 1 FROM TagsAsignados WHERE ' +
      '	ContextMark = ' + IntToSTr(ContextMark) + ' ' +
      'AND ContractSerialNumber = ' + FloatToStr(ContractSerialNumber) + ' ' +
	  'AND FechaHoraBaja IS NULL') = 1;
	mnu_inh_tag.Enabled := mnu_cierre.Enabled and (ContractSerialNumber > 0) and TagOk;
	mnu_reh_tag.Enabled := mnu_cierre.Enabled and (ContractSerialNumber > 0) and not TagOk;
end;

procedure TFormGestionCuentas.mnu_cierreClick(Sender: TObject);
Var
	f: TFormAltaOrdenServicio;
begin
	Application.CreateForm(TFormAltaOrdenServicio, f);
//	if f.InicializaCierreCuenta(gCodigoComunicacion,
//	  ObtenerDatosCuentasCliente.FieldByName('CodigoCuenta').AsInteger) then f.ShowModal;
	f.Release;
end;

procedure TFormGestionCuentas.mnu_inh_tagClick(Sender: TObject);
Var
	f: TFormAltaOrdenServicio;
begin
	Application.CreateForm(TFormAltaOrdenServicio, f);
 	//if f.InicializaInhabilitarTag(CodigoComunicacion,
    //  ObtenerDatosCuentasCliente.FieldByName('ContextMark').AsString,
 	//  ObtenerDatosCuentasCliente.FieldByName('ContractSerialNumber').AsString) then f.ShowModal;
	f.Release;
end;

procedure TFormGestionCuentas.mnu_reh_tagClick(Sender: TObject);
Var
	f: TFormAltaOrdenServicio;
begin
	Application.CreateForm(TFormAltaOrdenServicio, f);
 //	if f.InicializaRehabilitarTag(gCodigoComunicacion,
 //	  ObtenerDatosCuentasCliente.FieldByName('ContextMark').AsString,
 //	  ObtenerDatosCuentasCliente.FieldByName('ContractSerialNumber').AsString) then f.ShowModal;
	f.Release;
end;

procedure TFormGestionCuentas.ActualizarGrilla;
resourcestring
    MSG_OBTENER_CUENTAS = 'No se pudieron obtener las Cuentas del Cliente.';
    CAPTION_OBTENER_CUENTAS = 'Obtener Cuentas del Cliente';
begin
    try
	    with ObtenerDatosCuentasCliente do begin
            Close;
            Parameters.ParamByName('@CodigoCliente').value := FCodigoCliente;
            Open;
            btn_Editar.Enabled := RecordCount > 0;
        end;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_OBTENER_CUENTAS, e.Message, CAPTION_OBTENER_CUENTAS, MB_ICONSTOP);
            ObtenerDatosCuentasCliente.Close;
        end;
    end;
    Grilla.Refresh;
end;

procedure TFormGestionCuentas.btn_BuscarClick(Sender: TObject);
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes , f);
    if f.Inicializa(cbDocumento.MaskText, cbDocumento.ComboText, txt_Apellido.Text, txt_ApellidoMaterno.Text, txt_Nombre.Text) then begin
    	if (f.ShowModal = mrok) then begin
			FCodigoCliente          := f.Persona.CodigoPersona;
            FActualizarBusqueda := False;
            CargarTiposDocumento(DMConnections.BaseCAC, cbDocumento, Trim(f.Persona.TipoDocumento));
            cbDocumento.MaskText    := f.Persona.NumeroDocumento;
  			txt_Apellido.Text	    := Trim(f.Persona.Apellido);
            txt_ApellidoMaterno.Text:= Trim(f.Persona.ApellidoMaterno);
			txt_Nombre.Text		    := Trim(f.Persona.Nombre);
            FActualizarBusqueda     := True;
            ActualizarBusqueda(nil);
	   	end;
	end;
    f.Release;
end;

procedure TFormGestionCuentas.ActualizarBusqueda(Sender: TObject);
resourcestring
    MSG_OBTENER_CLIENTE = 'No se pudieron obtener los datos del Cliente.';
    CAPTION_OBTENER_CLIENTE = 'Obtener datos del Cliente';
begin
	if not FActualizarBusqueda then exit;
   	Screen.Cursor := crHourGlass;

    if (Sender <> nil) then begin
        FCodigoCLiente	:= 0;
        try
   	        BuscarClienteContacto.Close;
			//primero los vacio a todos
            with BuscarClienteContacto.Parameters do begin
                ParamByName('@TipoContacto').Value	    := TIPO_CLIENTE;
                ParamByName('@Patente').Value 			:= null;
                ParamByName('@CodigoCuenta').Value		:= null;
				ParamByName('@CodigoDocumento').Value	:= null;
            	ParamByName('@NumeroDocumento').Value	:= null;
            	ParamByName('@Apellido').value          := null;
                ParamByName('@ApellidoMaterno').value   := null;
            	ParamByName('@Nombre').value			:= null;
            end;
            if (Sender = cbDocumento) then begin
                FActualizarBusqueda := False;
                txt_Apellido.text := '';
                txt_ApellidoMaterno.text := '';
                txt_Nombre.text := '';
                FActualizarBusqueda := True;
	            //BuscarClienteContacto.Parameters.ParamByName('@CodigoDocumento').Value	:= IIf(cbDocumento.ItemIndex > -1, Trim(StrRight(cbDocumento.ComboText, 20)), null);
                BuscarClienteContacto.Parameters.ParamByName('@CodigoDocumento').Value	:= Trim(StrRight(cbDocumento.ComboText, 20));
    	        //BuscarClienteContacto.Parameters.ParamByName('@NumeroDocumento').Value	:= IIf(cbDocumento.MaskText <> '', Trim(cbDocumento.MaskText), null);
                BuscarClienteContacto.Parameters.ParamByName('@NumeroDocumento').Value := Trim(cbDocumento.MaskText);
			end else begin
                FActualizarBusqueda := False;
                cbDocumento.MaskText := '';
                FActualizarBusqueda := True;
	            BuscarClienteContacto.Parameters.ParamByName('@Apellido').value         := IIf(Trim(txt_apellido.Text) <> '', Trim(txt_apellido.Text), null);
                BuscarClienteContacto.Parameters.ParamByName('@ApellidoMaterno').value  := IIf(Trim(txt_apellidoMaterno.Text) <> '', Trim(txt_apellidoMaterno.Text), null);
    	        BuscarClienteContacto.Parameters.ParamByName('@Nombre').value			:= IIf(Trim(txt_nombre.Text) <> '', Trim(txt_nombre.Text), null);
            end;
            BuscarClienteContacto.Open;
            if (BuscarClienteContacto.RecordCount = 1) then begin
       	    	FActualizarBusqueda  	:= False;
	            FCodigoCLiente			:= BuscarClienteContacto.FieldByName('CodigoCliente').AsInteger;
				CargarTiposDocumento( DMConnections.BaseCAC, cbDocumento, Trim(BuscarClienteContacto.FieldByName('CodigoDocumento').AsString) );
                cbDocumento.MaskText    := Trim(BuscarClienteContacto.FieldByName('NumeroDocumento').AsString);
				txt_Apellido.Text		:= Trim(BuscarClienteContacto.FieldByName('Apellido').AsString);
                txt_ApellidoMaterno.Text:= Trim(BuscarClienteContacto.FieldByName('ApellidoMaterno').AsString);
				txt_Nombre.Text 		:= Trim(BuscarClienteContacto.FieldByName('Nombre').AsString);
                FActualizarBusqueda 	:= True;
            end;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_OBTENER_CLIENTE, e.message, CAPTION_OBTENER_CLIENTE, MB_ICONSTOP);
                BuscarClienteContacto.Close;
            end;
        end;
	end;
    ActualizarGrilla;
	Screen.Cursor := crDefault;
end;

procedure TFormGestionCuentas.btn_editarClick(Sender: TObject);
var
	f: TFormMantenimientoCuenta;
begin
    Application.CreateForm(TFormMantenimientoCuenta,f);
    if f.inicializa(0, ObtenerDatosCuentasCliente.FieldByName('CodigoCuenta').AsInteger) then f.ShowModal;
    f.Release
end;

procedure TFormGestionCuentas.btn_CloseClick(Sender: TObject);
begin
	Close;
end;

procedure TFormGestionCuentas.GrillaDblClick(Sender: TObject);
begin
	if not BuscarClienteContacto.IsEmpty then btn_Editar.OnClick(btn_Editar);
end;

procedure TFormGestionCuentas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormGestionCuentas.cbDocumentoChange(Sender: TObject);
begin
//    if (Sender as TMaskCombo). = ActiveControl then
        ActualizarBusqueda(Sender);
end;

end.
