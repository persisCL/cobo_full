object FormSolicitud: TFormSolicitud
  Left = 96
  Top = 149
  Width = 809
  Height = 300
  Caption = 'Listado de Solicitudes'
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 809
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 801
    Height = 102
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel1'
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 801
      Height = 102
      Align = alClient
      Caption = 'Filtros'
      TabOrder = 0
      DesignSize = (
        801
        102)
      object Label1: TLabel
        Left = 11
        Top = 24
        Width = 131
        Height = 13
        Caption = 'Fecha de Creacion:  &desde:'
        FocusControl = txt_FechaDesde
      end
      object Label2: TLabel
        Left = 248
        Top = 24
        Width = 29
        Height = 13
        Caption = '&hasta:'
        FocusControl = txt_FechaHasta
      end
      object Label3: TLabel
        Left = 388
        Top = 24
        Width = 36
        Height = 13
        Caption = 'Fuente:'
        FocusControl = cb_TipoSolicitud
      end
      object Label4: TLabel
        Left = 595
        Top = 24
        Width = 73
        Height = 13
        Caption = '&Vehiculos >= a:'
        FocusControl = txt_CantVehiculos
      end
      object Label5: TLabel
        Left = 11
        Top = 51
        Width = 53
        Height = 13
        Caption = 'Personer&ia:'
        FocusControl = cb_Personeria
      end
      object Label6: TLabel
        Left = 251
        Top = 51
        Width = 26
        Height = 13
        Caption = '&RUT:'
        FocusControl = txt_rut
      end
      object Label7: TLabel
        Left = 383
        Top = 51
        Width = 66
        Height = 13
        Caption = '&Apellido / RS:'
        FocusControl = txt_Apellido
      end
      object Label8: TLabel
        Left = 595
        Top = 51
        Width = 40
        Height = 13
        Caption = '&Patente:'
        FocusControl = txt_Patente
      end
      object Label9: TLabel
        Left = 11
        Top = 76
        Width = 79
        Height = 13
        Caption = 'Estado &Solicitud:'
        FocusControl = cb_EstadoSolicitud
      end
      object Label11: TLabel
        Left = 251
        Top = 76
        Width = 42
        Height = 13
        Caption = 'Limitar a:'
      end
      object txt_FechaDesde: TDateEdit
        Left = 148
        Top = 19
        Width = 91
        Height = 21
        AutoSelect = False
        TabOrder = 0
        Date = -693594.000000000000000000
      end
      object txt_FechaHasta: TDateEdit
        Left = 284
        Top = 19
        Width = 91
        Height = 21
        AutoSelect = False
        TabOrder = 1
        Date = 36526.000000000000000000
      end
      object cb_TipoSolicitud: TComboBox
        Left = 441
        Top = 19
        Width = 144
        Height = 21
        Style = csDropDownList
        ItemHeight = 0
        TabOrder = 2
      end
      object txt_CantVehiculos: TNumericEdit
        Left = 676
        Top = 17
        Width = 33
        Height = 21
        TabOrder = 3
        Decimals = 0
      end
      object cb_Personeria: TComboBox
        Left = 114
        Top = 46
        Width = 127
        Height = 21
        Style = csDropDownList
        ItemHeight = 0
        TabOrder = 4
      end
      object txt_Apellido: TEdit
        Left = 454
        Top = 44
        Width = 132
        Height = 21
        TabOrder = 6
        OnKeyPress = txt_ApellidoKeyPress
      end
      object txt_rut: TEdit
        Left = 284
        Top = 46
        Width = 91
        Height = 21
        Hint = 'N'#250'mero de documento'
        CharCase = ecUpperCase
        MaxLength = 9
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnKeyPress = txt_rutKeyPress
      end
      object txt_Patente: TEdit
        Left = 656
        Top = 43
        Width = 53
        Height = 21
        TabOrder = 7
        OnKeyPress = txt_ApellidoKeyPress
      end
      object cb_EstadoSolicitud: TComboBox
        Left = 113
        Top = 72
        Width = 128
        Height = 21
        Style = csDropDownList
        ItemHeight = 0
        TabOrder = 8
      end
      object txt_Top: TNumericEdit
        Left = 317
        Top = 72
        Width = 57
        Height = 21
        TabOrder = 11
        OnExit = txt_TopExit
        Decimals = 0
      end
      object btn_Filtrar: TButton
        Left = 717
        Top = 16
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Anchors = [akTop, akRight]
        Caption = '&Filtrar'
        Default = True
        TabOrder = 9
        OnClick = btn_FiltrarClick
      end
      object btn_Limpiar: TButton
        Left = 717
        Top = 48
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Anchors = [akTop, akRight]
        Caption = '&Limpiar'
        TabOrder = 10
        OnClick = btn_LimpiarClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 102
    Width = 801
    Height = 123
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 1
    object dbl_Solicitud: TDBListEx
      Left = 0
      Top = 0
      Width = 801
      Height = 123
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 75
          Header.Caption = 'Nro. Solicitud'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'CodigoSolicitudContacto'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 76
          Header.Caption = 'RUT'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'RUT'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 320
          Header.Caption = 'Nombre Completo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'Nombre'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 86
          Header.Caption = 'Cant. Vehiculos'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'CantidadVehiculos'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 130
          Header.Caption = 'Hora de Creacion'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'FechaHoraCreacion'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 62
          Header.Caption = 'Personeria'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'PersoneriaDescrip'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'DescripcionEstadoSolicitud'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'NumeroConvenio'
        end>
      DataSource = DS_SolicitudContacto
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = dbl_SolicitudDblClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 225
    Width = 801
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 801
      Height = 41
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        801
        41)
      object lbl_Mostar: TLabel
        Left = 184
        Top = 16
        Width = 434
        Height = 13
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'lbl_Mostar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BtnSalir: TButton
        Left = 714
        Top = 7
        Width = 79
        Height = 26
        Hint = 'Salir del visualizador de Solicitudes'
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Salir'
        TabOrder = 3
        OnClick = BtnSalirClick
      end
      object btn_Ver: TButton
        Left = 632
        Top = 7
        Width = 79
        Height = 26
        Hint = 'Detalle de la solicitud'
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Ver Detalle'
        TabOrder = 2
        OnClick = btn_VerClick
      end
      object btn_Editar: TButton
        Left = 11
        Top = 7
        Width = 79
        Height = 26
        Hint = 'Modificar la solicitud'
        Anchors = [akLeft, akBottom]
        Cancel = True
        Caption = '&Editar'
        TabOrder = 0
        OnClick = btn_EditarClick
      end
      object btn_Eliminar: TButton
        Left = 93
        Top = 7
        Width = 79
        Height = 26
        Hint = 'Eliminar la solicitud'
        Anchors = [akLeft, akBottom]
        Cancel = True
        Caption = '&Eliminar'
        TabOrder = 1
        OnClick = btn_EliminarClick
      end
    end
  end
  object ObtenerSolicitudContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterOpen = ObtenerSolicitudContactoAfterOpen
    AfterClose = ObtenerSolicitudContactoAfterOpen
    AfterScroll = ObtenerSolicitudContactoAfterScroll
    ProcedureName = 'ObtenerSolicitudContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraCreacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraFin'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoFuenteSolicitud'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoEstadoSolicitud'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CantidadVehiculos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@RUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@TOP'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@OrderBy'
        Attributes = [paNullable]
        DataType = ftFixedChar
        Size = 100
        Value = Null
      end>
    Left = 152
    Top = 144
  end
  object DS_SolicitudContacto: TDataSource
    DataSet = ObtenerSolicitudContacto
    Left = 184
    Top = 144
  end
  object EliminarSolicitudContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = ObtenerSolicitudContactoAfterOpen
    AfterClose = ObtenerSolicitudContactoAfterOpen
    ProcedureName = 'EliminarSolicitudContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 152
    Top = 176
  end
end
