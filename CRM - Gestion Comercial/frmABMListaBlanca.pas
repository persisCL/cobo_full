{           frmABMListaBlanca
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 15-Abril-2010
Description : ABM de la Lista Blanca para AMB.

Revision	:1
Author		: Nelson Droguett
Date		: 15-Abril-2010
Description	: Al poner una patente, obtiene la etiqueta y la categoria de la BD
              Al dar de alta o de baja, actualiza el campo HabilitadoAMB de la cuenta.

Revision	:2
Author		: Nelson Droguett
Date		: 30-Abril-2010
Description	: Que no pueda usar el boton editar del ABM
            	Que no pueda modificar los datos de los controles Etiqueta y Categoria.
                Si la patente no esta completa, no puede grabar
}
unit frmABMListaBlanca;

interface

uses
    DMConnection,
    PeaProcs,
    UtilProc,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, DmiCtrls, Buttons, ListBoxEx, DBListEx, DB, ADODB,
  VariantComboBox,utildb;

type
  TABMListaBlancaForm = class(TForm)
    Panel1: TPanel;
    btnNuevo: TSpeedButton;
    btnEliminar: TSpeedButton;
    btnSalir: TButton;
    gbxEdicion: TGroupBox;
    btnCancelar: TButton;
    btnAceptar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    dblLista: TDBListEx;
    spObtenerListaBlanca: TADOStoredProc;
    dsListaBlanca: TDataSource;
    Label4: TLabel;
    edtPatente: TEdit;
    spActualizarRegistroEnListaBlanca: TADOStoredProc;
    edtEtiqueta: TEdit;
    vcbCategorias: TVariantComboBox;
    Label3: TLabel;
    vcbNovedad: TVariantComboBox;
    btnEditar: TSpeedButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnNuevoClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure dsListaBlancaDataChange(Sender: TObject; Field: TField);
    procedure btnEliminarClick(Sender: TObject);
    procedure edtPatenteExit(Sender: TObject);
    procedure edtPatenteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure vcbNovedadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPatenteChange(Sender: TObject);
    procedure vcbNovedadEnter(Sender: TObject);
  private
    { Private declarations }
    FEdicion : byte;		{ 0 = browse, 1 = Nuevo, 2 = Edici�n}
  public
    { Public declarations }
    function Inicializar : boolean;
    procedure ObtenerCategorias;
    procedure ObtenerListaBlanca;
    procedure ActivarBotones(EsActivar : boolean; Grupo : byte);
    procedure LimpiaCampos;
  end;

var
  ABMListaBlancaForm: TABMListaBlancaForm;

implementation

{$R *.dfm}

const
    cEdicionBrowse		= 0;
    cEdicionInsertar	= 1;
    cEdicionEditar		= 2;
    cGrupoToolbar		= 1;		//botones Nuevo, Editar, Eliminar, Buscar
    cGrupoSalir			= 2;        //bot�n Salir
    cGrupoEdicion		= 3;		//Botones Aceptar y Cancelar
    cGrupoBrowse		= 4;		//botones Editar y Eliminar
    
{           ActivarBotones
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	: Habilita los campos para edici�n
}
procedure TABMListaBlancaForm.ActivarBotones;
begin
	case Grupo of
    	cGrupoToolbar :
        	begin
        		btnNuevo.Enabled := EsActivar;
        		btnEliminar.Enabled := EsActivar;
                //Rev.2 / 30-Abril-2010 / Nelson Droguett Sierra
        		//btnEditar.Enabled := EsActivar;
        	end;

    	cGrupoSalir :
        	begin
            	btnSalir.Enabled := EsActivar;
        	end;

        cGrupoEdicion :
        	begin
            	gbxEdicion.Enabled	:= EsActivar;
    			btnAceptar.Enabled	:= EsActivar;
    			btnCancelar.Enabled := EsActivar;
        	end;

        cGrupoBrowse :
        	begin
                btnEliminar.Enabled := EsActivar;
                //Rev.2 / 30-Abril-2010 / Nelson Droguett Sierra
                //btnEditar.Enabled	:= EsActivar;
            end;
    end;

end;

{
               LimpiaCampos
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	:	Limpia los campos de edici�n del ABM
}
procedure TABMListaBlancaForm.LimpiaCampos;
begin
	edtPatente.Text := '';
    edtEtiqueta.Text := '';
    vcbCategorias.ItemIndex := -1;
    vcbNovedad.ItemIndex := -1;
end;

{           btnAceptarClick
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	:	Graba los cambios a la lista blanca
}
procedure TABMListaBlancaForm.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_PATENTE		= 'No ha ingresado la patente';
    MSG_ETIQUETA	= 'No ha  ingreado la Etiqueta';
    MSG_CATEGORIA	= 'No ha ingresado la categor�a';
    MSG_NOVEDAD		= 'No ha seleccionado la Novedad';
    MSG_ERROR		= 'Ocurri� un error al actualizar la ListaBlanca';
    MSG_EXITO		= 'Registro ha sido grabado exitosamente';
var
	Mensaje : string;
begin
	if not ValidateControls(
    			[edtPatente, edtEtiqueta, vcbCategorias, vcbNovedad],
    			[edtPatente.Text <> '', edtEtiqueta.Text <> '', vcbCategorias.ItemIndex >= 0, vcbNovedad.ItemIndex >= 0],
				Caption,
				[MSG_PATENTE, MSG_ETIQUETA, MSG_CATEGORIA, MSG_NOVEDAD] ) then Exit;

    try
	    with spActualizarRegistroEnListaBlanca do begin
   	 		Parameters.Refresh;
        	Parameters.ParamByName('@Etiqueta').Value					:= edtEtiqueta.Text;
        	Parameters.ParamByName('@Categoria').Value					:= vcbCategorias.Value;
        	Parameters.ParamByName('@Patente').Value					:= edtPatente.Text;
        	Parameters.ParamByName('@Novedad').Value					:= vcbNovedad.Value;
        	Parameters.ParamByName('@GeneradoAutomaticamente').Value	:= False;
        	Parameters.ParamByName('@MensajeError').Value				:= '';
        	Parameters.ParamByName('@EsNuevo').Value					:= (FEdicion = cEdicionInsertar);
        	ExecProc;
        	Mensaje := Parameters.ParamByName('@MensajeError').Value;
        	if Mensaje <> '' then begin
        		MsgBoxErr(MSG_ERROR, Mensaje, Caption, MB_ICONERROR);
        	end
        	else begin
        		MsgBox(MSG_EXITO, Caption, MB_ICONINFORMATION);
            	ActivarBotones(True,  cGrupoToolbar);
            	ActivarBotones(True,  cGrupoSalir);
            	ActivarBotones(False, cGrupoEdicion);
            	ObtenerListaBlanca();
        	end;
    	end;
    except on e:exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;


{             btnEditarClick
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	:	Realiza las acciones para la edici�n de un
				registro existente.
}
procedure TABMListaBlancaForm.btnEditarClick(Sender: TObject);
begin
	FEdicion := cEdicionEditar;
    ActivarBotones(False, cGrupoToolbar);
    ActivarBotones(False, cGrupoSalir);
    ActivarBotones(True,  cGrupoEdicion);
    edtPatente.SetFocus;
end;

{             btnEliminarClick
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	:	Realiza las acciones para la eliminaci�n de un registro existente.
}
procedure TABMListaBlancaForm.btnEliminarClick(Sender: TObject);
resourcestring
	MSG_ELIMINAR	= 'Est� seguro de eliminar este registro?';
    MSG_SQL			= 'DELETE FROM ListaBlanca WHERE ContractSerialNumber = %d AND ContextMark = %d';
    MSG_ELIM_OK		= 'Registro ha sido borrado';
    MSG_ERROR		= 'Ocurri� un error al intentar eliminar el registro';

begin
    if 	(not spObtenerListaBlanca.FieldByName('ContractSerialNumber').IsNull) and
        (MsgBox(MSG_ELIMINAR, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then begin
        try
        	DMConnections.BaseCAC.Execute(Format(	MSG_SQL, 	[spObtenerListaBlanca.FieldByName('ContractSerialNumber').AsInteger,
            	                                                spObtenerListaBlanca.FieldByName('ContextMark').AsInteger]));
        	MsgBox(MSG_ELIM_OK,Caption, MB_ICONINFORMATION);
        	ObtenerListaBlanca();
        except on e:exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
        	end;
        end;
    end;

end;

{		btnNuevoClick
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	:	Realiza las acciones para la inserci�n de un nuevo registro.
}
procedure TABMListaBlancaForm.btnNuevoClick(Sender: TObject);
begin
	FEdicion := cEdicionInsertar;
    ActivarBotones(False, cGrupoToolbar);
    ActivarBotones(False, cGrupoSalir);
    ActivarBotones(True,  cGrupoEdicion);
    LimpiaCampos();
    edtPatente.SetFocus;
end;

{        btnSalirClick
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	: Cierra la ventana de ABM de Lista Blanca.
}
procedure TABMListaBlancaForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

{             dsListaBlancaDataChange
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	:	Utilizado para determinar los cambios de registros.
}
procedure TABMListaBlancaForm.dsListaBlancaDataChange(Sender: TObject;
  Field: TField);
begin
    if FEdicion = cEdicionBrowse then begin
    	with spObtenerListaBlanca do begin
        	edtEtiqueta.Text		:= FieldByName('Etiqueta').AsString;
            vcbCategorias.ItemIndex	:= vcbCategorias.Items.IndexOfValue(FieldByName('Categoria').AsInteger);
            edtPatente.Text			:= FieldByName('Patente').AsString;
            vcbNovedad.ItemIndex	:= vcbNovedad.Items.IndexOfValue(FieldByName('Novedad').AsString);
            ActivarBotones(not FieldByName('GeneradoAutomaticamente').AsBoolean, cGrupoBrowse);
        end;
    end;
end;

{
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 03-Mayo-2010
Description	:	Si la patente no esta completa, no puede grabar
}
procedure TABMListaBlancaForm.edtPatenteChange(Sender: TObject);
begin
end;

{
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	:	Acciones al abandonar el cuadro de ingreso de la patente
}
procedure TABMListaBlancaForm.edtPatenteExit(Sender: TObject);
begin
// Rev.1
	edtEtiqueta.Text := QueryGetValue(DMConnections.BaseCAC,
                                      'SELECT dbo.ObtenerEtiquetaPatente(''' + edtPatente.Text + ''')');

	vcbCategorias.Value := QueryGetValueInt(DMConnections.BaseCAC,
                                      'SELECT ISNULL(CodigoCategoria,0) FROM MaestroVehiculos WHERE Patente='''+edtPatente.Text+'''');
end;

{             edtPatenteExit
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	:	Acciones para el cuadro de ingreso de la patente en caso de
            	presionar ENTER
}
procedure TABMListaBlancaForm.edtPatenteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
// Rev.1
if ((Key=VK_RETURN) or (Key=VK_TAB)) then
begin
  vcbNovedad.SetFocus
end;
// FinRev.1
end;


{			FormClose
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	: Cierra el formulario actual
}
procedure TABMListaBlancaForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

{				Inicializar
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description:    Inicializa las variables y el entorno del formulario
}
function TABMListaBlancaForm.Inicializar;
resourcestring
	MSG_CAPTION	= 'ABM Lista Blanca';
    MSG_ERROR	= 'Ha ocurrido un error al inicializar el ABM de Lista Blanca';
begin
    CenterForm(Self);
    Caption := MSG_CAPTION;
    {agregar las novedades}
    vcbNovedad.Clear;
    vcbNovedad.Items.Add('ALTA', 'A');
    vcbNovedad.Items.Add('BAJA', 'B');
    
    LimpiaCampos();
    
    try
    	Result := True;
        ObtenerCategorias();
        ObtenerListaBlanca();
        ActivarBotones(True,  cGrupoToolbar);
        ActivarBotones(True,  cGrupoSalir);
    	ActivarBotones(False, cGrupoEdicion);
    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
        	Result := False;
    	end;
    end;
end;

{        ObtenerCategorias
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description:    Devuelve los registros asociados a la tabla categorias
}
procedure TABMListaBlancaForm.ObtenerCategorias;
var
    spCategorias : TADOStoredProc;
begin
    vcbCategorias.Clear;
    spCategorias := TADOStoredProc.Create(Self);
	spCategorias.Close;
    with spCategorias do begin
        Connection := DMConnections.BaseCAC;
    	ProcedureName := 'ObtenerCategorias';
        Parameters.Refresh;
        Open;
        while not EOF do begin
            vcbCategorias.Items.Add(	FieldByName('Descripcion').AsString,
                                    	FieldByName('Categoria').AsInteger );
        	Next;
        end;
    end;
    spCategorias.Close;
    spCategorias.Free;
end;

{      ObtenerListaBlanca
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description	:    Devuelve los registros asociados a la tabla de ListaBlanca
}
procedure TABMListaBlancaForm.ObtenerListaBlanca;
resourcestring
	MSG_ERROR = 'Ocurri� un error al obtener la lista blanca';
begin
	spObtenerListaBlanca.Close;
    spObtenerListaBlanca.Parameters.Refresh;
    try
    	spObtenerListaBlanca.Open;
        FEdicion := cEdicionBrowse;
    	dsListaBlancaDataChange(nil, nil);
    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;

{            btnCancelarClick
Author		: Nelson Droguett Sierra (mbecerra)
Date		: 19-Abril-2010
Description:    Cancela la edici�n del registro
}
procedure TABMListaBlancaForm.btnCancelarClick(Sender: TObject);
begin
    ActivarBotones(True,  cGrupoToolbar);
	ActivarBotones(True,  cGrupoSalir);
    ActivarBotones(False, cGrupoEdicion);
    FEdicion := cEdicionBrowse;
    dsListaBlancaDataChange(nil, nil);
end;

procedure TABMListaBlancaForm.vcbNovedadEnter(Sender: TObject);
begin
    if Trim(edtEtiqueta.Text) = '' then begin
        btnAceptar.Enabled := False;
        MsgBoxBalloon('La Patente NO existe o NO est� asociada a una cuenta Activa','Validaci�n Patente',MB_ICONERROR, edtPatente);
        edtPatente.SetFocus;
    end
    else btnAceptar.Enabled := True;
end;

procedure TABMListaBlancaForm.vcbNovedadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if Key=VK_RETURN then begin
        vcbNovedad.SetFocus;
    end;
end;

end.

