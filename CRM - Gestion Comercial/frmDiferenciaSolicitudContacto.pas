unit frmDiferenciaSolicitudContacto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ListBoxEx, DBListEx, StdCtrls, ExtCtrls, DPSControls,
  DMConnection, Peaprocs, Peatypes, Util, UtilDB, UtilProc;

type
  TformDiferenciaSolicitudContacto = class(TForm)
    GroupBox1: TGroupBox;
    PanelDeAbajo: TPanel;
    dbl_Solicitud: TDBListEx;
    ObtenerSolicitudContacto: TADOStoredProc;
    DS_SolicitudContacto: TDataSource;
    Pnl_LabelDescripcion: TPanel;
    lblDescripcionMensaje: TLabel;
    lblRutBuscado: TLabel;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    lblPersoneria: TLabel;
    lblRazonSocial: TLabel;
    lblNombre: TLabel;
    lblApellido: TLabel;
    lblApellidoMaterno: TLabel;
    lblSexo: TLabel;
    lblFechaNacimiento: TLabel;
    lblTelefonoPrincipal: TLabel;
    lblTelefonoAlternativo: TLabel;
    lblemail: TLabel;
    lblDomicilio: TLabel;
    Label1: TLabel;
    lbl_RazonSocial: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    lbl_FechaNacimiento: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    BtnCancelar: TButton;
    btnAceptar: TButton;
    btnSalir: TButton;
    procedure dbl_SolicitudDblClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoSolicitud: integer;
    function GetCodigoSolicitud: integer;
    procedure CargarPersonaExcel (DatosPersona: TDatosPersona);
  public
    { Public declarations }
    property CodigoSolicitud: integer read GetCodigoSolicitud;
    function Inicializar(CodigoDocumento, NumeroDocumento: AnsiString; DatosPersona: TDatosPersona): Boolean;
  end;

var
  formDiferenciaSolicitudContacto: TformDiferenciaSolicitudContacto;

implementation

{$R *.dfm}

function TformDiferenciaSolicitudContacto.GetCodigoSolicitud: integer;
begin
    Result := FCodigoSolicitud;
end;

function TformDiferenciaSolicitudContacto.Inicializar(CodigoDocumento, NumeroDocumento: AnsiString; DatosPersona: TDatosPersona): Boolean;
resourcestring
    ATENCION = 'Atenci�n';
    MSG_OPENTABLES = 'Error al buscar las Solicitudes.';
var
    tituloMensaje, Descripcion, Comentario: AnsiString;
begin
    //* *//
    result := false;
    tituloMensaje := '';
    Descripcion := '';
    Comentario := '';
    ObtenerMensaje(DMConnections.BaseCAC, MENSAJE_IMPORTACION_EXCEL,
                    tituloMensaje, Descripcion, Comentario);
    self.Caption := iif(tituloMensaje<>'', tituloMensaje, ATENCION);
    lblRutBuscado.Caption := lblRutBuscado.Caption + NumeroDocumento;

    lblDescripcionMensaje.Caption := Descripcion;
    //lblComentarioMensaje.Caption := comentario;
    with ObtenerSolicitudContacto do begin
        close;
        Parameters.Refresh;
        Parameters.ParamByName('@RUT').Value := NumeroDocumento;
        try
            Open;
        except
            on E: Exception do begin
              MsgBox(MSG_OPENTABLES,self.Caption,MB_ICONSTOP);
              exit;
            end;
        end;
    end;
    CargarPersonaExcel(DatosPersona);
    {
    if not(OpenTables([ObtenerSolicitudContacto])) then begin
        MsgBox(MSG_OPENTABLES,self.Caption,MB_ICONSTOP);
        result := false;
    end
    else begin
        btnAceptar.Enabled := not (ObtenerSolicitudContacto.IsEmpty);
	    Result := true;
    end;
    }
    result := true;
end;

procedure TformDiferenciaSolicitudContacto.dbl_SolicitudDblClick(
  Sender: TObject);
begin

    if not (ObtenerSolicitudContacto.IsEmpty) then begin
        FCodigoSolicitud := ObtenerSolicitudContacto.FieldByName('CodigoSolicitudContacto').AsInteger;
        ModalResult := mrOk;
    end;
(*
    if not (ObtenerSolicitudContacto.IsEmpty) then begin
        FCodigoSolicitud := ObtenerSolicitudContacto.Parameters.ParamByName('CodigoSolicitudContacto').Value;
        ModalResult := mrOk;
    end;
*)
end;

procedure TformDiferenciaSolicitudContacto.BtnCancelarClick(
  Sender: TObject);
begin
    FCodigoSolicitud := 0;
    ModalResult := mrCancel;
end;

procedure TformDiferenciaSolicitudContacto.btnAceptarClick(
  Sender: TObject);
begin
    if not (ObtenerSolicitudContacto.IsEmpty) then begin
        FCodigoSolicitud := ObtenerSolicitudContacto.FieldByName('CodigoSolicitudContacto').AsInteger;
        ModalResult := mrOk; 
    end;
end;

procedure TformDiferenciaSolicitudContacto.CargarPersonaExcel(
  DatosPersona: TDatosPersona);
begin
    if DatosPersona.Personeria = PERSONERIA_JURIDICA_DESC then begin
        lblFechaNacimiento.Visible := false;
        lbl_FechaNacimiento.Enabled := false
    end else begin
        lblRazonSocial.Visible := false;
        lbl_RazonSocial.Enabled := false;
    end;

    lblPersoneria.Caption := DatosPersona.Personeria;
    lblRazonSocial.Caption := DatosPersona.RazonSocial;
    lblNombre.Caption := DatosPersona.Nombre;
    lblApellido.Caption := DatosPersona.Apellido;
    lblApellidoMaterno.Caption := DatosPersona.ApellidoMaterno;
    lblSexo.Caption := DatosPersona.Sexo;
    lblFechaNacimiento.Caption :=  DatosPersona.FechaNacimiento;
    lblEmail.Caption := DatosPersona.Email;
    lblTelefonoPrincipal.Caption := DatosPersona.TelefonoPrincipal;
    lblTelefonoAlternativo.Caption := DatosPersona.TelefonoAlternativo;
    lblDomicilio.Caption := DatosPersona.Domicilio;
end;

procedure TformDiferenciaSolicitudContacto.btnSalirClick(Sender: TObject);
begin
    FCodigoSolicitud := 0;
    ModalResult := mrAbort;
end;

end.
