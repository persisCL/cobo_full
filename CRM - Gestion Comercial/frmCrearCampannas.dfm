object FormCrearCampannas: TFormCrearCampannas
  Left = 0
  Top = 0
  Caption = 'Crear Campa'#241'as'
  ClientHeight = 723
  ClientWidth = 1067
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1067
    Height = 43
    Align = alTop
    TabOrder = 0
    DesignSize = (
      1067
      43)
    object Label1: TLabel
      Left = 351
      Top = 15
      Width = 113
      Height = 13
      Anchors = [akLeft, akTop, akBottom]
      Caption = 'Motivo de Campa'#241'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbMotivoCampanna: TVariantComboBox
      Left = 468
      Top = 11
      Width = 241
      Height = 21
      Style = vcsDropDownList
      Anchors = [akLeft, akTop, akBottom]
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = cbMotivoCampannaChange
      Items = <>
    end
    object rgTipoCampanna: TRadioGroup
      Left = 1
      Top = 1
      Width = 323
      Height = 41
      Align = alLeft
      Caption = 'Tipo de Campa'#241'a'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Campa'#241'a Interna'
        'Campa'#241'a Externa')
      TabOrder = 0
      OnClick = rgTipoCampannaClick
    end
  end
  object pnlCampannas: TPanel
    Left = 0
    Top = 43
    Width = 1067
    Height = 438
    Align = alTop
    TabOrder = 1
    Visible = False
    object grpRechazos: TGroupBox
      Left = 1
      Top = 1
      Width = 1065
      Height = 48
      Align = alTop
      Caption = 'Rechazos de pago autom'#225'tico'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Visible = False
      object Label2: TLabel
        Left = 54
        Top = 23
        Width = 86
        Height = 13
        Caption = 'Medio de Pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 262
        Top = 23
        Width = 247
        Height = 13
        Caption = 'Cantidad m'#237'nima de rechazos consecutivos'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cb_PagoAutomatico: TVariantComboBox
        Left = 146
        Top = 20
        Width = 105
        Height = 21
        Style = vcsDropDownList
        Color = 16444382
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Items = <>
      end
      object seCantMinimaRechazosConsecutivos: TSpinEdit
        Left = 511
        Top = 18
        Width = 50
        Height = 22
        Color = 16444382
        MaxValue = 20
        MinValue = 1
        TabOrder = 1
        Value = 1
      end
    end
    object grpMorosos: TGroupBox
      Left = 1
      Top = 49
      Width = 1065
      Height = 79
      Align = alTop
      Caption = 'Morosos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      Visible = False
      object Label4: TLabel
        Left = 13
        Top = 23
        Width = 227
        Height = 13
        Caption = 'Tiempo m'#237'nimo de morosidad de factura'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 13
        Top = 53
        Width = 149
        Height = 13
        Caption = 'Importe m'#237'nimo de factura'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 358
        Top = 23
        Width = 259
        Height = 13
        Caption = 'Importe m'#237'nimo total de las facturas vencidas'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 358
        Top = 53
        Width = 218
        Height = 13
        Caption = 'Cantidad m'#237'nima de facturas vencidas'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 300
        Top = 23
        Width = 26
        Height = 13
        Caption = 'd'#237'as'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 234
        Top = 53
        Width = 8
        Height = 13
        Caption = '$'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 624
        Top = 23
        Width = 8
        Height = 13
        Caption = '$'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label27: TLabel
        Left = 748
        Top = 23
        Width = 121
        Height = 13
        Caption = 'Grupo de facturaci'#243'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object seTiempoMorosidad: TSpinEdit
        Left = 244
        Top = 18
        Width = 50
        Height = 22
        Color = 16444382
        MaxValue = 1000
        MinValue = 1
        TabOrder = 0
        Value = 1
      end
      object seCantidadFacturasVencidas: TSpinEdit
        Left = 635
        Top = 46
        Width = 50
        Height = 22
        Color = 16444382
        MaxValue = 36
        MinValue = 1
        TabOrder = 3
        Value = 1
      end
      object txtTotalFacturasVencidas: TEdit
        Left = 635
        Top = 19
        Width = 100
        Height = 21
        Color = 16444382
        TabOrder = 2
        OnKeyPress = txtImporteFacturaKeyPress
      end
      object txtImporteFactura: TEdit
        Left = 244
        Top = 49
        Width = 100
        Height = 21
        Color = 16444382
        TabOrder = 1
        OnKeyPress = txtImporteFacturaKeyPress
      end
      object cbbGrupoFacturacion: TVariantComboBox
        Left = 879
        Top = 18
        Width = 145
        Height = 21
        Style = vcsDropDownList
        Color = 16444382
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        Items = <>
      end
    end
    object grpPosiblesInfractores: TGroupBox
      Left = 1
      Top = 128
      Width = 1065
      Height = 78
      Align = alTop
      Caption = 'Posibles Infractores'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      Visible = False
      object Label11: TLabel
        Left = 32
        Top = 23
        Width = 234
        Height = 13
        Caption = 'Cantidad m'#237'nima de d'#237'as de la infracci'#243'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 32
        Top = 53
        Width = 179
        Height = 13
        Caption = 'Importe m'#237'nimo de la infracci'#243'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 328
        Top = 23
        Width = 26
        Height = 13
        Caption = 'd'#237'as'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 263
        Top = 53
        Width = 8
        Height = 13
        Caption = '$'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object seDiasInfraccion: TSpinEdit
        Left = 273
        Top = 18
        Width = 50
        Height = 22
        Color = 16444382
        MaxValue = 1000
        MinValue = 1
        TabOrder = 0
        Value = 1
      end
      object txtImporteInfraccion: TEdit
        Left = 273
        Top = 49
        Width = 100
        Height = 21
        Color = 16444382
        TabOrder = 1
        OnKeyPress = txtImporteFacturaKeyPress
      end
    end
    object grpArbitrario: TGroupBox
      Left = 1
      Top = 289
      Width = 1065
      Height = 146
      Align = alTop
      Caption = 'Arbitrario'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      Visible = False
      object Label15: TLabel
        Left = 29
        Top = 23
        Width = 81
        Height = 13
        Caption = 'Concesionaria'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 494
        Top = 53
        Width = 46
        Height = 13
        Caption = 'Comuna'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 29
        Top = 83
        Width = 104
        Height = 13
        Caption = 'Cantidad de TAGs'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 494
        Top = 83
        Width = 77
        Height = 13
        Caption = 'Valor Factura'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label19: TLabel
        Left = 598
        Top = 83
        Width = 8
        Height = 13
        Caption = '$'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label26: TLabel
        Left = 29
        Top = 53
        Width = 41
        Height = 13
        Caption = 'Regi'#243'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cbConcesionarias: TVariantComboBox
        Left = 143
        Top = 19
        Width = 250
        Height = 21
        Style = vcsDropDownList
        Color = 16444382
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Items = <>
      end
      object cbComunas: TVariantComboBox
        Left = 608
        Top = 49
        Width = 250
        Height = 21
        Style = vcsDropDownList
        Color = 16444382
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Items = <>
      end
      object seCantidadTAGs: TSpinEdit
        Left = 143
        Top = 78
        Width = 50
        Height = 22
        Color = 16444382
        MaxValue = 1000
        MinValue = 0
        TabOrder = 3
        Value = 1
      end
      object txtValorFactura: TEdit
        Left = 608
        Top = 79
        Width = 100
        Height = 21
        Color = 16444382
        TabOrder = 4
        OnKeyPress = txtImporteFacturaKeyPress
      end
      object cbRegiones: TVariantComboBox
        Left = 143
        Top = 51
        Width = 250
        Height = 21
        Style = vcsDropDownList
        Color = 16444382
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnChange = cbRegionesChange
        Items = <>
      end
      object rgArbitrarioSuscripcion: TRadioGroup
        Left = 29
        Top = 103
        Width = 829
        Height = 40
        Columns = 3
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Items.Strings = (
          'Suscripci'#243'n PAC'
          'Suscripci'#243'n PAT'
          'Suscripci'#243'n Boleta/Factura Electr'#243'nica')
        ParentFont = False
        TabOrder = 5
      end
    end
    object grpTagsDefectuosos: TGroupBox
      Left = 1
      Top = 206
      Width = 1065
      Height = 83
      Align = alTop
      Caption = 'TAGs Defectuosos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      Visible = False
      object rgTagsDefectuosos: TRadioGroup
        Left = 29
        Top = 12
        Width = 704
        Height = 61
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clBtnFace
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Items.Strings = (
          'TAG con Fecha de fin de garant'#237'a expirada'
          'TAG con bit '#8220'Status Battery Failure'#8221' activado')
        ParentBackground = False
        ParentColor = False
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 680
    Width = 1067
    Height = 43
    Align = alBottom
    TabOrder = 3
    DesignSize = (
      1067
      43)
    object btnCrearCampannaExterna: TBitBtn
      Left = 713
      Top = 8
      Width = 130
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Crear Campa'#241'a Externa'
      TabOrder = 1
      Visible = False
      OnClick = btnCrearCampannaExternaClick
    end
    object btnCrearCampannaInterna: TBitBtn
      Left = 713
      Top = 8
      Width = 130
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Crear Campa'#241'a Interna'
      TabOrder = 0
      OnClick = btnCrearCampannaInternaClick
    end
    object btnCancelar: TBitBtn
      Left = 857
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Cancelar'
      TabOrder = 2
      OnClick = btnCancelarClick
    end
    object btnSalir: TBitBtn
      Left = 952
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Salir'
      TabOrder = 3
      OnClick = btnSalirClick
    end
  end
  object pnlDetalleCampanna: TPanel
    Left = 0
    Top = 481
    Width = 1067
    Height = 199
    Align = alClient
    TabOrder = 2
    Visible = False
    object Label20: TLabel
      Left = 30
      Top = 20
      Width = 68
      Height = 13
      Caption = 'Descripci'#243'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel
      Left = 30
      Top = 56
      Width = 108
      Height = 13
      Caption = 'Medio de Contacto'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblVigencia: TLabel
      Left = 340
      Top = 94
      Width = 127
      Height = 13
      Caption = 'Vigencia Campa'#241'a del'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label24: TLabel
      Left = 655
      Top = 56
      Width = 82
      Height = 13
      Caption = #193'rea asignada'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object lblVigenciaAl: TLabel
      Left = 599
      Top = 94
      Width = 11
      Height = 13
      Caption = 'al'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabelDiaEnvio: TLabel
      Left = 173
      Top = 94
      Width = 69
      Height = 13
      Caption = 'D'#237'a de env'#237'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel
      Left = 341
      Top = 56
      Width = 51
      Height = 13
      Caption = 'Prioridad'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object btnSeleccionarPlantilla: TButton
      Left = 321
      Top = 50
      Width = 120
      Height = 25
      Caption = 'Seleccionar Plantilla'
      TabOrder = 8
      Visible = False
      OnClick = btnSeleccionarPlantillaClick
    end
    object edtDescripcionCampanna: TEdit
      Left = 164
      Top = 16
      Width = 562
      Height = 21
      Color = 16444382
      TabOrder = 0
    end
    object cbbMedioContacto: TComboBox
      Left = 164
      Top = 52
      Width = 140
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbbMedioContactoChange
    end
    object cbbPrioridad: TComboBox
      Left = 462
      Top = 52
      Width = 145
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      Visible = False
    end
    object cbbAreaAsignada: TVariantComboBox
      Left = 776
      Top = 52
      Width = 145
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Visible = False
      Items = <>
    end
    object txtInicioVigencia: TDateEdit
      Left = 475
      Top = 90
      Width = 122
      Height = 21
      AutoSelect = False
      Color = 16444382
      TabOrder = 4
      Date = 36526.000000000000000000
    end
    object txtTerminoVigencia: TDateEdit
      Left = 616
      Top = 90
      Width = 122
      Height = 21
      AutoSelect = False
      Color = 16444382
      TabOrder = 5
      Date = 36526.000000000000000000
    end
    object chkRecursiva: TCheckBox
      Left = 26
      Top = 92
      Width = 131
      Height = 17
      Caption = 'Recursiva Mensual'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = chkRecursivaClick
    end
    object seDiaEnvio: TSpinEdit
      Left = 254
      Top = 89
      Width = 50
      Height = 22
      Color = 16444382
      MaxValue = 30
      MinValue = 1
      TabOrder = 7
      Value = 1
      Visible = False
    end
  end
  object spObtenerMotivosCampannas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMotivosCampannas'
    Parameters = <>
    Left = 616
  end
  object spObtenerConcesionarias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConcesionarias'
    Parameters = <>
    Left = 360
    Top = 360
  end
  object spObtenerComunasChileRegion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerComunasChileRegion'
    Parameters = <>
    Left = 752
    Top = 368
  end
  object spObtenerAreasAtencionDeCasos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerAreasAtencionDeCasos'
    Parameters = <>
    Left = 528
    Top = 616
  end
  object spGuardarCampannas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GuardarCampannas'
    Parameters = <>
    Left = 840
    Top = 400
  end
  object spObtenerDatosCampannaExterna: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosCampannaExterna'
    Parameters = <>
    Left = 800
    Top = 576
  end
  object dlgSaveCampannaExterna: TSaveDialog
    Left = 960
    Top = 552
  end
  object spObtenerTiposDefectosTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTiposDefectosTAGs'
    Parameters = <>
    Left = 888
    Top = 280
  end
  object spEliminarCampanna: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarCampanna'
    Parameters = <>
    Left = 240
    Top = 640
  end
  object spObtenerRegionesComunasChile: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRegionesComunasChile'
    Parameters = <>
    Left = 320
    Top = 416
  end
  object spObtenerGruposFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerGruposFacturacion'
    Parameters = <>
    Left = 904
    Top = 40
  end
  object spObtenerCantidadClientesCampanna: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCantidadClientesCampanna'
    Parameters = <>
    Left = 800
    Top = 632
  end
end
