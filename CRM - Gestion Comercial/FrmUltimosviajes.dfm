object NavWindowUltimosViajes: TNavWindowUltimosViajes
  Left = 225
  Top = 171
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  Anchors = [akRight, akBottom]
  AutoScroll = False
  Caption = 'Listado de viajes'
  ClientHeight = 477
  ClientWidth = 742
  Color = 14732467
  Constraints.MinHeight = 504
  Constraints.MinWidth = 750
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    742
    477)
  PixelsPerInch = 96
  TextHeight = 13
  object gb_Parametros: TGroupBox
    Left = 3
    Top = 0
    Width = 736
    Height = 59
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Par'#225'metros de b'#250'squeda'
    Color = 14732467
    ParentColor = False
    TabOrder = 0
    object lb_CodigoCliente: TLabel
      Left = 7
      Top = 18
      Width = 71
      Height = 13
      Caption = 'C'#243'digo Cliente:'
    end
    object Label4: TLabel
      Left = 403
      Top = 18
      Width = 55
      Height = 13
      Caption = 'Fecha Final'
    end
    object Label3: TLabel
      Left = 300
      Top = 18
      Width = 60
      Height = 13
      Caption = 'Fecha Inicial'
    end
    object Label5: TLabel
      Left = 116
      Top = 18
      Width = 147
      Height = 13
      Caption = 'Rangos de fechas predefinidos'
    end
    object peCodigoCliente: TPickEdit
      Left = 7
      Top = 32
      Width = 103
      Height = 21
      Enabled = True
      TabOrder = 0
      OnChange = peCodigoClienteChange
      Decimals = 0
      EditorStyle = bteNumericEdit
      OnButtonClick = peCodigoClienteButtonClick
    end
    object deFechaFinal: TDateEdit
      Left = 403
      Top = 32
      Width = 93
      Height = 21
      AutoSelect = False
      TabOrder = 3
      OnChange = deFechaInicialChange
      Date = -693594
    end
    object deFechaInicial: TDateEdit
      Left = 300
      Top = 32
      Width = 96
      Height = 21
      AutoSelect = False
      TabOrder = 2
      OnChange = deFechaInicialChange
      Date = -693594
    end
    object cbRangosPredefinidos: TComboBox
      Left = 116
      Top = 32
      Width = 177
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbRangosPredefinidosChange
      Items.Strings = (
        'Sin Notificar'
        'Notificado'
        'En Agencia')
    end
  end
  object gb_Viajes: TGroupBox
    Left = 3
    Top = 106
    Width = 736
    Height = 185
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Viajes '
    TabOrder = 1
    DesignSize = (
      736
      185)
    object Viajes: TDBListEx
      Left = 5
      Top = 15
      Width = 724
      Height = 164
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 238
          Header.Caption = 'Cuenta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripcionCuenta'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 153
          Header.Caption = 'Concesionaria'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DecriConcesionaria'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 115
          Header.Caption = 'Fecha/Hora Inicio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHoraInicio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 110
          Header.Caption = 'Fecha/Hora Fin'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHoraFin'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 66
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Importe'
        end>
      DataSource = ds_Transacciones
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object gb_DetalleViaje: TGroupBox
    Left = 3
    Top = 291
    Width = 736
    Height = 183
    Anchors = [akLeft, akRight, akBottom]
    Caption = ' Detalle de viaje'
    TabOrder = 2
    DesignSize = (
      736
      183)
    object Image: TImagesVehicle
      Left = 488
      Top = 14
      Width = 242
      Height = 163
      ADOConnection = DMConnections.BaseCAC
      ConfigOverview.InteractiveMode = False
      ConfigJpeg.Shift = 0
      ConfigJpeg.ShowPositionPlate = False
      ConfigJpeg.Center = False
      ConfigJpeg.ShowMask = False
      Color = 16444382
      Anchors = [akTop, akRight, akBottom]
      TabOrder = 0
    end
    object dbgDetalleViajes: TDBListEx
      Left = 5
      Top = 15
      Width = 476
      Height = 161
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 61
          Header.Caption = 'Hora'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriFechaHora'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 131
          Header.Caption = 'Categor'#237'a'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Categoria'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 85
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Importe'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 99
          Header.Caption = 'Tipo de Horario'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TipoHorario'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 88
          Header.Caption = 'Pto. de Cobro'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriPuntosCobro'
        end>
      DataSource = ds_DetalleViaje
      DragReorder = True
      ParentColor = False
      TabOrder = 1
      TabStop = True
    end
  end
  object GroupBox1: TGroupBox
    Left = 3
    Top = 60
    Width = 736
    Height = 45
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Datos del Cliente'
    TabOrder = 3
    DesignSize = (
      736
      45)
    object lApellidoNombre: TLabel
      Left = 11
      Top = 19
      Width = 206
      Height = 13
      Anchors = [akLeft, akBottom]
      AutoSize = False
      Caption = 'Apellido Cliente'
    end
    object LDomicilio: TLabel
      Left = 220
      Top = 19
      Width = 74
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'DomicilioCliente'
    end
  end
  object ds_Transacciones: TDataSource
    DataSet = ObtenerViajesCliente
    OnDataChange = ds_TransaccionesDataChange
    Left = 491
    Top = 69
  end
  object ObtenerViajesCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'ObtenerViajesCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 523
    Top = 69
  end
  object PopupMenu: TPopupMenu
    OnPopup = PopupMenuPopup
    Left = 555
    Top = 69
    object mnu_reclamoviaje: TMenuItem
      Caption = 'Reclamo Por Viaje no realizado ...'
      OnClick = mnu_reclamoviajeClick
    end
  end
  object ds_DetalleViaje: TDataSource
    DataSet = ObtenerTransitosViaje
    OnDataChange = ds_DetalleViajeDataChange
    Left = 224
    Top = 330
  end
  object ObtenerTransitosViaje: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTransitosViaje'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroViaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 260
    Top = 330
  end
  object ObtenerCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 112
    Top = 112
  end
end
