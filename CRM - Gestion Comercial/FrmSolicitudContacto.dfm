object FormSolicitudContacto: TFormSolicitudContacto
  Left = 98
  Top = 55
  Width = 1222
  Height = 788
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  AutoScroll = True
  Caption = 'Alta de Solicitud'
  Color = 16776699
  Constraints.MaxHeight = 855
  Constraints.MaxWidth = 1222
  Constraints.MinHeight = 577
  Constraints.MinWidth = 950
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    1189
    750)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TScrollBox
    Left = 0
    Top = 0
    Width = 1206
    Height = 817
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      1202
      813)
    object pnlDatosContacto: TPanel
      Left = 0
      Top = 32
      Width = 1205
      Height = 667
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        1205
        667)
      object lblTipoConvenio: TLabel
        Left = 12
        Top = 15
        Width = 105
        Height = 13
        Caption = 'Tipo de Convenio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTipoCliente: TLabel
        Left = 308
        Top = 15
        Width = 91
        Height = 13
        Caption = 'Tipo de Cliente:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object GBMediosComunicacion: TGroupBox
        Left = 5
        Top = 157
        Width = 1197
        Height = 71
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Medios de comunicaci'#243'n'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        object lblEmail: TLabel
          Left = 422
          Top = 46
          Width = 28
          Height = 13
          Caption = 'Email:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lblEnviar: TLabel
          Left = 865
          Top = 46
          Width = 30
          Height = 13
          Caption = 'Enviar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          OnClick = lblEnviarClick
          OnMouseEnter = lblEnviarMouseEnter
          OnMouseLeave = lblEnviarMouseLeave
        end
        inline FreTelefonoPrincipal: TFrameTelefono
          Left = 9
          Top = 14
          Width = 607
          Height = 26
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 9
          ExplicitTop = 14
          ExplicitWidth = 607
          ExplicitHeight = 26
          inherited cbTipoTelefono: TVariantComboBox
            Left = 274
            ExplicitLeft = 274
          end
        end
        inline FreTelefonoSecundario: TFrameTelefono
          Left = 9
          Top = 39
          Width = 407
          Height = 26
          HorzScrollBar.Visible = False
          VertScrollBar.Visible = False
          TabOrder = 1
          TabStop = True
          ExplicitLeft = 9
          ExplicitTop = 39
          ExplicitWidth = 407
          ExplicitHeight = 26
          inherited txtTelefono: TEdit
            OnExit = FreTelefonoSecundariotxtTelefonoExit
          end
        end
        object NBook_Email: TNotebook
          Left = 455
          Top = 39
          Width = 404
          Height = 24
          PageIndex = 1
          TabOrder = 2
          object TPage
            Left = 0
            Top = 0
            Caption = 'PageSalir'
            object txtEMailContacto: TEdit
              Left = 3
              Top = 1
              Width = 350
              Height = 21
              Hint = 'Direcci'#243'n de e-mail'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 60
              ParentFont = False
              ParentShowHint = False
              PopupMenu = PopupMenu
              ShowHint = False
              TabOrder = 0
              Text = 'txtEMailContacto'
              OnChange = txtEMailContactoChange
            end
          end
          object TPage
            Left = 0
            Top = 0
            HelpContext = 1
            Caption = 'PageGuardar'
            object txtEmailParticular: TEdit
              Left = 2
              Top = 1
              Width = 400
              Height = 21
              Hint = 'Direcci'#243'n de e-mail'
              Color = 16444382
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 60
              ParentFont = False
              ParentShowHint = False
              PopupMenu = PopupMenu
              ShowHint = True
              TabOrder = 0
              OnChange = txtEMailContactoChange
            end
          end
        end
      end
      object GBDomicilios: TGroupBox
        Left = 5
        Top = 227
        Width = 1197
        Height = 116
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Domicilio '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        DesignSize = (
          1197
          116)
        inline FMDomicilioPrincipal: TFrameDomicilio
          Left = 4
          Top = 9
          Width = 1108
          Height = 110
          HorzScrollBar.Visible = False
          VertScrollBar.Visible = False
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 4
          ExplicitTop = 9
          ExplicitWidth = 1108
          ExplicitHeight = 110
          inherited Pnl_Arriba: TPanel
            Width = 1108
            ExplicitWidth = 1108
            inherited txt_DescripcionCiudad: TEdit
              MaxLength = 50
            end
            inherited txt_numeroCalle: TEdit
              OnExit = FMDomicilioPrincipaltxt_numeroCalleExit
            end
            inherited txt_CodigoPostal: TEdit
              Width = 170
              ExplicitWidth = 170
            end
          end
          inherited Pnl_Abajo: TPanel
            Top = 54
            Width = 919
            Align = alNone
            ExplicitTop = 54
            ExplicitWidth = 919
            DesignSize = (
              919
              25)
            inherited cb_Comunas: TComboBox
              Width = 345
              Anchors = [akLeft, akTop, akRight]
              ExplicitWidth = 345
            end
          end
          inherited Pnl_Medio: TPanel
            Top = 78
            Width = 614
            Align = alNone
            Visible = True
            ExplicitTop = 78
            ExplicitWidth = 614
          end
        end
      end
      object GBOtrosDatos: TGroupBox
        Left = 4
        Top = 463
        Width = 1197
        Height = 66
        Anchors = [akLeft, akTop, akRight]
        BiDiMode = bdLeftToRight
        Caption = 'Otros'
        Color = clBtnFace
        Ctl3D = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentBiDiMode = False
        ParentColor = False
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 6
        DesignSize = (
          1197
          66)
        object lblPreguntaFuenteOrigenContacto: TLabel
          Left = 845
          Top = 13
          Width = 275
          Height = 13
          Caption = 'Aca va la pregunta de la promo del Santander (de la base)'
          Color = clBtnFace
          FocusControl = cbFuentesOrigenContacto
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Visible = False
        end
        object lbl_POS: TLabel
          Left = 485
          Top = 15
          Width = 265
          Height = 13
          AutoSize = False
          Caption = 'Debe seleccionar un POS'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object lbl_Pregunta: TLabel
          Left = 8
          Top = 15
          Width = 251
          Height = 13
          AutoSize = False
          BiDiMode = bdLeftToRight
          Caption = #191'D'#243'nde quiere pasar a buscar su Telev'#237'a?'
          Color = clBtnFace
          FocusControl = cb_POS
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
        end
        object lbl_Ubicacion: TLabel
          Left = 8
          Top = 43
          Width = 145
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = 'Ubicaci'#243'n f'#237'sica de la carpeta:'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
        end
        object cb_RecibeClavePorMail: TCheckBox
          Left = 503
          Top = 43
          Width = 129
          Height = 17
          Hint = 'Recibe Clave Por eMail'
          BiDiMode = bdLeftToRight
          Caption = 'Recibe Clave Por Mail'
          Color = clBtnFace
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
        end
        object cbFuentesOrigenContacto: TComboBox
          Left = 1130
          Top = 10
          Width = 57
          Height = 21
          Hint = 'Fuente de Solicitud'
          Style = csDropDownList
          BiDiMode = bdLeftToRight
          Color = 16444382
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentBiDiMode = False
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = False
          Items.Strings = (
            'F'#237'sica                          F'
            'Jur'#237'dica                       J')
        end
        object cb_POS: TVariantComboBox
          Left = 265
          Top = 13
          Width = 214
          Height = 21
          Hint = 'C'#243'digo de Area'
          Style = vcsDropDownList
          Color = 16444382
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnChange = cb_POSChange
          Items = <>
        end
        object txt_UbicacionFisicaCarpeta: TEdit
          Left = 164
          Top = 40
          Width = 192
          Height = 21
          BiDiMode = bdLeftToRight
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 15
          ParentBiDiMode = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
        object cb_RecInfoMail: TCheckBox
          Left = 370
          Top = 43
          Width = 117
          Height = 17
          Hint = #191'Quiere recibir Informaci'#243'n por email?'
          BiDiMode = bdLeftToRight
          Caption = 'Recibir Info por Mail'
          Color = clBtnFace
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
        object btn_DocRequerido: TButton
          Left = 1103
          Top = 37
          Width = 82
          Height = 22
          Anchors = [akRight, akBottom]
          Caption = '&Documentaci'#243'n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnClick = btn_DocRequeridoClick
        end
        object chkInactivaDomicilioRNUT: TCheckBox
          Left = 638
          Top = 43
          Width = 147
          Height = 17
          Caption = 'Inactiva Domicilios RNUT'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          Visible = False
        end
        object chkInactivaMediosComunicacionRNUT: TCheckBox
          Left = 791
          Top = 43
          Width = 144
          Height = 17
          Caption = 'Inactiva Contactos RNUT'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          Visible = False
        end
      end
      inline FreDatoPersona: TFreDatoPresona
        Left = 5
        Top = 56
        Width = 1197
        Height = 101
        Anchors = [akLeft, akTop, akRight]
        Constraints.MinWidth = 900
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 5
        ExplicitTop = 56
        ExplicitWidth = 1197
        ExplicitHeight = 101
        DesignSize = (
          1197
          101)
        inherited ledTipoCliente: TLed
          Left = 1174
          Top = 54
          ExplicitLeft = 1174
          ExplicitTop = 54
        end
        inherited lblTipo: TLabel
          Left = 966
          Top = 58
          ExplicitLeft = 966
          ExplicitTop = 58
        end
        inherited lblSemaforo1: TLabel
          Left = 970
          Top = 38
          ExplicitLeft = 970
          ExplicitTop = 38
        end
        inherited LedSemaforo1: TLed
          Left = 1174
          Top = 33
          ExplicitLeft = 1174
          ExplicitTop = 33
        end
        inherited lblSemaforo3: TLabel
          Left = 970
          Top = 80
          ExplicitLeft = 970
          ExplicitTop = 80
        end
        inherited LedSemaforo3: TLed
          Left = 1174
          Top = 75
          ExplicitLeft = 1174
          ExplicitTop = 75
        end
        inherited Pc_Datos: TPageControl
          ActivePage = FreDatoPersona.Tab_Juridica
          inherited Tab_Juridica: TTabSheet
            inherited cbSexo_Contacto: TComboBox
              Top = 25
              ExplicitTop = 25
            end
          end
          inherited Tab_Fisica: TTabSheet
            inherited txt_ApellidoMaterno: TEdit
              Width = 164
              ExplicitWidth = 164
            end
          end
        end
        inherited Panel1: TPanel
          Width = 1197
          ExplicitWidth = 1197
          inherited lblRazonSocial: TLabel
            Left = 455
            ExplicitLeft = 455
          end
          inherited lblGiro: TLabel
            Left = 785
            Top = 7
            ExplicitLeft = 785
            ExplicitTop = 7
          end
          inherited cbPersoneria: TComboBox
            OnChange = FreDatoPresona1cbPersoneriaChange
          end
          inherited txtDocumento: TEdit
            OnChange = FreDatoPersonatxtDocumentoChange
            OnExit = FreDatoPersonatxtDocumentoExit
            OnKeyDown = FreDatoPersonatxtDocumentoKeyDown
            OnKeyPress = FreDatoPersonatxtDocumentoKeyPress
            OnKeyUp = FreDatoPersonatxtDocumentoKeyUp
          end
          inherited txtGiro: TEdit [6]
            Width = 370
            ExplicitWidth = 370
          end
          inherited txtRazonSocial: TEdit [7]
            Left = 541
            Top = 4
            ExplicitLeft = 541
            ExplicitTop = 4
          end
        end
        inherited cboTipoCliente: TDBLookupComboBox
          Left = 1041
          Top = 54
          TabOrder = 3
          ExplicitLeft = 1041
          ExplicitTop = 54
        end
        inherited cboSemaforo1: TDBLookupComboBox
          Left = 1041
          Top = 32
          TabOrder = 2
          ExplicitLeft = 1041
          ExplicitTop = 32
        end
        inherited cboSemaforo3: TDBLookupComboBox
          Left = 1041
          Top = 75
          ExplicitLeft = 1041
          ExplicitTop = 75
        end
        inherited spTiposClientes: TADOStoredProc
          Left = 675
          Top = 53
        end
        inherited srcTipoCliente: TDataSource
          Left = 608
          Top = 48
        end
        inherited spSemaforo1ObtenerLista: TADOStoredProc
          Left = 545
          Top = 0
        end
        inherited srcSemaforo1: TDataSource
          Left = 672
          Top = 0
        end
        inherited srcSemaforo3: TDataSource
          Left = 704
          Top = 0
        end
        inherited spSemaforo3ObtenerLista: TADOStoredProc
          Left = 768
          Top = 65527
        end
      end
      object Panel6: TPanel
        Left = 6
        Top = 34
        Width = 117
        Height = 16
        BevelOuter = bvNone
        Caption = 'Datos Personales'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
      end
      object pnl_RepLegal: TPanel
        Left = 340
        Top = 110
        Width = 510
        Height = 26
        Anchors = [akLeft, akTop, akRight]
        BevelOuter = bvNone
        TabOrder = 7
        object lbl_RepresentanteLegal: TLabel
          Left = 121
          Top = 7
          Width = 95
          Height = 13
          Caption = 'Representante legal'
        end
        object btn_RepresentanteLegal: TButton
          Left = 0
          Top = 0
          Width = 115
          Height = 26
          Align = alLeft
          Caption = '&Representante Legal'
          TabOrder = 0
          OnClick = btn_RepresentanteLegalClick
        end
      end
      object GBFacturacion: TGroupBox
        Left = 4
        Top = 343
        Width = 1197
        Height = 116
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Datos de Facturaci'#243'n'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        DesignSize = (
          1197
          116)
        object lbl1: TLabel
          Left = 8
          Top = 17
          Width = 285
          Height = 13
          Caption = #191'Desea recibir la factura mensual en su domicilio Particular? '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbl_OtroDomicilio: TLabel
          Left = 543
          Top = 17
          Width = 549
          Height = 13
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'aca va la descripci'#243'n del Otro domicilio...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbl_PagoAutomatico: TLabel
          Left = 8
          Top = 39
          Width = 126
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = 'Forma de pago autom'#225'tico'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
        end
        object lbl_DescripcionMedioPago: TLabel
          Left = 338
          Top = 39
          Width = 717
          Height = 13
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'aca va la descripci'#243'n del m.pago automatico'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object lbl2: TLabel
          Left = 8
          Top = 66
          Width = 166
          Height = 13
          Caption = 'Tipo Documento Electronico:'
        end
        object lbl3: TLabel
          Left = 8
          Top = 91
          Width = 147
          Height = 13
          Caption = 'Convenio de Facturaci'#243'n:'
        end
        object lbl4: TLabel
          Left = 497
          Top = 91
          Width = 120
          Height = 13
          Caption = 'RUT de Facturaci'#243'n:'
        end
        object lbl_NombreRutFacturacion: TLabel
          Left = 876
          Top = 91
          Width = 549
          Height = 13
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'Aca Nombre RUT de Faccturaci'#243'n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 409
        end
        object lblGrupoFacturacion: TLabel
          Left = 620
          Top = 66
          Width = 115
          Height = 13
          BiDiMode = bdRightToLeft
          Caption = 'lblGrupoFacturacion'
          ParentBiDiMode = False
        end
        object RBDomicilioEntregaOtro: TRadioButton
          Left = 334
          Top = 16
          Width = 49
          Height = 17
          Hint = 
            'Indica que otro es el de entrega de correspondencia (ver o ingre' +
            'sar los datos con el bot'#243'n Editar Domicilio)'
          Caption = '&Otro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = RBDomicilioEntregaOtroClick
        end
        object RBDomicilioEntrega: TRadioButton
          Left = 296
          Top = 16
          Width = 31
          Height = 17
          Hint = 
            'El domicilio ingresado es el de entrega de correspondencia (fact' +
            'uraci'#243'n)'
          Caption = 'Si'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          TabStop = True
          OnClick = RBDomicilioEntregaOtroClick
        end
        object BtnEditarDomicilioEntrega: TButton
          Left = 380
          Top = 15
          Width = 155
          Height = 20
          Hint = 'Editar domicilio de facturaci'#243'n'
          Caption = 'Editar &domicilio de facturaci'#243'n'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtnEditarDomicilioEntregaClick
        end
        object cb_PagoAutomatico: TVariantComboBox
          Left = 143
          Top = 36
          Width = 105
          Height = 21
          Hint = 'Indique si utiliza una forma de pago autom'#225'tica'
          Style = vcsDropDownList
          Color = 16444382
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnChange = cb_PagoAutomaticoChange
          Items = <>
        end
        object btn_EditarPagoAutomatico: TButton
          Left = 252
          Top = 37
          Width = 80
          Height = 20
          Hint = 'Editar medio de pago autom'#225'tico'
          Caption = 'Editar &pago'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = btn_EditarPagoAutomaticoClick
        end
        object vcbTipoDocumentoElectronico: TVariantComboBox
          Left = 180
          Top = 63
          Width = 159
          Height = 21
          Style = vcsDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 5
          OnChange = vcbTipoDocumentoElectronicoChange
          Items = <>
        end
        object btn_ConvenioFacturacion: TButton
          Left = 346
          Top = 89
          Width = 145
          Height = 20
          Hint = 'Seleccionar Convenios de Facturaci'#243'n por cuenta'
          Anchors = [akLeft, akBottom]
          Caption = 'Convenio de Facturacion '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          OnClick = btn_ConvenioFacturacionClick
        end
        object txt_NumeroConvenioFacturacion: TEdit
          Left = 180
          Top = 88
          Width = 159
          Height = 21
          Hint = 'N'#250'mero de Convenio de Facturaci'#243'n'
          Color = 16444382
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 7
        end
        object btn_MedioDoc: TButton
          Left = 1104
          Top = 89
          Width = 82
          Height = 20
          Hint = 'Medio de env'#237'o de los documentos de cobro'
          Anchors = [akRight, akBottom]
          Caption = '&Medio Env'#237'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          OnClick = btn_MedioDocClick
        end
        object txt_RUTFacturacion: TEdit
          Left = 621
          Top = 88
          Width = 117
          Height = 21
          Hint = 'Numero de Documento de Facturaci'#243'n'
          Color = 16444382
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 9
        end
        object btn_RUTFacturacion: TButton
          Left = 741
          Top = 89
          Width = 130
          Height = 20
          Hint = 'Seleccionar Convenios de Facturaci'#243'n por cuenta'
          Anchors = [akLeft, akBottom]
          Caption = 'RUT de Facturacion '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          OnClick = btn_RUTFacturacionClick
        end
        object chkCtaComercialPredeterminada: TCheckBox
          Left = 972
          Top = 65
          Width = 214
          Height = 17
          Anchors = [akTop, akRight]
          Caption = 'Cuenta Comercial Predeterminada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 11
          Visible = False
        end
        object btnGrupoFacturacion: TButton
          Left = 741
          Top = 64
          Width = 130
          Height = 20
          Caption = 'Grupo de Facturaci'#243'n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 12
          OnClick = btnGrupoFacturacionClick
        end
      end
      object cbbTipoConvenio: TVariantComboBox
        Left = 124
        Top = 13
        Width = 160
        Height = 21
        Hint = 'Tipo de Convenio de Facturaci'#243'n'
        Style = vcsDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnChange = cbbTipoConvenioChange
        Items = <>
      end
      object cbbTipoCliente: TVariantComboBox
        Left = 401
        Top = 13
        Width = 160
        Height = 21
        Hint = 'Tipo de Convenio de Facturaci'#243'n'
        Style = vcsDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        Visible = False
        OnChange = cbbTipoConvenioChange
        Items = <>
      end
    end
    object pnlVehiculos: TPanel
      Left = 3
      Top = 561
      Width = 1194
      Height = 212
      Alignment = taLeftJustify
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      DesignSize = (
        1194
        212)
      object Label2: TLabel
        Left = 3
        Top = -1
        Width = 70
        Height = 16
        Caption = 'Veh'#237'culos'
        FocusControl = dblVehiculos
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dblVehiculos: TDBListEx
        Left = 3
        Top = 41
        Width = 1183
        Height = 173
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 125
            Header.Caption = 'Fecha Alta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaAltaCuenta'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 65
            Header.Caption = 'Patente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'Patente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 25
            Header.Caption = 'DV'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'DigitoVerificadorPatente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 200
            Header.Caption = 'Marca'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Arial'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'DescripcionMarca'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 200
            Header.Caption = 'Modelo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'Modelo'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 35
            Header.Caption = 'A'#241'o'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'AnioVehiculo'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Nro. de Tag'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblVehiculosColumnsHeaderClick
            FieldName = 'SerialNumberaMostrar'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 125
            Header.Caption = 'Fecha Vto. TAG'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaDeVencimientoTag'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Convenio de Facturaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'NumeroConvenioFacturacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Cat. Interurbana'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'CategoriaInterurbana'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Cat. Urbana'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'CategoriaUrbana'
          end>
        DataSource = dsVehiculosContacto
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDblClick = dblVehiculosDblClick
        OnDrawText = dblVehiculosDrawText
      end
      object pnl_BotonesVehiculos: TPanel
        Left = 2
        Top = 19
        Width = 760
        Height = 22
        Anchors = [akLeft, akTop, akRight]
        BevelOuter = bvNone
        TabOrder = 1
        object btnAgregarVehiculo: TButton
          Left = 2
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Agregar Veh'#237'culo'
          Caption = '&Agregar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnAgregarVehiculoClick
        end
        object btnEditarVehiculo: TButton
          Left = 77
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Editar Veh'#237'culo'
          Caption = '&Editar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnEditarVehiculoClick
        end
        object btnEliminarVehiculo: TButton
          Left = 152
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Eliminar Veh'#237'culo'
          Caption = 'E&liminar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnEliminarVehiculoClick
        end
        object btn_AsignarTag: TButton
          Left = 227
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Asignar Telev'#237'a'
          Caption = '&Telev'#237'a'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btn_AsignarTagClick
        end
        object btn_EximirFacturacion: TButton
          Left = 302
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Eximir de facturaci'#243'n a los tr'#225'nsitos del veh'#237'culo'
          Caption = 'E&ximir'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnClick = btn_EximirFacturacionClick
        end
        object btn_BonificarFacturacion: TButton
          Left = 377
          Top = 0
          Width = 75
          Height = 20
          Hint = 'Colocar una bonificaci'#243'n para los tr'#225'nsitos del veh'#237'culo'
          Caption = 'Bo&nificar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          OnClick = btn_BonificarFacturacionClick
        end
        object btnConvFactVehiculo: TButton
          Left = 456
          Top = 0
          Width = 123
          Height = 20
          Hint = 'Editar Convenio de Facturaci'#243'n por Veh'#237'culo'
          Caption = '&Convenio Facturaci'#243'n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          OnClick = btnConvFactVehiculoClick
        end
      end
    end
    object pnl_Botones: TPanel
      Left = -21
      Top = 781
      Width = 1223
      Height = 28
      Anchors = [akLeft, akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 2
      DesignSize = (
        1223
        28)
      object lbl_EstadoSolicitud: TLabel
        Left = 17
        Top = 10
        Width = 674
        Height = 13
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'lbl_EstadoSolicitud'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 467
      end
      object NB_Botones: TNotebook
        Left = 883
        Top = 0
        Width = 340
        Height = 28
        Align = alRight
        Anchors = [akTop, akRight]
        PageIndex = 2
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          DesignSize = (
            340
            28)
          object btnSalir: TButton
            Left = 235
            Top = 1
            Width = 92
            Height = 25
            Hint = 'Salir de la Carga'
            Anchors = [akRight, akBottom]
            Caption = '&Salir'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = btnSalirClick
          end
          object btnNuevaSolicitud: TButton
            Left = 33
            Top = 1
            Width = 95
            Height = 25
            Hint = 'Nueva Solicitud'
            Anchors = [akRight, akBottom]
            Caption = 'Nueva &Solicitud'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Visible = False
            OnClick = btnNuevaSolicitudClick
          end
          object btn_NuevoContrato: TButton
            Left = 135
            Top = 1
            Width = 95
            Height = 25
            Hint = 'Nuevo Contrato'
            Anchors = [akRight, akBottom]
            Caption = 'Nuevo &Contrato'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            Visible = False
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageSolicitud'
          DesignSize = (
            340
            28)
          object btnGuardarDatos: TButton
            Left = 136
            Top = 1
            Width = 95
            Height = 25
            Hint = 'Guardar Datos'
            Anchors = [akRight, akBottom]
            Caption = '&Guardar Datos'
            Default = True
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btnGuardarDatosClick
          end
          object btn_Cancelar: TButton
            Left = 236
            Top = 1
            Width = 92
            Height = 25
            Hint = 'Cancelar carga Solicitud'
            Anchors = [akRight, akBottom]
            Cancel = True
            Caption = '&Cancelar'
            ModalResult = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = btn_CancelarClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 3
          Caption = 'PageContrato'
          DesignSize = (
            340
            28)
          object btn_Imprimir: TButton
            Left = 130
            Top = 1
            Width = 95
            Height = 25
            Hint = 'Imprimir Contrato'
            Anchors = [akRight, akBottom]
            Caption = '&Guardar'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btn_ImprimirClick
          end
          object btn_CancelarContrato: TButton
            Left = 236
            Top = 1
            Width = 92
            Height = 25
            Hint = 'Cancelar Carga de Contrato'
            Anchors = [akRight, akBottom]
            Cancel = True
            Caption = '&Cancelar'
            ModalResult = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = btn_CancelarContratoClick
          end
          object btn_GuardarSolicitud: TButton
            Left = 28
            Top = 1
            Width = 96
            Height = 25
            Hint = 'Guardar Datos como Solicitud'
            Anchors = [akRight, akBottom]
            Caption = '&Guardar Solicitud'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            Visible = False
            OnClick = btn_GuardarSolicitudClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 4
          Caption = 'PageCancelar'
          DesignSize = (
            340
            28)
          object btn_SalirSolo: TButton
            Left = 236
            Top = 1
            Width = 92
            Height = 25
            Hint = 'Salir'
            Anchors = [akRight, akBottom]
            Cancel = True
            Caption = '&Salir'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btn_SalirSoloClick
          end
          object btn_BlanquearClave: TButton
            Left = 132
            Top = 1
            Width = 92
            Height = 25
            Caption = '&Blanquear Clave'
            TabOrder = 1
            OnClick = btn_BlanquearClaveClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 5
          Caption = 'PageComvenioPuro'
          object btn_CancelarConvenioPuro: TButton
            Left = 344
            Top = 1
            Width = 92
            Height = 25
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 0
          end
          object btn_ImprimirConvenioPuro: TButton
            Left = 239
            Top = 1
            Width = 92
            Height = 25
            Caption = '&Imprimir'
            TabOrder = 1
          end
        end
      end
    end
  end
  object ActualizarMedioComunicacionContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioComunicacionContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@IndiceMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Direction = pdInputOutput
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Anexo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@HorarioDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@IndiceDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 794
    Top = 8
  end
  object ActualizarDatosContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 250
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 1
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@LugarNacimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@FechaNacimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoActividad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ApellidoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaternoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@NombreContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@SexoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@EmailContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoSituacionIVA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = 'password?'
      end
      item
        Name = '@Pregunta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Respuesta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 702
    Top = 8
  end
  object ActualizarDomicilioContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TipoDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Dpto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@DescripcionCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@IndiceDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 731
    Top = 8
  end
  object ActualizarDomicilioRelacionadoContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioRelacionadoContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Orden'
        Attributes = [paNullable]
        DataType = ftWord
        Direction = pdInputOutput
        Precision = 3
        Value = Null
      end>
    Left = 762
    Top = 8
  end
  object dspVehiculosContacto: TDataSetProvider
    DataSet = ObtenerVehiculosContacto
    ResolveToDataSet = True
    Left = 1125
    Top = 633
  end
  object dsVehiculosContacto: TDataSource
    DataSet = cdsVehiculosContacto
    Left = 1045
    Top = 705
  end
  object ObtenerVehiculosContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterCancel = cdsVehiculosContactoAfterScroll
    ProcedureName = 'ObtenerVehiculosContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftSmallint
        Direction = pdReturnValue
        Precision = 10
        Size = -1
        Value = 0
      end
      item
        Name = '@CodigoSolicitudContacto'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Left = 1129
    Top = 689
  end
  object ActualizarVehiculosContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarVehiculosContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoMarca'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Modelo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@AnioVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoTipoVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoColor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 819
    Top = 8
  end
  object ObtenerDatosSCContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosSContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 878
    Top = 10
  end
  object ObtenerDomiciliosContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomiciliosContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 909
    Top = 10
  end
  object ObtenerMediosComunicacionContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMediosComunicacionContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 932
    Top = 10
  end
  object ActualizarOperacionesFuenteSC: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarOperacionesFuenteSC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraInicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Operador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 671
    Top = 8
  end
  object ActualizarSolicitudContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarSolicitudContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraCreacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@CodigoFuenteSolicitud'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCitaPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoEstadoSolicitud'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFuenteOrigenContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoEstadoResultado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 640
    Top = 8
  end
  object QryTraerMediosComunicacion: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'codigosolicitudcontacto'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'indicepersona'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT s.indicemediocontacto, s.codigotipomediocontacto,'
      's.valor, s.codigoarea, s.anexo, s.horariodesde, s.horariohasta,'
      't.descripcion, t.formato as FormatoMedioContacto'
      
        'FROM solicitudcontactomedioscomunicacion s (NOLOCK), tiposmedioc' +
        'ontacto t (NOLOCK)'
      'WHERE codigosolicitudcontacto = :codigosolicitudcontacto'
      'AND indicepersona = :indicepersona'
      'and s.codigotipomediocontacto = t.codigotipomediocontacto'
      'order by indicemediocontacto')
    Left = 846
    Top = 10
  end
  object ActualizarDomicilioEntregaContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioEntregaContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@IndiceDomicilioEntrega'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 958
    Top = 10
  end
  object ObtenerDatosMedioPagoAutomaticoSolicitud: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosMedioPagoAutomaticoSolicitud;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 744
    Top = 35
  end
  object ActualizarEmailContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEmailContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmailAlta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1048
    Top = 8
  end
  object ActualizarConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CRM_CONVENIO_INSERT;1'
    Parameters = <>
    Left = 32
    Top = 653
  end
  object ActualizarDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Giro'
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@LugarNacimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@FechaNacimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoActividad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ApellidoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaternoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@NombreContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@SexoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@EmailContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoSituacionIVA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Pregunta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Respuesta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RecibeClavePorMail'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@InactivaDomicilioRNUT'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@InactivaMedioComunicacionRNUT'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Semaforo1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Semaforo3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 64
    Top = 653
  end
  object ActualizarConvenioMediosEnvioDocumentos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarConvenioMediosEnvioDocumentos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMedioEnvioDocumento'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioEnvio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 96
    Top = 653
  end
  object ActualizarDatosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 128
    Top = 653
  end
  object ActualizarCuenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'ActualizarCuenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DetallePasadas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoCategoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CentroCosto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaAltaCuenta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaBajaCuenta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoEstadoCuenta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaVencimientoTAG'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaAltaTAG'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaBajaTAG'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ResponsableInstalacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PatenteRNUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DigitoVerificadorPatenteRNUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@AccionRNUT'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Suspendida'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TagEnCliente'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@FechaInicioOriginalEximicion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaInicioEximicion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinalizacionEximicion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoMotivoEximicion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaInicioOriginalBonificacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaInicioBonificacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinalizacionBonificacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PorcentajeBonificacion'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@ImporteBonificacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 160
    Top = 653
  end
  object ActualizarConvenioMedioPagoAutomaticoPAT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarConvenioMedioPagoAutomaticoPAT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoEmisorTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 192
    Top = 651
  end
  object ActualizarConvenioMedioPagoAutomaticoPAC: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarConvenioMedioPagoAutomaticoPAC'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 224
    Top = 653
  end
  object ActualizarMaestroVehiculos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMaestroVehiculos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DigitoVerificadorPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoMarca'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Modelo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@AnioVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoColor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@TieneAcoplado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoCategoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@DigitoVerificadorValido'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@PatenteRVM'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DigitoVerificadorPatenteRVM'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Robado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 256
    Top = 653
  end
  object ActualizarTurnoMovimientoEntrega: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTurnoMovimientoEntrega'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoVehiculo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TieneAcoplado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 288
    Top = 653
  end
  object ActualizarDocumentacionPresentadaConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDocumentacionPresentadaConvenios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumentacionRespaldo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@NoCorresponde'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@PathArchivo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FechaPresentacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaBaja'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TipoPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumentacionPresentada'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 320
    Top = 653
  end
  object ActualizarPersonasDomicilios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarPersonasDomicilios'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Principal'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 360
    Top = 653
  end
  object ActualizarMedioComunicacionPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioComunicacionPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Anexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@HorarioDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Principal'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@EstadoVerificacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoMedioComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 392
    Top = 653
  end
  object ObtenerVehiculo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerVehiculo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end>
    Left = 456
    Top = 653
  end
  object ActualizarDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@DescripcionCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSegmento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Depto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 488
    Top = 653
  end
  object spActualizarRepresentantesConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRepresentantesConvenios'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoClienteConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRepLegal1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRepLegal2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRepLegal3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 520
    Top = 653
  end
  object ActualizarMedioPagoAutomaticoPAC: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioPagoAutomaticoPAC'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Telefono'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 1105
    Top = 10
  end
  object ActualizarMedioPagoAutomaticoPAT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioPagoAutomaticoPAT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Telefono'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoEmisorTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 1073
    Top = 10
  end
  object CrearOrdenServicioAltaCuenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearOrdenServicioAltaCuenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 986
    Top = 7
  end
  object BlanquearPasswordPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'BlanquearPasswordPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = Null
      end>
    Left = 917
    Top = 316
  end
  object PopupMenu: TPopupMenu
    OnPopup = PopupMenuPopup
    Left = 649
    Top = 54
    object mnu_EnviarMail: TMenuItem
      Caption = 'Enviar &Mail'
      OnClick = mnu_EnviarMailClick
    end
  end
  object ADOQuery1: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 612
    Top = 54
  end
  object spObtenerRepresentantesUltimoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRepresentantesUltimoConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersonaConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1020
    Top = 8
  end
  object cdsIndemnizaciones: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'codigoconcepto'
        DataType = ftInteger
      end
      item
        Name = 'descripcionMotivo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ContractSerialNumber'
        DataType = ftLargeint
      end
      item
        Name = 'ContextMark'
        DataType = ftInteger
      end
      item
        Name = 'CodigoAlmacen'
        DataType = ftInteger
      end
      item
        Name = 'TipoasignacionTag'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Patente'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 687
    Top = 710
    Data = {
      020100009619E0BD01000000180000000800000000000300000002010E636F64
      69676F636F6E636570746F0400010000000000116465736372697063696F6E4D
      6F7469766F010049000000010005574944544802000200140014436F6E747261
      637453657269616C4E756D62657208000100000000000B436F6E746578744D61
      726B04000100000000000D436F6469676F416C6D6163656E0400010000000000
      115469706F617369676E6163696F6E5461670100490000000100055749445448
      0200020014000D4F62736572766163696F6E6573010049000000010005574944
      544802000200140007506174656E746501004900000001000557494454480200
      020014000000}
  end
  object spGenerarMovimientoCuentaConTelevia: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GenerarMovimientoCuentaConTelevia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@DescripcionMotivo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@TipoAsignacionTag'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 767
    Top = 630
  end
  object SPAgregarAuditoriaConveniosCAC: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarAuditoriaConveniosCAC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Operacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 12
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Etiqueta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@HostName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@Accion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end>
    Left = 1016
    Top = 304
  end
  object spConveniosActivos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IncluirConveniosDeBaja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 615
    Top = 639
  end
  object spObtenerCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 567
    Top = 703
  end
  object spAgregarMovimientoListasDeAcceso: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarMovimientoListasDeAcceso'
    Parameters = <>
    Left = 792
    Top = 128
  end
  object cdsVehiculosContacto: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoSolicitudContacto'
        DataType = ftInteger
      end
      item
        Name = 'IndiceVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'TipoPatente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoMarca'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionMarca'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Modelo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoColor'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionColor'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AnioVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'ContextMark'
        DataType = ftSmallint
      end
      item
        Name = 'ContractSerialNumber'
        DataType = ftLargeint
      end
      item
        Name = 'TieneAcoplado'
        DataType = ftInteger
      end
      item
        Name = 'DigitoVerificadorPatente'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DigitoVerificadorValido'
        DataType = ftInteger
      end
      item
        Name = 'SerialNumberaMostrar'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'SerialOtraConsecionaria'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'CodigoConcesionaria'
        DataType = ftInteger
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'TagEnClienteOtroConvenio'
        DataType = ftBoolean
      end
      item
        Name = 'TeleviaNuevo'
        DataType = ftBoolean
      end
      item
        Name = 'FechaInicioOriginalEximicion'
        DataType = ftDateTime
      end
      item
        Name = 'FechaInicioEximicion'
        DataType = ftDateTime
      end
      item
        Name = 'FechaFinalizacionEximicion'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoMotivoEximicion'
        DataType = ftSmallint
      end
      item
        Name = 'FechaInicioEximicionEditable'
        DataType = ftBoolean
      end
      item
        Name = 'FechaFinEximicionEditable'
        DataType = ftBoolean
      end
      item
        Name = 'FechaInicioOriginalBonificacion'
        DataType = ftDateTime
      end
      item
        Name = 'FechaInicioBonificacion'
        DataType = ftDateTime
      end
      item
        Name = 'FechaFinalizacionBonificacion'
        DataType = ftDateTime
      end
      item
        Name = 'PorcentajeBonificacion'
        DataType = ftBCD
        Precision = 5
        Size = 2
      end
      item
        Name = 'ImporteBonificacion'
        DataType = ftInteger
      end
      item
        Name = 'FechaInicioBonificacionEditable'
        DataType = ftBoolean
      end
      item
        Name = 'FechaFinBonificacionEditable'
        DataType = ftBoolean
      end
      item
        Name = 'Almacen'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaAltaCuenta'
        DataType = ftDateTime
      end
      item
        Name = 'Robado'
        DataType = ftBoolean
      end
      item
        Name = 'Recuperado'
        DataType = ftBoolean
      end
      item
        Name = 'FechaDeVencimientoTag'
        DataType = ftDateTime
      end
      item
        Name = 'DescripConcesionarias'
        Attributes = [faReadonly, faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MismoConvenio'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'CodigoConvenioFacturacion'
        DataType = ftInteger
      end
      item
        Name = 'NumeroConvenioFacturacion'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Foraneo'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoCategoriaInterurbana'
        DataType = ftInteger
      end
      item
        Name = 'CodigoCategoriaUrbana'
        DataType = ftInteger
      end
      item
        Name = 'CategoriaInterurbana'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'CategoriaUrbana'
        DataType = ftString
        Size = 60
      end>
    IndexDefs = <
      item
        Name = 'DEFAULT_ORDER'
      end
      item
        Name = 'CHANGEINDEX'
      end>
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Size = -1
        Value = 0
      end
      item
        DataType = ftInteger
        Name = '@CodigoSolicitudContacto'
        ParamType = ptInput
        Size = -1
      end>
    ProviderName = 'dspVehiculosContacto'
    StoreDefs = True
    AfterScroll = cdsVehiculosContactoAfterScroll
    Left = 1021
    Top = 641
  end
  object spExistenMovimientosFacturacionInmediata: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ExistenMovimientosFacturacionInmediata'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TieneMovimientos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 983
    Top = 269
  end
  object spRegistrarMovimientoStock: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'RegistrarMovimientoStock'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoMovimiento'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoEmbalaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroInicialTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroFinalTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CategoriaTAG'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EstadoSituacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@EstadoControl'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@GuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaHoraEvento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@MotivoEntrega'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@ConstanciaCarabinero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaHoraConstancia'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DescripcionMotivoRegistro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 798
    Top = 707
  end
end
