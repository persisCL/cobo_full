unit FrmActivarTarjeta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, BuscaClientes, peaprocs, DMConnection,
  ExtCtrls, DB, ADODB, utilproc, util, peaTypes, StrUtils, DPSControls;

type
  TFormActivarTarjeta = class(TForm)
	cb_Cuentas: TComboBox;
	Label2: TLabel;
    btnAceptar: TDPSButton;
    btn_Salir: TDPSButton;
    txt_NumeroTarjeta: TNumericEdit;
    Label3: TLabel;
    Label4: TLabel;
    txt_Clave: TEdit;
    Bevel1: TBevel;
    ActivarTarjetaPrepaga: TADOStoredProc;
    Label1: TLabel;
    lSaldoActual: TLabel;
    Label5: TLabel;
    lImporteTarjeta: TLabel;
    lProximoSaldo: TLabel;
    Label8: TLabel;
    qryObtenerSaldoCuenta: TADOQuery;
    Label6: TLabel;
    LEstadoTarjeta: TLabel;
    ObtenerTarjeta: TADOStoredProc;
    Label7: TLabel;
    LFechaActivacion: TLabel;
    LCuentaActivada: TLabel;
	procedure btnAceptarClick(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
    procedure txt_NumeroTarjetaChange(Sender: TObject);
    procedure cb_CuentasChange(Sender: TObject);
    procedure txt_ClaveChange(Sender: TObject);
  private
    { Private declarations }
  public
	{ Public declarations }
	function Inicializa(CodigoCliente: integer): Boolean;
  end;

var
  FormActivarTarjeta: TFormActivarTarjeta;

implementation

{$R *.dfm}

resourcestring
    CAPTION_TARJETAS_PREPAGAS = 'Activar Tarjeta Prepaga';

function TFormActivarTarjeta.Inicializa(CodigoCliente: integer): Boolean;
resourcestring
    MSG_CLIENTE = 'El cliente no posee cuentas prepagas.';
begin
	Result := True;
	CargarCuentasPrepagas(DMConnections.BaseCAC, cb_Cuentas, CodigoCliente);
	cb_CuentasChange(cb_Cuentas);
	lEstadoTarjeta.Caption := '';
	lFechaActivacion.Caption := '';
	lImporteTarjeta.Caption := '';
	lCuentaActivada.Caption := '';
	lProximoSaldo.Caption := formatFloat(FORMATO_IMPORTE, qryObtenerSaldoCuenta.fieldByName('Saldo').AsFloat);
	if cb_Cuentas.Items.Count = 0 then begin
		result := false;
		msgBox(MSG_CLIENTE, CAPTION_TARJETAS_PREPAGAS, MB_OK);
	end else
		cb_Cuentas.enabled := cb_Cuentas.Items.Count <> 1;
end;

procedure TFormActivarTarjeta.btnAceptarClick(Sender: TObject);
resourcestring
    // Mensajes de error
    CAPTION_VALIDAR_TARJETA  = 'Validar Tarjeta Prepaga';
    MSG_TARJETA_ACTIVADA     = 'La tarjeta ya se encuentra activada.';
    MSG_TARJETA_INHABILITADA = 'La tarjeta esta inhabilitada.';
    MSG_CLAVE_INCORRECTA     = 'La clave de la tarjeta es incorrecta.';
    // Mensajes de confirmacion y error
    MSG_ACTIVAR              = '�Est� seguro de activar la Tarjeta seleccionada?';
    MSG_ERROR_ACTIVACION     = 'No se pudo activar la Tarjeta Prepaga.';
begin
	// Primero reviso la Tarjeta
	// Me fijo el estado, si ya no esta asignada a una cuenta y si la password es correcta
	if not ObtenerTarjeta.FieldByName('FechaActivacion').IsNull then begin
		msgBox(MSG_TARJETA_ACTIVADA, CAPTION_VALIDAR_TARJETA, MB_OK);
		exit;
	end;
	if (ObtenerTarjeta.FieldByName('Estado').AsString <> 'N') then begin
		msgBox(MSG_TARJETA_INHABILITADA, CAPTION_VALIDAR_TARJETA, MB_OK);
		exit;
	end;
	if (trim(ObtenerTarjeta.FieldByName('Clave').AsString) <> trim(txt_Clave.Text)) then begin
		msgBox(MSG_CLAVE_INCORRECTA, CAPTION_VALIDAR_TARJETA, MB_OK);
		exit;
	end;
	//si esta todo ok grabo el movimiento e inhabilito la tarjeta
	if MsgBox(MSG_ACTIVAR, CAPTION_TARJETAS_PREPAGAS, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
			ActivarTarjetaPrepaga.Parameters.ParamByName('@NumeroTarjeta').Value	:= txt_NumeroTarjeta.Value;
			ActivarTarjetaPrepaga.Parameters.ParamByName('@CodigoCuenta').Value   	:= Ival(StrRight(cb_Cuentas.Text, 10));
			ActivarTarjetaPrepaga.ExecProc;
			if ActivarTarjetaPrepaga.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
				MsgBoxErr(MSG_ERROR_ACTIVACION, ActivarTarjetaPrepaga.Parameters.ParamByName('@DescriError').Value,
				  CAPTION_TARJETAS_PREPAGAS, MB_ICONSTOP);
				Exit;
			end;
			ActivarTarjetaPrepaga.Close;
		except
			On E: exception do begin
				MsgBoxErr(MSG_ERROR_ACTIVACION, e.Message, CAPTION_TARJETAS_PREPAGAS, MB_ICONSTOP);
				ActivarTarjetaPrepaga.Close;
			end;
		end;
		ModalResult := mrOk;
	end;
end;

procedure TFormActivarTarjeta.btn_SalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormActivarTarjeta.txt_NumeroTarjetaChange(Sender: TObject);
resourcestring
    MSG_TARJETA_NOACTIVA = 'La Tarjeta Prepaga no esta activada.';
    MSG_ACTIVA_CUENTA    = 'Activa en la cuenta %d - %s';
var i: Integer;
begin
	// Obtengo la tarjeta y muestro la informaci�n asociada
	// Si esta activa a una cuenta del cliente actual lo muestro
	with ObtenerTarjeta do begin
		Close;
		Parameters.ParamByName('@NumeroTarjeta').value := txt_NumeroTarjeta.ValueInt;
		Open;
		if isEmpty then begin
			lEstadoTarjeta.Caption      := '';
			lFechaActivacion.Caption    := '';
			lImporteTarjeta.Caption     := formatFloat(FORMATO_IMPORTE, 0);
			lCuentaActivada.Caption     := '';
		end else begin
			lEstadoTarjeta.Caption := FieldByName('DescriEstado').AsString;
			if FieldByName('FechaActivacion').IsNull then
				lFechaActivacion.Caption := MSG_TARJETA_NOACTIVA
			else begin
				i := 0;
				while (i < cb_Cuentas.Items.Count) and
				  (FieldByName('CodigoCuenta').asInteger <> IVal(RightStr(cb_Cuentas.Items[i], 10))) do
					inc(i);
				lCuentaActivada.Caption := '';
				if i < cb_Cuentas.Items.count then begin
					lCuentaActivada.Caption := Format(MSG_ACTIVA_CUENTA,
					  [FieldByName('CodigoCuenta').AsInteger, FieldByName('Cuenta').AsString]);
				end;
				lFechaActivacion.Caption := FieldByName('DescriFechaActivacion').AsString;
			end;
			lImporteTarjeta.Caption := FieldByName('DescriImporte').AsString;
			lProximoSaldo.Caption := formatFloat(FORMATO_IMPORTE,
			  qryObtenerSaldoCuenta.fieldByName('Saldo').AsFloat + FieldByName('Importe').asFloat);
		end;
	end;
end;

procedure TFormActivarTarjeta.cb_CuentasChange(Sender: TObject);
var importe: Double;
begin
	with qryObtenerSaldoCuenta do begin
		close;
		parameters.ParamByName('CodigoCuenta').Value := IVal(RightStr(cb_Cuentas.text, 10));
		Open;
		lSaldoActual.Caption := formatFloat(FORMATO_IMPORTE, FieldByName('Saldo').asFloat);
		importe := 0;
		if not ObtenerTarjeta.IsEmpty then
			importe := ObtenerTarjeta.FieldByName('Importe').asFloat;
		lProximoSaldo.Caption := formatFloat(FORMATO_IMPORTE, fieldByName('Saldo').AsFloat + importe);
	end;
end;

procedure TFormActivarTarjeta.txt_ClaveChange(Sender: TObject);
begin
	btnAceptar.Enabled := (txt_NumeroTarjeta.ValueInt <> 0) and (trim(txt_Clave.Text) <> '');
end;

end.
