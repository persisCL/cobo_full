{********************************** File Header ********************************
File Name   : frmInfoPlanesComerciales
Author      : Castro, Ra�l <rcastro@dpsautomation.com>
Date Created: 19-Ene-2004
Language    : ES-AR
Description : Visualiza informaci�n descriptiva de los distintos Planes
			  Comerciales
*******************************************************************************}

unit frmInfoPlanesComerciales;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, DB, ADODB, Util, PeaProcs, UtilProc,
  DPSControls, UtilFacturacion;

type
  TFInfoPlanesComerciales = class(TForm)
	Panel25: TPanel;
	Label4: TLabel;
	cb_PlanesComerciales: TComboBox;
	REditPlanComercial: TRichEdit;
	qry_PlanComercial: TADOQuery;
    qry_CategoriaPlan: TADOQuery;
	qry_TipoPago: TADOQuery;
	qry_PlanesComPosibles: TADOQuery;
	Salir: TDPSButton;
	procedure cb_PlanesComercialesChange(Sender: TObject);
	procedure FormActivate(Sender: TObject);
	procedure SalirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
	Function Inicializar: Boolean;
  end;

var
  FInfoPlanesComerciales: TFInfoPlanesComerciales;

implementation

uses DMConnection;

{$R *.dfm}

procedure TFInfoPlanesComerciales.cb_PlanesComercialesChange(Sender: TObject);
resourcestring
	TITULO              = 'Datos del Plan comercial: ';
	TITULO_CATEGORIAS   = 'Categor�as: ';
	TITULO_TIPO_PAGO    = 'Formas de pago: ';
	TITULO_POSIBILIDAD  = 'Posibilidades de adhesi�n: ';

	procedure AgregarTexto(re: TRichEdit; const Texto: AnsiString;
	  FontName: TFontName = 'Arial'; FontSize: Integer = 10; FontStyle: TFontStyles = [];
	  FontColor: TColor = clBlack);
	begin
		re.SelAttributes.Name := FontName;
		re.SelAttributes.Size := FontSize;
		re.SelAttributes.Style := FontStyle;
		re.SelAttributes.Color := FontColor;
		re.SelText := Texto;
	end;

var
	plan: integer;

begin
	REditPlanComercial.Clear;

	if not (cb_PlanesComerciales.ItemIndex >= 0) then exit;

	plan := IVal(StrRight(cb_PlanesComerciales.Text, 20));

	with qry_PlanComercial do
	begin
		Close;
		Parameters.ParamByName('PlanCom').Value := plan;
		Open;

		//TITULO
		AgregarTexto(REditPlanComercial, CRLF + trim(FieldByName('Descripcion').AsString), 'Arial', 11, [fsBold, fsUnderline], clGreen);

		if not FieldByName('Observaciones').IsNull then AgregarTexto(REditPlanComercial, CRLF+ CRLF + FieldByName('Observaciones').asstring, 'Arial', 10);

		Close;
	end;

	//Categor�as del plan
	with qry_CategoriaPlan do
	begin
		Close;
		Parameters.ParamByName('PlanCom').Value := plan;
		Open;
		if not EOF then begin
			AgregarTexto(REditPlanComercial,CRLF +
					CRLF + TITULO_CATEGORIAS, 'Arial', 10, [fsBold]);
			first;
			while not EOF do
			begin
				AgregarTexto(REditPlanComercial,
				  CRLF + '   - ' + FieldByName('DescCategoria').asstring, 'Arial', 10);
				Next;
			end;
		end;
		Close;
	end;

	//Tipos de pago del plan
	with qry_TipoPago do
	begin
		Close;
		Parameters.ParamByName('PlanCom').Value := plan;
		Open;
		if not EOF then begin
			AgregarTexto(REditPlanComercial,CRLF +
					CRLF + TITULO_TIPO_PAGO, 'Arial', 10, [fsBold]);
			first;
			while not EOF do
			begin
				AgregarTexto(REditPlanComercial,
				  CRLF + '   - ' + FieldByName('DescTipoPago').asstring, 'Arial', 10);
				Next;
			end;
		end;
		Close;
    end;

	//Posibilidades de adhesion (PLANESCOMERCIALESPOSIBLES)
	with qry_PlanesComPosibles do
	begin
		Close;
		Parameters.ParamByName('PlanCom').Value := plan;
		Open;
		if not EOF then begin
			AgregarTexto(REditPlanComercial,CRLF +
					CRLF + TITULO_POSIBILIDAD, 'Arial', 10, [fsBold]);
			first;
			while not EOF do
			begin
				AgregarTexto(REditPlanComercial,
				  CRLF + '   - ' + FieldByName('DescCliente').asstring + ', ' +
				  FieldByName('DescAdhesion').asstring, 'Arial', 10);
				Next;
			end;
		end;
		Close;
	end;
end;

function TFInfoPlanesComerciales.Inicializar: Boolean;
resourceString
    MSG_INI_ERROR = 'Error inicializando el formulario.';
    MSG_INI_CAPTION = 'Error de inicializaci�n.';
begin
    try
    	CargarPlanesComerciales(DMConnections.BaseCAC, cb_PlanesComerciales);
	    cb_PlanesComerciales.ItemIndex := 0;
    	cb_PlanesComercialesChange(cb_PlanesComerciales);
        Result  := True;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INI_ERROR, e.message, MSG_INI_CAPTION, MB_ICONSTOP);
            Result := False;
        end;
    end;
end;

procedure TFInfoPlanesComerciales.FormActivate(Sender: TObject);
begin
	Inicializar
end;

procedure TFInfoPlanesComerciales.SalirClick(Sender: TObject);
begin
  Close
end;

end.
