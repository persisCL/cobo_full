{-----------------------------------------------------------------------------
 File Name: FrmRespuestaRNUT
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 24/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit FrmRespuestaRNUT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,UtilProc, StdCtrls, DPSControls, ExtCtrls, DmConnection, DB,
  ADODB, ListBoxEx, DBListEx, Util, ComCtrls, ImgList, DBClient, Provider,
  DmiCtrls;

type
  TFormRespuestaRNUT = class(TForm)
    pnl_Botones: TPanel;
    PControl: TPageControl;
    P_Recha_Entrada: TTabSheet;
    lbl_CantConvAfectadosEntrada: TLabel;
    Label3: TLabel;
    cb_CodigosErrorEntradasRNUT: TComboBox;
    dbl_RechazosImportacionRNUT: TDBListEx;
    ObtenerRechazosImportacionRNUT: TADOStoredProc;
    DS_ObtenerRechazosImportacionRNUT: TDataSource;
    Label4: TLabel;
    lbl_CantMostrarEntrada: TLabel;
    P_Recha_Salida: TTabSheet;
    cb_Estados: TComboBox;
    cb_Codigo_Error: TComboBox;
    dbl_Convenio: TDBListEx;
    txt_NumConvenio: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    lbl_CantConvAfectados: TLabel;
    lbl_CantMostrar: TLabel;
    ds_ObtenerConvenioRespuestaRNUT: TDataSource;
    ObtenerConvenioRespuestaRNUT: TADOStoredProc;
    ds_ObtenerDetalleRespuesta: TDataSource;
    ObtenerDetalleRespuesta: TADOStoredProc;
    ds_ObtenerEnviosConvenioRNUT: TDataSource;
    ObtenerEnviosConvenioRNUT: TADOStoredProc;
    ReenviarSalidaRNUT: TADOStoredProc;
    Imagenes: TImageList;
    cdsObtenerConvenioRespuestaRNUT: TClientDataSet;
    cdsObtenerConvenioRespuestaRNUTNumeroConvenio: TStringField;
    cdsObtenerConvenioRespuestaRNUTCantConveniosAfectados: TIntegerField;
    cdsObtenerConvenioRespuestaRNUTNumeroConvenioRNUT: TStringField;
    cdsObtenerConvenioRespuestaRNUTEstado: TStringField;
    cdsObtenerConvenioRespuestaRNUTFechaEnvio: TDateTimeField;
    cdsObtenerConvenioRespuestaRNUTMarcado: TBooleanField;
    pnl_BotonesConvenio: TPanel;
    Label5: TLabel;
    lbl_TipoError: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    txt_TOP: TNumericEdit;
    Label6: TLabel;
    GroupBox1: TGroupBox;
    dbl_DetalleRespuesta: TDBListEx;
    dbl_DetalleArchivos: TDBListEx;
    ObtenerConvenioRespuestaRNUTNumeroConvenio: TStringField;
    ObtenerConvenioRespuestaRNUTCantConveniosAfectados: TIntegerField;
    ObtenerConvenioRespuestaRNUTNumeroConvenioRNUT: TStringField;
    ObtenerConvenioRespuestaRNUTEstado: TStringField;
    ObtenerConvenioRespuestaRNUTFechaEnvio: TDateTimeField;
    ObtenerConvenioRespuestaRNUTMarcado: TIntegerField;
    ObtenerConvenioRespuestaRNUTCOMPLEMENTO: TStringField;
    cdsObtenerConvenioRespuestaRNUTCOMPLEMENTO: TStringField;
    P_Recha_Lista: TTabSheet;
    Label9: TLabel;
    Label10: TLabel;
    lbl_CantConvAfectados_Lista: TLabel;
    lbl_CantMostrar_Lista: TLabel;
    Label13: TLabel;
    lbl_TipoError_Lista: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    cb_Estados_Lista: TComboBox;
    cb_Codigo_Error_Lista: TComboBox;
    txt_NumTelevia: TEdit;
    txt_TOP_Lista: TNumericEdit;
    dbl_Convenio_Lista: TDBListEx;
    GroupBox2: TGroupBox;
    dbl_DetalleRespuesta_Lista: TDBListEx;
    dbl_DetalleArchivos_Lista: TDBListEx;
    ObtenerListasRespuestaRNUT: TADOStoredProc;
    ds_ObtenerListasRespuestaRNUT: TDataSource;
    ds_ObtenerListaDetallleRespuestaRNUT: TDataSource;
    ObtenerListaDetallleRespuestaRNUT: TADOStoredProc;
    ds_ObtenerListaEnviosRNUT: TDataSource;
    ObtenerListaEnviosRNUT: TADOStoredProc;
    btn_salir: TButton;
    btn_Reenviar: TButton;
    btn_Invertir: TButton;
    btn_Marcar: TButton;
    btnMarcarTodos: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_salirClick(Sender: TObject);
    procedure cb_EstadosChange(Sender: TObject);
    procedure ObtenerDetalleRespuestaAfterScroll(DataSet: TDataSet);
    procedure ObtenerConvenioRespuestaRNUTAfterOpen(DataSet: TDataSet);
    procedure PControlChange(Sender: TObject);
    procedure cb_CodigosErrorEntradasRNUTChange(Sender: TObject);
    procedure ObtenerRechazosImportacionRNUTAfterOpen(DataSet: TDataSet);
    procedure btn_ReenviarClick(Sender: TObject);
    procedure cb_Codigo_ErrorChange(Sender: TObject);
    procedure txt_NumConvenioChange(Sender: TObject);
    procedure txt_NumConvenioKeyPress(Sender: TObject; var Key: Char);
    procedure dbl_ConvenioColumns0HeaderClick(Sender: TObject);
    procedure dbl_ConvenioDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btn_MarcarClick(Sender: TObject);
    procedure btn_InvertirClick(Sender: TObject);
    procedure dbl_ConvenioClick(Sender: TObject);
    procedure txt_TOPChange(Sender: TObject);
    procedure txt_TOPExit(Sender: TObject);
    procedure cdsObtenerConvenioRespuestaRNUTAfterScroll(
      DataSet: TDataSet);
    procedure dbl_ConvenioDblClick(Sender: TObject);
    procedure btnMarcarTodosClick(Sender: TObject);
    procedure cb_Estados_ListaChange(Sender: TObject);
    procedure cb_Codigo_Error_ListaChange(Sender: TObject);
    procedure txt_NumTeleviaChange(Sender: TObject);
    procedure txt_TOP_ListaExit(Sender: TObject);
    procedure ObtenerListasRespuestaRNUTAfterOpen(DataSet: TDataSet);
    procedure dbl_Convenio_ListaClick(Sender: TObject);
    procedure dbl_Convenio_ListaColumns0HeaderClick(Sender: TObject);
    procedure ObtenerListaDetallleRespuestaRNUTAfterScroll(
      DataSet: TDataSet);
    procedure txt_NumTeleviaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
        CantMarcada: Integer;
        function CargarComboErroresImportacionRNUT(Combo: TComboBox; Conn: TADOConnection): Boolean;
        procedure CargarComboRespuestaEnvioRNUT(Conn: TADOConnection; Combo: TComboBox; Todos: Boolean = True; IncluirCodigo1: Boolean = False);
        function DarRut: Variant;
        function DarCodigoEstado: String;
        function DarCodigoRespuesta: Variant;
        function DarTop: Variant;
        function DarRutLista: Variant;
        function DarCodigoEstadoLista: String;
        function DarCodigoRespuestaLista: Variant;
        function DarTopLista: Variant;
        procedure ObtenerLista(CodigoEstado, CodigoRespuesta, NumeroTag, Top: Variant);

        procedure ObtenerConvenio(CodigoEstado, CodigoRespuesta, NumeroConvenio, Top : Variant);
        procedure CargarGrilla;
  public
    { Public declarations }
    function Inicializar(MDIChild: Boolean): Boolean;
  end;

var
  FormRespuestaRNUT: TFormRespuestaRNUT;

implementation

uses RStrings;

{$R *.dfm}

procedure TFormRespuestaRNUT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	if FormStyle <> fsNormal then Action :=  caFree;
end;

function TFormRespuestaRNUT.Inicializar(MDIChild: Boolean): Boolean;
ResourceString
    ERROR_INICIALIZACION = 'Error de Inicialización';
Var
	S: TSize;
begin
    Result := False;

    if MDIChild then begin
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
    end else begin
        FormStyle := fsNormal;
        Visible := False;
    end;

    PControl.ActivePage := P_Recha_Salida;
    try
        PControl.OnChange(PControl);
        Result := True;
    except
        on e: Exception do MsgBoxErr(ERROR_INICIALIZACION, e.Message, Caption, MB_ICONSTOP);
    end;
end;

procedure TFormRespuestaRNUT.btn_salirClick(Sender: TObject);
begin
    close;
end;

procedure TFormRespuestaRNUT.cb_EstadosChange(Sender: TObject);
begin
    ObtenerDetalleRespuesta.Close;
    ObtenerEnviosConvenioRNUT.Close;

    if StrRight(cb_Estados.Text, 1) = 'E' then begin
        CargarComboRespuestaEnvioRNUT(DMConnections.BaseCAC, cb_Codigo_Error);
    end;

    if cb_Codigo_Error.Visible then cb_Codigo_Error.OnChange(cb_Codigo_Error)
    else ObtenerConvenio(DarCodigoEstado, Null, DarRut, DarTOP);
end;

procedure TFormRespuestaRNUT.ObtenerDetalleRespuestaAfterScroll(
  DataSet: TDataSet);
begin
    Screen.Cursor := crHourGlass;
    try
        with ObtenerEnviosConvenioRNUT do begin
            Close;
            Parameters.ParamByName('@NumeroConvenioRNUT').Value := ObtenerDetalleRespuesta.FieldByName('NumeroConvenioRNUT').Value;
            Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := ObtenerDetalleRespuesta.FieldByName('CodigoEnvioConvenioRNUT').Value;
            Open;
        end;
    finally
        Screen.Cursor := crDefault;
    end; // finally

end;

procedure TFormRespuestaRNUT.ObtenerConvenioRespuestaRNUTAfterOpen(
  DataSet: TDataSet);
begin
    cb_Codigo_Error.Visible := DarCodigoEstado = 'E';
    lbl_TipoError.Visible := cb_Codigo_Error.Visible;

    if ObtenerConvenioRespuestaRNUT.RecordCount = 0 then begin
        lbl_CantConvAfectados.Caption := '0';
        lbl_CantMostrar.Caption := '0';
        EnableControlsInContainer(pnl_BotonesConvenio, False);
    end else begin
        lbl_CantConvAfectados.Caption := ObtenerConvenioRespuestaRNUT.FieldByName('CantConveniosAfectados').AsString;
		lbl_CantMostrar.Caption := IntToStr(ObtenerConvenioRespuestaRNUT.RecordCount);
		EnableControlsInContainer(pnl_BotonesConvenio, (DarCodigoEstado = 'E') or (DarCodigoEstado = 'O') or (DarCodigoEstado = 'S'));
	end;
	dbl_Convenio.Columns.Items[1].Sorting := csAscending;
end;

procedure TFormRespuestaRNUT.PControlChange(Sender: TObject);
resourcestring
	STR_ERROR_INICIACION = 'Error al inicialzar combo de error';
	STR_ERROR_PAGE_CONTROL = 'Error, Page Control no definida';
	STR_PENDIENTE = 'Pendiente';
	STR_CONFIRMADO_CON_RNUT = 'Confirmado (Con RNUT)';
	STR_CONFIRMADO_SIN_RNUT = 'Confirmado (Sin RNUT)';
	STR_SIN_ENVIAR = 'Sin Enviar';
begin
	// Cierro todos los SP
	ObtenerConvenioRespuestaRNUT.Close;
	ObtenerRechazosImportacionRNUT.Close;
	ObtenerListasRespuestaRNUT.Close;
	ObtenerListaDetallleRespuestaRNUT.Close;
	ObtenerListaEnviosRNUT.Close;

	if PControl.ActivePage = P_Recha_Salida then begin
			with cb_Estados do begin
				Clear;
				items.Add(STR_ERROR + Space(100) + 'E');
				items.Add(STR_PENDIENTE + Space(100) + 'P');
				items.Add(STR_CONFIRMADO_CON_RNUT + Space(100) + 'O');
    			Items.Add(STR_CONFIRMADO_SIN_RNUT + Space(100) + 'S');
				ItemIndex := 0;
			end;
			Refresh;
			cb_Estados.OnChange(cb_Estados);
	end else begin
		if PControl.ActivePage = P_Recha_Entrada then begin
			if not CargarComboErroresImportacionRNUT(cb_CodigosErrorEntradasRNUT, DMConnections.BaseCAC) then begin
				raise Exception.Create(STR_ERROR_INICIACION);
				exit;
			end;
			Refresh;
			cb_CodigosErrorEntradasRNUT.OnChange(cb_CodigosErrorEntradasRNUT);
		end else begin
			if PControl.ActivePage = P_Recha_Lista then begin
				with cb_Estados_Lista do begin
					Clear;
					Items.Add(STR_ERROR + Space(100) + 'E');
					Items.Add(STR_PENDIENTE + Space(100) + 'P');
					Items.Add(STR_CONFIRMADO_CON_RNUT + Space(100) + 'O');
					Items.Add(STR_CONFIRMADO_SIN_RNUT + Space(100) + 'S');
					Items.Add(STR_SIN_ENVIAR + Space(100) + 'N');
					ItemIndex := 0;
                end;
                Refresh;
                cb_Estados_Lista.OnChange(cb_Estados_Lista);
			end else  raise Exception.Create(STR_ERROR_PAGE_CONTROL);
        end;
    end;
end;

procedure TFormRespuestaRNUT.cb_CodigosErrorEntradasRNUTChange(
  Sender: TObject);
begin
    Screen.Cursor := crHourGlass;
    try
        with ObtenerRechazosImportacionRNUT do begin
            Close;
            if StrRight(cb_CodigosErrorEntradasRNUT.Text,2) = '-1' then Parameters.ParamByName('@CodigoEstado').Value := NULL
            else Parameters.ParamByName('@CodigoEstado').Value := StrRight(cb_CodigosErrorEntradasRNUT.Text,3);
            Open;
        end;
    finally
        Screen.Cursor := crDefault;
    end; // finally

    dbl_RechazosImportacionRNUT.SetFocus;

end;

procedure TFormRespuestaRNUT.ObtenerRechazosImportacionRNUTAfterOpen(
  DataSet: TDataSet);
begin
    if ObtenerRechazosImportacionRNUT.RecordCount = 0 then
        lbl_CantConvAfectadosEntrada.Caption := '0'
    else
        lbl_CantConvAfectadosEntrada.Caption := ObtenerRechazosImportacionRNUT.FieldByName('CantidadTotal').AsString;

    lbl_CantMostrarEntrada.Caption := IntToStr(ObtenerRechazosImportacionRNUT.RecordCount);
end;

{-----------------------------------------------------------------------------
  Function Name: CargarComboErroresImportacionRNUT
  Author:
  Date Created:  /  /
  Description:
  Parameters: Combo: TComboBox; Conn: TADOConnection
  Return Value: Boolean

  Revision : 1
    Date: 24/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TFormRespuestaRNUT.CargarComboErroresImportacionRNUT(Combo: TComboBox; Conn: TADOConnection): Boolean;
var
	SP: TADOQuery;
resourcestring
	STR_TODOS_ERRORES = 'Todos los Errores';
begin
    Screen.Cursor := crHourGlass;
    try
        try
            SP := TADOQuery.Create(nil);
            SP.SQL.Text := 'SELECT CodigoEstado, Descripcion FROM EstadoImportacionRNUT  WITH (NOLOCK) ORDER BY CodigoEstado';
            SP.Connection := Conn;
            SP.Open;

            Combo.Items.clear;
            Combo.Items.add(STR_TODOS_ERRORES + space(400) + '-1');

            while not SP.Eof do begin
                Combo.Items.add(Trim(SP.FieldByName('Descripcion').AsString) + space(400) + SP.FieldByName('CodigoEstado').AsString);
                SP.Next;
            end;
            Combo.ItemIndex := 0;
            Result := True;
        except
            Result := False;
        end;
    finally
        Screen.Cursor := crDefault;
    end; // finally

end;

procedure TFormRespuestaRNUT.btn_ReenviarClick(Sender: TObject);
resourcestring
	STR_ERROR_REENVIO = 'Error al Tratar de Reenviar Convenio';
begin
    cdsObtenerConvenioRespuestaRNUT.DisableControls;
    try
        with cdsObtenerConvenioRespuestaRNUT do begin
            First;
            while not eof do begin
                if FieldByName('Marcado').AsBoolean then begin
                    try
                        with ReenviarSalidaRNUT do begin
                            Parameters.ParamByName('@NumeroConvenioRNUT').Value := cdsObtenerConvenioRespuestaRNUT.FieldByName('NumeroConvenioRNUT').Value;
                            ExecProc;
                        end;
                    except
                        on e: Exception do begin
                            MsgBoxErr(STR_ERROR_REENVIO,e.Message, Caption, MB_ICONSTOP);
                        end;
                    end;
                end;
                Next;
            end;
        end;

        ReenviarSalidaRNUT.Close;
        ObtenerConvenioRespuestaRNUT.Close;
        ObtenerConvenio(DarCodigoEstado, DarCodigoRespuesta, DarRut, DarTop);
    finally
        cdsObtenerConvenioRespuestaRNUT.EnableControls;
    end; // finally

end;

procedure TFormRespuestaRNUT.cb_Codigo_ErrorChange(Sender: TObject);
begin
    ObtenerConvenio(DarCodigoEstado, DarCodigoRespuesta, DarRut, DarTop);
end;

procedure TFormRespuestaRNUT.txt_NumConvenioChange(Sender: TObject);
begin
    if (Length(txt_NumConvenio.Text) >= 17) or (Length(txt_NumConvenio.Text) = 0) then
        ObtenerConvenio(DarCodigoEstado, DarCodigoRespuesta, DarRut, DarTop);
end;

procedure TFormRespuestaRNUT.txt_NumConvenioKeyPress(Sender: TObject;
  var Key: Char);
begin
    if Key in ['0'..'9','k','K',#8] then Key := Key
    else Key := #0;
end;

procedure TFormRespuestaRNUT.dbl_ConvenioColumns0HeaderClick(
  Sender: TObject);
begin

    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;

    ObtenerConvenioRespuestaRNUT.Sort := TDBListExColumn(sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
    CargarGrilla;

end;

procedure TFormRespuestaRNUT.CargarComboRespuestaEnvioRNUT(Conn: TADOConnection; Combo: TComboBox; Todos: Boolean = True; IncluirCodigo1: Boolean = False);
Var
	SP: TADOStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    try
        Screen.Cursor := crHourGlass;
        SP.Connection := Conn;
        SP.ProcedureName := 'ObtenerTiposRespuestaProcesoRNUT';
        SP.Parameters.Refresh;
        SP.Open;
        combo.Clear;
        if Todos then combo.Items.Add('TODOS' + space(100) + '0');

        While not SP.Eof do begin
            if  IncluirCodigo1 or (SP.FieldByName('CodigoRespuesta').AsInteger > 1) then begin
                Combo.Items.Add(UpperCase(SP.FieldByName('Descripcion').AsString) +
                    Space(100) +
                    SP.FieldByName('CodigoRespuesta').AsString);
            end;
            SP.Next;
        end;
        Combo.ItemIndex := 0;
    finally
        SP.Close;
        SP.Free;
        Screen.Cursor := crDefault;
    end;
end;

function TFormRespuestaRNUT.DarCodigoRespuesta: Variant;
begin
    if not cb_Codigo_Error.Visible or (Trim(StrRight(cb_Codigo_Error.Text, 2)) = '0') or (Trim(StrRight(cb_Codigo_Error.Text, 2)) = '') then Result := Null
    else Result := Trim(StrRight(cb_Codigo_Error.Text, 2));
end;


function TFormRespuestaRNUT.DarRut: Variant;
begin
    if Trim(txt_NumConvenio.Text) = '' then Result := Null
    else Result := Trim(txt_NumConvenio.Text);
end;

procedure TFormRespuestaRNUT.dbl_ConvenioDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
var bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Marcado') then begin
            Text := '';
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            if (cdsObtenerConvenioRespuestaRNUT.FieldByName('Marcado').AsBoolean ) then
          	    Imagenes.GetBitmap(0, Bmp)
            else
                Imagenes.GetBitmap(1, Bmp);
            canvas.Draw(rect.Left, rect.Top,bmp);
            bmp.Free;
        end;
    end;
end;

procedure TFormRespuestaRNUT.ObtenerConvenio(CodigoEstado, CodigoRespuesta,
  NumeroConvenio, Top: Variant);
begin
    Screen.Cursor := crHourGlass;
    try
        ObtenerDetalleRespuesta.Close;
        ObtenerEnviosConvenioRNUT.Close;

        with ObtenerConvenioRespuestaRNUT do begin
            Close;
            Parameters.ParamByName('@Estado').Value             := CodigoEstado;
            Parameters.ParamByName('@CodigoRespuesta').Value    := CodigoRespuesta;
            Parameters.ParamByName('@NumeroConvenio').Value     := NumeroConvenio;
            Parameters.ParamByName('@Top').Value                := TOP;
            Open;
        end;
        CargarGrilla;
    finally
        Screen.Cursor := crDefault;
    end;
end;

function TFormRespuestaRNUT.DarCodigoEstado: String;
begin
    Result := StrRight(cb_Estados.Text, 1);
end;

procedure TFormRespuestaRNUT.btn_MarcarClick(Sender: TObject);
var
    auxValor: Boolean;
begin
    with cdsObtenerConvenioRespuestaRNUT do begin
        auxValor := not FieldByName('Marcado').AsBoolean;
        if auxValor then inc(CantMarcada)
        else dec(CantMarcada);

        Edit;
        FieldByName('Marcado').Value := iif(auxValor,1,0);
        post;

        btn_Reenviar.Enabled := CantMarcada > 0;
    end;
    dbl_Convenio.SetFocus;
end;

procedure TFormRespuestaRNUT.btn_InvertirClick(Sender: TObject);
var
    Book: TBookmark;
begin
    cdsObtenerConvenioRespuestaRNUT.DisableControls;
    Book := cdsObtenerConvenioRespuestaRNUT.GetBookmark;
    cdsObtenerConvenioRespuestaRNUT.First;
    while not cdsObtenerConvenioRespuestaRNUT.eof do begin
        btn_Marcar.Click;
        cdsObtenerConvenioRespuestaRNUT.next;
    end;
    cdsObtenerConvenioRespuestaRNUT.GotoBookmark(book);
    cdsObtenerConvenioRespuestaRNUT.FreeBookmark(Book);
    cdsObtenerConvenioRespuestaRNUT.EnableControls;
    dbl_Convenio.SetFocus;
end;

procedure TFormRespuestaRNUT.dbl_ConvenioClick(Sender: TObject);
begin
    Screen.Cursor := crHourGlass;
    try
        ObtenerDetalleRespuesta.Close;
        ObtenerDetalleRespuesta.Close;
        ObtenerEnviosConvenioRNUT.Close;
        ObtenerDetalleRespuesta.Parameters.ParamByName('@NumeroConvenioRNUT').Value := cdsObtenerConvenioRespuestaRNUT.FieldByName('NumeroConvenioRNUT').Value;
        ObtenerDetalleRespuesta.Open;
    finally
        Screen.Cursor := crDefault;
    end; // finally

end;


procedure TFormRespuestaRNUT.CargarGrilla;
begin
    CantMarcada := 0;
    cdsObtenerConvenioRespuestaRNUT.DisableControls;
    with ObtenerConvenioRespuestaRNUT do begin
        cdsObtenerConvenioRespuestaRNUT.EmptyDataSet;
        while not eof do begin
            cdsObtenerConvenioRespuestaRNUT.Append;
            cdsObtenerConvenioRespuestaRNUT.FieldByName('NumeroConvenio').Value := FieldByName('NumeroConvenio').Value;
            cdsObtenerConvenioRespuestaRNUT.FieldByName('NumeroConvenioRNUT').Value := FieldByName('NumeroConvenioRNUT').Value;
            cdsObtenerConvenioRespuestaRNUT.FieldByName('FechaEnvio').Value := FieldByName('FechaEnvio').Value;
            cdsObtenerConvenioRespuestaRNUT.FieldByName('Marcado').Value := 0;
            cdsObtenerConvenioRespuestaRNUT.FieldByName('Complemento').Value := FieldByName('Complemento').Value;
            cdsObtenerConvenioRespuestaRNUT.Post;
            Next;
        end;
    end;
    cdsObtenerConvenioRespuestaRNUT.First;
    cdsObtenerConvenioRespuestaRNUT.EnableControls;
    dbl_Convenio.OnClick(dbl_Convenio);
    dbl_Convenio.SetFocus;
end;

procedure TFormRespuestaRNUT.txt_TOPChange(Sender: TObject);
begin
    if TNumericEdit(Sender).ValueInt < 0 then TNumericEdit(Sender).ValueInt := 1;
end;

function TFormRespuestaRNUT.DarTop: Variant;
begin
    if txt_TOP.ValueInt < 0 then result := 1
    else Result := txt_TOP.ValueInt;
end;

procedure TFormRespuestaRNUT.txt_TOPExit(Sender: TObject);
begin
    ObtenerConvenio(DarCodigoEstado, DarCodigoRespuesta, DarRut, DarTOP);
end;

procedure TFormRespuestaRNUT.cdsObtenerConvenioRespuestaRNUTAfterScroll(
  DataSet: TDataSet);
begin
    if (FormRespuestaRNUT = nil) or (not FormRespuestaRNUT.Visible) then Exit;

    if StrRight(cb_Estados.Text, 1) = 'E' then begin
        dbl_Convenio.Hint := Trim(cdsObtenerConvenioRespuestaRNUT.FieldByName('Complemento').AsString);
        dbl_Convenio.ShowHint := True;
    end
    else begin
        dbl_Convenio.Hint := '';
        dbl_Convenio.ShowHint := False;
    end;
(*
    P:= dbl_Convenio.Columns[4].CurrentRect.BottomRight;
    P.Y := Mouse.CursorPos.Y;
    if StrRight(cb_Estados.Text, 1) = 'E' then begin
        MsgBoxBalloon(cdsObtenerConvenioRespuestaRNUT.FieldByName('Complemento').AsString, 'Motivo Rechazo', MB_ICONINFORMATION, P);
    end;
*)
end;

procedure TFormRespuestaRNUT.dbl_ConvenioDblClick(Sender: TObject);
begin
    if btn_Marcar.Enabled then btn_Marcar.Click;
end;


procedure TFormRespuestaRNUT.btnMarcarTodosClick(Sender: TObject);
var
    Book: TBookmark;
begin
    cdsObtenerConvenioRespuestaRNUT.DisableControls;
    Book := cdsObtenerConvenioRespuestaRNUT.GetBookmark;
    cdsObtenerConvenioRespuestaRNUT.First;
    while not cdsObtenerConvenioRespuestaRNUT.eof do begin
        cdsObtenerConvenioRespuestaRNUT.edit;
        cdsObtenerConvenioRespuestaRNUT.FieldByName('Marcado').Value := True;
        cdsObtenerConvenioRespuestaRNUT.Post;
        cdsObtenerConvenioRespuestaRNUT.Next;
    end;
    CantMarcada := cdsObtenerConvenioRespuestaRNUT.RecordCount;
    btn_Reenviar.Enabled := CantMarcada > 0;
    cdsObtenerConvenioRespuestaRNUT.GotoBookmark(book);
    cdsObtenerConvenioRespuestaRNUT.FreeBookmark(book);
    cdsObtenerConvenioRespuestaRNUT.EnableControls;
    dbl_Convenio.SetFocus;
end;


//******************************************************************************

function TFormRespuestaRNUT.DarCodigoEstadoLista: String;
begin
    Result := StrRight(cb_Estados_Lista.Text, 1);
end;

function TFormRespuestaRNUT.DarCodigoRespuestaLista: Variant;
begin
    if not cb_Codigo_Error_Lista.Visible or (Trim(StrRight(cb_Codigo_Error_Lista.Text, 2)) = '0') or (Trim(StrRight(cb_Codigo_Error_Lista.Text, 2)) = '') then Result := Null
    else Result := Trim(StrRight(cb_Codigo_Error_Lista.Text, 2));
end;

function TFormRespuestaRNUT.DarRutLista: Variant;
begin
    if Trim(txt_NumTelevia.Text) = '' then Result := Null
    else Result := Trim(txt_NumTelevia.Text);
end;

function TFormRespuestaRNUT.DarTopLista: Variant;
begin
    if txt_TOP_Lista.ValueInt < 0 then result := 1
    else Result := txt_TOP_Lista.ValueInt;
end;


procedure TFormRespuestaRNUT.ObtenerLista(CodigoEstado, CodigoRespuesta,
  NumeroTag, Top: Variant);
begin
    Screen.Cursor := crHourGlass;
    try
        ObtenerListasRespuestaRNUT.Close;
        ObtenerListaDetallleRespuestaRNUT.close;

        with ObtenerListasRespuestaRNUT do begin
            Close;
            Parameters.ParamByName('@Estado').Value             := CodigoEstado;
            Parameters.ParamByName('@CodigoRespuesta').Value    := CodigoRespuesta;
            Parameters.ParamByName('@NumeroTag').Value          := NumeroTag;
            Parameters.ParamByName('@Top').Value                := TOP;
            Open;
        end;
        //CargarGrilla;
    finally
        Screen.Cursor := crDefault;
    end; // finally
end;


procedure TFormRespuestaRNUT.cb_Estados_ListaChange(Sender: TObject);
begin
    ObtenerListaDetallleRespuestaRNUT.Close;
    ObtenerListaEnviosRNUT.Close;

    if StrRight(cb_Estados.Text, 1) = 'E' then begin
            CargarComboRespuestaEnvioRNUT(DMConnections.BaseCAC, cb_Codigo_Error_Lista);
    end;

    if cb_Codigo_Error_Lista.Visible then cb_Codigo_Error_Lista.OnChange(cb_Codigo_Error_Lista)
    else ObtenerLista(DarCodigoEstadoLista, Null, DarRutLista, DarTOPLista);
end;

procedure TFormRespuestaRNUT.cb_Codigo_Error_ListaChange(Sender: TObject);
begin
    ObtenerLista(DarCodigoEstadoLista, DarCodigoRespuestaLista, DarRutLista, DarTopLista);
end;

procedure TFormRespuestaRNUT.txt_NumTeleviaChange(Sender: TObject);
begin
    if (Length(txt_NumTelevia.Text) >= 11) or (Length(txt_NumTelevia.Text) = 0) then
        ObtenerLista(DarCodigoEstadoLista, DarCodigoRespuestaLista, DarRutLista, DarTopLista);
end;

procedure TFormRespuestaRNUT.txt_TOP_ListaExit(Sender: TObject);
begin
    ObtenerLista(DarCodigoEstadoLista, DarCodigoRespuestaLista, DarRutLista, DarTOPLista);
end;

procedure TFormRespuestaRNUT.ObtenerListasRespuestaRNUTAfterOpen(
  DataSet: TDataSet);
begin
    cb_Codigo_Error_Lista.Visible := (DarCodigoEstadoLista = 'E');
    lbl_TipoError_Lista.Visible := cb_Codigo_Error_Lista.Visible;

    if ObtenerListasRespuestaRNUT.RecordCount = 0 then begin
        lbl_CantConvAfectados_Lista.Caption := '0';
        lbl_CantMostrar_Lista.Caption := '0';
    end else begin
        lbl_CantConvAfectados_Lista.Caption := ObtenerListasRespuestaRNUT.FieldByName('CantidadRegAfectados').AsString;
        lbl_CantMostrar_Lista.Caption := IntToStr(ObtenerListasRespuestaRNUT.RecordCount);
    end;
    dbl_Convenio_Lista.Columns.Items[0].Sorting := csAscending;
end;

procedure TFormRespuestaRNUT.dbl_Convenio_ListaClick(Sender: TObject);
begin
    Screen.Cursor := crHourGlass;
    try
        ObtenerListaDetallleRespuestaRNUT.Close;
        ObtenerListaEnviosRNUT.Close;
        ObtenerListaDetallleRespuestaRNUT.Parameters.ParamByName('@NumeroEtiqueta').Value := ObtenerListasRespuestaRNUT.FieldByName('Televia').Value;
        ObtenerListaDetallleRespuestaRNUT.Parameters.ParamByName('@CodigoColor').Value := ObtenerListasRespuestaRNUT.FieldByName('CodigoColor').Value;
        ObtenerListaDetallleRespuestaRNUT.Open;
    finally
        Screen.Cursor := crDefault;
    end; // finally
end;

procedure TFormRespuestaRNUT.dbl_Convenio_ListaColumns0HeaderClick(
  Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;

    ObtenerListasRespuestaRNUT.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
    //CargarGrilla;
end;

procedure TFormRespuestaRNUT.ObtenerListaDetallleRespuestaRNUTAfterScroll(
  DataSet: TDataSet);
begin
    Screen.Cursor := crHourGlass;
    try
        with ObtenerListaEnviosRNUT do begin
            Close;
            Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := ObtenerListaDetallleRespuestaRNUT.FieldByName('CodigoEnvioConvenioRNUT').Value;
            Open;
        end;
    finally
        Screen.Cursor := crDefault;
    end; // finally

end;

procedure TFormRespuestaRNUT.txt_NumTeleviaKeyPress(Sender: TObject;
  var Key: Char);
begin
    if Key in ['0'..'9',#8] then Key := Key
    else Key := #0;
end;

end.
