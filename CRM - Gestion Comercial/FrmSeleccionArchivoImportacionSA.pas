{********************************** File Header ********************************
File Name   : FrmSeleccionArchivoImportacionSA.pas
Author      : cchiappero
Date Created: 30/06/2004
Language    : ES-AR
Description :
*******************************************************************************}

unit FrmSeleccionArchivoImportacionSA;

interface

uses
  Windows, Messages, Variants, Classes, Graphics, Controls, Forms, PeaProcs,
  Dialogs, StdCtrls, PeaTypes, Grids, DBGrids, IdGlobal,
  DPSGrid, ExtCtrls, DPSControls, DB, ADODB, SysUtils, DateUtils,
  ListBoxEx, DBListEx, DBClient, ComCtrls, Util;

type

  TFormSeleccionArchivoImportacionSA = class(TForm)
    pnlImportar: TPanel;
    dsArchivos: TDataSource;
    spActualizarArchivoImportarSantander: TADOStoredProc;
    spObtenerArchivosImportar: TADOStoredProc;
    spActualizarEstadoArchivoImportar: TADOStoredProc;
    dblArchivos: TDBListEx;
    Panel1: TPanel;
    cdVehiculos: TClientDataSet;
    cdVehiculosCodigoSolicitud: TIntegerField;
    cdVehiculosPatente: TStringField;
    cdVehiculosDigitoVerificador: TStringField;
    cdVehiculosCodigoMarca: TSmallintField;
    cdVehiculosModelo: TStringField;
    cdVehiculosAnioVehiculo: TSmallintField;
    cdVehiculosCodigoTipoVehiculo: TSmallintField;
    cdPersonas: TClientDataSet;
    cdPersonasCodigoSolicitudContacto: TIntegerField;
    cdPersonasRUT: TStringField;
    cdPersonasDVRUT: TStringField;
    cdPersonasPersoneria: TStringField;
    cdPersonasApellido: TStringField;
    cdPersonasApellidoMaterno: TStringField;
    cdPersonasNombre: TStringField;
    cdPersonasSexo: TStringField;
    cdPersonasFechaNacimiento: TDateTimeField;
    cdPersonasApellidoContactoComercial: TStringField;
    cdPersonasApellidoMaternoContactoCom: TStringField;
    cdPersonasNombreContactoComercial: TStringField;
    cdPersonasSexoContactoComercial: TStringField;
    cdPersonasCodigoRegion: TStringField;
    cdPersonasCodigoComuna: TStringField;
    cdPersonasCalle: TStringField;
    cdPersonasDetalleDomicilio: TStringField;
    cdPersonasTipoTelefonoPrincipal: TWordField;
    cdPersonasCodigoAreaTelefonoPrincipal: TSmallintField;
    cdPersonasNumeroTelefonoPrincipal: TStringField;
    cdPersonasHoraDesde: TDateTimeField;
    cdPersonasHoraHasta: TDateTimeField;
    cdPersonasTipoTelefonoAlternativo: TWordField;
    cdPersonasCodigoAreaTelefonoAlternativo: TSmallintField;
    cdPersonasNumeroTelefonoAlternativo: TStringField;
    cdPersonasEmail: TStringField;
    cdPersonasTipoMedioPagoAutomatico: TWordField;
    cdPersonasCodigoEmisorTarjetaCredito: TWordField;
    cdPersonasCodigoTipoTarjetaCredito: TWordField;
    cdPersonasNumeroTarjetaCredito: TStringField;
    cdPersonasFechaVenciemiento: TStringField;
    cdPersonasCodigoTipoCuentaBancaria: TWordField;
    cdPersonasCodigoBanco: TIntegerField;
    cdPersonasNroCuentaBancaria: TStringField;
    cdPersonasSucursal: TStringField;
    cdPersonasNumeroDocumentoTitular: TStringField;
    cdPersonasNombreTitular: TStringField;
    cdPersonasTelefonoTitular: TStringField;
    cdPersonasCodigoAreaTelefonoTitular: TSmallintField;
    qryValidarRegion: TADOQuery;
    qryValidarMarca: TADOQuery;
    qryValidarModelo: TADOQuery;
    qryValidarComuna: TADOQuery;
    qryValidarTipoVehiculo: TADOQuery;
    qryValidarTipoTelefono: TADOQuery;
    spCrearSolicitudImportacion: TADOStoredProc;
    spActualizarVehiculosContacto: TADOStoredProc;
    qryValidarAreaTelefono: TADOQuery;
    dsPersonas: TDataSource;
    cdPersonasNumero: TStringField;
    progress: TProgressBar;
    btnImportar: TButton;
    btnSalir: TButton;
    procedure btnImportarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spObtenerArchivosImportarAfterOpen(
      DataSet: TDataSet);
    procedure dblArchivos1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dblArchivosClick(Sender: TObject);
    procedure dblArchivosColumns4HeaderClick(Sender: TObject);
    procedure dblArchivosDblClick(Sender: TObject);
    procedure ActualizarPersonasVehiculos;

  private
    FErrores : String;
    FConteoErrores : integer;
    FAdvertencias : String;
    FArchivoImportacion : TStringList;
    FNombreArchivo : string;
    FArchivoError : TextFile;
    FNroLinea : integer;
    FCodigoSolicitud: integer;
    FEvento: string;
    FDirectorio: string;
    FOrdenActual: string;
    FFuenteOrigen : integer;
    procedure FinalizarImportacion;
    procedure LeerArchivos;
    procedure ActualizarEstadoArchivo(Archivo: string; Estado: integer; CodigoResultado: integer = 0; Observaciones: string = ''; ContadorPersonasAgregadas: integer = 0; ContadorVehiculosAgregados: integer = 0; CodigoSolicitud: integer = 0);
    procedure ObtenerArchivosImportacion(Orden: string);
    procedure ParsearPersona(Buffer: string);
    function PosValidarPersonas : boolean;
    procedure ParsearVehiculo(Buffer: string);
    function PosValidarVehiculos : boolean;
    procedure ValidacionFinalVehiculosContraContratos;
    procedure ValidarTipoTelefono(Tipo, Error : string);
    procedure ValidarCodigoAreaTelefono(CodigoArea,Telefono,Error : string);
    procedure Importar;
    procedure SetearErrorGlobalLinea(Error : string);
    procedure EscribirErrorGlobalLinea;
    procedure SetearErrorGlobal(Error, Error2 : string);
    procedure EscribirErrorGlobal;
    procedure ValidarRequeridos(cd: TClientDataSet);
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(CaptionAMostrar: string): boolean;
  end;

resourcestring
    IMPORTACION_EXISTOSA = 'Importaci�n Exitosa';
    IMPORTACION_ERRONEA  = 'Importaci�n con Errores';
    CAPTION_IMPORTACIONSOLICITUDES = 'Importacion Solicitudes';
    ERROR_FORMATOCAMPO = 'Error de Formato en el Campo %s con valor %s.';
    MSG_LINEANRO = 'L�nea Nro.: %d';
    MSG_ADVERTENCIAS = 'Advertencias: %s';
    ERROR_SOLICITUDSINVEHICULOS = 'La solicitud N� %d no tiene veh�culos asignados.';
    ERROR_IMPORTACION = 'Se produjeron %d errores en la importaci�n. Revise el archivo de errores %s y vuelva a intentarlo.';
    ERROR_CREARSOLICITUD = 'La solicitud contiene errores: ';

const
    ENTRYPERSONCOUNT = 40;
    ENTRYVEHICLECOUNT = 7;

var
  FormSeleccionArchivoImportacionSA: TFormSeleccionArchivoImportacionSA;

function GetEntryCount(const S: String; Separator: Char): integer;

implementation

uses DMConnection, RStrings, UtilProc, UtilDB, ConstParametrosGenerales, Abm_obj;

{$R *.dfm}

function GetEntryCount(const S: String; Separator: Char): integer;
var index : integer;
begin
   index := 0; result := 0;
   While (Index <= length(S)) do
   begin
      if (S[Index] = Separator) then result := result + 1;
      Index := Index + 1;
   end;
end;

function TFormSeleccionArchivoImportacionSA.Inicializar(CaptionAMostrar: string): boolean;
resourcestring
      MSG_CAPTION_IMPORTAR      = 'Importar Personas/Veh�culos de Santander.';
      MSG_ERROR_DIRECTORIO      = 'No se pudo acceder al directorio donde se encuentran los archivos de importaci�n. Verifique el directorio %s.';
      MSG_ERROR_PARAMETRO       = 'No se pudo encontrar el par�metro de directorio.';
Var
	S: TSize;
begin
    Result := false;
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);
    self.Caption := CaptionAMostrar;

    if ObtenerParametroGeneral(DMConnections.BaseCAC, Dir_Importacion_Santander, FDirectorio) then begin
      if not DirectoryExists(FDirectorio) then begin
          MsgBoxErr(Format(MSG_ERROR_DIRECTORIO, [FDirectorio]), MSG_ERROR_DIRECTORIO_ACCESO, MSG_CAPTION_IMPORTAR, MB_ICONSTOP);
          Exit;
      end;
    end else begin
        MsgBoxErr(MSG_ERROR_PARAMETRO, MSG_ERROR_DIRECTORIO_ACCESO, MSG_CAPTION_IMPORTAR, MB_ICONSTOP);
        exit;
    end;

    LeerArchivos;
    dblArchivosClick(nil);
    Result := true;
end;

procedure TFormSeleccionArchivoImportacionSA.FinalizarImportacion;
resourcestring
 MSG_CAPTION_IMPORTAR_CITAS = 'Importar Solicitudes.';
var
  error : string;
begin
    ObtenerArchivosImportacion(FOrdenActual);
    try
   	    CrearRegistroOperaciones(
        	    DMCOnnections.BaseCAC,
                SYS_CAC,
                RO_MOD_IMPORTACION_SOLICITUDES,
    	  	    RO_IMPORTAR_SOLICITUDES,
                SEV_INFO,
                GetMachineName,
                UsuarioSistema,
                FEvento,
    	  	    0,FCodigoSolicitud,0, FNombreArchivo ,
    	  	    PChar(error));
    except
        On E: Exception do begin
            MsgBoxErr(MSG_EXCEL_REGISTRO_OPERACIONES, e.message, CAPTION_IMPORTACIONSOLICITUDES, MB_ICONSTOP);
            Screen.Cursor := crDefault;
            Exit;
        end;
    end;
end;

procedure TFormSeleccionArchivoImportacionSA.LeerArchivos;
resourcestring
    MSG_ERROR_GENERAR_TABLA_ARCHIVOS = 'No se pudo generar la tabla de archivos.';
var
  I: Integer;
  MaskPtr: PChar;
  Ptr: PChar;
  AttrWord: Word;
  FileInfo: TSearchRec;
  SaveCursor: TCursor;

begin
    DMConnections.BaseCAC.BeginTrans;
    try
        AttrWord := DDL_READWRITE;

        I := 0;
        SaveCursor := Screen.Cursor;
        try
          MaskPtr := PChar(FDirectorio + '\*.dat');
          while MaskPtr <> nil do
          begin
            Ptr := StrScan (MaskPtr, ';');
            if Ptr <> nil then Ptr^ := #0;
            if FindFirst(MaskPtr, AttrWord, FileInfo) = 0 then
            begin
              repeat
                spActualizarArchivoImportarSantander.Close;
                spActualizarArchivoImportarSantander.Parameters.ParamByName('@Archivo').Value := FileInfo.Name;
                spActualizarArchivoImportarSantander.Parameters.ParamByName('@Estado').Value  := ESTADO_PENDIENTE;
                spActualizarArchivoImportarSantander.ExecProc;
                if I = 100 then Screen.Cursor := crHourGlass;
              until FindNext(FileInfo) <> 0;
              FindClose(FileInfo);
            end;
            if Ptr <> nil then begin
              Ptr^ := ';';
              Inc (Ptr);
            end;
            MaskPtr := Ptr;
          end;
        finally
          Screen.Cursor := SaveCursor;
        end;
        QueryExecute(DMConnections.BaseCAC,'EXEC ActualizarArchivosImportacionBorradosSA');
        FOrdenActual := '1 ASC';
        ObtenerArchivosImportacion(FOrdenActual);


        DMConnections.BaseCAC.CommitTrans;
    except
        on E: exception do begin
            DMConnections.BaseCAC.RollbackTrans;
            MsgBoxErr(MSG_ERROR_GENERAR_TABLA_ARCHIVOS, e.message, CAPTION_IMPORTACIONSOLICITUDES, MB_ICONSTOP);
        end;
    end;
end;



procedure TFormSeleccionArchivoImportacionSA.ActualizarEstadoArchivo(Archivo: string; Estado: integer;
    CodigoResultado: integer = 0; Observaciones: string = '';ContadorPersonasAgregadas: integer = 0;
    ContadorVehiculosAgregados: integer = 0; CodigoSolicitud: integer = 0);
begin
    spActualizarEstadoArchivoImportar.Close;
    spActualizarEstadoArchivoImportar.Parameters.ParamByName('@Archivo').Value := Archivo;
    spActualizarEstadoArchivoImportar.Parameters.ParamByName('@Observaciones').Value  := Observaciones;
    spActualizarEstadoArchivoImportar.Parameters.ParamByName('@CodigoResultado').Value  := CodigoResultado;
    spActualizarEstadoArchivoImportar.Parameters.ParamByName('@Estado').Value  := Estado;
    spActualizarEstadoArchivoImportar.Parameters.ParamByName('@PersonasAgregadas').Value  := ContadorPersonasAgregadas;
    spActualizarEstadoArchivoImportar.Parameters.ParamByName('@VehiculosAgregados').Value  := ContadorVehiculosAgregados;
    spActualizarEstadoArchivoImportar.ExecProc;
end;


procedure TFormSeleccionArchivoImportacionSA.ObtenerArchivosImportacion(Orden: string);
begin
    spObtenerArchivosImportar.Close;
    spObtenerArchivosImportar.Parameters.ParamByName('@Orden').Value := Orden;
    spObtenerArchivosImportar.Open;
end;



//******************************************************************/
procedure TFormSeleccionArchivoImportacionSA.Importar;
resourcestring
 ERROR_ELARCHIVOCONTIENEERRORES = 'El archivo %s contiene %d errores. Verifique el archivo %s.';
 ERROR_TIPOREGISTROINCOMPATIBLE = 'El archivo contiene tipos de registros distintos de V y P.';
 ERROR_ACTUALIZACIONBASEDATOS   = 'Errores Actualizaci�n en base de datos.';
 ERROR_APERTURASARCHIVO         = 'Error al intentar abrir los archivos.';
 STR_ERRORS_FILE                = 'Consultar. Archivo de Errores: ';
var
 NombreArchivoError : string;
 buffer : string;
 i : integer;
begin
   //Obtiene el nombre del archivo de par�metros generales.
   FNombreArchivo := spObtenerArchivosImportar.FieldByName('Archivo').AsString;
   FArchivoImportacion := TStringList.create;
   FArchivoImportacion.Text := FileToString(FDirectorio + '\' + FNombreArchivo);


   if pos('SANTANDER', AnsiUpperCase(FNombreArchivo)) > 0 then
    FFuenteOrigen := FUENTE_CUPONSANTANDER
   else FFuenteOrigen := FUENTE_CUPONPRESTO;




   //Genera el archivo para contener los errores.
   NombreArchivoError := FDirectorio + '\' + copy(ExtractFileName(FNombreArchivo), 1, pos('.',ExtractFileName(FNombreArchivo)) - 1) + '.err';
   AssignFile(FArchivoError, NombreArchivoError);

   try
       try
          DMConnections.BaseCAC.BeginTrans;
          FEvento := IMPORTACION_ERRONEA;


          cdPersonas.Open;
          cdPersonas.EmptyDataSet;

          cdVehiculos.Open;
          cdVehiculos.EmptyDataSet;

          Rewrite(FArchivoError);

          FNroLinea := 1;
          FConteoErrores := 0;
          FAdvertencias := '';

          progress.Max := (FArchivoImportacion.Count - 1) * 2;

          //No quiero que se detenga en el primer error que encuentre. Solo en los cr�ticos.
          for i := 0 to FArchivoImportacion.Count - 1 do begin
              Buffer := FArchivoImportacion.Strings[i];

              progress.Position := i;
              Self.Refresh;
              Application.ProcessMessages;

              //Si el tipo de registro es P lo parseo como persona, si es V lo parseo como veh{iculo.
              if (UpperCase(ParseParamByNumber(Buffer,1,';')) = 'P') then parsearPersona(Buffer)
              else if (UpperCase(ParseParamByNumber(Buffer,1,';')) = 'V') then parsearVehiculo(Buffer)
              else SetearErrorGlobalLinea(ERROR_TIPOREGISTROINCOMPATIBLE);
              FNroLinea := FNroLinea + 1;
          end;

          //Verifico que
          ValidacionFinalVehiculosContraContratos;

          if FConteoErrores > 0 then raise exception.CreateFmt(ERROR_ELARCHIVOCONTIENEERRORES,[FNombreArchivo, FConteoErrores, NombreArchivoError]);

          ActualizarPersonasVehiculos;

          DMConnections.BaseCAC.CommitTrans;
       except

            On E: EInOutError do MsgBoxErr(ERROR_APERTURASARCHIVO,E.Message,CAPTION_IMPORTACIONSOLICITUDES,MB_ICONSTOP);
            On E: Exception do begin
                progress.Position := progress.Max;
                DMConnections.BaseCAC.RollbackTrans;
                SetearErrorGlobal(ERROR_CREARSOLICITUD,E.message);
                EscribirErrorGlobal;

            end;
       end;
   finally
       if FAdvertencias <> '' then   Writeln(FArchivoError,Format(MSG_ADVERTENCIAS, [FAdvertencias]));

       if FConteoErrores > 0 then begin
            FEvento := IMPORTACION_ERRONEA;
            ActualizarEstadoArchivo(FNombreArchivo, ESTADO_CON_ERRORES, ARCHIVO_CONTIENERRORES,STR_ERRORS_FILE + NombreArchivoError);
            MsgBox(Format(ERROR_IMPORTACION,[FConteoErrores,NombreArchivoError]), CAPTION_IMPORTACIONSOLICITUDES ,MB_ICONSTOP);
       end else begin
            FEvento := IMPORTACION_EXISTOSA;
            ActualizarEstadoArchivo(FNombreArchivo, ESTADO_IMPORTADO, SOLICITUD_VEHICULOS,'', cdPersonas.RecordCount, cdVehiculos.RecordCount);
       end;
       if IsOpen(NombreArchivoError) then CloseFile(FArchivoError);
       FArchivoImportacion.Clear;

       cdVehiculos.EmptyDataSet;
       cdPersonas.EmptyDataSet;
       progress.Position := 0;
   end;
end;


procedure TFormSeleccionArchivoImportacionSA.ParsearPersona(Buffer: string);
resourcestring
    ERROR_CANTIDADCAMPOSINSUFICIENTESPERSONAS = 'Insuficiente cantidad de campos en el registro de personas.';
	CONST_ERR_FORMATO_FECHA = 'Formato de Fecha/Hora incorrecta. El formato debe ser ddmmyyyy en el caso de fecha y hhmm en el caso de horas. Valor Actual:';
var
 i : integer;
 campo : string;
begin
  try

	  //Verifica que la linea contenga la cantidad de entradas correctas;
	  if GetEntryCount(Buffer,';') <> ENTRYPERSONCOUNT then raise exception.Create(ERROR_CANTIDADCAMPOSINSUFICIENTESPERSONAS + '. L�nea: ' + InttoStr(FNroLinea));
	  cdPersonas.Append;
	  // Carga de Campo y Validacion de formatos
	  for i := 0 to ENTRYPERSONCOUNT - 1 do begin
			campo := Trim(ParseParamByNumber(Buffer,i + 2,';'));
			try
				if campo <> '' then begin
				  if cdPersonas.Fields[i].DataType = ftDateTime then begin
						if Length(campo) = 8 then cdPersonas.Fields[i].Value := EncodeDate(StrToInt(copy(campo,5,4)), StrToInt(copy(campo,3,2)), StrToInt(copy(campo,1,2)))
						else if Length(campo) <= 4 then begin
							campo := padL(campo,8,'0');
							cdPersonas.Fields[i].Value := EncodeTime(StrToInt(copy(campo,1,2)), StrToInt(copy(campo,3,2)),0,0)
						end
						else raise Exception.Create(CONST_ERR_FORMATO_FECHA + campo);
                  end
                  else cdPersonas.Fields[i].Value := Trim(campo);
                end;
            except
                 // El valor no corresponde con el campo.
                 SetearErrorGlobalLinea(Format(ERROR_FORMATOCAMPO,[cdPersonas.Fields[i].DisplayName, Campo]));
            end;
      end;
      // Verifico rangos de valores para cada campo del registro.
      if PosValidarPersonas then
        cdPersonas.Post else
      cdPersonas.Cancel;
    finally
        //Si existieron errores en la l�nea los escribo en el archivo.
        EscribirErrorGlobalLinea;
    end;
end;

procedure TFormSeleccionArchivoImportacionSA.ParsearVehiculo(Buffer: string);
resourcestring
 ERROR_CANTIDADCAMPOSINSUFICIENTESVEHICULO = 'Insuficiente cantidad de campos en el registro vehiculos.';
var
 i : integer;
 campo : string;
begin
    try
          //Verifica que la linea contenga la cantidad de entradas correctas;

          if GetEntryCount(Buffer,';') <> ENTRYVEHICLECOUNT then  raise exception.Create(ERROR_CANTIDADCAMPOSINSUFICIENTESVEHICULO + '. L�nea: ' + InttoStr(FNroLinea));
          cdVehiculos.Append;
          // Carga de Campo y Validacion de formatos
          for i := 0 to ENTRYVEHICLECOUNT - 1 do begin
            try
                 campo := Trim(ParseParamByNumber(buffer,i + 2,';'));
                 if campo <> '' then  cdVehiculos.Fields[i].AsString := campo;
            except
                 // El valor no corresponde con el campo.
                 SetearErrorGlobalLinea(Format(ERROR_FORMATOCAMPO,[cdVehiculos.Fields[i].DisplayName, campo]));
            end;
          end;
          // Verifico rangos de valores para cada campo del registro.
          if PosValidarVehiculos then
            cdVehiculos.Post
          else
            cdVehiculos.Cancel;
    finally
        //Si existieron errores en la l�nea los escribo en el archivo.
        EscribirErrorGlobalLinea;
    end;
end;

function TFormSeleccionArchivoImportacionSA.PosValidarPersonas: boolean;
resourcestring
  ERROR_PERSONERIA                      = 'La Personer�a debe ser J o P.';
  ERROR_DIGITOVERIFICADOR               = 'D�gito Verificador de RUT debe ser  0..9 o k.';
  ERROR_NOMBREPERSONAJURIDICA           = 'El campo nombre debe estar vacio para una Persona Jur�dica.';
  ERROR_SEXOPERSONAJURIDICA             = 'El campo sexo debe estar vacio para una Persona Jur�dica.';
  ERROR_FALTANCAMPOSCONTACTOCOMERCIAL   = 'Faltan Campos requeridos de Contacto Comercial por ser Persona Jur�dica.';
  ERROR_EMAILOBLIGATORIOPERSONAJURIDICA = 'La direccion de correo electr�nica es obligatoria para una Persona Jur�dica.';
  WRN_DATOSCOMERCIALESDESCARTADOS       = 'Datos Comercial descartados por ser persona f�sica';
  ERROR_SEXO                            = 'Sexo debe ser M o F';
  ERROR_TIPOMEDIOPAGOAUTOMATICO         = 'Valor Erroneo para Tipo Medio Pago Autom�tico. Debe ser: 0 - Ninguno, 1 - PAT, 2 - PAC.';
  ERROR_CAMPOS0CARGADOS                 = 'Datos de Tipo de Medio de Pago Autom�tico cargados para tipo 0 - Ninguno.';
  ERROR_TITULARCUENTAREQUERIDO          = 'Alg�n Valor de Titular de Cuenta requerido no se encuentra cargado.';
  ERROR_PATREQUERIDO                    = 'Alg�n Valor de PAT requerido no se encuentra cargado para Tipo de Medio de Pago Autom�tico tipo 1.';
  ERROR_PACREQUERIDO                    = 'Alg�n Valor de PAC requerido no se encuentra cargado para Tipo de Medio de Pago Autom�tico tipo 2.';
  ERROR_COMUNA                          = 'Comuna no existe en el Maestro de Comunas.';
  ERROR_TIPOTELEFONO                    = 'C�digo de tipo tel�fono principal incorrecto.';
  ERROR_TIPOTELEFONOALTERNATIVO         = 'C�digo de tipo tel�fono alternativo incorrecto.';
  ERROR_REGION                          = 'Regi�n no existe en el Maestro de Regiones.';
  ERROR_CODIGOAREATELEFONOPRINCIPAL     = 'Error en el Tel�fono Principal.';
  ERROR_CODIGOAREATELEFONOALTERNATIVO   = 'Error en el Tel�fono Alternativo.';
  ERROR_CODIGOAREATELEFONOTITULAR       = 'Error en el Tel�fono del Titular.';
begin
 try
    // Valido que los campos marcados como requeridos esten completos.
    ValidarRequeridos(cdPersonas);


    //Validaci�n de d�gito verificador del RUT
    if (cdPersonas.FieldByName('DVRUT').AsString <> '') then
      if not (cdPersonas.FieldByName('DVRUT').AsString[1]  in ['0'..'9','k','K']) then SetearErrorGlobalLinea(ERROR_DIGITOVERIFICADOR);

    //Validaci�n de la Personer�a jur�dica
    if (cdPersonas.FieldByName('Personeria').AsString <> 'J') and
       (cdPersonas.FieldByName('Personeria').AsString <> 'F') then  SetearErrorGlobalLinea(ERROR_PERSONERIA);

    //Si la Personer�a es Jur�dica entonces:
    if (cdPersonas.FieldByName('Personeria').AsString = 'J') then begin
       //El valor de nombre no corresponde.
       if (cdPersonas.FieldByName('Nombre').AsString <> '') then SetearErrorGlobalLinea(ERROR_NOMBREPERSONAJURIDICA);
       //El valor de sexo no  corresponde.
       if (cdPersonas.FieldByName('Sexo').AsString <> '') then SetearErrorGlobalLinea(ERROR_SEXOPERSONAJURIDICA);
       //El Nombre, Apellido y Sexo de Contacto debe contener un valor.
       if (cdPersonas.FieldByName('ApellidoContactoComercial').AsString = '') or
          (cdPersonas.FieldByName('NombreContactoComercial').AsString = '') or
          (cdPersonas.FieldByName('SexoContactoComercial').AsString = '') then SetearErrorGlobalLinea(ERROR_FALTANCAMPOSCONTACTOCOMERCIAL);
       //El Email debe contener un valor.
       if (cdPersonas.FieldByName('Email').AsString <> '') then SetearErrorGlobalLinea(ERROR_EMAILOBLIGATORIOPERSONAJURIDICA);
    end;

    //Si la Personer�a es F�sica entonces:
    if (cdPersonas.FieldByName('Personeria').AsString = 'F') then begin
         //El los datos de contacto comercial no corresponden.
         if (cdPersonas.FieldByName('ApellidoContactoComercial').AsString <> '') or
            (cdPersonas.FieldByName('NombreContactoComercial').AsString <> '') or
            (cdPersonas.FieldByName('ApellidoMaternoContactoCom').AsString <> '') or
            (cdPersonas.FieldByName('SexoContactoComercial').AsString <> '')
         then begin
              FAdvertencias := FAdvertencias + #9 + WRN_DATOSCOMERCIALESDESCARTADOS + #13#10;
              cdPersonas.FieldByName('ApellidoContactoComercial').Value := Null;
              cdPersonas.FieldByName('NombreContactoComercial').Value   := Null;
              cdPersonas.FieldByName('ApellidoMaternoContactoCom').Value := Null;
         end;
         //Valido el Sexo.
         if cdPersonas.FieldByName('Sexo').AsString <> '' then
           if not  (cdPersonas.FieldByName('Sexo').AsString[1] in ['M','F']) then SetearErrorGlobalLinea(ERROR_SEXO);
    end;

    //Valida valor tipo de Medio de Pago Autom�tico.
    if not (cdPersonas.FieldByName('TipoMedioPagoAutomatico').AsInteger in [0..2]) then  SetearErrorGlobalLinea(ERROR_TIPOMEDIOPAGOAUTOMATICO);

    //Si el tipo de pago autom�tico es manual.
    if cdPersonas.FieldByName('TipoMedioPagoAutomatico').AsInteger = 0 then begin
        //Los datos de medios de pago autom�tico no corresponde.
        if (cdPersonas.FieldByName('CodigoEmisorTarjetaCredito').AsString <> '') and
           (cdPersonas.FieldByName('CodigoTipoTarjetaCredito').AsString <> '') and
           (cdPersonas.FieldByName('NumeroTarjetaCredito').AsString <> '') and
           (cdPersonas.FieldByName('FechaVencimiento').AsString <> '')
                                    and
           (cdPersonas.FieldByName('CodigoTipoCuentaBancaria').AsString <> '') and
           (cdPersonas.FieldByName('CodigoBanco').AsString <> '') and
           (cdPersonas.FieldByName('NroCuentaBancaria').AsString <> '')  and
           (cdPersonas.FieldByName('Sucursal').AsString <> '')
                                    and
           (cdPersonas.FieldByName('NumeroDocumentoTitular').AsString <> '') and
           (cdPersonas.FieldByName('NombreTitular').AsString <> '') and
           (cdPersonas.FieldByName('CodigoAreaTitular').AsString <> '') and
           (cdPersonas.FieldByName('TelefonoTitular').AsString <> '') then
              SetearErrorGlobalLinea(ERROR_CAMPOS0CARGADOS);
    end;


    //Si es tipo de medio de pago autom�tico entonces los datos d
    if (cdPersonas.FieldByName('TipoMedioPagoAutomatico').AsInteger = 1) or
       (cdPersonas.FieldByName('TipoMedioPagoAutomatico').AsInteger = 2) then
          //Los datos de titular deben contener valor.
          if (cdPersonas.FieldByName('NumeroDocumentoTitular').AsString = '') or
             (cdPersonas.FieldByName('NombreTitular').AsString = '') or
             (cdPersonas.FieldByName('CodigoAreaTelefonoTitular').AsString = '') or
             (cdPersonas.FieldByName('TelefonoTitular').AsString = '') then
              SetearErrorGlobalLinea(ERROR_TITULARCUENTAREQUERIDO);

    //Si es tipo de medio de pago autom�tico es PAT.
    if (cdPersonas.FieldByName('TipoMedioPagoAutomatico').asinteger = 1) then
        if (cdPersonas.FieldByName('CodigoEmisorTarjetaCredito').AsString = '') and
           (cdPersonas.FieldByName('CodigoTipoTarjetaCredito').AsString = '') and
           (cdPersonas.FieldByName('NumeroTarjetaCredito').AsString = '') and
           (cdPersonas.FieldByName('FechaVencimiento').AsString = '') then
              SetearErrorGlobalLinea(ERROR_PATREQUERIDO);

    //Si es tipo de medio de pago autom�tico es PAC
    if (cdPersonas.FieldByName('TipoMedioPagoAutomatico').AsInteger = 2) then
        if (cdPersonas.FieldByName('CodigoTipoCuentaBancaria').AsString = '') and
           (cdPersonas.FieldByName('CodigoBanco').AsString = '') and
           (cdPersonas.FieldByName('NroCuentaBancaria').AsString = '')  and
           (cdPersonas.FieldByName('Sucursal').AsString = '') then
              SetearErrorGlobalLinea(ERROR_PACREQUERIDO);


    //Valida la regi�n
    if cdPersonas.FieldByName('CodigoRegion').AsString <> '' then  begin
            qryValidarRegion.Parameters.ParamByName('CodigoRegion').Value := cdPersonas.FieldByName('CodigoRegion').Value;
            qryValidarRegion.Open;
            if qryValidarRegion.Fields[0].AsInteger = 0 then SetearErrorGlobalLinea(ERROR_REGION);

            //Valida la comuna.
            if cdPersonas.FieldByName('CodigoComuna').AsString <> '' then begin
                    qryValidarComuna.Parameters.ParamByName('CodigoRegion').Value := cdPersonas.FieldByName('CodigoRegion').Value;
                    qryValidarComuna.Parameters.ParamByName('CodigoComuna').Value := cdPersonas.FieldByName('CodigoComuna').Value;
                    qryValidarComuna.Open;
                    if qryValidarComuna.Fields[0].AsInteger = 0 then SetearErrorGlobalLinea(ERROR_COMUNA);
            end;
    end;

    //Valida el tipo de tel�fono
    ValidarTipoTelefono(cdPersonas.FieldByName('TipoTelefonoPrincipal').AsString, ERROR_TIPOTELEFONO);
    ValidarTipoTelefono(cdPersonas.FieldByName('TipoTelefonoAlternativo').AsString, ERROR_TIPOTELEFONOALTERNATIVO);

    //Valida los C�digos de Area.
    ValidarCodigoAreaTelefono(cdPersonas.FieldByName('CodigoAreaTelefonoPrincipal').AsString, cdPersonas.FieldByName('NumeroTelefonoPrincipal').AsString, ERROR_CODIGOAREATELEFONOPRINCIPAL);
    ValidarCodigoAreaTelefono(cdPersonas.FieldByName('CodigoAreaTelefonoAlternativo').AsString, cdPersonas.FieldByName('NumeroTelefonoAlternativo').AsString, ERROR_CODIGOAREATELEFONOALTERNATIVO);
    ValidarCodigoAreaTelefono(cdPersonas.FieldByName('CodigoAreaTelefonoTitular').AsString, cdPersonas.FieldByName('TelefonoTitular').AsString, ERROR_CODIGOAREATELEFONOTITULAR);

    if FErrores <> '' then result := false else result := True;
 finally
    qryValidarTipoTelefono.Close;
    qryValidarComuna.Close;
    qryValidarRegion.Close;
 end;
end;

function TFormSeleccionArchivoImportacionSA.PosValidarVehiculos: boolean;
resourcestring
  ERROR_CAMPOVEHICULOREQUERIDO = '%s de Veh�culos es Requerido.';
  ERROR_ANIOVEHICULO = 'El a�o del modelo del vehiculo debe estar entre 1900 y %d';
  ERROR_MARCA = 'Marca de Veh�culo no existe en el Maestro de Marcas.';
  ERROR_TIPOVEHICULO = 'Tipo de Veh�culo no existe en el Maestro de Tipos de Veh�culo.';
  ERROR_SOLICITUDINEXISTENTE = ' Veh�culo asignado a una Solicitud inexistente o el registro de esta Solicitud conten�a errores.';
  ERROR_PATENTEMALCONFORMADA = 'Patente mal conformada. No se puede calcular el D�gito Verificador.';
var
 Letras, numeros, digito : string;
begin
  try
        if not cdPersonas.Locate('CodigoSolicitudContacto', cdVehiculos.FieldByName('CodigoSolicitud').AsInteger,[]) then
            SetearErrorGlobalLinea(ERROR_SOLICITUDINEXISTENTE)
        else begin
            //Veo si puedo reconstruir la patente en caso de que la hayan enviado en un formato erroneo.
            Letras := copy(cdVehiculos.FieldByName('Patente').AsString,1,2);
            Numeros := copy(cdVehiculos.FieldByName('Patente').AsString,3,4);
            Digito := copy(cdVehiculos.FieldByName('Patente').AsString,7,1);

            //Cuando Viene no siempre est� bien calculado.
            cdVehiculos.FieldByName('DigitoVerificador').AsString := ObtenerDigitoVerificadorPatente(DMConnections.BaseCAC, Letras + Numeros);

            if cdVehiculos.FieldByName('DigitoVerificador').AsString = '' then begin
              SetearErrorGlobalLinea(Format(ERROR_PATENTEMALCONFORMADA , [cdVehiculos.FieldByName('Patente').AsString] ) );
              cdVehiculos.FieldByName('DigitoVerificador').AsString := '0';
            end;

            cdVehiculos.FieldByName('Patente').AsString := Letras + Numeros;

            ValidarRequeridos(cdVehiculos);


            if (YearOf(cdVehiculos.FieldByName('AnioVehiculo').AsInteger) < 1900) or
               (YearOf(cdVehiculos.FieldByName('AnioVehiculo').AsInteger) > YearOf(NowBase(DMConnections.BaseCAC))) then
              SetearErrorGlobalLinea(Format(ERROR_ANIOVEHICULO , [YearOf(NowBase(DMConnections.BaseCAC))] ) );

            if cdVehiculos.FieldByName('CodigoMarca').AsString <> '' then begin
                qryValidarMarca.Parameters.ParamByName('codigoMarca').Value := cdVehiculos.FieldByName('CodigoMarca').Value;
                qryValidarMarca.Open;
                if qryValidarMarca.Fields[0].AsInteger = 0 then SetearErrorGlobalLinea(ERROR_MARCA);
            end;

            if cdVehiculos.FieldByName('CodigoTipoVehiculo').AsString <> '' then begin
                qryValidarTipoVehiculo.Parameters.ParamByName('CodigoTipoVehiculo').Value := cdVehiculos.FieldByName('CodigoTipoVehiculo').Value;
                qryValidarTipoVehiculo.Open;
                if qryValidarTipoVehiculo.Fields[0].AsInteger = 0 then SetearErrorGlobalLinea(ERROR_TIPOVEHICULO);
            end;
        end;
        if FErrores <> '' then result := false else result := True;
  finally
        qryValidarMarca.Close;
        qryValidarModelo.Close;
        qryValidarTipoVehiculo.Close;
  end;
end;


procedure TFormSeleccionArchivoImportacionSA.ValidacionFinalVehiculosContraContratos;
begin
 try
      cdPersonas.First;
      while not cdPersonas.Eof do
      begin
          cdVehiculos.Filtered := false;
          cdVehiculos.Filter := 'CodigoSolicitud = ' + cdPersonas.FieldByName('CodigoSolicitudContacto').asstring;
          cdVehiculos.Filtered := true;
          if cdVehiculos.RecordCount = 0 then  SetearErrorGlobal('Error: ', format(ERROR_SOLICITUDSINVEHICULOS,[cdPersonas.FieldByName('CodigoSolicitudContacto').asInteger]));
          cdPersonas.next;
      end;
 finally
     EscribirErrorGlobal;
     cdVehiculos.Filtered := false;
     cdVehiculos.Filter := '';
 end;
end;


procedure TFormSeleccionArchivoImportacionSA.ActualizarPersonasVehiculos;
resourcestring
 ERROR_GUARDANDOVALORES = 'Se produjo un error al intentar grabar el registro.';

var
 horafecha : TDateTime;
 codigoSolicitudContacto: integer;
begin
    try
      cdPersonas.First;
//      spCrearSolicitudImportacion.Parameters.Refresh;

      horafecha := NowBase(DMConnections.BaseCAC);

      cdVehiculos.Close;
      cdVehiculos.MasterSource := dsPersonas;
      cdVehiculos.MasterFields := 'CodigoSolicitudContacto';
      cdVehiculos.Open;


      while not cdPersonas.Eof do
      begin
            if progress.position < progress.Max then  progress.Position := progress.Position + 1;

            //********* DATOS DE LA SOLICITUD **********//
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoSolicitudContacto').Value := -1;
            spCrearSolicitudImportacion.Parameters.ParamByName('@FechaHoraCreacion').Value := horafecha;
            spCrearSolicitudImportacion.Parameters.ParamByName('@Observaciones').Value := 'Importado desde ' + spObtenerArchivosImportar.FieldByName('Archivo').AsString;


            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoFuenteSolicitud').Value := FFuenteOrigen;

            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoFuenteOrigenContacto').Value := iif(FFuenteOrigen = FUENTE_CUPONSANTANDER, FUENTE_ORIGEN_CONTACTO_SANTANDER, FUENTE_ORIGEN_CONTACTO_NO_SANTANDER);
            spCrearSolicitudImportacion.Parameters.ParamByName('@TipoMedioPagoAutomatico').Value := cdPersonas.FieldByName('TipoMedioPagoAutomatico').Value;

            //********* DATOS DE LA PERSONA **********//
            spCrearSolicitudImportacion.Parameters.ParamByName('@Personeria').Value := cdPersonas.FieldByName('Personeria').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@Apellido').Value := cdPersonas.FieldByName('Apellido').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@ApellidoMaterno').Value := cdPersonas.FieldByName('ApellidoMaterno').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@Nombre').Value := cdPersonas.FieldByName('Nombre').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';
            spCrearSolicitudImportacion.Parameters.ParamByName('@NumeroDocumento').Value := cdPersonas.FieldByName('RUT').AsString + cdPersonas.FieldByName('DVRUT').Value;

            spCrearSolicitudImportacion.Parameters.ParamByName('@Sexo').Value := cdPersonas.FieldByName('Sexo').Value;
            //spCrearSolicitudImportacion.Parameters.ParamByName('LugarNacimiento').Value := NULL;
            spCrearSolicitudImportacion.Parameters.ParamByName('@FechaNacimiento').Value := cdPersonas.FieldByName('FechaNacimiento').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@ApellidoContactoComercial').Value := cdPersonas.FieldByName('ApellidoContactoComercial').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@ApellidoMaternoContactoComercial').Value := cdPersonas.FieldByName('ApellidoMaternoContactoCom').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@NombreContactoComercial').Value := cdPersonas.FieldByName('NombreContactoComercial').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@SexoContactoComercial').Value := cdPersonas.FieldByName('SexoContactoComercial').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@EmailContactoComercial').Value := cdPersonas.FieldByName('Email').Value;

            //********* DATOS DEL DOMICILIO **********//
            //La calle est� desnormalizada.
            //spCrearSolicitudImportacion.Parameters.ParamByName('CodigoCalle').Value := ;
            //spCrearSolicitudImportacion.Parameters.ParamByName('CodigoSegmento').Value :=
            spCrearSolicitudImportacion.Parameters.ParamByName('@CalleDesnormalizada').Value := cdPersonas.FieldByName('Calle').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@Numero').Value := cdPersonas.FieldByName('Numero').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@Detalle').Value := cdPersonas.FieldByName('DetalleDomicilio').Value;
            //spCrearSolicitudImportacion.Parameters.ParamByName('CodigoPostal').Value :=
            //Es una region y comuna de Chile
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoRegion').Value := cdPersonas.FieldByName('CodigoRegion').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoComuna').Value := cdPersonas.FieldByName('CodigoComuna').Value;

            //********* DATOS DEL MEDIO DE COMUNICACION: TELEFONO PRINCIPAL **********//
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoTipoMedioContactoTelefonoPrincipal').Value := cdPersonas.FieldByName('TipoTelefonoPrincipal').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoAreaTelefonoPrincipal').Value := cdPersonas.FieldByName('CodigoAreaTelefonoPrincipal').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@ValorTelefonoPrincipal').Value := cdPersonas.FieldByName('NumeroTelefonoPrincipal').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@HorarioDesdeTelefonoPrincipal').Value := cdPersonas.FieldByName('HoraDesde').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@HorarioHastaTelefonoPrincipal').Value := cdPersonas.FieldByName('HoraHasta').Value;

            //********* DATOS DEL MEDIO DE COMUNICACION: EMAIL **********//
            // Fixeado el tipo de medio de contacto a 1 = E-mail
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoTipoMedioContactoeMAIL').Value := TIPO_MEDIO_CONTACTO_E_MAIL;
            spCrearSolicitudImportacion.Parameters.ParamByName('@Email').Value := cdPersonas.FieldByName('Email').Value;

            //********* DATOS DEL MEDIO DE COMUNICACION: TELEFONO ALTERNATIVO **********//
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoTipoMedioContactoTelefonoAlternativo').Value := cdPersonas.FieldByName('TipoTelefonoAlternativo').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoAreaTelefonoAlternativo').Value := cdPersonas.FieldByName('CodigoAreaTelefonoAlternativo').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@ValorTelefonoAlternativo').Value := cdPersonas.FieldByName('NumeroTelefonoAlternativo').Value;


            //********* DATOS DEL MEDIO DE PAGO AUTOMATICO: PAC **********//
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoTipoCuentaBancaria').Value := cdPersonas.FieldByName('CodigoTipoCuentaBancaria').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoBanco').Value := cdPersonas.FieldByName('CodigoBanco').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@NroCuentaBancaria').Value := cdPersonas.FieldByName('NroCuentaBancaria').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@Sucursal').Value := cdPersonas.FieldByName('Sucursal').Value;

            //********* DATOS DEL MEDIO DE PAGO AUTOMATICO: PAT **********//
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoTipoTarjetaCredito').Value := cdPersonas.FieldByName('CodigoTipoTarjetaCredito').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@NumeroTarjetaCredito').Value := cdPersonas.FieldByName('NumeroTarjetaCredito').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@FechaVencimiento').Value := cdPersonas.FieldByName('FechaVenciemiento').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoEmisorTarjetaCredito').Value := cdPersonas.FieldByName('CodigoEmisorTarjetaCredito').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@NumeroDocumentoMedioPagoAutomatico').Value := cdPersonas.FieldByName('NumeroDocumentoTitular').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@NombreMedioPagoAutomatico').Value := cdPersonas.FieldByName('NombreTitular').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@TelefonoMedioPagoAutomatico').Value := cdPersonas.FieldByName('TelefonoTitular').Value;
            spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoAreaMedioPagoAutomatico').Value := cdPersonas.FieldByName('CodigoAreaTelefonoTitular').Value;
            spCrearSolicitudImportacion.ExecProc;
            codigoSolicitudContacto := spCrearSolicitudImportacion.Parameters.ParamByName('@CodigoSolicitudContacto').Value;

            if codigoSolicitudContacto = -1 then
             raise Exception.Create(ERROR_GUARDANDOVALORES + '. ' +
                    cdVehiculos.FieldByName('CodigoSolicitud').asstring + ', ' +
                    cdPersonas.FieldByName('Apellido').asstring + ', ' +
                    cdPersonas.FieldByName('Nombre').asstring);

            cdVehiculos.First;
            while not cdVehiculos.Eof do
            begin
                  spActualizarVehiculosContacto.Parameters.ParamByName('@CodigoMarca').Value := cdVehiculos.FieldByName('CodigoMarca').AsString;
                  spActualizarVehiculosContacto.Parameters.ParamByName('@Modelo').Value := cdVehiculos.FieldByName('Modelo').AsString;
                  spActualizarVehiculosContacto.Parameters.ParamByName('@AnioVehiculo').Value := cdVehiculos.FieldByName('AnioVehiculo').AsString;
                  spActualizarVehiculosContacto.Parameters.ParamByName('@CodigoTipoVehiculo').Value := cdVehiculos.FieldByName('CodigoTipoVehiculo').AsString;
                  spActualizarVehiculosContacto.Parameters.ParamByName('@CodigoSolicitudContacto').Value := codigoSolicitudContacto;
                  spActualizarVehiculosContacto.Parameters.ParamByName('@TipoPatente').Value := 'CHL';
                  spActualizarVehiculosContacto.Parameters.ParamByName('@Patente').Value := cdVehiculos.FieldByName('Patente').AsString;
                  spActualizarVehiculosContacto.ExecProc;
                  cdVehiculos.Next;
            end;
            cdPersonas.Next;

            self.Refresh;
            application.ProcessMessages;

      end;
    finally
        spCrearSolicitudImportacion.Close;
        spActualizarVehiculosContacto.Close;
        cdVehiculos.Close;
        cdVehiculos.MasterFields := 'CodigoSolicitudContacto';
        cdVehiculos.MasterSource := nil;
        cdVehiculos.Open;
    end;
end;





procedure TFormSeleccionArchivoImportacionSA.SetearErrorGlobalLinea(Error: string);
begin
   Ferrores := Ferrores + #9 + Error  + #13#10;
end;

procedure TFormSeleccionArchivoImportacionSA.SetearErrorGlobal(Error, Error2: string);
begin
   Ferrores := Ferrores + Error  + #13#10;
   Ferrores := Ferrores + #9 + Error2 + #13#10;
end;

procedure TFormSeleccionArchivoImportacionSA.EscribirErrorGlobalLinea;
begin
    if FErrores <> '' then begin
        Writeln(FArchivoError, Format(MSG_LINEANRO,[FNroLinea]));
        Writeln(FArchivoError, FErrores);
        Inc(FConteoErrores);
        FErrores := '';
    end;
end;

procedure TFormSeleccionArchivoImportacionSA.EscribirErrorGlobal;
begin
    if FErrores <> '' then begin
        Writeln(FArchivoError, FErrores);
        Inc(FConteoErrores);
        FErrores := '';
    end;
end;


procedure TFormSeleccionArchivoImportacionSA.ValidarRequeridos(cd: TClientDataSet);
resourcestring
  ERROR_CAMPOREQUERIDO = '%s es requerido';
var
 i : integer;
begin
//        if cd.RecordCount = 0 then exit;
        for i := 0 to cd.FieldCount - 1 do
        if (cd.Fields[i].Required) and (cd.Fields[i].asstring = '') then
          SetearErrorGlobalLinea(Format(ERROR_CAMPOREQUERIDO,[cd.Fields[i].DisplayName]));
end;


procedure TFormSeleccionArchivoImportacionSA.ValidarCodigoAreaTelefono(CodigoArea, Telefono, Error: string);
resourcestring
 ERROR_AREAINCORRECTA = 'C�digo de �rea Incorrecta';
 ERROR_NUMEROTELEFONOINCORRECTO = 'La longitud del n�mero telef�nico no corresponde con el c�digo de �rea';
begin
    try
        if (CodigoArea = '') or (Telefono = '') then exit;
        qryValidarAreaTelefono.Parameters.ParamByName('CodigoArea').Value := CodigoArea;
        qryValidarAreaTelefono.Open;
        if qryValidarAreaTelefono.RecordCount = 0 then SetearErrorGlobalLinea(Error + ' ' + ERROR_AREAINCORRECTA)
        else if  (Length(Telefono) < qryValidarAreaTelefono.FieldByName('RangoDesde').AsInteger) or
                (Length(Telefono) < qryValidarAreaTelefono.FieldByName('RangoHasta').AsInteger) then SetearErrorGlobalLinea(Error + '. ' + ERROR_NUMEROTELEFONOINCORRECTO);

    finally
        qryValidarAreaTelefono.Close;
    end;
end;


procedure TFormSeleccionArchivoImportacionSA.ValidarTipoTelefono(
  Tipo, Error: string);
begin
 try
    if  Tipo <> '' then begin
        qryValidarTipoTelefono.Parameters.ParamByName('CodigoTipoMedioContacto').Value := tipo;
        qryValidarTipoTelefono.Open;
        if qryValidarTipoTelefono.Fields[0].AsInteger = 0 then SetearErrorGlobalLinea(Error);
    end;
 finally
     qryValidarTipoTelefono.close;
 end;
end;



procedure TFormSeleccionArchivoImportacionSA.btnImportarClick(Sender: TObject);
begin
        Screen.Cursor   := crHourGlass;
        try
            Importar;
        finally
            cdVehiculos.Close;
            cdPersonas.Close;
            FinalizarImportacion;
            Screen.Cursor   := crDefault;
        end;
end;



procedure TFormSeleccionArchivoImportacionSA.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormSeleccionArchivoImportacionSA.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action:= caFree;
end;

procedure TFormSeleccionArchivoImportacionSA.spObtenerArchivosImportarAfterOpen(
  DataSet: TDataSet);
begin
    btnImportar.Enabled := (DataSet.Active) and (DataSet.RecordCount > 0) and (spObtenerArchivosImportar.FieldByName('CodigoEstado').AsInteger = ESTADO_PENDIENTE);
end;

procedure TFormSeleccionArchivoImportacionSA.dblArchivos1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    dblArchivosClick(nil);
end;


procedure TFormSeleccionArchivoImportacionSA.dblArchivosClick(Sender: TObject);
begin
    btnImportar.Enabled := spObtenerArchivosImportar.FieldByName('CodigoEstado').AsInteger in [ESTADO_PENDIENTE,ESTADO_CON_ERRORES];
end;


procedure TFormSeleccionArchivoImportacionSA.dblArchivosColumns4HeaderClick(Sender: TObject);
begin
    TDBListExColumn(sender).Sorting:=iif(TDBListExColumn(sender).Sorting=csAscending ,csDescending,csAscending);
    spObtenerArchivosImportar.Sort := TDBListExColumn(sender).FieldName  + ' ' + iif(TDBListExColumn(sender).Sorting=csAscending ,'ASC','DESC');
end;

procedure TFormSeleccionArchivoImportacionSA.dblArchivosDblClick(Sender: TObject);
begin
    if btnImportar.Enabled then btnImportar.Click;
end;




end.

