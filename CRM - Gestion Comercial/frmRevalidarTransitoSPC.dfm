object RevalidarTransitoSPCForm: TRevalidarTransitoSPCForm
  Left = 0
  Top = 0
  Caption = 'RevalidarTransitoSPCForm'
  ClientHeight = 598
  ClientWidth = 977
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlInfraccion: TGroupBox
    Left = 0
    Top = 410
    Width = 977
    Height = 188
    Align = alBottom
    Caption = ' Datos Tr'#225'nsito : '
    TabOrder = 0
    DesignSize = (
      977
      188)
    object Label5: TLabel
      Left = 59
      Top = 97
      Width = 37
      Height = 13
      Caption = 'Estado:'
    end
    object lblEstado: TLabel
      Left = 104
      Top = 97
      Width = 43
      Height = 13
      Caption = 'lblEstado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 54
      Top = 59
      Width = 42
      Height = 13
      Caption = 'Patente:'
    end
    object Label7: TLabel
      Left = 8
      Top = 77
      Width = 3
      Height = 13
    end
    object Label8: TLabel
      Left = 45
      Top = 78
      Width = 51
      Height = 13
      Caption = 'Categor'#237'a:'
    end
    object lblPatente: TLabel
      Left = 104
      Top = 59
      Width = 48
      Height = 13
      Caption = 'lblPatente'
    end
    object lblCategoria: TLabel
      Left = 104
      Top = 78
      Width = 57
      Height = 13
      Caption = 'lblCategoria'
    end
    object lblAnularInfraccion: TLabel
      Left = 40
      Top = 29
      Width = 91
      Height = 13
      Cursor = crHandPoint
      Caption = 'Datos Actuales:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 302
      Top = 29
      Width = 85
      Height = 13
      Cursor = crHandPoint
      Caption = 'Nuevos Datos:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 36
      Top = 116
      Width = 60
      Height = 13
      Caption = 'NumCorrCA:'
    end
    object lblNumCorrCA: TLabel
      Left = 104
      Top = 116
      Width = 65
      Height = 13
      Caption = 'lblNumCorrCA'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 21
      Top = 135
      Width = 75
      Height = 13
      Caption = 'Fecha Transito:'
    end
    object lblFechaTTo: TLabel
      Left = 104
      Top = 135
      Width = 60
      Height = 13
      Caption = 'lblFechaTTo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 59
      Top = 154
      Width = 37
      Height = 13
      Caption = 'Portico:'
    end
    object lblPortico: TLabel
      Left = 104
      Top = 154
      Width = 43
      Height = 13
      Caption = 'lblPortico'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtPatente: TEdit
      Left = 304
      Top = 55
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 8
      TabOrder = 0
      Text = 'TXTPATENTE'
      OnChange = VerSiHabilita
      OnKeyPress = txtPatenteKeyPress
    end
    object cbCategoria: TVariantComboBox
      Left = 304
      Top = 82
      Width = 233
      Height = 22
      Style = vcsOwnerDrawVariable
      Enabled = False
      ItemHeight = 16
      TabOrder = 1
      Visible = False
      OnClick = VerSiHabilita
      Items = <>
    end
    object btnCancelar: TButton
      Left = 784
      Top = 79
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 2
      OnClick = btnCancelarClick
    end
    object btnAceptar: TButton
      Left = 784
      Top = 40
      Width = 75
      Height = 25
      Caption = '&Aceptar'
      Default = True
      TabOrder = 3
      OnClick = btnAceptarClick
    end
    object btnSalir: TButton
      Left = 876
      Top = 64
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Salir'
      TabOrder = 4
      OnClick = btnSalirClick
    end
    object grpRNVM: TGroupBox
      Left = 543
      Top = 32
      Width = 235
      Height = 129
      Caption = 'Ultima Consulta RNVM'
      TabOrder = 5
      object Label4: TLabel
        Left = 24
        Top = 40
        Width = 38
        Height = 13
        Caption = 'Modelo:'
      end
      object lblRNVMModelo: TLabel
        Left = 68
        Top = 40
        Width = 155
        Height = 13
        AutoSize = False
        Caption = 'lblRNVMModelo'
      end
      object Label11: TLabel
        Left = 29
        Top = 60
        Width = 33
        Height = 13
        Caption = 'Marca:'
      end
      object lblRNVMMarca: TLabel
        Left = 68
        Top = 60
        Width = 155
        Height = 13
        AutoSize = False
        Caption = 'lblRNVMMarca'
      end
      object Label13: TLabel
        Left = 11
        Top = 80
        Width = 51
        Height = 13
        Caption = 'Categor'#237'a:'
      end
      object lblRNVMCategoria: TLabel
        Left = 68
        Top = 80
        Width = 155
        Height = 13
        AutoSize = False
        Caption = 'lblRNVMCategoria'
      end
      object Label10: TLabel
        Left = 20
        Top = 21
        Width = 42
        Height = 13
        Caption = 'Patente:'
      end
      object lblRNVMPatente: TLabel
        Left = 68
        Top = 21
        Width = 76
        Height = 13
        Caption = 'lblRNVMPatente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 25
        Top = 99
        Width = 37
        Height = 13
        Caption = 'Fecha::'
      end
      object lblRNVMFecha: TLabel
        Left = 68
        Top = 99
        Width = 155
        Height = 13
        AutoSize = False
        Caption = 'lblRNVMFecha'
      end
    end
  end
  object gbImagenAmpliar: TGroupBox
    Left = 0
    Top = 0
    Width = 977
    Height = 410
    Align = alClient
    Caption = 'Imagen'
    TabOrder = 1
    object pgcImages: TPageControl
      Left = 2
      Top = 15
      Width = 973
      Height = 393
      ActivePage = tsFrontal1
      Align = alClient
      TabOrder = 0
      object tsFrontal1: TTabSheet
        Caption = ' Frontal 1 '
        object imgFrontal1: TImage
          Left = 0
          Top = 0
          Width = 965
          Height = 365
          Align = alClient
          Stretch = True
          ExplicitLeft = 2
        end
      end
      object tsFrontal2: TTabSheet
        Caption = ' Frontal 2 '
        ImageIndex = 1
        object imgFrontal2: TImage
          Left = 0
          Top = 0
          Width = 965
          Height = 365
          Align = alClient
          Stretch = True
          ExplicitWidth = 618
          ExplicitHeight = 382
        end
      end
      object tsContexto1: TTabSheet
        Caption = ' Contexto 1 '
        ImageIndex = 2
        object imgContexto1: TImage
          Left = 0
          Top = 0
          Width = 965
          Height = 365
          Align = alClient
          Stretch = True
          ExplicitWidth = 411
          ExplicitHeight = 274
        end
      end
      object tsContexto2: TTabSheet
        Caption = ' Contexto 2 '
        ImageIndex = 3
        object imgContexto2: TImage
          Left = 0
          Top = 0
          Width = 965
          Height = 365
          Align = alClient
          Stretch = True
          ExplicitWidth = 618
          ExplicitHeight = 382
        end
      end
      object tsContexto3: TTabSheet
        Caption = ' Contexto 3 '
        ImageIndex = 4
        object imgContexto3: TImage
          Left = 0
          Top = 0
          Width = 965
          Height = 365
          Align = alClient
          Stretch = True
          ExplicitWidth = 618
          ExplicitHeight = 382
        end
      end
      object tsContexto4: TTabSheet
        Caption = ' Contexto 4 '
        ImageIndex = 5
        object imgContexto4: TImage
          Left = 0
          Top = 0
          Width = 965
          Height = 365
          Align = alClient
          Stretch = True
          ExplicitWidth = 618
          ExplicitHeight = 382
        end
      end
      object tsContexto5: TTabSheet
        Caption = ' Contexto 5 '
        ImageIndex = 6
        object imgContexto5: TImage
          Left = 0
          Top = 0
          Width = 965
          Height = 365
          Align = alClient
          Stretch = True
          ExplicitWidth = 618
          ExplicitHeight = 382
        end
      end
      object tsContexto6: TTabSheet
        Caption = ' Contexto 6 '
        ImageIndex = 7
        object imgContexto6: TImage
          Left = 0
          Top = 0
          Width = 965
          Height = 365
          Align = alClient
          Stretch = True
          ExplicitWidth = 618
          ExplicitHeight = 382
        end
      end
    end
  end
  object tmerVisualizar: TTimer
    Enabled = False
    OnTimer = tmerVisualizarTimer
    Left = 512
    Top = 424
  end
  object spRevalidarTransito: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RevalidarTransito'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumCorrCA'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NuevaPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@NuevaCategoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 552
    Top = 424
  end
  object spObtenerUltimaConsultaRNVM: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerUltimaConsultaRNVM'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 584
    Top = 424
  end
  object spObtenerTransitosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandTimeout = 500
    ProcedureName = 'ObtenerTransitosCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftString
        Size = 14
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 14
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Portico'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CantidadAMostrar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadTransitos'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 301
    Top = 275
  end
end
