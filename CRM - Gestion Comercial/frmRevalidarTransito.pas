{********************************** File Header ********************************
File Name   : frmRevalidarTransito
Author      : mbecerra
Date Created: 07-04-2008
Language    : ES-CL
Revisi�n 1  : Recibe un listado de tr�nsitos a revalidar y despliega,
				para cada uno de ellos, algunos datos y da al operador
                la alternativa de modificar la Patente y/o la categor�a.

                Al presionar el bot�n Aceptar se invoca al SP RevalidarTransito

Revision 1
Author: mbecerra
Date: 10-Febrero-2009
Description: 	Producto de los cambios en facturaci�n, se agregan nuevos c�digos
            	de retorno de resultados.

                Se le quita la transaccionalidad a la aplicaci�n, pues ahora la tiene
                el stored RevalidarTransito

        01-Septiembre-2009
            1.-		El cliente desea permitir patentes con 5 caracteres, pero
                	que empiecen por las letras 'PR', ya que son patentes
                    provisorias.
Revision	:2
Author		: Nelson Droguett Sierra (pdominguez)
Date		: 07-Junio-2011
Description	: Se agrega la variable FImagePathNFI
              Function VerImagenTransito
                Se agrega la variable NombreCorto
                Se envia el par�metro FImagePathNFI en las llamadas a la funci�n
    		    ObtenerImagenTransito.
              Function Inicializar
                Se obtienen los parametrosGenerales de las nuevas rutas de imagenes  

Firma		: SS-377-NDR-20110607

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.

Firma       :   SS_1135_CQU_20131030
Descripcion :   Las im�genes de Infracciones se guardan en un directorio y las de tr�nsitos en otro,
                ahora se deben obtener las im�genes desde el directorio de infracciones, adicionalmente,
                se deben habilitar los botones F (F1, F2, etc)

Firma       :   SS_1222_MBE_20141022
Description :   No debe permitir revalidar un tr�nsito que NO es de CN y que
                el Operador le cambi� la categor�a

Firma       :   SS_1222_CQU_20150317
Description :   Se realiza cambio para que no se habilite el ComboBox de las categor�as
                cuando la concesionaria sea distinta de CN.

Firma       :   SS_1222_NDR_20150406
Description :   Se elimina referencia a FConcesionariaNativa

Firma		:	SS_1147ZZZ_NDR_20150415
Descripcion	:	Se reemplaza la funcion CONST_CODIGO_CONCESIONARIA_COSTANERA_NORTE() por dbo.ObtenerConcesionariaNativa()
              Se cambia todo lo relativo a CN por Nativa
--------------------------------------------------------------------------------}

unit frmRevalidarTransito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Grids, DBGrids, ComCtrls, DMConnection, DB, ADODB,
  UtilProc, RStrings, ImgList, OPImageOverview, DPSControls, DBCtrls, ImgProcs,
  ImagePlus, ToolWin, Buttons, ActnList, ActnMan, ConstParametrosGenerales, ImgTypes,
  JPEGPlus, Filtros, Math, formImagenOverview, UtilDB, Util, PeaProcs, RVMLogin,
  Provider, DBClient, ListBoxEx, DBListEx, DmiCtrls, ActualizarDatosInfractor,
  VariantComboBox, PeaTypes, XPStyleActnCtrls, JPeg, PeaProcsCN, SysUtilsCN;    // SS_1135_CQU_20131030

type
  TRevalidarTransitoForm = class(TForm)
    pnlInfraccion: TGroupBox;
    Label5: TLabel;
    lblEstado: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    lblPatente: TLabel;
    lblCategoria: TLabel;
    lblAnularInfraccion: TLabel;
    Label9: TLabel;
    txtPatente: TEdit;
    gbImagenAmpliar: TGroupBox;
    ImagenAmpliar: TImagePlus;
    txtNoHayImagen: TEdit;
    ActionManager: TActionManager;
    actZoomIn: TAction;
    actZoomOut: TAction;
    actZoomReset: TAction;
    actZoomPlate: TAction;
    actBrightPlus: TAction;
    actBrigthLess: TAction;
    actBrigthReset: TAction;
    actCenterToPlate: TAction;
    actResetImage: TAction;
    actRemarcarBordes: TAction;
    actSuavizarBordes: TAction;
    actImageOverview: TAction;
    ImageList1: TImageList;
    cbCategoria: TVariantComboBox;
    btnCancelar: TButton;
    btnAceptar: TButton;
    Label1: TLabel;
    lblNumCorrCA: TLabel;
    gbImagenes: TGroupBox;
    pnlThumb: TPanel;
    sbThumb0: TSpeedButton;
    sbThumb1: TSpeedButton;
    sbThumb2: TSpeedButton;
    sbThumb5: TSpeedButton;
    sbThumb4: TSpeedButton;
    sbThumb3: TSpeedButton;
    sbThumb6: TSpeedButton;
    CoolBar2: TCoolBar;
    ToolBar2: TToolBar;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    ToolButton1: TToolButton;
    chkVerMascara: TCheckBox;
    tmerVisualizar: TTimer;
    spRevalidarTransito: TADOStoredProc;
    btnSalir: TButton;
    Label2: TLabel;
    lblFechaTTo: TLabel;
    Label3: TLabel;
    lblPortico: TLabel;
    grpRNVM: TGroupBox;
    Label4: TLabel;
    lblRNVMModelo: TLabel;
    Label11: TLabel;
    lblRNVMMarca: TLabel;
    Label13: TLabel;
    lblRNVMCategoria: TLabel;
    spObtenerUltimaConsultaRNVM: TADOStoredProc;
    Label10: TLabel;
    lblRNVMPatente: TLabel;
    Label12: TLabel;
    lblRNVMFecha: TLabel;
    actImagenFrontal1: TAction;		// SS_1135_CQU_20131030
    actImagenFrontal2: TAction;		// SS_1135_CQU_20131030
    actImagenPosterior1: TAction;	// SS_1135_CQU_20131030
    actImagenPosterior2: TAction;	// SS_1135_CQU_20131030
    procedure FormShow(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure VerSiHabilita(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure actZoomInExecute(Sender: TObject);
    procedure actZoomOutExecute(Sender: TObject);
    procedure actZoomResetExecute(Sender: TObject);
    procedure actBrightPlusExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbThumb0Click(Sender: TObject);
    procedure sbThumb2Click(Sender: TObject);
    procedure sbThumb3Click(Sender: TObject);
    procedure actBrightLessExecute(Sender: TObject);
    procedure actBrightResetExecute(Sender: TObject);
    procedure actRemarcarBordesExecute(Sender: TObject);
    procedure actSuavizarBordesExecute(Sender: TObject);
    procedure actCenterToPlateExecute(Sender: TObject);
    procedure actResetImageExecute(Sender: TObject);
    procedure actImageOverviewExecute(Sender: TObject);
    procedure sbThumb1Click(Sender: TObject);
    procedure sbThumb4Click(Sender: TObject);
    procedure sbThumb5Click(Sender: TObject);
    procedure sbThumb6Click(Sender: TObject);
    procedure chkVerMascaraClick(Sender: TObject);
    procedure tmerVisualizarTimer(Sender: TObject);
    procedure txtPatenteKeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelarClick(Sender: TObject);
    procedure actImagenFrontal1Execute(Sender: TObject);	// SS_1135_CQU_20131030
    procedure actImagenFrontal2Execute(Sender: TObject);	// SS_1135_CQU_20131030
    procedure actImagenPosterior1Execute(Sender: TObject);	// SS_1135_CQU_20131030
    procedure actImagenPosterior2Execute(Sender: TObject);	// SS_1135_CQU_20131030
  private
    { Private declarations }
    FImagePath,FImagePathNFI: AnsiString;                                       //SS-377-NDR-20110607

	FJPG12Frontal: TJPEGPlusImage;
	FJPG12Frontal2: TJPEGPlusImage;
	FJPG12Posterior: TJPEGPlusImage;
	FJPG12Posterior2: TJPEGPlusImage;
	FImgOverview1: TBitMap;
	FImgOverview2: TBitMap;
	FImgOverview3: TBitMap;

	FImagenActual: TJPEGPlusImage;
    FTipoImagenActual: TTipoImagen;
    FShiftOriginal: Integer;

    FPositionPlate: Array[tiFrontal..tiPosterior2] of TRect;
    FFrmImagenOverview: TFrmImageOverview;
    FFormOverviewCargado: Boolean;

    // Imagenes Italianas               // SS_1091_CQU_20130516
    FImagenItalianaActual,              // SS_1091_CQU_20130516
    FJPGItalianoFrontal,                // SS_1091_CQU_20130516
	FJPGItalianoFrontal2,               // SS_1091_CQU_20130516
	FJPGItalianoPosterior,              // SS_1091_CQU_20130516
	FJPGItalianoPosterior2: TBitmap;    // SS_1091_CQU_20130516
    FCodigoConcesionariaNativa : Integer;   //SS_1147ZZZ_NDR_20150415 SS_1222_NDR_20150460 SS_1222_CQU_20150317

    procedure SeleccionarImagen(aGraphic: TGraphic; aTipoImagen: TTipoImagen);
  public
    { Public declarations }
    procedure Inicializar(txtCaption : string);
    procedure InicializarVariables;
    procedure VisualizaDatos;
    procedure Redibujar;
    procedure VerImagenTransito;
    procedure CentrarImagen;
    procedure AsignarRect(Indice : TTipoImagen; DataImage : TDataImage);
    procedure SiguienteTransito;
    procedure DespliegaMensaje(CodigoError : integer);
    procedure ConsultaRNVM;
  end;

var
  RevalidarTransitoForm: TRevalidarTransitoForm;
  lstListaTransitosARevalidar,                          {NumCorrCA de los Transitos a revalidar}
  lstListaPatenteyCategoriaARevalidar : TStringList;	{otros datos, como Patente y categoria de ese Transito}

implementation

{$R *.dfm}
ResourceString
    CAPTION_POS_PATENTE = 'Posici�n de Patente Desconocida';
    //MSG_SQL_CN      = 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_COSTANERA_NORTE()';            //SS_1222_CQU_20150317
    MSG_SQL_Nativa      = 'SELECT dbo.ObtenerConcesionariaNativa()';                          //SS_1147ZZZ_NDR_20150415 SS_1222_CQU_20150317


var
    IndiceCategoria : integer;
    EstadoTransito, CategoriaTransito, NumCorrCA : string;
    ImagenNumCorrCA : Int64;
    FechaTransito : TDateTime;
    PrimeraVezTimer : boolean;  {se usa para acelerar el Timer la siguiente vez}
    EsItaliano : Boolean;   // SS_1091_CQU_20130516

{-------------------------------------------------------------------------------
 Function StrToFechaHora
 Revision 1:
    Author : mbecerra
    Date : 09-04-2008
    Description : convierte un string de la forma ddmmyyyyhhnnsszz en un TDateTime. No efect�a
                	validaci�n de la fecha. Esto es para evitar leer la configuraci�n regional
-------------------------------------------------------------------------------}
function StrToFechaHora(fecha : string) : TDateTime;
var
    dia, mes, anno, hora, min, seg, mseg : word;
begin
  dia  := StrToInt(Copy(fecha,1,2));
  mes  := StrToInt(Copy(fecha,3,2));
  anno := StrToInt(Copy(fecha,5,4));
  hora := StrToInt(Copy(fecha,9,2));
  min  := StrToInt(Copy(fecha,11,2));
  seg  := StrToInt(Copy(fecha,13,2));
  mseg := StrToInt(Copy(fecha,15,2));
  Result := EncodeDate(anno, mes, dia) + EncodeTime(hora,min,seg,mseg);
end;

{-------------------------------------------------------------------------------
 Function GetItem
 Revision 1:
    Author : mbecerra
    Date : 08-04-2008
    Description : Function que recibe un string de la forma <campo1>sep<campo2>sep<campo3>
                	y devuelve el <campo i> indicado en el par�metro, o un string vac�o
                    si no lo encuentra
-------------------------------------------------------------------------------}
function GetItem(Texto : string; NumeroItem : integer; Separador : Char) : string;
var
    Elemento : string;
    i, Cual : integer;
begin
    i := 1;
    Elemento := '';
    Cual := 0;
    Texto := Texto + Separador;  {para asegurarnos de encontrar el separador en el �ltimo elemento}
    while i <= Length(Texto) do begin
        if Texto[i] = Separador then begin
            Inc(Cual);
            if Cual = NumeroItem then   {es el elemento que se debe retornar}
            	i := Length(Texto) + 1
            else
                Elemento := '';         {no es el elemento que se debe retornar}

        end
        else
          Elemento := Elemento + Texto[i];

    	Inc(i);
    end;

    Result := Elemento;
end;

procedure TRevalidarTransitoForm.ConsultaRNVM;
resourcestring
    SIN_DATOS = 'Sin Datos';
begin
	spObtenerUltimaConsultaRNVM.Close;
    spObtenerUltimaConsultaRNVM.CommandTimeout := 500;
    spObtenerUltimaConsultaRNVM.Parameters.ParamByName('@Patente').Value := txtPatente.Text;
    spObtenerUltimaConsultaRNVM.Open;
    grpRNVM.Visible := True;
    if spObtenerUltimaConsultaRNVM.IsEmpty then begin
        lblRNVMModelo.Caption    := SIN_DATOS;
        lblRNVMMarca.Caption     := '';
        lblRNVMCategoria.Caption := '';
        lblRNVMPatente.Caption   := txtPatente.Text;
        lblRNVMFecha.Caption     := '';
    end
    else begin
        lblRNVMModelo.Caption    := spObtenerUltimaConsultaRNVM.FieldByName('Model').AsString;
        lblRNVMMarca.Caption     := spObtenerUltimaConsultaRNVM.FieldByName('Manufacturer').AsString;
        lblRNVMCategoria.Caption := spObtenerUltimaConsultaRNVM.FieldByName('Category').AsString;
        lblRNVMPatente.Caption   := spObtenerUltimaConsultaRNVM.FieldByName('LicensePlate').AsString;
        lblRNVMFecha.Caption     := DateToStr(spObtenerUltimaConsultaRNVM.FieldByName('UltimaFechaModificacion').AsDateTime);
    end;

    spObtenerUltimaConsultaRNVM.Close;
end;

procedure TRevalidarTransitoForm.DespliegaMensaje;
resourcestring
        MENSAJE_DE_ERROR = 'C�digo %d' + #13#13 + 'Descripci�n: %s';
var
  Descripcion : string;
begin
    case CodigoError of
       -1: Descripcion := 'Estado = 1, Falta Relaci�n TransitosItemsPeajeCuenta';
       -2: Descripcion := 'Estado = 1, No se pudo borrar la relaci�n en TransitosItemsPeajeCuenta';
       -3: Descripcion := 'Estado = 1, No se pudo actualizar la tabla ItemsPeajeCuenta, con la resta del Importe';
       -4: Descripcion := 'Estado = 1, No se pudo borrar desde ItemsPeajeCuenta el registro cuyo importe es cero';
       -5: Descripcion := 'Estado = 10 � 14, Fall� el Store AnularTransitoInfraccion';
       -6: Descripcion := 'Estado = 12 � 13, Fall� la b�squeda de un DayPass';
       -7: Descripcion := 'Estado = 12 � 13, Fall� la eliminaci�n del DayPas';
       -8: Descripcion := 'Estado = 12 � 13, No se pudo dejar "sin usar" el DayPass';
       -9: Descripcion := 'No se pudo calcular el nuevo importe, para la nueva categor�a';
      -10: Descripcion := 'No se pudo insertar un detalle tr�nsito para la nueva patente y nueva categor�a';
      -11: Descripcion := 'No se pudo actualizar el tr�nsito con el nuevoID, Importe y TipoHorario';
      -12: Descripcion := 'Fall� el Store CambiarEstadoTransito';
      -13: Descripcion := 'Fall� el Store InsertarAuditoriaRevalidacionTransitos';
      -14: Descripcion := 'Fall� el Store ChequearTransitosLimbo2';
      -15: Descripcion := 'El Tr�nsito est� facturado (campo LoteFacturacion de MovimientosCuentas NO es NULL)';
      -16: Descripcion := 'No se eliminaron registros desde la tabla DetalleConsumoMovimientos';
      -17: Descripcion := 'No se pudo poner en NULL el campo NumeroMovimiento en la Tabla ItemsPeajeCuenta';
      -18: Descripcion := 'No se eliminaron registros desde la tabla MovimientosCuentas';
      -19: Descripcion := 'No se puede revalidar el transito pues tiene Daypass asociado y facturado';            // SS_1222_MBE_20141022
      -20: Descripcion := 'No puede cambiar la categor�a a un tr�nsito de otra Concesionaria';                    // SS_1222_MBE_20141022
    end;

	MsgBoxErr('No se pudo revalidar Tr�nsito ', Format(MENSAJE_DE_ERROR, [CodigoError, Descripcion]), Caption, MB_ICONERROR );
end;

procedure TRevalidarTransitoForm.SiguienteTransito;
begin
	lstListaTransitosARevalidar.Delete(0);
    lstListaPatenteyCategoriaARevalidar.Delete(0);
    tmerVisualizar.Enabled := True;
end;
procedure TRevalidarTransitoForm.AsignarRect;
begin
    FPositionPlate[Indice] := Rect(
    		Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),
            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),
            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),
            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));
end;

procedure TRevalidarTransitoForm.CentrarImagen;
begin
	if FPositionPlate[FTipoImagenActual].Left = 0 then begin
            ImagenAmpliar.Center
    end else
        ImagenAmpliar.CenterToPoint(Point(
            FPositionPlate[FTipoImagenActual].Left +
            ((FPositionPlate[FTipoImagenActual].right - FPositionPlate[FTipoImagenActual].Left) div 2),
            FPositionPlate[FTipoImagenActual].top +
            ((FPositionPlate[FTipoImagenActual].bottom - FPositionPlate[FTipoImagenActual].top) div 2)));
end;

procedure TRevalidarTransitoForm.chkVerMascaraClick(Sender: TObject);
begin
    Redibujar();
end;

procedure TRevalidarTransitoForm.InicializarVariables;
begin
	if Assigned(FImagenActual) then FreeAndNil(FImagenActual);
	if Assigned(FJPG12Frontal) then FreeAndNil(FJPG12Frontal);
	if Assigned(FJPG12Frontal2) then FreeAndNil(FJPG12Frontal2);
	if Assigned(FJPG12Posterior) then FreeAndNil(FJPG12Posterior);
	if Assigned(FJPG12Posterior2) then FreeAndNil(FJPG12Posterior2);
	if Assigned(FImgOverview1) then FreeAndNil(FImgOverview1);
	if Assigned(FImgOverview2) then FreeAndNil(FImgOverview2);
	if Assigned(FImgOverview3) then FreeAndNil(FImgOverview3);
    if Assigned(FImagenActual) then FreeAndNil(FImagenActual);
    // Imagenes Italianas                                                               // SS_1091_CQU_20130516
    if Assigned(FImagenItalianaActual)      then FreeAndNil(FImagenItalianaActual);     // SS_1091_CQU_20130516
    if Assigned(FJPGItalianoFrontal)        then FreeAndNil(FJPGItalianoFrontal);       // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoFrontal2)       then FreeAndNil(FJPGItalianoFrontal2);      // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior)      then FreeAndNil(FJPGItalianoPosterior);     // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior2)     then FreeAndNil(FJPGItalianoPosterior2);    // SS_1091_CQU_20130516
    // Cargar la Tabla de valores de confiabilidad para calcular ConfiabilidadTotal.
    FImagenActual       := TJPEGPlusImage.Create;
    FJPG12Frontal       := TJPEGPlusImage.Create;
    FJPG12Frontal2      := TJPEGPlusImage.Create;
    FJPG12Posterior     := TJPEGPlusImage.Create;
    FJPG12Posterior2    := TJPEGPlusImage.Create;
    FImgOverview1       := TBitMap.Create;
    FImgOverview1.PixelFormat := pf24bit;
    FImgOverview2             := TBitMap.Create;
    FImgOverview2.PixelFormat := pf24bit;
    FImgOverview3             := TBitMap.Create;
    FImgOverview3.PixelFormat := pf24bit;

    FTipoImagenActual := tiDesconocida;

    FFrmImagenOverview.ClearImagesOverview();
    FFormOverviewCargado := False;
    FFrmImagenOverview.Hide;
    // Imagenes Italianas                        // SS_1091_CQU_20130516
    FImagenItalianaActual   := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoFrontal     := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoFrontal2    := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoPosterior   := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoPosterior2  := TBitMap.Create;   // SS_1091_CQU_20130516
    // Inicializamos controles de imagenes
    FShiftOriginal      := -1;
    ImagenAmpliar.Scale	:= 1;
	actRemarcarBordes.Checked := False;
	actSuavizarBordes.Checked := False;
end;

procedure TRevalidarTransitoForm.VerImagenTransito;
ResourceString
    SIN_IMAGEN = '(Sin Imagen)';
    SQL_CODIGO_CONCESIONARIA = 'SELECT CodigoConcesionaria FROM Transitos WITH (NOLOCK) WHERE NumCorrCA=%d';   // SS_1222_CQU_20150317
var
    bmp: TBitMap;

  Procedure MostrarImagenBoton(btn: TSpeedButton; aGraphic: TGraphic);
  var
    aRect: TRect;
  begin
      bmp.FreeImage;
      bmp.Assign(aGraphic);
      bmp.Width := btn.Width;
      bmp.Height := btn.Height;
      bmp.TransparentColor := clRed;

      aRect := Rect(0, 0, btn.ClientRect.Right, btn.ClientRect.Bottom);
      bmp.Canvas.StretchDraw(aRect, aGraphic);
      btn.caption := EmptyStr;
      btn.Glyph.Assign(bmp);
      btn.Enabled := true;
  end;

  Procedure NoMostrarImagenBoton(btn: TSpeedButton);
  begin
      btn.Enabled := False;
      btn.Glyph.FreeImage;
      btn.Glyph.ReleaseHandle;
      btn.Glyph := nil;
      btn.Caption := SIN_IMAGEN;
  end;

var
	Error: TTipoErrorImg;
	DescriError, NombreCorto: AnsiString;                                       //SS-377-NDR-20110607
    DataImage: TDataImage;
    CodigoConcesionariaTransito : Integer;  // SS_1222_CQU_20150317
begin
	txtNoHayImagen.Visible := False;
    bmp := TBitMap.Create;
    try
    	Self.Update;
        Screen.Cursor := crHourGlass;
        //Cargo las imagenes

        InicializarVariables();
        NombreCorto := QueryGetValue(DMConnections.BaseCAC,Format('SELECT Concesionarias.NombreCorto FROM Transitos (NOLOCK) INNER JOIN Concesionarias (NOLOCK) ON Concesionarias.CodigoConcesionaria=Transitos.CodigoConcesionaria WHERE NumCorrCA=%d', [ImagenNumCorrCA]));                          //SS-377-NDR-20110607

        if EsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, ImagenNumCorrCA, FechaTransito, tiFrontal,  // SS_1091_CQU_20130516
            FJPGItalianoFrontal, DataImage, DescriError, Error) then begin                                              // SS_1091_CQU_20130516
                MostrarImagenBoton(sbThumb0, FJPGItalianoFrontal);                                                      // SS_1091_CQU_20130516
                AsignarRect(tiFrontal, DataImage);                                                                      // SS_1091_CQU_20130516
                sbThumb0.Click;                                                                                         // SS_1091_CQU_20130516
        end else                                                                                                        // SS_1091_CQU_20130516
        if ObtenerImagenTransito(FImagePathNFI, NombreCorto, ImagenNumCorrCA, FechaTransito, tiFrontal, FJPG12Frontal, DataImage, DescriError, Error) then begin                           //SS-377-NDR-20110607
        	MostrarImagenBoton(sbThumb0, FJPG12Frontal);
            AsignarRect(tiFrontal, DataImage);
            sbThumb0.Click;
        end
        else NoMostrarImagenBoton(sbThumb0);

        if EsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, ImagenNumCorrCA, FechaTransito, tiPosterior,// SS_1091_CQU_20130516
            FJPGItalianoPosterior, DataImage, DescriError, Error) then begin                                            // SS_1091_CQU_20130516
        	    MostrarImagenBoton(sbThumb1, FJPGItalianoPosterior);                                                    // SS_1091_CQU_20130516
                AsignarRect(tiPosterior, DataImage);                                                                    // SS_1091_CQU_20130516
                if FTipoImagenActual = tiDesconocida then sbThumb1.Click;                                               // SS_1091_CQU_20130516
        end else                                                                                                        // SS_1091_CQU_20130516
        if ObtenerImagenTransito(FImagePathNFI, NombreCorto, ImagenNumCorrCA, FechaTransito, tiPosterior, FJPG12Posterior, DataImage, DescriError, Error) then begin                       //SS-377-NDR-20110607
        	MostrarImagenBoton(sbThumb1, FJPG12Posterior);
            AsignarRect(tiPosterior, DataImage);
            if FTipoImagenActual = tiDesconocida then sbThumb1.Click;
        end
        else NoMostrarImagenBoton(sbThumb1);

        if ObtenerImagenTransito(FImagePathNFI, NombreCorto, ImagenNumCorrCA, FechaTransito, tiOverview1, FImgOverview1, DataImage, DescriError, Error) then begin                         //SS-377-NDR-20110607
        	MostrarImagenBoton(sbThumb2, FImgOverview1);
            AsignarRect(tiOverView1, DataImage);
            if FTipoImagenActual = tiDesconocida then sbThumb2.Click;
        end
        else NoMostrarImagenBoton(sbThumb2);

        if ObtenerImagenTransito(FImagePathNFI, NombreCorto, ImagenNumCorrCA, FechaTransito, tiOverview2, FImgOverview2, DataImage, DescriError, Error) then begin                         //SS-377-NDR-20110607
        	MostrarImagenBoton(sbThumb3, FImgOverview2);
            AsignarRect(tiOverview2, DataImage);
            if FTipoImagenActual = tiDesconocida then sbThumb3.Click;
        end
        else NoMostrarImagenBoton(sbThumb3);

        if ObtenerImagenTransito(FImagePathNFI, NombreCorto, ImagenNumCorrCA, FechaTransito, tiOverview3, FImgOverview3, DataImage, DescriError, Error) then begin                         //SS-377-NDR-20110607
        	MostrarImagenBoton(sbThumb4, FImgOverview3);
            AsignarRect(tiOverview3, DataImage);
            if FTipoImagenActual = tiDesconocida then sbThumb4.Click;
        end
        else NoMostrarImagenBoton(sbThumb4);

        if ObtenerImagenTransito(FImagePathNFI, NombreCorto, ImagenNumCorrCA, FechaTransito, tiFrontal2,    // SS_1091_CQU_20130516
            FJPGItalianoFrontal2, DataImage, DescriError, Error) then begin                                 // SS_1091_CQU_20130516
        	    MostrarImagenBoton(sbThumb5, FJPGItalianoFrontal2);                                         // SS_1091_CQU_20130516
                AsignarRect(tiFrontal2, DataImage);                                                         // SS_1091_CQU_20130516
                if FTipoImagenActual = tiDesconocida then sbThumb5.Click;                                   // SS_1091_CQU_20130516
        end else                                                                                            // SS_1091_CQU_20130516
        if ObtenerImagenTransito(FImagePathNFI, NombreCorto, ImagenNumCorrCA, FechaTransito, tiFrontal2, FJPG12Frontal2, DataImage, DescriError, Error) then begin                         //SS-377-NDR-20110607
        	MostrarImagenBoton(sbThumb5, FJPG12Frontal2);
            AsignarRect(tiFrontal2, DataImage);
            if FTipoImagenActual = tiDesconocida then sbThumb5.Click;
        end
        else NoMostrarImagenBoton(sbThumb5);

        if ObtenerImagenTransito(FImagePathNFI, NombreCorto, ImagenNumCorrCA, FechaTransito, tiPosterior2,  // SS_1091_CQU_20130516
            FJPGItalianoPosterior2, DataImage, DescriError, Error) then begin                               // SS_1091_CQU_20130516
        	MostrarImagenBoton(sbThumb6, FJPGItalianoPosterior2);                                           // SS_1091_CQU_20130516
            AsignarRect(tiPosterior2, DataImage);                                                           // SS_1091_CQU_20130516
            if FTipoImagenActual = tiDesconocida then sbThumb6.Click;                                       // SS_1091_CQU_20130516
        end else                                                                                            // SS_1091_CQU_20130516
        if ObtenerImagenTransito(FImagePathNFI, NombreCorto, ImagenNumCorrCA, FechaTransito, tiPosterior2, FJPG12Posterior2, DataImage, DescriError, Error) then begin                     //SS-377-NDR-20110607
        	MostrarImagenBoton(sbThumb6, FJPG12Posterior2);
            AsignarRect(tiPosterior2, DataImage);
            if FTipoImagenActual = tiDesconocida then sbThumb6.Click;
        end
        else NoMostrarImagenBoton(sbThumb6);

        if FTipoImagenActual = tiDesconocida then begin
        	ImagenAmpliar.Visible := False;
            EnableControlsInContainer(gbImagenAmpliar, False);
        end
        else begin
        	ImagenAmpliar.Visible := True;
            EnableControlsInContainer(gbImagenAmpliar, True);
        end;

        CodigoConcesionariaTransito := QueryGetIntegerValue(DMConnections.BaseCAC,Format(SQL_CODIGO_CONCESIONARIA, [ImagenNumCorrCA])); // SS_1222_CQU_20150317
        if CodigoConcesionariaTransito = FCodigoConcesionariaNativa                                                                     // SS_1147ZZZ_NDR_20150415 SS_1222_NDR_20150406 SS_1222_CQU_20150317
        then cbCategoria.Enabled := True                                                                                                // SS_1222_CQU_20150317
        else cbCategoria.Enabled := False;                                                                                              // SS_1222_CQU_20150317
        
    finally
    	bmp.free;
        Screen.Cursor := crDefault;
    end;

    txtNoHayImagen.Visible := FTipoImagenActual = tiDesconocida;
    chkVerMascara.Visible := not txtNoHayImagen.Visible;
    if txtNoHayImagen.Visible then txtNoHayImagen.Left := ((ImagenAmpliar.Width - txtNoHayImagen.Width) div 2) + ImagenAmpliar.Left;

end;
{-------------------------------------------------------------------------------
 Revision 1:
    Author : mbecerra
    Date : 08-04-2008
    Description : Habilita el bot�n btnAceptar si la patente y/o la categor�a
                	ingresada por el operador, difieren de las originales
                    De lo contrario lo Deshabilita.
-------------------------------------------------------------------------------}
procedure TRevalidarTransitoForm.VerSiHabilita(Sender: TObject);
begin
	btnAceptar.Enabled := (lblPatente.Caption <> txtPatente.Text) or (IndiceCategoria <> cbCategoria.ItemIndex);
    if (lblPatente.Caption <> txtPatente.Text)and (Length(txtPatente.Text)>= 6) then  begin
    	ConsultaRNVM();
    end
    else grpRNVM.Visible := False;
end;

procedure TRevalidarTransitoForm.Visualizadatos;
var
	Item : string;
begin
    grpRNVM.Visible := False;
	Item := lstListaPatenteyCategoriaARevalidar[0];
    NumCorrCA            := GetItem(Item, 1, #9);
	lblPatente.Caption   := GetItem(Item, 2, #9);
    lblEstado.Caption    := GetItem(Item, 3, #9);
    lblCategoria.Caption := GetItem(Item, 4, #9);
    CategoriaTransito    := GetItem(Item, 5, #9);  {c�digo categor�a}
    EstadoTransito       := GetItem(Item, 6, #9);  {c�digo estado}
    FechaTransito        := StrToFechaHora(GetItem(Item, 7, #9));
    lblPortico.Caption   := GetItem(Item,8,#9);
    EsItaliano           := StrToBool(GetItem(Item, 9, #9)); // SS_1091_CQU_20130516
    lblFechaTTo.Caption  := DateTimeToStr(FechaTransito);
    lblNumCorrCA.Caption := NumCorrCA;
    txtPatente.Text := lblPatente.Caption;
    IndiceCategoria := cbCategoria.Items.IndexOfValue(CategoriaTransito);
    cbCategoria.ItemIndex := IndiceCategoria;
    btnAceptar.Enabled := False;
    ImagenNumCorrCA := StrToInt64(NumCorrCA);
    VerImagenTransito();
end;

procedure TRevalidarTransitoForm.Redibujar;
var
    bmp: TBitMap;
begin
	Screen.Cursor := crHourGlass;

    bmp := TBitMap.create;
    bmp.PixelFormat := pf24bit;
	try
        // Reseteamos los filtros aplicados
        //bmp.Assign(FImagenActual);            // SS_1091_CQU_20130516
        if not EsItaliano then                  // SS_1091_CQU_20130516
            bmp.Assign(FImagenActual)           // SS_1091_CQU_20130516
        else                                    // SS_1091_CQU_20130516
            bmp.Assign(FImagenItalianaActual);  // SS_1091_CQU_20130516

    	// Aplicamos el Sharp
	    if actRemarcarBordes.Checked then begin
 		    Sharp(bmp);
    	end;

		// Aplicamos el Blurr
		if actSuavizarBordes.checked then begin
		    Blurr(bmp);
    	end;

        // Dibujamos la Mascara
        if chkVerMascara.Checked then
            DrawMask(bmp, FPositionPlate[FTipoImagenActual].top - 4);

        // Asignamos la nueva imagen
        ImagenAmpliar.Picture.Assign(bmp);
    finally
        Screen.Cursor := crDefault;
        bmp.Free;
    end;
end;

procedure TRevalidarTransitoForm.actBrightLessExecute(Sender: TObject);
begin
    if not EsItaliano then begin                          // SS_1091_CQU_20130516
        if FImagenActual.Shift > 4 then exit;
        // Si el FShift no esta inicializado lo asignamos
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift := FImagenActual.Shift + 1;
    end;                                                  // SS_1091_CQU_20130516
	Redibujar;
end;

procedure TRevalidarTransitoForm.actBrightPlusExecute(Sender: TObject);
begin
    if not EsItaliano then begin                            // SS_1091_CQU_20130516
        // Si el Shift supero el limite nos vamos
        if FImagenActual.Shift = 0 then exit;
        // Si el FShift no esta inicializado lo asignamos
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift := FImagenActual.Shift - 1;
    end;                                                    // SS_1091_CQU_20130516
	Redibujar;
end;

procedure TRevalidarTransitoForm.actBrightResetExecute(Sender: TObject);
begin
    if not EsItaliano then begin                            // SS_1091_CQU_20130516
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        if FImagenActual.Shift = FShiftOriginal then exit;
        FImagenActual.Shift := FShiftOriginal;
    end;                                                    // SS_1091_CQU_20130516
	Redibujar;
end;

procedure TRevalidarTransitoForm.actRemarcarBordesExecute(Sender: TObject);
begin
	Redibujar;
end;

procedure TRevalidarTransitoForm.actResetImageExecute(Sender: TObject);
begin
	// Volvemos a False todos los filtros seleccionados
	actRemarcarBordes.Checked := False;
	actSuavizarBordes.Checked := False;
    if not EsItaliano then begin                       // SS_1091_CQU_20130516
        // Volvelos el brillo al original
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift    := FShiftOriginal;
    end;                                               // SS_1091_CQU_20130516
    // Volvemos el Zoom al original
    ImagenAmpliar.Scale     := 1;
    // Redibujamos
    Redibujar;
	// Centramos la imagen
    CentrarImagen();
end;

procedure TRevalidarTransitoForm.actSuavizarBordesExecute(Sender: TObject);
begin
	Redibujar;
end;

procedure TRevalidarTransitoForm.actZoomInExecute(Sender: TObject);
begin
	ImagenAmpliar.Scale := ImagenAmpliar.Scale + 0.2;
end;

procedure TRevalidarTransitoForm.actZoomOutExecute(Sender: TObject);
begin
	if ImagenAmpliar.Scale < 0.5 then begin
    	exit;
    end;

    ImagenAmpliar.Scale := ImagenAmpliar.Scale - 0.2;
end;

procedure TRevalidarTransitoForm.actZoomResetExecute(Sender: TObject);
begin
	if ImagenAmpliar.Scale > 4.0 then begin
    	exit;
    end;

    ImagenAmpliar.Scale := 1;
end;

procedure TRevalidarTransitoForm.btnAceptarClick(Sender: TObject);
ResourceString
    LARGO_PATENTE		= 'No ha ingresado la patente o est� incompleta';
    PATENTE_INCORECTA	= 'La patente ingresada es incorrecta';
    CATEGORIA_NOSELEC	= 'No ha seleccionado la Categor�a';

var
  Respuesta, CodigoRetorno :  integer;
begin
    {-----------------------------------
        REV 01-Sept-2009
    -----------------------------------}
    if Length(txtPatente.Text) < 5 then begin
    	MsgBox(LARGO_PATENTE, Caption, MB_OK + MB_ICONEXCLAMATION);
        exit;
    end;

    if (Length(txtPatente.Text) = 5) and (Copy(txtPatente.Text,1,2) <> 'PR') then begin
    	MsgBox(PATENTE_INCORECTA, Caption, MB_OK + MB_ICONEXCLAMATION);
        exit;
    end;

    {----------------------------------
    	FIN REV
    -----------------------------------}
    
    if cbCategoria.ItemIndex < 0 then begin
        MsgBox(CATEGORIA_NOSELEC, Caption, MB_OK + MB_ICONEXCLAMATION);
        exit;
    end;

    spRevalidarTransito.Close;
    spRevalidarTransito.CommandTimeout := 500;
    spRevalidarTransito.Parameters.ParamByName('@NumCorrCA').Value := ImagenNumCorrCA;
    spRevalidarTransito.Parameters.ParamByName('@NuevaPatente').Value := txtPatente.Text;
    spRevalidarTransito.Parameters.ParamByName('@NuevaCategoria').Value := cbCategoria.Value;
    spRevalidarTransito.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
    //Rev 1 DMConnections.BaseCAC.BeginTrans;
    try
    	spRevalidarTransito.ExecProc;
        CodigoRetorno := spRevalidarTransito.Parameters.ParamByName('@RETURN_VALUE').Value;
        if CodigoRetorno < 0 then begin
          //Rev 1 DMConnections.BaseCAC.RollbackTrans();
          DespliegaMensaje(CodigoRetorno);
        end
        else begin
			//Rev 1 DMConnections.BaseCAC.CommitTrans;
        	MsgBox('Tr�nsito Revalidado exitosamente', Caption, MB_OK + MB_ICONEXCLAMATION);
    		SiguienteTransito();
        end;
    except on e:exception do begin
    	    //Rev 1 DMConnections.BaseCAC.RollbackTrans;
	    	{fracaso:  Preguntar si reintenta,
	        	si contin�a con el siguiente o cancela todo el proceso}
            MsgBoxErr('Ocurri� un Error en la Revalidaci�n del Tr�nsito', e.message, Caption, MB_ICONERROR );
	        Respuesta := MsgBox(' �Desea reintentarlo?' +#13#13 +
                            'Si = Reintentar' + #13 + 'No = continuar con el siguiente Tr�nsito' + #13 +
                            'Cancelar = Cancelar todo el proceso', Caption, MB_YESNOCANCEL + MB_ICONEXCLAMATION);
    	    if Respuesta = IDNO then begin {siguiente}
                SiguienteTransito()
	        end
	        else if Respuesta = IDCANCEL then begin {cancelar todo}
	        	Close;
            end;

	    end;                          	
    end; {try}

end;

procedure TRevalidarTransitoForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TRevalidarTransitoForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	if Assigned(FImagenActual) then FImagenActual.Free;
	if Assigned(FJPG12Frontal) then FJPG12Frontal.Free;
	if Assigned(FJPG12Frontal2) then FJPG12Frontal2.Free;
	if Assigned(FJPG12Posterior) then FJPG12Posterior.Free;
	if Assigned(FJPG12Posterior2) then FJPG12Posterior2.Free;
	if Assigned(FImgOverview1) then FImgOverview1.Free;
	if Assigned(FImgOverview2) then FImgOverview2.Free;
	if Assigned(FImgOverview3) then FImgOverview3.Free;
    if Assigned(FFrmImagenOverview) then FFrmImagenOverview.Free;
    // Imagenes Italianas                                                           // SS_1091_CQU_20130516
    if Assigned(FImagenItalianaActual)      then FImagenItalianaActual.Free;        // SS_1091_CQU_20130516
    if Assigned(FJPGItalianoFrontal)        then FJPGItalianoFrontal.Free;          // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoFrontal2)       then FJPGItalianoFrontal2.Free;         // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior)      then FJPGItalianoPosterior.Free;        // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior2)     then FJPGItalianoPosterior2.Free;       // SS_1091_CQU_20130516
end;

procedure TRevalidarTransitoForm.FormShow(Sender: TObject);
begin
	PrimeraVezTimer := True;
    CargarCategoriasVehiculos(DMConnections.BaseCAC, cbCategoria);
	tmerVisualizar.Enabled := True;
    txtPatente.Text := '';
    cbCategoria.ItemIndex := -1;
    lblPatente.Caption := '';
    lblCategoria.Caption := '';
    lblEstado.Caption := '';
    lblNumCorrCA.Caption := '';
    grpRNVM.Visible := False;
end;

{-------------------------------------------------------------------------------
 Revision 1:
    Author : mbecerra
    Date : 08-04-2008
    Description : Condiciones iniciales
-------------------------------------------------------------------------------}
procedure TRevalidarTransitoForm.Inicializar;
var
	S : TSize;
begin
    Caption := txtCaption;
    Position := poScreenCenter;
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);

    //ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS, FImagePath);                                             //SS-377-NDR-20110607
    ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN, FImagePathNFI);                       //SS-377-NDR-20110607
    //if Trim(FImagePath) = '' then begin                                                                                             //SS-377-NDR-20110607
    //	raise Exception.Create('No se encontro el Parametro ' + DIR_IMAGENES_TRANSITOS);                                              //SS-377-NDR-20110607
    //end;                                                                                                                            //SS-377-NDR-20110607
    if Trim(FImagePathNFI) = '' then begin                                                                                            //SS-377-NDR-20110607
    	raise Exception.Create('No se encontro el Parametro ' + DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN);                         //SS-377-NDR-20110607
    end;                                                                                                                              //SS-377-NDR-20110607

    Screen.Cursor := crHourGlass;
    // Inicializamos los controles para la imagen Overview
    FFrmImagenOverview := TfrmImageOverview.create(self);
    FFrmImagenOverview.Parent := self;
    FFrmImagenOverview.Left := Self.ScreenToClient(ImagenAmpliar.ClientToScreen(Point(0, ImagenAmpliar.Left))).X + 2 ;
    FFrmImagenOverview.top  := Self.ScreenToClient(ImagenAmpliar.ClientToScreen(Point(ImagenAmpliar.top, 0))).Y + 2;

    FCodigoConcesionariaNAtiva := SysUtilsCN.QueryGetIntegerValue(DMConnections.BaseCAC, MSG_SQL_NAtiva);   //SS_1147ZZZ_NDR_20150415 SS_1222_NDR_20150406 SS_1222_CQU_20150317
    
    Screen.Cursor := crDefault;
end;

procedure TRevalidarTransitoForm.SeleccionarImagen;
begin
    if FTipoImagenActual = aTipoImagen then exit;

    if assigned(aGraphic) then begin
		Screen.Cursor := crHourGlass;
        try
		  	if aTipoImagen in [tiFrontal, tiPosterior, tiFrontal2, tiPosterior2] then begin
                //FImagenActual.Assign(aGraphic);           // SS_1091_CQU_20130516
                if not EsItaliano then                      // SS_1091_CQU_20130516
				    FImagenActual.Assign(aGraphic)          // SS_1091_CQU_20130516
                else                                        // SS_1091_CQU_20130516
                    FImagenItalianaActual.Assign(aGraphic); // SS_1091_CQU_20130516
				FShiftOriginal := -1;
                FTipoImagenActual := aTipoImagen;
                Redibujar;
                actResetImageExecute(nil);
            end else begin
                FFormOverviewCargado := true;
                FFrmImagenOverview.ShowImageOverview(aGraphic, aTipoImagen);
                FFrmImagenOverview.Show;
            end;

        finally
            Screen.Cursor := crDefault;
		end;
	end;
end;

procedure TRevalidarTransitoForm.tmerVisualizarTimer(Sender: TObject);
begin
	{s�lo usado para invocar al procedimiento VisualizarDatos,
      ya que desde el OnShow no funciona}
    tmerVisualizar.Enabled := False;
    if PrimeraVezTimer then begin
    	PrimeraVezTimer := False;
        tmerVisualizar.Interval := 200;  {aceleramos el despliegue}
    end;

    if lstListaTransitosARevalidar.Count > 0 then begin
    	VisualizaDatos();
    end
    else begin
    	Close;
    end;
end;

procedure TRevalidarTransitoForm.txtPatenteKeyPress(Sender: TObject;
  var Key: Char);
begin
	if not (Key  in [#3, #22, #24, #26, '0'..'9','a'..'z','A'..'Z', Char(VK_BACK)]) then
        Key := #0;
end;

procedure TRevalidarTransitoForm.actCenterToPlateExecute(Sender: TObject);
begin
	ImagenAmpliar.Center;
end;

procedure TRevalidarTransitoForm.actImageOverviewExecute(Sender: TObject);
begin
	if FFrmImagenOverview.Showing then FFrmImagenOverview.Hide
    else if FFormOverviewCargado then FFrmImagenOverview.Show;
end;

procedure TRevalidarTransitoForm.sbThumb0Click(Sender: TObject);
begin
	//SeleccionarImagen(FJPG12Frontal, tiFrontal);          // SS_1091_CQU_20130516
    if not EsItaliano then                                  // SS_1091_CQU_20130516
        SeleccionarImagen(FJPG12Frontal, tiFrontal)         // SS_1091_CQU_20130516
    else                                                    // SS_1091_CQU_20130516
        SeleccionarImagen(FJPGItalianoFrontal, tiFrontal);  // SS_1091_CQU_20130516
end;

procedure TRevalidarTransitoForm.sbThumb1Click(Sender: TObject);
begin
    //SeleccionarImagen(FJPG12Posterior, tiPosterior);          // SS_1091_CQU_20130516
    if not EsItaliano then                                      // SS_1091_CQU_20130516
        SeleccionarImagen(FJPG12Posterior, tiPosterior)         // SS_1091_CQU_20130516
    else                                                        // SS_1091_CQU_20130516
        SeleccionarImagen(FJPGItalianoPosterior, tiPosterior);  // SS_1091_CQU_20130516
end;

procedure TRevalidarTransitoForm.sbThumb2Click(Sender: TObject);
begin
	SeleccionarImagen(FImgOverview1, tiOverview1);
end;

procedure TRevalidarTransitoForm.sbThumb3Click(Sender: TObject);
begin
	SeleccionarImagen(FImgOverview2, tiOverview2);
end;

procedure TRevalidarTransitoForm.sbThumb4Click(Sender: TObject);
begin
	SeleccionarImagen(FImgOverview3, tiOverview3);
end;

procedure TRevalidarTransitoForm.sbThumb5Click(Sender: TObject);
begin
	//SeleccionarImagen(FJPG12Frontal2, tiFrontal2);            // SS_1091_CQU_20130516
    if not EsItaliano then                                      // SS_1091_CQU_20130516
        SeleccionarImagen(FJPG12Frontal2, tiFrontal2)           // SS_1091_CQU_20130516
    else                                                        // SS_1091_CQU_20130516
        SeleccionarImagen(FJPGItalianoFrontal2, tiFrontal2);    // SS_1091_CQU_20130516
end;

procedure TRevalidarTransitoForm.sbThumb6Click(Sender: TObject);
begin
	//SeleccionarImagen(FJPG12Posterior2, tiPosterior2);            // SS_1091_CQU_20130516
    if not EsItaliano then                                          // SS_1091_CQU_20130516
        SeleccionarImagen(FJPG12Posterior2, tiPosterior2)           // SS_1091_CQU_20130516
    else                                                            // SS_1091_CQU_20130516
        SeleccionarImagen(FJPGItalianoPosterior2, tiPosterior2);    // SS_1091_CQU_20130516
end;

procedure TRevalidarTransitoForm.btnCancelarClick(Sender: TObject);
begin
	SiguienteTransito();
end;

{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   25-Noviembre-2013
Description     :   Habilita los Botones F1
Firma           :   SS_1135_CQU_20131030
-----------------------------------------------------------------------------}
procedure TRevalidarTransitoForm.actImagenFrontal1Execute(Sender: TObject);
begin
    if (sbThumb0.Enabled) then begin
        sbThumb0Click(sbThumb0);
        sbThumb0.Down := true;
        Redibujar();
        CentrarImagen;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   25-Noviembre-2013
Description     :   Habilita los Botones F3
Firma           :   SS_1135_CQU_20131030
-----------------------------------------------------------------------------}
procedure TRevalidarTransitoForm.actImagenFrontal2Execute(Sender: TObject);
begin
    if (sbThumb5.Enabled) then begin
        sbThumb5Click(sbThumb5);
        sbThumb5.down := true;
        Redibujar();
        CentrarImagen;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   25-Noviembre-2013
Description     :   Habilita los Botones F2
Firma           :   SS_1135_CQU_20131030
-----------------------------------------------------------------------------}
procedure TRevalidarTransitoForm.actImagenPosterior1Execute(Sender: TObject);
begin
    if (sbThumb1.Enabled) then begin
        sbThumb1Click(sbThumb1);
        sbThumb1.Down := true;
        Redibujar();
        CentrarImagen;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   25-Noviembre-2013
Description     :   Habilita los Botones F2
Firma           :   SS_1135_CQU_20131030
-----------------------------------------------------------------------------}
procedure TRevalidarTransitoForm.actImagenPosterior2Execute(Sender: TObject);
begin
    if (sbThumb6.Enabled) then begin
        sbThumb6Click(sbThumb6);
        sbThumb6.down := true;
        Redibujar();
        CentrarImagen;
    end;
end;

end.
