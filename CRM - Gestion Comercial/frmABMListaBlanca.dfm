object ABMListaBlancaForm: TABMListaBlancaForm
  Left = 0
  Top = 0
  Caption = 'ABMListaBlancaForm'
  ClientHeight = 452
  ClientWidth = 789
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 789
    Height = 49
    Align = alTop
    TabOrder = 0
    object btnNuevo: TSpeedButton
      Left = 31
      Top = 10
      Width = 23
      Height = 22
      Hint = 'Insertar Nuevo registro'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnNuevoClick
    end
    object btnEliminar: TSpeedButton
      Left = 59
      Top = 10
      Width = 23
      Height = 22
      Hint = 'Eliminar registro actual'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        300033FFFFFF3333377739999993333333333777777F3333333F399999933333
        3300377777733333337733333333333333003333333333333377333333333333
        3333333333333333333F333333333333330033333F33333333773333C3333333
        330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
        333333377F33333333FF3333C333333330003333733333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnEliminarClick
    end
    object btnEditar: TSpeedButton
      Left = 100
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Editar registro'
      Enabled = False
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      Visible = False
      OnClick = btnEditarClick
    end
    object btnSalir: TButton
      Left = 688
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnSalirClick
    end
  end
  object gbxEdicion: TGroupBox
    Left = 0
    Top = 368
    Width = 789
    Height = 84
    Align = alBottom
    Caption = 'Edici'#243'n:'
    TabOrder = 1
    DesignSize = (
      789
      84)
    object Label1: TLabel
      Left = 166
      Top = 24
      Width = 44
      Height = 13
      Caption = 'Etiqueta:'
    end
    object Label2: TLabel
      Left = 313
      Top = 24
      Width = 47
      Height = 13
      Caption = 'Categor'#237'a'
    end
    object Label4: TLabel
      Left = 16
      Top = 24
      Width = 42
      Height = 13
      Caption = 'Patente:'
    end
    object Label3: TLabel
      Left = 456
      Top = 24
      Width = 47
      Height = 13
      Caption = 'Novedad:'
    end
    object btnCancelar: TButton
      Left = 688
      Top = 38
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Cancelar'
      TabOrder = 5
      OnClick = btnCancelarClick
    end
    object btnAceptar: TButton
      Left = 592
      Top = 38
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Aceptar'
      TabOrder = 4
      OnClick = btnAceptarClick
    end
    object edtPatente: TEdit
      Left = 16
      Top = 40
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 10
      TabOrder = 0
      Text = 'EDTPATENTE'
      OnChange = edtPatenteChange
      OnExit = edtPatenteExit
      OnKeyDown = edtPatenteKeyDown
    end
    object edtEtiqueta: TEdit
      Left = 166
      Top = 40
      Width = 121
      Height = 21
      Enabled = False
      TabOrder = 1
      Text = 'edtEtiqueta'
    end
    object vcbCategorias: TVariantComboBox
      Left = 313
      Top = 40
      Width = 120
      Height = 21
      Style = vcsDropDownList
      DroppedWidth = 0
      Enabled = False
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
    object vcbNovedad: TVariantComboBox
      Left = 456
      Top = 40
      Width = 105
      Height = 21
      Style = vcsDropDownList
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 3
      OnEnter = vcbNovedadEnter
      OnKeyDown = vcbNovedadKeyDown
      Items = <>
    end
  end
  object dblLista: TDBListEx
    Left = 0
    Top = 49
    Width = 789
    Height = 319
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Manual'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Manual'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Patente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Patente'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 200
        Header.Caption = 'Etiqueta'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Etiqueta'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 80
        Header.Caption = 'Categoria'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Categoria'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Novedad'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NovedadStr'
      end>
    DataSource = dsListaBlanca
    DragReorder = True
    ParentColor = False
    TabOrder = 2
    TabStop = True
  end
  object spObtenerListaBlanca: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerListaBlanca;1'
    Parameters = <>
    Left = 536
    Top = 8
  end
  object dsListaBlanca: TDataSource
    DataSet = spObtenerListaBlanca
    OnDataChange = dsListaBlancaDataChange
    Left = 568
    Top = 8
  end
  object spActualizarRegistroEnListaBlanca: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRegistroEnListaBlanca'
    Parameters = <>
    Left = 624
    Top = 8
  end
end
