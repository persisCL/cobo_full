object FrmCambiarFechaUsoPaseDiario: TFrmCambiarFechaUsoPaseDiario
  Left = 435
  Top = 236
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Cambiar fecha uso Pase Diario'
  ClientHeight = 182
  ClientWidth = 322
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    322
    182)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 10
    Top = 3
    Width = 303
    Height = 131
  end
  object Label1: TLabel
    Left = 29
    Top = 59
    Width = 88
    Height = 13
    Caption = 'Fecha uso anterior'
  end
  object Label3: TLabel
    Left = 30
    Top = 13
    Width = 37
    Height = 13
    Caption = 'Patente'
  end
  object Label2: TLabel
    Left = 29
    Top = 36
    Width = 77
    Height = 13
    Caption = 'N'#250'mero de serie'
  end
  object Label4: TLabel
    Left = 28
    Top = 82
    Width = 90
    Height = 13
    Caption = 'Fecha vencimiento'
  end
  object Label5: TLabel
    Left = 28
    Top = 105
    Width = 98
    Height = 13
    Caption = 'Nueva fecha uso'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edFechaUsoAnterior: TDateEdit
    Left = 131
    Top = 54
    Width = 159
    Height = 21
    AutoSelect = False
    Enabled = False
    ReadOnly = True
    TabOrder = 2
    Date = -693594.000000000000000000
  end
  object btnCancelar: TButton
    Left = 244
    Top = 151
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 6
  end
  object btnAceptar: TButton
    Left = 165
    Top = 151
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 5
    OnClick = btnAceptarClick
  end
  object edPatente: TEdit
    Left = 131
    Top = 8
    Width = 159
    Height = 21
    Enabled = False
    ReadOnly = True
    TabOrder = 0
  end
  object edNumeroSerie: TEdit
    Left = 131
    Top = 31
    Width = 159
    Height = 21
    Enabled = False
    ReadOnly = True
    TabOrder = 1
  end
  object edFechaVencimiento: TDateEdit
    Left = 131
    Top = 77
    Width = 159
    Height = 21
    AutoSelect = False
    Enabled = False
    ReadOnly = True
    TabOrder = 3
    Date = -693594.000000000000000000
  end
  object edNuevaFechaUso: TDateEdit
    Left = 131
    Top = 101
    Width = 159
    Height = 21
    AutoSelect = False
    Color = 16444382
    TabOrder = 4
    Date = -693594.000000000000000000
  end
end
