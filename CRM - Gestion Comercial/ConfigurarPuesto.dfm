object FormConfigurarPuesto: TFormConfigurarPuesto
  Left = 250
  Top = 242
  BorderStyle = bsDialog
  Caption = 'Configuraci'#243'n del Puesto de Trabajo'
  ClientHeight = 122
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object gb_Antena: TGroupBox
    Left = 8
    Top = 16
    Width = 441
    Height = 57
    Caption = ' Antena de TAG '
    TabOrder = 0
    object Label1: TLabel
      Left = 11
      Top = 25
      Width = 49
      Height = 13
      Caption = 'Biblioteca:'
    end
    object txt_bibLIC: TEdit
      Left = 68
      Top = 24
      Width = 269
      Height = 21
      TabOrder = 0
    end
    object btn_setupLIC: TDPSButton
      Left = 342
      Top = 22
      Caption = 'Con&figurar ...'
      TabOrder = 1
      OnClick = btn_setupLICClick
    end
  end
  object btn_Aceptar: TDPSButton
    Left = 294
    Top = 84
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = btn_AceptarClick
  end
  object btn_Cancelar: TDPSButton
    Left = 374
    Top = 84
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
  object LIC: TTagReader
    Active = False
    DLL = 'COMBTECH.DLL'
    ID = 1
    PollingMode = pmSinglethreaded
    Left = 21
    Top = 91
  end
end
