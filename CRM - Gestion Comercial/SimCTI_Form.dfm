object FormSimCTI: TFormSimCTI
  Left = 250
  Top = 208
  BorderStyle = bsDialog
  Caption = 'Simulador de Llamadas CTI'
  ClientHeight = 190
  ClientWidth = 440
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 27
    Top = 88
    Width = 90
    Height = 13
    Caption = 'N'#250'mero de Cliente:'
  end
  object Label2: TLabel
    Left = 24
    Top = 16
    Width = 346
    Height = 13
    Caption = 
      'Este sistema simula el proceso que realiza el IVR al identificar' +
      ' a un cliente'
  end
  object Label3: TLabel
    Left = 24
    Top = 32
    Width = 399
    Height = 13
    Caption = 
      'que realiza una llamada. Este sistema env'#237'a al CAC los datos de ' +
      'la nueva LLAMADA'
  end
  object Label4: TLabel
    Left = 24
    Top = 48
    Width = 198
    Height = 13
    Caption = 'y el CODIGO de CLIENTE que la efectu'#243'.'
  end
  object Label5: TLabel
    Left = 27
    Top = 116
    Width = 88
    Height = 13
    Caption = 'Nombre y Apellido:'
  end
  object CodigoCliente: TNumericEdit
    Left = 136
    Top = 83
    Width = 97
    Height = 21
    MaxLength = 10
    TabOrder = 0
    OnChange = CodigoClienteChange
    Decimals = 0
  end
  object OPButton1: TDPSButton
    Left = 240
    Top = 80
    Caption = '&Buscar'
    TabOrder = 1
    OnClick = OPButton1Click
  end
  object btn_aceptar: TDPSButton
    Left = 264
    Top = 160
    Caption = '&Aceptar'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 3
    OnClick = btn_aceptarClick
  end
  object btn_Cancelar: TDPSButton
    Left = 344
    Top = 160
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
    OnClick = btn_CancelarClick
  end
  object txt_ApellidoNombre: TEdit
    Left = 136
    Top = 112
    Width = 281
    Height = 21
    TabStop = False
    Color = 14732467
    ReadOnly = True
    TabOrder = 2
  end
  object DdeClientConv: TDdeClientConv
    DdeService = 'CAC'
    DdeTopic = 'CTI'
    ConnectMode = ddeManual
    OnClose = DdeClientConvClose
    Left = 16
    Top = 160
    LinkInfo = (
      'Service CAC'
      'Topic CTI')
  end
  object CrearLlamada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'IVR_CrearLlamada;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@CodigoContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@TipoContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@CodigoLlamada'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end>
    Left = 48
    Top = 160
  end
end
