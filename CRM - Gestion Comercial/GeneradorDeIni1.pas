{

Etiqueta    : 20160315 MGO
Descripción : Se utiliza InstallIni en vez de ApplicationIni

}

unit GeneradorDeIni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  StdCtrls, ExtCtrls, UtilProc, Variants, Util,
  DPSControls, DmiCtrls, PeaProcs,main, DB, ADODB, VariantComboBox, DMConnection;

type
  TFormGeneradorIni = class(TForm)
	Label1: TLabel;
	Label2: TLabel;
	Bevel1: TBevel;
    btnAceptar: TDPSButton;
    btnCancelar: TDPSButton;
    Label3: TLabel;
    txt_FuenteSolicitud: TNumericEdit;
    cbPuntosEntrega: TVariantComboBox;
    qryPuntosEntrega: TADOQuery;
    cpPuntosVentas: TVariantComboBox;
    qryPuntosVentas: TADOQuery;
	procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
	{ Private declarations }

  public
	{ Public declarations }
	Function Inicializa: Boolean;
  end;

var
  FormGeneradorIni: TFormGeneradorIni;

implementation

{$R *.DFM}

Function TFormGeneradorIni.Inicializa: Boolean;
resourcestring
 MSG_YACONFIGURADO = 'Archivo de Inicio ya se encuentra configurado.';
var
 APuntoEntrega, APuntoVenta,AFuenteSolicitud : integer;
 i: integer;
begin
	result:=True;
    { INICIO : 20160315 MGO
    APuntoEntrega:= ApplicationIni.ReadInteger('General', 'POS', -1);
    APuntoVenta:= ApplicationIni.ReadInteger('General', 'PuntoVenta', -1);
    AFuenteSolicitud:= ApplicationIni.ReadInteger('General','FuenteSolicitud',-1);
    }
    APuntoEntrega:= InstallIni.ReadInteger('General', 'POS', -1);
    APuntoVenta:= InstallIni.ReadInteger('General', 'PuntoVenta', -1);
    AFuenteSolicitud:= InstallIni.ReadInteger('General','FuenteSolicitud',-1);
    // FIN : 20160315 MGO

    i:= 0;
	cbPuntosEntrega.Items.Add('NINGUNO', 0);
    qryPuntosEntrega.Close;
    qryPuntosEntrega.Open;
    qryPuntosEntrega.First;
    while not qryPuntosEntrega.Eof do begin
        cbPuntosEntrega.Items.Add(qryPuntosEntrega.fieldbyname('Descripcion').AsString, qryPuntosEntrega.fieldbyname('CodigoPuntoEntrega').AsInteger);
        if (qryPuntosEntrega.fieldbyname('CodigoPuntoEntrega').AsInteger = APuntoEntrega) then i := APuntoEntrega;
        qryPuntosEntrega.Next;
    end;

    cbPuntosEntrega.ItemIndex := i;

    if cbPuntosEntrega.ItemIndex > 0  then begin
        i:= 0;
        cpPuntosVentas.Items.Add('Seleccione', 0);
        qryPuntosVentas.Close;
        qryPuntosVentas.Parameters.ParamByName('CodigoPuntoEntrega').Value := APuntoEntrega;
        qryPuntosVentas.Open;
        qryPuntosVentas.First;
        while not qryPuntosVentas.Eof do begin
            cpPuntosVentas.Items.Add(qryPuntosVentas.fieldbyname('Descripcion').AsString, qryPuntosEntrega.fieldbyname('CodigoPuntoVenta').AsInteger);
            if (qryPuntosEntrega.fieldbyname('CodigoPuntoEntrega').AsInteger = APuntoEntrega) then i := APuntoEntrega;
            qryPuntosEntrega.Next;
        end;

        cpPuntosVentas.ItemIndex := i;
    end;


//    if APuntoEntrega > -1 then edtPuntoEntrega.ValueInt := APuntoEntrega;
    if APuntoVenta > -1 then edtPuntoVenta.ValueInt := APuntoVenta;
    if AFuenteSolicitud > -1 then txt_FuenteSolicitud.Value:=AFuenteSolicitud;
end;

procedure TFormGeneradorIni.btnAceptarClick(Sender: TObject);
resourcestring
  MSG_DEBE_COMPLETAR_PE =  'Debe completar Punto Entrega';
  MSG_DEBE_COMPLETAR_PV =  'Debe completar Punto Venta';
  MSG_DEBE_FUENTE = 'Debe completar la Fuente de Solicitud.';
begin
	if not ValidateControls([edtPuntoVenta,cbPuntosEntrega,txt_FuenteSolicitud],
	  [(Trim(edtPuntoVenta.Text) <> ''), cbPuntosEntrega.itemindex >= 0,(trim(txt_FuenteSolicitud.Text)<>'')], self.Caption,
	  [MSG_DEBE_COMPLETAR_PV, MSG_DEBE_COMPLETAR_PE,MSG_DEBE_FUENTE]) then begin
		Exit;
	end;

    { INICIO : 20160315 MGO
    ApplicationIni.WriteInteger('General', 'POS', iif(cbPuntosEntrega.ItemIndex = 0, -1, cbPuntosEntrega.Value));
    ApplicationIni.WriteInteger('General', 'PuntoVenta', edtPuntoVenta.ValueInt);
    ApplicationIni.WriteInteger('General', 'FuenteSolicitud', txt_FuenteSolicitud.ValueInt);
    }
    InstallIni.WriteInteger('General', 'POS', iif(cbPuntosEntrega.ItemIndex = 0, -1, cbPuntosEntrega.Value));
    InstallIni.WriteInteger('General', 'PuntoVenta', edtPuntoVenta.ValueInt);
    InstallIni.WriteInteger('General', 'FuenteSolicitud', txt_FuenteSolicitud.ValueInt);
    // FIN : 20160315 MGO

    MainForm.PuntoEntrega:=cbPuntosEntrega.Value;
    MainForm.PuntoVenta:= edtPuntoVenta.ValueInt;

    close;
end;

procedure TFormGeneradorIni.btnCancelarClick(Sender: TObject);
begin
    close;
end;

end.
