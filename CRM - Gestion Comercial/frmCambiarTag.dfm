object formCambiarTag: TformCambiarTag
  Left = 386
  Top = 182
  BorderStyle = bsDialog
  Caption = 'FormCambiarTag'
  ClientHeight = 477
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 14
    Width = 40
    Height = 13
    Caption = 'Patente:'
  end
  object lbl_Patente: TLabel
    Left = 56
    Top = 14
    Width = 65
    Height = 13
    Caption = 'lbl_Patente'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 33
    Width = 441
    Height = 288
    Caption = 'Televia a Devolver'
    TabOrder = 0
    object Label2: TLabel
      Left = 12
      Top = 28
      Width = 42
      Height = 13
      Caption = 'Etiqueta:'
    end
    object Label3: TLabel
      Left = 12
      Top = 52
      Width = 33
      Height = 13
      Caption = 'Estado'
    end
    object Bevel1: TBevel
      Left = 18
      Top = 81
      Width = 407
      Height = 29
    end
    object Label9: TLabel
      Left = 18
      Top = 116
      Width = 169
      Height = 13
      Caption = 'Motivos seleccionados actualmente'
    end
    object Label10: TLabel
      Left = 25
      Top = 88
      Width = 254
      Height = 13
      Caption = 'Para seleccionar un motivo de facturaci'#243'n haga click '
    end
    object lblSeleccionar: TLabel
      Left = 283
      Top = 88
      Width = 22
      Height = 13
      Cursor = crHandPoint
      Caption = 'ac'#225
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      OnClick = lblSeleccionarClick
    end
    object lblTotalMovimientos: TLabel
      Left = 343
      Top = 267
      Width = 78
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
    end
    object lblTotalMovimientosTxt: TLabel
      Left = 245
      Top = 267
      Width = 98
      Height = 13
      AutoSize = False
      Caption = 'Total Movimientos :'
    end
    object txt_TagDevuelto: TEdit
      Left = 60
      Top = 24
      Width = 145
      Height = 21
      MaxLength = 15
      TabOrder = 0
      OnKeyPress = txt_TagDevueltoKeyPress
    end
    object cb_EstadoTagDevuelto: TVariantComboBox
      Left = 60
      Top = 49
      Width = 145
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cb_EstadoTagDevueltoChange
      Items = <>
    end
    object ck_VehiculoRobado: TCheckBox
      Left = 267
      Top = 62
      Width = 100
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Vehiculo Robado'
      TabOrder = 2
    end
    object dblConceptos: TDBListEx
      Left = 18
      Top = 135
      Width = 407
      Height = 124
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 60
          Header.Caption = 'Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'CodigoConcepto'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 250
          Header.Caption = 'Descripci'#243'n de Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescripcionMotivo'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'Precio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'PrecioEnPesos'
        end>
      DataSource = dsConceptos
      DragReorder = True
      ParentColor = False
      PopupMenu = mnuGrillaConceptos
      TabOrder = 3
      TabStop = True
      OnContextPopup = dblConceptosContextPopup
      OnKeyDown = dblConceptosKeyDown
    end
    object gbModalidadEntregaTelevia: TGroupBox
      Left = 233
      Top = 11
      Width = 177
      Height = 49
      Caption = 'Modalidad de entrega del Telev'#237'a'
      TabOrder = 4
      object lblModalidadEntregaTelevia: TLabel
        Left = 10
        Top = 24
        Width = 158
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'lblModalidadEntregaTelevia'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 436
    Width = 456
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      456
      41)
    object btn_Aceptar: TButton
      Left = 290
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      Enabled = False
      TabOrder = 0
      OnClick = btn_AceptarClick
    end
    object btn_Cancelar: TButton
      Left = 371
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btn_CancelarClick
    end
  end
  object gb_NuevoTag: TGroupBox
    Left = 8
    Top = 328
    Width = 441
    Height = 97
    Caption = 'Televia a Entregar'
    TabOrder = 1
    object Label4: TLabel
      Left = 16
      Top = 28
      Width = 42
      Height = 13
      Caption = 'Etiqueta:'
    end
    object Label5: TLabel
      Left = 39
      Top = 58
      Width = 28
      Height = 13
      Caption = 'Verde'
    end
    object ledVerde: TLed
      Left = 18
      Top = 58
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clGreen
      ColorOff = clBtnFace
    end
    object ledGris: TLed
      Left = 18
      Top = 76
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clGray
      ColorOff = clBtnFace
    end
    object Label6: TLabel
      Left = 39
      Top = 76
      Width = 18
      Height = 13
      Caption = 'Gris'
    end
    object ledAmarilla: TLed
      Left = 141
      Top = 58
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clYellow
      ColorOff = clBtnFace
    end
    object Label7: TLabel
      Left = 162
      Top = 58
      Width = 36
      Height = 13
      Caption = 'Amarilla'
    end
    object Label8: TLabel
      Left = 162
      Top = 76
      Width = 29
      Height = 13
      Caption = 'Negra'
    end
    object ledNegra: TLed
      Left = 141
      Top = 76
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clBlack
      ColorOff = clBtnFace
    end
    object txt_TagEntregar: TEdit
      Left = 64
      Top = 24
      Width = 145
      Height = 21
      Enabled = False
      MaxLength = 15
      TabOrder = 0
      OnChange = txt_TagEntregarChange
      OnKeyPress = txt_TagDevueltoKeyPress
    end
  end
  object ObtenerEstadosConservacionTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerEstadosConservacionTAGs'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEstadoConservacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 176
    Top = 8
  end
  object spLeerDatosTAG: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'LeerDatosTAG'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 145
    Top = 9
  end
  object spObtenerEstadoConservacionTAG: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerEstadoConservacionTAG;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 8
  end
  object cdConceptos: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Seleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionMotivo'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'PrecioOrigen'
        DataType = ftInteger
      end
      item
        Name = 'Moneda'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Cotizacion'
        DataType = ftFloat
      end
      item
        Name = 'PrecioEnPesos'
        DataType = ftInteger
      end
      item
        Name = 'Comentario'
        DataType = ftString
        Size = 255
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 56
    Top = 216
    Data = {
      E40000009619E0BD010000001800000008000000000003000000E4000C53656C
      656363696F6E61646F02000300000000000E436F6469676F436F6E636570746F
      0400010000000000114465736372697063696F6E4D6F7469766F020049000000
      010005574944544802000200FF000C50726563696F4F726967656E0400010000
      000000064D6F6E656461020049000000010005574944544802000200FF000A43
      6F74697A6163696F6E08000400000000000D50726563696F456E5065736F7304
      000100000000000A436F6D656E746172696F0200490000000100055749445448
      02000200FF000000}
  end
  object dsConceptos: TDataSource
    DataSet = cdConceptos
    Left = 96
    Top = 216
  end
  object mnuGrillaConceptos: TPopupMenu
    Left = 144
    Top = 216
    object mnuEliminarConcepto: TMenuItem
      Caption = 'Eliminar Concepto'
      OnClick = mnuEliminarConceptoClick
    end
    object AgregarConceptos1: TMenuItem
      Caption = 'Agregar Conceptos'
      OnClick = AgregarConceptos1Click
    end
  end
  object spValidarSiTagTieneGarantia: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ValidarSiTagTieneGarantia'
    Parameters = <>
    Left = 264
    Top = 8
  end
end
