unit frmCrearCampannas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilProc, StdCtrls, Buttons, ExtCtrls, DB, ADODB, Spin,
  VariantComboBox, ComCtrls, Validate, DateEdit, ListBoxEx, DBListEx;

type
  TFormCrearCampannas = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cbMotivoCampanna: TVariantComboBox;
    spObtenerMotivosCampannas: TADOStoredProc;
    pnlCampannas: TPanel;
    grpRechazos: TGroupBox;
    grpMorosos: TGroupBox;
    grpPosiblesInfractores: TGroupBox;
    Label2: TLabel;
    cb_PagoAutomatico: TVariantComboBox;
    Label3: TLabel;
    seCantMinimaRechazosConsecutivos: TSpinEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    seTiempoMorosidad: TSpinEdit;
    Label8: TLabel;
    seCantidadFacturasVencidas: TSpinEdit;
    txtTotalFacturasVencidas: TEdit;
    txtImporteFactura: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    seDiasInfraccion: TSpinEdit;
    Label13: TLabel;
    Label14: TLabel;
    txtImporteInfraccion: TEdit;
    grpArbitrario: TGroupBox;
    grpTagsDefectuosos: TGroupBox;
    rgTagsDefectuosos: TRadioGroup;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    cbConcesionarias: TVariantComboBox;
    cbComunas: TVariantComboBox;
    seCantidadTAGs: TSpinEdit;
    txtValorFactura: TEdit;
    Label19: TLabel;
    spObtenerConcesionarias: TADOStoredProc;
    spObtenerComunasChileRegion: TADOStoredProc;
    Panel3: TPanel;
    btnCrearCampannaInterna: TBitBtn;
    btnCrearCampannaExterna: TBitBtn;
    btnCancelar: TBitBtn;
    pnlDetalleCampanna: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    lblVigencia: TLabel;
    Label24: TLabel;
    btnSalir: TBitBtn;
    edtDescripcionCampanna: TEdit;
    cbbMedioContacto: TComboBox;
    cbbPrioridad: TComboBox;
    cbbAreaAsignada: TVariantComboBox;
    spObtenerAreasAtencionDeCasos: TADOStoredProc;
    spGuardarCampannas: TADOStoredProc;
    txtInicioVigencia: TDateEdit;
    spObtenerDatosCampannaExterna: TADOStoredProc;
    dlgSaveCampannaExterna: TSaveDialog;
    spObtenerTiposDefectosTAGs: TADOStoredProc;
    spEliminarCampanna: TADOStoredProc;
    lblVigenciaAl: TLabel;
    txtTerminoVigencia: TDateEdit;
    rgTipoCampanna: TRadioGroup;
    Label26: TLabel;
    cbRegiones: TVariantComboBox;
    spObtenerRegionesComunasChile: TADOStoredProc;
    rgArbitrarioSuscripcion: TRadioGroup;
    Label27: TLabel;
    cbbGrupoFacturacion: TVariantComboBox;
    spObtenerGruposFacturacion: TADOStoredProc;
    chkRecursiva: TCheckBox;
    seDiaEnvio: TSpinEdit;
    LabelDiaEnvio: TLabel;
    btnSeleccionarPlantilla: TButton;
    spObtenerCantidadClientesCampanna: TADOStoredProc;
    procedure cbMotivoCampannaChange(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure txtImporteFacturaKeyPress(Sender: TObject; var Key: Char);
    procedure btnCrearCampannaInternaClick(Sender: TObject);
    procedure btnCrearCampannaExternaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure rgTipoCampannaClick(Sender: TObject);
    procedure cbRegionesChange(Sender: TObject);
    procedure chkRecursivaClick(Sender: TObject);
    procedure cbbMedioContactoChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure btnSeleccionarPlantillaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  	CodigoPlantilla: Integer;
	function ObtenerMotivosCampannas(): Boolean;
    function CargarMediosPagoAutomatico(): Boolean;
    function CargarGruposFacturacion(): Boolean;
    function CargarTiposDefectosTAGs(): Boolean;
	function CargarConcesionarias(): Boolean;
	function CargarRegiones(): Boolean;
	function CargarComunas(CodigoRegion: String): Boolean;
    function CargarPrioridades(): Boolean;
    function CargarMediosContacto(): Boolean;
    function CargarAreasAtencion(): Boolean;
    function ValidaCampos(var strError: String): Boolean;
    function CrearCampanna(TipoCampanna: Integer; var strCodCampanna, strError: String): Boolean;
    procedure ActivaCampannaRechazos;
    procedure ActivaCampannaMorosos;
    procedure ActivaCampannaInfractores;
    procedure ActivaCampannaTagsDefectuosos;
    procedure ActivaCampannaArbitrario;
    procedure pLimpiarDatos();
  public
    { Public declarations }
	function Inicializar(MDIChild: Boolean): Boolean;
  end;

var
  FormCrearCampannas: TFormCrearCampannas;

const
	COD_TIPO_CAMPANNA_INTERNA		= 1;
    COD_TIPO_CAMPANNA_EXTERNA		= 2;
	ITEM_MOTIVO_ARBITRARIO			= 0;
	ITEM_MOTIVO_MOROSOS				= 1;
	ITEM_MOTIVO_INFRACTORES			= 2;
	ITEM_MOTIVO_RECHAZOS			= 3;
	ITEM_MOTIVO_TAGS_DEFECTUOSOSO	= 4;

	ITEM_MEDIO_PAGO_PAT				= 'PAT';
    ITEM_MEDIO_PAGO_PAC				= 'PAC';
    ITEM_MEDIO_PAGO_TODOS			= 'Todos';

	ITEM_CONTACTO_FONO				= 'Tel�fono';
    ITEM_CONTACTO_EMAIL				= 'Email';
    ITEM_CONTACTO_CARTA				= 'Carta';

	ITEM_PRIORIDAD_ALTA				= 'Alta';
    ITEM_PRIORIDAD_MEDIA			= 'Media';
    ITEM_PRIORIDAD_BAJA				= 'Baja';

resourcestring
	CAPTION_CAMPANNAS	= 'Crear Campa�as';

implementation

uses
	DMConnection, PeaTypes, Util, ExportarToExcel, FrmSeleccionarPlantillaEMail;

{$R *.dfm}

function TFormCrearCampannas.Inicializar(MDIChild: Boolean): Boolean;
Var S: TSize;
begin
	Result := True;
	try
		if MDIChild then begin
			S := GetFormClientSize(Application.MainForm);
			SetBounds(0, 0, S.cx, S.cy);
		end else begin
			FormStyle := fsNormal;
			Visible := False;
		end;

        txtInicioVigencia.Date	:= Now;
        txtTerminoVigencia.Date	:= Now;

        if not ObtenerMotivosCampannas() then
        	Exit;

        if not CargarMediosPagoAutomatico() then
        	Exit;

        if not CargarGruposFacturacion() then
        	Exit;

        if not CargarTiposDefectosTAGs() then
        	Exit;

        if not CargarConcesionarias() then
        	Exit;

        if not CargarRegiones() then
        	Exit;
            
        if not CargarPrioridades() then
        	Exit;

        if not CargarMediosContacto() then
        	Exit;

        if not CargarAreasAtencion() then
        	Exit;
        
	except
		Result := False;
	end;
end;

function TFormCrearCampannas.ObtenerMotivosCampannas(): Boolean;
resourcestring
	MSG_ERROR_OBTENER_MOTIVOS = 'No se pudo obtener los motivos de Campa�as';
begin
	Result := False;
    try
        with spObtenerMotivosCampannas do begin
            if Active then
                Close;
            Open;
            while not Eof do begin
                cbMotivoCampanna.Items.Add(FieldByName('DescripcionMotivoCampanna').AsString, FieldByName('CodigoMotivoCampanna').AsString);
                Next;
            end;
            Close;
        end;
    except
    	MsgBox(MSG_ERROR_OBTENER_MOTIVOS, Self.Caption, MB_ICONSTOP);
    	Exit;
    end;
    Result := True;
end;

function TFormCrearCampannas.CargarMediosPagoAutomatico(): Boolean;
begin
    Result := False;
    try
        cb_PagoAutomatico.Items.Clear;
        cb_PagoAutomatico.Items.Add(ITEM_MEDIO_PAGO_PAT, 1);
        cb_PagoAutomatico.Items.Add(ITEM_MEDIO_PAGO_PAC, 2);
        cb_PagoAutomatico.Items.Add(ITEM_MEDIO_PAGO_TODOS, 3);
    except
    	Exit;
    end;
	Result := True;
end;

function TFormCrearCampannas.CargarGruposFacturacion(): Boolean;
begin
	Result := False;
    try
    	cbbGrupoFacturacion.Items.Add('Todos los Grupos', -1);
        with spObtenerGruposFacturacion, Parameters do begin
            if Active then
                Close;
            Open;
        	while not Eof do begin
            	cbbGrupoFacturacion.Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoGrupoFacturacion').AsString);
            	Next;
            end;
            Close;
        end;
        cbbGrupoFacturacion.ItemIndex	:= 0;
    except
    	on E:Exception do begin
        	MsgBox(E.Message, Self.Caption, MB_ICONSTOP);
			Exit;
        end;
    end;
	Result := True;
end;

function TFormCrearCampannas.CargarTiposDefectosTAGs(): Boolean;
var
	ContadorItems: Integer;
const
	MAX_ITEMS_UNA_COLUMNA = 4;
begin
    Result := False;
    try
    	rgTagsDefectuosos.Items.Clear;
        ContadorItems	:= 0;
    	with spObtenerTiposDefectosTAGs, Parameters do begin
            if Active then
            	Close;
            Parameters.Refresh;
            Open;
            while not Eof do begin
            	rgTagsDefectuosos.Items.Add(FieldByName('DescripcionDefectoTAG').AsString);
                Inc(ContadorItems);
            	Next;
            end;
            Close;
        end;
        if ContadorItems <= MAX_ITEMS_UNA_COLUMNA then
        	rgTagsDefectuosos.Columns	:= 1
        else
        	rgTagsDefectuosos.Columns	:= 2;
    except
		Exit;
    end;
    Result := True;
end;

function TFormCrearCampannas.CargarPrioridades(): Boolean;
begin
    Result := False;
    try
        cbbPrioridad.Items.Clear;
        cbbPrioridad.Items.Add(ITEM_PRIORIDAD_ALTA);
        cbbPrioridad.Items.Add(ITEM_PRIORIDAD_MEDIA);
        cbbPrioridad.Items.Add(ITEM_PRIORIDAD_BAJA);
    except
    	Exit;
    end;
	Result := True;
end;

function TFormCrearCampannas.CargarMediosContacto(): Boolean;
begin
    Result := False;
    try
        cbbMedioContacto.Items.Clear;
        cbbMedioContacto.Items.Add(ITEM_CONTACTO_FONO);
        cbbMedioContacto.Items.Add(ITEM_CONTACTO_EMAIL);
        cbbMedioContacto.Items.Add(ITEM_CONTACTO_CARTA);
    except
    	Exit;
    end;
	Result := True;
end;

function TFormCrearCampannas.CargarConcesionarias(): Boolean;
begin
    Result := False;
    try
    	cbConcesionarias.Items.Add('Todas las Concesionarias', -1);
        with spObtenerConcesionarias, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@FiltraPorConcepto').Value := 0;
            Open;
        	while not Eof do begin
            	cbConcesionarias.Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoConcesionaria').AsString);
            	Next;
            end;
            Close;
        end;
        cbConcesionarias.ItemIndex	:= 0;
    except
    	on E:Exception do begin
        	MsgBox(E.Message, Self.Caption, MB_ICONSTOP);
			Exit;
        end;
    end;
	Result := True;
end;

function TFormCrearCampannas.CargarRegiones(): Boolean;
begin
    Result := False;
    try
        with spObtenerRegionesComunasChile, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            Open;
        	while not Eof do begin
				cbRegiones.Items.Add(FieldByName('Region').AsString, FieldByName('CodigoRegion').AsString);
            	Next;
            end;
            Close;
        end;
    except
    	on E:Exception do begin
        	MsgBox(E.Message, Self.Caption, MB_ICONSTOP);
			Exit;
        end;
    end;
    Result := True;
end;

function TFormCrearCampannas.CargarComunas(CodigoRegion: String): Boolean;
begin
    Result := False;
    try
    	cbComunas.Items.Clear;
        with spObtenerComunasChileRegion, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@CodigoRegion').Value := IIf(CodigoRegion = '', null, CodigoRegion);
            Open;
        	while not Eof do begin
				cbComunas.Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoComuna').AsString);
            	Next;
            end;
            Close;
        end;
    except
    	on E:Exception do begin
        	MsgBox(E.Message, Self.Caption, MB_ICONSTOP);
			Exit;
        end;
    end;
    Result := True;
end;

function TFormCrearCampannas.CargarAreasAtencion(): Boolean;
begin
    Result := False;
    try
        with spObtenerAreasAtencionDeCasos, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            Open;
        	while not Eof do begin
            	if FieldByName('NombreAreaAtencionDeCaso').AsString = 'Atenci�n WEB' then begin
                    Next;
                    Continue;
                end;

				cbbAreaAsignada.Items.Add(FieldByName('NombreAreaAtencionDeCaso').AsString, FieldByName('CodigoAreaAtencionDeCaso').AsString);
            	Next;
            end;
            Close;
        end;
    except
    	on E:Exception do begin
        	MsgBox(E.Message, Self.Caption, MB_ICONSTOP);
			Exit;
        end;
    end;
    Result := True;
end;

procedure TFormCrearCampannas.cbbMedioContactoChange(Sender: TObject);
begin
	if rgTipoCampanna.ItemIndex = 0 then begin

        if (cbbMedioContacto.Text = ITEM_CONTACTO_FONO) then begin
        	lblVigencia.Caption 			:= 'Vigencia Campa�a del';
            lblVigenciaAl.Visible			:= True;
            txtTerminoVigencia.Visible		:= True;
            chkRecursiva.Checked			:= False;
            chkRecursiva.Visible			:= False;
            LabelDiaEnvio.Visible			:= False;
            seDiaEnvio.Value				:= 1;
            seDiaEnvio.Visible				:= False;
            Label22.Visible				    := True;
            Label24.Visible				    := True;
            cbbPrioridad.Visible			:= True;
            cbbAreaAsignada.Visible			:= True;
            btnSeleccionarPlantilla.Visible	:= False;
        end
        else begin
        	lblVigencia.Caption 			:= 'Fecha comprometida';
            lblVigenciaAl.Visible			:= False;
            txtTerminoVigencia.Visible		:= False;
            chkRecursiva.Visible			:= True;
            chkRecursiva.Checked			:= False;
            seDiaEnvio.Value				:= 1;
            Label22.Visible				    := False;
            Label24.Visible				    := False;
            cbbPrioridad.Visible			:= False;
            cbbAreaAsignada.Visible			:= False;
        	btnSeleccionarPlantilla.Visible	:= True;
        end;
    end;
end;

procedure TFormCrearCampannas.cbMotivoCampannaChange(Sender: TObject);
begin
	pLimpiarDatos();
	pnlDetalleCampanna.Visible	:= False;
	case cbMotivoCampanna.ItemIndex of
        ITEM_MOTIVO_ARBITRARIO			: ActivaCampannaArbitrario;
        ITEM_MOTIVO_MOROSOS				: ActivaCampannaMorosos;
        ITEM_MOTIVO_INFRACTORES			: ActivaCampannaInfractores;
        ITEM_MOTIVO_RECHAZOS			: ActivaCampannaRechazos;
        ITEM_MOTIVO_TAGS_DEFECTUOSOSO	: ActivaCampannaTagsDefectuosos;
    end;
    pnlDetalleCampanna.Visible			:= True;
    pnlCampannas.Align					:= alTop;
end;

procedure TFormCrearCampannas.cbRegionesChange(Sender: TObject);
begin
    if not CargarComunas(cbRegiones.Value) then
        Exit;
end;

procedure TFormCrearCampannas.chkRecursivaClick(Sender: TObject);
begin
	if chkRecursiva.Checked then begin
    	LabelDiaEnvio.Visible		:= True;
        seDiaEnvio.Visible			:= True;
        lblVigencia.Caption			:= 'Vigencia Campa�a del';
        lblVigenciaAl.Visible		:= True;
        txtTerminoVigencia.Visible	:= True;
    end
    else begin
    	LabelDiaEnvio.Visible		:= False;
        seDiaEnvio.Visible			:= False;
        lblVigencia.Caption			:= 'Fecha comprometida';
        lblVigenciaAl.Visible		:= False;
        txtTerminoVigencia.Visible	:= False;
    end;

end;

procedure TFormCrearCampannas.ActivaCampannaArbitrario;
begin
	pnlCampannas.Visible			:= True;
	pnlCampannas.Height				:= 150;

    cbConcesionarias.Enabled	    := True;
    cbComunas.Enabled			    := True;
    seCantidadTAGs.Enabled		    := True;
    txtValorFactura.Enabled		    := True;

    grpArbitrario.Visible			:= True;
	grpRechazos.Visible				:= False;
    grpMorosos.Visible				:= False;
    grpPosiblesInfractores.Visible	:= False;
    grpTagsDefectuosos.Visible		:= False;
end;

procedure TFormCrearCampannas.ActivaCampannaRechazos;
begin
	pnlCampannas.Visible			:= True;
	pnlCampannas.Height				:= 55;
    cb_PagoAutomatico.Enabled		:= True;
    seCantMinimaRechazosConsecutivos.Enabled	:= True;
	grpRechazos.Visible				:= True;
	grpRechazos.Visible				:= True;
    grpMorosos.Visible				:= False;
    grpPosiblesInfractores.Visible	:= False;
    grpTagsDefectuosos.Visible		:= False;
    grpArbitrario.Visible			:= False;
end;

procedure TFormCrearCampannas.ActivaCampannaMorosos;
begin
	pnlCampannas.Visible			:= True;
	pnlCampannas.Height				:= 85;

    seTiempoMorosidad.Enabled			:= True;
    txtImporteFactura.Enabled			:= True;
    txtTotalFacturasVencidas.Enabled	:= True;
    seCantidadFacturasVencidas.Enabled	:= True;

    grpMorosos.Visible				:= True;
	grpRechazos.Visible				:= False;
    grpPosiblesInfractores.Visible	:= False;
    grpTagsDefectuosos.Visible		:= False;
    grpArbitrario.Visible			:= False;
end;

procedure TFormCrearCampannas.ActivaCampannaInfractores;
begin
	pnlCampannas.Visible			:= True;
	pnlCampannas.Height				:= 85;

    seDiasInfraccion.Enabled		:= True;
    txtImporteInfraccion.Enabled	:= True;

    grpPosiblesInfractores.Visible	:= True;
	grpRechazos.Visible				:= False;
    grpMorosos.Visible				:= False;
    grpTagsDefectuosos.Visible		:= False;
    grpArbitrario.Visible			:= False;
end;

procedure TFormCrearCampannas.ActivaCampannaTagsDefectuosos;
begin
	pnlCampannas.Visible			:= True;
	pnlCampannas.Height				:= 115;
    rgTagsDefectuosos.Enabled		:= True;
    grpTagsDefectuosos.Visible		:= True;
	grpRechazos.Visible				:= False;
    grpMorosos.Visible				:= False;
    grpPosiblesInfractores.Visible	:= False;
    grpArbitrario.Visible			:= False;
end;

procedure TFormCrearCampannas.btnCancelarClick(Sender: TObject);
resourcestring
	MSG_CANCELAR	= '�Est� seguro que desea cancelar? Se perder�n los cambios';
begin
    if MsgBox(MSG_CANCELAR, CAPTION_CAMPANNAS, MB_ICONQUESTION or MB_YESNO) = IDYES then begin
    	pnlCampannas.Visible		:= False;
    	pnlDetalleCampanna.Visible	:= False;
		cbMotivoCampanna.Enabled	:= True;
        pLimpiarDatos();
    end;
end;

procedure TFormCrearCampannas.pLimpiarDatos();
begin
    edtDescripcionCampanna.Clear;
    cbbMedioContacto.ItemIndex	        := -1;
    cbbPrioridad.ItemIndex		        := -1;
    txtInicioVigencia.Date		        := Now;
    txtTerminoVigencia.Date		        := Now;
    cbbAreaAsignada.ItemIndex	        := -1;

    cb_PagoAutomatico.ItemIndex			:= -1;
    seCantMinimaRechazosConsecutivos.Value := 1;

    seTiempoMorosidad.Value			    := 1;
    txtImporteFactura.Text			    := '';
    txtTotalFacturasVencidas.Text	    := '';
    seCantidadFacturasVencidas.Value    := 1;
    cbbGrupoFacturacion.ItemIndex		:= -1;

    seDiasInfraccion.Value			    := 1;
    txtImporteInfraccion.Text		    := '';

    rgTagsDefectuosos.ItemIndex		    := -1;

    cbConcesionarias.ItemIndex		    := -1;
    cbComunas.ItemIndex				    := -1;
    seCantidadTAGs.Value			    := 0;
    txtValorFactura.Text			    := '';
    rgArbitrarioSuscripcion.ItemIndex	:= -1;
    
end;

procedure TFormCrearCampannas.rgTipoCampannaClick(Sender: TObject);
begin
	if rgTipoCampanna.ItemIndex = 0 then begin//Campa�a Interna
    	btnCrearCampannaExterna.Visible	:= False;
    	btnCrearCampannaInterna.Visible	:= True;
        chkRecursiva.Visible			:= True;
        chkRecursiva.Checked			:= False;
        seDiaEnvio.Value				:= 1;
    end
    else if rgTipoCampanna.ItemIndex = 1 then begin
    	btnCrearCampannaInterna.Visible	:= False;
    	btnCrearCampannaExterna.Visible	:= True;
        chkRecursiva.Checked			:= False;
        chkRecursiva.Visible			:= False;
        LabelDiaEnvio.Visible			:= False;
        seDiaEnvio.Value				:= 1;
        seDiaEnvio.Visible				:= False;
        lblVigencia.Caption			    := 'Vigencia Campa�a del';
        lblVigenciaAl.Visible		    := True;
        txtTerminoVigencia.Visible	    := True;
    end;
end;

procedure TFormCrearCampannas.txtImporteFacturaKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9',#8, #3, #22]) then
		Key := #0;
end;

procedure TFormCrearCampannas.btnCrearCampannaInternaClick(Sender: TObject);
resourcestring
    MSG_ERROR_CREAR_CAMPANNA		= 'Error al crear la Campa�a Interna';
    MSG_CAMPANNA_CREADA_OK			= 'La Campa�a Externa fue creada exitosamente con el N� %s y abarca %s clientes';
    MSG_NO_EXISTEN_DATOS			= 'No hay datos para los par�metros ingresados. No se cre� la Campa�a';
const
	USR_ORDEN_SERVICIO				= 'MKT';
var
	strErrorMsg, CodigoCampannaCreada, CantidadClientes: String;
begin
	if not ValidaCampos(strErrorMsg) then begin
    	MsgBox(strErrorMsg, Self.Caption, MB_ICONSTOP);
        Exit;
    end;

    if not CrearCampanna(COD_TIPO_CAMPANNA_INTERNA, CodigoCampannaCreada, strErrorMsg) then
        MsgBox(MSG_ERROR_CREAR_CAMPANNA +#13+ strErrorMsg, Self.Caption, MB_ICONSTOP)
    else begin

        with spObtenerCantidadClientesCampanna, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@CodigoCampanna').Value	:= CodigoCampannaCreada;
            Open;
            CantidadClientes	:= ParamByName('@CantidadClientes').Value;
            if EOF then begin
                Close;
                spEliminarCampanna.Parameters.Refresh;
                spEliminarCampanna.Parameters.ParamByName('@CodigoCampanna').Value	:= CodigoCampannaCreada;
                spEliminarCampanna.ExecProc;
                MsgBox(MSG_NO_EXISTEN_DATOS, Self.Caption, MB_ICONSTOP);
                Exit;
            end;
        end;
		MsgBox(Format(MSG_CAMPANNA_CREADA_OK, [CodigoCampannaCreada, CantidadClientes]), Self.Caption, MB_ICONINFORMATION);
    end;
end;

procedure TFormCrearCampannas.btnCrearCampannaExternaClick(Sender: TObject);
resourcestring
    MSG_ERROR_CREAR_CAMPANNA		= 'Error al crear la Campa�a Externa';
    MSG_NO_EXISTEN_DATOS			= 'No hay datos para los par�metros ingresados. No se puede crear archivo';
    MSG_ERROR_CREAR_CSV				= 'Error al crear archivo excel. No se ha creado la Campa�a';
    MSG_CAMPANNA_CREADA_OK			= 'La Campa�a Externa fue creada exitosamente con el N� %s y abarca %s clientes';
var
	strErrorMsg, CodigoCampannaCreada, CantidadClientes: string;
begin
	if not ValidaCampos(strErrorMsg) then begin
    	MsgBox(strErrorMsg, Self.Caption, MB_ICONSTOP);
        Exit;
    end;

    try
        if not CrearCampanna(COD_TIPO_CAMPANNA_EXTERNA, CodigoCampannaCreada, strErrorMsg) then
            MsgBox(MSG_ERROR_CREAR_CAMPANNA +#13+ strErrorMsg, Self.Caption, MB_ICONSTOP)
        else begin
            with spObtenerDatosCampannaExterna, Parameters do begin
                if Active then
                    Close;
                Parameters.Refresh;
                ParamByName('@CodigoCampanna').Value	:= CodigoCampannaCreada;
                ExecProc;
                CantidadClientes	:= ParamByName('@CantidadClientes').Value;
                if EOF then begin
                    Close;
                    spEliminarCampanna.Parameters.Refresh;
                    spEliminarCampanna.Parameters.ParamByName('@CodigoCampanna').Value	:= CodigoCampannaCreada;
                    spEliminarCampanna.ExecProc;
                    MsgBox(MSG_NO_EXISTEN_DATOS, Self.Caption, MB_ICONSTOP);
                    Exit;
                end;
            end;
        
            if not SaveToCSV(spObtenerDatosCampannaExterna, 'CampannaExterna') then begin
                spEliminarCampanna.Parameters.Refresh;
                spEliminarCampanna.Parameters.ParamByName('@CodigoCampanna').Value	:= CodigoCampannaCreada;
                spEliminarCampanna.ExecProc;
                MsgBox(MSG_ERROR_CREAR_CSV, Self.Caption, MB_ICONSTOP);
                Exit;
            end
            else
                MsgBox(Format(MSG_CAMPANNA_CREADA_OK, [CodigoCampannaCreada, CantidadClientes]), Self.Caption, MB_ICONINFORMATION);
        end;
    except
		on E:Exception do begin
        	MsgBox(MSG_ERROR_CREAR_CAMPANNA + #13+ E.Message, Self.Caption, MB_ICONSTOP);
        end;
    end;
    pLimpiarDatos();
	cbMotivoCampanna.ItemIndex := -1;
end;

function TFormCrearCampannas.ValidaCampos(var strError: String): Boolean;
resourcestring
	MSG_ERROR_TIPO_CAMPANNA						= 'Debe indicar Tipo de Campa�a';
	MSG_ERROR_MOTIVO_CAMPANNA 					= 'Debe seleccionar motivo de Campa�a';

	MSG_ERROR_MEDIO_PAGO						= 'Debe ingresar un Medio de Pago';
    MSG_ERROR_CANT_RECHAZOS						= 'La cantidad m�nima de rechazos consecutivos debe ser mayor a cero';

	MSG_ERROR_TIEMPO_MOROSIDAD_FACTURA			= 'El tiempo de morosidad de factura debe ser mayor a cero.';
    MSG_ERROR_IMPORTE_FACTURA					= 'Debe ingresar un valor (mayor a cero) para el importe de factura.';
    MSG_ERROR_IMPORTE_TOTAL_FACTURAS_VENC		= 'Debe ingresar un valor (mayor a cero) para el importe total de facturas vencidas.';
    MSG_ERROR_CANTIDAD_FACTURAS_VENCIDAS		= 'La cantidad de facturas vencidas debe ser mayor a cero.';

	MSG_ERROR_CANTIDAD_DIAS_INFRACCION			= 'La cantidad de d�as de infracci�n ser mayor a cero.';
    MSG_ERROR_IMPORTE_INFRACCION   				= 'Debe ingresar un valor (mayor a cero) para el importe de la infracci�n.';

	MSG_ERROR_TIPO_DEFECTO_TAG					= 'Debe seleccionar un tipo de defecto de TAG';

    MSG_ARBITRARIO_NULL							= 'Si selecciona Motivo Arbitrario, debe ingresar alg�n dato para esta campa�a';
	MSG_ERROR_CONCESIONARIA						= 'Debe seleccionar Concesionaria';
    MSG_ERROR_CANTIDAD_TAGS						= 'La cantidad de TAGs ser mayor a cero.';
    MSG_ERROR_VALOR_FACTURA						= 'Debe ingresar un valor (mayor a cero) para el valor de la factura.';

	MSG_ERROR_DESCRIPCION_CAMPANNA				= 'Debe ingresar la descripci�n de la campa�a';
    MSG_ERROR_MEDIO_CONTACTO		            = 'Debe seleccionar el medio de contacto';
    MSG_ERROR_INICIO_VIGENCIA_MENOR_ACTUAL      = 'La fecha inicio de vigencia no puede ser menor a la fecha actual';
    MSG_ERROR_TERMINO_MENOR_INICIO_VIGENCIA		= 'La fecha de t�rmino no puede ser mayor a la fecha de inicio de vigencia ';
    MSG_ERROR_PRIORIDAD				            = 'Debe seleccionar prioridad';
    MSG_ERROR_AREA_ASIGNADA			            = 'Debe seleccionar un �rea asignada';
begin
    Result := False;

    if rgTipoCampanna.ItemIndex < 0 then begin
    	strError := MSG_ERROR_TIPO_CAMPANNA;
        rgTipoCampanna.SetFocus;
        Exit;
    end;

    if cbMotivoCampanna.ItemIndex < 0 then begin
    	strError := MSG_ERROR_MOTIVO_CAMPANNA;
        cbMotivoCampanna.SetFocus;
        Exit;
    end;

    //Campa�a Motivo Rechazos de Pagos Autom�ticos
    if grpRechazos.Visible then begin
        if cb_PagoAutomatico.ItemIndex = -1 then begin
            strError := MSG_ERROR_MEDIO_PAGO;
            grpRechazos.SetFocus;
            Exit;
        end;

        if seCantMinimaRechazosConsecutivos.Value < 1 then begin
            strError := MSG_ERROR_CANT_RECHAZOS;
            seCantMinimaRechazosConsecutivos.SetFocus;
            Exit;
        end;
    end
    //Campa�a Motivo Morosos
    else if grpMorosos.Visible then begin
        if seTiempoMorosidad.Value < 1 then begin
            strError := MSG_ERROR_TIEMPO_MOROSIDAD_FACTURA;
            seTiempoMorosidad.SetFocus;
            Exit;
        end;
    
        if (Trim(TrimLeft(TrimRight(txtImporteFactura.Text))) = '') or (StrToInt(Trim(TrimLeft(TrimRight(txtImporteFactura.Text)))) < 0) then begin
            strError := MSG_ERROR_IMPORTE_FACTURA;
            txtImporteFactura.SetFocus;
            Exit;
        end;
    
        if (Trim(TrimLeft(TrimRight(txtTotalFacturasVencidas.Text))) = '') or (StrToInt(Trim(TrimLeft(TrimRight(txtTotalFacturasVencidas.Text)))) < 0) then begin
            strError := MSG_ERROR_IMPORTE_TOTAL_FACTURAS_VENC;
            txtTotalFacturasVencidas.SetFocus;
            Exit;
        end;
    
        if seCantidadFacturasVencidas.Value < 1 then begin
            strError := MSG_ERROR_CANTIDAD_FACTURAS_VENCIDAS;
            seCantidadFacturasVencidas.SetFocus;
            Exit;
        end;
    end
    else if grpPosiblesInfractores.Visible then begin
        if seDiasInfraccion.Value < 1 then begin
            strError := MSG_ERROR_CANTIDAD_DIAS_INFRACCION;
            seDiasInfraccion.SetFocus;
            Exit;
        end;

        if (Trim(TrimLeft(TrimRight(txtImporteInfraccion.Text))) = '') or (StrToInt(Trim(TrimLeft(TrimRight(txtImporteInfraccion.Text)))) < 0) then begin
            strError := MSG_ERROR_IMPORTE_INFRACCION;
            txtImporteInfraccion.SetFocus;
            Exit;
        end;
    end
	else if grpTagsDefectuosos.Visible then begin
        if rgTagsDefectuosos.ItemIndex < 0 then begin
            strError := MSG_ERROR_TIPO_DEFECTO_TAG;
            rgTagsDefectuosos.SetFocus;
            Exit;
        end;
    end
    else if grpArbitrario.Visible then begin
        if cbConcesionarias.ItemIndex < 0 then begin
            strError := MSG_ERROR_CONCESIONARIA;
            cbConcesionarias.SetFocus;
            Exit;
        end;

        if (rgArbitrarioSuscripcion.ItemIndex < 0) and ((seCantidadTAGs.Value < 1) or ((Trim(TrimLeft(TrimRight(txtValorFactura.Text))) = '') or (StrToInt(Trim(TrimLeft(TrimRight(txtValorFactura.Text)))) < 0)))then begin
			strError := MSG_ARBITRARIO_NULL;
			Exit;        
        end;

    end;

	if (Trim(TrimLeft(TrimRight(edtDescripcionCampanna.Text))) = '') then begin
    	strError	:= MSG_ERROR_DESCRIPCION_CAMPANNA;
        edtDescripcionCampanna.SetFocus;
    	Exit;
    end;
    
	if cbbMedioContacto.ItemIndex < 0 then begin
    	strError	:= MSG_ERROR_MEDIO_CONTACTO;
        cbbMedioContacto.SetFocus;
    	Exit;
    end;
    
	if Trunc(txtInicioVigencia.Date) < Trunc(Now) then begin
    	strError	:= MSG_ERROR_INICIO_VIGENCIA_MENOR_ACTUAL;
        txtInicioVigencia.SetFocus;
    	Exit;
    end;

    if txtTerminoVigencia.Visible then begin
        if Trunc(txtTerminoVigencia.Date) < Trunc(txtInicioVigencia.Date) then begin
            strError	:= MSG_ERROR_TERMINO_MENOR_INICIO_VIGENCIA;
            txtTerminoVigencia.SetFocus;
            Exit;
        end;
    end;

	{INICIO:	TASK_107_CFU_20170215    
	if cbbPrioridad.ItemIndex < 0 then begin
    	strError	:= MSG_ERROR_PRIORIDAD;
        cbbPrioridad.SetFocus;
    	Exit;
    end;
    
	if cbbAreaAsignada.ItemIndex < 0 then begin
    	strError	:= MSG_ERROR_AREA_ASIGNADA;
        cbbAreaAsignada.SetFocus;
    	Exit;
    end;
    TERMINO:	TASK_107_CFU_20170215}
    Result	:= True;
end;

function TFormCrearCampannas.CrearCampanna(TipoCampanna: Integer; var strCodCampanna, strError: String): Boolean;
begin
	Result	:= False;
    try
        with spGuardarCampannas, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
        
            ParamByName('@CodigoTipoCampanna').Value			:= TipoCampanna;
            ParamByName('@CodigoMotivoCampanna').Value	        := cbMotivoCampanna.Value;
            ParamByName('@DescripcionCampanna').Value	        := edtDescripcionCampanna.Text;

			ParamByName('@Recursiva').Value						:= (chkRecursiva.Visible and chkRecursiva.Checked);
			ParamByName('@DiaEnvio').Value						:= iif(chkRecursiva.Checked, seDiaEnvio.Value, null);

            ParamByName('@MedioContacto').Value					:= cbbMedioContacto.Text;
            ParamByName('@FechaInicioVigencia').Value  			:= Trunc(txtInicioVigencia.Date);
            ParamByName('@FechaTerminoVigencia').Value 			:= iif(txtTerminoVigencia.Visible, Trunc(txtTerminoVigencia.Date), null);
            ParamByName('@Prioridad').Value						:= cbbPrioridad.Text;
            ParamByName('@CodigoAreaAtencionDeCaso').Value		:= cbbAreaAsignada.Value;
            ParamByName('@ArchivoGenerado').Value				:= null;
            if ((cbbMedioContacto.Text = ITEM_CONTACTO_EMAIL) or (cbbMedioContacto.Text = ITEM_CONTACTO_CARTA)) and (CodigoPlantilla <> -1) then
				ParamByName('@CodigoPlantilla').Value	:= CodigoPlantilla
            else
            	ParamByName('@CodigoPlantilla').Value	:= null;
            if grpRechazos.Visible then begin
                ParamByName('@MedioDePago').Value				:= iif(cb_PagoAutomatico.ItemIndex < 0, null, cb_PagoAutomatico.Value);
                ParamByName('@RechazosConsecutivos').Value		:= iif(seCantMinimaRechazosConsecutivos.Value < 1, null, seCantMinimaRechazosConsecutivos.Value);
            end
            else begin
            	ParamByName('@MedioDePago').Value				:= null;
                ParamByName('@RechazosConsecutivos').Value		:= null;
            end;
            if grpMorosos.Visible then begin
                ParamByName('@TiempoMorosidadFactura').Value		:= iif(seTiempoMorosidad.Value < 1, null, seTiempoMorosidad.Value);
                ParamByName('@ImporteFactura').Value				:= iif(Trim(txtImporteFactura.Text) = '', null, txtImporteFactura.Text);
                ParamByName('@ImporteTotalFacturasVencidas').Value	:= iif(Trim(txtTotalFacturasVencidas.Text) = '', null, txtTotalFacturasVencidas.Text);
                ParamByName('@CantidadFacturasVencidas').Value		:= iif(seCantidadFacturasVencidas.Value < 1, null, seCantidadFacturasVencidas.Value);
            end
            else begin
            	ParamByName('@TiempoMorosidadFactura').Value		:= null;
                ParamByName('@ImporteFactura').Value				:= null;
                ParamByName('@ImporteTotalFacturasVencidas').Value	:= null;
                ParamByName('@CantidadFacturasVencidas').Value		:= null;
            end;
            if grpPosiblesInfractores.Visible then begin
                ParamByName('@DiasInfraccion').Value	        := iif(seDiasInfraccion.Value < 1, null, seDiasInfraccion.Value);
                ParamByName('@ImporteInfraccion').Value	        := iif(Trim(txtImporteInfraccion.Text) = '', null, txtImporteInfraccion.Text);
            end
            else begin
                ParamByName('@DiasInfraccion').Value	        := null;
                ParamByName('@ImporteInfraccion').Value	        := null;
            end;
            if grpTagsDefectuosos.Visible then begin
            	ParamByName('@CodigoTipoDefectoTAG').Value		:= rgTagsDefectuosos.ItemIndex + 1;
            end
            else begin
            	ParamByName('@CodigoTipoDefectoTAG').Value		:= null;
            end;
            if grpArbitrario.Visible then begin
                ParamByName('@CodigoConcesionaria').Value		:= iif(cbConcesionarias.ItemIndex <= 0, null, cbConcesionarias.Value);
                ParamByName('@CodigoRegion').Value	            := iif(cbRegiones.ItemIndex < 0, null, cbRegiones.Value);
                ParamByName('@CodigoComuna').Value	            := iif(cbComunas.ItemIndex < 0, null, cbComunas.Value);
                ParamByName('@CantidadTAGs').Value	            := iif(seCantidadTAGs.Value < 1, null, seCantidadTAGs.Value);
                ParamByName('@ValorFactura').Value	            := iif(Trim(txtValorFactura.Text) = '', null, txtValorFactura.Text);
                ParamByName('@Suscripcion').Value	            := iif(rgArbitrarioSuscripcion.ItemIndex <0, null, rgArbitrarioSuscripcion.ItemIndex);
                
            end
            else begin
            	ParamByName('@CodigoConcesionaria').Value		:= null;
                ParamByName('@CodigoComuna').Value	            := null;
                ParamByName('@CantidadTAGs').Value	            := null;
                ParamByName('@ValorFactura').Value	            := null;
            end;
            ParamByName('@UsuarioCreacion').Value				:= UsuarioSistema;
			ExecProc;

            strCodCampanna	:= ParamByName('@CodigoCampanna').Value;
        end;
    except
    	on E:Exception do begin
        	strError := E.Message;
            Exit;
        end;
    end;
    Result	:= True;
end;

procedure TFormCrearCampannas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormCrearCampannas.FormCreate(Sender: TObject);
begin
	CodigoPlantilla	:= -1;
end;

procedure TFormCrearCampannas.FormResize(Sender: TObject);
var
	PosLabelVigencia: Integer;
const
	DISTANCIA_LABEL		= 135;
    DISTANCIA_CALENDAR	= 141;
begin
	PosLabelVigencia			:= lblVigencia.Left;
	txtInicioVigencia.Left		:= PosLabelVigencia + DISTANCIA_LABEL;
    txtTerminoVigencia.Left		:= txtInicioVigencia.Left + DISTANCIA_CALENDAR;
end;

procedure TFormCrearCampannas.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormCrearCampannas.btnSeleccionarPlantillaClick(
  Sender: TObject);
var
	f: TFormSeleccionarPlantillaEMail;
begin
	try
        Application.CreateForm(TFormSeleccionarPlantillaEMail,f);
        if f.Inicializar() and (f.ShowModal = mrOK) then
            CodigoPlantilla	:= StrToInt(f.ListaCodigosPlantillas[f.rgPlantillas.ItemIndex]);
    finally
        f.Release;
    end;
end;

end.

