{********************************** File Header ********************************
File Name   : FrmRptNotaCobroDetalleInfracciones.pas
Author      : lgisuk
Date Created: 07/12/05
Language    : ES-AR
Description : Detalle de las infracciones incluidas en la nota de cobro

Revision 1:
    Author : nefernandez
    Date : 21/08/2007
    Description : SS 548: Cambio del atributo FNumeroComprobante y del par�metro
    NumeroComprobante (funci�n Inicializar) de Integer a Int64.

    
Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

*******************************************************************************}
unit frmRptNotaCobroDetalleInfracciones;

interface

uses
   //Detalle de Infracciones
   DMConnection,
   util,
   utilproc,
   UtilDb,
   PeaTypes,
   ConstParametrosGenerales,
   //Otros
   Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
   Dialogs, ppBands, ppReport, ppSubRpt, {ppChrt, ppChrtDP,} ppCtrls, ppStrtch,
   ppMemo, ppParameter, ppBarCod, jpeg, ppPrnabl, ppClass, UtilRB, ppCache,
   ppComm, ppRelatv, ppProd, ppModule, raCodMod, DB, ADODB, ppDB, ppDBPipe,
   ppVar, StdCtrls, daDataModule, ppRegion, ppCTMain, ppTxPipe, rbSetup,
   TeEngine, Series, ExtCtrls, TeeProcs, Chart, DbChart, TXComp, ppDevice,
   Printers, ImprimirWO, TXRB;

type
  TfRptNotaCobroDetalleInfracciones = class(TForm)
    rpt_ReporteNotaCobroInfractor: TppReport;
    rb_ReporteNotaCobroInfractor: TRBInterface;
    ppParameterList1: TppParameterList;
    sp_ObtenerNotaCobroInfractor: TADOStoredProc;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    raCodeModule3: TraCodeModule;
    dbpl_ObtenerInfraccionesComprobante: TppDBPipeline;
    sp_ObtenerInfraccionesComprobante: TADOStoredProc;
    dsObtenerInfraccionesComprobante: TDataSource;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppLabel4: TppLabel;
    pplblNumero: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppImage2: TppImage;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLine3: TppLine;
    ppLabel5: TppLabel;
    ppfechaVencimiento: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppusuario: TppLabel;
    ppfecha: TppLabel;
    procedure rb_ReporteNotaCobroInfractorExecute(Sender: TObject;var Cancelled: Boolean);
   private
    { Private declarations }
    FNumeroComprobante : Int64;
  public
    { Public declarations }
    function Inicializar(NumeroComprobante: Int64): Boolean;
  end;

var
  fRptNotaCobroDetalleInfracciones : TfRptNotaCobroDetalleInfracciones;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 07/12/2005
  Description:  Inicializacin de Este Formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfRptNotaCobroDetalleInfracciones.Inicializar(NumeroComprobante: Int64): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    FNumeroComprobante := NumeroComprobante;
    Result := rb_ReporteNotaCobroInfractor.Execute;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 07/12/2005
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRptNotaCobroDetalleInfracciones.rb_ReporteNotaCobroInfractorExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_ERROR_INVOICE_NOT_FOUND = 'No se encuentra el Comprobante';
    MSG_ERROR_PRINTING_THE_COLLECTION_NOTE = 'Se ha producido un error al imprimir la Nota de Cobro';
begin
    try
        with sp_ObtenerNotaCobroInfractor, Parameters do begin
            Close;
            Refresh;
            ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
            CommandTimeOut := 500;
            Open;
            if not IsEmpty then begin
                //Encabezado
                ppusuario.Caption:= QueryGetValue(dmconnections.BaseCAC,'select dbo.ObtenerNombreUsuario( ''' + usuariosistema + ''' ) ');//usuariosistema;
                ppfecha.Caption := DateToStr(Now);
                pplblNumero.Caption := FieldByName('NumeroComprobante').AsString;
                ppFechaVencimiento.Caption := DateToStr(FieldByName('FechaVencimiento').AsDateTime);
            end else begin
                Cancelled := True;
                MsgBox(MSG_ERROR_INVOICE_NOT_FOUND, Self.Caption, MB_ICONERROR);
                Exit;
            end;
        end;

        //Obtengo las infracciones incluidas en el comprobante
        with sp_ObtenerInfraccionesComprobante, Parameters do begin
            Close;
            Refresh;
            ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
            ParamByName('@CantidadInfracciones').Value := 0;
            CommandTimeOut := 500;
            Open;
        end;

    except
        on E : Exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR_PRINTING_THE_COLLECTION_NOTE, E.Message, Self.Caption, MB_ICONERROR);
        end;
    end;
end;

end.
