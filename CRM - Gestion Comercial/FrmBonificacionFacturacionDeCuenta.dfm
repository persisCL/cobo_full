object FormBonificacionFacturacionDeCuenta: TFormBonificacionFacturacionDeCuenta
  Left = 364
  Top = 162
  BorderStyle = bsDialog
  Caption = 'Bonificar Facturaci'#243'n a Veh'#237'culo'
  ClientHeight = 311
  ClientWidth = 308
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object gb_DatosVehiculo: TGroupBox
    Left = 8
    Top = 4
    Width = 289
    Height = 73
    Caption = ' Datos del Veh'#237'culo '
    TabOrder = 0
    object lbl_Patente: TLabel
      Left = 11
      Top = 20
      Width = 40
      Height = 13
      Caption = 'Patente:'
    end
    object lbl_Televia: TLabel
      Left = 11
      Top = 44
      Width = 40
      Height = 13
      Caption = 'Telev'#237'a:'
    end
    object lblPatente: TLabel
      Left = 61
      Top = 20
      Width = 58
      Height = 13
      Caption = 'lblPatente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTelevia: TLabel
      Left = 61
      Top = 44
      Width = 56
      Height = 13
      Caption = 'lblTelevia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object nb_Botones: TNotebook
    Left = 112
    Top = 274
    Width = 189
    Height = 29
    PageIndex = 1
    TabOrder = 2
    object TPage
      Left = 0
      Top = 0
      Caption = 'PageBotonesAceptarCancelar'
      object btn_Aceptar: TButton
        Left = 34
        Top = 2
        Width = 75
        Height = 25
        Caption = '&Aceptar'
        TabOrder = 0
        OnClick = btn_AceptarClick
      end
      object btn_Cancelar: TButton
        Left = 111
        Top = 2
        Width = 75
        Height = 25
        Cancel = True
        Caption = '&Cancelar'
        TabOrder = 1
        OnClick = btn_CancelarClick
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'PageBotonSalir'
      object btn_Salir: TButton
        Left = 111
        Top = 2
        Width = 75
        Height = 25
        Cancel = True
        Caption = '&Salir'
        TabOrder = 0
        OnClick = btn_SalirClick
      end
    end
  end
  object pnlDatos: TPanel
    Left = 8
    Top = 88
    Width = 289
    Height = 178
    BevelOuter = bvLowered
    TabOrder = 1
    object lbl_FechaDesde: TLabel
      Left = 10
      Top = 12
      Width = 76
      Height = 13
      Caption = 'Fecha Desde'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_FechaFin: TLabel
      Left = 10
      Top = 40
      Width = 61
      Height = 13
      Caption = 'Fecha Hasta'
    end
    object lbl_Usuario: TLabel
      Left = 10
      Top = 124
      Width = 36
      Height = 13
      Caption = 'Usuario'
    end
    object lbl_FechaModificacion: TLabel
      Left = 10
      Top = 152
      Width = 62
      Height = 13
      Caption = 'Fecha Modif.'
    end
    object lbl_Tipo: TLabel
      Left = 10
      Top = 68
      Width = 21
      Height = 13
      Caption = 'Tipo'
    end
    object lbl_Valor: TLabel
      Left = 10
      Top = 96
      Width = 24
      Height = 13
      Caption = 'Valor'
    end
    object lblCodUsuario: TLabel
      Left = 96
      Top = 124
      Width = 65
      Height = 13
      Caption = 'lblCodUsuario'
    end
    object lblFechaModif: TLabel
      Left = 96
      Top = 152
      Width = 66
      Height = 13
      Caption = 'lblFechaModif'
    end
    object txt_FechaInicio: TDateEdit
      Left = 94
      Top = 8
      Width = 98
      Height = 21
      Hint = 'Fecha desde la cual no facturar tr'#225'nsitos'
      AutoSelect = False
      Color = 16444382
      TabOrder = 0
      Date = -693594
    end
    object txt_FechaFin: TDateEdit
      Left = 94
      Top = 36
      Width = 98
      Height = 21
      Hint = 'Fecha hasta la cual no facturar tr'#225'nsitos'
      AutoSelect = False
      TabOrder = 1
      Date = -693594
    end
    object txt_Porcentaje: TNumericEdit
      Left = 94
      Top = 92
      Width = 55
      Height = 21
      Hint = 'Porcentaje de la bonificaci'#243'n'
      Color = 16444382
      MaxLength = 6
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Decimals = 2
    end
    object txt_Importe: TNumericEdit
      Left = 94
      Top = 92
      Width = 71
      Height = 21
      Hint = 'Importe de la bonificaci'#243'n'
      Color = 16444382
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      Decimals = 0
    end
    object rb_Porcentaje: TRadioButton
      Left = 94
      Top = 68
      Width = 72
      Height = 17
      Hint = 'Tipo de bonificaci'#243'n'
      Caption = '&Porcentaje'
      Checked = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = True
      OnClick = rb_PorcentajeClick
    end
    object rb_Importe: TRadioButton
      Left = 172
      Top = 68
      Width = 56
      Height = 17
      Hint = 'Tipo de bonificaci'#243'n'
      Caption = '&Importe'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = rb_ImporteClick
    end
  end
end
