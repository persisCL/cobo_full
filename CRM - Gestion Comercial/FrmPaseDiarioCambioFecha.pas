{-----------------------------------------------------------------------------
 File Name: FrmPaseDiarioCambioFecha.pas
 Author:    aunanue
 Date Created: 21/04/2017
 Language: ES-AR
 Description: Busqueda y edici�n de daypasses
 Firma: TASK_012_AUN_20170420-CU.COBO.CRM.PDS.302
-----------------------------------------------------------------------------}
unit FrmPaseDiarioCambioFecha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx,UtilProc,DmConnection,
  DB, ADODB, DmiCtrls, Validate, DateEdit,peaprocs,util,UtilDB,Abm_obj,RStrings, 
  FrmAperturaCierrePuntoVenta, Peatypes, frmRptCierreTurno, frmConfirmaContrasena, VariantComboBox,
  Frm_CambiarFechaUsoPaseDiario, ConstParametrosGenerales;

type
  TPaseDiarioConsultaEdicionForm = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    dbl_DayPass: TDBListEx;
    spObtenerDayPass: TADOStoredProc;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    txt_FechaDesde: TDateEdit;
    txt_FechaHasta: TDateEdit;
    Label2: TLabel;
    Label6: TLabel;
    txtPatente: TEdit;
    Label4: TLabel;
    dsObtenerDayPass: TDataSource;
    spActualizarFechaUsoDayPass: TADOStoredProc;
    btn_Filtrar: TButton;
    btn_Limpiar: TButton;
    BtnSalir: TButton;
    btnCambiarFecha: TButton;
    txtCantidadRegistros: TNumericEdit;
    lblMostrar: TLabel;
    lblRegistros: TLabel;
    txtNumeroSerie: TNumericEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure spObtenerDayPassAfterScroll(DataSet: TDataSet);
    procedure btnCambiarFechaClick(Sender: TObject);
    procedure dbl_DayPassColumns0HeaderClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(MDIChild: Boolean): Boolean;
  end;

implementation

uses Main, DB_CRUDCommonProcs;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 17/02/2005
  Description: Inicializa el Form de Cambio de Fecha de Pase Diario
  Parameters: MDIChild: Boolean
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TPaseDiarioConsultaEdicionForm.Inicializar(MDIChild: Boolean): Boolean;
Var S: TSize;
begin
	Result := True;
    try
        if MDIChild then begin
            S := GetFormClientSize(Application.MainForm);
            SetBounds(0, 0, S.cx, S.cy);
        end else begin
            FormStyle := fsNormal;
            Visible := False;
        end;
        txt_FechaHasta.Date:=Now;
        txtCantidadRegistros.ValueInt := 100;
        btn_Limpiar.Click;
        btnCambiarFecha.Enabled := False;
    except
        Result := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author:    flamas
  Date Created: 17/02/2005
  Description: Cierra la ventana
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TPaseDiarioConsultaEdicionForm.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    flamas
  Date Created: 17/02/2005
  Description: Cierra la ventana - Libera la memoria
  Parameters: Sender: TObject;  var Action: TCloseAction
  Return Value:
-----------------------------------------------------------------------------}
procedure TPaseDiarioConsultaEdicionForm.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
     Action := caFree;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_FiltrarClick
  Author:    flamas
  Date Created: 17/02/2005
  Description: Filtra los pases diarios de acuerdo a la selecci�n realizada
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TPaseDiarioConsultaEdicionForm.btn_FiltrarClick(Sender: TObject);
resourcestring
	MSG_PLATE_MUST_HAVA_A_VALUE		= 'Debe ingresar un valor de patente';
    MSG_ERROR_LOADING_DAY_PASSES 	= 'Error cargando Pases Diarios';
    CAP_INVALID_QTY                 = 'Cantidad Inv�lida';
    MSG_INVALID_QTY                 = 'La cantidad a mostrar debe ser mayor o igual a 1 (uno).';
begin
	// Valida que la fecha Desde no sea mayor que la fecha Hasta
    if (txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate) and (txt_FechaDesde.Date>txt_FechaHasta.Date) then begin
        MsgBoxBalloon(MSG_VALIDAR_ORDEN_FECHA,Caption,MB_ICONSTOP,txt_FechaDesde);
        exit;
    end;

    // Valida que la cantidad de registros a mostrar sea superior a 0 (cero)
    if not ValidateControls(
        [txtPatente, txtCantidadRegistros],
        [Trim(txtPatente.Text) <> '', txtCantidadRegistros.ValueInt > 0],
        Self.Caption,
        [MSG_PLATE_MUST_HAVA_A_VALUE, MSG_INVALID_QTY]) then begin
        Exit;
    end;

	try
    	btnCambiarFecha.Enabled := False;
    	spObtenerDayPass.DisableControls;
        spObtenerDayPass.Close;
        DB_CRUDCommonProcs.AddParameter(spObtenerDayPass.Parameters, '@Patente', ftString, pdInput, txtPatente.Text, 10);
        DB_CRUDCommonProcs.AddParameter(spObtenerDayPass.Parameters, '@ContractSerialNumber', ftLargeint, pdInput, iif(txtNumeroSerie.ValueInt = 0, NULL, txtNumeroSerie.ValueInt));
        DB_CRUDCommonProcs.AddParameter(spObtenerDayPass.Parameters, '@FechaDesde', ftDateTime, pdInput, iif(txt_FechaDesde.Date=nulldate,null,txt_FechaDesde.Date));
        DB_CRUDCommonProcs.AddParameter(spObtenerDayPass.Parameters, '@FechaHasta', ftDateTime, pdInput, iif(txt_FechaHasta.Date=nulldate,null,txt_FechaHasta.Date));
        DB_CRUDCommonProcs.AddParameter(spObtenerDayPass.Parameters, '@CantidadRegistros', ftInteger, pdInput, txtCantidadRegistros.ValueInt);

    	if not(OpenTables([spObtenerDayPass])) then
	       	MsgBox(MSG_ERROR_LOADING_DAY_PASSES,Caption, MB_ICONERROR);

        spObtenerDayPass.AfterScroll(spObtenerDayPass);
    finally
    	spObtenerDayPass.EnableControls;
    end;
    dbl_DayPass.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: FormShow
  Author:    flamas
  Date Created: 17/02/2005
  Description: Setea el Foco cuando abre el Form
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TPaseDiarioConsultaEdicionForm.FormShow(Sender: TObject);
begin
    txtPatente.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_LimpiarClick
  Author:    flamas
  Date Created: 17/02/2005
  Description: Borra los filtros seleccionados
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TPaseDiarioConsultaEdicionForm.btn_LimpiarClick(Sender: TObject);
begin
    btnCambiarFecha.Enabled := False;
    txtPatente.Clear;
    txtNumeroSerie.Clear;
    txt_FechaDesde.Clear;
    txt_FechaHasta.Clear;
    txtCantidadRegistros.Clear;
    txtCantidadRegistros.ValueInt := 100;
    spObtenerDayPass.close;
    txtPatente.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: spObtenerDayPassAfterScroll
  Author:    flamas
  Date Created: 17/02/2005
  Description: Habilita o deshabilita el bot�n de Cambiar Fecha
  Parameters: Sender: DataSet: TDataSet
  Return Value:
-----------------------------------------------------------------------------}
procedure TPaseDiarioConsultaEdicionForm.spObtenerDayPassAfterScroll(DataSet: TDataSet);
begin
	if not spObtenerDayPass.IsEmpty then
    	btnCambiarFecha.Enabled :=  (not spObtenerDayPass.FieldByName('Devuelto').AsBoolean) and
        							(not spObtenerDayPass.FieldByName('Usado').AsBoolean) and
                                    (not spObtenerDayPass.FieldByName('Vencido').AsBoolean) and
        							(spObtenerDayPass.FieldByName('TipoDayPass').AsString = TIPO_DAYPASS_NORMAL) and
                                    ExisteAcceso('Permiso_CambiarFechasPasesDiarios')
    else btnCambiarFecha.Enabled := False;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCambiarFechaClick
  Author:    flamas
  Date Created: 17/02/2005
  Description: Cambia la fecha de uso del Pase Diario
  Parameters: Sender: DataSet: TDataSet
  Return Value:
-----------------------------------------------------------------------------}
procedure TPaseDiarioConsultaEdicionForm.btnCambiarFechaClick(Sender: TObject);
resourcestring
	MSG_ERROR_UPDATING_DATE = 'Error cambiando la fecha de uso';
var
	f : TFrmCambiarFechaUsoPaseDiario;
begin
	// Verifica que est� habilitado el Bot�n de CambiarFecha
    // para el caso en que se llame desde el doble click
	if not btnCambiarFecha.Enabled then Exit;
    Screen.Cursor := crHourGlass;
    f := TFrmCambiarFechaUsoPaseDiario.Create(nil);
    try
        try
    	    if f.Inicializar(spObtenerDayPass.FieldByName('Patente').AsString,
			    				spObtenerDayPass.FieldByName('ContractSerialNumber').AsString,
				    			spObtenerDayPass.FieldByName('FechaVenta').AsDateTime,
                                spObtenerDayPass.FieldByName('FechaVencimiento').AsDateTime,
						    	spObtenerDayPass.FieldByName('FechaUso').AsDateTime) then begin
        		if (f.ShowModal = mrOK) then begin
                    spActualizarFechaUsoDayPass.Connection.BeginTrans;
                    DB_CRUDCommonProcs.AddParameter(spActualizarFechaUsoDayPass.Parameters, '@NumeroDayPass', ftLargeint, pdInput, spObtenerDayPass.FieldByName('NumeroDayPass').AsInteger);
                    DB_CRUDCommonProcs.AddParameter(spActualizarFechaUsoDayPass.Parameters, '@FechaUso', ftDateTime, pdInput, f.edNuevaFechaUso.Date);
                    DB_CRUDCommonProcs.AddParameter_CodigoUsuario(spActualizarFechaUsoDayPass.Parameters);
                    spActualizarFechaUsoDayPass.ExecProc;
                    spActualizarFechaUsoDayPass.Connection.CommitTrans;
                    btn_FiltrarClick(nil);
                end;
            end;
        except
            on e : exception do begin
                if spActualizarFechaUsoDayPass.Connection.InTransaction then
                    spActualizarFechaUsoDayPass.Connection.RollbackTrans;
		        MsgBoxErr(MSG_ERROR_UPDATING_DATE, e.Message, Self.Caption, MB_ICONERROR);
            end;
         end;
    finally
        if spActualizarFechaUsoDayPass.Connection.InTransaction then
            spActualizarFechaUsoDayPass.Connection.RollbackTrans;
    	f.Release;
        Screen.Cursor := crDefault;
    end;
end;

{********************************** File Header ********************************
File Name : FrmPaseDiarioCambioFecha.pas
Author : FLamas
Date Created: 13/07/2005
Language : ES-AR
Description : Ordena por Columna
*******************************************************************************}
procedure TPaseDiarioConsultaEdicionForm.dbl_DayPassColumns0HeaderClick(
  Sender: TObject);
begin
    if not spObtenerDayPass.Active then Exit;
    //
    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;
    //
    spObtenerDayPass.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

end.



