object FormModificacionConvenio: TFormModificacionConvenio
  Left = 78
  Top = 47
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  Caption = 'Modificaci'#243'n de Convenio'
  ClientHeight = 750
  ClientWidth = 1295
  Color = 16776699
  Constraints.MinHeight = 579
  Constraints.MinWidth = 900
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1295
    Height = 750
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      1295
      750)
    object pnlDatosContacto: TPanel
      Left = 0
      Top = 0
      Width = 1324
      Height = 744
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        1324
        744)
      object lblTipoConvenio: TLabel
        Left = 12
        Top = 15
        Width = 105
        Height = 13
        Caption = 'Tipo de Convenio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTipoCliente: TLabel
        Left = 308
        Top = 15
        Width = 91
        Height = 13
        Caption = 'Tipo de Cliente:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      inline FreDatoPersona: TFreDatoPresona
        Left = 5
        Top = 55
        Width = 1324
        Height = 108
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 5
        ExplicitTop = 55
        ExplicitWidth = 1324
        ExplicitHeight = 108
        inherited ledTipoCliente: TLed
          Left = 1264
          Top = 66
          ExplicitLeft = 1264
          ExplicitTop = 59
        end
        inherited lblTipo: TLabel
          Left = 1056
          Top = 67
          ExplicitLeft = 1056
          ExplicitTop = 67
        end
        inherited lblSemaforo1: TLabel
          Left = 1060
          Top = 44
          ExplicitLeft = 1060
          ExplicitTop = 44
        end
        inherited LedSemaforo1: TLed
          Left = 1264
          Top = 45
          ExplicitLeft = 1264
          ExplicitTop = 38
        end
        inherited lblSemaforo3: TLabel
          Left = 1060
          Top = 88
          ExplicitLeft = 1060
          ExplicitTop = 88
        end
        inherited LedSemaforo3: TLed
          Left = 1264
          Top = 87
          ExplicitLeft = 1264
          ExplicitTop = 80
        end
        inherited Panel1: TPanel [6]
          Width = 1324
          ParentBiDiMode = False
          ExplicitWidth = 1324
          DesignSize = (
            1324
            25)
          inherited cbPersoneria: TComboBox
            Top = 4
            OnChange = FreDatoPersonacbPersoneriaChange
            ExplicitTop = 4
          end
          inherited txtDocumento: TEdit
            MaxLength = 11
            OnKeyDown = FreDatoPersonatxtDocumentoKeyDown
          end
          inherited txtRazonSocial: TEdit
            OnChange = OnChangeGenerico
          end
          inherited txtGiro: TEdit
            Width = 464
            OnChange = OnChangeGenerico
            ExplicitWidth = 464
          end
        end
        inherited Pc_Datos: TPageControl [7]
          Height = 69
          ActivePage = FreDatoPersona.Tab_Juridica
          ExplicitHeight = 69
          inherited Tab_Juridica: TTabSheet
            ExplicitHeight = 59
            inherited txtApellidoMaterno_Contacto: TEdit
              OnChange = OnChangeGenerico
            end
            inherited cbSexo_Contacto: TComboBox
              OnChange = OnChangeGenerico
            end
            inherited txtApellido_Contacto: TEdit
              OnChange = OnChangeGenerico
            end
            inherited txtNombre_Contacto: TEdit
              OnChange = OnChangeGenerico
            end
          end
          inherited Tab_Fisica: TTabSheet
            inherited txt_ApellidoMaterno: TEdit
              OnChange = OnChangeGenerico
            end
            inherited txt_Apellido: TEdit
              OnChange = OnChangeGenerico
            end
            inherited cbSexo: TComboBox
              OnChange = OnChangeGenerico
            end
            inherited txtFechaNacimiento: TDateEdit
              OnChange = OnChangeGenerico
            end
            inherited txt_Nombre: TEdit
              OnChange = OnChangeGenerico
            end
          end
        end
        inherited cboTipoCliente: TDBLookupComboBox
          Left = 1131
          Top = 63
          TabOrder = 3
          OnCloseUp = FreDatoPersonacboTipoClienteCloseUp
          ExplicitLeft = 1131
          ExplicitTop = 63
        end
        inherited cboSemaforo1: TDBLookupComboBox
          Left = 1131
          Top = 41
          TabOrder = 2
          OnCloseUp = FreDatoPersonacboSemaforo1CloseUp
          ExplicitLeft = 1131
          ExplicitTop = 41
        end
        inherited cboSemaforo3: TDBLookupComboBox
          Left = 1131
          Top = 85
          OnCloseUp = FreDatoPersonacboSemaforo3CloseUp
          ExplicitLeft = 1131
          ExplicitTop = 85
        end
        inherited spTiposClientes: TADOStoredProc
          Left = 680
          Top = 65528
        end
        inherited srcTipoCliente: TDataSource
          Left = 640
          Top = 65528
        end
      end
      object GBMediosComunicacion: TGroupBox
        Left = 6
        Top = 162
        Width = 1284
        Height = 70
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Medios de comunicaci'#243'n'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        object lblEmail: TLabel
          Left = 426
          Top = 46
          Width = 28
          Height = 13
          Caption = 'Email:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lblEnviar: TLabel
          Left = 817
          Top = 46
          Width = 30
          Height = 13
          Caption = 'Enviar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          OnClick = lblEnviarClick
          OnMouseEnter = lblEnviarMouseEnter
          OnMouseLeave = lblEnviarMouseLeave
        end
        inline FreTelefonoPrincipal: TFrameTelefono
          Left = 9
          Top = 14
          Width = 607
          Height = 26
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 9
          ExplicitTop = 14
          ExplicitWidth = 607
          ExplicitHeight = 26
          inherited txtTelefono: TEdit
            Top = 2
            OnChange = OnChangeGenerico
            OnExit = nil
            ExplicitTop = 2
          end
          inherited cbTipoTelefono: TVariantComboBox
            OnChange = OnChangeGenerico
            OnExit = nil
          end
          inherited cbDesde: TComboBox
            OnChange = OnChangeGenerico
          end
          inherited cbHasta: TComboBox
            OnChange = OnChangeGenerico
          end
          inherited cbCodigosArea: TComboBox
            OnChange = OnChangeGenerico
          end
        end
        inline FreTelefonoSecundario: TFrameTelefono
          Left = 9
          Top = 39
          Width = 416
          Height = 26
          HorzScrollBar.Visible = False
          VertScrollBar.Visible = False
          TabOrder = 1
          TabStop = True
          ExplicitLeft = 9
          ExplicitTop = 39
          ExplicitWidth = 416
          ExplicitHeight = 26
          inherited txtTelefono: TEdit
            OnChange = OnChangeGenerico
            OnExit = nil
          end
          inherited cbTipoTelefono: TVariantComboBox
            OnChange = OnChangeGenerico
            OnExit = nil
          end
          inherited cbCodigosArea: TComboBox
            OnChange = OnChangeGenerico
          end
        end
        object NBook_Email: TNotebook
          Left = 456
          Top = 41
          Width = 358
          Height = 24
          TabOrder = 2
          object TPage
            Left = 0
            Top = 0
            Caption = 'PageSalir'
            object txtEMailContacto: TEdit
              Left = 3
              Top = 1
              Width = 350
              Height = 21
              Hint = 'Direcci'#243'n de e-mail'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 60
              ParentFont = False
              ParentShowHint = False
              PopupMenu = PopupMenu
              ShowHint = False
              TabOrder = 0
              Text = 'txtEMailContacto'
              OnChange = txtEMailContactoChange
              OnExit = txtEmailParticularExit
              OnKeyPress = txtEMailContactoKeyPress
            end
          end
          object TPage
            Left = 0
            Top = 0
            HelpContext = 1
            Caption = 'PageGuardar'
            object txtEmailParticular: TEdit
              Left = 3
              Top = 1
              Width = 350
              Height = 21
              Hint = 'Direcci'#243'n de e-mail'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 60
              ParentFont = False
              ParentShowHint = False
              PopupMenu = PopupMenu
              ShowHint = True
              TabOrder = 0
              OnChange = txtEMailContactoChange
              OnExit = txtEmailParticularExit
              OnKeyPress = txtEMailContactoKeyPress
            end
          end
        end
      end
      object GBDomicilios: TGroupBox
        Left = 4
        Top = 234
        Width = 1284
        Height = 116
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Domicilio '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        inline FMDomicilioPrincipal: TFrameDomicilio
          Left = 2
          Top = 11
          Width = 836
          Height = 102
          HorzScrollBar.Visible = False
          VertScrollBar.Visible = False
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 2
          ExplicitTop = 11
          ExplicitHeight = 102
          inherited Pnl_Arriba: TPanel
            inherited cb_Regiones: TComboBox
              OnChange = FMDomicilioPrincipalcb_RegionesChange
            end
            inherited txt_DescripcionCiudad: TEdit
              MaxLength = 50
              OnChange = OnChangeGenerico
            end
            inherited txt_numeroCalle: TEdit
              OnChange = OnChangeGenerico
            end
            inherited cb_BuscarCalle: TComboBuscarCalle
              OnChange = OnChangeGenerico
            end
            inherited txt_CodigoPostal: TEdit
              OnChange = OnChangeGenerico
            end
          end
          inherited Pnl_Abajo: TPanel
            inherited cb_Comunas: TComboBox
              OnChange = FMDomicilioPrincipalcb_ComunasChange
            end
            inherited txt_Depto: TEdit
              OnChange = OnChangeGenerico
            end
          end
          inherited Pnl_Medio: TPanel
            Visible = True
            inherited txt_Piso: TNumericEdit
              OnChange = OnChangeGenerico
            end
            inherited txt_Detalle: TEdit
              OnChange = OnChangeGenerico
            end
          end
        end
      end
      object GBOtrosDatos: TGroupBox
        Left = 4
        Top = 465
        Width = 1284
        Height = 72
        Anchors = [akLeft, akTop, akRight]
        BiDiMode = bdLeftToRight
        Caption = 'Otros'
        Color = clBtnFace
        Ctl3D = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentBiDiMode = False
        ParentColor = False
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 5
        DesignSize = (
          1284
          72)
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 90
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = 'Ubicaci'#243'n carpeta:'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
        end
        object lblEnListaAmarilla: TLabel
          Left = 554
          Top = 40
          Width = 200
          Height = 14
          Alignment = taCenter
          AutoSize = False
          Color = clYellow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Visible = False
        end
        object txt_UbicacionFisicaCarpeta: TEdit
          Left = 100
          Top = 13
          Width = 170
          Height = 21
          BiDiMode = bdLeftToRight
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 15
          ParentBiDiMode = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnChange = OnChangeGenerico
        end
        object cb_RecInfoMail: TCheckBox
          Left = 419
          Top = 16
          Width = 116
          Height = 17
          Hint = #191'Quiere recibir Informaci'#243'n por email?'
          Caption = 'Recibir Info por Mail'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnClick = OnChangeGenerico
        end
        object btn_DocRequerido: TButton
          Left = 1176
          Top = 14
          Width = 96
          Height = 20
          Anchors = [akTop, akRight]
          Caption = '&Documentaci'#243'n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          OnClick = btn_DocRequeridoClick
        end
        object cb_RecibeClavePorMail: TCheckBox
          Left = 551
          Top = 16
          Width = 132
          Height = 17
          Hint = 'Recibe Clave Por eMail'
          Caption = 'Recibe Clave Por Mail'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          OnClick = OnChangeGenerico
        end
        object chkInactivaDomicilioRNUT: TCheckBox
          Left = 684
          Top = 16
          Width = 159
          Height = 17
          Caption = 'Desactiva Domicilios RNUT'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          Visible = False
          OnClick = chkInactivaDomicilioRNUTClick
        end
        object chkInactivaMediosComunicacionRNUT: TCheckBox
          Left = 849
          Top = 16
          Width = 156
          Height = 17
          Caption = 'Desactiva Contactos RNUT'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          Visible = False
          OnClick = chkInactivaMediosComunicacionRNUTClick
        end
        object cb_NoGenerarIntereses: TCheckBox
          Left = 270
          Top = 16
          Width = 121
          Height = 17
          Hint = 'No generar Intereses en la facturaci'#243'n para este Convenio.'
          Caption = 'No generar Intereses'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          Visible = False
          OnClick = OnChangeGenerico
        end
      end
      object Panel6: TPanel
        Left = 6
        Top = 40
        Width = 131
        Height = 16
        BevelOuter = bvNone
        Caption = 'Datos Personales'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object pnl_RepLegal: TPanel
        Left = 415
        Top = 132
        Width = 370
        Height = 26
        BevelOuter = bvNone
        TabOrder = 1
        object lbl_RepresentanteLegal: TLabel
          Left = 122
          Top = 6
          Width = 261
          Height = 14
          AutoSize = False
          Caption = 'Representante legal'
        end
        object btn_RepresentanteLegal: TButton
          Left = 0
          Top = 0
          Width = 115
          Height = 26
          Align = alLeft
          Caption = '&Representante Legal'
          TabOrder = 0
          OnClick = btn_RepresentanteLegalClick
        end
      end
      object gbVehiculos: TGroupBox
        Left = 5
        Top = 537
        Width = 1283
        Height = 162
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = ' Veh'#237'culos '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        DesignSize = (
          1283
          162)
        object dblVehiculos: TDBListEx
          Left = 5
          Top = 38
          Width = 1275
          Height = 121
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Patente'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblVehiculosColumnsHeaderClick
              FieldName = 'Patente'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 85
              Header.Caption = 'Nro. de Tag'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = dblVehiculosColumnsHeaderClick
              FieldName = 'SerialNumberaMostrar'
            end
            item
              Alignment = taCenter
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Microsoft Sans Serif'
              Font.Style = [fsUnderline]
              Width = 65
              Header.Caption = 'Img Ref'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = True
              FieldName = 'ImagenReferencia'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 30
              Header.Caption = 'LV'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'IndicadorListaVerde'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 30
              Header.Caption = 'LN'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'IndicadorListaNegra'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 110
              Header.Caption = 'Fecha Alta'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = dblVehiculosColumnsHeaderClick
              FieldName = 'FechaCreacion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 110
              Header.Caption = 'Fecha Baja'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaBajaCuenta'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Microsoft Sans Serif'
              Font.Style = []
              Width = 110
              Header.Caption = 'Fecha Suspensi'#243'n'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'FechaSuspendida'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 70
              Header.Caption = 'Estado'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = dblVehiculosColumnsHeaderClick
              FieldName = 'Estado'
            end
            item
              Alignment = taCenter
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 80
              Header.Caption = 'Almacen'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Almacen'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 135
              Header.Caption = 'Marca'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblVehiculosColumnsHeaderClick
              FieldName = 'DescripcionMarca'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 135
              Header.Caption = 'Modelo'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblVehiculosColumnsHeaderClick
              FieldName = 'Modelo'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 35
              Header.Caption = 'A'#241'o'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = dblVehiculosColumnsHeaderClick
              FieldName = 'AnioVehiculo'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 110
              Header.Caption = 'Vto Garantia'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaVencimientoGarantia'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Microsoft Sans Serif'
              Font.Style = []
              Width = 200
              Header.Caption = 'Convenio de Facturaci'#243'n'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroConvenioFacturacion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Microsoft Sans Serif'
              Font.Style = []
              Width = 90
              Header.Caption = 'Suscrito'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'SuscritoListaBlanca'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Microsoft Sans Serif'
              Font.Style = []
              Width = 90
              Header.Caption = 'Habilitado'
              Header.Font.Charset = ANSI_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Microsoft Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'HabilitadoListaBlanca'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Cat. Interurbana'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'CategoriaInterurbana'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Cat. Urbana'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'CategoriaUrbana'
            end>
          DataSource = DSVehiculosContacto
          DragReorder = True
          ParentColor = False
          TabOrder = 2
          TabStop = True
          OnDblClick = dblVehiculosDblClick
          OnDrawText = dblVehiculosDrawText
          OnLinkClick = dblVehiculosLinkClick
        end
        object cbMostrarVehiculosEliminados: TCheckBox
          Left = 1083
          Top = 15
          Width = 185
          Height = 17
          Anchors = [akTop, akRight]
          Caption = '&Mostrar Veh'#237'culos dados de Baja'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = cbMostrarVehiculosEliminadosClick
        end
        object pnl_BotonesVehiculo: TPanel
          Left = 3
          Top = 13
          Width = 1074
          Height = 23
          Anchors = [akLeft, akTop, akRight]
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object btnAgregarVehiculo: TButton
            Left = 0
            Top = 3
            Width = 75
            Height = 20
            Hint = 'Agregar Veh'#237'culo'
            Caption = '&Agregar'
            TabOrder = 0
            OnClick = btnAgregarVehiculoClick
          end
          object btnEditarVehiculo: TButton
            Left = 76
            Top = 3
            Width = 75
            Height = 20
            Hint = 'Editar Veh'#237'culo'
            Caption = '&Editar'
            TabOrder = 1
            OnClick = btnEditarVehiculoClick
          end
          object btnEliminarVehiculo: TButton
            Left = 152
            Top = 3
            Width = 75
            Height = 20
            Hint = 'Eliminar Veh'#237'culo'
            Caption = 'E&liminar'
            TabOrder = 2
            OnClick = btnEliminarVehiculoClick
          end
          object btn_Cambiar: TButton
            Left = 226
            Top = 3
            Width = 75
            Height = 20
            Hint = 'Cambia el Televia de una Cuenta'
            Caption = 'Cambiar &Tag.'
            TabOrder = 3
            OnClick = btn_CambiarClick
          end
          object btn_CambioVehiculo: TButton
            Left = 301
            Top = 3
            Width = 75
            Height = 20
            Hint = 'Cambiar Veh'#237'culo del Telev'#237'a'
            Caption = '&Cambio Veh.'
            TabOrder = 4
            OnClick = btn_CambioVehiculoClick
          end
          object btn_Suspender: TButton
            Left = 999
            Top = 3
            Width = 75
            Height = 20
            Hint = 'Suspende una Cuenta'
            Caption = '&Suspender'
            TabOrder = 11
            Visible = False
            OnClick = btn_SuspenderClick
          end
          object btn_Perdido: TButton
            Left = 379
            Top = 3
            Width = 75
            Height = 20
            Hint = 'Gestiona la Perdida de un Televia'
            Caption = '&Perdido'
            TabOrder = 5
            OnClick = btn_PerdidoClick
          end
          object btn_EximirFacturacion: TButton
            Left = 455
            Top = 3
            Width = 75
            Height = 20
            Hint = 'Eximir de facturaci'#243'n a los tr'#225'nsitos del veh'#237'culo'
            Caption = 'E&ximir'
            TabOrder = 6
            OnClick = btn_EximirFacturacionClick
          end
          object btn_BonificarFacturacion: TButton
            Left = 531
            Top = 3
            Width = 76
            Height = 20
            Hint = 'Colocar una bonificaci'#243'n para los tr'#225'nsitos del veh'#237'culo'
            Caption = 'Bo&nificar'
            TabOrder = 7
            OnClick = btn_BonificarFacturacionClick
          end
          object btnConvFactVehiculo: TButton
            Left = 610
            Top = 3
            Width = 123
            Height = 20
            Hint = 'Editar Convenio de Facturaci'#243'n por Veh'#237'culo'
            Caption = '&Convenio Facturaci'#243'n'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 8
            OnClick = btnConvFactVehculoClick
          end
          object btnSuscribirLista: TButton
            Left = 736
            Top = 3
            Width = 123
            Height = 20
            Hint = 'Editar Convenio de Facturaci'#243'n por Veh'#237'culo'
            Caption = 'Suscribir &Lista Blanca'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 9
            OnClick = btnSuscribirListaClick
          end
          object btnDesuscribirLista: TButton
            Left = 862
            Top = 3
            Width = 123
            Height = 20
            Hint = 'Editar Convenio de Facturaci'#243'n por Veh'#237'culo'
            Caption = '&DeSuscribir Lista Blanca'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 10
            OnClick = btnDesuscribirListaClick
          end
        end
      end
      object GBFacturacion: TGroupBox
        Left = 4
        Top = 351
        Width = 1285
        Height = 116
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Datos de Facturaci'#243'n'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        DesignSize = (
          1285
          116)
        object lbl1: TLabel
          Left = 8
          Top = 17
          Width = 285
          Height = 13
          Caption = #191'Desea recibir la factura mensual en su domicilio Particular? '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbl_OtroDomicilio: TLabel
          Left = 543
          Top = 17
          Width = 637
          Height = 13
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'aca va la descripci'#243'n del Otro domicilio...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 410
        end
        object lbl_PagoAutomatico: TLabel
          Left = 8
          Top = 36
          Width = 126
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = 'Forma de pago autom'#225'tico'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
        end
        object lbl_DescripcionMedioPago: TLabel
          Left = 338
          Top = 41
          Width = 805
          Height = 13
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'aca va la descripci'#242'n del m.pago automatico'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          ExplicitWidth = 577
        end
        object lbl2: TLabel
          Left = 8
          Top = 64
          Width = 166
          Height = 13
          Caption = 'Tipo Documento Electronico:'
        end
        object lblConvenioFacturacion: TLabel
          Left = 8
          Top = 91
          Width = 147
          Height = 13
          Caption = 'Convenio de Facturaci'#243'n:'
        end
        object lbl4: TLabel
          Left = 497
          Top = 91
          Width = 120
          Height = 13
          Caption = 'RUT de Facturaci'#243'n:'
        end
        object lbl_NombreRutFacturacion: TLabel
          Left = 876
          Top = 91
          Width = 637
          Height = 13
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'Aca Nombre RUT de Facturaci'#243'n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 409
        end
        object lblGrupoFacturacion: TLabel
          Left = 619
          Top = 66
          Width = 115
          Height = 13
          BiDiMode = bdRightToLeft
          Caption = 'lblGrupoFacturacion'
          ParentBiDiMode = False
        end
        object RBDomicilioEntregaOtro: TRadioButton
          Left = 334
          Top = 15
          Width = 49
          Height = 17
          Hint = 
            'Indica que otro es el de entrega de correspondencia (ver o ingre' +
            'sar los datos con el bot'#243'n Editar Domicilio)'
          Caption = '&Otro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = RBDomicilioEntregaClick
        end
        object RBDomicilioEntrega: TRadioButton
          Left = 297
          Top = 15
          Width = 31
          Height = 17
          Hint = 
            'El domicilio ingresado es el de entrega de correspondencia (fact' +
            'uraci'#243'n)'
          Caption = 'Si'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          TabStop = True
          OnClick = RBDomicilioEntregaClick
        end
        object BtnEditarDomicilioEntrega: TButton
          Left = 380
          Top = 15
          Width = 155
          Height = 20
          Hint = 'Editar domicilio de facturaci'#243'n'
          Caption = 'Editar &domicilio de facturaci'#243'n'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtnEditarDomicilioEntregaClick
        end
        object cb_PagoAutomatico: TVariantComboBox
          Left = 143
          Top = 36
          Width = 105
          Height = 21
          Hint = 'Indique si utiliza una forma de pago autom'#225'tica'
          Style = vcsDropDownList
          Color = 16444382
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnChange = cb_PagoAutomaticoChange
          Items = <>
        end
        object btn_EditarPagoAutomatico: TButton
          Left = 248
          Top = 37
          Width = 80
          Height = 20
          Hint = 'Editar medio de pago autom'#225'tico'
          Caption = 'Editar &pago'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = btn_EditarPagoAutomaticoClick
        end
        object vcbTipoDocumentoElectronico: TVariantComboBox
          Left = 194
          Top = 63
          Width = 159
          Height = 21
          Style = vcsDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 5
          OnChange = vcbTipoDocumentoElectronicoChange
          Items = <>
        end
        object btn_ConvenioFacturacion: TButton
          Left = 346
          Top = 89
          Width = 145
          Height = 20
          Hint = 'Seleccionar Convenios de Facturaci'#243'n por cuenta'
          Anchors = [akLeft, akBottom]
          Caption = 'Convenio de Facturaci'#243'n '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          OnClick = btn_ConvenioFacturacionClick
        end
        object txt_NumeroConvenioFacturacion: TEdit
          Left = 180
          Top = 88
          Width = 159
          Height = 21
          Hint = 'N'#250'mero de Convenio de Facturaci'#243'n'
          Color = 16444382
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 8
        end
        object btn_MedioDoc: TButton
          Left = 1200
          Top = 88
          Width = 82
          Height = 20
          Hint = 'Medio de env'#237'o de los documentos de cobro'
          Anchors = [akRight, akBottom]
          Caption = '&Medio Env'#237'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 12
          OnClick = btn_MedioDocClick
        end
        object txt_RUTFacturacion: TEdit
          Left = 621
          Top = 87
          Width = 117
          Height = 21
          Hint = 'Numero de Documento de Facturaci'#243'n'
          Color = 16444382
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 10
        end
        object btn_RUTFacturacion: TButton
          Left = 740
          Top = 89
          Width = 130
          Height = 20
          Hint = 'Seleccionar Convenios de Facturaci'#243'n por cuenta'
          Anchors = [akLeft, akBottom]
          Caption = 'RUT de Facturaci'#243'n '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
          OnClick = btn_RUTFacturacionClick
        end
        object chkCtaComercialPredeterminada: TCheckBox
          Left = 1060
          Top = 63
          Width = 214
          Height = 17
          Anchors = [akTop, akRight]
          Caption = 'Cuenta Comercial Predeterminada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
          Visible = False
          OnClick = chkCtaComercialPredeterminadaClick
        end
        object btnGrupoFacturacion: TButton
          Left = 740
          Top = 63
          Width = 130
          Height = 20
          Caption = 'Grupo de Facturaci'#243'n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          OnClick = btnGrupoFacturacionClick
        end
      end
      object cbbTipoConvenio: TVariantComboBox
        Left = 124
        Top = 13
        Width = 160
        Height = 21
        Hint = 'Tipo de Convenio de Facturaci'#243'n'
        Style = vcsDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        Items = <>
      end
      object cbbTipoCliente: TVariantComboBox
        Left = 401
        Top = 13
        Width = 160
        Height = 21
        Hint = 'Tipo de Convenio de Facturaci'#243'n'
        Style = vcsDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        Visible = False
        OnChange = cbbTipoClienteChange
        Items = <>
      end
    end
    object pnl_Botones: TPanel
      Left = 0
      Top = 710
      Width = 1295
      Height = 40
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object lbl_EstadoSolicitud: TLabel
        Left = 8
        Top = 10
        Width = 193
        Height = 13
        AutoSize = False
        Caption = 'lbl_EstadoSolicitud'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NB_Botones: TNotebook
        Left = 775
        Top = 0
        Width = 520
        Height = 40
        Align = alRight
        PageIndex = 1
        TabOrder = 1
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object btn_Salir: TButton
            Left = 576
            Top = 9
            Width = 95
            Height = 25
            Caption = '&Salir'
            TabOrder = 3
            OnClick = btn_SalirClick
          end
          object btn_ReImprimir: TButton
            Left = 316
            Top = 9
            Width = 95
            Height = 25
            Hint = 'Re Imprimir'
            Caption = 'Re &Imprimir'
            TabOrder = 1
            OnClick = btn_ReImprmirModifClick
          end
          object btn_BlanquearClaveConsulta: TButton
            Left = 213
            Top = 9
            Width = 95
            Height = 25
            Hint = 'Blanquear Clave'
            Caption = '&Blanquear Clave'
            TabOrder = 0
            OnClick = btn_BlanquearClaveConsultaClick
          end
          object btn_CancelarConsulta: TButton
            Left = 420
            Top = 9
            Width = 95
            Height = 25
            Hint = 'Cancelar'
            Cancel = True
            Caption = '&Cancelar'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = btn_CancelarConsultaClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageContrato'
          object btn_Guardar: TButton
            Left = 320
            Top = 8
            Width = 95
            Height = 25
            Hint = 'Imprimir / Guardar'
            Caption = '&Guardar'
            Default = True
            ParentShowHint = False
            ShowHint = False
            TabOrder = 3
            OnClick = btn_GuardarClick
          end
          object btn_CancelarContrato: TButton
            Left = 584
            Top = 8
            Width = 92
            Height = 25
            Hint = 'Cancelar Carga de Contrato'
            Cancel = True
            Caption = '&Cancelar'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnClick = btn_CancelarContratoClick
          end
          object btn_Baja_Contrato: TButton
            Left = 112
            Top = 8
            Width = 95
            Height = 25
            Caption = '&Baja Convenio'
            TabOrder = 1
            OnClick = btn_Baja_ContratoClick
          end
          object btn_ReImprmirModif: TButton
            Left = 215
            Top = 8
            Width = 95
            Height = 25
            Hint = 'Re Imprimir'
            Caption = 'Re &Imprimir'
            TabOrder = 2
            OnClick = btn_ReImprmirModifClick
          end
          object btn_blanquearClaveModif: TButton
            Left = 9
            Top = 8
            Width = 95
            Height = 25
            Caption = '&Blanquear Clave'
            TabOrder = 0
            OnClick = btn_BlanquearClaveConsultaClick
          end
          object btn_CancelarModificar: TButton
            Left = 421
            Top = 8
            Width = 95
            Height = 25
            Cancel = True
            Caption = '&Cancelar'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = btn_CancelarConsultaClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 2
          Caption = 'PageAlta'
          object btn_GuardarAlta: TButton
            Left = 308
            Top = 9
            Width = 95
            Height = 25
            Hint = 'Guardar'
            Caption = '&Guardar'
            Default = True
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
          end
          object btn_CancelarContrato_Alta: TButton
            Left = 584
            Top = 8
            Width = 92
            Height = 25
            Hint = 'Cancelar Carga de Contrato'
            Cancel = True
            Caption = '&Cancelar'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = btn_CancelarContratoClick
          end
          object btn_CancelarGuardar: TButton
            Left = 413
            Top = 9
            Width = 95
            Height = 25
            Hint = 'Cancelar'
            Cancel = True
            Caption = '&Cancelar'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = btn_CancelarConsultaClick
          end
        end
      end
      object debuger: TButton
        Left = 136
        Top = 8
        Width = 75
        Height = 25
        Caption = 'debuger'
        TabOrder = 0
        Visible = False
        OnClick = debugerClick
      end
    end
  end
  object DSVehiculosContacto: TDataSource
    Left = 872
    Top = 616
  end
  object InsertarHistoricoConvenio: TADOStoredProc
    AutoCalcFields = False
    CacheSize = 50
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    MarshalOptions = moMarshalModifiedOnly
    CommandTimeout = 3000
    EnableBCD = False
    ProcedureName = 'InsertarHistoricoConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 16
    Top = 640
  end
  object BajaConvenioCompleto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BajaConvenioCompleto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 56
    Top = 640
  end
  object BlanquearPasswordPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BlanquearPasswordPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = Null
      end>
    Left = 88
    Top = 640
  end
  object PopupMenu: TPopupMenu
    Left = 825
    Top = 110
    object mnu_EnviarMail: TMenuItem
      Caption = 'Enviar &Mail'
      OnClick = mnu_EnviarMailClick
    end
  end
  object spCancelarUltimoAnexo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CancelarUltimoAnexoConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@AnexoCancelado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 144
    Top = 640
  end
  object Imagenes: TImageList
    Left = 581
    Top = 633
    Bitmap = {
      494C010106000800140410001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C9E9C00848684009C9E9C00CECFCE00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000848684009C9E9C009C9E9C009C9E9C0084868400CECFCE000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848684009C9E9C009C9E9C00B5B6B5009C9E9C009C9E9C0084868400CECF
      CE000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C9E
      9C009C9E9C009C9E9C00FFFFFF00B5B6B500FFFFFF009C9E9C009C9E9C009C9E
      9C00CECFCE0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008486
      84009C9E9C00B5B6B500B5B6B500FFFFFF00B5B6B500B5B6B5009C9E9C008486
      8400CECFCE0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C9E
      9C009C9E9C009C9E9C00FFFFFF00B5B6B500FFFFFF009C9E9C009C9E9C009C9E
      9C00CECFCE0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848684009C9E9C009C9E9C00B5B6B5009C9E9C009C9E9C0084868400CECF
      CE000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000848684009C9E9C009C9E9C009C9E9C0084868400CECFCE000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C9E9C00848684009C9E9C00CECFCE00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000848684006361630084868400CECFCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000029B60000219E000029B60000CECFCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000636163004A494A0063616300CECFCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000052CFFF0018AEFF0052CFFF00CECFCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000063616300E7E7E700E7E7E700E7E7E70063616300CECFCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000219E000029B6000029B6000029B60000219E0000CECFCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004A494A006361630063616300636163004A494A00CECFCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000018AEFF0052CFFF0052CFFF0052CFFF0018AEFF00CECFCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000063616300E7E7E700FFFFFF00FFFFFF00FFFFFF00E7E7E70063616300CECF
      CE00000000000000000000000000000000000000000000000000000000000000
      0000219E000029B6000029B6000000E7730029B6000029B60000219E0000CECF
      CE00000000000000000000000000000000000000000000000000000000000000
      00004A494A0063616300636163009C9E9C0063616300636163004A494A00CECF
      CE00000000000000000000000000000000000000000000000000000000000000
      000018AEFF0052CFFF008CE7FF00CEE7EF008CE7FF0052CFFF0018AEFF00CECF
      CE00000000000000000000000000000000000000000000000000000000006361
      6300E7E7E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7E7008486
      8400CECFCE0000000000000000000000000000000000000000000000000029B6
      000029B6000029B60000FFFFFF0000E77300FFFFFF0029B6000029B6000029B6
      0000CECFCE000000000000000000000000000000000000000000000000006361
      63006361630063616300FFFFFF009C9E9C00FFFFFF0063616300636163006361
      6300CECFCE0000000000000000000000000000000000000000000000000052CF
      FF0052CFFF0084DFFF00FFFFFF00CEE7EF00FFFFFF008CE7FF0052CFFF0052CF
      FF00CECFCE000000000000000000000000000000000000000000000000009C9E
      9C00E7E7E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7E7006361
      6300CECFCE00000000000000000000000000000000000000000000000000219E
      000029B6000000E7730000E77300FFFFFF0000E7730000E7730029B60000219E
      0000CECFCE000000000000000000000000000000000000000000000000004A49
      4A00636163009C9E9C009C9E9C00FFFFFF009C9E9C009C9E9C00636163004A49
      4A00CECFCE0000000000000000000000000000000000000000000000000018AE
      FF0052CFFF00C6E7F700C6E7F700FFFFFF00CEE7EF00CEE7EF0052CFFF0018AE
      FF00CECFCE000000000000000000000000000000000000000000000000006361
      6300E7E7E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7E7008486
      8400CECFCE0000000000000000000000000000000000000000000000000029B6
      000029B6000029B60000FFFFFF0000E77300FFFFFF0029B6000029B6000029B6
      0000CECFCE000000000000000000000000000000000000000000000000006361
      63006361630063616300FFFFFF009C9E9C00FFFFFF0063616300636163006361
      6300CECFCE0000000000000000000000000000000000000000000000000052CF
      FF0052CFFF008CE7FF00FFFFFF00CEE7EF00FFFFFF008CE7FF0052CFFF0052CF
      FF00CECFCE000000000000000000000000000000000000000000000000000000
      000063616300E7E7E700FFFFFF00FFFFFF00FFFFFF00E7E7E70063616300CECF
      CE00000000000000000000000000000000000000000000000000000000000000
      0000219E000029B6000029B6000000E7730029B6000029B60000219E0000CECF
      CE00000000000000000000000000000000000000000000000000000000000000
      00004A494A0063616300636163009C9E9C0063616300636163004A494A00CECF
      CE00000000000000000000000000000000000000000000000000000000000000
      000018AEFF0052CFFF008CE7FF00CEE7EF008CE7FF0052CFFF0018AEFF00CECF
      CE00000000000000000000000000000000000000000000000000000000000000
      00000000000063616300E7E7E700E7E7E700E7E7E70063616300CECFCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000219E000029B6000029B6000029B60000219E0000CECFCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004A494A006361630063616300636163004A494A00CECFCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000018AEFF0052CFFF0052CFFF0052CFFF0018AEFF00CECFCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000636163009C9E9C0063616300CECFCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000029B60000219E000029B60000CECFCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000636163004A494A0063616300CECFCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000052CFFF0018AEFF0052CFFF00CECFCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFF800100000000
      FFFF800100000000FFFF800100000000FC3F800100000000F81F800100000000
      F00F800100000000E007800100000000E007800100000000E007800100000000
      F00F800100000000F81F800100000000FC3F800100000000FFFF800100000000
      FFFF800100000000FFFFFFFF00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3FFC3FFC3FFC3FF81FF81FF81FF81F
      F00FF00FF00FF00FE007E007E007E007E007E007E007E007E007E007E007E007
      F00FF00FF00FF00FF81FF81FF81FF81FFC3FFC3FFC3FFC3FFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object spConvenio_BloquearConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'Convenio_BloquearConvenio'
    Parameters = <>
    Left = 400
    Top = 640
  end
  object spConvenio_ActualizarGenerarIntereses: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'Convenio_ActualizarGenerarIntereses;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EstadoGenerarIntereses'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@UsuarioSistema'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 352
    Top = 640
  end
  object SPObtenerUltimosConsumos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerUltimosConsumos'
    Parameters = <>
    Left = 184
    Top = 640
  end
  object SPAgregarAuditoriaConveniosCAC: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarAuditoriaConveniosCAC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Operacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 12
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Etiqueta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@HostName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@Accion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end>
    Left = 224
    Top = 640
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 543
    Top = 633
    Bitmap = {
      494C01010200040014040F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFC000000008003000400000000BFFB7FF400000000BFFB77F400000000
      BFFB63F400000000BFFB41F400000000BFFB48F400000000BFFB5C7400000000
      BFFB7E3400000000BFFB7F1400000000BFFB7F9400000000BFFB7FD400000000
      BFFB7FF4000000008003000400000000FFFFFFFC000000000000000000000000
      0000000000000000000000000000}
  end
  object spRegistraAuditoriaDomicilios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistraAuditoriaDomicilios'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@InactivaDomicilioRNUT'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@UsuarioSistema'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 440
    Top = 638
  end
  object spRegistraAuditoriaMediosDeContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistraAuditoriaMediosDeContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMedioComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@InactivaMedioContactoRNUT'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@UsuarioSistema'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 472
    Top = 638
  end
  object spExistenMovimientosFacturacionInmediata: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ExistenMovimientosFacturacionInmediata'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TieneMovimientos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 504
    Top = 638
  end
  object spEliminarRepresentantesConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarRepresentantesConvenios'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoClienteConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 720
    Top = 80
  end
  object tmrBloquearConvenio: TTimer
    Enabled = False
    Interval = 300000
    OnTimer = tmrBloquearConvenioTimer
    Left = 1248
    Top = 8
  end
  object cdsVehiculosContacto: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoSolicitudContacto'
        DataType = ftInteger
      end
      item
        Name = 'CodigoVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'TipoPatente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Patente'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoMarca'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionMarca'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Modelo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoColor'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionColor'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AnioVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'ContextMark'
        DataType = ftSmallint
      end
      item
        Name = 'ContractSerialNumber'
        DataType = ftBCD
        Precision = 18
        Size = 4
      end
      item
        Name = 'DigitoVerificadorPatente'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TieneAcoplado'
        DataType = ftInteger
      end
      item
        Name = 'DigitoVerificadorValido'
        DataType = ftInteger
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'SerialNumberaMostrar'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoConcesionaria'
        DataType = ftSmallint
      end
      item
        Name = 'Vendido'
        DataType = ftBoolean
      end
      item
        Name = 'Suspendido'
        DataType = ftBoolean
      end
      item
        Name = 'TagEnCliente'
        DataType = ftBoolean
      end
      item
        Name = 'TeleviaNuevo'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoEstadoConservacion'
        DataType = ftInteger
      end
      item
        Name = 'Robado'
        DataType = ftBoolean
      end
      item
        Name = 'Perdido'
        DataType = ftBoolean
      end
      item
        Name = 'Estado'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'IndicadorListaVerde'
        DataType = ftBoolean
      end
      item
        Name = 'IndicadorListaNegra'
        DataType = ftBoolean
      end
      item
        Name = 'VehiculoEliminado'
        DataType = ftBoolean
      end
      item
        Name = 'FechaBajaCuenta'
        DataType = ftDateTime
      end
      item
        Name = 'FechaCreacion'
        DataType = ftDateTime
      end
      item
        Name = 'FechaVencimientoGarantia'
        DataType = ftDateTime
      end
      item
        Name = 'Almacen'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FechaSuspendida'
        DataType = ftDateTime
      end
      item
        Name = 'ImagenReferencia'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoConvenioFacturacion'
        DataType = ftLargeint
      end
      item
        Name = 'NumeroConvenioFacturacion'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Foraneo'
        DataType = ftBoolean
      end
      item
        Name = 'SuscritoListaBlanca'
        DataType = ftBoolean
      end
      item
        Name = 'HabilitadoListaBlanca'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoCategoriaInterurbana'
        DataType = ftSmallint
      end
      item
        Name = 'CodigoCategoriaUrbana'
        DataType = ftSmallint
      end
      item
        Name = 'CategoriaInterurbana'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'CategoriaUrbana'
        DataType = ftString
        Size = 60
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterScroll = cdsVehiculosContactoAfterScroll
    Left = 792
    Top = 608
  end
end
