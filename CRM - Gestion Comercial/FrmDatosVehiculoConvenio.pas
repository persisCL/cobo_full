unit FrmDatosVehiculoConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, DmiCtrls, MaskCombo, PeaProcs,
  DMCOnnection, Util, UtilProc, peatypes,
  VariantComboBox, DB, ADODB, DBClient, RStrings, UtilDB, Mensajes, FrmObservacionesGeneral;

type
    TFormDatosVehiculoConvenio = class(TForm)
    pnlBotones: TPanel;
    btnAceptar: TDPSButton;
    btnCancelar: TDPSButton;
    GBVehiculo: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label47: TLabel;
    cb_color: TComboBox;
    cb_marca: TComboBox;
    txt_anio: TNumericEdit;
    lblTipoVehiculo: TLabel;
    cbTipoVehiculo: TVariantComboBox;
    txtDescripcionModelo: TEdit;
    txtPatente: TEdit;
    VerificarFormatoPatenteChilena: TADOStoredProc;
    txtDigitoVerificador: TEdit;
    cbConAcoplado: TCheckBox;
    GBCuenta: TGroupBox;
    lblTelevia: TLabel;
    txtNumeroTelevia: TEdit;
    cb_EstadoTelevia: TVariantComboBox;
    Label1: TLabel;
    Label2: TLabel;
    cb_TipoConvenioTelevia: TVariantComboBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    ComboBox1: TComboBox;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure txtPatenteKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure txtPatenteExit(Sender: TObject);
    procedure txt_anioExit(Sender: TObject);
    procedure txtDigitoVerificadorKeyPress(Sender: TObject; var Key: Char);
    procedure txtDigitoVerificadorExit(Sender: TObject);
    procedure txtNumeroTeleviaExit(Sender: TObject);
    procedure cbTipoVehiculoChange(Sender: TObject);
  private
    MiListaPatente: TStringList;
    FCodigoSolicitud: integer; 
    FObservaciones: string;
    FEsConvenio: Boolean;
    FPuedeValidar: Boolean;
    FContextMark: integer;
    FCodigoPuntoEntrega: integer;
    FListaTags: TStringList;
    FNumeroTag: string;
    FAsignarTag: boolean;
    Function GetTipoPatente: AnsiString;
    Procedure SetTipoPatente(Const Value: AnsiString);
    Function GetPatente: AnsiString;
    Procedure SetPatente(Const Value: AnsiString);
    Function GetCodigoMarca: integer;
    Procedure SetCodigoMarca(Const Value: integer);
    Function GetDescripcionMarca: AnsiString;
    Procedure SetCodigoTipo(Const Value: integer);
    Function GetCodigoTipo: integer;
    Function GetDescripcionTipo: AnsiString;
    Function GetDescripcionModelo: AnsiString;
    Procedure SetDescripcionModelo(Const Value: AnsiString);
    Function GetAnio: integer;
    Procedure SetAnio(Const Value: integer);
    function GetCategoria: integer;
    Procedure SetCodigoColor(Const Value: integer);
    Function GetDescripcionColor: AnsiString;
    function GetTieneAcoplado: boolean;
    function GetDetalleVehiculoSimple: AnsiString;
    function GetDigitoVerificador: AnsiString;
    function ValidarDigitoVerificador: boolean;
    function GetDetalleVehiculoCompleta: AnsiString;
    function BuscarPatenteSolicitud: boolean;
    function BuscarPatenteCuenta: boolean;
    function ValidarAnio: boolean;
    function ValidarPatenteChilena(EstaGuardando: boolean):boolean;
    function ValidarSolicitud: boolean;
    function ValidarConvenio: boolean;
    function GetCodigoColor: integer;
    function GetSerialNumber: DWORD;
    function GetTag: Ansistring;
    function ValidarDigitoVerificadorTag(NumeroTag:String):Boolean;
    { Private declarations }
  public
    { Public declarations }
    Property TipoPatente: AnsiString read GetTipoPatente;
    Property Patente: AnsiString read GetPatente;
    property Marca: integer read GetCodigoMarca;
    property DescripcionMarca: AnsiString read GetDescripcionMarca;
    property CodigoTipo: integer read GetCodigoTipo;
    property DescripcionTipo: AnsiString read GetDescripcionTipo;
    property DescripcionModelo: AnsiString read GetDescripcionModelo;
    property Anio: integer read GetAnio;
    property Color: integer read GetCodigoColor;
    property DescripcionColor: AnsiString read GetDescripcionColor;
    property CodigoCategoria: integer read GetCategoria;
    property DigitoVerificadorCorrecto: boolean read ValidarDigitoVerificador;
    property DigitoVerificador: AnsiString read GetDigitoVerificador;
    property DetalleVehiculoSimple: Ansistring read GetDetalleVehiculoSimple;
    property TieneAcoplado: boolean read GetTieneAcoplado;
    property DetalleVehiculoCompleto: Ansistring read GetDetalleVehiculoCompleta;
    property Observaciones: Ansistring read FObservaciones;
    property SerialNumber:DWORD read GetSerialNumber;
    property ContextMark: integer read FContextMark;
    property NumeroTag:AnsiString read GetTag;
    Function Inicializar(CaptionAMostrar: string; CodigoTipoVehiculo: integer = 1;  Solicitud: integer = -1; ListaPatentes: TStringList = nil;
        EsConvenio: boolean = False; CodigoPuntoEntrega: integer = -1; ListaTags: TStringList = nil; ContextMark: integer = CONTEXT_MARK): Boolean; overload; //Para el Alta.
    Function Inicializar(CaptionAMostrar: string; TipoPatente, Patente, Modelo: AnsiString;
                    CodigoMarca, Anio, CodigoTipo, CodigoColor: integer;
                    DigitoVerificador: string;
                    TieneAcoplado: boolean = False;
                    Observaciones: string = '';
                    Solicitud: integer = -1;
                    ListaPatentes: TStringList = nil;
                    EsConvenio: boolean = False;
                    CodigoPuntoEntrega: integer = -1; ListaTags: TStringList = nil; ContextMark: integer = CONTEXT_MARK; NumeroTag: string = ''; AsignarTag: boolean = False): Boolean; overload;
  end;

  function ValidarCategoriaTelevia(Conn :TADOConnection; CodigoTipoVehiculo: integer; TieneAcoplado:integer; ContextMark: integer; SerialNumber: DWORD): boolean;
  procedure ValidarTeleviaInterfase(Conn :TADOConnection; CodigoPuntoEntrega: integer; ContextMark: integer; SerialNumber: DWORD);
  function ValidarSituacionTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD): boolean;
  function ValidarExistenciaTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD): boolean;

ResourceString
    CAPTION_VALIDAR_DATOS_VEHICULO      = 'Validar datos del Veh�culo';
    MSG_ERROR_VALIDAR_PATENTE           = 'Debe indicar patente.';
    MSG_ERROR_VALIDAR_MARCA             = 'Debe indicar una marca.';
    MSG_VALIDAR_DIGITO_VERIFICADOR      = 'Debe ingresar un d�gito verificador.';
    MSG_ERROR_VALIDAR_ANIO              = 'El a�o es incorrecto. ' + #13#10 + 'Debe ser mayor a 1900 y menor al a�o siguiente al actual. ';
    MSG_ERROR_VALIDAR_TIPO              = 'Debe indicar un tipo.';

var
  FormDatosVehiculoConvenio: TFormDatosVehiculoConvenio;

implementation

{$R *.dfm}

Function TFormDatosVehiculoConvenio.Inicializar(CaptionAMostrar: string; CodigoTipoVehiculo: integer = 1; Solicitud: integer = -1; ListaPatentes: TStringList = nil; EsConvenio: boolean = False; CodigoPuntoEntrega: integer = -1; ListaTags: TStringList = nil; ContextMark: integer = CONTEXT_MARK): Boolean;
var
    tipoPatenteAux: AnsiString;
begin
    tipoPatenteAux := QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.CONST_TIPO_PATENTE_CHILE()');
    Self.Caption := CaptionAMostrar;
    SetTipoPatente(tipoPatenteAux);
    SetCodigoMarca(1);
    SetCodigoColor(1);
    SetCodigoTipo(CodigoTipoVehiculo);
    FCodigoSolicitud := Solicitud;
    MiListaPatente := ListaPatentes;
    cbConAcoplado.Visible := EsConvenio;
    txtDigitoVerificador.Visible := EsConvenio;
    FEsConvenio := EsConvenio;
    FCodigoPuntoEntrega:= CodigoPuntoEntrega;
    FListaTags:= ListaTags;
    FContextMark:= ContextMark;
    FAsignarTag:= False;

    FPuedeValidar := True;
    if not FEsConvenio then cbTipoVehiculo.Width := cb_marca.Width
    else begin
        lblTelevia.Visible := FEsConvenio;
        txtNumeroTelevia.Visible := FEsConvenio;
    end;

    Result := True;

end;

Function TFormDatosVehiculoConvenio.Inicializar(CaptionAMostrar: string; TipoPatente, Patente, Modelo: AnsiString;
                    CodigoMarca, Anio, CodigoTipo, CodigoColor: integer;
                    DigitoVerificador: string;
                    TieneAcoplado: boolean = False;
                    Observaciones: string = '';
                    Solicitud: integer = -1;
                    ListaPatentes: TStringList = nil;
                    EsConvenio: boolean = False;
                    CodigoPuntoEntrega: integer = -1; ListaTags: TStringList = nil; ContextMark: integer = CONTEXT_MARK; NumeroTag: string = ''; AsignarTag: boolean = False): Boolean;
begin
    self.Caption := CaptionAMostrar;
    SetTipoPatente(TipoPatente);
    SetPatente(Patente);
    SetCodigoMarca(CodigoMarca);
    SetDescripcionModelo(Modelo);
    SetCodigoColor(CodigoColor);
    SetAnio(Anio);
    SetCodigoTipo(CodigoTipo);
    cbConAcoplado.Checked := TieneAcoplado;
    FCodigoSolicitud := Solicitud;
    FObservaciones := Observaciones;
    MiListaPatente := ListaPatentes;
    cbConAcoplado.Visible := EsConvenio;
    txtDigitoVerificador.Visible := EsConvenio;
    FEsConvenio := EsConvenio;
    FPuedeValidar := True;
    FCodigoPuntoEntrega:= CodigoPuntoEntrega;
    FListaTags:= ListaTags;
    FContextMark:= ContextMark;
    FNumeroTag:= NumeroTag;
    FAsignarTag := AsignarTag;

    if not FEsConvenio then cbTipoVehiculo.Width := cb_marca.Width;
    if FEsConvenio then begin
        if trim(DigitoVerificador) <> '' then txtDigitoVerificador.Text := trim(DigitoVerificador[1]);
        //else txtDigitoVerificador.Text := ObtenerDigitoVerificadorPatente(DMConnections.BaseCAC, trim(self.Patente));
        txtNumeroTelevia.text := trim(FNumeroTag);
        lblTelevia.Visible := FEsConvenio;
        txtNumeroTelevia.Visible := FEsConvenio;
    end;


    Result := True;
end;

//TIPO DE PATENTE
Function TFormDatosVehiculoConvenio.GetTipoPatente: AnsiString;
begin
    //Result := trim(txtPatente.Text);
end;

Procedure TFormDatosVehiculoConvenio.SetTipoPatente(Const Value: AnsiString);
begin
//    if Trim(Value) = TipoPatente then Exit;
//    CargarTiposPatente(DMConnections.BaseCAC, mcPatente, Trim(Value));
end;

//PATENTE
Procedure TFormDatosVehiculoConvenio.SetPatente(Const Value: AnsiString);
begin
    //if Trim(Value) = Patente then Exit;
    txtPatente.Text := Trim(Value);
end;
Function TFormDatosVehiculoConvenio.GetPatente: AnsiString;
begin
    Result := trim(txtPatente.Text);
end;

//MARCA
Function TFormDatosVehiculoConvenio.GetCodigoMarca: integer;
begin
    Result := Ival(StrRight(cb_Marca.Text, 10));
end;

Procedure TFormDatosVehiculoConvenio.SetCodigoMarca(Const Value: integer);
begin
    if Value = Marca then Exit;
    CargarMarcasVehiculos(DMConnections.BaseCAC, cb_marca, Value);
    cb_Marca.OnChange(cb_Marca);
end;

Function TFormDatosVehiculoConvenio.GetDescripcionMarca: AnsiString;
begin
    if cb_Marca.ItemIndex < 0 then Result := ''
    else Result := Trim(StrLeft(cb_Marca.Text, 40));
end;

Procedure TFormDatosVehiculoConvenio.SetCodigoTipo(Const Value: integer);
begin
   if Value = CodigoTipo then Exit;
   CargarVehiculosTipos(DMConnections.BaseCAC, cbTipoVehiculo, False, Value);
   cbTipoVehiculo.onChange(cbTipoVehiculo);
end;

Function TFormDatosVehiculoConvenio.GetCodigoTipo: integer;
begin
    Result := Ival(cbTipoVehiculo.Value);
end;

Function TFormDatosVehiculoConvenio.GetDescripcionModelo: AnsiString;
begin
//    if cb_Modelo.ItemIndex < 0 then Result := ''
//    else Result := Trim(StrLeft(cb_Modelo.Text, 40));
    result := Trim(txtDescripcionModelo.Text);    
end;

//ANIO
Function TFormDatosVehiculoConvenio.GetAnio: integer;
begin
    Result := txt_Anio.ValueInt;
end;

Procedure TFormDatosVehiculoConvenio.SetAnio(Const Value: integer);
ResourceString
    MSG_CAPTION_VALIDAR_ANIO    = 'Validar A�o';
    MSG_ERROR_VALIDAR_ANIO      = 'El a�o especificado esta fuera del rango permitido';
begin
    if Value = Anio then Exit;
    if EsAnioCorrecto(Value) then txt_Anio.ValueInt := Value
    else MsgBox( MSG_ERROR_VALIDAR_ANIO, MSG_CAPTION_VALIDAR_ANIO, MB_ICONSTOP);
end;

//COLOR.
Function TFormDatosVehiculoConvenio.GetCodigoColor: integer;
begin
    Result := Ival(StrRight(cb_Color.Text, 10));
end;

Procedure TFormDatosVehiculoConvenio.SetCodigoColor(Const Value: integer);
begin
    if Value = Color then Exit;
    CargarColoresVehiculos(DMConnections.BaseCAC, cb_Color, Value);
end;

Function TFormDatosVehiculoConvenio.GetDescripcionColor: AnsiString;
begin
    if cb_Color.ItemIndex < 0 then Result := ''
    else Result := Trim(StrLeft(cb_Color.Text, 40));
end;

Function TFormDatosVehiculoConvenio.GetDescripcionTipo: AnsiString;
begin
    if cbTipoVehiculo.ItemIndex < 0 then Result := ''
    else Result := Trim(cbTipoVehiculo.Items[cbTipoVehiculo.ItemIndex].Caption);
end;

//DETALLES
Function TFormDatosVehiculoConvenio.GetDetalleVehiculoSimple: AnsiString;
begin
    Result := DescripcionMarca + ', ' + DescripcionModelo + ', ' + DescripcionColor;
end;

Function TFormDatosVehiculoConvenio.GetDetalleVehiculoCompleta: AnsiString;
begin
    Result := DetalleVehiculoSimple + ' Patente: ' + Patente;
end;


procedure TFormDatosVehiculoConvenio.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TFormDatosVehiculoConvenio.btnAceptarClick(Sender: TObject);
var
    Titulo, Descripcion, Comentario: string;
    AlmacenPuntoEntrega, CategoriaVehiculo: integer;

begin
    //VALIDACIONES
    //1- la sintaxis
    //2- que no exista en la solicitud
    //3- que no exista en otra solicitud

    if not ValidarPatenteChilena(true) then Exit;

	if not ValidateControls(
	  [txtPatente,
      txtPatente,
      txtDigitoVerificador,
	  cb_marca,
      txt_anio,
      txt_anio,
      txtNumeroTelevia],
	  [Trim(txtPatente.Text) <> '',
       not ValidarVehiculoOtraConcesionaria(DMConnections.BaseCAC,GetPatente),
      ((not txtDigitoVerificador.Visible) or (Trim(txtDigitoVerificador.Text) <> '')),
	  cb_marca.itemindex >= 0,
      trim(txt_anio.Text) <> '',
      EsAnioCorrecto(txt_anio.ValueInt),
      ((not txtNumeroTelevia.Visible) or ((not (trim(txtNumeroTelevia.Text) <> '')) or ((ValidarDigitoVerificadorTag(txtNumeroTelevia.Text)))))],
	  CAPTION_VALIDAR_DATOS_VEHICULO,
	  [MSG_ERROR_VALIDAR_PATENTE,
      MSG_ERROR_VEHICULO_OTRA_CONCESIONARIA,
      MSG_VALIDAR_DIGITO_VERIFICADOR,
	  MSG_ERROR_VALIDAR_MARCA,
      MSG_ERROR_VALIDAR_ANIO,
      MSG_ERROR_VALIDAR_ANIO,
      MSG_ERROR_VERIFICADOR_TAG]) then Exit;

    if txtNumeroTelevia.Visible then begin
        if not ValidarExistenciaTelevia(DMConnections.BaseCAC,FContextMark,GetSerialNumber) then
            ValidarTeleviaInterfase(DMConnections.BaseCAC, FCodigoPuntoEntrega, FContextMark, GetSerialNumber);

        AlmacenPuntoEntrega := ObtenerAlmacenPuntoEntrega(DMConnections.BaseCAC, FCodigoPuntoEntrega);

        CategoriaVehiculo := ObtenerCategoriaVehiculo(DMConnections.BaseCAC, GetCodigoTipo, GetTieneAcoplado);

        if not ValidateControls([//txtNumeroTelevia,
                                txtNumeroTelevia,
                                txtNumeroTelevia,
                                txtNumeroTelevia,
                                txtNumeroTelevia,
                                txtNumeroTelevia,
                                txtNumeroTelevia,
                                txtNumeroTelevia],
                                [//txtNumeroTelevia.Text <> '',
                                ((not (trim(txtNumeroTelevia.Text) <> '')) or (Length(txtNumeroTelevia.Text) >= 10)),
                                ((not (trim(txtNumeroTelevia.Text) <> '')) or (not ExisteCadenaEnStringList(FListaTags,Trim(intToStr(GetSerialNumber))))),
                                ((not (trim(txtNumeroTelevia.Text) <> '')) or (ValidarExistenciaTelevia(DMConnections.BaseCAC, FContextMark, GetSerialNumber))),
    //                            ValidarUbicacionTag,
                                ((not (trim(txtNumeroTelevia.Text) <> '')) or (ValidarSituacionTelevia(DMConnections.BaseCAC, FContextMark, GetSerialNumber))),
                                ((not (trim(txtNumeroTelevia.Text) <> '')) or (ValidarCategoriaTelevia(DMConnections.BaseCAC, GetCodigoTipo, iif(GetTieneAcoplado,1,0), FContextMark, GetSerialNumber))),
                                ((not (trim(txtNumeroTelevia.Text) <> '')) or (ValidarStockAlmacen(DMConnections.BaseCAC,AlmacenPuntoEntrega,CategoriaVehiculo))),
                                ((not (trim(txtNumeroTelevia.Text) <> '')) or ((ObtenerStockAlmacen(DMConnections.BaseCAC,AlmacenPuntoEntrega,CategoriaVehiculo) >= FListaTags.Count + 1)))
                                ],
                                MSG_CAPTION_ASIGNAR_TAG,
                                [//MSG_VALIDAR_CARGA_TAG,
                                 MSG_ERROR_NUMERO_TAG,
                                 MSG_ERROR_NUMERO_TAG_COVENIO,
                                 MSG_ERROR_NO_ESTA_TAG,
    //                             MSG_ERROR_UBICACION_TAG,
                                 MSG_ERROR_SITUACION_TAG,
                                 MSG_ERROR_CATEGORIA_TAG,
                                 MSG_ERROR_STOCK_TAG,
                                 MSG_ERROR_STOCK_TAG_ASIGNADOS]) then Exit;

    end;

    if (trim(txtPatente.Text) = '') then exit;
    if ExisteCadenaEnStringList(MiListaPatente, trim(txtPatente.Text)) then begin
            ObtenerMensaje(DMConnections.BaseCAC, iif(FEsConvenio, MENSAJE_PATENTE_EXISTENTE_CONVENIO, MENSAJE_PATENTE_EXISTENTE_SOLICITUD), Titulo, Descripcion, Comentario);
            MsgBox(Descripcion, Titulo, MB_ICONERROR or MB_OK);
          exit;
    end;

    if FEsConvenio and not ValidarConvenio then Exit;

    if not FEsConvenio and not ValidarSolicitud then Exit;

    ModalResult := mrOK;
end;

procedure TFormDatosVehiculoConvenio.SetDescripcionModelo(
  const Value: AnsiString);
begin
    txtDescripcionModelo.Text := value;
end;

procedure TFormDatosVehiculoConvenio.txtPatenteKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9', 'a'..'z', 'A'..'Z', #8]) then
        Key := #0;
end;

procedure TFormDatosVehiculoConvenio.FormShow(Sender: TObject);
begin
    if FAsignarTag then
        txtNumeroTelevia.SetFocus
    else
        txtPatente.setFocus;
end;

procedure TFormDatosVehiculoConvenio.txtPatenteExit(Sender: TObject);
begin
    if btnAceptar.Focused or btnCancelar.Focused then exit;
    if not ValidarPatenteChilena(False) then exit;
end;

function TFormDatosVehiculoConvenio.BuscarPatenteSolicitud: boolean;
var
    Titulo, Descripcion, Comentario: string;
    SolicitudPatente: integer;
    FObs: TFormObservacionesGeneral;
begin
    result:= false;

    //La funcion devuelve 0 si no la encontr�, con lo cual esta bien!!!
    SolicitudPatente := StrtoInt(QueryGetValue(DMConnections.BaseCAC,
                            'select dbo.ExistePatenteSolicitud('''
                            + Trim(txtPatente.Text) + ''')'));
    if SolicitudPatente > 0 then begin
        if SolicitudPatente <> FCodigoSolicitud then begin //si estoy en modificacion, la va a encontrar
            ObtenerMensaje(DMConnections.BaseCAC, MENSAJE_PATENTE_EXISTENTE, Titulo, Descripcion, Comentario);
            if MsgBox(Descripcion, Titulo, MB_ICONWARNING or MB_YESNO) <> mrYes then exit;
            Application.CreateForm(TFormObservacionesGeneral, FObs);
            if (FObs.inicializa(FObservaciones)) and (FObs.ShowModal = mrOk) then begin
                FObservaciones := FObs.Observaciones;
                FObs.Release;
            end;
        end;
    end;
    result := true;
end;

procedure TFormDatosVehiculoConvenio.txt_anioExit(Sender: TObject);
begin
    if btnCancelar.Focused then exit;
    ValidarAnio;
end;

function TFormDatosVehiculoConvenio.ValidarAnio: boolean;
begin
    result := false;
    if trim(txt_anio.Text) <> '' then begin
        if not (EsAnioCorrecto(txt_anio.ValueInt)) then begin
            MsgBoxBalloon(MSG_ERROR_VALIDAR_ANIO, CAPTION_VALIDAR_DATOS_VEHICULO, MB_ICONSTOP, txt_Anio);
//            txtPatente.SetFocus;
            exit;
        end;
    end;
    result := true;
end;

function TFormDatosVehiculoConvenio.ValidarPatenteChilena(
  EstaGuardando: boolean): boolean;
var
//    p: integer;
    patenteValida: boolean;
begin
    //Result:= False;

    if (trim(txtPatente.Text) = '') then begin
        Result:= true;
        exit;
    end;
//    p := -1;

    PatenteValida := true;
    FPuedeValidar := true;
    //1- la sintaxis
    if (Length(txtPatente.Text) = 6) and
      (UpperCase(txtPatente.text)[1] in ['A'..'Z']) and
      (UpperCase(txtPatente.text)[2] in ['A'..'Z']) and
      (txtPatente.text[3] in ['0'..'9']) and
      (txtPatente.text[4] in ['0'..'9']) and
      (txtPatente.text[5] in ['0'..'9']) and
      (txtPatente.text[6] in ['0'..'9']) then begin
        VerificarFormatoPatenteChilena.Parameters.ParamByName('@Patente').value := txtPatente.Text;
        VerificarFormatoPatenteChilena.ExecProc;
        PatenteValida := (VerificarFormatoPatenteChilena.parameters.paramByName('@Return_Value').value = 1);
        if not FEsConvenio and not PatenteValida then begin
            //MsgBoxBalloon(MSG_ERROR_VALIDAR_FORMATO_PATENTE, MSG_ERROR_PATENTE, MB_ICONSTOP, txtPatente);
            PatenteValida := True;
            if EstaGuardando then txtPatente.SetFocus;
        end else
        if  FEsConvenio and not PatenteValida then begin
            if not EstaGuardando then
                MsgBoxBalloon(MSG_NO_PUEDE_VALIDAR_PATENTE, Format(MSG_CAPTION_VALIDAR,[STR_PATENTE]), MB_ICONWARNING, txtPatente);
            FPuedeValidar := false;
            result:= true;
            exit;
        end;

    end else begin
       if not EstaGuardando and FEsConvenio then
            MsgBoxBalloon(MSG_NO_PUEDE_VALIDAR_PATENTE, Format(MSG_CAPTION_VALIDAR,[STR_PATENTE]), MB_ICONWARNING, txtPatente);
        FPuedeValidar := false;
    end;
    Result:= PatenteValida;
//    ValidarPatente;
end;

function TFormDatosVehiculoConvenio.GetDigitoVerificador: AnsiString;
begin
    if not DigitoVerificadorCorrecto then result:= Trim(txtDigitoVerificador.Text)
    else result := '';
end;

function TFormDatosVehiculoConvenio.ValidarDigitoVerificador: boolean;
begin
    if (QueryGetValue(DMConnections.BaseCAC, 'Exec VerificarPatente ''' + Trim(txtPatente.Text) +
        ''',''' + Trim(txtDigitoVerificador.Text) + '''') = '0') then
    //No es V�lida
        result := False
    else
        result := True;
end;

procedure TFormDatosVehiculoConvenio.txtDigitoVerificadorKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9', 'K','k', #8]) then
        Key := #0;
end;

procedure TFormDatosVehiculoConvenio.txtDigitoVerificadorExit(Sender: TObject);
begin
    if btnAceptar.Focused or btnCancelar.Focused then exit;

    if trim(txtDigitoVerificador.Text) = '' then begin
        MsgBoxBalloon(MSG_VALIDAR_DIGITO_VERIFICADOR, MSG_CAPTION_DIGITO_VERIFICADOR, MB_ICONSTOP, txtDigitoVerificador);
        Exit;
    end;

    if FPuedeValidar and not ValidarDigitoVerificador then begin
        MsgBoxBalloon(MSG_ERROR_DIGITO_VERIFICADOR, MSG_CAPTION_DIGITO_VERIFICADOR, MB_ICONWARNING, txtDigitoVerificador);
    end;
end;

function TFormDatosVehiculoConvenio.BuscarPatenteCuenta: boolean;
begin
    if (QueryGetValue(DMConnections.BaseCAC, 'Exec ExisteCuenta ''' + Trim(txtPatente.Text) +
        ''',''' + PATENTE_CHILE + '''') = '0') then
    //No Existe
        result := False
    else
        result := True;
end;

function TFormDatosVehiculoConvenio.ValidarConvenio: boolean;
begin
    Result := false;

    if FPuedeValidar and not ValidarDigitoVerificador then begin
        if MsgBox(MSG_ERROR_DIGITO_VERIFICADOR_PATENTE, CAPTION_VALIDAR_DATOS_VEHICULO, MB_ICONWARNING or MB_YESNO) <> mrYes  then
        Exit;
    end;

    if BuscarPatenteCuenta then begin
        MsgBoxBalloon(MSG_ERROR_PATENTE_CUENTA, MSG_ERROR_PATENTE, MB_ICONSTOP, txtPatente);
        Exit;
    end;

    Result:= true;
end;

function TFormDatosVehiculoConvenio.ValidarSolicitud: boolean;
begin
    result := BuscarPatenteSolicitud;
end;

function TFormDatosVehiculoConvenio.GetCategoria: integer;
begin
    result:= ObtenerCategoria(DMConnections.BaseCAC,self.CodigoTipo, self.TieneAcoplado);
end;

function TFormDatosVehiculoConvenio.GetTieneAcoplado: boolean;
begin
    result := cbConAcoplado.Checked;
end;

function TFormDatosVehiculoConvenio.GetTag: Ansistring;
begin
    result := Trim(txtNumeroTelevia.Text);
end;

function TFormDatosVehiculoConvenio.GetSerialNumber: DWORD;
begin
    if Trim(txtNumeroTelevia.Text) <> '' then
        result := EtiquetaToSerialNumber(PadL(Trim(txtNumeroTelevia.Text),11,'0'))
    else
        result := 0;
end;

function ValidarExistenciaTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD): boolean;
begin
    result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarUbicacionTag' + '''' +
              inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + '''') = 1;
end;

function ValidarCategoriaTelevia(Conn :TADOConnection; CodigoTipoVehiculo: integer; TieneAcoplado:integer; ContextMark: integer; SerialNumber: DWORD): boolean;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.ValidarCategoriaTAGporTipoVehiculo(' + inttostr(CodigoTipoVehiculo) + ',' +
    inttostr(TieneAcoplado) + ',' + inttostr(ContextMark) + ',' + intToStr(SerialNumber) + ')') = 1;
end;

procedure ValidarTeleviaInterfase(Conn :TADOConnection; CodigoPuntoEntrega: integer; ContextMark: integer; SerialNumber: DWORD);
begin
    QueryExecute(DMConnections.BaseCAC,'EXEC ValidarInterfaseTag' + '''' +
              inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + ''',''' + inttostr(CodigoPuntoEntrega) + ''',''' + Trim(UsuarioSistema) + '''');
end;

function ValidarSituacionTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD): boolean;
var
    CodigoSituacionEntregadoCliente: integer;
begin
    CodigoSituacionEntregadoCliente := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_ESTADO_SITUACION_PENDIENTE()');
    result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarSituacionTag' + '''' +
              inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + ''',''' + inttostr(CodigoSituacionEntregadoCliente) + '''') = 1;
    if not result then begin
        CodigoSituacionEntregadoCliente := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_ESTADO_SITUACION_DISPONIBLE()');
        result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarSituacionTag' + '''' +
                  inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + ''',''' + inttostr(CodigoSituacionEntregadoCliente) + '''') = 1;
    end;
end;

function TFormDatosVehiculoConvenio.ValidarDigitoVerificadorTag(
  NumeroTag: String): Boolean;
begin
    NumeroTag:=trim(NumeroTag);
    result:=true;
    if Luhn(copy(NumeroTag,0,length(NumeroTag)-1))<>StrRight(NumeroTag,1)  then result:=False;
end;

procedure TFormDatosVehiculoConvenio.txtNumeroTeleviaExit(Sender: TObject);
begin
    if btnAceptar.Focused or btnCancelar.Focused then exit;
    if not(ValidarDigitoVerificadorTag(txtNumeroTelevia.Text)) then begin
        MsgBoxBalloon(MSG_ERROR_VERIFICADOR_TAG,MSG_CAPTION_ASIGNAR_TAG,MB_ICONSTOP,txtNumeroTelevia);
    end;
end;

procedure TFormDatosVehiculoConvenio.cbTipoVehiculoChange(Sender: TObject);
begin
    cbConAcoplado.Enabled:=ObtenerCategoria(DMConnections.BaseCAC,CodigoTipo,false)<>ObtenerCategoria(DMConnections.BaseCAC,CodigoTipo,true);

    cbConAcoplado.Checked:=False;
end;

end.
