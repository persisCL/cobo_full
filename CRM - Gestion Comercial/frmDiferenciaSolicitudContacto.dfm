object formDiferenciaSolicitudContacto: TformDiferenciaSolicitudContacto
  Left = 58
  Top = 113
  Width = 820
  Height = 522
  BorderIcons = [biSystemMenu]
  Caption = 'Atenci'#243'n'
  Color = clBtnFace
  Constraints.MaxHeight = 600
  Constraints.MaxWidth = 1000
  Constraints.MinHeight = 280
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 5
    Top = 304
    Width = 801
    Height = 144
    Caption = 'Solicitudes '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object dbl_Solicitud: TDBListEx
      Left = 4
      Top = 16
      Width = 792
      Height = 112
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Hora de Creacion'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHoraCreacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 148
          Header.Caption = 'Raz'#243'n Social'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'RazonSocial'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 190
          Header.Caption = 'Nombre'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Nombre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Tel'#233'fono Principal'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TelefonoPrincipal'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Calle'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripcionCalle'
        end>
      DataSource = DS_SolicitudContacto
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = dbl_SolicitudDblClick
    end
  end
  object PanelDeAbajo: TPanel
    Left = 0
    Top = 449
    Width = 812
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      812
      39)
    object BtnCancelar: TButton
      Left = 632
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Cancelar Importaci'#243'n'
      Anchors = [akRight, akBottom]
      Caption = '&Cancelar'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtnCancelarClick
    end
    object btnAceptar: TButton
      Left = 550
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Procesar Importaci'#243'n'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Procesar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnSalir: TButton
      Left = 715
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Salir'
      Anchors = [akRight, akBottom]
      Caption = '&Salir'
      ModalResult = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnSalirClick
    end
  end
  object Pnl_LabelDescripcion: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 91
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object lblDescripcionMensaje: TLabel
      Left = 20
      Top = 24
      Width = 778
      Height = 65
      AutoSize = False
      Caption = 
        'Se han encontrado una o mas solicitudes con el RUT/RUN  ingresad' +
        'o.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object lblRutBuscado: TLabel
      Left = 20
      Top = 5
      Width = 493
      Height = 16
      AutoSize = False
      Caption = 'RUT buscado: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 91
    Width = 812
    Height = 215
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GroupBox2: TGroupBox
      Left = 5
      Top = 3
      Width = 801
      Height = 204
      Caption = 'Datos Excel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object lblPersoneria: TLabel
        Left = 131
        Top = 23
        Width = 55
        Height = 13
        Caption = 'Personer'#237'a:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblRazonSocial: TLabel
        Left = 131
        Top = 49
        Width = 66
        Height = 13
        Caption = 'Raz'#243'n Social:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblNombre: TLabel
        Left = 131
        Top = 73
        Width = 40
        Height = 13
        Caption = 'Nombre:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblApellido: TLabel
        Left = 131
        Top = 100
        Width = 40
        Height = 13
        Caption = 'Apellido:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblApellidoMaterno: TLabel
        Left = 131
        Top = 125
        Width = 82
        Height = 13
        Caption = 'Apellido Materno:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblSexo: TLabel
        Left = 131
        Top = 147
        Width = 27
        Height = 13
        Caption = 'Sexo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblFechaNacimiento: TLabel
        Left = 131
        Top = 174
        Width = 89
        Height = 13
        Caption = 'Fecha Nacimiento:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblTelefonoPrincipal: TLabel
        Left = 447
        Top = 49
        Width = 88
        Height = 13
        Caption = 'Tel'#233'fono Principal:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblTelefonoAlternativo: TLabel
        Left = 447
        Top = 73
        Width = 98
        Height = 13
        Caption = 'Tel'#233'fono Alternativo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblemail: TLabel
        Left = 447
        Top = 23
        Width = 28
        Height = 13
        Caption = 'Email:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblDomicilio: TLabel
        Left = 447
        Top = 100
        Width = 45
        Height = 13
        Caption = 'Domicilio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 17
        Top = 23
        Width = 67
        Height = 13
        Caption = 'Personer'#237'a:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_RazonSocial: TLabel
        Left = 17
        Top = 49
        Width = 80
        Height = 13
        Caption = 'Raz'#243'n Social:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 17
        Top = 73
        Width = 48
        Height = 13
        Caption = 'Nombre:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 17
        Top = 100
        Width = 50
        Height = 13
        Caption = 'Apellido:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 17
        Top = 125
        Width = 100
        Height = 13
        Caption = 'Apellido Materno:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 18
        Top = 147
        Width = 33
        Height = 13
        Caption = 'Sexo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_FechaNacimiento: TLabel
        Left = 18
        Top = 174
        Width = 107
        Height = 13
        Caption = 'Fecha Nacimiento:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 323
        Top = 23
        Width = 35
        Height = 13
        Caption = 'Email:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 323
        Top = 49
        Width = 108
        Height = 13
        Caption = 'Tel'#233'fono Principal:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 323
        Top = 73
        Width = 120
        Height = 13
        Caption = 'Tel'#233'fono Alternativo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 323
        Top = 100
        Width = 56
        Height = 13
        Caption = 'Domicilio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object ObtenerSolicitudContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerSolicitudContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaHoraCreacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraFin'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoFuenteSolicitud'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadVehiculos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@RUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@OrderBy'
        Attributes = [paNullable]
        DataType = ftFixedChar
        Size = 100
        Value = Null
      end>
    Left = 608
    Top = 230
  end
  object DS_SolicitudContacto: TDataSource
    DataSet = ObtenerSolicitudContacto
    Left = 640
    Top = 230
  end
end
