unit FrmGenerarOrdenPedidoTAGs;

//Firma         : SS_1147_MCA_20150409
//Descripcion   : si no existen categoria, no permite grabar.

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ListBoxEx, DBListEx, DB, ADODB, Grids, Util,
  DBGrids, DBClient, UtilProc, StrUtils;

type
  TFormGenerarOrdenPedidoTAGs = class(TForm)
    spObtenerCantidadesPedidoTAGs: TADOStoredProc;
    dsPedido: TDataSource;
    dbgPedidos: TDBGrid;
    cdsPedido: TClientDataSet;
    spRegistrarPedidoTAGs: TADOStoredProc;
    spRegistrarDetallePedidoTAGs: TADOStoredProc;
    btnAceptar: TButton;
    btnCancelar: TButton;
    btnReiniciar: TButton;
    procedure btnAceptarClick(Sender: TObject);
	procedure dbgPedidosKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnCancelarClick(Sender: TObject);
    procedure btnReiniciarClick(Sender: TObject);
    procedure cdsPedidoBeforePost(DataSet: TDataSet);
  private
	{ Private declarations }
	FCodigoPuntoEntrega : Integer;
    function GenerarPedido: Boolean;
  public
	{ Public declarations }
	function Inicializar(txtCaption: ANSIString; MDIChild: Boolean; CodigoPuntoEntrega: Integer): boolean;
  end;

var
  FormGenerarOrdenPedidoTAGs: TFormGenerarOrdenPedidoTAGs;

implementation

uses DMConnection;

{$R *.dfm}

function TFormGenerarOrdenPedidoTAGs.Inicializar(txtCaption: ANSIString; MDIChild: Boolean; CodigoPuntoEntrega: Integer): boolean;
var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

    FCodigoPuntoEntrega := CodigoPuntoEntrega;

    cdsPedido.CreateDataSet;
    cdsPedido.Open;

    result := GenerarPedido();
end;

function TFormGenerarOrdenPedidoTAGs.GenerarPedido: Boolean;
resourcestring
	MSG_ERROR_CANTIDADES_PEDIDO	= 'No pudo generarse la lista de pedidos';
	MSG_CAPTION_PEDIDO 			= 'Pedido de Reposici�n de Telev�a';
begin
	result := true;

    try
        cdsPedido.EmptyDataSet;

        with spObtenerCantidadesPedidoTAGs, Parameters do begin
            Close;
            ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
            Open;

            while not EoF do begin
                cdsPedido.AppendRecord([FieldByName('Categoria').AsInteger,
                                        FieldByName('Descripcion').AsString,
                                        FieldByName('CantidadSugerida').AsInteger]);
                Next;
            end;

            Close
        end;

        cdsPedido.First;
    except
		on e: Exception do begin
			MsgBoxErr(MSG_ERROR_CANTIDADES_PEDIDO, e.message, MSG_CAPTION_PEDIDO, MB_ICONSTOP);
	    	cdsPedido.EmptyDataSet;
    	    result := false
        end;
    end;
end;

procedure TFormGenerarOrdenPedidoTAGs.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_ERROR_REGISTRO_DETALLE_PEDIDO   = 'No pudo registrarse el detalle del pedido';
	MSG_CAPTION_REGISTRO_DETALLE_PEDIDO = 'Registro de Detalles de Pedido de Reposici�n de Telev�a';
	MSG_ERROR_REGISTRO_PEDIDO           = 'No pudo registrarse el pedido';
	MSG_CAPTION_REGISTRO_PEDIDO         = 'Registro de Pedido de Reposici�n de Telev�a';
	MSG_REGISTRO_PEDIDO_CORRECTO		= 'El Pedido de Reposici�n de Telev�a ha sido correctamente registrado';
var
	CodigoOrdenPedido,
	Cantidad: Integer;
	TodoOk: Boolean;
begin
	if cdsPedido.State in [dsEdit, dsInsert] then cdsPedido.Post;

	Cantidad := 0;

	cdsPedido.DisableControls;
	cdsPedido.First;

    if cdsPedido.FieldByName('Categoria').AsInteger = 0 then Exit;              //SS_1147_MCA_20150409
    

	while not cdsPedido.Eof do begin
		Cantidad := Cantidad + cdsPedido.FieldByName('Cantidad').AsInteger;
		cdsPedido.Next;
	end;


	if Cantidad = 0 then Exit;

	Screen.Cursor := crHourGlass;
	TodoOk:= False;
	DMConnections.BaseCAC.BeginTrans;

	try
		try
			with spRegistrarPedidoTAGs, Parameters do begin
				Close;

				ParamByName ('@CodigoOrdenPedido').Value    := 0;
				ParamByName ('@CodigoPuntoEntrega').Value   := FCodigoPuntoEntrega;
				ParamByName ('@Usuario').Value              := UsuarioSistema;

				ExecProc;

				CodigoOrdenPedido := ParamByName ('@CodigoOrdenPedido').Value;
				Close
			end;

			cdsPedido.DisableControls;
			cdsPedido.First;
			while not cdsPedido.Eof do begin
				try
					if cdsPedido.FieldByName('Cantidad').AsInteger > 0 then begin
						with spRegistrarDetallePedidoTAGs, Parameters do begin
							Close;

							ParamByName ('@CodigoOrdenPedido').Value := CodigoOrdenPedido;
							ParamByName ('@Categoria').Value := cdsPedido.FieldByName('Categoria').AsInteger;
							ParamByName ('@Cantidad').Value := cdsPedido.FieldByName('Cantidad').AsInteger;

							ExecProc;
						end
					end;

					cdsPedido.Next;
				except
					on e: Exception do begin
						Screen.Cursor := crDefault;
						MsgBoxErr(MSG_ERROR_REGISTRO_DETALLE_PEDIDO, e.message, MSG_CAPTION_REGISTRO_DETALLE_PEDIDO, MB_ICONSTOP);
						Exit;
					end;
				end
			end;

			TodoOk := True;
		except
			on e: Exception do begin
				Screen.Cursor := crDefault;

				DMConnections.BaseCAC.RollbackTrans;
				MsgBoxErr(MSG_ERROR_REGISTRO_PEDIDO, e.message, MSG_CAPTION_REGISTRO_PEDIDO, MB_ICONSTOP);
			end;
		end;
	finally
		Screen.Cursor := crDefault;
		cdsPedido.EnableControls;
		cdsPedido.EmptyDataSet;

		if TodoOk then begin
			DMConnections.BaseCAC.CommitTrans;
			MsgBox(MSG_REGISTRO_PEDIDO_CORRECTO, self.caption, MB_ICONINFORMATION);

			Close
		end
	end
end;

procedure TFormGenerarOrdenPedidoTAGs.dbgPedidosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    dbgPedidos.Options := dbgPedidos.Options + [dgEditing];
    if (ssCtrl in Shift) and (key = VK_DELETE) then key := 0;
    if (key = VK_INSERT) then key := 0;
	if (key = VK_DOWN) then begin
        dbgPedidos.Options := dbgPedidos.Options - [dgEditing];
    end;
end;

procedure TFormGenerarOrdenPedidoTAGs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree
end;

procedure TFormGenerarOrdenPedidoTAGs.btnCancelarClick(Sender: TObject);
begin
	Close
end;

procedure TFormGenerarOrdenPedidoTAGs.btnReiniciarClick(Sender: TObject);
begin
    GenerarPedido();
end;

procedure TFormGenerarOrdenPedidoTAGs.cdsPedidoBeforePost(
  DataSet: TDataSet);
resourcestring
	MSG_ERROR_CANTIDAD_NEGATIVA = 'La cantidad a solicitar no puede ser negativa';
begin
	if DataSet.FieldByName('Cantidad').AsInteger < 0 then begin
		MsgBox(MSG_ERROR_CANTIDAD_NEGATIVA, self.caption, MB_ICONINFORMATION);
		abort;
	end
end;

end.




