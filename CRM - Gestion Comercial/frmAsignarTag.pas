unit frmAsignarTag;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, PeaProcs, DB, ADODB, DMConnection, UtilDB, Peatypes,
  Util, RStrings;

type
  TFormAsignarTag = class(TForm)
    Panel1: TPanel;
    txtNumeroTelevia: TEdit;
    btnAceptar: TDPSButton;
    btnSalir: TDPSButton;
    Label1: TLabel;
    Label2: TLabel;
    lblPatente: TLabel;
    ObtenerUbicacionTag: TADOStoredProc;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
  private
    FContextMark: integer;
    FCodigoPuntoEntrega: integer;
    FPatente: string;
    FListaTags: TStringList;
    FTieneAcoplado: byte;
    FCodigoTipoVehiculo: integer;
    function GetTag: Ansistring;
    function GetSerialNumber: DWORD;
    function ValidarUbicacionTag: boolean;
    procedure ValidarTagInterfase;
    function ValidarExistenciaTag: boolean;
    function ValidarSituacionTag: boolean;
    function ValidarCategoriaTag: boolean;
  public
    property NumeroTag:AnsiString read GetTag;
    property SerialNumber:DWORD read GetSerialNumber;
    property ContextMark: integer read FContextMark;
    function Inicializar(Caption: TCaption; Patente: string; CodigoPuntoEntrega: integer; ListaTags: TStringList; CodigoTipoVehiculo: integer; TieneAcoplado: byte): Boolean;
  end;

  function ValidarCategoriaTelevia(Conn :TADOConnection; CodigoTipoVehiculo: integer; TieneAcoplado:integer; ContextMark: integer; SerialNumber: DWORD): boolean;
  procedure ValidarTeleviaInterfase(Conn :TADOConnection; CodigoPuntoEntrega: integer; ContextMark: integer; SerialNumber: DWORD);
  function ValidarSituacionTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD): boolean;
  function ValidarExistenciaTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD): boolean;


var
  FormAsignarTag: TFormAsignarTag;

implementation

{$R *.dfm}

{ TFormAsignarTag }

function TFormAsignarTag.Inicializar(Caption: TCaption;
  Patente: string; CodigoPuntoEntrega: integer; ListaTags: TStringList; CodigoTipoVehiculo: integer; TieneAcoplado: byte): Boolean;
begin
    self.Caption := Caption;
    lblPatente.Caption := Patente;
    FPatente := trim(Patente);
    FCodigoTipoVehiculo := CodigoTipoVehiculo;
    FTieneAcoplado := TieneAcoplado;
    FCodigoPuntoEntrega := CodigoPuntoEntrega;
    FContextMark := CONTEXT_MARK;
    FListaTags := ListaTags;
    result := true;
end;

procedure TFormAsignarTag.btnAceptarClick(Sender: TObject);
var
    AlmacenPuntoEntrega, CategoriaVehiculo: integer;
begin

    if not ValidarExistenciaTag then ValidarTagInterfase;

    AlmacenPuntoEntrega := ObtenerAlmacenPuntoEntrega(DMConnections.BaseCAC, FCodigoPuntoEntrega);

    CategoriaVehiculo := ObtenerCategoriaVehiculo(DMConnections.BaseCAC, FCodigoTipoVehiculo, Boolean(FTieneAcoplado));

    if not ValidateControls([txtNumeroTelevia,
                            txtNumeroTelevia,
                            txtNumeroTelevia,
                            txtNumeroTelevia,
                            txtNumeroTelevia,
                            txtNumeroTelevia,
                            txtNumeroTelevia,
                            txtNumeroTelevia,
                            lblPatente],
                            [txtNumeroTelevia.Text <> '',
                            Length(txtNumeroTelevia.Text) >= 10,
                            not ExisteCadenaEnStringList(FListaTags,Trim(intToStr(GetSerialNumber))),
                            ValidarExistenciaTag,
//                            ValidarUbicacionTag,
                            ValidarSituacionTag,
                            ValidarCategoriaTag,
                            ValidarStockAlmacen(DMConnections.BaseCAC,AlmacenPuntoEntrega,CategoriaVehiculo),
                            (ObtenerStockAlmacen(DMConnections.BaseCAC,AlmacenPuntoEntrega,CategoriaVehiculo) >= FListaTags.Count + 1),
                            not ValidarVehiculoOtraConcesionaria(DMConnections.BaseCAC,FPatente)],
                            MSG_CAPTION_ASIGNAR_TAG,
                            [MSG_VALIDAR_CARGA_TAG,
                             MSG_ERROR_NUMERO_TAG,
                             MSG_ERROR_NUMERO_TAG_COVENIO,
                             MSG_ERROR_NO_ESTA_TAG,
//                             MSG_ERROR_UBICACION_TAG,
                             MSG_ERROR_SITUACION_TAG,
                             MSG_ERROR_CATEGORIA_TAG,
                             MSG_ERROR_STOCK_TAG,
                             MSG_ERROR_STOCK_TAG_ASIGNADOS,
                             MSG_ERROR_VEHICULO_OTRA_CONCESIONARIA]) then Exit;

    ModalResult := mrOK;
end;

procedure TFormAsignarTag.btnSalirClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

function TFormAsignarTag.GetTag: Ansistring;
begin
    result := Trim(txtNumeroTelevia.Text);
end;

function TFormAsignarTag.GetSerialNumber: DWORD;
begin
    if Trim(txtNumeroTelevia.Text) <> '' then
        result := EtiquetaToSerialNumber(PadL(Trim(txtNumeroTelevia.Text),11,'0'))
    else
        result := 0;
end;

function TFormAsignarTag.ValidarUbicacionTag: boolean;
begin
    result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarUbicacionTag' + '''' +
              inttostr(FContextMark) + ''',''' + intToStr(GetSerialNumber) + ''',''' + inttostr(FCodigoPuntoEntrega) + '''') = 1;
end;

function TFormAsignarTag.ValidarExistenciaTag: boolean;
begin
    result := ValidarExistenciaTelevia(DMConnections.BaseCAC, FContextMark, GetSerialNumber);
(*
    result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarUbicacionTag' + '''' +
              inttostr(FContextMark) + ''',''' + intToStr(GetSerialNumber) + '''') = 1;
*)
end;

function ValidarExistenciaTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD): boolean;
begin
    result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarUbicacionTag' + '''' +
              inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + '''') = 1;
end;

function TFormAsignarTag.ValidarSituacionTag: boolean;
//var
//    CodigoSituacionEntregadoCliente: integer;
begin
    result := ValidarSituacionTelevia(DMConnections.BaseCAC, FContextMark, GetSerialNumber);

(*
    CodigoSituacionEntregadoCliente := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_ESTADO_SITUACION_PENDIENTE()');
    result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarSituacionTag' + '''' +
              inttostr(FContextMark) + ''',''' + intToStr(GetSerialNumber) + ''',''' + inttostr(CodigoSituacionEntregadoCliente) + '''') = 1;
    if not result then begin
        CodigoSituacionEntregadoCliente := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_ESTADO_SITUACION_DISPONIBLE()');
        result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarSituacionTag' + '''' +
                  inttostr(FContextMark) + ''',''' + intToStr(GetSerialNumber) + ''',''' + inttostr(CodigoSituacionEntregadoCliente) + '''') = 1;
    end;
*)
end;

function ValidarSituacionTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD): boolean;
var
    CodigoSituacionEntregadoCliente: integer;
begin
    CodigoSituacionEntregadoCliente := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_ESTADO_SITUACION_PENDIENTE()');
    result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarSituacionTag' + '''' +
              inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + ''',''' + inttostr(CodigoSituacionEntregadoCliente) + '''') = 1;
    if not result then begin
        CodigoSituacionEntregadoCliente := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_ESTADO_SITUACION_DISPONIBLE()');
        result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarSituacionTag' + '''' +
                  inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + ''',''' + inttostr(CodigoSituacionEntregadoCliente) + '''') = 1;
    end;
end;

procedure TFormAsignarTag.ValidarTagInterfase;
begin
    ValidarTeleviaInterfase(DMConnections.BaseCAC, FCodigoPuntoEntrega, FContextMark, GetSerialNumber);

(*
    QueryExecute(DMConnections.BaseCAC,'EXEC ValidarInterfaseTag' + '''' +
              inttostr(FContextMark) + ''',''' + intToStr(GetSerialNumber) + ''',''' + inttostr(FCodigoPuntoEntrega) + ''',''' + Trim(UsuarioSistema) + '''');
    *)
end;

procedure ValidarTeleviaInterfase(Conn :TADOConnection; CodigoPuntoEntrega: integer; ContextMark: integer; SerialNumber: DWORD);
begin
    QueryExecute(DMConnections.BaseCAC,'EXEC ValidarInterfaseTag' + '''' +
              inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + ''',''' + inttostr(CodigoPuntoEntrega) + ''',''' + Trim(UsuarioSistema) + '''');
end;

function TFormAsignarTag.ValidarCategoriaTag: boolean;
begin
    Result := ValidarCategoriaTelevia(DMConnections.BaseCAC, FCodigoTipoVehiculo, FTieneAcoplado, FContextMark, GetSerialNumber);
(*
    Result := QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.ValidarCategoriaTAGporTipoVehiculo(' + inttostr(FCodigoTipoVehiculo) + ',' +
    inttostr(FTieneAcoplado) + ',' + inttostr(FContextMark) + ',' + intToStr(GetSerialNumber) + ')') = 1;
*)
end;

function ValidarCategoriaTelevia(Conn :TADOConnection; CodigoTipoVehiculo: integer; TieneAcoplado:integer; ContextMark: integer; SerialNumber: DWORD): boolean;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.ValidarCategoriaTAGporTipoVehiculo(' + inttostr(CodigoTipoVehiculo) + ',' +
    inttostr(TieneAcoplado) + ',' + inttostr(ContextMark) + ',' + intToStr(SerialNumber) + ')') = 1;
end;

end.
