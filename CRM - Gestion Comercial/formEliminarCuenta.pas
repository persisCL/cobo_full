{-----------------------------------------------------------------------------
 File Name: formEliminarCuenta.pas
 Author:    flamas
 Date Created: 02/03/2005
 Language: ES-AR
 Description: Valida la Devoluci�n del Telev�a para eliminar una Cuenta
 
 Author:    jconcheyro
 Date Created: 19/12/2006
 Language: ES-AR
 Description: Se agrega el combo de Motivos para manejar la generacion de 
 	Movimientoscuenta por indemnizaciones

 
-----------------------------------------------------------------------------}
unit formEliminarCuenta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, VariantComboBox, DB, ADODB, PeaTypes, RStrings,
  Convenios, PeaProcs, UtilProc, DMConnection, Util, UtilDB, Menus, DBClient,
  ListBoxEx, DBListEx , frmSeleccionarMovimientosTelevia;

type
  TfrmEliminarCuenta = class(TForm)
    Label1: TLabel;
	lbl_Patente: TLabel;
    GroupBox1: TGroupBox;
	Label2: TLabel;
	Label3: TLabel;
    txt_TagDevuelto: TEdit;
	cb_EstadoTagDevuelto: TVariantComboBox;
    cK_TagRobado: TCheckBox;
    Panel1: TPanel;
    btnCancelar: TButton;
    btnAceptar: TButton;
    ObtenerEstadosConservacionTAGs: TADOStoredProc;
    Label10: TLabel;
    lblSeleccionar: TLabel;
    Bevel1: TBevel;
    Label9: TLabel;
    dblConceptos: TDBListEx;
    mnuGrillaConceptos: TPopupMenu;
    mnuEliminarConcepto: TMenuItem;
    dsConceptos: TDataSource;
    cdConceptos: TClientDataSet;
    CargarConceptos1: TMenuItem;
    lblTotalMovimientos: TLabel;
    lblTotalMovimientosTxt: TLabel;
    gbModalidadEntregaTelevia: TGroupBox;
    lblModalidadEntregaTelevia: TLabel;
    procedure cb_EstadoTagDevueltoChange(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure txt_TagDevueltoKeyPress(Sender: TObject; var Key: Char);
    procedure txt_TagDevueltoExit(Sender: TObject);
    procedure lblSeleccionarClick(Sender: TObject);
    procedure mnuEliminarConceptoClick(Sender: TObject);
    procedure dblConceptosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dblConceptosContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
  private
    { Private declarations }
	FTagOriginal : string;
    FCuentaSuspendida: Boolean;
    FVehiculo: TVehiculosConvenio;
    FImporteTotalMovimientos: int64;
    function  GetEstadoConservacion: integer;
    function  GetTagRobado: boolean;
	function  GetVehiculoRobado: boolean;
	function  GetTagVendido: Boolean;
  public
    { Public declarations }
    property EstadoConservacion: integer read GetEstadoConservacion;
    property TagRobado: Boolean read GetTagRobado;
    property TagVendido: Boolean read GetTagVendido;
    property VehiculoRobado: Boolean read GetVehiculoRobado;

    function Inicializar(Vehiculo: TVehiculosConvenio): Boolean;
  end;

var
  frmEliminarCuenta: TfrmEliminarCuenta;

implementation

resourcestring
	STR_ELIMINAR_CUENTA = 'Eliminar Cuenta';

{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 02/03/2005
  Description: Inicializa el FOrm de Eliminar Cuenta (Carga los Combos)
  Parameters: Vehiculo: TVehiculosConvenio
  Return Value: Boolean

  Revision :1
    Author : vpaszkowicz
    Date : 31/07/2009
    Description :Reemplazo el seteo de la suspensi�n por la propiedad
    EstaSuspendida.
-----------------------------------------------------------------------------}
function TfrmEliminarCuenta.Inicializar(Vehiculo: TVehiculosConvenio): Boolean;

    function CargarCombo:boolean;
    resourcestring
    	MSG_ERROR_ESTADOS_TELEVIAS = 'No se pudieron obtener los estados de los Telev�as';
    begin
    	Result := True;
        try
			cb_EstadoTagDevuelto.Items.Add(SIN_ESPECIFICAR, 0);
			ObtenerEstadosConservacionTAGs.Open;
			if not ObtenerEstadosConservacionTAGs.IsEmpty then begin
				while not ObtenerEstadosConservacionTAGs.Eof do begin
                	cb_EstadoTagDevuelto.Items.Add(Trim(ObtenerEstadosConservacionTAGs.fieldbyname('Descripcion').AsString), ObtenerEstadosConservacionTAGs.fieldbyname('CodigoEstadoConservacionTAG').AsInteger );
                	ObtenerEstadosConservacionTAGs.Next;
            	end;
            	cb_EstadoTagDevuelto.ItemIndex := 0;
            end else begin
				MsgBox(MSG_ERROR_ESTADOS_TELEVIAS, STR_ERROR, MB_ICONERROR);
            	Result:=False;
            end;
        except
        	on e : exception do begin
				MsgBoxErr(MSG_ERROR_ESTADOS_TELEVIAS, e.Message, STR_ERROR, MB_ICONERROR);
        		Result:=False;
            end;
        end;
    end;
var
	S: TSize;
begin
	Result := False;
	try
        FVehiculo := Vehiculo;
        FImporteTotalMovimientos := 0;
        DMConnections.BaseCAC.Connected := True;
        Result := 	DMConnections.BaseCAC.Connected and
        			CargarCombo;

        if FVehiculo.Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_EN_COMODATO then
            lblModalidadEntregaTelevia.Caption := CONST_STR_ALMACEN_COMODATO;

        if FVehiculo.Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_ARRIENDO then
            lblModalidadEntregaTelevia.Caption := CONST_STR_ALMACEN_ARRIENDO;

        // SS 769
		if FVehiculo.Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_ENTREGADO_EN_CUOTAS then
            lblModalidadEntregaTelevia.Caption := CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS;

    	// Si pudo Cargar el Combo trae el resto de los datos
    	if Result then begin
        	lbl_Patente.Caption := Trim(Vehiculo.Cuenta.Vehiculo.Patente);
    		FTagOriginal := SerialNumberToEtiqueta(IntToStr(Vehiculo.Cuenta.ContactSerialNumber));
            //FCuentaSuspendida := (Vehiculo.Cuenta.Suspendida <> NullDate);
            FCuentaSuspendida := (Vehiculo.Cuenta.EstaSuspendida);

    		// Habilita o deshabilita las Opciones
    		cb_EstadoTagDevueltoChange(nil);
        end;
	except
        on e: Exception do begin
			MsgBoxErr(Format (MSG_ERROR_INICIALIZACION,[STR_ELIMINAR_CUENTA]), e.Message, STR_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
    S := GetFormClientSize(Application.MainForm);
	CenterForm(Self);
end;


{-----------------------------------------------------------------------------
  Function Name: cb_EstadoTagDevueltoChange
  Author:    flamas
  Date Created: 02/03/2005
  Description: Habilita o Deshabilita los CheckBoxes de acuerdo al
  				estado del telev�a Devuelto
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}

procedure TfrmEliminarCuenta.cb_EstadoTagDevueltoChange(Sender: TObject);
begin
    txt_TagDevuelto.Enabled     := True;
    if (cb_EstadoTagDevuelto.Value = ESTADO_CONSERVACION_NO_DEVUELTO) then begin
        txt_TagDevuelto.Enabled     := False;
        txt_TagDevuelto.Clear;
    end;
    (* Habilitar el bot�n aceptar si NO es vac�o o No se permite ingresar el
    Tag. *)
    btnAceptar.Enabled := (Trim(txt_TagDevuelto.Text) <> EmptyStr) or (not txt_TagDevuelto.Enabled);

end;

procedure TfrmEliminarCuenta.dblConceptosContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
    mnuEliminarConcepto.Enabled := cdConceptos.RecordCount > 0;
end;

procedure TfrmEliminarCuenta.dblConceptosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = VK_DELETE) or (Key = VK_BACK) then mnuEliminarConceptoClick(Sender);
end;

{-----------------------------------------------------------------------------
  Function Name: GetEstadoConservacion
  Author:    flamas
  Date Created: 02/03/2005
  Description: Devuelve el Estado de Conservaci�n del Tag
  Parameters: None
  Return Value: integer
-----------------------------------------------------------------------------}
function  TfrmEliminarCuenta.GetEstadoConservacion: integer;
begin
	result := cb_EstadoTagDevuelto.Value;
end;

function  TfrmEliminarCuenta.GetTagRobado: boolean;
begin
	result := cK_TagRobado.Checked;
end;

function TfrmEliminarCuenta.GetTagVendido: Boolean;
begin
	Result := (EstadoConservacion = ESTADO_CONSERVACION_NO_DEVUELTO) and not(TagRobado);
end;

{-----------------------------------------------------------------------------
  Function Name: txt_TagDevueltoKeyPress
  Author:    flamas
  Date Created: 02/03/2005
  Description: S�lo permite que se ingresen n�meros
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmEliminarCuenta.txt_TagDevueltoKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9', #8]) then Key := #0;
end;

{-----------------------------------------------------------------------------
  Function Name: GetVehiculoRobado
  Author:    flamas
  Date Created: 02/03/2005
  Description: Devuelve el valor de Veh�culoRobado
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function  TfrmEliminarCuenta.GetVehiculoRobado: boolean;
begin
	result := False;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAceptarClick
  Author:    flamas
  Date Created: 02/03/2005
  Description: Valida el Telev�a y cierra el Form
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmEliminarCuenta.btnAceptarClick(Sender: TObject);
ResourceString
    MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION = 'Se debe ingresar al menos un motivo de indemnizaci�n';
begin

    if cdConceptos.IsEmpty then begin
         MsgBoxBalloon(MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION,'Error',MB_ICONSTOP, dblConceptos);
         dblConceptos.SetFocus;
         Exit;
    end;

	if txt_TagDevuelto.Enabled then begin
        // Valida que el Telev�a no est� en Blanco
        if (trim(txt_TagDevuelto.Text) = '') then begin
			MsgBoxBalloon(MSG_ERROR_TAG_VACIO, STR_ELIMINAR_CUENTA, MB_ICONERROR, txt_TagDevuelto );
            txt_TagDevuelto.SetFocus;
            txt_TagDevuelto.SelStart := 1000;
            Exit;
        end;

        // Valida la longitud del N�mero de Telev�a
        if (Length(txt_TagDevuelto.Text) < 10) then begin
			MsgBoxBalloon(MSG_ERROR_NUMERO_TAG, STR_ELIMINAR_CUENTA, MB_ICONERROR, txt_TagDevuelto);
            txt_TagDevuelto.SetFocus;
            txt_TagDevuelto.SelStart := 1000;
            Exit;
        end;

        // Valida que el Tag ingresado coincida con el del veh�culo
        if StrToFloat(FTagOriginal) <> StrToFloat(txt_TagDevuelto.Text) then begin
			MsgBoxBalloon(MSG_ERROR_NUMERO_TELEVIA_BORRAR,STR_ELIMINAR_CUENTA, MB_ICONERROR, txt_TagDevuelto);
            txt_TagDevuelto.SetFocus;
            txt_TagDevuelto.SelStart := 1000;
            Exit;
        end;
    end;

    ModalResult := mrOk;
end;

procedure TfrmEliminarCuenta.txt_TagDevueltoExit(Sender: TObject);
begin
    (* Habilitar el bot�n aceptar si NO es vac�o o No se permite ingresar el
    Tag. *)
    btnAceptar.Enabled := (Trim(txt_TagDevuelto.Text) <> EmptyStr) or (not txt_TagDevuelto.Enabled);
end;


procedure TfrmEliminarCuenta.lblSeleccionarClick(Sender: TObject);
var
    f: TfrmSeleccionarMovimientosTeleviaFORM;
begin
    Application.CreateForm(TfrmSeleccionarMovimientosTeleviaFORM, f);
    if f.Inicializar(CONST_OPERACION_TAG_ELIMINAR, FVehiculo.Cuenta.CodigoAlmacenDestino, '' ) and
            (f.ShowModal = mrOk) then begin
      // Cargamos el dataset con lo que se selecciono
        f.ListaConceptos.First;
        while not f.ListaConceptos.Eof do begin
            if f.ListaConceptos.FieldByName('Seleccionado').AsBoolean then begin
              //Agregamos el concepto a la lista
                cdConceptos.AppendRecord([f.ListaConceptos.FieldByName('Seleccionado').Value,
                  f.ListaConceptos.FieldByName('Codigoconcepto'   ).Value,
                  f.ListaConceptos.FieldByName('DescripcionMotivo').Value,
                  f.ListaConceptos.FieldByName('PrecioOrigen'     ).Value,
                  f.ListaConceptos.FieldByName('Moneda'           ).Value,
                  f.ListaConceptos.FieldByName('Cotizacion'       ).Value,
                  f.ListaConceptos.FieldByName('PrecioEnPesos'    ).Value,
                  f.ListaConceptos.FieldByName('Comentario'       ).Value]);
                FImporteTotalMovimientos := FImporteTotalMovimientos + f.ListaConceptos.FieldByName('PrecioEnPesos').AsVariant;
            end;
            f.ListaConceptos.Next;
      end;
      f.Release;
    end else begin
        f.Release;
    end;
    lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);

end;

procedure TfrmEliminarCuenta.mnuEliminarConceptoClick(Sender: TObject);
begin
    if not cdConceptos.eof then begin
        FImporteTotalMovimientos := FImporteTotalMovimientos - cdConceptos.FieldByName('PrecioEnPesos').AsVariant ;
        lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);
        cdConceptos.Delete;
    end;
end;

end.
