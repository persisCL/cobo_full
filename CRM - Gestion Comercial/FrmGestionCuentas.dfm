object FormGestionCuentas: TFormGestionCuentas
  Left = 145
  Top = 148
  Width = 704
  Height = 483
  Caption = 'Gesti'#243'n de Cuentas'
  Color = clBtnFace
  Constraints.MinHeight = 457
  Constraints.MinWidth = 627
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Grilla: TDBListEx
    Left = 0
    Top = 106
    Width = 696
    Height = 309
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 80
        Header.Caption = 'Cuenta'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'CodigoCuenta'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 250
        Header.Caption = 'Tipo de Pago'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescriTipoPago'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 210
        Header.Caption = 'Veh'#237'culo'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Vehiculo'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Estado'
        Header.Font.Charset = ANSI_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'EstadoCuenta'
      end>
    DataSource = DataSource1
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnDblClick = GrillaDblClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 696
    Height = 106
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label5: TLabel
      Left = 8
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Documento:'
    end
    object Label7: TLabel
      Left = 8
      Top = 49
      Width = 40
      Height = 13
      Caption = 'Apellido:'
    end
    object Label3: TLabel
      Left = 8
      Top = 76
      Width = 40
      Height = 13
      Caption = 'Nombre:'
    end
    object Label1: TLabel
      Left = 342
      Top = 49
      Width = 81
      Height = 13
      Caption = 'Apellido materno:'
    end
    object txt_Apellido: TEdit
      Left = 70
      Top = 44
      Width = 259
      Height = 21
      MaxLength = 40
      TabOrder = 2
      OnChange = ActualizarBusqueda
    end
    object txt_Nombre: TEdit
      Left = 70
      Top = 71
      Width = 259
      Height = 21
      MaxLength = 40
      TabOrder = 3
      OnChange = ActualizarBusqueda
    end
    object cbDocumento: TMaskCombo
      Left = 71
      Top = 16
      Width = 258
      Height = 21
      OnChange = cbDocumentoChange
      Items = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Color = clWindow
      ComboWidth = 80
      ItemIndex = -1
      OmitCharacterRequired = True
      TabOrder = 0
    end
    object txt_ApellidoMaterno: TEdit
      Left = 424
      Top = 44
      Width = 259
      Height = 21
      MaxLength = 40
      TabOrder = 4
      OnChange = ActualizarBusqueda
    end
    object btn_Buscar: TDPSButton
      Left = 598
      Top = 72
      Width = 84
      Caption = '&Buscar'
      TabOrder = 1
      OnClick = btn_BuscarClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 415
    Width = 696
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      696
      41)
    object btn_editar: TDPSButton
      Left = 529
      Top = 8
      Anchors = [akRight, akBottom]
      Caption = '&Editar'
      Default = True
      Enabled = False
      TabOrder = 0
      OnClick = btn_editarClick
    end
    object btn_Close: TDPSButton
      Left = 617
      Top = 8
      Anchors = [akRight, akBottom]
      Caption = '&Salir'
      TabOrder = 1
      OnClick = btn_CloseClick
    end
  end
  object DataSource1: TDataSource
    DataSet = ObtenerDatosCuentasCliente
    Left = 576
    Top = 203
  end
  object pop_Cuentas: TPopupMenu
    OnPopup = pop_CuentasPopup
    Left = 640
    Top = 203
    object mnu_cierre: TMenuItem
      Caption = 'Solicitar Cierre de Cuenta...'
      OnClick = mnu_cierreClick
    end
    object mnu_inh_tag: TMenuItem
      Caption = 'Inhabilitar Tag'
      OnClick = mnu_inh_tagClick
    end
    object mnu_reh_tag: TMenuItem
      Caption = 'Rehabilitar Tag'
      OnClick = mnu_reh_tagClick
    end
  end
  object BuscarClienteContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BuscarClienteContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoContacto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 148
    Top = 245
  end
  object ObtenerDatosCuentasCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosCuentasCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 608
    Top = 203
  end
end
