unit frmConvenioPromociones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ListBoxEx, DBListEx, StdCtrls, ExtCtrls, DPSControls,
  DMConnection, Peaprocs, Peatypes, Util, UtilDB, UtilProc, frmRegistrarLlamada, RStrings,
  FreDatosPersonaConsulta, CheckLst, FreDatosConvenioConsulta, ComCtrls;

type
  TformConvenioPromociones = class(TForm)
    GBPromociones: TGroupBox;
    PanelDeAbajo: TPanel;
    BtnCancelar: TDPSButton;
    btnAceptar: TDPSButton;
    GBPersona: TGroupBox;
    FrameDatosPersonaConsulta1: TFrameDatosPersonaConsulta;
    GroupBox1: TGroupBox;
    FrameDatosConvenioConsulta1: TFrameDatosConvenioConsulta;
    CheckListPromociones: TCheckListBox;
    GBDetallePromocion: TGroupBox;
    REditPlanComercial: TRichEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoConvenio: integer;
    function GetCodigoConvenio: integer;
  public
    { Public declarations }
    property CodigoConvenio: integer read GetCodigoConvenio;
    function Inicializar(CodigoConvenio: integer): Boolean;
  end;

var
  formConvenioPromociones: TformConvenioPromociones;

implementation

{$R *.dfm}

procedure TformConvenioPromociones.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//    action := caFree;
end;

function TformConvenioPromociones.GetCodigoConvenio: integer;
begin
    Result := FCodigoConvenio;
end;

function TformConvenioPromociones.Inicializar(CodigoConvenio: integer): Boolean;
resourcestring
    CONVENIOS = 'Convenios';
    MSG_OPENTABLES = 'Error al buscar los convenios.';

begin
    //LBPromociones.Clear;
    REditPlanComercial.Lines.LoadFromFile('d:\temp\Promocion.rtf');
    result := true;
end;

procedure TformConvenioPromociones.BtnCancelarClick(
  Sender: TObject);
begin
    ModalResult := mrCancel;
end;

end.
