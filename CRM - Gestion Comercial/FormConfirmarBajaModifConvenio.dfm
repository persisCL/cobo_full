object frmConfirmarBajaModifConvenio: TfrmConfirmarBajaModifConvenio
  Left = 241
  Top = 222
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Confirmar Baja Convenio'
  ClientHeight = 101
  ClientWidth = 346
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object txtEstado: TLabel
    Left = 2
    Top = 16
    Width = 341
    Height = 21
    Alignment = taCenter
    AutoSize = False
    Caption = 'Seleccione una opci'#243'n'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnConfirmar: TButton
    Left = 33
    Top = 52
    Width = 113
    Height = 25
    Caption = 'Confir&ma Baja/Camb'
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancelar: TButton
    Left = 193
    Top = 52
    Width = 113
    Height = 25
    Caption = '&Cancelar'
    Default = True
    ModalResult = 2
    TabOrder = 1
  end
end
