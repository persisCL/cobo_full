object ImagenesSPCForm: TImagenesSPCForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = ' Im'#225'genes disponibles Tr'#225'nsito SPC '
  ClientHeight = 601
  ClientWidth = 875
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pcImages: TPageControl
    Left = 0
    Top = 0
    Width = 875
    Height = 601
    ActivePage = tsFrontal1
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 802
    ExplicitHeight = 481
    object tsFrontal1: TTabSheet
      Caption = ' Frontal 1 '
      ExplicitWidth = 794
      ExplicitHeight = 453
      object imgFrontal1: TImage
        Left = 0
        Top = 0
        Width = 867
        Height = 573
        Align = alClient
        Stretch = True
        ExplicitWidth = 411
        ExplicitHeight = 282
      end
    end
    object tsFrontal2: TTabSheet
      Caption = ' Frontal 2 '
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgFrontal2: TImage
        Left = 0
        Top = 0
        Width = 794
        Height = 453
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
    end
    object tsContexto1: TTabSheet
      Caption = ' Contexto 1 '
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgContexto1: TImage
        Left = 0
        Top = 0
        Width = 794
        Height = 453
        Align = alClient
        Stretch = True
        ExplicitWidth = 411
        ExplicitHeight = 274
      end
    end
    object tsContexto2: TTabSheet
      Caption = ' Contexto 2 '
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgContexto2: TImage
        Left = 0
        Top = 0
        Width = 794
        Height = 453
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
    end
    object tsContexto3: TTabSheet
      Caption = ' Contexto 3 '
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgContexto3: TImage
        Left = 0
        Top = 0
        Width = 794
        Height = 453
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
    end
    object tsContexto4: TTabSheet
      Caption = ' Contexto 4 '
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgContexto4: TImage
        Left = 0
        Top = 0
        Width = 794
        Height = 453
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
    end
    object tsContexto5: TTabSheet
      Caption = ' Contexto 5 '
      ImageIndex = 6
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgContexto5: TImage
        Left = 0
        Top = 0
        Width = 794
        Height = 453
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
    end
    object tsContexto6: TTabSheet
      Caption = ' Contexto 6 '
      ImageIndex = 7
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgContexto6: TImage
        Left = 0
        Top = 0
        Width = 794
        Height = 453
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
    end
  end
end
