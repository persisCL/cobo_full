unit FechaVencimientoCheckList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, ExtCtrls, PeaProcs, DmConnection;

type
  TFrmFechaVencimientoCheckList = class(TForm)
    txt_FechaVencimiento: TDateEdit;
    Label1: TLabel;
    btn_Cancelar: TButton;
    btn_Aceptar: TButton;
    Bevel1: TBevel;
    procedure btn_AceptarClick(Sender: TObject);
  private
    procedure SetFechaVencimiento(const Value: TDateTime);
    function GetFechaVencimiento: TDateTime;
    { Private declarations }
  public
    property FechaVencimiento: TDateTime read GetFechaVencimiento write SetFechaVencimiento;
    { Public declarations }
  end;

var
  FrmFechaVencimientoCheckList: TFrmFechaVencimientoCheckList;

implementation

{$R *.dfm}

{ TFrmFechaVencimientoCheckList }

function TFrmFechaVencimientoCheckList.GetFechaVencimiento: TDateTime;
begin
    Result := txt_FechaVencimiento.Date;
end;

procedure TFrmFechaVencimientoCheckList.SetFechaVencimiento(
  const Value: TDateTime);
begin
    txt_FechaVencimiento.Date := Value;
end;

procedure TFrmFechaVencimientoCheckList.btn_AceptarClick(Sender: TObject);
resourcestring
    MSG_FECHA_POSTERIOR_A_HOY 	= 'La fecha debe ser posterior al d�a de hoy';
    MSG_DEBE_SER_DIA_HABIL 		= 'La fecha debe ser un d�a laborable';
begin
	if not ValidateControls(
    		[	txt_FechaVencimiento,
             	txt_FechaVencimiento],
    		[	txt_FechaVencimiento.Date > NowBase(DMConnections.BaseCAC),
                EsDiaHabil(DMConnections.BaseCAC, txt_FechaVencimiento.Date)],
    		caption,
            [	MSG_FECHA_POSTERIOR_A_HOY,
            	MSG_DEBE_SER_DIA_HABIL]) then Exit;
end;

end.
