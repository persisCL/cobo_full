unit frmConvenioCuentasRUT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB, ListBoxEx, DBListEx, DBClient, DMConnection,
  UtilProc, StrUtils, Util, ImgList, PeaTypes, PeaProcsCN, DateUtils;
type
    TOnClickRegSeleccionado = procedure (CodigoConvenio: Integer; IndiceVehiculo: Integer ) of object;

type
  TformConvenioCuentasRUT = class(TForm)
    GB_DatosPersona: TGroupBox;
    lbl_Personeria: TLabel;
    lbl1: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl_Nombre: TLabel;
    lblRazonSocial: TLabel;
    txt_RUT: TEdit;
    txt_Nombre: TEdit;
    txt_Apellido: TEdit;
    txt_ApellidoMaterno: TEdit;
    txt_RazonSocial: TEdit;
    txt_Personeria: TEdit;
    lstConvenios: TDBListEx;
    ds_VehiculosConveniosRUT: TDataSource;
    btn_Seleccionar: TButton;
    BtnSalir: TButton;
    cdsVehiculosRUT: TClientDataSet;
    lnCheck: TImageList;
    chkList: TCheckBox;
    procedure BtnSalirClick(Sender: TObject);
    function GetOnClickRegSeleccionado: TOnClickRegSeleccionado;
    procedure SetOnClickRegSeleccionado(const Value: TOnClickRegSeleccionado);
    procedure btn_SeleccionarClick(Sender: TObject);
    procedure lstConveniosDblClick(Sender: TObject);
    procedure lstConveniosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure lstConveniosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure chkListClick(Sender: TObject);
  private
    { Private declarations }
    FOnClickRegSeleccionado: TOnClickRegSeleccionado;
    procedure InicializarVariables();
  public
    CodigoConvenio: Integer;
    Vehiculo :  array of TCuentaVehiculo;
    function Inicializar(NumeroDocumento: string; CodigoConvenioFacturacion: Integer; VehiculosAgregados: string): Boolean;
    property OnClickRegSeleccionado: TOnClickRegSeleccionado read GetOnClickRegSeleccionado write SetOnClickRegSeleccionado;
  end;

var
  formConvenioCuentasRUT: TformConvenioCuentasRUT;
  ClicchkList: Boolean ;
implementation

{$R *.dfm}
procedure TformConvenioCuentasRUT.InicializarVariables();
begin
    txt_Personeria.Text:= '';
    txt_RUT.Text:= '';
    txt_Nombre.Text:= '';
    txt_Apellido.Text:= '';
    txt_ApellidoMaterno.Text:= '';
    txt_RazonSocial.Text:= '';
    txt_RazonSocial.Visible:=False;
    lblRazonSocial.Visible:=False;
    lbl_Nombre.Caption:='Nombre:'
end;

procedure TformConvenioCuentasRUT.lstConveniosDblClick(Sender: TObject);
begin
   btn_SeleccionarClick(Sender);
end;

function TformConvenioCuentasRUT.Inicializar(NumeroDocumento: string; CodigoConvenioFacturacion: Integer; VehiculosAgregados: string): Boolean;
var
    sp: TADOStoredProc;
    I, cuantos: Integer;
    MSG_RESULT_SEARCH, MSG_ATENTION: string;
    AuxPatente: string;
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    Result:=False;
    cdsVehiculosRUT.CreateDataSet;
    MSG_RESULT_SEARCH := 'No se han encontrados vehiculos disponibles del Convenio RNUT';
    MSG_ATENTION      := 'Atenci�n';
    try
        sp:= TADOStoredProc.Create(nil);
        sp.Connection:=DMConnections.BaseCAC;
        sp.ProcedureName := 'ConvenioCuentasRUT_SELECT';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@NumeroDocumento').Value := NumeroDocumento;
        sp.Parameters.ParamByName('@CodigoConvenioFacturacion').Value := CodigoConvenioFacturacion;
        sp.Open;

        if not sp.IsEmpty then
        begin
            cuantos:=0;
            for i := 0 to sp.RecordCount - 1 do
            begin
                AuxPatente:=Trim(sp.FieldByName('Patente').AsString);
                if not AnsiContainsStr(VehiculosAgregados, AuxPatente) then
                begin
                    ClicchkList:=False;
                    chkList.Checked:=True;
                    cdsVehiculosRUT.InsertRecord([
                                              sp.FieldByName('CodigoConvenio').AsInteger,
                                              sp.FieldByName('NumeroConvenio').AsString,
                                              sp.FieldByName('IndiceVehiculo').AsInteger,
                                              sp.FieldByName('CodigoVehiculo').AsInteger,
                                              sp.FieldByName('Patente').AsString,
                                              sp.FieldByName('SerialNumberaMostrar').AsString,
                                              sp.FieldByName('Marca').AsString,
                                              sp.FieldByName('Modelo').AsString,
                                              sp.FieldByName('AnioVehiculo').AsString,
                                              sp.FieldByName('CodigoCliente').AsInteger,
                                              sp.FieldByName('TipoConvenio').AsString,
                                              //sp.FieldByName('DescripcionTipoVehiculo').AsString,              //TASK_106_JMA_20170206
                                              sp.FieldByName('FechaAltaCuenta').AsString,
                                              sp.FieldByName('FechaVencimientoTAG').AsString,
                                              sp.FieldByName('CodigoAlmacen').AsString,
                                              sp.FieldByName('Concesionaria').AsString,
                                              True
                                             ]);
                    cuantos:=cuantos+1;
                end;
                sp.Next;
            end;
            if cuantos>0 then
                 Result:=True
            else
               MsgBox(MSG_RESULT_SEARCH, MSG_ATENTION, MB_ICONINFORMATION);

            ClicchkList:=True;
        end
        else
        begin
            MsgBox(MSG_RESULT_SEARCH, MSG_ATENTION, MB_ICONINFORMATION);
        end;
        sp.Close;

    except
        on e: Exception do begin
              MsgBox(e.Message, MSG_ATENTION, MB_ICONINFORMATION);
        end;
    end;
    Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;

procedure TformConvenioCuentasRUT.BtnSalirClick(Sender: TObject);
begin
 Close;
end;

//INICIO: TASK_051_ECA_20160707
procedure TformConvenioCuentasRUT.btn_SeleccionarClick(Sender: TObject);
var
  ContVehiculos, i: Integer; //TASK_051_ECA_20160707
begin
    ContVehiculos:= 0;
    if cdsVehiculosRUT.RecordCount > 0 then
    Begin
        cdsVehiculosRUT.First;
        while not cdsVehiculosRUT.eof do
        begin
          if cdsVehiculosRUT.FieldByName('Chequeado').AsBoolean then
             ContVehiculos := ContVehiculos +1;

          cdsVehiculosRUT.Next;
        end;

        if ContVehiculos > 0 then
        begin
            SetLength(Vehiculo, ContVehiculos);
            cdsVehiculosRUT.First;
            i:=0;
            while not cdsVehiculosRUT.eof do
            begin
                if cdsVehiculosRUT.FieldByName('Chequeado').AsBoolean then
                begin
                   Vehiculo[i].CodigoConvenioRNUT:= cdsVehiculosRUT.FieldByName('CodigoConvenio').Value;
                   Vehiculo[i].Vehiculo.CodigoVehiculo:=cdsVehiculosRUT.FieldByName('CodigoVehiculo').Value;
                   Vehiculo[i].Vehiculo.Patente:=cdsVehiculosRUT.FieldByName('Patente').Value;
                   Vehiculo[i].SerialNumberaMostrar:=cdsVehiculosRUT.FieldByName('SerialNumberaMostrar').AsString;
                   Vehiculo[i].FechaCreacion:=cdsVehiculosRUT.FieldByName('FechaAltaCuenta').AsDateTime;
                   Vehiculo[i].CodigoAlmacenDestino:=cdsVehiculosRUT.FieldByName('CodigoAlmacen').AsInteger;
                   Vehiculo[i].Vehiculo.Marca:=cdsVehiculosRUT.FieldByName('Marca').Value;
                   Vehiculo[i].Vehiculo.Modelo:=cdsVehiculosRUT.FieldByName('Modelo').Value;
                   //Vehiculo[i].Vehiculo.Tipo:=cdsVehiculosRUT.FieldByName('DescripcionTipoVehiculo').Value;       //TASK_106_JMA_20170206
                   Vehiculo[i].Vehiculo.Anio:=cdsVehiculosRUT.FieldByName('AnioVehiculo').Value;
                   Vehiculo[i].FechaBajaTag:=cdsVehiculosRUT.FieldByName('FechaVencimientoTAG').AsDateTime;
                   i:=i+1;
                end;
                cdsVehiculosRUT.Next;
            end;
            Close;
        end
        else
        begin
            ShowMsgBoxCN('Veh�culos Por RUT', '!Debe seleccionar al menos un veh�culo!', MB_ICONWARNING, Self)
        end;
    End;

    {SetLength(Vehiculo, 0);
    if Assigned(Self.OnClickRegSeleccionado) then begin
       self.OnClickRegSeleccionado(cdsVehiculosRUT.FieldByName('CodigoConvenio').Value,cdsVehiculosRUT.FieldByName('CodigoVehiculo').Value);
        SetLength(Vehiculo,16);
        Vehiculo[0] := cdsVehiculosRUT.FieldByName('Patente').Value;
        Vehiculo[1] := cdsVehiculosRUT.FieldByName('SerialNumberaMostrar').Value;
        Vehiculo[2] :='';
        Vehiculo[3] :='';
        Vehiculo[4] :='';
        Vehiculo[5] := cdsVehiculosRUT.FieldByName('FechaAltaCuenta').Value;
        Vehiculo[6] :='';
        Vehiculo[7] :='';
        Vehiculo[8] :='';
        Vehiculo[9] := string(iif(cdsVehiculosRUT.FieldByName('CodigoAlmacen').Value=null,0,cdsVehiculosRUT.FieldByName('CodigoAlmacen').Value)); //TASK_025_ECA_20160606
        Vehiculo[10]:= cdsVehiculosRUT.FieldByName('Marca').Value;
        Vehiculo[11]:= cdsVehiculosRUT.FieldByName('Modelo').Value;
        Vehiculo[12]:= cdsVehiculosRUT.FieldByName('DescripcionTipoVehiculo').Value;
        Vehiculo[13]:= cdsVehiculosRUT.FieldByName('AnioVehiculo').Value;
        Vehiculo[14]:= cdsVehiculosRUT.FieldByName('FechaVencimientoTAG').Value;
        Vehiculo[15]:= '';
       Close;
    end;}
end;
//FIN: TASK_051_ECA_20160707

function TformConvenioCuentasRUT.GetOnClickRegSeleccionado: TOnClickRegSeleccionado;
begin
    result := FOnClickRegSeleccionado;
end;

procedure TformConvenioCuentasRUT.SetOnClickRegSeleccionado(const Value: TOnClickRegSeleccionado);
begin
    FOnClickRegSeleccionado := Value;
end;

//INCIO: TASK_051_ECA_20160707
procedure TformConvenioCuentasRUT.lstConveniosDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin

    if Column = lstConvenios.Columns[0] then begin
		lnCheck.Draw(lstConvenios.Canvas, Rect.Left + 3, Rect.Top + 1, Iif(ds_VehiculosConveniosRUT.DataSet.FieldByName('Chequeado').AsBoolean,1,0));
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;

end;

procedure TformConvenioCuentasRUT.lstConveniosLinkClick(Sender: TCustomDBListEx;
  Column: TDBListExColumn);
var
  contar: Integer;
begin
   ds_VehiculosConveniosRUT.DataSet.Edit;
   ds_VehiculosConveniosRUT.DataSet.FieldByName('Chequeado').AsBoolean:= not ds_VehiculosConveniosRUT.DataSet.FieldByName('Chequeado').AsBoolean;
   ds_VehiculosConveniosRUT.DataSet.Post;

   contar:=0;
   cdsVehiculosRUT.First;
   while not cdsVehiculosRUT.eof do
   begin
       if ds_VehiculosConveniosRUT.DataSet.FieldByName('Chequeado').AsBoolean=False then
       begin
            ClicchkList:=False;
            chkList.Checked:= False;
            ClicchkList:=True;
            Break;
       end
       else
       begin
           contar:=contar+1;
       end;
       cdsVehiculosRUT.Next;
   end;
   cdsVehiculosRUT.First;
   
   if contar=cdsVehiculosRUT.RecordCount then
      chkList.Checked:= True;
end;

procedure TformConvenioCuentasRUT.chkListClick(Sender: TObject);
begin
    if ClicchkList then
        if cdsVehiculosRUT.RecordCount > 0 then
        begin
            cdsVehiculosRUT.First;
            while not cdsVehiculosRUT.eof do
            begin
                ds_VehiculosConveniosRUT.DataSet.Edit;
                ds_VehiculosConveniosRUT.DataSet.FieldByName('Chequeado').AsBoolean:= chkList.Checked;
                ds_VehiculosConveniosRUT.DataSet.Post;
                cdsVehiculosRUT.Next;
            end;
            cdsVehiculosRUT.First;
        end;
end;
//FIN: TASK_051_ECA_20160707
end.
