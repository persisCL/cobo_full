{********************************** File Header ********************************
File Name   : FrmConsultaTransitosNoCliente.pas
Author      : rcastro
Date Created: 11/04/2005
Language    : ES-AR
Description : Consulta los tr�nsitos de Patentes no Clientes

revision 1:
autor :aganc
fecha : 13/06/2006
descripcion : se agrego a la grilla ,la cuatro columnas correspondientes
a los action list leidos de los transitos   .
se modifico el spObtenerTransitosPatenteNoCliente para que su resultado
incluya estos datos

Revision : 2
    Author : fsandi
    Date : 20/11/2006
    Description : se agrego la opcion de busqueda por Televia.
        Se modific� el spObtenerTransitosPatenteNoCliente para que su resultado
        incluya esta b�squeda y para optimizar su funcionamiento.

Revision 3
Author      : mbecerra
Date        : 30-Septiembre-2010
Description :   (Ref SS 926)
                Se valida que la etiqueta ingresada sea v�lida.

                Adem�s se cambia el valor predefinido de 100 a 10.000
                en la cantidad de registros a desplegar

  Firma       : SS_1021_HUR_20120328
  Descripcion : Se agregan los campos Velocidad y Carril.
                Se modifican los indices de las columnas en el procedimiento dbl_TransitosDrawText

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.

Author		: Manuel Cabello Ocaranza
Date		: 14-Febrero-2014
Firma		: SS_1170_MCA_20140214
Description : Se agregan 4 columnas nuevas: Exception Handling, MMI Contact Operator, Illegal Tampering y Not in Holder (TR y NH)


Author		: Manuel Cabello Ocaranza
DAte		: 25/03/2014
Firma		: SS_1175_MCA_20140325
Description	: se unifican las consultas de transitos y transitos no cliente manteniendo la funcionalidad de ambas consultas.
        	se reemplaza nombre de TADOStore spObtenerTransitosPatenteNoCliente por spObtenerTransitosCliente


Author		: Manuel Cabello Ocaranza
DAte		: 03/06/2014
Firma		: SS_1175_MCA_20140603
Description	: se reemplaza el visor de imagenes, reemplazando unit frmImagenesTransitos por FrmvisorImagenes


Firma       : SS_1368_MGO_20150821
Descripcion : Se agrega llamada a SPs ObtenerDatosCuentaPatente y ObtenerDatosCuentaTag;
              para obtener telev�a si ingresa patente, o patente si ingresa telev�a, respectivamente.

Firma       : SS_1368_MGO_20151009
Descripcion : Se agrega validaci�n para resultados NULL
*******************************************************************************}
unit FrmConsultaTransitosNoCliente;

interface

uses
  //consultas transito no cliente
  DmConnection,
  UtilProc,
  peaprocs,
  util,
  UtilDB,
  Peatypes,
  MenuesContextuales,
  FrmAperturaCierrePuntoVenta,
  frmRptCierreTurno,
  frmConfirmaContrasena,
  Frm_CambiarFechaUsoPaseDiario,
  ConstParametrosGenerales,
  //ImagenesTransitos,             								//SS_1175_MCA_20140603
  frmVisorImagenes,                                             //SS_1175_MCA_20140603
  ImgTypes,
  frmImagenesSPC,
  PeaProcsCN,

  // General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx, DB, ADODB,
  DmiCtrls, VariantComboBox, Validate, DateEdit,Abm_obj,RStrings,StrUtils,
  ImgList, TimeEdit;		//SS_1175_MCA_20140325

const						//SS_1175_MCA_20140325
  CONST_RANGO_DIAS = 1;		//SS_1175_MCA_20140325

type
  TFormConsultaTransitosNoCliente = class(TForm)
    Panel1: TPanel;
	GroupBox1: TGroupBox;
    Panel2: TPanel;
	dbl_Transitos: TDBListEx;
    //spObtenerTransitosPatenteNoCliente: TADOStoredProc;                       //SS_1175_MCA_20140325
    spObtenerTransitosCliente: TADOStoredProc;                                  //SS_1175_MCA_20140325
    Panel3: TPanel;
    Panel4: TPanel;
	Label1: TLabel;
	txt_FechaDesde: TDateEdit;
    txt_FechaHasta: TDateEdit;
    Label2: TLabel;
	Label6: TLabel;
	edPatente: TEdit;
	dsTransitos: TDataSource;
    Label3: TLabel;
    Cantidad: TNumericEdit;
    lblTotales: TLabel;
    lnCheck: TImageList;
    ilCamara: TImageList;
    btn_Filtrar: TButton;
    btn_Limpiar: TButton;
    BtnSalir: TButton;
    lblTelevia: TLabel;
    txtTelevia: TEdit;
    HoraDesde: TTimeEdit;				//SS_1175_MCA_20140325
    HoraHasta: TTimeEdit;				//SS_1175_MCA_20140325
    cbEstado: TVariantComboBox;			//SS_1175_MCA_20140325
    lbl1: TLabel;						//SS_1175_MCA_20140325
    lbl2: TLabel;						//SS_1175_MCA_20140325
    cbCategoria: TVariantComboBox;		//SS_1175_MCA_20140325
    lbl3: TLabel;						//SS_1175_MCA_20140325

    cbPortico: TVariantComboBox;		//SS_1175_MCA_20140325
    spObtenerDatosCuentaPatente: TADOStoredProc;  		//SS_1368_MGO_20150821
    spObtenerDatosCuentaTag: TADOStoredProc;		  	  //SS_1368_MGO_20150821
    procedure FormShow(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure dbl_TransitosColumns0HeaderClick(Sender: TObject);
    procedure dbl_TransitosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure dbl_TransitosLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure dbl_TransitosCheckLink(Sender: TCustomDBListEx;Column: TDBListExColumn; var IsLink: Boolean);
    procedure dbl_TransitosContextPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
    procedure BtnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtTeleviaKeyPress(Sender: TObject; var Key: Char);
    procedure dbl_TransitosColumns13HeaderClick(Sender: TObject);				//SS_1170_MCA_20140214
    procedure dbl_TransitosColumns14HeaderClick(Sender: TObject);               //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns7HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns8HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns9HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns10HeaderClick(Sender: TObject);               //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns11HeaderClick(Sender: TObject);               //SS_1170_MCA_20140214
    procedure dbl_TransitosColumns12HeaderClick(Sender: TObject);               //SS_1170_MCA_20140214
  private                                                                       
    Function  ListaTransitosInclude(valor:string):boolean;
    Function  ListaTransitosExclude(valor:string):boolean;
    Function  ListaTransitosIn(valor:string):boolean;
	{ Private declarations }
    procedure CargarComboEstados(EstadoActual: Integer = 0);					//SS_1175_MCA_20140325
    procedure CargarComboCategorias(CategoriaActual: Integer = 0);				//SS_1175_MCA_20140325
    procedure CargarComboPorticos(PorticoActual: Integer = 0);					//SS_1175_MCA_20140325
  public
	{ Public declarations }
	function Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
  end;

var
  FormConsultaTransitosNoCliente: TFormConsultaTransitosNoCliente;
  FListaTransitos:TStringList; //Guarda los transitos seleccionados en
                               //caso de querer realizar un reclamo sobre
                               //ellos.

implementation

{$R *.dfm}


const
//	CONST_COLUMNA_IMAGEN = 9;
//	CONST_COLUMNA_IMAGEN = 13;  // SS_1024_PDO_20120420
	//CONST_COLUMNA_IMAGEN = 15; //SS_1170_MCA_20140214 // SS_1024_PDO_20120420
//	CONST_COLUMNA_IMAGEN = 19;   //SS_1175_MCA_20140325 //SS_1170_MCA_20140214
    CONST_COLUMNA_IMAGEN = 17;   //SS_1175_MCA_20140325 //SS_1170_MCA_20140214



{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.Inicializar
  Author:    rcastro
  Date:      05-May-2005
  Arguments: txtCaption: String; MDIChild: Boolean
  Result:    Boolean
-----------------------------------------------------------------------------}
function TFormConsultaTransitosNoCliente.Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
Var S: TSize;
begin
    Result := True;
	try
		if MDIChild then begin
			S := GetFormClientSize(Application.MainForm);
			SetBounds(0, 0, S.cx, S.cy);
		end else begin
			FormStyle := fsNormal;
			Visible := False;
		end;

		CenterForm(Self);
		Caption := AnsiReplaceStr(txtCaption, '&', '');

		txt_FechaHasta.Date:=Now;
		btn_Limpiar.Click;
        CargarComboEstados(0);													//SS_1175_MCA_20140325
        CargarComboCategorias(0);												//SS_1175_MCA_20140325
        CargarComboPorticos(0);													//SS_1175_MCA_20140325
        lblTotales.Caption := '';                                               //TASK_059_GLE_20170308
        flistatransitos.Clear;                                                  //TASK_059_GLE_20170308
    //INICIA: TASK_059_GLE_20170308
    Except
        on E : Exception do begin
            MsgBoxErr('Error al Inicializar', e.Message, 'Inicializar()', MB_ICONERROR);
            result := False;
        end;
    end;
    {
	except
		result := False;
	end;

	lblTotales.Caption := '';
    flistatransitos.Clear;
    }
    //TERMINA: TASK_059_GLE_20170308
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.FormShow
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.FormShow(Sender: TObject);
begin
	edPatente.SetFocus;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.btn_FiltrarClick
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject
  Result:    None

Revision : 1
    Author : fsandi
    Date : 20/11/2006
    Description :
        - Agregu� la b�squeda por telev�a.
        - Agregu� un la aparici�n de un mensaje en el caso de que no se obtengan
        datos al realizar la b�squeda. 
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.btn_FiltrarClick(Sender: TObject);
resourcestring
    MSG_LICENSE_PLATE           = 'Debe ingresar un valor de patente';
    MSG_ERROR_LOADING_TRANSITS	= 'Error cargando Tr�nsitos';
    MSG_FILTER                  = 'La cantidad de tr�nsitos debe ser mayor que cero';
    MSG_TELEVIA                 = 'El n�mero de Telev�a no es correcto';
    MSG_BUSQUEDA_VACIA          = 'No se encontr� ning�n registro con esos par�metros de b�squeda';
var                             //SS_1368_MGO_20150821
    SerialNumber : Integer;     //SS_1368_MGO_20150821
    Patente : String;           //SS_1368_MGO_20150821
begin
    try                         //TASK_059_GLE_20170308
    spObtenerTransitosCliente.Close;                                            //SS_1175_MCA_20140325
    //spObtenerTransitosPatenteNoCliente.Close;									//SS_1175_MCA_20140325
	//if not ValidateControls([edPatente, Cantidad], [(Trim(edPatente.Text) <> ''), (Cantidad.ValueInt > 0)], Caption, [MSG_LICENSE_PLATE, MSG_FILTER]) then Exit;
    if not validatecontrols([cantidad],[(Cantidad.ValueInt > 0)],caption,[MSG_FILTER]) then exit;

	// Valida que la fecha Desde no sea mayor que la fecha Hasta

// if (txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate) and (txt_FechaDesde.Date>txt_FechaHasta.Date) then begin //SS_1175_MCA_20140325
    	if (txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate) and (txt_FechaDesde.Date>txt_FechaHasta.Date) and
         	((txt_FechaHasta.Date + HoraHasta.Time) - (txt_FechaDesde.Date + HoraDesde.Time) > CONST_RANGO_DIAS)then begin			//SS_1175_MCA_20140325
         	MsgBoxBalloon(MSG_VALIDAR_ORDEN_FECHA,Caption,MB_ICONSTOP,txt_FechaDesde);
	        Exit;
		end;

    // Verifica que el numero de televia este compuesto solamente de n�meros y tenga mas de 10 digitos.

    //REV.3 Se agrega la validaci�n del ContracSerialNumber
    if (txtTelevia.Text <> '') and (
            (Length(Trim(TxtTelevia.Text)) < 10) or (not PermitirSoloDigitos(txtTelevia.Text)) or
            (txtTelevia.Text <> SerialNumberToEtiqueta(EtiquetaToSerialNumber(txtTelevia.Text)))
        ) then  begin

        MsgBoxBalloon(MSG_TELEVIA, Caption, MB_ICONSTOP, txtTelevia);
        Exit;
    end;

    //------------------------------------------------------------------
    //--BEGIN : SS_1368_MGO_20150821------------------------------------
    //------------------------------------------------------------------
    if (edPatente.Text <> '') and (txtTelevia.Text = '') then
  	begin
      with spObtenerDatosCuentaPatente do
  		begin
        Close;
        Parameters.Refresh;
        Parameters.ParamByName('@Patente').Value              := edPatente.Text;
        Parameters.ParamByName('@FechaHora').Value            := null;                 //SS_1368_MGO_20151009
        Parameters.ParamByName('@CodigoConvenio').Value       := null;
        Parameters.ParamByName('@CodigoConvenioFacturacion').Value       := null;     //JLO  20160907
        Parameters.ParamByName('@IndiceVehiculo').Value       := null;
        Parameters.ParamByName('@EstadoCuenta').Value         := null;
        Parameters.ParamByName('@Categoria').Value            := null;
        Parameters.ParamByName('@ContextMark').Value          := null;
        Parameters.ParamByName('@ContractSerialNumber').Value := null;
        Parameters.ParamByName('@CodigoPersona').Value        := null;
        ExecProc;

        //SerialNumber := Parameters.ParamByName('@ContractSerialNumber').Value;          //SS_1368_MGO_20151009
        //txtTelevia.Text := SerialNumberToEtiqueta(SerialNumber);							          //SS_1368_MGO_20151009
        try                                                                               //SS_1368_MGO_20151009
            if not VarIsNull(Parameters.ParamByName('@ContractSerialNumber').Value) then begin  //SS_1368_MGO_20151009
                SerialNumber := (Parameters.ParamByName('@ContractSerialNumber').Value);        //SS_1368_MGO_20151009
                txtTelevia.Text := SerialNumberToEtiqueta(SerialNumber);                        //SS_1368_MGO_20151009
            end;							                                                            //SS_1368_MGO_20151009
        except                                                                            //SS_1368_MGO_20151009
            txtTelevia.Text := '';                                                        //SS_1368_MGO_20151009
        end;                                                                              //SS_1368_MGO_20151009
      end;
    end;

    if (txtTelevia.Text <> '') and (edPatente.Text = '') then
    begin
      with spObtenerDatosCuentaTag do
      begin
        Close;
        Parameters.Refresh;
        Parameters.ParamByName('@ContextMark').Value 			    := null;
        Parameters.ParamByName('@ContractSerialNumber').Value := EtiquetaToSerialNumber(txtTelevia.Text);
        Parameters.ParamByName('@FechaHora').Value            := null;            //SS_1368_MGO_20151009
        Parameters.ParamByName('@CodigoConvenio').Value       := null;
        Parameters.ParamByName('@IndiceVehiculo').Value       := null;
        Parameters.ParamByName('@EstadoCuenta').Value         := null;
        Parameters.ParamByName('@Patente').Value              := null;
        Parameters.ParamByName('@Categoria').Value            := null;
        Parameters.ParamByName('@CodigoPersona').Value        := null;
        Parameters.ParamByName('@CodigoConvenioFacturacion').Value := null;
        ExecProc;

        //Patente := Parameters.ParamByName('@Patente').Value;										//SS_1368_MGO_20151009
		    //edPatente.Text := Patente;                                              //SS_1368_MGO_20151009
        try                                                                       //SS_1368_MGO_20151009
            if not VarIsNull(Parameters.ParamByName('@Patente').Value) then begin //SS_1368_MGO_20151009
                Patente := Parameters.ParamByName('@Patente').Value;              //SS_1368_MGO_20151009
                edPatente.Text := Patente;                                        //SS_1368_MGO_20151009
            end;                                                                  //SS_1368_MGO_20151009
        except                                                                    //SS_1368_MGO_20151009
            edPatente.Text := '';                                                 //SS_1368_MGO_20151009
        end;                                                                      //SS_1368_MGO_20151009
      end;
    end;
    //------------------------------------------------------------------
    //--END : SS_1368_MGO_20150821------------------------------------
    //------------------------------------------------------------------

    spObtenerTransitosCliente.DisableControls;                                  //SS_1175_MCA_20140325
    //spObtenerTransitosPatenteNoCliente.DisableControls						//SS_1175_MCA_20140325
    try
    	//with spObtenerTransitosPatenteNoCliente do begin						//SS_1175_MCA_20140325
        with spObtenerTransitosCliente do begin                                 //SS_1175_MCA_20140325
		    Close;
            CommandTimeout := 500;
		    Parameters.ParamByName('@Patente').Value 		 := iif(edPatente.Text = '',null,edpatente.text);
		    //Parameters.ParamByName('@FechaDesde').Value		 := iif(txt_FechaDesde.Date=nulldate,null,FormatDateTime('yyyy-mm-dd',txt_FechaDesde.Date));								//SS_1175_MCA_20140325
			Parameters.ParamByName('@FechaDesde').Value		 		:= iif(txt_FechaDesde.Date=nulldate,null,FormatDateTime('yyyymmdd hh:nn',(txt_FechaDesde.Date + HoraDesde.Time)));      //SS_1175_MCA_20140325
		    //Parameters.ParamByName('@FechaHasta').Value		 := iif(txt_FechaHasta.Date=nulldate,null,FormatDateTime('yyyy-mm-dd',txt_FechaHasta.Date));                                //SS_1175_MCA_20140325
            Parameters.ParamByName('@FechaHasta').Value		 		:= iif(txt_FechaHasta.Date=nulldate,null,FormatDateTime('yyyymmdd hh:nn',(txt_FechaHasta.Date + HoraHasta.Time)));      //SS_1175_MCA_20140325
            Parameters.ParamByName('@Estado').Value					:= iif(CbEstado.Value = 0, Null, CbEstado.Value);																		//SS_1175_MCA_20140325
            Parameters.ParamByName('@Categoria').Value				:= iif(CbCategoria.Value = 0, Null, CbCategoria.Value);                                                                 //SS_1175_MCA_20140325
        	Parameters.ParamByName('@Portico').Value				:= iif(CbPortico.Value = 0, Null, CbPortico.Value);                                                                     //SS_1175_MCA_20140325
		    Parameters.ParamByName('@CantidadAMostrar').Value		:= Cantidad.ValueInt;
		    Parameters.ParamByName('@CantidadTransitos').Value		:= 0;
            Parameters.ParamByName('@ContractSerialNumber').Value 	:= iif(txtTelevia.Text = '', Null, Peaprocs.EtiquetaToSerialNumber(txtTelevia.Text));
        end;

        //if not(OpenTables([spObtenerTransitosPatenteNoCliente])) then begin	//SS_1175_MCA_20140325
		if not(OpenTables([spObtenerTransitosCliente])) then begin              //SS_1175_MCA_20140325
			  MsgBox(MSG_ERROR_LOADING_TRANSITS,Caption, MB_ICONERROR);
        end;

        //lblTotales.Caption := iif(spObtenerTransitosPatenteNoCliente.IsEmpty, '', 'Visualizados ' + IntToStr(spObtenerTransitosPatenteNoCliente.RecordCount) + 		//SS_1175_MCA_20140325
        lblTotales.Caption := iif(spObtenerTransitosCliente.IsEmpty, '', 'Visualizados ' + IntToStr(spObtenerTransitosCliente.RecordCount) +                            //SS_1175_MCA_20140325
																	' tr�nsitos') ;
        //if spObtenerTransitosPatenteNoCliente.IsEmpty then begin				//SS_1175_MCA_20140325
        if spObtenerTransitosCliente.IsEmpty then begin                         //SS_1175_MCA_20140325
            MsgBox(MSG_BUSQUEDA_VACIA, Caption, MB_ICONINFORMATION);
            lblTotales.Caption := EmptyStr;                                     //SS_1175_MCA_20140325
        end;

    finally
    	//spObtenerTransitosPatenteNoCliente.EnableControls;					//SS_1175_MCA_20140325
	    spObtenerTransitosCliente.EnableControls;                               //SS_1175_MCA_20140325
    end;
	dbl_Transitos.SetFocus;
    //INICIA: TASK_059_GLE_20170308
    Except
        on E : Exception do begin
            MsgBoxErr('Error al Filtrar Tr�nsitos', e.Message, 'btn_FiltrarClick()', MB_ICONERROR);
        end;
    end;
    //TERMINA: TASK_059_GLE_20170308
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.btn_LimpiarClick
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.btn_LimpiarClick(Sender: TObject);
begin
	  edPatente.Clear;
      txtTelevia.Clear;
	  txt_FechaDesde.Clear;
	  txt_FechaHasta.Clear;
      //spObtenerTransitosPatenteNoCliente.close;								//SS_1175_MCA_20140325
      //spObtenerTransitosCliente.close;                                        //SS_1175_MCA_20140325    //TASK_059_GLE_20170308
      if spObtenerTransitosCliente.Active then Close;                                                     //TASK_059_GLE_20170308
      HoraDesde.Time := 0;			                                            //SS_1175_MCA_20140325
      HoraHasta.Time := 0;			                                            //SS_1175_MCA_20140325
      //INICIA: TASK_059_GLE_20170308
      {
      CargarComboCategorias;		                                            //SS_1175_MCA_20140325
      CargarComboEstados;			                                            //SS_1175_MCA_20140325
      CargarComboPorticos;			                                            //SS_1175_MCA_20140325
      }
      //TERMINA: TASK_059_GLE_20170308
      edPatente.SetFocus;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.dbl_TransitosColumns0HeaderClick
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.dbl_TransitosColumns0HeaderClick(Sender: TObject);
begin
	//if spObtenerTransitosPatenteNoCliente.IsEmpty then Exit;					//SS_1175_MCA_20140325
	if spObtenerTransitosCliente.IsEmpty then Exit;                             //SS_1175_MCA_20140325

	if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   	else TDBListExColumn(sender).Sorting := csAscending;
	//spObtenerTransitosPatenteNoCliente.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');		//SS_1175_MCA_20140325
	spObtenerTransitosCliente.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');		//SS_1175_MCA_20140325
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.dbl_TransitosColumns13HeaderClick
  Author:    mcabello
  Date:      14-Feb-2014
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.dbl_TransitosColumns10HeaderClick(
  Sender: TObject);
begin
        if not dbl_Transitos.DataSource.DataSet.Eof then                        //SS_1170_MCA_20140214
	 	MsgBoxBalloon('Lista Gris',                                       		//SS_1170_MCA_20140214
        				'',                                               		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;

procedure TFormConsultaTransitosNoCliente.dbl_TransitosColumns11HeaderClick(
  Sender: TObject);
begin
	if not dbl_Transitos.DataSource.DataSet.Eof then                        	//SS_1170_MCA_20140214
	 	MsgBoxBalloon('Tomar Fotograf�a',                                       //SS_1170_MCA_20140214
        				'',                                               		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;

procedure TFormConsultaTransitosNoCliente.dbl_TransitosColumns12HeaderClick(
  Sender: TObject);
begin
	if not dbl_Transitos.DataSource.DataSet.Eof then                        	//SS_1170_MCA_20140214
	 	MsgBoxBalloon('Cont�cte al Operador',                                   //SS_1170_MCA_20140214
        				'',                                               		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;

procedure TFormConsultaTransitosNoCliente.dbl_TransitosColumns13HeaderClick(    //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
 		if not dbl_Transitos.DataSource.DataSet.Eof then                        //SS_1170_MCA_20140214
	 	MsgBoxBalloon('Telev�a Removido',                                       //SS_1170_MCA_20140214
        				'',                                               		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.dbl_TransitosColumns14HeaderClick
  Author:    mcabello
  Date:      14-Feb-2014
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.dbl_TransitosColumns14HeaderClick(    //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dbl_Transitos.DataSource.DataSet.Eof then                            //SS_1170_MCA_20140214
		MsgBoxBalloon('Telev�a fuera de su base',                               //SS_1170_MCA_20140214
        				'',                                                		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;

procedure TFormConsultaTransitosNoCliente.dbl_TransitosColumns7HeaderClick(		//SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
		if not dbl_Transitos.DataSource.DataSet.Eof then                        //SS_1170_MCA_20140214
	 	MsgBoxBalloon('Lista Negra',                                       		//SS_1170_MCA_20140214
        				'',                                               		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormConsultaTransitosNoCliente.dbl_TransitosColumns8HeaderClick(     //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dbl_Transitos.DataSource.DataSet.Eof then                        	//SS_1170_MCA_20140214
	 	MsgBoxBalloon('Lista Amarilla',                                       	//SS_1170_MCA_20140214
        				'',                                               		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormConsultaTransitosNoCliente.dbl_TransitosColumns9HeaderClick(     //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dbl_Transitos.DataSource.DataSet.Eof then                        	//SS_1170_MCA_20140214
	 	MsgBoxBalloon('Lista Verde',                                       		//SS_1170_MCA_20140214
        				'',                                               		//SS_1170_MCA_20140214
                        MB_ICONQUESTION,                                        //SS_1170_MCA_20140214
                        Mouse.CursorPos.X,                                      //SS_1170_MCA_20140214
                        Mouse.CursorPos.Y);                                     //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosInclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: agrego un valor a la lista de transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormConsultaTransitosNoCliente.ListaTransitosInclude(valor:string):boolean;
begin
    result:=false;
    try
        if not ListaTransitosIn(valor) then flistatransitos.Add(valor);
        result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: txtTeleviaKeyPress
  Author:    fsandi
  Date Created: 20/11/2006
  Description: se verifica que solo se ingresen numeros
  Patameters: none
  Return Value: none
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.txtTeleviaKeyPress(Sender: TObject;
  var Key: Char);
begin
    // #08: Backspace.
    if (not (Key in ['0'..'9', #08])) then Exit;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosExclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: quito un valor a la lista de Transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormConsultaTransitosNoCliente.ListaTransitosExclude(valor:string):boolean;
var index:integer;
begin
    result:=false;
    try
        index:=flistatransitos.IndexOf(valor);
        if index <> -1 then flistatransitos.Delete(index);
        result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosIn
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: Devuelve si un valor esta en la lista transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormConsultaTransitosNoCliente.ListaTransitosIn(valor:string):boolean;
begin
    result := not (flistatransitos.IndexOf(valor) = -1);
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.dbl_TransitosDrawText
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean
  Result:    None

  Revision 2:
      Author : ggomez
      Date : 03/07/2006
      Description :
        - Para las columnas de las action list, cambi� el uso de indice por
        el Nombre del campo.
        - Increment� en 4 (cantidad de columnas de action list) para las
        columnas que eran 10 (Env�o Carta Inf.) y 11 (Fecha Env�o).

-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.dbl_TransitosDrawText(Sender: TCustomDBListEx;
Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
var
	i: Integer;
begin
    // Revision 1
    // Armo las imagenes en las celdas de la grilla donde se muestra un campo boolean

    //BlackList

//    if Column = dbl_transitos.Columns[5] then begin
    if Column.FieldName = 'BlackList' then begin
    	//if (spObtenerTransitosPatenteNoCliente.FieldByName('BlackList').Value = 0) then i := 0		//SS_1175_MCA_20140325
        if (spObtenerTransitosCliente.FieldByName('BlackList').Value = 0) then i := 0                   //SS_1175_MCA_20140325
        else i := 1;
		lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;
    // YellowList
//    if Column = dbl_transitos.Columns[6] then begin
    if Column.FieldName = 'YellowList' then begin
	    //if (spObtenerTransitosPatenteNoCliente.FieldByName('YellowList').Value = 0) then i := 0		//SS_1175_MCA_20140325
        if (spObtenerTransitosCliente.FieldByName('YellowList').Value = 0) then i := 0					//SS_1175_MCA_20140325
        else i := 1;
		lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;
    //GreenList
//    if Column = dbl_transitos.Columns[7] then begin
    if Column.FieldName = 'GreenList' then begin
        //if (spObtenerTransitosPatenteNoCliente.FieldByName('GreenList').Value = 0) then i := 0	   	//SS_1175_MCA_20140325
        if (spObtenerTransitosCliente.FieldByName('GreenList').Value = 0) then i := 0					//SS_1175_MCA_20140325
        else i := 1;
		lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;
    //GrayList
//    if Column = dbl_transitos.Columns[8] then begin
    if Column.FieldName = 'GrayList' then begin
		//if(spObtenerTransitosPatenteNoCliente.FieldByName('GrayList').value = 0) then i := 0 				//SS_1175_MCA_20140325
        if(spObtenerTransitosCliente.FieldByName('GrayList').value = 0) then i := 0 						//SS_1175_MCA_20140325
        else i := 1;
		lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;
    //ExceptionHandling
      if Column.FieldName = 'ExceptionHandling' then begin                                               	//SS_1170_MCA_20140214
      	//if(spObtenerTransitosPatenteNoCliente.FieldByName('ExceptionHandling').value = 0) then i := 0    	//SS_1175_MCA_20140325 //SS_1170_MCA_20140214
        if(spObtenerTransitosCliente.FieldByName('ExceptionHandling').value = 0) then i := 0    			//SS_1175_MCA_20140325 //SS_1170_MCA_20140214
        else i := 1;                                                                                        //SS_1170_MCA_20140214
        lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);                                 //SS_1170_MCA_20140214
        DefaultDraw := False;                                                                               //SS_1170_MCA_20140214
        ItemWidth   := lnCheck.Width + 2;                                                                   //SS_1170_MCA_20140214
    end;                                                                                                    //SS_1170_MCA_20140214
    //ExceptionHandling
    if Column.FieldName = 'MMIContactOperator' then begin                                                   //SS_1170_MCA_20140214
    	//if(spObtenerTransitosPatenteNoCliente.FieldByName('MMIContactOperator').value = 0) then i := 0    //SS_1175_MCA_20140325 //SS_1170_MCA_20140214
        if(spObtenerTransitosCliente.FieldByName('MMIContactOperator').value = 0) then i := 0      			//SS_1175_MCA_20140325 //SS_1170_MCA_20140214
        else i := 1;                                                                                        //SS_1170_MCA_20140214
        lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);                                 //SS_1170_MCA_20140214
        DefaultDraw := False;                                                                               //SS_1170_MCA_20140214
        ItemWidth   := lnCheck.Width + 2;                                                                   //SS_1170_MCA_20140214
    end;                                                                                                    //SS_1170_MCA_20140214
    //IllegalTampering
    if Column.FieldName = 'IllegalTampering' then begin                                         			//SS_1170_MCA_20140214
	    //if(spObtenerTransitosPatenteNoCliente.FieldByName('IllegalTampering').value = 0) then i := 0     	//SS_1175_MCA_20140325 //SS_1170_MCA_20140214
        if(spObtenerTransitosCliente.FieldByName('IllegalTampering').value = 0) then i := 0     			//SS_1175_MCA_20140325 //SS_1170_MCA_20140214
        else i := 1;                                                                                        //SS_1175_MCA_20140325 //SS_1170_MCA_20140214
        lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);                                 //SS_1170_MCA_20140214
        DefaultDraw := False;                                                                               //SS_1170_MCA_20140214
        ItemWidth   := lnCheck.Width + 2;                                                                   //SS_1170_MCA_20140214
    end;                                                                                                    //SS_1170_MCA_20140214
    //NotInHolder
    if Column.FieldName = 'NotInHolder' then begin                                              			//SS_1170_MCA_20140214
    	//if(spObtenerTransitosPatenteNoCliente.FieldByName('NotInHolder').value = 0) then i := 0           //SS_1175_MCA_20140325 //SS_1170_MCA_20140214
        if(spObtenerTransitosCliente.FieldByName('NotInHolder').value = 0) then i := 0             			//SS_1175_MCA_20140325 //SS_1170_MCA_20140214
        else i := 1;                                                                            			//SS_1170_MCA_20140214
        lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);                     			//SS_1170_MCA_20140214
        DefaultDraw := False;                                                                   			//SS_1170_MCA_20140214
        ItemWidth   := lnCheck.Width + 2;                                                       			//SS_1170_MCA_20140214
    end;

    // Fin  Revision 1

	// Muestra si esta seleccionado para realizar un reclamo o no.
	if Column = dbl_transitos.Columns[0] then begin
		//verifica si esta seleccionado o no
        //i := iif(not ListaTransitosIn(spObtenerTransitosPatenteNoCliente.FieldByName('NumCorrCA').AsString), 0, 1);	//SS_1175_MCA_20140325
		i := iif(not ListaTransitosIn(spObtenerTransitosCliente.FieldByName('NumCorrCA').AsString), 0, 1);				//SS_1175_MCA_20140325
		//i := iif(ObtenerCasosValidacionPermitidos.RecNo in FCasosElegidos, 0, 1);
		lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;

    // Inicio bloque SS_1021_PDO_20120420
    {
    //esta era 6 + 4 +2=10
	if Column = dbl_Transitos.Columns[12] then begin    // SS_1021_HUR_20120329
		Text := RStr(spObtenerTransitosPatenteNoCliente.FieldByName('Importe').AsFloat, 10, 2)
	end	else begin    //esta era 1 + 4 =5
		if Column = dbl_Transitos.Columns[7] then begin  // SS_1021_HUR_20120329
			Text := FormatDateTime('dd/mm/yyyy hh:mm', spObtenerTransitosPatenteNoCliente.FieldByName('FechaHoraTransito').AsDateTime)
		end	else begin
			if (Column = dbl_Transitos.Columns[CONST_COLUMNA_IMAGEN]) and
				(spObtenerTransitosPatenteNoCliente.FieldByName('RegistrationAccessibility').AsInteger > 0)
			then begin
				ilCamara.Draw(dbl_transitos.Canvas, Rect.Left + 20, Rect.Top + 1,
								iif(spObtenerTransitosPatenteNoCliente.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0));
				DefaultDraw := False;
				ItemWidth   := ilCamara.Width + 2;
			end
		end
	end;
    }
    if column.FieldName = 'Importe' then begin
		//Text := RStr(spObtenerTransitosPatenteNoCliente.FieldByName('Importe').AsFloat, 10, 2)			//SS_1175_MCA_20140325
        //Text := RStr(spObtenerTransitosCliente.FieldByName('Importe').AsString  , 10, 2)
        Text := spObtenerTransitosCliente.FieldByName('Importe').AsString	//JLO  20160907						//SS_1175_MCA_20140325
    end;

    if column.FieldName = 'FechaHoraTransito' then begin
      //Text := FormatDateTime('dd/mm/yyyy hh:mm', spObtenerTransitosPatenteNoCliente.FieldByName('FechaHoraTransito').AsDateTime)		//SS_1175_MCA_20140325
      Text := FormatDateTime('dd/mm/yyyy hh:mm', spObtenerTransitosCliente.FieldByName('FechaHoraTransito').AsDateTime)					//SS_1175_MCA_20140325
    end;

    with spObtenerTransitosCliente do begin
        if (Column = dbl_Transitos.Columns[CONST_COLUMNA_IMAGEN]) and (FieldByName('RegistrationAccessibility').AsInteger > 0) then begin       		//SS_1175_MCA_20140325
            ilCamara.Draw(dbl_transitos.Canvas, Rect.Left + 20, Rect.Top + 1,
                            //iif(spObtenerTransitosPatenteNoCliente.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0)); 	//SS_1175_MCA_20140325
                            iif(FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0)); 				//SS_1175_MCA_20140325
            DefaultDraw := False;
            ItemWidth   := ilCamara.Width + 2;
        end;
    end;
    // Fin bloque SS_1021_PDO_20120420

//INICIA: TASK_048_GLE_20170206

	//Muestra la velocidad       Columna 5 - Velocidad
    if Column.FieldName = 'Velocidad' then begin //Column = dbl_transitos.Columns[5] then begin
        Text := spObtenerTransitosCliente.FieldByName('Velocidad').AsString;
    end;

    //Muestra si hay Pase Diario       Columna 18 - DayPass
    if Column.FieldName = 'DayPass' then begin //Column = dbl_transitos.Columns[18] then begin
        Text := spObtenerTransitosCliente.FieldByName('DayPass').AsString;
    end;

     //Muestra si se envi� o no carta  Columna 19 - Env�o Carta Inf.
	if Column = dbl_transitos.Columns[19] then begin
		Text := iif(spObtenerTransitosCliente.FieldByName('FechaDeEnvio').AsString = '', 'NO', 'SI');
 	end;

	//Muestra la fecha de envio       Columna 20 - Fecha Env�o
    if Column.FieldName = 'FechaDeEnvio' then begin //Column = dbl_transitos.Columns[20] then begin
        Text := spObtenerTransitosCliente.FieldByName('FechaDeEnvio').AsString;
    end;

    //Muestra el c�digo de la carta   Columna 21 - C�digo Carta Inf.
    if Column.FieldName = 'CodigoCartaAInfractor' then begin //Column = dbl_transitos.Columns[21] then begin
        Text := spObtenerTransitosCliente.FieldByName('CodigoCartaAInfractor').AsString;
    end;

    //Muestra el disco                Columna 22 - Disco
    if Column.FieldName = 'Disco' then begin //Column = dbl_transitos.Columns[22] then begin
        Text := spObtenerTransitosCliente.FieldByName('Disco').AsString;
    end;

    //Muestra el Archivo                Columna 23 - Archivo
    if Column.FieldName = 'Archivo' then begin //Column = dbl_transitos.Columns[23] then begin
        Text := spObtenerTransitosCliente.FieldByName('Archivo').AsString;
    end;

    //Muestra el Nombre del Archivo     Columna 24 - Nombre Archivo
    if Column.FieldName = 'NombreArchivo' then begin //Column = dbl_transitos.Columns[24] then begin
        Text := spObtenerTransitosCliente.FieldByName('NombreArchivo').AsString;
    end
    //TERMINA: TASK_048_GLE_20170206//TASK_048_GLE_20170206 Comenta - Sin uso

end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.dbl_TransitosLinkClick
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TCustomDBListEx; Column: TDBListExColumn
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.dbl_TransitosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
Var
	Numcorrca: string;
    //EsItaliano : Boolean; //TASK_048_GLE_20170206 Comenta - Sin uso
    lImagen: string;
    ImgSPCForm: TImagenesSPCForm;                                                                                 //SS_1175_MCA_20140603
begin
	//EsItaliano:= False;	 //SS_1175_MCA_20140603    //TASK_048_GLE_20170206 Comenta - Sin uso
    //selecciono/desselecciono transito
    if Column = dbl_transitos.Columns[0] then begin
    	//Numcorrca := spObtenerTransitosPatenteNoCliente.FieldByName('numcorrca').Asstring;					//SS_1175_MCA_20140325
        Numcorrca := spObtenerTransitosCliente.FieldByName('numcorrca').Asstring;								//SS_1175_MCA_20140325
	    if ListaTransitosin(Numcorrca) then ListaTransitosExclude(Numcorrca)
	    else ListaTransitosInclude(Numcorrca);
        //dibuja el cambio
  	    Sender.Invalidate;
        Sender.Repaint;

        
    end;


    if Column = dbl_transitos.Columns[CONST_COLUMNA_IMAGEN] then try

        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if spObtenerTransitosCliente['RegistrationAccessibility'] > 0 then try
            ImgSPCForm := TImagenesSPCForm.Create(Self);

            if ImgSPCForm.Inicializar(spObtenerTransitosCliente['numcorrca'], spObtenerTransitosCliente['RegistrationAccessibility']) then begin

                ImgSPCForm.ShowModal;
            end
            else ShowMsgBoxCN('Im�genes del Tr�nsito','No existen Im�genes disponibles para el Tr�nsito seleccionado.', MB_ICONINFORMATION, Self);
        finally
            if Assigned(ImgSPCForm) then FreeAndNil(ImgSPCForm);
        end;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;

end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.dbl_TransitosCheckLink
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.dbl_TransitosCheckLink(Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
    if (column = dbl_Transitos.Columns[CONST_COLUMNA_IMAGEN]) then begin
		IsLink := (spObtenerTransitosCliente['RegistrationAccessibility'] > 0);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: dblTransitosContextPopup
  Author: lgisuk
  Date Created: 14/04/2005
  Description: asigna el menu contextual a la grilla
  Parameters: Sender: TObject; MousePos: TPoint; var Handled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.dbl_TransitosContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
    //if not spObtenerTransitosPatenteNoCliente.IsEmpty then Handled :=			//SS_1175_MCA_20140325
        if not spObtenerTransitosCliente.IsEmpty then Handled :=				//SS_1175_MCA_20140325
    MostrarMenuContextualMultiplesTransitosNoCliente(dbl_Transitos.ClientToScreen(MousePos),Flistatransitos);
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.BtnSalirClick
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.BtnSalirClick(Sender: TObject);
begin
	//spObtenerTransitosPatenteNoCliente.Close;									//SS_1175_MCA_20140325
    spObtenerTransitosCliente.Close;											//SS_1175_MCA_20140325
	Close;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosNoCliente.FormClose
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject; var Action: TCloseAction
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosNoCliente.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
	 action := caFree;
end;

procedure TFormConsultaTransitosNoCliente.CargarComboEstados(EstadoActual: Integer = 0);			//SS_1175_MCA_20140325
resourceString                                                                                  	//SS_1175_MCA_20140325
    MSG_CUALQUIER_ESTADO = 'Todos';                                                                 //SS_1175_MCA_20140325
var                                                                                                 //SS_1175_MCA_20140325
    //indice: Integer;                                                                              //SS_1175_MCA_20140325
	  Qry: TADOQuery;                                                                               //SS_1175_MCA_20140325
begin                                                                                               //SS_1175_MCA_20140325
	  Qry := TAdoQuery.Create(nil);                                                                 //SS_1175_MCA_20140325
	  try                                                                                           //SS_1175_MCA_20140325
		    Qry.Connection := DmConnections.BaseCAC;                                                //SS_1175_MCA_20140325
		    Qry.SQL.Text := ' select * from EstadoTransitos  WITH (NOLOCK) ORDER BY Descripcion';   //SS_1175_MCA_20140325
		    Qry.Open;                                                                               //SS_1175_MCA_20140325
		    CbEstado.Clear;                                                                         //SS_1175_MCA_20140325
        CbEstado.Items.Add(MSG_CUALQUIER_ESTADO, 0);                                                //SS_1175_MCA_20140325
        //indice := 0;                                                                              //SS_1175_MCA_20140325
        While not Qry.Eof do begin                                                                  //SS_1175_MCA_20140325
			      CbEstado.Items.Add(Qry.FieldByName('Descripcion').AsString,                       //SS_1175_MCA_20140325
            Istr(Qry.FieldByName('Estado').AsInteger));                                             //SS_1175_MCA_20140325
			      if (Qry.FieldByName('Estado').AsInteger = EstadoActual) then begin                //SS_1175_MCA_20140325
            	//indice := CbEstado.Items.Count - 1;                                               //SS_1175_MCA_20140325
        end;                                                                                        //SS_1175_MCA_20140325
        Qry.Next;                                                                                   //SS_1175_MCA_20140325
		end;                                                                                        //SS_1175_MCA_20140325
		CbEstado.ItemIndex := 0;                                                                    //SS_1175_MCA_20140325
	  finally                                                                                       //SS_1175_MCA_20140325
		    Qry.Close;                                                                              //SS_1175_MCA_20140325
		    Qry.Free;                                                                               //SS_1175_MCA_20140325
	  end;                                                                                          //SS_1175_MCA_20140325
end;                                                                                                //SS_1175_MCA_20140325

procedure TFormConsultaTransitosNoCliente.CargarComboCategorias(CategoriaActual: Integer = 0);      //SS_1175_MCA_20140325
resourceString                                                                                      //SS_1175_MCA_20140325
    MSG_CUALQUIER_CATEGORIA = 'Todas';                                                              //SS_1175_MCA_20140325
var                                                                                                 //SS_1175_MCA_20140325
    //indice: Integer;                                                                              //SS_1175_MCA_20140325
	  Qry: TADOQuery;                                                                               //SS_1175_MCA_20140325
begin                                                                                               //SS_1175_MCA_20140325
	  Qry := TAdoQuery.Create(nil);                                                                 //SS_1175_MCA_20140325
	  try                                                                                           //SS_1175_MCA_20140325
		    Qry.Connection := DmConnections.BaseCAC;                                                //SS_1175_MCA_20140325
		    Qry.SQL.Text := ' select * from Categorias  WITH (NOLOCK) ORDER BY Descripcion';        //SS_1175_MCA_20140325
		    Qry.Open;                                                                               //SS_1175_MCA_20140325
		    CbCategoria.Clear;                                                                      //SS_1175_MCA_20140325
        CbCategoria.Items.Add(MSG_CUALQUIER_CATEGORIA, 0);                                          //SS_1175_MCA_20140325
        //indice := 0;                                                                              //SS_1175_MCA_20140325
        While not Qry.Eof do begin                                                                  //SS_1175_MCA_20140325
			      CbCategoria.Items.Add(Qry.FieldByName('Descripcion').AsString,                    //SS_1175_MCA_20140325
        //INICIA: TASK_059_GLE_20170308
        {
		Istr(Qry.FieldByName('Categoria').AsInteger));                                          //SS_1175_MCA_20140325
        if (Qry.FieldByName('Categoria').AsInteger = CategoriaActual) then begin          //SS_1175_MCA_20140325
        //indice := CbCategoria.Items.Count - 1;                                        //SS_1175_MCA_20140325
        end;                                                                              //SS_1175_MCA_20140325
        }
        Istr(Qry.FieldByName('CodigoCategoria').AsInteger));
        //TERMINA: TASK_059_GLE_20170308
		Qry.Next;                                                                             //SS_1175_MCA_20140325
		end;                                                                                        //SS_1175_MCA_20140325
		CbCategoria.ItemIndex := 0;
        If Qry.Active then Qry.Close;                                                               //TASK_059_GLE_20170308
        Qry.Free;                                                                                   //TASK_059_GLE_20170308
        //INICIA: TASK_059_GLE_20170308
        Except
            on E : Exception do begin
                MsgBoxErr('Error al Cargar Combo Categorias', e.Message, 'CargarComboCategorias()', MB_ICONERROR);
                If Qry.Active then Qry.Close;
		        Qry.Free;
            end;
        end;
        {
  	    finally                                                                                     //SS_1175_MCA_20140325
	  	    Qry.Close;                                                                              //SS_1175_MCA_20140325
		    Qry.Free;                                                                               //SS_1175_MCA_20140325
	    end;                                                                                        //SS_1175_MCA_20140325
        }
        //TERMINA: TASK_059_GLE_20170308
end;                                                                                                //SS_1175_MCA_20140325

procedure TFormConsultaTransitosNoCliente.CargarComboPorticos(PorticoActual: Integer = 0);          //SS_1175_MCA_20140325
resourceString                                                                                      //SS_1175_MCA_20140325
    MSG_CUALQUIER_PORTICO = 'Todas';                                                                //SS_1175_MCA_20140325
var                                                                                                 //SS_1175_MCA_20140325
    //indice: Integer;                                                                              //SS_1175_MCA_20140325
	  Qry: TADOQuery;                                                                               //SS_1175_MCA_20140325
begin                                                                                               //SS_1175_MCA_20140325
	  Qry := TAdoQuery.Create(nil);                                                                 //SS_1175_MCA_20140325
	  try                                                                                           //SS_1175_MCA_20140325
		    Qry.Connection := DmConnections.BaseCAC;                                                //SS_1175_MCA_20140325
		    Qry.SQL.Text := ' select NumeroPuntoCobro, Descripcion from PuntosCobro  WITH (NOLOCK) ORDER BY Descripcion'; //SS_1175_MCA_20140325
		    Qry.Open;                                                                               //SS_1175_MCA_20140325
		    CbPortico.Clear;                                                                        //SS_1175_MCA_20140325
        CbPortico.Items.Add(MSG_CUALQUIER_PORTICO, 0);                                              //SS_1175_MCA_20140325
        //indice := 0;                                                                              //SS_1175_MCA_20140325
        While not Qry.Eof do begin                                                                  //SS_1175_MCA_20140325
			      CbPortico.Items.Add(Qry.FieldByName('Descripcion').AsString,                      //SS_1175_MCA_20140325
            Istr(Qry.FieldByName('NumeroPuntoCobro').AsInteger));                                   //SS_1175_MCA_20140325
			      if (Qry.FieldByName('NumeroPuntoCobro').AsInteger = PorticoActual) then begin     //SS_1175_MCA_20140325
                //indice := CbPortico.Items.Count - 1;                                              //SS_1175_MCA_20140325
        end;                                                                                        //SS_1175_MCA_20140325
			  Qry.Next;                                                                             //SS_1175_MCA_20140325
		end;                                                                                        //SS_1175_MCA_20140325
		CbPortico.ItemIndex := 0;                                                                   //SS_1175_MCA_20140325
  	finally                                                                                         //SS_1175_MCA_20140325
	  	Qry.Close;                                                                                  //SS_1175_MCA_20140325
		  Qry.Free;                                                                                 //SS_1175_MCA_20140325
	  end;                                                                                          //SS_1175_MCA_20140325
end;                                                                                                //SS_1175_MCA_20140325



initialization
    FListaTransitos := TStringList.Create;
finalization
    FreeAndNil(FlistaTransitos);
end.
