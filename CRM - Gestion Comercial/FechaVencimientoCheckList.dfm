object FrmFechaVencimientoCheckList: TFrmFechaVencimientoCheckList
  Left = 286
  Top = 209
  BorderStyle = bsDialog
  Caption = 'Lista de chequeo'
  ClientHeight = 89
  ClientWidth = 228
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 5
    Top = 6
    Width = 218
    Height = 47
  end
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 94
    Height = 13
    Caption = 'Fecha Vencimiento:'
  end
  object txt_FechaVencimiento: TDateEdit
    Left = 120
    Top = 20
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 0
    Date = -693594
  end
  object btn_Cancelar: TButton
    Left = 57
    Top = 59
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 1
  end
  object btn_Aceptar: TButton
    Left = 141
    Top = 59
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = btn_AceptarClick
  end
end
