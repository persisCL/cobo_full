{-----------------------------------------------------------------------------
 Unit Name: FrmReporteAnaliticoCierreDeTurno
 Author:    ggomez
 Purpose: Reporte con el Anal�tico de Cierre de Turno.
 History:

 Firma        : SS_1147_NDR_20140710
 Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.
-----------------------------------------------------------------------------}


unit FrmReporteAnaliticoCierreDeTurno;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppComm, ppRelatv, ppProd, ppClass, ppReport, UtilRB, DB, ppDB,
  ppDBPipe, ADODB,
  DMConnection, UtilProc, ppBands, ppCache, ppVar, ppCtrls, ppPrnabl,
  ppParameter,
  ConstParametrosGenerales;                                                     //SS_1147_NDR_20140710;

type
  TFormReporteAnaliticoCierreDeTurno = class(TForm)
    rbi_AnaliticoCierreDeTurno: TRBInterface;
    rpt_analiticoCierreDeTurno: TppReport;
    pln_DatosReporte: TppDBPipeline;
    dsDatosReporte: TDataSource;
    sp_ObtenerAnaliticoCierreDeTurno: TADOStoredProc;
    ppParameterList1: TppParameterList;
    hr_Reporte: TppHeaderBand;
    img_Logo: TppImage;
    Titulo: TppLabel;
    ppLabel3: TppLabel;
    txt_TextoLogo: TppLabel;
    lbl_TituloNumeroTurno: TppLabel;
    lbl_NumeroTurno: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    dt_CodigoMedioPago: TppDetailBand;
    txt_Comprobante: TppDBText;
    ft_Reporte: TppFooterBand;
    lbl_Pagina: TppLabel;
    lbl_NumeroPagina: TppSystemVariable;
    ppGroup1: TppGroup;
    grhd_CodigoMedioPago: TppGroupHeaderBand;
    txt_MedioDePago: TppDBText;
    grft_CodigoMedioPago: TppGroupFooterBand;
    txt_Total: TppDBCalc;
    lbl_TipoDePago: TppLabel;
    lbl_ColComprobante: TppLabel;
    lbl_Fecha: TppLabel;
    txt_FechaEmision: TppDBText;
    lbl_Importe: TppLabel;
    txt_Importe: TppDBText;
    lbl_FechaPago: TppLabel;
    txt_FechaHora: TppDBText;
    lbl_Total: TppLabel;
    lbl_Banco: TppLabel;
    txt_Banco: TppDBText;
    lbl_NumeroCheque: TppLabel;
    txt_NumeroCheque: TppDBText;
    lbl_FechaCheque: TppLabel;
    txt_FechaCheque: TppDBText;
    lbl_TitUsuarioTurno: TppLabel;
    lbl_UsuarioTurno: TppLabel;
    ppLine3: TppLine;
    txt_CodigoTipoMedioPago: TppDBText;
    sh_MedioPago: TppShape;
    ppLine4: TppLine;
    procedure rbi_AnaliticoCierreDeTurnoExecute(Sender: TObject;
      var Cancelled: Boolean);
    procedure lbl_BancoPrint(Sender: TObject);
    procedure lbl_NumeroChequePrint(Sender: TObject);
    procedure lbl_FechaChequePrint(Sender: TObject);
    procedure txt_BancoPrint(Sender: TObject);
    procedure txt_NumeroChequePrint(Sender: TObject);
    procedure txt_FechaChequePrint(Sender: TObject);
  private
    { Private declarations }
    FNumeroTurno: Integer;
    FUsuarioTurno: AnsiString;
    function MostrarCheque: Boolean;
  public
    { Public declarations }
	function Inicializar (NumeroTurno: Integer; UsuarioTurno: AnsiString): Boolean;

  end;

var
  FormReporteAnaliticoCierreDeTurno: TFormReporteAnaliticoCierreDeTurno;

implementation

{$R *.dfm}

{ TFormReporteAnaliticoCierreDeTurno }

function TFormReporteAnaliticoCierreDeTurno.Inicializar(
  NumeroTurno: Integer; UsuarioTurno: AnsiString): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo, TextoLogo: AnsiString;                                              //SS_1147_NDR_20140710
begin
  ObtenerParametroGeneral(FConnectionDatosBase, LOGO_REPORTES, RutaLogo);       //SS_1147_NDR_20140710
	try                                                                           //SS_1147_NDR_20140710
		img_Logo.Picture.LoadFromFile(Trim(RutaLogo));                              //SS_1147_NDR_20140710
	except                                                                        //SS_1147_NDR_20140710
		On E: Exception do begin                                                    //SS_1147_NDR_20140710
			Screen.Cursor := crDefault;                                               //SS_1147_NDR_20140710
			MsgBox(E.message, Self.Caption, MB_ICONSTOP);                             //SS_1147_NDR_20140710
			Screen.Cursor := crHourGlass;                                             //SS_1147_NDR_20140710
		end;                                                                        //SS_1147_NDR_20140710
	end;                                                                          //SS_1147_NDR_20140710

  ObtenerParametroGeneral(FConnectionDatosBase, TEXTO_LOGO_REPORTES, TextoLogo);//SS_1147_NDR_20140710
	try                                                                           //SS_1147_NDR_20140710
  	txt_TextoLogo.Text := TextoLogo;                                            //SS_1147_NDR_20140710
	except                                                                        //SS_1147_NDR_20140710
		On E: Exception do begin                                                    //SS_1147_NDR_20140710
			Screen.Cursor := crDefault;                                               //SS_1147_NDR_20140710
			MsgBox(E.message, Self.Caption, MB_ICONSTOP);                             //SS_1147_NDR_20140710
			Screen.Cursor := crHourGlass;                                             //SS_1147_NDR_20140710
		end;                                                                        //SS_1147_NDR_20140710
	end;                                                                          //SS_1147_NDR_20140710

	FNumeroTurno := NumeroTurno;
  FUsuarioTurno := UsuarioTurno;
	rbi_AnaliticoCierreDeTurno.Execute;
	Result := True;
end;

procedure TFormReporteAnaliticoCierreDeTurno.rbi_AnaliticoCierreDeTurnoExecute(
  Sender: TObject; var Cancelled: Boolean);
resourcestring
	MSG_ERROR_AL_IMPRIMIR_INFORME = 'Error al imprimir el informe.';
	MSG_ERROR = 'Error';
begin
	try
		try
			Screen.Cursor := crHourGlass;
            with sp_ObtenerAnaliticoCierreDeTurno, Parameters do begin
            	Close;
                ParamByName('@NumeroTurno').Value := FNumeroTurno;
                Open;
            end; // with

            lbl_NumeroTurno.Caption := IntToStr(FNumeroTurno);
            lbl_UsuarioTurno.Caption := FUsuarioTurno;

		finally
			Screen.Cursor := crDefault;
		end
	except
		on E: Exception do begin
			MsgBoxErr(MSG_ERROR_AL_IMPRIMIR_INFORME, E.Message, MSG_ERROR,
            	MB_ICONERROR );
		end;
	end;
end;

procedure TFormReporteAnaliticoCierreDeTurno.lbl_BancoPrint(
  Sender: TObject);
begin
	TppLabel(Sender).Visible := MostrarCheque;
end;

function TFormReporteAnaliticoCierreDeTurno.MostrarCheque: Boolean;
begin
	// 4, es el c�digo de Medio de Pago para Cheque.
	Result := (txt_CodigoTipoMedioPago.Text = '4');
end;

procedure TFormReporteAnaliticoCierreDeTurno.lbl_NumeroChequePrint(
  Sender: TObject);
begin
	TppLabel(Sender).Visible := MostrarCheque;
end;

procedure TFormReporteAnaliticoCierreDeTurno.lbl_FechaChequePrint(
  Sender: TObject);
begin
	TppLabel(Sender).Visible := MostrarCheque;
end;

procedure TFormReporteAnaliticoCierreDeTurno.txt_BancoPrint(
  Sender: TObject);
begin
	TppDBText(Sender).Visible := MostrarCheque;

end;

procedure TFormReporteAnaliticoCierreDeTurno.txt_NumeroChequePrint(
  Sender: TObject);
begin
	TppDBText(Sender).Visible := MostrarCheque;

end;

procedure TFormReporteAnaliticoCierreDeTurno.txt_FechaChequePrint(
  Sender: TObject);
begin
	TppDBText(Sender).Visible := MostrarCheque;

end;

end.
