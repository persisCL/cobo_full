unit PatronBusqueda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBClient;

type
  TfrmPatronBusqueda = class(TForm)
    edBuscar: TEdit;
    btnBuscar: TButton;
    lbl1: TLabel;
    procedure btnBuscarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure edBuscarKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPatronBusqueda: TfrmPatronBusqueda;

implementation

uses ABMPersonas;

{$R *.dfm}

procedure TfrmPatronBusqueda.btnBuscarClick(Sender: TObject);

begin
  TEdit(TForm(Owner).FindComponent('edFiltro')).Text := edBuscar.Text;
  Close;
 end;


procedure TfrmPatronBusqueda.edBuscarKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
    btnBuscarClick(nil);
end;

procedure TfrmPatronBusqueda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmPatronBusqueda.FormCreate(Sender: TObject);
begin
   edBuscar.Text :=  TEdit(TForm(Owner).FindComponent('edFiltro')).Text;
end;

end.
