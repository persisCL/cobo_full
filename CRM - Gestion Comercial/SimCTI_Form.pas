unit SimCTI_Form;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DdeMan, StdCtrls, DmiCtrls, Util, UtilProc, Db, DBTables, ADODB, UtilDb,
  BuscaClientes, DmConnection, variants, DPSControls;

type
  TFormSimCTI = class(TForm)
    CodigoCliente: TNumericEdit;
	Label1: TLabel;
    DdeClientConv: TDdeClientConv;
    CrearLlamada: TADOStoredProc;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    OPButton1: TDPSButton;
    btn_aceptar: TDPSButton;
    btn_Cancelar: TDPSButton;
    txt_ApellidoNombre: TEdit;
    Label5: TLabel;
    procedure DdeClientConvClose(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btn_aceptarClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    procedure CodigoClienteChange(Sender: TObject);
    procedure OPButton1Click(Sender: TObject);
  private
    { Private declarations }
	FConectado: Boolean;
    TipoContacto: AnsiString;
  public
    { Public declarations }
  end;

var
  FormSimCTI: TFormSimCTI;

implementation

{$R *.DFM}

procedure TFormSimCTI.DdeClientConvClose(Sender: TObject);
begin
	FConectado := False;
end;

procedure TFormSimCTI.FormCreate(Sender: TObject);
begin
	FConectado := False;
end;

procedure TFormSimCTI.btn_aceptarClick(Sender: TObject);
resourcestring
    MSG_CONEXION = 'No se puede establecer la conexión con la aplicación.';
    MSG_CREARLLAMADA = 'No se pudo crear la llamada.';
    CAPTION_CREARLLAMADA = 'Crear Llamada';
Var
	Str: AnsiString;
begin
	try
    	if not FConectado then begin
			if not DDEClientConv.SetLink('CAC', 'CTI') then
                Raise exception.create(MSG_CONEXION);
   			if not DDEClientConv.OpenLink then
                Raise exception.create(MSG_CONEXION);
            FConectado := True;
        end;
        CrearLlamada.Parameters.ParamByName('@CodigoContacto').Value := CodigoCliente.Value;
        CrearLlamada.Parameters.ParamByName('@TipoContacto').Value := TipoContacto;
        CrearLlamada.Parameters.ParamByName('@CodigoLlamada').Value := Null;
		CrearLlamada.ExecProc;
		Str := '^048|2|01|' + IntToStr(CrearLlamada.Parameters.ParamByName('@CodigoLlamada').Value);
		Str := Copy(Str, 1, 1) + IStr0(Length(Str), 3) + Copy(Str, 5, 255);
		if not DDEClientConv.PokeData('CTI_Data', PChar(Str)) then
          Raise exception.create('Error al enviar PokeData');
	except
		on E: Exception do begin
        	MsgBoxErr(MSG_CREARLLAMADA, e.message, CAPTION_CREARLLAMADA, MB_ICONSTOP);
        end;
	end;

end;

procedure TFormSimCTI.btn_CancelarClick(Sender: TObject);
begin
	Close;
end;

procedure TFormSimCTI.CodigoClienteChange(Sender: TObject);
begin
	btn_Aceptar.Enabled :=  (CodigoCliente.Value > 0);
end;

procedure TFormSimCTI.OPButton1Click(Sender: TObject);
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa('','DNI', '', '', True(*soloclientes*)) and (f.Showmodal = mrOk) then begin
    	CodigoCliente.Value		:= f.CodigoCliente;
        TipoContacto            := f.TipoContacto;
        txt_ApellidoNombre.Text	:= Trim(f.Apellido) + ', ' + Trim(f.Nombre);
    end;
    f.Release;
end;

end.
