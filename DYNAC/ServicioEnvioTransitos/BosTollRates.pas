// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost:54912/BosTollRates.svc?wsdl
//  >Import : http://localhost:54912/BosTollRates.svc?wsdl:0
//  >Import : http://localhost:54912/BosTollRates.svc?xsd=xsd0
//  >Import : http://localhost:54912/BosTollRates.svc?xsd=xsd2
//  >Import : http://localhost:54912/BosTollRates.svc?xsd=xsd1
// Encoding : utf-8
// Version  : 1.0
// (3/15/2017 4:40:41 PM - - $Rev: 10138 $)
// ************************************************************************ //

unit BosTollRates;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_UNBD = $0002;
  IS_NLBL = $0004;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:decimal         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:dateTime        - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:int             - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:long            - "http://www.w3.org/2001/XMLSchema"[Gbl]

  receiveTollRatesBOSData = class;              { "http://schemas.datacontract.org/2004/07/DynacTest"[GblCplx] }
  gantryData           = class;                 { "http://schemas.datacontract.org/2004/07/DynacTest"[GblCplx] }
  receiveTollRatesBOSDataResponse = class;      { "http://schemas.datacontract.org/2004/07/DynacTest"[GblCplx] }
  receiveTransitsBOSData = class;               { "http://schemas.datacontract.org/2004/07/DynacTest"[GblCplx] }
  transactionData      = class;                 { "http://schemas.datacontract.org/2004/07/DynacTest"[GblCplx] }
  receiveTransitsBOSDataResponse = class;       { "http://schemas.datacontract.org/2004/07/DynacTest"[GblCplx] }
  receiveTollRatesBOSData2 = class;             { "http://schemas.datacontract.org/2004/07/DynacTest"[GblElm] }
  gantryData2          = class;                 { "http://schemas.datacontract.org/2004/07/DynacTest"[GblElm] }
  receiveTollRatesBOSDataResponse2 = class;     { "http://schemas.datacontract.org/2004/07/DynacTest"[GblElm] }
  receiveTransitsBOSData2 = class;              { "http://schemas.datacontract.org/2004/07/DynacTest"[GblElm] }
  transactionData2     = class;                 { "http://schemas.datacontract.org/2004/07/DynacTest"[GblElm] }
  receiveTransitsBOSDataResponse2 = class;      { "http://schemas.datacontract.org/2004/07/DynacTest"[GblElm] }

  ArrayOfgantryData = array of gantryData;      { "http://schemas.datacontract.org/2004/07/DynacTest"[GblCplx] }


  // ************************************************************************ //
  // XML       : receiveTollRatesBOSData, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  receiveTollRatesBOSData = class(TRemotable)
  private
    FGantryData: ArrayOfgantryData;
    FGantryData_Specified: boolean;
    FGantryId: WideString;
    FGantryId_Specified: boolean;
    FNewYearRate: Boolean;
    FNewYearRate_Specified: boolean;
    procedure SetGantryData(Index: Integer; const AArrayOfgantryData: ArrayOfgantryData);
    function  GantryData_Specified(Index: Integer): boolean;
    procedure SetGantryId(Index: Integer; const AWideString: WideString);
    function  GantryId_Specified(Index: Integer): boolean;
    procedure SetNewYearRate(Index: Integer; const ABoolean: Boolean);
    function  NewYearRate_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property GantryData:  ArrayOfgantryData  Index (IS_OPTN or IS_NLBL) read FGantryData write SetGantryData stored GantryData_Specified;
    property GantryId:    WideString         Index (IS_OPTN or IS_NLBL) read FGantryId write SetGantryId stored GantryId_Specified;
    property NewYearRate: Boolean            Index (IS_OPTN) read FNewYearRate write SetNewYearRate stored NewYearRate_Specified;
  end;



  // ************************************************************************ //
  // XML       : gantryData, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  gantryData = class(TRemotable)
  private
    FAmount: TXSDecimal;
    FAmount_Specified: boolean;
    FCategoryType: WideString;
    FCategoryType_Specified: boolean;
    FDayOfWeek: WideString;
    FDayOfWeek_Specified: boolean;
    FEndDate: TXSDateTime;
    FEndDate_Specified: boolean;
    FEndTime: TXSDateTime;
    FEndTime_Specified: boolean;
    FID: Integer;
    FID_Specified: boolean;
    FPlanID: Integer;
    FPlanID_Specified: boolean;
    FRateType: WideString;
    FRateType_Specified: boolean;
    FRoadSegment: WideString;
    FRoadSegment_Specified: boolean;
    FStartDate: TXSDateTime;
    FStartDate_Specified: boolean;
    FStartTime: TXSDateTime;
    FStartTime_Specified: boolean;
    procedure SetAmount(Index: Integer; const ATXSDecimal: TXSDecimal);
    function  Amount_Specified(Index: Integer): boolean;
    procedure SetCategoryType(Index: Integer; const AWideString: WideString);
    function  CategoryType_Specified(Index: Integer): boolean;
    procedure SetDayOfWeek(Index: Integer; const AWideString: WideString);
    function  DayOfWeek_Specified(Index: Integer): boolean;
    procedure SetEndDate(Index: Integer; const ATXSDateTime: TXSDateTime);
    function  EndDate_Specified(Index: Integer): boolean;
    procedure SetEndTime(Index: Integer; const ATXSDateTime: TXSDateTime);
    function  EndTime_Specified(Index: Integer): boolean;
    procedure SetID(Index: Integer; const AInteger: Integer);
    function  ID_Specified(Index: Integer): boolean;
    procedure SetPlanID(Index: Integer; const AInteger: Integer);
    function  PlanID_Specified(Index: Integer): boolean;
    procedure SetRateType(Index: Integer; const AWideString: WideString);
    function  RateType_Specified(Index: Integer): boolean;
    procedure SetRoadSegment(Index: Integer; const AWideString: WideString);
    function  RoadSegment_Specified(Index: Integer): boolean;
    procedure SetStartDate(Index: Integer; const ATXSDateTime: TXSDateTime);
    function  StartDate_Specified(Index: Integer): boolean;
    procedure SetStartTime(Index: Integer; const ATXSDateTime: TXSDateTime);
    function  StartTime_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Amount:       TXSDecimal   Index (IS_OPTN) read FAmount write SetAmount stored Amount_Specified;
    property CategoryType: WideString   Index (IS_OPTN or IS_NLBL) read FCategoryType write SetCategoryType stored CategoryType_Specified;
    property DayOfWeek:    WideString   Index (IS_OPTN or IS_NLBL) read FDayOfWeek write SetDayOfWeek stored DayOfWeek_Specified;
    property EndDate:      TXSDateTime  Index (IS_OPTN) read FEndDate write SetEndDate stored EndDate_Specified;
    property EndTime:      TXSDateTime  Index (IS_OPTN) read FEndTime write SetEndTime stored EndTime_Specified;
    property ID:           Integer      Index (IS_OPTN) read FID write SetID stored ID_Specified;
    property PlanID:       Integer      Index (IS_OPTN) read FPlanID write SetPlanID stored PlanID_Specified;
    property RateType:     WideString   Index (IS_OPTN or IS_NLBL) read FRateType write SetRateType stored RateType_Specified;
    property RoadSegment:  WideString   Index (IS_OPTN or IS_NLBL) read FRoadSegment write SetRoadSegment stored RoadSegment_Specified;
    property StartDate:    TXSDateTime  Index (IS_OPTN) read FStartDate write SetStartDate stored StartDate_Specified;
    property StartTime:    TXSDateTime  Index (IS_OPTN) read FStartTime write SetStartTime stored StartTime_Specified;
  end;



  // ************************************************************************ //
  // XML       : receiveTollRatesBOSDataResponse, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  receiveTollRatesBOSDataResponse = class(TRemotable)
  private
    FDescriptionError: WideString;
    FDescriptionError_Specified: boolean;
    FStatus: WideString;
    FStatus_Specified: boolean;
    procedure SetDescriptionError(Index: Integer; const AWideString: WideString);
    function  DescriptionError_Specified(Index: Integer): boolean;
    procedure SetStatus(Index: Integer; const AWideString: WideString);
    function  Status_Specified(Index: Integer): boolean;
  published
    property DescriptionError: WideString  Index (IS_OPTN or IS_NLBL) read FDescriptionError write SetDescriptionError stored DescriptionError_Specified;
    property Status:           WideString  Index (IS_OPTN or IS_NLBL) read FStatus write SetStatus stored Status_Specified;
  end;

  ArrayOftransactionData = array of transactionData;   { "http://schemas.datacontract.org/2004/07/DynacTest"[GblCplx] }


  // ************************************************************************ //
  // XML       : receiveTransitsBOSData, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  receiveTransitsBOSData = class(TRemotable)
  private
    FBunchId: Int64;
    FBunchId_Specified: boolean;
    FTransactionData: ArrayOftransactionData;
    FTransactionData_Specified: boolean;
    procedure SetBunchId(Index: Integer; const AInt64: Int64);
    function  BunchId_Specified(Index: Integer): boolean;
    procedure SetTransactionData(Index: Integer; const AArrayOftransactionData: ArrayOftransactionData);
    function  TransactionData_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property BunchId:         Int64                   Index (IS_OPTN) read FBunchId write SetBunchId stored BunchId_Specified;
    property TransactionData: ArrayOftransactionData  Index (IS_OPTN or IS_NLBL) read FTransactionData write SetTransactionData stored TransactionData_Specified;
  end;



  // ************************************************************************ //
  // XML       : transactionData, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  transactionData = class(TRemotable)
  private
    FGantryID: Integer;
    FGantryID_Specified: boolean;
    FLicensePlate: WideString;
    FLicensePlate_Specified: boolean;
    FTAGNumber: Int64;
    FTAGNumber_Specified: boolean;
    FTransactionDateTime: TXSDateTime;
    FTransactionDateTime_Specified: boolean;
    FTransactionID: Int64;
    FTransactionID_Specified: boolean;
    procedure SetGantryID(Index: Integer; const AInteger: Integer);
    function  GantryID_Specified(Index: Integer): boolean;
    procedure SetLicensePlate(Index: Integer; const AWideString: WideString);
    function  LicensePlate_Specified(Index: Integer): boolean;
    procedure SetTAGNumber(Index: Integer; const AInt64: Int64);
    function  TAGNumber_Specified(Index: Integer): boolean;
    procedure SetTransactionDateTime(Index: Integer; const ATXSDateTime: TXSDateTime);
    function  TransactionDateTime_Specified(Index: Integer): boolean;
    procedure SetTransactionID(Index: Integer; const AInt64: Int64);
    function  TransactionID_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property GantryID:            Integer      Index (IS_OPTN) read FGantryID write SetGantryID stored GantryID_Specified;
    property LicensePlate:        WideString   Index (IS_OPTN or IS_NLBL) read FLicensePlate write SetLicensePlate stored LicensePlate_Specified;
    property TAGNumber:           Int64        Index (IS_OPTN) read FTAGNumber write SetTAGNumber stored TAGNumber_Specified;
    property TransactionDateTime: TXSDateTime  Index (IS_OPTN) read FTransactionDateTime write SetTransactionDateTime stored TransactionDateTime_Specified;
    property TransactionID:       Int64        Index (IS_OPTN) read FTransactionID write SetTransactionID stored TransactionID_Specified;
  end;



  // ************************************************************************ //
  // XML       : receiveTransitsBOSDataResponse, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  receiveTransitsBOSDataResponse = class(TRemotable)
  private
    FDescriptionError: WideString;
    FDescriptionError_Specified: boolean;
    FStatus: WideString;
    FStatus_Specified: boolean;
    procedure SetDescriptionError(Index: Integer; const AWideString: WideString);
    function  DescriptionError_Specified(Index: Integer): boolean;
    procedure SetStatus(Index: Integer; const AWideString: WideString);
    function  Status_Specified(Index: Integer): boolean;
  published
    property DescriptionError: WideString  Index (IS_OPTN or IS_NLBL) read FDescriptionError write SetDescriptionError stored DescriptionError_Specified;
    property Status:           WideString  Index (IS_OPTN or IS_NLBL) read FStatus write SetStatus stored Status_Specified;
  end;



  // ************************************************************************ //
  // XML       : receiveTollRatesBOSData, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  receiveTollRatesBOSData2 = class(receiveTollRatesBOSData)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : gantryData, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  gantryData2 = class(gantryData)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : receiveTollRatesBOSDataResponse, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  receiveTollRatesBOSDataResponse2 = class(receiveTollRatesBOSDataResponse)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : receiveTransitsBOSData, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  receiveTransitsBOSData2 = class(receiveTransitsBOSData)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : transactionData, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  transactionData2 = class(transactionData)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : receiveTransitsBOSDataResponse, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/DynacTest
  // ************************************************************************ //
  receiveTransitsBOSDataResponse2 = class(receiveTransitsBOSDataResponse)
  private
  published
  end;


  // ************************************************************************ //
  // Namespace : http://tempuri.org/
  // soapAction: http://tempuri.org/IBosTollRates/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // binding   : BasicHttpBinding_IBosTollRates
  // service   : BosTollRates
  // port      : BasicHttpBinding_IBosTollRates
  // URL       : http://localhost:54912/BosTollRates.svc
  // ************************************************************************ //
  IBosTollRates = interface(IInvokable)
  ['{BD3935DE-0B0A-9526-8EDC-1692A1DAFD74}']
    function  ReceiveTollRates(const rates: receiveTollRatesBOSData): receiveTollRatesBOSDataResponse; stdcall;
    function  ReceiveTransits(const rates: receiveTransitsBOSData): receiveTransitsBOSDataResponse; stdcall;
  end;

function GetIBosTollRates(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): IBosTollRates;


implementation
  uses SysUtils;

function GetIBosTollRates(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): IBosTollRates;
const
  defWSDL = 'http://localhost:54912/BosTollRates.svc?wsdl';
  defURL  = 'http://localhost:54912/BosTollRates.svc';
  defSvc  = 'BosTollRates';
  defPrt  = 'BasicHttpBinding_IBosTollRates';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as IBosTollRates);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


destructor receiveTollRatesBOSData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FGantryData)-1 do
    FreeAndNil(FGantryData[I]);
  SetLength(FGantryData, 0);
  inherited Destroy;
end;

procedure receiveTollRatesBOSData.SetGantryData(Index: Integer; const AArrayOfgantryData: ArrayOfgantryData);
begin
  FGantryData := AArrayOfgantryData;
  FGantryData_Specified := True;
end;

function receiveTollRatesBOSData.GantryData_Specified(Index: Integer): boolean;
begin
  Result := FGantryData_Specified;
end;

procedure receiveTollRatesBOSData.SetGantryId(Index: Integer; const AWideString: WideString);
begin
  FGantryId := AWideString;
  FGantryId_Specified := True;
end;

function receiveTollRatesBOSData.GantryId_Specified(Index: Integer): boolean;
begin
  Result := FGantryId_Specified;
end;

procedure receiveTollRatesBOSData.SetNewYearRate(Index: Integer; const ABoolean: Boolean);
begin
  FNewYearRate := ABoolean;
  FNewYearRate_Specified := True;
end;

function receiveTollRatesBOSData.NewYearRate_Specified(Index: Integer): boolean;
begin
  Result := FNewYearRate_Specified;
end;

destructor gantryData.Destroy;
begin
  FreeAndNil(FAmount);
  FreeAndNil(FEndDate);
  FreeAndNil(FEndTime);
  FreeAndNil(FStartDate);
  FreeAndNil(FStartTime);
  inherited Destroy;
end;

procedure gantryData.SetAmount(Index: Integer; const ATXSDecimal: TXSDecimal);
begin
  FAmount := ATXSDecimal;
  FAmount_Specified := True;
end;

function gantryData.Amount_Specified(Index: Integer): boolean;
begin
  Result := FAmount_Specified;
end;

procedure gantryData.SetCategoryType(Index: Integer; const AWideString: WideString);
begin
  FCategoryType := AWideString;
  FCategoryType_Specified := True;
end;

function gantryData.CategoryType_Specified(Index: Integer): boolean;
begin
  Result := FCategoryType_Specified;
end;

procedure gantryData.SetDayOfWeek(Index: Integer; const AWideString: WideString);
begin
  FDayOfWeek := AWideString;
  FDayOfWeek_Specified := True;
end;

function gantryData.DayOfWeek_Specified(Index: Integer): boolean;
begin
  Result := FDayOfWeek_Specified;
end;

procedure gantryData.SetEndDate(Index: Integer; const ATXSDateTime: TXSDateTime);
begin
  FEndDate := ATXSDateTime;
  FEndDate_Specified := True;
end;

function gantryData.EndDate_Specified(Index: Integer): boolean;
begin
  Result := FEndDate_Specified;
end;

procedure gantryData.SetEndTime(Index: Integer; const ATXSDateTime: TXSDateTime);
begin
  FEndTime := ATXSDateTime;
  FEndTime_Specified := True;
end;

function gantryData.EndTime_Specified(Index: Integer): boolean;
begin
  Result := FEndTime_Specified;
end;

procedure gantryData.SetID(Index: Integer; const AInteger: Integer);
begin
  FID := AInteger;
  FID_Specified := True;
end;

function gantryData.ID_Specified(Index: Integer): boolean;
begin
  Result := FID_Specified;
end;

procedure gantryData.SetPlanID(Index: Integer; const AInteger: Integer);
begin
  FPlanID := AInteger;
  FPlanID_Specified := True;
end;

function gantryData.PlanID_Specified(Index: Integer): boolean;
begin
  Result := FPlanID_Specified;
end;

procedure gantryData.SetRateType(Index: Integer; const AWideString: WideString);
begin
  FRateType := AWideString;
  FRateType_Specified := True;
end;

function gantryData.RateType_Specified(Index: Integer): boolean;
begin
  Result := FRateType_Specified;
end;

procedure gantryData.SetRoadSegment(Index: Integer; const AWideString: WideString);
begin
  FRoadSegment := AWideString;
  FRoadSegment_Specified := True;
end;

function gantryData.RoadSegment_Specified(Index: Integer): boolean;
begin
  Result := FRoadSegment_Specified;
end;

procedure gantryData.SetStartDate(Index: Integer; const ATXSDateTime: TXSDateTime);
begin
  FStartDate := ATXSDateTime;
  FStartDate_Specified := True;
end;

function gantryData.StartDate_Specified(Index: Integer): boolean;
begin
  Result := FStartDate_Specified;
end;

procedure gantryData.SetStartTime(Index: Integer; const ATXSDateTime: TXSDateTime);
begin
  FStartTime := ATXSDateTime;
  FStartTime_Specified := True;
end;

function gantryData.StartTime_Specified(Index: Integer): boolean;
begin
  Result := FStartTime_Specified;
end;

procedure receiveTollRatesBOSDataResponse.SetDescriptionError(Index: Integer; const AWideString: WideString);
begin
  FDescriptionError := AWideString;
  FDescriptionError_Specified := True;
end;

function receiveTollRatesBOSDataResponse.DescriptionError_Specified(Index: Integer): boolean;
begin
  Result := FDescriptionError_Specified;
end;

procedure receiveTollRatesBOSDataResponse.SetStatus(Index: Integer; const AWideString: WideString);
begin
  FStatus := AWideString;
  FStatus_Specified := True;
end;

function receiveTollRatesBOSDataResponse.Status_Specified(Index: Integer): boolean;
begin
  Result := FStatus_Specified;
end;

destructor receiveTransitsBOSData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FTransactionData)-1 do
    FreeAndNil(FTransactionData[I]);
  SetLength(FTransactionData, 0);
  inherited Destroy;
end;

procedure receiveTransitsBOSData.SetBunchId(Index: Integer; const AInt64: Int64);
begin
  FBunchId := AInt64;
  FBunchId_Specified := True;
end;

function receiveTransitsBOSData.BunchId_Specified(Index: Integer): boolean;
begin
  Result := FBunchId_Specified;
end;

procedure receiveTransitsBOSData.SetTransactionData(Index: Integer; const AArrayOftransactionData: ArrayOftransactionData);
begin
  FTransactionData := AArrayOftransactionData;
  FTransactionData_Specified := True;
end;

function receiveTransitsBOSData.TransactionData_Specified(Index: Integer): boolean;
begin
  Result := FTransactionData_Specified;
end;

destructor transactionData.Destroy;
begin
  FreeAndNil(FTransactionDateTime);
  inherited Destroy;
end;

procedure transactionData.SetGantryID(Index: Integer; const AInteger: Integer);
begin
  FGantryID := AInteger;
  FGantryID_Specified := True;
end;

function transactionData.GantryID_Specified(Index: Integer): boolean;
begin
  Result := FGantryID_Specified;
end;

procedure transactionData.SetLicensePlate(Index: Integer; const AWideString: WideString);
begin
  FLicensePlate := AWideString;
  FLicensePlate_Specified := True;
end;

function transactionData.LicensePlate_Specified(Index: Integer): boolean;
begin
  Result := FLicensePlate_Specified;
end;

procedure transactionData.SetTAGNumber(Index: Integer; const AInt64: Int64);
begin
  FTAGNumber := AInt64;
  FTAGNumber_Specified := True;
end;

function transactionData.TAGNumber_Specified(Index: Integer): boolean;
begin
  Result := FTAGNumber_Specified;
end;

procedure transactionData.SetTransactionDateTime(Index: Integer; const ATXSDateTime: TXSDateTime);
begin
  FTransactionDateTime := ATXSDateTime;
  FTransactionDateTime_Specified := True;
end;

function transactionData.TransactionDateTime_Specified(Index: Integer): boolean;
begin
  Result := FTransactionDateTime_Specified;
end;

procedure transactionData.SetTransactionID(Index: Integer; const AInt64: Int64);
begin
  FTransactionID := AInt64;
  FTransactionID_Specified := True;
end;

function transactionData.TransactionID_Specified(Index: Integer): boolean;
begin
  Result := FTransactionID_Specified;
end;

procedure receiveTransitsBOSDataResponse.SetDescriptionError(Index: Integer; const AWideString: WideString);
begin
  FDescriptionError := AWideString;
  FDescriptionError_Specified := True;
end;

function receiveTransitsBOSDataResponse.DescriptionError_Specified(Index: Integer): boolean;
begin
  Result := FDescriptionError_Specified;
end;

procedure receiveTransitsBOSDataResponse.SetStatus(Index: Integer; const AWideString: WideString);
begin
  FStatus := AWideString;
  FStatus_Specified := True;
end;

function receiveTransitsBOSDataResponse.Status_Specified(Index: Integer): boolean;
begin
  Result := FStatus_Specified;
end;

initialization
  InvRegistry.RegisterInterface(TypeInfo(IBosTollRates), 'http://tempuri.org/', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(IBosTollRates), 'http://tempuri.org/IBosTollRates/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(IBosTollRates), ioDocument);
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfgantryData), 'http://schemas.datacontract.org/2004/07/DynacTest', 'ArrayOfgantryData');
  RemClassRegistry.RegisterXSClass(receiveTollRatesBOSData, 'http://schemas.datacontract.org/2004/07/DynacTest', 'receiveTollRatesBOSData');
  RemClassRegistry.RegisterXSClass(gantryData, 'http://schemas.datacontract.org/2004/07/DynacTest', 'gantryData');
  RemClassRegistry.RegisterXSClass(receiveTollRatesBOSDataResponse, 'http://schemas.datacontract.org/2004/07/DynacTest', 'receiveTollRatesBOSDataResponse');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOftransactionData), 'http://schemas.datacontract.org/2004/07/DynacTest', 'ArrayOftransactionData');
  RemClassRegistry.RegisterXSClass(receiveTransitsBOSData, 'http://schemas.datacontract.org/2004/07/DynacTest', 'receiveTransitsBOSData');
  RemClassRegistry.RegisterXSClass(transactionData, 'http://schemas.datacontract.org/2004/07/DynacTest', 'transactionData');
  RemClassRegistry.RegisterXSClass(receiveTransitsBOSDataResponse, 'http://schemas.datacontract.org/2004/07/DynacTest', 'receiveTransitsBOSDataResponse');
  RemClassRegistry.RegisterXSClass(receiveTollRatesBOSData2, 'http://schemas.datacontract.org/2004/07/DynacTest', 'receiveTollRatesBOSData2', 'receiveTollRatesBOSData');
  RemClassRegistry.RegisterXSClass(gantryData2, 'http://schemas.datacontract.org/2004/07/DynacTest', 'gantryData2', 'gantryData');
  RemClassRegistry.RegisterXSClass(receiveTollRatesBOSDataResponse2, 'http://schemas.datacontract.org/2004/07/DynacTest', 'receiveTollRatesBOSDataResponse2', 'receiveTollRatesBOSDataResponse');
  RemClassRegistry.RegisterXSClass(receiveTransitsBOSData2, 'http://schemas.datacontract.org/2004/07/DynacTest', 'receiveTransitsBOSData2', 'receiveTransitsBOSData');
  RemClassRegistry.RegisterXSClass(transactionData2, 'http://schemas.datacontract.org/2004/07/DynacTest', 'transactionData2', 'transactionData');
  RemClassRegistry.RegisterXSClass(receiveTransitsBOSDataResponse2, 'http://schemas.datacontract.org/2004/07/DynacTest', 'receiveTransitsBOSDataResponse2', 'receiveTransitsBOSDataResponse');

end.