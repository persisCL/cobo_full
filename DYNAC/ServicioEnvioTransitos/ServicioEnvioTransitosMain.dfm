object Kapsch_ServicioEnvioTransitosDynac: TKapsch_ServicioEnvioTransitosDynac
  OldCreateOrder = False
  OnCreate = ServiceCreate
  DisplayName = 'KapschDynacTransitsServices'
  StartType = stManual
  BeforeInstall = ServiceBeforeInstall
  AfterInstall = ServiceAfterInstall
  AfterUninstall = ServiceAfterUninstall
  OnStart = ServiceStart
  OnStop = ServiceStop
  Height = 291
  Width = 414
  object BaseEventos: TADOConnection
    LoginPrompt = False
    Left = 152
    Top = 104
  end
  object tmr_EjecutarProcesos: TTimer
    Enabled = False
    OnTimer = tmr_EjecutarProcesosTimer
    Left = 256
    Top = 104
  end
  object BaseDynac: TADOConnection
    LoginPrompt = False
    Left = 152
    Top = 184
  end
end
