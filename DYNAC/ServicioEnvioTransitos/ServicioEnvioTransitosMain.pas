unit ServicioEnvioTransitosMain;

interface

uses
    // by Developer
    Registry, Util, IniFiles, EventLog, ActiveX, DateUtils, Variants, Crypto,
    // by default
    Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs, DB, ADODB, ExtCtrls,
    BosTollRates, XSBuiltIns, Dynac, UtilProc;

type
  TKapsch_ServicioEnvioTransitosDynac = class(TService)
    BaseEventos: TADOConnection;
    BaseDynac: TADOConnection;
    tmr_EjecutarProcesos: TTimer;
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure tmr_EjecutarProcesosTimer(Sender: TObject);
    procedure ServiceBeforeInstall(Sender: TService);
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceAfterUninstall(Sender: TService);
  private
    { Private declarations }

    FTrabajando,
    FDeteniendo : Boolean;

    FLogPath,
    FDatabaseNameEventos, FServerEventos,
    FDatabaseNameDynac, FServerDynac : string;

    FInstancia,
    FIntervaloEjecucion,
    FIntervaloEsperaError,
    FTiempoEsperaDetencion,
    FCantidadTransitosAEnviar : Integer;

    FEjecucionOk_EventLog: Boolean;

    FUsuariosistema : string;

    FUsernameHash,
    FEncryptedUsername,
    FPasswordHash,
    FEncryptedPassword : Ansistring;


    procedure LeerParametrosIni;
    function CheckDBConnections: Boolean;
    procedure AddDebug(s: AnsiString; SoloLog: Boolean = True; Error: Boolean = False);
    procedure AddEvent(pProcedimiento, pMensaje: AnsiString; pError: Boolean = False; pInfoAdicional: AnsiString = '');
    procedure EjecutarProcesos;

  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  Kapsch_ServicioEnvioTransitosDynac: TKapsch_ServicioEnvioTransitosDynac;


const
    cServiceDescription         = 'Servicio de envio de Transitos a Dynac';
    cModulo                     = 'ServicioEnvioTransitosDynac';
    cCodigoSistema              = 200;


implementation

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  Kapsch_ServicioEnvioTransitosDynac.Controller(CtrlCode);
end;

function TKapsch_ServicioEnvioTransitosDynac.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TKapsch_ServicioEnvioTransitosDynac.ServiceAfterInstall(Sender: TService);
    var
        Registro: TRegistry;

    const
        cServicioInstalado  = 'Servicio [%s - Instancia: %d] Instalado en %s.';
        cProcedimiento      = 'ServiceAfterInstall';
begin
    Registro := TRegistry.Create(KEY_READ or KEY_WRITE);

    with Registro do try
        RootKey := HKEY_LOCAL_MACHINE;
        if OpenKey('\SYSTEM\CurrentControlSet\Services\' + Name, false) then begin
            WriteString('Description', cServiceDescription);
            CloseKey;
        end;
    finally
        Free;
    end;

    if CheckDBConnections then begin

        AddEvent(cProcedimiento, Format(cServicioInstalado, [cServiceDescription, FInstancia, GetMachineName]));
    end;
end;

procedure TKapsch_ServicioEnvioTransitosDynac.ServiceAfterUninstall(Sender: TService);
    const
        cServicioDesinstalado   = 'Servicio [%s - Instancia: %d] Desinstalado en %s, Instancia: %d';
        cProcedimiento          = 'ServiceAfterUninstall';
begin
    if CheckDBConnections then begin

        AddEvent(cProcedimiento, Format(cServicioDesinstalado, [cServiceDescription, FInstancia, GetMachineName]));
    end;
end;

procedure TKapsch_ServicioEnvioTransitosDynac.ServiceBeforeInstall(Sender: TService);
    const
        cDisplayName = '%s - Instancia: %d';
begin
    LeerParametrosIni;

    if CheckDBConnections then begin
        DisplayName := Format(cDisplayName, [DisplayName, Finstancia])
    end
    else DisplayName := Format(cDisplayName, [DisplayName, IntToStr(Finstancia)]);
end;

procedure TKapsch_ServicioEnvioTransitosDynac.ServiceCreate(Sender: TObject);
    const
        cName = '%s_Instancia_%d';
begin
    LeerParametrosIni;

    Name := Format(cName, [Name, FInstancia]);
end;

procedure TKapsch_ServicioEnvioTransitosDynac.LeerParametrosIni;
begin
    FLogPath                := GoodDir(InstallIni.ReadString('Paths', 'Log', ExtractFilePath(GetExeName) + 'Log'));

    FDatabaseNameEventos    := InstallIni.ReadString('Database BO_Eventos', 'DatabaseName', 'BO_Eventos');
    FServerEventos          := InstallIni.ReadString('Database BO_Eventos', 'Server', GetMachineName);

    FDatabaseNameDynac    := InstallIni.ReadString('Database BO_DYNAC', 'DatabaseName', 'BO_DYNAC');
    FServerDynac          := InstallIni.ReadString('Database BO_DYNAC', 'Server', GetMachineName);

    FInstancia              := InstallIni.ReadInteger('General', 'Instancia',0);
    FIntervaloEjecucion     := InstallIni.ReadInteger('General', 'IntervaloEjecucion', 1000);
    FIntervaloEsperaError   := InstallIni.ReadInteger('General', 'IntervaloEsperaError', 300000);
    FTiempoEsperaDetencion  := InstallIni.ReadInteger('General', 'TiempoEsperaDetencion', 60000);
    FCantidadTransitosAEnviar:= InstallIni.ReadInteger('General', 'CantidadTransitosAEnviar', 150);
    FEjecucionOk_EventLog            := InstallIni.ReadBool('General', 'EjecucionOk_EventLog', True);

    FUsuarioSistema         := InstallIni.ReadString('General', 'UsuarioSistema', 'usr_' + cModulo);

    FUsernameHash           := InstallIni.ReadString('General', 'UsernameHash', 'Cualquiera');
    FEncryptedUsername      := InstallIni.ReadString('General', 'Username', 'Cualquiera');
    FPasswordHash           := InstallIni.ReadString('General', 'PasswordHash', 'Cualquiera');
    FEncryptedPassword      := InstallIni.ReadString('General', 'Password', 'Cualquiera');

    ForceDirectories(FLogPath);
end;

function TKapsch_ServicioEnvioTransitosDynac.CheckDBConnections: Boolean;
    const
        cErrorConexion      = 'Se produjo un error al conectar con la DB. Error: %s, Server: %s, Database: %s.';
        cConnectionString   = 'Provider=SQLOLEDB.1; Workstation ID=%s; Initial Catalog=%s; Data Source=%s';
begin
    
    try
        if BaseEventos.Connected then BaseEventos.Close;

        with BaseEventos do begin
            Connected           := False;
            ConnectionString    := Format(cConnectionString, [GetMachineName, FDatabaseNameEventos, FServerEventos]);
            KeepConnection      := True;
            LoginPrompt         := False;
        end;
        BaseEventos.Open(DecodePWDEx(FEncryptedUsername, FUsernameHash), DecodePWDEx(FEncryptedPassword, FPasswordHash));

        Result := BaseEventos.Connected;
    except
        on e: Exception do begin
            Result := False;
            AddDebug(Format(cErrorConexion, [e.Message, FServerEventos, FDatabaseNameEventos]), False, True);
        end;
    end;
    
    if Result then
    begin
        try
            if BaseDynac.Connected then BaseDynac.Close;

            with BaseDynac do begin
                Connected           := False;
                ConnectionString    := Format(cConnectionString, [GetMachineName, FDatabaseNameDynac, FServerDynac]);
                KeepConnection      := True;
                LoginPrompt         := False;
            end;
            BaseDynac.Open(DecodePWDEx(FEncryptedUsername, FUsernameHash), DecodePWDEx(FEncryptedPassword, FPasswordHash));

            Result := BaseDynac.Connected;
        except
            on e: Exception do begin
                Result := False;
                AddDebug(Format(cErrorConexion, [e.Message, FServerDynac, FDatabaseNameDynac]), False, True);
            end;
        end;
    end;

end;

procedure TKapsch_ServicioEnvioTransitosDynac.AddDebug(s: AnsiString; SoloLog: Boolean = True; Error: Boolean = False);
    resourceString
        MSG_PROCESAR_FILE_OPEN_ERROR = 'Ha ocurrido el error "%s" al intentar la apertura del archivo Log %s del Servicio %s';
        MSG_PROCESAR_FILE_WRITE_ERROR = 'Ha ocurrido el error "%s" al intentar escribir el mensaje "%s" en el archivo Log %s del Servicio %s';

    var
        ArchivoLog: TextFile;
        NombreArchivoLog: string;
        Ahora: TDateTime;

    const
        cLineaLog = '%s - %s - %s';
        cError = 'ERROR';
        cInfo = 'INFO';
begin
    try
        Ahora := Now;

        NombreArchivoLog := Format(FLogPath + 'Log_%s_%s.txt',[cModulo, FormatDateTime('yyyymmdd', Ahora)]);
        AssignFile(ArchivoLog, NombreArchivoLog);
        try
            if FileExists(NombreArchivoLog) then begin
                Append(ArchivoLog);
            end
            else begin
                Rewrite(ArchivoLog);
            end;
        except
            on e:exception do begin
                EventLogReportEvent(elError, Format(MSG_PROCESAR_FILE_OPEN_ERROR,
                  [e.Message, NombreArchivoLog, cModulo]), '');
            end;
        end;

        try
            Writeln(ArchivoLog,  Format(cLineaLog, [FormatDateTime('dd/mm/yyyy hh:nn:ss.zzz', Ahora), iif(Error, cError, cInfo), s]));
        except
            on e:exception do begin
                EventLogReportEvent(elError, Format(MSG_PROCESAR_FILE_WRITE_ERROR,
                  [e.Message, s, NombreArchivoLog, cModulo]), '');
            end;
        end;
    finally
        CloseFile(ArchivoLog);
    end;
end;

procedure TKapsch_ServicioEnvioTransitosDynac.AddEvent(pProcedimiento, pMensaje: string; pError: Boolean = False; pInfoAdicional: AnsiString = '');
    var
        lLOG_EventoAppAgregar: TADOStoredProc;
begin
    try
        try
            lLOG_EventoAppAgregar := TADOStoredProc.Create(nil);

            with lLOG_EventoAppAgregar do begin

                Connection      := BaseEventos;
                ProcedureName   := 'LOG_EventoAppAgregar';

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoSistema').Value      := cCodigoSistema;
                Parameters.ParamByName('@NombreProceso').Value      := cModulo + '.' + pProcedimiento;
                Parameters.ParamByName('@ErrorEstado').Value        := iif(pError, 1, 0);
                Parameters.ParamByName('@ErrorMensaje').Value       := pMensaje;
                Parameters.ParamByName('@ErrorInfoAdicional').Value := iif(pInfoAdicional = EmptyStr, Null, pInfoAdicional);
                ExecProc;
            end;
        except
            on E: Exception do begin
                if pError then
                    AddDebug('pMensaje: ' + pMensaje + ', Exception: ' + e.Message, False, True)
                else
                    AddDebug('Exception: ' + e.Message, False, True);
            end;
        end;
    finally
        if Assigned(lLOG_EventoAppAgregar) then begin

            if lLOG_EventoAppAgregar.Active then lLOG_EventoAppAgregar.Close;
            FreeAndNil(lLOG_EventoAppAgregar);
        end;
    end;
end;

procedure TKapsch_ServicioEnvioTransitosDynac.ServiceStart(Sender: TService; var Started: Boolean);
    const
        cConnectionString       = 'Provider=SQLOLEDB.1; Workstation ID=%s; Initial Catalog=%s; Data Source=%s';
        cProcedimiento          = 'ServiceStart';
        cErrorIniciandoServicio = 'NO se pudo inciar el Servicio [%s - Instancia: %d] en %s, deb�do al siguiente Error: %s.';
        cServicioIniciado       = 'Servicio [%s - Instancia: %d] iniciado en %s.';
begin
    try

        CoInitialize(nil);

        LeerParametrosIni;
        AddDebug('Iniciando Servicio ...', False, False);

        FTrabajando := False;
        FDeteniendo := False;

        tmr_EjecutarProcesos.Enabled := true;
        Started := True;

        if CheckDBConnections then begin
            AddEvent(cProcedimiento, Format(cServicioIniciado, [cServiceDescription, FInstancia, GetMachineName]), False);
        end;

        //AddDebug('Servicio Iniciado.', False, False);
    except
        on e: Exception do begin
        
            Started := False;
            AddEvent(cProcedimiento, Format(cErrorIniciandoServicio, [cServiceDescription, FInstancia, GetMachineName, e.Message]), True);
        end;
    end;
end;

procedure TKapsch_ServicioEnvioTransitosDynac.ServiceStop(Sender: TService; var Stopped: Boolean);
    var
        lTiempoEspera: Integer;

    const
        cProcedimiento              = 'ServiceStop';
        cErrorDeteniendoServicio    = 'Se produjo un Error deteniendo el Servicio [%s - Instancia: %d] en %s. Error: %s.';
        cServicioDetenido           = 'Servicio [%s - Instancia: %d] Detenido en %s.';

begin
    try
        try
            AddDebug('Deteniendo Servicio ...', False, False);

            FDeteniendo   := True;
            tmr_EjecutarProcesos.Enabled  := False;
            lTiempoEspera := 0;

            while FTrabajando and (lTiempoEspera < FTiempoEsperaDetencion) do begin
                AddDebug('Esperando a que el Servicio finalice el trabajo en curso ...', False, False);
                Sleep(250);
                lTiempoEspera := lTiempoEspera + 250
            end;

            if CheckDBConnections then begin
                AddEvent(cProcedimiento, Format(cServicioDetenido, [cServiceDescription, FInstancia, GetMachineName ]), False);
            end;

            AddDebug('Servicio Detenido.', False, False);
        except
            on e: Exception do begin
                AddEvent(cProcedimiento, Format(cErrorDeteniendoServicio, [cServiceDescription, FInstancia, GetMachineName, e.Message]), True);
            end;
        end;
    finally
        CoUninitialize;
        Stopped := True;
    end;
end;

procedure TKapsch_ServicioEnvioTransitosDynac.tmr_EjecutarProcesosTimer(Sender: TObject);
begin
    try
        tmr_EjecutarProcesos.Enabled := False;

        FTrabajando := True;
        if not FDeteniendo then begin

            LeerParametrosIni;

            if CheckDBConnections then begin
                EjecutarProcesos;
                tmr_EjecutarProcesos.Interval := FIntervaloEjecucion
            end
            else begin
                tmr_EjecutarProcesos.Interval := FIntervaloEsperaError
            end;
        end;

        FTrabajando := False;
    finally
        tmr_EjecutarProcesos.Enabled := not FDeteniendo;
    end;
end;

procedure TKapsch_ServicioEnvioTransitosDynac.EjecutarProcesos;
const
    cProcedimiento = 'EjecutarProcesos';
    cDatosEnviados = 'Se han enviado %s Transitos a Dynac';
    cErrorDynac = 'Error reportado por WS Dynac';
    cErrorEnvioTransitos = 'Error BO enviando Transitos';
var
    rates: receiveTransitsBOSData;
    transits: ArrayOftransactionData;
    resultado : receiveTransitsBOSDataResponse;
    i: Integer;
    oTransits: TTransits;
    WSCliente: IBosTollRates;
    enviados : string;
begin
    
    try
        try
            resultado := nil;
            oTransits := TTransits.Create;
            rates := receiveTransitsBOSData.Create;
            WSCliente := GetIBosTollRates();
            enviados:= '';

            oTransits.Transitos_AEnviar_SELECT(FCantidadTransitosAEnviar, BaseDynac);
            if oTransits.ClientDataSet.RecordCount > 0 then
            begin

                oTransits.ClientDataSet.First;

                rates.BunchId := oTransits.BunchID;

                SetLength(transits, oTransits.ClientDataSet.RecordCount);

                for i := 0 to oTransits.ClientDataSet.RecordCount - 1 do
                begin
                    transits[i] := transactionData.Create;
                    transits[i].TransactionID := oTransits.NumCorrCO;
                    transits[i].LicensePlate := oTransits.PatenteDetectada;
                    transits[i].TAGNumber := oTransits.TAGNumeroSerie;
                    transits[i].TransactionDateTime := DateTimeToXSDateTime(oTransits.FechaHora);
                    transits[i].GantryID := oTransits.NumeroPuntoCobro;
                    oTransits.ClientDataSet.Next;
                end;

                rates.TransactionData := transits;

                resultado := WSCliente.ReceiveTransits(rates);

                if resultado.Status = '0' then
                begin
                    oTransits.Transitos_MarcarEnviados(rates.BunchId, oTransits.ClientDataSet, BaseDynac);
                    if FEjecucionOk_EventLog then
                        AddEvent(cProcedimiento,Format(cDatosEnviados, [IntToStr(oTransits.ClientDataSet.RecordCount)]));
                end
                else
                begin
                    if FEjecucionOk_EventLog then
                        AddEvent(cProcedimiento,cErrorDynac, True, resultado.DescriptionError);     
                end;

            end;

        except
            on e: Exception do begin
                AddEvent(cProcedimiento,cErrorEnvioTransitos, True, e.Message);
            end;
        end;      
    finally
        // Liberar Objetos
        if Assigned(oTransits) then
            oTransits.Free;
        if Assigned(rates) then
            rates.Destroy;
        if Assigned(resultado) then
            resultado.Destroy;
    end;

end;

end.
