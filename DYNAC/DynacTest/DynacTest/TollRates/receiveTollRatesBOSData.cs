﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DynacTest
{
    [DataContract]
    public class receiveTollRatesBOSData
    {
        [DataMember]
        public string GantryId { get; set; }

        [DataMember]
        public bool NewYearRate { get; set; }
               
        [DataMember]
        public List<gantryData> GantryData { get; set; }

    }
}