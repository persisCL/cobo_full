﻿using System.Runtime.Serialization;

namespace DynacTest
{
    [DataContract]
    public class receiveTollRatesBOSDataResponse
    {
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string DescriptionError { get; set; }

    }
}