﻿using System;
using System.Runtime.Serialization;

namespace DynacTest
{
    [DataContract]
    public class gantryData
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int PlanID { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }

        [DataMember]
        public string DayOfWeek { get; set; }

        [DataMember]
        public DateTime StartTime { get; set; }

        [DataMember]
        public DateTime EndTime { get; set; }
                
        [DataMember]
        public string RoadSegment { get; set; }

        [DataMember]
        public string RateType { get; set; }

        [DataMember]
        public string CategoryType { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

    }
}