﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DynacTest
{
    [DataContract]
    public class receiveTransitsBOSData
    {
        [DataMember]
        public long BunchId { get; set; }
               
        [DataMember]
        public List<transactionData> TransactionData { get; set; }

    }
}