﻿using System;
using System.Runtime.Serialization;

namespace DynacTest
{
    [DataContract]
    public class transactionData
    {
        [DataMember]
        public long TransactionID { get; set; }

        [DataMember]
        public string LicensePlate { get; set; }

        [DataMember]
        public long TAGNumber { get; set; }

        [DataMember]
        public DateTime TransactionDateTime { get; set; }

        [DataMember]
        public int GantryID { get; set; }               

    }
}