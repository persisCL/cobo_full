﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace DynacTest
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class BosTollRates : IBosTollRates
    {
        public receiveTollRatesBOSDataResponse ReceiveTollRates(receiveTollRatesBOSData rates)
        {            
            receiveTollRatesBOSDataResponse response = new receiveTollRatesBOSDataResponse();
            response.Status = "0";
            response.DescriptionError = "testOK";

            var xmlserializer = new XmlSerializer(typeof(receiveTollRatesBOSData));
            var stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter))
            {
                xmlserializer.Serialize(writer, rates);                
            }
            stringWriter.ToString();


            return response;
        }

        public receiveTransitsBOSDataResponse ReceiveTransits(receiveTransitsBOSData rates)
        {
            receiveTransitsBOSDataResponse response = new receiveTransitsBOSDataResponse();
            response.Status = "0";
            response.DescriptionError = "testOK";

            var xmlserializer = new XmlSerializer(typeof(receiveTransitsBOSData));
            var stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter))
            {
                xmlserializer.Serialize(writer, rates);
            }
            stringWriter.ToString();


            return response;
        }
    }
}
