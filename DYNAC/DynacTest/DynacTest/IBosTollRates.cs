﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace DynacTest
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IBosTollRates
    {

        [OperationContract]
        receiveTollRatesBOSDataResponse ReceiveTollRates(receiveTollRatesBOSData rates);

        [OperationContract]
        receiveTransitsBOSDataResponse ReceiveTransits(receiveTransitsBOSData rates);

    }

}
