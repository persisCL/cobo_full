program cmdActualizaEjecutables;

{$APPTYPE CONSOLE}

uses
  Windows,
  SysUtils,
  ADODB,
  IdHash,
  IdHashMessageDigest,
  DB,
  Classes,
  ActiveX,
  Console in 'Console.pas';

const

  MENSAJE_INCIO     = #13 + #10 + 'Instalador y actualizador de ejecutables' + #13 + #10;
  MENSAJE_INCIO2    = #13 + #10 + 'Iniciando instalacion/actualizacion, Numero de Proyecto: ';
  LINEA             = '------------------------------------------------------------------------------';

resourcestring

  SCannotCreateDir  = 'No se puede crear directorio';

var

  Sistema       : string;
  ADO           : TADOConnection;
  qry           : TADOQuery;
  Proyecto      : Integer;
  Hora, Inicio, Lapso  : TDateTime;
  Esperar       : Boolean;
  Conexion,
  Conexion01,
  Conexion02,
  Conexion03,
  Disco,
  Hash,
  HashNew,
  Archivo,
  Parametro     : string;



function MD5(const fileName : string) : string;
var
    idmd5   : TIdHashMessageDigest5;
    fs      : TFileStream;


begin
    if FileExists(fileName) then begin
        idmd5   := TIdHashMessageDigest5.Create;
        try
            fs     := TFileStream.Create(fileName, fmOpenRead OR fmShareDenyWrite) ;
            result := idmd5.AsHex(idmd5.HashValue(fs)) ;
        finally
           fs.Free;
           idmd5.Free;
        end;
    end else
        Result  := '';
end;

procedure DeleteDirectory(const Name: string);
var
  F: TSearchRec;
begin
  if FindFirst(Name + '\*', faAnyFile, F) = 0 then begin
    try
      repeat
        if (F.Attr and faDirectory <> 0) then begin
          if (F.Name <> '.') and (F.Name <> '..') then begin
            DeleteDirectory(Name + '\' + F.Name);
          end;
        end else begin
          DeleteFile(Name + '\' + F.Name);
        end;
      until FindNext(F) <> 0;
    finally
      FindClose(F);
    end;
    RemoveDir(Name);
  end;
end;

function TraeArchivoDesdeBDD(ID: integer) : Boolean;
var
    ADO       : TADOConnection;
    qry       : TADOQuery;
    Archivo   : string;
begin
  try

    ADO := TADOConnection.Create(nil);
    with ADO do begin
        ConnectionString := Conexion;
        Open;
    end;
    qry := TADOQuery.Create(nil);
    with qry do begin
        Connection                              := ADO;
        SQL.Text                                := 'select top 1  * from Ejecutables where ID = :ID order by Date desc';
        Parameters.ParamByName('ID').Value      := ID;
        Open;
        Hash                                    := FieldByName('Hash').AsString;
        Archivo                                 := Disco + FieldByName('FullFileName').AsString;
//        writeln;
//        Writeln('-->',Archivo);
//        writeln(ExtractFilePath(Archivo));
//        Writeln(ExcludeTrailingPathDelimiter(ExtractFilePath(Archivo)));

        ForceDirectories(ExcludeTrailingPathDelimiter(ExtractFilePath(Archivo)));
        TBlobField(FieldByName('FileContent')).SaveToFile(Archivo + '.tmp');
        Close;
    end;
    ADO.Connected := False;
    FreeAndNil(qry);
    FreeAndNil(ADO);
    Result := True;
  except
    Result := False;
  end;
end;



function ForceDirectories(Dir: string): Boolean;
var
  E: EInOutError;
begin
  Result := True;
  if Dir = '' then
  begin
    E := EInOutError.CreateRes(@SCannotCreateDir);
    E.ErrorCode := 3;
    raise E;
  end;
  Dir := ExcludeTrailingPathDelimiter(Dir);
{$IFDEF MSWINDOWS}
  if (Length(Dir) < 3) or DirectoryExists(Dir)
    or (ExtractFilePath(Dir) = Dir) then Exit; // avoid 'xyz:\' problem.
{$ENDIF}
{$IFDEF LINUX}
  if (Dir = '') or DirectoryExists(Dir) then Exit;
{$ENDIF}
  Result := ForceDirectories(ExtractFilePath(Dir)) and CreateDir(Dir);
end;


begin

  Writeln(MENSAJE_INCIO);
  Disco := 'C:';

  Conexion01 := 'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=bo_installer;Password=bo_installer;Initial Catalog=BO_UTILES;Data Source=148.198.229.133\bo_qa';
  Conexion02 := 'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=bo_installer;Password=bo_installer;Initial Catalog=BO_UTILES;Data Source=148.198.229.133\bo_qa_scada';
  Conexion03 := 'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=bo_installer;Password=bo_installer;Initial Catalog=BO_UTILES;Data Source=148.198.229.133\BO_SH_QA';

  coInitialize(nil);
  try
//    if ParamCount = 0 then begin
//      Write('Ingrese Archivo:');
//      Readln(Sistema)
//      //Sistema := 'XX';
//    end else
//      Sistema := ParamStr(1);
//    Writeln('Intentando actualizar:' + Sistema);


    if (ParamCount <= 2) and (ParamCount > 0) then begin
      Parametro := ParamStr(1);
      if Parametro = 'QA-RDM' then
        Proyecto := 1
      else if Parametro = 'QA-SCADA' then
        Proyecto := 2
      else if Parametro = 'QA-SH' then
        Proyecto := 3
      else
        raise Exception.Create('Parametro incorrecto.');
    end else
        raise Exception.Create('Cantidad de Parametros incorrecta.');

    Esperar := False;
    if ParamCount = 2 then begin
      Parametro := ParamStr(2);
      if Parametro = '-w' then
         Esperar := True;
    end;

    if Proyecto = 1 then begin
      DeleteDirectory('C:\KTC_BO\ADM');
      DeleteDirectory('C:\KTC_BO\CRM');
      DeleteDirectory('C:\KTC_BO\FAC');
      DeleteDirectory('C:\KTC_BO\INT');
      DeleteDirectory('C:\KTC_BO\REC');
      DeleteDirectory('C:\KTC_BO\REP');
      DeleteDirectory('C:\KTC_BO\SOR');
      DeleteDirectory('C:\KTC_BO\TAG');
    end;

    if Proyecto = 1 then
      Conexion := Conexion01;
    if Proyecto = 2 then
      Conexion := Conexion02;
    if Proyecto = 3 then
      Conexion := Conexion03;


    TextColor(Yellow);
    TextBackground(Blue);
    writeln;
    Writeln(MENSAJE_INCIO2,Proyecto);
    Writeln(LINEA);
    Writeln;

    ADO := TADOConnection.Create(nil);
    with ADO do begin
        ConnectionString := Conexion;
        Open;
    end;
    qry := TADOQuery.Create(nil);
    with qry do begin

        Connection                              := ADO;
        SQL.Text                                := 'select ID, Hash, FileName, FullFileName, Getdate() as Hora ' +
                                                   ' from                                      ' +
                                                   ' (select                                   ' +
                                                   '     id,                                   ' +
                                                   '     Hash,                                 ' +
                                                   '     FileName,                             ' +
                                                   '     Date,                                 ' +
                                                   '     FullFileName,                         ' +
                                                   '     TypeOfRelease,                        ' +
                                                   '     ROW_NUMBER() over (partition by TypeOfRelease, FullFileName order by Date desc) as NumRec ' +
                                                   '     from Ejecutables) a                   ' +
                                                   ' where                                     ' +
                                                   '       NumRec=1   ';
                                                  // '       NumRec=1 and TypeOfRelease=:Rel     ';

        //Parameters.ParamByName('Rel').Value := ReleaseType;

        Open;
        Inicio := Date + Time;
        if not eof then
          Hora   := FieldByName('Hora').AsDateTime;
        Lapso := Hora;
        while not eof do begin
            TextColor(Yellow);
            TextBackground(Blue);

            Archivo                                 := Disco + FieldByName('FullFileName').AsString;
            Write(DateTimeToStr(Lapso),': ','Verificando...' + Archivo);
            Hash                                    := FieldByName('Hash').AsString;
            HashNew                                 := MD5(Archivo);
            {$B-}
            if (Hash <> HashNew) and TraeArchivoDesdeBDD(FieldByName('ID').AsInteger) then begin
              Writeln;
              Write('Copiando...' + Archivo);
              HashNew                               := MD5(Archivo + '.tmp');
              if Hash <> HashNew then  begin

                TextColor(White);
                TextBackground(Red);

                Writeln('Error en la descarga del Archivo' + Archivo);

                TextColor(Yellow);
                TextBackground(Blue);
              end else begin
                DeleteFile(Archivo);
                RenameFile(Archivo + '.tmp', Archivo);
                Writeln('...Copiado');
                Writeln(LINEA);
              end;
            end else
              if  (Hash <> HashNew) then begin
                Writeln;

                TextColor(White);
                TextBackground(Red);

                Writeln('Error: No se pudo copiar:' + Archivo);
                Writeln(LINEA);
              end else begin
                Writeln('...OK');
                Writeln(LINEA);
              end;
              Lapso := Hora + Date + Time - Inicio;
            Next;
        end;
        Close;
    end;
    ADO.Connected := False;
    FreeAndNil(qry);
    FreeAndNil(ADO);

  except
    on E:Exception do begin
      TextColor(White);
      TextBackground(Red);
      Writeln(E.Classname, ': ', E.Message);
      TextColor(White);
      TextBackground(Black);
    end;
  end;
  CoUninitialize;
  Writeln;
  TextColor(LightGray);
  TextBackground(Black);

  if Esperar then begin
    TextColor(Yellow);
    TextBackground(Blue);
    Writeln('Terminado - Revise la carpeta C:\KTC_BO\, Presione ENTER...');
    Readln;
    TextColor(LightGray);
    TextBackground(Black);
    ClrScr;
  end;



end.




{
    BLACK             = 0,
    DARKBLUE          = FOREGROUND_BLUE,
    DARKGREEN         = FOREGROUND_GREEN,
    DARKCYAN          = FOREGROUND_GREEN | FOREGROUND_BLUE,
    DARKRED           = FOREGROUND_RED,
    DARKMAGENTA       = FOREGROUND_RED | FOREGROUND_BLUE,
    DARKYELLOW        = FOREGROUND_RED | FOREGROUND_GREEN,
    DARKGRAY          = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
    GRAY              = FOREGROUND_INTENSITY,
    BLUE              = FOREGROUND_INTENSITY | FOREGROUND_BLUE,
    GREEN             = FOREGROUND_INTENSITY | FOREGROUND_GREEN,
    CYAN              = FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE,
    RED               = FOREGROUND_INTENSITY | FOREGROUND_RED,
    MAGENTA           = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_BLUE,
    YELLOW            = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN,
    WHITE             = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
}
