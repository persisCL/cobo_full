library Actualizador;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,Classes,Consts,Forms,Dialogs,ShellApi,Windows,
  Actualizando in 'Actualizando.pas' {frmActualizando};

Type
  TCBProgress = Function(const iLectura,iTotalFichero: Integer): Boolean;

{$R *.RES}

function ExecAndWait(const ExecuteFile, ParamString : string): boolean;
    var
        SEInfo: TShellExecuteInfo;
        ExitCode: DWORD;
begin
    FillChar(SEInfo, SizeOf(SEInfo), 0);
    SEInfo.cbSize := SizeOf(TShellExecuteInfo);

    with SEInfo do begin
        fMask := SEE_MASK_NOCLOSEPROCESS;
        Wnd := Application.Handle;
        lpFile := PChar(ExecuteFile);
        lpParameters := PChar(ParamString);
        nShow := SW_HIDE;
    end;

    if ShellExecuteEx(@SEInfo) then
    begin
        repeat
            Application.ProcessMessages;
            GetExitCodeProcess(SEInfo.hProcess, ExitCode);
        until (ExitCode <> STILL_ACTIVE) or Application.Terminated;
        Result:=True;
    end
    else Result := False;
end;

procedure RegisterOCXorDLL(pLibrary: AnsiString);
    type
        TRegFunc = function : HResult; stdcall;
    var
        ARegFunc : TRegFunc;
        aHandle  : THandle;
begin
    try
        aHandle := LoadLibrary(PChar(pLibrary));
        if aHandle <> 0 then try
            ARegFunc := GetProcAddress(aHandle,'DllRegisterServer');

            if Assigned(ARegFunc) then begin
                ExecAndWait('regsvr32','/s ' + pLibrary);
            end;
        finally
            FreeLibrary(aHandle);
        end;
    except
        on e: Exception do begin
            raise Exception.Create((Format('Unable to register %s. Error: %s', [pLibrary, e.Message])));
        end;
    end;
end;

function ProgresoCopia(const iLectura,iTotalFichero: Integer): Boolean;
begin
    with frmActualizando do begin
        Gauge1.MaxValue := iTotalFichero;
        Gauge1.Progress := Gauge1.Progress + iLectura;
    end;

    Result := True;
end;

procedure MiCopyFile(const FileName, DestName: TFileName; CBProgress: TCBProgress);
    var
        CopyBuffer      : Pointer; { buffer for copying }
        BytesCopied,
        vBytesTotFile   : Longint;
        Source,
        Dest            : Integer; { handles }
        Destination     : TFileName; { holder for expanded destination name }

    const
        ChunkSize       : Longint = 8192; { copy in 8K chunks }

    function GetFileSize2(const FileName: string): LongInt;
        var
            SearchRec: TSearchRec;
    begin
        if FindFirst(ExpandFileName(FileName), faAnyFile, SearchRec) = 0 then begin
          Result := SearchRec.Size
        end
        else Result := -1;
    end;
begin
    Destination := ExpandFileName(DestName); { expand the destination path }

    GetMem(CopyBuffer, ChunkSize); { allocate the buffer }
    try
        vBytesTotFile := GetFileSize2(FileName);
        Source := FileOpen(FileName, fmShareDenyWrite); { open source file }

        if Source < 0 then raise EFOpenError.CreateFmt('Error abriendo Fichero %s.', [FileName]);

        try
            Dest := FileCreate(Destination); { create output file; overwrite existing }

            if Dest < 0 then raise EFCreateError.CreateFmt('Error Creando Fichero %s', [Destination]);

            try
                repeat

                    BytesCopied := FileRead(Source, CopyBuffer^, ChunkSize); { read chunk }
                    If Assigned(CBProgress) Then CBProgress(BytesCopied,vBytesTotFile);

                    if BytesCopied > 0 then begin{ if we read anything... }
                        FileWrite(Dest, CopyBuffer^, BytesCopied); { ...write chunk }
                    end;

                until BytesCopied < ChunkSize; { until we run out of chunks }

            finally
                FileClose(Dest); { close the destination file }
            end;
        finally
            FileClose(Source); { close the source file }
        end;
    finally
        FreeMem(CopyBuffer, ChunkSize); { free the buffer }
    end;
end;

function HayQueActualizar(pModuloActual,pModuloActualizado: String):Boolean; Stdcall; Export;
begin
    if FileExists(pModuloActualizado) then begin
        Result := FileAge(pModuloActual) < FileAge(pModuloActualizado)
    end
    else Result := False;
end;

procedure _ActualizaModulo(pModuloActual,pModuloActualizado: String);
    Var
        lFicheroActual,
        lFicheroEnRed: TSearchRec;
        lFileAgeActual,
        lFileHandleActual,
        lFileAgeRed: Integer;
begin
    case FileExists(pModuloActual) Of
        True:
            begin
                if (FindFirst(pModuloActual,faArchive,lFicheroActual) = 0) And (FindFirst(pModuloActualizado,faArchive,lFicheroEnRed) = 0) then begin

                    lFileAgeActual := FileAge(pModuloActual);
                    lFileAgeRed    := FileAge(pModuloActualizado);

                    if (lFileAgeActual < lFileAgeRed) then try

                        DeleteFile(PChar(pModuloActual));

                        Application.CreateForm(TfrmActualizando, frmActualizando);

                        with frmActualizando do begin
                            lblAccion.Caption := Format(lblAccion.Caption,[ExtractFileName(pModuloActual)]);
                            Show;
                            Update;
                        end;

                        MiCopyFile(pModuloActualizado,pModuloActual,ProgresoCopia);
                        lFileHandleActual := FileOpen(pModuloActual,fmOpenWrite);
                        FileSetDate(lFileHandleActual,lFileAgeRed);
                        FileClose(lFileHandleActual);
                    Finally
                        if Assigned(frmActualizando) then FreeAndNil(frmActualizando);
                    end;
                end;
            end;
        False:
            try

                lFileAgeRed := FileAge(pModuloActualizado);
                Application.CreateForm(TfrmActualizando,frmActualizando);

                with frmActualizando do begin
                    lblAccion.Caption := Format('Instalando M�dulo %s. Espere Por Favor ...',[ExtractFileName(pModuloActual)]);
                    Show;
                    Update;
                end;

                MiCopyFile(pModuloActualizado,pModuloActual,ProgresoCopia);
                lFileHandleActual := FileOpen(pModuloActual,fmOpenWrite);
                FileSetDate(lFileHandleActual,lFileAgeRed);
                FileClose(lFileHandleActual);
            finally
                if Assigned(frmActualizando) then FreeAndNil(frmActualizando);
            end;
    end;
end;


procedure ActualizaModulo(pModuloActual,pModuloActualizado: String); Stdcall; Export;
begin
    _ActualizaModulo(pModuloActual, pModuloActualizado);
end;

procedure ActualizaModulo2; Stdcall; Export;
    var
        pModuloActual,
        pModuloActualizado: String;

begin
    pModuloActual       := ParamStr(3);
    pModuloActualizado  := ParamStr(4);

    _ActualizaModulo(pModuloActual, pModuloActualizado);

    ShellExecute(Application.Handle,'OPEN',PChar(pModuloActual),'','',SW_SHOWNORMAL);
End;

procedure ActualizaModulo3; Stdcall; Export;
    var
        pModuloActual,
        pModuloActualizado,
        pModuloALanzar: String;
Begin

    pModuloActual       := ParamStr(3);
    pModuloActualizado  := ParamStr(4);
    pModuloALanzar      := ParamStr(5);

    _ActualizaModulo(pModuloActual, pModuloActualizado);

    ShellExecute(Application.Handle,'OPEN',PChar(pModuloALanzar),'','',SW_SHOWNORMAL);
End;

procedure ActualizaDLLorOCX; Stdcall; Export;
    var
        pModuloActual,
        pModuloActualizado,
        pModuloALanzar: String;
        pRegistrar: Boolean;
Begin

    pModuloActual       := ParamStr(3);
    pModuloActualizado  := ParamStr(4);
    pModuloALanzar      := ParamStr(5);
    pRegistrar          := StrToBool(ParamStr(6));

    _ActualizaModulo(pModuloActual, pModuloActualizado);

    if pRegistrar then RegisterOCXorDLL(pModuloActual);

    ShellExecute(Application.Handle,'OPEN',PChar(pModuloALanzar),'','',SW_SHOWNORMAL);
End;

Exports ActualizaModulo, ActualizaModulo2, ActualizaModulo3, HayQueActualizar, ActualizaDLLorOCX;

begin
end.
