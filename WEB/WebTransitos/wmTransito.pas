{-----------------------------------------------------------------------------
 File Name: wmTransito
 Author:
 Date Created:
 Description:

Revision : 1
Date: 13/09/2010
Author: jjofre
Description:  Ref (SS_917)
        -Se a�ade o modifican las siguientes funciones/Procedimientos
            DescargarCSV: se cambia la extension del archivo de txt a csv.

Firma       : SS_1094_NDR_20130313
Description : La web en la opci�n descargar detalle de transacciones est� validando que el cliente tenga envio de detalle de transacciones por email.
              Esto NO se ha solicitado. Siempre se debe poder descargar el detalle de transacciones.

Firma       :   SS_1282_CQU_20150528
Descripcion :   Se modifica el procedimiento almacenado spObtenerUltimoComprobante (SQL) agregandole un nuevo campo,
                en el procedure AveriguarUltimaFactura ()Delphi) se agrega validacion al nuevo campo 'PuedeImprimir'
                y si es 0 se despliega el nuevo mensaje MSG_NK_SIN_TRANSITOS_DISPONIBLES. Esto es unicamente para Vespucio.
                En el caso de que el comprobante cumpla la condicion anterior y a�n as� no tenga tr�nsitos se desplegar�
                el mensaje MSG_NK_SIN_TRANSACCIONES_PEAJE. Esto para ambas concesionarias.

Firma       : SS_1305_MCA_20150612
Descripcion : se agrega bitacora de uso para el sitio WEB

Firma       : SS_1305_NDR_20150630
Descripcion : Se agrega el c�digo de sesion a la bitacora

Firma			  : SS_1304_NDR_20150721
Descripcion	: Recupera el ultimo comprobante de un convenio

Firma       :   SS_1379_MCA_20150911
Descripcion : se reemplaza plantilla de error por una nueva para el mensaje de descarga detalle transacciones.
-----------------------------------------------------------------------------}
unit wmTransito;

interface

uses
  SysUtils, Classes, HTTPApp, dmConvenios, Parametros, Eventlog, FuncionesWEB,
  ManejoErrores, sesiones, CSVUtils, utildb, UtilHTTP, Util;

type
  TwmTransitos = class(TWebModule)
    procedure WebModuleCreate(Sender: TObject);
    procedure WebModuleException(Sender: TObject; E: Exception; var Handled: Boolean);

    procedure DescargarCSV(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure wmTransitosPruebaCGIAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
  private
    FDM: TdmConveniosWEB;
    Procedure RegistrarBitacoraUso(dm: TdmConveniosWEB; CodigoSesion, CodigoPersona, CodigoConvenio, IdBitacoraAccion: Integer; NumeroDocumento, Observacion: string);      //SS_1305_NDR_20150630  SS_1305_MCA_20150612
  public
    function GenerarCSVTransitos(DM: TDMConveniosweb; CodigoConvenio: integer; TipoComprobante: string; NroComprobante: integer; var CSV: string; var Error: string): boolean;
    function AveriguarUltimaFactura(dm: TDMConveniosWeb; CodigoConvenio: integer; var NroComprobante: int64; var Error: string): boolean;
    Procedure AgregarLog(dm: TDMConveniosWeb; Observaciones : String);
 end;

var
  wmTransitos: TwmTransitos;


implementation

{$R *.DFM}

const
        TAG_CODIGO_SESION = 'CodigoSesion';
        TAG_CONVENIO = 'Convenio';

        CODIGO_ENVIO_NOTA_COBRO = '1';
        CODIGO_ENVIO_DETALLE_TRANSITOS = '2';
        CODIGO_ENVIO_INFORME_TRIMESTRAL = '3';

        CODIGO_ENVIO_CORREO_POSTAL = 1;
        CODIGO_ENVIO_CORREO_EMAIL = 2;

procedure TwmTransitos.DescargarCSV(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
    TipoComprobante,
    Plantilla: string;
    NroComprobante: int64;
    CodigoConvenio,
    CodigoSesion: integer;
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);

    try
        TipoComprobante := 'NK';

        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                if AveriguarUltimaFactura(fdm, CodigoConvenio, NroComprobante, Error) then begin
                    if not GenerarCSVTransitos(FDM, CodigoConvenio, TipoComprobante, NroComprobante, Plantilla, Error) then begin
                        //Plantilla := GenerarError(FDM, Request, Error);                                                             //SS_1379_MCA_20150911
                        Plantilla := ObtenerPlantilla(FDM, Request, 'ErrorCSV');                                                      //SS_1379_MCA_20150911
                        Plantilla := ReplaceTag(Plantilla, 'Error', Error);                                                           //SS_1379_MCA_20150911
                        //EventLogReportEvent(elError, 'Error al Generar CSV de tr�nsitos: ' + Error, '');
                        RegistrarBitacoraUso(FDM,CodigoSesion,  0, CodigoConvenio, 7, '', 'Error al Generar CSV de tr�nsitos: ' + Error);				//SS_1305_NDR_20150630 SS_1305_MCA_20150612
                    end else begin
                        response.ContentType := 'text/csv';
                        response.SetCustomHeader('Content-Disposition', 'attachment; filename=' + inttostr(NroComprobante) + '.csv'); // Rev 1.  SS_917
                        RegistrarBitacoraUso(FDM,CodigoSesion,  0, CodigoConvenio, 7, '', 'Descarga Correcta Detalle Comprobante');					//SS_1305_NDR_20150630 SS_1305_MCA_20150612
                    end;
                end else begin
                    //Plantilla := GenerarError(FDM, Request, Error);                                                                 //SS_1379_MCA_20150911
                    Plantilla := ObtenerPlantilla(FDM, Request, 'ErrorCSV');                                                          //SS_1379_MCA_20150911
                    Plantilla := ReplaceTag(Plantilla, 'Error', Error);                                                               //SS_1379_MCA_20150911
                    RegistrarBitacoraUso(FDM,CodigoSesion,  0, CodigoConvenio, 8, '', 'Error al averiguar ultima factura: ' + Error);					//SS_1305_NDR_20150630 SS_1305_MCA_20150612
                    //EventLogReportEvent(elError, 'Error al averiguar ultima factura: ' + Error, '');
                end;
            end else begin
                //Plantilla := GenerarError(FDM, Request, Error);                                                                     //SS_1379_MCA_20150911
                Plantilla := ObtenerPlantilla(FDM, Request, 'ErrorCSV');                                                              //SS_1379_MCA_20150911
                Plantilla := ReplaceTag(Plantilla, 'Error', Error);                                                                   //SS_1379_MCA_20150911
                RegistrarBitacoraUso(FDM,CodigoSesion,  0, CodigoConvenio, 8, '', 'Error al verificar convenio: ' + Error);							//SS_1305_NDR_20150630 SS_1305_MCA_20150612
                //EventLogReportEvent(elError, 'Error al verificar convenio: ' + Error, '');
            end;
        end else begin																			                //SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM,CodigoSesion,  0, CodigoConvenio, 4, '', 'Sesion Expirada');				//SS_1305_NDR_20150630 SS_1305_MCA_20150612
        end;
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'DescargarCSV');
            RegistrarBitacoraUso(FDM,CodigoSesion,  0, CodigoConvenio, 8, '', 'Se ha producido un error DescargarCSV: ' + e.Message);			//SS_1305_NDR_20150630 SS_1305_MCA_20150612
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmTransitos.WebModuleCreate(Sender: TObject);
begin
    try
        FDM := TdmConveniosWEB.Create(Self);
        CargarValoresIniciales(FDM);
    except
        on e: exception do begin
            EventLogReportEvent(elError, 'Error al inicializar: ' + e.Message, '');
        end;
    end;
end;

procedure TwmTransitos.WebModuleException(Sender: TObject; E: Exception; var Handled: Boolean);
begin
    handled := true;
    try
        Response.Content := GenerarError(fdm, Request, e.Message);
        EventLogReportEvent(elError, 'WebmoduleException: ' + E.Message, '');
    except
        on e2: exception do begin
            Response.Content := 'Hubo un error: ' + e2.Message;
            EventLogReportEvent(elError, 'WebmoduleException: Hubo un error "' + e2.Message + '" al presentar la excepcion "' + E.Message + '"', '');
        end;
    end;
end;

procedure TwmTransitos.wmTransitosPruebaCGIAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
  var Handled: Boolean);
resourcestring
    htmlCode =
        '<BR/><span style=''font-size:10.0pt;font-family:"Tahoma"''>' +
        '<b style=''mso-bidi-font-weight:normal''>M�todo:</b> %s, ' +
        '<b style=''mso-bidi-font-weight:normal''>Path:</b> %s, ' +
        '<b style=''mso-bidi-font-weight:normal''>Estado:</b> %s' +
        '</span>';
var
    Plantilla, Metodos: String;
    Veces: Integer;
begin
    try
        Plantilla := ObtenerPlantilla(FDM, Request, 'PruebaCGI');
        Plantilla := ReplaceTag(Plantilla, 'CGIFile', ExtractFileName(ISAPICGIFile));
        Plantilla := ReplaceTag(Plantilla, 'Version', GetFileVersionString(ISAPICGIFile, True));
        Metodos   := '';
        for Veces := 0 to Actions.Count - 1 do begin
            Metodos :=
                Metodos +
                Format(htmlCode,
                        [Actions.Items[Veces].Name + iif(Actions.Items[Veces].Default, ' (Default)',''),
                         Actions.Items[Veces].PathInfo,
                         iif(Actions.Items[Veces].Enabled, 'Habilitado', 'No Habilitado')]);
        end;
        Plantilla := ReplaceTag(Plantilla, 'Metodos', Metodos);
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'PruebaCGI');
        end;
    end;
    Response.Content := Plantilla;

end;

function TwmTransitos.GenerarCSVTransitos(DM: TDMConveniosWEB; CodigoConvenio: integer; TipoComprobante: string; NroComprobante: integer; var CSV: string; var Error: string): boolean;
var
    DetalleFacturacion: integer;
begin
    result := false;
    // primero averiguo si tiene contratado el servicio...
    //BEGIN : SS_1094_NDR_20130313-------------------------------------------------------------------------------
    //DetalleFacturacion := strtointdef(QueryGetValue(fdm.base, 'SELECT dbo.WEB_ObtenerTipoMedioEnvioDocumento(' + inttostr(CodigoConvenio) + ', ' + CODIGO_ENVIO_DETALLE_TRANSITOS + ')'), 0);

    //if DetalleFacturacion <> CODIGO_ENVIO_CORREO_EMAIL then begin
    //    Error := MSG_SERVICIO_DETALLE_FACTURACION_NO_CONTRATADO;
    //    exit;
    //end;
    //END : SS_1094_NDR_20130313-------------------------------------------------------------------------------


    // genero el CSV
    try
        with dm.spObtenerTransitosComprobante do begin
            Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            Parameters.ParamByName('@Comprobante').Value := NroComprobante;

            open;
            if IsEmpty or Eof then begin                    // SS_1282_CQU_20150528
                Close;                                      // SS_1282_CQU_20150528
                Error := MSG_NK_SIN_TRANSACCIONES_PEAJE;    // SS_1282_CQU_20150528
                Exit;                                       // SS_1282_CQU_20150528
            end;                                            // SS_1282_CQU_20150528

            result := DatasetToExcelCSV(dm.spObtenerTransitosComprobante, CSV, Error);

            close;
        end;
    except
        on e: exception do begin
            Error := e.Message;
        end;
    end;
end;

procedure TwmTransitos.AgregarLog(dm: TDMConveniosWeb; Observaciones: String);
begin
    try
        with dm.spAgregarLog do begin
            Parameters.ParamByName('@Observaciones').Value := Observaciones;
            Execproc;
        end;
    except
       on E : Exception do begin
            EventLogReportEvent(elError, 'AgregarLog - ' + E.Message, '');
       end;
    end;
end;

function TwmTransitos.AveriguarUltimaFactura(dm: TDMConveniosWeb;
  CodigoConvenio: integer; var NroComprobante: int64;
  var Error: string): boolean;
var
    TipoComprobante: string;
begin
    result := false;
    with dm.spObtenerUltimoComprobanteParaDetalleTransito do begin              //SS_1304_NDR_20150721
        parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        open;

        if not eof then begin
            TipoComprobante := fieldbyname('TipoComprobante').AsString;
            NroComprobante := fieldbyname('NumeroComprobante').AsInteger;
            if FieldByName('PuedeImprimir').AsInteger = 0 then begin        // SS_1282_CQU_20150528
                Close;                                                      // SS_1282_CQU_20150528
                Error := MSG_NK_SIN_TRANSITOS_DISPONIBLES;                  // SS_1282_CQU_20150528
                Exit;                                                       // SS_1282_CQU_20150528
            end;                                                            // SS_1282_CQU_20150528
        end else begin
            close;
            Error := MSG_CONVENIO_SIN_NK;
            exit;
        end;

        close;
    end;

    result := True;
end;

Procedure TwmTransitos.RegistrarBitacoraUso(dm: TdmConveniosWEB; CodigoSesion, CodigoPersona, CodigoConvenio, IdBitacoraAccion: Integer; NumeroDocumento, Observacion: string);      //SS_1305_NDR_20150630 SS_1305_MCA_20150612
begin																												//SS_1305_MCA_20150612
    try																												//SS_1305_MCA_20150612
        with dm.spWEB_CV_RegistrarBitacoraUso do begin																//SS_1305_MCA_20150612
            parameters.Refresh;																						//SS_1305_MCA_20150612
            parameters.ParamByName('@CodigoSesion').Value       := CodigoSesion;									//SS_1305_NDR_20150630
            parameters.ParamByName('@CodigoPersona').Value      := CodigoPersona;									//SS_1305_MCA_20150612
            parameters.ParamByName('@CodigoConvenio').Value     := CodigoConvenio;									//SS_1305_MCA_20150612
            parameters.ParamByName('@NumeroDocumento').Value    := NumeroDocumento;									//SS_1305_MCA_20150612
            parameters.ParamByName('@IdBitacoraAccion').Value   := IdBitacoraAccion;								//SS_1305_MCA_20150612
            parameters.ParamByName('@Observacion').Value        := Observacion;										//SS_1305_MCA_20150612
            CommandTimeOut := 30;																					//SS_1305_MCA_20150612
            Execproc;																								//SS_1305_MCA_20150612
        end;																										//SS_1305_MCA_20150612
    except																											//SS_1305_MCA_20150612
        on E : Exception do begin																					//SS_1305_MCA_20150612
            AgregarLog(DM, 'WEB_CV_RegistrarBitacoraUso - ' + E.Message);											//SS_1305_NDR_20150630
            EventLogReportEvent(elError, 'WEB_CV_RegistrarBitacoraUso - ' + E.Message, '');							//SS_1305_MCA_20150612
        end;																										//SS_1305_MCA_20150612
    end;																											//SS_1305_MCA_20150612
end;																												//SS_1305_MCA_20150612

end.
