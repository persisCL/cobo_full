object wmComprobantes: TwmComprobantes
  OldCreateOrder = False
  OnCreate = WebModuleCreate
  Actions = <
    item
      Name = 'ver'
      PathInfo = '/Ver'
      OnAction = Ver
    end
    item
      Name = 'VerNoFacturados'
      PathInfo = '/VerNoFacturados'
      OnAction = wmComprobantesVerNoFacturadosAction
    end
    item
      Default = True
      Name = 'DetalleComprobante'
      PathInfo = '/DetalleComprobante'
      OnAction = DetalleComprobante
    end
    item
      Name = 'PruebaCGI'
      PathInfo = '/PruebaCGI'
      OnAction = wmComprobantesPruebaCGIAction
    end>
  OnException = WebModuleException
  Height = 478
  Width = 469
  object rptUltimosConsumos: TppReport
    AutoStop = False
    Columns = 2
    ColumnPositions.Strings = (
      '7000'
      '105000')
    DataPipeline = ppUltimosConsumosDetalle
    NoDataBehaviors = [ndBlankReport]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'DETALLE CONSUMOS'
    PrinterSetup.PaperName = 'Letter'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 10000
    PrinterSetup.mmMarginLeft = 3000
    PrinterSetup.mmMarginRight = 3000
    PrinterSetup.mmMarginTop = 6000
    PrinterSetup.mmPaperHeight = 279000
    PrinterSetup.mmPaperWidth = 216000
    PrinterSetup.PaperSize = 1
    Template.FileName = 
      'D:\CVS Projects\SRV-DESARROLLO\Desarrollo De Software\Peaje\Orie' +
      'nte Poniente\Desarrollo\www\WEBConvenios\Detalle de transitos no' +
      ' facturados.rtm'
    Units = utMillimeters
    AllowPrintToArchive = True
    AllowPrintToFile = True
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    ModalCancelDialog = False
    ModalPreview = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    ShowCancelDialog = False
    ShowPrintDialog = False
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 64
    Top = 184
    Version = '12.04'
    mmColumnWidth = 95000
    DataPipelineName = 'ppUltimosConsumosDetalle'
    object ppHeaderBand2: TppHeaderBand
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 22225
      mmPrintPosition = 0
      object ppRegion2: TppRegion
        UserName = 'Region2'
        Brush.Style = bsClear
        Caption = 'Region2'
        Pen.Style = psClear
        Stretch = True
        Transparent = True
        mmHeight = 21431
        mmLeft = 1323
        mmTop = 794
        mmWidth = 204523
        BandType = 0
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppShape14: TppShape
          UserName = 'Shape14'
          Gradient.EndColor = clWhite
          Gradient.StartColor = clWhite
          Gradient.Style = gsNone
          mmHeight = 6615
          mmLeft = 108215
          mmTop = 7938
          mmWidth = 46831
          BandType = 0
        end
        object ppShape15: TppShape
          UserName = 'Shape15'
          Gradient.EndColor = clWhite
          Gradient.StartColor = clWhite
          Gradient.Style = gsNone
          mmHeight = 6615
          mmLeft = 154781
          mmTop = 7938
          mmWidth = 46302
          BandType = 0
        end
        object ppShape16: TppShape
          UserName = 'Shape16'
          Brush.Color = clBlack
          Gradient.EndColor = clWhite
          Gradient.StartColor = clWhite
          Gradient.Style = gsNone
          mmHeight = 5292
          mmLeft = 108215
          mmTop = 2910
          mmWidth = 92869
          BandType = 0
        end
        object ppLine6: TppLine
          UserName = 'Line6'
          Border.BorderPositions = []
          Border.Color = clBlack
          Border.Style = psSolid
          Border.Visible = False
          Border.Weight = 1.000000000000000000
          Pen.Color = clWhite
          Pen.Width = 2
          Position = lpLeft
          Weight = 1.500000000000000000
          mmHeight = 5292
          mmLeft = 154517
          mmTop = 2910
          mmWidth = 3969
          BandType = 0
        end
        object ppLabel9: TppLabel
          UserName = 'Label9'
          HyperlinkColor = clBlue
          Border.BorderPositions = []
          Border.Color = clBlack
          Border.Style = psSolid
          Border.Visible = False
          Border.Weight = 1.000000000000000000
          Caption = ' Fecha de Emisi'#243'n '
          Ellipsis = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          TextAlignment = taCentered
          Transparent = True
          mmHeight = 3895
          mmLeft = 109008
          mmTop = 3704
          mmWidth = 43656
          BandType = 0
        end
        object ppLabel11: TppLabel
          UserName = 'Label11'
          HyperlinkColor = clBlue
          Border.BorderPositions = []
          Border.Color = clBlack
          Border.Style = psSolid
          Border.Visible = False
          Border.Weight = 1.000000000000000000
          Caption = 'Convenio'
          Ellipsis = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          TextAlignment = taCentered
          Transparent = True
          mmHeight = 3810
          mmLeft = 170960
          mmTop = 3704
          mmWidth = 14478
          BandType = 0
        end
        object ppShape19: TppShape
          UserName = 'Shape19'
          Brush.Color = clBlack
          Gradient.EndColor = clWhite
          Gradient.StartColor = clWhite
          Gradient.Style = gsNone
          Pen.Color = clGray
          Shape = stRoundRect
          mmHeight = 5556
          mmLeft = 3175
          mmTop = 15081
          mmWidth = 200290
          BandType = 0
        end
        object ppLabel19: TppLabel
          UserName = 'Label19'
          HyperlinkColor = clBlue
          Border.BorderPositions = []
          Border.Color = clBlack
          Border.Style = psSolid
          Border.Visible = False
          Border.Weight = 1.000000000000000000
          Caption = 'DETALLE DE TRANSITOS NO FACTURADOS'
          Ellipsis = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Name = 'Arial'
          Font.Size = 10
          Font.Style = [fsBold]
          Transparent = True
          mmHeight = 4191
          mmLeft = 7409
          mmTop = 15875
          mmWidth = 74888
          BandType = 0
        end
        object lblFechaEmision: TppLabel
          UserName = 'lblFechaEmision'
          HyperlinkColor = clBlue
          OnGetText = lblFechaEmisionGetText
          AutoSize = False
          Border.BorderPositions = []
          Border.Color = clBlack
          Border.Style = psSolid
          Border.Visible = False
          Caption = 'lblFechaEmision'
          Ellipsis = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 11
          Font.Style = []
          TextAlignment = taCentered
          Transparent = True
          mmHeight = 4498
          mmLeft = 109538
          mmTop = 8996
          mmWidth = 44186
          BandType = 0
        end
        object lblConvenio: TppLabel
          UserName = 'lblConvenio'
          HyperlinkColor = clBlue
          OnGetText = lblConvenioGetText
          AutoSize = False
          Border.BorderPositions = []
          Border.Color = clBlack
          Border.Style = psSolid
          Border.Visible = False
          Caption = 'lblConvenio'
          Ellipsis = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 11
          Font.Style = []
          TextAlignment = taCentered
          Transparent = True
          mmHeight = 4487
          mmLeft = 155840
          mmTop = 8996
          mmWidth = 44186
          BandType = 0
        end
        object ppImage1: TppImage
          UserName = 'Image1'
          AlignHorizontal = ahCenter
          AlignVertical = avCenter
          AutoSize = True
          MaintainAspectRatio = False
          Border.BorderPositions = []
          Border.Color = clBlack
          Border.Style = psSolid
          Border.Visible = False
          Picture.Data = {
            0954474946496D6167654749463839617E002E00D50000171411D4D4D3429C29
            999999504E4CB6B5B47D7B7AFFFFFF25221FA8A7A571B55F43403E5F5D5BE8F3
            E54EA236B8DAAEA1CE948B8A883333337DBB6BC6C5C4E2E2E1D0E7CA66AF5166
            666689C17ADCEDD8F4F9F2ADD4A25AA844C4E0BDF0F0F0E3E2E294C786514F4D
            ADA5A5262320615F5DFFFFFF0000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000000000000000000000000000000000000021F90405140026002C
            000000007E002E000006FFC08370482C1A05C8A472C9145C1E9BCD63D2AC5AAF
            D8AC0062EC7ABF5F2DD301691C1A218778CD5E73C1F078B1DD790C39EAB67EDF
            7CCBFF606B0E1C44197C87885B808B5E62131B445489936D7E8C9707590E1E45
            21949F6E98A258171A451AA0A95996A2805717904586AAB47DAD97561DB173B5
            BD4AACB77156A64616BEC78AC1AEB65DC6C8BDC0CA614C1D5FA8CFB5D1D25DCC
            5E79D8A9DADBBC4B665F10E0AAE2E343637117E9A0EBEC994B0A711ADFF188F3
            EC4CF7F83AEC4BD46FDCBF3F1B140C3C54705B9345E816EA6928AD8985450D14
            4A0C456FDA9210973C68DC8885A2B2260E762DB2309264B78E475E32D290419F
            4B2426830DBB2545D2CDFF6430633681A5ACA7CD81396F5D01B8CD028496F192
            B6C29201A6050E19E081933A0A8B02951D2D3C8030416BB6A0DCB29442DBEC2A
            04082114C89D7B7415DBBB78F3EADDCBB7AFDFBF80030B1E4CB8B0E1C3723E50
            A0504471802E012854408C29F26476154400D84C627200CD9C1F0B1940623300
            09050C9836BDE0C007099B896C662064F38003055603487060C16AD444403336
            1260B38103C501888E007BB3880F427C9B06AE5A77EBE69B5B07280D4084040C
            C84B93004D424875EE0B3E485F7D3B82E9CBC9017C48CEB8BA69E8BA630BA160
            7A7891019B497000800008814176CD45505B7EBDE537C007BA1DE71B098F4177
            006C9D1D509D64D9ED27FFDA019A0928C407DC01305C02A62540A0109A894004
            7FBBA9A71F88002C408282461CB8590007BA882200C741D8DD0130262063812C
            9E36048CFEFD78DC103FE2E89E8930028041014594469B10EE0967A0690CF4B8
            20068B8DB65905308A981B090400806311EB0DE0DB71185AE8DC8067A6398496
            434CB9D8640CEC4844A0005CD6A68920E826C165156C866305A5FD785B839C69
            765C95357EB91A09C3C186016C4F16B11A68052407DE018D0EA9A3699C0A91EA
            9BA06D86657359CE482B6EB1BA29446E267E29A0A3AE6DA663A4787627028ED8
            D50882103F56D0A68B45C0580200DC8140E0A44E5EB8DA02CBEEBA997FA54920
            8208CB9A262A0004EC27F02C111F108A6375AEE6A76091AB3D766079430819C1
            BD16263B6411EE91F023B507F829C4A101E8CBEF10F0C60BC0A4427037400503
            30561A021554D05CC20C44504100D275AB596B07AC6A9A8B536AAB1F6C0B2CC6
            1893536239E56F46044AEEC9C51E47206D30CB3AC4C843F05AB16405EB069E7D
            291E40E86ABC2D7829B034BA68B386770A395DD1F2C1680089FF8266046C0A4A
            A72008256607DD948A193784DAE6E9D63473D432301CDC24C8CD2C7976BB3AAE
            CCE35A688008C73120026F9F0D8EDCB8881F178108E07D30AE01058C7BE2B846
            487E4002E38A1600A10B4460E1E28D3FAEB708589A87F8B8CB0601003B}
          mmHeight = 12171
          mmLeft = 3440
          mmTop = 2117
          mmWidth = 33338
          BandType = 0
        end
      end
    end
    object ppColumnHeaderBand1: TppColumnHeaderBand
      mmBottomOffset = 0
      mmHeight = 7938
      mmPrintPosition = 0
      object ppLabel14: TppLabel
        UserName = 'Label14'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Fecha/Hora'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        WordWrap = True
        mmHeight = 2921
        mmLeft = 1588
        mmTop = 3440
        mmWidth = 13293
        BandType = 2
      end
      object ppLabel21: TppLabel
        UserName = 'Label21'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Patente'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 2921
        mmLeft = 25135
        mmTop = 3440
        mmWidth = 8731
        BandType = 2
      end
      object ppLabel22: TppLabel
        UserName = 'Label22'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'P. Cobro'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 2921
        mmLeft = 37042
        mmTop = 3440
        mmWidth = 10319
        BandType = 2
      end
      object ppLabel23: TppLabel
        UserName = 'Label23'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Horario'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 2921
        mmLeft = 51594
        mmTop = 3440
        mmWidth = 8731
        BandType = 2
      end
      object ppLabel24: TppLabel
        UserName = 'Label102'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Categor'#237'a'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 2921
        mmLeft = 61648
        mmTop = 3440
        mmWidth = 11377
        BandType = 2
      end
      object ppLabel27: TppLabel
        UserName = 'Label27'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Importe'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        WordWrap = True
        mmHeight = 2910
        mmLeft = 79640
        mmTop = 3440
        mmWidth = 14023
        BandType = 2
      end
      object ppLine3: TppLine
        UserName = 'Line3'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Weight = 0.750000000000000000
        mmHeight = 794
        mmLeft = 529
        mmTop = 2117
        mmWidth = 94192
        BandType = 2
      end
      object ppLine8: TppLine
        UserName = 'Line8'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Position = lpBottom
        Weight = 0.750000000000000000
        mmHeight = 1058
        mmLeft = 529
        mmTop = 6879
        mmWidth = 94192
        BandType = 2
      end
    end
    object ppDetailBand5: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 3704
      mmPrintPosition = 0
      object ppDBText7: TppDBText
        UserName = 'DBText7'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'FechaHora'
        DataPipeline = ppUltimosConsumosDetalle
        DisplayFormat = 'dd/mm/yy hh:nn'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        ParentDataPipeline = False
        TextAlignment = taCentered
        Transparent = True
        DataPipelineName = 'ppUltimosConsumosDetalle'
        mmHeight = 2910
        mmLeft = 1588
        mmTop = 265
        mmWidth = 21167
        BandType = 4
      end
      object ppDBText10: TppDBText
        UserName = 'DBText10'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Patente'
        DataPipeline = ppUltimosConsumosDetalle
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppUltimosConsumosDetalle'
        mmHeight = 2879
        mmLeft = 25135
        mmTop = 265
        mmWidth = 8731
        BandType = 4
      end
      object ppDBText11: TppDBText
        UserName = 'DBText11'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'PuntoCobro'
        DataPipeline = ppUltimosConsumosDetalle
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppUltimosConsumosDetalle'
        mmHeight = 2879
        mmLeft = 37042
        mmTop = 265
        mmWidth = 12435
        BandType = 4
      end
      object ppDBText12: TppDBText
        UserName = 'DBText12'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'TipoHorario'
        DataPipeline = ppUltimosConsumosDetalle
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppUltimosConsumosDetalle'
        mmHeight = 2879
        mmLeft = 51594
        mmTop = 265
        mmWidth = 8731
        BandType = 4
      end
      object ppDBText13: TppDBText
        UserName = 'DBText13'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Categoria'
        DataPipeline = ppUltimosConsumosDetalle
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppUltimosConsumosDetalle'
        mmHeight = 2879
        mmLeft = 61648
        mmTop = 529
        mmWidth = 14288
        BandType = 4
      end
      object ppDBText14: TppDBText
        UserName = 'DBText14'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Importe'
        DataPipeline = ppUltimosConsumosDetalle
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        ParentDataPipeline = False
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppUltimosConsumosDetalle'
        mmHeight = 2910
        mmLeft = 78581
        mmTop = 529
        mmWidth = 15081
        BandType = 4
      end
    end
    object ppColumnFooterBand1: TppColumnFooterBand
      mmBottomOffset = 0
      mmHeight = 6350
      mmPrintPosition = 0
      object ppLine5: TppLine
        UserName = 'Line5'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        ParentWidth = True
        Weight = 0.750000000000000000
        mmHeight = 265
        mmLeft = 0
        mmTop = 794
        mmWidth = 95000
        BandType = 6
      end
      object ppDBCalc1: TppDBCalc
        UserName = 'DBCalc1'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        DataField = 'Importe'
        DataPipeline = ppUltimosConsumosDetalle
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppUltimosConsumosDetalle'
        mmHeight = 2879
        mmLeft = 76465
        mmTop = 1323
        mmWidth = 17198
        BandType = 6
      end
      object ppLabel1: TppLabel
        UserName = 'Label1'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Total'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        WordWrap = True
        mmHeight = 2921
        mmLeft = 1588
        mmTop = 2106
        mmWidth = 5800
        BandType = 6
      end
      object ppCrossTab1: TppCrossTab
        UserName = 'CrossTab1'
        DataPipeline = ppUltimosConsumosDetalle
        Stretch = True
        Style = 'Standard'
        DataPipelineName = 'ppUltimosConsumosDetalle'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 3440
        mmWidth = 95000
        BandType = 6
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object dsUltimosConsumosDetalle: TDataSource
    DataSet = dmConveniosWEB.spObtenerUltimosConsumosDetalle
    Left = 64
    Top = 88
  end
  object ppUltimosConsumosDetalle: TppDBPipeline
    DataSource = dsUltimosConsumosDetalle
    CloseDataSource = True
    UserName = 'UltimosConsumosDetalle'
    Left = 64
    Top = 136
  end
end
