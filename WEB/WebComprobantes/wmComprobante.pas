{-----------------------------------------------------------------------------
 Unit Name: wmComprobante
 Contiene funciones que permites imprimir comprobantes desde la web

 Revision : 1
    Author : jjofre
    Date   : 26/02/2010
    Description : SS 830

          -Se modifica la funcion Ver() para que emita una alerta por email
           a los administradores de sistema de CN mediante el stored
           spWeb_AlertaCaf cuando no existan CAF en la suite DBNET del site.

          -Se a�adio la constante MSG_ERROR_CAF para que el mensaje de error sea
          mas claro al cliente cuando el sistema NO pueda generar el comprobante.

          -Ademas se pasa como parametro al stored spWeb_AlertaCaf
          el error real generado por la aplicacion al momento de intentar
          generar el comprobante, ya que la no generacion del comprobante
          puede ser originado por distintos motivos, falta de dll,
          falta de caf, etc.

 Revision : 2
 Author : jjofre
 Date   : 05/05/2010
 Description : SS 830
                Se modifica la funcion Ver() , para mayot claridad
                con los comprobantes antes de FE
                se pasa al stored spWEB_AlertaCaf el TipoComprobante
                y NumeroComprobante (y no el fiscal)

 Revision : 3
 Author : jjofre
 Date   : 13/09/2010
 Description : (ref SS_917)
                  -Se a�ade o modifica las siguientes funciones
                    DetalleComprobante
                  -Se agregan los objetos
                    FTipoComprobante
                    FNumeroComprobante

 Revision : 4
 Author : pdominguez
 Date   : 26/10/2010
 Description : SS 917 - Modificaciones WEB
    - Se modifica el procedimiento DetalleComprobante.

Firma       : WEB-NDR-20130110
Description : No poner el CodigoVehiculo en el Tag del reporte para el nombre de archivo del .PDF
-----------------------------------------------------------------------------}

unit wmComprobante;

interface

uses
  Windows, SysUtils, Classes, HTTPApp, Sesiones, dmConvenios, ManejoDatos, FuncionesWEB, Parametros, EventLog,
  ppDB, DB, ADODB, ppDBPipe, ppBands, ppCtrls, ppPrnabl, ppClass, ppStrtch, TXRB,
  ppRegion, ppCache, ppComm, ppRelatv, ppProd, ppReport, ppSubRpt, ppDevice,
  utilhttp, util, frmReporteTransitos,
  variants, ppGIFImage, ppCTMain, GIFImg, ppParameter, frmReporteFacturacionDetalladaWeb, ppPDFDevice;

type
  TwmComprobantes = class(TWebModule)
    rptUltimosConsumos: TppReport;
    ppHeaderBand2: TppHeaderBand;
    ppRegion2: TppRegion;
    ppShape14: TppShape;
    ppShape15: TppShape;
    ppShape16: TppShape;
    ppLine6: TppLine;
    ppLabel9: TppLabel;
    ppLabel11: TppLabel;
    ppShape19: TppShape;
    ppLabel19: TppLabel;
    ppColumnHeaderBand1: TppColumnHeaderBand;
    ppLine5: TppLine;
    ppLabel14: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel27: TppLabel;
    ppLine8: TppLine;
    ppDetailBand5: TppDetailBand;
    ppDBText7: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppColumnFooterBand1: TppColumnFooterBand;
    dsUltimosConsumosDetalle: TDataSource;
    ppUltimosConsumosDetalle: TppDBPipeline;
    ppLine3: TppLine;
    lblFechaEmision: TppLabel;
    lblConvenio: TppLabel;
    ppImage1: TppImage;
    ppDBCalc1: TppDBCalc;
    ppLabel1: TppLabel;
    ppCrossTab1: TppCrossTab;
    procedure lblConvenioGetText(Sender: TObject; var Text: string);
    procedure lblFechaEmisionGetText(Sender: TObject; var Text: string);
    procedure wmComprobantesVerNoFacturadosAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModuleCreate(Sender: TObject);
    procedure WebModuleException(Sender: TObject; E: Exception; var Handled: Boolean);

    procedure Ver(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure DetalleComprobante(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean); // Rev 3.  SS_917
    procedure wmComprobantesPruebaCGIAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean); // Rev 3.  SS_917
    

  private
    FDM: TdmConveniosWEB;
    FCodigoConvenio: integer;
    FTipoComprobante: String; // Rev 3.  SS_917
    FNumeroComprobante: Int64;   // Rev 3.  SS_917

    function GenerarPDFUltimosConsumos(CodigoConvenio, IndiceVehiculo: integer; var Plantilla: string; var Error: string): boolean;
    function ReporteAPDF(Reporte: TppReport; var Resultado: string; var Error: string): boolean;
    function ExportarReporte(Reporte: TppReport; Dispositivo: TppPDFDevice; var Error: string): boolean;
  public
    { Public declarations }
  end;

var
  wmComprobantes: TwmComprobantes;

implementation

{$R *.DFM}
const
    TAG_CODIGO_SESION = 'CodigoSesion';

function TwmComprobantes.ExportarReporte(Reporte: TppReport;
  Dispositivo: TppPDFDevice; var Error: string): boolean;
begin
    try
        Dispositivo.Publisher := Reporte.Publisher;
        Dispositivo.Reset;
        Reporte.PrintToDevices;
        result := true;
    except
        on e: exception do begin
            result := false;
            Error := '[ExportarReporte] ' + e.message;
        end;
    end;
end;

function TwmComprobantes.GenerarPDFUltimosConsumos(CodigoConvenio,
  IndiceVehiculo: integer; var Plantilla, Error: string): boolean;
begin
    result := false;
    try
        FCodigoConvenio := CodigoConvenio;
        FDM.spObtenerUltimosConsumosDetalle.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        FDM.spObtenerUltimosConsumosDetalle.Parameters.ParamByName('@IndiceVehiculo').Value := iif(IndiceVehiculo > 0, IndiceVehiculo, null);
        FDM.spObtenerUltimosConsumosDetalle.Open;

        //rptUltimosConsumos.Tag := StrToInt(IntToStr(CodigoConvenio) + IntToStr(iif(IndiceVehiculo > 0, IndiceVehiculo, 0)));  //WEB-NDR-20130110
        rptUltimosConsumos.Tag := CodigoConvenio;                                                                               //WEB-NDR-20130110

        result := ReporteAPDF(rptUltimosConsumos, Plantilla, Error);

        FDM.spObtenerUltimosConsumosDetalle.Close;
    except
        on e: exception do begin
            FDM.spObtenerUltimosConsumosDetalle.Close;
            Error := 'GenerarPDFUltimosConsumos: [Interno]' + e.Message;
        end;
    end;
end;

procedure TwmComprobantes.lblConvenioGetText(Sender: TObject; var Text: string);
begin
    Text := ManejoDatos.ObtenerDescripcionConvenio(FDM, FCodigoConvenio);
end;

procedure TwmComprobantes.lblFechaEmisionGetText(Sender: TObject;
  var Text: string);
begin
    Text := FormatDateTime('DD/MM/YYYY', now);
end;

function TwmComprobantes.ReporteAPDF(Reporte: TppReport; var Resultado,
  Error: string): boolean;
var
    Exportador: TppPDFDevice;
    Salida: TStringStream;
    Salida2: TMemoryStream;
begin
    try
        Salida := TStringStream.Create('');
        Salida2 := TMemoryStream.Create;
        Error := '';
        Exportador := TppPDFDevice.Create(nil);

        Exportador.PDFSettings.OptimizeImageExport := False;                                                                                              // SS_995_PDO_20111006
        Exportador.PDFSettings.ScaleImages := False;
        Exportador.FileName := GetTempDir + FormatDateTime('yyyymmddhhnnsszzz_', Now) + IntToStr(Reporte.Tag) + '.pdf';

//    Exportador.PrintToStream := True;
//    Exportador.ReportStream := Salida;

        Result := ExportarReporte(Reporte, Exportador, Error);

        //CopyFile(PChar(Exportador.FileName), PChar('C:\' + ExtractFileName(Exportador.FileName)), False);

        Salida2.LoadFromFile(Exportador.FileName);
        Salida2.Position := 0;
        Salida2.SaveToStream(Salida);

        Resultado := Salida.DataString;
    finally
      if Assigned(Exportador) then FreeAndNil(Exportador);
      if Assigned(Salida) then FreeAndNil(Salida);
      if Assigned(Salida2) then FreeAndNil(Salida2);
    end;
end;

procedure TwmComprobantes.Ver(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
resourcestring
    MSG_ERROR_CAF	 = 'Estimado Cliente, No se pudo generar su comprobante electr�nico, favor comunicarse con el call center'; // Revision 1)
var
    NroComprobante: int64;
    EsZip: boolean;
    TipoComprobante,
    Error,
    Plantilla: string;
    CodigoSesion: integer;
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    TipoComprobante := request.ContentFields.Values['TipoComprobante'];
    NroComprobante := StrToIntDef(request.ContentFields.Values['NumeroComprobante'], -1);
    
    try
        if TipoComprobante = '' then TipoComprobante := 'NK';
        if NroComprobante = -1 then raise(Exception.Create('Faltan Par�metros (NumeroComprobante)'));

        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if not GenerarPDFComprobante(FDM, TipoComprobante, NroComprobante, Plantilla, EsZip, Error) then begin
                Plantilla := GenerarError(FDM, Request, MSG_ERROR_CAF);
                with FDM.spWEB_AlertaCaf do begin
                    Parameters.ParamByName('@NumeroComprobante').Value :=  NroComprobante; //Rev 2
                    Parameters.ParamByName('@TipoComprobante').Value:= TipoComprobante;    //Rev 2
                    Parameters.ParamByName('@MensajeError').Value:=Error;
                    ExecProc;
                end;

            end else begin
                if not EsZip then begin
                    response.ContentType := 'application/pdf';
                    response.SetCustomHeader('Content-Disposition', 'attachment; filename=' + inttostr(NroComprobante) + '.pdf');
                end else begin
                    response.ContentType := 'application/zip';
                    response.SetCustomHeader('Content-Disposition', 'attachment; filename=' + inttostr(NroComprobante) + '.zip');
                end;
            end;
        end;
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'VerComprobante');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmComprobantes.WebModuleCreate(Sender: TObject);
begin
    try
        FDM := TdmConveniosWEB.Create(Self);
        CargarValoresIniciales(FDM);
        FDM.SetearTiemposConsultas;
    except
        on e: exception do begin
            EventLogReportEvent(elError, 'Error al inicializar: ' + e.Message, '');
        end;
    end;
end;

procedure TwmComprobantes.WebModuleException(Sender: TObject; E: Exception; var Handled: Boolean);
begin
    handled := true;
    try
        Response.Content := GenerarError(fdm, Request, e.Message);
    except
        on e: exception do begin
            Response.Content := 'Hubo un error: ' + e.Message;
        end;
    end;
end;
{-----------------------------------------------------------------------------
Procedure: wmComprobantesVerNoFacturadosAction
Author:  jjofre
Date:      19-Abr-2010
Description: se modific� el mensaje que envia al usuario por el error de time out
-----------------------------------------------------------------------------}
procedure TwmComprobantes.wmComprobantesVerNoFacturadosAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
    Plantilla: string;
    IndiceVehiculo,
    CodigoConvenio,
    CodigoSesion: integer;
const
    TAG_CONVENIO = 'Convenio';
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    IndiceVehiculo := StrToIntDef(request.ContentFields.Values['IndiceVehiculo'], -1);
    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin

                frmRptTransitos := TfrmRptTransitos.Create(nil);
                try
                    if not frmRptTransitos.GenerarPDFUltimosConsumos(FDM, CodigoConvenio, IndiceVehiculo, Plantilla, Error) then begin
                        Plantilla := GenerarMensaje(FDM, Request, Error)
                    end else begin
                        response.ContentType := 'application/pdf';
                        response.SetCustomHeader('Content-Disposition', 'attachment; filename=Detalle_consumos_' + formatdatetime('YYYYMMDDHHNNSSZZZ', now) + '.pdf');
                    end;
                finally
                    frmRptTransitos.Release;
                end;
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
                Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
            end;
        end;
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'VerConsumos');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

{-----------------------------------------------------------------------------
Procedure: DetalleComprobante
Author:  jjofre
Date:      13/09/2010
Description: (REV 3 SS_917)obtiene el reporte de detalle de transitos desde
        la pesta�a transacciones,en la cartola web (este reporte es el mismo
        que se emite en el cac o facturacion).

 Revision : 4
 Author : pdominguez
 Date   : 26/10/2010
 Description : SS 917 - Modificaciones WEB
    - Se incluye el env�o del par�metro EsAviso en la Generaci�n del PDF de
    detalle de Transacciones.
-----------------------------------------------------------------------------}
procedure TwmComprobantes.DetalleComprobante(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);

var
    NroComprobante: int64;
    TipoComprobante,
    Error,
    Plantilla: string;
    CodigoSesion: integer;
    EsAviso: Boolean;
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    TipoComprobante := request.ContentFields.Values['TipoComprobante'];
    NroComprobante := StrToIntDef(request.ContentFields.Values['NumeroComprobante'], -1);

    try
        if TipoComprobante = '' then TipoComprobante := 'NK';
        if NroComprobante = -1 then raise(Exception.Create('Debe Seleccionar un comprobante'));

        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if not GenerarPDFTransaccionesComprobante(FDM, TipoComprobante, NroComprobante, Plantilla, Error, EsAviso) then begin
                if not EsAviso then
                    Plantilla := GenerarError(FDM, Request, Error)
                else Plantilla := GenerarMensaje(FDM, Request, Error);
            end else begin
                response.ContentType := 'application/pdf';
                response.SetCustomHeader('Content-Disposition', 'attachment; filename=' + 'DetalleComprobante_' +inttostr(NroComprobante) + '.pdf');
            end
        end;
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'VerComprobante');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmComprobantes.wmComprobantesPruebaCGIAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
  var Handled: Boolean);
resourcestring
    htmlCode =
        '<BR/><span style=''font-size:10.0pt;font-family:"Tahoma"''>' +
        '<b style=''mso-bidi-font-weight:normal''>M�todo:</b> %s, ' +
        '<b style=''mso-bidi-font-weight:normal''>Path:</b> %s, ' +
        '<b style=''mso-bidi-font-weight:normal''>Estado:</b> %s' +
        '</span>';
var
    Plantilla, Metodos: String;
    Veces: Integer;
begin
    try
        Plantilla := ObtenerPlantilla(FDM, Request, 'PruebaCGI');
        Plantilla := ReplaceTag(Plantilla, 'CGIFile', ExtractFileName(ISAPICGIFile));
        Plantilla := ReplaceTag(Plantilla, 'Version', GetFileVersionString(ISAPICGIFile, True));
        Metodos   := '';
        for Veces := 0 to Actions.Count - 1 do begin
            Metodos :=
                Metodos +
                Format(htmlCode,
                        [Actions.Items[Veces].Name + iif(Actions.Items[Veces].Default, ' (Default)',''),
                         Actions.Items[Veces].PathInfo,
                         iif(Actions.Items[Veces].Enabled, 'Habilitado', 'No Habilitado')]);
        end;
        Plantilla := ReplaceTag(Plantilla, 'Metodos', Metodos);
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'PruebaCGI');
        end;
    end;
    Response.Content := Plantilla;
end;

end.
