program Comprobantes;
{
Firma          : SS_1147_MCA_20150505
 Descripcion    : se reemplaza Costanera Norte por Vespucio Sur

Firma		:	SS_1397_MGO_20151015
Descripcion	:	Se agrega referencia a frmReporteFacturacionDetallada e hijos
}
{$APPTYPE CONSOLE}

uses
  WebBroker,
  CGIApp,
  Eventlog,
  Util,
  wmComprobante in 'wmComprobante.pas' {wmComprobantes: TWebModule},
  dmConvenios in '..\Comunes\dmConvenios.pas' {dmConveniosWEB: TDataModule},
  Sesiones in '..\Comunes\Sesiones.pas',
  Parametros in '..\Comunes\Parametros.pas',
  ManejoDatos in '..\Comunes\ManejoDatos.pas',
  ManejoErrores in '..\Comunes\ManejoErrores.pas',
  Ejecucion in '..\Comunes\Ejecucion.pas',
  Combos in '..\Comunes\Combos.pas',
  ConstParametrosGenerales in '..\..\Comunes\ConstParametrosGenerales.pas',
  PeaTypes in '..\..\Comunes\PeaTypes.pas',
  ImprimirWO in '..\..\Comunes\ImprimirWO.pas' {frmImprimirWO},
  FuncionesWEB in '..\Comunes\FuncionesWEB.pas',
  Zip in '..\Comunes\Zip.pas',
  ZipDlls in '..\Comunes\ZipDlls.pas',
  ReporteFactura in '..\..\Comunes\ReporteFactura.pas' {frmReporteFactura},
  XMLCAC in '..\..\Comunes\XMLCAC.pas',
  frmReporteTransitos in 'frmReporteTransitos.pas' {frmRptTransitos},
  frmReporteNotaDebitoElectronica in '..\..\Comunes\frmReporteNotaDebitoElectronica.pas' {ReporteNotaDebitoElectronicaForm},
  frmReporteBoletaFacturaElectronica in '..\..\Comunes\frmReporteBoletaFacturaElectronica.pas' {ReporteBoletaFacturaElectronicaForm},
  frmReporteNotaCreditoElectronica in '..\..\Comunes\frmReporteNotaCreditoElectronica.pas' {ReporteNotaCreditoElectronicaForm},
  DTEControlDLL in '..\..\FacturaElectronica\DTEControlDLL.pas',
  CSVUtils in '..\..\Comunes\CSVUtils.pas',
  frmReporteFacturacionDetalladaWeb in '..\Comunes\frmReporteFacturacionDetalladaWeb.pas' {FormReporteFacturacionDetalladaWeb},
  frmReporteFacturacionDetallada in '..\..\Facturacion\frmReporteFacturacionDetallada.pas' {FormReporteFacturacionDetallada},
  frmSeleccFactDetallada in '..\..\Facturacion\frmSeleccFactDetallada.pas' {fSeleccFactDetallada},
  PeaProcs in '..\..\Comunes\PeaProcs.pas',
  SysUtilsCN in '..\..\Comunes\SysUtilsCN.pas',
  frmBloqueosSistema in '..\..\Comunes\frmBloqueosSistema.pas' {BloqueosSistemaForm},
  frmMuestraMensaje in '..\..\Comunes\frmMuestraMensaje.pas' {PanelMensajesForm},
  DMConnection in '..\..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  PeaProcsCN in '..\..\Comunes\PeaProcsCN.pas',
  ImgTypes in '..\..\Comunes\ImgTypes.pas',
  RStrings in '..\..\Comunes\RStrings.pas',
  MsgBoxCN in '..\..\Comunes\MsgBoxCN.pas' {frmMsgBoxCN},
  AvisoTagVencidos in '..\..\Comunes\Avisos\AvisoTagVencidos.pas',
  NotificacionImpresionReporte in '..\..\Comunes\Avisos\NotificacionImpresionReporte.pas' {FormNotificacionImpresionReporte},
  frmMuestraPDF in '..\..\Comunes\frmMuestraPDF.pas' {MuestraPDFForm},
  Diccionario in '..\..\Comunes\Diccionario.pas',
  ClaveValor in '..\..\Comunes\ClaveValor.pas',
  Notificacion in '..\..\Comunes\Avisos\Notificacion.pas',
  NotificacionImpresionDoc in '..\..\Comunes\Avisos\NotificacionImpresionDoc.pas',
  TipoFormaAtencion in '..\..\Comunes\Avisos\TipoFormaAtencion.pas',
  Aviso in '..\..\Comunes\Avisos\Aviso.pas',
  frmVentanaAviso in '..\..\Comunes\Avisos\frmVentanaAviso.pas' {VentanaAvisoForm},
  EncriptaRijandel in '..\..\Comunes\EncriptaRijandel.pas',
  base64 in '..\..\Comunes\base64.pas',
  AES in '..\..\Comunes\AES.pas',
  DMComunicacion in '..\..\Comunes\DMComunicacion.pas' {DMComunicaciones: TDataModule},
  RVMTypes in '..\..\Comunes\RVMTypes.pas',
  RVMClient in '..\..\Comunes\RVMClient.pas',
  RVMLogin in '..\..\Comunes\RVMLogin.pas' {frmLogin},
  RVMBind in '..\..\Comunes\RVMBind.pas',
  MaskCombo in '..\..\Componentes\MaskCombo\MaskCombo.pas',
  FrmRptInformeUsuario in '..\..\Comunes\FrmRptInformeUsuario.pas' {FormRptInformeUsuario},
  UtilReportes in '..\..\Comunes\UtilReportes.pas',
  FrmRptInformeUsuarioConfig in '..\..\Comunes\FrmRptInformeUsuarioConfig.pas' {FormRptInformeUsuarioConfig},
  FactoryINotificacion in '..\..\Comunes\Avisos\FactoryINotificacion.pas',
  frmMensajeACliente in '..\..\Comunes\frmMensajeACliente.pas' {FormMensajeACliente};

{********************************
  * END   : SS_1397_MGO_20151015  *
  *********************************}

{$R *.RES}

begin
  ISAPICGIFile := GetExeName;
  EventLogAppName := 'WEBComprobantes';
  Application.Initialize;
  EventLogRegisterSource;
  //Application.Title := 'Visor de Comprobantes de Costanera Norte';            //SS_1147_MCA_20150506
  Application.Title := 'Visor de Comprobantes de Vespucio Sur';                 //SS_1147_MCA_20150506
  Application.CreateForm(TwmComprobantes, wmComprobantes);
  Application.Run;
end.
