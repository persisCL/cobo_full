{-----------------------------------------------------------------------------
Firma       : WEB-NDR-20130110
Description : No poner el CodigoVehiculo en el Tag del reporte para el nombre de archivo del .PDF

Firma       : SS_1397_MGO_20151015
Description : Se reemplazan nombres de columnas y reordenan. Se agrega logo y
              aclaración de los tipos de tarifa en el footer.

Firma       : SS_1397_MGO_20151015
Description : Se reemplaza LOGO_REPORTES por LOGO_REPORTES_WEB
-----------------------------------------------------------------------------}


unit frmReporteTransitos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ppCTMain, ppCtrls, ppBands, ppGIFImage, ppPrnabl,
  ADODB, TXRB, dmConvenios, util, ManejoDatos,
  ppSubRpt, ppDevice,
  ppClass, ppStrtch, ppRegion, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  ppParameter, ComObj, ppPDFDevice, ConstParametrosGenerales;                   //SS_1397_MGO_20151015

type
  TfrmRptTransitos = class(TForm)
    rptUltimosConsumos: TppReport;
    ppUltimosConsumosDetalle: TppDBPipeline;
    dsUltimosConsumosDetalle: TDataSource;
    ppParameterList1: TppParameterList;
    ppHeaderBand2: TppHeaderBand;
    ppRegion2: TppRegion;
    ppShape14: TppShape;
    ppShape15: TppShape;
    ppShape16: TppShape;
    ppLine6: TppLine;
    ppLabel9: TppLabel;
    ppLabel11: TppLabel;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppShape19: TppShape;
    ppLabel19: TppLabel;
    ppColumnHeaderBand1: TppColumnHeaderBand;
    ppLabel14: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel27: TppLabel;
    ppLine8: TppLine;
    ppLine3: TppLine;
    ppDetailBand5: TppDetailBand;
    ppDBText7: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppColumnFooterBand1: TppColumnFooterBand;
    ppLine5: TppLine;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppLogo: TppImage;                         //SS_1397_MGO_20151015
    ppFooterBand1: TppFooterBand;             //SS_1397_MGO_20151015
    plblleyenda: TppLabel;                    //SS_1397_MGO_20151015
    procedure ppDBText16GetText(Sender: TObject; var Text: string);
    procedure ppDBText17GetText(Sender: TObject; var Text: string);
  private
    FCodigoConvenio: integer;
    FDM: TdmConveniosWEB;
  public
    function GenerarPDFUltimosConsumos(DM: TdmConveniosWEB; CodigoConvenio, IndiceVehiculo: integer; var Plantilla: string; var Error: string): boolean;
    function ReporteAPDF(Reporte: TppReport; var Resultado: string; var Error: string): boolean;
    function ExportarReporte(Reporte: TppReport; Dispositivo: TppPDFDevice; var Error: string): boolean;
  end;

var
  frmRptTransitos: TfrmRptTransitos;

implementation

uses Parametros;

{$R *.dfm}

function TfrmRptTransitos.ExportarReporte(Reporte: TppReport;
  Dispositivo: TppPDFDevice; var Error: string): boolean;
begin
    try
        Dispositivo.Publisher := Reporte.Publisher;
        Dispositivo.Reset;
        Reporte.PrintToDevices;
        result := true;
    except
        on e: exception do begin
            result := false;
            Error := '[ExportarReporte] ' + e.message;
        end;
    end;
end;
{-----------------------------------------------------------------------------
  Procedure: GenerarPDFUltimosConsumos
  Author:
  Date:      11-Abr-2008
  Arguments: DM: TdmConveniosWEB; CodigoConvenio, IndiceVehiculo: integer; var Plantilla, Error: string
  Result:    boolean
  Purpose:
  Revision 1:
    Author: lcanteros
    Description: se modifica la consulta donde recupera los datos del vehiculo
    por que en el combo ya no salen repetidas las patentes entonces tomo el codigo de vehiculo
    y antes se tomaba el indice de vehiculo, por esto las patentes salian duplicadas en el combo
-----------------------------------------------------------------------------}
function TfrmRptTransitos.GenerarPDFUltimosConsumos(DM: TdmConveniosWEB; CodigoConvenio, IndiceVehiculo: integer; var Plantilla, Error: string): boolean;
var                                                                             //SS_1397_MGO_20151015
    RutaLogo: AnsiString;                                                       //SS_1397_MGO_20151015
begin
    result := false;
    try
        FCodigoConvenio := CodigoConvenio;
        FDM := DM;
        with FDM.spObtenerUltimosConsumosDetalleVehiculo, Parameters do begin
            Close;
            ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            ParamByName('@CodigoVehiculo').Value := iif(IndiceVehiculo > 0, IndiceVehiculo, null);
            Open;
        end;

        //rptUltimosConsumos.Tag := StrToInt(IntToStr(CodigoConvenio) + IntToStr(iif(IndiceVehiculo > 0, IndiceVehiculo, 0)));  //WEB-NDR-20130110
        rptUltimosConsumos.Tag := CodigoConvenio;                                                                               //WEB-NDR-20130110

        //ObtenerParametroGeneral(FDM.Base, LOGO_REPORTES, RutaLogo);           //SS_1397_MGO_20151015  //SS_1397_MGO_20151008
        ObtenerParametroGeneral(FDM.Base, LOGO_REPORTES_WEB, RutaLogo);         //SS_1397_MGO_20151015
        if FileExists(Trim(RutaLogo)) then                                      //SS_1397_MGO_20151015
            ppLogo.Picture.LoadFromFile(Trim(RutaLogo));                        //SS_1397_MGO_20151015

        result := ReporteAPDF(rptUltimosConsumos, Plantilla, Error);

        FDM.spObtenerUltimosConsumosDetalleVehiculo.Close;
    except
        on e: exception do begin
            FDM.spObtenerUltimosConsumosDetalleVehiculo.Close;
            if e is EOleException then
                Error := MSG_ERROR_TIME_OUT_CN
            else
                Error := 'GenerarPDFUltimosConsumos: [Interno]' + e.Message;
        end;
    end;
end;

procedure TfrmRptTransitos.ppDBText16GetText(Sender: TObject; var Text: string);
begin
    Text := FormatDateTime('DD/MM/YYYY', now);
end;

procedure TfrmRptTransitos.ppDBText17GetText(Sender: TObject; var Text: string);
begin
    Text := ManejoDatos.ObtenerDescripcionConvenio(FDM, FCodigoConvenio);
end;

function TfrmRptTransitos.ReporteAPDF(Reporte: TppReport; var Resultado,
  Error: string): boolean;
var
    Exportador: TppPDFDevice;
    Salida: TStringStream;
    Salida2: TMemoryStream;
begin
    try
        Salida := TStringStream.Create('');
        Salida2 := TMemoryStream.Create;
        Error := '';
        Exportador := TppPDFDevice.Create(nil);

        Exportador.PDFSettings.OptimizeImageExport := False;                                                                                              // SS_995_PDO_20111006
        Exportador.PDFSettings.ScaleImages := False;
        Exportador.FileName := GetTempDir + FormatDateTime('yyyymmddhhnnsszzz_', Now) + IntToStr(Reporte.Tag) + '.pdf';

//    Exportador.PrintToStream := True;
//    Exportador.ReportStream := Salida;

        Result := ExportarReporte(Reporte, Exportador, Error);

        //CopyFile(PChar(Exportador.FileName), PChar('C:\' + ExtractFileName(Exportador.FileName)), False);        

        Salida2.LoadFromFile(Exportador.FileName);
        Salida2.Position := 0;
        Salida2.SaveToStream(Salida);

        Resultado := Salida.DataString;
    finally
      if Assigned(Exportador) then FreeAndNil(Exportador);
      if Assigned(Salida) then FreeAndNil(Salida);
      if Assigned(Salida2) then FreeAndNil(Salida2);
    end;
end;

end.
