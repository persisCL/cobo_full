unit ManejoErrores;

interface

uses eventlog, dmconvenios, sysutils;

procedure ProcesarError(DM: TdmConveniosWeb; Modulo: string; E: Exception);

implementation

procedure ProcesarError(DM: TdmConveniosWeb; Modulo: string; E: Exception);
begin
    try
        EventLogReportEvent(elError, Modulo + ': ' + e.message, '');
        dm.Base.Close;
    except
    end;
end;

end.
 