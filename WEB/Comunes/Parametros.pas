{-----------------------------------------------------------------------------
 Unit Name: Parametros
 Author:
 Date:
 Description:
 History:
 Revision : 1
    Author :   lcanteros
    Date Created: 28-05-2008
    Language : ES-AR
    Description : Se agrega el mensaje (217) configurable de la salida por time out
Revision : 3
    Author :   dAllegretti
    Date Created: 28/01/2009
    Language : ES-AR
    Description : SS 301: Se agrega el mensaje MSG_COMPROBANTE_PENDIENTE para el caso de que existan mensajes sin confirmaci�n

Revision : 4
    Author :   mpiazza
    Date Created: 09/02/2010
    Language : ES-AR
    Description : SS-864 - se agrega el mensaje MSG_OLVIDO_SU_CLAVE

 Revision : 5
    Author : mpiazza
    Date Created: 10/09/2009
    Language : ES-AR
    Description : SS-511 WebCaptcha mensaje de error MSG_CAPTCHA_VALIDACION


 Firma          : SS_1147_MCA_20150505
 Descripcion    : se reemplaza Costanera Norte por Vespucio Sur

 Firma          : SS_1332_CQU_20151106 (Ref. SS_1282_CQU_20150528)
 Descripcion    : Se agrega el mensaje MSG_NK_SIN_TRANSITOS_DISPONIBLES
                  y se usa en aquellos comprobante cuya fecha de emision
                  sea manor al 2 de Mayo de 2015.
-----------------------------------------------------------------------------}
unit Parametros;

interface

uses dmConvenios, inifiles, util, sysutils, variants, ManejoErrores, ConstParametrosGenerales;

const
    FIN_LINEA = #13#10;
    CODIGO_BANCO_SANTANDER = 3;
    CODIGO_BANCO_TRANSBANK = 29;

var
    DIRECTORIO_PLANTILLAS : string = '';
    DIRECTORIO_CONFIRMACIONES_WEBPAY: string = '';
    DIRECTORIO_WEBPAY: string = '';
    NOMBRE_EXE : string = '';
    BASE_PATH : string = '';
//ss-511
    GENERACION_CLAVE_SMTP_SERVER : string = 'mail.vespuciosur.cl';
    GENERACION_CLAVE_SMTP_FROM : string = 'Vespucio Sur <info@vespuciosur.cl>';
    GENERACION_CLAVE_SMTP_USER : string = '';
    GENERACION_CLAVE_SMTP_PASS : string = '';

    EDAD_MINIMA : integer = 18;
    CODIGO_PAIS : string = 'CHI';

    TIPO_DOMICILIO_COMERCIAL : integer = 2;
    MSG_ERROR_DEMASIADOS_USUARIOS : string = 'Existen 2 usuarios con el mismo RUT. Si quiere modificar sus datos por favor comuniquese con el Call Center';
    MSG_ERROR_NO_PASSWORD : string = 'Usted no tiene un password cargado. Por favor, comuniquese con el CALL CENTER';
    MSG_ERROR_ALREADY_IN_SESSION : string = 'Ya existe una sesi�n abierta para su usuario y clave. Reintente de nuevo en unos minutos.';

    MSG_DEBE_ELEGIR_SEXO : string = 'Debe elegir el sexo para la persona de contacto.';
    MSG_DEBE_ELEGIR_EMAIL: string = 'Debe especificar un direcci�n de e-mail.';
    MSG_EMAIL_INVALIDO : string = 'El e-mail ingresado ha sido escrito incorrectamente. Verifiquelo y vuelva a intentarlo.';
    MSG_DEBE_COMPLETAR_TELEFONO : string = 'Debe completar los datos completos del telefono de contacto, incluyendo c�digo de �rea y tipo de tel�fono.';
    MSG_TELEFONO_INCORRECTO : string = 'El tel�fono indicado como principal es incorrecto.';
    MSG_DEBE_COMPLETAR_OTRO_TELEFONO : string = 'Si ha elegido proveer un telefono adicional, debe ingresar todos sus datos, incluyendo c�digo de �rea y tipo de tel�fono.';
    MSG_DEBE_COMPLETAR_HORARIO_CONTACTO : string = 'Debe completar el horario de contacto.';
    MSG_HORARIOS_INCORRECTOS: String = 'El horario inicial debe ser menor al horario final.';
    MSG_DEBE_COMPLETAR_FECHA_NACIMIENTO : string = 'Debe completar la fecha de nacimiento.';
    MSG_FECHA_INVALIDA : string = 'La fecha ingresada es invalida. Por favor, verifique haber ingresado los datos correctamente.';
	MSG_FECHA_INVALIDA_VIEJA : string = 'La fecha ingresada es invalida. No se pueden ingresar fechas de nacimientos anteriores al 1/1/1900';
    MSG_FECHA_DEMASIADO_JOVEN : string = 'El usuario no tiene la edad m�nima para contratar el servicio.';
    MSG_CARACTERES_INVALIDOS_NUMEROS : string = 'Se han encontrado caracteres inv�lidos en %s. Verifique que haya escrito solo n�meros.';
    MSG_DATOS_ERRONEOS : string = 'Se han encontrado datos err�neos en la entrada. Consulte al administrador del sistema.';

    MSG_TIPO_TELEFONO_NO_CORRESPONDE : string = 'El codigo de area ingresado no se corresponde con el tipo de telefono elegido.';

    MSG_FECHA_NACIMIENTO_INCORRECTA : string = 'La fecha de nacimiento ingresada es incorrecta. Corrijala y vuelva a intentar.';
    MSG_DEBE_COMPLETAR_NOMBRE_CONTACTO : string = 'Debe completar el nombre del contacto.';
    MSG_DEBE_COMPLETAR_APELLIDO_PATERNO_CONTACTO : string = 'Debe completar el apellido paterno del contacto.';
    MSG_CARACTERES_INVALIDOS_LETRAS : string = 'Se han encontrado caracteres inv�lidos en %s. Verifique que haya escrito solo letras.';
    MSG_DEBE_ELEGIR_REGION : string = 'Debe seleccionar una regi�n.';
    MSG_DEBE_ELEGIR_COMUNA : string = 'Debe seleccionar una comuna.';
    MSG_DEBE_ELEGIR_CALLE : string = 'Debe seleccionar una calle.';
    MSG_DEBE_COMPLETAR_CALLE_NO_NORMALIZADA : string = 'Si ha elegido "Otra" como calle, debe ingresar su nombre en el cuadro provisto.';
    MSG_DEBE_CARGAR_NUMERO : string = 'Debe ingresar un n�mero.';
    MSG_DEBE_NUMERO_NO_VALIDO : string = 'El n�mero cargado no es un n�mero v�lido.';
    MSG_ANIO_INCORRECTO : string = 'Ha ingresado un a�o de veh�culo incorrecto.';
    MSG_ANIO_FUERA_RANGO : String = 'Ha ingresado un a�o de veh�culo incorrecto. Recuerde que el formato es de 4 digitos (19xx o 200x)';

    MSG_DEBE_ELEGIR_MARCA : string = 'Debe seleccionar la marca de todos los veh�culos.';
    MSG_DEBE_ELEGIR_TIPO_VEHICULO : string = 'Debe seleccionar el tipo de todos los veh�culos.';
    MSG_DEBE_LLENAR_ANIO : string = 'Debe escribir el a�o de todos los veh�culos.';

    MSG_ERROR_PASSWORD_RESETEADO_VENCIDO : string = 'Ha expirado el plazo para ingresar con su nuevo password, por favor comun�quese con el Call Center';

    MSG_RUT_O_CLAVE_INCORRECTO : string = 'RUT o clave incorrecto. Verifique su clave, e intente nuevamente.';
    MSG_CLAVE_NO_CONFIGURADA : string = 'Usted no tiene una clave configurada. Por favor, presione Continuar para comenzar el proceso de generaci�n de clave.';
    MSG_DEBE_CAMBIAR_PASSWORD : string = 'Su clave debe ser cambiada. A continuaci�n ser� conducido a la pagina de cambio de clave.';
    MSG_PASSWORD_CAMBIADO : string = 'La clave ha sido cambiada con �xito.';
    MSG_DATOS_PERSONALES_CAMBIADOS : string = 'Los datos personales han sido cambiados con �xito.';
    MSG_DEBE_ESPECIFICAR_REGION : string = 'Debe especificar una regi�n.';
    MSG_DOMICILIO_CAMBIADO : string = 'Los datos de domicilio han sido cambiados con �xito.';
    MSG_CONVENIO_SIN_PAGO_AUTOMATICO : string = 'Este convenio no tiene datos de pago autom�tico.';
    MSG_VEHICULOS_CAMBIADOS : string = 'Los datos de su/s vehiculo/s han sido cambiados con �xito.';
    MSG_RUT_INEXISTENTE : string = 'Su RUT no figura como titular de ningun convenio.';
    MSG_NO_TIENE_CLAVE : string = 'Usted no tiene una clave configurada. Por favor, presione Continuar para comenzar el proceso de generaci�n de clave.';
    MSG_NO_TIENE_PREGUNTA_SECRETA : string = 'Usted no tiene pregunta secreta, por favor llame al call center.';
    MSG_OLVIDO_SU_CLAVE : string = 'Para obtener una nueva clave, por favor llame al call center.';
    MSG_PASSWORD_CAMBIADO_E_INGRESE : string = 'Su clave ha sido cambiada. Por favor, ingrese al sistema.';
    MSG_RESPUESTA_INCORRECTA : string = 'La respuesta indicada no es correcta. Por favor escribala tal como la escribi� al cargar los datos.';
    MSG_PREGUNTA_CAMBIADA : string = 'Su pregunta ha sido cambiada con �xito.';
    MSG_SECCION_DISPONIBLE_LUEGO_DE_INAUGURACION : string = 'Esta secci�n estar� disponible luego de la inauguraci�n oficial de la autopista.';
    MSG_LA_OPERACION_NO_PUEDE_SER_REALIZADA : string = 'La operaci�n no puede ser realizada. <br><cr>El motivo informado por el sistema es: %s';
    MSG_TBK_TRANSACCION_NO_REALIZADA : string = 'No se ha podido procesar el pago del comprobante %s <BR><BR><CENTER>SU TRANSACCION NO PUDO SER REALIZADA</CENTER><br>Por favor, vuelva a intentarlo, y si el problema persiste comun�quese con su banco emisor.';
    MSG_AVISO_TIENE_DEBITO : string = 'ATENCION: El comprobante seleccionado est� marcado para ser debitado automaticamente. Si contin�a, existe la posibilidad de que se realize un doble pago.';
    MSG_NO_EXISTE_RUT_TITULAR : string = 'Nuestra base de datos no registra ning�n convenio con ese RUT. S�lo se pueden registrar RUT de titulares de convenios.';
    MSG_YA_POSEE_CLAVE : string = 'Usted ya posee una clave, si no la recuerda utilize la opci�n ''Olvid� su clave''';
    MSG_COMPLETE_LOS_DATOS : string = 'Por favor complete todos los datos.';
    MSG_MAIL_INCORRECTO : string = 'El e-mail ha sido mal ingresado. Verifique que est� escrito de manera correcta.';
    MSG_ERROR_CONFIRMACION_EMAIL : string = 'El e-mail ingresado para confirmar no corresponde al e-mail de la base de datos ni al e-mail que luego ingres� inicialmente, por favor reintente.';
    MSG_EMAIL_CON_CLAVE_ENVIADO : string = 'Se ha enviado un e-mail con su nueva clave. Ingrese nuevamente al sitio, donde se le pedir� que la cambie por una clave propia.';
    MSG_ULTIMA_NK_PAGA : string = 'La �ltima nota de cobro ya est� paga.';
    MSG_ULTIMA_NK_VENCIDA : string = 'La �ltima nota de cobro se ha vencido y no puede ser pagada. Su saldo ser� incluido en la pr�xima nota de cobro.';
    MSG_CONVENIO_SIN_NK : string = 'No se han emitido notas de cobro para este convenio.';
    MSG_NK_SIN_TRANSITOS_DISPONIBLES : string = 'El comprobante no tiene tr�nsitos disponibles';   // SS_1332_CQU_20151106
    MSG_NK_SIN_TRANSACCIONES_PEAJE : string = 'Comprobante sin transacciones de peaje';            // SS_1332_CQU_20151106
    MSG_COMPROBANTE_NO_EXISTE : string = 'No existe el comprobante especificado.';
    MSG_COMPROBANTE_NO_ES_DE_CONVENIO : string = 'El comprobante especificado no pertenece al convenio activo.';
    MSG_COMPROBANTE_YA_PAGADO : string = 'El comprobante especificado ya est� pago.';
    MSG_COMPROBANTE_VENCIDO : string = 'El comprobante especificado se ha vencido y no puede ser pagado. Su saldo ser� incluido en la pr�xima nota de cobro.';
    MSG_LINK_WEBPAY : string = 'Pagar On Line';
    MSG_DATOS_MEDIO_ENVIO_GUARDADOS : string = 'Las preferencias de env�o del convenio %s han sido guardadas. Los cambios efectuados ser�n aplicables a su Nota de Cobro del mes %s';
    MSG_SERVICIO_DETALLE_FACTURACION_NO_CONTRATADO : string = 'Usted no est� adherido al servicio de facturaci�n detallada electr�nica. Puede suscribirse desde el panel de control (opci�n "Env�o de Documentos") o en forma telef�nica.';
    MSG_DEMASIADOS_VEHICULOS : string = 'AVISO: Esta opci�n no est� disponible para convenio con mas de %d veh�culos<BR>';
    MSG_CONVENIO_CARPETA_LEGAL : string = 'La operaci�n no puede ser realizada. <br><cr>El motivo informado por el sistema es: %s';
    // leer de la base
    MSG_BUSQUEDA_ACOTADA : string = 'Nota: S�lo se muestran los primeros 50 resultados. Acote su b�squeda.';

    INICIO_OPERACIONES : tdatetime = 38443;   // 1/04/2005

    TITULO_EMAIL_GENERACION_CLAVE : string = 'Env�o clave personal Vespucio Sur';  //SS_1147_MCA_20150505

    TIME_OUT_CONSULTAS_WEB : integer  = 120;
    MSG_ERROR_TIME_OUT_CN : string = 'La Cantidad de Tr�nsitos a visualizar excede el m�ximo admisible'+FIN_LINEA+
        ' para este Servicio. Si desea Ud. El detalle aun, escribanos a '+ FIN_LINEA+
        'consultas@vespuciosur.cl solicit�ndolo mediante su RUT y patentes de inter�s';
	MSG_COMPROBANTE_PENDIENTE : string = 'Existen confirmaci�nes de pago pendientes para este convenio.' + FIN_LINEA + 'Por favor vuelva a intentar saldar su deuda m�s tarde.';//Revision 3
    MSG_CAPTCHA_VALIDACION : string = 'Texto de verificaci�n mal ingresado. Verifique que est� escrito de manera correcta';
    //TIME_OUT_CONSULTAS_WEB : AnsiString ;
    MSG_COMPROBANTE_ESTA_PAGO: string  = 'Este comprobante ya fue pagado usando WebPay.' ;//Revision 4
    
//    NK_MULTIPLE_COMANDO: string = '';
//    NK_MULTIPLE_EXTENSION: string = '';
//    NK_MULTIPLE_MIME_TYPE: string = '';

procedure CargarValoresIniciales(dm: TdmConveniosWEB);

function FacturacionHabilitada(dm: TdmConveniosWEB): boolean;

function ObtenerValorParametroInt(DM: TdmConveniosWEB; Nombre: string; Default: integer): Integer;

function ObtenerAnchoCodigoBarra: double;

implementation

function ObtenerAnchoCodigoBarra: double;
var
    inifile: TIniFile;
begin
    iniFile := TIniFile.create(GetIniFileName);
    result := inifile.ReadInteger('General', 'AnchoBC', 203) / 1000;
    inifile.free;
end;

function FacturacionHabilitada(dm: TdmConveniosWEB): boolean;
begin
    Result := ObtenerValorParametroInt(DM, 'DeshabilitarFacturacionWEB', 0) = 0;
end;

function ObtenerValorParametroInt(DM: TdmConveniosWEB; Nombre: string; Default: integer): Integer;
begin
    try
        with DM.spObtenerParametroGeneral do begin
            Parameters.ParamByName('@Nombre').Value := Nombre;
            execproc;
            if Parameters.ParamByName('@Valor').Value = NULL then begin
                Result := Default;
            end else begin
                Result := Parameters.ParamByName('@Valor').Value;
            end;
        end;
    except
        on e: exception do begin
            ProcesarError(DM, 'ObtenerValorInt [' + Nombre + ']', e);
            Result := Default;
        end;
    end;
end;


function ObtenerMensaje(dm: TdmConveniosWEB; Numero: integer; Otro: String = ''): String;
begin
    try
        with DM.spObtenerMensaje do begin
            Parameters.ParamByName('@Codigo').Value := Numero;
            execproc;
            if (trim(Parameters.ParamByName('@Mensaje').Value) = '') OR
               (Parameters.ParamByName('@Mensaje').Value = NULL) then
                Result := otro
            else
                Result := Parameters.ParamByName('@Mensaje').Value;
        end;
    except
        on e: exception do begin
            ProcesarError(DM, 'ObtenerMensaje [' + inttostr(Numero) + ']', e);
            Result := Otro;
        end;
    end;
end;


function ObtenerValorInt(dm: TdmConveniosWEB; Nombre: string; Default: integer): Integer;
begin
    try
        with DM.spObtenerParametroGeneral do begin
            Parameters.ParamByName('@Nombre').Value := Nombre;
            execproc;
            if VarIsNull(Parameters.ParamByName('@Valor').value) then begin
                Result := Default;
            end else begin
                Result := Parameters.ParamByName('@Valor').Value;
            end;
        end;
    except
        on e: exception do begin
            ProcesarError(DM, 'ObtenerValorInt [' + Nombre + ']', e);
            Result := Default;
        end;
    end;
end;

function ObtenerValor(dm: TdmConveniosWEB; Nombre, Default: string): string;
begin
    try
        with DM.spObtenerParametroGeneral do begin
            Parameters.ParamByName('@Nombre').Value := Nombre;
            execproc;
            if VarIsNull(Parameters.ParamByName('@Valor').value) then begin
                Result := Default;
            end else begin
                Result := Parameters.ParamByName('@Valor').Value;
            end;
        end;
    except
        on e: exception do begin
            ProcesarError(DM, 'ObtenerValor [' + Nombre + ']', e);
            Result := Default;
        end;
    end;
end;
{******************************** Procedure Header *****************************
Procedure Name: CargarValoresIniciales
Author :
Date Created :
Parameters : dm: TdmConveniosWEB
Description :


Revision : 1
    Author : mpiazza
    Date : 25/02/2010
    Description : SS-511 WebCaptcha  se agrega los parametros
    GENERACION_CLAVE_SMTP_USER y GENERACION_CLAVE_SMTP_PASS
*******************************************************************************}
procedure CargarValoresIniciales(dm: TdmConveniosWEB);
begin
    BASE_PATH := GetBasePath;
    EDAD_MINIMA := ObtenerValorInt(DM, 'PS_EDAD_MINIMA', EDAD_MINIMA);

    MSG_FECHA_INVALIDA := ObtenerMensaje(DM, 168,  MSG_FECHA_INVALIDA);
	MSG_FECHA_INVALIDA_VIEJA := ObtenerMensaje(DM, 169, MSG_FECHA_INVALIDA_VIEJA);
    MSG_FECHA_DEMASIADO_JOVEN := ObtenerMensaje(DM, 170, MSG_FECHA_DEMASIADO_JOVEN);

	MSG_ERROR_PASSWORD_RESETEADO_VENCIDO := ObtenerMensaje(DM, 175, MSG_ERROR_PASSWORD_RESETEADO_VENCIDO);

    MSG_TIPO_TELEFONO_NO_CORRESPONDE := ObtenerMensaje(DM, 167, MSG_TIPO_TELEFONO_NO_CORRESPONDE);

    MSG_ERROR_NO_PASSWORD := ObtenerMensaje(DM, 147, MSG_ERROR_NO_PASSWORD);
    MSG_ERROR_ALREADY_IN_SESSION := ObtenerMensaje(DM, 148, MSG_ERROR_ALREADY_IN_SESSION);

    MSG_ERROR_DEMASIADOS_USUARIOS := ObtenerMensaje(DM, 146, MSG_ERROR_DEMASIADOS_USUARIOS);

    MSG_DEBE_COMPLETAR_NOMBRE_CONTACTO := ObtenerMensaje(DM, 106, MSG_DEBE_COMPLETAR_NOMBRE_CONTACTO );
    MSG_DEBE_COMPLETAR_APELLIDO_PATERNO_CONTACTO := ObtenerMensaje(DM, 107, MSG_DEBE_COMPLETAR_APELLIDO_PATERNO_CONTACTO );
    MSG_DEBE_ELEGIR_SEXO := ObtenerMensaje(DM, 108, MSG_DEBE_ELEGIR_SEXO );
    MSG_EMAIL_INVALIDO := ObtenerMensaje(DM, 109, MSG_EMAIL_INVALIDO );
    MSG_DEBE_COMPLETAR_TELEFONO := ObtenerMensaje(DM, 110, MSG_DEBE_COMPLETAR_TELEFONO );
    //MSG_CARACTERES_INVALIDOS_LETRAS := ObtenerMensaje(DM, 111);
    //MSG_CARACTERES_INVALIDOS_NUMEROS := ObtenerMensaje(DM, 112);
    MSG_DEBE_COMPLETAR_HORARIO_CONTACTO := ObtenerMensaje(DM, 113, MSG_DEBE_COMPLETAR_HORARIO_CONTACTO );
    MSG_DEBE_COMPLETAR_FECHA_NACIMIENTO := ObtenerMensaje(DM, 114, MSG_DEBE_COMPLETAR_FECHA_NACIMIENTO );
    //MSG_FECHA_NACIMIENTO_INCORRECTA := ObtenerMensaje(DM, 115);
    MSG_DEBE_ELEGIR_REGION := ObtenerMensaje(DM, 116, MSG_DEBE_ELEGIR_REGION );
    MSG_DEBE_ELEGIR_COMUNA := ObtenerMensaje(DM, 117, MSG_DEBE_ELEGIR_COMUNA );
    MSG_DEBE_ELEGIR_CALLE := ObtenerMensaje(DM, 118, MSG_DEBE_ELEGIR_CALLE );
    MSG_DEBE_COMPLETAR_CALLE_NO_NORMALIZADA := ObtenerMensaje(DM, 119, MSG_DEBE_COMPLETAR_CALLE_NO_NORMALIZADA );
    MSG_DEBE_CARGAR_NUMERO := ObtenerMensaje(DM, 120, MSG_DEBE_CARGAR_NUMERO );
    MSG_DEBE_NUMERO_NO_VALIDO := ObtenerMensaje(DM, 121, MSG_DEBE_NUMERO_NO_VALIDO );
    MSG_DEBE_ELEGIR_MARCA := ObtenerMensaje(DM, 123, MSG_DEBE_ELEGIR_MARCA );
    MSG_DEBE_ELEGIR_TIPO_VEHICULO := ObtenerMensaje(DM, 124, MSG_DEBE_ELEGIR_TIPO_VEHICULO );
    MSG_DEBE_LLENAR_ANIO := ObtenerMensaje(DM, 125, MSG_DEBE_LLENAR_ANIO );
    MSG_ANIO_INCORRECTO := ObtenerMensaje(DM, 126, MSG_ANIO_INCORRECTO );
    MSG_DEBE_COMPLETAR_OTRO_TELEFONO := ObtenerMensaje(DM, 134, MSG_DEBE_COMPLETAR_OTRO_TELEFONO );
    MSG_ANIO_FUERA_RANGO := Obtenermensaje(DM, 137, MSG_ANIO_FUERA_RANGO );
    MSG_HORARIOS_INCORRECTOS := ObtenerMensaje(DM, 139, MSG_HORARIOS_INCORRECTOS );
    MSG_TELEFONO_INCORRECTO := ObtenerMensaje(DM, 142, MSG_TELEFONO_INCORRECTO );


    MSG_RUT_O_CLAVE_INCORRECTO := ObtenerMensaje(DM, 180, MSG_RUT_O_CLAVE_INCORRECTO);
    MSG_CLAVE_NO_CONFIGURADA := ObtenerMensaje(DM, 181, MSG_CLAVE_NO_CONFIGURADA);
    MSG_DEBE_CAMBIAR_PASSWORD := ObtenerMensaje(DM, 182, MSG_DEBE_CAMBIAR_PASSWORD);
    MSG_PASSWORD_CAMBIADO := ObtenerMensaje(DM, 183, MSG_PASSWORD_CAMBIADO);
    MSG_DATOS_PERSONALES_CAMBIADOS := ObtenerMensaje(DM, 184, MSG_DATOS_PERSONALES_CAMBIADOS);
    MSG_DEBE_ESPECIFICAR_REGION := ObtenerMensaje(DM, 185, MSG_DEBE_ESPECIFICAR_REGION);
    MSG_DOMICILIO_CAMBIADO := ObtenerMensaje(DM, 186, MSG_DOMICILIO_CAMBIADO);
    MSG_CONVENIO_SIN_PAGO_AUTOMATICO := ObtenerMensaje(DM, 187, MSG_CONVENIO_SIN_PAGO_AUTOMATICO);
    MSG_VEHICULOS_CAMBIADOS := ObtenerMensaje(DM, 188, MSG_VEHICULOS_CAMBIADOS);
    MSG_RUT_INEXISTENTE := ObtenerMensaje(DM, 189, MSG_RUT_INEXISTENTE);
    MSG_NO_TIENE_CLAVE := ObtenerMensaje(DM, 190, MSG_NO_TIENE_CLAVE);
    MSG_NO_TIENE_PREGUNTA_SECRETA := ObtenerMensaje(DM, 191, MSG_NO_TIENE_PREGUNTA_SECRETA);
    MSG_PASSWORD_CAMBIADO_E_INGRESE := ObtenerMensaje(DM, 192, MSG_PASSWORD_CAMBIADO_E_INGRESE);
    MSG_RESPUESTA_INCORRECTA := ObtenerMensaje(DM, 193, MSG_RESPUESTA_INCORRECTA);
    MSG_PREGUNTA_CAMBIADA := ObtenerMensaje(DM, 194, MSG_PREGUNTA_CAMBIADA);
    MSG_SECCION_DISPONIBLE_LUEGO_DE_INAUGURACION := ObtenerMensaje(DM, 195, MSG_SECCION_DISPONIBLE_LUEGO_DE_INAUGURACION);
    MSG_LA_OPERACION_NO_PUEDE_SER_REALIZADA := ObtenerMensaje(DM, 196, MSG_LA_OPERACION_NO_PUEDE_SER_REALIZADA);
    MSG_TBK_TRANSACCION_NO_REALIZADA := ObtenerMensaje(DM, 197, MSG_TBK_TRANSACCION_NO_REALIZADA);
    MSG_AVISO_TIENE_DEBITO := ObtenerMensaje(DM, 198, MSG_AVISO_TIENE_DEBITO);
    MSG_NO_EXISTE_RUT_TITULAR := ObtenerMensaje(DM, 199, MSG_NO_EXISTE_RUT_TITULAR);
    MSG_YA_POSEE_CLAVE := ObtenerMensaje(DM, 200, MSG_YA_POSEE_CLAVE);
    MSG_COMPLETE_LOS_DATOS := ObtenerMensaje(DM, 201, MSG_COMPLETE_LOS_DATOS);
    MSG_MAIL_INCORRECTO := ObtenerMensaje(DM, 202, MSG_MAIL_INCORRECTO);
    MSG_ERROR_CONFIRMACION_EMAIL := ObtenerMensaje(DM, 203, MSG_ERROR_CONFIRMACION_EMAIL);
    MSG_EMAIL_CON_CLAVE_ENVIADO := ObtenerMensaje(DM, 204, MSG_EMAIL_CON_CLAVE_ENVIADO);
    MSG_ULTIMA_NK_PAGA := ObtenerMensaje(DM, 205, MSG_ULTIMA_NK_PAGA);
    MSG_ULTIMA_NK_VENCIDA := ObtenerMensaje(DM, 206, MSG_ULTIMA_NK_VENCIDA);
    MSG_CONVENIO_SIN_NK := ObtenerMensaje(DM, 207, MSG_CONVENIO_SIN_NK);
    MSG_COMPROBANTE_NO_EXISTE := ObtenerMensaje(DM, 208, MSG_COMPROBANTE_NO_EXISTE);
    MSG_COMPROBANTE_NO_ES_DE_CONVENIO := ObtenerMensaje(DM, 209, MSG_COMPROBANTE_NO_ES_DE_CONVENIO);
    MSG_COMPROBANTE_YA_PAGADO := ObtenerMensaje(DM, 210, MSG_COMPROBANTE_YA_PAGADO);
    MSG_NK_SIN_TRANSITOS_DISPONIBLES := ObtenerMensaje(DM, 219, MSG_NK_SIN_TRANSITOS_DISPONIBLES);  // SS_1332_CQU_20151106
    MSG_NK_SIN_TRANSACCIONES_PEAJE := ObtenerMensaje(DM, 220, MSG_NK_SIN_TRANSACCIONES_PEAJE);      // SS_1332_CQU_20151106

    MSG_COMPROBANTE_VENCIDO := ObtenerMensaje(DM, 211, MSG_COMPROBANTE_VENCIDO);
    MSG_LINK_WEBPAY := ObtenerMensaje(DM, 212, MSG_LINK_WEBPAY);

    MSG_DATOS_MEDIO_ENVIO_GUARDADOS := ObtenerMensaje(DM, 213, MSG_DATOS_MEDIO_ENVIO_GUARDADOS);
    MSG_SERVICIO_DETALLE_FACTURACION_NO_CONTRATADO := ObtenerMensaje(DM, 214, MSG_SERVICIO_DETALLE_FACTURACION_NO_CONTRATADO);
    MSG_DEMASIADOS_VEHICULOS := ObtenerMensaje(DM, 215, MSG_DEMASIADOS_VEHICULOS);
    MSG_BUSQUEDA_ACOTADA := ObtenerMensaje(DM, 216, MSG_BUSQUEDA_ACOTADA);
    MSG_ERROR_TIME_OUT_CN := ObtenerMensaje(DM, 217, MSG_ERROR_TIME_OUT_CN);
    GENERACION_CLAVE_SMTP_SERVER := ObtenerValor(dm, 'GENERACION_CLAVE_SMTP_SERVER', GENERACION_CLAVE_SMTP_SERVER);
    GENERACION_CLAVE_SMTP_FROM := ObtenerValor(dm, 'GENERACION_CLAVE_SMTP_FROM', GENERACION_CLAVE_SMTP_FROM);
    GENERACION_CLAVE_SMTP_USER := ObtenerValor(dm, 'GENERACION_CLAVE_SMTP_USER', GENERACION_CLAVE_SMTP_USER);
    GENERACION_CLAVE_SMTP_PASS := ObtenerValor(dm, 'GENERACION_CLAVE_SMTP_PASS', GENERACION_CLAVE_SMTP_PASS);


    TITULO_EMAIL_GENERACION_CLAVE := ObtenerValor(dm, 'TITULO_EMAIL_GENERACION_CLAVE', TITULO_EMAIL_GENERACION_CLAVE);

    TIME_OUT_CONSULTAS_WEB := ObtenerValorInt(dm, 'TIME_OUT_CONSULTAS_WEB', TIME_OUT_CONSULTAS_WEB);
    //ObtenerParametroGeneral(dm, 'TIME_OUT_CONSULTAS_WEB', TIME_OUT_CONSULTAS_WEB); // obtengo el tiempo max de consulta por parametro general
    if (TIME_OUT_CONSULTAS_WEB < 30)  then  TIME_OUT_CONSULTAS_WEB := 30; // tiempo minimo son 30 segundos

end;


var
    inifile: TIniFile;

initialization

    if isLibrary then begin
        NOMBRE_EXE := 'Convenios.dll';
    end else begin
        NOMBRE_EXE := 'Convenios.exe';
    end;

    iniFile := TIniFile.create(GetIniFileName);
    DIRECTORIO_PLANTILLAS := goodDir(inifile.ReadString('General', 'DirectorioPlantillas', curDir + 'Plantillas'));
    DIRECTORIO_CONFIRMACIONES_WEBPAY := goodDir(inifile.ReadString('General', 'DirectorioConfirmacionesWebpay', curDir));
    DIRECTORIO_WEBPAY := goodDir(inifile.ReadString('General', 'DirectorioWebpay', curDir));
//    NK_MULTIPLE_COMANDO := inifile.ReadString('General', 'ComandoNKMultiple', '"' + GetProgramFilesDir + 'Winrar\rar.exe" a -ep %s %s %s');
//    NK_MULTIPLE_EXTENSION := inifile.ReadString('General', 'ExtensionNKMultiple', 'rar');
//    NK_MULTIPLE_MIME_TYPE := inifile.ReadString('General', 'MimeTypeNKMultiple', 'application/rar');


    {$IFDEF WEBDEBUGGER}
    NOMBRE_EXE := inifile.ReadString('General', 'NombreExe', iif(isLibrary, 'CGIINscripcion.dll', 'CGIINscripcion.exe'));
    {$ENDIF}
    inifile.free;
end.
