{
    File Name : Combos
    Author :
    Date Created:
    Language : ES-AR
    Description :

    Revision : 1
    Author : jjofre
    Date : 13/09/2010
    Description : (// Rev 1.  SS_917 )
                -Se a�adieron o modificaron las siguientes funciones/procedimientos
                    GenerarComboNotasCobro  : Debido a que se cambi� el orden del stored
                    que cargaba dicho combo, se modifica la funci�n para que obtenga la
                    ultima nk emitida como la primera,

    Revision : 2
        Author : pdominguez
        Date   : 05/07/2010
        Description: Infractores Fase 2
            - Se modificaron los siguientes procedimientos/funciones:
                GenerarComboConcesionarias.

    Revision : 3
    Author          :  Eduardo Baeza O.
    Date Created    : 28_Abril_2011
    firma           : PAR00133_EBA_20110419
    Description     : Fase 2 - Pagina Web Convenios
            - Se crea funci�n que devuelve s�lo las concesionarias que tienen itemscuentas relacionados al convenio
            - Se agrega la llamada a UtilDB para ocupar el metodo QueryGetValueInt
}

unit Combos;

interface

uses
    sysutils, util, Parametros, dmConvenios, variants,
    UtilDB //PAR00133_EBA_20110419
    ;
Const
    SEPARADOR_PIPE = '|';
    FORMATO_DDMMYYYY = 'dd/mm/yyyy';
Type
    TDatosUltimaNK = record
        Numero: string;
        PeriodoInicial, PeriodoFinal: TDateTime;
    end;

{---------------------------------------------------------------------------}
function GenerarComboSexo(Seleccionado: string): string;
function GenerarComboDias(Seleccionado: string): string;
function GenerarComboMeses(Seleccionado: string): string;
function GenerarComboAnios(Seleccionado: string): string;
function GenerarComboHorarioInicial(Seleccionado: string): string;
function GenerarComboHorarioFinal(Seleccionado: string): string;

function GenerarComboTiposTelefonos(DM: TdmConveniosWeb; Seleccionado: string): string;
function GenerarComboCodigosDeArea(DM: TdmConveniosWeb; Seleccionado: string): string;

function GenerarComboComunas(DM: TdmConveniosWeb; Region, Seleccionado: string; var ComunaElegida: string): string;
function GenerarComboRegiones(DM: TdmConveniosWeb; Seleccionado: string; var RegionElegida: string): string;
function GenerarArrayCalles(DM: TdmConveniosWeb; Region, Comuna, Texto: string): string;

function GenerarComboConvenios(DM: TdmConveniosWeb; CodigoPersona, Seleccionado: integer; var ConvenioElegido: integer): string;
function GenerarComboMarcas(DM: TdmConveniosWeb; Seleccionado: string): string;
function GenerarComboTipoVehiculos(DM: TdmConveniosWeb; Seleccionado: string): string;

function GenerarComboVehiculosConvenio(DM: TdmConveniosWeb; CodigoConvenio: integer; Seleccionado: string): string;

function GenerarComboVehiculosConvenioOpcionTodos(DM: TdmConveniosWeb; CodigoConvenio: integer; Seleccionado: string): string;
function GenerarComboVehiculosConvenioSinDuplicados(DM: TdmConveniosWeb; CodigoConvenio: integer; Seleccionado: string): string;

function GenerarComboTodasLasComunas(DM: TdmConveniosWeb): string;

function GenerarComboNotasCobro(DM: TdmConveniosWeb; CodigoConvenio: integer; Seleccionado: string; EsRecarga: boolean; var DatosNk: TDatosUltimaNK; var NumerosNK, IniciosNK, FinalizacionesNK: string): string;  // Rev 1.  SS_917
function GenerarComboConcesionarias(DM: TdmConveniosWeb; Seleccionado: string; TieneSeleccioneOpcion: Boolean = True; CodigoTipoConcesionaria: Integer = 0): string;

function GenerarComboConcesionariasAsociadasItemsConvenio(DM: TdmConveniosWeb; Seleccionado: string; CodigoConvenio, IndiceVehiculo: integer;  TieneSeleccioneOpcion: Boolean = True; CodigoTipoConcesionaria: Integer = 0): string;

implementation

{-----------------------------------------------------------------------------
  Procedure: ArmarValoresNotasDeCobro
  Author:    lcanteros
  Date:      18-Jun-2008
  Arguments: PNumero, PInicio, PFin: string; var Numeros, Inicios, Fines: string
  Result:    None
  Description: Va armando los strings de datos de las NK para enviar a la plantilla
  History:   
-----------------------------------------------------------------------------}
procedure ArmarValoresNotasDeCobro(PNumero, PInicio, PFin: string; var Numeros, Inicios, Fines: string);
begin
    if Numeros = EmptyStr then
        Numeros := PNumero
    else
        Numeros := Numeros + SEPARADOR_PIPE + PNumero;

    if Inicios = EmptyStr then
        Inicios := PInicio
    else
        Inicios := Inicios + SEPARADOR_PIPE + PInicio;

    if Fines = EmptyStr then
        Fines := PFin
    else
        Fines := Fines + SEPARADOR_PIPE + PFin;
end;
{***************************************************************
  Revision 1
  Lgisuk
  27/02/06
  ahora carga por default la ultima nota de cobro
  y no lista las sin facturar porque ya hay un menu para eso
  Revision : 2
    Author : lcanteros
    Date : 18/06/2008
    Description : devuelve los valores de las NK para mostrar en la
    Web (SS 675)
  Revision : 3
    Author : Nelson Droguett Sierra.
    Date : 30/06/2009
    Description : En  el combo debe decir la descripcion del comprobante Fiscal y en
    el caso de no estar terminado, decir 'En Proceso'

*****************************************************************  }
function GenerarComboNotasCobro(DM: TdmConveniosWeb; CodigoConvenio: integer; Seleccionado: string; EsRecarga: boolean; var DatosNk: TDatosUltimaNK; var NumerosNK, IniciosNK, FinalizacionesNK: string): string;
var
    UnoElegido: boolean;  UltFechaIni, UltFechaFin: TDateTime;
    UltNotCob, UltDescTipoComprobanteFiscal: string;
    primeraNK: boolean;
begin
    result := '<OPTION Value="">Seleccionar</OPTION>';

    UnoElegido := false;
    primeraNK:= false;// Rev 1.  SS_917
    with DM.spObtenerNotasCobro do begin
        parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        Open;
        while not eof do begin

            // Rev. 3 / 30-06-2009 / Nelson Droguett Sierra
            //result := result + '<OPTION VALUE="' + FieldByName('NumeroComprobante').AsString + '"' +
            //  iif(FieldByName('NumeroComprobante').AsString = Seleccionado, ' selected', '') +
            //  '> Nota de cobro ' + FieldByName('NumeroComprobante').AsString + '</OPTION>' + CRLF;


            result := result + '<OPTION VALUE="' + FieldByName('NumeroComprobante').AsString + '"' +
              iif(FieldByName('NumeroComprobante').AsString = Seleccionado, ' selected', '') +
              '> '+ FieldByName('DescTipoComprobanteFiscal').AsString + '</OPTION>' + CRLF;

            if FieldByName('NumeroComprobante').AsString = Seleccionado then UnoElegido := true;
            //UltNotCob := FieldByName('NumeroComprobante').AsString;

            // Rev 1.  SS_917
            if not primeraNK then begin
                UltNotCob := FieldByName('NumeroComprobanteFiscal').AsString;
                UltFechaIni :=  FieldByName('PeriodoInicial').AsDateTime;
                UltFechaFin := iif(FieldByName('PeriodoFinal').AsDateTime = NullDate, Now, FieldByName('PeriodoFinal').AsDateTime);
                UltDescTipoComprobanteFiscal:=FieldByName('DescTipoComprobanteFiscal').AsString;
            end;
            primeraNK := true;
            // END Rev 1.  SS_917

            ArmarValoresNotasDeCobro(FieldByName('NumeroComprobante').AsString,
                                    FormatDateTime(FORMATO_DDMMYYYY, FieldByName('PeriodoInicial').AsDateTime),
                                    iif(FieldByName('PeriodoFinal').AsDateTime = NullDate, FormatDateTime(FORMATO_DDMMYYYY, Now), FormatDateTime(FORMATO_DDMMYYYY, FieldByName('PeriodoFinal').AsDateTime)),
                                    NumerosNK, IniciosNK, FinalizacionesNK);
            Next;
        end;

        if eof and not UnoElegido and not EsRecarga then begin
            // si es el ultimo item, y no habia nada seleccionado,
            // hacemos que se seleccione el ultimo, que seria la ultima nota de cobro...
            DatosNk.Numero := UltNotCob;
            DatosNk.PeriodoInicial := UltFechaIni;
            DatosNk.PeriodoFinal := UltFechaFin;
            result := stringreplace(result, '> '+UltDescTipoComprobanteFiscal+ ' <' , ' selected> '+UltDescTipoComprobanteFiscal+ ' <', []);
        end;
        Close;
    end;
end;



function GenerarComboTodasLasComunas(DM: TdmConveniosWeb): string;
begin
    result := '';
    with dm.spObtenerTodasLasComunas do begin
        Open;
        while not Eof do begin
            result := result +
              '<OPTION Value="' +
              fieldbyname('CodigoRegion').AsString + '_' +
              fieldbyname('CodigoComuna').AsString +
              '">' +
              fieldbyname('Comuna').AsString + ' (' +
              fieldbyname('Region').AsString + ')' +
              '</OPTION>';
            Next;
        end;
        Close;
    end;
end;

function GenerarComboTipoVehiculos(DM: TdmConveniosWeb; Seleccionado: string): string;
begin
    result := '<OPTION Value="">Elija</OPTION>';
    with DM.spObtenerTiposVehiculos do begin
        Open;
        while not eof do begin
            result := result + '<OPTION VALUE="' + FieldByName('CodigoTipoVehiculo').AsString + '"' +
              iif(FieldByName('CodigoTipoVehiculo').AsString = Seleccionado, ' selected', '') +
              '>' + FieldByName('Descripcion').AsString + '</OPTION>' + CRLF;
            Next;
        end;
        Close;
    end;
end;

function GenerarComboVehiculosConvenio(DM: TdmConveniosWeb; CodigoConvenio: integer; Seleccionado: string): string;
begin
    result := '<OPTION Value="">Todos</OPTION>';
    with DM.spObtenerPatentesVehiculosConvenio do begin
        Parameters.ParamByName('@CodigoConvenio').value := CodigoConvenio;
        Open;
        while not eof do begin
            result := result + '<OPTION VALUE="' + FieldByName('IndiceVehiculo').AsString + '"' +
              iif(FieldByName('IndiceVehiculo').AsString = Seleccionado, ' selected', '') +
              '>' + FieldByName('Patente').AsString + '</OPTION>' + CRLF;
            Next;
        end;
        Close;
    end;
end;

function GenerarComboVehiculosConvenioOpcionTodos(DM: TdmConveniosWeb; CodigoConvenio: integer; Seleccionado: string): string;
begin
    result := '<OPTION Value="S">Seleccione una Patente</OPTION>';
    result := Result + '<OPTION Value="">Todas las patentes (solo descarga PDF)</OPTION>';
    with DM.spObtenerPatentesVehiculosConvenio do begin
        Parameters.ParamByName('@CodigoConvenio').value := CodigoConvenio;
        Open;
        while not eof do begin
            result := result + '<OPTION VALUE="' + FieldByName('IndiceVehiculo').AsString + '"' +
              iif(FieldByName('IndiceVehiculo').AsString = Seleccionado, ' selected', '') +
              '>' + FieldByName('Patente').AsString + '</OPTION>' + CRLF;
            Next;
        end;
        Close;
    end;
end;
{-----------------------------------------------------------------------------
  Procedure: GenerarComboVehiculosConvenioSinDuplicados
  Author:    lcanteros
  Date:      09-Abr-2008
  Arguments: DM: TdmConveniosWeb; CodigoConvenio: integer; Seleccionado: string
  Result:    string
  Purpose:  Obtiene las patentes de un convenio para el combo
-----------------------------------------------------------------------------}
function GenerarComboVehiculosConvenioSinDuplicados(DM: TdmConveniosWeb; CodigoConvenio: integer; Seleccionado: string): string;
begin
    result := '<OPTION Value="S">Seleccione una Patente</OPTION>';
    result := Result + '<OPTION Value="">Todas las patentes (solo descarga PDF)</OPTION>';
    with DM.spObtenerPatentesVehiculosSinDuplicados do begin
        Parameters.ParamByName('@CodigoConvenio').value := CodigoConvenio;
        Open;
        while not eof do begin
            result := result + '<OPTION VALUE="' + FieldByName('CodigoVehiculo').AsString + '"' +
              iif(FieldByName('CodigoVehiculo').AsString = Seleccionado, ' selected', '') +
              '>' + FieldByName('Patente').AsString + '</OPTION>' + CRLF;
            Next;
        end;
        Close;
    end;
end;

function GenerarComboMarcas(DM: TdmConveniosWeb; Seleccionado: string): string;
begin
    result := '<OPTION Value="">Elija</OPTION>';
    with DM.spObtenerMarcas do begin
        Open;
        while (not eof) do begin
            result := result + '<OPTION VALUE="' + FieldByName('CodigoMarca').AsString + '"' +
              iif(FieldByName('CodigoMarca').AsString = Seleccionado, ' selected', '') +
              '>' + UpperCase(FieldByName('Descripcion').AsString) + '</OPTION>' + CRLF;
            Next;
        end;
        Close;
    end;
end;


function StringToJavascript(const s: string): string;
begin
    result := '''' + stringreplace(s, '''', '\''', [rfReplaceAll]) + '''';
end;


function GenerarComboSexo(Seleccionado: string): string;
begin
    result :=
        '<OPTION VALUE=""' + iif(uppercase(Seleccionado) = '', ' selected', '') + '>Seleccionar genero</OPTION>' + CRLF +
        '<OPTION VALUE="M"' + iif(uppercase(Seleccionado) = 'M', ' selected', '') + '>Masculino</OPTION>' + CRLF +
        '<OPTION VALUE="F"' + iif(uppercase(Seleccionado) = 'F', ' selected', '') + '>Femenino</OPTION>' + CRLF;
end;

function GenerarComboDias(Seleccionado: string): string;
var
    i: integer;
begin
    result := '';
    result := result + '<OPTION Value="">D�a</OPTION>';
    for i := 1 to 31 do begin
        if inttostr(i) = Seleccionado then begin
            result := result + '<OPTION VALUE="' + inttostr(i) + '" selected>' + inttostr(i) + '</OPTION>';
        end else begin
            result := result + '<OPTION VALUE="' + inttostr(i) + '">' + inttostr(i) + '</OPTION>';
        end;
    end;
end;

function GenerarComboMeses(Seleccionado: string): string;
const
    Meses : array[1..12] of AnsiString = ('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
var
    i: integer;
begin
    result := '';
    result := result + '<OPTION Value="">Mes</OPTION>';
    for i := 1 to 12 do begin
        if inttostr(i) = Seleccionado then begin
            result := result + '<OPTION VALUE="' + inttostr(i) + '" selected>' + Meses[i] + '</OPTION>';
        end else begin
            result := result + '<OPTION VALUE="' + inttostr(i) + '">' + Meses[i] + '</OPTION>';
        end;
    end;
end;

function GenerarComboAnios(Seleccionado: string): string;
var
    i: integer;
    anios: word;
begin
    anios := EDAD_MINIMA;
    result := '<OPTION Value="">A�o</OPTION>';
    for i := (year(now) - anios) downto 1900 do begin
        if inttostr(i) = Seleccionado then begin
            result := result + '<OPTION VALUE="' + inttostr(i) + '" selected>' + inttostr(i) + '</OPTION>';
        end else begin
            result := result + '<OPTION VALUE="' + inttostr(i) + '">' + inttostr(i) + '</OPTION>';
        end;
    end;
end;

function GenerarComboTiposTelefonos(DM: TdmConveniosWeb; Seleccionado: string): string;
begin
    result := '<OPTION Value="">Elija</OPTION>' + result;
    with DM.spObtenerTiposTelefonos do begin
        Open;
        while not eof do begin
            result := result + '<OPTION VALUE="' + FieldByName('CodigoTipoMedioContacto').AsString + '"' +
            iif(FieldByName('CodigoTipoMedioContacto').AsString = Seleccionado, ' selected', '') +
            '>' + FieldByName('Descripcion').AsString + '</OPTION>' + CRLF;
            Next;
        end;
        Close;
    end;
end;

function GenerarComboCodigosDeArea(DM: TdmConveniosWeb; Seleccionado: string): string;
begin
    //if Seleccionado = '' then Seleccionado := '2';
    result := '<OPTION Value="">Elija</OPTION>';
    with DM.spObtenerCodigosAreaTelefonos do begin
        Open;
        while not eof do begin
            result := result + '<OPTION VALUE="' + FieldByName('CodigoArea').AsString + '"' +
            iif(FieldByName('CodigoArea').AsString = Seleccionado, ' selected', '') +
            '>' + FieldByName('CodigoArea').AsString + '</OPTION>' + CRLF;
            Next;
        end;
        close;
    end;
end;

function GenerarComboHorarios(Seleccionado: string): string;
var
    i: integer;
begin
    result := '<OPTION Value=""' + iif(seleccionado = '', ' selected', '') + '>-Hora-</OPTION>';
    for i := 0 to 23 do begin
        result := result + '<OPTION Value="' + FormatFloat('00', i) + ':00"' +
          iif(FormatFloat('00', i) + ':00' = seleccionado, ' selected', '') +
          '>' + FormatFloat('00', i) + ':00</OPTION>';
    end;
end;

function GenerarComboHorarioInicial(Seleccionado: string): string;
begin
    if Seleccionado = '' then Seleccionado := '09:00';
    result := GenerarComboHorarios(Seleccionado);
end;

function GenerarComboHorarioFinal(Seleccionado: string): string;
begin
    if Seleccionado = '' then Seleccionado := '18:00';
    result := GenerarComboHorarios(Seleccionado);
end;

function GenerarComboRegiones(DM: TdmConveniosWeb; Seleccionado: string; var RegionElegida: string): string;
begin
    result := '';
    with DM.spObtenerRegiones do begin
        Parameters.ParamByName('@CodigoPais').Value := CODIGO_PAIS;
        Open;
        if not eof then begin
            // por defecto siempre el primer elemento de un combo esta seleccionado
            RegionElegida := FieldByName('CodigoRegion').AsString;
        end;
        while not eof do begin
            if (FieldByName('CodigoRegion').AsString = Seleccionado) or
              ((Seleccionado = '') and (FieldByName('TipoRegionDefault').AsBoolean)) then begin
                RegionElegida := FieldByName('CodigoRegion').AsString;
                result := result + '<OPTION VALUE="' + FieldByName('CodigoRegion').AsString + '"' +
                  ' selected>' + FieldByName('Descripcion').AsString + '</OPTION>' + CRLF;
            end else begin
                result := result + '<OPTION VALUE="' + FieldByName('CodigoRegion').AsString + '"' +
                  '>' + FieldByName('Descripcion').AsString + '</OPTION>' + CRLF;
            end;
            Next;
        end;
        close;
    end;
end;

function GenerarComboComunas(DM: TdmConveniosWeb; Region, Seleccionado: string; var ComunaElegida: string): string;
begin
    result := '<OPTION Value="">Elija</OPTION>';
    ComunaElegida := '';
    with DM.spObtenerComunas do begin
        Parameters.ParamByName('@CodigoPais').Value := CODIGO_PAIS;
        Parameters.ParamByName('@CodigoRegion').Value := Region;
        Open;
        while not eof do begin
            if (FieldByName('CodigoComuna').AsString = Seleccionado) then begin
                ComunaElegida := FieldByName('CodigoComuna').AsString;
                result := result + '<OPTION VALUE="' + FieldByName('CodigoComuna').AsString + '"' +
                  ' selected>' + FieldByName('Descripcion').AsString + '</OPTION>' + CRLF;
            end else begin
                result := result + '<OPTION VALUE="' + FieldByName('CodigoComuna').AsString + '"' +
                  '>' + FieldByName('Descripcion').AsString + '</OPTION>' + CRLF;
            end;
            Next;
        end;
        close;
    end;
end;

function GenerarArrayCalles(DM: TdmConveniosWeb; Region, Comuna, Texto: string): string;
begin
    result := '';
    if Texto = '' then begin
        result := 'new Array(' + QuotedStr('Escriba un texto y presione "Buscar".                                ') +
          ', 0, 0)';
        exit;
    end;
    with dm.spBuscarCalles do begin
        parameters.ParamByName('@Region').Value := Region;
        parameters.ParamByName('@Comuna').Value := iif(Comuna <> '', Comuna, NULL);
        parameters.ParamByName('@Cadena').Value := Texto;
        open;
        while not eof do begin
            if result <> '' then result := result + ',';
            result := result + 'new Array(' +
              StringToJavascript(trim(FieldByName('Calle').AsString))
              + ', ' + FieldByName('CodigoCalle').AsString + ', ' + Quotedstr(FieldByName('CodigoComuna').AsString) + ')' + CRLF;
            next;
        end;
        close;
    end;
    if result = '' then begin
        result := 'new Array(' + QuotedStr('No se han encontrado coincidencias.                                ') +
          ', 0, 0)';
    end;
end;

function GenerarComboConvenios(DM: TdmConveniosWeb; CodigoPersona, Seleccionado: integer; var ConvenioElegido: integer): string;
begin
    result := '';
    with dm.spObtenerConveniosCliente do begin
        Parameters.ParamByName('@CodigoCliente').value := CodigoPersona;
        open;
        if recordcount = 0 then begin
            ConvenioElegido := 0;
            result := '<OPTION VALUE="" selected>No se registran convenios.</OPTION>' + CRLF;
        end else begin
            // por defecto siempre el primer elemento de un combo esta seleccionado
            ConvenioElegido := FieldByName('CodigoConvenio').AsInteger;

            while not eof do begin
                if (FieldByName('CodigoConvenio').AsInteger = Seleccionado) then begin
                    ConvenioElegido := FieldByName('CodigoConvenio').AsInteger;
                    result := result + '<OPTION VALUE="' + FieldByName('CodigoConvenio').AsString + '"' +
                      ' selected>' + FieldByName('NumeroConvenio').AsString + '</OPTION>' + CRLF;
                end else begin
                    result := result + '<OPTION VALUE="' + FieldByName('CodigoConvenio').AsString + '"' +
                      '>' + FieldByName('NumeroConvenio').AsString + '</OPTION>' + CRLF;
                end;
                Next;
            end;
        end;
        close;
    end;
end;
{
    Function Name: GenerarComboConcesionarias
    Author :
    Date Created :
    Description  :

    Revision : 2
        Author : pdominguez
        Date   : 05/07/2010
        Description: Infractores Fase 2
            - Se a�aden los par�metros TieneSeleccioneOpcion y CodigoTipoConcesionaria.
}
function GenerarComboConcesionarias(DM: TdmConveniosWeb; Seleccionado: string; TieneSeleccioneOpcion: Boolean = True; CodigoTipoConcesionaria: Integer = 0): string;
begin
    if TieneSeleccioneOpcion then
        result := '<OPTION Value=""'+iif( (Trim(Seleccionado)='') OR (Trim(Seleccionado)='-1'),' selected','')+'>Seleccione Concesionaria</OPTION>'
    else Result := '';

    with DM.spObtenerConcesionarias do begin
        Parameters.ParamByName('@CodigoTipoConcesionaria').Value := iif(CodigoTipoConcesionaria = 0, NULL, CodigoTipoConcesionaria);
        Open;
        while not eof do begin
            result := result + '<OPTION VALUE="' +  FieldByName('CodigoConcesionaria').AsString + '"' +
              iif(FieldByName('CodigoConcesionaria').AsString = Seleccionado, ' selected', '') +
              '>' + FieldByName('Nombrecorto').AsString + ' - '+FieldByName('Descripcion').AsString + '</OPTION>' + CRLF;
            Next;
        end;
        Close;
    end;
end;

{
    Revision        : 3
    Function Name   : GenerarComboConcesionariasAsociadasItemsConvenio
    Author          :  Eduardo Baeza O.
    Date Created    : 28_Abril_2011
    firma           : PAR00133_EBA_20110419
    Description     : Fase 2 - Pagina Web Convenios
            - Devuelve s�lo las concesionarias que tienen itemscuentas relacionados al convenio
}
function GenerarComboConcesionariasAsociadasItemsConvenio(DM: TdmConveniosWeb; Seleccionado: string; CodigoConvenio, IndiceVehiculo: integer;  TieneSeleccioneOpcion: Boolean = True; CodigoTipoConcesionaria: Integer = 0): string;
begin
    if TieneSeleccioneOpcion then
        result := '<OPTION Value=""'+iif( (Trim(Seleccionado)='') OR (Trim(Seleccionado)='-1'),' selected','')+'>Seleccione Concesionaria</OPTION>'
    else Result := '';

    // cuando este store es ejecutado se actualizan las concesionarias que tienen algun itemcuenta sin numero de movimiento
    with dm.spObtenerUltimosConsumos do begin
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        parameters.ParamByName('@IndiceVehiculo').Value := iif(IndiceVehiculo = -1, NULL, QueryGetValueInt(dm.Base, 'SELECT dbo.ObtenerIndiceVehiculo(' + inttostr(IndiceVehiculo) + ')', 600));
        Open;
        close;
    end;

    with DM.spObtenerConcesionariasAsociadasItemsConvenio do begin
        Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        Parameters.ParamByName('@CodigoConcesionaria').Value := NULL;
        Open;
        while not eof do begin
            result := result + '<OPTION VALUE="' +  FieldByName('CodigoConcesionaria').AsString + '"' +
              iif(FieldByName('CodigoConcesionaria').AsString = Seleccionado, ' selected', '') +
              '>' + FieldByName('Nombrecorto').AsString + ' - '+FieldByName('Descripcion').AsString + '</OPTION>' + CRLF;
            Next;
        end;
        Close;
    end;
end;

end.
