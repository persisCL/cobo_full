{********************************** Unit Header ********************************
File Name : ManejoDatos.pas 
Author : mpiazza (Comentario Base)
Date Created: 12/04/2010
Language : ES-AR
Description :

Revision : 1
    Author : mpiazza
    Date : 12/04/2010
    Description : ref:ss-772 (Modificar Domiclilio Particular y de Facturacion)
                  Se modificaron los procedimientos:
                              GrabarCambiosDatosDomicilio
                              CargarDatosDomicilioRequest
                              CargarDatosDomicilio
                              CargarDatosDomicilioConvenio
Revision : 2
    Author : mpiazza
    Date : 04/05/2010
    Description : ref:ss-772 (Modificar Domiclilio Particular y de Facturacion)
                  Se corrigen los procedimientos:
                              GrabarCambiosDatosDomicilio
                              CargarDatosDomicilioRequest
Revision 	: 3
Author		: Nelson Droguett Sierra
Date		: 22-Junio-2010
Description	: (Ref. Fase 2) Se agrega la concesionaria

  Revision 3
  Author:    jjofre
  Date Created: 16/06/2010
  Description: (Ref SS 756) se agregan transacciones expl�citas en la
               base de datos para la funcion RegistrarPagoComprobanteSTD.

  Revision 		:4
  Author		: Nelson Droguett Sierra
  Date			: 09-Julio-2010
  Description	: Se valida el area de los telefonos celulares.

  Revision 		:5
  Author		: jose jofre
  Date			: 01/08/2010
  Description	: ref. 469
            - Se modificaron/a�adieron los siguientes Procedimientos/Funciones
                    GrabarCambiosDatosDomicilioConvenio
                    GrabarCambiosDatosDomicilio

  Revision 		:6
  Author		: jose jofre
  Date			: 13/09/2010
  Description	: ref. (Rev 6.  SS_917)
            - Se modificaron/a�adieron los siguientes Procedimientos/Funciones
                    GenerarPDFTransaccionesComprobante
                    CargarDatosCartolaTransacciones

              -Se agregan las referencias
                    frmReporteFacturacionDetalladaWeb
                    CSVUtils

  Revision 		:7
  Author		: jose jofre
  Date			: 08/10/2010
  Description	: ref. (//Rev 7.  SS_917)
            - Se modificaron/a�adieron los siguientes Procedimientos/Funciones
                 CargarDatosCSV: procedure que obtiene los transitos de un convenio
                                para cargarlos en el csv.

  Revision    : 5
  Date        : 19-Abril-2011
  Author      : Eduardo Baeza Ortega
  Firma       : PAR00133_EBA_20110419
  Description : PAR00133 - Fase 2 - Pagina Web Convenios
                Se cambia la forma de mostrar las concesionarias, s�lo se mostrar�n
                las que tengan itemcuentas relacionados al convenio

Firma       : SS966-NDR-20110810
Description : Se implementa el boton de pago para Banco De Chile (Basado en BDP STD)
Firma       : SS-983-NDR-20111004
Description : Se deben mostrar por ley los 3 ultimos comprobantes para poderlos imprimir (no solo el ultimo)

Firma       : SS-966-NDR-20111130
Description : Los pagos se registran en una tabla de trabajo y luego se registran en forma definitiva.

Firma       : SS-966-NDR-20120103
Description : En el procedure AgregarAuditoriaBDP_MPINIBCH se agrega el parametro SRVREC

Firma       : SS_1038_GVI_20120723
Description : Se agrega ultimo numero de comprobante en los parametros de la funcion Pagar

Firma       : SS_1070_MCO_20121005
Description : Se agrega Procedure AgregarEmailMensaje que registra los datos en la tabla EMAIL_Mensajes para el posterior envio del Email

Firma       :   SS_1221_MBE_20141017
Description :   Se agrega una validaci�n, para impedir que si el �ltimo comprobante es NULL, arroje
                un error de  " '' is not a valid integer value"

Firma       :   SS_1147_MCA_20150416
Description :   se formatea numero con separador de miles.

Firma       :   SS_1147_CQU_20150522
Descripcion :   Se modifica la funci�n AveriguarUltimasFacturas agergando el string list PuedeImprimir
                este nuevo objeto devolver� el TipoComprobante (NK, SD, etc) para evaluar si imprimir o no.

Firma       :   SS_1276_NDR_20150528
Descripcion :   Los cambios de domicilio deben generar un XML para RNUT

Firma       :   SS_1304_MCA_20150709
Descripcion :   Implementacion Grilla de Pago WEB

Firma       : SS_1305_MCA_20150612
Descripcion : se agrega bitacora de uso para el sitio WEB

Firma       :   SS_1311_CQU_20150622
Descripcion :   Se modifica el par�metro @CodigoUsuario en aquellas llamadas de los botones de pago,
                los nuevos nombres a colocar son:
                    �	WEB-BCH     = pago por el bot�n de pago banco de chile
                    �	WEB-BDP     = pago por el bot�n de pago banco santander
                    �	WEB-PAY     = Pago a trav�s de WebPay
                    �	WEB-ESTADO  = Pago a trav�s del nuevo bot�n banco estado.

Firma       : SS_1353_NDR_20150729
Descripcion : Que los botones de pago registren el pago en tablas temporales para no contener la respuesta a la entidad pagadora
              luego internamente se consolidan estos pagos "temporales" en las tablas definitivas
              Los procesos que obtienen la deuda deben tomar en cuenta estos pagos "en proceso"

	Firma       : SS_1356_MCA_20150903
	Descripcion : se agrega BDP Santander para VS

Firma       : SS_1390_MGO_20150928
Descripcion : Se agrega resultado "ConDetalle" al m�todo AveriguarUltimasFacturas, para saber si un
              Comprobante posee o no tr�nsitos

Firma       :   SS_1304_MCA_20151008
Descripcion :   se implementa campo estadopagopendiente para que una vez finalizado el(os) registros de pago, recien pueda procesar

Firma       : SS_1397_MGO_20151008
Descripcion : Se modifica m�todo CargarDatosCartolaTransacciones para renombrar columnas y eliminar el c�digo de tr�nsito

Firma       : SS_1397_MGO_20151015
Descripcion : Se cambia referencia a frmReporteFacturacionDetalladaWeb por frmReporteFacturacionDetallada

Firma       : SS_1332_CQU_20151106
Descripcion : Se agrega la liberaci�n de DTE en el m�todo GenerarPDFComprobante
			  Se agrega resultado "ConDetalle" al m�todo AveriguarUltimasFacturas, para saber si un
              Comprobante posee o no tr�nsitos
*******************************************************************************}
unit ManejoDatos;

interface

uses
    Windows, sysutils, DB, ADODB, dmconvenios, utilhttp, util, combos,
    Parametros, HTTPApp, dateutils, utildb, funcionesweb, variants, ReporteFactura,
    forms, Ejecucion, Zip, ManejoErrores, EventLog, ConstParametrosGenerales,
    PeaTypes, frmReporteBoletaFacturaElectronica, frmReporteFacturacionDetallada, CSVUtils, Classes,       //SS_1397_MGO_20151015   //SS-983-NDR-20111004
    XMLCAC;																										                  //SS_1276_NDR_20150528

const
    CODIGO_ERROR_MENSAJE_DUPLICADO = 2;
    DIAS_DEFAULT_CONSULTA_TRANSACCIONES = -30;

function ObtenerDescripcionConvenio(DM: TDMConveniosWEB; CodigoConvenio: integer): string;
function ObtenerRUTCrudo(DM: TDMConveniosWEB; CodigoPersona: integer): string;

// carga de datos
procedure PersonaCargarDatosPersonales(dm: TDMConveniosWEB; var Plantilla: string; CodigoPersona: integer);
procedure EmpresaCargarDatosPersonales(dm: TDMConveniosWEB; var Plantilla: string; CodigoPersona: integer);
procedure CargarDatosDomicilio(dm: TDMConveniosWeb; var Plantilla: string; CodigoPersona: integer);
procedure CargarDatosDomicilioRequest(DM: TDMConveniosWEB; Request: TWEBRequest; var Plantilla: string);
procedure CargarDatosDomicilioConvenio(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio: integer);
procedure CargarDatosMedioPagoConvenio(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, TipoMedioPago: integer);
procedure CargarDatosVehiculosConvenio(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, CodigoPersona: integer);
procedure CargarDatosFacturacionConvenio(dm: TDMConveniosWeb; Request: TWEBRequest; var Plantilla: string; CodigoConvenio: integer);
procedure CargarDatosUltimosMovimiento(dm: TDMConveniosWeb; Request: TWEBRequest; var Plantilla: string; CodigoConvenio, IndiceVehiculo: integer);
//Rev.3 / 22-Junio-2010 / Nelson Droguett Sierra---------------------------------------------------------------------------------------------------------------------------
procedure CargarDatosUltimosMovimientosVehiculo(dm: TDMConveniosWeb; Request: TWEBRequest; var Plantilla: string; CodigoConvenio, IndiceVehiculo, Concesionaria: integer);
procedure CargarDatosCartolaPagos(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, IndiceVehiculo, Comprobante: integer; FechaDesde, FechaHasta: TDateTime);
procedure CargarDatosCartolaComprobantes(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, IndiceVehiculo, Comprobante: integer; FechaDesde, FechaHasta: TDateTime);
//Rev.3 / 22-Junio-2010 / Nelson Droguett Sierra---------------------------------------------------------------------------------------------------------------------------
procedure CargarDatosCartolaTransacciones(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, IndiceVehiculo, Comprobante, Cantidad, CodigoConcesionaria: Integer; FechaDesde, FechaHasta: TDateTime; var PlantillacsvTransacciones :string); // Rev 8.  SS_917
procedure CargarDatosEstadoDeCuenta(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio: integer);
procedure CargarDatosCliente(dm: TDMConveniosWeb; var Plantilla: string; CodigoPersona: integer);
procedure CargarDatosTarifas(dm: TDMConveniosWeb; var Plantilla: string);
procedure CargarDatosReclamos(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio: integer);
//procedure CargarDatosPago(dm: TDMConveniosWeb; var Plantilla: string; TipoComprobante: string; NumeroComprobante: integer); //SS_1304_MCA_20150709
procedure CargarDatosPago(dm: TDMConveniosWeb; var Plantilla: string; TipoComprobante: string; NumeroComprobante: integer; Importe: Integer); //SS_1304_MCA_20150709
//Rev.3 / 05-Julio-2010 / Nelson Droguett Sierra -------------------------------------
procedure CargarDatosCartolaEstacionamientos(dm: TDMConveniosWeb;
                                             var Plantilla: string;
                                             CodigoConvenio, CodigoConcesionaria, IndiceVehiculo: integer;
                                             FechaDesde, FechaHasta: TDateTime); //Rev.3 / 22-Junio-2010 / Nelson Droguett Sierra
// Fase 2 - Pagina Web Convenios - PAR00133_EBA_20110419 ----------------------------------------------------------------------------------------------------------------------------------
procedure CargarDatosUltimosMovimientosVehiculoEstacionamiento(dm: TDMConveniosWeb; Request: TWEBRequest; var Plantilla: string; CodigoConvenio, IndiceVehiculo, Concesionaria: integer);
// Fase 2 - Pagina Web Convenios - PAR00133_EBA_20110419 ----------------------------------------------------------------------------------------------------------------------------------

function AveriguarUltimaFactura(dm: TDMConveniosWeb; CodigoConvenio: integer; var NroComprobante: int64; var Error: string): boolean;
function AveriguarUltimasFacturas(  dm: TDMConveniosWeb;                        //SS-983-NDR-20111004
                                    CodigoConvenio: integer;                    //SS-983-NDR-20111004
                                    var TiposComprobantesFiscales:TStringList ;      //SS-983-NDR-20111004
                                    var NrosComprobantesFiscales:TStringList ;       //SS-983-NDR-20111004
                                    var NrosComprobantes:TStringList ;               //SS-983-NDR-20111004
                                    var FechasComprobantes:TStringList ;             //SS-983-NDR-20111004
                                    var MontosComprobantes:TStringList ;             //SS-983-NDR-20111004
                                    var ImprimirComprobantes:TStringList;        	// SS_1332_CQU_20151106
                                    var DetallesComprobantes:TStringList;        	// SS_1332_CQU_20151106
                                    var Error: string): boolean;                //SS-983-NDR-20111004

function GenerarUltimaFactura(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio: integer; var NroComprobante: int64; var EsZip: boolean; var Error: string): boolean;
function GenerarPDFComprobante(dm: TDMConveniosWeb; TipoComprobante: string; NroComprobante: int64; var StringPDF: string; var EsZip: boolean; var Error: string): boolean;
function RegistrarPagoComprobante(dm: TDMConveniosWeb; TipoComprobante: string; NumeroComprobante : Integer; Importe : Int64; Cuotas, CodigoRespuesta: integer; Autorizacion, TipoVenta, Tarjeta: string; FechaProceso: tdatetime; var Error: string): boolean;
function RegistrarPagoComprobanteSTD(dm: TDMConveniosWeb; TipoComprobante: string; NumeroComprobante : Integer; Importe : Int64; CodigoRespuesta: integer; FechaProceso: tdatetime; var Error: string): boolean;
function RegistrarPagoComprobanteBCH(dm: TDMConveniosWeb; TipoComprobante: string; NumeroComprobante : Integer; Importe : Int64; CodigoRespuesta: integer; FechaProceso: tdatetime; var Error: string): boolean;                     //SS966-NDR-20110810
function RegistrarPagoPendienteComprobanteSTD(dm: TDMConveniosWeb; TipoComprobante: string; NumeroComprobante : Integer; Importe : Int64; CodigoRespuesta: integer; FechaProceso: tdatetime; CodigoSesion: Integer; var Error: string): boolean;            //SS_1304_MCA_20150709 //SS-1353-NDR-20150729
function RegistrarPagoPendienteComprobanteBCH(dm: TDMConveniosWeb; TipoComprobante: string; NumeroComprobante : Integer; Importe : Int64; CodigoRespuesta: integer; FechaProceso: tdatetime; CodigoSesion: Integer; var Error: string): boolean;            //SS_1304_MCA_20150709 //SS-966-NDR-20111130
function VerificarMACRespuesta(Request: TWebRequest; var Error: string): boolean;
function VerificarPrecioComprobante(dm: TDMConveniosWeb; IdTrx: string; Precio: Int64; var Error: string): boolean;
//function ObtenerUltimaNotaCobroAPagar(dm: TDMConveniosWeb; CodigoConvenio: integer; var TipoComprobante: string; var NumeroComprobante: int64; var Error: string): boolean;       //SS_1304_MCA_20150709
function ObtenerUltimaNotaCobroAPagar(dm: TDMConveniosWeb; CodigoSesion: integer; var TipoComprobante: string; var NumeroComprobante: int64; var CodigoConvenio: Integer; var Error: string): boolean;           //SS_1304_MCA_20150709
function EsConvenioCarpetaLegal(dm: TDMConveniosWeb; CodigoConvenio: integer; var Error: string): boolean;
function CargarSaldosConvenios(dm: TDMConveniosWeb; var Plantilla: string; CodigoPersona: integer): boolean;
procedure CargarDatosCSV(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, IndiceVehiculo: Integer; FechaDesde, FechaHasta: TDateTime; Estacionamientos: Boolean = False);//Rev 7.  SS_917	// SS_1006_PDO_20121115
// verificacion de datos
function PersonaVerificarDatosPersonales(DM: TDMConveniosWEB; Request: TWebRequest; var Error: string): boolean;
function EmpresaVerificarDatosPersonales(DM: TDMConveniosWEB; Request: TWebRequest; var Error: string): boolean;
function VerificarDatosDomicilio(DM: TdmConveniosWeb; Request: TWebRequest; var Error: string): boolean;
function VerificarDatosVehiculos(DM: TdmConveniosWeb; Request: TWebRequest; var Error: string): boolean;
function VerificarSiEsPagable(DM: TdmConveniosWeb; TipoComprobante: string; NumeroComprobante, CodigoConvenio: integer; var Error: string): boolean;
procedure CargarDatosComprobante(DM: TdmConveniosWeb; var Plantilla: string; TipoComprobante: string; NumeroComprobante: integer; CodigoSesion: integer);

// grabacion
function GrabarCambiosDatosPersonalesPersona(DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion: integer; var Error: string): boolean;
function GrabarCambiosDatosPersonalesEmpresa(DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion: integer; var Error: string): boolean;
function GrabarCambiosDatosDomicilio(DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion: integer; var Error: string): boolean;
function GrabarCambiosDatosDomicilioConvenio(DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion, CodigoConvenio: integer; var Error: string): boolean;
function GrabarCambiosVehiculos(DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion, CodigoConvenio: integer; var Error: string): boolean;


function FormatearFecha(Valor: variant; formato: string = 'dd/mm/yy'; ValorSiNull: string = ''): string;
function ValidarDatosOlvidados(DM: TDMConveniosWEB; RUT, Patente, DigitoVerificadorPatente: string; var CodigoPersona: integer; var Error: string): boolean;

// devuelve true si el mail suministrado coincide con el mail registrado para la persona.
function VerificarMailRegistrado(DM: TDMConveniosWEB; CodigoPersona: integer; MailAVerificar: string): boolean;
function ObtenerFechaProximaFacturacion(FDM: TDMConveniosWEB; CodigoConvenio: integer): tdatetime;

// actualiza el email de contacto
procedure ActualizarMailPersona(DM: TDMConveniosWEB; CodigoPersona: integer; NuevoMail: string);
function EsConvenioRNUT(DM: TDMConveniosWEB; CodigoConvenio: integer): boolean;
function ObtenerConcesionariaConvenio(DM: TDMConveniosWEB; CodigoConvenio: integer): string;
function GrabarCambiosMediosEnvio(DM: TDMConveniosWEB; Request: TWebRequest; CodigoConvenio, EleccionMedioEnvio: integer; var Error: string): boolean;
function NoVioPantallaEnvioDocumentos(DM: TDMConveniosWEB; CodigoPersona: integer; var CodigoConvenio: integer): boolean;
function PuedePedirDetalleImpreso(DM: TdmConveniosWeb; CodigoConvenio: integer): boolean;

//Auditoria
Procedure AgregarLog(dm: TDMConveniosWeb; Observaciones : String);
function AgregarAuditoria(dm: TDMConveniosWeb; TipoComprobante : String;
    NumeroComprobante : Int64; Importe : INT64; CodigoBanco : Integer; CodigoSesionWeb: Integer): String;
//Lo transformo en funci�n
//Procedure ActualizarConfirmacionAuditoria(dm: TDMConveniosWeb; IdTrx : String; CodigoBanco : Integer; CodigoRespuesta : String; ConfirmacionBanco : Boolean);
function ActualizarConfirmacionAuditoria(dm: TDMConveniosWeb; IdTrx : String; CodigoBanco : Integer; CodigoRespuesta : String; ConfirmacionBanco : Boolean): Boolean;
//Lo transformo en funci�n
//Procedure ActualizarRespuestaBancoAuditoria(dm: TDMConveniosWeb; IdTrx : String; RespuestaBanco : String; CodigoBanco : Integer);
function ActualizarRespuestaBancoAuditoria(dm: TDMConveniosWeb; IdTrx : String; RespuestaBanco : String; CodigoBanco : Integer): Boolean;
Procedure ActualizarGeneroReciboAuditoria(dm: TDMConveniosWeb; IdTrx : String; GeneroRecibo : Boolean; Observaciones : String; CodigoBanco : Integer);
procedure Auditoria_ActualizarRespuestaAConfirmacion(dm: TDMConveniosWeb; IdTrx : String; CodigoBanco : Integer; RespuestaEnviada: String);
//Mensajeria
Function ParseXMLField( Xml, Campo : String) : String;
//Procedure AgregarAuditoriaBDP_MPINI(dm: TDMConveniosWeb; Request: TWebRequest; IDTRX : String);		//SS_1356_MCA_20150903
Procedure AgregarAuditoriaBDP_MPINI(dm: TDMConveniosWeb; IDTRX, IDCOM, SRVREC, GLOSA : String; MONTO, TOTAL, PRECIO, DATOADIC: Int64; NROPAGOS, CANTIDAD:Integer);	//SS_1356_MCA_20150903
Procedure AgregarAuditoriaBDP_MPOUT(dm: TDMConveniosWeb; Request: TWebRequest);
Procedure AgregarAuditoriaBDP_MPFIN(dm: TDMConveniosWeb; Request: TWebRequest);

Procedure AgregarAuditoriaBDP_MPINIBCH(dm: TDMConveniosWeb; Request: TWebRequest; IDTRX, SRVREC : String);   //SS966-NDR-20110810  //SS-966-NDR-20120103
Procedure AgregarAuditoriaBDP_MPOUTBCH(dm: TDMConveniosWeb; Request: TWebRequest);                   //SS966-NDR-20110810
Procedure AgregarAuditoriaBDP_MPFINBCH(dm: TDMConveniosWeb; Request: TWebRequest);                   //SS966-NDR-20110810
Procedure AgregarAuditoriaBDP_MPCONBCH(dm: TDMConveniosWeb; Request: TWebRequest);                   //SS966-NDR-20110810
Procedure AgregarAuditoriaBDP_MPRESBCH(dm: TDMConveniosWeb; Request: TWebRequest);                   //SS966-NDR-20110810
Procedure RegistrarBitacoraUso(dm: TdmConveniosWEB; CodigoSesion, CodigoPersona, CodigoConvenio, IdBitacoraAccion: Integer; NumeroDocumento, Observacion: string);      //SS_1305_MCA_20150612



// Registro del env�o de email en la generaci�n de clave
procedure AgregarEmailEnvioClave(DM: TDMConveniosWEB; var CodigoMensaje: Int64; Email, Asunto: String; CodigoPersona: Integer; Usuario: String);
procedure AgregarEmailMensaje(DM: TdmConveniosWEB; var Email: string; CodigoPlantilla, CodigoFirma, CodigoConvenio: Integer; Parametros: String); //SS_1070_MCO_20121005
procedure ConfirmarEmailEnvioClave(DM: TDMConveniosWEB; CodigoMensaje: Int64);

function VerificarSiExisteSolicitudPendiente(DM: TdmConveniosWeb; TipoComprobante: string; NumeroComprobante, CodigoBanco: integer; Importe: Int64; var Error: string): boolean;//Revision 6
function VerificarSiEstaPago(DM: TdmConveniosWeb; TipoComprobante: string; NumeroComprobante, CodigoBanco: integer; Importe: Int64; var Error: string): boolean;//Revision 7
function GenerarPDFTransaccionesComprobante (dm: TDMConveniosWeb; TipoComprobante: string; NroComprobante: int64; var StringPDF: string; var Error: string; var EsAviso: Boolean): boolean; // Rev 6.  SS_917

Function IsValidEMailCN(email: String): Boolean;

//function GenerarTablaDeudaConvenio(DM: TdmConveniosWEB; CodigoSesion: Integer; var NrosComprobantes: string):string;												//SS_1356_MCA_20150903 //SS_1304_MCA_20150709
function GenerarTablaDeudaConvenio(DM: TdmConveniosWEB; CodigoSesion: Integer; var NrosComprobantes: string; Importe: Integer):string;								//SS_1356_MCA_20150903 //SS_1304_MCA_20150709


function RealizarPagoComprobantes(dm: TDMConveniosWeb; CodigoSesion, Cuotas, CodigoRespuesta: integer; Autorizacion, TipoVenta, Tarjeta: string; FechaProceso: tdatetime; IDTRX: string; var Error: string): boolean;	//SS_1304_MCA_20150709
function RealizarPagoComprobantesBCH(dm: TDMConveniosWeb; CodigoSesion: Integer; CodigoRespuesta: integer; FechaProceso: tdatetime; var Error: string): boolean;		//SS_1304_MCA_20150709
function RealizarPagoComprobantesSTD(dm: TDMConveniosWeb; CodigoSesion: Integer; CodigoRespuesta: integer; FechaProceso: tdatetime; var Error: string): boolean;		//SS_1304_MCA_20150709

function RegistrarPagoPendienteComprobante(dm: TDMConveniosWeb; TipoComprobante: string; NumeroComprobante : Integer; Importe : Int64; Cuotas:Integer; CodigoRespuesta: integer;Autorizacion, TipoVenta, Tarjeta: string; FechaProceso: tdatetime; CodigoSesion: Integer; CodigoUsuario:String; var Error: string): boolean;                //SS_1304_MCA_20150709 //SS-1353-NDR-20150729


implementation

//uses Classes;                                                                 //SS-983-NDR-20111004

function PuedePedirDetalleImpreso(DM: TdmConveniosWeb; CodigoConvenio: integer): boolean;
var
    MaximaCantidadVehiculos,
    CantidadVehiculosConvenio: integer;
begin
    try
        CantidadVehiculosConvenio := QueryGetValueInt(dm.Base, 'WEB_CV_ObtenerCantidadVehiculosConvenio ' + inttostr(CodigoConvenio));
        MaximaCantidadVehiculos := ObtenerValorParametroInt(dm, 'TOPE_VEHICULOS_FACT_DETALLADA', 99);
        result := CantidadVehiculosConvenio <= MaximaCantidadVehiculos;
    except
        result := true;
    end;
end;


function ObtenerFechaProximaFacturacion(FDM: TDMConveniosWEB; CodigoConvenio: integer): tdatetime;
begin
    with fdm.spObtenerFechasProximaFacturacion do begin
        parameters.parambyname('@CodigoConvenio').value := CodigoConvenio;
        open;
        result := fieldbyname('Ejecucion').asdatetime;
        close;
    end;
end;

function NoVioPantallaEnvioDocumentos(DM: TDMConveniosWEB; CodigoPersona: integer; var CodigoConvenio: integer): boolean;
begin
    //try
        with dm.spVioPantallaMediosEnvio do begin
            parameters.Refresh;
            parameters.parambyname('@CodigoPersona').value := CodigoPersona;
            parameters.parambyname('@YaLoVio').value := Null;
            parameters.parambyname('@CodigoConvenio').value := Null;

            execproc;

            result := parameters.parambyname('@YaLoVio').value = 0;
            if result then begin
                CodigoConvenio := parameters.parambyname('@CodigoConvenio').value;
            end;
        end;
    //except
    //    result := false;
    //end;
end;

function GrabarCambiosMediosEnvio(DM: TDMConveniosWEB; Request: TWebRequest; CodigoConvenio, EleccionMedioEnvio: integer; var Error: string): boolean;
const
    CODIGO_ENVIO_CORREO_POSTAL = 1;
    CODIGO_ENVIO_CORREO_EMAIL = 2;
var
    NotaCobro,
    ResumenTrimestral,
    DetalleTransitos: integer;
begin
//    NotaCobro := strtointdef(request.ContentFields.values['NotaCobroEmail'], 0);
//    ResumenTrimestral := strtointdef(request.ContentFields.values['ResumenTrimestrarMail'], 0);
//    DetalleTransitos := strtointdef(request.ContentFields.values['DetalleFacturacion'], 0);

    NotaCobro := 0;
    DetalleTransitos := 0;
    ResumenTrimestral := 0;
    case EleccionMedioEnvio of
        1:  begin
                NotaCobro := CODIGO_ENVIO_CORREO_EMAIL;
                DetalleTransitos := CODIGO_ENVIO_CORREO_EMAIL;
            end;
        2:  begin
                NotaCobro := CODIGO_ENVIO_CORREO_EMAIL;
            end;
        3:  begin
                DetalleTransitos := CODIGO_ENVIO_CORREO_EMAIL;
            end;
        4:  begin
                DetalleTransitos := CODIGO_ENVIO_CORREO_POSTAL;
            end;
    else
    end;

    try
        with dm.spGrabarMediosEnvio do begin
            parameters.Refresh;                                                 //SS_1393_MGO_20151022
            parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            parameters.ParamByName('@MedioEnvioNotaCobro').Value := NotaCobro;
            parameters.ParamByName('@MedioEnvioResumenTrimestral').Value := ResumenTrimestral;
            parameters.ParamByName('@MedioEnvioDetalleTransitos').Value := DetalleTransitos;
            parameters.ParamByName('@CodigoUsuario').Value := 'WEB';
            parameters.ParamByName('@Todos').Value := 1;                        //SS_1393_MGO_20151022

            ExecProc;
            result := true;
        end;
    except
        on e: exception do begin
            result := false;
            Error := e.Message;
        end;
    end;
end;

function ObtenerConcesionariaConvenio(DM: TDMConveniosWEB; CodigoConvenio: integer): string;
begin
    result := QueryGetValue(dm.Base, 'SELECT dbo.ObtenerConcesionariaConvenio(' + inttostr(CodigoConvenio) + ')');
end;

function EsConvenioRNUT(DM: TDMConveniosWEB; CodigoConvenio: integer): boolean;
begin
    result := StrToBool(QueryGetValue(dm.Base, 'SELECT dbo.WEB_EsConvenioRNUT(' + inttostr(CodigoConvenio) + ')'));
end;

procedure ActualizarMailPersona(DM: TDMConveniosWEB; CodigoPersona: integer; NuevoMail: string);
begin
    try
        with dm.spCambiarMailPersona do begin
            Parameters.Refresh;                                                 //SS_1393_MGO_20151022
            Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
            Parameters.ParamByName('@NuevoMail').Value := NuevoMail;
            Parameters.ParamByName('@Usuario').Value := 'WEB';                  //SS_1393_MGO_20151022
            execproc;
        end;
    except
        on e: exception do raise exception.create('Error actualizando el email: ' + e.Message);
    end;
end;

function VerificarMailRegistrado(DM: TDMConveniosWEB; CodigoPersona: integer; MailAVerificar: string): boolean;
begin
    with dm.spVerificarMailPersona do begin
        Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
        Parameters.ParamByName('@MailAVerificar').Value := MailAVerificar;

        execproc;

        result := Parameters.ParamByName('@RETURN_VALUE').Value = 0;
    end;
end;

function ValidarDatosOlvidados(DM: TDMConveniosWEB; RUT, Patente, DigitoVerificadorPatente: string; var CodigoPersona: integer; var Error: string): boolean;
begin
    // Agrego ceros al RUT para normalizar la longitud a 9 digitos
    RUT := Padl(Trim(RUT), 9, '0');

    if not dm.Base.Connected then dm.Base.Open;

    // lo mando a validar...
    with dm.spValidarDatosGeneracionPassword do begin
        parameters.ParamByName('@NumeroDocumento').Value := trim(RUT);
        parameters.ParamByName('@Patente').Value := trim(uppercase(Patente));
        parameters.ParamByName('@DigitoVerificadorPatente').Value := trim(uppercase(DigitoVerificadorPatente));

        execproc;
        //CodigoPersona := parameters.ParamByName('@CodigoPersona').Value;                                                                      //SS_1305_MCA_20150612
        CodigoPersona := iif(parameters.ParamByName('@CodigoPersona').Value=null, 0 , parameters.ParamByName('@CodigoPersona').Value);          //SS_1305_MCA_20150612
        result := parameters.ParamByName('@RETURN_VALUE').Value = 0;
        if not result then begin
            Error := parameters.ParamByName('@MensajeError').Value;
        end;

    end;
end;

//function ObtenerUltimaNotaCobroAPagar(dm: TDMConveniosWeb; CodigoConvenio: integer; var TipoComprobante: string; var NumeroComprobante: int64; var Error: string): boolean;       //SS_1304_MCA_20150709
function ObtenerUltimaNotaCobroAPagar(dm: TDMConveniosWeb; CodigoSesion: integer; var TipoComprobante: string; var NumeroComprobante: int64; var CodigoConvenio : Integer; var Error: string): boolean;           //SS_1304_MCA_20150709
begin
    result := false;
    with dm.spObtenerUltimoComprobante do begin
        try
            //parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;//SS_1304_MCA_20150709
            parameters.ParamByName('@CodigoSesion').Value := CodigoSesion;      //SS_1304_MCA_20150709
            open;

            if not eof then begin
                TipoComprobante := fieldbyname('TipoComprobante').AsString;
                NumeroComprobante := fieldbyname('NumeroComprobante').AsInteger;
                CodigoConvenio  := FieldByName('CodigoConvenio').AsInteger;	//SS_1304_MCA_20150709


                // compruebo si esta impago
                if QueryGetValueInt(dm.Base, 'WEB_CV_VerificarComprobanteEstaImpago ' + quotedstr(TipoComprobante) + ', ' + inttostr(NumeroComprobante) + ', ' + inttostr(CodigoConvenio)) <= 0 then begin	// SS966-NDR-20110810
                    Error := MSG_ULTIMA_NK_PAGA;
                    exit;
                end;

                // verifico que no est� vencido la fecha de pago
                if QueryGetValueInt(dm.Base, 'WEB_CV_VerificarComprobanteTodaviaPagable ' + quotedstr(TipoComprobante) + ', ' + inttostr(NumeroComprobante) + ', ' + inttostr(CodigoConvenio)) = 0 then begin
                    Error := MSG_ULTIMA_NK_VENCIDA;
                    exit;
                end;

                result := true;
            end else begin
                Error := MSG_CONVENIO_SIN_NK;
            end;
        finally
            close;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: CargarDatosComprobante
Author :
Date Created :
Description :
Parameters : DM: TdmConveniosWeb; var Plantilla: string; TipoComprobante: string; NumeroComprobante: integer
Return Value : None

Revision 1:
    Author : ggomez
    Date : 07/09/2006
    Description : Cambi� la forma de formatear los valores de ImporteSinDecimales
        y de ImporteDesc, debido a que esos valores ahora pueden ser bigint, ya
        no se los puede acceder como AsInteger, ahora se acceden como AsFloat.

Revision 2:
    Author : ggomez
    Date : 16/01/2007
    Description : Quit� la generaci�n del valor del campo TBK_ID_SESION.
        Esto lo hice pues ahora este valor ser� generado cuando se almacenan en
        auditor�a los datos de la transacci�n de pago.

Revision : 3
    Author : ggomez
    Date : 17/01/2007
    Description : Quit� el reemplazo en la plantilla del TAG STD_ID_SESION pues
        ya no se utiliza m�s, debido a que el IDTRX se genera con el Codigo de
        Auditor�a.
*******************************************************************************}
procedure CargarDatosComprobante(DM: TdmConveniosWeb; var Plantilla: string; TipoComprobante: string; NumeroComprobante: integer; CodigoSesion: integer);
var
    SRVREC : String;
begin
    with dm.spObtenerDatosComprobante do begin
        try
            parameters.parambyname('@TipoComprobante').value := TipoComprobante;
            parameters.parambyname('@NumeroComprobante').value := NumeroComprobante;
            open;

            plantilla := replacetag(plantilla, 'TipoComprobante',fieldbyname('TipoComprobante').AsString);
            plantilla := replacetag(plantilla, 'TipoComprobanteDesc',fieldbyname('TipoComprobanteDesc').AsString);
            plantilla := replacetag(plantilla, 'NroComprobante',fieldbyname('NumeroComprobante').AsString);
            //plantilla := replacetag(plantilla, 'Importe', fieldbyname('Importe').AsString);			//SS_1304_MCA_20150709
            plantilla := replacetag(plantilla, 'Importe', fieldbyname('ImporteTotal').AsString);		//SS_1304_MCA_20150709
            //plantilla := replacetag(plantilla, 'ImporteDesc', formatfloat('#,##0', fieldbyname('Importe').AsFloat / 100));		//SS_1304_MCA_20150709
            plantilla := replacetag(plantilla, 'ImporteDesc', formatfloat('#,##0', fieldbyname('ImporteTotal').AsFloat / 100));		//SS_1304_MCA_20150709

            //Datos para boton de pago santander
            SRVREC := QueryGetValue(dm.Base, 'SELECT TOP 1 SRVREC FROM WEB_CV_Servicios_Recaudacion_BDP WITH (NOLOCK) ORDER BY FechaHora DESC');
            plantilla := replacetag(plantilla, 'SRVREC', SRVREC);
            //plantilla := replacetag(plantilla, 'ImporteSinDecimales', FloatToStr(Trunc(fieldbyname('Importe').AsFloat / 100)));		//SS_1304_MCA_20150709
            plantilla := replacetag(plantilla, 'ImporteSinDecimales', FloatToStr(Trunc(fieldbyname('ImporteTotal').AsFloat / 100)));	//SS_1304_MCA_20150709


        finally
            close;
        end;
    end;
end;

function VerificarSiEsPagable(DM: TdmConveniosWeb; TipoComprobante: string; NumeroComprobante, CodigoConvenio: integer; var Error: string): boolean;
begin
    result := false;
    // compruebo si el comprobante existe
    if QueryGetValueInt(dm.Base, 'WEB_CV_VerificarSiExisteComprobante ' + quotedstr(TipoComprobante) + ', ' + inttostr(NumeroComprobante)) = 0 then begin
        Error := MSG_COMPROBANTE_NO_EXISTE;
        exit;
    end;

    // verifico que el comprobante corresponda al convenio
    if QueryGetValueInt(dm.Base, 'WEB_CV_VerificarComprobanteConConvenio ' + quotedstr(TipoComprobante) + ', ' + inttostr(NumeroComprobante) + ', ' + inttostr(CodigoConvenio)) = 0 then begin
        Error := MSG_COMPROBANTE_NO_ES_DE_CONVENIO;
        exit;
    end;

    // compruebo si esta impago
    if QueryGetValueInt(dm.Base, 'WEB_CV_VerificarComprobanteEstaImpago ' + quotedstr(TipoComprobante) + ', ' + inttostr(NumeroComprobante) + ', ' + inttostr(CodigoConvenio)) = 0 then begin
        Error := MSG_COMPROBANTE_YA_PAGADO;
        exit;
    end;

    // verifico que no est� vencido la fecha de pago
    if QueryGetValueInt(dm.Base, 'WEB_CV_VerificarComprobanteTodaviaPagable ' + quotedstr(TipoComprobante) + ', ' + inttostr(NumeroComprobante) + ', ' + inttostr(CodigoConvenio)) = 0 then begin
        Error := MSG_COMPROBANTE_VENCIDO;
        exit;
    end;

    result := true;
end;


{******************************** Function Header ******************************
Function Name: VerificarPrecioComprobante
Author :
Date Created :
Description :
Parameters : dm: TDMConveniosWeb; TipoComprobante: string; NumeroComprobante: int64;
    Precio: Int64; var Error: string
Return Value : boolean

Revision 1:
    Author : ggomez
    Date : 07/09/2006
    Description : cambi� el tipo de datos del par�metro Precio, pues ahora el
        TotalAPagar es un bigint.

Revision 2:
    Author : ggomez
    Date : 11/09/2006
    Description : Correg� un error que ocurr�a al comparar
        parameters.parambyname('@TotalAPagar').Value = Precio. Ahora se almacena
        parameters.parambyname('@TotalAPagar').Value en una variable Int64 y se
        compara Precio contra esta variable.

Revision 3:
    Author : lgisuk
    Date : 22/12/2006
    Description : Ahora obtengo el total de web_cv_auditoria
*******************************************************************************}
function VerificarPrecioComprobante(dm: TDMConveniosWeb; IdTrx: string; Precio: Int64; var Error: string): boolean;
var
    Importe : Int64;
begin
    try
        with dm.spObtenerImporteComprobante do begin
            Parameters.ParamByName('@IDTRX').Value := IdTrx;
            ExecProc;
            Importe := parameters.parambyname('@Importe').Value;
            Result := (Importe = Precio);
            if not Result then begin
                Error := 'El precio no coincide. Deberia ser ' + IntToStr(Importe)
                            + ' pero WebPay inform� haber cobrado ' + IntToStr(Precio);
            end;
        end;
    except
        on E : Exception do begin
            Result := False;
            Error := E.Message;
        end;
    end;
end;

//procedure CargarDatosPago(dm: TDMConveniosWeb; var Plantilla: string; TipoComprobante: string; NumeroComprobante: integer); //SS_1304_MCA_20150709
procedure CargarDatosPago(dm: TDMConveniosWeb; var Plantilla: string; TipoComprobante: string; NumeroComprobante: integer; Importe: Integer);   //SS_1304_MCA_20150709
begin
    with dm.spObtenerDatosPago do begin
        try
            parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
            parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
            open;

            plantilla := replacetag(plantilla, 'Fecha', formatdatetime('dd/mm/yy', fieldbyname('FechaHora').AsDateTime));
            plantilla := replacetag(plantilla, 'Hora', formatdatetime('hh:nn', fieldbyname('FechaHora').AsDateTime));

            plantilla := replacetag(plantilla, 'TipoComprobante',fieldbyname('TipoComprobante').AsString);
            plantilla := replacetag(plantilla, 'TipoComprobanteDesc',fieldbyname('TipoComprobanteDesc').AsString);
            plantilla := replacetag(plantilla, 'NroComprobante', fieldbyname('TipoComprobante').AsString + ' ' + fieldbyname('NumeroComprobante').AsString);         //SS_1304_MCA_20150709
            //plantilla := replacetag(plantilla, 'Importe', formatfloat('#,###', fieldbyname('Importe').AsFloat / 100));
            plantilla := replacetag(plantilla, 'Importe', formatfloat('#,###', Importe / 100));

            plantilla := replacetag(plantilla, 'Cuotas', fieldbyname('Cuotas').AsString);

            if fieldbyname('WEB_TipoVenta').AsString = 'VN' then begin
                plantilla := replacetag(plantilla, 'TipoCuotas', 'Sin Cuotas');
                plantilla := replacetag(plantilla, 'TipoPago', 'Cr�dito');
            end else if fieldbyname('WEB_TipoVenta').AsString = 'VC' then begin
                plantilla := replacetag(plantilla, 'TipoCuotas', 'Cuotas Normales');
                plantilla := replacetag(plantilla, 'TipoPago', 'Cr�dito');
            end else if fieldbyname('WEB_TipoVenta').AsString = 'SI' then begin
                plantilla := replacetag(plantilla, 'TipoCuotas', 'Sin Inter�s');
                plantilla := replacetag(plantilla, 'TipoPago', 'Cr�dito');
            end else if fieldbyname('WEB_TipoVenta').AsString = 'CI' then begin
                plantilla := replacetag(plantilla, 'TipoCuotas', 'Cuotas Comercio');
                plantilla := replacetag(plantilla, 'TipoPago', 'Cr�dito');
            end else if FieldByName('Web_TipoVenta').AsString = 'VD' then begin
                plantilla := replacetag(plantilla, 'TipoCuotas', 'Debito');
                plantilla := ReplaceTag(Plantilla, 'TipoPago', 'RedCompra');
            end;

            plantilla := replacetag(plantilla, 'Cliente', fieldbyname('Cliente').AsString);
            plantilla := replacetag(plantilla, 'Convenio', fieldbyname('CodigoConvenio').AsString);
            plantilla := replacetag(plantilla, 'FinalTarjeta', fieldbyname('Web_NumeroFinalTarjetaCredito').AsString);

            plantilla := replacetag(plantilla, 'CodigoAutorizacion',fieldbyname('Autorizacion').AsString);
        finally
            close;
        end;
    end;
end;

function VerificarMACRespuesta(Request: TWebRequest; var Error: string): boolean;
var
    NombreArchivo: string;
    Respuesta: string;
begin
    result := False;
    //Diego: la extension del arhivo tiene que ser .tmp ej: "TBK_ID_TRANSACCION"&".tmp"
    //       y DIRECTORIO_CONFIRMACIONES_WEBPAY debe apuntar a "inetpub\wwwroot\cgi-bin\temporal"
    //       porque alli esperan encontrar estos archivos, la gente de Transbank segun el manual.

    NombreArchivo := Request.ContentFields.values['TBK_ID_TRANSACCION'] + '.txt';
    stringtofile(request.Content, DIRECTORIO_CONFIRMACIONES_WEBPAY + NombreArchivo);

    // lo mando a ejecutar... si sale mal, el error ya viene cargado
    if not Ejecutar(DIRECTORIO_WEBPAY + 'tbk_check_mac.exe ' + DIRECTORIO_CONFIRMACIONES_WEBPAY + NombreArchivo, Respuesta, Error) then exit;

    if pos('CORRECTO', Respuesta) < 1 then begin
        Error := 'Error verificando el MAC: la respuesta fue ' + Respuesta + ' ' + Error;				//SS_1356_MCA_20150903
    end else begin
        result := true;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name:  RegistrarPagoComprobante
  Author:    drodriguez
  Date Created:
  Description: Registro el pago recibido
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  03/08/2007
  Cambio parametro Importe de INTEGER a INT64
  Revision : 2
      Author : vpaszkowicz
      Date : 03/10/2008
      Description : Agrego transacciones
-----------------------------------------------------------------------------}
function RegistrarPagoComprobante(dm: TDMConveniosWeb; TipoComprobante: string; NumeroComprobante : Integer; Importe : Int64; Cuotas, CodigoRespuesta: integer; Autorizacion, TipoVenta, Tarjeta: string; FechaProceso: tdatetime; var Error: string): boolean;
var
    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
begin
    Result := False;

    // Intentamos registrar el pago de un comprobante.
    Reintentos  := 0;

    while Reintentos < 3 do begin

        try
            dm.Base.BeginTrans;
            with dm.spRegistrarPago do begin
                parameters.Refresh;
                parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
                parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
                parameters.ParamByName('@Importe').Value := Importe;
                parameters.ParamByName('@CodigodeRespuesta').Value := CodigoRespuesta;
                parameters.ParamByName('@CodigodeAutorizacion').Value := Autorizacion;
                parameters.ParamByName('@Fechaproceso').Value := Fechaproceso;
                parameters.ParamByName('@UltimosDigitosTarjeta').Value := Tarjeta;
                parameters.ParamByName('@TipoVenta').Value := TipoVenta;

                parameters.ParamByName('@Cuotas').Value := Cuotas;
                parameters.ParamByName('@DescripcionError').Value := '';
                // Revision 2
                //parameters.ParamByName('@CodigoUsuario').Value := 'WEB'; // La variable "UsuarioSistema" esta vacio en este momento;  // SS_1311_CQU_20150622
                parameters.ParamByName('@CodigoUsuario').Value := 'WEB-PAY';                                                            // SS_1311_CQU_20150622
                execproc;

                result := parameters.ParamByName('@RETURN_VALUE').Value = 0;
                if not result then begin
                    Error := inttostr(parameters.ParamByName('@RETURN_VALUE').Value) + ' ' + parameters.ParamByName('@DescripcionError').Value;
                    dm.Base.RollbackTrans;
                    Exit;
                end;
                //Salgo del ciclo por Ok
                dm.Base.CommitTrans;//Revision 3
                Break;

            end;
        except
            on e: exception do begin
                if dm.Base.InTransaction then dm.Base.RollbackTrans; //Revision 3
                if (pos('deadlock', e.Message) > 0) then begin
                    mensajeDeadlock := e.message;
                    inc(Reintentos);
                    sleep(2500);
                end else begin
                    result := false;
                    error := e.Message;
                    Exit;
                end;
            end;
        end;
    end;
     //una vez que se reintento 3 veces x deadlock lo informo
     if Reintentos = 3 then begin
           result := false;
           error := mensajeDeadLock;
           if dm.Base.InTransaction then dm.Base.RollbackTrans; //Revision 3
     end;
end;

{-----------------------------------------------------------------------------
  Function Name:  RegistrarPagoComprobanteSTD
  Author:    lgisuk
  Date Created: 03/08/2006
  Description: Registro el pago recibido
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
  Revision 1
  Author:    lgisuk
  Date Created: 30/11/2006
  Description: Aumentamos los reintentos a cinco
-----------------------------------------------------------------------------
  Revision 2
  Author:    lgisuk
  Date Created: 03/08/2007
  Description: Cambio importe de integer a int64
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
  Revision 3
  Author:    dAllegretti
  Date Created: 26/09/2008
  Description: Se agrega el manejo de transacciones a nivel de Delphi
-----------------------------------------------------------------------------}
function RegistrarPagoComprobanteSTD(dm: TDMConveniosWeb; TipoComprobante: string; NumeroComprobante : Integer; Importe : Int64; CodigoRespuesta: integer; FechaProceso: tdatetime; var Error: string): boolean;
Const
    MAX_REINTENTOS = 5;
var
    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
begin
    Result := False;

    // Intentamos registrar el pago de un comprobante.
    Reintentos  := 0;

    while Reintentos < MAX_REINTENTOS do begin

        try
          //dm.Base.BeginTrans; //Revision 3
            QueryExecute(DM.Base, 'BEGIN TRAN spRegistrarPagoSTD');
            with dm.spRegistrarPagoSTD do begin
                parameters.Refresh;
                parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
                parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
                parameters.ParamByName('@Importe').Value := Importe;
                parameters.ParamByName('@CodigodeRespuesta').Value := CodigoRespuesta;
                parameters.ParamByName('@Fechaproceso').Value := Fechaproceso;
                parameters.ParamByName('@DescripcionError').Value := '';
                // Revision 2
                //parameters.ParamByName('@CodigoUsuario').Value := 'WEB'; // La variable "UsuarioSistema" esta vacio en este momento;  // SS_1311_CQU_20150622
                parameters.ParamByName('@CodigoUsuario').Value := 'WEB-BDP';                                                            // SS_1311_CQU_20150622
                CommandTimeOut := 5000;
                Execproc;

                result := parameters.ParamByName('@RETURN_VALUE').Value = 0;
                if not result then begin
                    Error := IntToStr(parameters.ParamByName('@RETURN_VALUE').Value) + ' ' + parameters.ParamByName('@DescripcionError').Value;
                    //dm.Base.RollbackTrans;
                    QueryExecute(DM.Base, 'ROLLBACK TRAN spRegistrarPagoSTD');
                    Exit;
                end;
                //Salgo del ciclo por Ok
                //dm.Base.CommitTrans;//Revision 3
                QueryExecute(DM.Base, 'COMMIT TRAN spRegistrarPagoSTD');
                Break;
            end;
        except
            on e: exception do begin
                //if dm.Base.InTransaction then dm.Base.RollbackTrans; //Revision 3
                QueryExecute(DM.Base, 'ROLLBACK TRAN spRegistrarPagoSTD');
                if (pos('deadlock', e.Message) > 0) then begin
                    mensajeDeadlock := e.message;
                    inc(Reintentos);
                    sleep(2500);
                end else begin
                    result := false;
                    error := e.Message;
                    exit; //Revision 3
                end;
            end;
        end;

    end;

    //una vez que se reintento 5 veces x deadlock lo informo
     if Reintentos = MAX_REINTENTOS then begin
           result := false;
           error := mensajeDeadLock;
           //if dm.Base.InTransaction then dm.Base.RollbackTrans; //Revision 3
           QueryExecute(DM.Base, 'ROLLBACK TRAN spRegistrarPagoSTD');
     end;

end;

function FormatearFecha(Valor: variant; formato: string = 'dd/mm/yy'; ValorSiNull: string = ''): string;
begin
    if VarIsNull(Valor) or not VarIsType(Valor, varDate) then begin
        result := ValorSiNull;
    end else begin
        result := formatdatetime(Formato, Valor);
    end;
end;

procedure CargarDatosTarifas(dm: TDMConveniosWeb; var Plantilla: string);
var
    Inicio, Repeticion, Fin, Temp: string;
begin
    with dm.spObtenerPreciosTramos do begin
        open;

        SepararPartes(Plantilla, 'RepeticionInicio', 'RepeticionFin', Inicio, Repeticion, Fin);

        Plantilla := Inicio;

        while not eof do begin
            Temp := Repeticion;

            Temp := ReplaceTag(Temp, 'CodigoTramo', fieldbyname('CodigoTramo').AsString);
            Temp := ReplaceTag(Temp, 'Descripcion', fieldbyname('Descripcion').AsString);
            Temp := ReplaceTag(Temp, 'Longitud', formatfloat('0.00', fieldbyname('Longitud').AsFloat));
            Temp := ReplaceTag(Temp, 'Normal', inttostr(fieldbyname('Normal').AsInteger div 100));
            Temp := ReplaceTag(Temp, 'Congestion', inttostr(fieldbyname('Congestion').AsInteger div 100));
            Temp := ReplaceTag(Temp, 'Punta', inttostr(fieldbyname('Punta').AsInteger div 100));

            Plantilla := Plantilla + Temp;
            next;
        end;

        Plantilla := Plantilla + Fin;

        close;
    end;
end;

function GenerarPDFComprobante(dm: TDMConveniosWeb; TipoComprobante: string;
  NroComprobante: int64; var StringPDF: string; var EsZip: boolean; var Error: string): boolean;
var
    f:TfrmReporteFactura;
    g:TReporteBoletaFacturaElectronicaForm;
    StringDetalle: string;
    TempDir: string;
    Archivo,
    Temp1, temp2: string;
    bEsComprobanteElectronico,bInicializar,bMostrarConsumoAParte : Boolean;
//    LineaComandos, Nada: string;
    Z: TZip;
begin
    try
        result := true; // SS_917_20101104
        try
            //--------------------------------------------------------------------
            with dm.spObtenerDatosComprobante do begin
                try
                    parameters.parambyname('@TipoComprobante').value := TipoComprobante;
                    parameters.parambyname('@NumeroComprobante').value := NroComprobante;
                    open;

                    if  (FieldByName('TipoComprobanteFiscal').AsString = TC_FACTURA_AFECTA) OR
                        (FieldByName('TipoComprobanteFiscal').AsString = TC_FACTURA_EXENTA) OR
                        (FieldByName('TipoComprobanteFiscal').AsString = TC_BOLETA_AFECTA) OR
                        (FieldByName('TipoComprobanteFiscal').AsString = TC_BOLETA_EXENTA) then
                    begin
                        g := TReporteBoletaFacturaElectronicaForm.create(nil);
                        bEsComprobanteElectronico:=True;
                    end
                    else
                    begin
                        f := TfrmReporteFactura.create(nil);
                        bEsComprobanteElectronico:=False;
                    end;
                finally
                    close;
                end;
            end;
            //---------------------------------------------------------------------
        except
            on e: exception do begin
                // SS_917_20101104
    //            raise(exception.create('Error al crear el form : ' + e.message));
                Result := False;
                Error := 'Error al crear el form : ' + e.message;
                // Fin Rev. SS_917_20101104
            end;
        end;

        if Result then // SS_917_20101104
            try
                if bEsComprobanteElectronico then bInicializar := g.Inicializar(dm.Base, TipoComprobante, NroComprobante, Error, True, True, oiWeb)
                else bInicializar := f.Inicializar( dm.Base, TipoComprobante, NroComprobante, Error, True, True);

                if bInicializar then begin

                    //if bEsComprobanteElectronico then result := g.EjecutarPDF(StringPDF, Error)   // SS_1332_CQU_20151106
                    //else result := f.EjecutarPDF(StringPDF, Error);                               // SS_1332_CQU_20151106
                    if bEsComprobanteElectronico then begin                                         // SS_1332_CQU_20151106
                        result := g.EjecutarPDF(StringPDF, Error);                                  // SS_1332_CQU_20151106
                        // Intento liberar la DBNet                                                 // SS_1332_CQU_20151106
                        try                                                                         // SS_1332_CQU_20151106
                            g.LiberarDBNet;                                                         // SS_1332_CQU_20151106
                        except on E : Exception do begin                                            // SS_1332_CQU_20151106
                                Error := 'GenerarPDFComprobante: [Interno]' + E.Message;            // SS_1332_CQU_20151106
                            end;                                                                    // SS_1332_CQU_20151106
                        end;                                                                        // SS_1332_CQU_20151106
                    end else result := f.EjecutarPDF(StringPDF, Error);                             // SS_1332_CQU_20151106

                    if result then begin

                        if bEsComprobanteElectronico then bMostrarConsumoAParte := g.MostrarConsumoAparte
                        else bMostrarConsumoAParte := f.MostrarConsumoAparte;

                        if not bMostrarConsumoAparte then begin
                            // esta todo listo
                            EsZip := false;
                        end else begin
                            EsZip := true;

                            if bEsComprobanteElectronico then Result := g.EjecutarDetallePDF(StringDetalle, Error)
                            else Result := f.EjecutarDetallePDF(StringDetalle, Error);

                            if Result then begin
                                Z := TZip.create(nil);
                                TempDir := GetTempDir;
                                if (length(TempDir) > 0) and (TempDir[length(TempDir)] = '\') then TempDir := copy(TempDir, 1, length(TempDir) - 1);
                                ForceDirectories(TempDir);
                                TempDir := TempDir + '\';
                                Temp1 := TempDir + 'Nota de cobro ' + inttostr(NroComprobante) + '.pdf' ;
                                Temp2 := TempDir + 'Anexo Nota de cobro ' + inttostr(NroComprobante) + '.pdf';
                                Archivo := TempDir + inttostr(NroComprobante) + '.zip' ;
                                StringToFile(StringPDF, Temp1);
                                StringToFile(StringDetalle, Temp2);

                                Z.ShowProgressDialog := false;
                                Z.Filename := Archivo;
                                Z.FileSpecList.Add(Temp1);
                                Z.FileSpecList.Add(Temp2);
                                if Z.Add = 2 then begin
                                    StringPDF := FileToString(Archivo);
                                end else begin
                                    Error := 'Error al agregar los archivos al zip.';
                                    result := false;
                                end;

        //                        LineaComandos := '"' + format(NK_MULTIPLE_COMANDO, ['"' + Archivo + '"', '"' + Temp1 + '"', '"' + Temp2 + '"']) + '"';
        //                        if Ejecutar(LineaComandos, nada, Error) then begin
        //                            StringPDF := FileToString(Archivo);
                                        // LineaComandos + CRLF + Nada; //
        //                            NK_MULTIPLE_EXTENSION := 'txt';
        //                        end else begin
        //                            result := false;
        //                        end;
                                DeleteFile(Temp1);
                                DeleteFile(Temp2);
                                DeleteFile(Archivo);
                                Z.Free;
                            end;
                        end;
                    end;
                end else begin
                    Result := False;
                    Error := 'GenerarPDFComprobante: Inicializar ' + Error;
                end;
                // si no inicializo, el error ya va cargado
            except
                on e: exception do begin
                    Result := False;
                    Error := 'GenerarPDFComprobante: [Interno]' + e.Message;
                end;
            end;
    finally
        if Assigned(g) then FreeAndNil(g);
        if Assigned(f) then FreeAndNil(f);
//        if bEsComprobanteElectronico then g.Release
//        else f.Release;
    end;
end;

function AveriguarUltimaFactura(dm: TDMConveniosWeb; CodigoConvenio: integer; var NroComprobante: int64; var Error: string): boolean;
var
    TipoComprobante: string;
begin
    result := false;
    with dm.spObtenerUltimoComprobante do begin
        parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        open;

        if not eof then begin
            TipoComprobante := fieldbyname('TipoComprobante').AsString;
            NroComprobante := fieldbyname('NumeroComprobante').AsInteger;
        end else begin
            close;
            Error := MSG_CONVENIO_SIN_NK;
            exit;
        end;

        close;
    end;

    result := True;
end;

function AveriguarUltimasFacturas(  dm: TDMConveniosWeb;                                                                                    //SS-983-NDR-20110929
                                    CodigoConvenio: integer;                                                                                //SS-983-NDR-20110929
                                    var TiposComprobantesFiscales:TstringList ;                                                             //SS-983-NDR-20110929
                                    var NrosComprobantesFiscales:TstringList ;                                                              //SS-983-NDR-20110929
                                    var NrosComprobantes:TstringList ;                                                                      //SS-983-NDR-20110929
                                    var FechasComprobantes:TstringList ;                                                                    //SS-983-NDR-20110929
                                    var MontosComprobantes:TstringList ;                                                                    //SS-983-NDR-20110929
                                    var ImprimirComprobantes:TStringList ;                                                                  //SS_1332_CQU_20151106
                                    var DetallesComprobantes:TStringList ;                                                                  //SS_1332_CQU_20151106
                                    var Error: string): boolean;                                                                            //SS-983-NDR-20110929
var                                                                                                                                         //SS-983-NDR-20110929
    TipoComprobante: string;                                                                                                                //SS-983-NDR-20110929
begin                                                                                                                                       //SS-983-NDR-20110929
    Result := false;                                                                                                                        //SS-983-NDR-20110929
    With dm.spObtenerUltimosComprobantes do begin                                                                                           //SS-983-NDR-20110929
        Close;                                                                                                                              //SS-983-NDR-20110929
        Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;                                                                  //SS-983-NDR-20110929
        Parameters.ParamByName('@Cuantos').Value        := 3;                                                                               //SS-983-NDR-20110929
        Open;                                                                                                                               //SS-983-NDR-20110929
        First;                                                                                                                              //SS-983-NDR-20110929
        if (Not Eof) then                                                                                                                   //SS-983-NDR-20110929
        begin                                                                                                                               //SS-983-NDR-20110929
          while (not eof) do begin                                                                                                          //SS-983-NDR-20110929
              TiposComprobantesFiscales.Add(FieldByName('TipoComprobanteFiscal').AsString);                      //SS-983-NDR-20110929
              NrosComprobantesFiscales.Add(Trim(IntToStr(FieldByName('NumeroComprobanteFiscal').AsInteger)));    //SS-983-NDR-20110929
              NrosComprobantes.Add(Trim(IntToStr(FieldByName('NumeroComprobante').AsInteger)));                  //SS-983-NDR-20110929
              FechasComprobantes.Add(FormatDateTime('dd"-"mm"-"yyyy',FieldByName('FechaEmision').AsDateTime));   //SS-983-NDR-20110929
              MontosComprobantes.Add(Trim(FieldByName('TotalComprobante').AsString));                           //SS_1147_MCA_20150416      //SS-983-NDR-20110929
              ImprimirComprobantes.Add(FieldByName('PuedeImprimir').AsString);                                  //SS_1332_CQU_20151106
              DetallesComprobantes.Add(FieldByName('ConDetalle').AsString);                                     //SS_1332_CQU_20151106
              Next;                                                                                                                         //SS-983-NDR-20110929
          end;                                                                                                                              //SS-983-NDR-20110929
        end                                                                                                                                 //SS-983-NDR-20110929
        else                                                                                                                                //SS-983-NDR-20110929
        begin                                                                                                                               //SS-983-NDR-20110929
            Close;                                                                                                                          //SS-983-NDR-20110929
            Error := MSG_CONVENIO_SIN_NK;                                                                                                   //SS-983-NDR-20110929
            exit;                                                                                                                           //SS-983-NDR-20110929
        end;                                                                                                                                //SS-983-NDR-20110929
        Close;                                                                                                                              //SS-983-NDR-20110929
    end;                                                                                                                                    //SS-983-NDR-20110929
    Result:=True;                                                                                                                           //SS-983-NDR-20110929
end;                                                                                                                                        //SS-983-NDR-20110929

function GenerarUltimaFactura(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio: integer; var NroComprobante: int64; var EsZip: boolean; var Error: string): boolean;
var
    TipoComprobante: string;
begin
    result := false;
    with dm.spObtenerUltimoComprobante do begin
        parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        open;

        if not eof then begin
            TipoComprobante := fieldbyname('TipoComprobante').AsString;
            NroComprobante := fieldbyname('NumeroComprobante').AsInteger;
        end else begin
            close;
            Error := MSG_CONVENIO_SIN_NK;
            exit;
        end;

        close;
    end;

    result := GenerarPDFComprobante(dm, TipoComprobante, NroComprobante, Plantilla, EsZip, Error);
end;

procedure CargarDatosCliente(dm: TDMConveniosWeb; var Plantilla: string; CodigoPersona: integer);
begin
    with dm.spObtenerDatosCliente do begin
        parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
        open;

        Plantilla := ReplaceTag(Plantilla, 'Apellido', FieldByName('Apellido').AsString);
        Plantilla := ReplaceTag(Plantilla, 'Nombre', FieldByName('Nombre').AsString);
        Plantilla := ReplaceTag(Plantilla, 'DomicilioPrincipal', FieldByName('DomicilioPrincipal').AsString);
        Plantilla := ReplaceTag(Plantilla, 'TelefonoParticular', FieldByName('TelefonoParticular').AsString);
        Plantilla := ReplaceTag(Plantilla, 'TelefonoComercial', FieldByName('TelefonoComercial').AsString);
        Plantilla := ReplaceTag(Plantilla, 'TelefonoCelular', FieldByName('TelefonoCelular').AsString);
        Plantilla := ReplaceTag(Plantilla, 'Email', FieldByName('Email').AsString);
        close;
    end;
end;


procedure CargarDatosEstadoDeCuenta(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio: integer);
begin
    with dm.spObtenerResumenDeudaConvenio do begin
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        open;


        Plantilla := ReplaceTag(Plantilla, 'FechaEstadoCuenta', formatdatetime('dd/mm/yy', now));

        Plantilla := ReplaceTag(Plantilla, 'DeudaPeaje', IntToStr(FieldByName('DeudaPeaje').AsVariant div 100));
        Plantilla := ReplaceTag(Plantilla, 'DeudaIntereses', IntToStr(FieldByName('DeudaIntereses').AsVariant div 100));
        Plantilla := ReplaceTag(Plantilla, 'DeudaDescuentos', IntToStr(FieldByName('DeudaDescuentos').AsVariant div 100));
        Plantilla := ReplaceTag(Plantilla, 'DeudaOtros', IntToStr(FieldByName('DeudaOtros').AsVariant div 100));
        Plantilla := ReplaceTag(Plantilla, 'DeudaTotal', IntToStr(
          (
          FieldByName('DeudaPeaje').AsVariant +
          FieldByName('DeudaIntereses').AsVariant +
          FieldByName('DeudaDescuentos').AsVariant +
          FieldByName('DeudaOtros').AsVariant
          ) div 100
        ));
        close;
    end;
end;

procedure CargarDatosReclamos(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio: integer);
var
    Estado,
    Inicio, Repeticion, Fin, Temp: string;
begin
    with dm.spReclamos do begin
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        open;

        // genero las repeticiones necesarias
        SepararPartes(Plantilla, '#RepeticionInicio#', '#RepeticionFin#', Inicio, Repeticion, Fin);
        Plantilla := Inicio;

        if eof then begin
                temp := repeticion;

                temp := ReplaceTag(temp, 'FechaHoraCreacion', 'Sin datos');
                temp := ReplaceTag(temp, 'DescripcionTipoOrdenServicio', '');
                temp := ReplaceTag(temp, 'Prioridad', '');
                temp := ReplaceTag(temp, 'Estado', '');
                temp := ReplaceTag(temp, 'FechaHoraFin', '');
                temp := ReplaceTag(temp, 'FechaCompromiso', '');

                Plantilla := Plantilla + temp;
        end else begin
            while not eof do begin
                temp := repeticion;

                temp := ReplaceTag(temp, 'FechaHoraCreacion', FormatearFecha(fieldbyname('FechaHoraCreacion').value));
                temp := ReplaceTag(temp, 'DescripcionTipoOrdenServicio', fieldbyname('DescripcionTipoOrdenServicio').asString);
                temp := ReplaceTag(temp, 'Prioridad', fieldbyname('Prioridad').asString);

                Estado := uppercase(fieldbyname('Estado').asString);
                temp := ReplaceTag(temp, 'Estado',
                  iif(Estado = 'C', 'Cancelado',
                  iif(Estado = 'P', 'Pendiente',
                  iif(Estado = 'T', 'Terminada', 'Desconocido')))
                );

                temp := ReplaceTag(temp, 'FechaHoraFin', FormatearFecha(fieldbyname('FechaHoraFin').value));
                temp := ReplaceTag(temp, 'FechaCompromiso', FormatearFecha(fieldbyname('FechaCompromiso').value));

                Plantilla := Plantilla + temp;
                next;
            end;
        end;

        Plantilla := Plantilla + Fin;

        close;
    end;
end;

procedure CargarDatosCartolaComprobantes(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, IndiceVehiculo, Comprobante: integer; FechaDesde, FechaHasta: TDateTime);
var
    Inicio, Repeticion, Fin, Temp: string;
    TotalComprobantes, TotalAPagares: double;
    UltimoComprobante : String;                                                                             //SS_1038_GVI_20120723
begin
    with dm.spWEB_CV_ObtenerUltimaNotaDeCobro do begin                                                       //SS_1038_GVI_20120723
        Parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);   //SS_1038_GVI_20120723
        Open;                                                                                                //SS_1038_GVI_20120723
          UltimoComprobante := fieldbyname('NumeroComprobante').asString;                                    //SS_1038_GVI_20120723
        Close;                                                                                               //SS_1038_GVI_20120723
    end;                                                                                                     //SS_1038_GVI_20120723

    with dm.spComprobantes do begin
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        open;

        if UltimoComprobante = '' then begin                                                                 //SS_1221_MBE_20141017
            UltimoComprobante := Fieldbyname('NumeroComprobante').asString;                                  //SS_1221_MBE_20141017
        end;                                                                                                 //SS_1221_MBE_20141017

        // genero las repeticiones necesarias
        SepararPartes(Plantilla, '#RepeticionInicio3#', '#RepeticionFin3#', Inicio, Repeticion, Fin);
        Plantilla := Inicio;

        TotalComprobantes := 0;
        TotalAPagares := 0;
        if eof then begin
                temp := repeticion;

                temp := ReplaceTag(temp, 'FechaEmision', 'Sin datos');
                temp := ReplaceTag(temp, 'TipoComprobante', '');
                temp := ReplaceTag(temp, 'NumeroComprobante', '');
                temp := ReplaceTag(temp, 'TipoComprobanteDesc', '');
                temp := ReplaceTag(temp, 'FechaVencimiento', '');
                temp := ReplaceTag(temp, 'TotalComprobante', '');
                temp := ReplaceTag(temp, 'TotalAPagar', '');
                temp := ReplaceTag(temp, 'EstadoPago', '');
                temp := ReplaceTag(temp, 'PeriodoInicial', '');
                temp := ReplaceTag(temp, 'PeriodoFinal', '');

                Plantilla := Plantilla + temp;
        end else begin
            while not eof do begin
                temp := repeticion;
                temp := ReplaceTag(temp, 'FechaEmision', formatdatetime('dd/mm/yy', fieldbyname('FechaEmision').asDateTime));
//                temp := ReplaceTag(temp, 'TipoComprobante', fieldbyname('TipoComprobante').asString);
//                temp := ReplaceTag(temp, 'TipoComprobanteDesc', fieldbyname('TipoComprobanteDesc').asString);
//                temp := ReplaceTag(temp, 'NumeroComprobante', fieldbyname('NumeroComprobante').asString);
//              Rev.8 / 15-04-2009 / Nelson Droguett Sierra ---------------------------------------------------------------------------------
                temp := ReplaceTag(temp, 'TipoComprobante', fieldbyname('TipoComprobanteFiscal').asString);
                temp := ReplaceTag(temp, 'TipoComprobanteDesc', fieldbyname('DescTipoComprobanteFiscal').asString);
                temp := ReplaceTag(temp, 'NumeroComprobante', fieldbyname('NumeroComprobanteFiscal').asString);
//              -----------------------------------------------------------------------------------------------------------------------------
                temp := ReplaceTag(temp, 'FechaVencimiento', FormatearFecha(fieldbyname('FechaVencimiento').Value));
                temp := ReplaceTag(temp, 'TotalComprobante', FormatFloat('#,##0', fieldbyname('TotalComprobante').asFloat));
                temp := ReplaceTag(temp, 'TotalAPagar', FormatFloat('#,##0', fieldbyname('TotalAPagar').asFloat));

                TotalComprobantes := TotalComprobantes + fieldbyname('TotalComprobante').asFloat;
                TotalAPagares := TotalAPagares + fieldbyname('TotalAPagar').asFloat;

                if (uppercase(fieldbyname('TipoComprobante').asString) = 'NK') and (fieldbyname('Pagable').asInteger = 1) then begin

                    temp := ReplaceTag(temp, 'EstadoPago', fieldbyname('EstadoPago').asString +
                      ' <A HREF="javascript:Pagar(' +
                      '''' + fieldbyname('TipoComprobante').asString + ''', ' +
//                      '''' + fieldbyname('NumeroComprobante').asString + ''', ' +            //SS_1038_GVI_20120723: Se Comenta esta linea  ya que se utilizara el ultimo comprobante
                      '''' + UltimoComprobante + ''',' +                                       //SS_1038_GVI_20120723: Se Agrega linea con el ultimo comprobante del convenio
                      '''' + FormatFloat('0', fieldbyname('TotalAPagar').asFloat * 100) + '''' +
                      ');">[' + MSG_LINK_WEBPAY + ']</A>'
                      );
                end else begin
                    temp := ReplaceTag(temp, 'EstadoPago', fieldbyname('EstadoPago').asString);
                end;

                temp := ReplaceTag(temp, 'PeriodoInicial', FormatearFecha(fieldbyname('PeriodoInicial').value));
                temp := ReplaceTag(temp, 'PeriodoFinal', FormatearFecha(fieldbyname('PeriodoFinal').value));

                Plantilla := Plantilla + temp;
                next;
            end;
        end;

        Plantilla := Plantilla + Fin;

        Plantilla := ReplaceTag(Plantilla, 'TotalComprobantes', FormatFloat('#,##0', TotalComprobantes));
        Plantilla := ReplaceTag(Plantilla, 'TotalAPagares', FormatFloat('#,##0', TotalAPagares));

        close;
    end;
end;
{-------------------------------------------------------------------------------
  Revision 1
  Lgisuk
  14/11/06
  ahora la busqueda ya no esta acotada a 50 registros sino que el
usuario puede elegir cuantos registros desea visualizar.
Revision : 2
    Author : lcanteros
    Date : 18/06/2008
    Description : Modificaciones (SS 675)
------------------------------------------------------------------------------- }
procedure CargarDatosCartolaTransacciones(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, IndiceVehiculo, Comprobante, Cantidad, CodigoConcesionaria: integer; FechaDesde, FechaHasta: TDateTime; var PlantillacsvTransacciones :string);// Rev 8.  SS_917
resourcestring
    MSG_TRANSITOS = 'Se muestran %d Transitos de %d';
var
    Inicio, Repeticion, Fin, Temp: string;
    kms, total: double; TotalRegistros: integer;
    CantidadAMostrar :integer; // Rev 8.  SS_917
    FecIniNK, FecFinNK: string;
    error:string;  // Rev 8.  SS_917
    CantidadTransitos :Integer;  // Rev 8.  SS_917
begin
    PlantillacsvTransacciones:= '';
    with dm.spTransitos do begin
		//Rev.3 / 22-Junio-2010 / Nelson Droguett Sierra
        parameters.Refresh;
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        parameters.ParamByName('@IndiceVehiculo').Value := iif(IndiceVehiculo = -1, NULL, IndiceVehiculo);
       // parameters.ParamByName('@Comprobante').Value := iif(Comprobante = 0, NULL, Comprobante); // Rev 8.  SS_917

        if (FechaDesde = NULLDATE) then
            parameters.ParamByName('@FechaDesde').Value := NULL
        else
            parameters.ParamByName('@FechaDesde').Value := FechaDesde;

        if (FechaHasta = NULLDATE) then
            parameters.ParamByName('@FechaHasta').Value := NULL
        else
            parameters.ParamByName('@FechaHasta').Value := FechaHasta;

        //parameters.ParamByName('@Cantidad').Value := Cantidad;  // Rev 8.  SS_917
        //Rev.3 / 22-Junio-2010 / Nelson Droguett Sierra ----------------------------
        if CodigoConcesionaria>0 then
           parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;

        open;
        // Rev 8.  SS_917
        CantidadTransitos:= RecordCount;
        DatasetToExcelCSV(dm.SpTransitos, PlantillacsvTransacciones, error );
        dm.SpTransitos.First;
        // END Rev 8.  SS_917

        kms := 0;
        Total := 0;
        CantidadAMostrar:= 0;   // Rev 8.  SS_917

        // genero las repeticiones necesarias
        SepararPartes(Plantilla, '#RepeticionInicio2#', '#RepeticionFin2#', Inicio, Repeticion, Fin);
        Plantilla := Inicio;

        if eof then begin
                temp := repeticion;

                temp := ReplaceTag(temp, 'FechaHora', 'Sin datos');
		        //Rev.3 / 22-Junio-2010 / Nelson Droguett Sierra ----------------------------
                temp := ReplaceTag(temp, 'Concesionaria', '');
                temp := ReplaceTag(temp, 'PuntoCobro', '');
                temp := ReplaceTag(temp, 'Patente', '');
                temp := ReplaceTag(temp, 'Categoria', '');
                temp := ReplaceTag(temp, 'Etiqueta', '');
                temp := ReplaceTag(temp, 'TotalKms', '');
                temp := ReplaceTag(temp, 'TotalTransacciones', '');
                temp := ReplaceTag(temp, 'Kms', '');
                temp := ReplaceTag(temp, 'Importe', '');
                temp := ReplaceTag(temp, 'Nota', '');
                temp := ReplaceTag(temp, 'TipoComprobante', '');
                temp := ReplaceTag(temp, 'NumeroComprobante', '');
                temp := ReplaceTag(temp, 'TipoComprobanteDesc', '');
                temp := ReplaceTag(temp, 'IDTransito', 'Vacio');

                Plantilla := Plantilla + temp;
                Plantilla := ReplaceTag(Plantilla, 'FechaDesde', formatDateTime('dd"/"mm"/"yyyy', FechaDesde));
                Plantilla := ReplaceTag(Plantilla, 'FechaHasta', formatDateTime('dd"/"mm"/"yyyy', FechaHasta));
        end else begin
            TotalRegistros := RecordCount;
            FecIniNK := formatdatetime('dd/mm/yyyy', fieldbyname('FechaHora').asDateTime);
            while (not eof) and (Cantidad <> CantidadAMostrar) do begin   // Rev 8.  SS_917
                temp := repeticion;
                //temp := ReplaceTag(temp, 'IDTransito', fieldbyname('NumCorrCA').asString);                                        //SS_1397_MGO_20151008
                temp := ReplaceTag(temp, 'FechaHora', formatdatetime('dd/mm/yy hh:nn', fieldbyname('FechaHora').asDateTime));
                //temp := ReplaceTag(temp, 'PuntoCobro', fieldbyname('PuntoCobro').asString);                                       //SS_1397_MGO_20151008
                temp := ReplaceTag(temp, 'PuntoCobro', fieldbyname('Portico').asString);                                            //SS_1397_MGO_20151008
                temp := ReplaceTag(temp, 'Patente', fieldbyname('Patente').asString);
                temp := ReplaceTag(temp, 'Categoria', fieldbyname('Categoria').asString);
                //temp := ReplaceTag(temp, 'Etiqueta', fieldbyname('Etiqueta').asString); // televia                                //SS_1397_MGO_20151008
                temp := ReplaceTag(temp, 'Etiqueta', fieldbyname('Tag').asString); // televia                                       //SS_1397_MGO_20151008
                temp := ReplaceTag(temp, 'Kms', formatfloat('0.00', fieldbyname('Kms').AsFloat));
                temp := ReplaceTag(temp, 'Importe', FormatFloat('#,##0.00', fieldbyname('Importe').asFloat));
                //temp := ReplaceTag(temp, 'TipoComprobante', fieldbyname('TipoComprobante').asString);
                //temp := ReplaceTag(temp, 'TipoComprobanteDesc', fieldbyname('TipoComprobanteDesc').asString);
                //temp := ReplaceTag(temp, 'NumeroComprobante', fieldbyname('NumeroComprobante').asString);
                // Rev. 10 / 30-06-2009 / Nelson Droguett Sierra
                temp := ReplaceTag(temp, 'TipoComprobante', '');
                //temp := ReplaceTag(temp, 'TipoComprobanteDesc', fieldbyname('DescTipoComprobanteFiscal').asString);               //SS_1397_MGO_20151008
                temp := ReplaceTag(temp, 'TipoComprobanteDesc', fieldbyname('Comprobante').asString);                               //SS_1397_MGO_20151008
                temp := ReplaceTag(temp, 'NumeroComprobante', '');

		        //Rev.3 / 22-Junio-2010 / Nelson Droguett Sierra ----------------------------
                //temp := ReplaceTag(temp, 'Concesionaria', fieldbyname('NombreCorto').asString);                                   //SS_1397_MGO_20151008
                temp := ReplaceTag(temp, 'Concesionaria', fieldbyname('Eje').asString);                                             //SS_1397_MGO_20151008

                kms := kms + fieldbyname('Kms').AsFloat;
                Total := Total + fieldbyname('Importe').asFloat;

                Plantilla := Plantilla + temp;

                if TotalRegistros = RecNo then FecFinNK := formatdatetime('dd/mm/yyyy', fieldbyname('FechaHora').asDateTime);

                next;
                CantidadAMostrar:= CantidadAMostrar +1;  // Rev 8.  SS_917
            end;

            // muestro las fechas por las que se filtr�
            Plantilla := ReplaceTag(Plantilla, 'FechaDesde', FormatDateTime('dd"/"mm"/"yyyy', FechaDesde));
            Plantilla := ReplaceTag(Plantilla, 'FechaHasta', FormatDateTime('dd"/"mm"/"yyyy', FechaHasta));
        end;

        Plantilla := Plantilla + Fin;

        Plantilla := ReplaceTag(Plantilla, 'TotalKms', formatfloat('0.00', Kms));
        Plantilla := ReplaceTag(Plantilla, 'TotalTransacciones', FormatFloat('#,##0.00', Total));

        Plantilla := ReplaceTag(Plantilla, 'Nota', '');

         // Rev 8.  SS_917
        if ( CantidadTransitos > Cantidad) then begin
            Plantilla := ReplaceTag(Plantilla, 'MensajeTransitos',Format(MSG_TRANSITOS, [Cantidad, CantidadTransitos]));
        end else Plantilla := ReplaceTag(Plantilla, 'MensajeTransitos','');
         // END Rev 8.  SS_917
        close;
    end;
end;


procedure CargarDatosCartolaEstacionamientos(dm: TDMConveniosWeb;
                                             var Plantilla: string;
                                             CodigoConvenio, CodigoConcesionaria, IndiceVehiculo: integer;
                                             FechaDesde, FechaHasta: TDateTime);
var
    Inicio, Repeticion, Fin, Temp: string;
    kms, total: double; TotalRegistros: integer;
    FecIniNK, FecFinNK: string;
begin
    with dm.spEstacionamientos do begin
        parameters.Refresh;
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        parameters.ParamByName('@IndiceVehiculo').Value := iif(IndiceVehiculo = -1, NULL, IndiceVehiculo);

        if (FechaDesde = NULLDATE) then
            parameters.ParamByName('@FechaDesde').Value := NULL
        else
            parameters.ParamByName('@FechaDesde').Value := StartOfTheDay(FechaDesde);

        if (FechaHasta = NULLDATE) then
            parameters.ParamByName('@FechaHasta').Value := NULL
        else
            parameters.ParamByName('@FechaHasta').Value := EndOfTheDay(FechaHasta);

        if CodigoConcesionaria>0 then
           parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria
        else
           parameters.ParamByName('@CodigoConcesionaria').Value := NULL;

        open;

        kms := 0;
        Total := 0;

        // genero las repeticiones necesarias
        SepararPartes(Plantilla, '#RepeticionInicio2#', '#RepeticionFin2#', Inicio, Repeticion, Fin);
        Plantilla := Inicio;

        if eof then begin
                temp := repeticion;
                temp := ReplaceTag(temp, 'Patente', 'Sin datos');
                temp := ReplaceTag(temp, 'Concesionaria', '');
                temp := ReplaceTag(temp, 'LugarEntrada', '');
                temp := ReplaceTag(temp, 'FHE', '');
                temp := ReplaceTag(temp, 'LugarSalida', '');
                temp := ReplaceTag(temp, 'FHS', '');
                temp := ReplaceTag(temp, 'Monto', '');
                temp := ReplaceTag(temp, 'Monto', '');
                // SS_1006_1015_PDO_20121113 - Inicio bloque
                temp := ReplaceTag(temp, 'Categoria', '');
                temp := ReplaceTag(temp, 'Televia', '');
                temp := ReplaceTag(temp, 'Comprobante', '');
                // SS_1006_1015_PDO_20121113 - Fin bloque

                Plantilla := Plantilla + temp;
                Plantilla := ReplaceTag(Plantilla, 'FechaDesde', formatDateTime('dd"/"mm"/"yyyy', FechaDesde));
                Plantilla := ReplaceTag(Plantilla, 'FechaHasta', formatDateTime('dd"/"mm"/"yyyy', FechaHasta));
        end else begin
            TotalRegistros := RecordCount;
            while not eof do begin
                temp := repeticion;
                temp := ReplaceTag(temp, 'Patente', fieldbyname('Patente').asString);
                temp := ReplaceTag(temp, 'Concesionaria', fieldbyname('ConcesionariaDescr').asString);
                temp := ReplaceTag(temp, 'LugarEntrada', fieldbyname('EntradaDescr').asString);
                temp := ReplaceTag(temp, 'FHE', iif(fieldbyname('FechaHoraEntrada').IsNull, '', FormatDateTime('dd/mm/yyy hh:nn', fieldbyname('FechaHoraEntrada').asDatetime)));
                temp := ReplaceTag(temp, 'LugarSalida', fieldbyname('SalidaDescr').asString);
                temp := ReplaceTag(temp, 'FHS', iif(fieldbyname('FechaHoraSalida').IsNull, '', FormatDateTime('dd/mm/yyy hh:nn', fieldbyname('FechaHoraSalida').asDatetime)));
                temp := ReplaceTag(temp, 'Monto', FormatFloat(FORMATO_IMPORTE, fieldbyname('Importe').asFloat));
                // SS_1006_1015_PDO_20121113 - Inicio bloque
                temp := ReplaceTag(temp, 'Categoria', fieldbyname('Categoria').asString);
                temp := ReplaceTag(temp, 'Televia', fieldbyname('Etiqueta').asString);
                temp := ReplaceTag(temp, 'Comprobante', fieldbyname('DescTipoComprobanteFiscal').asString);
                // SS_1006_1015_PDO_20121113 - Fin bloque

                Plantilla := Plantilla + temp;
                next;
            end;
            // muestro las fechas por las que se filtr�
            Plantilla := ReplaceTag(Plantilla, 'FechaDesde', FormatDateTime('dd"/"mm"/"yyyy', FechaDesde));
            Plantilla := ReplaceTag(Plantilla, 'FechaHasta', FormatDateTime('dd"/"mm"/"yyyy', FechaHasta));
        end;

        Plantilla := Plantilla + Fin;
        close;
    end;
end;




procedure CargarDatosCartolaPagos(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, IndiceVehiculo, Comprobante: integer; FechaDesde, FechaHasta: TDateTime);
var
    Inicio, Repeticion, Fin, Temp: string;
begin
    with dm.spPagos do begin
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        open;

        // genero las repeticiones necesarias
        SepararPartes(Plantilla, '#RepeticionInicio4#', '#RepeticionFin4#', Inicio, Repeticion, Fin);
        Plantilla := Inicio;

        if eof then begin
                temp := repeticion;

                temp := ReplaceTag(temp, 'FechaHora', 'Sin datos');
                temp := ReplaceTag(temp, 'Importe', '');
                temp := ReplaceTag(temp, 'SaldoAnterior', '');
                temp := ReplaceTag(temp, 'SaldoActual', '');
                temp := ReplaceTag(temp, 'MedioPago', '');

                Plantilla := Plantilla + temp;
        end else begin
            while not eof do begin
                temp := repeticion;

                temp := ReplaceTag(temp, 'FechaHora', FormatearFecha(fieldbyname('FechaHora').value));
                temp := ReplaceTag(temp, 'Importe', FormatFloat('#,##0', fieldbyname('Importe').asFloat / 100));
                temp := ReplaceTag(temp, 'SaldoAnterior', FormatFloat('#,##0', fieldbyname('SaldoAnterior').asFloat / 100));
                temp := ReplaceTag(temp, 'SaldoActual', FormatFloat('#,##0', fieldbyname('SaldoActual').asFloat / 100));
                temp := ReplaceTag(temp, 'MedioPago', fieldbyname('MedioPago').asString);

                Plantilla := Plantilla + temp;
                next;
            end;
        end;

        Plantilla := Plantilla + Fin;

        close;
    end;
end;


{******************************** Function Header ******************************
Function Name: CargarSaldosConvenios
Author : nefernandez
Date Created : 22/05/2008
Description : SS 698: Genera los registros de convenios con su saldo para un RUT
*******************************************************************************}
function CargarSaldosConvenios(dm: TDMConveniosWeb; var Plantilla: string; CodigoPersona: integer): boolean;
const
    WEB_SALDOxCONVENIOxRUT_TO = 'WEB_SALDOxCONVENIOxRUT_TO';
var
    Inicio, Repeticion, Fin, Temp: string;
    TotalSaldo: Double;
    TimeOutSaldoConvenio: Integer;
begin

    if ObtenerParametroGeneral(dm.Base, WEB_SALDOxCONVENIOxRUT_TO, TimeOutSaldoConvenio) then begin
        dm.spObtenerSaldosDeConveniosxRUT.CommandTimeout := TimeOutSaldoConvenio;
    end;

    with dm.spObtenerSaldosDeConveniosxRUT do begin
        parameters.ParamByName('@CodigoCliente').Value := CodigoPersona;
        open;

        if eof then begin
            result := False; //no tiene convenios
            close;
            exit;
        end;

        SepararPartes(Plantilla, '#RepeticionInicio1#', '#RepeticionFin1#', Inicio, Repeticion, Fin);
        Plantilla := Inicio;

        TotalSaldo := 0;
        while not eof do begin
            temp := repeticion;

            temp := ReplaceTag(temp, 'Convenio', fieldbyname('NumeroConvenio').asString);
            temp := ReplaceTag(temp, 'Saldo', FormatFloat('#,##0', fieldbyname('SaldoConvenio').asFloat / 100));

            Plantilla := Plantilla + temp;

            TotalSaldo := TotalSaldo + fieldbyname('SaldoConvenio').asFloat;

            next;
        end;

        Plantilla := Plantilla + Fin;

        Plantilla := ReplaceTag(Plantilla, 'TotalSaldo', FormatFloat('#,##0', TotalSaldo / 100));

        close;
        result := true;
    end;
end;

{******************************** Function Header ******************************
Function Name: CargarDatosUltimosMovimiento
Author :
Date Created :
Description :
Parameters : dm: TDMConveniosWeb; Request: TWEBRequest; var Plantilla: string; CodigoConvenio, IndiceVehiculo: integer
Return Value : None

Revision 1:
    Author : ggomez
    Date : 18/01/2006
    Description : Modifiqu� para que en el combo muestre "Seleccione una patente"
        en lugar de "Todos" y para que el SP se ejecute s�lo si se ha
        seleccionado una patente.

*******************************************************************************}
procedure CargarDatosUltimosMovimiento(dm: TDMConveniosWeb; Request: TWEBRequest; var Plantilla: string; CodigoConvenio, IndiceVehiculo: integer);
resourcestring
    MSG_SELECCIONE_PATENTE = 'Seleccione una patente';
var
        Inicio, Repeticion, Fin, Temp: string;
begin
    CargarDatosDomicilioConvenio(dm, plantilla, CodigoConvenio);

    Plantilla := ReplaceTag(Plantilla, 'DescConvenio', ObtenerDescripcionConvenio(dm, CodigoConvenio));
    Plantilla := ReplaceTag(Plantilla, 'IndiceVehiculoValores',
                                            StringReplace(
                                                GenerarComboVehiculosConvenio(dm, CodigoConvenio, inttostr(IndiceVehiculo)),
                                                'Todos',
                                                MSG_SELECCIONE_PATENTE,
                                                [rfReplaceAll]));
    //Rev.3 / 22-Junio-2010 / Nelson Droguett Sierra---------------------------------------------------------------------------------------------------------------------------
    Plantilla := ReplaceTag(Plantilla, 'ConcesionariaValores', GenerarComboConcesionarias(dm,'1'));

    with dm.spObtenerUltimosConsumos do begin
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        parameters.ParamByName('@IndiceVehiculo').Value := iif(IndiceVehiculo = -1, NULL, IndiceVehiculo);
        // Si seleccion� una patente, abrir el SP.
        if IndiceVehiculo > -1 then begin
            Open;
        end;

        // genero las repeticiones necesarias
        SepararPartes(Plantilla, '#RepeticionInicio#', '#RepeticionFin#', Inicio, Repeticion, Fin);
        Plantilla := Inicio;

        // Si NO seleccion� una patente, mostrar el texto de seleccionar una patente.
        if IndiceVehiculo = -1 then begin
                temp := repeticion;
                temp := ReplaceTag(temp, 'Concepto', MSG_SELECCIONE_PATENTE);
                temp := ReplaceTag(temp, 'Importe', '');
                Plantilla := Plantilla + temp;
        end else begin
            if eof then begin
                    temp := repeticion;
                    temp := ReplaceTag(temp, 'Concepto', 'No hay consumos registrados.');
                    temp := ReplaceTag(temp, 'Importe', '');
                    Plantilla := Plantilla + temp;
            end else begin
                while not eof do begin
                    temp := repeticion;
                    temp := ReplaceTag(temp, 'Concepto', fieldbyname('Concepto').asString);
                    temp := ReplaceTag(temp, 'Importe', FormatFloat('#,##0.00', fieldbyname('Importe').asFloat));
                    Plantilla := Plantilla + temp;
                    next;
                end;
            end;
        end; // else if IndiceVehiculo = -1

        Plantilla := Plantilla + Fin;

        close;
    end;

    with dm.spObtenerUltimosConsumosDetalle do begin
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        parameters.ParamByName('@IndiceVehiculo').Value := iif(IndiceVehiculo = -1, NULL, IndiceVehiculo);
        // Si seleccion� una patente, abrir el SP.
        if IndiceVehiculo > -1 then begin
            Open;
        end;

        // genero las repeticiones necesarias
        SepararPartes(Plantilla, '#RepeticionInicio2#', '#RepeticionFin2#', Inicio, Repeticion, Fin);
        Plantilla := Inicio;

        // Si NO seleccion� una patente, mostrar el texto de seleccionar una patente.
        if IndiceVehiculo = -1 then begin
                temp := repeticion;
                temp := ReplaceTag(temp, 'Fecha', MSG_SELECCIONE_PATENTE);
                temp := ReplaceTag(temp, 'Hora', '');
                temp := ReplaceTag(temp, 'Patente', '');
                temp := ReplaceTag(temp, 'Horario', '');
                temp := ReplaceTag(temp, 'PuntoCobro', '');
                temp := ReplaceTag(temp, 'Categoria', '');
                temp := ReplaceTag(temp, 'Importe', '');
                //Rev.3 / 02-Julio-2010 / Nelson Droguett Sierra --------------------
                temp := ReplaceTag(temp, 'Concesionaria', '');
                //FinRev.3-----------------------------------------------------------
                Plantilla := Plantilla + temp;
        end else begin
            if eof then begin
                    temp := repeticion;
                    temp := ReplaceTag(temp, 'Fecha', 'No hay tr�nsitos');
                    temp := ReplaceTag(temp, 'Hora', '');
                    temp := ReplaceTag(temp, 'Patente', '');
                    temp := ReplaceTag(temp, 'Horario', '');
                    temp := ReplaceTag(temp, 'PuntoCobro', '');
                    temp := ReplaceTag(temp, 'Categoria', '');
                    temp := ReplaceTag(temp, 'Importe', '');
                    //Rev.3 / 02-Julio-2010 / Nelson Droguett Sierra --------------------
                    temp := ReplaceTag(temp, 'Concesionaria', '');
                    //FinRev.3-----------------------------------------------------------
                    Plantilla := Plantilla + temp;
            end else begin
                while not eof do begin
                    temp := repeticion;
                    temp := ReplaceTag(temp, 'Fecha', formatdatetime('dd/mm/yyyy', fieldbyname('FechaHora').asDateTime));
                    temp := ReplaceTag(temp, 'Hora', formatdatetime('hh:nn', fieldbyname('FechaHora').asDateTime));
                    temp := ReplaceTag(temp, 'Patente', fieldbyname('Patente').asString);
                    temp := ReplaceTag(temp, 'Horario', fieldbyname('TipoHorario').asString);
                    temp := ReplaceTag(temp, 'PuntoCobro', fieldbyname('PuntoCobro').asString);
                    temp := ReplaceTag(temp, 'Categoria', fieldbyname('Categoria').asString);
                    temp := ReplaceTag(temp, 'Importe', FormatFloat('#,##0.00', fieldbyname('Importe').asFloat));
                    //Rev.3 / 02-Julio-2010 / Nelson Droguett Sierra --------------------
                    temp := ReplaceTag(temp, 'Concesionaria', fieldbyname('NombreCorto').asString);
                    //FinRev.3-----------------------------------------------------------
                    Plantilla := Plantilla + temp;
                    next;
                end;
            end;
        end; // else if IndiceVehiculo = -1

        Plantilla := Plantilla + Fin;

        close;
    end;

end;
{-----------------------------------------------------------------------------
  Procedure: CargarDatosUltimosMovimientoVehiculo
  Author:    lcanteros
  Date:      10-Abr-2008
  Arguments: dm: TDMConveniosWeb; Request: TWEBRequest; var Plantilla: string; CodigoConvenio, IndiceVehiculo: integer
  Result:    None
  Purpose:   Carga los transitos no facturados filtrando por codigo de vehiculo y no
  con indicevehiculo como estaba antes

    Revision    : 5
    Date        : 28-Abril-2011
    Author      : Eduardo Baeza Ortega
    Firma       : PAR00133_EBA_20110419
    Description : Fase 2 - Pagina Web Convenios
                 Se cambia la forma de mostrar las concesionarias, s�lo se mostrar�n
                 las que tengan itemscuentas relacionados al convenio
-----------------------------------------------------------------------------}
procedure CargarDatosUltimosMovimientosVehiculo(dm: TDMConveniosWeb; Request: TWEBRequest; var Plantilla: string; CodigoConvenio, IndiceVehiculo, Concesionaria: integer);
resourcestring
    MSG_SELECCIONE_PATENTE = 'Seleccione una patente';
var
        Inicio, Repeticion, Fin, Temp: string;
        HayConsumos :boolean;
        TipoConcesionaria: integer;
begin

    HayConsumos := false;
    CargarDatosDomicilioConvenio(dm, plantilla, CodigoConvenio);

    Plantilla := ReplaceTag(Plantilla, 'DescConvenio', ObtenerDescripcionConvenio(dm, CodigoConvenio));
    {Plantilla := ReplaceTag(Plantilla, 'IndiceVehiculoValores',
                                            StringReplace(
                                                GenerarComboVehiculosConvenio(dm, CodigoConvenio, inttostr(IndiceVehiculo)),
                                                'Todos',
                                                MSG_SELECCIONE_PATENTE,
                                                [rfReplaceAll]));}

    Plantilla := ReplaceTag(Plantilla, 'IndiceVehiculoValores',
                                                GenerarComboVehiculosConvenioSinDuplicados(dm, CodigoConvenio, inttostr(IndiceVehiculo)));

    //-------------------------------------------
    // Inicio bloque PAR00133_EBA_20110419
    //-------------------------------------------

	//Rev.3 / 22-Junio-2010 / Nelson Droguett Sierra --------------------------------                                                 
//    Plantilla := ReplaceTag(Plantilla, 'ConcesionariaValores',
//                                                GenerarComboConcesionarias(dm, IntToStr(Concesionaria)));

    Plantilla := ReplaceTag(Plantilla, 'ConcesionariaValores',
                                                GenerarComboConcesionariasAsociadasItemsConvenio(dm, IntToStr(Concesionaria), CodigoConvenio, IndiceVehiculo));

    TipoConcesionaria := 1;
    with DM.spObtenerConcesionariasAsociadasItemsConvenio do begin
        Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        Parameters.ParamByName('@CodigoConcesionaria').Value := iif(Concesionaria = -1, NULL, Concesionaria) ;
        Open;
        if not eof then begin
            First;
            TipoConcesionaria := FieldByName('TipoConcesionaria').asInteger;
        end;
        Close;
    end;
    if TipoConcesionaria = 2 then begin
       Plantilla := ObtenerPlantilla(DM, Request, 'UltimosConsumosEstacionamiento');
       CargarDatosUltimosMovimientosVehiculoEstacionamiento(DM, Request, Plantilla, CodigoConvenio, IndiceVehiculo, Concesionaria);
    end
    else begin
    //-------------------------------------------
    // fin bloque PAR00133_EBA_20110419
    //-------------------------------------------

        with dm.spObtenerUltimosConsumosVehiculo do begin
            parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
            parameters.ParamByName('@CodigoVehiculo').Value := iif(IndiceVehiculo = -1, NULL, IndiceVehiculo);
    	    //Rev.3 / 22-Junio-2010 / Nelson Droguett Sierra --------------------------------
            parameters.ParamByName('@CodigoConcesionaria').Value := iif(Concesionaria = -1, NULL, Concesionaria);
            // Si seleccion� una patente, abrir el SP.
            if IndiceVehiculo > -1 then begin
                Open;
            end;

            // genero las repeticiones necesarias
            SepararPartes(Plantilla, '#RepeticionInicio#', '#RepeticionFin#', Inicio, Repeticion, Fin);
            Plantilla := Inicio;

            // Si NO seleccion� una patente, mostrar el texto de seleccionar una patente.
            if IndiceVehiculo = -1 then begin
                temp := repeticion;
                temp := ReplaceTag(temp, 'Concepto', MSG_SELECCIONE_PATENTE);
                temp := ReplaceTag(temp, 'Importe', '');
                Plantilla := Plantilla + temp;
            end else begin
                if eof then begin
                    temp := repeticion;
                    temp := ReplaceTag(temp, 'Concepto', 'No hay consumos registrados. ');
                    temp := ReplaceTag(temp, 'Importe', '');
                    Plantilla := Plantilla + temp;
                end else begin
                    while not eof do begin
                        temp := repeticion;
                        temp := ReplaceTag(temp, 'Concepto', fieldbyname('Concepto').asString);
                        temp := ReplaceTag(temp, 'Importe', FormatFloat('#,##0.00', fieldbyname('Importe').asFloat));
                        Plantilla := Plantilla + temp;
                        next;
                    end;
                    HayConsumos := true;
                end;
            end; // else if IndiceVehiculo = -1

            Plantilla := Plantilla + Fin;

            close;
        end;

        with dm.spObtenerUltimosConsumosDetalleVehiculo do begin
            parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
            parameters.ParamByName('@CodigoVehiculo').Value := iif(IndiceVehiculo = -1, NULL, IndiceVehiculo);
    	    //Rev.3 / 22-Junio-2010 / Nelson Droguett Sierra --------------------------------
            parameters.ParamByName('@CodigoConcesionaria').Value := iif(Concesionaria = -1, NULL, Concesionaria);

            // Si seleccion� una patente, abrir el SP.
            if (IndiceVehiculo > -1) and HayConsumos then begin
                Open;
                HayConsumos := RecordCount > 0;
            end;

            // genero las repeticiones necesarias
            SepararPartes(Plantilla, '#RepeticionInicio2#', '#RepeticionFin2#', Inicio, Repeticion, Fin);
            Plantilla := Inicio;

            // Si NO seleccion� una patente, mostrar el texto de seleccionar una patente.
            if IndiceVehiculo = -1 then begin
                temp := repeticion;
                temp := ReplaceTag(temp, 'Fecha', MSG_SELECCIONE_PATENTE);
                temp := ReplaceTag(temp, 'Hora', '');
                temp := ReplaceTag(temp, 'Patente', '');
                temp := ReplaceTag(temp, 'Horario', '');
                temp := ReplaceTag(temp, 'PuntoCobro', '');
                temp := ReplaceTag(temp, 'Categoria', '');
                //Rev.3 / 02-Julio-2010 / Nelson Droguett Sierra --------------------
                temp := ReplaceTag(temp, 'Concesionaria', '');
                //FinRev.3-----------------------------------------------------------
                temp := ReplaceTag(temp, 'Importe', '');
                Plantilla := Plantilla + temp;
            end else begin
                if not HayConsumos then begin
                    temp := repeticion;
                    temp := ReplaceTag(temp, 'Fecha', 'No hay tr�nsitos');
                    temp := ReplaceTag(temp, 'Hora', '');
                    temp := ReplaceTag(temp, 'Patente', '');
                    temp := ReplaceTag(temp, 'Horario', '');
                    temp := ReplaceTag(temp, 'PuntoCobro', '');
                    temp := ReplaceTag(temp, 'Categoria', '');
                    //Rev.3 / 02-Julio-2010 / Nelson Droguett Sierra --------------------
                    temp := ReplaceTag(temp, 'Concesionaria', '');
                    //FinRev.3-----------------------------------------------------------
                    temp := ReplaceTag(temp, 'Importe', '');
                    Plantilla := Plantilla + temp;
                end else begin
                    First;
                    while not eof do begin
                        temp := repeticion;
                        temp := ReplaceTag(temp, 'Fecha', formatdatetime('dd/mm/yyyy', fieldbyname('FechaHora').asDateTime));
                        temp := ReplaceTag(temp, 'Hora', formatdatetime('hh:nn', fieldbyname('FechaHora').asDateTime));
                        temp := ReplaceTag(temp, 'Patente', fieldbyname('Patente').asString);
                        temp := ReplaceTag(temp, 'Horario', fieldbyname('TipoHorario').asString);
                        temp := ReplaceTag(temp, 'PuntoCobro', fieldbyname('PuntoCobro').asString);
                        temp := ReplaceTag(temp, 'Categoria', fieldbyname('Categoria').asString);
                        temp := ReplaceTag(temp, 'Importe', FormatFloat('#,##0.00', fieldbyname('Importe').asFloat));
                        //Rev.3 / 02-Julio-2010 / Nelson Droguett Sierra --------------------
                        temp := ReplaceTag(temp, 'Concesionaria', fieldbyname('NombreCorto').asString);
                        //FinRev.3-----------------------------------------------------------
                        Plantilla := Plantilla + temp;
                        next;
                    end;
                end;
            end; // else if IndiceVehiculo = -1

            Plantilla := Plantilla + Fin;

            close;
        end;
    end;    // fin if TipoConcesionaria = 2 then begin PAR00133_EBA_20110419
end;


procedure CargarDatosFacturacionConvenio(dm: TDMConveniosWeb; Request: TWEBRequest; var Plantilla: string; CodigoConvenio: integer);
begin
    with dm.spObtenerUltimaFactura do begin
        parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        open;

        if recordcount <> 0 then begin
            Last;
            Plantilla := ReplaceTag(Plantilla, 'Fecha', formatdatetime('dd/mm/yyyy', fieldbyname('Fecha').asDateTime));
            Plantilla := ReplaceTag(Plantilla, 'Vencimiento', formatdatetime('dd/mm/yyyy', fieldbyname('Vencimiento').asDateTime));
            Plantilla := ReplaceTag(Plantilla, 'ImporteTotal', FormatFloat('#,###0', fieldbyname('ImporteTotal').asFloat));
            Plantilla := ReplaceTag(Plantilla, 'NumeroComprobante', FormatFloat('000000000000000000', fieldbyname('NumeroComprobante').asInteger));

        end else begin
            Plantilla := ObtenerPlantilla(DM, Request, 'Aviso');
            Plantilla := ReplaceTag(Plantilla, 'Aviso', 'No hay datos de facturaci�n para el convenio elegido.');
            Plantilla := ReplaceTag(Plantilla, 'SiguientePaso', 'PanelDeControl');
        end;

        close;
    end;
end;

function ObtenerDescripcionConvenio(DM: TDMConveniosWEB; CodigoConvenio: integer): string;
begin
    result := QueryGetValue(dm.Base, 'SELECT dbo.FormatearNumeroConvenio(NumeroConvenio) FROM Convenio  WITH (NOLOCK) WHERE CodigoConvenio = ' + inttostr(CodigoConvenio));
end;

function ObtenerRUTCrudo(DM: TDMConveniosWEB; CodigoPersona: integer): string;
begin
    result := QueryGetValue(dm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));
end;


function VerificarTipoVehiculo(DM: TDMConveniosWEB; CodigoTipoVehiculo: string): boolean;
begin
    result := QueryGetValueInt(DM.Base,
      'SELECT COUNT(*) ' +
      'FROM VehiculosTipos  WITH (NOLOCK) ' +
      'WHERE CodigoTipoVehiculo = ' + quotedstr(CodigoTipoVehiculo)) > 0;
end;

function VerificarMarca(DM: TDMConveniosWEB; CodigoMarca: string): boolean;
begin
    result := QueryGetValueInt(DM.Base,
      'SELECT COUNT(*) ' +
      'FROM VehiculosMarcas  WITH (NOLOCK) ' +
      'WHERE CodigoMarca = ' + quotedstr(CodigoMarca)) > 0;
end;


function VerificarAnio(Anio: string): boolean;
var
    IntAnio: integer;
    Limite: integer;
begin
    IntAnio := strtointdef(Anio, 0);
    Limite := year(now);
    if month(now) > 5 then inc(Limite);
    result := (IntAnio > 1899) and (IntAnio <= Limite);
end;

function ObtenerCodigoSegmento(DM: TdmConveniosWeb; Region, Comuna, Numero: AnsiString; Calle: Integer): integer;
var
    nro: integer;
begin
    result := 0;
    if Calle = -1 then exit;
    nro := QueryGetValueInt(dm.Base, 'SELECT dbo.ConvertirNumeroCalle(''' + Numero + ''')');
    with dm.spObtenerCodigoSegmento do begin
        parameters.ParamByName('@CodigoPais').Value := CODIGO_PAIS;
        parameters.ParamByName('@CodigoRegion').Value := Region;
        parameters.ParamByName('@CodigoComuna').Value := Comuna;
        parameters.ParamByName('@CodigoCalle').Value := Calle;
        parameters.ParamByName('@Numero').Value := nro;
        execproc;
        if parameters.ParamByName('@CodigoSegmento').Value = NULL then begin
            result := 0;
        end else begin
            result := parameters.ParamByName('@CodigoSegmento').Value;
        end;
    end;
end;

function ObtenerCodigoPostal(DM: TdmConveniosWeb; Region, Comuna, Numero: AnsiString; Calle: Integer): AnsiString;
var
    numeroCalle: Integer;
begin
    try
        numerocalle := QueryGetValueInt(DM.Base, format('SELECT dbo.formatearNumeroCalle(''%s'')', [TRIM(Numero)]));
        result := QueryGetValue(DM.Base, format('EXEC ObtenerCP ''%s'', ''%s'', ''%s'', %d, %d',[CODIGO_PAIS, Region, Comuna, Calle, NumeroCalle]));
    except
        on e: exception do begin
            ProcesarError(dm, 'ObtenerCodigoPostal', e);
            result := '';
        end;
    end;
end;

function VerificarRegion(DM: TdmConveniosWeb; CodigoRegion: string): boolean;
begin
    result := QueryGetValueInt(DM.Base,
      'SELECT COUNT(*) ' +
      'FROM Regiones  WITH (NOLOCK) ' +
      'WHERE CodigoPais = ' + quotedstr(CODIGO_PAIS) +
      '  AND CodigoRegion = ' + quotedstr(CodigoRegion)) > 0;
end;

function VerificarComuna(DM: TdmConveniosWeb; CodigoRegion, CodigoComuna: string): boolean;
begin
    result := QueryGetValueInt(DM.Base,
      'SELECT COUNT(*) ' +
      'FROM Comunas  WITH (NOLOCK) ' +
      'WHERE CodigoPais = ' + quotedstr(CODIGO_PAIS) +
      '  AND CodigoRegion = ' + quotedstr(CodigoRegion) +
      '  AND CodigoComuna = ' + quotedstr(CodigoComuna)) > 0;
end;

function VerificarCalle(DM: TdmConveniosWeb; CodigoRegion, CodigoComuna, CodigoCalle: string): boolean;
begin
    result := QueryGetValueInt(DM.Base,
      'SELECT COUNT(*) ' +
      'FROM MaestroCALLES  WITH (NOLOCK) ' +
      'WHERE CodigoPais = ' + quotedstr(CODIGO_PAIS) +
      '  AND CodigoRegion = ' + quotedstr(CodigoRegion) +
      '  AND CodigoComuna = ' + quotedstr(CodigoComuna) +
      '  AND CodigoCalle = ' + quotedstr(CodigoCalle)) > 0;
end;

function VerificarNumero(DM: TdmConveniosWeb; Region, Comuna, Calle, Numero: AnsiString): boolean;
var
    NumeroCalle: Integer;
begin
    result := False;
    try
        numeroCalle := QueryGetValueInt(DM.Base, format('SELECT dbo.FormatearNumeroCalle (''%s'')',[Numero]));
        if QueryGetValueInt(DM.Base, format('SELECT DBO.WEB_NumeroCalleValido(''%s'', ''%s'', ''%s'', %d, %d)',
          [CODIGO_PAIS, region, Comuna, strToInt(Calle), NumeroCalle])) = 1 then result := true;
    except
        on e: exception do begin
            ProcesarError(dm, 'VerificarNumero', e);
            result := false;
        end;
    end;
end;

function ValidarNumeroCalle(Numero: string): boolean;
var
    i: integer;
const
    permitidos = ['0'..'9', 'a'..'z', 'A'..'Z', '.', '/', '-'];
begin
    result := false;
    for i := 1 to length(Numero) do begin
        if not (Numero[i] in PERMITIDOS) then exit;
    end;
    result := true;
end;

function VerificarSoloLetras(Texto: string): boolean;
var
    i: integer;
begin
    result := false;
    for i := 1 to length(Texto) do begin
        if not (Texto[i] in ['A'..'Z', 'a'..'z', ' ', '�', '�', '�', '�',
          '�', '�', '�', '�', '�',
          '�', '�', '�', '�', '�',
          '�', '�', '�', '�', '�',
          '�', '�', '�', '�', '�', '/'
        ]) then exit;
    end;
    result := true;
end;

function VerificarHorarios(HorarioContactoDesde, HorarioContactoHasta: AnsiString; var Error: AnsiString):boolean;
begin
    result := true;
    if (((HorarioContactoDesde = '') and (HorarioContactoHasta <> '')) or
        ((HorarioContactoHasta = '') and (HorarioContactoDesde <> ''))) then begin
        Error := MSG_DEBE_COMPLETAR_HORARIO_CONTACTO;
        result := False;
        exit;
    end;

    if HorarioContactoDesde <> '' then begin
        if (StrToTimeDef(HorarioContactoDesde, -1) = -1) then begin
            Error := MSG_DATOS_ERRONEOS;
            result := false;
            exit;
        end;
    end;

    if HorarioContactoHasta <> '' then begin
        if (StrToTimeDef(HorarioContactoHasta, -1) = -1) then begin
            Error := MSG_DATOS_ERRONEOS;
            result := false;
            exit;
        end;
    end;

    if StrToTimeDef(HorarioContactoDesde, -1) >= StrToTimeDef(HorarioContactoHasta, -1) then begin
        Error := MSG_HORARIOS_INCORRECTOS;
        result := false;
        exit;
    end;
end;


function VerificarCorrespondenciaTipoTelefono(dm: TdmConveniosWeb; TipoTelefono, CodigoArea: ANsiString): boolean;
begin
    result := true;
    if (TipoTelefono = '5') {celular} then begin
        //if (CodigoArea <> '8') and (CodigoArea <> '9') then begin
        //Rev.4 / 09-Julio-2010 / Nelson Droguett Sierra ------------------------------
        if QueryGetValue(dm.base, 'SELECT TipoLinea FROM CodigosAreaTelefono WITH (NOLOCK) WHERE CodigoArea = ' + CodigoArea)<>'C' then begin
            result := false;
        end;
    end;
end;

function VerificarNumeroTelefono(DM: TdmConveniosWeb; CodigoArea, Telefono: ANsiString): Boolean;
begin
    try
        result := QueryGetValueInt(DM.Base,
            'Exec ValidarTelefonoCorrecto ' + CodigoArea + ', '''+ Trim(Telefono) + '''') = 1;
    except
        on e: exception do begin
            ProcesarError(dm, 'VerificarNumeroTelefono', e);
            result:= false;
        end;
    end;
end;

function VerificarTipoTelefono(DM: TdmConveniosWeb; TipoTelefono: string): boolean;
begin
    result := false;
    if StrToIntDef(TipoTelefono, -1) = -1 then exit;
    result := QueryGetValueInt(DM.Base, 'SELECT COUNT(*) FROM TiposMedioContacto  WITH (NOLOCK) where Formato = ''T'' AND CodigoTipoMedioContacto = ' + TipoTelefono) > 0;
end;

function VerificarSoloNumeros(Texto: string): boolean;
var
    i: integer;
begin
    result := false;
    for i := 1 to length(Texto) do begin
        if not (Texto[i] in ['0'..'9', ' ']) then exit;
    end;
    result := true;
end;

function VerificarCodigoArea(DM: TdmConveniosWeb; CodigoArea: string): boolean;
begin
    result := false;
    if StrToIntDef(CodigoArea, -1) = -1 then exit;
    result := QueryGetValueInt(DM.Base, 'SELECT COUNT(*) FROM CodigosAreaTelefono  WITH (NOLOCK) WHERE CodigoArea = ' + CodigoArea) > 0;
end;


function DominioMailEsValido(DM: TdmConveniosWeb; mail: string): boolean;
begin
    with dm.spVerificarDominioMail do begin
        Parameters.ParamByName('@Dominio').Value := copy(mail, pos('@', mail) + 1, length(mail));
        execproc;
        result := Parameters.ParamByName('@Return_Value').Value = 0;
    end;
end;

function PersonaVerificarDatosPersonales(DM: TDMConveniosWEB; Request: TWebRequest; var Error: string): boolean;
var
    SexoContacto,
    EmailContacto,
    TipoTelefonoContacto,
    CodigoAreaTelefonoContacto,
    NumeroTelefonoContacto,
    HorarioContactoDesde,
    HorarioContactoHasta,
    DiaNacimientoContacto,
    MesNacimientoContacto,
    AnioNacimientoContacto,
    TipoOtroTelefonoContacto,
    CodigoAreaOtroTelefonoContacto,
    NumeroOtroTelefonoContacto: string;
    FechaNacimiento: TDateTime;
begin
    result := false;

    // cargo los valores
    with Request.ContentFields do begin
        SexoContacto := Trim(Values['SexoContacto']);
        EmailContacto := Trim(Values['EmailContacto']);
        TipoTelefonoContacto := Trim(Values['TipoTelefonoContacto']);
        CodigoAreaTelefonoContacto := Trim(Values['CodigoAreaTelefonoContacto']);
        NumeroTelefonoContacto := Trim(Values['NumeroTelefonoContacto']);
        HorarioContactoDesde := Trim(Values['HorarioContactoDesde']);
        HorarioContactoHasta := Trim(Values['HorarioContactoHasta']);
        TipoOtroTelefonoContacto := Trim(Values['TipoOtroTelefonoContacto']);
        CodigoAreaOtroTelefonoContacto := Trim(Values['CodigoAreaOtroTelefonoContacto']);
        NumeroOtroTelefonoContacto := Trim(Values['NumeroOtroTelefonoContacto']);
        DiaNacimientoContacto := Trim(Values['DiaNacimientoContacto']);
        MesNacimientoContacto := Trim(Values['MesNacimientoContacto']);
        AnioNacimientoContacto := Trim(Values['AnioNacimientoContacto']);
    end;

    // verifico el sexo
    if SexoContacto = '' then begin
        Error := MSG_DEBE_ELEGIR_SEXO;
        exit;
    end;
    if not (SexoContacto[1] in ['M', 'F']) then begin
        Error := MSG_DEBE_ELEGIR_SEXO;
        exit;
    end;

    // verifico el email
    if EmailContacto <> '' then begin
        if not IsValidEMail(EmailContacto) then begin
            Error := MSG_EMAIL_INVALIDO;
            exit;
        end;
        if not DominioMailEsValido(DM, EmailContacto) then begin
            Error := MSG_EMAIL_INVALIDO;
            exit;
        end;
    end;

    // verifico la edad
	if (strtointdef(AnioNacimientoContacto, -1) <> -1)
      and (strtointdef(MesNacimientoContacto, -1) <> -1)
      and (strtointdef(DiaNacimientoContacto, -1) <> -1) then begin
	    if IsValidDate(strtointdef(AnioNacimientoContacto, -1), strtointdef(MesNacimientoContacto, -1), strtointdef(DiaNacimientoContacto, -1)) then begin
            FechaNacimiento := EncodeDate(strtointdef(AnioNacimientoContacto, -1), strtointdef(MesNacimientoContacto, -1), strtointdef(DiaNacimientoContacto, -1));
			if FechaNacimiento > now then begin
            	// este tipo nacio en el futuro...
	        	Error := MSG_FECHA_INVALIDA;
    	        exit;
            end;
            if FechaNacimiento < EncodeDate(1900, 1, 1) then begin
				// si es del siglo pasado, la rechazamos
                Error := MSG_FECHA_INVALIDA_VIEJA;
                exit;
            end;
            if Now - FechaNacimiento < 365 * EDAD_MINIMA then begin
            	// si tiene menos de 18, tambien
                Error := MSG_FECHA_DEMASIADO_JOVEN;
                exit;
            end;
        end else begin
        	Error := MSG_FECHA_INVALIDA;
            exit;
        end;
    end;

    // verifico el tel�fono
    if (TipoTelefonoContacto = '') or (CodigoAreaTelefonoContacto = '') or (NumeroTelefonoContacto = '') then begin
        Error := MSG_DEBE_COMPLETAR_TELEFONO;
        exit;
    end;
    if not VerificarSoloNumeros(NumeroTelefonoContacto) then begin
        Error := Format(MSG_CARACTERES_INVALIDOS_NUMEROS, ['Numero de Telefono']);
        exit;
    end;
    if not VerificarCodigoArea(DM, CodigoAreaTelefonoContacto) or not VerificarTipoTelefono(DM, TipoTelefonoContacto) then begin
        Error := MSG_DATOS_ERRONEOS;
        exit;
    end;
    if not VerificarNumeroTelefono(DM, CodigoAreaTelefonoContacto, NumeroTelefonoContacto) then begin
        Error := MSG_TELEFONO_INCORRECTO;
        exit;
    end;
    if not VerificarCorrespondenciaTipoTelefono(dm, TipoTelefonoContacto, CodigoAreaTelefonoContacto) then begin
        Error := MSG_TIPO_TELEFONO_NO_CORRESPONDE;
        exit;
    end;


    if not VerificarHorarios(HorarioContactoDesde, HorarioContactoHasta, Error) then exit;

    // verifico el otro tel�fono (solo si lo carg�)
    if NumeroOtroTelefonoContacto <> '' then begin
        if not VerificarSoloNumeros(NumeroOtroTelefonoContacto) then begin
            Error := Format(MSG_CARACTERES_INVALIDOS_NUMEROS, ['Numero de Otro Telefono']);
            exit;
        end;
        if CodigoAreaOtroTelefonoContacto = '' then begin
            Error := MSG_DEBE_COMPLETAR_OTRO_TELEFONO;
            exit;
        end;
        if not VerificarCodigoArea(DM, CodigoAreaOtroTelefonoContacto) or not VerificarTipoTelefono(DM, TipoOtroTelefonoContacto) then begin
            Error := MSG_DATOS_ERRONEOS;
            exit;
        end;
        if not VerificarNumeroTelefono(DM, CodigoAreaOtroTelefonoContacto, NumeroOtroTelefonoContacto) then begin
            Error := MSG_TELEFONO_INCORRECTO;
            exit;
        end;
        if not VerificarCorrespondenciaTipoTelefono(dm, TipoOtroTelefonoContacto, CodigoAreaOtroTelefonoContacto) then begin
            Error := MSG_TIPO_TELEFONO_NO_CORRESPONDE;
            exit;
        end;
    end;

    // verifico la fecha de nacimiento
    if (((DiaNacimientoContacto = '') and ((MesNacimientoContacto <> '') or (AnioNacimientoContacto <> ''))) or
        ((MesNacimientoContacto = '') and ((DiaNacimientoContacto <> '') or (AnioNacimientoContacto <> ''))) or
        ((AnioNacimientoContacto = '') and ((DiaNacimientoContacto <> '') or (MesNacimientoContacto <> '')))) then begin
        Error := MSG_DEBE_COMPLETAR_FECHA_NACIMIENTO;
        exit;
    end;

    if (DiaNacimientoContacto <> '') and (MesNacimientoContacto <> '') and (AnioNacimientoContacto <> '') then begin
        try
            EncodeDate(StrToInt(AnioNacimientoContacto), StrToInt(MesNacimientoContacto), StrToInt(DiaNacimientoContacto));
        except
            Error := MSG_FECHA_NACIMIENTO_INCORRECTA;
            exit;
        end;
    end;

    // puf! despues de tantas comprobaciones, si estamos ac�, es porque est� todo bien
    result := true;
end;

function CrearQuery(Conexion: TADOConnection; SQL: string): TADOQuery;
begin
    result := TADOQuery.create(nil);
    result.Connection := Conexion;
    result.SQL.text := SQL;
end;

{******************************** Function Header ******************************
Revision 1:
    Author : nefernandez
    Date : 28/09/2007
    Description : Cambio la consulta para que la condicion de Activo sea de la
    tabla PersonasMediosComunicacion
******************************** Function Header ******************************}
procedure CargarDatosTelefonos(dm: TDMConveniosWEB; var Plantilla: string; CodigoPersona: integer);
var
    EmailContacto,
    TipoTelefonoContacto,
    CodigoAreaTelefonoContacto,
    NumeroTelefonoContacto,
    HorarioContactoDesde,
    HorarioContactoHasta,
    TipoOtroTelefonoContacto,
    CodigoAreaOtroTelefonoContacto,
    NumeroOtroTelefonoContacto: string;
    qryTelefonos: TADOQuery;
begin
    qryTelefonos := CrearQuery(dm.base,
      'SELECT MC.*, TMC.Formato ' +
      'FROM MediosComunicacion MC  WITH (NOLOCK) ' +
      'INNER JOIN PersonasMediosComunicacion  PMC WITH (NOLOCK) ON MC.CodigoMedioComunicacion = PMC.CodigoMedioComunicacion ' +
      'INNER JOIN TiposMedioContacto  TMC WITH (NOLOCK) ON MC.CodigoTipoMedioContacto = TMC.CodigoTipoMedioContacto ' +
      'WHERE PMC.CodigoPersona = ' + inttostr(CodigoPersona) + ' ' +
      '  AND PMC.Activo = 1 ' +
//      '  AND TMC.Formato = ''T'' ' +
      'ORDER BY PRINCIPAL DESC, MC.CodigoMedioComunicacion ASC ');

    try
        with qryTelefonos do begin
            open;
            filter := 'Formato = ''T''';
            filtered := true;
            first;

            TipoTelefonoContacto := '';
            CodigoAreaTelefonoContacto := '';
            NumeroTelefonoContacto := '';
            HorarioContactoDesde := '';
            HorarioContactoHasta := '';
            TipoOtroTelefonoContacto := '';
            CodigoAreaOtroTelefonoContacto := '';
            NumeroOtroTelefonoContacto := '';
            EmailContacto := '';

            if not eof then begin
                // estoy en el telefono principal
                TipoTelefonoContacto := trim(FieldByName('CodigoTipoMedioContacto').AsString);
                CodigoAreaTelefonoContacto := trim(FieldByName('CodigoArea').AsString);
                NumeroTelefonoContacto := trim(FieldByName('Valor').AsString);
                if not FieldByName('HorarioDesde').IsNull then
                  HorarioContactoDesde := FormatDateTime('hh:nn', FieldByName('HorarioDesde').AsDateTime);
                if not FieldByName('HorarioHasta').IsNull then
                  HorarioContactoHasta := FormatDateTime('hh:nn', FieldByName('HorarioHasta').AsDateTime);

                next;
                // localizo el telefono secundario
                if not eof then begin
                    TipoOtroTelefonoContacto := FieldByName('CodigoTipoMedioContacto').AsString;
                    CodigoAreaOtroTelefonoContacto := FieldByName('CodigoArea').AsString;
                    NumeroOtroTelefonoContacto := FieldByName('Valor').AsString;
                end;
            end;

            // el email...
            filtered := false;
            filter := 'Formato = ''E''';
            filtered := true;
            first;
            if not eof then begin
                EmailContacto := FieldByName('Valor').AsString;
            end;
            close;
        end;

        Plantilla := ReplaceTag(Plantilla, 'TipoTelefonoContacto', TipoTelefonoContacto);
        Plantilla := ReplaceTag(Plantilla, 'CodigoAreaTelefonoContacto', CodigoAreaTelefonoContacto);
        Plantilla := ReplaceTag(Plantilla, 'NumeroTelefonoContacto', NumeroTelefonoContacto);
        Plantilla := ReplaceTag(Plantilla, 'HorarioContactoDesde', HorarioContactoDesde);
        Plantilla := ReplaceTag(Plantilla, 'HorarioContactoHasta', HorarioContactoHasta);

        Plantilla := ReplaceTag(Plantilla, 'TelefonoDesc', '(' + CodigoAreaTelefonoContacto + ') ' + NumeroTelefonoContacto);

        Plantilla := ReplaceTag(Plantilla, 'TipoOtroTelefonoContacto', TipoOtroTelefonoContacto);
        Plantilla := ReplaceTag(Plantilla, 'CodigoAreaOtroTelefonoContacto', CodigoAreaOtroTelefonoContacto);
        Plantilla := ReplaceTag(Plantilla, 'NumeroOtroTelefonoContacto', NumeroOtroTelefonoContacto);

        if CodigoAreaOtroTelefonoContacto <> '' then begin
            Plantilla := ReplaceTag(Plantilla, 'OtroTelefonoDesc', '(' + CodigoAreaOtroTelefonoContacto + ') ' + NumeroOtroTelefonoContacto);
        end else begin
            Plantilla := ReplaceTag(Plantilla, 'OtroTelefonoDesc', '');
        end;


        Plantilla := ReplaceTag(Plantilla, 'ValoresTipoTelefonoContacto', GenerarComboTiposTelefonos(DM, TipoTelefonoContacto));
        Plantilla := ReplaceTag(Plantilla, 'ValoresTipoOtroTelefonoContacto', GenerarComboTiposTelefonos(DM, TipoOtroTelefonoContacto));
        Plantilla := ReplaceTag(Plantilla, 'ValoresCodigoAreaTelefonoContacto', GenerarComboCodigosDeArea(DM, CodigoAreaTelefonoContacto));
        Plantilla := ReplaceTag(Plantilla, 'ValoresCodigoAreaOtroTelefonoContacto', GenerarComboCodigosDeArea(DM, CodigoAreaOtroTelefonoContacto));
        Plantilla := ReplaceTag(Plantilla, 'ValoresHorarioContactoDesde', GenerarComboHorarioInicial(HorarioContactoDesde));
        Plantilla := ReplaceTag(Plantilla, 'ValoresHorarioContactoHasta', GenerarComboHorarioFinal(HorarioContactoHasta));

        Plantilla := ReplaceTag(Plantilla, 'EmailContacto', EmailContacto);
    finally
        qryTelefonos.Free;
    end;
end;


procedure PersonaCargarDatosPersonales(dm: TDMConveniosWEB; var Plantilla: string; CodigoPersona: integer);
var
    qryDatosPersona: TADOQuery;
    RUT,
    RUTDV,
    RUTCrudo: string;
    NombreContacto,
    ApellidoPaternoContacto,
    ApellidoMaternoContacto,
    SexoContacto,
    DiaNacimientoContacto,
    MesNacimientoContacto,
    AnioNacimientoContacto,
    EmailContacto: string;
begin
    qryDatosPersona := CrearQuery(dm.Base, 'SELECT * FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));
    try
        qryDatosPersona.Open;

        with qryDatosPersona do begin
            // la parte del RUT
            RUTCrudo := trim(fieldbyname('NumeroDocumento').asstring);

            if RUTCrudo = '' then begin
                RUT := '';
                RUTDV := '';
            end else begin
                RUT := copy(RUTCrudo, 1, length(RUTCrudo) - 1);
                RUTDV := copy(RUTCrudo, length(RUTCrudo), 1);
            end;

            Plantilla := ReplaceTag(Plantilla, 'RUT', RUT);
            Plantilla := ReplaceTag(Plantilla, 'RUTDV', RUTDV);

            // los datos personales
            NombreContacto := trim(FieldByName('Nombre').AsString);
            ApellidoPaternoContacto := trim(FieldByName('Apellido').AsString);
            ApellidoMaternoContacto := trim(FieldByName('ApellidoMaterno').AsString);
            SexoContacto := trim(FieldByName('Sexo').AsString);
            EmailContacto := trim(FieldByName('EmailContactoComercial').AsString);
            if not FieldByName('FechaNacimiento').IsNull then begin
                DiaNacimientoContacto := inttostr(day(FieldByName('FechaNacimiento').AsDateTime));
                MesNacimientoContacto := inttostr(month(FieldByName('FechaNacimiento').AsDateTime));
                AnioNacimientoContacto := inttostr(year(FieldByName('FechaNacimiento').AsDateTime));
            end else begin
                DiaNacimientoContacto := ParseParamByNumber(FieldByName('ApellidoMaternoContactoComercial').AsString, 3);
                MesNacimientoContacto := ParseParamByNumber(FieldByName('ApellidoMaternoContactoComercial').AsString, 2);
                AnioNacimientoContacto := ParseParamByNumber(FieldByName('ApellidoMaternoContactoComercial').AsString, 1);
            end;

            Plantilla := ReplaceTag(Plantilla, 'NombreContacto', NombreContacto);
            Plantilla := ReplaceTag(Plantilla, 'ApellidoPaternoContacto', ApellidoPaternoContacto);
            Plantilla := ReplaceTag(Plantilla, 'ApellidoMaternoContacto', ApellidoMaternoContacto);
            Plantilla := ReplaceTag(Plantilla, 'SexoContacto', SexoContacto);
            Plantilla := ReplaceTag(Plantilla, 'SexoContactoDesc', iif(SexoContacto = 'M', 'Masculino', 'Femenino'));
            Plantilla := ReplaceTag(Plantilla, 'ValoresSexoContacto', GenerarComboSexo(SexoContacto));

            Plantilla := ReplaceTag(Plantilla, 'DiaNacimientoContacto', DiaNacimientoContacto);
            Plantilla := ReplaceTag(Plantilla, 'MesNacimientoContacto', MesNacimientoContacto);
            Plantilla := ReplaceTag(Plantilla, 'AnioNacimientoContacto', AnioNacimientoContacto);

            Plantilla := ReplaceTag(Plantilla, 'ValoresDiaNacimientoContacto', GenerarComboDias(DiaNacimientoContacto));
            Plantilla := ReplaceTag(Plantilla, 'ValoresMesNacimientoContacto', GenerarComboMeses(MesNacimientoContacto));
            Plantilla := ReplaceTag(Plantilla, 'ValoresAnioNacimientoContacto', GenerarComboAnios(AnioNacimientoContacto));

            // los telefonos
            CargarDatosTelefonos(dm, Plantilla, CodigoPersona);
        end;

        qryDatosPersona.close;
    finally
        qryDatosPersona.Free;
    end;
end;

procedure EmpresaCargarDatosPersonales(dm: TDMConveniosWEB; var Plantilla: string; CodigoPersona: integer);
var
    qryDatosEmpresa: TADOQuery;
    RUT,
    RUTDV,
    RUTCrudo: string;
    NombreEmpresa,
    NombreContacto,
    ApellidoPaternoContacto,
    ApellidoMaternoContacto,
    SexoContacto,
    EmailContacto: string;
begin
    NombreEmpresa := '';
    NombreContacto := '';
    ApellidoPaternoContacto := '';
    ApellidoMaternoContacto := '';
    SexoContacto := '';
    EmailContacto := '';

    qryDatosEmpresa := CrearQuery(dm.Base, 'SELECT * FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));
    try
        qryDatosEmpresa.Open;

        with qryDatosEmpresa do begin
            // la parte del RUT
            RUTCrudo := trim(fieldbyname('NumeroDocumento').asstring);

            if RUTCrudo = '' then begin
                RUT := '';
                RUTDV := '';
            end else begin
                RUT := copy(RUTCrudo, 1, length(RUTCrudo) - 1);
                RUTDV := copy(RUTCrudo, length(RUTCrudo), 1);
            end;

            Plantilla := ReplaceTag(Plantilla, 'RUT', RUT);
            Plantilla := ReplaceTag(Plantilla, 'RUTDV', RUTDV);

            // los datos personales
            NombreEmpresa := trim(FieldByName('Apellido').AsString);
            NombreContacto := trim(FieldByName('NombreContactoComercial').AsString);
            ApellidoPaternoContacto := trim(FieldByName('ApellidoContactoComercial').AsString);
            ApellidoMaternoContacto := trim(FieldByName('ApellidoMaternoContactoComercial').AsString);
            SexoContacto := trim(FieldByName('SexoContactoComercial').AsString);
            EmailContacto := trim(FieldByName('EmailContactoComercial').AsString);

            Plantilla := ReplaceTag(Plantilla, 'NombreEmpresa', NombreEmpresa);
            Plantilla := ReplaceTag(Plantilla, 'NombreContacto', NombreContacto);
            Plantilla := ReplaceTag(Plantilla, 'ApellidoPaternoContacto', ApellidoPaternoContacto);
            Plantilla := ReplaceTag(Plantilla, 'ApellidoMaternoContacto', ApellidoMaternoContacto);
            Plantilla := ReplaceTag(Plantilla, 'SexoContacto', SexoContacto);
            Plantilla := ReplaceTag(Plantilla, 'SexoContactoDesc', iif(SexoContacto = 'M', 'Masculino', 'Femenino'));
            Plantilla := ReplaceTag(Plantilla, 'ValoresSexoContacto', GenerarComboSexo(SexoContacto));

            // los telefonos
            CargarDatosTelefonos(dm, Plantilla, CodigoPersona);
        end;

        qryDatosEmpresa.close;
    finally
        qryDatosEmpresa.Free;
    end;
end;

function EmpresaVerificarDatosPersonales(DM: TDMConveniosWEB; Request: TWebRequest; var Error: string): boolean;
var
    NombreContacto,
    ApellidoPaternoContacto,
    ApellidoMaternoContacto,
    SexoContacto,
    EmailContacto,
    TipoTelefonoContacto,
    CodigoAreaTelefonoContacto,
    NumeroTelefonoContacto,
    HorarioContactoDesde,
    HorarioContactoHasta,
    TipoOtroTelefonoContacto,
    CodigoAreaOtroTelefonoContacto,
    NumeroOtroTelefonoContacto: string;
begin
    result := false;

    // cargo los valores
    with Request.ContentFields do begin
        NombreContacto := Trim(Values['NombreContacto']);
        ApellidoPaternoContacto := Trim(Values['ApellidoPaternoContacto']);
        ApellidoMaternoContacto := Trim(Values['ApellidoMaternoContacto']);
        SexoContacto := Trim(Values['SexoContacto']);
        EmailContacto := Trim(Values['EmailContacto']);
        TipoTelefonoContacto := Trim(Values['TipoTelefonoContacto']);
        CodigoAreaTelefonoContacto := Trim(Values['CodigoAreaTelefonoContacto']);
        NumeroTelefonoContacto := Trim(Values['NumeroTelefonoContacto']);
        HorarioContactoDesde := Trim(Values['HorarioContactoDesde']);
        HorarioContactoHasta := Trim(Values['HorarioContactoHasta']);
        TipoOtroTelefonoContacto := Trim(Values['TipoOtroTelefonoContacto']);
        CodigoAreaOtroTelefonoContacto := Trim(Values['CodigoAreaOtroTelefonoContacto']);
        NumeroOtroTelefonoContacto := Trim(Values['NumeroOtroTelefonoContacto']);
    end;

    // verifico el nombre
    if NombreContacto = '' then begin
        Error := MSG_DEBE_COMPLETAR_NOMBRE_CONTACTO;
        exit;
    end;
    if not VerificarSoloLetras(NombreContacto) then begin
        Error := Format(MSG_CARACTERES_INVALIDOS_LETRAS, ['Nombre de contacto']);
        exit;
    end;

    // verifico el apellido paterno
    if ApellidoPaternoContacto = '' then begin
        Error := MSG_DEBE_COMPLETAR_APELLIDO_PATERNO_CONTACTO;
        exit;
    end;
    if not VerificarSoloLetras(ApellidoPaternoContacto) then begin
        Error := Format(MSG_CARACTERES_INVALIDOS_LETRAS, ['Apellido Paterno']);
        exit;
    end;

    // verifico el apellido materno
    if not VerificarSoloLetras(ApellidoMaternoContacto) then begin
        Error := Format(MSG_CARACTERES_INVALIDOS_LETRAS, ['Apellido Materno']);
        exit;
    end;

    // verifico el sexo
    if SexoContacto = '' then begin
        Error := MSG_DEBE_ELEGIR_SEXO;
        exit;
    end;

    if not (SexoContacto[1] in ['M', 'F']) then begin
        Error := MSG_DEBE_ELEGIR_SEXO;
        exit;
    end;

    // verifico el email
    if EmailContacto = '' then begin
        Error := MSG_DEBE_ELEGIR_EMAIL;
        exit;
    end else begin
        if not IsValidEMail(EmailContacto) then begin
            Error := MSG_EMAIL_INVALIDO;
            exit;
        end;
        if not DominioMailEsValido(DM, EmailContacto) then begin
            Error := MSG_EMAIL_INVALIDO;
            exit;
        end;
    end;

    // verifico el tel�fono
    if (TipoTelefonoContacto = '') or (CodigoAreaTelefonoContacto = '') or (NumeroTelefonoContacto = '') then begin
        Error := MSG_DEBE_COMPLETAR_TELEFONO;
        exit;
    end;
    if not VerificarSoloNumeros(NumeroTelefonoContacto) then begin
        Error := Format(MSG_CARACTERES_INVALIDOS_NUMEROS, ['Numero de Telefono']);
        exit;
    end;
    if not VerificarCodigoArea(DM, CodigoAreaTelefonoContacto) or not VerificarTipoTelefono(DM, TipoTelefonoContacto) then begin
        Error := MSG_DATOS_ERRONEOS;
        exit;
    end;
    if not VerificarNumeroTelefono(DM, CodigoAreaTelefonoContacto, NumeroTelefonoContacto) then begin
        Error := MSG_TELEFONO_INCORRECTO;
        exit;
    end;

    if not VerificarCorrespondenciaTipoTelefono(dm, TipoTelefonoContacto, CodigoAreaTelefonoContacto) then begin
        Error := MSG_TIPO_TELEFONO_NO_CORRESPONDE;
        exit;
    end;

    if not VerificarHorarios(HorarioContactoDesde, HorarioContactoHasta, Error) then exit;

    // verifico el otro tel�fono (solo si lo carg�)
    if NumeroOtroTelefonoContacto <> '' then begin
        if not VerificarSoloNumeros(NumeroOtroTelefonoContacto) then begin
            Error := Format(MSG_CARACTERES_INVALIDOS_NUMEROS, ['Numero de Otro Telefono']);
            exit;
        end;
        if CodigoAreaOtroTelefonoContacto = '' then begin
            Error := MSG_DEBE_COMPLETAR_OTRO_TELEFONO;
            exit;
        end;
        if not VerificarCodigoArea(DM, CodigoAreaOtroTelefonoContacto) or not VerificarTipoTelefono(DM, TipoOtroTelefonoContacto) then begin
            Error := MSG_DATOS_ERRONEOS;
            exit;
        end;
        if not VerificarNumeroTelefono(DM, CodigoAreaOtroTelefonoContacto, NumeroOtroTelefonoContacto) then begin
            Error := MSG_TELEFONO_INCORRECTO;
            exit;
        end;
        if not VerificarCorrespondenciaTipoTelefono(dm, TipoOtroTelefonoContacto, CodigoAreaOtroTelefonoContacto) then begin
            Error := MSG_TIPO_TELEFONO_NO_CORRESPONDE;
            exit;
        end;
    end;

    // puf! despues de tantas comprobaciones, si estamos ac�, es porque est� todo bien
    result := true;
end;

function GrabarCambiosDatosPersonalesPersona(DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion: integer; var Error: string): boolean;
var
    SexoContacto,
    EmailContacto,
    NumeroTelefonoContacto,
    HorarioContactoDesde,
    HorarioContactoHasta,
    NumeroOtroTelefonoContacto: string;
    DiaNacimientoContacto,
    MesNacimientoContacto,
    AnioNacimientoContacto,
    TipoTelefonoContacto,
    CodigoAreaTelefonoContacto,
    TipoOtroTelefonoContacto,
    CodigoAreaOtroTelefonoContacto: integer;
    FechaNacimiento: TDateTime;
    CodigoPersona: integer;
begin
    result := false;

    if not PersonaVerificarDatosPersonales(DM, Request, Error) then exit;

    CodigoPersona := QueryGetValueInt(dm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));

    try
        // cargo los valores
        with Request.ContentFields do begin
            SexoContacto := Trim(Values['SexoContacto']);
            EmailContacto := Trim(Values['EmailContacto']);
            TipoTelefonoContacto := strtoint(Trim(Values['TipoTelefonoContacto']));
            CodigoAreaTelefonoContacto := strtoint(Trim(Values['CodigoAreaTelefonoContacto']));
            NumeroTelefonoContacto := Trim(Values['NumeroTelefonoContacto']);
            HorarioContactoDesde := Trim(Values['HorarioContactoDesde']);
            HorarioContactoHasta := Trim(Values['HorarioContactoHasta']);
            TipoOtroTelefonoContacto := strtointdef(Trim(Values['TipoOtroTelefonoContacto']), 0);
            CodigoAreaOtroTelefonoContacto := strtointdef(Trim(Values['CodigoAreaOtroTelefonoContacto']), 0);
            NumeroOtroTelefonoContacto := Trim(Values['NumeroOtroTelefonoContacto']);
            DiaNacimientoContacto := strtointdef(Trim(Values['DiaNacimientoContacto']), 0);
            MesNacimientoContacto := strtointdef(Trim(Values['MesNacimientoContacto']), 0);
            AnioNacimientoContacto := strtointdef(Trim(Values['AnioNacimientoContacto']), 0);
            if (DiaNacimientoContacto <> 0) and (MesNacimientoContacto <> 0) and (AnioNacimientoContacto <> 0) and IsValidDate(AnioNacimientoContacto, MesNacimientoContacto, DiaNacimientoContacto) then begin
                FechaNacimiento := EncodeDate(AnioNacimientoContacto, MesNacimientoContacto, DiaNacimientoContacto);
            end else begin
                FechaNacimiento := NULLDATE;
            end;
        end;

        with dm.spActualizarDatosPersona do begin
            parameters.ParamByName('@CodigoPersona').value := CodigoPersona;
            parameters.ParamByName('@Sexo').value := SexoContacto;
            parameters.ParamByName('@FechaNacimiento').value := iif(FechaNacimiento = NULLDATE, NULL, FechaNacimiento);
            parameters.ParamByName('@EmailContactoComercial').value := iif(EmailContacto = '', NULL, EmailContacto);
            parameters.ParamByName('@CodigoUsuario').value := 'WEB';
            ExecProc;
        end;

        with dm.spActualizarMediosComunicacion do begin
            parameters.ParamByName('@CodigoPersona').value := CodigoPersona;
            parameters.ParamByName('@TipoTelefonoContacto').value := TipoTelefonoContacto;
            parameters.ParamByName('@CodigoAreaTelefonoContacto').value := CodigoAreaTelefonoContacto;
            parameters.ParamByName('@NumeroTelefonoContacto').value := NumeroTelefonoContacto;
            parameters.ParamByName('@HorarioContactoDesde').value := HorarioContactoDesde;
            parameters.ParamByName('@HorarioContactoHasta').value := HorarioContactoHasta;
            //Parameters.ParamByName('@Usuario').Value := 'WEB';                  // SS_1332_CQU_20151229 //SS_1305_MCA_20150612
            if trim(NumeroOtroTelefonoContacto) <> '' then begin
                parameters.ParamByName('@TipoOtroTelefonoContacto').value := TipoOtroTelefonoContacto;
                parameters.ParamByName('@CodigoAreaOtroTelefonoContacto').value := CodigoAreaOtroTelefonoContacto;
                parameters.ParamByName('@NumeroOtroTelefonoContacto').value := NumeroOtroTelefonoContacto;
            end else begin
                parameters.ParamByName('@TipoOtroTelefonoContacto').value := NULL;
                parameters.ParamByName('@CodigoAreaOtroTelefonoContacto').value := NULL;
                parameters.ParamByName('@NumeroOtroTelefonoContacto').value := NULL;
            end;
            ExecProc;
        end;

        result := true;
    except
        on e: exception do begin
            Error := 'Error actualizando los datos de la persona: ' + e.Message;
        end;
    end;
end;

function GrabarCambiosDatosPersonalesEmpresa(DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion: integer; var Error: string): boolean;
var
    CodigoPersona: integer;
    NombreContacto,
    ApellidoPaternoContacto,
    ApellidoMaternoContacto,
    SexoContacto,
    NumeroTelefonoContacto,
    HorarioContactoDesde,
    HorarioContactoHasta,
    NumeroOtroTelefonoContacto,
    EmailContacto: string;
    TipoTelefonoContacto,
    CodigoAreaTelefonoContacto,
    TipoOtroTelefonoContacto,
    CodigoAreaOtroTelefonoContacto: integer;
begin
    result := false;

    if not EmpresaVerificarDatosPersonales(DM, Request, Error) then exit;

    CodigoPersona := QueryGetValueInt(dm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));

    try
        // cargo los valores
        with Request.ContentFields do begin
            NombreContacto := Trim(Values['NombreContacto']);
            ApellidoPaternoContacto := Trim(Values['ApellidoPaternoContacto']);
            ApellidoMaternoContacto := Trim(Values['ApellidoMaternoContacto']);
            SexoContacto := Trim(Values['SexoContacto']);
            EmailContacto := Trim(Values['EmailContacto']);
            TipoTelefonoContacto := strtoint(Trim(Values['TipoTelefonoContacto']));
            CodigoAreaTelefonoContacto := strtoint(Trim(Values['CodigoAreaTelefonoContacto']));
            NumeroTelefonoContacto := Trim(Values['NumeroTelefonoContacto']);
            HorarioContactoDesde := Trim(Values['HorarioContactoDesde']);
            HorarioContactoHasta := Trim(Values['HorarioContactoHasta']);
            TipoOtroTelefonoContacto := strtointdef(Trim(Values['TipoOtroTelefonoContacto']), 0);
            CodigoAreaOtroTelefonoContacto := strtointdef(Trim(Values['CodigoAreaOtroTelefonoContacto']), 0);
            NumeroOtroTelefonoContacto := Trim(Values['NumeroOtroTelefonoContacto']);
        end;

        with dm.spActualizarDatosEmpresa do begin
            parameters.ParamByName('@CodigoPersona').value := CodigoPersona;
            parameters.ParamByName('@SexoContactoComercial').value := SexoContacto;
            parameters.ParamByName('@EmailContactoComercial').value := iif(EmailContacto = '', NULL, EmailContacto);
            parameters.ParamByName('@ApellidoContactoComercial').value := ApellidoPaternoContacto;
            parameters.ParamByName('@ApellidoMaternoContactoComercial').value := ApellidoMaternoContacto;
            parameters.ParamByName('@NombreContactoComercial').value := NombreContacto;
            parameters.ParamByName('@CodigoUsuario').value := 'WEB';
            ExecProc;
        end;

        with dm.spActualizarMediosComunicacion do begin
            parameters.ParamByName('@CodigoPersona').value := CodigoPersona;
            parameters.ParamByName('@TipoTelefonoContacto').value := TipoTelefonoContacto;
            parameters.ParamByName('@CodigoAreaTelefonoContacto').value := CodigoAreaTelefonoContacto;
            parameters.ParamByName('@NumeroTelefonoContacto').value := NumeroTelefonoContacto;
            parameters.ParamByName('@HorarioContactoDesde').value := HorarioContactoDesde;
            parameters.ParamByName('@HorarioContactoHasta').value := HorarioContactoHasta;
            //Parameters.ParamByName('@Usuario').Value := 'WEB';                  // SS_1332_CQU_20151229 //SS_1305_MCA_20150612
            if trim(NumeroOtroTelefonoContacto) <> '' then begin
                parameters.ParamByName('@TipoOtroTelefonoContacto').value := TipoOtroTelefonoContacto;
                parameters.ParamByName('@CodigoAreaOtroTelefonoContacto').value := CodigoAreaOtroTelefonoContacto;
                parameters.ParamByName('@NumeroOtroTelefonoContacto').value := NumeroOtroTelefonoContacto;
            end else begin
                parameters.ParamByName('@TipoOtroTelefonoContacto').value := NULL;
                parameters.ParamByName('@CodigoAreaOtroTelefonoContacto').value := NULL;
                parameters.ParamByName('@NumeroOtroTelefonoContacto').value := NULL;
            end;
            ExecProc;
        end;

        result := true;
    except
        on e: exception do begin
            Error := 'Error actualizando los datos de la persona: ' + e.Message;
        end;
    end;
end;
{*******************************************************************************
Revision 1
Author: mpiazza
Date:22-03-2010
Description:(Ref SS-772)-Modificar Domiclilio Particular y de Facturacion desde WEB
Se agregan las variables y campos Dpto y piso

*******************************************************************************}
procedure CargarDatosDomicilio(dm: TDMConveniosWeb; var Plantilla: string; CodigoPersona: integer);
var
    Region, //RegionElegida,
    Comuna, //ComunaElegida,
    Calle,
    CalleNoNormalizada,
    Numero,
    Detalle,
    CodigoPostal    : string;
    Dpto            : string;
    Piso            : string;
    CalleDesc       : string;
begin
    with dm.spObtenerDomicilioPrincipal do begin
        parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
        open;
        if not eof then begin
            if not eof then begin
                Region := trim(FieldByName('CodigoRegion').asstring);
                Comuna := trim(FieldByName('CodigoComuna').asstring);
                if FieldByName('CodigoCalle').IsNULL then begin
                    Calle := '-1';
                end else begin
                    Calle := trim(FieldByName('CodigoCalle').asstring);
                end;
                CalleNoNormalizada := trim(FieldByName('CalleDesnormalizada').asstring);
                Numero := trim(FieldByName('Numero').asstring);
                Detalle := trim(FieldByName('Detalle').asstring);
                CodigoPostal := trim(FieldByName('CodigoPostal').asstring);
                Dpto := trim(FieldByName('Dpto').asstring);
                Piso := trim(FieldByName('Piso').asstring);
            end;
        end;
        close;
    end;

    // domicilio 1
    Plantilla := ReplaceTag(Plantilla, 'Region', Region);
    Plantilla := ReplaceTag(Plantilla, 'Comuna', Comuna);
    Plantilla := ReplaceTag(Plantilla, 'Calle', Calle);
    Plantilla := ReplaceTag(Plantilla, 'CalleNoNormalizada', CalleNoNormalizada);
    Plantilla := ReplaceTag(Plantilla, 'Numero', Numero);
    Plantilla := ReplaceTag(Plantilla, 'Detalle', Detalle);
    Plantilla := ReplaceTag(Plantilla, 'CodigoPostal', CodigoPostal);
    Plantilla := ReplaceTag(Plantilla, 'Dpto', Dpto);
    Plantilla := ReplaceTag(Plantilla, 'Piso', Piso);

    Plantilla := ReplaceTag(Plantilla, 'ValoresRegion', GenerarComboRegiones(DM, Region, Region));
    Plantilla := ReplaceTag(Plantilla, 'ValoresComuna', GenerarComboComunas(DM, Region, Comuna, Comuna));


    Plantilla := ReplaceTag(Plantilla, 'RegionDesc', QueryGetValue(dm.base, 'SELECT Descripcion FROM Regiones  WITH (NOLOCK) WHERE CodigoPais = ' + quotedstr(CODIGO_PAIS) + ' AND CodigoRegion = ' + quotedstr(Region)));
    Plantilla := ReplaceTag(Plantilla, 'ComunaDesc', QueryGetValue(dm.base, 'SELECT Descripcion FROM Comunas  WITH (NOLOCK) WHERE CodigoPais = ' + quotedstr(CODIGO_PAIS) + ' AND CodigoRegion = ' + quotedstr(Region) + ' AND CodigoComuna = ' + quotedstr(Comuna)));


    if Calle = '-1' then begin
        // eligi� "otra" calle
        CalleDesc := CalleNoNormalizada;
    end else begin
        CalleDesc := QueryGetValue(dm.base, 'SELECT Descripcion FROM MaestroCalles  WITH (NOLOCK) WHERE CodigoPais = ' + quotedstr(CODIGO_PAIS) + ' AND CodigoRegion = ' + quotedstr(Region) + ' AND CodigoComuna = ' + quotedstr(Comuna) + ' AND CodigoCalle = ' + inttostr(strtointdef(Calle, 0)))
    end;
    Plantilla := ReplaceTag(Plantilla, 'CalleDesc', StringToHTML(CalleDesc));

end;
{*******************************************************************************
Revision 1
Author: mpiazza
Date:22-03-2010
Description:(Ref SS-772)-Modificar Domiclilio Particular y de Facturacion desde WEB
Se agregan las variables y campos Dpto y piso

Revision 2
Author: mpiazza
Date:03-05-2010
Description:(Ref SS-511)- para que no me muestre la etiqueta de #CalleDesc
            cuando recarga el java script seleccionando una nueva region se
            modifica el condicional para saber si trae resultados y
            sino devolver blancos
*******************************************************************************}
procedure CargarDatosDomicilioConvenio(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio: integer);
var
    Region, RegionElegida,
    Comuna, ComunaElegida,
    Calle,
    CalleNoNormalizada,
    Numero,
    Detalle,
    CodigoPostal: string;
    Dpto        : string;
    Piso        : string;
    CalleDesc   : string;
begin
    with dm.spObtenerDomicilioConvenio do begin
        parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        open;
        if not eof then begin
            if not eof then begin
                Region := trim(FieldByName('CodigoRegion').asstring);
                Comuna := trim(FieldByName('CodigoComuna').asstring);
                if FieldByName('CodigoCalle').IsNULL then begin
                    Calle := '-1';
                end else begin
                    Calle := trim(FieldByName('CodigoCalle').asstring);
                end;
                CalleNoNormalizada := trim(FieldByName('CalleDesnormalizada').asstring);
                Numero := trim(FieldByName('Numero').asstring);
                Detalle := trim(FieldByName('Detalle').asstring);
                CodigoPostal := trim(FieldByName('CodigoPostal').asstring);
                Dpto := trim(FieldByName('Dpto').asstring);
                Piso := trim(FieldByName('Piso').asstring);
            end;
        end;
        close;
    end;

    // domicilio 1
    Plantilla := ReplaceTag(Plantilla, 'Region', Region);
    Plantilla := ReplaceTag(Plantilla, 'Comuna', Comuna);
    Plantilla := ReplaceTag(Plantilla, 'Calle', Calle);
    Plantilla := ReplaceTag(Plantilla, 'CalleNoNormalizada', CalleNoNormalizada);
    Plantilla := ReplaceTag(Plantilla, 'Numero', Numero);
    Plantilla := ReplaceTag(Plantilla, 'Detalle', Detalle);
    Plantilla := ReplaceTag(Plantilla, 'CodigoPostal', CodigoPostal);

    Plantilla := ReplaceTag(Plantilla, 'ValoresRegion', GenerarComboRegiones(DM, Region, RegionElegida));
    Plantilla := ReplaceTag(Plantilla, 'ValoresComuna', GenerarComboComunas(DM, RegionElegida, Comuna, ComunaElegida));


    Plantilla := ReplaceTag(Plantilla, 'Dpto', Dpto);
    Plantilla := ReplaceTag(Plantilla, 'Piso', Piso);

    Plantilla := ReplaceTag(Plantilla, 'RegionDesc', QueryGetValue(dm.base, 'SELECT Descripcion FROM Regiones  WITH (NOLOCK) WHERE CodigoPais = ' + quotedstr(CODIGO_PAIS) + ' AND CodigoRegion = ' + quotedstr(Region)));
    Plantilla := ReplaceTag(Plantilla, 'ComunaDesc', QueryGetValue(dm.base, 'SELECT Descripcion FROM Comunas  WITH (NOLOCK) WHERE CodigoPais = ' + quotedstr(CODIGO_PAIS) + ' AND CodigoRegion = ' + quotedstr(Region) + ' AND CodigoComuna = ' + quotedstr(Comuna)));

    if Calle = '-1' then begin
        // eligi� "otra" calle
        CalleDesc := CalleNoNormalizada;
    end else begin
        CalleDesc := QueryGetValue(dm.base, 'SELECT Descripcion FROM MaestroCalles  WITH (NOLOCK) WHERE CodigoPais = ' + quotedstr(CODIGO_PAIS) + ' AND CodigoRegion = ' + quotedstr(Region) + ' AND CodigoComuna = ' + quotedstr(Comuna) + ' AND CodigoCalle = ' + inttostr(strtointdef(Calle, 0)))
    end;
    Plantilla := ReplaceTag(Plantilla, 'CalleDesc', StringToHTML(CalleDesc));

end;


function VerificarDatosDomicilio(DM: TdmConveniosWeb; Request: TWebRequest; var Error: string): boolean;
var
    Region,
    Ciudad,
    Comuna,
    Calle,
    CalleNoNormalizada,
    Numero,
    Detalle,
    CodigoPostal: string;
begin
    result := false;

    // cargo los valores
    with Request.ContentFields do begin
        Region := Trim(Values['Region']);
        Ciudad := Trim(Values['Ciudad']);
        Comuna := Trim(Values['Comuna']);
        Calle := Trim(Values['Calle']);
        CalleNoNormalizada := Trim(Values['CalleNoNormalizada']);
        Numero := Trim(Values['Numero']);
        Detalle := Trim(Values['Detalle']);
        CodigoPostal := Trim(Values['CodigoPostal']);
    end;

    // Valido la region
    if Region = '' then begin
        Error := MSG_DEBE_ELEGIR_REGION;
        exit;
    end;
    if not VerificarRegion(DM, Region) then begin
        Error := MSG_DATOS_ERRONEOS;
        exit;
    end;

    // valido la comuna
    if Comuna = '' then begin
        Error := MSG_DEBE_ELEGIR_COMUNA;
        exit;
    end;
    if not VerificarComuna(DM, Region, Comuna) then begin
        Error := MSG_DATOS_ERRONEOS;
        exit;
    end;

    // V�lido la calle
    if Calle = '' then begin
        Error := MSG_DEBE_ELEGIR_CALLE;
        exit;
    end;

    // V�lido el numero
    if Numero = '' then begin
        Error := MSG_DEBE_CARGAR_NUMERO;
        exit;
    end;

    if not ValidarNumeroCalle(Numero) then begin
         Error := MSG_DEBE_NUMERO_NO_VALIDO;
         exit;
    end;

    // Verificamos que sean solo letras
{  if (UpperCase(trim(numero)) <> 'S/N') then begin
       if VerificarSoloNumeros(numero) then begin
             Error := MSG_DEBE_NUMERO_NO_VALIDO;
             exit;
        end;
    end;}

    if Calle = '-1' then begin
        if CalleNoNormalizada = '' then begin
            Error := MSG_DEBE_COMPLETAR_CALLE_NO_NORMALIZADA;
            exit;
        end;
    end else begin
        if not VerificarCalle(DM, Region, Comuna, Calle) then begin
            Error := MSG_DATOS_ERRONEOS;
            exit;
        end;
      if (UpperCase(trim(numero)) <> 'S/N') then begin
            if not VerificarNumero(DM, Region, Comuna, Calle, Numero) then begin
                Error := MSG_DEBE_NUMERO_NO_VALIDO;
                exit;
            end;
        end;
    end;

    // verifico el segundo domicilio, si corresponde...
    if Request.ContentFields.Values['OtroDomicilio'] = '1' then begin
        // cargo los valores
        with Request.ContentFields do begin
            Region := Trim(Values['Region2']);
            Ciudad := Trim(Values['Ciudad2']);
            Comuna := Trim(Values['Comuna2']);
            Calle := Trim(Values['Calle2']);
            CalleNoNormalizada := Trim(Values['CalleNoNormalizada2']);
            Numero := Trim(Values['Numero2']);
            Detalle := Trim(Values['Detalle2']);
            CodigoPostal := Trim(Values['CodigoPostal2']);
        end;

        // Valido la region
        if Region = '' then begin
            Error := MSG_DEBE_ELEGIR_REGION;
            exit;
        end;
        if not VerificarRegion(DM, Region) then begin
            Error := MSG_DATOS_ERRONEOS;
            exit;
        end;

        // valido la comuna
        if Comuna = '' then begin
            Error := MSG_DEBE_ELEGIR_COMUNA;
            exit;
        end;
        if not VerificarComuna(DM, Region, Comuna) then begin
            Error := MSG_DATOS_ERRONEOS;
            exit;
        end;

        // V�lido la calle
        if Calle = '' then begin
            Error := MSG_DEBE_ELEGIR_CALLE;
            exit;
        end;

        // V�lido el numero
        if Numero = '' then begin
            Error := MSG_DEBE_CARGAR_NUMERO;
            exit;
        end;

        if not ValidarNumeroCalle(Numero) then begin
             Error := MSG_DEBE_NUMERO_NO_VALIDO;
             exit;
        end;
        // Verificamos que sean solo letras
{       if (UpperCase(trim(numero)) <> 'S/N') then begin
            if VerificarSoloNumeros(numero) then begin
                 Error := MSG_DEBE_NUMERO_NO_VALIDO;
                 exit;
            end;
        end;}

        if Calle = '-1' then begin
            if CalleNoNormalizada = '' then begin
                Error := MSG_DEBE_COMPLETAR_CALLE_NO_NORMALIZADA;
                exit;
            end;
        end else begin
            if not VerificarCalle(DM, Region, Comuna, Calle) then begin
                Error := MSG_DATOS_ERRONEOS;
                exit;
            end;
            if (UpperCase(trim(numero)) <> 'S/N') then begin
                if not VerificarNumero(DM, Region, Comuna, Calle, Numero) then begin
                    Error := MSG_DEBE_NUMERO_NO_VALIDO;
                    exit;
                end;
            end;
        end;
    end;

    result := true;
end;

function GrabarDomicilio(DM: TDMConveniosWEB; Request: TWebRequest; var CodigoDomicilio: integer; var Error: string): boolean;
var
    Calle: integer;
    CalleNoNormalizada,
    Region,
    Comuna,
    Numero,
    Detalle,
    CodigoPostal: string;
    Dpto        : string;
begin
    result := false;

    if not VerificarDatosDomicilio(DM, Request, Error) then exit;

    try
        with request.contentFields do begin
            Region := Trim(Values['Region']);
            Comuna := Trim(Values['Comuna']);
            Calle := strtoint(Trim(Values['Calle']));
            CalleNoNormalizada := Trim(Values['CalleNoNormalizada']);
            Numero := Trim(Values['Numero']);
            Detalle := Trim(Values['Detalle']);
            Dpto := Trim(Values['Dpto']);
        end;

        if Calle <> -1 then begin
            CodigoPostal := ObtenerCodigoPostal(DM, Region, Comuna, Numero, Calle);
        end else begin
            CodigoPostal := '';
        end;

        with dm.spCambiarDomicilio do begin
            parameters.ParamByName('@CodigoPais').value := CODIGO_PAIS;
            parameters.ParamByName('@CodigoRegion').value := Region;
            parameters.ParamByName('@CodigoComuna').value := Comuna;
            parameters.ParamByName('@CodigoCalle').value := Calle;
            parameters.ParamByName('@CalleDesnormalizada').value := iif(Calle > 0, NULL, CalleNoNormalizada);
            parameters.ParamByName('@Numero').value := Numero;
            parameters.ParamByName('@CodigoPostal').value := CodigoPostal;
            parameters.ParamByName('@CodigoSegmento').value := ObtenerCodigoSegmento(DM, Region, Comuna, Numero, Calle);
            parameters.ParamByName('@Detalle').value := Detalle;
            parameters.ParamByName('@CodigoDomicilio').value := CodigoDomicilio;
            parameters.ParamByName('@Depto').value := Dpto;
            if CodigoDomicilio = 0 then begin
                parameters.ParamByName('@CodigoTipoDomicilio').value := TIPO_DOMICILIO_COMERCIAL;
            end else begin
                parameters.ParamByName('@CodigoTipoDomicilio').value := NULL;
            end;
            execproc;
            // devuelvo el valor de codigo domicilio, por si se creo uno nuevo
            CodigoDomicilio := parameters.ParamByName('@CodigoDomicilio').value;
        end;

        result := true;
    except
        on e: exception do begin
            Error := 'Error actualizando los datos de domicilio: ' + e.Message;
        end;
    end;
end;
{******************************** Function Header ******************************
Function Name: GrabarCambiosDatosDomicilio
Author : mpiazza (comentario base)
Date Created : 12/04/2010
Description : Guarda los cambios de Domicilio.
Parameters : DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion: integer; Error: string
Return Value : Boolean

Revision 1
Author: mpiazza
Date:22-03-2010
Description:(Ref SS-772)-Modificar Domiclilio Particular y de Facturacion desde WEB

*******************************************************************************}
function GrabarCambiosDatosDomicilio(DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion: integer; var Error: string): boolean;
var
    CodigoPersona,
    CodigoDomicilio: integer;
begin
    result := false;

    CodigoPersona := QueryGetValueInt(dm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));

    try
        with dm.spObtenerDomicilioPrincipal do begin
            parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
            open;
            if not eof then begin
                CodigoDomicilio := FieldByName('CodigoDomicilio').asInteger;
            end else begin
                Raise(Exception.Create('El usuario no tiene domicilio principal definido.'));
            end;
            close;
        end;

        result := GrabarDomicilio(DM, Request, CodigoDomicilio, Error);

        with DM.spWEB_RegistraAuditoriaDomicilios do begin
            Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilio ;
            Parameters.ParamByName('@CodigoPersona').Value  :=  CodigoPersona ;
            Parameters.ParamByName('@Usuario').Value := 'WEB';
            ExecProc;
        end;

    except
        on e: exception do begin
            Error := 'Error actualizando los datos de domicilio: ' + e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: GrabarCambiosDatosDomicilioConvenio
Author :
Date Created :
Description :
Parameters : DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion, CodigoConvenio: integer; var Error: string
Return Value : boolean

Revision 1:
    Author : ggomez
    Date : 02/10/2006
    Description :
        - Cambi� para que al generar el archivo XML de Novedades, se
        obtenga el par�metro general que indica en donde dejar los XML, desde la
        BD del SOR.
        - Agregu� que si ocurre alg�n error en las operaciones que se realizan
        para generar el XML, se genere un evento en el SOR.
        Si registrar el evento en el SOR da error, genera un registro en los
        sucesos de windows.

*******************************************************************************}
function GrabarCambiosDatosDomicilioConvenio(DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion, CodigoConvenio: integer; var Error: string): boolean;
resourcestring                                                                                              //SS_1276_NDR_20150528
    MSG_XMLFILE_ERROR       = 'No se gener� archivo de novedades XML';                                      //SS_1276_NDR_20150528
    //MSG_ERROR_INSERT_LOG_SOR= 'Error al intentar insertar evento en la tabla Eventos.';                     //SS_1276_NDR_20150528
var
    DesdoblarDomicilio: boolean;
    CodigoPersona, CodigoDomicilio: integer;

    CodigoConcesionaria:Integer;                                                                            //SS_1276_NDR_20150528
    EsConvenioAInformarRNUT,EsConvenioSinCuenta,FEstaConvenioDeBaja:boolean;                                //SS_1276_NDR_20150528
    DescriError:  string;                                                                                   //SS_1276_NDR_20150528
    FRNUTUTF8: Integer;                                                                                     //SS_1276_NDR_20150528
    FPathNovedadXML: string;                                                                                //SS_1276_NDR_20150528
    //spEventosSOR: TADOStoredProc;                                                                           //SS_1276_NDR_20150528
    //NumeroConvenio:string;                                                                                  //SS_1276_NDR_20150528
    CodigoConcesionariaNativa:Integer;																		//SS_1276_NDR_20150528

begin
    result := false;
    CodigoConcesionariaNativa:=QueryGetValueInt(DM.Base, 'SELECT dbo.ObtenerConcesionariaNativa()');        //SS_1276_NDR_20150528
    try
        CodigoDomicilio := QueryGetValueInt(dm.base, 'SELECT CodigoDomicilioFacturacion FROM Convenio  WITH (NOLOCK) WHERE CodigoConvenio = ' + inttostr(CodigoConvenio));
        CodigoPersona := QueryGetValueInt(dm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));

        if QueryGetValueInt(dm.Base, 'SELECT COUNT(*) FROM PersonasDomicilios  WITH (NOLOCK) WHERE Principal = 1 AND Activo = 1 AND CodigoDomicilio = ' + inttostr(CodigoDomicilio)) <> 0 then begin
            // este domicilio de convenio es un domicilio principal de una persona,
            // asi que creo uno nuevo
            DesdoblarDomicilio := true;
            CodigoDomicilio := 0;
        end else begin
            // puede  ser que no sea el domicilio principal, pero por un bug del importador
            {MESSAGE WARN 'Ojo aca, revisar'}
            DesdoblarDomicilio := false;
        end;

        result := GrabarDomicilio(DM, Request, CodigoDomicilio, Error);

        // Actualizamos el la acci�n del RNUT
        with DM.WEB_CV_ActualizarConvenioAccionRNUT do begin
            parameters.ParamByName('@CodigoConvenio').value := CodigoConvenio;
            execProc;
        end;

        if DesdoblarDomicilio then begin
            // Acabo de crear un nuevo domicilio, vamos a linkearlo...
            with dm.spLinkearDomicilio do begin
                Parameters.ParamByName('@CodigoDomicilio').value := CodigoDomicilio;
                Parameters.ParamByName('@CodigoConvenio').value := CodigoConvenio;
                execproc;
            end;
        end;

        with DM.spWEB_RegistraAuditoriaDomicilios do begin
            Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilio ;
            Parameters.ParamByName('@CodigoPersona').Value  := CodigoPersona ;
            Parameters.ParamByName('@Usuario').Value := 'WEB';
            Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            ExecProc;
        end;


        EsConvenioAInformarRNUT := StrToBool(QueryGetValue(DM.Base, Format('SELECT dbo.EsConvenioAInformarRNUT(%d)', [CodigoConvenio])));                                                                                 //SS_1276_NDR_20150528
        EsConvenioSinCuenta     := StrToBool(QueryGetValue(DM.Base, Format('SELECT SinCuenta FROM Convenio WITH (NOLOCK,INDEX=PK_Convenio) WHERE CodigoConvenio=%d', [CodigoConvenio])));                                 //SS_1276_NDR_20150528
        FEstaConvenioDeBaja     := (QueryGetValueInt(DM.Base, Format('SELECT CodigoEstadoConvenio FROM Convenio WITH (NOLOCK,INDEX=PK_Convenio) WHERE CodigoConvenio=%d', [CodigoConvenio]))=ESTADO_CONVENIO_BAJA);       //SS_1276_NDR_20150528
        CodigoConcesionaria     := QueryGetValueInt(DM.Base, Format('SELECT CodigoConcesionaria FROM Convenio WITH (NOLOCK,INDEX=PK_Convenio) WHERE CodigoConvenio=%d', [CodigoConvenio]));                               //SS_1276_NDR_20150528
        //NumeroConvenio          := QueryGetValue(DM.Base, Format('SELECT NumeroConvenio FROM Convenio WITH (NOLOCK,INDEX=PK_Convenio) WHERE CodigoConvenio=%d', [CodigoConvenio]));                                       //SS_1276_NDR_20150528
        ObtenerParametroGeneral(DM.Base,'PATH_ARCHIVO_SALIDA_XML_WEB', FPathNovedadXML);                                                                                                                                  //SS_1276_NDR_20150528
        ObtenerParametroGeneral(DM.Base,'RNUT_CODIFICAR_UTF8', FRNUTUTF8);                                                                                                                                                //SS_1276_NDR_20150528

        if (CodigoConcesionaria = CodigoConcesionariaNativa ) And (not EsConvenioSinCuenta) and (not FEstaConvenioDeBaja) then                                                                                            //SS_1276_NDR_20150528
        begin                                                                                                                                                                                                             //SS_1276_NDR_20150528
          if not GenerarNovedadXML( DM.Base,                                                     //SS_1276_NDR_20150528
                                    CodigoConvenio,                                              //SS_1276_NDR_20150528
                                    CodigoConcesionariaNativa ,                                  //SS_1276_NDR_20150528
                                    FPathNovedadXML,iif(FRNUTUTF8 = 1, True, False),             //SS_1276_NDR_20150528
                                    DescriError) then                                            //SS_1276_NDR_20150528
          begin                                                                                                                                                                                                           //SS_1276_NDR_20150528
            if DescriError <> EmptyStr then                                                                                                                                                                               //SS_1276_NDR_20150528
            begin                                                                                                                                                                                                         //SS_1276_NDR_20150528
              //try                                                                                                                                                                                                         //SS_1276_NDR_20150528
              //    spEventosSOR :=  TADOStoredProc.Create(nil);                                                                                                                                                            //SS_1276_NDR_20150528
              //    spEventosSOR.Connection := DM.BaseSOR;                                                                                                                                                                  //SS_1276_NDR_20150528
              //    spEventosSOR.ProcedureName := 'CrearEvento';                                                                                                                                                            //SS_1276_NDR_20150528
              //    spEventosSOR.Parameters.Refresh;                                                                                                                                                                        //SS_1276_NDR_20150528
              //    spEventosSOR.Parameters.ParamByName('@CodigoTipoEvento').Value := 213;                                                                                                                                  //SS_1276_NDR_20150528
              //    spEventosSOR.Parameters.ParamByName('@CodigoSistema').Value := 9;                                                                                                                                       //SS_1276_NDR_20150528
              //    spEventosSOR.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;                                                                                                                          //SS_1276_NDR_20150528
              //    spEventosSOR.Parameters.ParamByName('@NombreArchivo').Value := Application.Title;                                                                                                                       //SS_1276_NDR_20150528
              //    spEventosSOR.Parameters.ParamByName('@Detalle').Value := DescriError + ' ' +                                                                                                                            //SS_1276_NDR_20150528
              //    'CONVENIO: ' + NumeroConvenio;                                                                                                                                                                          //SS_1276_NDR_20150528
              //    spEventosSOR.ExecProc;                                                                                                                                                                                  //SS_1276_NDR_20150528
              //except                                                                                                                                                                                                      //SS_1276_NDR_20150528
              //    on e: Exception do begin                                                                                                                                                                                //SS_1276_NDR_20150528
              //        AgregarLog(DM, MSG_ERROR_INSERT_LOG_SOR + ' - ' + E.Message );                                                                                                                                      //SS_1276_NDR_20150528
              //        EventLogReportEvent(elError, MSG_ERROR_INSERT_LOG_SOR + ' - ' + E.Message , '');                                                                                                                    //SS_1276_NDR_20150528
              //                                                                                                                                                                                                            //SS_1276_NDR_20150528
              //    end;                                                                                                                                                                                                    //SS_1276_NDR_20150528
              //end;                                                                                                                                                                                                        //SS_1276_NDR_20150528
              AgregarLog(DM, MSG_XMLFILE_ERROR );                                                                                                                                                                         //SS_1276_NDR_20150528
              EventLogReportEvent(elError, MSG_XMLFILE_ERROR , '');                                                                                                                                                       //SS_1276_NDR_20150528
            end;                                                                                                                                                                                                          //SS_1276_NDR_20150528
          end;                                                                                                                                                                                                            //SS_1276_NDR_20150528
        end;                                                                                                                                                                                                              //SS_1276_NDR_20150528
    except
        on e: exception do begin
            Error := 'Error actualizando los datos de domicilio: ' + e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: CargarDatosDomicilioRequest
Author : mpiazza (comentario base)
Date Created : 12/04/2010
Description : Carga los datos de domicilio segun domicilio principal
Parameters : DM: TDMConveniosWEB; Request: TWEBRequest; var Plantilla: string
Return Value : N/A

Revision 1
Author: mpiazza
Date:22-03-2010
Description:(Ref SS-772)-Modificar Domiclilio Particular y de Facturacion desde WEB
Se agregan las variables y campos Dpto y piso

*******************************************************************************}
procedure CargarDatosDomicilioRequest(DM: TDMConveniosWEB; Request: TWEBRequest; var Plantilla: string);
var
    Region, RegionElegida,
    Comuna, ComunaElegida,
    Calle,
    CalleNoNormalizada,
    Numero,
    Detalle,
    CodigoPostal: string;
    Dpto            : string;
    Piso            : string;
begin
    with Request.ContentFields do begin
        Region := Values['Region'];
        Comuna := Values['Comuna'];
        Calle := Values['Calle'];
        CalleNoNormalizada := Values['CalleNoNormalizada'];
        Numero := Values['Numero'];
        Detalle := Values['Detalle'];
        CodigoPostal := Values['CodigoPostal'];
        Dpto := Values['Dpto'];
        Piso := Values['Piso'];
    end;
    if trim(Calle) = '' then Calle := '-1';

    // domicilio 1
    Plantilla := ReplaceTag(Plantilla, 'Region', Region);
    Plantilla := ReplaceTag(Plantilla, 'Comuna', Comuna);
    Plantilla := ReplaceTag(Plantilla, 'Calle', Calle);
    Plantilla := ReplaceTag(Plantilla, 'CalleNoNormalizada', CalleNoNormalizada);
    Plantilla := ReplaceTag(Plantilla, 'Numero', Numero);
    Plantilla := ReplaceTag(Plantilla, 'Detalle', Detalle);
    Plantilla := ReplaceTag(Plantilla, 'CodigoPostal', CodigoPostal);

    Plantilla := ReplaceTag(Plantilla, 'ValoresRegion', GenerarComboRegiones(DM, Region, RegionElegida));
    Plantilla := ReplaceTag(Plantilla, 'ValoresComuna', GenerarComboComunas(DM, RegionElegida, Comuna, ComunaElegida));

    Plantilla := ReplaceTag(Plantilla, 'Dpto', StringToHTML(Dpto));
    Plantilla := ReplaceTag(Plantilla, 'Piso', StringToHTML(Piso));

    Plantilla := ReplaceTag(Plantilla, 'CalleDesc', CalleNoNormalizada);

end;

procedure CargarDatosMedioPagoConvenio(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, TipoMedioPago: integer);
var
    NroDocMedioPago,
    NombreMedioPago,
    CodigoAreaTelefonoMedioPago,
    TelefonoMedioPago,
    TipoTarjeta,
    CC,
    CC1, CC2, CC3, CC4,
    Vencimiento,
    VencimientoMes, VencimientoAnio,
    TipoCuenta,
    Banco,
    NroCuenta,
    Sucursal: string;
    SinVerificar: boolean;
    EmisorTarjeta: integer;
begin
    NroDocMedioPago := '';
    NombreMedioPago := '';
    TelefonoMedioPago := '';
    CodigoAreaTelefonoMedioPago := '';
    TipoCuenta := '';
    Banco := '';
    NroCuenta := '';
    Sucursal := '';
    EmisorTarjeta := 0;
    TipoTarjeta := '';
    CC1 := '';
    CC2 := '';
    CC3 := '';
    CC4 := '';
    VencimientoMes := '';
    VencimientoAnio := '';
    SinVerificar := false;
    if TipoMedioPago = 2 then begin
        with dm.spObtenerDatosPAC do begin
            parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            open;
            if not eof then begin
                TipoCuenta := trim(FieldByName('CodigoTipoCuentaBancaria').asstring);
                Banco := trim(FieldByName('CodigoBanco').asstring);
                NroCuenta := trim(FieldByName('NroCuentaBancaria').asstring);
                Sucursal := trim(FieldByName('Sucursal').asstring);
                //REV.11 SinVerificar := FieldByName('Confirmado').AsString <> '1';
                SinVerificar := not FieldByName('Confirmado').AsBoolean;
                NroCuenta := StringOfChar('X', Length(NroCuenta));
                //FIN REV.11
            end;
            close;
        end;
    end;

    if TipoMedioPago = 1 then begin
        with dm.spObtenerDatosPAT do begin
            parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            open;
            if not eof then begin
                TipoTarjeta := FieldByName('CodigoTipoTarjetaCredito').AsString;
                CC := FieldByName('NumeroTarjetaCredito').AsString;
                CC1 := trim(copy(CC, 1, 4));
                CC2 := trim(copy(CC, 5, 4));
                CC3 := trim(copy(CC, 9, 4));
                CC4 := trim(copy(CC, 13, 4));
                EmisorTarjeta := FieldByName('CodigoEmisorTarjetaCredito').AsInteger;
                Vencimiento := FieldByName('FechaVencimiento').AsString;
                VencimientoMes := ParseParamByNumber(Vencimiento, 1, '/');
                VencimientoAnio := ParseParamByNumber(Vencimiento, 2, '/');
                SinVerificar := FieldByName('Confirmado').AsString <> '1';
            end;
            close;
        end;
    end;

    with dm.spObtenerDatosMandante do begin
        parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        open;
        if not eof then begin
             NroDocMedioPago := trim(FieldByName('NumeroDocumento').asstring);
             NombreMedioPago := trim(FieldByName('Nombre').asstring);
             TelefonoMedioPago := trim(FieldByName('Telefono').asstring);
             CodigoAreaTelefonoMedioPago := trim(FieldByName('CodigoArea').asstring);
        end;
        close;
    end;

    Plantilla := ReplaceTag(Plantilla, 'TipoTarjeta', TipoTarjeta);
    Plantilla := ReplaceTag(Plantilla, 'CC1', CC1);
    Plantilla := ReplaceTag(Plantilla, 'CC2', CC2);
    Plantilla := ReplaceTag(Plantilla, 'CC3', CC3);
    Plantilla := ReplaceTag(Plantilla, 'CC4', CC4);
    Plantilla := ReplaceTag(Plantilla, 'VencimientoMes', VencimientoMes);
    Plantilla := ReplaceTag(Plantilla, 'VencimientoAnio', VencimientoAnio);
    Plantilla := ReplaceTag(Plantilla, 'TipoCuenta', TipoCuenta);
    Plantilla := ReplaceTag(Plantilla, 'Banco', Banco);
    Plantilla := ReplaceTag(Plantilla, 'NroCuenta', NroCuenta);
    Plantilla := ReplaceTag(Plantilla, 'Sucursal', Sucursal);
    Plantilla := ReplaceTag(Plantilla, 'EmisorTarjeta', inttostr(EmisorTarjeta));

    Plantilla := ReplaceTag(Plantilla, 'NroDocMedioPago', NroDocMedioPago);
    Plantilla := ReplaceTag(Plantilla, 'NombreMedioPago', NombreMedioPago);
    Plantilla := ReplaceTag(Plantilla, 'TelefonoMedioPago', TelefonoMedioPago);
    Plantilla := ReplaceTag(Plantilla, 'CodigoAreaTelefonoMedioPago', CodigoAreaTelefonoMedioPago);

    // para mostrar los datos
    if strtointdef(TipoTarjeta, -1) > 0 then begin
        Plantilla := ReplaceTag(Plantilla, 'TipoTarjetaDesc', QueryGetValue(dm.base, 'SELECT Descripcion FROM TarjetasCreditoTipos  WITH (NOLOCK) WHERE CodigoTipoTarjetaCredito = ' + TipoTarjeta));
    end else begin
        Plantilla := ReplaceTag(Plantilla, 'TipoTarjetaDesc', '');
    end;

    if strtointdef(TipoCuenta, -1) > 0 then begin
        Plantilla := ReplaceTag(Plantilla, 'TipoCuentaDesc', QueryGetValue(dm.base, 'SELECT Descripcion FROM TiposCuentasBancarias  WITH (NOLOCK) WHERE CodigoTipoCuentaBancaria = ' + TipoCuenta));
    end else begin
        Plantilla := ReplaceTag(Plantilla, 'TipoCuentaDesc', '');
    end;

    if strtointdef(Banco, -1) > 0 then begin
        Plantilla := ReplaceTag(Plantilla, 'BancoDesc', QueryGetValue(dm.base, 'SELECT Descripcion FROM Bancos  WITH (NOLOCK) WHERE CodigoBanco = ' + Banco));
    end else begin
        Plantilla := ReplaceTag(Plantilla, 'BancoDesc', '');
    end;

     case TipoMedioPago of
        -1: Plantilla := ReplaceTag(Plantilla, 'MedioDePagoPreferido', 'No Seleccionado');
        0: Plantilla := ReplaceTag(Plantilla, 'MedioDePagoPreferido', 'Ninguno');
        1: Plantilla := ReplaceTag(Plantilla, 'MedioDePagoPreferido', iif(SinVerificar, 'PAT (No Confirmado)', 'PAT'));
        2: Plantilla := ReplaceTag(Plantilla, 'MedioDePagoPreferido', iif(SinVerificar, 'PAC (No Confirmado)', 'PAC'));
    end;

    Plantilla := ReplaceTag(Plantilla, 'TipoMedioPago', inttostr(TipoMedioPago));

    if DateUtils.IsValidDate(strtointdef(VencimientoAnio, -1), strtointdef(VencimientoMes, -1), 1) then begin
        Plantilla := ReplaceTag(Plantilla, 'FechaVencimiento', VencimientoMes + '/' + VencimientoAnio);
    end else begin
        Plantilla := ReplaceTag(Plantilla, 'FechaVencimiento', '');
    end;

    if SinVerificar then begin
        Plantilla := ReplaceTag(Plantilla, 'EstadoVerificacion', '(No Confirmado)');
    end else begin
        Plantilla := ReplaceTag(Plantilla, 'EstadoVerificacion', '');
    end;
end;

//Revision 1
//24/08/2007
//Lgisuk
//Mostraba los codigos en lugar de la descripcion

procedure CargarDatosVehiculosConvenio(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, CodigoPersona: integer);

    function SetCombo(const Combo, valor: string): string;
    begin
        result := stringReplace(Combo, 'VALUE="' + valor + '"', 'VALUE="' + valor + '" selected', [rfIgnoreCase]);
    end;

var
    i: integer;
    sufijo, //NumeroConvenio,
    Patente, Modelo, Anio,
    CodigoVehiculo,
    Inicio, Repeticion, Fin, temp,
    MarcaDesc, TipoVehiculoDesc,
    Marca, TipoVehiculo: string;
//    ComboMarcas, ComboTiposVehiculos: string;
begin
//    NumeroConvenio := QueryGetValue(dm.Base, 'SELECT NumeroConvenio FROM Convenio  WITH (NOLOCK) WHERE CodigoConvenio = ' + inttostr(CodigoConvenio));
//    ComboMarcas := GenerarComboMarcas(DM, '');
//    ComboMarcas := stringreplace(ComboMarcas, 'selected', '', []);

//    ComboTiposVehiculos := GenerarComboTipoVehiculos(DM, '');
//    ComboTiposVehiculos := stringreplace(ComboTiposVehiculos, 'selected', '', []);

    SepararPartes(Plantilla, '#RepeticionInicio#', '#RepeticionFin#', Inicio, Repeticion, Fin);
    Plantilla := Inicio;


    with dm.spObtenerVehiculosConvenio do begin
        parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
        parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        open;

        i := 0;
        while not eof do begin
            temp := repeticion;
            inc(i);
            sufijo := inttostr(i);

            Patente := '';
            Modelo := '';
            Anio := '';
            Marca := '';
            TipoVehiculo := '';

            Patente := trim(FieldByName('Patente').AsString);
            Modelo := trim(FieldByName('Modelo').AsString);
            Anio := FieldByName('AnioVehiculo').AsString;
            Marca := FieldByName('CodigoMarca').AsString;
            TipoVehiculo := FieldByName('CodigoTipoVehiculo').AsString;
            CodigoVehiculo := FieldByName('CodigoVehiculo').AsString;
            MarcaDesc := FieldByName('MarcaDesc').AsString;
            TipoVehiculoDesc := FieldByName('TipoVehiculoDesc').AsString;

            Temp := ReplaceTag(Temp, 'Nro', Sufijo);

            Temp := ReplaceTag(Temp, 'CodigoVehiculo' + Sufijo, CodigoVehiculo);
            Temp := ReplaceTag(Temp, 'Patente' + Sufijo, Patente);
            Temp := ReplaceTag(Temp, 'Modelo' + Sufijo, Modelo);
            Temp := ReplaceTag(Temp, 'Anio' + Sufijo, Anio);
            Temp := ReplaceTag(Temp, 'Marca' + Sufijo, Marca);
            Temp := ReplaceTag(Temp, 'TipoVehiculo' + Sufijo, TipoVehiculo);
            Temp := ReplaceTag(Temp, 'MarcaDesc' + Sufijo, MarcaDesc);
            Temp := ReplaceTag(Temp, 'TipoVehiculoDesc' + Sufijo, TipoVehiculoDesc);

//            Temp := ReplaceTag(Temp, 'ValoresMarca' + Sufijo, SetCombo(ComboMarcas, Marca));
//            Temp := ReplaceTag(Temp, 'ValoresTipoVehiculo' + Sufijo, SetCombo(ComboTiposVehiculos, TipoVehiculo));

{            if strtointdef(Marca, -1) > 0 then begin
                Temp := ReplaceTag(Temp, 'MarcaDesc' + Sufijo, QueryGetValue(dm.Base, 'SELECT Descripcion FROM VehiculosMarcas  WITH (NOLOCK) WHERE CodigoMarca = ' + Marca));
            end else begin
                Temp := ReplaceTag(Temp, 'MarcaDesc' + Sufijo, '');
            end;
            if strtointdef(TipoVehiculo, -1) > 0 then begin
                Temp := ReplaceTag(Temp, 'TipoVehiculoDesc' + Sufijo, QueryGetValue(dm.Base, 'SELECT Descripcion FROM VehiculosTipos  WITH (NOLOCK) WHERE CodigoTipoVehiculo = ' + TipoVehiculo));
            end else begin
                Temp := ReplaceTag(Temp, 'TipoVehiculoDesc' + Sufijo, '');
            end;
}
            next;
            Plantilla := Plantilla + temp;
        end;

        close;
    end;
    Plantilla := ReplaceTag(Plantilla, 'CantidadVehiculos', inttostr(i));
    Plantilla := Plantilla + Fin;
end;

function VerificarDatosVehiculos(DM: TdmConveniosWeb; Request: TWebRequest; var Error: string): boolean;
var
    Marca,
    TipoVehiculo,
    Modelo,
    Anio,
    Sufijo: string;
    i, CantidadVehiculos: integer;
begin
    result := false;
    CantidadVehiculos := StrToInt(Request.ContentFields.values['CantidadVehiculos']);

    for i := 1 to CantidadVehiculos do begin
        Sufijo := inttostr(i);
        with Request.ContentFields do begin
            Modelo       := Trim(Values['Modelo' + Sufijo]);
            Anio         := Trim(Values['Anio' + Sufijo]);
            Marca        := Trim(Values['Marca' + Sufijo]);
            TipoVehiculo := Trim(Values['TipoVehiculo' + Sufijo]);
        end;

        // verifico la marca
        if Marca = '' then begin
            Error := MSG_DEBE_ELEGIR_MARCA;
            exit;
        end;
        if not VerificarMarca(DM, Marca) then begin
            Error := MSG_DATOS_ERRONEOS;
            exit;
        end;

        // verifico el tipo de veh�culo
        if TipoVehiculo = '' then begin
            Error := MSG_DEBE_ELEGIR_TIPO_VEHICULO;
            exit;
        end;
        if not VerificarTipoVehiculo(DM, TipoVehiculo) then begin
            Error := MSG_DATOS_ERRONEOS;
            exit;
        end;

        // Verificar a�o del vehiculo
        if not VerificarSoloNumeros(Anio) then begin
            Error := MSG_ANIO_INCORRECTO;
            exit;
        end;

        if not VerificarAnio(Anio) then begin
            Error := MSG_ANIO_FUERA_RANGO;
            exit;
        end;
    end;
    result := true;
end;

function GrabarCambiosVehiculos(DM: TDMConveniosWEB; Request: TWebRequest; CodigoSesion, CodigoConvenio: integer; var Error: string): boolean;
var
    Anio, CodigoVehiculo,
    TipoVehiculo, Marca,
    CantidadVehiculos, i: integer;
    Modelo: string;
begin
    result := false;
    if not VerificarDatosVehiculos(DM, Request, Error) then exit;

    try
        CantidadVehiculos := StrToInt(Request.ContentFields.values['CantidadVehiculos']);

        for i := 1 to CantidadVehiculos do begin
            CodigoVehiculo := strtoint(Request.ContentFields.values['CodigoVehiculo' + inttostr(i)]);
            Anio := strtoint(Request.ContentFields.values['Anio' + inttostr(i)]);
            Modelo := Request.ContentFields.values['Modelo' + inttostr(i)];
            TipoVehiculo := strtoint(Request.ContentFields.values['TipoVehiculo' + inttostr(i)]);
            Marca := strtoint(Request.ContentFields.values['Marca' + inttostr(i)]);

            with dm.spActualizarVehiculoConvenio do begin
                parameters.parambyname('@CodigoConvenio').value := CodigoConvenio;
                parameters.parambyname('@CodigoVehiculo').value := CodigoVehiculo;
                parameters.parambyname('@TipoVehiculo').value := TipoVehiculo;
                parameters.parambyname('@Marca').value := Marca;
                parameters.parambyname('@Anio').value := Anio;
                parameters.parambyname('@Modelo').value := Modelo;
                execproc;
            end;

        end;
        result := true;
    except
        on e: exception do begin
            Error := 'Error actualizando los datos de vehiculos: ' + e.Message;
        end;
    end;
end;

function EsConvenioCarpetaLegal(dm: TDMConveniosWeb; CodigoConvenio: integer; var Error: string): Boolean;
begin
    Result := False;

    if QueryGetValue(dm.Base, format('SELECT DBO.ConvenioEstaEnCarpetaLegal(%d)',[CodigoConvenio])) = 'True' then begin
      Result :=True;
      Error := 'El Convenio se encuentra en Proceso de Legales.';
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: AgregarLog
  Author:    lgisuk
  Date Created: 15/09/2006
  Description: Registro en el log
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
Procedure AgregarLog(dm: TDMConveniosWeb; Observaciones : String);
begin
    try
        with dm.spAgregarLog do begin
            Parameters.ParamByName('@Observaciones').Value := Observaciones;
            Execproc;
        end;
    except
       on E : Exception do begin
            EventLogReportEvent(elError, 'AgregarLog - ' + E.Message, '');
       end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AgregarAuditoria
  Author:    lgisuk
  Date Created: 11/09/2006
  Description: Creo registro al ingresar a la pantalla de prepago
  Parameters:
  Return Value: String
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 25/10/2006
 Description: Ahora se envia el codigo de banco en lugar del nombre del
              Banco, ahora si hay una excepcion es manejada por el evento
              que la llama.
--------------------------------------------------------------------------------
Revision : 1
    Author : ggomez
    Date : 16/01/2007
    Description : Modifiqu� para que sea funci�n para que retorne el identificador
        de la transacci�n de pago que se est� registrando en auditor�a (IDTRX).
-------------------------------------------------------------------------------
Revision : 2
    Author : ggomez
    Date : 17/01/2007
    Description : Agregu� el par�metro CodigoSesionWeb para poder almacenarlo en
        auditor�a, para luego recuperarlo para verificar si la sesi�n web a�n
        est� vigente.
------------------------------------------------------------------------------
Revision : 3
    Author : lgisuk
    Date : 13/07/2007
    Description : Ahora si el stored procedure retorna un valor distinto de cero
    lanzamos una excepcion.
-----------------------------------------------------------------------------}
function AgregarAuditoria(dm: TDMConveniosWeb; TipoComprobante : String; NumeroComprobante : Int64; Importe : INT64; CodigoBanco : Integer; CodigoSesionWeb: Integer): String;
ResourceString
    MSG_ERROR_AGREGAR_AUDITORIA = 'Error al agregar en auditoria - Comprobante : ';
begin
    //Creo el registro de auditoria
    with dm.sp_WEB_CV_AgregarAuditoria do begin
        Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
        Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
        Parameters.ParamByName('@Importe').Value := Importe;
        Parameters.ParamByName('@CodigoBanco').Value := CodigoBanco;
        Parameters.ParamByName('@CodigoSesionWeb').Value := CodigoSesionWeb;
        Parameters.ParamByName('@IDTRX').Value := Null;
        ExecProc;
        if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
            Raise Exception.Create(MSG_ERROR_AGREGAR_AUDITORIA + IntToStr(NumeroComprobante));
        end;
        Result := Parameters.ParamByName('@IDTRX').Value;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarConfirmacionAuditoria
  Author:    lgisuk
  Date Created: 11/09/2006
  Description: Registro la confirmacion del banco
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 25/10/2006
 Description: Ahora se envia el codigo de banco en lugar del nombre del
              Banco y la confirmacion del banco Aceptado / Rechazado
              se guarda como un BIT, si hay una excepcion es manejada por
              el evento que la llama.
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 20/11/2006
 Description: Ahora si se produce un error es manejado en el procedimiento
              y regitrado en el log. guardo informacion de la operacion
              a realizar para que si sucede un error podamos identificar
              con mas certeza el motivo del fallo
-----------------------------------------------------------------------------}
//Procedure ActualizarConfirmacionAuditoria(dm: TDMConveniosWeb; IdTrx : String; CodigoBanco : Integer; CodigoRespuesta : String; ConfirmacionBanco : Boolean);
function ActualizarConfirmacionAuditoria(dm: TDMConveniosWeb; IdTrx : String; CodigoBanco : Integer; CodigoRespuesta : String; ConfirmacionBanco : Boolean): Boolean;
const
    STR_TRANSACCION = 'Transaccion: ';
    STR_RESPUESTA   = 'Respuesta Banco: ';
var
    IdentificadorOperacion : String;
begin
    //Armo una linea con la informacion de la operacion a realizar, asi si sucede
    //un error podremos identificar con mas certeza la operacion que fallo y el
    //motivo del fallo
    IdentificadorOperacion := STR_TRANSACCION + IdTrx + ' ' + STR_RESPUESTA + CodigoRespuesta;
    Result := False;
    //Actualizo el registro de auditoria
    try
        with dm.sp_WEB_CV_ActualizarConfirmacionAuditoria do begin
            Parameters.ParamByName('@IDTRX').Value := IdTrx;
            Parameters.ParamByName('@CodigoBanco').Value := CodigoBanco;
            Parameters.ParamByName('@CodigoRespuesta').Value := CodigoRespuesta;
            Parameters.ParamByName('@ConfirmacionBanco').Value := ConfirmacionBanco;
            Execproc;
            Result := (Parameters.ParamByName('@Return_Value').Value <> CODIGO_ERROR_MENSAJE_DUPLICADO);
        end;
    except
        on E : Exception do begin
            AgregarLog(DM, 'ActualizarConfirmacionAuditoria - ' + E.Message + ' ' + IdentificadorOperacion);
            EventLogReportEvent(elError, 'ActualizarConfirmacionAuditoria - ' + E.Message + ' ' + IdentificadorOperacion, '');
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarRespuestaBancoAuditoria
  Author:    lgisuk
  Date Created: 10/11/2006
  Description: Registro el mensaje recibido del banco
  Parameters:
  Return Value: Boolean
--------------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 16/11/2006
 Description: Ahora verifico si la operacion de actualizacion tuvo exito
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 20/11/2006
 Description: Ahora si se produce un error al hacer esta actualizacion, es
              manejada en esta rutina, de modo que se pueda continuar con la
              registracion del pago. guardo informacion de la operacion
              a realizar para que si sucede un error podamos identificar
              con mas certeza el motivo del fallo.
 Revision :1
     Author : vpaszkowicz
     Date : 22/11/2007
     Description : Capturo los c�digos de error. Si es MensajeDuplicado
     quiere decir que el banco me envi� el mensaje 2 veces, si deseo
-----------------------------------------------------------------------------}
function ActualizarRespuestaBancoAuditoria(dm: TDMConveniosWeb; IdTrx : String; RespuestaBanco : String; CodigoBanco : Integer): Boolean;
const
    STR_TRANSACCION = 'Transaccion: ';
    STR_RESPUESTA   = 'Respuesta Banco: ';
var
    IdentificadorOperacion : String;
begin
    Result := False;
    //Armo una linea con la informacion de la operacion a realizar, asi si sucede
    //un error podremos identificar con mas certeza la operacion que fallo y el
    //motivo del fallo
    IdentificadorOperacion := STR_TRANSACCION + IdTrx + ' ' + STR_RESPUESTA + RespuestaBanco;

    //Actualizo el registro de auditoria
    try
        with dm.SPWEB_CV_ActualizarRespuestaBancoAuditoria do begin
            Parameters.ParamByName('@IDTRX').Value := IdTrx;
            Parameters.ParamByName('@CodigoBanco').Value := CodigoBanco;
            Parameters.ParamByName('@RespuestaBanco').Value := RespuestaBanco;
            Execproc;
            result := (Parameters.ParamByName('@RETURN_VALUE').value <> CODIGO_ERROR_MENSAJE_DUPLICADO);
        end;
    except
        on E : Exception do begin
            AgregarLog(DM, 'ActualizarRespuestaBancoAuditoria - ' + E.Message + ' ' + IdentificadorOperacion);
            EventLogReportEvent(elError, 'ActualizarRespuestaBancoAuditoria - ' + E.Message + ' ' + IdentificadorOperacion, '');
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarGeneroReciboAuditoria
  Author:    lgisuk
  Date Created: 11/09/2006
  Description: Registro si se genero el recibo auditoria
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 25/10/2006
 Description: Ahora se envia el codigo de banco en lugar del nombre del
              Banco
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 20/11/2006
 Description: guardo informacion de la operacion
              a realizar para que si sucede un error podamos identificar
              con mas certeza el motivo del fallo.
-----------------------------------------------------------------------------}
Procedure ActualizarGeneroReciboAuditoria(dm: TDMConveniosWeb; IdTrx : String; GeneroRecibo : Boolean; Observaciones : String; CodigoBanco : Integer);
const
    STR_TRANSACCION = 'Transaccion: ';
    STR_OBSERVACIONES   = 'Respuesta Banco: ';
var
    IdentificadorOperacion : String;
begin

    //Armo una linea con la informacion de la operacion a realizar, asi si sucede
    //un error podremos identificar con mas certeza la operacion que fallo y el
    //motivo del fallo
    IdentificadorOperacion := STR_TRANSACCION + IdTrx + ' ' + STR_OBSERVACIONES + Observaciones;

    //Actualizo el registro de auditoria
    try
        with dm.sp_WEB_ActualizarGeneroReciboAuditoria do begin
            Parameters.ParamByName('@IDTRX').Value := IdTrx;
            Parameters.ParamByName('@GeneroRecibo').Value := GeneroRecibo;
            Parameters.ParamByName('@Observaciones').Value := Observaciones;
            Parameters.ParamByName('@CodigoBanco').Value := CodigoBanco;
            Execproc;
        end;
    except
        on E : Exception do begin
            AgregarLog(DM, 'ActualizarGeneroReciboAuditoria - ' + E.Message + ' ' + IdentificadorOperacion);
            EventLogReportEvent(elError, 'ActualizarGeneroReciboAuditoria - ' + E.Message + ' ' + IdentificadorOperacion, '');
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: Auditoria_ActualizarRespuestaAConfirmacion
Author : ggomez
Date Created : 13/01/2007
Description: Actualiza la auditor�a con la respuesta enviada hacia el ente recaudador
    debido a la llegada de la confirmaci�n del pago por parte del ente.
    Este procedimiento debe ser utilizado para dejar registro del momento en el
    que se env�a la respuesta al ente en la cual se indica ACEPTADO o RECHAZO.
    Esto se hizo para poder obtener luego el tiempo que transcurre entre la
    llegada de la confirmaci�n del pago por parte del ente recaudador y el momento
    en el que respondemos a esa confirmaci�n.
Parameters : DM: TDMConveniosWeb; IdTrx : String; RespuestaEnviada: String
Return Value : None
*******************************************************************************}
procedure Auditoria_ActualizarRespuestaAConfirmacion(DM: TDMConveniosWeb; IdTrx : String;
    CodigoBanco : Integer; RespuestaEnviada: String);
const
    STR_TRANSACCION = 'Transacci�n: ';
var
    IdentificadorOperacion : String;
begin

    // Armo una linea con la informacion de la operacion a realizar, asi si sucede
    // un error podremos identificar con mas certeza la operacion que fallo y el
    // motivo del fallo
    IdentificadorOperacion := STR_TRANSACCION + IdTrx;

    //Actualizo el registro de auditoria
    try
        DM.sp_WEB_CV_Auditoria_ActualizarRespuestaAConfirmacion.Parameters.ParamByName('@IDTRX').Value := IdTrx;
        DM.sp_WEB_CV_Auditoria_ActualizarRespuestaAConfirmacion.Parameters.ParamByName('@CodigoBanco').Value := CodigoBanco;
        DM.sp_WEB_CV_Auditoria_ActualizarRespuestaAConfirmacion.Parameters.ParamByName('@RespuestaAConfirmacionBanco').Value := RespuestaEnviada;
        DM.sp_WEB_CV_Auditoria_ActualizarRespuestaAConfirmacion.ExecProc;
    except
        on E : Exception do begin
            AgregarLog(DM, 'Auditoria_ActualizarRespuestaAConfirmacion - ' + E.Message + ' ' + IdentificadorOperacion);
            EventLogReportEvent(elError, 'Auditoria_ActualizarRespuestaAConfirmacion - ' + E.Message + ' ' + IdentificadorOperacion, '');
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ParseXMLField
  Author:    lgisuk
  Date Created: 03/08/2006
  Description: Obtengo el valor de un campo XML
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function ParseXMLField( Xml, Campo : String) : String;
begin
    xml := trim(xml);
    campo := trim(campo);
    Result := copy(XML,pos('<'+Campo+'>', xml) + length('<'+Campo+'>'), pos('</'+campo+'>', xml) - pos('<'+campo+'>', xml) - length('<'+campo+'>'));
end;

{-----------------------------------------------------------------------------
  Function Name:  AgregarAuditoriaBDP_MPINI
  Author:    lgisuk
  Date Created: 03/10/2006
  Description: registro la entrada al banco
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 25/10/2006
 Description: Ahora registra en la tabla Web_cv_Auditoria_BDP en lugar de
              RendicionesBDP, ahora si hay una excepcion es manejada por
              el evento que la llama.

Revision : 1
    Author : ggomez
    Date : 16/01/2007
    Description : Agregu� el par�metro IDTRX.
              
-----------------------------------------------------------------------------}
//Procedure AgregarAuditoriaBDP_MPINI(dm: TDMConveniosWeb; Request: TWebRequest; IDTRX : String);																		//SS_1356_MCA_20150903
Procedure AgregarAuditoriaBDP_MPINI(dm: TDMConveniosWeb; IDTRX, IDCOM, SRVREC, GLOSA : String; MONTO, TOTAL, PRECIO, DATOADIC: Int64; NROPAGOS, CANTIDAD:Integer);		//SS_1356_MCA_20150903
begin

    with dm.spAgregarAuditoriaBDP_MPINI do begin
        parameters.ParamByName('@IDTRX').Value := IDTRX;															//SS_1356_MCA_20150903
        parameters.ParamByName('@IDCOM').Value := IDCOM;															//SS_1356_MCA_20150903
        parameters.ParamByName('@TOTAL').Value := TOTAL;															//SS_1356_MCA_20150903
        parameters.ParamByName('@NROPAGOS').Value := NROPAGOS;														//SS_1356_MCA_20150903
        parameters.ParamByName('@SRVREC').Value := SRVREC;															//SS_1356_MCA_20150903
        parameters.ParamByName('@MONTO').Value := MONTO;															//SS_1356_MCA_20150903
        parameters.ParamByName('@GLOSA').Value := GLOSA;															//SS_1356_MCA_20150903
        parameters.ParamByName('@CANTIDAD').Value := CANTIDAD;														//SS_1356_MCA_20150903
        parameters.ParamByName('@PRECIO').Value := PRECIO;															//SS_1356_MCA_20150903
        parameters.ParamByName('@DATOADIC').Value := DATOADIC;														//SS_1356_MCA_20150903
		//parameters.ParamByName('@IDCOM').Value := ParseXMLField(Request.ContentFields.Text,'IDCOM');				//SS_1356_MCA_20150903
        //parameters.ParamByName('@TOTAL').Value := ParseXMLField(Request.ContentFields.Text,'TOTAL');				//SS_1356_MCA_20150903
        //parameters.ParamByName('@NROPAGOS').Value := ParseXMLField(Request.ContentFields.Text,'NROPAGOS');		//SS_1356_MCA_20150903
        //parameters.ParamByName('@SRVREC').Value := ParseXMLField(Request.ContentFields.Text,'SRVREC');			//SS_1356_MCA_20150903
        //parameters.ParamByName('@MONTO').Value := ParseXMLField(Request.ContentFields.Text,'MONTO');				//SS_1356_MCA_20150903
        //parameters.ParamByName('@GLOSA').Value := ParseXMLField(Request.ContentFields.Text,'GLOSA');				//SS_1356_MCA_20150903
        //parameters.ParamByName('@CANTIDAD').Value := ParseXMLField(Request.ContentFields.Text,'CANTIDAD');		//SS_1356_MCA_20150903
        //parameters.ParamByName('@PRECIO').Value := ParseXMLField(Request.ContentFields.Text,'PRECIO');			//SS_1356_MCA_20150903
        //parameters.ParamByName('@DATOADIC').Value := ParseXMLField(Request.ContentFields.Text,'DATOADIC');		//SS_1356_MCA_20150903
        Execproc;
    end;
    
end;

{-----------------------------------------------------------------------------
  Function Name:  AgregarAuditoriaBDP_MPOUT
  Author:    lgisuk
  Date Created: 03/10/2006
  Description: registro la notificacion del pago
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 25/10/2006
 Description: Ahora registra en la tabla Web_cv_Auditoria_BDP en lugar de
              RendicionesBDP, ahora si hay una excepcion es manejada por
              el evento que la llama.
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 20/11/2006
 Description: Ahora si se produce un error al hacer esta actualizacion, es
              manejada en esta rutina, de modo que no impida la
              respuesta al banco.
-----------------------------------------------------------------------------}
Procedure AgregarAuditoriaBDP_MPOUT(dm: TDMConveniosWeb; Request: TWebRequest);
begin
    try
        with dm.spAgregarAuditoriaBDP_MPOUT do begin
            parameters.ParamByName('@IDTRX').Value := ParseXMLField(Request.ContentFields.Text,'IDTRX');
            parameters.ParamByName('@CODRET').Value := ParseXMLField(Request.ContentFields.Text,'CODRET');
            parameters.ParamByName('@DESCRET').Value := ParseXMLField(Request.ContentFields.Text,'DESCRET');
            parameters.ParamByName('@IDCOM').Value := ParseXMLField(Request.ContentFields.Text,'IDCOM');
            parameters.ParamByName('@TOTAL').Value := ParseXMLField(Request.ContentFields.Text,'TOTAL');
            parameters.ParamByName('@NROPAGOS').Value := ParseXMLField(Request.ContentFields.Text,'NROPAGOS');
            parameters.ParamByName('@FECHATRX').Value := ParseXMLField(Request.ContentFields.Text,'FECHATRX');
            parameters.ParamByName('@FECHACONT').Value := ParseXMLField(Request.ContentFields.Text,'FECHACONT');
            parameters.ParamByName('@NUMCOMP').Value := ParseXMLField(Request.ContentFields.Text,'NUMCOMP');
            parameters.ParamByName('@IDREG').Value := ParseXMLField(Request.ContentFields.Text,'IDREG');
            Execproc;
        end;
     except
        on E : Exception do begin
            AgregarLog(DM, 'AgregarAuditoriaBDP_MPOUT - ' + E.Message);
            EventLogReportEvent(elError, 'AgregarAuditoriaBDP_MPOUT - ' + E.Message, '');
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name:  AgregarAuditoriaBDP_MPFIN
  Author:    lgisuk
  Date Created: 03/10/2006
  Description: registro la notificacion de salida
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 25/10/2006
 Description: Ahora registra en la tabla Web_cv_Auditoria_BDP en lugar de
              RendicionesBDP
-----------------------------------------------------------------------------}
Procedure AgregarAuditoriaBDP_MPFIN(dm: TDMConveniosWeb; Request: TWebRequest);
begin
    try
        with dm.spAgregarAuditoriaBDPMPFIN do begin
            parameters.ParamByName('@IDTRX').Value := ParseXMLField(Request.ContentFields.Text,'IDTRX');
            parameters.ParamByName('@CODRET').Value := ParseXMLField(Request.ContentFields.Text,'CODRET');
            parameters.ParamByName('@NROPAGOS').Value := ParseXMLField(Request.ContentFields.Text,'NROPAGOS');
            parameters.ParamByName('@TOTAL').Value := ParseXMLField(Request.ContentFields.Text,'TOTAL');
            parameters.ParamByName('@INDPAGO').Value := ParseXMLField(Request.ContentFields.Text,'INDPAGO');
            parameters.ParamByName('@IDREG').Value := ParseXMLField(Request.ContentFields.Text,'IDREG');
            Execproc;
        end;
    except
        on E : Exception do begin
            AgregarLog(DM, 'AgregarAuditoriaBDPMPFIN - ' + E.Message);
            EventLogReportEvent(elError, 'AgregarAuditoriaBDPMPFIN - ' + E.Message, '');
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure Name: AgregarEmailEnvioClave
  Author: nefernandez
  Date Created: 14/05/2007
  Description: Se registro en la base de datos el env�o de email al generar la clave
  Parameters: DM, CodigoMensaje, Email, Asunto, CodigoPersona, Usuario
  Return Value: CodigoMensaje
-----------------------------------------------------------------------------}
procedure AgregarEmailEnvioClave(DM: TDMConveniosWEB; var CodigoMensaje: Int64; Email, Asunto: String; CodigoPersona: Integer; Usuario: String);
begin
    try
        with dm.spEMAIL_AgregarEnvioClave do begin
            Parameters.ParamByName('@Email').Value := Email;
            Parameters.ParamByName('@Asunto').Value := Asunto;
            Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
            Parameters.ParamByName('@CodigoUsuario').Value := Usuario;

            execproc;
            CodigoMensaje := Parameters.ParamByName('@CodigoMensaje').Value;
        end;
    except
        on e: exception do raise exception.create('Error al registrar el env�o del email: ' + e.Message);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure Name: AgregarEmailMensaje
  Author: mcabello
  Date Created: 05/10/2012
  Description: Registra en la tabla Email_Mensajes para envio de email con el mailer
  Parameters: DM, Email, CodigoPlantilla, CodigoFirma, CodigoConvenio, Parametros
  Return Value:
-----------------------------------------------------------------------------}


procedure AgregarEmailMensaje(DM: TdmConveniosWEB; var Email: string; CodigoPlantilla, CodigoFirma, CodigoConvenio: Integer; Parametros: String);
begin
    try
        with DM.spEMAIL_AgregarMensaje do begin
            Parameters.ParamByName('@Email').Value := Email;
            Parameters.ParamByName('@CodigoPlantilla').Value := CodigoPlantilla;
            Parameters.ParamByName('@CodigoFirma').Value := CodigoFirma;
            Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            Parameters.ParamByName('@Parametros').Value := Parametros;
            ExecProc;
        end;

    except
        on e: Exception do raise Exception.Create('Error al guardar el mensaje: '  + e.Message);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure Name: ConfirmarEmailEnvioClave
  Author: nefernandez
  Date Created: 15/05/2007
  Description: Se marca como enviado el registro del email de generaci�n de clave
  Parameters: DM, CodigoMensaje
  Return Value: 
-----------------------------------------------------------------------------}
procedure ConfirmarEmailEnvioClave(DM: TDMConveniosWEB; CodigoMensaje: Int64);
begin
    try
        with dm.spEMAIL_ConfirmarEnvioClave do begin
            Parameters.ParamByName('@CodigoMensaje').Value := CodigoMensaje;

            execproc;
        end;
    except
        on e: exception do raise exception.create('Error al confirmar el env�o del email: ' + e.Message);
    end;
end;

{***************************************************
  Function: VerificarSiExisteSolicitudPendiente
  Author: dAllegretti
  Date: 28/01/2009
  Description: Indica si existe una respuesta del banco pendiente
 ***************************************************}
function VerificarSiExisteSolicitudPendiente(DM: TdmConveniosWeb; TipoComprobante: string; NumeroComprobante, CodigoBanco: integer; Importe: Int64; var Error: string): boolean;
begin
    result := False;
    dm.SpWEB_CV_ExisteSolicitudPendiente.Close;
    dm.SpWEB_CV_ExisteSolicitudPendiente.parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
    dm.SpWEB_CV_ExisteSolicitudPendiente.parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
    dm.SpWEB_CV_ExisteSolicitudPendiente.parameters.ParamByName('@ImporteComprobante').Value := Importe;
    dm.SpWEB_CV_ExisteSolicitudPendiente.parameters.ParamByName('@CodigoBanco').Value := CodigoBanco;
    dm.SpWEB_CV_ExisteSolicitudPendiente.parameters.ParamByName('@Rta').Value := 0;
    dm.SpWEB_CV_ExisteSolicitudPendiente.ExecProc;
    // compruebo si el comprobante existe
    if  dm.SpWEB_CV_ExisteSolicitudPendiente.parameters.ParamByName('@Rta').Value = 0 then begin
        error := MSG_COMPROBANTE_PENDIENTE;
    end;
    Result := dm.SpWEB_CV_ExisteSolicitudPendiente.parameters.ParamByName('@Rta').Value = 0;

end;

{******************************** Function Header ******************************
Function Name: VerificarSiEstaPago
Author : vpaszkowicz
Date Created : 24/02/2009
Description : Agrego validaci�n para comprobar que no exista un pago en la fecha.
Parameters : DM: TdmConveniosWeb; TipoComprobante: string; NumeroComprobante, CodigoBanco: integer; Importe: Int64; var Error: string
Return Value : boolean
*******************************************************************************}
function VerificarSiEstaPago(DM: TdmConveniosWeb; TipoComprobante: string; NumeroComprobante, CodigoBanco: integer; Importe: Int64; var Error: string): boolean;
begin
    result := False;
    dm.ComprobanteEstaPago.Close;
    dm.ComprobanteEstaPago.parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
    dm.ComprobanteEstaPago.parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
    dm.ComprobanteEstaPago.parameters.ParamByName('@ImporteComprobante').Value := Importe;
    dm.ComprobanteEstaPago.parameters.ParamByName('@Rta').Value := 0;
    dm.ComprobanteEstaPago.ExecProc;
    // compruebo si el comprobante existe
    if  dm.ComprobanteEstaPago.parameters.ParamByName('@Rta').Value = 1 then begin
        error := MSG_COMPROBANTE_ESTA_PAGO;
    end;
    Result := (dm.ComprobanteEstaPago.parameters.ParamByName('@Rta').Value = 1);

end;

{-----------------------------------------------------------------------------
  Function Name:  GenerarPDFTransaccionesComprobante
  Author:    jjofre
  Date Created: 13/09/2010
  Description: (SS_917 Modificaciones Web) Genera el reporte del detalle de transitos
                por comprobante identico al CAC.
  Return Value: Boolean
-----------------------------------------------------------------------------}
function GenerarPDFTransaccionesComprobante (dm: TDMConveniosWeb; TipoComprobante: string; NroComprobante: int64; var StringPDF: string; var Error: string; var EsAviso: Boolean): boolean;
var
    //f:TFormReporteFacturacionDetalladaWeb;																		//SS_1397_MGO_20151015
	f:TFormReporteFacturacionDetallada;																				//SS_1397_MGO_20151015
begin
    try
        result := false;
        EsAviso := False;
        try
            //f:= TFormReporteFacturacionDetalladaWeb.Create(nil);													//SS_1397_MGO_20151015
            f:= TFormReporteFacturacionDetallada.Create(nil);														//SS_1397_MGO_20151015
            //if f.Inicializar(dm.Base, TipoComprobante, NroComprobante, StringPDF, Error, EsAviso) then begin		//SS_1397_MGO_20151015
            if f.Inicializar(dm.Base, TipoComprobante, NroComprobante, Error, EsAviso, True) then begin				//SS_1397_MGO_20151015
                Result:= f.EjecutarPDF(StringPDF, Error);
            end
        finally
            FreeAndNil(f);
        end;
    except
        on e: exception do begin
            result := false;
            Error := e.Message;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name:  CargarDatosCSV
  Author:    jjofre
  Date Created: 08/10/2010
  Description: (SS_917 Modificaciones Web) Genera el csv del detalle de transitos
-----------------------------------------------------------------------------}
procedure CargarDatosCSV(dm: TDMConveniosWeb; var Plantilla: string; CodigoConvenio, IndiceVehiculo: Integer; FechaDesde, FechaHasta: TDateTime; Estacionamientos: Boolean = False);// Rev 8.  SS_917	// SS_1006_PDO_20121115
var
    error:string;
begin

    with dm.spTransitos do begin
    	Parameters.Refresh;		// SS_1006_PDO_20121115
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        parameters.ParamByName('@IndiceVehiculo').Value := iif(IndiceVehiculo = -1, NULL, IndiceVehiculo);

        if (FechaDesde = NULLDATE) then
            parameters.ParamByName('@FechaDesde').Value := NULL
        else
            parameters.ParamByName('@FechaDesde').Value := FechaDesde;

        if (FechaHasta = NULLDATE) then
            parameters.ParamByName('@FechaHasta').Value := NULL
        else
            parameters.ParamByName('@FechaHasta').Value := FechaHasta;
																
        if Estacionamientos then begin									// SS_1006_20121115
        	parameters.ParamByName('@Estacionamientos').Value := 1;		// SS_1006_20121115
        end;															// SS_1006_20121115


        open;
        DatasetToExcelCSV(dm.SpTransitos, Plantilla, error );
        close;
    end;
end;
{-----------------------------------------------------------------------------
  Procedure: CargarDatosUltimosMovimientoEstacionamiento
  Revision    : 5
  Date        : 20-Abril-2011
  Author      : Eduardo Baeza Ortega
  Firma       : PAR00133_EBA_20110419
  Description : Fase 2 - Pagina Web Convenios
                Carga los estacionamientos no facturados filtrado por c�digo de vehiculo
-----------------------------------------------------------------------------}
procedure CargarDatosUltimosMovimientosVehiculoEstacionamiento(dm: TDMConveniosWeb; Request: TWEBRequest; var Plantilla: string; CodigoConvenio, IndiceVehiculo, Concesionaria: integer);
resourcestring
    MSG_SELECCIONE_PATENTE = 'Seleccione una patente';
var
        Inicio, Repeticion, Fin, Temp: string;
        HayConsumos :boolean;
begin

    HayConsumos := false;
    CargarDatosDomicilioConvenio(dm, plantilla, CodigoConvenio);

    Plantilla := ReplaceTag(Plantilla, 'codigoconcesionaria', IntToStr(Concesionaria));

    Plantilla := ReplaceTag(Plantilla, 'DescConvenio', ObtenerDescripcionConvenio(dm, CodigoConvenio));
    Plantilla := ReplaceTag(Plantilla, 'IndiceVehiculoValores', GenerarComboVehiculosConvenioSinDuplicados(dm, CodigoConvenio, inttostr(IndiceVehiculo)));
    Plantilla := ReplaceTag(Plantilla, 'ConcesionariaValores', GenerarComboConcesionariasAsociadasItemsConvenio(dm, IntToStr(Concesionaria), CodigoConvenio, IndiceVehiculo));

    with dm.spObtenerUltimosConsumosEstacionamientos do begin
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        parameters.ParamByName('@CodigoVehiculo').Value := iif(IndiceVehiculo = -1, NULL, IndiceVehiculo);
        parameters.ParamByName('@CodigoConcesionaria').Value := iif(Concesionaria = -1, NULL, Concesionaria);
        // Si seleccion� una patente, abrir el SP.
        if IndiceVehiculo > -1 then begin
            Open;
        end;

        // genero las repeticiones necesarias
        SepararPartes(Plantilla, '#RepeticionInicio#', '#RepeticionFin#', Inicio, Repeticion, Fin);
        Plantilla := Inicio;

        // Si NO seleccion� una patente, mostrar el texto de seleccionar una patente.
        if IndiceVehiculo = -1 then begin
                temp := repeticion;
                temp := ReplaceTag(temp, 'Concepto', MSG_SELECCIONE_PATENTE);
                temp := ReplaceTag(temp, 'Importe', '');
                Plantilla := Plantilla + temp;
        end else begin
            if eof then begin
                    temp := repeticion;
                    temp := ReplaceTag(temp, 'Concepto', 'No hay consumos registrados. ');
                    temp := ReplaceTag(temp, 'Importe', '');
                    Plantilla := Plantilla + temp;
            end else begin
                while not eof do begin
                    temp := repeticion;
                    temp := ReplaceTag(temp, 'Concepto', fieldbyname('Concepto').asString);
                    temp := ReplaceTag(temp, 'Importe', FormatFloat('#,##0.00', fieldbyname('Importe').asFloat));
                    Plantilla := Plantilla + temp;
                    next;
                end;
                HayConsumos := true;
            end;
        end; // else if IndiceVehiculo = -1

        Plantilla := Plantilla + Fin;

        close;
    end;

    with dm.spObtenerUltimosConsumosDetalleEstacionamientos do begin
        parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
        parameters.ParamByName('@CodigoVehiculo').Value := iif(IndiceVehiculo = -1, NULL, IndiceVehiculo);
        parameters.ParamByName('@CodigoConcesionaria').Value := iif(Concesionaria = -1, NULL, Concesionaria);

        // Si seleccion� una patente, abrir el SP.
        if (IndiceVehiculo > -1) and HayConsumos then begin
            Open;
            HayConsumos := RecordCount > 0;
        end;

        // genero las repeticiones necesarias
        SepararPartes(Plantilla, '#RepeticionInicio2#', '#RepeticionFin2#', Inicio, Repeticion, Fin);
        Plantilla := Inicio;

        // Si NO seleccion� una patente, mostrar el texto de seleccionar una patente.
        if IndiceVehiculo = -1 then begin
                temp := repeticion;
                temp := ReplaceTag(temp, 'Patente', MSG_SELECCIONE_PATENTE);
                temp := ReplaceTag(temp, 'Concesionaria', '');
                temp := ReplaceTag(temp, 'LugarEntrada', '');
                temp := ReplaceTag(temp, 'FHE', '');
                temp := ReplaceTag(temp, 'LugarSalida', '');
                temp := ReplaceTag(temp, 'FHS', '');
                temp := ReplaceTag(temp, 'Monto', '');
                Plantilla := Plantilla + temp;
        end else begin
            if not HayConsumos then begin
                temp := repeticion;
                temp := ReplaceTag(temp, 'Patente', 'No hay estacionamientos');
                temp := ReplaceTag(temp, 'Concesionaria', '');
                temp := ReplaceTag(temp, 'LugarEntrada', '');
                temp := ReplaceTag(temp, 'FHE', '');
                temp := ReplaceTag(temp, 'LugarSalida', '');
                temp := ReplaceTag(temp, 'FHS', '');
                temp := ReplaceTag(temp, 'Monto', '');
                Plantilla := Plantilla + temp;
            end else begin
                First;
                while not eof do begin
                    temp := repeticion;
                    temp := ReplaceTag(temp, 'Patente', fieldbyname('Patente').asString);
                    temp := ReplaceTag(temp, 'Concesionaria', fieldbyname('Concesionaria').asString);
                    temp := ReplaceTag(temp, 'LugarEntrada', fieldbyname('LugarEntrada').asString);
                    temp := ReplaceTag(temp, 'FHE', fieldbyname('FechaHoraEntrada').asString);
                    temp := ReplaceTag(temp, 'LugarSalida', fieldbyname('LugarSalida').asString);
                    temp := ReplaceTag(temp, 'FHS', fieldbyname('FechaHoraSalida').asString);
                    temp := ReplaceTag(temp, 'Monto', fieldbyname('Importe').asString);
                    Plantilla := Plantilla + temp;
                    next;
                end;
            end;
        end; // else if IndiceVehiculo = -1

        Plantilla := Plantilla + Fin;

        close;
    end;

end;


//SS966-NDR-20110810-----------------------------------------------------------------
//Procedure AgregarAuditoriaBDP_MPINIBCH
// Mantiene una tabla de auditoria exclusiva del BCH, en este caso con el resultado del
// envio del mensaje de inicio de la operacion
//-----------------------------------------------------------------------------------
Procedure AgregarAuditoriaBDP_MPINIBCH(dm: TDMConveniosWeb; Request: TWebRequest; IDTRX, SRVREC : String);		//SS-966-NDR-20120103
begin
    try
      with dm.spAgregarAuditoriaBDP_MPINIBCH do begin
          parameters.ParamByName('@IDTRX').Value      := IDTRX;
          parameters.ParamByName('@IDCOM').Value      := ParseXMLField(Request.ContentFields.Text,'IDCOM');
          parameters.ParamByName('@TOTAL').Value      := ParseXMLField(Request.ContentFields.Text,'TOTAL');
          parameters.ParamByName('@NROPAGOS').Value   := ParseXMLField(Request.ContentFields.Text,'NROPAGOS');
          //parameters.ParamByName('@SRVREC').Value     := ParseXMLField(Request.ContentFields.Text,'SRVREC');    //SS-966-NDR-20120103
          parameters.ParamByName('@SRVREC').Value     := SRVREC;                                                  //SS-966-NDR-20120103
          parameters.ParamByName('@MONTO').Value      := ParseXMLField(Request.ContentFields.Text,'MONTO');
          parameters.ParamByName('@GLOSA').Value      := ParseXMLField(Request.ContentFields.Text,'GLOSA');
          parameters.ParamByName('@CANTIDAD').Value   := ParseXMLField(Request.ContentFields.Text,'CANTIDAD');
          parameters.ParamByName('@PRECIO').Value     := ParseXMLField(Request.ContentFields.Text,'PRECIO');
          parameters.ParamByName('@DATOADIC').Value   := ParseXMLField(Request.ContentFields.Text,'DATOADIC');
          Execproc;
      end;
     except
        on E : Exception do begin
            AgregarLog(DM, 'AgregarAuditoriaBDP_MPINIBCH - ' + E.Message);
            EventLogReportEvent(elError, 'AgregarAuditoriaBDP_MPINIBCH - ' + E.Message, '');
        end;
    end;
end;

//SS966-NDR-20110810-----------------------------------------------------------------
//Procedure AgregarAuditoriaBDP_MPOUTBCH
// Mantiene una tabla de auditoria exclusiva del BCH, en este caso con el resultado de la
// recepcion del mensaje incluido en la invocacion hecha por BCH a la URL de Notificacion
//-----------------------------------------------------------------------------------
Procedure AgregarAuditoriaBDP_MPOUTBCH(dm: TDMConveniosWeb; Request: TWebRequest);
begin
    try
        with dm.spAgregarAuditoriaBDP_MPOUTBCH do begin
            parameters.ParamByName('@IDTRX').Value      := ParseXMLField(Request.ContentFields.Text,'IDTRX');
            parameters.ParamByName('@CODRET').Value     := ParseXMLField(Request.ContentFields.Text,'CODRET');
            parameters.ParamByName('@DESCRET').Value    := ParseXMLField(Request.ContentFields.Text,'DESCRET');
            parameters.ParamByName('@IDCOM').Value      := ParseXMLField(Request.ContentFields.Text,'IDCOM');
            parameters.ParamByName('@TOTAL').Value      := ParseXMLField(Request.ContentFields.Text,'TOTAL');
            parameters.ParamByName('@NROPAGOS').Value   := ParseXMLField(Request.ContentFields.Text,'NROPAGOS');
            parameters.ParamByName('@FECHATRX').Value   := ParseXMLField(Request.ContentFields.Text,'FECHATRX');
            parameters.ParamByName('@FECHACONT').Value  := ParseXMLField(Request.ContentFields.Text,'FECHACONT');
            parameters.ParamByName('@NUMCOMP').Value    := ParseXMLField(Request.ContentFields.Text,'NUMCOMP');
            parameters.ParamByName('@IDREG').Value      := ParseXMLField(Request.ContentFields.Text,'IDREG');
            Execproc;
        end;
     except
        on E : Exception do begin
            AgregarLog(DM, 'AgregarAuditoriaBDP_MPOUTBCH - ' + E.Message);
            EventLogReportEvent(elError, 'AgregarAuditoriaBDP_MPOUTBCH - ' + E.Message, '');
        end;
    end;
end;

//SS966-NDR-20110810-----------------------------------------------------------------
//Procedure AgregarAuditoriaBDP_MPFINBCH
// Mantiene una tabla de auditoria exclusiva del BCH, en este caso con el resultado de la
// recepcion del mensaje de fin de la operacion emitido desde el banco
//-----------------------------------------------------------------------------------
Procedure AgregarAuditoriaBDP_MPFINBCH(dm: TDMConveniosWeb; Request: TWebRequest);
begin
    try
        with dm.spAgregarAuditoriaBDP_MPFINBCH do begin
            parameters.ParamByName('@IDTRX').Value      := ParseXMLField(Request.ContentFields.Text,'IDTRX');
            parameters.ParamByName('@CODRET').Value     := ParseXMLField(Request.ContentFields.Text,'CODRET');
            parameters.ParamByName('@NROPAGOS').Value   := ParseXMLField(Request.ContentFields.Text,'NROPAGOS');
            parameters.ParamByName('@TOTAL').Value      := ParseXMLField(Request.ContentFields.Text,'TOTAL');
            parameters.ParamByName('@INDPAGO').Value    := ParseXMLField(Request.ContentFields.Text,'INDPAGO');
            parameters.ParamByName('@IDREG').Value      := ParseXMLField(Request.ContentFields.Text,'IDREG');
            Execproc;
        end;
    except
        on E : Exception do begin
            AgregarLog(DM, 'AgregarAuditoriaBDPMPFINBCH - ' + E.Message);
            EventLogReportEvent(elError, 'AgregarAuditoriaBDPMPFINBCH - ' + E.Message, '');
        end;
    end;
end;


//SS966-NDR-20110810-----------------------------------------------------------------
//Procedure AgregarAuditoriaBDP_MPCONBCH
// Mantiene una tabla de auditoria exclusiva del BCH, en este caso con el resultado del
// envio hacia el Banco de un mensaje de confirmacion
//-----------------------------------------------------------------------------------
Procedure AgregarAuditoriaBDP_MPCONBCH(dm: TDMConveniosWeb; Request: TWebRequest);
begin
    try
        with dm.spAgregarAuditoriaBDP_MPCONBCH do begin
            parameters.ParamByName('@IDCOM').Value := ParseXMLField(Request.ContentFields.Text,'IDCOM');
            parameters.ParamByName('@IDTRX').Value := ParseXMLField(Request.ContentFields.Text,'IDTRX');
            parameters.ParamByName('@TOTAL').Value := ParseXMLField(Request.ContentFields.Text,'TOTAL');
            parameters.ParamByName('@IDREG').Value := ParseXMLField(Request.ContentFields.Text,'IDREG');
            Execproc;
        end;
     except
        on E : Exception do begin
            AgregarLog(DM, 'AgregarAuditoriaBDP_MPCONBCH - ' + E.Message);
            EventLogReportEvent(elError, 'AgregarAuditoriaBDP_MPCONBCH - ' + E.Message, '');
        end;
    end;
end;

//SS966-NDR-20110810-----------------------------------------------------------------
//Procedure AgregarAuditoriaBDP_MPRESBCH
// Mantiene una tabla de auditoria exclusiva del BCH, en este caso con el resultado de la
// recepcion del mensaje de resultado de la confirmacion enviada en el MPCON
//-----------------------------------------------------------------------------------
Procedure AgregarAuditoriaBDP_MPRESBCH(dm: TDMConveniosWeb; Request: TWebRequest);
begin
    try
        with dm.spAgregarAuditoriaBDP_MPRESBCH do begin
            parameters.ParamByName('@CODRET').Value     := ParseXMLField(Request.ContentFields.Text,'CODRET');
            parameters.ParamByName('@DESCRET').Value    := ParseXMLField(Request.ContentFields.Text,'DESCRET');
            parameters.ParamByName('@IDCOM').Value      := ParseXMLField(Request.ContentFields.Text,'IDCOM');
            parameters.ParamByName('@IDTRX').Value      := ParseXMLField(Request.ContentFields.Text,'IDTRX');
            parameters.ParamByName('@TOTAL').Value      := ParseXMLField(Request.ContentFields.Text,'TOTAL');
            parameters.ParamByName('@NROPAGOS').Value   := ParseXMLField(Request.ContentFields.Text,'NROPAGOS');
            parameters.ParamByName('@FECHATRX').Value   := ParseXMLField(Request.ContentFields.Text,'FECHATRX');
            parameters.ParamByName('@FECHACONT').Value  := ParseXMLField(Request.ContentFields.Text,'FECHACONT');
            parameters.ParamByName('@NUMCOMP').Value    := ParseXMLField(Request.ContentFields.Text,'NUMCOMP');
            parameters.ParamByName('@IDREG').Value      := ParseXMLField(Request.ContentFields.Text,'IDREG');
            Execproc;
        end;
     except
        on E : Exception do begin
            AgregarLog(DM, 'AgregarAuditoriaBDP_MPRESBCH - ' + E.Message);
            EventLogReportEvent(elError, 'AgregarAuditoriaBDP_MPRESBCH - ' + E.Message, '');
        end;
    end;
end;

//SS966-NDR-20110810-----------------------------------------------------------------
//function RegistrarPagoComprobanteBCH
//Registra el pago realizado del comprobante
//-----------------------------------------------------------------------------------
function RegistrarPagoComprobanteBCH(   dm: TDMConveniosWeb; TipoComprobante: string;
                                        NumeroComprobante : Integer;
                                        Importe : Int64;
                                        CodigoRespuesta: integer;
                                        FechaProceso: tdatetime;
                                        var Error: string): boolean;
Const
    MAX_REINTENTOS = 5;
var
    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
begin
    Result := False;

    Reintentos  := 0;
    while Reintentos < MAX_REINTENTOS do begin
        try
            QueryExecute(DM.Base, 'BEGIN TRAN spRegistrarPagoBCH');
            with dm.spRegistrarPagoBCH do begin
                parameters.Refresh;
                parameters.ParamByName('@NumeroComprobante').Value  := NumeroComprobante;
                parameters.ParamByName('@TipoComprobante').Value    := TipoComprobante;
                parameters.ParamByName('@Importe').Value            := Importe;
                parameters.ParamByName('@CodigodeRespuesta').Value  := CodigoRespuesta;
                parameters.ParamByName('@Fechaproceso').Value       := Fechaproceso;
                parameters.ParamByName('@DescripcionError').Value   := '';
                //parameters.ParamByName('@CodigoUsuario').Value      := 'WEB'; // La variable "UsuarioSistema" esta vacio en este momento; // SS_1311_CQU_20150622
                parameters.ParamByName('@CodigoUsuario').Value      := 'WEB-BCH';                                                           // SS_1311_CQU_20150622
                //CommandTimeOut := 5000;                                       //SS-966-NDR-20111130
                CommandTimeOut := 30;                                           //SS-966-NDR-20111130
                Execproc;

                result := parameters.ParamByName('@RETURN_VALUE').Value = 0;
                if not result then begin
                    Error := IntToStr(parameters.ParamByName('@RETURN_VALUE').Value) + ' ' + parameters.ParamByName('@DescripcionError').Value;
                    QueryExecute(DM.Base, 'ROLLBACK TRAN spRegistrarPagoBCH');
                    Exit;
                end;
                QueryExecute(DM.Base, 'COMMIT TRAN spRegistrarPagoBCH');
                Break;
            end;
        except
            on e: exception do begin
                QueryExecute(DM.Base, 'ROLLBACK TRAN spRegistrarPagoBCH');
                if (pos('deadlock', e.Message) > 0) then begin
                    mensajeDeadlock := e.message;
                    inc(Reintentos);
                    sleep(2500);
                end else begin
                    result := false;
                    error := e.Message;
                    exit;
                end;
            end;
        end;
    end;

    //una vez que se reintento 5 veces x deadlock lo informo
     if Reintentos = MAX_REINTENTOS then begin
           result := false;
           error := mensajeDeadLock;
           QueryExecute(DM.Base, 'ROLLBACK TRAN spRegistrarPagoBCH');
     end;
end;

//SS-966-NDR-20111130-----------------------------------------------------------------
//function RegistrarPagoPendienteComprobanteBCH
//Registra el pago realizado del comprobante en una tabla de trabajo.
//-----------------------------------------------------------------------------------
function RegistrarPagoPendienteComprobanteBCH(   dm: TDMConveniosWeb;
                                                 TipoComprobante: string;
                                                 NumeroComprobante : Integer;
                                                 Importe : Int64;
                                                 CodigoRespuesta: integer;
                                                 FechaProceso: tdatetime;
                                                 CodigoSesion: Integer;				//SS_1304_MCA_20150709
                                                 var Error: string): boolean;
Const
    MAX_REINTENTOS = 5;
var
    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
    mensajeTimeOut : AnsiString;
begin
    Result := False;

    Reintentos  := 0;
    while Reintentos < MAX_REINTENTOS do begin
        try
            QueryExecute(DM.Base, 'BEGIN TRAN trxRegistrarPagoPendienteBCH');
            with dm.spRegistrarPagoPendienteBCH do begin
                parameters.Refresh;
                parameters.ParamByName('@NumeroComprobante').Value  := NumeroComprobante;
                parameters.ParamByName('@TipoComprobante').Value    := TipoComprobante;
                parameters.ParamByName('@Importe').Value            := Importe;
                parameters.ParamByName('@CodigodeRespuesta').Value  := CodigoRespuesta;
                parameters.ParamByName('@Fechaproceso').Value       := Fechaproceso;
                parameters.ParamByName('@CodigoSesion').Value       := CodigoSesion;			//SS_1304_MCA_20150709
                parameters.ParamByName('@DescripcionError').Value   := '';
                //parameters.ParamByName('@CodigoUsuario').Value      := 'WEB'; // La variable "UsuarioSistema" esta vacio en este momento;   // SS_1311_CQU_20150622
                parameters.ParamByName('@CodigoUsuario').Value      := 'WEB-BCH';                                                             // SS_1311_CQU_20150622
                CommandTimeOut := 30;
                Execproc;
                result := parameters.ParamByName('@RETURN_VALUE').Value = 0;
                if not result then begin
                    Error := IntToStr(parameters.ParamByName('@RETURN_VALUE').Value) + ' ' + parameters.ParamByName('@DescripcionError').Value;
                    QueryExecute(DM.Base, 'ROLLBACK TRAN trxRegistrarPagoPendienteBCH');
                    Exit;
                end;
                QueryExecute(DM.Base, 'COMMIT TRAN trxRegistrarPagoPendienteBCH');
                Break;
            end;
        except
            on e: exception do begin
                QueryExecute(DM.Base, 'ROLLBACK TRAN trxRegistrarPagoPendienteBCH');
                if (pos('DEADLOCK', UpperCase(e.Message)) > 0) then begin
                    mensajeDeadlock := e.message;
                    inc(Reintentos);
                    sleep(2000);
                end
                else if (pos('TIMEOUT', UpperCase(e.Message)) > 0) then begin
                    mensajeTimeOut := e.message;
                    inc(Reintentos);
                    sleep(2000);
                end
                else begin
                    result := false;
                    error := e.Message;
                    exit;
                end;
            end;
        end;
    end;
    //una vez que se reintento 5 veces x deadlock lo informo
    if Reintentos = MAX_REINTENTOS then begin
          QueryExecute(DM.Base, 'ROLLBACK TRAN trxRegistrarPagoPendienteBCH');
          result := false;
          error := Trim(mensajeDeadLock)+Trim(mensajeTimeOut);
          Exit;
    end;
end;

Function IsValidEMailCN(email: String): Boolean;

    procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings);
    begin
        Assert(Assigned(Strings));
        Strings.Clear;
        Strings.Delimiter := Delimiter;
        Strings.DelimitedText := Input;
    end;

    function HasNumbers(const S: string): Boolean;
        var
        	i: integer;
    begin
        Result := True;
        i := 1;
        while i <= Length(S) do begin
            if(S[i] in ['0'..'9']) then exit;
            i := i+1;
        end;
        Result := False;
    end;

    Function IsAccountValid(S:String):Boolean;
    begin
        Result:=False;
        if S = '' then Exit;
        if Pos('..', S ) <> 0 then Exit;
        if (Copy(S, 1, 1) = '.') or
           (Copy(S, 1, 1) = '_') or
           (Copy(S, 1, 1) = '-') then Exit;
        if (Copy(S, Length(S), 1) = '.') then exit;
        Result:=True;
    end;

    Function IsDomainValid(S:String):Boolean;
        var
          	ls: TStringList;
          	I: Integer;
    begin
        Result := False;
        ls:= TStringList.Create;
        try
            Split('.',S, ls);
            if (ls.Count <= 1) then exit;
            for I := 0 to ls.Count - 1 do begin
              if ls[i] = '' then Exit;
              if Pos('_', ls[i] ) <> 0 then Exit;
              if (Pos('-', ls[i] ) = Length(ls[i])) or (Pos('-', ls[i])=1) then Exit;
              if(i=ls.Count-1) and (HasNumbers(ls[i])) then Exit;
            end;
            Result := True;
        finally
          	ls.Free;
        end;
    end;

    Var
   		i, AtCount: Integer;
	    Account, Domain: String;
    	ls: TStringList;
begin
	Result := False;
	email := Trim(email);
	AtCount := 0;
	// Buscamos caracteres raros y arrobas
	for i := 1 to Length(email) do begin
		case email[i] of
			'@': Inc(AtCount);
			'a'..'z', 'A'..'Z', '0'..'9', '_', '-', '.':;
			else Exit;
		end;
	end;
	// Tiene una arroba?
	if AtCount <> 1 then Exit;
	// Separamos la cuenta y el dominio
  	ls := TStringList.Create;
    try
        Split('@',email,ls);
        Account := ls[0];
        Domain := ls[1];
        // La cuenta es v�lida?
        if(not IsAccountValid(Account)) then Exit;
        // El dominio es v�lido?
        if(not IsDomainValid(Domain)) then Exit;
        // Todo Ok
        Result := True;
    finally
    	ls.Free;
    end;
end;

Procedure RegistrarBitacoraUso(dm: TdmConveniosWEB; CodigoSesion, CodigoPersona, CodigoConvenio, IdBitacoraAccion: Integer; NumeroDocumento, Observacion: string);	//SS_1305_MCA_20150612
begin																																					//SS_1305_MCA_20150612
    try																												//SS_1305_MCA_20150612
        with dm.spWEB_CV_RegistrarBitacoraUso do begin																//SS_1305_MCA_20150612
            parameters.Refresh;																						//SS_1305_MCA_20150612
            parameters.ParamByName('@CodigoSesion').Value       := CodigoSesion;									//SS_1305_MCA_20150612
            parameters.ParamByName('@CodigoPersona').Value      := CodigoPersona;									//SS_1305_MCA_20150612
            parameters.ParamByName('@CodigoConvenio').Value     := CodigoConvenio;									//SS_1305_MCA_20150612
            parameters.ParamByName('@NumeroDocumento').Value    := NumeroDocumento;									//SS_1305_MCA_20150612
            parameters.ParamByName('@IdBitacoraAccion').Value   := IdBitacoraAccion;								//SS_1305_MCA_20150612
            parameters.ParamByName('@Observacion').Value        := Observacion;										//SS_1305_MCA_20150612
            CommandTimeOut := 30;																					//SS_1305_MCA_20150612
            Execproc;																								//SS_1305_MCA_20150612
        end;																										//SS_1305_MCA_20150612
    except																											//SS_1305_MCA_20150612
        on E : Exception do begin																					//SS_1305_MCA_20150612
            AgregarLog(DM, 'WEB_CV_RegistrarBitacoraUso - ' + E.Message);											//SS_1305_MCA_20150612
            EventLogReportEvent(elError, 'WEB_CV_RegistrarBitacoraUso - ' + E.Message, '');							//SS_1305_MCA_20150612
        end;																										//SS_1305_MCA_20150612
    end;																											//SS_1305_MCA_20150612
end;																												//SS_1305_MCA_20150612
//inicio bloque SS_1304_MCA_20150709
//function GenerarTablaDeudaConvenio(DM: TdmConveniosWEB; CodigoSesion: Integer; var NrosComprobantes: string):string;						//SS_1356_MCA_20150903
function GenerarTablaDeudaConvenio(DM: TdmConveniosWEB; CodigoSesion: Integer; var NrosComprobantes: string; Importe: Integer):string;		//SS_1356_MCA_20150903
var
    Tabla:string;
    TotalComprobantes: integer;
    i:Integer;
begin
    Tabla := '';
    TotalComprobantes :=0;
    NrosComprobantes := EmptyStr;
    i:=0;
     try																										//SS_1305_MCA_20150612
        with dm.spWEB_CV_ObtenerDeudaCliente do begin															//SS_1305_MCA_20150612
            parameters.Refresh;																					//SS_1305_MCA_20150612
            parameters.ParamByName('@CodigoSesion').Value := CodigoSesion;								    	//SS_1305_MCA_20150612
            CommandTimeOut := 30;																				//SS_1305_MCA_20150612
            Open;																								//SS_1305_MCA_20150612
            First;
            if (not eof) then begin

                while (not eof) do begin
                    Tabla := Tabla + '<tr>'+ CRLF;
                    Tabla := Tabla + '<td valign="top" class="estilo1" align="center">'+ FieldByName('NumeroConvenio').AsString +'</td>' + CRLF;
                    Tabla := Tabla + '<td valign="top" class="estilo1" align="left">'+ FieldByName('NumeroComprobanteFiscal').AsString +'</td>'+ CRLF;
                    Tabla := Tabla + '<td valign="top" class="estilo1" align="rigth">'+ FieldByName('SaldoDesc').AsString +'</td>'+ CRLF;
                    Tabla := Tabla + '<td valign="top" class="estilo1" align="center" ><input type="checkbox" name="pagar" id="pagar" checked value="'+ FieldByName('Saldo').AsString + '" onclick="sumatotal(this);"/> </td>'+ CRLF;
                    Tabla := Tabla + '</tr>'+ CRLF;
                    Tabla := Tabla + '<input type="hidden" name="TipoComprobante" value="'+ FieldByName('TipoComprobante').AsString + ' ' + FieldByName('NumeroComprobante').AsString +'" />';
                    NrosComprobantes := NrosComprobantes + FieldByName('NumeroComprobante').AsString;
                    TotalComprobantes := TotalComprobantes + FieldByName('Saldo').AsInteger;
                    Importe := Importe + TotalComprobantes;					//SS_1356_MCA_20150903
                    Next;

                    if not eof then NrosComprobantes := NrosComprobantes + ',';

                    Inc(i);
                end;
                Tabla := Tabla + '<tr>'+ CRLF;
                Tabla := Tabla + '<td valign="top" class="estilo1" align="center">&nbsp</td>'+ CRLF;
                Tabla := Tabla + '<td valign="top" class="estilo1" align="center">&nbsp</td>'+ CRLF;
                Tabla := Tabla + '<td valign="top" class="estilo1" align="center">Total a Pagar</td>'+ CRLF;
                Tabla := Tabla + '<td id="TotalPagar" valign="top" class="estilo1" align="center" style="font-weight:bold">$ ' + formatfloat('#,###', TotalComprobantes) +'</td>'+ CRLF;
                Tabla := Tabla + '</tr>'+ CRLF;
                //NrosComprobantes := Copy(NrosComprobantes,  1, Length(NrosComprobantes) -1);
            end;
            close;
            Result:= Tabla;

        end;																										//SS_1305_MCA_20150612
    except																											//SS_1305_MCA_20150612
        on E : Exception do begin																					//SS_1305_MCA_20150612
            AgregarLog(DM, 'WEB_CV_ObtenerDeudaCliente - ' + E.Message);											//SS_1305_MCA_20150612
            EventLogReportEvent(elError, 'WEB_CV_ObtenerDeudaCliente - ' + E.Message, '');							//SS_1305_MCA_20150612
        end;																										//SS_1305_MCA_20150612
    end;


end;
function RealizarPagoComprobantes(dm: TDMConveniosWeb; CodigoSesion, Cuotas, CodigoRespuesta: integer; Autorizacion, TipoVenta, Tarjeta: string; FechaProceso: TDateTime; IDTRX: string; var Error: string): boolean;
resourcestring
    MSG_ERROR_GET_OFICINA_VIRTUAL = 'Error al carga la pagina de Exito';			//SS_1280_MCA_20150812
var
    TipoComprobante, DescripcionError: string;
    NumeroComprobante: Integer;
    Importe : Int64;
    CodigoUsuario: string;                                                       //SS_1280_MCA_20150812
    EsOficinaVirtual: integer;                                                   //SS_1280_MCA_20150812
begin
    Result := False;															//SS_1304_MCA_20150709
    try
        with dm.spWEBObtenerDetalleDeudaConvenios do begin
            parameters.Refresh;
            parameters.ParamByName('@CodigoSesion').Value := CodigoSesion;
            CommandTimeOut := 30;
            Open;
            First;
            while (not eof) do begin

                TipoComprobante := FieldByName('TipoComprobante').AsString;
                NumeroComprobante := FieldByName('NumeroComprobante').AsInteger;
                Importe := FieldByName('Importe').AsInteger;

                //----------------------------------------------------------------------------------------------------------------
                //BEGIN : SS_1353_NDR_20150729 -----------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------
                //if not RegistrarPagoComprobante(dm, TipoComprobante, NumeroComprobante, Importe, Cuotas, CodigoRespuesta, Autorizacion, TipoVenta, Tarjeta, FechaProceso, Error) then begin
                //    DescripcionError := 'Error al intentar grabar un pago de webpay. El procedimiento interno lo rechazo con el mensaje: ' + error;
                //    EventLogReportEvent(elError, DescripcionError, '');
                //    ActualizarGeneroReciboAuditoria(DM, IDTRX, FALSE, DescripcionError, CODIGO_BANCO_TRANSBANK);
                //    AgregarLog(DM, 'WebpayAceptacion - ' + DescripcionError);
                //    Result := False;
                //    Exit;
                //
                //end;

                try																			    //SS_1280_MCA_20150812
                        EsOficinaVirtual := QueryGetValueInt(dm.Base,								//SS_1280_MCA_20150812
                            'SELECT  CONVERT(INT, EsOficinaVirtual) '+											//SS_1280_MCA_20150812
                            'FROM WEB_CV_Sesiones WITH (NOLOCK) ' +								//SS_1280_MCA_20150812
                            'WHERE CodigoSesion = ' + IntToStr(CodigoSesion));				//SS_1280_MCA_20150812
                except																			//SS_1280_MCA_20150812
                    on E : Exception do begin													//SS_1280_MCA_20150812
                        raise Exception.Create(MSG_ERROR_GET_OFICINA_VIRTUAL + E.Message);		//SS_1280_MCA_20150812
                    end;																		//SS_1280_MCA_20150812
                end;																			//SS_1280_MCA_20150812

                if EsOficinaVirtual = 0 then														//SS_1280_MCA_20150812
                    CodigoUsuario := 'WEB-TBK-SC'													//SS_1280_MCA_20150812
                else																			//SS_1280_MCA_20150812
                    CodigoUsuario := 'WEB-TBK';												//SS_1280_MCA_20150812

                if not RegistrarPagoPendienteComprobante( dm,
                                                          TipoComprobante,
                                                          NumeroComprobante,
                                                          Importe,
                                                          Cuotas,
                                                          CodigoRespuesta,
                                                          Autorizacion,
                                                          TipoVenta,
                                                          Tarjeta,
                                                          FechaProceso,
                                                          CodigoSesion,         //SS_1304_MCA_20150709
                                                          CodigoUsuario,		//SS_1280_MCA_20150812
                                                          Error) then
                begin
                    DescripcionError := 'Error al intentar grabar un pago de webpay. El procedimiento interno lo rechazo con el mensaje: ' + error;
                    EventLogReportEvent(elError, DescripcionError, '');
                    ActualizarGeneroReciboAuditoria(DM, IDTRX, FALSE, DescripcionError, CODIGO_BANCO_TRANSBANK);
                    AgregarLog(DM, 'WebpayAceptacion - ' + DescripcionError);
                    //Result := False;										//SS_1304_MCA_20150709
                    Exit;
                end;
                //----------------------------------------------------------------------------------------------------------------
                //END : SS_1353_NDR_20150729 -----------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------
                Next;
            end;

            try																					//SS_1304_MCA_20150709
{INICIO: TASK_009_JMA_20160427			
               DM.Base.Execute('UPDATE WEB_CV_PagosPendientes_BDP_TBK SET EstadoPagoPendiente = 1 WHERE CodigoSesion = '+ IntToStr(CodigoSesion));                            //SS_1304_MCA_20151008
}
               DM.Base.Execute('UPDATE WEB_PagosPendientes_BDP SET EstadoPagoPendiente = 1 WHERE CodigoSesion = '+ IntToStr(CodigoSesion));                            
{TERMINO: TASK_009_JMA_20160427}			   
                QueryExecute(DM.Base, 'BEGIN TRAN EjecutaPagosPendientesTBK');					//SS_1304_MCA_20150709
                with dm.spEjecutaPagosPendientes do begin										//SS_1304_MCA_20150709
                    parameters.Refresh;															//SS_1304_MCA_20150709
                    CommandTimeOut := 5000;														//SS_1304_MCA_20150709
                    Execproc;																	//SS_1304_MCA_20150709
                    if (parameters.ParamByName('@RETURN_VALUE').Value <> 0) then				//SS_1304_MCA_20150709
                    begin																		//SS_1304_MCA_20150709
                        QueryExecute(DM.Base, 'ROLLBACK TRAN EjecutaPagosPendientesTBK');		//SS_1304_MCA_20150709
                    end																			//SS_1304_MCA_20150709
                    else																		//SS_1304_MCA_20150709
                    begin																		//SS_1304_MCA_20150709
                        QueryExecute(DM.Base, 'COMMIT TRAN EjecutaPagosPendientesTBK');			//SS_1304_MCA_20150709
                    end;																		//SS_1304_MCA_20150709
                end;																			//SS_1304_MCA_20150709
            except																				//SS_1304_MCA_20150709
                on e: exception do begin														//SS_1304_MCA_20150709
                    QueryExecute(DM.Base, 'ROLLBACK TRAN EjecutaPagosPendientesTBK');			//SS_1304_MCA_20150709
                    AgregarLog(DM, 'RealizarPagoComprobantes - ' + E.Message);					//SS_1304_MCA_20150709
                end;																			//SS_1304_MCA_20150709
            end; 																				//SS_1304_MCA_20150709
            Close;																				//SS_1304_MCA_20150709
        end;
        Result := True;                                                                         //SS_1304_MCA_20150709	
    except
        on E : Exception do begin
            AgregarLog(DM, 'RealizarPagoComprobantes - ' + E.Message);
            EventLogReportEvent(elError, 'RealizarPagoComprobantes - ' + E.Message, '');
        end;
    end;

end;

function RealizarPagoComprobantesBCH(dm: TDMConveniosWeb; CodigoSesion: Integer; CodigoRespuesta: integer; FechaProceso: tdatetime; var Error: string): boolean;
var
    TipoComprobante, DescripcionError: string;
    NumeroComprobante: Integer;
    Importe : Int64;
begin
        Result := True;
    try
        with dm.spWEBObtenerDetalleDeudaConvenios do begin
            parameters.Refresh;
            parameters.ParamByName('@CodigoSesion').Value := CodigoSesion;
            CommandTimeOut := 30;
            Open;
            First;
            while (not eof) do begin

                TipoComprobante := FieldByName('TipoComprobante').AsString;
                NumeroComprobante := FieldByName('NumeroComprobante').AsInteger;
                Importe := FieldByName('Importe').AsInteger;

                if not RegistrarPagoPendienteComprobanteBCH(dm, TipoComprobante, NumeroComprobante, Importe, CodigoRespuesta, FechaProceso, CodigoSesion, Error) then begin			//SS_1304_MCA_20150709
                    Result := False;
                    Exit;
                end;
                Next;
            end;
            Close;
        end;

        try
{INICIO: TASK_009_JMA_20160427			
            DM.Base.Execute('UPDATE WEB_CV_PagosPendientes_BDP_BCH SET EstadoPagoPendiente = 1 WHERE CodigoSesion = '+ IntToStr(CodigoSesion));                            //SS_1304_MCA_20151008
}
			DM.Base.Execute('UPDATE WEB_PagosPendientes_BDP SET EstadoPagoPendiente = 1 WHERE CodigoSesion = '+ IntToStr(CodigoSesion));                            //SS_1304_MCA_20151008
{TERMINO: TASK_009_JMA_20160427}
            QueryExecute(DM.Base, 'BEGIN TRAN EjecutaPagosPendientesBCH');
            with dm.spEjecutaPagosPendientesBCH do begin
                parameters.Refresh;
                CommandTimeOut := 30;
                Execproc;
                if (parameters.ParamByName('@RETURN_VALUE').Value <> 0) then
                begin
                    QueryExecute(DM.Base, 'ROLLBACK TRAN EjecutaPagosPendientesBCH');
                    Result := False;													//SS_1304_MCA_20150709
                end
                else
                begin
                    QueryExecute(DM.Base, 'COMMIT TRAN EjecutaPagosPendientesBCH');
                end;
            end;
        except
            on e: exception do begin
                QueryExecute(DM.Base, 'ROLLBACK TRAN EjecutaPagosPendientesBCH');
                Result := False;														//SS_1304_MCA_20150709
            end;
        end;
    except
        on E : Exception do begin
            Result := False;
            AgregarLog(DM, 'RealizarPagoComprobantesBCH - ' + E.Message);
            EventLogReportEvent(elError, 'RealizarPagoComprobantesBCH - ' + E.Message, '');
        end;
    end;
end;

function RealizarPagoComprobantesSTD(dm: TDMConveniosWeb; CodigoSesion: Integer; CodigoRespuesta: integer; FechaProceso: tdatetime; var Error: string): boolean;
var
    TipoComprobante, DescripcionError: string;
    NumeroComprobante: Integer;
    Importe : Int64;
begin
        Result := True;
    try
        with dm.spWEBObtenerDetalleDeudaConvenios do begin
            parameters.Refresh;
            parameters.ParamByName('@CodigoSesion').Value := CodigoSesion;
            CommandTimeOut := 30;
            Open;
            First;
            while (not eof) do begin

                TipoComprobante := FieldByName('TipoComprobante').AsString;
                NumeroComprobante := FieldByName('NumeroComprobante').AsInteger;
                Importe := FieldByName('Importe').AsInteger;

                //----------------------------------------------------------------------------------------------------------------
                //BEGIN : SS_1353_NDR_20150729 -----------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------
                //if not RegistrarPagoComprobanteSTD(dm, TipoComprobante, NumeroComprobante, Importe, CodigoRespuesta, FechaProceso, Error) then begin
                //    Result := False;
                //    Exit;
                //end;
                if not RegistrarPagoPendienteComprobanteSTD(dm,
                                                            TipoComprobante,
                                                            NumeroComprobante,
                                                            Importe,
                                                            CodigoRespuesta,
                                                            FechaProceso,
                                                            CodigoSesion,				//SS_1304_MCA_20150709
                                                            Error) then
                begin
                    Result := False;
                    Exit;
                end;
                //----------------------------------------------------------------------------------------------------------------
                //END : SS_1353_NDR_20150729 -----------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------
                Next;
            end;
            Close;
            try
{INICIO: TASK_009_JMA_20160427			
                DM.Base.Execute('UPDATE WEB_CV_PagosPendientes_BDP_STD SET EstadoPagoPendiente = 1 WHERE CodigoSesion = '+ IntToStr(CodigoSesion));                            //SS_1304_MCA_20151008
}
				DM.Base.Execute('UPDATE WEB_PagosPendientes_BDP SET EstadoPagoPendiente = 1 WHERE CodigoSesion = '+ IntToStr(CodigoSesion));                            
{TERMINO: TASK_009_JMA_20160427}				
                QueryExecute(DM.Base, 'BEGIN TRAN EjecutaPagosPendientesSTD');
                with dm.spEjecutaPagosPendientesSTD do begin
                    parameters.Refresh;
                    CommandTimeOut := 30;
                    Execproc;
                    if (parameters.ParamByName('@RETURN_VALUE').Value <> 0) then
                    begin
                        QueryExecute(DM.Base, 'ROLLBACK TRAN EjecutaPagosPendientesSTD');
                        Result := False;													//SS_1304_MCA_20150709
                    end
                    else
                    begin
                        QueryExecute(DM.Base, 'COMMIT TRAN EjecutaPagosPendientesSTD');
                    end;
                end;
            except
                on e: exception do begin
                    QueryExecute(DM.Base, 'ROLLBACK TRAN EjecutaPagosPendientesSTD');
                    Result := False;														//SS_1304_MCA_20150709
                end;
            end;

        end;

    except
        on E : Exception do begin
            Result := False;
            AgregarLog(DM, 'RealizarPagoComprobantesSTD - ' + E.Message);
            EventLogReportEvent(elError, 'RealizarPagoComprobantesSTD - ' + E.Message, '');
        end;
    end;
end;
//fin bloque SS_1304_MCA_20150709

//----------------------------------------------------------------------------------------------------------------
//BEGIN : SS_1353_NDR_20150729 -----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------
function RegistrarPagoPendienteComprobante(   dm: TDMConveniosWeb;
                                              TipoComprobante: string;
                                              NumeroComprobante : Integer;
                                              Importe : Int64;
                                              Cuotas:Integer;
                                              CodigoRespuesta: integer;
                                              Autorizacion, TipoVenta, Tarjeta: string;
                                              FechaProceso: tdatetime;
                                              CodigoSesion: Integer;            //SS_1304_MCA_20150709
                                              CodigoUsuario :string;            //SS_1280_MCA_20150812
                                              var Error: string): boolean;
Const
    MAX_REINTENTOS = 5;
var
    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
    mensajeTimeOut : AnsiString;
begin
    Result := False;

    Reintentos  := 0;
    while Reintentos < MAX_REINTENTOS do begin
        try
            QueryExecute(DM.Base, 'BEGIN TRAN trxRegistrarPagoPendiente');
            with dm.spRegistrarPagoPendiente do begin
                parameters.Refresh;
                parameters.ParamByName('@NumeroComprobante').Value      := NumeroComprobante;
                parameters.ParamByName('@TipoComprobante').Value        := TipoComprobante;
                parameters.ParamByName('@Importe').Value                := Importe;
                parameters.ParamByName('@CodigodeRespuesta').Value      := CodigoRespuesta;
                parameters.ParamByName('@CodigodeAutorizacion').Value   := Autorizacion;
                parameters.ParamByName('@Fechaproceso').Value           := Fechaproceso;
                parameters.ParamByName('@Cuotas').Value                 := Cuotas;
                parameters.ParamByName('@UltimosDigitosTarjeta').Value  := Tarjeta;
                parameters.ParamByName('@TipoVenta').Value              := TipoVenta;
                parameters.ParamByName('@CodigoSesion').Value           := CodigoSesion;            //SS_1304_MCA_20150709
                parameters.ParamByName('@CodigoUsuario').Value          := CodigoUsuario;            //SS_1280_MCA_20150812
                parameters.ParamByName('@DescripcionError').Value       := '';
                CommandTimeOut := 30;
                Execproc;
                result := parameters.ParamByName('@RETURN_VALUE').Value = 0;
                if not result then begin
                    Error := IntToStr(parameters.ParamByName('@RETURN_VALUE').Value) + ' ' + parameters.ParamByName('@DescripcionError').Value;
                    QueryExecute(DM.Base, 'ROLLBACK TRAN trxRegistrarPagoPendiente');
                    Exit;
                end;
                QueryExecute(DM.Base, 'COMMIT TRAN trxRegistrarPagoPendiente');
                Break;
            end;
        except
            on e: exception do begin
                QueryExecute(DM.Base, 'ROLLBACK TRAN trxRegistrarPagoPendiente');
                if (pos('DEADLOCK', UpperCase(e.Message)) > 0) then begin
                    mensajeDeadlock := e.message;
                    inc(Reintentos);
                    sleep(2000);
                end
                else if (pos('TIMEOUT', UpperCase(e.Message)) > 0) then begin
                    mensajeTimeOut := e.message;
                    inc(Reintentos);
                    sleep(2000);
                end
                else begin
                    result := false;
                    error := e.Message;
                    exit;
                end;
            end;
        end;
    end;
    //una vez que se reintento 5 veces x deadlock lo informo
    if Reintentos = MAX_REINTENTOS then begin
          QueryExecute(DM.Base, 'ROLLBACK TRAN trxRegistrarPagoPendiente');
          result := false;
          error := Trim(mensajeDeadLock)+Trim(mensajeTimeOut);
          Exit;
    end;

end;


function RegistrarPagoPendienteComprobanteSTD(   dm: TDMConveniosWeb;
                                                 TipoComprobante: string;
                                                 NumeroComprobante : Integer;
                                                 Importe : Int64;
                                                 CodigoRespuesta: integer;
                                                 FechaProceso: tdatetime;
                                                 CodigoSesion: Integer;						//SS_1304_MCA_20150709
                                                 var Error: string): boolean;
Const
    MAX_REINTENTOS = 5;
var
    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
    mensajeTimeOut : AnsiString;
begin
    Result := False;

    Reintentos  := 0;
    while Reintentos < MAX_REINTENTOS do begin
        try
            QueryExecute(DM.Base, 'BEGIN TRAN trxRegistrarPagoPendienteSTD');
            with dm.spRegistrarPagoPendienteSTD do begin
                parameters.Refresh;
                parameters.ParamByName('@NumeroComprobante').Value  := NumeroComprobante;
                parameters.ParamByName('@TipoComprobante').Value    := TipoComprobante;
                parameters.ParamByName('@Importe').Value            := Importe;
                parameters.ParamByName('@CodigodeRespuesta').Value  := CodigoRespuesta;
                parameters.ParamByName('@Fechaproceso').Value       := Fechaproceso;
                parameters.ParamByName('@CodigoSesion').Value       := CodigoSesion;				//SS_1304_MCA_20150709
                parameters.ParamByName('@DescripcionError').Value   := '';
                //parameters.ParamByName('@CodigoUsuario').Value      := 'WEB'; // La variable "UsuarioSistema" esta vacio en este momento;   // SS_1311_CQU_20150622
                parameters.ParamByName('@CodigoUsuario').Value      := 'WEB-STD';                                                             // SS_1311_CQU_20150622
                CommandTimeOut := 30;
                Execproc;
                result := parameters.ParamByName('@RETURN_VALUE').Value = 0;
                if not result then begin
                    Error := IntToStr(parameters.ParamByName('@RETURN_VALUE').Value) + ' ' + parameters.ParamByName('@DescripcionError').Value;
                    QueryExecute(DM.Base, 'ROLLBACK TRAN trxRegistrarPagoPendienteSTD');
                    Exit;
                end;
                QueryExecute(DM.Base, 'COMMIT TRAN trxRegistrarPagoPendienteSTD');
                Break;
            end;
        except
            on e: exception do begin
                QueryExecute(DM.Base, 'ROLLBACK TRAN trxRegistrarPagoPendienteSTD');
                if (pos('DEADLOCK', UpperCase(e.Message)) > 0) then begin
                    mensajeDeadlock := e.message;
                    inc(Reintentos);
                    sleep(2000);
                end
                else if (pos('TIMEOUT', UpperCase(e.Message)) > 0) then begin
                    mensajeTimeOut := e.message;
                    inc(Reintentos);
                    sleep(2000);
                end
                else begin
                    result := false;
                    error := e.Message;
                    exit;
                end;
            end;
        end;
    end;
    //una vez que se reintento 5 veces x deadlock lo informo
    if Reintentos = MAX_REINTENTOS then begin
          QueryExecute(DM.Base, 'ROLLBACK TRAN trxRegistrarPagoPendienteSTD');
          result := false;
          error := Trim(mensajeDeadLock)+Trim(mensajeTimeOut);
          Exit;
    end;
end;
//----------------------------------------------------------------------------------------------------------------
//END : SS_1353_NDR_20150729 -----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

end.
