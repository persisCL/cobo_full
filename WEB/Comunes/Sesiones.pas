{********************************** Unit Header ********************************
File Name : Sesiones.pas
Author :
Date Created:
Language : ES-AR
Description :


Revision : 2
    Author : mpiazza
    Date : 07/08/2009
    Description : SS 511  WebCaptcha
            - Se agregan funciones para la generacion y validacion de Captcha

   Firma       : SS_1113_MCA_20130704
    Descripcion : se agregan parametros generales con el tiempo de expiracion para la sesion y pago WEB

	Firma		: SS_1248_MCA_20150602
	Descripcion	: se cambia mensaje de password 

    Firma       : SS_1304_MCA_20150709
    Descripcion : se agrega grilla con todas las deudas del RUT

    Firma       : SS_1280_MCA_20150814
    Descripcion : se agrega parametro EsOficinaVirtual para indicar si la sesion fue iniciada a traves de la oficina virtual o no.

*******************************************************************************}
unit Sesiones;

interface

uses
    sysutils, HTTPApp, Variants, dmConvenios, utildb, funcionesweb, utilhttp;

type
    TLoginResult = (lrOK, lrNoUser, lrManyUsers, lrNoPassword, lrWrongPassword, lrAlreadyInSession, lrPasswordReseteadoVencido);


function DebeCambiarPassword(DM: TdmConveniosWeb; CodigoSesion: integer): boolean;

function CheckLogin(DM: TdmConveniosWeb; RUT, Password, IP: string; var CodigoSesion, CodigoPersona: integer): TLoginResult;
//function GenerarSesion(DM: TdmConveniosWeb; CodigoPersona: integer; IP: string;): integer;                                     //SS_1280_MCA_20150814
function GenerarSesion(DM: TdmConveniosWeb; CodigoPersona: integer; IP: string; EsOficinaVirtual: Boolean): integer;             //SS_1280_MCA_20150814
function VerificarSesion(DM: TdmConveniosWeb; Request: TWEBRequest; CodigoSesion: integer; var Plantilla: string): boolean;
function VerificarConvenio(DM: TdmConveniosWeb; CodigoSesion, CodigoConvenio: integer; var Error: string): boolean;

function ChequearNuevoPassword(Nuevo, Repeticion: string; var Error: string): boolean;
function CambiarPassword(DM: TdmConveniosWeb; CodigoSesion: integer; Actual, Nuevo, Repeticion, Pregunta, Respuesta: string; var Error: string): boolean;
function CambiarPreguntaSecreta(DM: TdmConveniosWeb; CodigoSesion: integer; Password, pregunta, respuesta: string; var Error: string): boolean;

function GenerarSesionCaptcha(DM: TdmConveniosWeb; CodigoDocumento,  IP, Captcha: string): integer;
function ObtenerSesionCaptcha(DM: TdmConveniosWeb; IP : string): string;

implementation

function DebeCambiarPassword(DM: TdmConveniosWeb; CodigoSesion: integer): boolean;
begin
    result := QueryGetValueInt(DM.Base, 'WEB_CV_DebeCambiarPassword ' + inttostr(CodigoSesion)) <> 0;
end;

function VerificarConvenio(DM: TdmConveniosWeb; CodigoSesion, CodigoConvenio: integer; var Error: string): boolean;

begin
    result := false;

    if CodigoConvenio = 0 then begin
        Error := 'Debe seleccionar un convenio';
        exit;
    end;

    if QueryGetValueInt(dm.Base, 'SELECT COUNT(*) FROM Convenio C  WITH (NOLOCK) INNER JOIN WEB_CV_Sesiones S  WITH (NOLOCK) ON C.CodigoCliente = S.CodigoPersona WHERE C.CodigoConvenio = ' + inttostr(CodigoConvenio) + ' AND S.CodigoSesion = ' + inttostr(CodigoSesion)) = 0 then begin
        Error := 'No puede editar este convenio. Su IP ha sido registrada.';
        exit;
    end;

    result := true;
end;

function CambiarPreguntaSecreta(DM: TdmConveniosWeb; CodigoSesion: integer; Password, pregunta, respuesta: string; var Error: string): boolean;
begin
    result := false;
    if trim(pregunta) = '' then begin
        Error := 'Debe especificar una pregunta.';
        exit;
    end;
    if trim(respuesta) = '' then begin
        Error := 'Debe especificar una respuesta.';
        exit;
    end;
    with dm.spCambiarPregunta do begin
        parameters.ParamByName('@CodigoSesion').Value := CodigoSesion;
        parameters.ParamByName('@Password').Value := Password;
        if Pregunta <> '' then begin
          parameters.ParamByName('@Pregunta').Value := Pregunta;
        end;
        if Respuesta <> '' then begin
          parameters.ParamByName('@Respuesta').Value := Respuesta;
        end;
        execproc;
        result := (Parameters.ParamByName('@RETURN_VALUE').Value = 0);
        if not result then begin
            Error := 'El password ingresado no es correcto.';
        end;
    end;
end;

function ChequearNuevoPassword(Nuevo, Repeticion: string; var Error: string): boolean;
begin
    result := false;
    if Nuevo <> Repeticion then begin
        Error := 'El nuevo password no coincide con su repeticion.';
        exit;
    end;
    if Nuevo = '' then begin
        Error := 'El nuevo password no puede estar vac�o.';
        exit;
    end;
    if Length(Nuevo) < 6 then begin
        Error := 'El nuevo password debe tener como minimo 6 caracteres.';
        exit;
    end;
    result := true;
end;
{----------------------------------------------------------------------------
Revision : 1
    Date: 02/03/2010
    Author: mpiazza
    Description:  ss-511 se agrega parametros para
                  modificar pregunta y respuesta secreta

-----------------------------------------------------------------------------}
function CambiarPassword(DM: TdmConveniosWeb; CodigoSesion: integer; Actual, Nuevo, Repeticion, Pregunta, Respuesta: string; var Error: string): boolean;
begin
    result := false;
    if not ChequearNuevoPassword(Nuevo, Repeticion, Error) then exit;
    if Nuevo = Actual then begin
        Error := 'El nuevo password no puede ser igual al anterior. Favor ingresar una nueva password';			//SS_1248_MCA_20150602
        exit;
    end;
    with dm.spCambiarPassword do begin
        parameters.ParamByName('@CodigoSesion').Value := CodigoSesion;
        parameters.ParamByName('@Password').Value := Actual;
        parameters.ParamByName('@NuevoPassword').Value := Nuevo;
        parameters.ParamByName('@Pregunta').Value := Pregunta;
        parameters.ParamByName('@Respuesta').Value := Respuesta;

        execproc;
        result := (Parameters.ParamByName('@RETURN_VALUE').Value = 0);
        if not result then begin
            Error := 'El password actual ingresado no es correcto.';
        end;
    end;
end;

function VerificarSesion(DM: TdmConveniosWeb; Request: TWEBRequest; CodigoSesion: integer; var Plantilla: string): boolean;
var
    Error: string;
    TiempoExpiracionSesionWEB: Integer; //SS_1113_MCA_20130704
begin
    TiempoExpiracionSesionWEB := QueryGetValueInt(dm.Base, 'SELECT ISNULL(Valor, ValorDefault) FROM ParametrosGenerales WITH (NOLOCK) WHERE Nombre = ''TIEMPO_EXPIRACION_SESION_WEB'''); //SS_1113_MCA_20130704
    result := false;
    // borro las sesiones vencidas
    dm.Base.Execute(
      'DELETE FROM WEB_CV_Sesiones WHERE ValidaHasta < GETDATE()'
    );
    Error := '';
    if CodigoSesion < 0 then begin
        Error := 'Debe iniciar sesi�n para continuar.';
    end else if QueryGetValueInt(dm.Base, 'SELECT COUNT(*) FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion)) = 0 then begin
        Error := 'Su sesi�n ha expirado. Debe iniciar sesi�n nuevamente para continuar.';
    // verifico que provenga del IP que tengo registrado.
    end else if QueryGetValueInt(dm.Base, 'SELECT COUNT(*) FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion) + ' AND IP = ' + quotedstr(Request.RemoteAddr)) = 0 then begin
        //Error := 'Se ha detectado un cambio de IP. En este momento hay una sesi�n con su RUT iniciada en otra ubicaci�n de red. Espere 10 minutos y vuelva a intentarlo.';
        Error := 'Se ha detectado un cambio de IP. En este momento hay una sesi�n con su RUT iniciada en otra ubicaci�n de red. Espere ' + IntToStr(TiempoExpiracionSesionWEB) + ' minutos y vuelva a intentarlo.';  //SS_1113_MCA_20130704
    end;

    if Error <> '' then begin
        Plantilla := ObtenerPlantilla(DM, Request, 'Login');
        Plantilla := ReplaceTag(Plantilla, 'ErrorLogin', Error);
        Plantilla := ReplaceTag(Plantilla, 'RUT', '');
        Plantilla := ReplaceTag(Plantilla, 'RUTDV', '');
        dm.Base.Execute('DELETE A FROM WEB_CV_DetalleDeudaConvenios A LEFT JOIN WEB_CV_Sesiones B WITH (NOLOCK) ON A.CodigoSesion=B.CodigoSesion WHERE ValidaHasta <= GETDATE()-0.5');      //SS_1304_MCA_20150709
        exit;
    end;

    result := true;
    // La sesion esta ok, le extiendo el tiempo
    dm.Base.Execute(
      'UPDATE WEB_CV_Sesiones SET ValidaHasta = DATEADD(mi, ' + IntToStr(TiempoExpiracionSesionWEB) + ', GetDate()) WHERE CodigoSesion = ' + inttostr(CodigoSesion)        //SS_1113_MCA_20130704
    );
end;

//function GenerarSesion(DM: TdmConveniosWeb; CodigoPersona: integer; IP: string;): integer;                               //SS_1280_MCA_20150814
function GenerarSesion(DM: TdmConveniosWeb; CodigoPersona: integer; IP: string; EsOficinaVirtual: Boolean): integer;       //SS_1280_MCA_20150814
begin
    result := random(100000000);
    with dm.spCrearSesion do begin
        parameters.ParamByName('@CodigoSesion').Value := result;
        parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
        parameters.ParamByName('@IP').Value := IP;
        Parameters.ParamByName('@EsOficinaVirtual').Value := EsOficinaVirtual;   //SS_1280_MCA_20150814
        execproc;
    end;
end;

{*******************************************************************************
Revision : 1
    Author : nefernandez
    Date : 29/04/2008
    Description : SS 692: Se agrega el valor por defecto al par�metro @CodigoPersona
    porque estaba dando este error en Producci�n.
*******************************************************************************}
function CheckLogin(DM: TdmConveniosWeb; RUT, Password, IP: string; var CodigoSesion, CodigoPersona: integer): TLoginResult;
begin
    with dm.spLogin do begin
        parameters.ParamByName('@RUT').Value := RUT;
        parameters.ParamByName('@Password').Value := Password;
        parameters.ParamByName('@IP').Value := IP;
        parameters.ParamByName('@CodigoSesion').Value := NULL;
        parameters.ParamByName('@CodigoPersona').Value := NULL;
        execproc;
        result := TLoginResult(parameters.ParamByName('@RETURN_VALUE').Value);

        if parameters.ParamByName('@CodigoPersona').Value > 0 then				//SS_1305_MCA_20150612
            CodigoPersona := parameters.ParamByName('@CodigoPersona').Value;	//SS_1305_MCA_20150612

        if result = lrOK then begin                                         
            // puede ser que tenga una sesion ya abierta ese usuario desde el mismo
            // IP, en ese caso nos devuelve el ID
            if parameters.ParamByName('@CodigoSesion').Value = NULL then begin
                CodigoSesion := 0;
            end else begin
                CodigoSesion := parameters.ParamByName('@CodigoSesion').Value;
            end;
        end;
    end;
end;
{******************************** Function Header ******************************
Function Name: GenerarSesionCaptcha
Author : mpiazza
Date Created : 09/09/2009
Parameters : DM: TdmConveniosWeb; CodigoDocumento,  IP, Captcha: string
Return Value : string
Description : inicia la sesion captcha

Revision : 1
    Author :
    Date :
    Description :
*******************************************************************************}
function GenerarSesionCaptcha(DM: TdmConveniosWeb; CodigoDocumento,  IP, Captcha: string): integer;
begin

    with dm.spIniciarWEB_Captcha do begin
        parameters.ParamByName('@CodigoSesion').Value := NULL;
        parameters.ParamByName('@NumeroDocumento').Value := CodigoDocumento;
        parameters.ParamByName('@IP').Value := IP;
        parameters.ParamByName('@Captcha').Value := CaptCha;
        execproc;
        result := parameters.ParamByName('@CodigoSesion').Value;
    end;

end;

{******************************** Function Header ******************************
Function Name: ObtenerSesionCaptcha
Author : mpiazza
Date Created : 09/09/2009
Parameters : DM: TdmConveniosWeb; IP : string
Return Value : string
Description : obtiene segun el ip la sesion y la palabra captcha

Revision : 1
    Author :
    Date :
    Description :
*******************************************************************************}
function ObtenerSesionCaptcha(DM: TdmConveniosWeb; IP : string): string;
begin

    with dm.spWEB_ObtenerCaptcha do begin
        parameters.ParamByName('@CodigoSesion').Value := NULL;
        parameters.ParamByName('@NumeroDocumento').Value := NULL;
        parameters.ParamByName('@IP').Value := IP;
        execproc;
        result := parameters.ParamByName('@Captcha').Value;
    end;


end;

end.
