{
Firma       : SS_1335_NDR_20150707
Descripcion : Se aumenta el TIMEOUT cuando ejecuta un comando de consola a 10 segundos (estaba en 1 decima de segundo)
}
unit Ejecucion;

interface

uses windows, sysutils;

function Ejecutar(Comando: String; var Resultado: string; var Error: string) : boolean;

implementation

function Ejecutar(Comando: String; var Resultado: string; var Error: string) : boolean;
const
    MAX_BUFFER = 1024;
var
   AtributosSeguridad:      TSECURITYATTRIBUTES;
   InformacionInicio:       TSTARTUPINFO;
   InformacionDelProceso:   TPROCESSINFORMATION;
   HandlePipeIn:            THANDLE;            // Pipe input
   HandlePipeOut:           THANDLE;            // Pipe output
   HandleConsoleOut:        THANDLE;            // Pipe output
   HandleEsteProceso:       THANDLE;            // current process handle

   ComandoAEjecutar:        string;
   Leidos:                  cardinal;
   Buffer:                  array[0..MAX_BUFFER] of char;
begin
    Result := false;
    Resultado := '';
    HandleEsteProceso := GetCurrentProcess;

    // averiguo donde esta el procesador de comandos
    ComandoAEjecutar := GetEnvironmentVariable('COMSPEC') + ' /C ' + Comando;

    // Creo el Pipe
    ZeroMemory(@AtributosSeguridad, sizeof(AtributosSeguridad));
    AtributosSeguridad.nLength := sizeof(AtributosSeguridad);
    AtributosSeguridad.bInheritHandle := True;
    if not CreatePipe(HandlePipeOut, HandlePipeIn, @AtributosSeguridad, 0) then begin
        Error := 'Error ' + inttostr(GetLastError);
        exit;
    end;

    // trato de duplicar el handle
    if not DuplicateHandle(HandleEsteProceso, HandlePipeIn, HandleEsteProceso, @HandleConsoleOut, 0, TRUE, DUPLICATE_CLOSE_SOURCE or DUPLICATE_SAME_ACCESS) then begin
        // hay error!!!!
        CloseHandle(HandlePipeOut);
        CloseHandle(HandlePipeIn);
        CloseHandle(HandleConsoleOut);
        Error := 'Error al duplica handles: ' + inttostr(GetLastError);
        exit;
    end;

    // empiezo a llenar la estructura
    ZeroMemory(@InformacionInicio, sizeof(InformacionInicio));
    InformacionInicio.cb := sizeof(InformacionInicio);

    InformacionInicio.dwFlags    := InformacionInicio.dwFlags or STARTF_USESTDHANDLES;
    InformacionInicio.hStdOutput := HandleConsoleOut;
    InformacionInicio.hStdError  := HandleConsoleOut;

    if CreateProcess(nil,                     // lpApplication
                     pChar(ComandoAEjecutar), // lpCommandLine
                     nil,                     // lpProcessAttributes
                     nil,                     // lpThreadAttributes
                     True,                    // bInheritHandles
                     NORMAL_PRIORITY_CLASS or CREATE_NEW_CONSOLE, // dwCreateFlags
                     nil,                     // lpEnvironment
                     nil,                     // lpCurrentDirectory
                     InformacionInicio,       // lpStartupInfo
                     InformacionDelProceso)   // lpProcessInformation
    then begin
        // ni idea de porque se hace...
        CloseHandle(HandleConsoleOut);

        // espero a que termine
        while WaitForSingleObject(InformacionDelProceso.hProcess, 100) <> WAIT_OBJECT_0 do begin         //SS_1335_NDR_20150707
            // mientras tanto, leo del pipe de salida...
            ReadFile(HandlePipeOut, Buffer, MAX_BUFFER, Leidos, nil);
            Resultado := Resultado + copy(buffer, 1, Leidos);
        end;

        // una ultima leida, por si quedo algo en el buffer...
        ReadFile(HandlePipeOut, Buffer, MAX_BUFFER, Leidos, nil);
        Resultado := Resultado + copy(buffer, 1, Leidos);

        // cierro los handles
        // ATENCION: Dentro del debugger puede dar erorr C0000008, ignorarlo
        // desaparece al correrlo en producción (averiguado por internet)
        CloseHandle(InformacionDelProceso.hThread);
        CloseHandle(InformacionDelProceso.hProcess);
        CloseHandle(HandlePipeOut);
        CloseHandle(HandlePipeIn);
        CloseHandle(HandleConsoleOut);

        result := true;
    end else begin
        Error := 'Error al crear proceso: ' + inttostr(GetLastError);
        exit;
    end;
end;

end.
