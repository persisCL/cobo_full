// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL         : http://oficina.vespuciosur.lan/webServiceOficinaVirtual.dll/soap/IOficinaVirtual
// Encoding     : utf-8
// Version      : 1.0
// Firma        : SS_1332_CQU_20150721
// Descripcion  : Interface con el WebService
//
// Firma        : SS_1332_CQU_20151106 (Ref. SS_1390_MGO_20150928)
// Descripcion  : Se agrega m�todo GenerarDetalleComprobante del WS
//
// Firma        : SS_1397_MGO_20151106
// Descripcion  : Se agrega m�todo GenerarTransitosNoFacturados del WS
// ************************************************************************ //

unit IntOficinaVirtual;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[]
  // !:long            - "http://www.w3.org/2001/XMLSchema"[]


  // ************************************************************************ //
  // Namespace : urn:OficinaVirtualIntf-IOficinaVirtual
  // soapAction: urn:OficinaVirtualIntf-IOficinaVirtual#%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // binding   : IOficinaVirtualbinding
  // service   : IOficinaVirtualservice
  // port      : IOficinaVirtualPort
  // URL       : http://oficina.vespuciosur.lan/webServiceOficinaVirtual.dll/soap/IOficinaVirtual
  // ************************************************************************ //
  IOficinaVirtual = interface(IInvokable)
  ['{84617D88-6AE6-EFFB-01C6-14521CC8E61A}']
    function  GenerarComprobante(const CodigoSesionWeb, TipoComprobante: WideString; const NumeroComprobante: Int64): WideString; stdcall;
    function  GenerarDetalleComprobante(const CodigoSesionWeb, TipoComprobante : AnsiString; NumeroComprobante : Int64; FormatoArchivo: AnsiString): WideString; stdcall;	// SS_1332_CQU_20151106
    function  ConfirmarImpresion(const CodigoSesionWeb: WideString; const Archivo: WideString): WideString; stdcall;														// SS_1332_CQU_20151106
    function  GenerarTransitosNoFacturados(const CodigoSesionWeb : WideString; CodigoConvenio, IndiceVehiculo : Int64): WideString; stdcall;                                        //SS_1397_MGO_20151106
  end;

function GetIOficinaVirtual(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): IOficinaVirtual;


implementation
  uses SysUtils;

function GetIOficinaVirtual(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): IOficinaVirtual;
const
  //defWSDL = 'http://oficina.vespuciosur.lan/webServiceOficinaVirtual.dll/wsdl/IOficinaVirtual';
  //defURL  = 'http://oficina.vespuciosur.lan/webServiceOficinaVirtual.dll/soap/IOficinaVirtual';
  defSvc  = 'IOficinaVirtualservice';
  defPrt  = 'IOficinaVirtualPort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defSvc
    else
      Addr := defPrt;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as IOficinaVirtual);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(IOficinaVirtual), 'urn:OficinaVirtualIntf-IOficinaVirtual', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(IOficinaVirtual), 'urn:OficinaVirtualIntf-IOficinaVirtual#%operationName%');

end.