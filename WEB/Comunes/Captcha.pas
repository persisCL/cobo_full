{********************************** Unit Header ********************************
file: Captcha.pas
Author : mpiazza
Date : 09/09/2009
Description : SS 511:
    -  Clase para la generacion de palabras aleatorias
      - palabras aleatorias
      _ Generar Imagen Captcha

*******************************************************************************}

unit Captcha;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, jpeg, DmiCtrls, IdBaseComponent, IdComponent,
  IdCustomTCPServer, IdCustomHTTPServer, IdHTTPServer,IdContext;

type
 TCaptCha = class( TObject)
 private
    Procedure letrasAnguladas(var img : TImage; c : string; angulo : integer; nextPos : Integer; color: Tcolor);

    Function generaImg(var img : TImage; palabra : string):string;
    Procedure GeneraRuido(var img : TImage; Factor:integer);
    procedure JPEGtoBMP(const FileName: TFileName);
    function ConvertBMP2JPG(var Instream: TStream):TMemoryStream;
 public
    function PalabraAleatoria(Longitud: integer): string;
    function GeneraStreamCaptCha(var img : TImage; Factor:integer; var Instream: TStream; sCadena : string ):string;
 end;

implementation

{******************************** Procedure Header *****************************
Procedure Name:  letrasAnguladas
Author : mpiazza
Date Created : 09/09/2009
Parameters : img : TImage; c : string; angulo : integer; nextPos : Integer; color: Tcolor
Description : va imprimiendo en una imagen cada una de las letras en forma no alineada

Revision : 1
    Author :
    Date :
    Description :
*******************************************************************************}
Procedure TCaptCha.letrasAnguladas(var img : TImage; c : string; angulo : integer; nextPos : Integer; color: Tcolor);
var
   logfont:TLogFont;
   font: Thandle;
begin
   LogFont.lfheight:=30;
   logfont.lfwidth:=10;
   logfont.lfweight:=900;

   LogFont.lfEscapement:=angulo;
   logfont.lfcharset:=1;
   logfont.lfoutprecision:=OUT_TT_ONLY_PRECIS;
   logfont.lfquality:= DEFAULT_QUALITY;
   logfont.lfpitchandfamily:= FF_SWISS;
   logfont.lfUnderline := 0;
   logfont.lfStrikeOut := 0;

   font:=createfontindirect(logfont);

   Selectobject(img.canvas.handle,font);

   SetTextColor(img.canvas.handle,rgb(color,180,color));
   SetBKmode(img.canvas.handle,transparent);

   img.canvas.textout(nextPos,5,c);
   SetTextColor(img.canvas.handle,rgb(Random(255) ,Random(255),Random(255)));
   deleteobject(font);
end;


{******************************** Procedure Header *****************************
Procedure Name:  PalabraAleatoria
Author : mpiazza
Date Created : 09/09/2009
Parameters : Longitud: integer
Description : Genera una palabra aleatoria

Revision : 1
    Author : mpiazza
    Date : 03/05/2010
    Description : SS-511: se cambia los caracteres a usar.
                  - Se eliminan los numero
                  - Se elimina la letra O del conjunto de letras posibles
*******************************************************************************}
function TCaptCha.PalabraAleatoria(Longitud: integer): string;
const
  Letras = 'ABCDEFGHIJKLMNPQRSTUVWXYZ';
var
  n  : integer;
begin
  Result:='';
  for n:=1 to Longitud do Result:=Result+Letras[1+Random(Length(Letras))];

   if length(Result) > Longitud then
      Result := copy(Result,1,Longitud);

end;

{******************************** Procedure Header *****************************
Procedure Name:
Author : mpiazza
Date Created : 09/09/2009
Parameters : var Instream: TStream
Description : pasa un stream bmp a formato jpg en stream

Revision : 1
    Author :
    Date :
    Description :
*******************************************************************************}
function TCaptCha.ConvertBMP2JPG(var Instream: TStream):TMemoryStream;
var
	JpegImage   : TJpegImage;
  Bitmap      : TBitmap;
  resultado   : TMemoryStream;
begin
	JpegImage   := TJpegImage.Create();
  Bitmap      := TBitmap.Create();
  try    // Load and display the bitmap image
    Bitmap.LoadFromStream(InStream);
    InStream.Position := 0;
    JpegImage.Assign(Bitmap);

    JpegImage.SaveToStream( InStream  );

  finally
    JpegImage.Free;
    Bitmap.Free;
  end;
end;

{******************************** Procedure Header *****************************
Procedure Name:
Author : mpiazza
Date Created : 09/09/2009
Parameters : var img : TImage; palabra : string
Description : Genera cada una de las letras

Revision : 1
    Author :
    Date :
    Description :
*******************************************************************************}
function TCaptCha.generaImg(var img : TImage; palabra : string):string;
var
   vX : integer;
begin

   for vX := 1 to Length(palabra) do
      letrasAnguladas(img, palabra[vX],(Random(800)+1 -400),30*vX-15, Random(8000));

   result := palabra;
end;

{******************************** Procedure Header *****************************
Procedure Name:
Author : mpiazza
Date Created : 09/09/2009
Parameters : var img: TImage; Factor: integer
Description : Genera todo el ruido en la imagen para que esta sea dificil de
              pasar por el OCR

Revision : 1
    Author :
    Date :
    Description :
*******************************************************************************}
procedure TCaptCha.GeneraRuido(var img: TImage; Factor: integer);
var
   Height : integer;
   Width  : integer;
   PuntosTotal : integer;
   color : tcolor;
   Index :integer;
begin

   Height := img.Picture.Height;
   Width  := img.Picture.Width;

   PuntosTotal := (Height * Width) Div Factor;
   for Index := 0 to Factor do begin
     color := Random(8000) ;
     img.Canvas.Pixels[ Random(Width), Random(Height) ] := color;
   end;

   for Index := 0 to Width do begin
     color := Random(8000) ;
     img.Canvas.Pixels[ Random(1) + Index , Index - 30+ (Round(Sin(Index)) * 5) ] := color;
     img.Canvas.Pixels[ Index , Random(5)  ] := color;
     img.Canvas.Pixels[ Index , Random(5) +25  ] := color;
   end;


end;

{******************************** Procedure Header *****************************
Procedure Name:
Author : mpiazza
Date Created : 09/09/2009
Parameters : FileName: TFileName
Description : pasa una imagen jpeg a bmp (para uso futuro)

Revision : 1
    Author :
    Date :
    Description :
*******************************************************************************}
procedure TCaptCha.JPEGtoBMP(const FileName: TFileName);
var
  jpeg: TJPEGImage;
  bmp:  TBitmap;
begin
  jpeg := TJPEGImage.Create;
  try
    jpeg.CompressionQuality := 100; {Default Value}
    jpeg.LoadFromFile(FileName);
    bmp := TBitmap.Create;
    try
      bmp.Assign(jpeg);
      bmp.SaveTofile(ChangeFileExt(FileName, '.bmp'));
    finally
      bmp.Free
    end;
  finally
    jpeg.Free
  end;
end;

{******************************** Procedure Header *****************************
Procedure Name:
Author : mpiazza
Date Created : 09/09/2009
Parameters : img: TImage; Factor: integer; Instream: TStream; sCadena : string
Description :  Genra una imagen en stream con formato jpg para que sea
              utilizada como captcha

Revision : 1
    Author :
    Date :
    Description :
*******************************************************************************}
function TCaptCha.GeneraStreamCaptCha( var img: TImage;
  Factor: integer; var Instream: TStream; sCadena : string ): string;
begin
    sCadena := generaImg(img, sCadena);
    GeneraRuido( img, 200+ Random(500));
    img.Picture.Bitmap.SaveToStream(Instream);
    Instream.Position := 0;
    ConvertBMP2JPG( Instream);
    Instream.Position := 0;
    result := sCadena;
end;

end.
