{-----------------------------------------------------------------------------
 File Name      :   ImpresionDocumentos
 Author         :   Claudio Quezada Ib��ez
 Firma          :   SS_1332_CQU_20150721
 Date Created   :   21-07-2017
 Language       :   ES-CL
 Description    :   Clase Gen�rica encargada de imprimir los documentos de la OficinaVirtual
                    inicialmente seran solo comprobantes NK.
                    - Se eliminan los archivos generados que tengan una antiguedad
                    mayor a PARAMETRO minutos.
                    - Los comprobantes ser�n grabados en la carpeta OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS
                    y se entregar� un link a la CarpetaVirtual, configurada en IIS, que apunte a ese directorio.

                    OJO: Se hace entrega para CN, se eliminan firmas y c�digo inservible (comentarios)

                    - Se reemplaza el agregarlog por el GrabarLogBaseDatos
                    para que registre si el parametro esta activado. (Ref. SS_1332_MCA_20150817)
                    - Se agrega m�todo GenerarDetalleComprobante y adapta la clase
                    para permitir la descarga de reportes de tr�nsitos de un Comprobante
                    en formato PDF o CSV. (Ref. SS_1390_MGO_20150928)
                    - Se agrega par�metro a la generaci�n del detalle para que se muestre el logo de la web (Ref. SS_1397_MGO_20151015)
                    Ref. SS_1332_CQU_20151106
                        - Se realizan algunos ajustes menores, por ejemplo se agrega una funci�n para liberar la DTE
                        - Se utiliza la funci�n UpperCase para obligar que el TipoComprobante sea en may�scula.
                        - Se corrige un error generado por la SS_1307 ya que en ella se cambi�
                        el m�todo Inicializar de frmReporteBoletaFacturaElectronica
                        y se pone por defecto en "oiSistema" sin considerar la variable "EsWEB"
                        que se pasa en el mismo inicializar.
                        - Se mueve la inicializaci�n de variables desde el Create al Inicilizar.
                        - Se agrega la impresi�n y compresi�n del comprobante y detalle.
                        - Cuando se llama a GenerarNombreArchivo dicha funci�n busca
                        si es que se ha generado un PDF con anterioridad, se quita el filtro "pdf"
                        para que busque cualquier archivio (ZIP / PDF).
                        - Se agrega la eliminaci�n de los archivos ZIP
                        - Se modifica la forma de imprimir, ahora se usa la existente en ManejoDatos que es LA MISMA
                        que se implement� ac� incialmente.
                        - Se agregan "$REGION" para "acortar" el codigo, con estas secciones
                        podemos tener ordenado de mejor manera y es m�s accesible.

 Firma        : SS_1397_MGO_20151106
 Description  : Se agrega m�todo Inicializar para tr�nsitos no facturados y GenerarTransitosNoFacturados

 Firma         	: SS_1332_CQU_20151116
 Descripcion    : Se agrega una validaci�n cuando no encuentra el tipo de comprobante

 Firma         	: SS_1332_CQU_20151229
 Description    : Se agrega una validaci�n de conexion con el servidor.
 -----------------------------------------------------------------------------}
unit ImpresionDocumentos;

interface

uses
    SysUtils, StrUtils, Util, RStrings, SysUtilsCN,
    frmReporteBoletaFacturaElectronica, ConstParametrosGenerales,
    Classes, dmConvenios, ManejoDatos, ManejoErrores, PeaTypes,
    Types, ADODB, DB, PeaProcs, Variants, DateUtils, Windows,                   //SS_1390_MGO_20150928
	ReporteFactura, Zip,														// SS_1332_CQU_20151106
    frmReporteFacturacionDetallada, CSVUtils, frmReporteTransitos;              //SS_1397_MGO_20151106       //SS_1390_MGO_20150928

type

    TImpresionDocumentos = class
    private
        FConexion           : TdmConveniosWEB;
        FCodigoSesion,
        FNumeroComprobante  : Int64;
        FUsuarioSistema,
        FDirectorioPDF,
        FDirectorioDBNet,
        FTipoComprobante    : string;
        FFecha              : TDateTime;
        FMinutosBorrar      : Integer;
        FGrabarLog          : Boolean;
        FOrigenImpresion    : TOrigenImpresion; // SS_1332_CQU_20150917
        FCodigoConvenio     : Int64;            //SS_1397_MGO_20151106
        FIndiceVehiculo     : Int64;            //SS_1397_MGO_20151106
        /// <summary>
        /// Verifica que la sesi�n indicada se encuentre v�lida en el servidor WEB
        /// </summary>
        function ValidarSesion : Boolean;
        /// <summary>
        /// Genera el nombre y ruta del archivio PDF
        /// </summary>
        /// <param name="SoloNombre">Variable que devolver� UNICAMENTE el nombre del archivo</param>
        /// <param name="YaExiste">Variable que indicar� si la NK ya fue impresa durante el d�a</param>
        /// <param name="Extension">Extensi�n del archivo a generar ('.pdf' o '.csv'). Por defecto: '.pdf'</param>
        /// <param name="EsDetalle">Variable que indicar� si el archivo a generar es Detalle de un Comprobante. Por defecto: False</param>
        /// <returns>Ruta completa del archivo</returns>
        function GenerarNombreArchivo(var SoloNombre : string; out YaExiste : Boolean; Extension : String = '.pdf'; EsDetalle: Boolean = False) : string;       //SS_1390_MGO_20150928
        /// <summary>
        /// Borra los archivos con una antiguedad mayor a 10 minutos
        /// </summary>
        /// <param name="pTipoArchivo">Tipo Archivo a Borrar (JPG, TED, PDF)</param>
        /// <param name="pRuta">Carpeta a Borrar</param>
        procedure BorrarArchivos(pTipoArchivo, pRuta : string);
        /// <summary>
        /// Obtiene la fecha de un archivo
        /// </summary>
        /// <param name="Nombre">Nombre del archivo a consultar</param>
        /// <returns>Retorna la fecha de creacion</returns>
        function GetFileDateTime(Nombre: string): TDateTime;
        /// <summary>
        /// Graba en la tabla WEB_CV_LOG s�lo si se ha indicado en el par�metro general
        /// </summary>
        /// <param name="Mensje">Texto a grabar en la Base de Datos</param>
        procedure GrabarLogBaseDatos(Mensje : string);
    public
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="CodigoSesionWeb">Codigo de la sesion en la que se desea trabajar</param>
        /// <param name="pUsuarioSistema">Codigo de usuario que utiliza el WebService</param>
        /// <param name="pOrigenImpresion">Inidica el Origen de la llamada (ver TOrigenImpresion)</param>			// SS_1332_CQU_20151106
        constructor Create(const CodigoSesionWeb, pUsuarioSistema : String; pOrigenImpresion : TOrigenImpresion);	// SS_1332_CQU_20151106
        /// <summary>
        /// Destructor de la Clase
        /// </summary>
        destructor Destroy;
        /// <summary>
        /// Funcion que inicializa los valores principales de la clase
        /// </summary>
        /// <param name="TipoComprobante">Tipo del comprobante que desea imprimir (Solo acepta NK)</param>
        /// <param name="NumeroComprobante">N�mero del comprobante que desea imprimir</param>
        /// <param name="Respuesta">Mensaje de error (si corresponde)</param>
        /// <returns>True si se puede imprimir; False si no</returns>
        function Inicializar(const TipoComprobante : AnsiString; const NumeroComprobante : Int64; out Respuesta: AnsiString) : Boolean; overload;  //SS_1397_MGO_20151106
        {********************************
        * BEGIN : SS_1397_MGO_20151106  *
        *********************************}
        /// <summary>
        /// Funcion que inicializa los valores principales de la clase
        /// </summary>
        /// <param name="CodigoConvenio">El Codigo de Convenio para el cual se imprime el reporte</param>
        /// <param name="IndiceVehiculo">Indice del veh�culo para el cual se imprime el reporte (-1 = Todos)</param>
        /// <param name="Respuesta">Mensaje de error (si corresponde)</param>
        /// <returns>True si se puede imprimir; False si no</returns>
        function Inicializar(const CodigoConvenio, IndiceVehiculo : Int64; out Respuesta: AnsiString) : Boolean; overload;
        /// <summary>
        /// Genera un archivo en formato PDF con el reporte de tr�nsitos no facturados
        /// </summary>
        /// <param name="Respuesta">Mensaje de error o ruta del archivo generado</param>
        /// <returns>True si pudo generar el archvio; False si no</returns>
        function GenerarTransitosNoFacturados(out Respuesta : AnsiString) : Boolean;
        {********************************
        * END   : SS_1397_MGO_20151106  *
        *********************************}
        /// <summary>
        /// Genera un archivo en formato PDF con el comprobante indicado
        /// </summary>
        /// <param name="Respuesta">Mensaje de error o ruta del archivo generado</param>
        /// <returns>True si pudo generar el archvio; False si no</returns>
        function GenerarComprobante(out Respuesta : AnsiString) : Boolean;
        {********************************
        * BEGIN : SS_1390_MGO_20150928  *
        *********************************}
        /// <summary>
        /// Genera un archivo en formato PDF o CSV con el detalle del comprobante indicado
        /// </summary>
        /// <param name="FormatoArchivo">Formato del archivo a generar (PDF o CSV)</param>
        /// <param name="Respuesta">Mensaje de error o ruta del archivo generado</param>
        /// <returns>True si pudo generar el archvio; False si no</returns>
        function GenerarDetalleComprobante(FormatoArchivo : AnsiString; out Respuesta : AnsiString) : Boolean;
        {********************************
        * END   : SS_1390_MGO_20150928  *
        *********************************}
        /// <summary>
        /// Confirma la impresion del comprobante
        /// </summary>
        /// <param name="Archivo">Nombre del archivo a confirmar</param>
        /// <param name="Respuesta">Mensaje de error o OK si se borr� el archivo</param>
        procedure ConfirmarImpresion(const NombreArchivo : AnsiString; out Respuesta : AnsiString);
    end;

implementation

const
    // Tabla WEB_CV_Bitacora_Accion
    BitacoraAccion_SesionExpirada               =   4;
    BitacoraAccion_ImpresionComprobante         =   5;
    BitacoraAccion_ErrorImpresionComprobante    =   6;
    BitacoraAccion_ImpresionDetalleTran         =   7;
    BitacoraAccion_ErrorImpresionDetalleTran    =   8;
    // ParametrosGenerales
    OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS            =   'OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS';
    OFICINA_VIRTUAL_MIN_BORRA_ARCHIVOS          =   'OFICINA_VIRTUAL_MIN_BORRA_ARCHIVOS';
    OFICINA_VIRTUAL_GRABAR_LOG                  =   'OFICINA_VIRTUAL_GRABAR_LOG';
    DIR_WEB_TIMBRE_ELECTRONICO                  =   'DIR_WEB_TIMBRE_ELECTRONICO';
    // Consultas SQL
    SQL_VERIFICA_SESION                         =   'SELECT dbo.EsSesionWEBValida(%d)';
    SQL_VERIFICA_NK                             =   'SELECT dbo.ExisteComprobante(''%s'', %d)';
    SQL_OBTENER_TIPO_COMPROBANTE_FISCAL         = 	'SELECT TipoComprobanteFiscal FROM Comprobantes (NOLOCK) '
                                                    + 'WHERE TipoComprobante = ''%s'' AND NumeroComprobante = %d';
resourcestring
    MSG_LOG_DE_FUNCIONES                        =   'TImpresionDocumentos, %s';
    MSG_SESION_INVALIDA                         =   'Codigo de Sesion es Inv�lido o no es Num�rico' + CRLF;
    MSG_SESION_CADUCADA                         =   'La sesi�n ha caducado' + CRLF;
    MSG_TIPO_COMPROBANTE_INVALIDO               =   'El tipo de comprobante indicado (%s) no es v�lido' + CRLF;
    MSG_NUMERO_COMPROBANTE_INVALIDO             =   'El n�mero del comprobante indicado (%d) no es v�lido' + CRLF;
    MSG_NUMERO_COMPROBANTE_NO_EXISTE            =   'El n�mero del comprobante indicado (%d) no existe' + CRLF;
    MSG_SIN_PARAMETRO_GENERAL                   =   'No se encontr� el par�metro general %s' + CRLF;
    MSG_ERROR_BORRADO                           =   'No se pudo eliminar el archivo %s';
    MSG_ERROR_BORRANDO                          =   'No se pudo eliminar el archivo %s, Mensaje del sisrtema: %s';
    MSG_INICIANDO_BORRADO                       =   'Iniciando borrado (%s)';
    MSG_ARCHIVO_NO_EXISTE                       =   'El archivo %s, no existe';
    MSG_FINALIZANDO_BORRADO                     =   'Finalizando borrado (%s)';
    MSG_CONVENIO_INVALIDO                       =   'El c�digo de convenio indicado (%d) no es v�lido' + CRLF;        //SS_1397_MGO_20151106
    MSG_VEHICULO_INVALIDO                       =   'El �ndice de veh�culo indicado (%d) no es v�lido' + CRLF;        //SS_1397_MGO_20151106
    MSG_ERROR_SIN_CONEXION                      =   'No se puede establecer conexi�n con el servidor';                // SS_1332_CQU_20151229

    {$REGION 'TImpresionDocumentos'}
    constructor TImpresionDocumentos.Create(const CodigoSesionWeb, pUsuarioSistema : String; pOrigenImpresion : TOrigenImpresion);	// SS_1332_CQU_20151106
    begin
        FConexion := TdmConveniosWEB.Create(nil);
        FUsuarioSistema := pUsuarioSistema;
        FCodigoSesion := StrToIntDef(CodigoSesionWeb, -1);	// SS_1332_CQU_20151106
        FOrigenImpresion := pOrigenImpresion;				// SS_1332_CQU_20151106
    end;

    destructor TImpresionDocumentos.Destroy;
    begin
        FConexion.Destroy;	// SS_1332_CQU_20151106
        inherited Destroy;
    end;

    function TImpresionDocumentos.Inicializar(const TipoComprobante : AnsiString; const NumeroComprobante : Int64; out Respuesta: AnsiString) : Boolean overload; //SS_1397_MGO_20151106
    var
        sRespuesta : string;
        tmpInteger : Integer;	// SS_1332_CQU_20151106
    begin
		Result := True;								    // SS_1332_CQU_20151229
        try                                             // SS_1332_CQU_20151229
            FFecha      := NowBaseCN(FConexion.Base);   // SS_1332_CQU_20151229
        except                                          // SS_1332_CQU_20151229
            Respuesta   := MSG_ERROR_SIN_CONEXION;      // SS_1332_CQU_20151229
            Result      := False;                       // SS_1332_CQU_20151229
        end;                                            // SS_1332_CQU_20151229
        if Result then begin                            // SS_1332_CQU_20151229
        // Aumento el Tabulado                          // SS_1332_CQU_20151229
            try
                // INICIO BLOQUE // SS_1332_CQU_20151106
                // Obtener Par�metro General que indica los miutos antes de borrar los rachivos generados				//SS_1397_MGO_20151106
                if not ObtenerParametroGeneral(FConexion.Base, OFICINA_VIRTUAL_GRABAR_LOG, tmpInteger) then				//SS_1397_MGO_20151106
                begin																									//SS_1397_MGO_20151106	
                    sRespuesta := sRespuesta + Format(MSG_SIN_PARAMETRO_GENERAL, [OFICINA_VIRTUAL_GRABAR_LOG]);			//SS_1397_MGO_20151106
                end else FGrabarLog := (tmpInteger = 1);																//SS_1397_MGO_20151106

                GrabarLogBaseDatos(Format(MSG_LOG_DE_FUNCIONES, ['Inicializando Variables']));
                // Obtener Par�metro General donde dejar los PDF
                if not ObtenerParametroGeneral(FConexion.Base, OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS, FDirectorioPDF) then
                begin
                    sRespuesta := sRespuesta + Format(MSG_SIN_PARAMETRO_GENERAL, [OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS]);
                end;

                // Obtener Par�metro General donde est� DBNet
                if not ObtenerParametroGeneral(FConexion.Base, DIR_WEB_TIMBRE_ELECTRONICO, FDirectorioDBNet) then
                begin
                    sRespuesta := sRespuesta + Format(MSG_SIN_PARAMETRO_GENERAL, [DIR_WEB_TIMBRE_ELECTRONICO]);
                end;

                // Obtener Par�metro General que indica los miutos antes de borrar los rachivos generados
                if not ObtenerParametroGeneral(FConexion.Base, OFICINA_VIRTUAL_MIN_BORRA_ARCHIVOS, FMinutosBorrar) then
                begin
                    sRespuesta := sRespuesta + Format(MSG_SIN_PARAMETRO_GENERAL, [DIR_WEB_TIMBRE_ELECTRONICO]);
                end;

    //          // Obtener Par�metro General que indica los miutos antes de borrar los rachivos generados					//SS_1397_MGO_20151106
    //          if not ObtenerParametroGeneral(FConexion.Base, OFICINA_VIRTUAL_GRABAR_LOG, tmpInteger) then					//SS_1397_MGO_20151106
    //          begin																										//SS_1397_MGO_20151106
    //              sRespuesta := sRespuesta + Format(MSG_SIN_PARAMETRO_GENERAL, [OFICINA_VIRTUAL_GRABAR_LOG]);				//SS_1397_MGO_20151106
    //          end else FGrabarLog := (tmpInteger = 1);																	//SS_1397_MGO_20151106

            	// Verifico el c�digo de la sesi�n
            	if FCodigoSesion = -1 then
                	sRespuesta := sRespuesta + MSG_SESION_INVALIDA
            	else if not ValidarSesion then
                	sRespuesta := sRespuesta + MSG_SESION_CADUCADA;

            	// Obtengo la fecha del sistema
            	//FFecha := NowBaseCN(FConexion.Base);                                                                      // SS_1332_CQU_20151229
            	GrabarLogBaseDatos(Format(MSG_LOG_DE_FUNCIONES, ['Inicializaci�n Terminada']));
				// FIN BLOQUE // SS_1332_CQU_20151106
            	// Vrifico el TipoComprobante
            	if  UpperCase(TipoComprobante) <> 'NK' then begin // SS_1332_CQU_20150909
                	sRespuesta := sRespuesta + Format(MSG_TIPO_COMPROBANTE_INVALIDO, [TipoComprobante]);
            	end else
                	FTipoComprobante := TipoComprobante;
            	// Verifico el NumeroComprobante
            	if NumeroComprobante <= 0 then begin
                	sRespuesta := sRespuesta + Format(MSG_NUMERO_COMPROBANTE_INVALIDO, [NumeroComprobante]);
            	end else
                	FNumeroComprobante := NumeroComprobante;

            	// Si no hay error con los par�metros se procesa lo dem�s.
            	if sRespuesta = EmptyStr then begin	// SS_1332_CQU_20151106
                	// Comprobar Existencia Datos
                	if not QueryGetBooleanValue(FConexion.Base, Format(SQL_VERIFICA_NK, [FTipoComprobante, FNumeroComprobante])) then
                	begin
                    	sRespuesta := sRespuesta + Format(MSG_NUMERO_COMPROBANTE_NO_EXISTE, [FNumeroComprobante]);
                	end;
                	// Borro los archivos viejos (N minutos atr�s)
                	ConfirmarImpresion(EmptyStr, sRespuesta);						// SS_1332_CQU_20151106
                	// Grabar el LOG												// SS_1332_CQU_20151106
                	if sRespuesta <> EmptyStr then begin							// SS_1332_CQU_20151106
                    	GrabarLogBaseDatos(sRespuesta);								// SS_1332_CQU_20151106
                    	// Limpio sRespuesta si el mensaje es que termin� de borrar	// SS_1332_CQU_20151106
                    	if Pos('Finalizando borrado', sRespuesta) > 0 then			// SS_1332_CQU_20151106
                        sRespuesta := EmptyStr;									// SS_1332_CQU_20151106
                	end;															// SS_1332_CQU_20151106
            	end else GrabarLogBaseDatos(sRespuesta);
        	finally
            	if sRespuesta = EmptyStr then Result := True						// SS_1332_CQU_20151106
            	else begin
                	Respuesta := sRespuesta;
                	Result := False;
                end;
            end;
        end;                                                                        // SS_1332_CQU_20151229
    end;

    {********************************
    * BEGIN : SS_1397_MGO_20151106  *
    *********************************}
    function TImpresionDocumentos.Inicializar(const CodigoConvenio, IndiceVehiculo : Int64; out Respuesta: AnsiString) : Boolean overload;
    var
        sRespuesta : string;
        tmpInteger : Integer;
    begin
		Result := True;								    // SS_1332_CQU_20151229
        try                                             // SS_1332_CQU_20151229
            FFecha      := NowBaseCN(FConexion.Base);   // SS_1332_CQU_20151229
        except                                          // SS_1332_CQU_20151229
            Respuesta   := MSG_ERROR_SIN_CONEXION;      // SS_1332_CQU_20151229
            Result      := False;                       // SS_1332_CQU_20151229
        end;                                            // SS_1332_CQU_20151229
        if Result then begin                            // SS_1332_CQU_20151229
        // Aumento el Tabulado                          // SS_1332_CQU_20151229
            try
                GrabarLogBaseDatos(Format(MSG_LOG_DE_FUNCIONES, ['Inicializando Variables']));
                // Obtener Par�metro General donde dejar los PDF
                if not ObtenerParametroGeneral(FConexion.Base, OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS, FDirectorioPDF) then
                begin
                    sRespuesta := sRespuesta + Format(MSG_SIN_PARAMETRO_GENERAL, [OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS]);
                end;

                // Obtener Par�metro General que indica los miutos antes de borrar los archivos generados
                if not ObtenerParametroGeneral(FConexion.Base, OFICINA_VIRTUAL_MIN_BORRA_ARCHIVOS, FMinutosBorrar) then
                begin
                    sRespuesta := sRespuesta + Format(MSG_SIN_PARAMETRO_GENERAL, [OFICINA_VIRTUAL_MIN_BORRA_ARCHIVOS]);
                end;

                // Obtener Par�metro General que indica si se guardan los Log
                if not ObtenerParametroGeneral(FConexion.Base, OFICINA_VIRTUAL_GRABAR_LOG, tmpInteger) then
                begin
                    sRespuesta := sRespuesta + Format(MSG_SIN_PARAMETRO_GENERAL, [OFICINA_VIRTUAL_GRABAR_LOG]);
                end else FGrabarLog := (tmpInteger = 1);

                // Verifico el c�digo de la sesi�n
                if FCodigoSesion = -1 then
                    sRespuesta := sRespuesta + MSG_SESION_INVALIDA
                else if not ValidarSesion then
                    sRespuesta := sRespuesta + MSG_SESION_CADUCADA;

                // Obtengo la fecha del sistema
                //FFecha := NowBaseCN(FConexion.Base);  // SS_1332_CQU_20151229
                GrabarLogBaseDatos(Format(MSG_LOG_DE_FUNCIONES, ['Inicializaci�n Terminada']));

                //Verifico los par�metros
                if  CodigoConvenio <= 0 then begin
                    sRespuesta := sRespuesta + Format(MSG_CONVENIO_INVALIDO, [CodigoConvenio]);
                end else
                    FCodigoConvenio := CodigoConvenio;

                if IndiceVehiculo < -1 then begin
                    sRespuesta := sRespuesta + Format(MSG_VEHICULO_INVALIDO, [IndiceVehiculo]);
                end else
                    FIndiceVehiculo := IndiceVehiculo;

                // Si no hay error borro los archivos viejos.
                if sRespuesta = EmptyStr then begin
                    ConfirmarImpresion(EmptyStr, sRespuesta);
                    // Grabar el LOG
                    if sRespuesta <> EmptyStr then begin
                        GrabarLogBaseDatos(sRespuesta);
                        // Limpio sRespuesta si el mensaje es que termin� de borrar
                        if Pos('Finalizando borrado', sRespuesta) > 0 then
                            sRespuesta := EmptyStr;
                    end;
                end else GrabarLogBaseDatos(sRespuesta);
            finally
                if sRespuesta = EmptyStr then Result := True
                else begin
                    Respuesta := sRespuesta;
                    Result := False;
                end;
            end;
        end;                                            // SS_1332_CQU_20151229
    end;

    function TImpresionDocumentos.GenerarTransitosNoFacturados(out Respuesta : AnsiString) : Boolean;
    resourcestring
        MSG_INICIO_IMPRESION    =   'Se ha solicitado imprimir los tr�nsitos no facturados - CodigoConvenio %d IndiceVehiculo %d con la sesi�n %d';
        MSG_FIN_IMPRESION       =   'Se ha finalizado la impresi�n los tr�nsitos no facturados - CodigoConvenio %d IndiceVehiculo %d con la sesi�n %d';
        MSG_IMPRESION_ERROR     =   'Ha ocurrido un error generando el reporte, mensaje del sistema: %s';
    var
        ReporteNoFacturados: TfrmRptTransitos;
        ContenidoArchivo,
        NombreArchivo,
        RutaCompletaArchivo,
        DescriErr : String;
        ArchivoPDF : TStringList;
        YaExisteArchivo : Boolean;
    begin
        try
            try
                // Registrar Bitacora
                GrabarLogBaseDatos(Format(MSG_INICIO_IMPRESION, [FCodigoConvenio, FIndiceVehiculo, FCodigoSesion]));

                ReporteNoFacturados := TfrmRptTransitos.Create(nil);
                GrabarLogBaseDatos('Inicializando Reporte');
                try
                    if not ReporteNoFacturados.GenerarPDFUltimosConsumos(FConexion, FCodigoConvenio, FIndiceVehiculo, ContenidoArchivo, DescriErr) then begin
                        DescriErr := Format(MSG_IMPRESION_ERROR, [DescriErr]);
                        GrabarLogBaseDatos(DescriErr);
                        Exit;
                    end else begin
                        RutaCompletaArchivo := GenerarNombreArchivo(NombreArchivo, YaExisteArchivo);
                        if not YaExisteArchivo then begin
                            StringToFile(ContenidoArchivo, RutaCompletaArchivo);

                            if FileExists(RutaCompletaArchivo)
                            then DescriErr := NombreArchivo
                            else DescriErr := 'No se pudo obtener el reporte';
                            // Grabo el log
                            GrabarLogBaseDatos('Generado Reporte: ' + DescriErr);
                        end else begin
                            DescriErr := NombreArchivo;
                            GrabarLogBaseDatos('Archivo existe, no se ejecuta el reporte: ' + DescriErr);
                        end;
                    end;
                finally
                    
                end;
            except
                on E : Exception do begin
                    DescriErr := Format(MSG_IMPRESION_ERROR, [E.Message]);
                    GrabarLogBaseDatos(DescriErr);
                end;
            end;
        finally
            // Registrar Bitacora
            GrabarLogBaseDatos(Format(MSG_FIN_IMPRESION, [FCodigoConvenio, FIndiceVehiculo, FCodigoSesion]));
            if Assigned(ReporteNoFacturados) then FreeAndNil(ReporteNoFacturados);
            Respuesta := DescriErr;
        end;
    end;
    {********************************
    * END   : SS_1397_MGO_20151106  *
    *********************************}

    procedure TImpresionDocumentos.GrabarLogBaseDatos(Mensje : string);
    begin
        if FGrabarLog then begin
            try
                AgregarLog(FConexion, Format(MSG_LOG_DE_FUNCIONES, [Mensje]));
            except on E : Exception do
                begin
                    ProcesarError(FConexion, Format(MSG_LOG_DE_FUNCIONES, [E.Message]), e);
                    raise Exception.Create(E.Message);	// SS_1332_CQU_20151106
                end;
            end;
        end;
    end;

    function TImpresionDocumentos.ValidarSesion;
    var
        Resultado : Boolean;
    begin
        Resultado := False;
        try
            try
                GrabarLogBaseDatos('Validando sesi�n');
                if QueryGetBooleanValue(FConexion.Base, Format(SQL_VERIFICA_SESION, [FCodigoSesion])) then Resultado := True;
            except
                GrabarLogBaseDatos('No sepudo Validar la sesi�n');
                Resultado := False;
            end;
        finally
            Result := Resultado;
        end;
    end;

    function TImpresionDocumentos.GenerarNombreArchivo(var SoloNombre : string; out YaExiste : Boolean; Extension: String = '.pdf'; EsDetalle: Boolean = False) : string;             //SS_1390_MGO_20150928
    resourcestring
        STR_DETALLE = 'detalle';
        STR_NO_FACTURADOS = '_NoFacturados';									//SS_1397_MGO_20151106
    var
        Nombre,
        FiltroBusqueda,
        Directorio : string;
        SR : TSearchRec;
    begin
        GrabarLogBaseDatos('Generando nombre de archivo PDF');
        Directorio := FDirectorioPDF;
        YaExiste    := False;													// SS_1332_CQU_20151106
        // Genero un filtro para buscar el archivo
        //FiltroBusqueda := GoodDir(Directorio) + FormatDateTime('yyyymmdd', FFecha) + '*' + FTipoComprobante + IntToStr(FNumeroComprobante) + iif(EsDetalle, STR_DETALLE, '') + '*';		//SS_1397_MGO_20151106	// SS_1332_CQU_20151106
        FiltroBusqueda := GoodDir(Directorio) + FormatDateTime('yyyymmdd', FFecha) + '*';                                                                                                   //SS_1397_MGO_20151106
        if FCodigoConvenio > 0 then begin                                                                                                                                                   //SS_1397_MGO_20151106
            //Tr�nsitos no facturados                                                                                                                                                       //SS_1397_MGO_20151106
            FiltroBusqueda := FiltroBusqueda + IntToStr(FCodigoConvenio) + IntToStr(FIndiceVehiculo) + STR_NO_FACTURADOS + '*';                                                             //SS_1397_MGO_20151106
        end else begin                                                                                                                                                                      //SS_1397_MGO_20151106
            //Comprobantes                                                                                                                                                                  //SS_1397_MGO_20151106
            FiltroBusqueda := FiltroBusqueda + FTipoComprobante + IntToStr(FNumeroComprobante) + iif(EsDetalle, STR_DETALLE, '') + '*';                                                     //SS_1397_MGO_20151106
        end;                                                                                                                                                                                //SS_1397_MGO_20151106
         // Busco el archivo, por si se gener� con anterioridad
		// INICIO BLOQUE NUEVO // SS_1332_CQU_20151106
        if SysUtils.FindFirst(FiltroBusqueda, faArchive, SR) = 0
        then begin
            repeat
                // Si no es detalle, podr�a ser un archivo comprimido
                if (not EsDetalle) then begin
                    // Podr�a ser ZIP
                    if (Pos('.zip', SR.Name) > 0) then begin
                        SoloNombre  := SR.Name;
                        Nombre      := GoodDir(FDirectorioPDF) + SoloNombre;
                        YaExiste    := True;
                        Break;
                    // Podr�a ser PDF
                    end else if (Pos(STR_DETALLE, SR.Name) = 0) then begin
                        // Si se gener� hace durante el d�a, devuelvo ese archivo
                        SoloNombre  := SR.Name;
                        Nombre      := GoodDir(FDirectorioPDF) + SoloNombre;
                        YaExiste    := True;
                        break;
                    end;
                // Es detalle, DEBE tener la palabra detalle
                end else if (Pos(STR_DETALLE, SR.Name) > 0) and (Pos(Extension, SR.Name) > 0) then begin
                    // Si se gener� hace durante el d�a, devuelvo ese archivo
                    SoloNombre  := SR.Name;
                    Nombre      := GoodDir(FDirectorioPDF) + SoloNombre;
                    YaExiste    := True;
                    break;
                end;
            until SysUtils.FindNext(SR) <> 0;
            // Cerramos la b�squeda
            SysUtils.FindClose(SR);
        end;
        // Significa que no lo encontr� en el segundo ciclo.
        if not YaExiste then begin
            // Si no se gener�, creo un nuevo nombre.
            //Nombre      := FormatDateTime('yyyymmddhhnnsszzz', FFecha) + FTipoComprobante + IntToStr(FNumeroComprobante) + iif(EsDetalle, STR_DETALLE, '');           //SS_1397_MGO_20151106                         //SS_1390_MGO_20150928
            //SoloNombre  := GoodFileName(Nombre, Extension);																//SS_1397_MGO_20151106
			Nombre := GoodDir(Directorio) + FormatDateTime('yyyymmddhhnnsszzz', FFecha);                                 	//SS_1397_MGO_20151106
            if FCodigoConvenio > 0 then begin                                                                            	//SS_1397_MGO_20151106
                //Tr�nsitos no Facturados                                                                                	//SS_1397_MGO_20151106
                Nombre := Nombre + IntToStr(FCodigoConvenio) + IntToStr(FIndiceVehiculo) + STR_NO_FACTURADOS;            	//SS_1397_MGO_20151106
            end else begin                                                                                               	//SS_1397_MGO_20151106
                //Comprobantes                                                                                           	//SS_1397_MGO_20151106
                Nombre := Nombre + FTipoComprobante + IntToStr(FNumeroComprobante) + iif(EsDetalle, STR_DETALLE, '');    	//SS_1397_MGO_20151106
            end;                                                                                                         	//SS_1397_MGO_20151106
            SoloNombre  := ExtractFileName(Nombre + Extension);																//SS_1397_MGO_20151106
            Nombre      := GoodDir(FDirectorioPDF) + SoloNombre;
            YaExiste    := False;
        end;
		// FIN BLOQUE NUEVO // SS_1332_CQU_20151106
        Result := Nombre;
    end;

    function TImpresionDocumentos.GenerarComprobante(out Respuesta : AnsiString) : Boolean;
    resourcestring
        MSG_INICIO_IMPRESION    =   'Se ha solicitado imprimir el comprobante TipoComprobante %s NumeroComprobante %d con la sesi�n %d';
        MSG_FIN_IMPRESION       =   'Se ha finalizado la impresi�n del comprobante TipoComprobante %s NumeroComprobante %d con la sesi�n %d';
        MSG_IMPRESION_ERROR     =   'Ha ocurrido un error generando el comprobante tipo %s y n�mero %d, mensaje del sistema: %s';
	// INICIO BLOQUE NUEVO // SS_1332_CQU_20151106
    var
        ReporteElectronico        	: TReporteBoletaFacturaElectronicaForm;
        ReporteFactura              : TfrmReporteFactura;
        spObtenerDatosComprobante   : TADOStoredProc;
        Compresor                   : TZip;
        PDFComprobante,
        PDFConsumoAparte,
        NombreArchivoComprobante,
        NombreArchivoAnexo,
        NombreArchivoZIP,
        RutaCompletaArchivo,
        DescriErr                   : String;
        YaExisteArchivo,
        ReporteIniciado,
        FuncionEjecutada,
        ConsumoSeparado,
        EsComprobanteElectronico    : Boolean;
    begin
        try
            try
                // Registrar Bitacora
                GrabarLogBaseDatos(Format(MSG_INICIO_IMPRESION, [FTipoComprobante, FNumeroComprobante, FCodigoSesion]));
                {$REGION 'ObtenerComprobante'}
                // Inicializo el Procedimiento
                spObtenerDatosComprobante := TADOStoredProc.Create(nil);
                // Obtengo los datos del comprobante
                with spObtenerDatosComprobante do begin
                    Connection := FConexion.Base;
                    ProcedureName := 'WEB_CV_ObtenerDatosComprobante';
                    Parameters.Refresh;
                    Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
                    Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
                    try
                        Open;
                        if IsEmpty then																// SS_1332_CQU_20151116
                            raise Exception.Create('No se pudo obtener el comprobante');			// SS_1332_CQU_20151116
                        if  (FieldByName('TipoComprobanteFiscal').AsString = TC_FACTURA_AFECTA) OR
                            (FieldByName('TipoComprobanteFiscal').AsString = TC_FACTURA_EXENTA) OR
                            (FieldByName('TipoComprobanteFiscal').AsString = TC_BOLETA_AFECTA) OR
                            (FieldByName('TipoComprobanteFiscal').AsString = TC_BOLETA_EXENTA) then
                        begin
                            ReporteElectronico := TReporteBoletaFacturaElectronicaForm.Create(nil);
                            EsComprobanteElectronico := True;
                        end else begin
                            ReporteFactura := TfrmReporteFactura.Create(nil);
                            EsComprobanteElectronico := False;
                        end;
                    finally
                        Close;
                    end;
                end;
                // Libero el SP
                spObtenerDatosComprobante.Destroy;
                {$ENDREGION}
                // Genero el Nombre del Archvio (Directorio Incluido) y verifico que no exista
                RutaCompletaArchivo := GenerarNombreArchivo(NombreArchivoComprobante, YaExisteArchivo, '.pdf', False);
                // Verifico que no exista el archivo
                if YaExisteArchivo then begin
                    DescriErr := NombreArchivoComprobante;
                    GrabarLogBaseDatos('Archivo existe, no se ejecuta EjecutarPDF: ' + DescriErr);
                end else begin
                    {$REGION 'GenerarComprobante'}
                    // Si es electr�nico cargo uno tipo de reporte.
                    GrabarLogBaseDatos('Inicializando Reporte');
                    if EsComprobanteElectronico
                    then ReporteIniciado := ReporteElectronico.Inicializar(FConexion.Base, FTipoComprobante, FNumeroComprobante, DescriErr, True, True, FOrigenImpresion)
                    else ReporteIniciado := ReporteFactura.Inicializar(FConexion.Base, FTipoComprobante, FNumeroComprobante, DescriErr, True, True);

                    // Si no inici� el reporte, aviso y salgo.
                    if not ReporteIniciado then begin
                        DescriErr := Format(MSG_IMPRESION_ERROR, [FTipoComprobante, FNumeroComprobante, DescriErr]);
                        GrabarLogBaseDatos(DescriErr);
                        Exit;
                    end else begin
                        // Cargo el PDF
                        GrabarLogBaseDatos('Intentando EjecutarPDF');
                        if EsComprobanteElectronico then begin
                            FuncionEjecutada := ReporteElectronico.EjecutarPDF(PDFComprobante, DescriErr);
                            try
                                // Debo liberar la DTE luego de usarla para no cargar la memoria
                                GrabarLogBaseDatos('Intentando Liberar DTE');
                                ReporteElectronico.LiberarDBNet;
                                GrabarLogBaseDatos('DTE Liberada');
                            except on E : Exception do
                                begin
                                    GrabarLogBaseDatos('Error al intentar Liberar DTE');
                                end;
                            end;
                        end else FuncionEjecutada := ReporteFactura.EjecutarPDF(PDFComprobante, DescriErr);
                        {$REGION 'Comprimir'}
                        // Entro s�lo si se gener� el comprobante.
                        if FuncionEjecutada then begin
                            // Verifico si hay que desplegar el consumo a parte.
                            if EsComprobanteElectronico
                            then ConsumoSeparado := ReporteElectronico.MostrarConsumoAparte
                            else ConsumoSeparado := ReporteFactura.MostrarConsumoAparte;
                            // Si se depliega el consumo aparte debo generar un ZIP.
                            if ConsumoSeparado then begin
                                // Ejecuto el detalle a parte.
                                if EsComprobanteElectronico
                                then FuncionEjecutada := ReporteElectronico.EjecutarDetallePDF(PDFConsumoAparte, DescriErr)
                                else FuncionEjecutada := ReporteFactura.EjecutarDetallePDF(PDFConsumoAparte, DescriErr);
                                // Comprimo los archivos y los devuelvo.
                                if FuncionEjecutada then begin
                                    try
                                        // Cambio la extensi�n del archivo
                                        NombreArchivoAnexo  := ReplaceStr(RutaCompletaArchivo, '.pdf', '_Anexo.pdf');
                                        NombreArchivoZIP    := ReplaceStr(RutaCompletaArchivo, '.pdf', '.zip');
                                        // Grabo los archivos PDF
                                        StringToFile(PDFComprobante, RutaCompletaArchivo);
                                        StringToFile(PDFConsumoAparte, NombreArchivoAnexo);
                                        // Instancio el Objeto
                                        Compresor       := TZip.create(nil);
                                        // Oculto el cuadro de di�logo
                                        Compresor.ShowProgressDialog := false;
                                        // Indico el nombre del archivo comprimido
                                        Compresor.Filename := NombreArchivoZIP;
                                        // Comprimo los archivo PDF
                                        Compresor.FileSpecList.Add(RutaCompletaArchivo);
                                        Compresor.FileSpecList.Add(NombreArchivoAnexo);
                                        // Verifico que haya comprimido ambos archivos
                                        if Compresor.Add = 2 then begin
                                            // Elimino Comprobante y Anexo
                                            SysUtils.DeleteFile(RutaCompletaArchivo);
                                            SysUtils.DeleteFile(NombreArchivoAnexo);
                                            // Verifico que el archivo se haya creado
                                            if FileExists(NombreArchivoZIP)
                                            then DescriErr := ExtractFileName(NombreArchivoZIP)
                                            else DescriErr := 'No se pudo obtener el comprobante';
                                            // Grabo el log
                                            GrabarLogBaseDatos('Terminado EjecutarPDF: ' + DescriErr);
                                        end else begin
                                            raise Exception.Create('Error al intentar comprimir Comprobante y Anexo');
                                        end;
                                        // Libero el ZIP
                                        if Assigned(Compresor) then FreeAndNil(Compresor);
                                    except on E : Exception do
                                        DescriErr := Format(MSG_IMPRESION_ERROR, [FTipoComprobante, FNumeroComprobante, E.Message]);
                                    end;
                                end;
                            end else begin
                                // Grabo los archivos PDF
                                StringToFile(PDFComprobante, RutaCompletaArchivo);
                                // Verifico que el archivo se haya creado
                                if FileExists(RutaCompletaArchivo)
                                then DescriErr := NombreArchivoComprobante
                                else DescriErr := 'No se pudo obtener el comprobante';
                            end;
                        // Si no se gener� el comprobante, aviso y salgo.
                        end else begin
                            DescriErr := Format(MSG_IMPRESION_ERROR, [FTipoComprobante, FNumeroComprobante, DescriErr]);
                            GrabarLogBaseDatos(DescriErr);
                            Exit;
                        end;
                        {$ENDREGION}
                    end;
                    if Assigned(ReporteFactura) then FreeAndNil(ReporteFactura);
                    if Assigned(ReporteElectronico) then FreeAndNil(ReporteElectronico);
                    {$ENDREGION}
                end;
            except
                on E : Exception do begin
                    DescriErr := Format(MSG_IMPRESION_ERROR, [FTipoComprobante, FNumeroComprobante, E.Message]);
                    GrabarLogBaseDatos(DescriErr);
                end;
            end;
        finally
            // Registrar Bitacora
            GrabarLogBaseDatos(Format(MSG_FIN_IMPRESION, [FTipoComprobante, FNumeroComprobante, FCodigoSesion]));
            Respuesta := DescriErr;
        end;
    end;
	// FIN BLOQUE NUEVO // SS_1332_CQU_20151106

    {********************************
    * BEGIN : SS_1390_MGO_20150928  *
    *********************************}
    function TImpresionDocumentos.GenerarDetalleComprobante(FormatoArchivo : AnsiString; out Respuesta : AnsiString) : Boolean;
    resourcestring
        MSG_INICIO_IMPRESION    =   'Se ha solicitado imprimir el detalle del comprobante TipoComprobante %s NumeroComprobante %d con la sesi�n %d';
        MSG_FIN_IMPRESION       =   'Se ha finalizado la impresi�n del detalle del comprobante TipoComprobante %s NumeroComprobante %d con la sesi�n %d';
        MSG_IMPRESION_ERROR     =   'Ha ocurrido un error generando el detalle del comprobante tipo %s y n�mero %d, mensaje del sistema: %s';
        MSG_ARCHIVO_ERROR       =   'Ha ocurrido un error generando el archivo: %s';
    var
        ReporteDetalle: TFormReporteFacturacionDetallada;
        spObtenerTransitosComprobante: TADOStoredProc;
        TipoComprobanteFiscal,
        ContenidoArchivo,
        RutaCompletaArchivo,
        NombreArchivo,
        Extension,
        DescriErr : String;
        Archivo : TStringList;
        YaExisteArchivo,
        Cobrada : Boolean;
    begin
        try
            try
                // Registrar Bitacora
                GrabarLogBaseDatos(Format(MSG_INICIO_IMPRESION, [FTipoComprobante, FNumeroComprobante, FCodigoSesion]));
                // Obtengo el TipoComprobanteFiscal
                TipoComprobanteFiscal := QueryGetStringValue(FConexion.Base,
                                                Format(SQL_OBTENER_TIPO_COMPROBANTE_FISCAL,
                                                [FTipoComprobante, FNumeroComprobante]));
                // Verifico que se pueda imprimir por ac�
                if	(TipoComprobanteFiscal = TC_FACTURA_EXENTA ) or
                    (TipoComprobanteFiscal = TC_FACTURA_AFECTA ) or
                    (TipoComprobanteFiscal = TC_BOLETA_EXENTA  ) or
                    (TipoComprobanteFiscal = TC_BOLETA_AFECTA  ) then
                begin
                    ReporteDetalle := TFormReporteFacturacionDetallada.Create(nil);
                    GrabarLogBaseDatos('Inicializando Reporte');
                    if not ReporteDetalle.Inicializar(FConexion.Base, FTipoComprobante, FNumeroComprobante, DescriErr, Cobrada, True) then begin             //SS_1397_MGO_20151015
                        DescriErr := Format(MSG_IMPRESION_ERROR, [FTipoComprobante, FNumeroComprobante, DescriErr]);
                        GrabarLogBaseDatos(DescriErr);
                        Exit;
                    end else begin
                        // Genero el Nombre del Archvio (Directorio Incluido) y verifico que no exista
                        Extension := iif(UpperCase(FormatoArchivo) = 'PDF', '.pdf', '.csv');
                        RutaCompletaArchivo := GenerarNombreArchivo(NombreArchivo, YaExisteArchivo, Extension, True);
                        if not YaExisteArchivo then begin
                            if UpperCase(FormatoArchivo) = 'PDF' then begin
                                //Generar PDF
                                GrabarLogBaseDatos('Intentando EjecutarPDF');
                                try
                                    if ReporteDetalle.EjecutarPDF(ContenidoArchivo, DescriErr) then begin
                                        GrabarLogBaseDatos('Terminado EjecutarPDF: ' + DescriErr);
                                    end else begin
                                        DescriErr := Format(MSG_IMPRESION_ERROR, [FTipoComprobante, FNumeroComprobante, DescriErr]);
                                        GrabarLogBaseDatos(DescriErr);
                                    end;
                                except
                                    on E : Exception do begin
                                        DescriErr := Format(MSG_IMPRESION_ERROR, [FTipoComprobante, FNumeroComprobante, E.Message]);
                                        GrabarLogBaseDatos(DescriErr);
                                    end;
                                end;
                            end else if UpperCase(FormatoArchivo) = 'CSV' then begin
                                //Generar CSV
                                GrabarLogBaseDatos('Intentando DatasetToExcelCSV');
                                try
                                    spObtenerTransitosComprobante := TADOStoredProc.Create(nil);
                                    with spObtenerTransitosComprobante do begin
                                        Connection    := FConexion.Base;
                                        ProcedureName := 'WEB_CV_ObtenerTransitosComprobante';
                                        Parameters.Refresh;
                                        Parameters.ParamByName('@Comprobante').Value := FNumeroComprobante;
                                        Open;
                                        if DatasetToExcelCSV(spObtenerTransitosComprobante, ContenidoArchivo, DescriErr) then begin
                                            GrabarLogBaseDatos('Terminado DatasetToExcelCSV: ' + DescriErr)
                                        end else begin
                                            DescriErr := Format(MSG_IMPRESION_ERROR, [FTipoComprobante, FNumeroComprobante, DescriErr]);
                                            GrabarLogBaseDatos(DescriErr);
                                        end;
                                        Close;
                                    end;
                                except
                                    on E : Exception do begin
                                        DescriErr := Format(MSG_IMPRESION_ERROR, [FTipoComprobante, FNumeroComprobante, E.Message]);
                                        GrabarLogBaseDatos(DescriErr);
                                    end;
                                end;
                            end;
                            try
                                Archivo := TStringList.Create;
                                Archivo.Add(ContenidoArchivo);
                                Archivo.SaveToFile(RutaCompletaArchivo);
                                Archivo.Free;
                                if FileExists(RutaCompletaArchivo) then DescriErr := NombreArchivo
                                else DescriErr := 'No se pudo obtener el detalle del comprobante';
                            except
                                on E : Exception do begin
                                    DescriErr := Format(MSG_ARCHIVO_ERROR, [E.Message]);
                                end;
                            end;
                        end else begin
                            DescriErr := NombreArchivo;
                            GrabarLogBaseDatos('Archivo existe: ' + DescriErr);
                        end;
                    end;
                end else begin
                    DescriErr := Format(MSG_TIPO_COMPROBANTE_INVALIDO, [FTipoComprobante]);
                    GrabarLogBaseDatos(DescriErr);
                end;
            except
                on E : Exception do begin
                    DescriErr := Format(MSG_IMPRESION_ERROR, [FTipoComprobante, FNumeroComprobante, E.Message]);
                    GrabarLogBaseDatos(DescriErr);
                end;
            end;
        finally
            // Registrar Bitacora
            GrabarLogBaseDatos(Format(MSG_FIN_IMPRESION, [FTipoComprobante, FNumeroComprobante, FCodigoSesion]));
            if Assigned(ReporteDetalle) then FreeAndNil(ReporteDetalle);
            if Assigned(spObtenerTransitosComprobante) then spObtenerTransitosComprobante.Free;
            Respuesta := DescriErr;
        end;
    end;
    {********************************
    * END   : SS_1390_MGO_20150928  *
    *********************************}

    procedure TImpresionDocumentos.ConfirmarImpresion(const NombreArchivo : AnsiString; out Respuesta : AnsiString);
    begin
        try
            Respuesta := Format(MSG_INICIANDO_BORRADO, [FDirectorioPDF + NombreArchivo]);
            GrabarLogBaseDatos(Respuesta);
            if ValidarSesion then begin
                if NombreArchivo <> '' then begin
                    if FileExists(FDirectorioPDF + NombreArchivo) then begin
                        if not SysUtils.DeleteFile(FDirectorioPDF + NombreArchivo) then
                        begin
                            Respuesta := Format(MSG_ERROR_BORRADO, [NombreArchivo]);
                        end else begin
                            Respuesta := Format(MSG_FINALIZANDO_BORRADO, [NombreArchivo]);
                        end;
                    end else begin
                        Respuesta := Format(MSG_ARCHIVO_NO_EXISTE, [NombreArchivo]);
                    end;
                    GrabarLogBaseDatos(Respuesta);
                end else
					//GrabarLogBaseDatos('No se indic� nombre de archivo');							// SS_1332_CQU_20151106
                // No especific� un archivo, se borran todos los viejos								// SS_1332_CQU_20151106
				begin																				// SS_1332_CQU_20151106
                    // Primero los PDF																// SS_1332_CQU_20151106
                    BorrarArchivos('PDF', FDirectorioPDF);											// SS_1332_CQU_20151106
                    // Luego los CSV																// SS_1332_CQU_20151106
                    BorrarArchivos('CSV', FDirectorioPDF);											// SS_1332_CQU_20151106
                    // Luego los ZIP																// SS_1332_CQU_20151106
                    BorrarArchivos('ZIP', FDirectorioPDF);											// SS_1332_CQU_20151106
                    // Luego los TED																// SS_1332_CQU_20151106
                    BorrarArchivos('TED', GoodDir(FDirectorioDBNet + 'in\ted\'));					// SS_1332_CQU_20151106
                    // Luegos los JPG																// SS_1332_CQU_20151106
                    BorrarArchivos('JPG', GoodDir(FDirectorioDBNet + 'out\html\'));					// SS_1332_CQU_20151106
                    Respuesta := Format(MSG_FINALIZANDO_BORRADO, [FDirectorioPDF + NombreArchivo]);	// SS_1332_CQU_20151106
                end;																				// SS_1332_CQU_20151106
            end else begin
                Respuesta := Respuesta + MSG_SESION_CADUCADA;
                GrabarLogBaseDatos(Respuesta);
            end;
        except
            on E : Exception do begin
                Respuesta := Format(MSG_ERROR_BORRANDO, [NombreArchivo, e.Message]);
                GrabarLogBaseDatos(Respuesta);
            end;
        end;
    end;

    function TImpresionDocumentos.GetFileDateTime(Nombre: string): TDateTime;
    var
        Created : TDateTime;
        FileHandle : integer;
        FTimeC,FTimeA,FTimeM : TFileTime;
        LTime : TFileTime;
        STime : TSystemTime;
    begin
        // Abrir el fichero
        FileHandle := FileOpen(Nombre, fmShareDenyNone);
        // inicializar
        Created := 0.0;
        // verifico que se pueda acceder
        if FileHandle < 0 then
            raise Exception.Create('No se pudo abrir el archivo para obtener su fecha')	// SS_1332_CQU_20151106
        else begin
            // Obtener las fechas
            GetFileTime(FileHandle, @FTimeC, @FTimeA, @FTimeM);
            // Cerrar
            FileClose(FileHandle);
            // Creado
            FileTimeToLocalFileTime(FTimeC, LTime);
            if FileTimeToSystemTime(LTime,STime) then begin
                Created := EncodeDate(STime.wYear,STime.wMonth,STime.wDay);
                Created := Created + EncodeTime(STime.wHour, STime.wMinute, STime.wSecond, STime.wMilliSeconds);
            end;
        end;
        result := Created;
    end;

    {---------------------------------------------------------------------------
                TImpresionDocumentos.BorrarArchivos()

    Description :   Se encarga de borrar los archivos generados por la impresion de documentos (WebService)
                    que tengan una antiguedad superior a 30 minutos (parametro general)
                    Adicionalmente elimina los .ted y .jpg que gener� DBNet
    ------------------------------------------------------------------------------}
    procedure TImpresionDocumentos.BorrarArchivos(pTipoArchivo, pRuta : string);
    resourcestring
        MSG_INICIO_BORRADO    =   'Inicio de eliminaci�n de archivos %s creados hace %d minutos';
        MSG_SIN_ARCHIVOS      =   'No existen archivos %s que borrar';
        MSG_TERMINO_BORRADO   =   'Finalizada eliminaci�n de archivos %s creados hace %d minutos';
        MSG_ERROR_BORRANDO    =   'Error borrando archivo %s, Mensaje: %s';
    var
        SR: TSearchRec;
        dt: TDateTime;
        Dir,
        sFiltro,
        sRutaArchivo  : string;
    begin
        try
            GrabarLogBaseDatos(Format(MSG_INICIO_BORRADO, [pTipoArchivo, FMinutosBorrar]));
            Dir := GoodDir(pRuta);
            // Que sea un archivo v�lido
            if pTipoArchivo = 'PDF' then sFiltro := '*NK*.pdf'
            else if pTipoArchivo = 'CSV' then sFiltro := '*.csv'                //SS_1390_MGO_20150928
            else if pTipoArchivo = 'TED' then sFiltro := '*.ted'
            else if pTipoArchivo = 'JPG' then sFiltro := '*.jpg'
			else if pTipoArchivo = 'ZIP' then sFiltro := '*.zip'				// SS_1332_CQU_20151106
            else begin
                GrabarLogBaseDatos(Format(MSG_ERROR_BORRADO, ['No especific� un archivo v�lido (PDF, TED, JPG)']));
                Exit;
            end;
            // Busco los archivos en el directorio
            if FindFirst(Dir + sFiltro,  faArchive , SR) = 0 then begin
                repeat
                    sRutaArchivo := GoodDir(Dir) + SR.Name;
                    try
                        dt:= GetFileDateTime(sRutaArchivo);
                        if (MinutesBetween(dt, FFecha) > FMinutosBorrar) then
                            SysUtils.DeleteFile(sRutaArchivo);
                    except
                        on E : Exception do
                            GrabarLogBaseDatos(Format(MSG_ERROR_BORRANDO, [(sRutaArchivo), E.Message]));
                    end;
                until FindNext(SR) <> 0;
                SysUtils.FindClose(SR);
            end else
                GrabarLogBaseDatos(Format(MSG_SIN_ARCHIVOS, [pTipoArchivo]));
        finally
            GrabarLogBaseDatos(Format(MSG_TERMINO_BORRADO, [pTipoArchivo, FMinutosBorrar]));
        end;
    end;
    {$ENDREGION}
end.
