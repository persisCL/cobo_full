{********************************** Unit Header ********************************
File Name : dmConvenios.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision : 1
    Author : pdominguez
    Date   : 14/04/2010
    Description: Configuración Ambientes
        - Se reconfigura las directivas de compilación, para la diferenciación
        de los parámetros y funcionalidades dependiendo del ambiente. Se incluye
        el archivo Ambiente.inc el cual contiene:
            - Definición de variable de ambiente,
            - Constante AmbienteArchivoINI, la cual indica el archivo ini a
            emplear.
*******************************************************************************}
unit FuncionesWEB;

interface

uses
    Util, dmConvenios, HTTPApp, sysutils, Parametros, utilHTTP, eventlog, ManejoErrores;

const
    PREFIJO_PLANTILLAS = 'CV_';

    ERROR_LEYENDO_PLANTILLA =
      '<HTML>' + CRLF +
      '<BODY>' + CRLF +
      '<H2>Error obteniendo la plantilla</H2>' + CRLF +
      'Ha ocurrido un error al leer la plantilla. Consulte al administrador del sistema.' + CRLF +
      '</BODY>' + CRLF +
      '</HTML>';

    ERROR_NO_PLANTILLA =
      '<HTML>' + CRLF +
      '<BODY>' + CRLF +
      '<H2>Error de configuración</H2>' + CRLF +
      'No se ha encontrado la plantilla %s. Consulte al administrador del sistema.' + CRLF +
      '</BODY>' + CRLF +
      '</HTML>';

    ERROR_GENERAL =
      '<HTML>' + CRLF +
      '<BODY>' + CRLF +
      '<H2>Error General</H2>' + CRLF +
      'Ha ocurrido una excepción y no se puede mostrar la página. <CR>' + CRLF +
      'El mensaje de error devuelto es: %s. <CR>' + CRLF +
      'Consulte al administrador del sistema.' + CRLF +
      '</BODY>' + CRLF +
      '</HTML>';


    function ObtenerPlantilla(DM: TdmConveniosWeb; Request: TWebRequest; const NombrePlantilla: string): string;
    function AtenderError(Request: TWebRequest; DM: TdmConveniosWeb; e: exception; const Modulo: string): string;
    function GenerarError(DM: TdmConveniosWeb; Request: TWebRequest; const Error: string): string;
    function GenerarMensaje(DM: TdmConveniosWeb; Request: TWebRequest; const Mensaje: string): string;
    procedure SepararPartes(const Plantilla, TagInicio, TagFin: string; var Principio, Repeticion, Fin: string);



implementation

{$I Ambiente.inc}

procedure SepararPartes(const Plantilla, TagInicio, TagFin: string; var Principio, Repeticion, Fin: string);
begin
    Principio := Plantilla;
    Repeticion := '';
    Fin := '';

    if pos(TagInicio, Plantilla) = 0 then exit;

    Principio := copy(Plantilla, 1, pos(TagInicio, Plantilla) - 1);
    Repeticion := copy(Plantilla, pos(TagInicio, Plantilla) + length(TagInicio), length(Plantilla));

    if pos(TagFin, Repeticion) = 0 then exit;
    Fin := copy(Repeticion, pos(TagFin, Repeticion) + length(TagFin), Length(Repeticion));
    Repeticion := copy(Repeticion, 1, pos(TagFin, Repeticion) - 1);
end;

function ObtenerPlantilla(DM: TdmConveniosWeb; Request: TWebRequest; const NombrePlantilla: string): string;
begin
    if fileexists(DIRECTORIO_PLANTILLAS + PREFIJO_PLANTILLAS + NombrePlantilla + '.htm') then begin
        result := FileToString(DIRECTORIO_PLANTILLAS + PREFIJO_PLANTILLAS + NombrePlantilla + '.htm');
        if result = '' then begin
            result := ERROR_LEYENDO_PLANTILLA;
        end else begin
            {$IFNDEF WEBDEBUGGER}
            Result := StringReplace(Result, '<head>',
              '<head><base href=' + BASE_PATH + '>', [rfIgnoreCase]);
            {$ENDIF}

            {$IFDEF DESARROLLO}
                Result := StringReplace(Result, '</body>',
              Format('<hr><CENTER>%s</CENTER></body>',[GetAmbienteInfo]), [rfIgnoreCase]);
            {$ENDIF}
            {$IFDEF TEST}
                Result := StringReplace(Result, '</body>',
              Format('<hr><CENTER>%s</CENTER></body>',[GetAmbienteInfo]), [rfIgnoreCase]);
            {$ENDIF}

            Result := ReplaceTag(Result, 'BaseArchivo', BASE_PATH + NOMBRE_EXE);
            Result := ReplaceTag(Result, 'Base', BASE_PATH);

            try
                // si es la pagina de error, no hay hints.
                if UpperCase(NombrePlantilla) = 'ERROR' then exit;
                // ahora nos ocupamos de los hints:
                with DM.spObtenerHints do begin
                    Parameters.ParamByName('@Formulario').Value := PREFIJO_PLANTILLAS + NombrePlantilla;
                    open;
                    while not eof do begin
                        Result := ReplaceTag(Result, 'HINT_' + trim(FieldByName('Componente').AsString), FieldByName('Hint').AsString, true);
                        next;
                    end;
                    close;
                end;
            except
                on e: exception do begin
                    ProcesarError(DM, 'ObtenerPlantilla', e);
                end;
            end;
        end;
    end else begin
        result := format(ERROR_NO_PLANTILLA, [DIRECTORIO_PLANTILLAS + NombrePlantilla + '.htm']);
    end;
end;

function AtenderError(Request: TWebRequest; DM: TdmConveniosWeb; e: exception; const Modulo: string): string;
begin
    try
        EventLogReportEvent(eventlog.elError, Modulo + ': ' + e.Message, '');
        if Assigned(DM) and assigned(Request) then
            result := GenerarError(DM, Request, e.Message)
        else
            result := ERROR_GENERAL;
        dm.Base.Close;
    except
        on e: exception do begin
            ProcesarError(DM, 'AtenderError', e);
            result := format(ERROR_GENERAL, [e.message]);
        end;
    end;
end;

function GenerarError(DM: TdmConveniosWeb; Request: TWebRequest; const Error: string): string;
begin
    Result := ObtenerPlantilla(DM, Request, 'Error');
    Result := ReplaceTag(result, 'Error', Error);
end;

function GenerarMensaje(DM: TdmConveniosWeb; Request: TWebRequest; const Mensaje: string): string;
begin
    Result := ObtenerPlantilla(DM, Request, 'Mensaje');
    Result := ReplaceTag(result, 'Mensaje', Mensaje);
end;



end.
