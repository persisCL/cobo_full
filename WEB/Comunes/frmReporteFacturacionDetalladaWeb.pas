{-----------------------------------------------------------------------------
 Unit Name: frmReporteFacturacionDetalladaWeb
 Author:    jjofre
Date Created: 13/09/2010
 Purpose:   Generar el reporte de Facturacion Detallada desde la web, especificamente
            desde la pesta�a transacciones en la cartola.

 Revision : 1
 Author: pdominguez
 Date  : 26/10/2010
 Description: SS 917 - Modificaciones WEB
    - Se llama al m�todo preparar dentro de la inicializaci�n, para poder
    determinar si tienen datos a imprimir antes de lanzar el reporte.
-----------------------------------------------------------------------------}


unit frmReporteFacturacionDetalladaWeb;

interface

uses
 Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppBands, ppReport, ppSubRpt, ppCtrls, TXrb,
  ppStrtch, ppMemo, ppParameter, ppBarCod, jpeg, ppPrnabl, ppClass, UtilRB,
  ppCache, ppComm, ppRelatv, ppProd, ppModule, raCodMod, DB,
  ADODB, ppDB, ppDBPipe,util,utilproc, ppVar, StdCtrls, daDataModule,
  ppRegion, ppCTMain, ppTxPipe, rbSetup, TeEngine, Series, ExtCtrls,
  TeeProcs, Chart, DbChart, TXComp, ppDevice, ConstParametrosGenerales, Printers, PeaTypes, UtilDb,
  ppPDFDevice, dmConvenios, ppTypes;

type
  TFormReporteFacturacionDetalladaWeb = class(TForm)
    Label3: TLabel;
    dsDetalleTransitos: TDataSource;
    spDetalleTransitos: TADOStoredProc;
    rptReporteFacturacionDetallada: TppReport;
    ppDetallada: TppDBPipeline;
    qryComprobantes: TADOQuery;
    ppdbComprobantes: TppDBPipeline;
    dsComprobantes: TDataSource;
    qryComprobantesTipoComprobante: TStringField;
    qryComprobantesNumeroComprobante: TLargeintField;
    qryComprobantesFechaVencimiento: TDateTimeField;
    qryComprobantesPeriodoInicial: TDateTimeField;
    qryComprobantesPeriodoFinal: TDateTimeField;
    qryComprobantesFechaEmision: TDateTimeField;
    qryComprobantesCodigoConvenio: TIntegerField;
    ppParameterList1: TppParameterList;
    qryComprobantesTipoComprobanteFiscal: TStringField;
    qryComprobantesNumeroComprobanteFiscal: TLargeintField;
    qryComprobantesDescripcion: TStringField;
    spDetalleTransitosPatente: TStringField;
    spDetalleTransitosConcesionaria: TStringField;
    spDetalleTransitosDescriPtoCobroEntrada: TStringField;
    spDetalleTransitosFechaHoraEntrada: TDateTimeField;
    spDetalleTransitosDescriPtoCobroSalida: TStringField;
    spDetalleTransitosFechaHoraSalida: TDateTimeField;
    spDetalleTransitosDescriTipoHorario: TStringField;
    spDetalleTransitosImporte: TBCDField;
    spDetalleTransitosDescriImporte: TStringField;
    ppHeaderBand3: TppHeaderBand;
    ppShape28: TppShape;
    ppShape26: TppShape;
    ppShape27: TppShape;
    ppShape22: TppShape;
    ppShape21: TppShape;
    ppShape24: TppShape;
    ppShape23: TppShape;
    ppLabel38: TppLabel;
    ppDBText22: TppDBText;
    ppDBText20: TppDBText;
    ppLabel35: TppLabel;
    ppLabel34: TppLabel;
    ppShape25: TppShape;
    ppDBText21: TppDBText;
    ppLabel32: TppLabel;
    ppDBText24: TppDBText;
    ppDBText23: TppDBText;
    ppColumnHeaderBand2: TppColumnHeaderBand;
    ppDetailBand6: TppDetailBand;
    ppSubReport3: TppSubReport;
    ppChildReport3: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppSubReport4: TppSubReport;
    ppChildReport4: TppChildReport;
    ppTitleBand5: TppTitleBand;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLine4: TppLine;
    ppLine5: TppLine;
    ppLabel1: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppDetailBand5: TppDetailBand;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText29: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppSummaryBand5: TppSummaryBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand6: TppGroupHeaderBand;
    ppGroupFooterBand6: TppGroupFooterBand;
    ppShape11: TppShape;
    ppShape12: TppShape;
    ppDBCalc6: TppDBCalc;
    ppLabel24: TppLabel;
    ppGroup2: TppGroup;
    ppGroupHeaderBand7: TppGroupHeaderBand;
    ppGroupFooterBand7: TppGroupFooterBand;
    ppShape9: TppShape;
    ppShape10: TppShape;
    ppLabel23: TppLabel;
    ppDBText31: TppDBText;
    ppDBCalc5: TppDBCalc;
    ppDetailBand3: TppDetailBand;
    ppColumnFooterBand2: TppColumnFooterBand;
    ppNumeroComprobante: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppShape7: TppShape;
    ppShape8: TppShape;
    ppLabel16: TppLabel;
    ppDBText14: TppDBText;
    ppDBText1: TppDBText;
    ppShape1: TppShape;
    ppLogo: TppImage;
    procedure rptReporteFacturacionDetalladaBeforePrint(Sender: TObject);
    procedure ppDBText14Print(Sender: TObject);
    procedure ppDBText14Format(Sender: TObject; DisplayFormat: string; DataType: TppDataType; Value: Variant; var Text: string);
    private
     FDM: TdmConveniosWEB;
    { Private declarations }
    FTipoComprobante: String;
    FNumeroComprobante: Int64;
    FCodigoConvenio: integer;
    FConexion: TADOConnection;
    procedure Preparar;

  public
    { Public declarations }
    function Inicializar(Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: int64; var Plantilla: string; var Error: string; var EsAviso: Boolean): Boolean;  // Rev. 6 SS_917_20101026
    function EjecutarPDF(var Resultado: string; var Error: String):Boolean;
    procedure ObtenerDetalleTransitos(TipoCompro: AnsiString; NumeroCompro: Int64);
    function ExportarReporte(Reporte: TppReport; Dispositivo: TppPDFDevice; var Error: string): boolean;
    function ReporteAPDF(Reporte: TppReport; var Resultado: string; var Error: string): boolean;
  end;


var
  FormReporteFacturacionDetalladaWeb: TFormReporteFacturacionDetalladaWeb;

implementation

{$R *.dfm}


resourcestring
    ERROR_INIT                        = 'Error de Inicializaci�n';
	ERROR_PRINTING                    = 'Error de Impresi�n';
	ERROR_INIT_DESCRIPTION            = 'No Se Encuentra El Comprobante';
    STR_ERROR                         = 'Error';

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    jjofre
  Date Created: 13/09/2010
  Description: Inicializa y adquiere los datos para imprimir el detalle comprobante
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReporteFacturacionDetalladaWeb.Inicializar(Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: int64; var Plantilla: string ;var Error: string; var EsAviso: Boolean): Boolean;
resourcestring
	MSG_MISSED_IMAGES_FILES = 'No existen todas las im�genes requeridas para '
		+ CRLF + 'poder imprimir una Nota de Cobro en el sistema';
	MSG_COMPROBANTE_NO_EXISTE = 'El Comprobante %s %d no existe';
    MSG_SIN_PEAJES = 'El Comprobante no tiene Conceptos de Peaje facturados. No es posible imprimir el Detalle de Transacciones.'; // Rev. 1 SS_917_20101026
var
    RutaLogo: AnsiString;                                                       //SS_1397_MCA_20150930
begin
    try
        if not Conexion.Connected then Conexion.Open;
        FConexion := Conexion;
        qryComprobantes.Connection := Conexion ;
        spDetalleTransitos.Connection := FConexion;
        qryComprobantes.Close;
        qryComprobantes.Parameters.ParamByName('TipoComprobante').Value := tipoComprobante;
        qryComprobantes.Parameters.ParamByName('NumeroComprobante').Value := numeroComprobante;
        qryComprobantes.Open;

        //ObtenerParametroGeneral(FConexion, LOGO_REPORTES, RutaLogo);              //SS_1397_MGO_20151015  //SS_1397_MCA_20150930
        ObtenerParametroGeneral(FConexion,                                          //SS_1397_MGO_20151015
          LOGO_REPORTES_WEB, RutaLogo);                  //SS_1397_MGO_20151015
        try                                                                         //SS_1397_MCA_20150930
            ppLogo.Picture.LoadFromFile(Trim(RutaLogo));                            //SS_1397_MCA_20150930
        except                                                                      //SS_1397_MCA_20150930
            On E: Exception do begin                                                //SS_1397_MCA_20150930
                Screen.Cursor := crDefault;                                         //SS_1397_MCA_20150930
                MsgBox(E.message, Self.Caption, MB_ICONSTOP);                       //SS_1397_MCA_20150930
                Screen.Cursor := crHourGlass;                                       //SS_1397_MCA_20150930
            end;                                                                    //SS_1397_MCA_20150930
        end;


        if qryComprobantes.IsEmpty then begin
            Result := False;
		    Error:= format(MSG_COMPROBANTE_NO_EXISTE,[tipoComprobante, NumeroComprobante]) ;
            Exit;
        end;

        FTipoComprobante := TipoComprobante;
        FNumeroComprobante := NumeroComprobante;
        FCodigoConvenio := qryComprobantes.FieldByName('CodigoConvenio').AsInteger;
        // Rev. 1 SS_917_20101026
        EsAviso := False;
        Preparar;

        spDetalleTransitos.Open;
        if spDetalleTransitos.RecordCount = 0 then begin
            EsAviso := True;
            raise Exception.Create(MSG_SIN_PEAJES);
        end;
        // Fin Rev. 1 SS_917_20101026
        Result:= True;
    except
        on e:Exception do begin
            result := false;
            Error := e.message;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Preparar
  Author:    jjofre
  Date Created: 13/09/2010
  Description: Setea las variables  y obtiene los transitos para el reporte
            de detalle transacciones
-----------------------------------------------------------------------------}
procedure TFormReporteFacturacionDetalladaWeb.Preparar;
var
	OldDecimalSeparator : char;
    OldThousandSeparator : char;
begin

    // Guarda los separadores decimales y los cambia
    // por "," para decimales y "." para miles
    OldDecimalSeparator	:= DecimalSeparator;
    OldThousandSeparator:= ThousandSeparator;
    DecimalSeparator	:= ',';
    ThousandSeparator	:= '.';

    qryComprobantes.Close;
    qryComprobantes.Parameters.ParamByName('TipoComprobante').Value := FTipoComprobante;
    qrycomprobantes.Parameters.ParamByName('NumeroComprobante').Value := FNumeroComprobante;
    qryComprobantes.Open;

    ObtenerDetalleTransitos(FTipoComprobante, FNumeroComprobante);

    // Vuelve a dejar los separadores decimales como estaban
    DecimalSeparator  := OldDecimalSeparator;
    ThousandSeparator := OldThousandSeparator;
end;


{-----------------------------------------------------------------------------
  Function Name: EjecutarPDF
  Author:    jjofre
  Date Created: 13/09/2010
  Description: Si la inicializacion sale OK, se ejecuta el reporte
-----------------------------------------------------------------------------}
function TFormReporteFacturacionDetalladaWeb.EjecutarPDF(var Resultado: string; var Error: String):Boolean;
begin
    //Se ejecuta el reporte
    try
		Result := reporteAPDF(rptReporteFacturacionDetallada, Resultado, Error);
	except
        on e:Exception do begin
            Error := e.Message;
            Result := False;
        end;
    end;
end;

{-------------------------------------------------------------------------------
Function Name: ExportarReporte
Author : jjofre
Date Created: 13/09/2010
Description : El reporte es finalmente exportado a pdf.
-------------------------------------------------------------------------------}
function TFormReporteFacturacionDetalladaWeb.ExportarReporte(Reporte: TppReport; Dispositivo: TppPDFDevice; var Error: string): boolean;
begin
    try
        TppPDFDevice(Dispositivo).PDFSettings.OptimizeImageExport := False;                                                                                              // SS_995_PDO_20111006
        TppPDFDevice(Dispositivo).PDFSettings.ScaleImages := False;

        Dispositivo.Publisher := Reporte.Publisher;
        Dispositivo.Reset;
        Reporte.PrintToDevices;
        result := true;
    except
        on e: exception do begin
            result := false;
            Error := e.message;
        end;
    end;
end;


{-------------------------------------------------------------------------------
Function Name: ReporteAPDF
Author : jjofre
Date Created: 13/09/2010
Description : Setea y prepara los objetos necesarios para exportar el reporte
            a pdf.
-------------------------------------------------------------------------------}
function TFormReporteFacturacionDetalladaWeb.ReporteAPDF(Reporte: TppReport; var Resultado: string; var Error: string): boolean;
var
    Exportador: TppPDFDevice;
    Salida: TStringStream;
    Salida2: TMemoryStream;
begin
    try
        Salida := TStringStream.Create('');
        Salida2 := TMemoryStream.Create;
        Error := '';
        Exportador := TppPDFDevice.Create(nil);

        Exportador.PDFSettings.OptimizeImageExport := False;                                                                                              // SS_995_PDO_20111006
        Exportador.PDFSettings.ScaleImages := False;
        Exportador.FileName := GetTempDir + FormatDateTime('yyyymmddhhnnsszzz_', Now) + IntToStr(FNumeroComprobante) + '.pdf';

//    Exportador.PrintToStream := True;
//    Exportador.ReportStream := Salida;

        Result := ExportarReporte(Reporte, Exportador, Error);

        //CopyFile(PChar(Exportador.FileName), PChar('C:\' + ExtractFileName(Exportador.FileName)), False);

        Salida2.LoadFromFile(Exportador.FileName);
        Salida2.Position := 0;
        Salida2.SaveToStream(Salida);

        Resultado := Salida.DataString;
    finally
      if Assigned(Exportador) then FreeAndNil(Exportador);
      if Assigned(Salida) then FreeAndNil(Salida);
      if Assigned(Salida2) then FreeAndNil(Salida2);
    end;
end;

{-------------------------------------------------------------------------------
Function Name: ObtenerDetalleTransitos
Author : jjofre
Date Created: 13/09/2010
Description : Obtiene los registros de los tr�nsitos de la Nota de Cobro.
-------------------------------------------------------------------------------}
procedure TFormReporteFacturacionDetalladaWeb.ObtenerDetalleTransitos(
  TipoCompro: AnsiString; NumeroCompro: Int64);
var
    sp: TADOStoredProc;
  	NumCorrCADesde: Int64;
    NumCorrCAHasta: Int64;
    NumCorrEstacionamientoDesde: Int64;   // SS_1006_HUR_20120213
    NumCorrEstacionamientoHasta: Int64;   // SS_1006_HUR_20120213
begin
    spDetalleTransitos.Close;

    sp := TADOStoredProc.Create(nil);
    try
        sp.Connection       := spDetalleTransitos.Connection;
        // Colocar 500 segundos, pues hay convenios para los que puede demorar algo m�s que 30 seg.
        sp.CommandTimeout   := 500;

        // Cargar la Tabla Temporal Global con Los NumCorrCA de los tr�nsitos de la NK.
        sp.ProcedureName := 'ImprimirFactDetallada_CargarNumCorrCADeNotaCobro';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@TipoComprobante').Value     := TipoCompro;
        sp.Parameters.ParamByName('@NumeroComprobante').Value   := NumeroCompro;
        sp.ExecProc;
        sp.Close;

        // Cargar la Tabla Temporal Global con el resto de los datos de
        // tr�nsitos de la NK.
        sp.ProcedureName := 'ImprimirFactDetallada_CargarTransitosDeNotaCobro';
        sp.Parameters.Refresh;

        // Inicializar variables
        NumCorrCADesde  := -1;
        NumCorrCAHasta  := 0;
        NumCorrEstacionamientoDesde  := -1;     // SS_1006_HUR_20120213
        NumCorrEstacionamientoHasta  := 0;      // SS_1006_HUR_20120213
        // Mientras haya tr�nsitos para cargar.
        while ((NumCorrCADesde <> NumCorrCAHasta) and (NumCorrEstacionamientoDesde <> NumCorrEstacionamientoHasta)) do begin // SS_1006_HUR_20120213
            NumCorrCADesde := NumCorrCaHasta + 1;
            NumCorrEstacionamientoDesde := NumCorrEstacionamientoHasta + 1;         // SS_1006_HUR_20120213

            sp.Parameters.ParamByName('@TipoComprobante').Value     := TipoCompro;
            sp.Parameters.ParamByName('@NumeroComprobante').Value   := NumeroCompro;
            sp.Parameters.ParamByName('@NumCorrCADesde').Value  := NumCorrCADesde;
            sp.Parameters.ParamByName('@NumCorrEstacionamientoDesde').Value  := NumCorrEstacionamientoDesde; // SS_1006_HUR_20120213
            sp.Parameters.ParamByName('@NumCorrCAHasta').Value  := 0;
            sp.Parameters.ParamByName('@NumCorrEstacionamientoHasta').Value  := 0;    // SS_1006_HUR_20120213
            sp.ExecProc;

            // Obtener el �ltimo n�mero de tr�nsito procesado
            NumCorrCAHasta := sp.Parameters.ParamByName('@NumCorrCAHasta').Value;
            NumCorrEstacionamientoHasta := sp.Parameters.ParamByName('@NumCorrEstacionamientoHasta').Value; // SS_1006_HUR_20120213

            sp.Close;
        end; // while
    finally
        sp.Close;
        sp.Free;
    end; // finally

    // Obtener los datos de los consumos.
    spDetalleTransitos.Open;
end;

procedure TFormReporteFacturacionDetalladaWeb.ppDBText14Format(Sender: TObject; DisplayFormat: string; DataType: TppDataType;
  Value: Variant; var Text: string);
begin
	Text := FormatFloat(DisplayFormat, Value);
	Text := StringReplace(Text,',','#', [rfReplaceAll]);
	Text := StringReplace(Text,'.',',', [rfReplaceAll]);
	Text := StringReplace(Text,'#','.', [rfReplaceAll]);
end;

procedure TFormReporteFacturacionDetalladaWeb.ppDBText14Print(Sender: TObject);
begin
end;

{-------------------------------------------------------------------------------
Function Name: rptReporteFacturacionDetalladaBeforePrint
Author : jjofre
Date Created: 13/09/2010
Description : funcion ejecutada justo antes que se ejecute el reporte, y que llama a
                la funcion preparar .
-------------------------------------------------------------------------------}
procedure TFormReporteFacturacionDetalladaWeb.rptReporteFacturacionDetalladaBeforePrint(
  Sender: TObject);
begin
{ // Rev. 1 SS_917_20101026
    try
        Preparar;
    except
        on e: Exception do begin
            Exit;
        end;
    end;
    }
end;

end.

