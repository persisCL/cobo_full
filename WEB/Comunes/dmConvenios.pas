{********************************** Unit Header ********************************
File Name : dmConvenios.pas
Author : mpiazza (comentario base)
Date Created: 13/04/2010

Revision : 0
    Author : pdominguez
    Date   : 14/04/2010
    Description: Configuraci�n Ambientes
        - Se reconfigura las directivas de compilaci�n, para la diferenciaci�n
        de los par�metros y funcionalidades dependiendo del ambiente. Se incluye
        el archivo Ambiente.inc el cual contiene:
            - Definici�n de variable de ambiente,
            - Constante AmbienteArchivoINI, la cual indica el archivo ini a
            emplear.

Revision 1
    Author: jjofre
    Date Created:  26/02/2010
    Description :(Ref. SS 830)
                se agrega el objeto spWEB_AlertaCaf : TADOStoredProc

Revision : 3
    Author : jjofre
    Date : 01/08/2010
    Description : SS 469: Se agregan el stored spWEB_RegistraAuditoriaDomicilios

Revision : 10
    Author : pdominguez
    Date   : 05/07/2010
    Description: Infractores Fase 2
        - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
            spObtenerConcesionarias: TADOStoredProc

    Revision        : 11
    Author          : Eduardo Baeza O.
    Date Created    : 24_Abril_2011
    firma           : PAR00133_EBA_20110419
    Description     : Fase 2 - Pagina Web Convenios
            - Se modifican/a�adieron los siguientes objetos/Constantes/variables:
                spEstacionamientos: TADOStoredProc;
                spObtenerUltimosConsumosEstacionamientos: TADOStoredProc;
                spObtenerUltimosConsumosDetalleEstacionamientos: TADOStoredProc;
                spObtenerEstacionamientosComprobante: TADOStoredProc;
                spObtenerConcesionariasAsociadasItemsConvenio: TADOStoredProc;

Firma       : SS966-NDR-20110810
Description : Se implementa el boton de pago para Banco De Chile (Basado en BDP STD)

Firma       : SS985-NDR-20111004
Description : Se agrega el TADOStoredProc spObtenerUltimosComprobantes

Firma       : SS-966-NDR-20111130
Description : Los pagos se registran en una tabla de trabajo y luego se registran en forma definitiva.

Firma       : SS_1038_GVI_20120723
Description : Se agrega TADOStoreProc para la obtencion de la ultima NK por medio del SP WEB_CV_ObtenerUltimaNotaDeCobro

Firma       : SS_1070_MCO_20121005
Description : Se agrega TADOStoredProc para registrar el envio de clave personal del cliente en la tabla Email_Mensajes


Firma		: SS_1276_NDR_20150528
Description	: Cambios de domicilio facturacion deben generar XML al RNUT

Firma		: SS_1305_MCA_20150612
Description : se agregan procedimiento para registrar en la bitacora de uso WEB


Firma		: SS_1304_MCA_20150709
Description : se agregan 2 TADOStoredProc spWEBObtenerDetalleDeudaConvenios y spWEB_CV_ObtenerDeudaCliente para realizar el pago de los convenios


Firma       : SS_1353_NDR_20150729
Descripcion : Que los botones de pago registren el pago en tablas temporales para no contener la respuesta a la entidad pagadora
              luego internamente se consolidan estos pagos "temporales" en las tablas definitivas
              Los procesos que obtienen la deuda deben tomar en cuenta estos pagos "en proceso"

Firma       : SS_1397_MGO_20151021
Descripcion : Se modifica funci�n NowBase para llamar a la NowBase de PeaProcs

*******************************************************************************}
unit dmConvenios;

interface

uses
  SysUtils, Classes, DB, ADODB, UtilDB, Eventlog, HTTPApp, Util, IniFiles, PeaProcs;	//SS_1397_MGO_20151021

type
  TdmConveniosWEB = class(TDataModule)
    Base: TADOConnection;
    spObtenerHints: TADOStoredProc;
    spCambiarPregunta: TADOStoredProc;
    spCambiarPassword: TADOStoredProc;
    spLogin: TADOStoredProc;
    spCrearSesion: TADOStoredProc;
    spObtenerTiposTelefonos: TADOStoredProc;
    spObtenerCodigosAreaTelefonos: TADOStoredProc;
    spVerificarDominioMail: TADOStoredProc;
    spActualizarDatosPersona: TADOStoredProc;
    spActualizarMediosComunicacion: TADOStoredProc;
    spObtenerDomicilioPrincipal: TADOStoredProc;
    spObtenerRegiones: TADOStoredProc;
    spObtenerComunas: TADOStoredProc;
    spBuscarCalles: TADOStoredProc;
    spCambiarDomicilio: TADOStoredProc;
    spObtenerCodigoSegmento: TADOStoredProc;
    spActualizarDatosEmpresa: TADOStoredProc;
    spObtenerConveniosCliente: TADOStoredProc;
    spObtenerDomicilioConvenio: TADOStoredProc;
    spLinkearDomicilio: TADOStoredProc;
    spObtenerDatosPAC: TADOStoredProc;
    spObtenerDatosPAT: TADOStoredProc;
    spObtenerDatosMandante: TADOStoredProc;
    spObtenerVehiculosConvenio: TADOStoredProc;
    spActualizarVehiculoConvenio: TADOStoredProc;
    spObtenerMarcas: TADOStoredProc;
    spObtenerTiposVehiculos: TADOStoredProc;
    spObtenerParametroGeneral: TADOStoredProc;
    spObtenerMensaje: TADOStoredProc;
    WEB_CV_ActualizarConvenioAccionRNUT: TADOStoredProc;
    spObtenerUltimaFactura: TADOStoredProc;
    spObtenerUltimosConsumos: TADOStoredProc;
    spObtenerUltimosConsumosDetalle: TADOStoredProc;
    spObtenerPatentesVehiculosConvenio: TADOStoredProc;
    spComprobantes: TADOStoredProc;
    spPagos: TADOStoredProc;
    spReclamos: TADOStoredProc;
    spObtenerResumenDeudaConvenio: TADOStoredProc;
    spObtenerDatosCliente: TADOStoredProc;
    spObtenerUltimoComprobante: TADOStoredProc;
    spObtenerPreciosTramos: TADOStoredProc;
    spRegistrarPago: TADOStoredProc;
    spRegistrarRechazo: TADOStoredProc;
    spObtenerDatosPago: TADOStoredProc;
    spObtenerDatosComprobante: TADOStoredProc;
    spObtenerTodasLasComunas: TADOStoredProc;
    spValidarDatosGeneracionPassword: TADOStoredProc;
    spVerificarMailPersona: TADOStoredProc;
    spCambiarMailPersona: TADOStoredProc;
    spObtenerNotasCobro: TADOStoredProc;
    spGrabarMediosEnvio: TADOStoredProc;
    spVioPantallaMediosEnvio: TADOStoredProc;
    spObtenerTransitosComprobante: TADOStoredProc;
    spObtenerFechasProximaFacturacion: TADOStoredProc;
    SPRegistrarPagoSTD: TADOStoredProc;
    SpAgregarLog: TADOStoredProc;
    SPAgregarAuditoriaBDP_MPINI: TADOStoredProc;
    SPAgregarAuditoriaBDP_MPOUT: TADOStoredProc;
    SP_WEB_CV_AgregarAuditoria: TADOStoredProc;
    SP_WEB_CV_ActualizarConfirmacionAuditoria: TADOStoredProc;
    SP_WEB_ActualizarGeneroReciboAuditoria: TADOStoredProc;
    SPAgregarAuditoriaBDPMPFIN: TADOStoredProc;
    SPWEB_CV_ActualizarRespuestaBancoAuditoria: TADOStoredProc;
    SpTransitos: TADOStoredProc;
    SPObtenerImporteComprobante: TADOStoredProc;
    sp_WEB_CV_Auditoria_ActualizarRespuestaAConfirmacion: TADOStoredProc;
    spEMAIL_AgregarEnvioClave: TADOStoredProc;
    spEMAIL_ConfirmarEnvioClave: TADOStoredProc;
    spObtenerPatentesVehiculosSinDuplicados: TADOStoredProc;
    spObtenerUltimosConsumosVehiculo: TADOStoredProc;
    spObtenerUltimosConsumosDetalleVehiculo: TADOStoredProc;
    spObtenerSaldosDeConveniosxRUT: TADOStoredProc;
    SpWEB_CV_ExisteSolicitudPendiente: TADOStoredProc;
    ComprobanteEstaPago: TADOStoredProc;
    spIniciarWEB_Captcha: TADOStoredProc;
    spWEB_ObtenerCaptcha: TADOStoredProc;
    spObtenerInfraccionesAFacturar: TADOStoredProc;
    spObtenerVerificacionCertificado: TADOStoredProc;
    spVerificarFormatoPatenteChilena: TADOStoredProc;
    spVerificarRUT: TADOStoredProc;
    spWEB_AlertaCaf: TADOStoredProc;
    spWEB_RegistraAuditoriaDomicilios: TADOStoredProc;
    spObtenerConcesionarias: TADOStoredProc;
    spEstacionamientos: TADOStoredProc;             // PAR00133_EBA_20110419
    spObtenerUltimosConsumosEstacionamientos: TADOStoredProc;   // PAR00133_EBA_20110419
    spObtenerUltimosConsumosDetalleEstacionamientos: TADOStoredProc; // PAR00133_EBA_20110419
    spObtenerEstacionamientosComprobante: TADOStoredProc; // PAR00133_EBA_20110419
    spObtenerConcesionariasAsociadasItemsConvenio: TADOStoredProc;
    SPAgregarAuditoriaBDP_MPINIBCH: TADOStoredProc;                             // SS966-NDR-20110810
    SPAgregarAuditoriaBDP_MPOUTBCH: TADOStoredProc;                             // SS966-NDR-20110810
    SPAgregarAuditoriaBDP_MPFINBCH: TADOStoredProc;                             // SS966-NDR-20110810
    SPAgregarAuditoriaBDP_MPCONBCH: TADOStoredProc;                             // SS966-NDR-20110810
    SPAgregarAuditoriaBDP_MPRESBCH: TADOStoredProc;                             // SS966-NDR-20110810
    spRegistrarPagoBCH: TADOStoredProc;                                         // SS966-NDR-20110810
    spObtenerUltimosComprobantes: TADOStoredProc;                               // SS983-NDR-20111004
    spRegistrarPagoPendienteBCH: TADOStoredProc;                                //SS-966-NDR-20111130
    spEjecutaPagosPendientesBCH: TADOStoredProc;                                //SS-966-NDR-20111130
    spWEB_CV_ObtenerUltimaNotaDeCobro: TADOStoredProc;                          //SS_1038_GVI_20120723
    spEMAIL_AgregarMensaje: TADOStoredProc;                                     //SS_1070_MCO_20121005
    spWEB_CV_RegistrarBitacoraUso: TADOStoredProc;								//SS_1305_MCA_20150612
    spWEB_CV_ObtenerDeudaCliente: TADOStoredProc;								//SS_1304_MCA_20150709
    spWEBObtenerDetalleDeudaConvenios: TADOStoredProc;                          //SS_1304_MCA_20150709
    spObtenerUltimoComprobanteParaDetalleTransito: TADOStoredProc;              //SS_1304_NDR_20150721
    spRegistrarPagoPendiente: TADOStoredProc;                                   //SS_1353_NDR_20150729
    spEjecutaPagosPendientes: TADOStoredProc;                                   //SS_1353_NDR_20150729
    spRegistrarPagoPendienteSTD: TADOStoredProc;                                //SS_1353_NDR_20150729
    spEjecutaPagosPendientesSTD: TADOStoredProc;                                //SS_1353_NDR_20150729
    //BaseSOR: TADOConnection;                                     				      //SS_1276_NDR_20150528
    procedure DataModuleCreate(Sender: TObject);
    procedure BaseWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure BaseBeforeConnect(Sender: TObject);
    //procedure BaseSORBeforeConnect(Sender: TObject);							            //SS_1276_NDR_20150528
    //procedure BaseSORWillConnect(Connection: TADOConnection;					        //SS_1276_NDR_20150528
    //  var ConnectionString, UserID, Password: WideString;						          //SS_1276_NDR_20150528
    //  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);		  //SS_1276_NDR_20150528
  private
    { Private declarations }

  public
    { Public declarations }
    function NowBase():TDateTime;
    procedure SetearTiemposConsultas;
  end;

    function GetBasePath: string;
    function GetServer: string;
    function GetBase: string;
    function GetUser: string;
    function GetPassword: string;
    function GetPlantillasPath: String;
    function GetIniFileName: String;
    function GetAmbienteInfo: String;


//    function GetBaseSOR: string;												                      //SS_1276_NDR_20150528
//    function GetUserSOR: string;												                      //SS_1276_NDR_20150528
//    function GetPasswordSOR: string;											                    //SS_1276_NDR_20150528


var
    ISAPICGIFile: String;

implementation

uses Parametros;

{$R *.dfm}
{$I Ambiente.inc}

procedure TdmConveniosWEB.DataModuleCreate(Sender: TObject);
begin
    Base.KeepConnection := true;
    try
        Base.ConnectionString :=
          'Provider=SQLOLEDB.1;' +
          'Password=' + GetPassword + ';' +
          'User ID=' + GetUser + ';' +
          'Initial Catalog=' + GetBase + ';' +
          'Data Source=' + GetServer;
    except
    end;

//    BaseSOR.KeepConnection := true;											//SS_1276_NDR_20150528
//    try																		              //SS_1276_NDR_20150528
//        BaseSOR.ConnectionString :=											//SS_1276_NDR_20150528
//          'Provider=SQLOLEDB.1;' +											//SS_1276_NDR_20150528
//          'Password=' + GetPasswordSOR + ';' +					//SS_1276_NDR_20150528
//          'User ID=' + GetUserSOR + ';' +								//SS_1276_NDR_20150528
//          'Initial Catalog=' + GetBaseSOR + ';' +				//SS_1276_NDR_20150528
//          'Data Source=' + GetServer;										//SS_1276_NDR_20150528
//    except																	            //SS_1276_NDR_20150528
//    end;																	              //SS_1276_NDR_20150528

end;

{******************************** function Header ******************************
function Name: NowBase
Author       : mpiazza
Date Created : 05/03/2009
Description  : Devuelve la fecha y hora de la Base de Datos
Parameters   : None
Return Value : TDateTime

*******************************************************************************}
function TdmConveniosWEB.NowBase: TDateTime;
begin
  //result:=StrToDateTime(QueryGetValue(Base,'SELECT GETDATE()'));				      //SS_1397_MGO_20151021
	result:= PeaProcs.NowBase(Base);                                              //SS_1397_MGO_20151021
end;

//procedure TdmConveniosWEB.BaseSORBeforeConnect(Sender: TObject);															 									  //SS_1276_NDR_20150528
//begin																													                                                    //SS_1276_NDR_20150528
//    BaseSOR.ConnectionString :=																							                                      //SS_1276_NDR_20150528
//      'Provider=SQLOLEDB.1;' +																							                                      //SS_1276_NDR_20150528
//      'User ID=sa;' +																									                      	     	    	    		//SS_1276_NDR_20150528
//      'Initial Catalog=' + GetBaseSOR + ';' +																			                                //SS_1276_NDR_20150528
//      'Data Source=' + GetServer;																						                                      //SS_1276_NDR_20150528
//end;																													                                                    //SS_1276_NDR_20150528
//
//procedure TdmConveniosWEB.BaseSORWillConnect(Connection: TADOConnection;																					//SS_1276_NDR_20150528
//  var ConnectionString, UserID, Password: WideString;																	                            //SS_1276_NDR_20150528
//  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);												   		  		  		    //SS_1276_NDR_20150528
//Var																														                                                    //SS_1276_NDR_20150528
//	DescriError: AnsiString;																							                    	     	    	    		//SS_1276_NDR_20150528
//begin																													                                                    //SS_1276_NDR_20150528
//	if not SecureDatabase(BaseSOR, UserID, Password, DescriError, 'Parametros') then begin		   		  		  		    //SS_1276_NDR_20150528
//        EventLogReportEvent(elError, 'Error al intentar asegurar la base de datos SOR: ' + DescriError, '');			//SS_1276_NDR_20150528
//        EventStatus := esCancel;																						                                      //SS_1276_NDR_20150528
//	end;																												                                                    //SS_1276_NDR_20150528
//end;																													                                                    //SS_1276_NDR_20150528

procedure TdmConveniosWEB.BaseWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
Var
	DescriError: AnsiString;
begin
	if not SecureDatabase(Base, UserID, Password, DescriError, 'Parametros') then begin
        EventLogReportEvent(elError, 'Error al intentar asegurar la base de datos: ' + DescriError, '');
        EventStatus := esCancel;
	end;
end;

procedure TdmConveniosWEB.BaseBeforeConnect(Sender: TObject);
begin
    Base.ConnectionString :=
      'Provider=SQLOLEDB.1;' +
      'User ID=sa;' +
      'Initial Catalog=' + GetBase + ';' +
      'Data Source=' + GetServer;
end;
{-----------------------------------------------------------------------------
  Procedure: SetearTiemposConsultas
  Author:    lcanteros
  Date:      08-May-2008
  Description: Asigna el tiempo de consulta por parametro general
-----------------------------------------------------------------------------}
procedure TdmConveniosWEB.SetearTiemposConsultas;
begin
    spObtenerUltimosConsumos.CommandTimeout := TIME_OUT_CONSULTAS_WEB;
    spObtenerUltimosConsumosDetalle.CommandTimeout := TIME_OUT_CONSULTAS_WEB;
    spObtenerUltimosConsumosVehiculo.CommandTimeout := TIME_OUT_CONSULTAS_WEB;
    spObtenerUltimosConsumosDetalleVehiculo.CommandTimeout := TIME_OUT_CONSULTAS_WEB;
end;

function GetBasePath: string;
var
    inifile: TIniFile;
begin
    iniFile := TIniFile.create(GetIniFileName);
    result := inifile.ReadString('General', 'BasePath', 'http://localhost/');
    inifile.Destroy;
end;

function GetServer: string;
var
    inifile: TIniFile;
begin
    iniFile := TIniFile.create(GetIniFileName);
    result := inifile.ReadString('General', 'Server', 'SRV-DATABASE\DBCOSTANERA');
    inifile.Destroy;
end;

function GetBase: string;
var
    inifile: TIniFile;
begin
    iniFile := TIniFile.create(GetIniFileName);
    result := inifile.ReadString('General', 'Base', 'OP_CAC');
    inifile.Destroy;
end;

function GetUser: string;
var
    inifile: TIniFile;
begin
    iniFile := TIniFile.create(GetIniFileName);
    result := inifile.ReadString('General', 'User', 'usr_op');
    inifile.Destroy;
end;

function GetPassword: string;
var
    inifile: TIniFile;
begin
    iniFile := TIniFile.create(GetIniFileName);
    result := inifile.ReadString('General', 'Password', 'usr_op');
    inifile.Destroy;
end;

//function GetBaseSOR: string;							                        						//SS_1276_NDR_20150528
//var																				                                    //SS_1276_NDR_20150528
//    inifile: TIniFile;															                          //SS_1276_NDR_20150528
//begin																			                                    //SS_1276_NDR_20150528
//    iniFile := TIniFile.create(GetIniFileName);									              //SS_1276_NDR_20150528
//    result := inifile.ReadString('General', 'BaseSOR', 'SOR');					      //SS_1276_NDR_20150528
//    inifile.Destroy;															                            //SS_1276_NDR_20150528
//end;																			                                    //SS_1276_NDR_20150528

//function GetUserSOR: string;													                        //SS_1276_NDR_20150528
//var																				                                    //SS_1276_NDR_20150528
//    inifile: TIniFile;															                          //SS_1276_NDR_20150528
//begin																			                                    //SS_1276_NDR_20150528
//    iniFile := TIniFile.create(GetIniFileName);									              //SS_1276_NDR_20150528
//    result := inifile.ReadString('General', 'UserSOR', 'usr_op');				      //SS_1276_NDR_20150528
//    inifile.Destroy;															                            //SS_1276_NDR_20150528
//end;																			                                    //SS_1276_NDR_20150528

//function GetPasswordSOR: string;												                      //SS_1276_NDR_20150528
//var																				                                    //SS_1276_NDR_20150528
//    inifile: TIniFile;															                          //SS_1276_NDR_20150528
//begin																			                                    //SS_1276_NDR_20150528
//    iniFile := TIniFile.create(GetIniFileName);									              //SS_1276_NDR_20150528
//    result := inifile.ReadString('General', 'PasswordSOR', 'usr_op');			    //SS_1276_NDR_20150528
//    inifile.Destroy;															                            //SS_1276_NDR_20150528
//end;																			                                    //SS_1276_NDR_20150528

{******************************** Function Header ******************************
Procedure Name: GetPlantillasPath
Author : pdominguez
Date Created : 05/10/2009
Parameters : None
Return Value : String
Description : SS 719
    - Devuelve la ruta donde se encuentran las plantillas, carg�ndola del archivo
    de configuraci�n.
*******************************************************************************}
function GetPlantillasPath: String;
var
    inifile: TIniFile;
begin
    try
        iniFile := TIniFile.create(GetIniFileName);
        Result := goodDir(inifile.ReadString('General', 'DirectorioPlantillas', curDir + 'Plantillas'));
    finally
        inifile.Free;
    end;
end;

{******************************** Function Header ******************************
Procedure Name: GetIniFileName
Author : pdominguez
Date Created : 14/04/2010
Parameters : None
Return Value : String
Description : Configuraci�n ambientes
    - Devuelve la cadena del archivo Ini que corresponda. Carga el valor del
    archivo Ambiente.inc mediante la directiva $I Ambiente.inc.
*******************************************************************************}
function GetIniFileName: String;
begin
    Result := AmbienteArchivoINI;
end;

{******************************** Function Header ******************************
Procedure Name: GetAmbienteInfo
Author : pdominguez
Date Created : 14/04/2010
Parameters : None
Return Value : String
Description : Configuraci�n ambientes
    - Devuelve la cadena con toda la informaci�n del ambiente en el cual se
    est� trabajando.
*******************************************************************************}
function GetAmbienteInfo: String;
    ResourceString
        AMBIENTE_INFO_DESARROLLO = 'Desarrollo';
        AMBIENTE_INFO_TEST = 'Test';
        AMBIENTE_INFO_VESPUCIOSUR = 'VespucioSur';
        AMBIENTE_INFO_AMBIENTE = ' Ambiente: ';
        AMBIENTE_INFO_FILE = 'ISAPI/CGI: ';
        AMBIENTE_INFO_FILE_VERSION = 'Ver.: ';
        AMBIENTE_INFO_INSTANCIA = ' SQL Server: ';
        AMBIENTE_INFO_BD = ' BD: ';
        AMBIENTE_INFO_USUARIO = ' User: ';
        AMBIENTE_INFO_PATH_PLANTILLAS = ' Path Plantillas: ';
        AMBIENTE_INFO_INI_FILE = ' Ini: ';
        ESTILO_10 = '<SPAN class=estiloDev1>%s</span>';
        ESTILO_3 = '<SPAN class=estiloDev2>%s</span>';
        AMBIENTE_INFO_GENERAL =
            '%s, %s, %s, %s<BR>%s, %s, %s, %s';
begin
    Result := '';
    {$IFDEF DESARROLLO}
        Result :=
            Format(
                AMBIENTE_INFO_GENERAL,
                [Format(ESTILO_10,[AMBIENTE_INFO_AMBIENTE]) + Format(ESTILO_3,[AMBIENTE_INFO_DESARROLLO]),
                 Format(ESTILO_10,[AMBIENTE_INFO_INI_FILE]) + Format(ESTILO_3,[GetIniFileName]),
                 Format(ESTILO_10,[AMBIENTE_INFO_FILE]) + Format(ESTILO_3,[ExtractFileName(ISAPICGIFile)]),
                 Format(ESTILO_10,[AMBIENTE_INFO_FILE_VERSION]) + Format(ESTILO_3,[GetFileVersionString(ISAPICGIFile, True)]),
                 Format(ESTILO_10,[AMBIENTE_INFO_PATH_PLANTILLAS]) + Format(ESTILO_3,[GetPlantillasPath]),
                 Format(ESTILO_10,[AMBIENTE_INFO_INSTANCIA]) + Format(ESTILO_3,[GetServer]),
                 Format(ESTILO_10,[AMBIENTE_INFO_BD]) + Format(ESTILO_3,[GetBase]),
                 Format(ESTILO_10,[AMBIENTE_INFO_USUARIO]) + Format(ESTILO_3,[GetUser])]);
    {$ELSE}
        {$IFDEF TEST}
        Result :=
            Format(
                AMBIENTE_INFO_GENERAL,
                [Format(ESTILO_10,[AMBIENTE_INFO_AMBIENTE]) + Format(ESTILO_3,[AMBIENTE_INFO_TEST]),
                 Format(ESTILO_10,[AMBIENTE_INFO_INI_FILE]) + Format(ESTILO_3,[GetIniFileName]),
                 Format(ESTILO_10,[AMBIENTE_INFO_FILE]) + Format(ESTILO_3,[ExtractFileName(ISAPICGIFile)]),
                 Format(ESTILO_10,[AMBIENTE_INFO_FILE_VERSION]) + Format(ESTILO_3,[GetFileVersionString(ISAPICGIFile, True)]),
                 Format(ESTILO_10,[AMBIENTE_INFO_PATH_PLANTILLAS]) + Format(ESTILO_3,[GetPlantillasPath]),
                 Format(ESTILO_10,[AMBIENTE_INFO_INSTANCIA]) + Format(ESTILO_3,[GetServer]),
                 Format(ESTILO_10,[AMBIENTE_INFO_BD]) + Format(ESTILO_3,[GetBase]),
                 Format(ESTILO_10,[AMBIENTE_INFO_USUARIO]) + Format(ESTILO_3,[GetUser])]);
        {$ELSE}
            {$IFDEF VESPUCIOSURDESARROLLO}
                Result :=
                    Format(
                        AMBIENTE_INFO_GENERAL,
                        [Format(ESTILO_10,[AMBIENTE_INFO_AMBIENTE]) + Format(ESTILO_3,[AMBIENTE_INFO_VESPUCIOSUR]),
                         Format(ESTILO_10,[AMBIENTE_INFO_INI_FILE]) + Format(ESTILO_3,[GetIniFileName]),
                         Format(ESTILO_10,[AMBIENTE_INFO_FILE]) + Format(ESTILO_3,[ExtractFileName(ISAPICGIFile)]),
                         Format(ESTILO_10,[AMBIENTE_INFO_FILE_VERSION]) + Format(ESTILO_3,[GetFileVersionString(ISAPICGIFile, True)]),
                         Format(ESTILO_10,[AMBIENTE_INFO_PATH_PLANTILLAS]) + Format(ESTILO_3,[GetPlantillasPath]),
                         Format(ESTILO_10,[AMBIENTE_INFO_INSTANCIA]) + Format(ESTILO_3,[GetServer]),
                         Format(ESTILO_10,[AMBIENTE_INFO_BD]) + Format(ESTILO_3,[GetBase]),
                         Format(ESTILO_10,[AMBIENTE_INFO_USUARIO]) + Format(ESTILO_3,[GetUser])]);
            {$ENDIF}
        {$ENDIF}
    {$ENDIF}
end;

end.
