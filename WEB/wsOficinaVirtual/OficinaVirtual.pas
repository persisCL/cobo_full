{-----------------------------------------------------------------------------
 File Name      : OficinaVirtual
 Author         : Claudio Quezada Ib��ez
 Firma          : SS_1332_CQU_20150721
 Date Created   : 21-07-2015
 Language       : ES-CL
 Description    : M�dulo web para el nuevo Servicio Web 
-----------------------------------------------------------------------------}
unit OficinaVirtual;

interface

uses
  SysUtils, Classes, HTTPApp, InvokeRegistry, WSDLIntf, TypInfo, WebServExp,
  WSDLBind, XMLSchema, WSDLPub, SOAPPasInv, SOAPHTTPPasInv, SOAPHTTPDisp,
  WebBrokerSOAP;

type
  TwsOficinaVirtual = class(TWebModule)
    HTTPSoapDespachador: THTTPSoapDispatcher;
    HTTPSoapPascalInvocador: THTTPSoapPascalInvoker;
    WSDLHTMLPublicador: TWSDLHTMLPublish;
    procedure DefaultHandlerAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  wsOficinaVirtual: TwsOficinaVirtual;

implementation

{$R *.dfm}

procedure TwsOficinaVirtual.DefaultHandlerAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  WSDLHTMLPublicador.ServiceInfo(Sender, Request, Response, Handled);

end;

end.
