{-----------------------------------------------------------------------------
 File Name      : OficinaVirtualIntf
 Author         : Claudio Quezada Ib��ez
 Firma          : SS_1332_CQU_20150721
 Date Created   : 21-07-2015
 Language       : ES-CL
 Description    : Invokable interface IOficinaVirtual
				  Se agrega el m�todo "GenerarDetalleComprobante" para generar
                  el reporte de tr�nsitos de un comprobante, en PDF o CSV. (Ref. SS_1390_MGO_20150928)

 Firma          : SS_1397_MGO_20151106
 Description    : Se agrega el m�todo "GenerarTransitosNoFacturados".
----------------------------------------------------------------------------}

unit OficinaVirtualIntf;

interface

uses InvokeRegistry, Types, XSBuiltIns;

type

  { Invokable interfaces must derive from IInvokable }
  IOficinaVirtual = interface(IInvokable)
    ['{24121052-B5E8-41F1-98AB-9FC40215E957}']
    {$REGION 'GenerarComprobante'}
    /// <summary>
    /// GenerarComprobante: Imprime el comprobante indicado seg�n TipoComprobante y NumeroComprobante
    /// </summary>
    /// <param name="CodigoSesionWeb">Codigo de la sesi�n web que desea imprimir el comprobante</param>
    /// <param name="TipoComprobante">Tipo del Comprobante que se desea imprimir</param>
    /// <param name="NumeroComprobante">N�mero del Comprobante que se desea imprimir</param>
    /// <param name="Respuesta">string con la URL de donde se dej� el archivo pdf con el comprobante o el error de por qu� no se pudo generar</param>
    procedure GenerarComprobante(const CodigoSesionWeb, TipoComprobante : AnsiString; NumeroComprobante : Int64; out Respuesta: AnsiString); stdcall;
    /// <summary>
    /// GenerarDetalleComprobante: Imprime el detalle de un comprobante indicado seg�n TipoComprobante y NumeroComprobante
    /// </summary>
    /// <param name="CodigoSesionWeb">Codigo de la sesi�n web que desea imprimir el comprobante</param>
    /// <param name="TipoComprobante">Tipo del Comprobante que se desea imprimir</param>
    /// <param name="NumeroComprobante">N�mero del Comprobante que se desea imprimir</param>
    /// <param name="FormatoArchivo">Formato del archivo a devolver (PDF o CSV)</param>
    /// <param name="Respuesta">string con la URL de donde se dej� el archivo pdf con el comprobante o el error de por qu� no se pudo generar</param>
    procedure GenerarDetalleComprobante(const CodigoSesionWeb, TipoComprobante : AnsiString; NumeroComprobante : Int64; FormatoArchivo: AnsiString; out Respuesta: AnsiString); stdcall;
    {********************************
    * BEGIN : SS_1397_MGO_20151106  *
    *********************************}
    /// <summary>
    /// GenerarTransitosNoFacturados: Imprime el reporte de tr�nsitos no facturados seg�n CodigoConvenio y IndiceVehiculo
    /// </summary>
    /// <param name="CodigoSesionWeb">Codigo de la sesi�n web que desea imprimir el comprobante</param>
    /// <param name="CodigoConvenio">El Codigo de Convenio para el cual se imprime el reporte</param>
    /// <param name="IndiceVehiculo">Indice del veh�culo para el cual se imprime el reporte (-1 = Todos)</param>
    /// <param name="Respuesta">string con la URL de donde se dej� el archivo pdf con el comprobante o el error de por qu� no se pudo generar</param>
    procedure GenerarTransitosNoFacturados(const CodigoSesionWeb : AnsiString; CodigoConvenio, IndiceVehiculo : Int64; out Respuesta: AnsiString); stdcall;
    {********************************
    * END   : SS_1397_MGO_20151106  *
    *********************************}
    /// <summary>
    /// ConfirmarImpresion: Confirma la impresion del comprobante.
    /// </summary>
    /// <param name="CodigoSesionWeb">Codigo de la sesi�n web que desea imprimir el comprobante</param>
    /// <param name="Archivo">Nombre del archivo a confirmar</param>
    /// <param name="Respuesta">string el error de por qu� no se pudo generar</param>
    procedure ConfirmarImpresion(const CodigoSesionWeb, Archivo : AnsiString; out Respuesta: AnsiString); stdcall;
    {$ENDREGION}
  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface(TypeInfo(IOficinaVirtual));

end.
