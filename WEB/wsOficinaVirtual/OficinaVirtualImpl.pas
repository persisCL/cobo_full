{-----------------------------------------------------------------------------
 File Name      : OficinaVirtualImpl
 Author         : Claudio Quezada Ib��ez
 Firma          : SS_1332_CQU_20150721
 Date Created   : 21-07-2015
 Language       : ES-CL
 Description    : Invokable implementation File for TOficinaVirtual which implements IOficinaVirtual
				  Se agrega el m�todo "GenerarDetalleComprobante" para generar
                  el reporte de tr�nsitos de un comprobante, en PDF o CSV. (Ref. SS_1390_MGO_20150928)
 
 Firma          : SS_1397_MGO_20151106
 Description    : Se agrega el m�todo "GenerarTransitosNoFacturados".
 -----------------------------------------------------------------------------}

unit OficinaVirtualImpl;

interface

uses
    InvokeRegistry, Types, XSBuiltIns, OficinaVirtualIntf, SysUtils, StrUtils,
    Util, ImpresionDocumentos, dmConvenios, PeaTypes;

type

  { TOficinaVirtual }
  TOficinaVirtual = class(TInvokableClass, IOficinaVirtual)
  const
        Usuario = 'WEB';
  public
        {$REGION 'Publica'}
        /// <summary>
        /// GenerarComprobante: Imprime el comprobante indicado seg�n TipoComprobante y NumeroComprobante
        /// </summary>
        /// <param name="CodigoSesionWeb">Codigo de la sesi�n web que desea imprimir el comprobante</param>
        /// <param name="TipoComprobante">Tipo del Comprobante que se desea imprimir</param>
        /// <param name="NumeroComprobante">N�mero del Comprobante que se desea imprimir</param>
        /// <param name="Respuesta">string con la URL de donde se dej� el archivo pdf con el comprobante o el error de por qu� no se pudo generar</param>
        procedure GenerarComprobante(const CodigoSesionWeb, TipoComprobante : AnsiString; NumeroComprobante : Int64; out Respuesta: AnsiString); stdcall;
        /// <summary>
        /// GenerarDetalleComprobante: Imprime el detalle de un comprobante indicado seg�n TipoComprobante y NumeroComprobante
        /// </summary>
        /// <param name="CodigoSesionWeb">Codigo de la sesi�n web que desea imprimir el comprobante</param>
        /// <param name="TipoComprobante">Tipo del Comprobante que se desea imprimir</param>
        /// <param name="NumeroComprobante">N�mero del Comprobante que se desea imprimir</param>
        /// <param name="FormatoArchivo">Formato del archivo a devolver (PDF o CSV)</param>
        /// <param name="Respuesta">string con la URL de donde se dej� el archivo pdf con el comprobante o el error de por qu� no se pudo generar</param>
        procedure GenerarDetalleComprobante(const CodigoSesionWeb, TipoComprobante : AnsiString; NumeroComprobante : Int64; FormatoArchivo: AnsiString; out Respuesta: AnsiString); stdcall;
        {********************************
        * BEGIN : SS_1397_MGO_20151106  *
        *********************************}
        /// <summary>
        /// GenerarTransitosNoFacturados: Imprime el reporte de tr�nsitos no facturados seg�n CodigoConvenio y IndiceVehiculo
        /// </summary>
        /// <param name="CodigoSesionWeb">Codigo de la sesi�n web que desea imprimir el comprobante</param>
        /// <param name="CodigoConvenio">El Codigo de Convenio para el cual se imprime el reporte</param>
        /// <param name="IndiceVehiculo">Indice del veh�culo para el cual se imprime el reporte (-1 = Todos)</param>
        /// <param name="Respuesta">string con la URL de donde se dej� el archivo pdf con el comprobante o el error de por qu� no se pudo generar</param>
        procedure GenerarTransitosNoFacturados(const CodigoSesionWeb : AnsiString; CodigoConvenio, IndiceVehiculo : Int64; out Respuesta: AnsiString); stdcall;
        {********************************
        * END   : SS_1397_MGO_20151106  *
        *********************************}
        /// <summary>
        /// ConfirmarImpresion: Confirma la impresion del comprobante.
        /// </summary>
        /// <param name="CodigoSesionWeb">Codigo de la sesi�n web que desea imprimir el comprobante</param>
        /// <param name="Archivo">Nombre del archivo a confirmar</param>
        /// <param name="Respuesta">string con la URL de donde se dej� el archivo pdf con el comprobante o el error de por qu� no se pudo generar</param>
        procedure ConfirmarImpresion(const CodigoSesionWeb, Archivo: string; out Respuesta: AnsiString); stdcall;
        {$ENDREGION}
  end;

implementation

{$REGION 'Publica'}
procedure TOficinaVirtual.GenerarComprobante(const CodigoSesionWeb, TipoComprobante : AnsiString; NumeroComprobante : Int64; out Respuesta: AnsiString); stdcall;
var
    fImpresionNK : TImpresionDocumentos;
    pRespuesta : string;
begin
    try
        try
            fImpresionNK := TImpresionDocumentos.Create(CodigoSesionWeb, Usuario, oiWeb);
            if not fImpresionNK.Inicializar(TipoComprobante, NumeroComprobante, pRespuesta) then
                Respuesta := pRespuesta
            else begin
                fImpresionNK.GenerarComprobante(pRespuesta);
                Respuesta := pRespuesta;
            end;
        except
            on e: Exception do begin
                Respuesta := 'Ha ocurrido un error, mensaje del sistema: ' + e.Message;
            end;
        end;
    finally
        fImpresionNK.Destroy;
    end;
end;

procedure TOficinaVirtual.GenerarDetalleComprobante(const CodigoSesionWeb, TipoComprobante : AnsiString; NumeroComprobante : Int64; FormatoArchivo: AnsiString; out Respuesta: AnsiString); stdcall;
var
    fImpresionNK : TImpresionDocumentos;
    pRespuesta : string;
begin
    try
        try
            fImpresionNK := TImpresionDocumentos.Create(CodigoSesionWeb, Usuario, oiWeb);
            if not fImpresionNK.Inicializar(TipoComprobante, NumeroComprobante, pRespuesta) then
                Respuesta := pRespuesta
            else begin
                fImpresionNK.GenerarDetalleComprobante(FormatoArchivo, pRespuesta);
                Respuesta := pRespuesta;
            end;
        except
            on e: Exception do begin
                Respuesta := 'Ha ocurrido un error, mensaje del sistema: ' + e.Message;
            end;
        end;
    finally
        fImpresionNK.Destroy;
    end;
end;
{********************************
* BEGIN : SS_1397_MGO_20151106  *
*********************************}
procedure TOficinaVirtual.GenerarTransitosNoFacturados(const CodigoSesionWeb : AnsiString; CodigoConvenio, IndiceVehiculo : Int64; out Respuesta: AnsiString); stdcall;
var
    fImpresionNK : TImpresionDocumentos;
    pRespuesta : string;
begin
    try
        try
            fImpresionNK := TImpresionDocumentos.Create(CodigoSesionWeb, Usuario, oiWeb);
            if not fImpresionNK.Inicializar(CodigoConvenio, IndiceVehiculo, pRespuesta) then
                Respuesta := pRespuesta
            else begin
                fImpresionNK.GenerarTransitosNoFacturados(pRespuesta);
                Respuesta := pRespuesta;
            end;
        except
            on e: Exception do begin
                Respuesta := 'Ha ocurrido un error, mensaje del sistema: ' + e.Message;
            end;
        end;
    finally
        fImpresionNK.Destroy;
    end;
end;
{********************************
* END   : SS_1397_MGO_20151106  *
*********************************}

procedure TOficinaVirtual.ConfirmarImpresion(const CodigoSesionWeb, Archivo: string; out Respuesta: AnsiString); stdcall;
var
    fImpresionNK : TImpresionDocumentos;
    pRespuesta : string;
begin
    try
        try
            fImpresionNK := TImpresionDocumentos.Create(CodigoSesionWeb, Usuario, oiWeb);
            fImpresionNK.ConfirmarImpresion(Archivo, Respuesta);
            Respuesta := pRespuesta;
        except
            on e: Exception do begin
                Respuesta := 'Ha ocurrido un error, mensaje del sistema: ' + e.Message;
            end;
        end;
    finally
        fImpresionNK.Destroy;
    end;
end;
{$ENDREGION}

initialization
{ Invokable classes must be registered }
   InvRegistry.RegisterInvokableClass(TOficinaVirtual);
end.

