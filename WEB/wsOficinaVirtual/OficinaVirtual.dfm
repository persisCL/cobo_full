object wsOficinaVirtual: TwsOficinaVirtual
  OldCreateOrder = False
  Actions = <
    item
      Default = True
      Name = 'DefaultHandler'
      PathInfo = '/'
      OnAction = DefaultHandlerAction
    end>
  Height = 230
  Width = 415
  object HTTPSoapDespachador: THTTPSoapDispatcher
    Dispatcher = HTTPSoapPascalInvocador
    WebDispatch.PathInfo = 'soap*'
    Left = 60
    Top = 11
  end
  object HTTPSoapPascalInvocador: THTTPSoapPascalInvoker
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 60
    Top = 67
  end
  object WSDLHTMLPublicador: TWSDLHTMLPublish
    WebDispatch.MethodType = mtAny
    WebDispatch.PathInfo = 'wsdl*'
    Left = 60
    Top = 123
  end
end
