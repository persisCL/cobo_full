library webServiceOficinaVirtual;
{
Firma       : SS_1397_MGO_20151027
Descripcion : Referencia a frmReporteFacturacionDetallada

Firma       : SS_1397_MGO_20151106
Descripcion : Referencia a frmReporteTransitos
}
uses
  ActiveX,
  ComObj,
  WebBroker,
  ISAPIApp,
  ISAPIThreadPool,
  OficinaVirtual in 'OficinaVirtual.pas' {OficinaVirtual: TWebModule},
  OficinaVirtualImpl in 'OficinaVirtualImpl.pas',
  OficinaVirtualIntf in 'OficinaVirtualIntf.pas',
  ConstParametrosGenerales in '..\..\Comunes\ConstParametrosGenerales.pas',
  SysUtilsCN in '..\..\Comunes\SysUtilsCN.pas',
  ClaveValor in '..\..\Comunes\ClaveValor.pas',
  Diccionario in '..\..\Comunes\Diccionario.pas',
  frmBloqueosSistema in '..\..\Comunes\frmBloqueosSistema.pas' {BloqueosSistemaForm},
  MsgBoxCN in '..\..\Comunes\MsgBoxCN.pas' {frmMsgBoxCN},
  PeaProcs in '..\..\Comunes\PeaProcs.pas',
  PeaProcsCN in '..\..\Comunes\PeaProcsCN.pas',
  PeaTypes in '..\..\Comunes\PeaTypes.pas',
  RStrings in '..\..\Comunes\RStrings.pas',
  RVMBind in '..\..\Comunes\RVMBind.pas',
  RVMClient in '..\..\Comunes\RVMClient.pas',
  RVMLogin in '..\..\Comunes\RVMLogin.pas' {frmLogin},
  RVMTypes in '..\..\Comunes\RVMTypes.pas',
  Aviso in '..\..\Comunes\Avisos\Aviso.pas',
  AvisoTagVencidos in '..\..\Comunes\Avisos\AvisoTagVencidos.pas',
  FactoryINotificacion in '..\..\Comunes\Avisos\FactoryINotificacion.pas',
  frmVentanaAviso in '..\..\Comunes\Avisos\frmVentanaAviso.pas' {VentanaAvisoForm},
  Notificacion in '..\..\Comunes\Avisos\Notificacion.pas',
  NotificacionImpresionDoc in '..\..\Comunes\Avisos\NotificacionImpresionDoc.pas',
  NotificacionImpresionReporte in '..\..\Comunes\Avisos\NotificacionImpresionReporte.pas' {FormNotificacionImpresionReporte},
  TipoFormaAtencion in '..\..\Comunes\Avisos\TipoFormaAtencion.pas',
  DMConnection in '..\..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  MaskCombo in '..\..\Componentes\MaskCombo\MaskCombo.pas',
  MaskComboDsgn in '..\..\Componentes\MaskCombo\MaskComboDsgn.pas',
  DMComunicacion in '..\..\Comunes\DMComunicacion.pas' {DMComunicaciones: TDataModule},
  EncriptaRijandel in '..\..\Comunes\EncriptaRijandel.pas',
  AES in '..\..\Comunes\AES.pas',
  base64 in '..\..\Comunes\base64.pas',
  frmMuestraMensaje in '..\..\Comunes\frmMuestraMensaje.pas' {PanelMensajesForm},
  frmMuestraPDF in '..\..\Comunes\frmMuestraPDF.pas' {MuestraPDFForm},
  ImgTypes in '..\..\Comunes\ImgTypes.pas',
  FrmRptInformeUsuario in '..\..\Comunes\FrmRptInformeUsuario.pas' {FormRptInformeUsuario},
  FrmRptInformeUsuarioConfig in '..\..\Comunes\FrmRptInformeUsuarioConfig.pas' {FormRptInformeUsuarioConfig},
  UtilReportes in '..\..\Comunes\UtilReportes.pas',
  frmReporteBoletaFacturaElectronica in '..\..\Comunes\frmReporteBoletaFacturaElectronica.pas' {ReporteBoletaFacturaElectronicaForm},
  DTEControlDLL in '..\..\FacturaElectronica\DTEControlDLL.pas',
  ImprimirWO in '..\..\Comunes\ImprimirWO.pas' {frmImprimirWO},
  ManejoDatos in '..\Comunes\ManejoDatos.pas',
  dmConvenios in '..\Comunes\dmConvenios.pas' {dmConveniosWEB: TDataModule},
  Parametros in '..\Comunes\Parametros.pas',
  ManejoErrores in '..\Comunes\ManejoErrores.pas',
  Combos in '..\Comunes\Combos.pas',
  FuncionesWEB in '..\Comunes\FuncionesWEB.pas',
  ReporteFactura in '..\..\Comunes\ReporteFactura.pas' {frmReporteFactura},
  Ejecucion in '..\Comunes\Ejecucion.pas',
  Zip in '..\Comunes\Zip.pas',
  ZipDlls in '..\Comunes\ZipDlls.pas',
  frmReporteFacturacionDetalladaWeb in '..\Comunes\frmReporteFacturacionDetalladaWeb.pas' {FormReporteFacturacionDetalladaWeb},
  CSVUtils in '..\..\Comunes\CSVUtils.pas',
  XMLCAC in '..\..\Comunes\XMLCAC.pas',
  ImpresionDocumentos in '..\Comunes\ImpresionDocumentos.pas',
  frmReporteFacturacionDetallada in '..\..\Facturacion\frmReporteFacturacionDetallada.pas' {FormReporteFacturacionDetallada},
  frmSeleccFactDetallada in '..\..\Facturacion\frmSeleccFactDetallada.pas' {fSeleccFactDetallada},
  frmReporteTransitos in '..\Comunes\frmReporteTransitos.pas' {frmRptTransitos},		//SS_1397_MGO_20151106
  frmMensajeACliente in '..\..\Comunes\frmMensajeACliente.pas';

{$R *.res}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;

begin
  CoInitFlags := COINIT_MULTITHREADED;
  Application.Initialize;
  Application.CreateForm(TwsOficinaVirtual, wsOficinaVirtual);
  Application.Run;
end.
