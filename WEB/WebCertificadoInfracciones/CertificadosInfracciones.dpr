{********************************** Unit Header ********************************
program : CertificadosInfracciones
Author : pdominguez
Date Created: 06/10/2009
Language : ES-AR
Description : SS 719
    - CGI para la generación del Certificado de Infracciones en la WEB,
    Verificación y Reimpresión.

Firma		:	SS_1397_MGO_20151021
Descripcion	:	Se agrega referencia a frmReporteFacturacionDetallada e hijos
*******************************************************************************}

program CertificadosInfracciones;

{$APPTYPE CONSOLE}

uses
  WebBroker,
  CGIApp,
  Eventlog,
  Util,
  wmCertificadosInfracciones in 'wmCertificadosInfracciones.pas' {CertificadosInfraccionesWebModule: TWebModule},
  dmConvenios in '..\Comunes\dmConvenios.pas' {dmConveniosWEB: TDataModule},
  Sesiones in '..\Comunes\Sesiones.pas',
  Parametros in '..\Comunes\Parametros.pas',
  FuncionesWEB in '..\Comunes\FuncionesWEB.pas',
  ManejoErrores in '..\Comunes\ManejoErrores.pas',
  ConstParametrosGenerales in '..\..\Comunes\ConstParametrosGenerales.pas',
  PeaTypes in '..\..\Comunes\PeaTypes.pas',
  frmReportCertificadoInfracciones in '..\..\Comunes\frmReportCertificadoInfracciones.pas' {ReportCertificadoInfraccionesForm},
  {*******************************
  * BEGIN : SS_1397_MGO_20151021 *
  ********************************}
  Combos in '..\Comunes\Combos.pas',
  IntOficinaVirtual in '..\Comunes\IntOficinaVirtual.pas',
  frmReporteFacturacionDetallada in '..\..\Facturacion\frmReporteFacturacionDetallada.pas' {FormReporteFacturacionDetallada},
  frmSeleccFactDetallada in '..\..\Facturacion\frmSeleccFactDetallada.pas' {fSeleccFactDetallada},
  PeaProcs in '..\..\Comunes\PeaProcs.pas',
  frmBloqueosSistema in '..\..\Comunes\frmBloqueosSistema.pas' {BloqueosSistemaForm},
  frmMuestraMensaje in '..\..\Comunes\frmMuestraMensaje.pas' {PanelMensajesForm},
  DMConnection in '..\..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  PeaProcsCN in '..\..\Comunes\PeaProcsCN.pas',
  ImgTypes in '..\..\Comunes\ImgTypes.pas',
  RStrings in '..\..\Comunes\RStrings.pas',
  MsgBoxCN in '..\..\Comunes\MsgBoxCN.pas' {frmMsgBoxCN},
  AvisoTagVencidos in '..\..\Comunes\Avisos\AvisoTagVencidos.pas',
  NotificacionImpresionReporte in '..\..\Comunes\Avisos\NotificacionImpresionReporte.pas' {FormNotificacionImpresionReporte},
  frmMuestraPDF in '..\..\Comunes\frmMuestraPDF.pas' {MuestraPDFForm},
  Diccionario in '..\..\Comunes\Diccionario.pas',
  ClaveValor in '..\..\Comunes\ClaveValor.pas',
  Notificacion in '..\..\Comunes\Avisos\Notificacion.pas',
  NotificacionImpresionDoc in '..\..\Comunes\Avisos\NotificacionImpresionDoc.pas',
  TipoFormaAtencion in '..\..\Comunes\Avisos\TipoFormaAtencion.pas',
  Aviso in '..\..\Comunes\Avisos\Aviso.pas',
  frmVentanaAviso in '..\..\Comunes\Avisos\frmVentanaAviso.pas' {VentanaAvisoForm},
  EncriptaRijandel in '..\..\Comunes\EncriptaRijandel.pas',
  base64 in '..\..\Comunes\base64.pas',
  AES in '..\..\Comunes\AES.pas',
  DMComunicacion in '..\..\Comunes\DMComunicacion.pas' {DMComunicaciones: TDataModule},
  RVMTypes in '..\..\Comunes\RVMTypes.pas',
  RVMClient in '..\..\Comunes\RVMClient.pas',
  RVMLogin in '..\..\Comunes\RVMLogin.pas' {frmLogin},
  RVMBind in '..\..\Comunes\RVMBind.pas',
  MaskCombo in '..\..\Componentes\MaskCombo\MaskCombo.pas',
  FrmRptInformeUsuario in '..\..\Comunes\FrmRptInformeUsuario.pas' {FormRptInformeUsuario},
  UtilReportes in '..\..\Comunes\UtilReportes.pas',
  FrmRptInformeUsuarioConfig in '..\..\Comunes\FrmRptInformeUsuarioConfig.pas' {FormRptInformeUsuarioConfig},
  FactoryINotificacion in '..\..\Comunes\Avisos\FactoryINotificacion.pas',
  SysUtilsCN in '..\..\Comunes\SysUtilsCN.pas',
  frmMensajeACliente in '..\..\Comunes\frmMensajeACliente.pas' {FormMensajeACliente};

{*******************************
  * END : SS_1397_MGO_20151021   *
  ********************************}

{$R *.res}


begin
  ISAPICGIFile := GetExeName;
  EventLogAppName := 'WEBCertificadoInfracciones';
  Application.Initialize;
  EventLogRegisterSource;
  Application.CreateForm(TCertificadosInfraccionesWebModule, CertificadosInfraccionesWebModule);
  Application.Run;
end.
