object CertificadosInfraccionesWebModule: TCertificadosInfraccionesWebModule
  OldCreateOrder = False
  OnCreate = WebModuleCreate
  Actions = <
    item
      Default = True
      Name = 'CertificadoInfracciones'
      PathInfo = '/CertificadoInfracciones'
      OnAction = CertificadosInfraccionesWebModuleCertificadoInfraccionesAction
    end
    item
      Name = 'VerificarCertificadoInfracciones'
      PathInfo = '/VerificarCertificadoInfracciones'
      OnAction = CertificadosInfraccionesWebModuleVerificarCertificadoAction
    end
    item
      Name = 'PruebaCGI'
      PathInfo = '/PruebaCGI'
      OnAction = CertificadosInfraccionesWebModulePruebaCGIAction
    end>
  OnException = WebModuleException
  Height = 257
  Width = 328
end
