{********************************** Unit Header ********************************
File Name : wmCertificadosInfracciones
Author : pdominguez
Date Created: 05/10/2009
Language : ES-AR
Description : SS 719
    - WEbModule para el CGI de generaci�n del Certificado de Infracciones en la WEB.

Revision : 1
    Author : pdominguez
    Date   : 14/12/2009
    Description : SS 719
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            EmitirCertificadoInfracciones
            CertificadosInfraccionesWebModuleCertificadoInfraccionesAction
            VerificarCertificadoInfracciones

Revision : 2
    Author : pdominguez
    Date   : 29/03/2010
    Description : SS 719
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            EmitirCertificadoInfracciones

Revision : 3
    Author : pdominguez
    Date   : 05/07/2010
    Description : Infractores Fase 2
        - Se a�adi� la variable TAG_CONCESIONARIAS.
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            CertificadosInfraccionesWebModuleCertificadoInfraccionesAction,
            EmitirCertificadoInfracciones

Firma       : PAR00114-NDR-20110901
Description : Habilitar Seleccion de Concesionaria en el certificado.

	
Firma		: PAR00114-NDR-20111005
Description	: Habilitar la diferenciacion del logo por concesionaria en la verificacion del certificado

Firma       : SS_1147_MCA_20150511
Descripcion : se reemplaza Costanera Norte por Vespucio Sur


Firma       : SS_1328_NDR_20150701
Descripcion : Problema de conversion de fecha al emitir certificado de regularizacion
*******************************************************************************}

unit wmCertificadosInfracciones;

interface

uses
  SysUtils, Classes, HTTPApp, UtilHttp, Variants,
  dmConvenios, Parametros, EventLog, FuncionesWeb, frmReportCertificadoInfracciones, Windows;

type
  TCertificadosInfraccionesWebModule = class(TWebModule)
    procedure WebModuleCreate(Sender: TObject);
    procedure WebModuleException(Sender: TObject; E: Exception; var Handled: Boolean);
    procedure CertificadosInfraccionesWebModuleCertificadoInfraccionesAction(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);
    procedure CertificadosInfraccionesWebModuleVerificarCertificadoAction(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);
    procedure EmitirCertificadoInfracciones(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure VerificarCertificadoInfracciones(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure CertificadosInfraccionesWebModulePruebaCGIAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
      var Handled: Boolean);
  private
    { Private declarations }
    FDM: TdmConveniosWEB;
  public
    { Public declarations }
  end;

var
    CertificadosInfraccionesWebModule: TCertificadosInfraccionesWebModule;
const
    CONST_USUARIO_WEB = 'Web';
    TAG_VALOR_FECHA_DESDE = 'FechaDesde';
    TAG_VALOR_FECHA_HASTA = 'FechaHasta';
    TAG_PATENTE = 'Patente';
    TAG_CONCESIONARIAS = 'ValoresConcesionarias';

implementation

{$R *.dfm}

Uses Util, PeaTypes, Combos;

procedure TCertificadosInfraccionesWebModule.WebModuleCreate(Sender: TObject);
begin
    try
        FDM := TdmConveniosWEB.Create(Self);
        CargarValoresIniciales(FDM);
    except
        on e: exception do begin
            EventLogReportEvent(elError, 'Error al inicializar: ' + e.Message, '');
        end;
    end;
end;

procedure TCertificadosInfraccionesWebModule.WebModuleException(Sender: TObject; E: Exception; var Handled: Boolean);
begin
    handled := true;
    try
        Response.Content := GenerarError(fdm, Request, e.Message);
        EventLogReportEvent(elError, 'WebmoduleException: ' + E.Message, '');
    except
        on e2: exception do begin
            Response.Content := 'Hubo un error: ' + e2.Message;
            EventLogReportEvent(elError, 'WebmoduleException: Hubo un error "' + e2.Message + '" al presentar la excepcion "' + E.Message + '"', '');
        end;
    end;
end;

{******************************* Procedure Header ******************************
Procedure Name: CertificadosInfraccionesWebModuleCertificadoInfraccionesAction
Author : pdominguez
Date Created : 05/10/2009
Parameters : Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean
Description : SS 719
    - Procesa las peticiones para emitir el Certificado.

Revision : 1
    Author : pdominguez
    Date   : 14/12/2009
    Description : SS 719
        - Se tratan las fechas enviadas y recibidas en el formato "dd/mm/yyyy"
        para evitar los problemas con la Configuraci�n Regional.

Revision : 3
    Author : pdominguez
    Date   : 05/07/2010
    Description : Infractores Fase 2
        - Se a�ade la carga del Combo de Concesionarias en la Plantilla HTML
*******************************************************************************}
procedure TCertificadosInfraccionesWebModule.CertificadosInfraccionesWebModuleCertificadoInfraccionesAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    var
        Plantilla, Ahora: String; // Rev. 1 (SS 719)
        LocalFormatSettings: TFormatSettings; // Rev. 1 (SS 719)
begin
    if Request.MethodType = mtPost then
        EmitirCertificadoInfracciones(Sender,Request,Response,Handled)
    else
        try
            Plantilla := ObtenerPlantilla(FDM, Request, 'CertificadoInfracciones');
            // Rev. 1 (SS 719)
            GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT,LocalFormatSettings);
            LocalFormatSettings.ShortDateFormat := 'dd/mm/yyyy';
            DateTimeToString(Ahora,'',Date, LocalFormatSettings);
            Plantilla := ReplaceTag(Plantilla,TAG_PATENTE,'');
            Plantilla := ReplaceTag(Plantilla,TAG_VALOR_FECHA_DESDE,Ahora);
            Plantilla := ReplaceTag(Plantilla,TAG_VALOR_FECHA_HASTA,Ahora);
            // Fin Rev. 1 (SS 719)

            Plantilla := ReplaceTag(Plantilla, TAG_CONCESIONARIAS, GenerarComboConcesionarias(FDM, '1', False, 1)); //PAR00114-NDR-20110901

            Response.Content := Plantilla;
            Handled := True;
        except
            on e: exception do begin
                Response.Content := AtenderError(Request, FDM, e, 'CertificadoInfracciones');
                Handled := True;
            end;
        end;

end;

{******************************* Procedure Header ******************************
Procedure Name: CertificadosInfraccionesWebModuleVerificarCertificadoAction
Author : pdominguez
Date Created : 04/11/2009
Parameters : Sender: TObject; Request: TWebRequest; Response: TWebResponse;
    var Handled: Boolean
Description : SS 719
    - Procesa las peticiones para Verificar y remitir los Certificados de
    Infracciones.
*******************************************************************************}
procedure TCertificadosInfraccionesWebModule.CertificadosInfraccionesWebModuleVerificarCertificadoAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    var
        Plantilla: String;
begin
    if Request.MethodType = mtPost then
        VerificarCertificadoInfracciones(Sender,Request,Response,Handled)
    else
        try
            Plantilla := ObtenerPlantilla(FDM, Request, 'VerificarCertificadoInfracciones');

            Response.Content := Plantilla;
            Handled := True;
        except
            on e: exception do begin
                Response.Content := AtenderError(Request, FDM, e, 'VerificarCertificadoInfracciones');
                Handled := True;
            end;
        end;

end;

{******************************* Procedure Header ******************************
Procedure Name: EmitirCertificadoInfracciones
Author : pdominguez
Date Created : 05/10/2009
Parameters : Sender: TObject; Request: TWebRequest; Response: TWebResponse;
    var Handled: Boolean
Description : SS 719
    - Emite el Certificado de Infracciones en formato PDF.

Revision : 1
    Author : pdominguez
    Date   : 14/12/2009
    Description : SS 719
        - Se tratan las fechas enviadas y recibidas en el formato "dd/mm/yyyy"
        para evitar problemas con la configuraci�n Regional.

Revision : 2
    Author : pdominguez
    Date   : 29/03/2010
    Description : SS 719
        - Se controla que los datos devueltos por la Consulta al RNVM devuelva
        valores para los campos Nombre y NumeroDocumento.

Revision : 3
    Author : pdominguez
    Date   : 05/07/2010
    Description : Infractores Fase 2
        - Se a�ade la asicgnaci�n del valor al par�metro @CodigoConcesionaria al objeto
        spObtenerInfraccionesAFacturar.
        - Se a�ade el valor del par�metro Codigoconcesionaria en la llamada a la funci�n
        ImprimirCertificadoWEB.
*******************************************************************************}
procedure TCertificadosInfraccionesWebModule.EmitirCertificadoInfracciones(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    resourcestring
        MSG_NO_EXISTEN_DATOS = 'NO existen datos para la patente entre las fechas indicadas.';
//        MSG_EXISTEN_IMPAGOS = 'La patente indicada posee Infracciones pendientes de pago. P�ngase en contacto con Costanera Norte para resolver dicha situaci�n.';      //SS_1147_MCA_20150511
        MSG_EXISTEN_IMPAGOS = 'La patente indicada posee Infracciones pendientes de pago. P�ngase en contacto con Vespucio Sur para resolver dicha situaci�n.';           //SS_1147_MCA_20150511
        MSG_FECHA_HASTA_NO_VALIDA = 'La Fecha "Hasta" no puede ser superior a la fecha actual.';
        MSG_INTERVALO_FECHAS_NO_VALIDO = 'La Fecha "Desde" NO puede ser mayor que la Fecha "Hasta".';
        MSG_FORMATO_PATENTE_NO_VALIDO = 'La Patente ingresada NO es correcta. El Formato de la Patente NO es V�lido.';
        MSG_INGRESE_PATENTE = 'Debe ingresar una Patente.';
        DESC_TRATAMIENTO = 'Don/Do�a/La Empresa';
        DESC_RELACION = 'due�o/due�a';
        MSG_ERROR_INESPERADO = 'Se produjo un Error Inesperado. Error: %s.';
        //MSG_ERROR_CONSULTA_RNVM = 'Debe concurrir a las oficinas comerciales de Costanera Norte para la Emisi�n del Certificado.'; //SS_1147_MCA_20150511 // Rev. 2 (SS 719)
        MSG_ERROR_CONSULTA_RNVM = 'Debe concurrir a las oficinas comerciales de Vespucio Sur para la Emisi�n del Certificado.'; //SS_1147_MCA_20150511 // Rev. 2 (SS 719)
    var
        Error,
        Plantilla: String;
        ReportCertificadoInfraccionesForm: TReportCertificadoInfraccionesForm;

    // Rev. 1 (SS 719)
    function ConvertirFechaHora(FechaHora: string): TDateTime;
        var
            LocalFormatSettings: TFormatSettings;
            d, m, a: integer;                                                   //SS_1328_NDR_20150701
            dtFechaHora:TDateTime;                                              //SS_1328_NDR_20150701
    begin
        //GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT,LocalFormatSettings);   //SS_1328_NDR_20150701
        //LocalFormatSettings.ShortDateFormat := 'dd/mm/yyyy';                  //SS_1328_NDR_20150701
        //Result := StrToDateTime(FechaHora, LocalFormatSettings);              //SS_1328_NDR_20150701
        d := strtointdef(ParseParamByNumber(FechaHora, 1, '/'), 0);             //SS_1328_NDR_20150701
        m := strtointdef(ParseParamByNumber(FechaHora, 2, '/'), 0);             //SS_1328_NDR_20150701
        a := strtointdef(ParseParamByNumber(FechaHora, 3, '/'), 0);             //SS_1328_NDR_20150701
        TryEncodedate(a, m, d, dtFechaHora);                                    //SS_1328_NDR_20150701
        Result:=dtFechaHora;                                                    //SS_1328_NDR_20150701
    end;
    // Fin Rev. 1 (SS 719)

    function ValidarPatenteFechas: Boolean;
        var
            Patente: String;
            FechaDesde, FechaHasta: TDateTime;
    begin
        try
            Patente := Trim(request.contentfields.values['Patente']);
            FechaDesde := ConvertirFechaHora(Request.ContentFields.values['FechaDesde']);   // Rev. 1 (SS 719)
            FechaHasta := ConvertirFechaHora(Request.ContentFields.values['FechaHasta']);   // Rev. 1 (SS 719)

            if Patente = '' then begin
                Error := MSG_INGRESE_PATENTE;
                Result := False;
            end 
            else try

                if Length(Patente) <= 6 then
                    with FDM.spVerificarFormatoPatenteChilena, Parameters do try
                        ParamByName('@Patente').Value := Patente;
                        ExecProc;
                        if ParamByName('@RETURN_VALUE').Value = 0 then Error := MSG_FORMATO_PATENTE_NO_VALIDO;
                    finally
                        Close;
                    end;

                if Error = '' then
                    if FechaHasta > FDM.NowBase then
                        Error := MSG_FECHA_HASTA_NO_VALIDA
                    else if FechaDesde > FechaHasta then Error := MSG_INTERVALO_FECHAS_NO_VALIDO;

            finally
                Result := (Error = '');
            end;
        except
            on E:Exception do begin
                Error := Format(MSG_ERROR_INESPERADO,[E.Message]);
                Result := False;
            end;

        end;
    end;

begin
    try
        Error := '';
        Plantilla := '';
        
        if ValidarPatenteFechas then begin

            with FDM.spObtenerInfraccionesAFacturar, Parameters do begin
                ParamByName('@Patente').Value := request.contentfields.values['Patente'];
                ParamByName('@FechaInicial').Value := NULL;
                ParamByName('@FechaFinal').Value := NULL;
                ParamByName('@SePuedeEmitirCertificado').Value := NULL;
                ParamByName('@ValidarInfraccionesImpagas').Value := NULL;
                //ParamByName('@CodigoConcesionaria').Value := CODIGO_CN;                                      //PAR00114-NDR-20110901
                ParamByName('@CodigoConcesionaria').Value := Request.ContentFields.values['Concesionaria'];    //PAR00114-NDR-20110901
                Open;

                if IsEmpty then
                    Plantilla := GenerarMensaje(FDM, Request, MSG_NO_EXISTEN_DATOS)
                else if ParamByName('@SePuedeEmitirCertificado').Value then begin

                    if not (FieldByName('Nombre').IsNull or FieldByName('NumeroDocumento').IsNull) then begin  // Rev. 2 (SS 719)

                        Close;
                        ParamByName('@FechaInicial').Value := ConvertirFechaHora(request.contentfields.values['FechaDesde']);   // Rev. 1 (SS 719)
                        ParamByName('@FechaFinal').Value := ConvertirFechaHora(request.contentfields.values['FechaHasta']);     // Rev. 1 (SS 719)
                        Open;

                        if IsEmpty then
                            Plantilla := GenerarMensaje(FDM, Request, MSG_NO_EXISTEN_DATOS)
                        else try
                            ReportCertificadoInfraccionesForm := TReportCertificadoInfraccionesForm.Create(Nil);

                            ReportCertificadoInfraccionesForm.ImprimirCertificadoWEB(
                                FDM.Base,
                                FDM.spObtenerInfraccionesAFacturar,
                                FDM.NowBase,
                                ConvertirFechaHora(Request.ContentFields.values['FechaDesde']),
                                ConvertirFechaHora(Request.ContentFields.values['FechaHasta']),
                                DESC_TRATAMIENTO,
                                FieldByName('Nombre').AsString,
                                FieldByName('NumeroDocumento').AsString,
                                DESC_RELACION,
                                UpperCase(Request.ContentFields.values['Patente']),
                                '',     // C�digo Verificador
                                False,  // ReImprimir
                                Plantilla,
                                Error,
                                CONST_USUARIO_WEB,
                                GetPlantillasPath,
                                StrToInt(Request.ContentFields.values['Concesionaria'])); // Rev. 3 (Infractores Fase 2)      //PAR00114-NDR-20110901
                                //CODIGO_CN); // Rev. 3 (Infractores Fase 2)                                                  //PAR00114-NDR-20110901

                            if Trim(Error) = '' then begin
                                response.ContentType := 'application/pdf';
                                response.SetCustomHeader('Content-Disposition', Format('attachment; filename=Certificado_Infracciones_%s.pdf',[Request.ContentFields.values['Patente']]));
                            end
                            else Plantilla := GenerarMensaje(FDM, Request, Error);
                        finally
                            if Assigned(ReportCertificadoInfraccionesForm) then FreeAndNil(ReportCertificadoInfraccionesForm);
                        end;
                    // Rev. 2 (SS 719)
                    end
                    else Plantilla := GenerarMensaje(FDM, Request, MSG_ERROR_CONSULTA_RNVM);
                    // Fin Rev. 2 (SS 719)
                end
                else Plantilla := GenerarMensaje(FDM, Request, MSG_EXISTEN_IMPAGOS);
            end;
        end
        else Plantilla := GenerarMensaje(FDM, Request, Error);
    except
        on E:Exception do begin
          Plantilla := GenerarError(FDM, Request, e.Message);
        end;
    end;

    Response.Content := Plantilla;
    Handled := true;
end;

{******************************* Procedure Header ******************************
Procedure Name: VerificarCertificadoInfracciones
Author : pdominguez
Date Created : 04/11/2009
Parameters : Sender: TObject; Request: TWebRequest; Response: TWebResponse;
    var Handled: Boolean
Description : SS 719
    - Reemite el Certificado de Infracciones para Verificar la veracidad del
    mismo.

Revision : 1
    Author : pdominguez
    Date   : 17/12/2009
    Description : SS 719
        - Se justifica a ceros por la izquierda el n�mero de documento (RUT),
        para que su longitud sea siempre 9, y la comparaci�n del RUT entrado
        con el RUT almacenado sea correcta aunque no se ingresen los ceros no
        significativos.
*******************************************************************************}
procedure TCertificadosInfraccionesWebModule.VerificarCertificadoInfracciones(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    resourcestring
        DESC_TRATAMIENTO_GENERICO = 'Don/Do�a/La Empresa';
        DESC_TRATAMIENTO_DON = 'Don';
        DESC_TRATAMIENTO_DONA = 'Do�a';
        DESC_TRATAMIENTO_EMPRESA = 'La Empresa';
        DESC_RELACION_GENERICA = 'due�o/due�a';
        DESC_RELACION_DON = 'due�o';
        DESC_RELACION_DONA = 'due�a';
        DESC_RELACION_EMPRESA = 'due�a';
        MSG_DOCUMENTO_NO_VALIDO = 'El documento NO es V�lido. El C�digo Verificador ingresado NO es V�lido.';
        MSG_RUT_NO_VALIDO = 'El RUT ingresado NO es V�lido. Debe Ingresar el RUT de la persona a la cual se emiti� el Certificado.';
        MSG_RUT_INCORRECTO = 'El RUT ingresado NO es Correcto.';
        MSG_INGRESE_RUT = 'Debe ingresar un N�mero de RUT.';
        MSG_INGRESE_CODIGO_VERIFICADOR = 'Debe ingresar el C�digo Verificador';
        MSG_ERROR_INESPERADO = 'Se produjo un Error Inesperado. Error: %s.';
    var
        Error,
        Plantilla: String;
        ReportCertificadoInfraccionesForm: TReportCertificadoInfraccionesForm;

    function VerificarRUTCodigoVerificador: Boolean;
        var
            NumeroDocumento,
            CodigoVerificador: String;
    begin
        NumeroDocumento := PadL(Trim(request.contentfields.values['NumeroDocumento']),9,'0'); // Rev. 1 (SS 719)
        CodigoVerificador := Trim(request.contentfields.values['CodigoVerificador']);
    
        if NumeroDocumento = '' then 
            Error := MSG_INGRESE_RUT
        else with FDM.spVerificarRUT, Parameters do try
            try
                ParamByName('@Numero').Value := Copy(NumeroDocumento, 1, Length(NumeroDocumento) - 1);
                ParamByName('@Digito').Value := Copy(NumeroDocumento, Length(NumeroDocumento), 1);
                Open;
                if FieldList[0].AsInteger = 0 then Error := MSG_RUT_INCORRECTO;

                if Error = '' then 
                    if CodigoVerificador = '' then Error := MSG_INGRESE_CODIGO_VERIFICADOR;
            except
                on E:Exception do Error := Format(MSG_ERROR_INESPERADO,[E.Message]);
            end;
        finally
            if Active then Close;
            Result := (Error = '');
        end;
    end;

begin
    try
        Error := '';
        Plantilla := '';

        if VerificarRUTCodigoVerificador then begin

            with FDM.spObtenerVerificacionCertificado, Parameters do try
                ParamByName('@CodigoVerificadorCompleto').Value := StrToInt(request.contentfields.values['CodigoVerificador']);
                ParamByName('@NumeroDocumento').Value := PadL(Trim(request.contentfields.values['NumeroDocumento']),9,'0'); // Rev. 1 (SS 719)
                ParamByName('@Patente').Value := NULL;
                ParamByName('@FechaDesde').Value := NULL;
                ParamByName('@FechaHasta').Value := NULL;
                ParamByName('@Personeria').Value := NULL;
                ParamByName('@Sexo').Value := NULL;
                ParamByName('@NombreCertificado').Value := NULL;
                ParamByName('@FechaCertificado').Value := NULL;
                ParamByName('@NumeroDocumentoCertificado').Value := NULL;
                ParamByName('@IDHistoricoCertificado').Value := NULL;
                ParamByName('@CodigoConcesionaria').Value := NULL;                  //PAR00114-NDR-20111005
                Open;

                if ParamByName('@IDHistoricoCertificado').Value <> NULL then begin

                    if ParamByName('@NumeroDocumentoCertificado').Value <>  PadL(Trim(request.contentfields.values['NumeroDocumento']),9,'0') then // Rev. 1 (SS 719)
                        Plantilla := GenerarMensaje(FDM, Request, MSG_RUT_NO_VALIDO)
                    else try
                        ReportCertificadoInfraccionesForm := TReportCertificadoInfraccionesForm.Create(Nil);

                        ReportCertificadoInfraccionesForm.ImprimirCertificadoWEB(
                            FDM.Base,
                            FDM.spObtenerVerificacionCertificado,
                            ParamByName('@FechaCertificado').Value,
                            ParamByName('@FechaDesde').Value,
                            ParamByName('@FechaHasta').Value,
                            iif(ParamByName('@Personeria').Value <> NULL, iif(ParamByName('@Personeria').Value = PERSONERIA_JURIDICA,DESC_TRATAMIENTO_EMPRESA, iif(ParamByName('@Sexo').Value = SEXO_FEMENINO,DESC_TRATAMIENTO_DONA,DESC_TRATAMIENTO_DON)), DESC_TRATAMIENTO_GENERICO),
                            ParamByName('@NombreCertificado').Value,
                            ParamByName('@NumeroDocumentoCertificado').Value,
                            iif((ParamByName('@Personeria').Value <> NULL) and (ParamByName('@Sexo').Value <> NULL), iif(ParamByName('@Personeria').Value = PERSONERIA_JURIDICA,DESC_RELACION_EMPRESA,iif(ParamByName('@Sexo').Value = SEXO_FEMENINO,DESC_RELACION_DONA,DESC_RELACION_DON)), DESC_RELACION_GENERICA),
                            ParamByName('@Patente').Value,
                            Trim(request.contentfields.values['CodigoVerificador']),
                            True, // ReImprimir
                            Plantilla,
                            Error,
                            CONST_USUARIO_WEB,
                            GetPlantillasPath,                                           //PAR00114-NDR-20111005
                            ParamByName('@CodigoConcesionaria').Value);                  //PAR00114-NDR-20111005

                        if Trim(Error) = '' then begin
                            response.ContentType := 'application/pdf';
                            response.SetCustomHeader('Content-Disposition', Format('attachment; filename=Certificado_Infracciones_%s.pdf',[ParamByName('@Patente').Value]));
                        end
                        else Plantilla := GenerarMensaje(FDM, Request, Error);
                    finally
                        if Assigned(ReportCertificadoInfraccionesForm) then FreeAndNil(ReportCertificadoInfraccionesForm);
                    end;
                end
                else Plantilla := GenerarMensaje(FDM, Request, MSG_DOCUMENTO_NO_VALIDO);
            finally
                if FDM.spObtenerVerificacionCertificado.Active then FDM.spObtenerVerificacionCertificado.Close;
            end;
        end
        else Plantilla := GenerarMensaje(FDM, Request, Error);
    except
        on E:Exception do begin
          Plantilla := GenerarError(FDM, Request, E.Message);
        end;
    end;

    Response.Content := Plantilla;
    Handled := true;
end;

procedure TCertificadosInfraccionesWebModule.CertificadosInfraccionesWebModulePruebaCGIAction(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
resourcestring
    htmlCode =
        '<BR/><span style=''font-size:10.0pt;font-family:"Tahoma"''>' +
        '<b style=''mso-bidi-font-weight:normal''>M�todo:</b> %s, ' +
        '<b style=''mso-bidi-font-weight:normal''>Path:</b> %s, ' +
        '<b style=''mso-bidi-font-weight:normal''>Estado:</b> %s' +
        '</span>';
var
    Plantilla, Metodos: String;
    Veces: Integer;
begin
    try
        Plantilla := ObtenerPlantilla(FDM, Request, 'PruebaCGI');
        Plantilla := ReplaceTag(Plantilla, 'CGIFile', ExtractFileName(ISAPICGIFile));
        Plantilla := ReplaceTag(Plantilla, 'Version', GetFileVersionString(ISAPICGIFile, True));
        Metodos   := '';
        for Veces := 0 to Actions.Count - 1 do begin
            Metodos :=
                Metodos +
                Format(htmlCode,
                        [Actions.Items[Veces].Name + iif(Actions.Items[Veces].Default, ' (Default)',''),
                         Actions.Items[Veces].PathInfo,
                         iif(Actions.Items[Veces].Enabled, 'Habilitado', 'No Habilitado')]);
        end;
        Plantilla := ReplaceTag(Plantilla, 'Metodos', Metodos);
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'PruebaCGI');
        end;
    end;
    Response.Content := Plantilla;

end;

end.
