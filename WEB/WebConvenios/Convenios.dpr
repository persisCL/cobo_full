library Convenios;
{
Firma		:	SS_1397_MGO_20151021
Descripcion	:	Se agrega referencia a frmReporteFacturacionDetallada e hijos
}
{$APPTYPE CONSOLE}

uses
  WebBroker,
  ActiveX,
  ComObj,
  ISAPIApp,
  ISAPIThreadPool,
  EventLog,
  IniFiles,
  wmConvenios in 'wmConvenios.pas' {wmConveniosWEB: TWebModule},
  dmConvenios in '..\Comunes\dmConvenios.pas' {dmConveniosWEB: TDataModule},
  Parametros in '..\Comunes\Parametros.pas',
  FuncionesWEB in '..\Comunes\FuncionesWEB.pas',
  Sesiones in '..\Comunes\Sesiones.pas',
  ManejoDatos in '..\Comunes\ManejoDatos.pas',
  Combos in '..\Comunes\Combos.pas',
  Zip in '..\Comunes\Zip.pas',
  ZipDlls in '..\Comunes\ZipDlls.pas',
  ManejoErrores in '..\Comunes\ManejoErrores.pas',
  ConstParametrosGenerales in '..\..\Comunes\ConstParametrosGenerales.pas',
  Ejecucion in '..\Comunes\Ejecucion.pas',
  PeaTypes in '..\..\Comunes\PeaTypes.pas',
  ImprimirWO in '..\..\Comunes\ImprimirWO.pas' {frmImprimirWO},
  ReporteFactura in '..\..\Comunes\ReporteFactura.pas' {frmReporteFactura},
  frmReporteNotaDebitoElectronica in '..\..\Comunes\frmReporteNotaDebitoElectronica.pas' {ReporteNotaDebitoElectronicaForm},
  frmReporteBoletaFacturaElectronica in '..\..\Comunes\frmReporteBoletaFacturaElectronica.pas' {ReporteBoletaFacturaElectronicaForm},
  frmReporteNotaCreditoElectronica in '..\..\Comunes\frmReporteNotaCreditoElectronica.pas' {ReporteNotaCreditoElectronicaForm},
  DTEControlDLL in '..\..\FacturaElectronica\DTEControlDLL.pas',
  Captcha in '..\Comunes\Captcha.pas',
  CSVUtils in '..\..\Comunes\CSVUtils.pas',
  frmReporteFacturacionDetalladaWeb in '..\Comunes\frmReporteFacturacionDetalladaWeb.pas' {FormReporteFacturacionDetalladaWeb},
  XMLCAC in '..\..\Comunes\XMLCAC.pas',
  utilhttp in '..\..\..\Utiles\Source\utilhttp.pas',
  IntOficinaVirtual in '..\Comunes\IntOficinaVirtual.pas',
  frmReporteFacturacionDetallada in '..\..\Facturacion\frmReporteFacturacionDetallada.pas' {FormReporteFacturacionDetallada},
  frmSeleccFactDetallada in '..\..\Facturacion\frmSeleccFactDetallada.pas' {fSeleccFactDetallada},
  PeaProcs in '..\..\Comunes\PeaProcs.pas',
  frmBloqueosSistema in '..\..\Comunes\frmBloqueosSistema.pas' {BloqueosSistemaForm},
  frmMuestraMensaje in '..\..\Comunes\frmMuestraMensaje.pas' {PanelMensajesForm},
  DMConnection in '..\..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  PeaProcsCN in '..\..\Comunes\PeaProcsCN.pas',
  ImgTypes in '..\..\Comunes\ImgTypes.pas',
  RStrings in '..\..\Comunes\RStrings.pas',
  MsgBoxCN in '..\..\Comunes\MsgBoxCN.pas' {frmMsgBoxCN},
  AvisoTagVencidos in '..\..\Comunes\Avisos\AvisoTagVencidos.pas',
  NotificacionImpresionReporte in '..\..\Comunes\Avisos\NotificacionImpresionReporte.pas' {FormNotificacionImpresionReporte},
  frmMuestraPDF in '..\..\Comunes\frmMuestraPDF.pas' {MuestraPDFForm},
  Diccionario in '..\..\Comunes\Diccionario.pas',
  ClaveValor in '..\..\Comunes\ClaveValor.pas',
  Notificacion in '..\..\Comunes\Avisos\Notificacion.pas',
  NotificacionImpresionDoc in '..\..\Comunes\Avisos\NotificacionImpresionDoc.pas',
  TipoFormaAtencion in '..\..\Comunes\Avisos\TipoFormaAtencion.pas',
  Aviso in '..\..\Comunes\Avisos\Aviso.pas',
  frmVentanaAviso in '..\..\Comunes\Avisos\frmVentanaAviso.pas' {VentanaAvisoForm},
  EncriptaRijandel in '..\..\Comunes\EncriptaRijandel.pas',
  base64 in '..\..\Comunes\base64.pas',
  AES in '..\..\Comunes\AES.pas',
  DMComunicacion in '..\..\Comunes\DMComunicacion.pas' {DMComunicaciones: TDataModule},
  RVMTypes in '..\..\Comunes\RVMTypes.pas',
  RVMClient in '..\..\Comunes\RVMClient.pas',
  RVMLogin in '..\..\Comunes\RVMLogin.pas' {frmLogin},
  RVMBind in '..\..\Comunes\RVMBind.pas',
  MaskCombo in '..\..\Componentes\MaskCombo\MaskCombo.pas',
  FrmRptInformeUsuario in '..\..\Comunes\FrmRptInformeUsuario.pas' {FormRptInformeUsuario},
  UtilReportes in '..\..\Comunes\UtilReportes.pas',
  FrmRptInformeUsuarioConfig in '..\..\Comunes\FrmRptInformeUsuarioConfig.pas' {FormRptInformeUsuarioConfig},
  FactoryINotificacion in '..\..\Comunes\Avisos\FactoryINotificacion.pas',
  SysUtilsCN in '..\..\Comunes\SysUtilsCN.pas',
  frmMensajeACliente in '..\..\Comunes\frmMensajeACliente.pas' {FormMensajeACliente};

{*******************************
  * END : SS_1397_MGO_20151021   *
  ********************************}

//SS_1276_NDR_20150528


{$R *.RES}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;

//var
//    inifile: TIniFile;

begin
{
    iniFile := TIniFile.create(IniFileName);
    ISAPIThreadPool.NumberOfThreads := iniFile.ReadInteger('General', 'NumberOfThreads', 25);
    Application.MaxConnections := iniFile.ReadInteger('General', 'MaxConnections', 25);
    inifile.free;
}
    ISAPICGIFile := 'Convenios.dll';

  CoInitFlags := COINIT_MULTITHREADED;
  Application.Initialize;
  EventLogAppName := 'WEBConvenios';
  Application.CreateForm(TwmConveniosWEB, wmConveniosWEB);
  Application.Run;
end.
