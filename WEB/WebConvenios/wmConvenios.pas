{********************************** Unit Header ********************************
Revision 1
Author : nefernandez
Date : 11/05/2007
Description : SS 511:
    - La p�gina de Generaci�n de Clave los datos que se deben verificar ahora son:
      Patente, D�gito verificador de la patente y el e-mail.
    - Se registra en la base de datos el email enviado al generar la clave.
    - En la pagina de Domicilio de Facturaci�n, ya no se pueden cambiar los datos.
    - Si eligi� "Olvid� Clave" se debe presentar la p�gina de Generaci�n de Clave
    Revision :5
        Author : vpaszkowicz
        Date : 24/02/2009
        Description :Controlo que el comprobante no este pago por este canal de
        pago y este monto.
    Revision : 6
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

    Revision : 7
    Author :   mpiazza
    Date Created: 10/02/2010
    Language : ES-AR
    Description : SS-864 - se cambia el mensaje
                  MSG_NO_TIENE_PREGUNTA_SECRETA por MSG_OLVIDO_SU_CLAVE
    Revision : 4
    Date: 19/10/2009
    Author: mpiazza
    Description:    Ref ss-787 se modifica GenerarClave

    Revision : 5
    Date: 05/05/2010
    Author: mpiazza
    Description: Ref ss-511 se modifica GenerarClave

    Revision : 5
    Date: 05/05/2010
    Author: mpiazza
    Description: Ref ss-772 se modifica:
                  -CambiarDatosDomicilio

    Revision : 6
    Date: 07-Julio-2010
    Author: Nelson Droguett Sierra
    Description: Ref.Fase 2 - Se agrega Concesionaria.

    Revision : 7
    Author : jjofre
    Date : 17/08/2010
    Description : (SS-908)
                  -TwmConveniosWEB.Logout: se redirecciona a BASE_PATH

    Revision : 8
    Author : jjofre
    Date : 13/09/2010
    Description : (// Rev 8.  SS_917)
                -Se a�adieron o modificaron las siguientes funciones/procedimientos
                    DescargarCSVDetalleTransitos
                    wmConveniosWEBpruebaAction

                -Se a�ade el objeto PlantillacsvTransacciones

    Revision : 9  // Anulada el 15/11/2010 a petici�n de CN
    Author : jjofre
    Date : 05/10/2010
    Description : (//Rev.9 SS_756)
                -Se a�adieron o modificaron las siguientes funciones/procedimientos
                    TwmConveniosWEB.wmConveniosWEBSalidaSTDAction  : se valida si el comprobante fue pagado antes
                    por la interfaz bdp, si no fue pagado, se hace el pago del comprobante, despues de la aceptacion
                    del banco.

    Revision : 10
    Author : jjofre
    Date : 08/10/2010
    Description : (//Rev.10 SS_756)
                -Se a�adieron o modificaron las siguientes funciones/procedimientos
                  DescargarCSVDetalleTransitos : Se llama a la nueva funcion CargarDatosCSV que trae los transitos

    Revision : 11 // Anulada el 15/11/2010 a petici�n de CN
    Author : pdominguez
    Date : 12/10/2010
    Description : SS 756 - Pagos BDP No Registrados.
        - Se modific� el Procedimiento wmConveniosWEBSalidaSTDAction.

    Revision : 12
    Author : pdominguez
    Date : 22/10/2010
    Description : SS 917 - Modificaciones WEB
        - Se cambia el m�todo para leer las fechas devueltas y evitar el
        posible conflicto en la conversi�n, por la configuraci�n regional.

    Revision : 13
    Author : pdominguez
    Date : 28/10/2010
    Description : SS 917 - Modificaciones WEB
        - se crea el procedimiento wmConveniosWEBDescargarArchivoAyudaExcelAction

    Firma       : SS966-NDR-20110810
    Description : Boton de Pago Banco de Chile
                Se implementa la funcionalidad de boton de pago Banco de Chile.

    Firma       : SS-966-NDR-20111128
    Description : Se deharcodea el Codigo de Servicio de recaudacion SRVREC=004168

    Firma       : SS-966-NDR-20120103
    Description : Al llamar a AgregarAuditoriaBDP_MPINIBCH se envia el parametro CodigoServicioRecaudacion

    Firma       : SS_1070_MCA_20121005
    Description : Se comentado codigo de envio de clave por smtp y se implementa el envio atraves del mailer insertando los datos
                  en la tabla Email_Mensajes

    Firma       : SS_660_MCA_20130624
    descripcion : Se valida si el convenio esta en lista amarilla o cobranza judicial para mostrar el mensaje web

    Firma       : SS_1113_MCA_20130704
    Descripcion : se agregan parametros generales con el tiempo de expiracion para la sesion y pago WEB

	Firma		: SS_1197_MCA_20140617
    Description : se corrige problema al obtener la clave secreta, ya que si no tenia clave configurada
                	daba un error con el campo Reintentos.

    Firma       :   SS_1147_CQU_20150522
    Descripcion :   Se agrega el string list slTiposComprobantes para recibir el TipoComprobante (NK, SD, etc) interno,
                    por petici�n de FBE s�lo se imprimir�n comprobantes NK y cuya fecha de emisi�n sea mayor o igual al 2-May-2015
                    esto se realiza en el procedimiento VerUltimaNotaDeCobro 

	Firma       : SS_1147_MCA_20150528
    Descripcion : se activa la impresion del comprobantes desde la DLL y no de Comprobantes.exe

    Firma       : SS_1248_MCA_20150602
    Descripcion : nuevo proceso de generacion de clave y recuperacion de clave secreta.

    Firma       : SS_1305_MCA_20150612
    Descripcion : se agrega bitacora de uso para el sitio WEB

    Firma		:	SS_1327_MBE_20150630
    Descripcion	:	Se cambia el mensaje de "Costanera Norte" por "Vespucio Sur"
	Firma       : SS_1304_MCA_20150709
    Descripcion : se agrega grilla con todas las deudas del RUT

    Firma       :   SS_1332_CQU_20150721
    Descripcion :   Se agrega llamada al WebService IntOficinaVirtual que ser� el encargado de imprimir los comprobantes NK
                    Este WS requiere que exista una CarpetaVirtual llamada Comprobantes y que apunte a la ruta especificada
                    en el parametro general llamado OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS tambien se requiere tener configurado
                    el parametro general OFICINA_VIRTUAL_URL_UBICACION para indicar donde se encuentra el Webservice OficinaVirtual.
                    Este proceso requiere la plantilla CV_DescargarArchivo.htm

    Firma		: SS_1280_MCA_20150812
	descripcion	: se agrega proceso de pago on line sin ingresar a la oficina virtual

 	Firma       : SS_1356_MCA_20150903
    Descripcion : se agrega BDP Santander

    Firma       : SS_1390_MGO_20150928
    Descripcion : Se agrega columna "Detalle" en el m�todo VerUltimaNotaDeCobro, que muestra links para descargar
                  el reporte de tr�nsitos de un convenio en formato PDF o CSV.

    Firma       : SS_1397_MGO_20151021
    Descripcion : Se reemplaza Now por FDM.NowBase() en listado de Estacionamientos

    Firma       : SS_1332_CQU_20151106
    Descripcion : Se agrega la liberaci�n del componente (FreeAndNil)
                  y la evaluaci�n del error "no se pudo generar" ya que no lo estaba realizando.

    Firma       : SS_1397_MGO_20151106
    Description : Se agrega m�todo VerNoFacturados

	Firma		:	SS_1332_CQU_20151116
	Descripcion	:	Se agrega control de errores y se quita la liberaci�n del componente (FreeAndNil)

	Firma		: SS_1332_CQU_20151229
    Descripcion : Se quita la liberaci�n del componente wsOficinaVirtual (FreeAndNil) ya que no aplica para este tipo
*******************************************************************************}
unit wmConvenios;

interface

uses
  SysUtils, Classes, HTTPApp, Combos, forms,
  Parametros, dmConvenios, FuncionesWEB, util,
  UtilHTTP, sesiones, utilDB, ManejoDatos, Dateutils,
  Eventlog, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdMessageClient, IdSMTP, IdMessage, IdExplicitTLSClientServerBase, IdSMTPBase,
  PeaTypes, ComObj, Captcha, ExtCtrls, jpeg, Graphics, StrUtils,                //SS966-NDR-20110810
  IntOficinaVirtual,                                                            // SS_1332_CQU_20150721
  ConstParametrosGenerales, ADODB, Variants;                                 //SS-966-NDR-20111128
const
        TITULO_AVISO = 'Aviso';

        TAG_CODIGO_SESION = 'CodigoSesion';
        TAG_RUT = 'RUT';
        TAG_RUTDV = 'RUTDV';
        TAG_SIGUIENTE_PASO = 'SiguientePaso';
        TAG_CONVENIO = 'Convenio';
		// Rev. SS 511-787-864
        TAG_CAPTCHA = 'Captchaimg';
        LINK_CAPTCHA = '/ValidacionCaptcha';
		// Fin Rev. SS 511-787-864
        PLANTILLA_AVISO = 'Aviso';

        CODIGO_ENVIO_NOTA_COBRO = '1';
        CODIGO_ENVIO_DETALLE_TRANSITOS = '2';
        CODIGO_ENVIO_INFORME_TRIMESTRAL = '3';

        CODIGO_ENVIO_CORREO_POSTAL = '1';
        CODIGO_ENVIO_CORREO_EMAIL = '2';

        CODIGO_BANCO_SANTANDER = 3;
        CODIGO_BANCO_TRANSBANK = 29;
        CODIGO_BANCO_BANCOCHILE = 1;                                            //SS966-NDR-20110810

        NOMBRE_VARIABLE_CODIGO_SESION_URL   = 'CodigoSesion';
        TAG_PARAMETRO_CODIGO_SESION_EXITO   = 'ParametroCodigoSesionExito';               
        TAG_PARAMETRO_CODIGO_SESION_FRACASO = 'ParametroCodigoSesionFracaso';
        TAG_IDTRX_SANTANDER                 = 'IdTrxSTD';
        TAG_CODIGO_SESION_WEB               = 'CODIGO_SESION_WEB';
        TAG_IDTRX_BANCOCHILE                = 'IdTrxBCH';                       //SS966-NDR-20110810
        TAG_PATENTE 						= 'Patente';						//SS_1197_MCA_20140617
        TAG_PATENTEDV						= 'DigitoVerificadorPatente';       //SS_1197_MCA_20140617
        TAG_PREGUNTA						= 'Pregunta';                       //SS_1197_MCA_20140617
        TAG_RESPUESTA                       = 'Respuesta';                      //SS_1197_MCA_20140617
        TAG_EMAIL	                        = 'email';                          //SS_1197_MCA_20140617

type
  TwmConveniosWEB = class(TWebModule)
    procedure wmConveniosWEBRedirigirSantanderAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure wmConveniosWEBRedirigirTransBankAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure wmConveniosWEBSalidaSTDAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure wmConveniosWEBNotificacionSTDAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);

    procedure wmConveniosWEBRedirigirBancoChileAction(Sender: TObject;                     //SS966-NDR-20110810
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);                 //SS966-NDR-20110810
    procedure wmConveniosWEBSalidaBCHAction(Sender: TObject;                               //SS966-NDR-20110810
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);                 //SS966-NDR-20110810
    procedure wmConveniosWEBNotificacionBCHAction(Sender: TObject;                         //SS966-NDR-20110810
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);                 //SS966-NDR-20110810

    procedure WebModuleCreate(Sender: TObject);
    procedure WebModuleException(Sender: TObject; E: Exception; var Handled: Boolean);
    procedure Login(Sender: TObject; Request: TWebRequest;  Response: TWebResponse; var Handled: Boolean);
    procedure DoLogin(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure PanelDeControl(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure CambiarPass(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure CambiarDatosPersonales(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure CambiarDatosDomicilio(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure BuscaCalle(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure CambiarDatosDomicilioConvenio(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure CambiarDatosMedioPagoConvenio(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure CambiarDatosVehiculosConvenio(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure logout(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure OlvidoPassword(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure CambiarPregunta(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure RemoteLogin(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure VerUltimaNotaDeCobro(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure Cartola(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure EstadoCuenta(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure VerTarifas(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure VerComprobante(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure VerNoFacturados(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);                 //SS_1397_MGO_20151106
    procedure VerReclamos(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure Debug(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebpayAceptacion(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebpayFracaso(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebpayExito(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebPayPrePago(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure PagarUltimaNotaCobro(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure VerConsumos(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure GenerarClave(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure DoGenerarClave(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure BlanquearPass(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure MediosEnvio(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure GrabarMediosEnvio(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    Function  ParseXMLField( Xml, Campo : String) : String;
    procedure wmConveniosWEBpruebaAction(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);
    procedure SaldoxConveniosxRUT(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure wmConveniosWEBValidacionCaptchaAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure DescargarCSVDetalleTransitos(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure wmConveniosWEBDescargarArchivoAyudaExcelAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
      var Handled: Boolean);
    procedure wmConveniosWEBValidarInscripcionAraucoTAGAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
      var Handled: Boolean);
    procedure wmConveniosWEBRealizarInscripccionAraucoTAGAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
      var Handled: Boolean);
    procedure wmConveniosWEBReenviarInscripcionAraucoTAGAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
      var Handled: Boolean);
    procedure wmConveniosWEBDescargarCSVDetalleEstacionamientosAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
      var Handled: Boolean);
    procedure PagarDeudaOnline(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);			//SS_1280_MCA_20150812
    procedure DoPagarDeudaOnline(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);			//SS_1280_MCA_20150812
    procedure PagarUltimaNotaCobroOnline(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);	//SS_1280_MCA_20150812
    procedure WebPayPrePagoOnline(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);			//SS_1280_MCA_20150812
    procedure WebpayFracasoTBK(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);			//SS_1280_MCA_20150812
    procedure WebpayExitoTBK(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);				//SS_1280_MCA_20150812
  private
    FDM: TdmConveniosWEB;
    PlantillacsvTransacciones: string; // Rev 8.  SS_917
    function AgregarAuditoria_AdhesionPAK(
        pNumeroDocumento, pPatente, pEMail: string;
        pCodigoPersona, pCumpleRequisitos, pAdheridoEnvioEMail, pImpresionContrato, pFirmaContrato, pCodigoAuditoria: Integer): Integer;



  public
    { Public declarations }
  end;

var
  wmConveniosWEB: TwmConveniosWEB;

implementation

{$IFDEF WEBDEBUGGER}
uses WebReq;
{$ENDIF}

{$R *.DFM}

procedure TwmConveniosWEB.WebModuleCreate(Sender: TObject);
begin
    try
        FDM := TdmConveniosWEB.Create(Self);
        CargarValoresIniciales(FDM);
        FDM.SetearTiemposConsultas;
    except
        on e: exception do begin
            EventLogReportEvent(elError, 'Error al inicializar: ' + e.Message, '');
        end;
    end;
end;

procedure TwmConveniosWEB.WebModuleException(Sender: TObject; E: Exception; var Handled: Boolean);
begin
    handled := true;
    try
        Response.Content := GenerarError(fdm, Request, e.Message);
    except
        on e: exception do begin
            Response.Content := 'Hubo un error: ' + e.Message;
        end;
    end;
end;


{******************************** Procedure Header *****************************
Procedure Name: Login
Author :
Date Created :
Parameters : Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean
Description :

*******************************************************************************}
procedure TwmConveniosWEB.Login(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Plantilla: string;
    ArchivoPedido: string;
begin
    // por si piden un archivo .js o .css
    ArchivoPedido := Request.PathInfo;
    if pos(Request.ScriptName, ArchivoPedido) <> 0 then begin
        ArchivoPedido := copy(ArchivoPedido, pos(Request.ScriptName, ArchivoPedido) + length(Request.ScriptName) + 1, length(ArchivoPedido));
    end;
    while pos('/', ArchivoPedido) > 0 do ArchivoPedido := copy(ArchivoPedido, pos('/', ArchivoPedido) + 1, length(ArchivoPedido));

    if (pos('.css', ArchivoPedido) > 0) or (pos('.js', ArchivoPedido) > 0) then begin
        Response.Content := FileToString(DIRECTORIO_PLANTILLAS + ArchivoPedido);
        Response.Content := ReplaceTag(Response.Content, 'BaseArchivo', BASE_PATH + NOMBRE_EXE);
        exit;
    end;
    // fin

    try
        Plantilla := ObtenerPlantilla(FDM, Request, 'Login');
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'Login');
        end;
    end;
    Plantilla := ReplaceTag(Plantilla, 'ErrorLogin', '');
    Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
    Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);

    Response.Content := Plantilla;
    Handled := true;
end;
procedure TwmConveniosWEB.DoLogin(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
    RUT, Password, IP,
    Plantilla: string;
    CodigoConvenio,
    CodigoPersona,
    CodigoSesion: integer;
	  Inhabilitado, CobranzaJudicial, TotalConvenios,                             //SS_660_MCA_20130624
    CodigoEmpresaCobranza: integer;                                             //SS_660_MCA_20130624
	  NombreEmpresa, DireccionEmpresa, NumeroConvenio: string;                    //SS_660_MCA_20130624
    Mensaje: string;                                                            //SS_660_MCA_20130624
begin
    try
        RUT := Request.ContentFields.Values[TAG_RUT] + Request.ContentFields.Values[TAG_RUTDV];
        Password := Request.ContentFields.Values['Password'];
        IP := Request.RemoteAddr;

        case CheckLogin(FDM, RUT, Password, IP, CodigoSesion, CodigoPersona) of
            lrOK:               begin
                                    // si no tenia una sesion abierta, le genero una
                                    if CodigoSesion = 0 then CodigoSesion := GenerarSesion(FDM, CodigoPersona, Request.RemoteAddr, True);				//SS_1280_MCA_20150812

                                    Request.ContentFields.Values[TAG_CODIGO_SESION] := inttostr(CodigoSesion);

                                    // si tiene que ir a la "pantalla compulsiva"...
                                    if NoVioPantallaEnvioDocumentos(FDM, CodigoPersona, CodigoConvenio) then begin
                                        Request.ContentFields.Values[TAG_CONVENIO] := inttostr(CodigoConvenio);
                                        // lo mando a la pantalla
                                        MediosEnvio(Sender, Request, Response, Handled);

                                        exit;
                                    end;

                                    //BEGIN SS_660_MCA_20130624
            						//busca si el cliente tiene algun convenio en lista amarilla
									Inhabilitado := QueryGetValueInt(FDM.Base, 'SELECT dbo.EstaPersonaEnListaAmarilla(' + IntToStr(CodigoPersona) + ')');
									//Retorna el codigo de la empresa recaudadora
									CobranzaJudicial := QueryGetValueInt(FDM.Base, 'SELECT dbo.EstaPersonaEnCobranzaJudicial(' + IntToStr(CodigoPersona) + ')');

                                    If ((CobranzaJudicial = 1) and (Inhabilitado = 1)) then begin
                                        CodigoEmpresaCobranza := QueryGetValueInt(FDM.Base, 'SELECT dbo.ObtenerCodigoEmpresaCobranzaJudicial(' + IntToStr(CodigoPersona) + ')');
                                        NombreEmpresa := QueryGetValue(FDM.Base, 'SELECT NombreEmpresa FROM EmpresasRecaudadoras WITH(nolock) WHERE CodigoEmpresasRecaudadoras = ' + IntToStr(CodigoEmpresaCobranza));
                                        DireccionEmpresa := QueryGetValue(FDM.Base, 'SELECT ISNULL(CalleDesnormalizada + '' '' + RTRIM(Numero), ''Sin Direccion Registrada'') FROM EmpresasRecaudadoras WITH(nolock) WHERE CodigoEmpresasRecaudadoras = ' + IntToStr(CodigoEmpresaCobranza));
                                        Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoCobranzaMoroso');
                                        Plantilla := ReplaceTag(Plantilla, 'EmpresaCobranza', NombreEmpresa);
                                        Plantilla := ReplaceTag(Plantilla, 'DireccionEmpresaCobranza', DireccionEmpresa);
                                        Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PaneldeControl');
                                        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                                    end else begin

                                        If (CobranzaJudicial > 0) then begin
                                            TotalConvenios := QueryGetValueInt(FDM.Base, 'SELECT dbo.TotalConveniosEnCobranzaJudicial(' + IntToStr(CodigoPersona) + ')');
                                            if TotalConvenios > 1 then Begin
                                                Mensaje := 'Presenta m�s de un convenio en Cobranza Judicial';
                                            end else begin
                                                NumeroConvenio := QueryGetValue(FDM.Base, 'SELECT dbo.ObtenerNumeroConvenioEnCobranzaJudicial(' + IntToStr(CodigoPersona) + ')');
                                                Mensaje := 'El convenio ' + NumeroConvenio + 'se encuentra en Cobranza Judicial';
                                            End;
                                            CodigoEmpresaCobranza := QueryGetValueInt(FDM.Base, 'SELECT dbo.ObtenerCodigoEmpresaCobranzaJudicial(' + IntToStr(CodigoPersona) + ')');
                                            NombreEmpresa := QueryGetValue(FDM.Base, 'SELECT NombreEmpresa FROM EmpresasRecaudadoras WITH(nolock) WHERE CodigoEmpresasRecaudadoras = ' + IntToStr(CodigoEmpresaCobranza));
										    DireccionEmpresa := QueryGetValue(FDM.Base, 'SELECT ISNULL(CalleDesnormalizada + '' '' + RTRIM(Numero), ''Sin Direccion Registrada'') FROM EmpresasRecaudadoras WITH(nolock) WHERE CodigoEmpresasRecaudadoras = ' + IntToStr(CodigoEmpresaCobranza));
										    Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoCobranzaJudicial');
                                            Plantilla := ReplaceTag(Plantilla, 'Convenio', Mensaje);
										    Plantilla := ReplaceTag(Plantilla, 'EmpresaCobranza', NombreEmpresa);
										    Plantilla := ReplaceTag(Plantilla, 'DireccionEmpresaCobranza', DireccionEmpresa);
										    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PaneldeControl');
										    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                                        end else begin
                                        	If Inhabilitado = 1 Then Begin
                                            	TotalConvenios := QueryGetValueInt(FDM.Base, 'SELECT dbo.TotalConveniosEnListaAmarilla(' + IntToStr(CodigoPersona) + ')');
                                             	if TotalConvenios > 1 then Begin
                                                	Mensaje := 'Presenta m�s de un convenio inhabilitado';
                                            	end else begin
                                                	NumeroConvenio := QueryGetValue(FDM.Base, 'SELECT dbo.ObtenerNumeroConvenioEnListaAmarilla(' + IntToStr(CodigoPersona) + ')');
	                                                Mensaje := 'El convenio ' + NumeroConvenio + 'se encuentra inhabilitado';
    	                                        End;

                                            	Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoInhabilitadoMorosidad');
	                                            Plantilla := ReplaceTag(Plantilla, 'Convenio', Mensaje);
                                                Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PaneldeControl');
                                                Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                                            end else begin
                                                CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ IntToStr(CodigoPersona) + ', 0)');					//SS_1305_MCA_20150612
                                                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'DoLogin - Inicio Sesion correcta PANEL DE CONTROL');				//SS_1305_MCA_20150612
                                            	PanelDeControl(Sender, Request, Response, Handled);
                                                exit;
                                            end;
                                        end;
                                    end;

                                    // lo mando al panel de control
                                    //PanelDeControl(Sender, Request, Response, Handled);
                                    //exit;
                                    //END SS_660_MCA_20130624
                                end;
            lrNoUser,
            lrWrongPassword:    begin
                                    Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoLogin');
                                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_RUT_O_CLAVE_INCORRECTO);
                                    CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');		//SS_1305_MCA_20150612
                                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 2, RUT, 'DoLogin - ' + MSG_RUT_O_CLAVE_INCORRECTO);				//SS_1305_MCA_20150612
{                                    Plantilla := ObtenerPlantilla(FDM, Request, 'Login');
                                    Plantilla := ReplaceTag(Plantilla, 'ErrorLogin', 'RUT o clave incorrecto');
                                    Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
                                    Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);}
                                end;
            lrManyUsers:        begin
                                    Plantilla := GenerarError(FDM, Request, MSG_ERROR_DEMASIADOS_USUARIOS);
                                    CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');			//SS_1305_MCA_20150612
                                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 2, RUT, 'DoLogin - ' + MSG_ERROR_DEMASIADOS_USUARIOS);				//SS_1305_MCA_20150612
                                end;
            lrNoPassword:       begin
                                    // no tiene password
                                    CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');			//SS_1305_MCA_20150612
                                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 2, RUT, 'DoLogin - ' + MSG_CLAVE_NO_CONFIGURADA);					//SS_1305_MCA_20150612
                                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_CLAVE_NO_CONFIGURADA);
                                    Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
                                    Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
                                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'GenerarClave');
                                    Plantilla := ReplaceTag(Plantilla, 'Reintentos', '0');

//                                    Plantilla := GenerarError(FDM, Request, MSG_ERROR_NO_PASSWORD);
                                end;
            lrAlreadyInSession: begin
                                    Plantilla := GenerarError(FDM, Request, MSG_ERROR_ALREADY_IN_SESSION);
                                    CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');				//SS_1305_MCA_20150612
                                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 2, RUT, 'DoLogin - ' + MSG_ERROR_ALREADY_IN_SESSION);					//SS_1305_MCA_20150612
                                end;
            lrPasswordReseteadoVencido:
                                begin
                                    Plantilla := GenerarError(FDM, Request, MSG_ERROR_PASSWORD_RESETEADO_VENCIDO);
                                    CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');					//SS_1305_MCA_20150612
                                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 2, RUT, 'DoLogin - ' + MSG_ERROR_PASSWORD_RESETEADO_VENCIDO);				//SS_1305_MCA_20150612
                                end;

        end;
    except
        on e: exception do begin
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 2, RUT, 'DoLogin - ' + 'Se ha producido un error : ' + e.Message);				//SS_1305_MCA_20150612
            Plantilla := AtenderError(Request, FDM, e, 'DoLogin');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

{----------------------------------------------------------------------------
Revision : 2
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.PanelDeControl(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
	// Rev. SS 511-787-864
    Plantilla : string;                                                         //SS_1248_MCA_20150602
    //Pregunta,                                                                 //SS_1248_MCA_20150602
    //Respuesta                                                                 //SS_1248_MCA_20150602

	// Rev. SS 511-787-864
    ConvenioElegido,
    CodigoPersona,
    CodigoSesion: integer;
    RUT: string;					//SS_1305_MCA_20150612
    CodigoConvenio: Integer;		//SS_1305_MCA_20150612
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));				//SS_1305_MCA_20150612
    RUT :=  QueryGetValue(fdm.base, 'SELECT NumeroDocumento FROM Personas WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));								//SS_1305_MCA_20150612
    CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');									//SS_1305_MCA_20150612

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
			// Rev. SS 511-787-864
            //CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));			//SS_1305_MCA_20150612
            //Pregunta := QueryGetValue(fdm.base, 'SELECT ISNULL(Pregunta,'''') FROM Personas WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));		//SS_1248_MCA_20150602
            //Respuesta := QueryGetValue(fdm.base, 'SELECT ISNULL(Respuesta,'''') FROM Personas WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));	//SS_1248_MCA_20150602
			// Fin Rev. SS 511-787-864
            if not DebeCambiarPassword(FDM, CodigoSesion) then begin
                Plantilla := ObtenerPlantilla(FDM, Request, 'PanelDeControl');
                Plantilla := ReplaceTag(Plantilla, 'Bienvenida', QueryGetValue(fdm.Base, 'EXEC WEB_CV_ObtenerBienvenida @CodigoSesion = ' + inttostr(CodigoSesion)));
                Plantilla := ReplaceTag(Plantilla, 'ValoresConvenio', GenerarComboConvenios(FDM, CodigoPersona, strtointdef(Request.contentfields.Values[TAG_CONVENIO], 0), ConvenioElegido));

            end else begin
                Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_DEBE_CAMBIAR_PASSWORD);
                Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'CambiarPass');
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'PanelDeControl - ' + MSG_DEBE_CAMBIAR_PASSWORD);				//SS_1305_MCA_20150612
                // Rev. SS 511-787-864
				//Plantilla := ReplaceTag(Plantilla, 'Pregunta', Pregunta);     //SS_1248_MCA_20150602
                //Plantilla := ReplaceTag(Plantilla, 'Respuesta', Respuesta);   //SS_1248_MCA_20150602
				// Fin Rev. SS 511-787-864
            end;
        end else begin																	//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'PanelDeControl - Sesion Expirada');		//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'PanelDeControl');
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'PanelDeControl - Se ha producido un error Panel de Control : ' + e.Message);					//SS_1305_MCA_20150612
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

{----------------------------------------------------------------------------
Revision : 1
    Date: 02/03/2010
    Author: mpiazza
    Description:  ss-511 se agrega parametros para
                  modificar pregunta y respuesta secreta

-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.CambiarPass(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
	// Rev. SS 511-787-864
    Plantilla,
    Pregunta,
    Respuesta,
    RUT: string;				//SS_1305_MCA_20150612
    CodigoSesion,
    CodigoPersona,				//SS_1305_MCA_20150612
    CodigoConvenio: integer;	//SS_1305_MCA_20150612
	// Fin Rev. SS 511-787-864
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));				//SS_1305_MCA_20150612
    CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');									//SS_1305_MCA_20150612
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));								//SS_1305_MCA_20150612

      try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if Request.QueryFields.values['Inicio'] = '' then begin
			// Rev. SS 511-787-864
              if (request.ContentFields.values['PreguntayRespuesta']<> '') and
                    (request.ContentFields.values['Pregunta'] = '') and
                    (request.ContentFields.values['Respuesta'] = '' ) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarPass');												//SS_1248_MCA_20150602
                    Plantilla := ReplaceTag(Plantilla, 'ErrorCambio', MSG_COMPLETE_LOS_DATOS);
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 11, RUT, 'CambiarPass - ' + MSG_COMPLETE_LOS_DATOS);							//SS_1305_MCA_20150612
              end else begin
			// Fin Rev. SS 511-787-864
                if CambiarPassword(fdm,
                  CodigoSesion,
                  request.ContentFields.values['Password'],
                  request.ContentFields.values['NuevoPassword'],
                  request.ContentFields.values['RepitaPassword'],
				  // Rev. SS 511-787-864
                  request.ContentFields.values['Pregunta'],
                  request.ContentFields.values['Respuesta'],
				  // Fin Rev. SS 511-787-864
                  Error) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_PASSWORD_CAMBIADO);
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 11, RUT, 'CambiarPass - ' + MSG_PASSWORD_CAMBIADO);				//SS_1305_MCA_20150612
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                end else begin
					// Rev. SS 511-787-864
                    if (request.ContentFields.values['Pregunta'] = '') or (request.ContentFields.values['Respuesta'] = '') then begin
                        Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarPass');																	//SS_1248_MCA_20150602
                        Plantilla := ReplaceTag(Plantilla, 'ErrorCambio', Error);
                        RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 11, RUT, 'CambiarPass - ' + Error);							//SS_1305_MCA_20150612
                    end
                    else begin
                        Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarPass');
                        Plantilla := ReplaceTag(Plantilla, 'Pregunta',request.ContentFields.values['Pregunta']);
                        Plantilla := ReplaceTag(Plantilla, 'Respuesta',request.ContentFields.values['Respuesta']);
                        Plantilla := ReplaceTag(Plantilla, 'ErrorCambio', Error);																	//SS_1248_MCA_20150602
                        RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 11, RUT, 'CambiarPass - ' + Error);							//SS_1305_MCA_20150612
                    end;
					// Rev. SS 511-787-864
                end;
              end;
            end else begin
                Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarPass');
				// Rev. SS 511-787-864
                //CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));	//SS_1305_MCA_20150612
                //Pregunta := QueryGetValue(fdm.base, 'SELECT ISNULL(Pregunta,'''') FROM Personas WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));			//SS_1248_MCA_20150602
                //Respuesta := QueryGetValue(fdm.base, 'SELECT ISNULL(Respuesta,'''') FROM Personas WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));		//SS_1248_MCA_20150602

                //Plantilla := ReplaceTag(Plantilla, 'Pregunta', Pregunta);																									//SS_1248_MCA_20150602
                //Plantilla := ReplaceTag(Plantilla, 'Respuesta', Respuesta);																								//SS_1248_MCA_20150602
				// Fin Rev. SS 511-787-864
                Plantilla := ReplaceTag(Plantilla, 'ErrorCambio', '');
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 11, RUT, 'CambiarPass - ' + 'Debe Cambiar Password - Panel de Control');					//SS_1305_MCA_20150612
            end;
        end else begin																	//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarPass - Sesion Expirada');		//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'CambiarPass');
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 11, RUT, 'CambiarPass - ' + 'Se ha producido un error CambiarPass: ' + E.Message);				//SS_1305_MCA_20150612
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;


procedure TwmConveniosWEB.CambiarDatosPersonales(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
    Plantilla,
    RUT: string;			//SS_1305_MCA_20150612
    EsPersona: boolean;
    CodigoPersona,
    CodigoSesion,
    CodigoConvenio: integer;	//SS_1305_MCA_20150612
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));				//SS_1305_MCA_20150612
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);																						//SS_1305_MCA_20150612
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));								//SS_1305_MCA_20150612

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            //CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));		//SS_1305_MCA_20150612

            EsPersona := uppercase(QueryGetValue(fdm.Base, 'SELECT Personeria FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona))) = 'F';
            if Request.ContentFields.values['Finalizar'] = '' then begin
                if EsPersona then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarDatosPersonalesPersona');
                    PersonaCargarDatosPersonales(FDM, Plantilla, CodigoPersona);
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosPersonalesPersona - Cambiar Datos Personales Persona');		//SS_1305_MCA_20150612
                end else begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarDatosPersonalesEmpresa');
                    EmpresaCargarDatosPersonales(FDM, Plantilla, CodigoPersona);
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosPersonalesEmpresa - Cambiar Datos Personales Empresa');		//SS_1305_MCA_20150612
                end;
            end else begin
                if EsPersona then begin
                    if not PersonaVerificarDatosPersonales(FDM, Request, Error) then begin
                        Plantilla := GenerarError(FDM, Request, Error);
                        RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosPersonalesPersona - ' + Error);				//SS_1305_MCA_20150612
                    end else begin
                        if GrabarCambiosDatosPersonalesPersona(FDM, Request, CodigoSesion, Error) then begin
                            Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                            Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                            Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_DATOS_PERSONALES_CAMBIADOS);
                            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosPersonalesPersona - ' + MSG_DATOS_PERSONALES_CAMBIADOS);		//SS_1305_MCA_20150612
                        end else begin
                            Plantilla := GenerarError(FDM, Request, Error);
                            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosPersonalesPersona - ' + Error);			//SS_1305_MCA_20150612
                        end;
                    end;
                end else begin
                    if not EmpresaVerificarDatosPersonales(FDM, Request, Error) then begin
                        Plantilla := GenerarError(FDM, Request, Error);
                        RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosPersonalesEmpresa - ' + Error);				//SS_1305_MCA_20150612
                    end else begin
                        if GrabarCambiosDatosPersonalesEmpresa(FDM, Request, CodigoSesion, Error) then begin
                            Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                            Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                            Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_DATOS_PERSONALES_CAMBIADOS);
                            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosPersonalesEmpresa - ' + MSG_DATOS_PERSONALES_CAMBIADOS);		//SS_1305_MCA_20150612
                        end else begin
                            Plantilla := GenerarError(FDM, Request, Error);
                            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosPersonalesEmpresa - ' + Error);				//SS_1305_MCA_20150612
                        end;
                    end;
                end;
            end;
        end else begin																		                                                    //SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosPersonales - Sesion Expirada');			//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'CambiarDatosPersonales');
        end;
    end;

    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmConveniosWEB.CambiarDatosDomicilio(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
    Plantilla,
    RUT: string;						//SS_1305_MCA_20150612
    CodigoPersona,
	// Rev. SS 511-787-864
    CodigoSesion,
    CodigoConvenio,
    CodigoEstadoConvenio: integer;
	// Fin Rev. SS 511-787-864
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));			//SS_1305_MCA_20150612
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);																					//SS_1305_MCA_20150612
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));							//SS_1305_MCA_20150612

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if Request.ContentFields.values['Finalizar'] = '' then begin
                //CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));			//SS_1305_MCA_20150612
				// Rev. SS 511-787-864
                //CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);																					//SS_1305_MCA_20150612
                CodigoEstadoConvenio := QueryGetValueInt(FDM.base, 'SELECT CodigoEstadoConvenio FROM Convenio WITH (NOLOCK) WHERE CodigoConvenio = ' + inttostr(CodigoConvenio));
                case CodigoEstadoConvenio of
                    1: begin
                        Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarDatosDomicilio');
                    end;
                    2: begin
                        Plantilla := ObtenerPlantilla(FDM, Request, 'VerDatosDomicilio');
                    end;
                end;
				// Rev. SS 511-787-864
                if (request.ContentFields.Values['ControlActivo'] <> '') then begin
                    Plantilla := ReplaceTag(Plantilla, 'InitCode', 'form1.' + request.ContentFields.Values['ControlActivo'] + '.focus();');
                end else begin
                    Plantilla := ReplaceTag(Plantilla, 'InitCode', '');
                end;
                if Request.QueryFields.values['Inicio'] <> '' then begin
                    CargarDatosDomicilio(FDM, Plantilla, CodigoPersona);
                    Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'CambiarDatosDomicilio - Cargar Datos Domicilio');			//SS_1305_MCA_20150612
                end else begin
                    CargarDatosDomicilioRequest(FDM, Request, Plantilla);
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'CambiarDatosDomicilio - Cargar Datos Domicilio');			//SS_1305_MCA_20150612
                end;
            end else begin
                if not VerificarDatosDomicilio(FDM, Request, Error) then begin
                    Plantilla := GenerarError(FDM, Request, Error);
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'CambiarDatosDomicilio - ' + Error);							//SS_1305_MCA_20150612
                end else begin
                    if GrabarCambiosDatosDomicilio(FDM, Request, CodigoSesion, Error) then begin
                        Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                        Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                        Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, 'Los datos de domicilio han sido cambiados con �xito.');
                        RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'CambiarDatosDomicilio - Los datos de domicilio han sido cambiados con �xito.');			//SS_1305_MCA_20150612
                    end else begin
                        Plantilla := GenerarError(FDM, Request, Error);
                        RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'CambiarDatosDomicilio - ' + Error);					//SS_1305_MCA_20150612
                    end;
                end;
            end;
        end else begin																				//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosDomicilio - Sesion Expirada');		//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));	// Rev. SS 511-787-864
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'CambiarDatosDomicilio');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;


procedure TwmConveniosWEB.BuscaCalle(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
    Plantilla: string;
    Region, Comuna, Texto, ComunaElegida: string;
begin
    try
        Plantilla := ObtenerPlantilla(FDM, Request, 'BuscadorCalle');

        Region := Request.QueryFields.Values['Region'];
        Comuna := Request.QueryFields.Values['Comuna'];
        Texto := Request.QueryFields.Values['Texto'];

        Plantilla := ReplaceTag(Plantilla, 'CodigoRegion', Region);
        Plantilla := ReplaceTag(Plantilla, 'CodigoComuna', Comuna);
        Plantilla := ReplaceTag(Plantilla, 'Texto', Texto);

        if (Region = '') then begin
            Raise(Exception.Create(MSG_DEBE_ESPECIFICAR_REGION));
        end;

        Plantilla := ReplaceTag(
          Plantilla,
          'Region',
          QueryGetValue(FDM.Base, 'SELECT Descripcion FROM Regiones  WITH (NOLOCK) WHERE CodigoRegion = ' + quotedstr(Region) + ' AND CodigoPais = ' + quotedstr(CODIGO_PAIS))
        );

        if Comuna <> '' then begin
            Plantilla := ReplaceTag(
              Plantilla,
              'Comuna',
              QueryGetValue(FDM.Base, 'SELECT Descripcion FROM Comunas  WITH (NOLOCK) WHERE CodigoPais = ' + quotedstr(CODIGO_PAIS) + ' AND CodigoRegion = ' + quotedstr(Region) + ' AND CodigoComuna = ' + quotedstr(Comuna))
            );
        end else begin
            Plantilla := ReplaceTag(Plantilla, 'Comuna', '<No especificada>');
        end;

        Plantilla := ReplaceTag(Plantilla, 'ValoresComuna', GenerarComboComunas(FDM, Region, Comuna, ComunaElegida));

        Plantilla := ReplaceTag(Plantilla, 'ArrayCalles', GenerarArrayCalles(FDM, Region, Comuna, Texto));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'BuscaCalle');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

{******************************** Procedure Header *****************************
Procedure Name: CambiarDatosDomicilioConvenio
Author :
Date Created :
Parameters : Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean
Description :

Revision 1
Author: mpiazza
Date:05-05-2010
Description:(Ref SS-772)- Se realizan los cambios
*******************************************************************************}
procedure TwmConveniosWEB.CambiarDatosDomicilioConvenio(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
    Plantilla,
    RUT: string;					//SS_1305_MCA_20150612
    CodigoConvenio,
	// Rev. SS 511-787-864
    CodigoSesion,
    CodigoEstadoConvenio,
    CodigoPersona: Integer;			//SS_1305_MCA_20150612
	// Fin Rev. SS 511-787-864
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));			//SS_1305_MCA_20150612

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error ) then begin

                CodigoEstadoConvenio := QueryGetValueInt(FDM.base, 'SELECT CodigoEstadoConvenio FROM Convenio WITH (NOLOCK) WHERE CodigoConvenio = ' + inttostr(CodigoConvenio));	// Rev. SS 511-787-864

                if Request.ContentFields.values['Finalizar'] = '' then begin
                    //CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona  FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));
                    if EsConvenioRNUT(fdm, CodigoConvenio) then begin
                        Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarDatosDomicilioRNUT');
                        Plantilla := ReplaceTag(Plantilla, 'Concesionaria', ObtenerConcesionariaConvenio(FDM, CodigoConvenio));
                    end else begin
						// Rev. SS 511-787-864
                        case CodigoEstadoConvenio of
                            1: Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarDatosDomicilioConvenio');
                            2: Plantilla := ObtenerPlantilla(FDM, Request, 'VerDatosDomicilioConvenio');
                        end;
						// Fin Rev. SS 511-787-864
                    end;

                    if (request.ContentFields.Values['ControlActivo'] <> '') then begin
                        Plantilla := ReplaceTag(Plantilla, 'InitCode', 'form1.' + request.ContentFields.Values['ControlActivo'] + '.focus();');
                    end else begin
                        Plantilla := ReplaceTag(Plantilla, 'InitCode', '');
                    end;

                    if Request.QueryFields.values['Inicio'] <> '' then begin
                        CargarDatosDomicilioConvenio(FDM, Plantilla, CodigoConvenio);
                    end else begin
                        CargarDatosDomicilioRequest(FDM, Request, Plantilla);
                    end;
                end else begin
                    if not VerificarDatosDomicilio(FDM, Request, Error) then begin
                        Plantilla := GenerarError(FDM, Request, Error);
                    end else begin
                        if GrabarCambiosDatosDomicilioConvenio(FDM, Request, CodigoSesion, CodigoConvenio, Error) then begin
                            Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                            Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                            Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_DOMICILIO_CAMBIADO);
                        end else begin
                            Plantilla := GenerarError(FDM, Request, Error);
                        end;
                    end;
                end;
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
            end;
        end else begin																		                                                //SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, 0, CodigoConvenio, 4, RUT, 'CambiarDatosDomicilioConvenio - Sesion Expirada');			//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
        Plantilla := ReplaceTag(Plantilla, 'DescConvenio', 'Convenio: ' + ObtenerDescripcionConvenio(FDM, CodigoConvenio) + IIF((CodigoEstadoConvenio = 2), ' (Baja)',''));	// Rev. SS 511-787-864
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'CambiarDatosDomicilio');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmConveniosWEB.CambiarDatosMedioPagoConvenio(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
    Plantilla,
    RUT: string;				//SS_1305_MCA_20150612
    TipoMedioPago,
    CodigoConvenio,
    CodigoSesion,
    CodigoPersona: integer;		//SS_1305_MCA_20150612
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));				//SS_1305_MCA_20150612
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));								//SS_1305_MCA_20150612

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                TipoMedioPago := QueryGetValueInt(fdm.base, 'SELECT dbo.WEB_ObtenerTipoMedioPagoConvenio(' + inttostr(CodigoConvenio) + ')');
                if (TipoMedioPago <> 1) and (TipoMedioPago <> 2) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_CONVENIO_SIN_PAGO_AUTOMATICO);
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                end else begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarDatosMedioPagoConvenio');
                    CargarDatosMedioPagoConvenio(FDM, Plantilla, CodigoConvenio, TipoMedioPago);
                end;
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
            end;
        end else begin
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosMedioPagoConvenio - Sesion Expirada');				//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
        Plantilla := ReplaceTag(Plantilla, 'DescConvenio', ObtenerDescripcionConvenio(FDM, CodigoConvenio));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'CambiarDatosMedioPagoConvenio');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmConveniosWEB.CambiarDatosVehiculosConvenio(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
{    Inicio, Repeticion, Fin,
    temp, sufijo,}
    Error,
    Plantilla,
    RUT: string;					//SS_1305_MCA_20150612
//    i, CantidadVehiculos,
    CodigoConvenio,
    CodigoPersona,
    CodigoSesion: integer;
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));				//SS_1305_MCA_20150612
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));								//SS_1305_MCA_20150612

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            //CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));		//SS_1305_MCA_20150612
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                if Request.ContentFields.values['Finalizar'] = '' then begin
                    //CantidadVehiculos := QueryGetValueInt(fdm.Base, 'WEB_CV_ObtenerCantidadVehiculosConvenio ' + inttostr(CodigoConvenio));

                    Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarDatosVehiculos');
                    // genero las repeticiones necesarias
{                    SepararPartes(Plantilla, '#RepeticionInicio#', '#RepeticionFin#', Inicio, Repeticion, Fin);
                    Plantilla := Inicio;
                    for i:= 1 to CantidadVehiculos do begin
                        temp := repeticion;
                        Sufijo := inttostr(i);
                        Temp := ReplaceTag(temp, 'Nro', Sufijo);
                        ReplaceTag(temp, 'top', intToStr(82 + (i - 1)*112));
                        Plantilla := Plantilla + temp;
                    end;
                    Plantilla := Plantilla + Fin;
}
                    CargarDatosVehiculosConvenio(FDM, Plantilla, CodigoConvenio, CodigoPersona);
                end else begin
                    if not VerificarDatosVehiculos(FDM, Request, Error) then begin
                        Plantilla := GenerarError(FDM, Request, Error);
                    end else begin
                        if GrabarCambiosVehiculos(FDM, Request, CodigoSesion, CodigoConvenio, Error) then begin
                            Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                            Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                            Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_VEHICULOS_CAMBIADOS);
                        end else begin
                            Plantilla := GenerarError(FDM, Request, Error);
                        end;
                     end;
                end;
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
            end;
        end else begin																					//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'CambiarDatosVehiculosConvenio - Sesion Expirada');			//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
        Plantilla := ReplaceTag(Plantilla, 'DescConvenio', ObtenerDescripcionConvenio(FDM, CodigoConvenio));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'CambiarDatosVehiculosConvenio');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmConveniosWEB.logout(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
    Plantilla, RUT: string;									//SS_1305_MCA_20150612
    CodigoSesion, CodigoConvenio, CodigoPersona: integer;	//SS_1305_MCA_20150612
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    if CodigoSesion > 0 then begin																																	//SS_1305_MCA_20150612
        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));		//SS_1305_MCA_20150612
        CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');							//SS_1305_MCA_20150612
        RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));						//SS_1305_MCA_20150612
    end;																																							//SS_1305_MCA_20150612

    try
        // cerramos la sesion
        fdm.Base.Execute(
            'DELETE FROM WEB_CV_Sesiones WHERE CodigoSesion = ' + inttostr(CodigoSesion)
        );
        RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 3, RUT, 'Logout - Fin Sesi�n');			//SS_1305_MCA_20150612
 //       response.SendRedirect(BASE_PATH + NOMBRE_EXE + '/Login');
          response.SendRedirect('/redirhome.htm');
//        Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoLogin');
//        Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, 'Ha sido desconectado con �xito del sistema.');

    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'Logout');
        end;
    end;

    Response.Content := Plantilla;
    Handled := true;
end;
{-----------------------------------------------------------------------------

Revision : 4
    Author :   mpiazza
    Date Created: 10/02/2010
    Language : ES-AR
    Description : SS-864 - se cambia el mensaje
                  MSG_NO_TIENE_PREGUNTA_SECRETA por MSG_OLVIDO_SU_CLAVE

Revision : 5
    Author :   mpiazza
    Date Created: 03/03/2010
    Language : ES-AR
    Description : SS-511 Se incorpora validacion de RecibeClavePorMail
-----------------------------------------------------------------------------}
// INICIO COMENTARIO SS_1248_MCA_20150602

//procedure TwmConveniosWEB.OlvidoPassword(Sender: TObject;
//  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
//resourcestring
//    MSG_NO_RECIBE_CLAVE  = 'Para recuperar la palabra clave, debe comunicarse con el call center.'; // Rev. SS 511-787-864
//var
//    Error,
//    Plantilla: string;
//    RUT, Pregunta, Respuesta,
//    RespuestaCorrecta,
//    Password, RepitaPassword: string;
//    RecibeClavePorMail: boolean ; // Rev. SS 511-787-864
//    CodigoDocumento   : string  ; // Rev. SS 511-787-864
//begin
//    try
//        RUT := Request.ContentFields.Values[TAG_RUT] + Request.ContentFields.Values[TAG_RUTDV];
//        RUT := Padl(Trim(RUT), 9, '0');
//        CodigoDocumento := 'RUT';	// Rev. SS 511-787-864
//        Respuesta := Request.ContentFields.Values['Respuesta'];
//        Pregunta := Request.ContentFields.Values['Pregunta'];	// Rev. SS 511-787-864
//        Password := Request.ContentFields.Values['Password'];
//        RepitaPassword := Request.ContentFields.Values['RepitaPassword'];
//        RecibeClavePorMail := StrToBool(QueryGetValue(fdm.Base, Format('SELECT dbo.ObtenerRecibeClavePorMail(%s, %s)', [quotedstr(RUT) ,quotedstr(CodigoDocumento) ])));	// Rev. SS 511-787-864
//
//        if RecibeClavePorMail  then begin	// Rev. SS 511-787-864
//          if QueryGetValueInt(fdm.Base, 'SELECT COUNT(*) FROM Personas P  WITH (NOLOCK) INNER JOIN Clientes C  WITH (NOLOCK) ON P.CodigoPersona = C.CodigoCliente WHERE NumeroDocumento = ' + quotedstr(RUT)) = 0 then begin
//              Plantilla := GenerarError(FDM, Request, MSG_RUT_INEXISTENTE);
//          end else begin
//              // si la respuesta, y repita password vienen en blanco significa
//              // que recien nos llamaron.
//              if (Respuesta = '') and (RepitaPassword = '') then begin
//                  Password := QueryGetValue(fdm.Base, 'SELECT Password FROM Personas  WITH (NOLOCK) WHERE NumeroDocumento = ' + quotedstr(RUT));
//                  if Password = '' then begin
//                      // no tiene password...
//                      Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
//                      Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_NO_TIENE_CLAVE);
//                      Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
//                      Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
//                      Plantilla := ReplaceTag(Plantilla, TAG_PATENTE, '');                                     //SS_1197_MCA_20140617
//                      Plantilla := ReplaceTag(Plantilla, TAG_PATENTEDV, '');                                   //SS_1197_MCA_20140617
//                      Plantilla := ReplaceTag(Plantilla, TAG_PREGUNTA, '');                                    //SS_1197_MCA_20140617
//                      Plantilla := ReplaceTag(Plantilla, TAG_RESPUESTA, '');                                   //SS_1197_MCA_20140617
//                      Plantilla := ReplaceTag(Plantilla, TAG_EMAIL, '');                                       //SS_1197_MCA_20140617
//                      Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'GenerarClave');
//                      Plantilla := ReplaceTag(Plantilla, 'Reintentos', '0');     							   //SS_1197_MCA_20140617
//                  end else begin
//                      Pregunta := QueryGetValue(fdm.Base, 'SELECT ISNULL(Pregunta,'''') FROM Personas  WITH (NOLOCK) WHERE NumeroDocumento = ' + quotedstr(RUT));	// Rev. SS 511-787-864
//                      Respuesta := QueryGetValue(fdm.Base, 'SELECT ISNULL(Respuesta,'''') FROM Personas  WITH (NOLOCK) WHERE NumeroDocumento = ' + quotedstr(RUT));	// Rev. SS 511-787-864
//
//                      // oh oh, no tiene pregunta ... lo mandamos a la otra pantalla.
//                      if (Pregunta = '') or (Respuesta = '') then begin	// Rev. SS 511-787-864
//                          GenerarClave(Sender, Request, Response, Handled);
//                          // Plantilla := GenerarError(FDM, Request, MSG_OLVIDO_SU_CLAVE);
//                      end else begin
//                          Plantilla := ObtenerPlantilla(FDM, Request, 'OlvidoPassword');
//                          Plantilla := ReplaceTag(Plantilla, 'Pregunta', Pregunta);
//                          Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
//                          Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
//                      end;
//                  end;
//              end else begin
//                  // vemos si la respuesta es correcta
//                  RespuestaCorrecta := QueryGetValue(fdm.Base, 'SELECT Respuesta FROM Personas  WITH (NOLOCK) WHERE NumeroDocumento = ' + quotedstr(RUT));
//
//                  if lowercase(RespuestaCorrecta) = lowercase(Respuesta) then begin
//                      if ChequearNuevoPassword(Password, RepitaPassword, Error) then begin
//                          fdm.Base.Execute(
//                            'UPDATE Personas ' +
//                            'SET Password = ' + quotedstr(Password) + ' ' +
//                            'WHERE NumeroDocumento = ' + quotedstr(RUT)
//                          );
//
//                          // el password fue cambiado con exito!!!
//                          // lo mandamos a la pantalla de login
//                              Plantilla := ObtenerPlantilla(FDM, Request, 'Login');
//                              Plantilla := ReplaceTag(Plantilla, 'ErrorLogin', MSG_PASSWORD_CAMBIADO_E_INGRESE);
//                              Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
//                              Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
//                      end else begin
//                          Plantilla := GenerarError(FDM, Request, Error);
//                      end;
//                  end else begin
//                      Plantilla := GenerarError(FDM, Request, MSG_RESPUESTA_INCORRECTA);
//                  end;
//              end;
//          end;
//        end else begin
//			// Rev. SS 511-787-864
//            Plantilla := ObtenerPlantilla(FDM, Request, 'Mensaje');
//            Plantilla := ReplaceTag(Plantilla , 'Mensaje', MSG_NO_RECIBE_CLAVE );
//			// Fin Rev. SS 511-787-864
//        end;
//    except
//        on e: exception do begin
//            Plantilla := AtenderError(Request, FDM, e, 'OlvidoPassword');
//        end;
//    end;
//
//    if Response.Content = '' then Response.Content := Plantilla;	// Rev. SS 511-787-864
//    Handled := true;
//end;
// FIN COMENTARIO SS_1248_MCA_20150602
procedure TwmConveniosWEB.CambiarPregunta(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
    Plantilla: string;
    CodigoSesion, CodigoPersona: integer;							//SS_1305_MCA_20150612
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));					//SS_1305_MCA_20150612

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if Request.QueryFields.values['Inicio'] = '' then begin
                if CambiarPreguntaSecreta(fdm,
                  CodigoSesion,
                  request.ContentFields.values['Password'],
                  request.ContentFields.values['Pregunta'],
                  request.ContentFields.values['Respuesta'],
                  Error) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_PREGUNTA_CAMBIADA);
                end else begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarPregunta');
                    Plantilla := ReplaceTag(Plantilla, 'ErrorCambio', Error);
                end;
            end else begin
                Plantilla := ObtenerPlantilla(FDM, Request, 'CambiarPregunta');
                Plantilla := ReplaceTag(Plantilla, 'ErrorCambio', '');
            end;
        end else begin																			//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, 0, 4, '', 'Sesion Expirada');				//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'CambiarPregunta');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

{-----------------------------------------------------------------------------
  Function Name: RemoteLogin
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean
  Return Value: N/A

  Revision : 1
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.RemoteLogin(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    RUT,
    Plantilla,
    codigologin: string;
begin
    codigologin := Request.QueryFields.Values['CodigoLogin'];

    try
        RUT := QueryGetValue(fdm.Base, 'SELECT Usuario FROM WEB_CV_LoginRemoto  WITH (NOLOCK) WHERE ID = ' + CodigoLogin);
        if rut <> '' then begin
            Request.ContentFields.Values[TAG_RUT] := copy(rut, 1, length(rut) - 1);
            Request.ContentFields.Values[TAG_RUTDV] := copy(rut, length(rut), 1);
            Request.ContentFields.Values['Password'] := QueryGetValue(fdm.Base, 'SELECT Password FROM WEB_CV_LoginRemoto  WITH (NOLOCK) WHERE ID = ' + CodigoLogin);
        end;

        fdm.Base.Execute(
            'DELETE FROM WEB_CV_LoginRemoto WHERE ID = ' + CodigoLogin
        );

        DoLogin(Sender, Request, Response, Handled);
        exit;
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'CambiarPregunta');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmConveniosWEB.VerUltimaNotaDeCobro(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
    Plantilla,
    RUT: string;				//SS_1305_MCA_20150612
    iCounter:integer;                                                           //SS983-NDR-20111004
    slTiposComprobantesFiscales:TStringList;                                    //SS983-NDR-20111004
    slNrosComprobantesFiscales:TStringList;                                     //SS983-NDR-20111004
    slNrosComprobantes:TStringList;                                             //SS983-NDR-20111004
    slFechasComprobantes:TStringList;                                           //SS983-NDR-20111004
    slMontosComprobantes:TStringList;                                           //SS983-NDR-20111004
    slImprimirComprobantes:TStringList;                                         // SS_1332_CQU_20151106 (1390)
    slDetallesComprobantes:TStringList;                                         // SS_1332_CQU_20151106 (1390)

    CodigoConvenio,
    CodigoSesion, CodigoPersona: integer;										//SS_1305_MCA_20150612
begin
    slTiposComprobantesFiscales:=TStringList.Create;                            //SS983-NDR-20111004
    slNrosComprobantesFiscales:=TStringList.Create;                             //SS983-NDR-20111004
    slNrosComprobantes:=TStringList.Create;                                     //SS983-NDR-20111004
    slFechasComprobantes:=TStringList.Create;                                   //SS983-NDR-20111004
    slMontosComprobantes:=TStringList.Create;                                   //SS983-NDR-20111004
    slImprimirComprobantes:=TStringList.Create;                                 // SS_1332_CQU_20151106 (1390)
    slDetallesComprobantes:=TStringList.Create;                                 // SS_1332_CQU_20151106 (1390)

    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));					//SS_1305_MCA_20150612
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + IntToStr(CodigoPersona));			//SS_1305_MCA_20150612

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                if not FacturacionHabilitada(fdm) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_SECCION_DISPONIBLE_LUEGO_DE_INAUGURACION);
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                    Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
                    Response.Content := Plantilla;
                    Handled := true;
                    exit;
                end;

                    Plantilla := ObtenerPlantilla(FDM, Request, 'PreDescargarNC');
                    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                    Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 5, RUT, 'VerUltimaNotaDeCobro - Listado de Comprobantes a Imprimir');						//SS_1305_MCA_20150612
                if AveriguarUltimasFacturas(    FDM,                                                                                                                           //SS983-NDR-20111004
                                                CodigoConvenio,                                                                                                                //SS983-NDR-20111004
                                                slTiposComprobantesFiscales,                                                                                                   //SS983-NDR-20111004
                                                slNrosComprobantesFiscales,                                                                                                    //SS983-NDR-20111004
                                                slNrosComprobantes,                                                                                                            //SS983-NDR-20111004
                                                slFechasComprobantes,                                                                                                          //SS983-NDR-20111004
                                                slMontosComprobantes,                                                                                                          //SS983-NDR-20111004
                                                slImprimirComprobantes,         // SS_1332_CQU_20151106 (1390)
                                                slDetallesComprobantes,         // SS_1332_CQU_20151106 (1390)
                                                Error) then begin                                                                                                              //SS983-NDR-20111004
                   Plantilla := ReplaceTag(Plantilla, 'TipoComprobante', 'NK');                                                                                                //SS983-NDR-20111004

                   for iCounter := 1 to slNrosComprobantesFiscales.Count do                                                                                                    //SS983-NDR-20111004
                   begin                                                                                                                                                       //SS983-NDR-20111004
                     Plantilla := ReplaceTag(Plantilla, 'TipoComprobanteFiscal'+Trim(IntToStr(iCounter)), slTiposComprobantesFiscales[iCounter-1]);                            //SS983-NDR-20111004
                     Plantilla := ReplaceTag(Plantilla, 'NumeroComprobanteFiscal'+Trim(IntToStr(iCounter)), slNrosComprobantesFiscales[iCounter-1]);                           //SS983-NDR-20111004
                     Plantilla := ReplaceTag(Plantilla, 'FechaComprobante'+Trim(IntToStr(iCounter)), slFechasComprobantes[iCounter-1]);                                        //SS983-NDR-20111004
                     Plantilla := ReplaceTag(Plantilla, 'TotalComprobante'+Trim(IntToStr(iCounter)), slMontosComprobantes[iCounter-1]);                                        //SS983-NDR-20111004
                     Plantilla := ReplaceTag(Plantilla, 'NumeroComprobante'+Trim(IntToStr(iCounter)), slNrosComprobantes[iCounter-1]);                                         //SS983-NDR-20111004
                     if slImprimirComprobantes[iCounter-1] = '0' then                                                                                                          	// SS_1332_CQU_20151106 (1390)
                          Plantilla := ReplaceText(Plantilla, '<a href="javascript:VerComprobante('+Trim(IntToStr(iCounter))+');">Imprimir</a>', 'No Disponible');             	// SS_1332_CQU_20151106 (1390)
                      if (slImprimirComprobantes[iCounter-1] = '0') or (slDetallesComprobantes[iCounter-1] = '0') then begin                                                   	// SS_1332_CQU_20151106 (1390)
                          Plantilla := ReplaceText(Plantilla, '<a href="javascript:VerDetallePDF('+Trim(IntToStr(iCounter))+')">PDF</a>&nbsp;/&nbsp;'                          	// SS_1332_CQU_20151106 (1390)
                                        + '<a href="javascript:VerDetalleCSV('+Trim(IntToStr(iCounter))+')">Excel</a>', 'No Disponible');                                      	// SS_1332_CQU_20151106 (1390)
                      end;																																						// SS_1332_CQU_20151106 (1390)
                   end;                                                                                                                                                        //SS983-NDR-20111004

                   for iCounter := slNrosComprobantesFiscales.Count+1 to 3 do                                                                                                       //SS983-NDR-20111004
                   begin                                                                                                                                                            //SS983-NDR-20111004
                     Plantilla := ReplaceTag(Plantilla, 'TipoComprobanteFiscal'+Trim(IntToStr(iCounter)), '');                                                                      //SS983-NDR-20111004
                     Plantilla := ReplaceTag(Plantilla, 'NumeroComprobanteFiscal'+Trim(IntToStr(iCounter)), '');                                                                    //SS983-NDR-20111004
                     Plantilla := ReplaceTag(Plantilla, 'FechaComprobante'+Trim(IntToStr(iCounter)), '');                                                                           //SS983-NDR-20111004
                     Plantilla := ReplaceTag(Plantilla, 'TotalComprobante'+Trim(IntToStr(iCounter)), '');                                                                           //SS983-NDR-20111004
                     Plantilla := ReplaceTag(Plantilla, 'NumeroComprobante'+Trim(IntToStr(iCounter)), '');                                                                          //SS983-NDR-20111004
                     //Plantilla := ReplaceText(Plantilla , '<a href="javascript:VerComprobante('+Trim(IntToStr(iCounter))+');"><img src="Imagenes/bot_imprimir.gif" ></a>' , '' ); // SS_1332_CQU_20151106 (1390)  //SS983-NDR-20111004
                     Plantilla := ReplaceText(Plantilla , '<a href="javascript:VerComprobante('+Trim(IntToStr(iCounter))+');">Imprimir</a>' , '' );                                 // SS_1332_CQU_20151106 (1390)
                     Plantilla := ReplaceText(Plantilla, '<a href="javascript:VerDetallePDF('+Trim(IntToStr(iCounter))+')">PDF</a>&nbsp;/&nbsp;'                           			// SS_1332_CQU_20151106 (1390)
                                        + '<a href="javascript:VerDetalleCSV('+Trim(IntToStr(iCounter))+')">Excel</a>', '');                                         				// SS_1332_CQU_20151106 (1390)
                   end;                                                                                                                                                             //SS983-NDR-20111004        

                    end else begin
                        Plantilla := GenerarError(FDM, Request, Error);												//SS_1305_MCA_20150612
                        RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 5, RUT, 'VerUltimaNotaDeCobro - ' + Error);		//SS_1305_MCA_20150612
                    end;
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
            end;
        end else begin																				//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'VerUltimaNotaDeCobro - Sesion Expirada');		//SS_1305_MCA_20150612
        end;
    except
        on e: exception do begin
            FreeAndNil(slTiposComprobantesFiscales);                                   //SS983-NDR-20111004
            FreeAndNil(slNrosComprobantesFiscales);                                    //SS983-NDR-20111004
            FreeAndNil(slNrosComprobantes);                                            //SS983-NDR-20111004
            FreeAndNil(slFechasComprobantes);                                          //SS983-NDR-20111004
            FreeAndNil(slMontosComprobantes);                                          //SS983-NDR-20111004
            FreeAndNil(slImprimirComprobantes);                                       //SS_1332_CQU_20151106 (1390)
            FreeAndNil(slDetallesComprobantes);                                       //SS_1332_CQU_20151106 (1390)
            Plantilla := AtenderError(Request, FDM, e, 'VerUltimaFactura');     //SS983-NDR-20111004
        end;
    end;
    FreeAndNil(slTiposComprobantesFiscales);                                   //SS983-NDR-20111004
    FreeAndNil(slNrosComprobantesFiscales);                                    //SS983-NDR-20111004
    FreeAndNil(slNrosComprobantes);                                            //SS983-NDR-20111004
    FreeAndNil(slFechasComprobantes);                                          //SS983-NDR-20111004
    FreeAndNil(slMontosComprobantes);                                          //SS983-NDR-20111004
    FreeAndNil(slImprimirComprobantes);                                       //SS_1332_CQU_20151106 (1390)
    FreeAndNil(slDetallesComprobantes);                                       //SS_1332_CQU_20151106 (1390)
    Response.Content := Plantilla;                                              //SS983-NDR-20111004
    Handled := true;
end;

procedure TwmConveniosWEB.VerConsumos(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
    Plantilla: string;
    IndiceVehiculo,
    CodigoConvenio,
    CodigoSesion,
    //Rev.6 / 07-Julio-2010 / Nelson Droguett Sierra --------------------------------
    Concesionaria, CodigoPersona: integer;					//SS_1305_MCA_20150612
    RUT: string;											//SS_1305_MCA_20150612
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    IndiceVehiculo := StrToIntDef(request.ContentFields.Values['IndiceVehiculo'], -1);
    //Rev.6 / 07-Julio-2010 / Nelson Droguett Sierra --------------------------------
    Concesionaria := StrToIntDef(request.ContentFields.Values['Concesionaria'], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));				//SS_1305_MCA_20150612
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + IntToStr(CodigoPersona));								//SS_1305_MCA_20150612

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                if not FacturacionHabilitada(fdm) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_SECCION_DISPONIBLE_LUEGO_DE_INAUGURACION);
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                    Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
                    Response.Content := Plantilla;
                    Handled := true;
                    exit;
                end;

                Plantilla := ObtenerPlantilla(FDM, Request, 'UltimosConsumos');
                CargarDatosUltimosMovimientosVehiculo(FDM, Request, Plantilla, CodigoConvenio, IndiceVehiculo, Concesionaria);
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'VerConsumos - UltimosConsumos');		//SS_1305_MCA_20150612
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'VerConsumos - ' + Error);		//SS_1305_MCA_20150612
            end;
        end else begin																				//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'VerConsumos - Sesion Expirada');		//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
    except
        on e: exception do begin
            if e is EOleException then
                Plantilla := GenerarMensaje(FDM, Request, MSG_ERROR_TIME_OUT_CN)
            else
                Plantilla := AtenderError(Request, FDM, e, 'VerConsumos');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

{----------------------------------------------------------------------------
//Revision 1
//Lgisuk
//14/11/06
//Ahora se obtiene de un campo la cantidad de registros a obtener
//este valor es definido por el usuario.
Revision : 2
    Author : lcanteros
    Date : 18/06/2008
    Description : (SS 675) ENvio de periodos iniciales de las NK para que
    cuando el operador seleccione una NK del combo setee por default las fechas inicial
    y final de la NK seleccionada en las fechas de filtro
-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.Cartola(Sender: TObject; Request: TWebRequest;Response: TWebResponse; var Handled: Boolean);
var
    Error,
    Plantilla: string;
    CodigoPersona,
    CodigoConvenio,
    CodigoSesion: integer;
    TabActual,
    Comprobante,
    IndiceVehiculo,
    //Rev.6 / 07-Julio-2010 / Nelson Droguett Sierra---------------------------------
    Concesionaria,
    CantidadRegistros, NumerosNK, PeriodosIniNK, PeriodosFinNK: string;
    d, m, a: integer;
    FechaDesde, FechaHasta: tdatetime;
    RecargarTransacciones: boolean;
    UltimaNK: TDatosUltimaNK;
    Tipo_Informe_Transitos : Integer; // Rev 8.  SS_917
    RUT: string;						//SS_1305_MCA_20150612

begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);

    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));				//SS_1305_MCA_20150612
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + IntToStr(CodigoPersona));								//SS_1305_MCA_20150612

    TabActual := request.ContentFields.Values['TabActual'];
    Tipo_Informe_Transitos:=  StrToIntdef(request.ContentFields.Values['TipoDetalleTransito'],1);  // Rev 8.  SS_917
    if TabActual = '' then TabActual := 'Comprobantes';
    RecargarTransacciones := Request.ContentFields.Values['RecargaTransacciones'] = 'true';

    UltimaNK.Numero := '';
    UltimaNK.PeriodoInicial := NullDate;
    UltimaNK.PeriodoFinal := NullDate;

    try
        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                if not FacturacionHabilitada(fdm) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_SECCION_DISPONIBLE_LUEGO_DE_INAUGURACION);
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                    Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
                    Response.Content := Plantilla;
                    Handled := true;
                    exit;
                end;

                if TabActual = 'Transacciones' then begin
                    // Rev 8.  SS_917
                    if Tipo_Informe_Transitos = 1 then begin
                        Plantilla := ObtenerPlantilla(FDM, Request, 'Cartola_Transacciones');
                    end else begin
                        Plantilla := ObtenerPlantilla(FDM, Request, 'Cartola_Transacciones_2');
                    end;
                    // END Rev 8.  SS_917
                    
                    IndiceVehiculo := request.ContentFields.Values['Vehiculo'];
                    //Rev.6 / 07-Julio-2010 / Nelson Droguett Sierra---------------------------------
                    Concesionaria := request.ContentFields.Values['Concesionaria'];
                    Plantilla := ReplaceTag(Plantilla, 'ValoresConcesionaria', GenerarComboConcesionarias(fdm, Concesionaria, True, 1));    // SS_1006_1015_PDO_20121112
                    //FinRev.6-----------------------------------------------------------------------
                    Plantilla := ReplaceTag(Plantilla, 'ValoresVehiculo', GenerarComboVehiculosConvenio(fdm, CodigoConvenio, IndiceVehiculo));



                    Comprobante := request.ContentFields.Values['Comprobante'];
                    Plantilla := ReplaceTag(Plantilla, 'ValoresComprobante', GenerarComboNotasCobro(fdm, CodigoConvenio, Comprobante, RecargarTransacciones, UltimaNK, NumerosNK, PeriodosIniNK, PeriodosFinNK));
                    // envio los datos de periodos iniciales y finales de las NK
                    Plantilla := ReplaceTag(Plantilla, 'NumerosNK', NumerosNK);
                    Plantilla := ReplaceTag(Plantilla, 'PeriodosInicialesNK', PeriodosIniNK);
                    Plantilla := ReplaceTag(Plantilla, 'PeriodosFinalesNK', PeriodosFinNK);

                    d := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 1, '/'), 0);
                    m := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 2, '/'), 0);
                    a := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 3, '/'), 0);

                    if not TryEncodedate(a, m, d, FechaDesde) or not RecargarTransacciones then begin
                        Comprobante := UltimaNK.Numero;
                        if Comprobante <> EmptyStr then
                            FechaDesde := UltimaNK.PeriodoInicial
                        else
                            FechaDesde := IncDay(FDM.NowBase(), DIAS_DEFAULT_CONSULTA_TRANSACCIONES);  // Rev 8.  SS_917
                    end;

                    d := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 1, '/'), 0);
                    m := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 2, '/'), 0);
                    a := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 3, '/'), 0);
                    // si la fecha estaba cargada, por las dudas la recodifico
                    if not TryEncodedate(a, m, d, FechaHasta) or not RecargarTransacciones then begin
//                    if not TryEncodedate(a, m, d, FechaHasta) or not RecargarTransacciones or (StrToIntDef(Comprobante,0) <= 0) then begin
                        if Comprobante <> EmptyStr then
                            FechaHasta := UltimaNK.PeriodoFinal
                        else
                            FechaHasta := FDM.NowBase();  // Rev 8.  SS_917
                    end;

                    //Obtengo la cantidad de registros
                    CantidadRegistros := request.ContentFields.Values['ECantidadRegistros']; //Agregado en revision 1
                    //si es la primera vez
                    try
                       StrToInt(CantidadRegistros);
                    except
                       //establezco valor default desde el parametro general CANTIDAD_REGISTROS_TRANSITOS_WEB
                       CantidadRegistros := IntToStr(ObtenerValorParametroInt(fdm, 'CANTIDAD_REGISTROS_TRANSITOS_WEB', 5000));// Rev 8.  SS_917
                    end;
                    Plantilla := ReplaceTag(Plantilla, 'ECantidadRegistros', CantidadRegistros);
                    //Rev.6 / 07-Julio-2010 / Nelson Droguett Sierra---------------------------------
                    CargarDatosCartolaTransacciones(FDM, Plantilla, CodigoConvenio, strtointdef(IndiceVehiculo, -1), strtointdef(Comprobante, 0), strtointdef(CantidadRegistros, 5000), strtointdef(Concesionaria, -1), FechaDesde, FechaHasta, PlantillacsvTransacciones);  // Rev 8.  SS_917
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'Cartola - Tab Transacciones');				//SS_1305_MCA_20150612
                end else if TabActual = 'Pagos' then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'Cartola_Pagos');
                    CargarDatosCartolaPagos(FDM, Plantilla, CodigoConvenio, strtointdef(IndiceVehiculo, -1), strtointdef(Comprobante, 0), FechaDesde, FechaHasta);
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'Cartola - Tab Pagos');						//SS_1305_MCA_20150612
                end else if TabActual = 'Comprobantes' then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'Cartola_Comprobantes');
                    CargarDatosCartolaComprobantes(FDM, Plantilla, CodigoConvenio, strtointdef(IndiceVehiculo, -1), strtointdef(Comprobante, 0), FechaDesde, FechaHasta);
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'Cartola - Tab Comprobantes');				//SS_1305_MCA_20150612
                //Rev.6 / 07-Julio-2010 / Nelson Droguett Sierra---------------------------------
                end else if TabActual = 'Estacionamientos' then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'Cartola_Estacionamientos');
                    IndiceVehiculo := request.ContentFields.Values['Vehiculo'];
                    Concesionaria := request.ContentFields.Values['Concesionaria'];
                    Plantilla := ReplaceTag(Plantilla, 'ValoresVehiculo', GenerarComboVehiculosConvenio(fdm, CodigoConvenio, IndiceVehiculo));
                    Plantilla := ReplaceTag(Plantilla, 'ValoresConcesionaria', GenerarComboConcesionarias(fdm, Concesionaria, True, 2));

                    d := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 1, '/'), 0);
                    m := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 2, '/'), 0);
                    a := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 3, '/'), 0);

                    if not TryEncodedate(a, m, d, FechaDesde) or not RecargarTransacciones then begin
                        Comprobante := UltimaNK.Numero;
                        if Comprobante <> EmptyStr then
                            FechaDesde := UltimaNK.PeriodoInicial
                        else
                            //FechaDesde := IncDay(Now, DIAS_DEFAULT_CONSULTA_TRANSACCIONES);                          //SS_1397_MGO_20151021
                            FechaDesde := IncDay(FDM.NowBase(), DIAS_DEFAULT_CONSULTA_TRANSACCIONES);                  //SS_1397_MGO_20151021
                    end;

                    d := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 1, '/'), 0);
                    m := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 2, '/'), 0);
                    a := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 3, '/'), 0);
                    // si la fecha estaba cargada, por las dudas la recodifico
                    if not TryEncodedate(a, m, d, FechaHasta) or not RecargarTransacciones or (StrToIntDef(Comprobante,0) <= 0) then begin
                        if Comprobante <> EmptyStr then
                            FechaHasta := UltimaNK.PeriodoFinal
                        else                                                                                           
                            //FechaHasta := now;                                                                       //SS_1397_MGO_20151021
                            FechaHasta := FDM.NowBase();                                                               //SS_1397_MGO_20151021
                    end;
                    CargarDatosCartolaEstacionamientos(FDM, Plantilla, CodigoConvenio, strtointdef(Concesionaria, -1), strtointdef(IndiceVehiculo, -1), FechaDesde, FechaHasta);
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'Cartola - Tab Estacionamientos');					//SS_1305_MCA_20150612
                //FinRev.6-----------------------------------------------------------------------
                end;

                CargarDatosCliente(FDM, Plantilla, CodigoPersona);
                CargarDatosDomicilioConvenio(fdm, plantilla, CodigoConvenio);
//              CargarDatosEstadoDeCuenta(fdm, plantilla, CodigoConvenio);
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'Cartola - ' + Error);							//SS_1305_MCA_20150612
            end;
        end else begin																					//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'VerConsumos - Sesion Expirada');			//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
        Plantilla := ReplaceTag(Plantilla, 'DescConvenio', ObtenerDescripcionConvenio(FDM, CodigoConvenio));
        Plantilla := ReplaceTag(Plantilla, 'TabActual', TabActual);
        Plantilla := ReplaceTag(Plantilla, TAG_RUT, ObtenerRUTCrudo(FDM, CodigoPersona));
    except
        on e: exception do begin
            if e is EOleException then
                Plantilla := GenerarMensaje(FDM, Request, MSG_ERROR_TIME_OUT_CN)
            else
                Plantilla := AtenderError(Request, FDM, e, 'VerConsumos');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;


{******************************** Procedure Header *****************************
Procedure Name: EstadoCuenta
Author :
Date Created :
Parameters : Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean
Description :

*******************************************************************************}
procedure TwmConveniosWEB.EstadoCuenta(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
    Plantilla: string;
    CodigoPersona,
    CodigoConvenio,
    CodigoSesion: integer;
    RUT: string;				//SS_1305_MCA_20150612
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));					//SS_1305_MCA_20150612
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + IntToStr(CodigoPersona));									//SS_1305_MCA_20150612

    try
        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                Plantilla := ObtenerPlantilla(FDM, Request, 'ResumenCuenta');
                CargarDatosDomicilioConvenio(fdm, plantilla, CodigoConvenio);
                CargarDatosCliente(FDM, Plantilla, CodigoPersona);
                CargarDatosEstadoDeCuenta(FDM, plantilla, CodigoConvenio);
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'ResumenCuenta - Resumen Cuenta Cliente');				//SS_1305_MCA_20150612
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'ResumenCuenta - ' + Error);								//SS_1305_MCA_20150612
            end;
        end else begin																				//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'ResumenCuenta - Sesion Expirada');		//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
        Plantilla := ReplaceTag(Plantilla, 'DescConvenio', ObtenerDescripcionConvenio(FDM, CodigoConvenio));
        Plantilla := ReplaceTag(Plantilla, TAG_RUT, ObtenerRUTCrudo(FDM, CodigoPersona));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'ResumenCuenta');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmConveniosWEB.VerTarifas(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
    Plantilla: string;
    CodigoSesion, CodigoPersona: integer;						//SS_1305_MCA_20150612
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));				//SS_1305_MCA_20150612
    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            Plantilla := ObtenerPlantilla(FDM, Request, 'ConsultaTarifas');
            CargarDatosTarifas(FDM, Plantilla);
        end else begin																	//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, 0, 4, '', 'Sesion Expirada');		//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'VerTarifas');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmConveniosWEB.VerComprobante(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
resourcestring                                                                  												//SS_1147_MCA_20150528
    MSG_ERROR_CAF	 = 'Estimado Cliente, No se pudo generar su comprobante electr�nico, favor comunicarse con el call center'; //SS_1147_MCA_20150528
    OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS = 'OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS';                                                      //SS_1332_CQU_20151106 (1390)
var
    NroComprobante: int64;
    TipoComprobante,
    Error,
    Plantilla,                                                                  //SS_1332_CQU_20151106 (1390)
    sDirectorioReportes,                                                        //SS_1332_CQU_20151106 (1390)
    TipoDetalle: string;                                                        //SS_1332_CQU_20151106 (1390)
    CodigoSesion: integer;
    CodigoConvenio, CodigoPersona: Integer;					//SS_1305_MCA_20150612
    esZip: Boolean;
	RUT : string;											//SS_1305_MCA_20150612
    sRespuestaWS : AnsiString;													// SS_1332_CQU_20151106
    wsOficinaVirtual : IOficinaVirtual;                                         // SS_1332_CQU_20151106
    RutaWebService : string;                                                    // SS_1332_CQU_20151106
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    TipoComprobante := request.ContentFields.Values['TipoComprobante'];
    NroComprobante := StrToIntDef(request.ContentFields.Values['NumeroComprobante'], -1);
    TipoDetalle := UpperCase(Request.ContentFields.Values['TipoDetalle']);                 		//SS_1332_CQU_20151106 (1390)
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);				//SS_1305_MCA_20150612
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));		//SS_1305_MCA_20150612
	RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + IntToStr(CodigoPersona));						//SS_1305_MCA_20150612
    ObtenerParametroGeneral(FDM.Base, 'OFICINA_VIRTUAL_URL_UBICACION', RutaWebService);                     // SS_1332_CQU_20151106
    try
        if TipoComprobante = '' then TipoComprobante := 'NK';                                                                       //SS_1147_MCA_20150528
        if NroComprobante = -1 then raise(Exception.Create('Faltan Par�metros (NumeroComprobante)'));                               //SS_1147_MCA_20150528

        if (TipoDetalle <> '') and (TipoDetalle <> 'PDF') and (TipoDetalle <> 'CSV') then                                           //SS_1332_CQU_20151106 (1390)
            raise(Exception.Create('Par�metro Inv�lido (TipoDetalle)'));                                                            //SS_1332_CQU_20151106 (1390)

        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            try                                                                                                                             // SS_1332_CQU_20151106
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 5, RUT, 'Llamando a GetIOficinaVirtual');            // SS_1332_CQU_20151106
                wsOficinaVirtual := GetIOficinaVirtual(False, RutaWebService);                                                              // SS_1332_CQU_20151106
                if TipoDetalle <> '' then                                                                           						//SS_1332_CQU_20151106 (1390)
                begin                                                                                               						//SS_1332_CQU_20151106 (1390)
                    sRespuestaWS := wsOficinaVirtual.GenerarDetalleComprobante( IntToStr(CodigoSesion),             						//SS_1332_CQU_20151106 (1390)
                                                                                TipoComprobante,                    						//SS_1332_CQU_20151106 (1390)
                                                                                NroComprobante,                     						//SS_1332_CQU_20151106 (1390)
                                                                                TipoDetalle)                       							//SS_1332_CQU_20151106 (1390)
                end                                                                                                 						//SS_1332_CQU_20151106 (1390)
                else                                                                                                						//SS_1332_CQU_20151106 (1390)
                begin                                                                                               						//SS_1332_CQU_20151106 (1390)
                    sRespuestaWS := wsOficinaVirtual.GenerarComprobante(  IntToStr(CodigoSesion),                   						//SS_1332_CQU_20151106 (1390)
                                                                          TipoComprobante,                          						//SS_1332_CQU_20151106 (1390)
                                                                          NroComprobante);                          						//SS_1332_CQU_20151106 (1390)
                end;                                                                                                						//SS_1332_CQU_20151106 (1390)

                ObtenerParametroGeneral(FDM.Base, OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS, sDirectorioReportes);                                   // SS_1332_CQU_20151106 (1390)
                if (Pos('TImpresionDocumentos', sRespuestaWS) > 0) or                                                                       // SS_1332_CQU_20151106
                    (Pos('No se pudo obtener el comprobante', sRespuestaWS) > 0)                                                            // SS_1332_CQU_20151106
                then begin                                                                                                                  // SS_1332_CQU_20151106 (1390)
                    Plantilla := GenerarError(FDM, Request, MSG_ERROR_CAF);                                                                 // SS_1332_CQU_20151106
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 6, RUT, 'GetIOficinaVirtual: ' + sRespuestaWS);  // SS_1332_CQU_20151106
                    with FDM.spWEB_AlertaCaf do begin                                                                                       // SS_1332_CQU_20151106
                        Parameters.ParamByName('@NumeroComprobante').Value :=  NroComprobante;                                              // SS_1332_CQU_20151106
                        Parameters.ParamByName('@TipoComprobante').Value:= TipoComprobante;                                                 // SS_1332_CQU_20151106
                        Parameters.ParamByName('@MensajeError').Value:=sRespuestaWS;                                                        // SS_1332_CQU_20151106
                        ExecProc;                                                                                                           // SS_1332_CQU_20151106
                    end;                                                                                                                    // SS_1332_CQU_20151106
                end else begin                                                                                                              // SS_1332_CQU_20151106
                    Plantilla := ObtenerPlantilla(FDM,Request,'DescargarArchivo');                                                          // SS_1332_CQU_20151106
                    Plantilla := ReplaceTag(Plantilla, 'Archivo', '../Comprobantes/' + sRespuestaWS);                                       // SS_1332_CQU_20151106
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 5, RUT,                                          // SS_1332_CQU_20151106
                                        'GetIOficinaVirtual: Descarga existosa del reporte');                                               // SS_1332_CQU_20151106 (1390)
                end;                                                                                                                        // SS_1332_CQU_20151106
                //FreeAndNil(wsOficinaVirtual);                                                                                             // SS_1332_CQU_20151116  // SS_1332_CQU_20151106
            except                                                                                                                          // SS_1332_CQU_20151106
                on e:exception do begin                                                                                                     // SS_1332_CQU_20151106
                    Plantilla := GenerarError(FDM, Request, 'Error en GetIOficinaVirtual');                                                 // SS_1332_CQU_20151116
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 6, RUT,                                          // SS_1332_CQU_20151106
                                        'Error en GetIOficinaVirtual, Mensaje:' + e.Message);                                               // SS_1332_CQU_20151106
                end;                                                                                                                        // SS_1332_CQU_20151106
            end;                                                                                                                            // SS_1332_CQU_20151106
        end else begin																				//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 4, RUT, 'VerComprobante - Sesion Expirada');		//SS_1305_MCA_20150612
        end;
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'VerComprobante');
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 12, RUT, 'VerComprobante - Se ha producido un error VerComprobante: ' + e.Message);			//SS_1305_MCA_20150612
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

{********************************
* BEGIN : SS_1397_MGO_20151106  *
*********************************}
procedure TwmConveniosWEB.VerNoFacturados(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
resourcestring
    MSG_ERROR = 'Estimado Cliente, No se pudo generar el reporte, favor comunicarse con el call center';
    OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS = 'OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS';
var
    Error,
    sDirectorioReportes,
    Plantilla: string;
    IndiceVehiculo,
    CodigoConvenio,
    CodigoPersona,
    CodigoSesion: integer;
    RUT: string;
    sRespuestaWS : AnsiString;
    wsOficinaVirtual : IOficinaVirtual;
    RutaWebService : string;
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    IndiceVehiculo := StrToIntDef(request.ContentFields.Values['IndiceVehiculo'], -1);
    ObtenerParametroGeneral(FDM.Base, 'OFICINA_VIRTUAL_URL_UBICACION', RutaWebService);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));		//SS_1305_MCA_20150612
	  RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + IntToStr(CodigoPersona));
    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            try
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 5, RUT, 'Llamando a GetIOficinaVirtual');
                wsOficinaVirtual := GetIOficinaVirtual(False, RutaWebService);

                sRespuestaWS := wsOficinaVirtual.GenerarTransitosNoFacturados(IntToStr(CodigoSesion), CodigoConvenio, IndiceVehiculo);

                ObtenerParametroGeneral(FDM.Base, OFICINA_VIRTUAL_DIR_TMP_ARCHIVOS, sDirectorioReportes);                                   //SS_1390_MGO_20150928
                //if Pos('TImpresionDocumentos', sRespuestaWS) > 0 then begin                                                               //SS_1390_MGO_20150928// SS_1332_CQU_20150721
                if (Pos('TImpresionDocumentos', sRespuestaWS) > 0)                                                                          //SS_1390_MGO_20150928
                 // or not FileExists(GoodDir(sDirectorioReportes) + sRespuestaWS)
                 then begin                                                 //SS_1390_MGO_20150928           // SS_1332_CQU_20150721
                    Plantilla := GenerarError(FDM, Request, MSG_ERROR);                                                                 // SS_1332_CQU_20150721
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 6, RUT, 'GetIOficinaVirtual: ' + sRespuestaWS);  // SS_1332_CQU_20150721
                end else begin                                                                                                              // SS_1332_CQU_20150721
                    Plantilla := ObtenerPlantilla(FDM,Request,'DescargarArchivo');                                                          // SS_1332_CQU_20150721
                    Plantilla := ReplaceTag(Plantilla, 'Archivo', '../Comprobantes/' + sRespuestaWS);                                       // SS_1332_CQU_20150721
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 5, RUT,                                          // SS_1332_CQU_20150721
                                        'GetIOficinaVirtual: Descarga existosa del reporte');                                               // SS_1390_MGO_20150928
                end;
                //FreeAndNil(wsOficinaVirtual);                                                                                             // SS_1332_CQU_20151229
            except
                on e:exception do begin                                                                                                     
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 6, RUT,                                          
                                        'Error en GetIOficinaVirtual, Mensaje:' + e.Message);                                               
                end;
            end;
        end;
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'VerNoFacturados');
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 12, RUT, 'VerNoFacturados - Se ha producido un error: ' + e.Message);
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;
{********************************
* END   : SS_1397_MGO_20151106  *
*********************************}

procedure TwmConveniosWEB.VerReclamos(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Error,
    Plantilla: string;
    CodigoConvenio,
    CodigoSesion, CodigoPersona: integer;					//SS_1305_MCA_20150612
    RUT: string;											//SS_1305_MCA_20150612
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));			//SS_1305_MCA_20150612
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + IntToStr(CodigoPersona));							//SS_1305_MCA_20150612
    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                Plantilla := ObtenerPlantilla(FDM, Request, 'VerReclamos');
                CargarDatosReclamos(FDM, Plantilla, CodigoConvenio);
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'VerReclamos - Cargar Reclamos');		//SS_1305_MCA_20150612
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
            end;
        end else begin																				//SS_1305_MCA_20150612
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 1, RUT, 'VerReclamos - Sesion Expirada');		//SS_1305_MCA_20150612
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'VerReclamos');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

{******************************** Procedure Header *****************************
Procedure Name: GenerarClave
Author :
Date Created :
Parameters : Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean
Description :


Revision : 2
    Author : mpiazza
    Date : 10/09/2009
    Description : SS-511 WebCaptcha  se genera la palabra y se la guarda en
                  una SesionCaptcha

Revision : 3
    Author : mpiazza
    Date : 19/10/2009
    Description : SS-787 se agrega la validacion RecibeClavePorMail con el cual
                  si no genera clave genera un mensaje

Revision : 4
    Author : mpiazza
    Date : 25/02/2010
    Description : SS-511 WebCaptcha  agrega la recepcion del request de los
    campos:
            Patente
            DigitoVerificadorPatente
            email
Revision : 5
    Author : mpiazza
    Date : 30/03/2010
    Description : SS-511 WebCaptcha  validacion para  los campos:
                  Patente
                  DigitoVerificadorPatente
                  Email
                  ConfirmacionEmail
                  Pregunta
                  Respuesta

Revision :6
    Author : mpiazza
    Date : 03/05/2010
    Description : SS-511 WebCaptcha
              _ Agrega constante CANT_CARACTERES_CAPTCHA
              _ Se agrega contador de reintentos (3) para el DV

*******************************************************************************}
procedure TwmConveniosWEB.GenerarClave(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
const
    CANT_CARACTERES_CAPTCHA   = 4;
resourcestring
    //MSG_NO_RECIBE_CLAVE  = 'Para recuperar la palabra clave, debe comunicarse con el call center.';                                   //SS_1304_MCA_20150709
    MSG_NO_RECIBE_CLAVE = 'Estimado Cliente, ud. no puede recibir su clave por E-mail, favor comun�quese con nuestro Call Center.';     //SS_1304_MCA_20150709
var
    Password,
    CodigoDocumento,
    RUT, RUTDV, RUTCOMPLETO,
    Plantilla: string;
    CaptCha           : TCaptCha;
    PalabraAleatoria  : string;
    IdSesionCaptcha   : integer;
    RecibeClavePorMail: boolean;

    Email  : string;
    ConfirmacionEmail  : string;
    NuevoPass  : string;
    Patente  : string;
    DigitoVerificadorPatente: string;

    //Pregunta    : string    ;                                                 //SS_1248_MCA_20150602
    //Respuesta   : string    ;                                                 //SS_1248_MCA_20150602
    TelefonoCelular: string;                                                    //SS_1248_MCA_20150602
    TelefonoParticular: string;                                                 //SS_1248_MCA_20150602
    Reintentos  : integer   ;
begin
    try
        CaptCha   := TCaptCha.create;
        Plantilla := ObtenerPlantilla(FDM, Request, 'GenerarClave');
        CodigoDocumento := 'RUT';
        RUT := Request.ContentFields.Values[TAG_RUT];
        RUT := Padl(Trim(RUT), 8, '0');
        RUTDV := Request.ContentFields.Values[TAG_RUTDV];
        RUTCOMPLETO := RUT + RUTDV;
		// Rev. SS 511-787-864                                                                      //        if Request.ContentFields.IndexOf('Patente') < 0 then
        if Not (Request.MethodType = mtPost) then begin
            Patente := '';
            DigitoVerificadorPatente := '';
            Email :=  '';
            ConfirmacionEmail := '';
            //Pregunta := '';																	//SS_1248_MCA_20150602
            //Respuesta := '';																	//SS_1248_MCA_20150602
            TelefonoCelular := '';                                                             //SS_1248_MCA_20150602
            TelefonoParticular := '';                                                          //SS_1248_MCA_20150602
        end else begin
            Patente := Request.ContentFields.Values['Patente'] ;
            DigitoVerificadorPatente := Request.ContentFields.Values['DigitoVerificadorPatente'];
            Email := Request.ContentFields.Values['Email'];
            ConfirmacionEmail := Request.ContentFields.Values['ConfirmacionEmail'];
            TelefonoCelular := Request.ContentFields.Values['telefonocelular'];                 //SS_1248_MCA_20150602
            TelefonoParticular := Request.ContentFields.Values['telefonoparticular'];           //SS_1248_MCA_20150602
            //Pregunta := Request.ContentFields.Values['Pregunta'] ; //ss-511					//SS_1248_MCA_20150602
            //Respuesta := Request.ContentFields.Values['Respuesta'] ; //ss-511					//SS_1248_MCA_20150602
            if Request.ContentFields.Values['Reintentos']  = '' then begin
              Reintentos := 0;
            end else begin
              Reintentos := strtoint(Request.ContentFields.Values['Reintentos']) ; //ss-511
            end;


        end;
{
        if Request.ContentFields.IndexOf('DigitoVerificadorPatente') < 0 then
          DigitoVerificadorPatente := ''
        else
          DigitoVerificadorPatente := Request.ContentFields.Values['DigitoVerificadorPatente'];

        if Request.ContentFields.IndexOf('Email') < 0 then
          Email :=  ''
        else
          Email := Request.ContentFields.Values['Email'];

        if Request.ContentFields.IndexOf('ConfirmacionEmail') < 0 then
          ConfirmacionEmail := ''
        else
          ConfirmacionEmail := Request.ContentFields.Values['ConfirmacionEmail'];

        if Request.ContentFields.IndexOf('Pregunta') < 0 then
          Pregunta := ''
        else
          Pregunta := Request.ContentFields.Values['Pregunta'] ; //ss-511

        if Request.ContentFields.IndexOf('Respuesta') < 0 then
          Respuesta := ''
        else
          Respuesta := Request.ContentFields.Values['Respuesta'] ; //ss-511
}
		// Fin Rev. SS 511-787-864
        Password := QueryGetValue(fdm.Base, 'SELECT Password FROM Personas  WITH (NOLOCK) WHERE NumeroDocumento = ' + quotedstr(RUT + RUTDV));
        RecibeClavePorMail := StrToBool(QueryGetValue(fdm.Base, Format('SELECT dbo.ObtenerRecibeClavePorMail(%s, %s)', [quotedstr(RUTCOMPLETO) ,quotedstr(CodigoDocumento) ])));

        if RecibeClavePorMail  then begin
	        if QueryGetValueInt(fdm.Base, 'SELECT count(*) FROM Personas  WITH (NOLOCK) WHERE NumeroDocumento = ' + quotedstr(RUT + RUTDV)) = 0 then begin
	            Plantilla := GenerarError(fdm, Request, MSG_NO_EXISTE_RUT_TITULAR);
                RegistrarBitacoraUso(FDM, 0, 0, 0, 12, RUTCOMPLETO, MSG_NO_EXISTE_RUT_TITULAR);				//SS_1305_MCA_20150612
	        // Revision 1: Si eligi� "Olvid� Clave" se debe presentar la pantalla de Generar Clave
	        // end else if Password <> '' then begin
	        //    Plantilla := GenerarError(fdm, Request, MSG_YA_POSEE_CLAVE);

	        end else begin
	            Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
	            Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
                Plantilla := ReplaceTag(Plantilla, 'Patente', Patente);										//SS_1248_MCA_20150602
                Plantilla := ReplaceTag(Plantilla, 'DigitoVerificadorPatente', DigitoVerificadorPatente);	//SS_1248_MCA_20150602
                Plantilla := ReplaceTag(Plantilla, 'email', email);											//SS_1248_MCA_20150602
                //Plantilla := ReplaceTag(Plantilla, 'Pregunta', Pregunta);                                	//SS_1248_MCA_20150602
                //Plantilla := ReplaceTag(Plantilla, 'Respuesta', Respuesta);                              	//SS_1248_MCA_20150602
                Plantilla := ReplaceTag(Plantilla, 'telefonocelular', TelefonoCelular);                    	//SS_1248_MCA_20150602
                Plantilla := ReplaceTag(Plantilla, 'telefonoparticular', TelefonoParticular);              	//SS_1248_MCA_20150602
                Plantilla := ReplaceTag(Plantilla, 'Reintentos', inttostr(Reintentos));						//SS_1248_MCA_20150602

                PalabraAleatoria := CaptCha.PalabraAleatoria(CANT_CARACTERES_CAPTCHA);
                IdSesionCaptcha := GenerarSesionCaptcha(FDM, Request.ContentFields.Values[TAG_RUT], Request.RemoteAddr, PalabraAleatoria);
                Plantilla := ReplaceTag(Plantilla, TAG_CAPTCHA, inttostr(IdSesionCaptcha) );

	            //Plantilla := ReplaceTag(Plantilla, 'ValoresComuna', GenerarComboTodasLasComunas(fdm));
                RegistrarBitacoraUso(FDM, 0, 0, 0, 12, RUTCOMPLETO, 'Se ha solicitado la generacion de la clave secreta');				//SS_1305_MCA_20150612
	        end;
        end else begin
            Plantilla := ObtenerPlantilla(FDM, Request, 'Mensaje');
            Plantilla := ReplaceTag(Plantilla , 'Mensaje', MSG_NO_RECIBE_CLAVE );
            RegistrarBitacoraUso(FDM, 0, 0, 0, 12, RUTCOMPLETO, MSG_NO_RECIBE_CLAVE);				//SS_1305_MCA_20150612
        end;
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'GenerarClave');
            RegistrarBitacoraUso(FDM, 0, 0, 0, 12, RUTCOMPLETO, 'Se ha producido un error GenerarClave : ' + e.Message);				//SS_1305_MCA_20150612
        end;
    end;

    Response.Content := Plantilla;
    Handled := true;
end;


{******************************** Procedure Header *****************************
Procedure Name: DoGenerarClave
Author :
Date Created :
Parameters : Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean
Description :



Revision : 2
    Author : mpiazza
    Date :
    Description :   SS-511 WebCaptcha  se incorpora la validacion de cantidad
                    de reintentos por error del DV llevando a una pagina de aviso
Revision : 3
    Author : mpiazza
    Date :  07_05_2010
    Description : SS-511 El servicio de mail se le agrega el subject
                  TITULO_EMAIL_GENERACION_CLAVE

*******************************************************************************}
procedure TwmConveniosWEB.DoGenerarClave(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Plantilla: string;
    CodigoPersona: integer;
    Error,
    Email, ConfirmacionEmail,
    NuevoPass,
    RUT, Patente, DigitoVerificadorPatente: string;
    CodigoMensaje: Int64;
    CadenaCaptcha : string  ;
    //Pregunta    : string    ;                                                 //SS_1248_MCA_20150602
    //Respuesta   : string    ;                                                 //SS_1248_MCA_20150602
    TelefonoCelular, TelefonoParticular: string;                                //SS_1248_MCA_20150602
    //MSG         : TIdMessage;         SS_1070_MCA_20121005
    captchavalue: string    ;
    Reintentos  : integer   ;
    CodigoConvenio: Integer ;                                                   //SS_1070_MCA_20121005
    CodigoFirma : Integer   ;                                                   //SS_1070_MCA_20121005
    CodigoPlantilla : Integer ;                                                 //SS_1070_MCA_20121005
begin
    try

        CadenaCaptcha := ObtenerSesionCaptcha(FDM, Request.RemoteAddr );

        //SE OBTIENEN LOS CODIGOS DE PLANTILLA Y FIRMA PARA EL ENVIO DEL EMAIL CON LA NUEVA CLAVE
        ObtenerParametroGeneral(FDM.Base, 'COD_PLANTILLA_ENVIO_PASS_WEB', CodigoPlantilla);  //SS_1070_MCA_20121005
        ObtenerParametroGeneral(FDM.Base, 'COD_FIRMA_ENVIO_PASS_WEB', CodigoFirma);  //SS_1070_MCA_20121005
        //modificacion ss-511
        RUT := Request.ContentFields.Values[TAG_RUT] + Request.ContentFields.Values[TAG_RUTDV];
        RUT := Padl(Trim(RUT), 9, '0');
//        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE NumeroDocumento = ''' + RUT + '''');
        Patente := Request.ContentFields.Values['Patente'];
        DigitoVerificadorPatente := Request.ContentFields.Values['DigitoVerificadorPatente'];
        Email := Request.ContentFields.Values['Email'];
        ConfirmacionEmail := Request.ContentFields.Values['ConfirmacionEmail'];
        captchavalue := UpperCase(Request.ContentFields.Values['captchavalue']);
        //Pregunta := Request.ContentFields.Values['Pregunta'] ; //ss-511        //SS_1248_MCA_20150602
        //Respuesta := Request.ContentFields.Values['Respuesta'] ; //ss-511      //SS_1248_MCA_20150602
        TelefonoCelular := Request.ContentFields.Values['TelefonoCelular'];      //SS_1248_MCA_20150602
        TelefonoParticular := Request.ContentFields.Values['TelefonoParticular'];//SS_1248_MCA_20150602

        if Request.ContentFields.Values['Reintentos']  = '' then begin
          Reintentos := 0;
        end else begin
          Reintentos := strtoint(Request.ContentFields.Values['Reintentos']) ; //ss-511
        end;

        //modificacion ss-511

        if not ( UpperCase(CadenaCaptcha) = captchavalue) then begin
            //Plantilla := GenerarError(FDM, Request, MSG_CAPTCHA_VALIDACION );

            Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
            Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_CAPTCHA_VALIDACION);

            Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
            Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
            Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'GenerarClave');

            Plantilla := ReplaceTag(Plantilla, 'Patente', Patente);
            Plantilla := ReplaceTag(Plantilla, 'DigitoVerificadorPatente', DigitoVerificadorPatente);
            Plantilla := ReplaceTag(Plantilla, 'email', email);
            //Plantilla := ReplaceTag(Plantilla, 'Pregunta', Pregunta);         								//SS_1248_MCA_20150602
            //Plantilla := ReplaceTag(Plantilla, 'Respuesta', Respuesta);       								//SS_1248_MCA_20150602
            Plantilla := ReplaceTag(Plantilla, 'TelefonoCelular', TelefonoCelular);								//SS_1248_MCA_20150602
            Plantilla := ReplaceTag(Plantilla, 'TelefonoParticular', TelefonoParticular);						//SS_1248_MCA_20150602

            Plantilla := ReplaceTag(Plantilla, 'Reintentos', inttostr(Reintentos));
            RegistrarBitacoraUso(FDM, 0, 0, 0, 12, RUT, MSG_CAPTCHA_VALIDACION);								//SS_1305_MCA_20150612
            Response.Content := Plantilla;
            Handled := True;
            exit;

        end;

        // Revision 1: solo se verifican los siguientes datos.
        if (Request.ContentFields.Values[TAG_RUT] = '') or
          (Request.ContentFields.Values[TAG_RUTDV] = '') or
          (Request.ContentFields.Values['Email'] = '') or
          (Request.ContentFields.Values['Patente'] = '') or
          (Request.ContentFields.Values['DigitoVerificadorPatente'] = '') or
          ((Request.ContentFields.Values['TelefonoCelular'] = '') and           //SS_1248_MCA_20150602
          (Request.ContentFields.Values['TelefonoParticular'] = '')) then begin //SS_1248_MCA_20150602
         // (Request.ContentFields.Values['Pregunta'] = '') or                  //SS_1248_MCA_20150602 //ss-511
         // (Request.ContentFields.Values['Respuesta'] = '') then begin         //SS_1248_MCA_20150602 //ss-511

            Plantilla := GenerarError(FDM, Request, MSG_COMPLETE_LOS_DATOS);
            RegistrarBitacoraUso(FDM, 0, 0, 0, 12, RUT, MSG_COMPLETE_LOS_DATOS);								//SS_1305_MCA_20150612
            Response.Content := Plantilla;
            Handled := true;
            exit;
        end else begin
        {
            RUT := Request.ContentFields.Values[TAG_RUT] + Request.ContentFields.Values[TAG_RUTDV];
            RUT := Padl(Trim(RUT), 9, '0');
            Patente := Request.ContentFields.Values['Patente'];
            DigitoVerificadorPatente := Request.ContentFields.Values['DigitoVerificadorPatente'];
            Email := Request.ContentFields.Values['Email'];
            ConfirmacionEmail := Request.ContentFields.Values['ConfirmacionEmail'];
         }
            if not IsValidEMail(Email) then begin
                Plantilla := GenerarError(FDM, Request, MSG_MAIL_INCORRECTO);
                RegistrarBitacoraUso(FDM, 0, 0, 0, 12, RUT, MSG_MAIL_INCORRECTO);									//SS_1305_MCA_20150612

                Response.Content := Plantilla;
                Handled := true;
                exit;
            end;

            if (ConfirmacionEmail <> '') and (not IsValidEMail(ConfirmacionEmail)) then begin
                Plantilla := GenerarError(FDM, Request, MSG_MAIL_INCORRECTO);
                RegistrarBitacoraUso(FDM, 0, 0, 0, 12, RUT, MSG_MAIL_INCORRECTO);									//SS_1305_MCA_20150612
                Response.Content := Plantilla;
                Handled := true;
                exit;
            end;

            if ValidarDatosOlvidados(FDM, RUT, Patente, DigitoVerificadorPatente, CodigoPersona, Error) then begin
                if not VerificarMailRegistrado(fdm, CodigoPersona, Email) then begin
                    // el mail no coincide con el registrado... a ver...
                    if ConfirmacionEmail = '' then begin
                        // bueno, lo mandamos a que reconfirme el email...
                        Plantilla := ObtenerPlantilla(FDM, Request, 'ConfirmarEmail');
//                        Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, 'La operaci�n no puede ser realizada. <br><cr>El motivo informado por el sistema es: ' + Error);
                        Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'DoGenerarClave');

                        Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
                        Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
                        Plantilla := ReplaceTag(Plantilla, 'Patente', Patente);
                        Plantilla := ReplaceTag(Plantilla, 'DigitoVerificadorPatente', DigitoVerificadorPatente);
                        Plantilla := ReplaceTag(Plantilla, 'email', email);
                        Plantilla := ReplaceTag(Plantilla, 'captchavalue', captchavalue);
                        //Plantilla := ReplaceTag(Plantilla, 'Pregunta', Pregunta);                     //SS_1248_MCA_20150602
                        //Plantilla := ReplaceTag(Plantilla, 'Respuesta', Respuesta);                   //SS_1248_MCA_20150602
                        Plantilla := ReplaceTag(Plantilla, 'TelefonoCelular', TelefonoCelular);         //SS_1248_MCA_20150602
                        Plantilla := ReplaceTag(Plantilla, 'TelefonoParticular', TelefonoParticular);   //SS_1248_MCA_20150602
                        Plantilla := ReplaceTag(Plantilla, 'Reintentos', inttostr(Reintentos));
                        RegistrarBitacoraUso(FDM, 0, CodigoPersona, 0, 12, RUT, 'Solicitud de Confirmaci�n Email');		//SS_1305_MCA_20150612
                        Response.Content := Plantilla;
                        Handled := true;
                        exit;
                    end else if uppercase(trim(ConfirmacionEmail)) <> uppercase(trim(Email)) then begin
                        // no coincide la confirmacion
                        // si el nuevo mail confirmado es el que figura en la base,
                        // quiere decir que el tipo se acordo de con cual mail lo habia
                        // registrado, asi que todo bien, seguimos.
                        // Sino, lo mandamos al Call Center.
                        if not VerificarMailRegistrado(fdm, CodigoPersona, ConfirmacionEmail) then begin
                            Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoLogin');
                            Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_ERROR_CONFIRMACION_EMAIL);
                            RegistrarBitacoraUso(FDM, 0, CodigoPersona, 0, 12, RUT, MSG_ERROR_CONFIRMACION_EMAIL);			//SS_1305_MCA_20150612
                            Response.Content := Plantilla;
                            Handled := true;
                            exit;
                        end else begin
                            // se acordo del mail que figura en su cuenta
                            // lo cargamos aqui, asi se le envia alli
                            email := ConfirmacionEmail;
                            // sigo
                        end;
                    end else begin
                        // bien, quiere cambiarlo al email. Lo hacemos.
                        ActualizarMailPersona(fdm, CodigoPersona, Email);
                        // sigo
                    end;
                end else begin
                    // me fijo que tenga grabado el mail
                    ConfirmacionEmail := QueryGetValue(fdm.Base, 'SELECT dbo.ObtenerEmailPersona(' + inttostr(CodigoPersona) + ')');
                    if ConfirmacionEmail = '' then begin
                        // no tiene mail, lo mandamos a grabar
                        ActualizarMailPersona(fdm, CodigoPersona, Email);
                    end;
                end;

                NuevoPass := inttostr(Random(999999) + 100000);                 //SS_1248_MCA_20150602

                // Ejecuto el SP a mano, seteando el pass.
                fdm.base.execute('WEB_CV_ResetearPasswordPersona @NumeroDocumento=' + quotedstr(RUT) + ', @NuevoPasssword=' + quotedstr(NuevoPass)); //+ ', @Pregunta=' + quotedstr(Pregunta) + ', @Respuesta=' + quotedstr(Respuesta) );			//SS_1248_MCA_20150602
                // Actualizo algun el medio de contacto si es que lo informa
                if (TelefonoCelular <> '') then begin																															//SS_1248_MCA_20150602
                    fdm.base.execute('WEB_CV_ActualizarMediosComunicacion ' + IntToStr(CodigoPersona) + ', 5, 9, ' + quotedstr(TelefonoCelular) +', NULL, NULL, NULL, NULL, NULL');	//SS_1248_MCA_20150602
                end;																																						//SS_1248_MCA_20150602
                if (TelefonoParticular <> '') then begin																														//SS_1248_MCA_20150602
                    fdm.base.execute('WEB_CV_ActualizarMediosComunicacion ' + IntToStr(CodigoPersona) + ', 3, 2, ' + quotedstr(TelefonoParticular) +', NULL, NULL, NULL, NULL, NULL');	//SS_1248_MCA_20150602
                end;																																						//SS_1248_MCA_20150602

                try
                    // Revision 1: Se registra en la base de datos el env�o de email al generar la clave
                   // AgregarEmailEnvioClave (fdm, CodigoMensaje, Email, TITULO_EMAIL_GENERACION_CLAVE, CodigoPersona, 'WEB'); --SS_1070_MCA_20121005
                    //ss-511 Se cambia el valor por defecto en una constante

                    //GENERACION_CLAVE_SMTP_SERVER := ObtenerValor(fdm, 'GENERACION_CLAVE_SMTP_SERVER', GENERACION_CLAVE_SMTP_SERVER);
                    //GENERACION_CLAVE_SMTP_FROM := ObtenerValor(fdm, 'GENERACION_CLAVE_SMTP_FROM', GENERACION_CLAVE_SMTP_FROM);

                  CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');    //SS_1070_MCA_20121005
                  AgregarEmailMensaje(FDM, Email, CodigoPlantilla, CodigoFirma, CodigoConvenio, NuevoPass);                                             //SS_1070_MCA_20121005


{Smtp.Host := GENERACION_CLAVE_SMTP_SERVER;    //SS_1070_MCA_20121005
Smtp.Port := 25;                               //SS_1070_MCA_20121005
Smtp.Username := GENERACION_CLAVE_SMTP_USER;   //SS_1070_MCA_20121005
Smtp.Password := GENERACION_CLAVE_SMTP_PASS;   //SS_1070_MCA_20121005
Smtp.AuthType:= atDefault;                     //SS_1070_MCA_20121005
MSG         := TIdMessage.Create(nil);          //SS_1070_MCA_20121005
MSG.Subject := TITULO_EMAIL_GENERACION_CLAVE;  //SS_1070_MCA_20121005
MSG.Body.Add('Estimado Cliente: ' + #13#10 +    //SS_1070_MCA_20121005
                      #13#10 +                  //SS_1070_MCA_20121005
                      'Se ha creado una clave temporal para usted.' + #13#10 +    //SS_1070_MCA_20121005
                      'Su clave temporal es: ' + NuevoPass + #13#10 +              //SS_1070_MCA_20121005
                      #13#10 +                                                       //SS_1070_MCA_20121005
                      'Por favor, ingrese a http://www.costaneranorte.cl/' + #13#10 + //SS_1070_MCA_20121005
                      'y cambie su clave por una nueva creada por usted.' + #13#10 +   //SS_1070_MCA_20121005
                      #13#10 +                                                             //SS_1070_MCA_20121005
                      'Este mensaje se ha generado autom�ticamente. Por favor no lo responda.' + #13#10 +   //SS_1070_MCA_20121005
                      #13#10 +                       //SS_1070_MCA_20121005
                      'Costanera Norte' + #13#10 +       //SS_1070_MCA_20121005
                      'http://www.costaneranorte.cl/'); //SS_1070_MCA_20121005
MSG.From.text := GENERACION_CLAVE_SMTP_FROM;   //SS_1070_MCA_20121005

MSG.Recipients.EMailAddresses  := Email;      //SS_1070_MCA_20121005
Smtp.Connect();                             //SS_1070_MCA_20121005
Smtp.Send(MSG);                              //SS_1070_MCA_20121005
Smtp.Disconnect();    }                      //SS_1070_MCA_20121005
                                             {
                    Smtp.QuickSend(GENERACION_CLAVE_SMTP_SERVER, TITULO_EMAIL_GENERACION_CLAVE, Email, GENERACION_CLAVE_SMTP_FROM,
                      'Estimado Cliente: ' + #13#10 +
                      #13#10 +
                      'Se ha creado una clave temporal para usted.' + #13#10 +
                      'Su clave temporal es: ' + NuevoPass + #13#10 +
                      #13#10 +
                      'Por favor, ingrese a http://www.costaneranorte.cl/' + #13#10 +
                      'y cambie su clave por una nueva creada por usted.' + #13#10 +
                      #13#10 +
                      'Este mensaje se ha generado autom�ticamente. Por favor no lo responda.' + #13#10 +
                      #13#10 +
                      'Costanera Norte' + #13#10 +
                      'http://www.costaneranorte.cl/'
                      );
                                           }
//MSG.free;                             //SS_1070_MCA_20121005
                    // Revision 1: Se marca como enviado el registro del envio del email
                   // ConfirmarEmailEnvioClave (fdm, CodigoMensaje);                  --SS_1070_MCA_20121005
                except
                    on e: exception do begin															//SS_1305_MCA_20150612
                        raise exception.create('SMTP: ' + e.Message);									//SS_1305_MCA_20150612
                        RegistrarBitacoraUso(FDM, 0, 0, 0, 12, RUT, e.Message);	                        //SS_1305_MCA_20150612
                    end;																				//SS_1305_MCA_20150612

                end;

                Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoLogin');
                Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_EMAIL_CON_CLAVE_ENVIADO);
                RegistrarBitacoraUso(FDM, 0, CodigoPersona, CodigoConvenio, 12, RUT, MSG_EMAIL_CON_CLAVE_ENVIADO);			//SS_1305_MCA_20150612
            end else begin
                {
                Aqui se evalua si el DV es el dato invalido, siendo asi se incrementa
                el contador de reintentos, si la cantidad es mayor a 3 lo envio a home
                }
                inc(Reintentos);
                if Reintentos > 2 then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'Reintentos');
                end else begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, Error);

                    Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
                    Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'GenerarClave');

                    Plantilla := ReplaceTag(Plantilla, 'Patente', Patente);
                    Plantilla := ReplaceTag(Plantilla, 'DigitoVerificadorPatente', DigitoVerificadorPatente);
                    Plantilla := ReplaceTag(Plantilla, 'email', email);
                    //Plantilla := ReplaceTag(Plantilla, 'Pregunta', Pregunta);                     //SS_1248_MCA_20150602
                    //Plantilla := ReplaceTag(Plantilla, 'Respuesta', Respuesta);                   //SS_1248_MCA_20150602
                    Plantilla := ReplaceTag(Plantilla, 'TelefonoCelular', TelefonoCelular);         //SS_1248_MCA_20150602
                    Plantilla := ReplaceTag(Plantilla, 'TelefonoParticular', TelefonoParticular);   //SS_1248_MCA_20150602
                    Plantilla := ReplaceTag(Plantilla, 'Reintentos', inttostr(Reintentos));
                    RegistrarBitacoraUso(FDM, 0, 0, 0, 12, RUT, Error);			//SS_1305_MCA_20150612
                    Response.Content := Plantilla;
                    Handled := True;
                    exit;
                    //Plantilla := GenerarError(FDM, Request, Error  );
                end;

            end;
        end;
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'DoGenerarClave');
            RegistrarBitacoraUso(FDM, 0, 0, 0, 12, RUT, 'DoGenerarClave - se ha producido un error DoGenerarClave: ' + e.Message);		//SS_1305_MCA_20150612
        end;
    end;

    Response.Content := Plantilla;
    Handled := true;
end;

{******************************** Function Header ******************************
Function Name: SaldoxConveniosxRUT
Author : nefernandez
Date Created : 22/05/2008
Description : SS 698: Genera la p�gina de Saldos de Convenios por RUT.
*******************************************************************************}
procedure TwmConveniosWEB.SaldoxConveniosxRUT(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
resourcestring
    MSG_ERROR_NO_CONVENIOS  = 'El RUT ingresado no tiene convenios asociados.';
    MSG_ERROR_TIME_OUT      = 'La consulta para el RUT ingresado excede el tiempo contemplado para este tipo de consultas.';
    MSG_ERROR_EXCEPCION     = 'La consulta seleccionada no se encuentra disponible. Por favor intenter m�s tarde.';
const
    TAG_NOMBRE_PERSONA = 'NombrePersona';
var
    RUT, RUTDV,
    Plantilla: string;
    CodigoPersona: Integer;
begin
    try
        Plantilla := ObtenerPlantilla(FDM, Request, 'SaldoxConveniosxRUT');

        RUT := Request.ContentFields.Values[TAG_RUT];
        RUT := Padl(Trim(RUT), 8, '0');
        RUTDV := Request.ContentFields.Values[TAG_RUTDV];

        CodigoPersona := QueryGetValueInt(fdm.Base, 'SELECT CodigoPersona FROM Personas  WITH (NOLOCK) WHERE NumeroDocumento = ' + quotedstr(RUT + RUTDV));

        if (CodigoPersona = 0) then begin
            //Plantilla := GenerarError(fdm, Request, MSG_NO_EXISTE_RUT_TITULAR);
            Plantilla := GenerarMensaje(FDM, Request, MSG_NO_EXISTE_RUT_TITULAR)
        end else begin
            Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
            Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
            Plantilla := ReplaceTag(Plantilla, TAG_NOMBRE_PERSONA, QueryGetValue(fdm.Base, 'EXEC WEB_CV_ObtenerNombrePersona @CodigoPersona = ' + inttostr(CodigoPersona)));

            if not CargarSaldosConvenios(FDM, Plantilla, CodigoPersona) then begin
                Plantilla := GenerarError(FDM, Request, MSG_ERROR_NO_CONVENIOS);
            end;

        end;
    except
        on e: exception do begin
            if e is EOleException then
                Plantilla := GenerarMensaje(FDM, Request, MSG_ERROR_TIME_OUT)
            else
                Plantilla := GenerarMensaje(FDM, Request, MSG_ERROR_EXCEPCION)
                //Plantilla := AtenderError(Request, FDM, e, 'VerConsumos');
        end;
    end;

    Response.Content := Plantilla;
    Handled := true;
end;

{-----------------------------------------------------------------------------
  Function Name: BlanquearPass
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject;
              Request: TWebRequest;
              Response: TWebResponse;
              var Handled: Boolean
  Return Value: N/A

  Revision : 1
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.BlanquearPass(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Plantilla: string;
    CodigoSesion: integer;
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            fdm.Base.Execute(
                'update personas ' +
                'set password = NULL, ' +
            	'DebeCambiarPassword = 0 ' +
                'where CodigoPersona = (SELECT CodigoPersona from web_CV_sesiones  WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion) + ')'
            );

            Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
            Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, 'Su password ha sido blanqueada. Para reingresar presione "Continuar".');
            Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'Login');
            Plantilla := ReplaceTag(Plantilla, TAG_RUT, '');
            Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, '');
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'BlanquearPass');
        end;
    end;
    Response.Content := Plantilla;
end;

procedure TwmConveniosWEB.MediosEnvio(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Email,
    Error,
    Plantilla: string;
//    DetalleFacturacion,
    CodigoPersona,
    CodigoConvenio,
    CodigoSesion: integer;
    EstadoActual: integer;
    MedioEnvioNK,
    MedioEnvioDetalle: string;
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                Plantilla := ObtenerPlantilla(FDM, Request, 'EnvioDocumentos');

                CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));
                Email := QueryGetValue(fdm.Base, 'SELECT dbo.ObtenerEmailPersona(' + inttostr(CodigoPersona) + ')');
                Plantilla := ReplaceTag(Plantilla, 'EmailContacto', Email);
{
                Plantilla := ReplaceTag(Plantilla, 'EstadoNotaCobroEmail',
                  iif(QueryGetValue(fdm.base, 'SELECT dbo.WEB_ObtenerTipoMedioEnvioDocumento(' + inttostr(CodigoConvenio) + ', ' + CODIGO_ENVIO_NOTA_COBRO + ')') = CODIGO_ENVIO_CORREO_EMAIL, 'checked', ''));

                Plantilla := ReplaceTag(Plantilla, 'EstadoResumenTrimestralMail',
                  iif(QueryGetValue(fdm.base, 'SELECT dbo.WEB_ObtenerTipoMedioEnvioDocumento(' + inttostr(CodigoConvenio) + ', ' + CODIGO_ENVIO_INFORME_TRIMESTRAL + ')') = CODIGO_ENVIO_CORREO_EMAIL, 'checked', ''));

                DetalleFacturacion := strtointdef(QueryGetValue(fdm.base, 'SELECT dbo.WEB_ObtenerTipoMedioEnvioDocumento(' + inttostr(CodigoConvenio) + ', ' + CODIGO_ENVIO_DETALLE_TRANSITOS + ')'), 0);

                Plantilla := ReplaceTag(Plantilla, 'DetalleFacturacion0Checked', iif(DetalleFacturacion = 0, 'checked', ''));
                Plantilla := ReplaceTag(Plantilla, 'DetalleFacturacion1Checked', iif(DetalleFacturacion = 1, 'checked', ''));
                Plantilla := ReplaceTag(Plantilla, 'DetalleFacturacion2Checked', iif(DetalleFacturacion = 2, 'checked', ''));
}
                MedioEnvioNK := QueryGetValue(fdm.base, 'SELECT dbo.WEB_ObtenerTipoMedioEnvioDocumento(' + inttostr(CodigoConvenio) + ', ' + CODIGO_ENVIO_NOTA_COBRO + ')');
                MedioEnvioDetalle := QueryGetValue(fdm.base, 'SELECT dbo.WEB_ObtenerTipoMedioEnvioDocumento(' + inttostr(CodigoConvenio) + ', ' + CODIGO_ENVIO_DETALLE_TRANSITOS + ')');

                if PuedePedirDetalleImpreso(FDM, CodigoConvenio) then begin
                    Plantilla := ReplaceTag(Plantilla, 'MSG_PROHIBIDO', '');
                    Plantilla := ReplaceTag(Plantilla, 'EstiloNA', 'style="display: none"');
                    Plantilla := ReplaceTag(Plantilla, 'EstiloCheck', '');
                end else begin
                    Plantilla := ReplaceTag(Plantilla, 'MSG_PROHIBIDO', format(MSG_DEMASIADOS_VEHICULOS, [ObtenerValorParametroInt(fdm, 'TOPE_VEHICULOS_FACT_DETALLADA', 99)]));
                    Plantilla := ReplaceTag(Plantilla, 'EstiloCheck', 'style="display: none"');
                    Plantilla := ReplaceTag(Plantilla, 'EstiloNA', '');
                end;

                if MedioEnvioNK = CODIGO_ENVIO_CORREO_EMAIL then begin
                    if MedioEnvioDETALLE = CODIGO_ENVIO_CORREO_EMAIL then begin
                        EstadoActual := 1;
                    end else begin
                        EstadoActual := 2;
                    end;
                end else begin
                    if MedioEnvioDETALLE = CODIGO_ENVIO_CORREO_EMAIL then begin
                        EstadoActual := 3;
                    end else begin
                        if MedioEnvioDETALLE = CODIGO_ENVIO_CORREO_POSTAL then begin
                            EstadoActual := 4;
                        end else begin
                            EstadoActual := 0;
                        end;
                    end;
                end;
                Plantilla := ReplaceTag(Plantilla, 'EstadoActual', inttostr(EstadoActual));

                Plantilla := ReplaceTag(Plantilla, 'CostoDetalleTransitos',
                  IntToStr(ObtenerValorParametroInt(FDM, 'PRECIO_IMPR_FACTURACION_DETALLADA', 1000) div 100)
                );
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
            end;
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
        Plantilla := ReplaceTag(Plantilla, 'DescConvenio', ObtenerDescripcionConvenio(FDM, CodigoConvenio));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'MediosEnvio');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmConveniosWEB.GrabarMediosEnvio(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    function ObtenerMes(Fecha: TDateTime): String;
    var
        a, m, d: word;
    begin
        DecodeDate(Fecha, a, m, d);
        case m of
            1: result := 'Enero';
            2: result := 'Febrero';
            3: result := 'Marzo';
            4: result := 'Abril';
            5: result := 'Mayo';
            6: result := 'Junio';
            7: result := 'Julio';
            8: result := 'Agosto';
            9: result := 'Septiembre';
            10: result := 'Octubre';
            11: result := 'Noviembre';
            12: result := 'Diciembre';
        else
            result := 'pr�ximo';
        end;
    end;
var
    Email,
    Error,
    Plantilla: string;
    CodigoPersona,
    CodigoConvenio,
    CodigoSesion: integer;
    EleccionMedioEnvio: integer;
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    Email := request.ContentFields.Values['EmailContacto'];

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin

                CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));

                EleccionMedioEnvio := strtointdef(request.ContentFields.Values['EleccionMedioEnvio'], 0);

                // entre las opciones 1 y 3 el email es obligatorio
                if ((EleccionMedioEnvio > 0) and (EleccionMedioEnvio < 4)) or (Email <> '') then begin
                    if not IsValidEMail(Email) then raise(Exception.Create('El email ingresado no es v�lido.'));
                    ActualizarMailPersona(fdm, CodigoPersona, Email);
                end;

                if GrabarCambiosMediosEnvio(FDM, Request, CodigoConvenio, EleccionMedioEnvio, Error) then begin

                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);

                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                    Plantilla := ReplaceTag(Plantilla, 'Aviso', format(MSG_DATOS_MEDIO_ENVIO_GUARDADOS, [ObtenerDescripcionConvenio(FDM, CodigoConvenio), ObtenerMes(ObtenerFechaProximaFacturacion(FDM, CodigoConvenio))]));
                end else begin
                    Plantilla := GenerarError(FDM, Request, Error);
                end;
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
            end;
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'MediosEnvio');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;


{-----------------------------------------------------------------------------
  Function Name: wmConveniosWEBNotificacionSTDAction
  Author:    lgisuk
  Date Created: 03/08/2006
  Description: Recupera y parsea el xml del pago y lo registra en la
               base de datos
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 23/10/2006
 Description: Intentaba convertir el IDTrx en numero cuando el IDTrx era muy
              grande, lo que provocaba que de aritmetic overflow. Ahora se lee
              el importe que informa el Banco y no se lee de Auditoria.
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 25/10/2006
 Description: Se modificaron las rutinas de auditoria, ahora guardan los datos
              utilizando los tipos de datos adecuados, en caso de no poder
              registrar el mensaje en auditoria no continua con el pago
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 03/11/2006
 Description: Ahora se registra el pago primero y en auditoria despues
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 10/11/2006
 Description: Ahora guardo el mensaje completo recibido del banco en un campo
              de la tabla web_cv_auditoria
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 16/11/2006
 Description: Ahora si no se pudo actualizar el mensaje recibido del banco en
              la tabla de auditoria no se continua con el pago.
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 17/11/2006
 Description: Para registrar el pago se tomara la fecha actual y no la fecha
              informa el banco.
-----------------------------------------------------------------------------
Author: lgisuk
 Date Created: 20/11/2006
 Description: Ahora si no puede actualizar aditoria con la confirmacion y el
              mensaje XML de todos modos intento registrar el pago. y
              si despues de realizado el pago se produce algun error al escribir
              en auditoria los errores son manejados en cada rutina de modo que
              no impida la respuesta al banco. se agrega a los mensajes de error
              informacion acerca del pago que se estaba realizando de modo que
              si sucede un error podamos identificar con mas certeza la
              operacion que fallo y el motivo del fallo.
-----------------------------------------------------------------------------
 Revision 1
Author: lgisuk
 Date Created: 16/07/2007
 Description: Ahora verifico si el banco envio la respuesta en el tiempo
               establecido si no es asi genero una excepcion.
-----------------------------------------------------------------------------
 Revision 2
 Author: lgisuk
 Date Created: 16/07/2007
 Description: Ahora BDP tiene un parametro general independiente para determinar
 si la respuesta del banco llego a tiempo, WEB_CANT_SEG_VALIDEZ_RESPUESTA_BDP.
 ------------------------------------------------------------------------------
 Revision : 3
     Author : vpaszkowicz
     Date : 22/11/2007
     Description : Controlo que si no registr� la actualizaci�n de la respuesta
     del banco, que no siga adelante con el pago.
 ------------------------------------------------------------------------------
 Revision : 4
     Author : nefernandez
     Date : 04/04/2008
     Description : Se pasa la FechaHora del servidor de base de datos para
     registrar la fecha de pago. Anteriormente se le pasaba la FechaHora del
     servidor web.
-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.wmConveniosWEBNotificacionSTDAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
resourcestring
    MSG_ERROR_QUERY     = 'Boton de Pago - Error al obtener el numero de comprobante : ';
    MSG_ERROR_NUMCOMP   = 'Boton de Pago - El numero de comprobante no es numerico : ';
    MSG_ERROR_TOTAL     = 'Boton de Pago - El importe no es numerico : ';
    MSG_ERROR_CODRET    = 'Boton de Pago - El Codigo de Respuesta no es numerico : ';
    MSG_ERROR_BANK      = 'Boton de Pago - Ocurrio un error en el Banco y no hubo Pago : ';
    MSG_ERROR_INTERNAL  = 'Boton de Pago - Error al intentar grabar un pago. El procedimiento interno lo rechazo con el mensaje: ';
    MSG_ERROR_PAYMENT   = 'Boton de Pago - Error al intentar grabar un pago : ';
    MSG_ERROR_ANSWER_BEHIND_SCHEDULE = 'Boton de Pago - La respuesta del banco llego despues de %d segundos ';
    MSG_ERROR_GET_SESSION_CODE = 'Error al obtener el C�digo de Sesi�n Web';		//SS_1304_MCA_20150709
const
    STR_CODIGO_ACEPTADO = '0000';
    STR_NK = 'NK';
    //Respuesta al banco
    STR_ACEPTADO  = '<NOTIFICA>OK</NOTIFICA>';
    STR_RECHAZADO = '<NOTIFICA>NOK</NOTIFICA>';
    //Detalle de la transaccion
    STR_TRANSACCION = 'Transaccion: ';
    STR_COMPROBANTE = 'Comprobante: ';
    STR_IMPORTE = 'Importe: ';
    STR_RESPUESTA = 'Respuesta Banco: ';
    STR_NOTIFICACION_STD = 'NotificacionSTD - ';
var
    IDTRX : String;
    sNumeroComprobante : String;
    sImporte : String;
    sCodigodeRespuesta : String;
    IdentificadorOperacion : String;
    NumeroComprobante : INT64;
    Importe : INT64;
    CodigodeRespuesta : Integer;
    FechaProceso: TDateTime; //Revision 4
    sError : string;
    CodigoSesion: Integer;			//SS_1304_MCA_20150709
begin
    AgregarLog(FDM, 'NotificacionSTD -' + Request.ContentFields.Text);
      try
        //Recupero y parseo el xml

        //Obtengo el identificador de transaccion
        IDTRX := ParseXMLField(Request.ContentFields.Text,'IDTRX');

        try																				//SS_1304_MCA_20150709
            CodigoSesion := QueryGetValueInt(FDM.Base,									//SS_1304_MCA_20150709
                'SELECT CodigoSesionWeb FROM Web_CV_Auditoria WITH (NOLOCK)'			//SS_1304_MCA_20150709
                    + ' WHERE CodigoBanco = ' + IntToStr(CODIGO_BANCO_SANTANDER)		//SS_1304_MCA_20150709
                        + ' AND IDTRX = ' + QuotedStr(IdTrx));							//SS_1304_MCA_20150709
        except																			//SS_1304_MCA_20150709
            on E : Exception do begin													//SS_1304_MCA_20150709
                raise Exception.Create(MSG_ERROR_GET_SESSION_CODE + E.Message);			//SS_1304_MCA_20150709
            end;																		//SS_1304_MCA_20150709
        end;																			//SS_1304_MCA_20150709
        //Obtengo el numero de comprobante que envie a cobrar
        try
            sNumeroComprobante := QueryGetValue(FDM.Base,'SELECT NumeroComprobante FROM Web_CV_Auditoria WITH (NOLOCK) WHERE CodigoBanco = '+ IntToStr(CODIGO_BANCO_SANTANDER) +' and IDTRX = ' + QuotedStr(IdTrx));
        Except
            on E : Exception do begin
                Raise Exception.Create(MSG_ERROR_QUERY + E.Message);
            end;
        end;

        //Obtengo el importe
        sImporte := ParseXMLField(Request.ContentFields.Text,'TOTAL');

        //Obtengo el codigo de respuesta
        sCodigoDeRespuesta := ParseXMLField(Request.ContentFields.Text,'CODRET');

        //Armo una linea con la informacion de la operacion a realizar, asi si sucede un error podremos identificar con mas certeza la operacion que fallo y el motivo del fallo
        IdentificadorOperacion := STR_TRANSACCION + IdTrx + ' ' + STR_COMPROBANTE + sNumeroComprobante + ' '+ STR_IMPORTE + sImporte + ' ' + STR_RESPUESTA +  sCodigodeRespuesta;

        //verifico si la respuesta llego a tiempo
        If QueryGetValueInt(fdm.Base,'SELECT DBO.WEB_RespuestaValida_BDP (' + QuotedStr(IDTRX) + ',' + 'GETDATE()' + ')' ) = 0 then begin
            //creo una excepcion informado que la respuesta llego tarde
            Raise Exception.Create(format(MSG_ERROR_ANSWER_BEHIND_SCHEDULE,[ObtenerValorParametroInt(fdm, 'WEB_CANT_SEG_VALIDEZ_RESPUESTA', 99)]));
        end;

        //Obtengo el numero de comprobante
        try
            NumeroComprobante := StrToINT64(sNumeroComprobante);
        except
            Response.Content := STR_RECHAZADO;
            EventLogReportEvent(elError, MSG_ERROR_NUMCOMP + sNumeroComprobante + ' ' + IdentificadorOperacion, '');
            RegistrarBitacoraUso(FDM, CodigoSesion, 0, 0, 13, '', 'WEBNotificacionSTD - ' + MSG_ERROR_NUMCOMP + sNumeroComprobante + ' ' + IdentificadorOperacion);   //SS_1305_MCA_20150612
            AgregarLog(FDM, STR_NOTIFICACION_STD + MSG_ERROR_NUMCOMP + sNumeroComprobante + ' ' + IdentificadorOperacion);
            Exit;
        end;

        //Actualizo el registro de auditoria
        ActualizarConfirmacionAuditoria(FDM, IDTRX, CODIGO_BANCO_SANTANDER, SCodigoDeRespuesta, IIF(SCodigoDeRespuesta = STR_CODIGO_ACEPTADO, TRUE, FALSE));

        //Guardo el mensaje recibido del banco en auditoria
        //Transformo esto en funci�n. Si sale por false quiere decir que el mensaje est� duplicado, o sea,
        //el banco me est� mandando 2 veces el mismo pago como evento.
        if not ActualizarRespuestaBancoAuditoria(FDM, IDTRX, Copy(Request.ContentFields.Text, 1, 1000), CODIGO_BANCO_SANTANDER) then begin
            Response.Content := STR_ACEPTADO;
            Exit;
        end;

        //Obtengo el importe
        try
            Importe := StrToINT64(sImporte) * 100;
        except
            Response.Content := STR_RECHAZADO;
            EventLogReportEvent(elError, MSG_ERROR_TOTAL + sImporte + ' ' + IdentificadorOperacion, '');
            ActualizarGeneroReciboAuditoria(FDM, IDTRX, FALSE, MSG_ERROR_TOTAL + sImporte + ' ' + IdentificadorOperacion, CODIGO_BANCO_SANTANDER);
            AgregarLog(FDM, STR_NOTIFICACION_STD + MSG_ERROR_TOTAL + sImporte + ' ' + IdentificadorOperacion);
            RegistrarBitacoraUso(FDM, CodigoSesion, 0, 0, 13, '', 'WEBNotificacionSTD - ' + MSG_ERROR_TOTAL + sImporte + ' ' + IdentificadorOperacion);   //SS_1305_MCA_20150612
            Exit;
        end;

        //Obtengo el codigo de respuesta
        try
            CodigoDeRespuesta := StrToINT64(sCodigoDeRespuesta);
        except
            Response.Content := STR_RECHAZADO;
            EventLogReportEvent(elError, MSG_ERROR_CODRET + sCodigoDeRespuesta + ' ' + IdentificadorOperacion, '');
            ActualizarGeneroReciboAuditoria(FDM, IDTRX, FALSE, MSG_ERROR_CODRET + sCodigoDeRespuesta + ' ' + IdentificadorOperacion, CODIGO_BANCO_SANTANDER);
            AgregarLog(FDM, STR_NOTIFICACION_STD + MSG_ERROR_CODRET + sCodigoDeRespuesta + ' ' + IdentificadorOperacion);
            RegistrarBitacoraUso(FDM, CodigoSesion, 0, 0, 13, '', 'WEBNotificacionSTD - ' + MSG_ERROR_BANK + sCodigodeRespuesta + ' ' + IdentificadorOperacion);   //SS_1305_MCA_20150612
            Exit;
        end;

        //Verifico si fue aceptado por el banco
        if sCodigoDeRespuesta <> STR_CODIGO_ACEPTADO then begin
              Response.Content := STR_ACEPTADO; //porque el banco ya me lo envio rechazado
              EventLogReportEvent(elError, MSG_ERROR_BANK + sCodigodeRespuesta + ' ' + IdentificadorOperacion, '');
              ActualizarGeneroReciboAuditoria(FDM, IDTRX, FALSE, MSG_ERROR_BANK + sCodigodeRespuesta + ' ' + IdentificadorOperacion, CODIGO_BANCO_SANTANDER);
              AgregarLog(FDM, STR_NOTIFICACION_STD + MSG_ERROR_BANK + sCodigodeRespuesta + ' ' + IdentificadorOperacion);
              RegistrarBitacoraUso(FDM, CodigoSesion, 0, 0, 13, '', 'WEBNotificacionSTD - ' + MSG_ERROR_BANK + sCodigodeRespuesta + ' ' + IdentificadorOperacion);   //SS_1305_MCA_20150612
              Exit;
        end;

        //Registro en la base de datos
        FechaProceso := QueryGetValueDateTime(fdm.Base, 'SELECT GETDATE()');
        if not RealizarPagoComprobantesSTD(fdm, CodigoSesion, CodigodeRespuesta, FechaProceso, sError) then begin			//SS_1304_MCA_20150709
        //if not RegistrarPagoComprobanteSTD(fdm, CodigodeRespuesta, FechaProceso, sError) then begin						//SS_1304_MCA_20150709
              Response.content := STR_RECHAZADO;
              EventLogReportEvent(elError, MSG_ERROR_INTERNAL + sError + ' ' + IdentificadorOperacion, '');
              ActualizarGeneroReciboAuditoria(FDM, IDTRX, FALSE, MSG_ERROR_INTERNAL + sError + ' ' + IdentificadorOperacion, CODIGO_BANCO_SANTANDER);
              AgregarLog(FDM, STR_NOTIFICACION_STD + MSG_ERROR_INTERNAL + sError + ' ' + IdentificadorOperacion);
              RegistrarBitacoraUso(FDM, CodigoSesion, 0, 0, 13, '', 'WEBNotificacionSTD - ' + MSG_ERROR_INTERNAL + sError + ' ' + IdentificadorOperacion);   //SS_1305_MCA_20150612
              Exit;
        end;

        RegistrarBitacoraUso(FDM, CodigoSesion, 0, 0, 13, '', 'WEBNotificacionSTD - se ha realiza el pago existosamente');                //SS_1305_MCA_20150612
        //El recibo se creo con exito
        ActualizarGeneroReciboAuditoria(FDM, IDTRX, TRUE, '', CODIGO_BANCO_SANTANDER);

        //Registro la notificacion del pago
        AgregarAuditoriaBDP_MPOUT(fdm, Request);

        //Envio notificacion de aceptacion de pago
        Response.Content := STR_ACEPTADO;

    except
        on E : Exception do begin
            Response.Content:= STR_RECHAZADO;
            EventLogReportEvent(elError, MSG_ERROR_PAYMENT + E.Message + ' ' + IdentificadorOperacion, '');
            AgregarLog(FDM, STR_NOTIFICACION_STD + MSG_ERROR_PAYMENT + E.Message + ' ' + IdentificadorOperacion);
        end;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: wmConveniosWEBSalidaSTDAction
  Author:    lgisuk
  Date Created: 03/08/2006
  Description: Muestra un comprobante del pago realizado.
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 23/10/2006
 Description: Intentaba convertir el IDTrx en numero cuando el IDTrx era muy
              grande, lo que provocaba que de aritmetic overflow. Ahora se lee
              el importe que informa el Banco y no se lee de Auditoria.
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 25/10/2006
 Description: Se renombro la tabla RendicionesBDP a WEB_CV_AUDITORIA_BDP

 Revision : 1
     Author : ggomez
     Date : 17/01/2007
     Description :
        - Quit� la obtenci�n del CodigoSesion desde el IDTRX.
        - Agregu� que la obtenci�n del CodigoSesion se realice desde la tabla de
        Auditor�a.
-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.wmConveniosWEBSalidaSTDAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
Resourcestring
    MSG_ERROR_QUERY = 'Boton de Pago - Error al obtener el numero de comprobante : ';
    MSG_ERROR_VALUE_NOT_NUMERIC = 'Boton de Pago - Error el importe es invalido : ';
    MSG_ERROR_BDP = 'Su transaccion no pudo ser realizada';
    MSG_ERROR_GET_SESSION_CODE = 'Error al obtener el C�digo de Sesi�n Web';
    MSG_ERROR_GET_OFICINA_VIRTUAL = 'Error al carga la pagina de Exito';			//SS_1280_MCA_20150812
var
    IDTRX : String;
    NumeroComprobante : string;
    Importe : Integer;
    sCodigodeRespuesta : String;
    CodigoSesion : Integer;
    Plantilla: string;
    EsOficinaVirtual: boolean;					//SS_1280_MCA_20150812
begin
    AgregarLog(FDM, 'SalidaSTD -' + Request.ContentFields.Text);
    //Registro la notificacion del pago
    AgregarAuditoriaBDP_MPFIN(fdm, Request);

    try
        //Obtengo el IDTRX
        IDTRX := ParseXMLField(Request.ContentFields.Text,'IDTRX');

        // Obtener el CodigoSesion desde la tabla de Auditoria.
        try
            CodigoSesion := QueryGetValueInt(FDM.Base,
                'SELECT CodigoSesionWeb FROM Web_CV_Auditoria WITH (NOLOCK)'
                    + ' WHERE CodigoBanco = ' + IntToStr(CODIGO_BANCO_SANTANDER)
                        + ' AND IDTRX = ' + QuotedStr(IdTrx));

        except
            on E : Exception do begin
                raise Exception.Create(MSG_ERROR_GET_SESSION_CODE + E.Message);
            end;
        end;

         try																			//SS_1280_MCA_20150812
           EsOficinaVirtual := (QueryGetValue(FDM.Base,									//SS_1280_MCA_20150812
                'SELECT EsOficinaVirtual '+												//SS_1280_MCA_20150812
                'FROM WEB_CV_Sesiones WITH (NOLOCK) ' +									//SS_1280_MCA_20150812
                'WHERE CodigoSesion = ' + IntToStr(CodigoSesion))='1');					//SS_1280_MCA_20150812
        except																			//SS_1280_MCA_20150812
            on E : Exception do begin													//SS_1280_MCA_20150812
                raise Exception.Create(MSG_ERROR_GET_OFICINA_VIRTUAL + E.Message);		//SS_1280_MCA_20150812
            end;																		//SS_1280_MCA_20150812
        end;																			//SS_1280_MCA_20150812

        //Obtengo el numero de comprobante que envie a cobrar
        try
            NumeroComprobante := QueryGetValue(FDM.Base, 'SELECT NumeroComprobante FROM Web_CV_Auditoria WITH (NOLOCK) WHERE CodigoBanco = '+ IntToStr(CODIGO_BANCO_SANTANDER) +' and IDTRX = ' + QuotedStr(IdTrx));
        Except
            on E : Exception do begin
                Raise Exception.Create(MSG_ERROR_QUERY + E.Message);
            end;
        end;

        //Obtengo el importe
        try
            Importe := StrToInt(ParseXMLField(Request.ContentFields.Text,'TOTAL'));
        Except
            on E : Exception do begin
                Raise Exception.Create(MSG_ERROR_VALUE_NOT_NUMERIC + E.Message);
            end;
        end;

        //Obtengo el codigo de Respuesta
        sCodigoDeRespuesta := ParseXMLField(Request.ContentFields.Text,'CODRET');

        //Verifico la sesion
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin

            //Verifico si fue aceptado por el banco
            if sCodigoDeRespuesta = '0000' then begin

            if EsOficinaVirtual then										//SS_1280_MCA_20150812
                Plantilla := ObtenerPlantilla(FDM, Request, 'ExitoSTD')		//SS_1280_MCA_20150812
            else															//SS_1280_MCA_20150812
                Plantilla := ObtenerPlantilla(FDM, Request, 'ExitoSTD_SC');	//SS_1280_MCA_20150812
            
                //Muestro el recibo
                //Plantilla := ObtenerPlantilla(FDM, Request, 'ExitoSTD');													//SS_1304_MCA_20150709
                Plantilla := replacetag(Plantilla, 'Fecha', formatdatetime('dd/mm/yy', FDM.NowBase())); // Rev 8.  SS_917
                Plantilla := replacetag(Plantilla, 'TipoComprobanteDesc','Nota de Cobro');
                Plantilla := replacetag(Plantilla, 'NroComprobante', NumeroComprobante);
                Plantilla := replacetag(Plantilla, 'Importe', formatfloat('#,###', Importe));
                RegistrarBitacoraUso(FDM, CodigoSesion, 0, 0, 13, '', 'SalidaSTD - Pago Existoso a travez de Banco Santander');   //SS_1305_MCA_20150612
            end else begin

                //Informo el rechazo
                Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_ERROR_BDP);
                if EsOficinaVirtual then			//SS_1280_MCA_20150812
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl')		//SS_1280_MCA_20150812
                else																				//SS_1280_MCA_20150812
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'logout');				//SS_1280_MCA_20150812

                Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
                RegistrarBitacoraUso(FDM, CodigoSesion, 0, 0, 13, '', 'SalidaSTD - ' + MSG_ERROR_BDP);   //SS_1305_MCA_20150612
            end;

        end;

    except
        on E : Exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'FracasoSTD');
            AgregarLog(FDM, 'FracasoSTD - ' + E.Message);
            RegistrarBitacoraUso(FDM, CodigoSesion, 0, 0, 13, '', 'FracasoSTD - ' + E.Message);   //SS_1305_MCA_20150612
        end;
    end;
    //Muestro el comprobante en el navegador
    Response.Content := Plantilla;

    Handled := true;
end;

{-----------------------------------------------------------------------------
  Function Name: wmConveniosWEBpruebaAction
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject;
              Request: TWebRequest;
              Response: TWebResponse;
              var Handled: Boolean);
  Return Value: N/A


-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.wmConveniosWEBpruebaAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var sl : Tstringlist;
begin
    //grabo la hora que comenze
    sl := tstringlist.create();
    sl.Text := datetimetostr(FDM.NowBase()); // Rev 8.  SS_917
    sl.SaveToFile('d:\inetpub\wwwroot\costanera\debug.htm');
    sl.free;
    //supuestamente aqui se dio la validacion que llevara dos segundos
    delay(2000);
    //respondo el aceptado el banco
    Response.Content := 'ACEPTADO' + datetimetostr(FDM.NowBase()); // Rev 8.  SS_917
    //obligo a salir el mensaje en ese momento
    Response.SendResponse;
    //inmediatamente registro si salio
    sl := tstringlist.create();
    sl.Text := datetimetostr(FDM.NowBase()); // Rev 8.  SS_917
    sl.SaveToFile('d:\inetpub\wwwroot\costanera\debug1.htm');
    sl.free;

    //sigo haciendo otro proceso de 15 segundos
    delay(30000);
    //grabo y salgo
    sl := tstringlist.create();
    sl.Text := datetimetostr(FDM.NowBase()); // Rev 8.  SS_917
    sl.SaveToFile('d:\inetpub\wwwroot\costanera\debug2.htm');
    sl.free;
end;
procedure TwmConveniosWEB.Debug(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
    plantilla: string;
    i: integer;
begin
    plantilla := '<HTML><HEAD><TITLE>Pagina de debug</TITLE></HEAD><BODY><H2>Informacion de debug:</H2><HR>';

    plantilla := plantilla + 'URL: ' + request.PathInfo  + '<BR>';
    plantilla := plantilla + 'Metodo: ' + request.Method + '<BR>';
    plantilla := plantilla + 'Version Protocolo: ' + request.ProtocolVersion + '<BR>';
    plantilla := plantilla + 'Referer: ' + request.Referer + '<BR>';

    plantilla := plantilla + '<BR><H3>QueryFields:</H3>';
    if request.QueryFields.Count = 0 then begin
            plantilla := plantilla + 'No hay<BR>';
    end else begin
        for i:=0 to request.QueryFields.Count - 1 do begin
            plantilla := plantilla + request.QueryFields[i] + '<BR>';
        end;
    end;

    plantilla := plantilla + '<BR><H3>ContentFields:</H3>';
    if request.ContentFields.Count = 0 then begin
            plantilla := plantilla + 'No hay<BR>';
    end else begin
        for i:=0 to request.ContentFields.Count - 1 do begin
            plantilla := plantilla + request.ContentFields[i] + '<BR>';
        end;
    end;

    plantilla := plantilla + '<BR><H3>CookieFields:</H3>';
    if request.CookieFields.Count = 0 then begin
            plantilla := plantilla + 'No hay<BR>';
    end else begin
        for i:=0 to request.CookieFields.Count - 1 do begin
            plantilla := plantilla + request.CookieFields[i] + '<BR>';
        end;
    end;

    plantilla := plantilla + '</BODY></HTML>';

    response.Content := Plantilla;
end;

{******************************** Function Header ******************************
Function Name: WebpayAceptacion
Author : drodriguez
Date Created :
Description :
Parameters : Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean
Return Value : None
********************************************************************************
Revision 1:
    Author : ggomez
    Date : 07/09/2006
    Description :
        - Cambi� el tipo de datos de la variable Importe y la asignaci�n
        de un valor a ella, pues ahora el TotalAPagar es un bigint.
        Este cambio se hizo pues el m�todo VerificarPrecioComprobante ahora
        recibe un Int64.
********************************************************************************
Revision 2:
 Author: lgisuk
 Date Created: 25/10/2006
 Description: Se modificaron las rutinas de auditoria, ahora guardan los datos
              utilizando los tipos de datos adecuados
********************************************************************************
Revision 3:
 Author: lgisuk
 Date Created: 22/12/2006
 Description: Ahora la verificacion de precio se hace sobre la tabla de auditoria
********************************************************************************
Revision 4:
 Author: lgisuk
 Date Created: 22/12/2006
 Description: ahora primero devolvemos aceptado al banco y luego intentamos
            registrar el pago en nuestro sistema
********************************************************************************
Revision 5:
    Author : ggomez
    Date : 13/01/2007
    Description : Agregu� que registre en auditor�a, el momento en el que se
        env�a la respuesta a la confirmaci�n enviada por el ente recaudador.
********************************************************************************
Revision 6:
    Author : lgisuk
    Date : 16/07/2007
    Description : Ahora si no se recibe la respuesta del banco en un plazo
    determinado el pago es rechazado.
    Revision :1
        Author : vpaszkowicz
        Date : 09/04/2008
        Description : Transformo en funci�n la confirmaci�n de auditor�a y la
        uso para detectar confirmaciones duplicadas.

    Revision :2
        Author : nefernandez
        Date : 20/01/2009
        Description : Se modifica para que tome la fecha del servidor de base de datos
        para registrar el PagoComprobante. Obs: Antes la fecha se formaba con el dia y el mes que eran
        reportados por transbank, mientras que el a�o era obtenido del servidor web.
********************************************************************************}
procedure TwmConveniosWEB.WebpayAceptacion(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
resourcestring
    MSG_ERROR_ANSWER_BEHIND_SCHEDULE = 'Boton de Pago - La respuesta del banco llego despues de %d segundos ';
var
    Autorizacion,
    TipoVenta,
    TipoComprobante,
    Comprobante,
    Tarjeta, TBK_ID_SESION: string;
    Fecha: TDateTime;
    Cuotas,
    NumeroComprobante,
    Respuesta: integer;
    Importe: Int64;
    Error: string;
//    sl: tstringlist;
    DescripcionError : String;
    MensajeErrorCompleto: string;
    CodigoSesionWEB: Integer;				//SS_1304_MCA_20150709
Begin
//    Debug(Sender, Request, Response, handled);

//    sl := tstringlist.create();
//    sl.Text := Response.Content;
//    sl.SaveToFile('d:\inetpub\wwwroot\costanera\debug.htm');
//    sl.free;

    AgregarLog(FDM, 'WebPayAceptacion - ' + Request.ContentFields.Text);
	try
        // Obtengo par�metros de TBK_Bp_Resultado
        Comprobante := request.contentfields.values['TBK_ORDEN_COMPRA'];
        TipoComprobante := ParseParamByNumber(Comprobante, 1, ' ');
        NumeroComprobante := strtoint(ParseParamByNumber(Comprobante, 2, ' '));

        Importe := StrToInt64(request.contentfields.values['TBK_Monto']);
        Cuotas := strtoint(request.contentfields.values['TBK_NUMERO_CUOTAS']);

        (*
        //Comentado en Revision 2
        temp := request.contentfields.values['TBK_Fecha_Transaccion'];
        m := strtoint(copy(temp, 1, 2));
        d := strtoint(copy(temp, 3, 2));
        a := year(now);

        temp := request.contentfields.values['TBK_Hora_Transaccion'];
        h := strtoint(copy(temp, 1, 2));
        n := strtoint(copy(temp, 3, 2));
        s := strtoint(copy(temp, 5, 2));

        Fecha := EncodeDatetime(a, m, d, h, n, s, 0);
        *)

        Autorizacion := request.contentfields.values['TBK_Codigo_Autorizacion'];
        Tarjeta := request.contentfields.values['TBK_Final_Numero_Tarjeta'];
        Respuesta := strtointdef(request.contentfields.values['TBK_Respuesta'], -1);
        TipoVenta := request.contentfields.values['TBK_Tipo_Pago'];

        TBK_ID_SESION := request.contentfields.values['TBK_ID_SESION'];

        //Actualizo el registro de auditoria
        //Controlo la posible duplicidad del mensaje us�ndolo como funci�n.
        if ActualizarConfirmacionAuditoria(fdm, TBK_ID_SESION, CODIGO_BANCO_TRANSBANK, IntToStr(Respuesta), IIF(Respuesta = 0, TRUE, FALSE)) then begin
            //verifico si la respuesta llego a tiempo
            If QueryGetValueInt(fdm.Base,'SELECT DBO.WEB_RespuestaValida (' + QuotedStr(TBK_ID_SESION) + ',' + 'GETDATE()' + ')' ) = 0 then begin
                //creo una excepcion informado que la respuesta llego tarde
                MensajeErrorCompleto:= MSG_ERROR_ANSWER_BEHIND_SCHEDULE + TBK_ID_SESION ;
                Raise Exception.Create(format(MensajeErrorCompleto,[ObtenerValorParametroInt(fdm, 'WEB_CANT_SEG_VALIDEZ_RESPUESTA', 99)]));

            end;

            if (Respuesta = 0) then begin

                //hago varias verificaciones...
                if not VerificarMACRespuesta(Request, Error) then begin

                    // MAC invalido
                    Response.content := 'RECHAZADO';
                    DescripcionError := 'Error al intentar grabar un pago de webpay. La verificacion MAC lo rechazo con el mensaje: ' + error;
                    EventLogReportEvent(elError, DescripcionError, '');
                    ActualizarGeneroReciboAuditoria(FDM, TBK_ID_SESION, FALSE, DescripcionError, CODIGO_BANCO_TRANSBANK);
                    AgregarLog(FDM, 'WebpayAceptacion - ' + DescripcionError);

                    Auditoria_ActualizarRespuestaAConfirmacion(FDM, TBK_ID_SESION, CODIGO_BANCO_TRANSBANK, Response.Content);

                end else if not VerificarPrecioComprobante(fdm, TBK_ID_SESION, Importe, Error) then begin

                    // Precio invalido
                    Response.content := 'RECHAZADO';
                    DescripcionError := 'Error al intentar grabar un pago de webpay. La verificacion de precio lo rechazo con el mensaje: ' + error;
                    EventLogReportEvent(elError, DescripcionError, '');
                    ActualizarGeneroReciboAuditoria(FDM, TBK_ID_SESION, FALSE, DescripcionError, CODIGO_BANCO_TRANSBANK);
                    AgregarLog(FDM, 'WebpayAceptacion - ' + DescripcionError);

                    Auditoria_ActualizarRespuestaAConfirmacion(FDM, TBK_ID_SESION, CODIGO_BANCO_TRANSBANK, Response.Content);
                end else begin

                    //si el mensaje del banco cumple en formato y contenido, proviene del banco y el banco mando respuesa
                    //de aprobacion devolvemos aceptado inmendiatamente.
                    Response.content := 'ACEPTADO';
                    Response.SendResponse;

                    Auditoria_ActualizarRespuestaAConfirmacion(FDM, TBK_ID_SESION, CODIGO_BANCO_TRANSBANK, Response.Content);

                    //Revision 2
                    Fecha := QueryGetValueDateTime(fdm.Base, 'SELECT GETDATE()');
                    CodigoSesionWEB := QueryGetValueInt(FDM.Base, 'SELECT CodigoSesionWEB FROM WEB_CV_Auditoria WHERE IDTRX = '''+ TBK_ID_SESION + '''');		//SS_1304_MCA_20150709
                    //luego intentamos registrar el pago
                    if RealizarPagoComprobantes(fdm, CodigoSesionWEB, Cuotas, Respuesta, Autorizacion, TipoVenta, Tarjeta, Fecha, TBK_ID_SESION, Error) then begin				//SS_1304_MCA_20150709                        //si la Registracion del pago tuvo exito, actualizo que pudo generar el recibo
                        DescripcionError := '';
                        ActualizarGeneroReciboAuditoria(FDM, TBK_ID_SESION, TRUE, DescripcionError, CODIGO_BANCO_TRANSBANK);
                        fdm.Base.Execute('DELETE A FROM WEB_CV_DetalleDeudaConvenios A WHERE A.CodigoSesion = ' + IntToStr(CodigoSesionWEB));                            //SS_1304_MCA_20150709
                    end;
                end;

            end else begin

                //Rechazo externo//
                Response.content := 'ACEPTADO';
                DescripcionError := 'Rechazado por TransBank';
                ActualizarGeneroReciboAuditoria(FDM, TBK_ID_SESION, FALSE, DescripcionError, CODIGO_BANCO_TRANSBANK);

                Auditoria_ActualizarRespuestaAConfirmacion(FDM, TBK_ID_SESION, CODIGO_BANCO_TRANSBANK, Response.Content);
            end;
        end
        else begin //Si no pudo actualizar auditor�a.
            //El banco me envi� 2 veces la confirmaci�n.
            Response.content := 'RECHAZADO';
            DescripcionError := 'Error al intentar grabar un pago de webpay. Respuesta del banco duplicada.';
            EventLogReportEvent(elError, DescripcionError, '');
            //ActualizarGeneroReciboAuditoria(FDM, TBK_ID_SESION, FALSE, DescripcionError, CODIGO_BANCO_TRANSBANK);
            AgregarLog(FDM, 'WebpayAceptacion - ' + DescripcionError);
            //Auditoria_ActualizarRespuestaAConfirmacion(FDM, TBK_ID_SESION, CODIGO_BANCO_TRANSBANK, Response.Content);
        end;
	except
        on E : Exception do begin
            Response.content:= 'RECHAZADO';
            DescripcionError := 'Error al intentar grabar un pago de webpay: ' + e.message;
            EventLogReportEvent(elError, DescripcionError, '');
            ActualizarGeneroReciboAuditoria(FDM, TBK_ID_SESION, FALSE, DescripcionError, CODIGO_BANCO_TRANSBANK);
            AgregarLog(FDM, 'WebpayAceptacion - ' + E.Message);

            Auditoria_ActualizarRespuestaAConfirmacion(FDM, TBK_ID_SESION, CODIGO_BANCO_TRANSBANK, Response.Content);
        end;
	end;

end;

{-----------------------------------------------------------------------------
  Function Name: WebpayFracaso
  Author:    drodriguez
  Date Created:
  Description: Fracaso
  Parameters:
  Return Value: Boolean

Revision : 1
    Author : ggomez
    Date : 16/01/2007
    Description :
        - Agregu� la lectura de la variable CodigoSesion desde la URL. Esa variable
        es incorporada en la URL de Fracaso que se le informa al banco que debe
        ejecutar en caso de transacci�n de pago con fracaso.
        - Agregu� para que en caso de error haya m�s detalles que permitan
        identificar que es lo que fall�.

-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.WebpayFracaso(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
Resourcestring
    MSG_ERROR_WEBPAY = 'Su transaccion no pudo ser realizada';
    MSG_ERROR_OBTENER_CODIGO_SESION = 'Error obtener CodigoSesion desde URL: %s';
    MSG_ERROR_OBTENER_CODIGO_SESION_COMO_ENTERO = 'Error convertir a entero el CodigoSesion: %s. URL: %s';
    MSG_ERROR_OBTENER_COMPROBANTE = 'Error obtener Comprobante. TBK_ORDEN_COMPRA: %s';
    MSG_ERROR_OBTENER_TIPO_COMPROBANTE = 'Error obtener Tipo de Comprobante: %s';
    MSG_ERROR_OBTENER_NRO_COMPROBANTE = 'Error obtener N� de Comprobante: %s';
Const
    STR_RECHAZO = 'RECHAZADO';
var
    Plantilla: string;
    CodigoSesion: integer;
    TipoComprobante,
    Comprobante: string;
    NumeroComprobante: integer;
    CodigoSesionEnURL: AnsiString;
    DescriError: String;
    CodigoPersona, CodigoConvenio: Integer; //SS_1305_MCA_20150612
    RUT: string;    //SS_1305_MCA_20150612
begin
    DescriError := EmptyStr;
    AgregarLog(FDM, 'WebPayFracaso - ' + Request.ContentFields.Text);
    try
        CodigoSesionEnURL := EmptyStr;
        DescriError := Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]);
        CodigoSesionEnURL := Request.QueryFields.Values[NOMBRE_VARIABLE_CODIGO_SESION_URL];
        DescriError := EmptyStr;

        // Si no se ley� el Codigo de Sesi�n desde la URL, entonces tirar error
        // Pues algo cambi� la URL que se le indic� al banco que ejecute para
        // invocar la pantalla de Exito.
        if CodigoSesionEnURL = EmptyStr then begin
            raise Exception.Create(Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]));
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionEnURL), 0, 0, 13, '', Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]));      //SS_1305_MCA_20150612
        end;

        DescriError := Format(MSG_ERROR_OBTENER_CODIGO_SESION_COMO_ENTERO, [CodigoSesionEnURL, Request.URL]);
        CodigoSesion := StrToInt(CodigoSesionEnURL);
        DescriError := EmptyStr;

        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + IntToStr(CodigoSesion));      //SS_1305_MCA_20150612
        CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 1)');                           //SS_1305_MCA_20150612
        RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));                     //SS_1305_MCA_20150612


        DescriError := Format(MSG_ERROR_OBTENER_COMPROBANTE, [Request.ContentFields.Values['TBK_ORDEN_COMPRA']]);
        Comprobante := Request.ContentFields.Values['TBK_ORDEN_COMPRA'];
        DescriError := EmptyStr;

        DescriError := Format(MSG_ERROR_OBTENER_TIPO_COMPROBANTE, [Comprobante]);
        TipoComprobante := ParseParamByNumber(Comprobante, 1, ' ');
        DescriError := EmptyStr;

        DescriError := Format(MSG_ERROR_OBTENER_NRO_COMPROBANTE, [Comprobante]);
        NumeroComprobante := strtoint(ParseParamByNumber(Comprobante, 2, ' '));
        DescriError := EmptyStr;

        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoFracaso');
            //Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, format(MSG_TBK_TRANSACCION_NO_REALIZADA, [QueryGetValue(fdm.Base, 'SELECT dbo.ObtenerDescripcionTipoComprobante(' + quotedstr(TipoComprobante) + ')') + ' ' + inttostr(NumeroComprobante)]));}
            Plantilla := ReplaceTag(Plantilla, 'NroComprobante', Comprobante);
            Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, format(MSG_TBK_TRANSACCION_NO_REALIZADA, [QueryGetValue(fdm.Base, 'SELECT dbo.ObtenerDescripcionTipoComprobante(' + quotedstr(TipoComprobante) + ')') + ' ' + inttostr(NumeroComprobante)]));      //SS_1305_MCA_20150612
        end;

        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on E : Exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'WebPayFracaso');
            AgregarLog(FDM, 'WebpayFracaso - ' + E.Message
                + iif(DescriError <> EmptyStr, '. ' + DescriError, EmptyStr));
            RegistrarBitacoraUso(FDM,  CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'Se ha producido un error WebpayFracaso: ' + e.Message + DescriError);      //SS_1305_MCA_20150612
        end;
    end;

    Response.Content := Plantilla;
    Handled := true;
end;

{-----------------------------------------------------------------------------
  Function Name: ParseXMLField
  Author:    lgisuk
  Date Created: 03/08/2006
  Description: Obtengo el valor de un campo XML
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TwmConveniosWEB.ParseXMLField( Xml, Campo : String) : String;
begin
    xml := trim(xml);
    campo := trim(campo);
    Result := copy(XML,pos('<'+Campo+'>', xml) + length('<'+Campo+'>'), pos('</'+campo+'>', xml) - pos('<'+campo+'>', xml) - length('<'+campo+'>'));
end;

{-----------------------------------------------------------------------------
  Function Name: PagarUltimaNotaCobro
  Author:    drodriguez
  Date Created:
  Description: Pagar ultima nota de cobro
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  30/03/2007
  Ahora se incluye en la pagina de error el numero de convenio y el
  codigo de sesion
-------------------------------------------------------------------------------}
procedure TwmConveniosWEB.PagarUltimaNotaCobro(Sender: TObject;  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Plantilla: string;
    CodigoConvenio,
    CodigoSesion: integer;
    TipoComprobante: string;
    NumeroComprobante: int64;
    Error: string;
    CodigoPersona: integer;			//SS_1305_MCA_20150612
    RUT: string;					//SS_1305_MCA_20150612
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));		//SS_1305_MCA_20150612
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + IntToStr(CodigoPersona));						//SS_1305_MCA_20150612

    try
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                if not FacturacionHabilitada(fdm) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_SECCION_DISPONIBLE_LUEGO_DE_INAUGURACION);
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                    Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobro - ' + MSG_SECCION_DISPONIBLE_LUEGO_DE_INAUGURACION);      //SS_1305_MCA_20150612
                    Response.Content := Plantilla;
                    Handled := true;
                    exit;
                end;

                //Verifico si el convenio esta en carpeta legal
                if EsConvenioCarpetaLegal(FDM, CodigoConvenio, Error) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, format(MSG_CONVENIO_CARPETA_LEGAL, [Error]));
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                    Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
                    Response.Content := Plantilla;
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobro - ' + format(MSG_CONVENIO_CARPETA_LEGAL, [Error]));      //SS_1305_MCA_20150612
                    Handled := true;
                    exit;
                end;

                if ObtenerUltimaNotaCobroAPagar(FDM, CodigoSesion, TipoComprobante, NumeroComprobante, CodigoConvenio, Error) then begin		//SS_1304_MCA_20150709
                //if ObtenerUltimaNotaCobroAPagar(FDM, CodigoConvenio, TipoComprobante, NumeroComprobante, Error) then begin					//SS_1304_MCA_20150709
				    // paso la llamada al prepago
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobro - ' + 'Pagar deuda Vigente');      //SS_1305_MCA_20150612

                    response.SendRedirect(
                        BASE_PATH + NOMBRE_EXE + '/WebPay/PrePago?' +
                        TAG_CODIGO_SESION + '=' + inttostr(CodigoSesion) + '&' +
                        'Convenio' + '=' + inttostr(CodigoConvenio) + '&' +
                        'TipoComprobante' + '=' + TipoComprobante + '&' +
                        'NumeroComprobante' + '=' + inttostr(NumeroComprobante)
                    );


                    (*
                    request.contentfields.values['TipoComprobante'] := TipoComprobante;
                    request.contentfields.values['NumeroComprobante'] := inttostr(NumeroComprobante);
                    WebPayPrePago(Sender, Request, Response, Handled); *)
                    exit;
                end else begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, format(MSG_LA_OPERACION_NO_PUEDE_SER_REALIZADA, [Error]));
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobro - ' + format(MSG_LA_OPERACION_NO_PUEDE_SER_REALIZADA, [Error]));   //SS_1305_MCA_20150612
                end;
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
                 RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobro - ' + Error);   //SS_1305_MCA_20150612
            end;
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on E : Exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'PagarUltimaNotaCobro');
            AgregarLog(FDM, 'PagarUltimaNotaCobro - ' + E.Message);
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobro - ' + 'Se ha producido un error PagarUltimaNotaCobro: ' + e.Message);   //SS_1305_MCA_20150612
        end;
    end;
    Response.Content := Plantilla;
end;









{-----------------------------------------------------------------------------
  Function Name: WebPayPrePago
  Author:    drodriguez
  Date Created:
  Description: Pre Pago
  Parameters:
  Return Value: Boolean

Revision : 2
    Author : ggomez
    Date : 16/01/2007
    Description :
        - Colocar en la URL de Exito y de Fracaso, el CodigoSesion, asi cuando
        se invoquen estas URL nuestro sitio web pueda obtener el CodigoSesion
        para verificar si la sesi�n ha caducado o no.

Revision : 3
    Author : ggomez
    Date : 17/01/2007
    Description : Agregu� el reemplzao del TAG CODIGO_SESION_WEB para
        que pueda ser accedido desde

  Revision 4
  lgisuk
  30/03/2007
  Ahora se incluye en la pagina de error el numero de convenio y el
  codigo de sesion
-------------------------------------------------------------------------------}
procedure TwmConveniosWEB.WebPayPrePago(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Plantilla: string;
    CodigoConvenio,
    CodigoSesion: integer;
    TipoComprobante: string;
    NumeroComprobante: integer;
    Error: string;
    ParametroCodigoSesion: String;
    TiempoExpiracionPagoWEB: Integer;  // SS_1113_MCA_20130704
    CodigoPersona: integer;				//SS_1305_MCA_20150612
    Tabla: string;						//SS_1304_MCA_20150709
    ComprobantesSeleccionados: string;	//SS_1304_MCA_20150709
    RUT: string;						//SS_1305_MCA_20150612
    Importe: integer;					//SS_1356_MCA_20150903
begin
    try
        if request.MethodType = mtPost then begin
            CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
            CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
            TipoComprobante := request.contentfields.values['TipoComprobante'];
            NumeroComprobante := strtoint(request.contentfields.values['NumeroComprobante']);
        end else begin
            CodigoSesion := StrToIntDef(request.QueryFields.Values[TAG_CODIGO_SESION], -1);
            CodigoConvenio := StrToIntDef(request.QueryFields.Values[TAG_CONVENIO], -1);
            TipoComprobante := request.Queryfields.values['TipoComprobante'];
            NumeroComprobante := strtoint(request.Queryfields.values['NumeroComprobante']);
        end;

        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));			//SS_1305_MCA_20150612
        RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + IntToStr(CodigoPersona));							//SS_1305_MCA_20150612

        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                if VerificarSiEsPagable(FDM, TipoComprobante, NumeroComprobante, CodigoConvenio, Error) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'Prepago');

                    // Reemplazar el TAG del par�metro de C�digo de Sesi�n de
                    // las URL de Exito y de Fracaso para incorporarles el CodigoSesion
                    ParametroCodigoSesion := '?' + NOMBRE_VARIABLE_CODIGO_SESION_URL + '=' + IntToStr(CodigoSesion);
                    //CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));				//SS_1305_MCA_20150612
                    Plantilla := ReplaceTag(Plantilla, TAG_PARAMETRO_CODIGO_SESION_EXITO, ParametroCodigoSesion);
                    Plantilla := ReplaceTag(Plantilla, TAG_PARAMETRO_CODIGO_SESION_FRACASO, ParametroCodigoSesion);

                    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION_WEB, IntToStr(CodigoSesion));

                    //Verifico si el convenio esta en carpeta legal
                    if EsConvenioCarpetaLegal(FDM, CodigoConvenio, Error) then begin
                        Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                        Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, format(MSG_CONVENIO_CARPETA_LEGAL, [Error]));
                        Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
                        Response.Content := Plantilla;
                        RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WebPayPrePago - ' + format(MSG_CONVENIO_CARPETA_LEGAL, [Error]));   //SS_1305_MCA_20150612
                        Handled := true;
                        exit;
                    end;

                    // lleno los datos
                    CargarDatosComprobante(fdm, Plantilla, TipoComprobante, NumeroComprobante, CodigoSesion);
                    Plantilla := ReplaceTag(Plantilla, 'TablaDeudaConvenio', GenerarTablaDeudaConvenio(FDM, CodigoSesion, ComprobantesSeleccionados, Importe));    	//SS_1356_MCA_20150903	//SS_1304_MCA_20150709
					//Plantilla := ReplaceTag(Plantilla, 'TablaDeudaConvenio', GenerarTablaDeudaConvenio(FDM, CodigoSesion, ComprobantesSeleccionados));    		//SS_1356_MCA_20150903  //SS_1304_MCA_20150709
                    Plantilla := ReplaceTag(Plantilla, 'Seleccion', ComprobantesSeleccionados);															//SS_1304_MCA_20150709
                    Plantilla := ReplaceTag(Plantilla, 'Importe', IntToStr(Importe));																	//SS_1356_MCA_20150903 
                    // compruebo que no tenga un debito automatico
                    if QueryGetValueInt(fdm.Base, 'WEB_CV_VerificarComprobanteTieneDebitoAutomatico ' + quotedstr(TipoComprobante) + ', ' + inttostr(NumeroComprobante)) = 0 then begin
                        Plantilla := ReplaceTag(Plantilla, 'AvisoDebito', '');
                    end else begin
                        Plantilla := ReplaceTag(Plantilla, 'AvisoDebito', MSG_AVISO_TIENE_DEBITO);
                    end;

                    ObtenerParametroGeneral(FDM.Base, 'TIEMPO_EXPIRACION_PAGO_WEB', TiempoExpiracionPagoWEB);  //SS_1113_MCA_20130704
                    // le extiendo el tiempo de sesion a 30 minutos para darle tiempo a pagar
                    fdm.Base.Execute(
                      'UPDATE WEB_CV_Sesiones SET ValidaHasta = DATEADD(mi, ' + IntToStr(TiempoExpiracionPagoWEB) + ', GetDate()) WHERE CodigoSesion = ' + inttostr(CodigoSesion) //SS_1113_MCA_20130704
                    );
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WebPayPrePago - ' + 'Se ha ingresado a Pagar Deuda Vigente');   //SS_1305_MCA_20150612
                end else begin
                    // parece que no se puede pagar...
                    Plantilla := GenerarError(FDM, Request, Error);
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WebPayPrePago - ' + Error);   //SS_1305_MCA_20150612
                end;
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WebPayPrePago - ' + Error);        //SS_1305_MCA_20150612
            end;
        end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'WebPayPrePago');
            AgregarLog(FDM, 'WebPayPrePago - ' + E.Message);
            RegistrarBitacoraUso(FDM, CodigoSesion, 0, CodigoConvenio, 13, '', 'WebPayPrePago - ' + 'Se ha producido un error WebPayPrePago: ' + e.Message);   //SS_1305_MCA_20150612
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;



{-----------------------------------------------------------------------------
  Function Name: wmConveniosWEBRedirigirTransBankAction
  Author:    lgisuk
  Date Created: 18/09/2006
  Description: Registro que un usuario solicito pagar, y redirecciono al sitio
               de TransBank
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 25/10/2006
 Description: Se modificaron las rutinas de auditoria, ahora guardan los datos
              utilizando los tipos de datos adecuados
-----------------------------------------------------------------------------
Revision : 1
    Author : ggomez
    Date : 16/01/2007
    Description :
        - Agregu� la generaci�n del TBK_ID_SESION (IDTRX).
        Se hace aqu� para poder utilizar como IDTRX un valor que contenga
        el C�digo de Auditor�a asociado a la transacci�n de pago.
-----------------------------------------------------------------------------
Revision : 2
   Author : lgisuk
    Date : 13/07/2007
    Description :
        - Agregue transaccion en delphi para realizar la registracion
        en auditoria.
-----------------------------------------------------------------------------
Revision : 3
   Author : lgisuk
    Date : 16/07/2007
    Description :
        - Ahora verifico si el cliente tiene saldo antes de continuar
        si no tiene salgo genero una excepcion.

-----------------------------------------------------------------------------
Revision : 4
   Author : dAllegretti
    Date : 28/01/2009
    Description :
        - Verifico que no existan solicitudes enviadas pendientes de respuestas.
-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.wmConveniosWEBRedirigirTransBankAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;var Handled: Boolean);
resourcestring
    MSG_REGISTRANDO_AUDITORIA = 'Registrando datos en auditor�a';
    MSG_ERROR_NON_DEBT = 'El cliente no tiene deuda, se cancela el pago, comprobante: ';
    MSG_ERROR_EMPTY_CONTENTFIELDS = 'Se ha producido un error al redirigir a transbank, Por Favor Intentelo m�s tarde';        //SS_1304_MCA_20150709
var
    TBK_TIPO_TRANSACCION,
    TBK_MONTO,
    TBK_ORDEN_COMPRA,
    TBK_ID_SESION,
    TBK_URL_EXITO,
    TBK_URL_FRACASO : String;
    TipoComprobante : String;
    NumeroComprobante : Integer;
    Importe: Int64;
    Plantilla : String;
    // Para almacenar la operaci�n a realizar para que en caso de error poder
    // dejar m�s detalles del error ocurrido.
    DescriError: String;
    CodigoSesionWebStr: String;
    ComprobantesSeleccionados : string;				//SS_1304_MCA_20150709
    CodigoPersona, CodigoConvenio: Integer;			//SS_1305_MCA_20150612
    RUT :String;									//SS_1305_MCA_20150612
begin
    DescriError := EmptyStr;
    AgregarLog(FDM, 'RedirigirTransbank - ' + Request.ContentFields.Text);
    try
        if Request.ContentFields.Text = EmptyStr then                                               //SS_1304_MCA_20150709
        begin                                                                                       //SS_1304_MCA_20150709
             Plantilla := ObtenerPlantilla(FDM, Request, 'Aviso');                                  //SS_1304_MCA_20150709
             Plantilla := ReplaceTag(Plantilla, 'Aviso', MSG_ERROR_EMPTY_CONTENTFIELDS);            //SS_1304_MCA_20150709
             AgregarLog(FDM, 'RedirigirTransbank - ' + MSG_ERROR_EMPTY_CONTENTFIELDS);
             Response.Content := Plantilla;                                                         //SS_1304_MCA_20150709
             Handled := true;                                                                       //SS_1304_MCA_20150709
             Exit;                                                                                  //SS_1304_MCA_20150709
        end;                                                                                        //SS_1304_MCA_20150709

        // Obtener el C�digo de Sesi�n Web para almacenarla en Auditor�a.
        CodigoSesionWebStr := Request.ContentFields.Values[TAG_CODIGO_SESION_WEB];
        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + CodigoSesionWebStr);		//SS_1305_MCA_20150612
        CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');						//SS_1305_MCA_20150612
        RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));					//SS_1305_MCA_20150612

        //Obtengo los datos de Prepago
        TBK_TIPO_TRANSACCION := request.contentfields.values['TBK_TIPO_TRANSACCION'];
        TBK_MONTO := request.contentfields.values['TBK_MONTO'];
        TBK_ORDEN_COMPRA := request.contentfields.values['TBK_ORDEN_COMPRA'];

        TBK_URL_EXITO := request.contentfields.values['TBK_URL_EXITO'];
        TBK_URL_FRACASO := request.contentfields.values['TBK_URL_FRACASO'];

        //Obtengo el tipo y numero de Comprobante
        TipoComprobante := ParseParamByNumber(TBK_ORDEN_COMPRA, 1, ' ');
        NumeroComprobante := StrToIntDef(ParseParamByNumber(TBK_ORDEN_COMPRA, 2, ' '),-1);

        Importe := StrToInt64(request.contentfields.values['TBK_Monto']);

        ComprobantesSeleccionados := Request.ContentFields.Values['TBK_COMPROBANTES'];												//SS_1304_MCA_20150709
        FDM.Base.Execute('UPDATE WEB_CV_DetalleDeudaConvenios SET Seleccionado = 0 WHERE CodigoSesion = '+ CodigoSesionWebStr);		//SS_1304_MCA_20150709
        FDM.Base.Execute('UPDATE WEB_CV_DetalleDeudaConvenios SET Seleccionado = 1 WHERE CodigoSesion = '+ CodigoSesionWebStr + ' AND NumeroComprobante IN ('+ ComprobantesSeleccionados +')');	//SS_1304_MCA_20150709
        //Revision 4
        If VerificarSiExisteSolicitudPendiente( fdm, TipoComprobante, NumeroComprobante, CODIGO_BANCO_TRANSBANK, Importe, DescriError)then begin
            //Creo una excepcion informando que el pago esta en proceso
            Raise Exception.Create(DescriError);
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'PrepagoRedirectTransbank - ' + DescriError);   //SS_1305_MCA_20150612
        end;
        //Fin Revision 4


        //Verifico si el cliente tiene deuda para pagar, sino salgo
        If QueryGetValueInt(fdm.Base,'SELECT DBO.WEB_ConvenioTieneDeuda (' + QuotedStr(TipoComprobante) + ',' + IntToStr(NumeroComprobante) + ')' ) = 0 then begin
            //creo una excepcion informado que no tiene deuda para pagar
            Raise Exception.Create(MSG_ERROR_NON_DEBT + IntToStr(NumeroComprobante));
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'PrepagoRedirectTransbank - ' + DescriError);   //SS_1305_MCA_20150612
        end;

        if VerificarSiEstaPago(fdm, TipoComprobante, NumeroComprobante, CODIGO_BANCO_TRANSBANK, Importe, DescriError)then begin
            //Creo una excepcion informando que el comprobante esta pago por este canal
            Raise Exception.Create(DescriError);
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'PrepagoRedirectTransbank - ' + DescriError);   //SS_1305_MCA_20150612
        end;
        //Fin Revision 5
        
        //Inicio la Trasaccion
        Fdm.Base.BeginTrans;
        try

        // Dejar registro en Auditor�a para la transacci�n de pago y obtener el IDTRX.
            DescriError := MSG_REGISTRANDO_AUDITORIA;
            TBK_ID_SESION := AgregarAuditoria(fdm, TipoComprobante, NumeroComprobante, Importe, CODIGO_BANCO_TRANSBANK, StrToInt(CodigoSesionWebStr));
            DescriError := EmptyStr;

            //Acepto la transaccion
            Fdm.Base.CommitTrans;
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'PrepagoRedirectTransbank - Registrado En auditoria IDTRX: ' + TBK_ID_SESION);   //SS_1305_MCA_20150612
        Except
             on E : Exception do begin
                 //Cancelo la Transaccion
                 Fdm.Base.RollbackTrans;
                 //lanzo una excepcion para para que sea manejada
                 Raise Exception.Create(E.Message);
                 RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'PrepagoRedirectTransbank - ' + E.Message);   //SS_1305_MCA_20150612
             end;
        end;

        //Cargo la plantilla
        Plantilla := ObtenerPlantilla(FDM, Request, 'PrepagoRedirectTransbank');

        //Lleno los datos a Enviar a TransBank
        Plantilla := ReplaceTag(Plantilla, 'TBK_TIPO_TRANSACCION', TBK_TIPO_TRANSACCION);
        Plantilla := ReplaceTag(Plantilla, 'TBK_MONTO', TBK_MONTO);
        Plantilla := ReplaceTag(Plantilla, 'TBK_ORDEN_COMPRA', TBK_ORDEN_COMPRA);
        Plantilla := ReplaceTag(Plantilla, 'TBK_ID_SESION',TBK_ID_SESION);
        Plantilla := ReplaceTag(Plantilla, 'TBK_URL_EXITO',TBK_URL_EXITO);
        Plantilla := ReplaceTag(Plantilla, 'TBK_URL_FRACASO',TBK_URL_FRACASO);
        RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'PrepagoRedirectTransbank - Redireccionado a Webpay-Transbank');  //SS_1305_MCA_20150612
    except
        on E : Exception do begin
            Plantilla := AtenderError(Request, FDM, E, 'RedirigirTransBank');
            AgregarLog(FDM, 'RedirigirTransBank - ' + E.Message
                + iif(DescriError <> EmptyStr, '. ' + DescriError, EmptyStr));
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'PrepagoRedirectTransbank - Se ha producido un error al redireccionar a transbank' + E.Message);       //SS_1305_MCA_20150612
        end;
    end;
    //Muestro la pagina en el navegador
    Response.Content := Plantilla;
    Handled := True;
end;

{-----------------------------------------------------------------------------
  Function Name: WebpayExito
  Author:    drodriguez
  Date Created:
  Description: Exito
  Parameters:
  Return Value: Boolean

Revision : 1
    Author : ggomez
    Date : 16/01/2007
    Description :
        - Agregu� la lectura de la variable CodigoSesion desde la URL. Esa variable
        es incorporada en la URL de Exito que se le informa al banco que debe
        ejecutar en caso de transacci�n de pago con exito.
        - Agregu� para que en caso de error haya m�s detalles que permitan
        identificar que es lo que fall�.
-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.WebpayExito(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
resourcestring
    MSG_ERROR_OBTENER_CODIGO_SESION = 'Error obtener CodigoSesion desde URL: %s';
    MSG_ERROR_OBTENER_CODIGO_SESION_COMO_ENTERO = 'Error convertir a entero el CodigoSesion: %s. URL: %s';
    MSG_ERROR_OBTENER_COMPROBANTE = 'Error obtener Comprobante. TBK_ORDEN_COMPRA: %s';
    MSG_ERROR_OBTENER_TIPO_COMPROBANTE = 'Error obtener Tipo de Comprobante: %s';
    MSG_ERROR_OBTENER_NRO_COMPROBANTE = 'Error obtener N� de Comprobante: %s';
    MSG_ERROR_GET_SESSION_CODE  = 'Error al obtener el C�digo de Sesi�n Web';	//SS_1304_MCA_20150709
var
    Plantilla: string;
    CodigoSesion: integer;
    TipoComprobante,
    Comprobante: string;
    NumeroComprobante: integer;
    CodigoSesionEnURL: AnsiString;
    DescriError: String;
    CodigoPersona, CodigoConvenio, ImportePagado, CodigoSesionWEB: Integer;                           //SS_1304_MCA_20150709
    RUT, IdTrx: string;                                                              //SS_1304_MCA_20150709

begin
    DescriError := EmptyStr;
    AgregarLog(FDM, 'WebPayExito - ' + Request.ContentFields.Text);
    try
        if Request.ContentFields.Text = EmptyStr then begin
             Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoFracaso');
             Plantilla := ReplaceTag(Plantilla, 'NroComprobante', '');
             Response.Content := Plantilla;
             Handled := true;
             Exit;
        end;


        CodigoSesionEnURL := EmptyStr;
        DescriError := Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]);
        CodigoSesionEnURL := Request.QueryFields.Values[NOMBRE_VARIABLE_CODIGO_SESION_URL];
        DescriError := EmptyStr;

        // Si no se ley� el Codigo de Sesi�n desde la URL, entonces tirar error
        // Pues algo cambi� la URL que se le indic� al banco que ejecute para
        // invocar la pantalla de Exito.
        if CodigoSesionEnURL = EmptyStr then begin
             raise Exception.Create(Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]));
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionEnURL), 0, 0, 13, '', Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]));  //SS_1305_MCA_20150612
        end;

        DescriError := Format(MSG_ERROR_OBTENER_CODIGO_SESION_COMO_ENTERO, [CodigoSesionEnURL, Request.URL]);
        CodigoSesion := StrToInt(CodigoSesionEnURL);
        DescriError := EmptyStr;

        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + IntToStr(CodigoSesion));
        CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');
        RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));

        IdTrx := Request.ContentFields.Values['TBK_ID_SESION'];                         //SS_1304_MCA_20150709

        try																			//SS_1304_MCA_20150709
            ImportePagado := QueryGetValueInt(FDM.Base,									//SS_1304_MCA_20150709
                'SELECT Importe FROM Web_CV_Auditoria WITH (NOLOCK)'			        //SS_1304_MCA_20150709
                    + ' WHERE CodigoBanco = ' + IntToStr(CODIGO_BANCO_TRANSBANK)		//SS_1304_MCA_20150709
                        + ' AND IDTRX = ' + QuotedStr(IdTrx));					        //SS_1304_MCA_20150709
        except																			//SS_1304_MCA_20150709
            on E : Exception do begin													//SS_1304_MCA_20150709
                raise Exception.Create(MSG_ERROR_GET_SESSION_CODE + E.Message);			//SS_1304_MCA_20150709
            end;																		//SS_1304_MCA_20150709
        end;																			//SS_1304_MCA_20150709

        try																			//SS_1304_MCA_20150709
            CodigoSesionWEB := QueryGetValueInt(FDM.Base,									//SS_1304_MCA_20150709
                'SELECT CodigoSesionWEB FROM Web_CV_Auditoria WITH (NOLOCK)'			        //SS_1304_MCA_20150709
                    + ' WHERE CodigoBanco = ' + IntToStr(CODIGO_BANCO_TRANSBANK)		//SS_1304_MCA_20150709
                        + ' AND IDTRX = ' + QuotedStr(IdTrx));					        //SS_1304_MCA_20150709
        except																			//SS_1304_MCA_20150709
            on E : Exception do begin													//SS_1304_MCA_20150709
                raise Exception.Create(MSG_ERROR_GET_SESSION_CODE + E.Message);			//SS_1304_MCA_20150709
            end;																		//SS_1304_MCA_20150709
        end;

        DescriError := Format(MSG_ERROR_OBTENER_COMPROBANTE, [Request.ContentFields.Values['TBK_ORDEN_COMPRA']]);
        Comprobante := Request.ContentFields.Values['TBK_ORDEN_COMPRA'];
        DescriError := EmptyStr;

        DescriError := Format(MSG_ERROR_OBTENER_TIPO_COMPROBANTE, [Comprobante]);
        TipoComprobante := ParseParamByNumber(Comprobante, 1, ' ');
        DescriError := EmptyStr;

        DescriError := Format(MSG_ERROR_OBTENER_NRO_COMPROBANTE, [Comprobante]);
        NumeroComprobante := StrToInt(ParseParamByNumber(Comprobante, 2, ' '));
        DescriError := EmptyStr;

        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            Plantilla := ObtenerPlantilla(FDM, Request, 'Exito');           
            CargarDatosPago(fdm, Plantilla, TipoComprobante, NumeroComprobante, ImportePagado);
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionEnURL), CodigoPersona, CodigoConvenio, 13, RUT, 'WebpayExito - Transbank - Se ha realizado el pago exitosamente Transbank');  //SS_1305_MCA_20150612
        end;

        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
    except
        on E : Exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'WebpayExito');
            AgregarLog(FDM, 'WebpayExito - ' + E.Message
                + iif(DescriError <> EmptyStr, '. ' + DescriError, EmptyStr));
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionEnURL), CodigoPersona, CodigoConvenio, 13, RUT, 'Se ha producido un error WebpayExito: ' + E.Message);  //SS_1305_MCA_20150612
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

{-----------------------------------------------------------------------------
  Function Name: wmConveniosWEBRedirigirSantanderAction
  Author:    lgisuk
  Date Created: 18/09/2006
  Description: Registro que un usuario solicito pagar, y redirecciono al sitio
               de Santander
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
 Revision : 1
 Author: lgisuk
 Date Created: 25/10/2006
 Description: Se modificaron las rutinas de auditoria, ahora guardan los datos
              utilizando los tipos de datos adecuados
------------------------------------------------------------------------------
Revision : 2
    Author : ggomez
    Date : 17/01/2007
    Description :
        - Cambi� el uso de 'NK' por la constante TC_NOTA_COBRO.
        - Agregu� m�s detalle en caso de error.
-----------------------------------------------------------------------------
 Revision : 3
 Author: lgisuk
 Date Created: 16/07/2007
 Description: Ahora verifico que el cliente tenga deuda antes de continuar
              sino lanzo una excepcion.
-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.wmConveniosWEBRedirigirSantanderAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;var Handled: Boolean);
resourcestring
    MSG_REGISTRANDO_AUDITORIA = 'Registrando datos en auditor�a';
    MSG_REGISTRANDO_AUDITORIA_SANTANDER = 'Registrando datos en auditor�a Santander';
    MSG_ERROR_NON_DEBT = 'El cliente no tiene deuda, se cancela el pago, comprobante: ';
    MSG_GLOSA_PAGO_STD = 'Pago Peaje con Banco Santader Santiago';			//SS_1356_MCA_20150903
var
    data : String;
    TipoComprobante : String;
    NumeroComprobante : Integer;
    Importe : Int64;
    IDTRX : String;
    Plantilla : String;
    // Para almacenar la operaci�n a realizar para que en caso de error poder
    // dejar m�s detalles del error ocurrido.
    DescriError: String;
    CodigoSesionWebStr: String;
    CodigoServicioRecaudacion:AnsiString;                                       //SS-966-NDR-20111128
    ComprobantesSeleccionados: string;					//SS_1304_MCA_20150709
    CodigoPersona, CodigoConvenio : Integer;			//SS_1305_MCA_20150612
    RUT: string;										//SS_1305_MCA_20150612
    TiempoExpiracionPagoWEB: Integer;					//SS_1356_MCA_20150903
    CodigoIdCom, XMLDataPart1, XMLDataPart2 : string;	//SS_1356_MCA_20150903
begin
    AgregarLog(FDM, 'RedirigirSantander - ' + Request.ContentFields.Text);
    DescriError := EmptyStr;
    try
        XMLDataPart1 := '<MPINI><IDCOM><#IdCom></IDCOM><IDTRX><#IdTrxSTD></IDTRX><TOTAL><#ImporteSinDecimales></TOTAL><NROPAGOS>1</NROPAGOS><DETALLE>';																						//SS_1356_MCA_20150903
        XMLDataPart2 := '<SRVREC><#SRVREC></SRVREC><MONTO><#ImporteSinDecimales></MONTO><GLOSA><#glosa></GLOSA><CANTIDAD>1</CANTIDAD><PRECIO><#ImporteSinDecimales></PRECIO><DATOADIC><#NroComprobante></DATOADIC></DETALLE></MPINI>';		//SS_1356_MCA_20150903
        // Obtener el C�digo de Sesi�n Web para almacenarla en Auditor�a.
        CodigoSesionWebStr := Request.ContentFields.Values[TAG_CODIGO_SESION_WEB];

        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + CodigoSesionWebStr);				//SS_1305_MCA_20150612
        RUT :=  QueryGetValue(fdm.base, 'SELECT NumeroDocumento FROM Personas WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));							//SS_1305_MCA_20150612
        CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');								//SS_1305_MCA_20150612

        TipoComprobante := Request.ContentFields.Values['TipoComprobante'];					//SS_1356_MCA_20150903
        NumeroComprobante := strtoint(Request.ContentFields.Values['NumeroComprobante']);	//SS_1356_MCA_20150903
        Importe:= strtoint(request.contentfields.values['Importe']) div 100;				//SS_1356_MCA_20150903
        //Obtengo los datos de Prepago
        //data := request.contentfields.values['data'];																	//SS_1356_MCA_20150903
        ObtenerParametroGeneral(FDM.Base, 'CODIGO_SERVICIO_BDP_STD', CodigoIdCom);										//SS_1356_MCA_20150903
        ObtenerParametroGeneral(FDM.Base, 'CODIGO_SERVICIO_RECAUDACION_BDP_STD', CodigoServicioRecaudacion);			//SS_1356_MCA_20150903                     //SS-966-NDR-20111128
        //Obtengo el tipo y numero de Comprobante
        //TipoComprobante := TC_NOTA_COBRO;
		//NumeroComprobante := strtoint(ParseXMLField(Request.ContentFields.Text,'DATOADIC'));			//SS_1356_MCA_20150903
        //TipoComprobante := QueryGetValue(fdm.base, 'SELECT TipoComprobante FROM WEB_CV_DetalleDeudaConvenios WHERE CodigoSesion='+ CodigoSesionWebStr + ' AND NumeroComprobante = ' + IntToStr(NumeroComprobante)); //SS_1356_MCA_20150903 //SS_1305_MCA_20150612


        //Importe := strtoint(ParseXMLField(Request.ContentFields.Text,'TOTAL'));		//SS_1356_MCA_20150903

        //Verifico si el cliente tiene deuda para pagar, sino salgo
        If QueryGetValueInt(fdm.Base,'SELECT DBO.WEB_ConvenioTieneDeuda (' + QuotedStr(TipoComprobante) + ',' + IntToStr(NumeroComprobante) + ')' ) = 0 then begin
            //creo una excepcion informado que no tiene deuda para pagar
            Raise Exception.Create(MSG_ERROR_NON_DEBT + IntToStr(NumeroComprobante));
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'WEBRedirigirSantander - ' + MSG_ERROR_NON_DEBT + IntToStr(NumeroComprobante));  //SS_1305_MCA_20150612
        end;

        //Inicio la Trasaccion
        Fdm.Base.BeginTrans;
        try

            // Dejar registro en Auditor�a para la transacci�n de pago y obtener el IDTRX.
            DescriError := MSG_REGISTRANDO_AUDITORIA;
            IDTRX := AgregarAuditoria(fdm, TipoComprobante, NumeroComprobante,
						//Importe * 100, CODIGO_BANCO_SANTANDER, StrToInt(CodigoSesionWebStr));	//SS_1356_MCA_20150903
                        Importe, CODIGO_BANCO_SANTANDER, StrToInt(CodigoSesionWebStr));			//SS_1356_MCA_20150903
            DescriError := EmptyStr;

            //Registro la entrada al banco
            DescriError := MSG_REGISTRANDO_AUDITORIA_SANTANDER;
			//AgregarAuditoriaBDP_MPINI(fdm, Request, IDTRX);																											//SS_1356_MCA_20150903
            AgregarAuditoriaBDP_MPINI(FDM, IDTRX, CodigoIdCom, CodigoServicioRecaudacion, MSG_GLOSA_PAGO_STD, Importe, Importe, Importe,  NumeroComprobante, 1, 1);		//SS_1356_MCA_20150903
            DescriError := EmptyStr;

            //Acepto la transaccion
            Fdm.Base.CommitTrans;
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'WEBRedirigirSantander - Registrado en auditoria IDTRX: ' + IDTRX);  //SS_1305_MCA_20150612
        Except
             on E : Exception do begin
                 //Cancelo la Transaccion
                 Fdm.Base.RollbackTrans;
                 //lanzo una excepcion para para que sea manejada
                 Raise Exception.Create(E.Message);
             end;
        end;

        // Reemplazar en data la variable TAG_IDTRX_SANTANDER con el valor
        // obtenido al agregar los datos en Auditor�a.
		//data := ReplaceTag(data, TAG_IDTRX_SANTANDER, IDTRX);										//SS_1356_MCA_20150903
        XMLDataPart1 := ReplaceTag(XMLDataPart1, 'IdCom', CodigoIdCom);								//SS_1356_MCA_20150903
        XMLDataPart1 := ReplaceTag(XMLDataPart1, TAG_IDTRX_SANTANDER, IDTRX);						//SS_1356_MCA_20150903
        XMLDataPart1 := ReplaceTag(XMLDataPart1, 'ImporteSinDecimales', IntToStr(Importe));			//SS_1356_MCA_20150903
        XMLDataPart2 := ReplaceTag(XMLDataPart2, 'ImporteSinDecimales', IntToStr(Importe));			//SS_1356_MCA_20150903
        XMLDataPart2 := ReplaceTag(XMLDataPart2, 'NroComprobante', IntToStr(NumeroComprobante));	//SS_1356_MCA_20150903
        XMLDataPart2 := ReplaceTag(XMLDataPart2, 'glosa', MSG_GLOSA_PAGO_STD);						//SS_1356_MCA_20150903
        XMLDataPart2 := ReplaceTag(XMLDataPart2, 'SRVREC', CodigoServicioRecaudacion);              //SS_1356_MCA_20150903 //SS-966-NDR-20111128

        ComprobantesSeleccionados := Request.ContentFields.Values['COMPROBANTES'];													//SS_1304_MCA_20150709
        FDM.Base.Execute('UPDATE WEB_CV_DetalleDeudaConvenios SET Seleccionado = 0 WHERE CodigoSesion = '+ CodigoSesionWebStr);		//SS_1304_MCA_20150709
        FDM.Base.Execute('UPDATE WEB_CV_DetalleDeudaConvenios SET Seleccionado = 1 WHERE CodigoSesion = '+ CodigoSesionWebStr + ' AND NumeroComprobante IN ('+ ComprobantesSeleccionados +')');	//SS_1304_MCA_20150709
        // Reemplaza el dato del ServicioRecaudacion por el SRVREC que corresponde al BancoSantander                                //SS_1356_MCA_20150903 //SS-966-NDR-20111128
        //ObtenerParametroGeneral(FDM.Base, 'CODIGO_SERVICIO_RECAUDACION_BDP_STD', CodigoServicioRecaudacion);                      //SS_1356_MCA_20150903 //SS-966-NDR-20111128
        //data := ReplaceText(data,                                                                                                 //SS_1356_MCA_20150903 //SS-966-NDR-20111128
        //                    '<#SRVREC>',                                                                                          //SS_1356_MCA_20150903 //SS-966-NDR-20111128
        //                    CodigoServicioRecaudacion                                                                             //SS_1356_MCA_20150903 //SS-966-NDR-20111128
        //                   );                                                                                                     //SS_1356_MCA_20150903 //SS-966-NDR-20111128

        //Cargo la plantilla
        Plantilla := ObtenerPlantilla(FDM, Request, 'PrepagoRedirectSantander');

        ObtenerParametroGeneral(FDM.Base, 'TIEMPO_EXPIRACION_PAGO_WEB', TiempoExpiracionPagoWEB);  //SS_1356_MCA_20150903
        // le extiendo el tiempo de sesion a 30 minutos para darle tiempo a pagar
        fdm.Base.Execute('UPDATE WEB_CV_Sesiones SET ValidaHasta = DATEADD(mi, ' + IntToStr(TiempoExpiracionPagoWEB) + ', GetDate()) WHERE CodigoSesion = ' + CodigoSesionWebStr); //SS_1356_MCA_20150903	
        //Lleno los datos a Enviar a Santander
        Plantilla := ReplaceTag(Plantilla, 'data', XMLDataPart1+XMLDataPart2);			//SS_1356_MCA_20150903
        RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'WEBRedirigirSantander - redireccionado a Banco Santander');  //SS_1305_MCA_20150612
        AgregarLog(FDM, 'RedirigirSantander - ' + XMLDataPart1+XMLDataPart2);			//SS_1356_MCA_20150903
    except
        on E : Exception do begin
            Plantilla := AtenderError(Request, FDM, E, 'RedirigirSantander');
            AgregarLog(FDM, 'RedirigirSantander - ' + E.Message
                + iif(DescriError <> EmptyStr, '. ' + DescriError, EmptyStr));
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'WEBRedirigirSantander - Se ha producido un error Pago Santander: ' + E.Message);  //SS_1305_MCA_20150612
        end;
    end;
    //Muestro la pagina en el navegador
    Response.Content := Plantilla;
end;


{-----------------------------------------------------------------------------
  Function Name: wmConveniosWEBValidacionCaptchaAction
  Author:    mpiazza
  Date Created: 08/08/2009
  Description: Genera la imagen CaptCha y la envia via REquest
  Parameters: Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean
  Return Value:
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.wmConveniosWEBValidacionCaptchaAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
  CadenaCaptcha : string    ;
  tmp       : TImage;
  fondo     : TImage;
  S         : TMemoryStream;
  CaptCha   : TCaptCha;
begin

  CadenaCaptcha := ObtenerSesionCaptcha(FDM, Request.RemoteAddr);
  if CadenaCaptcha = '' then CadenaCaptcha := 'ERROR!!!!';
  try

        //genero todos los objetos necesarios
        CaptCha :=  TCaptCha.create();

        //GenerarSesionCaptcha(FDM, Request.ContentFields.Values[TAG_RUT], Request.RemoteAddr, CaptCha.PalabraAleatoria(6));

        S := TMemoryStream.Create;
        tmp := TImage.Create(nil);

        tmp.Picture.Bitmap.Width := 240;
        tmp.Picture.Bitmap.Height := 60;
        tmp.Picture.Bitmap.Canvas.Brush.Color := clMedGray;

        CaptCha.GeneraStreamCaptCha(tmp,   200+ Random(500),  TStream(S), CadenaCaptcha);
        S.Position := 0;
        //JpegImage.LoadFromStream(tstream(s));
        Response.ContentType := 'image/jpeg';
        S.Position := 0;
        Response.ContentStream := S;
        Response.SendResponse;

  finally
      CaptCha.free;
      tmp.free;
      S.Clear;
  end;

end;

{-----------------------------------------------------------------------------
  Function Name: DescargarCSVDetalleTransitos
  Author:    jjofre
  Date Created: 13/09/2010
  Description: Genera el CSV de todas los transitos incluidos en el rango de fechas
             especificados desde la cartola/Transacciones, independiente si la cantidad
             de registros indicados por el cliente, es menor a la cantidad ttal de transitos encontrados
             en el rango especificado

    Revision : 12
    Author : pdominguez
    Date : 22/10/2010
    Description : SS 917 - Modificaciones WEB
        - Se cambia el m�todo para leer las fechas devueltas y evitar el
        posible conflicto en la conversi�n, por la configuraci�n regional.
-----------------------------------------------------------------------------}
procedure TwmConveniosWEB.DescargarCSVDetalleTransitos(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
resourcestring
    MSG_ERROR_FECHA = 'Se produjo en Error al Convertir la Fecha %: %s.';
var
    Error,
    IndiceVehiculo,Plantilla: string;
    CodigoConvenio,
    CodigoSesion: integer;
    d, m, a: integer; // Rev. 12 SS_917_20101022
    FechaDesde, FechaHasta: tdatetime;  //Rev.10 SS_756

begin
    if request.MethodType = mtPost then begin
        CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
        CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    end else begin
        CodigoSesion := StrToIntDef(request.QueryFields.Values[TAG_CODIGO_SESION], -1);
        CodigoConvenio := StrToIntDef(request.QueryFields.Values[TAG_CONVENIO], -1);
    end;

    IndiceVehiculo := request.ContentFields.Values['Vehiculo'];     //Rev.10 SS_756

//    FechaDesde := StrToDateTime(request.ContentFields.Values['FechaDesde']);    //Rev.10 SS_756  // Rev. 12 SS_917_20101022
//    FechaHasta := StrToDateTime(request.ContentFields.Values['FechaHasta']);    //Rev.10 SS_756  // Rev. 12 SS_917_20101022

    try
        // Rev. 12 SS_917_20101022
        d := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 1, '/'), 0);
        m := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 2, '/'), 0);
        a := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 3, '/'), 0);
        if not TryEncodedate(a, m, d, FechaDesde) then raise exception.Create(Format(MSG_ERROR_FECHA,['Desde', request.ContentFields.Values['FechaDesde']]));

        d := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 1, '/'), 0);
        m := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 2, '/'), 0);
        a := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 3, '/'), 0);
        if not TryEncodedate(a, m, d, FechaHasta) then raise exception.Create(Format(MSG_ERROR_FECHA,['Hasta', request.ContentFields.Values['FechaHasta']]));
        // Fin Rev. 12 SS_917_20101022

        CargarDatosCSV(FDM, Plantilla, CodigoConvenio, strtointdef(IndiceVehiculo, -1), FechaDesde, FechaHasta);  //Rev.10 SS_756
        response.ContentType := 'text/csv';
        response.SetCustomHeader('Content-Disposition', 'attachment; filename=' + 'Detalle_Transitos' + '.csv'); // Rev 2.  SS_917
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'DescargarCSV');
        end;
    end;
    Response.Content := Plantilla;     //Rev.10 SS_756
    Handled := true;
end;

{
    Revision : 13
    Author : pdominguez
    Date : 28/10/2010
    Description : SS 917 - Modificaciones WEB
        - Se implementa el proceso para descargar el archivo de ayuda para la captura desde el Excel.
}
procedure TwmConveniosWEB.wmConveniosWEBDescargarArchivoAyudaExcelAction(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
    Plantilla: String;
begin
    try
        Plantilla := '';
        Plantilla := FileToString(DIRECTORIO_PLANTILLAS + 'AyudaExcel.txt', fmShareDenyNone);
        response.ContentType := 'text/csv';
        response.SetCustomHeader('Content-Disposition', 'attachment; filename=' + 'AyudaExcel.txt');
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'DescargarArchivoAyudaExcel');
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;



procedure TwmConveniosWEB.wmConveniosWEBDescargarCSVDetalleEstacionamientosAction(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
resourcestring
    MSG_ERROR_FECHA = 'Se produjo en Error al Convertir la Fecha %: %s.';
var
    Error,
    IndiceVehiculo,Plantilla: string;
    CodigoConvenio,
    CodigoSesion: integer;
    d, m, a: integer; // Rev. 12 SS_917_20101022
    FechaDesde, FechaHasta: tdatetime;  //Rev.10 SS_756

begin
    if request.MethodType = mtPost then begin
        CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
        CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    end else begin
        CodigoSesion := StrToIntDef(request.QueryFields.Values[TAG_CODIGO_SESION], -1);
        CodigoConvenio := StrToIntDef(request.QueryFields.Values[TAG_CONVENIO], -1);
    end;

    IndiceVehiculo := request.ContentFields.Values['Vehiculo'];     //Rev.10 SS_756

//    FechaDesde := StrToDateTime(request.ContentFields.Values['FechaDesde']);    //Rev.10 SS_756  // Rev. 12 SS_917_20101022
//    FechaHasta := StrToDateTime(request.ContentFields.Values['FechaHasta']);    //Rev.10 SS_756  // Rev. 12 SS_917_20101022

    try
        // Rev. 12 SS_917_20101022
        d := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 1, '/'), 0);
        m := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 2, '/'), 0);
        a := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaDesde'], 3, '/'), 0);
        if not TryEncodedate(a, m, d, FechaDesde) then raise exception.Create(Format(MSG_ERROR_FECHA,['Desde', request.ContentFields.Values['FechaDesde']]));

        d := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 1, '/'), 0);
        m := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 2, '/'), 0);
        a := strtointdef(ParseParamByNumber(request.ContentFields.Values['FechaHasta'], 3, '/'), 0);
        if not TryEncodedate(a, m, d, FechaHasta) then raise exception.Create(Format(MSG_ERROR_FECHA,['Hasta', request.ContentFields.Values['FechaHasta']]));
        // Fin Rev. 12 SS_917_20101022

        CargarDatosCSV(FDM, Plantilla, CodigoConvenio, strtointdef(IndiceVehiculo, -1), FechaDesde, FechaHasta, True);  //Rev.10 SS_756
        response.ContentType := 'text/csv';
        response.SetCustomHeader('Content-Disposition', 'attachment; filename=' + 'Detalle_Estacionamientos' + '.csv'); // Rev 2.  SS_917
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'DescargarCSV');
        end;
    end;
    Response.Content := Plantilla;     //Rev.10 SS_756
    Handled := true;
end;


//----------------------------------------------------------------
//SS966-NDR-20110810
// procedure wmConveniosWEBRedirigirBancoChileAction
// Invoca a la p�gina del boton de pago BCH
//----------------------------------------------------------------
procedure TwmConveniosWEB.wmConveniosWEBRedirigirBancoChileAction(
  Sender: TObject; Request: TWebRequest; Response: TWebResponse;
  var Handled: Boolean);
resourcestring
    MSG_REGISTRANDO_AUDITORIA               = 'Registrando datos en auditor�a';
    MSG_REGISTRANDO_AUDITORIA_BANCOCHILE    = 'Registrando datos en auditor�a BancoChile';
    MSG_ERROR_NON_DEBT                      = 'El cliente no tiene deuda, se cancela el pago, comprobante: ';
    MSG_ERROR_EMPTY_CONTENTFIELDS = 'Se ha producido un error al redirigir a Banco de Chile, Por Favor Intentelo m�s tarde';        //SS_1304_MCA_20150709
var
    data                : String;
    TipoComprobante     : String;
    NumeroComprobante   : Integer;
    Importe             : Int64;
    IDTRX               : String;
    Plantilla           : String;
    // Para almacenar la operaci�n a realizar para que en caso de error poder dejar m�s detalles del error ocurrido.
    DescriError: String;
    CodigoSesionWebStr: String;
    CodigoServicioRecaudacion:AnsiString;                                        //SS-966-NDR-20111128
    RUT, ComprobantesSeleccionados: string;					                     //SS_1304_MCA_20150709
    CodigoPersona, CodigoConvenio : integer;									 //SS_1305_MCA_20150612
begin
    DescriError := EmptyStr;
    AgregarLog(FDM, 'RedirigirBancoChile - ' + Request.ContentFields.Text);                         //SS_1304_MCA_20150709
    try
        if Request.ContentFields.Text = EmptyStr then                                               //SS_1304_MCA_20150709
        begin                                                                                       //SS_1304_MCA_20150709
             Plantilla := ObtenerPlantilla(FDM, Request, 'Aviso');                                  //SS_1304_MCA_20150709
             Plantilla := ReplaceTag(Plantilla, 'Aviso', MSG_ERROR_EMPTY_CONTENTFIELDS);            //SS_1304_MCA_20150709
             Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'logout');                      //SS_1304_MCA_20150709
             AgregarLog(FDM, 'RedirigirBancoChile - ' + MSG_ERROR_EMPTY_CONTENTFIELDS);             //SS_1304_MCA_20150709
             Response.Content := Plantilla;                                                         //SS_1304_MCA_20150709
             Handled := true;                                                                       //SS_1304_MCA_20150709
             Exit;                                                                                  //SS_1304_MCA_20150709
        end;

        // Obtener el C�digo de Sesi�n Web para almacenarla en Auditor�a.
        CodigoSesionWebStr := Request.ContentFields.Values[TAG_CODIGO_SESION_WEB];

        //Obtengo los datos de Prepago
        data := request.contentfields.values['data'];

        //Obtengo el tipo y numero de Comprobante
        //TipoComprobante := TC_NOTA_COBRO;
        NumeroComprobante := strtoint(ParseXMLField(Request.ContentFields.Text,'DATOADIC'));
        TipoComprobante := QueryGetValue(fdm.base, 'SELECT TipoComprobante FROM WEB_CV_DetalleDeudaConvenios WHERE CodigoSesion='+ CodigoSesionWebStr + ' AND NumeroComprobante = ' + IntToStr(NumeroComprobante)); //SS_1305_MCA_20150612

        Importe := strtoint(ParseXMLField(Request.ContentFields.Text,'TOTAL'));
        ObtenerParametroGeneral(FDM.Base, 'CODIGO_SERVICIO_RECAUDACION_BDP_BCH', CodigoServicioRecaudacion);            //SS-966-NDR-20111128

        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + CodigoSesionWebStr);		//SS_1305_MCA_20150612
        CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');						//SS_1305_MCA_20150612
        RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));					//SS_1305_MCA_20150612

        //Verifico si el cliente tiene deuda para pagar, sino salgo
        If QueryGetValueInt(fdm.Base,'SELECT DBO.WEB_ConvenioTieneDeuda (' + QuotedStr(TipoComprobante) + ',' + IntToStr(NumeroComprobante) + ')' ) <= 0 then begin
            //creo una excepcion informado que no tiene deuda para pagar
            Plantilla := ObtenerPlantilla(FDM, Request, 'Aviso');
            Plantilla := ReplaceTag(Plantilla, 'Aviso', MSG_ERROR_NON_DEBT  + IntToStr(NumeroComprobante) );
            Plantilla := ReplaceTag(Plantilla, 'SiguientePaso', 'window.close()');
            Plantilla := ReplaceText(Plantilla , 'IrA('+''''+'window.close()'+''''+')', 'window.close()');
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'RedirigirBancoChile - ' + MSG_ERROR_NON_DEBT  + IntToStr(NumeroComprobante));   //SS_1305_MCA_20150612
            //Raise Exception.Create(MSG_ERROR_NON_DEBT + IntToStr(NumeroComprobante));
        end
        else
        begin
          //Inicio la Trasaccion
          //Fdm.Base.BeginTrans;                                                            //SS-966-NDR-20111128
          QueryExecute(Fdm.Base, 'BEGIN TRANSACTION trxRegistrarPagoBCH');                  //SS-966-NDR-20111128
          try

              // Dejar registro en Auditor�a para la transacci�n de pago y obtener el IDTRX.
              DescriError := MSG_REGISTRANDO_AUDITORIA;
              IDTRX := AgregarAuditoria(  fdm,
                                          TipoComprobante,
                                          NumeroComprobante,
                                          Importe * 100,
                                          CODIGO_BANCO_BANCOCHILE,
                                          StrToInt(CodigoSesionWebStr)
                                      );
              DescriError := EmptyStr;

              //Registro la entrada al banco
              DescriError := MSG_REGISTRANDO_AUDITORIA_BANCOCHILE;
              AgregarAuditoriaBDP_MPINIBCH(fdm, Request, IDTRX,CodigoServicioRecaudacion);    //SS-966-NDR-20120103
              DescriError := EmptyStr;

              //Acepto la transaccion
              //Fdm.Base.CommitTrans;                                                         //SS-966-NDR-20111128
              QueryExecute(Fdm.Base, 'COMMIT TRANSACTION trxRegistrarPagoBCH');               //SS-966-NDR-20111128
              RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'RedirigirBancoChile - Registrado en auditoria IDTRX: ' + IDTRX);   //SS_1305_MCA_20150612
          Except
               on E : Exception do begin
                   //Cancelo la Transaccion
                   //Fdm.Base.RollbackTrans;                                                  //SS-966-NDR-20111128
                   QueryExecute(Fdm.Base, 'ROLLBACK TRANSACTION trxRegistrarPagoBCH');        //SS-966-NDR-20111128
                   //lanzo una excepcion para para que sea manejada
                   Raise Exception.Create(E.Message);
               end;
          end;

          ComprobantesSeleccionados := Request.ContentFields.Values['COMPROBANTES'];														//SS_1304_MCA_20150709
          FDM.Base.Execute('UPDATE WEB_CV_DetalleDeudaConvenios SET Seleccionado = 0 WHERE CodigoSesion = '+ CodigoSesionWebStr);			//SS_1304_MCA_20150709
          FDM.Base.Execute('UPDATE WEB_CV_DetalleDeudaConvenios SET Seleccionado = 1 WHERE CodigoSesion = '+ CodigoSesionWebStr + ' AND NumeroComprobante IN ('+ ComprobantesSeleccionados +')');	//SS_1304_MCA_20150709
          // Reemplazar en data la variable TAG_IDTRX_BANCOCHILE con el valor obtenido al agregar los datos en Auditor�a.
          data := ReplaceTag(data, TAG_IDTRX_BANCOCHILE, IDTRX);


          // Reemplaza el dato del ServicioRecaudacion por el SRVREC que corresponde al BancoDeChile                      //SS-966-NDR-20111128
          data := ReplaceText(data,                                                                                       //SS-966-NDR-20111128
                              '<#SRVREC>',                                                                                //SS-966-NDR-20111128
                              CodigoServicioRecaudacion                                                                   //SS-966-NDR-20111128
                             );                                                                                           //SS-966-NDR-20111128
          data := ReplaceText(data,                                                                                       //SS-966-NDR-20111128
                              '>9144<',                                                                                   //SS-966-NDR-20111128
                              '>'+CodigoServicioRecaudacion+'<'                                                           //SS-966-NDR-20111128
                              );                                                                                          //SS-966-NDR-20111128

          //Cargo la plantilla
          Plantilla := ObtenerPlantilla(FDM, Request, 'PrepagoRedirectBancoChile');

          //Lleno los datos a Enviar a Banco De Chile
          Plantilla := ReplaceTag(Plantilla, 'data', data);
          RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'PrepagoRedirectBancoChile - Redirigido a Banco de Chile');   //SS_1305_MCA_20150612
        end;
    except
        on E : Exception do begin
            Plantilla := AtenderError(Request, FDM, E, 'RedirigirBancoChile');
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionWebStr), CodigoPersona, CodigoConvenio, 13, RUT, 'PrepagoRedirectBancoChile - ' + E.Message);            //SS_1305_MCA_20150612
            AgregarLog(FDM, 'RedirigirBancoChile - ' + E.Message
                + iif(DescriError <> EmptyStr, '. ' + DescriError, EmptyStr));

        end;
    end;
    //Muestro la pagina en el navegador
    Response.Content := Plantilla;
end;


//----------------------------------------------------------------
//SS966-NDR-20110810
// procedure wmConveniosWEBSalidaBCHAction
// Acci�n asociada a la URL invocada por el servicio BDP del banco, informando el resultado del pago
//----------------------------------------------------------------
procedure TwmConveniosWEB.wmConveniosWEBSalidaBCHAction(Sender: TObject;
                                                        Request: TWebRequest;
                                                        Response: TWebResponse;
                                                        var Handled: Boolean);
Resourcestring
    MSG_ERROR_QUERY             = 'Boton de Pago - Error al obtener el numero de comprobante : ';
    MSG_ERROR_VALUE_NOT_NUMERIC = 'Boton de Pago - Error el importe es invalido : ';
    MSG_ERROR_BDP               = 'Su transaccion no pudo ser realizada';
    MSG_ERROR_GET_SESSION_CODE  = 'Error al obtener el C�digo de Sesi�n Web';
    MSG_ERROR_GET_OFICINA_VIRTUAL = 'Error al carga la pagina de Exito';			//SS_1280_MCA_20150812
var
    IDTRX                       : String;
    NumeroComprobante           : string;
    Importe                     : Integer;
    sCodigodeRespuesta,sIndPago : String;
    CodigoSesion                : Integer;
    Plantilla                   : string;
    data                        : string;
    CodigoPersona, CodigoConvenio : integer;			//SS_1305_MCA_20150612
    RUT                         : String;				//SS_1305_MCA_20150612
    EsOficinaVirtual            : boolean;				//SS_1280_MCA_20150812
begin
    //Registro la notificacion del pago
    AgregarAuditoriaBDP_MPFINBCH(fdm, Request);
    AgregarLog(FDM, 'SalidaBCH - ' + Request.ContentFields.Text);                         //SS_1304_MCA_20150709
    try
        //Obtengo el IDTRX
        IDTRX       := ParseXMLField(Request.ContentFields.Text,'IDTRX');
        sIndPago    := ParseXMLField(Request.ContentFields.Text,'INDPAGO');

        // Obtener el CodigoSesion desde la tabla de Auditoria.
        try
           CodigoSesion := QueryGetValueInt(FDM.Base,
                'SELECT CodigoSesionWeb '+
                'FROM Web_CV_Auditoria WITH (NOLOCK) ' +
                'WHERE CodigoBanco = ' + IntToStr(CODIGO_BANCO_BANCOCHILE) + ' ' +
                'AND IDTRX = ' + QuotedStr(IdTrx));
        except
            on E : Exception do begin
                raise Exception.Create(MSG_ERROR_GET_SESSION_CODE + E.Message);
            end;
        end;

        try																				//SS_1280_MCA_20150812
           EsOficinaVirtual := (QueryGetValue(FDM.Base,									//SS_1280_MCA_20150812
                'SELECT EsOficinaVirtual '+												//SS_1280_MCA_20150812
                'FROM WEB_CV_Sesiones WITH (NOLOCK) ' +									//SS_1280_MCA_20150812
                'WHERE CodigoSesion = ' + IntToStr(CodigoSesion))='1');					//SS_1280_MCA_20150812
        except																			//SS_1280_MCA_20150812
            on E : Exception do begin													//SS_1280_MCA_20150812
                raise Exception.Create(MSG_ERROR_GET_OFICINA_VIRTUAL + E.Message);		//SS_1280_MCA_20150812
            end;																		//SS_1280_MCA_20150812
        end;																			//SS_1280_MCA_20150812

        //Obtengo el numero de comprobante que envie a cobrar
        try
            NumeroComprobante := QueryGetValue(FDM.Base,    'SELECT NumeroComprobante '+
                                                            'FROM Web_CV_Auditoria WITH (NOLOCK) '+
                                                            'WHERE CodigoBanco = '+ IntToStr(CODIGO_BANCO_BANCOCHILE) +' '+
                                                            'AND IDTRX = ' + QuotedStr(IdTrx)
                                                );
        Except
            on E : Exception do begin
                Raise Exception.Create(MSG_ERROR_QUERY + E.Message);
            end;
        end;

        //Obtengo el importe
        try
            Importe := StrToInt(ParseXMLField(Request.ContentFields.Text,'TOTAL'));
        Except
            on E : Exception do begin
                Raise Exception.Create(MSG_ERROR_VALUE_NOT_NUMERIC + E.Message);
            end;
        end;

        //Obtengo el codigo de Respuesta
        sCodigoDeRespuesta := ParseXMLField(Request.ContentFields.Text,'CODRET');

        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + IntToStr(CodigoSesion));			//SS_1305_MCA_20150612
        CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');								//SS_1305_MCA_20150612
        RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));							//SS_1305_MCA_20150612


        //Verifico la sesion
        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin

            //Verifico si fue aceptado por el banco
            if sCodigoDeRespuesta = '0000' then begin
                //Muestro el recibo
                if EsOficinaVirtual then											//SS_1280_MCA_20150812
                    Plantilla := ObtenerPlantilla(FDM, Request, 'ExitoBCH')			//SS_1280_MCA_20150812
                else Plantilla := ObtenerPlantilla(FDM, Request, 'ExitoBCH_SC');	//SS_1280_MCA_20150812

                Plantilla := replacetag(Plantilla, 'Fecha', formatdatetime('dd/mm/yy', FDM.NowBase())); // Rev 8.  SS_917
                Plantilla := replacetag(Plantilla, 'TipoComprobanteDesc','Nota de Cobro');
                Plantilla := replacetag(Plantilla, 'NroComprobante', NumeroComprobante);
                Plantilla := replacetag(Plantilla, 'Importe', formatfloat('#,###', Importe));
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WEBSalidaBCH - Pago Existoso Banco de Chile');   //SS_1305_MCA_20150612
            end else if ((sCodigoDeRespuesta = '0001') and (sIndPago='S')) then
            begin
                data := '<?xml version=''1.0'' ?><MPCON><IDCOM>7649613001</IDCOM><IDTRX><#IdTrxBCH></IDTRX><TOTAL><#ImporteSinDecimales></TOTAL><IDREG><#IdRegBCH></IDREG></MPCON>';
                data := ReplaceTag(data, 'IdRegBCH', ParseXMLField(Request.ContentFields.Text,'IDREG'));
                data := ReplaceTag(data, 'IdTrxBCH', IDTRX);
                data := ReplaceTag(data, 'ImporteSinDecimales', IntToStr(Importe));
                Plantilla := ObtenerPlantilla(FDM, Request, 'PrepagoRedirectBancoChileMPCON');
                Plantilla := ReplaceTag(Plantilla, 'data', data);
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PrepagoRedirectBancoChileMPCON - Re-Enviado a Banco de Chile');   //SS_1305_MCA_20150612
            end else begin
                //Informo el rechazo
                Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_ERROR_BDP+sCodigoDeRespuesta);
                if EsOficinaVirtual then														//SS_1280_MCA_20150812
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl')	//SS_1280_MCA_20150812
                else Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'logout');			//SS_1280_MCA_20150812

                Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WEBSalidaBCH - ' + MSG_ERROR_BDP+sCodigoDeRespuesta);   //SS_1305_MCA_20150612
            end;
        end;
    except
        on E : Exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'FracasoBCH');
            AgregarLog(FDM, 'FracasoBCH - ' + E.Message);
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WEBSalidaBCH - ' + E.Message);   //SS_1305_MCA_20150612
        end;
    end;
    //Muestro el comprobante en el navegador
    Plantilla := ReplaceText(Plantilla, '</form>', '<input type="HIDDEN" name="ACTION" value="VP"></form>');
    Response.Content := Plantilla;
    Handled := true;
end;

//----------------------------------------------------------------
//SS966-NDR-20110810
// procedure wmConveniosWEBNotificacionBCHAction
// Accion asociada URL invocada por el servicio BDP del banco, notificando el pago
// realizado en el sitio del banco
//----------------------------------------------------------------
procedure TwmConveniosWEB.wmConveniosWEBNotificacionBCHAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
resourcestring
    MSG_ERROR_QUERY     = 'Boton de Pago - Error al obtener el numero de comprobante : ';
    MSG_ERROR_NUMCOMP   = 'Boton de Pago - El numero de comprobante no es numerico : ';
    MSG_ERROR_TOTAL     = 'Boton de Pago - El importe no es numerico : ';
    MSG_ERROR_CODRET    = 'Boton de Pago - El Codigo de Respuesta no es numerico : ';
    MSG_ERROR_BANK      = 'Boton de Pago - Ocurrio un error en el Banco y no hubo Pago : ';
    MSG_ERROR_INTERNAL  = 'Boton de Pago - Error al intentar grabar un pago. El procedimiento interno lo rechazo con el mensaje: ';
    MSG_ERROR_PAYMENT   = 'Boton de Pago - Error al intentar grabar un pago : ';
    MSG_ERROR_ANSWER_BEHIND_SCHEDULE = 'Boton de Pago - La respuesta del banco llego despues de %d segundos ';
    MSG_ERROR_GET_SESSION_CODE = 'Error al obtener el C�digo de Sesi�n Web';		//SS_1304_MCA_20150709
const
    STR_CODIGO_ACEPTADO = '0000';

    STR_NK = 'NK';

    //Respuesta al banco
    STR_ACEPTADO  = '<NOTIFICA>OK</NOTIFICA>';
    STR_RECHAZADO = '<NOTIFICA>NOK</NOTIFICA>';

    STR_TRANSACCION = 'Transaccion: ';
    STR_COMPROBANTE = 'Comprobante: ';
    STR_IMPORTE = 'Importe: ';
    STR_RESPUESTA = 'Respuesta Banco: ';
    STR_NOTIFICACION_BCH = 'NotificacionBCH - ';
var
    IDTRX : String;
    sNumeroComprobante : String;
    sImporte : String;
    sCodigodeRespuesta : String;
    IdentificadorOperacion : String;
    NumeroComprobante : INT64;
    Importe : INT64;
    CodigodeRespuesta : Integer;
    FechaProceso: TDateTime;
    sError : string;
    CodigoSesion, CodigoConvenio, CodigoPersona:Integer;			//SS_1304_MCA_20150709
    RUT: String;													//SS_1305_MCA_20150612
begin
    AgregarLog(FDM, 'NotificacionBCH - ' + Request.ContentFields.Text);                         //SS_1304_MCA_20150709
      try
        //Recupero y parseo el xml
        //Obtengo el identificador de transaccion
        IDTRX := ParseXMLField(Request.ContentFields.Text,'IDTRX');

        try																				//SS_1304_MCA_20150709
           CodigoSesion := QueryGetValueInt(FDM.Base,									//SS_1304_MCA_20150709
                'SELECT CodigoSesionWeb '+												//SS_1304_MCA_20150709
                'FROM Web_CV_Auditoria WITH (NOLOCK) ' +								//SS_1304_MCA_20150709
                'WHERE CodigoBanco = ' + IntToStr(CODIGO_BANCO_BANCOCHILE) + ' ' +		//SS_1304_MCA_20150709
                'AND IDTRX = ' + QuotedStr(IdTrx));										//SS_1304_MCA_20150709
        except																			//SS_1304_MCA_20150709
            on E : Exception do begin													//SS_1304_MCA_20150709
                raise Exception.Create(MSG_ERROR_GET_SESSION_CODE + E.Message);			//SS_1304_MCA_20150709
            end;																		//SS_1304_MCA_20150709
        end;																			//SS_1304_MCA_20150709

        //Obtengo el numero de comprobante que envie a cobrar
        try
            sNumeroComprobante := QueryGetValue(FDM.Base,   'SELECT NumeroComprobante '+
                                                            'FROM Web_CV_Auditoria WITH (NOLOCK) '+
                                                            'WHERE CodigoBanco = '+ IntToStr(CODIGO_BANCO_BANCOCHILE) +' '+
                                                            'AND IDTRX = ' + QuotedStr(IdTrx)
                                                );
        Except
            on E : Exception do begin
                Raise Exception.Create(MSG_ERROR_QUERY + E.Message);
            end;
        end;

        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + IntToStr(CodigoSesion));			//SS_1305_MCA_20150612
        CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');								//SS_1305_MCA_20150612
        RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));							//SS_1305_MCA_20150612


        //Obtengo el importe
        sImporte := ParseXMLField(Request.ContentFields.Text,'TOTAL');

        //Obtengo el codigo de respuesta
        sCodigoDeRespuesta := ParseXMLField(Request.ContentFields.Text,'CODRET');

        //Armo una linea con la informacion de la operacion a realizar, asi si sucede un error podremos identificar con mas certeza la operacion que fallo y el motivo del fallo
        IdentificadorOperacion := STR_TRANSACCION + IdTrx + ' ' + STR_COMPROBANTE + sNumeroComprobante + ' '+ STR_IMPORTE + sImporte + ' ' + STR_RESPUESTA +  sCodigodeRespuesta;

        //verifico si la respuesta llego a tiempo
        If QueryGetValueInt(fdm.Base,'SELECT DBO.WEB_RespuestaValida_BDP (' + QuotedStr(IDTRX) + ',' + 'GETDATE()' + ')' ) = 0 then begin
            //creo una excepcion informado que la respuesta llego tarde
            Raise Exception.Create(format(MSG_ERROR_ANSWER_BEHIND_SCHEDULE,[ObtenerValorParametroInt(fdm, 'WEB_CANT_SEG_VALIDEZ_RESPUESTA', 99)]));
        end;

        //Obtengo el numero de comprobante
        try
            NumeroComprobante := StrToINT64(sNumeroComprobante);
        except
            Response.Content := STR_RECHAZADO;
            EventLogReportEvent(elError, MSG_ERROR_NUMCOMP + sNumeroComprobante + ' ' + IdentificadorOperacion, '');
            AgregarLog(FDM, STR_NOTIFICACION_BCH + MSG_ERROR_NUMCOMP + sNumeroComprobante + ' ' + IdentificadorOperacion);
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WEBNotificacionBCH - ' +  MSG_ERROR_NUMCOMP + sNumeroComprobante + ' ' + IdentificadorOperacion);   //SS_1305_MCA_20150612
            Exit;
        end;

        //Actualizo el registro de auditoria
        ActualizarConfirmacionAuditoria(FDM, IDTRX, CODIGO_BANCO_BANCOCHILE, SCodigoDeRespuesta, IIF(SCodigoDeRespuesta = STR_CODIGO_ACEPTADO, TRUE, FALSE));

        //Guardo el mensaje recibido del banco en auditoria
        //Transformo esto en funci�n. Si sale por false quiere decir que el mensaje est� duplicado, o sea,
        //el banco me est� mandando 2 veces el mismo pago como evento.
        if not ActualizarRespuestaBancoAuditoria(FDM, IDTRX, Copy(Request.ContentFields.Text, 1, 1000), CODIGO_BANCO_BANCOCHILE) then begin
            Response.Content := STR_ACEPTADO;
            Exit;
        end;

        //Obtengo el importe
        try
            Importe := StrToINT64(sImporte) * 100;
        except
            Response.Content := STR_RECHAZADO;
            EventLogReportEvent(elError, MSG_ERROR_TOTAL + sImporte + ' ' + IdentificadorOperacion, '');
            ActualizarGeneroReciboAuditoria(FDM, IDTRX, FALSE, MSG_ERROR_TOTAL + sImporte + ' ' + IdentificadorOperacion, CODIGO_BANCO_BANCOCHILE);
            AgregarLog(FDM, STR_NOTIFICACION_BCH + MSG_ERROR_TOTAL + sImporte + ' ' + IdentificadorOperacion);
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WEBNotificacionBCH - ' +  MSG_ERROR_TOTAL + sImporte + ' ' + IdentificadorOperacion);   //SS_1305_MCA_20150612
            Exit;
        end;

        //Obtengo el codigo de respuesta
        try
            CodigoDeRespuesta := StrToINT64(sCodigoDeRespuesta);
        except
            Response.Content := STR_RECHAZADO;
            EventLogReportEvent(elError, MSG_ERROR_CODRET + sCodigoDeRespuesta + ' ' + IdentificadorOperacion, '');
            ActualizarGeneroReciboAuditoria(FDM, IDTRX, FALSE, MSG_ERROR_CODRET + sCodigoDeRespuesta + ' ' + IdentificadorOperacion, CODIGO_BANCO_BANCOCHILE);
            AgregarLog(FDM, STR_NOTIFICACION_BCH + MSG_ERROR_CODRET + sCodigoDeRespuesta + ' ' + IdentificadorOperacion);
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WEBNotificacionBCH - ' +  MSG_ERROR_CODRET + sCodigoDeRespuesta + ' ' + IdentificadorOperacion);   //SS_1305_MCA_20150612
            Exit;
        end;

        //Verifico si fue aceptado por el banco
        if sCodigoDeRespuesta <> STR_CODIGO_ACEPTADO then begin
                Response.Content := STR_ACEPTADO; //porque el banco ya me lo envio rechazado
                EventLogReportEvent(elError, MSG_ERROR_BANK + sCodigodeRespuesta + ' ' + IdentificadorOperacion, '');
                ActualizarGeneroReciboAuditoria(FDM, IDTRX, FALSE, MSG_ERROR_BANK + sCodigodeRespuesta + ' ' + IdentificadorOperacion, CODIGO_BANCO_BANCOCHILE);
                AgregarLog(FDM, STR_NOTIFICACION_BCH + MSG_ERROR_BANK + sCodigodeRespuesta + ' ' + IdentificadorOperacion);
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WEBNotificacionBCH - ' +  MSG_ERROR_BANK + sCodigodeRespuesta + ' ' + IdentificadorOperacion);   //SS_1305_MCA_20150612
                Exit;
              end;
        //Registro en la base de datos
        FechaProceso := QueryGetValueDateTime(fdm.Base, 'SELECT GETDATE()');

        //if not RegistrarPagoComprobanteBCH(fdm, STR_NK, NumeroComprobante, Importe, CodigodeRespuesta, FechaProceso, sError) then begin                          //SS-966-NDR-20111128

        if not RealizarPagoComprobantesBCH(fdm, CodigoSesion, CodigodeRespuesta, FechaProceso, sError) then begin		//SS_1304_MCA_20150709
        //    if not RegistrarPagoPendienteComprobanteBCH(fdm, STR_NK, NumeroComprobante, Importe, CodigodeRespuesta, FechaProceso, sError) then begin             //SS_1304_MCA_20150709      //SS-966-NDR-20111128	
              Response.content := STR_RECHAZADO;
              EventLogReportEvent(elError, MSG_ERROR_INTERNAL + sError + ' ' + IdentificadorOperacion, '');
              ActualizarGeneroReciboAuditoria(FDM, IDTRX, FALSE, MSG_ERROR_INTERNAL + sError + ' ' + IdentificadorOperacion, CODIGO_BANCO_BANCOCHILE);
              AgregarLog(FDM, STR_NOTIFICACION_BCH + MSG_ERROR_INTERNAL + sError + ' ' + IdentificadorOperacion);
              RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WEBNotificacionBCH - ' +  MSG_ERROR_INTERNAL + sError + ' ' + IdentificadorOperacion);   //SS_1305_MCA_20150612
              Exit;
        end;

        RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WEBNotificacionBCH - se ha realiza el pago existosamente');                //SS_1305_MCA_20150612

        //El recibo se creo con exito
        ActualizarGeneroReciboAuditoria(FDM, IDTRX, TRUE, '', CODIGO_BANCO_BANCOCHILE);

        //Registro la notificacion del pago
        AgregarAuditoriaBDP_MPOUTBCH(fdm, Request);
        //Envio notificacion de aceptacion de pago
        Response.Content := STR_ACEPTADO;
    except
        on E : Exception do begin
            Response.Content:= STR_RECHAZADO;
            EventLogReportEvent(elError, MSG_ERROR_PAYMENT + E.Message + ' ' + IdentificadorOperacion, '');
            AgregarLog(FDM, STR_NOTIFICACION_BCH + MSG_ERROR_PAYMENT + E.Message + ' ' + IdentificadorOperacion);
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WEBNotificacionBCH - ' + STR_NOTIFICACION_BCH + MSG_ERROR_PAYMENT + E.Message + ' ' + IdentificadorOperacion);                //SS_1305_MCA_20150612
        end;
  	end;
end;

function TwmConveniosWEB.AgregarAuditoria_AdhesionPAK(
	pNumeroDocumento, pPatente, pEMail: string;
    pCodigoPersona, pCumpleRequisitos, pAdheridoEnvioEMail, pImpresionContrato, pFirmaContrato, pCodigoAuditoria: Integer): Integer;
	var
        spAgregarAuditoria_AdhesionPAK: TADOStoredProc;
        TransaccionLocal: Boolean;
begin
	try
    	try
            spAgregarAuditoria_AdhesionPAK := TADOStoredProc.Create(nil);
            TransaccionLocal := False;

            with spAgregarAuditoria_AdhesionPAK do begin
                Connection    := FDM.Base;
                ProcedureName := 'AgregarAuditoria_AdhesionPAK';

                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@Usuario').Value                := 'WEB';
                	ParamByName('@NumeroDocumento').Value        := IIf(pNumeroDocumento = EmptyStr, null, pNumeroDocumento);
                	ParamByName('@Patente').Value                := IIf(pPatente = EmptyStr, null, pPatente);
                	ParamByName('@CodigoPersona').Value          := pCodigoPersona;
                	ParamByName('@CumpleRequisitos').Value       := pCumpleRequisitos;
                	ParamByName('@EMail').Value        	         := IIf(pEMail = EmptyStr, null, pEMail);
                	ParamByName('@AdheridoEnvioEMail').Value     := IIf(pAdheridoEnvioEMail = -1, null, pAdheridoEnvioEMail);
                	ParamByName('@ImpresionContrato').Value      := IIf(pImpresionContrato = -1, null, pImpresionContrato);
                	ParamByName('@FirmaContrato').Value          := IIf(pFirmaContrato = -1, null, pFirmaContrato);
                	ParamByName('@AdhesionPreInscripcion').Value := null;
                	ParamByName('@IDCodigoAuditoria').Value  	 := IIf(pCodigoAuditoria = -1, null, pCodigoAuditoria);

                    if not FDM.Base.InTransaction then begin
                        FDM.Base.BeginTrans;
                        TransaccionLocal := True;
                    end;

	                ExecProc;

                    if TransaccionLocal then begin
                    	if FDM.Base.InTransaction then FDM.Base.CommitTrans;
                    end;

                    Result := ParamByName('@IDCodigoAuditoria').Value;
                end;
            end;
        except
        	on e: Exception do begin
                if TransaccionLocal then begin
                    if FDM.Base.InTransaction then FDM.Base.RollbackTrans;
                end;

            	raise Exception.Create(e.Message);
            end;
        end;
    finally
    	if Assigned(spAgregarAuditoria_AdhesionPAK) then FreeAndNil(spAgregarAuditoria_AdhesionPAK);
    end;
end;


procedure TwmConveniosWEB.wmConveniosWEBValidarInscripcionAraucoTAGAction(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
    var
        Plantilla,
        Impedimentos,
        NumeroDocumento,
        Nombre,
        EMail,
        EMailConfirmacion: String;
        CodigoSesion,
        CodigoAuditoria,
        CodigoPersona,
        EnvioPorEMail,
        EnvioPorMailSeleccionado: Integer;
        spValidarRestriccionesAdhesionPAK: TADOStoredProc;
	const
    	cSQLValidarPersonaAdheridaPA = 'SELECT 1 WHERE dbo.ValidarPersonaAdheridaPAK(%d) = 1';
        cSQLObtenerEmailPersona      = 'SELECT dbo.ObtenerEmailPersona(%d)';
        cSQLValidarConveniosPersonaEnvioPorEMail = 'SELECT CASE dbo.ValidarConveniosPersonaEnvioPorEMail(%d) WHEN 1 THEN 1 ELSE 0 END';
        cSQLValidarPersonaInscritaWebPA = 'SELECT 1 WHERE dbo.ValidarPersonaInscritaWebPAK(%d) = 1';

begin
    if Request.MethodType = mtGet then try
		CodigoSesion := StrToIntDef(Request.QueryFields.Values[TAG_CODIGO_SESION], -1);

        try
	        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin

                CodigoPersona   := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));
                Nombre          := QueryGetValue(fdm.base, 'SELECT RTRIM(Nombre) AS Nombre FROM Personas WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));
                EnvioPorEMail   := QueryGetValueInt(FDM.Base, Format(cSQLValidarConveniosPersonaEnvioPorEMail, [CodigoPersona]));
                NumeroDocumento := QueryGetValue(fdm.base, 'SELECT RTRIM(NumeroDocumento) AS NumeroDocumento FROM Personas WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));


                spValidarRestriccionesAdhesionPAK := TADOStoredProc.Create(nil);

                if QueryGetValueInt(FDM.Base, Format(cSQLValidarPersonaAdheridaPA, [CodigoPersona])) = 0 then begin
	                if QueryGetValueInt(FDM.Base, Format(cSQLValidarPersonaInscritaWebPA, [CodigoPersona])) = 0 then begin

                        with spValidarRestriccionesAdhesionPAK do begin
                            Connection := FDM.Base;
                            ProcedureName := 'ValidarRestriccionesAdhesionPAK';
                            Parameters.Refresh;
                            Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
                            Parameters.ParamByName('@ConDetalle').Value := 0;
                            Open;

                            if Parameters.ParamByName('@PuedeAdherirsePA').Value = 0 then begin

                                AgregarAuditoria_AdhesionPAK(
                                    NumeroDocumento,
                                    EmptyStr,
                                    EmptyStr,
                                    CodigoPersona,
                                    0,
                                    -1,
                                    -1,
                                    -1,
                                    -1);

                                Plantilla := ObtenerPlantilla(FDM, Request, 'AraucoTAGImpedimentos');
                                Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));

                                while not eof do begin
                                    Impedimentos := Impedimentos + '* ' + FieldByName('Descripcion').AsString + '<br><br>';
                                    Next;
                                end;

                                Plantilla := ReplaceTag(Plantilla, 'Usuario', Nombre);
                                Plantilla := ReplaceTag(Plantilla, 'Impedimentos', Impedimentos);
                            end
                            else begin

                                CodigoAuditoria :=
                                    AgregarAuditoria_AdhesionPAK(
                                        NumeroDocumento,
                                        EmptyStr,
                                        EmptyStr,
                                        CodigoPersona,
                                        1,
                                        -1,
                                        -1,
                                        -1,
                                        -1);

                                EMail     := QueryGetValue(FDM.Base, Format(cSQLObtenerEmailPersona, [CodigoPersona]));

                                Plantilla := ObtenerPlantilla(FDM, Request, 'AraucoTAGEMail');
                                Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
                                Plantilla := ReplaceTag(Plantilla, 'CodigoAuditoria', IntToStr(CodigoAuditoria));
                                Plantilla := ReplaceTag(Plantilla, 'NumeroDocumento', NumeroDocumento);
                                Plantilla := ReplaceTag(Plantilla, 'Usuario', Nombre);
                                Plantilla := ReplaceTag(Plantilla, 'EMail', EMail);
                                Plantilla := ReplaceTag(Plantilla, 'ValorCheckOptionBoleta', IntToStr(EnvioPorEMail));
                            end;
                        end;
                    end
                    else begin

                        AgregarAuditoria_AdhesionPAK(
                            NumeroDocumento,
                            EmptyStr,
                            EmptyStr,
                            CodigoPersona,
                            3,
                            -1,
                            -1,
                            -1,
                            -1);

                        Plantilla := ObtenerPlantilla(FDM, Request, 'AraucoTAGYaInscrito');
                        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
                        Plantilla := ReplaceTag(Plantilla, 'Usuario', Nombre);
                    end;
                end
                else begin

                        AgregarAuditoria_AdhesionPAK(
                            NumeroDocumento,
                            EmptyStr,
                            EmptyStr,
                            CodigoPersona,
                            2,
                            -1,
                            -1,
                            -1,
                            -1);

                        Plantilla := ObtenerPlantilla(FDM, Request, 'AraucoTAGYaAdherido');
                        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
                        Plantilla := ReplaceTag(Plantilla, 'Usuario', Nombre);
                end;
        	end;
        except
            on e: Exception do begin
                Plantilla := AtenderError(Request, FDM, e, 'AraucoTAGEMail');
            end;
        end;

        Response.Content := Plantilla;
        Handled := True;
    finally
    	if Assigned(spValidarRestriccionesAdhesionPAK) then FreeAndNil(spValidarRestriccionesAdhesionPAK);
    end
    else try
		CodigoSesion := StrToIntDef(Request.ContentFields.Values[TAG_CODIGO_SESION], -1);

        if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin

            EMail		      		 := Trim(Request.ContentFields.Values['EMail']);
            EMailConfirmacion 		 := Trim(Request.ContentFields.Values['EMailConfirmacion']);
            EnvioPorMailSeleccionado := StrToInt(Request.ContentFields.Values['BoletaPorEMail']);
            CodigoAuditoria          := StrToInt(Request.ContentFields.Values['CodigoAuditoria']);
            NumeroDocumento          := Request.ContentFields.Values['NumeroDocumento'];

            if not IsValidEMailCN(EMail) then begin
                Plantilla := ObtenerPlantilla(FDM, Request, 'AraucoTAGError');
                Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
                Plantilla := ReplaceTag(Plantilla, 'Error', 'El formato del E-Mail indicado No es correcto. Debe indicar un E-Mail v�lido para seguir con el proceso de Adhesi�n al Servicio Arauco TAG.');
            end
            else if (EMail <> EMailConfirmacion) then begin
                Plantilla := ObtenerPlantilla(FDM, Request, 'AraucoTAGError');
                Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
                Plantilla := ReplaceTag(Plantilla, 'Error', 'No coincide la Confirmaci�n con el E-Mail indicado. Debe confirmar el E-Mail indicado, para seguir con el proceso de Adhesi�n al Servicio Arauco TAG.');
            end
            else begin

                AgregarAuditoria_AdhesionPAK(
                    Trim(NumeroDocumento),
                    EmptyStr,
                    EMail,
                    CodigoPersona,
                    1,
                    EnvioPorMailSeleccionado,
                    -1,
                    -1,
                    CodigoAuditoria);

                Plantilla := ObtenerPlantilla(FDM, Request, 'AraucoTAGConfirma');
                Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
                Plantilla := ReplaceTag(Plantilla, 'CodigoAuditoria', IntToStr(CodigoAuditoria));
                Plantilla := ReplaceTag(Plantilla, 'NumeroDocumento', NumeroDocumento);
                Plantilla := ReplaceTag(Plantilla, 'EMail', EMail);
                Plantilla := ReplaceTag(Plantilla, 'EnvioPorMailSeleccionado', IntToStr(EnvioPorMailSeleccionado));
            end;
        end;

        Response.Content := Plantilla;
        Handled := True;
    except
        on e: exception do begin
            Response.Content := AtenderError(Request, FDM, e, 'AraucoTAGEMail');
            Handled := True;
        end;
    end;
end;

procedure TwmConveniosWEB.wmConveniosWEBRealizarInscripccionAraucoTAGAction(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
	var
    	CodigoSesion,
        CodigoPersona,
        CodigoAuditoria,
        EnvioPorEMail,
        EnvioPorMailSeleccionado: Integer;
        Plantilla,
        EMail,
        EMailPersona,
        Nombre,
        NumeroDocumento: String;

        CodigoMedioComunicacionEmailPersona: Integer;
        spActualizarMedioComunicacionPersona,
        spActualizarConveniosPersonaEnvioPorEMail,
        spAgregarInscripcionWebAdhesionPAK: TADOStoredProc;

	const
        cSQLObtenerCodigoMedioComunicacionEmailPersona = 'SELECT dbo.ObtenerCodigoMedioComunicacionEmailPersona(%d)';
    	cSQLObtenerEmailPersona = 'SELECT dbo.ObtenerEmailPersona(%d)';
        cSQLValidarConveniosPersonaEnvioPorEMail = 'SELECT CASE dbo.ValidarConveniosPersonaEnvioPorEMail(%d) WHEN 1 THEN 1 ELSE 0 END';
begin
    try
        try
           	spActualizarMedioComunicacionPersona      := TADOStoredProc.Create(nil);
            spActualizarConveniosPersonaEnvioPorEMail := TADOStoredProc.Create(nil);
            spAgregarInscripcionWebAdhesionPAK        := TADOStoredProc.Create(nil);

			CodigoSesion  := StrToIntDef(Request.ContentFields.Values[TAG_CODIGO_SESION], -1);

            if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin

                EMail		      		 := Trim(Request.ContentFields.Values['EMail']);
                EnvioPorMailSeleccionado := StrToInt(Request.ContentFields.Values['EnvioPorMailSeleccionado']);
                CodigoAuditoria          := StrToInt(Request.ContentFields.Values['CodigoAuditoria']);
	            NumeroDocumento          := Request.ContentFields.Values['NumeroDocumento'];


                CodigoPersona := QueryGetValueInt(FDM.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));
                EMailPersona  := QueryGetValue(FDM.Base, Format(cSQLObtenerEmailPersona, [CodigoPersona]));
                EnvioPorEMail := QueryGetValueInt(FDM.Base, Format(cSQLValidarConveniosPersonaEnvioPorEMail, [CodigoPersona]));

            	CodigoMedioComunicacionEmailPersona := QueryGetValueInt(FDM.Base, Format(cSQLObtenerCodigoMedioComunicacionEmailPersona, [CodigoPersona]));

                FDM.Base.BeginTrans;

                if EMailPersona <> EMail then begin

                    with spActualizarMedioComunicacionPersona do begin

                        Connection    := FDM.Base;
                        ProcedureName := 'ActualizarMedioComunicacionPersona';
                        Parameters.Refresh;
                        with Parameters do begin
                            ParamByName('@CodigoTipoMedioContacto').Value := 1;
                            ParamByName('@CodigoArea').Value              := null;
                            ParamByName('@Valor').Value                   := EMail;
                            ParamByName('@Anexo').Value                   := null;
                            ParamByName('@CodigoDomicilio').Value         := null;
                            ParamByName('@HorarioDesde').Value            := null;
                            ParamByName('@HorarioHasta').Value            := null;
                            ParamByName('@Observaciones').Value           := null;
                            ParamByName('@CodigoPersona').Value           := CodigoPersona;
                            ParamByName('@Principal').Value               := 0;
                            ParamByName('@EstadoVerificacion').Value      := null;
                            ParamByName('@CodigoMedioComunicacion').Value := IIf(CodigoMedioComunicacionEmailPersona = 0, null, CodigoMedioComunicacionEmailPersona);
                        end;

                        ExecProc;
                    end;
                end;

                if EnvioPorEMail <> EnvioPorMailSeleccionado then begin

                    with spActualizarConveniosPersonaEnvioPorEMail do begin
                        Connection    := FDM.Base;
                        ProcedureName := 'ActualizarConveniosPersonaEnvioPorEMail';
                        Parameters.Refresh;
                        with Parameters do begin
                            ParamByName('@CodigoPersona').Value := CodigoPersona;
                            ParamByName('@Usuario').Value       := UsuarioSistema;
                        end;

                        ExecProc;
                    end;
                end;

                with spAgregarInscripcionWebAdhesionPAK do begin
                    Connection    := FDM.Base;
                    ProcedureName := 'AgregarInscripcionWebAdhesionPAK';
                    Parameters.Refresh;
                    with Parameters do begin
                        ParamByName('@CodigoPersona').Value 	   := CodigoPersona;
                        ParamByName('@EMail').Value 			   := EMail;
                        ParamByName('@AdheridoEnvioEMail').Value   := EnvioPorMailSeleccionado;
                        ParamByName('@Usuario').Value       	   := 'WEB';
                    end;

                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                    	raise Exception.Create('Se produjo un error al realizar la Inscripci�n de Adhesi�n al Servicio Arauco TAG. Por favor reint�ntelo m�s tarde.');
                    end;
                end;

                AgregarAuditoria_AdhesionPAK(
                    Trim(NumeroDocumento),
                    EmptyStr,
                    EmptyStr,
                    CodigoPersona,
                    1,
                    -1,
                    1,
                    -1,
                    CodigoAuditoria);

                if FDM.Base.InTransaction then FDM.Base.CommitTrans;

	            Nombre    := QueryGetValue(fdm.base, 'SELECT RTRIM(Nombre) AS Nombre FROM Personas WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));

                Plantilla := ObtenerPlantilla(FDM, Request, 'AraucoTAGExito');
                Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
                Plantilla := ReplaceTag(Plantilla, 'Usuario', Nombre);
                Plantilla := ReplaceTag(Plantilla, 'Envio', 'enviado');
            end;
        except
            on e: Exception do begin
                if FDM.Base.InTransaction then FDM.Base.RollbackTrans;

                Plantilla := AtenderError(Request, FDM, e, 'AraucoTAGConfirma');
            end;
        end;

        Response.Content := Plantilla;
        Handled := True;
    finally
    	if Assigned(spActualizarMedioComunicacionPersona) then FreeAndNil(spActualizarMedioComunicacionPersona);
    	if Assigned(spActualizarConveniosPersonaEnvioPorEMail) then FreeAndNil(spActualizarConveniosPersonaEnvioPorEMail);
    	if Assigned(spAgregarInscripcionWebAdhesionPAK) then FreeAndNil(spAgregarInscripcionWebAdhesionPAK);
    end;
end;


procedure TwmConveniosWEB.wmConveniosWEBReenviarInscripcionAraucoTAGAction(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
	var
    	CodigoSesion,
        CodigoPersona: Integer;
        Plantilla,
        Nombre: string;

        spEMAIL_AgregarMensajeContratoAdhesionPAK: TADOStoredProc;
begin
	try
        try
        	spEMAIL_AgregarMensajeContratoAdhesionPAK := TADOStoredProc.Create(nil);

			CodigoSesion  := StrToIntDef(Request.ContentFields.Values[TAG_CODIGO_SESION], -1);

            if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin

                CodigoPersona := QueryGetValueInt(FDM.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));

                with spEMAIL_AgregarMensajeContratoAdhesionPAK do begin
                    Connection    := FDM.Base;
                    ProcedureName := 'EMAIL_AgregarMensajeContratoAdhesionPAK';

                    Parameters.Refresh;
                    with Parameters do begin
                        ParamByName('@CodigoPersona').Value 	   := CodigoPersona;
                        ParamByName('@EMail').Value 			   := null;
                        ParamByName('@AdheridoEnvioEMail').Value   := null;
                        ParamByName('@Usuario').Value       	   := 'WEB';
                    end;

                    FDM.Base.BeginTrans;

                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                    	raise Exception.Create('Se produjo un error al reenviar el e-mail de Inscripci�n de Adhesi�n al Servicio Arauco TAG. Por favor reint�ntelo m�s tarde.');
                    end;

                    if FDM.Base.InTransaction then FDM.Base.CommitTrans;

                    Nombre    := QueryGetValue(fdm.base, 'SELECT RTRIM(Nombre) AS Nombre FROM Personas WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));

                    Plantilla := ObtenerPlantilla(FDM, Request, 'AraucoTAGExito');
                    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
                    Plantilla := ReplaceTag(Plantilla, 'Usuario', Nombre);
                    Plantilla := ReplaceTag(Plantilla, 'Envio', 'reenviado');
                end;
            end;
        except
            on e: Exception do begin
                if FDM.Base.InTransaction then FDM.Base.RollbackTrans;

                Plantilla := AtenderError(Request, FDM, e, 'AraucoTAGConfirma');
            end;
        end;

        Response.Content := Plantilla;
        Handled := True;
    finally
    	if Assigned(spEMAIL_AgregarMensajeContratoAdhesionPAK) then FreeAndNil(spEMAIL_AgregarMensajeContratoAdhesionPAK);
    end;
end;

//----------------------------------------------------------------
//SS_1248_MCA_20150602
// procedure TwmConveniosWEB.OlvidoPassword
// procedimiento encargado de recuperar la clave secreta a traves del email. valida que ingrese el rut y el correo, el correo se valida que sea el mismo
// que tiene registrado en el sistema
//----------------------------------------------------------------
procedure TwmConveniosWEB.OlvidoPassword(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
resourcestring
//    MSG_CLAVE_ENVIADA       = 'Su clave de acceso ha sido enviada al correo electr�nico registrado por Costanera Norte';  										// SS_1327_MBE_20150630
    MSG_CLAVE_ENVIADA       = 'Su clave de acceso ha sido enviada al correo electr�nico registrado por Vespucio Sur';  	 										// SS_1327_MBE_20150630
//    MSG_NO_RECIBE_CLAVE     = 'El correo electr�nico indicado no coincide con el registrado por Costanera Norte, favor comun�quese con nuestro Call Center';	 // SS_1327_MBE_20150630
    MSG_NO_RECIBE_CLAVE     = 'El correo electr�nico indicado no coincide con el registrado por Vespucio Sur, favor comun�quese con nuestro Call Center';			 // SS_1327_MBE_20150630
    MSG_NO_RECIBE_CLAVE_EMAIL = 'Estimado Cliente, ud. no puede recibir su clave por E-mail, favor comun�quese con nuestro Call Center.';
var
    Error,
    Plantilla, Password: string;
    RUT, Email, EmailRegistrado: string;
    RecibeClavePorMail: boolean ;
    CodigoDocumento   : string  ;
    CodigoCliente : Integer;
    CodigoFirma : Integer   ;
    CodigoPlantilla : Integer ;
    CodigoConvenio: integer;
    NuevoPass  : string;				//SS_1248_MCA_20150602
begin
    try
        RUT := Request.ContentFields.Values[TAG_RUT] + Request.ContentFields.Values[TAG_RUTDV];
        RUT := Padl(Trim(RUT), 9, '0');
        Email := Request.ContentFields.Values['email'];
        CodigoDocumento := 'RUT';
        RecibeClavePorMail := StrToBool(QueryGetValue(fdm.Base, Format('SELECT dbo.ObtenerRecibeClavePorMail(%s, %s)', [quotedstr(RUT) ,quotedstr(CodigoDocumento) ])));
        CodigoCliente :=  QueryGetValueInt(fdm.Base, 'SELECT CodigoPersona FROM Personas P  WITH (NOLOCK) WHERE NumeroDocumento = ' + quotedstr(RUT));		//SS_1305_MCA_20150612
        CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoCliente) + ', 0)');					//SS_1305_MCA_20150612

        if RecibeClavePorMail  then begin
            if QueryGetValueInt(fdm.Base, 'SELECT COUNT(*) FROM Personas P  WITH (NOLOCK) INNER JOIN Clientes C  WITH (NOLOCK) ON P.CodigoPersona = C.CodigoCliente WHERE NumeroDocumento = ' + quotedstr(RUT)) = 0 then begin
                Plantilla := GenerarError(FDM, Request, MSG_RUT_INEXISTENTE);
                RegistrarBitacoraUso(FDM, 0, CodigoCliente, CodigoConvenio, 11, RUT, MSG_RUT_INEXISTENTE);				//SS_1305_MCA_20150612
            end else begin
                // obtengo el mail registrado, si es que el cliente inform�
                Password := QueryGetValue(fdm.Base, 'SELECT Password FROM Personas  WITH (NOLOCK) WHERE NumeroDocumento = ' + quotedstr(RUT));
                if Password = '' then begin
                // no tiene password...
                    RegistrarBitacoraUso(FDM, 0, CodigoCliente, CodigoConvenio, 11, RUT, MSG_NO_TIENE_CLAVE);			//SS_1305_MCA_20150612
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_NO_TIENE_CLAVE);
                    Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
                    Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
                    Plantilla := ReplaceTag(Plantilla, TAG_PATENTE, '');                                   
                    Plantilla := ReplaceTag(Plantilla, TAG_PATENTEDV, '');                                 
                    Plantilla := ReplaceTag(Plantilla, 'TelefonoCelular', '');
                    Plantilla := ReplaceTag(Plantilla, 'TelefonoParticular', '');
                    Plantilla := ReplaceTag(Plantilla, TAG_EMAIL, '');
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'GenerarClave');
                    Plantilla := ReplaceTag(Plantilla, 'Reintentos', '0');
                    Response.Content := Plantilla;
                    Handled := true;
                    exit;
                end;
                if Email = '' then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'OlvidoPassword');
                    Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
                    Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
                    RegistrarBitacoraUso(FDM, 0, CodigoCliente, CodigoConvenio, 11, RUT, 'Solicitud de Clave por Email');			//SS_1305_MCA_20150612
                end else
                begin
                    //CodigoCliente :=  QueryGetValueInt(fdm.Base, 'SELECT CodigoPersona FROM Personas P  WITH (NOLOCK) WHERE NumeroDocumento = ' + quotedstr(RUT));		//SS_1305_MCA_20150612
                    if CodigoCliente > 0 then    begin
                        EmailRegistrado := QueryGetValue(FDM.Base, Format('SELECT dbo.ObtenerEmailPersona(%d)', [CodigoCliente]));
                    end;

                    if uppercase(trim(EmailRegistrado)) = uppercase(trim(Email)) then begin
                        NuevoPass := inttostr(Random(999999) + 100000);				//SS_1248_MCA_20150602
                        fdm.Base.Execute(		
                            'UPDATE Personas ' +
                            'SET Password = ' + quotedstr(NuevoPass) + ', ' +				//SS_1248_MCA_20150602
                            ' DebeCambiarPassword = 1, ' +
                            'VencimientoPassword = DATEADD(hh, 6, getdate()),'+				//SS_1248_MCA_20150602
                            'Pregunta = NULL, ' +											//SS_1248_MCA_20150602
                            'Respuesta = NULL ' +											//SS_1248_MCA_20150602
                            'WHERE NumeroDocumento = ' + quotedstr(RUT)
                          );

                        ObtenerParametroGeneral(FDM.Base, 'COD_PLANTILLA_ENVIO_PASS_WEB', CodigoPlantilla);
                        ObtenerParametroGeneral(FDM.Base, 'COD_FIRMA_ENVIO_PASS_WEB', CodigoFirma);

                        if not IsValidEMail(Email) then begin
                            Plantilla := GenerarError(FDM, Request, MSG_MAIL_INCORRECTO);
                            RegistrarBitacoraUso(FDM, 0, CodigoCliente, CodigoConvenio, 11, RUT, MSG_MAIL_INCORRECTO);		//SS_1305_MCA_20150612
                            Response.Content := Plantilla;
                            Handled := true;
                            exit;
                        end;

                        //CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoCliente) + ', 1)');		//SS_1305_MCA_20150612
                        AgregarEmailMensaje(FDM, Email, CodigoPlantilla, CodigoFirma, CodigoConvenio, NuevoPass);                       //SS_1248_MCA_20150602


                        Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                        Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_CLAVE_ENVIADA);
                        Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'Login');
                        RegistrarBitacoraUso(FDM, 0, CodigoCliente, CodigoConvenio, 11, RUT, MSG_CLAVE_ENVIADA);			//SS_1305_MCA_20150612
                    end else begin
                        Plantilla := ObtenerPlantilla(FDM, Request, 'Mensaje');
                        Plantilla := ReplaceTag(Plantilla , 'Mensaje', MSG_NO_RECIBE_CLAVE );
                        RegistrarBitacoraUso(FDM, 0, CodigoCliente, CodigoConvenio, 11, RUT, MSG_NO_RECIBE_CLAVE);			//SS_1305_MCA_20150612
                    end;
                end;
            end;
        end else begin
            Plantilla := ObtenerPlantilla(FDM, Request, 'Mensaje');
            Plantilla := ReplaceTag(Plantilla , 'Mensaje', MSG_NO_RECIBE_CLAVE_EMAIL );
            RegistrarBitacoraUso(FDM, 0, CodigoCliente, CodigoConvenio, 11, RUT, MSG_NO_RECIBE_CLAVE_EMAIL);			//SS_1305_MCA_20150612
        end;
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'OlvidoPassword');
            RegistrarBitacoraUso(FDM, 0, CodigoCliente, CodigoConvenio, 11, RUT, 'Se ha producido un error OlvidoPassword: ' +  e.Message);			//SS_1305_MCA_20150612
        end;
    end;

    if Response.Content = '' then Response.Content := Plantilla;	// Rev. SS 511-787-864
    Handled := true;
end;

// INICIO COMENTARIO SS_1280_MCA_20150812 //
procedure TwmConveniosWEB.PagarDeudaOnline(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
const
    CANT_CARACTERES_CAPTCHA   = 6;
var
    CaptCha : TCaptCha;
    Plantilla: string;
    PalabraAleatoria, RUT  : string;
    IdSesionCaptcha   : integer;
    CodigoSesion, CodigoPersona, CodigoConvenio: Integer;
begin
    try
        CodigoSesion := 0;
        //AgregarLog(FDM, 'PagarDeudaOnline - ' + Request.ContentFields.Text);					//SS_1304_MCA_20150709
        CaptCha   := TCaptCha.create;
        Plantilla := ObtenerPlantilla(FDM, Request, 'PagarDeudaOnline');
        PalabraAleatoria := CaptCha.PalabraAleatoria(CANT_CARACTERES_CAPTCHA);
        RUT :=  Request.ContentFields.Values[TAG_RUT] + Request.ContentFields.Values[TAG_RUTDV];
        RUT := Padl(Trim(RUT), 9, '0');
        IdSesionCaptcha := GenerarSesionCaptcha(FDM, Request.ContentFields.Values[TAG_RUT], Request.RemoteAddr, PalabraAleatoria);
        CodigoSesion := StrToInt(IIf(Request.ContentFields.Values[TAG_CODIGO_SESION] = EmptyStr, 0, Request.ContentFields.Values[TAG_CODIGO_SESION]));
        //AgregarLog(FDM, 'CodigoSesion: ' + IntToStr(CodigoSesion));
        Plantilla := ReplaceTag(Plantilla, TAG_CAPTCHA, inttostr(IdSesionCaptcha));
        Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
        Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
        CodigoPersona :=  QueryGetValueInt(fdm.Base, 'SELECT CodigoPersona FROM Personas P  WITH (NOLOCK) WHERE NumeroDocumento = ' + quotedstr(RUT));
        CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');
        if (CodigoPersona > 0) AND (CodigoConvenio > 0) then
        begin
            if CodigoSesion = 0 then CodigoSesion := GenerarSesion(FDM, CodigoPersona, Request.RemoteAddr, False);
            Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
            Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, IntToStr(CodigoConvenio));
        end
        else begin
           Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
           Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_RUT_INEXISTENTE);
           Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'logout');
        end;

        Response.Content := Plantilla;
        Handled := true;
    except
        on E : Exception do begin
            AgregarLog(FDM, 'PagarDeudaOnline - ' + E.Message);
          //  RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobroOnline - ' + 'Se ha producido un error PagarUltimaNotaCobro: ' + e.Message);   //SS_1305_MCA_20150612
        end;

    end;
end;

procedure TwmConveniosWEB.PagarUltimaNotaCobroOnline(Sender: TObject;  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Plantilla: string;
    CodigoConvenio,
    CodigoSesion: integer;
    TipoComprobante: string;
    NumeroComprobante: int64;
    Error: string;
    CodigoPersona: integer;
    RUT: string;
begin
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));
    RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + IntToStr(CodigoPersona));

    try
        //if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                if not FacturacionHabilitada(fdm) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_SECCION_DISPONIBLE_LUEGO_DE_INAUGURACION);
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'logout');
                    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                    Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobroOnline - ' + MSG_SECCION_DISPONIBLE_LUEGO_DE_INAUGURACION);
                    Response.Content := Plantilla;
                    Handled := true;
                    exit;
                end;

                //Verifico si el convenio esta en carpeta legal
                if EsConvenioCarpetaLegal(FDM, CodigoConvenio, Error) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, format(MSG_CONVENIO_CARPETA_LEGAL, [Error]));
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'logout');
                    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                    Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
                    Response.Content := Plantilla;
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobroOnline - ' + format(MSG_CONVENIO_CARPETA_LEGAL, [Error]));
                    Handled := true;
                    exit;
                end;

                if ObtenerUltimaNotaCobroAPagar(FDM, CodigoSesion, TipoComprobante, NumeroComprobante, CodigoConvenio, Error) then begin
                //if ObtenerUltimaNotaCobroAPagar(FDM, CodigoConvenio, TipoComprobante, NumeroComprobante, Error) then begin
				    // paso la llamada al prepago
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobroOnline - ' + 'Pagar deuda Vigente');

                    response.SendRedirect(
                        BASE_PATH + NOMBRE_EXE + '/WebPay/PrePagoOnline?' +
                        TAG_CODIGO_SESION + '=' + inttostr(CodigoSesion) + '&' +
                        'Convenio' + '=' + inttostr(CodigoConvenio) + '&' +
                        'TipoComprobante' + '=' + TipoComprobante + '&' +
                        'NumeroComprobante' + '=' + inttostr(NumeroComprobante)
                    );


                    (*
                    request.contentfields.values['TipoComprobante'] := TipoComprobante;
                    request.contentfields.values['NumeroComprobante'] := inttostr(NumeroComprobante);
                    WebPayPrePago(Sender, Request, Response, Handled); *)
                    exit;
                end else begin
                    Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                    Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, format(MSG_LA_OPERACION_NO_PUEDE_SER_REALIZADA, [Error]));
                    Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'logout');
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobroOnline - ' + format(MSG_LA_OPERACION_NO_PUEDE_SER_REALIZADA, [Error]));
                end;
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
                 RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobroOnline - ' + Error);
            end;
        //end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on E : Exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'PagarUltimaNotaCobroOnline');
            AgregarLog(FDM, 'PagarUltimaNotaCobroOnline - ' + E.Message);
            RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'PagarUltimaNotaCobroOnline - ' + 'Se ha producido un error PagarUltimaNotaCobro: ' + e.Message);   //SS_1305_MCA_20150612
        end;
    end;
    Response.Content := Plantilla;
end;

procedure TwmConveniosWEB.WebPayPrePagoOnline(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    Plantilla: string;
    CodigoConvenio,
    CodigoSesion: integer;
    TipoComprobante: string;
    NumeroComprobante: integer;
    Error: string;
    ParametroCodigoSesion: String;
    TiempoExpiracionPagoWEB: Integer;
    CodigoPersona: integer;
    ComprobantesSeleccionados: string;
    RUT: string;
    Importe: integer;				//SS_1356_MCA_20150903
begin
    try
        if request.MethodType = mtPost then begin
            CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
            CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
            TipoComprobante := request.contentfields.values['TipoComprobante'];
            NumeroComprobante := strtoint(request.contentfields.values['NumeroComprobante']);
        end else begin
            CodigoSesion := StrToIntDef(request.QueryFields.Values[TAG_CODIGO_SESION], -1);
            CodigoConvenio := StrToIntDef(request.QueryFields.Values[TAG_CONVENIO], -1);
            TipoComprobante := request.Queryfields.values['TipoComprobante'];
            NumeroComprobante := strtoint(request.Queryfields.values['NumeroComprobante']);
        end;
        //AgregarLog(FDM, 'WebPayPrePagoOnline: ' + Request.ContentFields.Text);					//SS_1304_MCA_20150709
        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));
        RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + IntToStr(CodigoPersona));

        //if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            if VerificarConvenio(FDM, CodigoSesion, CodigoConvenio, Error) then begin
                if VerificarSiEsPagable(FDM, TipoComprobante, NumeroComprobante, CodigoConvenio, Error) then begin
                    Plantilla := ObtenerPlantilla(FDM, Request, 'PrepagoOnline');

                    // Reemplazar el TAG del par�metro de C�digo de Sesi�n de
                    // las URL de Exito y de Fracaso para incorporarles el CodigoSesion
                    ParametroCodigoSesion := '?' + NOMBRE_VARIABLE_CODIGO_SESION_URL + '=' + IntToStr(CodigoSesion);
                    //CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + inttostr(CodigoSesion));
                    Plantilla := ReplaceTag(Plantilla, TAG_PARAMETRO_CODIGO_SESION_EXITO, ParametroCodigoSesion);
                    Plantilla := ReplaceTag(Plantilla, TAG_PARAMETRO_CODIGO_SESION_FRACASO, ParametroCodigoSesion);

                    Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION_WEB, IntToStr(CodigoSesion));

                    //Verifico si el convenio esta en carpeta legal
                    if EsConvenioCarpetaLegal(FDM, CodigoConvenio, Error) then begin
                        Plantilla := ObtenerPlantilla(FDM, Request, PLANTILLA_AVISO);
                        Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, format(MSG_CONVENIO_CARPETA_LEGAL, [Error]));
                        Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PanelDeControl');
                        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
                        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, inttostr(CodigoConvenio));
                        Response.Content := Plantilla;
                        RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WebPayPrePagoOnline - ' + format(MSG_CONVENIO_CARPETA_LEGAL, [Error]));
                        Handled := true;
                        exit;
                    end;

                    // lleno los datos
                    CargarDatosComprobante(fdm, Plantilla, TipoComprobante, NumeroComprobante, CodigoSesion);
					//Plantilla := ReplaceTag(Plantilla, 'TablaDeudaConvenio', GenerarTablaDeudaConvenio(FDM, CodigoSesion, ComprobantesSeleccionados));					//SS_1356_MCA_20150903
                    Plantilla := ReplaceTag(Plantilla, 'TablaDeudaConvenio', GenerarTablaDeudaConvenio(FDM, CodigoSesion, ComprobantesSeleccionados, Importe));				//SS_1356_MCA_20150903
                    Plantilla := ReplaceTag(Plantilla, 'Seleccion', ComprobantesSeleccionados);
                    Plantilla := ReplaceTag(Plantilla, 'Importe', IntToStr(Importe));					//SS_1356_MCA_20150903
                    // compruebo que no tenga un debito automatico
                    if QueryGetValueInt(fdm.Base, 'WEB_CV_VerificarComprobanteTieneDebitoAutomatico ' + quotedstr(TipoComprobante) + ', ' + inttostr(NumeroComprobante)) = 0 then begin
                        Plantilla := ReplaceTag(Plantilla, 'AvisoDebito', '');
                    end else begin
                        Plantilla := ReplaceTag(Plantilla, 'AvisoDebito', MSG_AVISO_TIENE_DEBITO);
                    end;

                    ObtenerParametroGeneral(FDM.Base, 'TIEMPO_EXPIRACION_PAGO_WEB', TiempoExpiracionPagoWEB);

                    // le extiendo el tiempo de sesion a 30 minutos para darle tiempo a pagar
                    fdm.Base.Execute(
                      'UPDATE WEB_CV_Sesiones SET ValidaHasta = DATEADD(mi, ' + IntToStr(TiempoExpiracionPagoWEB) + ', GetDate()) WHERE CodigoSesion = ' + inttostr(CodigoSesion)
                    );
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WebPayPrePagoOnline - ' + 'Se ha ingresado a Pagar Deuda Vigente');
                end else begin
                    // parece que no se puede pagar...
                    Plantilla := GenerarError(FDM, Request, Error);
                    RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WebPayPrePagoOnline - ' + Error);
                end;
            end else begin
                Plantilla := GenerarError(FDM, Request, Error);
                RegistrarBitacoraUso(FDM, CodigoSesion, CodigoPersona, CodigoConvenio, 13, RUT, 'WebPayPrePagoOnline - ' + Error);
            end;
        //end;
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on e: exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'WebPayPrePagoOnline');
            AgregarLog(FDM, 'WebPayPrePagoOnline - ' + E.Message);
            RegistrarBitacoraUso(FDM, CodigoSesion, 0, CodigoConvenio, 13, '', 'WebPayPrePagoOnline - ' + 'Se ha producido un error WebPayPrePago: ' + e.Message);
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmConveniosWEB.WebpayFracasoTBK(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
Resourcestring
    MSG_ERROR_WEBPAY = 'Su transaccion no pudo ser realizada';
    MSG_ERROR_OBTENER_CODIGO_SESION = 'Error obtener CodigoSesion desde URL: %s';
    MSG_ERROR_OBTENER_CODIGO_SESION_COMO_ENTERO = 'Error convertir a entero el CodigoSesion: %s. URL: %s';
    MSG_ERROR_OBTENER_COMPROBANTE = 'Error obtener Comprobante. TBK_ORDEN_COMPRA: %s';
    MSG_ERROR_OBTENER_TIPO_COMPROBANTE = 'Error obtener Tipo de Comprobante: %s';
    MSG_ERROR_OBTENER_NRO_COMPROBANTE = 'Error obtener N� de Comprobante: %s';
Const
    STR_RECHAZO = 'RECHAZADO';
var
    Plantilla: string;
    CodigoSesion: integer;
    TipoComprobante,
    Comprobante: string;
    NumeroComprobante: integer;
    CodigoSesionEnURL: AnsiString;
    DescriError: String;
begin
    DescriError := EmptyStr;
    AgregarLog(FDM, 'WebPayFracasoTBK - ' + Request.ContentFields.Text);					//SS_1304_MCA_20150709
    try
        CodigoSesionEnURL := EmptyStr;
        DescriError := Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]);
        CodigoSesionEnURL := Request.QueryFields.Values[NOMBRE_VARIABLE_CODIGO_SESION_URL];
        DescriError := EmptyStr;

        // Si no se ley� el Codigo de Sesi�n desde la URL, entonces tirar error
        // Pues algo cambi� la URL que se le indic� al banco que ejecute para
        // invocar la pantalla de Exito.
        if CodigoSesionEnURL = EmptyStr then begin
            raise Exception.Create(Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]));
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionEnURL), 0, 0, 13, '', Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]));
        end;

        DescriError := Format(MSG_ERROR_OBTENER_CODIGO_SESION_COMO_ENTERO, [CodigoSesionEnURL, Request.URL]);
        CodigoSesion := StrToInt(CodigoSesionEnURL);
        DescriError := EmptyStr;

        DescriError := Format(MSG_ERROR_OBTENER_COMPROBANTE, [Request.ContentFields.Values['TBK_ORDEN_COMPRA']]);
        Comprobante := Request.ContentFields.Values['TBK_ORDEN_COMPRA'];
        DescriError := EmptyStr;

        DescriError := Format(MSG_ERROR_OBTENER_TIPO_COMPROBANTE, [Comprobante]);
        TipoComprobante := ParseParamByNumber(Comprobante, 1, ' ');
        DescriError := EmptyStr;

        DescriError := Format(MSG_ERROR_OBTENER_NRO_COMPROBANTE, [Comprobante]);
        NumeroComprobante := strtoint(ParseParamByNumber(Comprobante, 2, ' '));
        DescriError := EmptyStr;

        //if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoFracasoTBK');
            //Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, format(MSG_TBK_TRANSACCION_NO_REALIZADA, [QueryGetValue(fdm.Base, 'SELECT dbo.ObtenerDescripcionTipoComprobante(' + quotedstr(TipoComprobante) + ')') + ' ' + inttostr(NumeroComprobante)]));}
            Plantilla := ReplaceTag(Plantilla, 'NroComprobante', Comprobante);
            Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'logout');
            RegistrarBitacoraUso(FDM, CodigoSesion, 0, 0, 13, '', format(MSG_TBK_TRANSACCION_NO_REALIZADA, [QueryGetValue(fdm.Base, 'SELECT dbo.ObtenerDescripcionTipoComprobante(' + quotedstr(TipoComprobante) + ')') + ' ' + inttostr(NumeroComprobante)]));      //SS_1305_MCA_20150612
        //end;

        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, inttostr(CodigoSesion));
    except
        on E : Exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'WebPayFracasoTBK');
            AgregarLog(FDM, 'WebPayFracasoTBK - ' + E.Message
                + iif(DescriError <> EmptyStr, '. ' + DescriError, EmptyStr));
            RegistrarBitacoraUso(FDM, CodigoSesion, 0, 0, 13, '', 'Se ha producido un error WebpayFracaso: ' + e.Message + DescriError);
        end;
    end;

    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmConveniosWEB.WebpayExitoTBK(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
resourcestring
    MSG_ERROR_OBTENER_CODIGO_SESION = 'Error obtener CodigoSesion desde URL: %s';
    MSG_ERROR_OBTENER_CODIGO_SESION_COMO_ENTERO = 'Error convertir a entero el CodigoSesion: %s. URL: %s';
    MSG_ERROR_OBTENER_COMPROBANTE = 'Error obtener Comprobante. TBK_ORDEN_COMPRA: %s';
    MSG_ERROR_OBTENER_TIPO_COMPROBANTE = 'Error obtener Tipo de Comprobante: %s';
    MSG_ERROR_OBTENER_NRO_COMPROBANTE = 'Error obtener N� de Comprobante: %s';
    MSG_ERROR_GET_SESSION_CODE  = 'Error al obtener el C�digo de Sesi�n Web';
var
    Plantilla: string;
    CodigoSesion: integer;
    TipoComprobante,
    Comprobante: string;
    NumeroComprobante: integer;
    CodigoSesionEnURL: AnsiString;
    DescriError: String;
    CodigoPersona, CodigoConvenio, ImportePagado, CodigoSesionWEB: Integer;					//SS_1304_MCA_20150709
    RUT, IdTrx: string;

begin
    DescriError := EmptyStr;
    AgregarLog(FDM, 'WebPayExitoTBK - ' + Request.ContentFields.Text);				//SS_1304_MCA_20150709
    try
        if Request.ContentFields.Text = EmptyStr then begin
             Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoFracasoTBK');
             Plantilla := ReplaceTag(Plantilla, 'NroComprobante', '');
             Response.Content := Plantilla;
             Handled := true;
             Exit;
        end;


        CodigoSesionEnURL := EmptyStr;
        DescriError := Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]);
        CodigoSesionEnURL := Request.QueryFields.Values[NOMBRE_VARIABLE_CODIGO_SESION_URL];
        DescriError := EmptyStr;

        // Si no se ley� el Codigo de Sesi�n desde la URL, entonces tirar error
        // Pues algo cambi� la URL que se le indic� al banco que ejecute para
        // invocar la pantalla de Exito.
        if CodigoSesionEnURL = EmptyStr then begin
             raise Exception.Create(Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]));
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionEnURL), 0, 0, 13, '', Format(MSG_ERROR_OBTENER_CODIGO_SESION, [Request.URL]));
        end;

        DescriError := Format(MSG_ERROR_OBTENER_CODIGO_SESION_COMO_ENTERO, [CodigoSesionEnURL, Request.URL]);
        CodigoSesion := StrToInt(CodigoSesionEnURL);
        DescriError := EmptyStr;

        CodigoPersona := QueryGetValueInt(fdm.base, 'SELECT CodigoPersona FROM WEB_CV_Sesiones WITH (NOLOCK) WHERE CodigoSesion = ' + IntToStr(CodigoSesion));
        CodigoConvenio := QueryGetValueInt(fdm.base, 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona('+ inttostr(CodigoPersona) + ', 0)');
        RUT := QueryGetValue(fdm.Base, 'SELECT NumeroDocumento FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = ' + inttostr(CodigoPersona));

        IdTrx := Request.ContentFields.Values['TBK_ID_SESION'];

        try
            ImportePagado := QueryGetValueInt(FDM.Base,
                'SELECT Importe FROM Web_CV_Auditoria WITH (NOLOCK)'
                    + ' WHERE CodigoBanco = ' + IntToStr(CODIGO_BANCO_TRANSBANK)
                        + ' AND IDTRX = ' + QuotedStr(IdTrx));
        except
            on E : Exception do begin
                raise Exception.Create(MSG_ERROR_GET_SESSION_CODE + E.Message);
            end;
        end;

        try																								//SS_1304_MCA_20150709
            CodigoSesionWEB := QueryGetValueInt(FDM.Base,												//SS_1304_MCA_20150709
                'SELECT CodigoSesionWEB FROM Web_CV_Auditoria WITH (NOLOCK)'							//SS_1304_MCA_20150709
                    + ' WHERE CodigoBanco = ' + IntToStr(CODIGO_BANCO_TRANSBANK)						//SS_1304_MCA_20150709
                        + ' AND IDTRX = ' + QuotedStr(IdTrx));											//SS_1304_MCA_20150709
        except																							//SS_1304_MCA_20150709
            on E : Exception do begin																	//SS_1304_MCA_20150709
                raise Exception.Create(MSG_ERROR_GET_SESSION_CODE + E.Message);							//SS_1304_MCA_20150709
            end;																						//SS_1304_MCA_20150709
        end;																							//SS_1304_MCA_20150709
        DescriError := Format(MSG_ERROR_OBTENER_COMPROBANTE, [Request.ContentFields.Values['TBK_ORDEN_COMPRA']]);
        Comprobante := Request.ContentFields.Values['TBK_ORDEN_COMPRA'];
        DescriError := EmptyStr;

        DescriError := Format(MSG_ERROR_OBTENER_TIPO_COMPROBANTE, [Comprobante]);
        TipoComprobante := ParseParamByNumber(Comprobante, 1, ' ');
        DescriError := EmptyStr;

        DescriError := Format(MSG_ERROR_OBTENER_NRO_COMPROBANTE, [Comprobante]);
        NumeroComprobante := StrToInt(ParseParamByNumber(Comprobante, 2, ' '));
        DescriError := EmptyStr;

        //if VerificarSesion(FDM, Request, CodigoSesion, Plantilla) then begin
            Plantilla := ObtenerPlantilla(FDM, Request, 'ExitoTBK');
            Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'logout');
            CargarDatosPago(fdm, Plantilla, TipoComprobante, NumeroComprobante, ImportePagado);
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionEnURL), CodigoPersona, CodigoConvenio, 13, RUT, 'WebpayExitoTBK - Transbank - Se ha realizado el pago exitosamente Transbank');
        //end;

        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
    except
        on E : Exception do begin
            Plantilla := AtenderError(Request, FDM, e, 'WebpayExitoTBK');			//SS_1304_MCA_20150709
            AgregarLog(FDM, 'WebpayExitoTBK - ' + E.Message							//SS_1304_MCA_20150709
                + iif(DescriError <> EmptyStr, '. ' + DescriError, EmptyStr));
            RegistrarBitacoraUso(FDM, StrToInt(CodigoSesionEnURL), CodigoPersona, CodigoConvenio, 13, RUT, 'WebpayExitoTBK - Se ha producido un error WebpayExito: ' + E.Message);  //SS_1305_MCA_20150612
        end;
    end;
    Response.Content := Plantilla;
    Handled := true;
end;

procedure TwmConveniosWEB.DoPagarDeudaOnline(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
    CadenaCaptcha : string;
    Plantilla: string;
    captchavalue: string;
    CodigoSesion, CodigoConvenio : Integer;
begin
    CadenaCaptcha := ObtenerSesionCaptcha(FDM, Request.RemoteAddr);
    captchavalue := UpperCase(Request.ContentFields.Values['captchavalue']);
    CodigoSesion := StrToIntDef(request.ContentFields.Values[TAG_CODIGO_SESION], -1);
    CodigoConvenio := StrToIntDef(request.ContentFields.Values[TAG_CONVENIO], -1);
    if not ( UpperCase(CadenaCaptcha) = captchavalue) then begin
        Plantilla := ObtenerPlantilla(FDM, Request, 'AvisoSC');
        Plantilla := ReplaceTag(Plantilla, TITULO_AVISO, MSG_CAPTCHA_VALIDACION);
        Plantilla := ReplaceTag(Plantilla, TAG_RUT, Request.ContentFields.Values[TAG_RUT]);
        Plantilla := ReplaceTag(Plantilla, TAG_RUTDV, Request.ContentFields.Values[TAG_RUTDV]);
        Plantilla := ReplaceTag(Plantilla, TAG_SIGUIENTE_PASO, 'PagarDeudaOnLine');
    end
    else begin
        Plantilla := ObtenerPlantilla(FDM, Request, 'PagarUltimaNotaCobroOnline');
        Plantilla := ReplaceTag(Plantilla, TAG_CODIGO_SESION, IntToStr(CodigoSesion));
        Plantilla := ReplaceTag(Plantilla, TAG_CONVENIO, IntToStr(CodigoConvenio));
    end;
    Response.Content := Plantilla;
    Handled := True;
end;
// FIN COMENTARIO SS_1280_MCA_20150812 //
initialization
{$IFDEF WEBDEBUGGER}
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := TwmConveniosWEB;
{$ENDIF}
end.
