object wmConveniosWEB: TwmConveniosWEB
  OldCreateOrder = False
  OnCreate = WebModuleCreate
  Actions = <
    item
      Default = True
      Name = 'Login'
      PathInfo = '/login'
      OnAction = Login
    end
    item
      Name = 'DoLogin'
      PathInfo = '/DoLogin'
      OnAction = DoLogin
    end
    item
      Name = 'PanelDeControl'
      PathInfo = '/PanelDeControl'
      OnAction = PanelDeControl
    end
    item
      Name = 'CambiarPass'
      PathInfo = '/CambiarPass'
      OnAction = CambiarPass
    end
    item
      Name = 'CambiarDatosPersonales'
      PathInfo = '/CambiarDatosPersonales'
      OnAction = CambiarDatosPersonales
    end
    item
      Name = 'CambiarDatosDomicilio'
      PathInfo = '/CambiarDatosDomicilio'
      OnAction = CambiarDatosDomicilio
    end
    item
      Name = 'BuscaCalle'
      PathInfo = '/BuscaCalle'
      OnAction = BuscaCalle
    end
    item
      Name = 'CambiarDatosDomicilioConvenio'
      PathInfo = '/CambiarDatosDomicilioConvenio'
      OnAction = CambiarDatosDomicilioConvenio
    end
    item
      Name = 'CambiarDatosMedioPagoConvenio'
      PathInfo = '/CambiarDatosMedioPagoConvenio'
      OnAction = CambiarDatosMedioPagoConvenio
    end
    item
      Name = 'CambiarDatosVehiculosConvenio'
      PathInfo = '/CambiarDatosVehiculosConvenio'
      OnAction = CambiarDatosVehiculosConvenio
    end
    item
      Name = 'logout'
      PathInfo = '/logout'
      OnAction = logout
    end
    item
      Name = 'OlvidoPassword'
      PathInfo = '/OlvidoPassword'
      OnAction = OlvidoPassword
    end
    item
      Name = 'CambiarPregunta'
      PathInfo = '/CambiarPregunta'
      OnAction = CambiarPregunta
    end
    item
      Name = 'RemoteLogin'
      PathInfo = '/RemoteLogin'
      OnAction = RemoteLogin
    end
    item
      Name = 'VerUltimaNotaDeCobro'
      PathInfo = '/VerUltimaNotaDeCobro'
      OnAction = VerUltimaNotaDeCobro
    end
    item
      Name = 'PagarUltimaNotaCobro'
      PathInfo = '/PagarUltimaNotaCobro'
      OnAction = PagarUltimaNotaCobro
    end
    item
      Name = 'Cartola'
      PathInfo = '/Cartola'
      OnAction = Cartola
    end
    item
      Name = 'EstadoCuenta'
      PathInfo = '/EstadoCuenta'
      OnAction = EstadoCuenta
    end
    item
      Name = 'VerTarifas'
      PathInfo = '/VerTarifas'
      OnAction = VerTarifas
    end
    item
      Name = 'VerComprobante'
      PathInfo = '/VerComprobante'
      OnAction = VerComprobante
    end
    item
      Name = 'VerReclamos'
      PathInfo = '/VerReclamos'
      OnAction = VerReclamos
    end
    item
      Name = 'WebPayPrePago'
      PathInfo = '/WebPay/PrePago'
      OnAction = WebPayPrePago
    end
    item
      Name = 'RedirigirTransBank'
      PathInfo = '/RedirigirTransBank'
      OnAction = wmConveniosWEBRedirigirTransBankAction
    end
    item
      Name = 'WebpayAceptacion'
      PathInfo = '/Webpay/Aceptacion'
      OnAction = WebpayAceptacion
    end
    item
      Name = 'WebPayExito'
      PathInfo = '/WebPay/Exito'
      OnAction = WebpayExito
    end
    item
      Name = 'WebPayFracaso'
      PathInfo = '/WebPay/Fracaso'
      OnAction = WebpayFracaso
    end
    item
      Name = 'RedirigirSantander'
      PathInfo = '/RedirigirSantander'
      OnAction = wmConveniosWEBRedirigirSantanderAction
    end
    item
      Name = 'NotificacionSTD'
      PathInfo = '/NotificacionSTD'
      OnAction = wmConveniosWEBNotificacionSTDAction
    end
    item
      Name = 'SalidaSTD'
      PathInfo = '/SalidaSTD'
      OnAction = wmConveniosWEBSalidaSTDAction
    end
    item
      Name = 'Debug'
      PathInfo = '/debug'
      OnAction = Debug
    end
    item
      Name = 'VerConsumos'
      PathInfo = '/VerConsumos'
      OnAction = VerConsumos
    end
    item
      Name = 'GenerarClave'
      PathInfo = '/GenerarClave'
      OnAction = GenerarClave
    end
    item
      Name = 'DoGenerarClave'
      PathInfo = '/DoGenerarClave'
      OnAction = DoGenerarClave
    end
    item
      Name = 'BlanquearPass'
      PathInfo = '/BlanquearPass'
      OnAction = BlanquearPass
    end
    item
      Name = 'MediosEnvio'
      PathInfo = '/MediosEnvio'
      OnAction = MediosEnvio
    end
    item
      Name = 'GrabarMediosEnvio'
      PathInfo = '/GrabarMediosEnvio'
      OnAction = GrabarMediosEnvio
    end
    item
      Name = 'SaldoxConveniosxRUT'
      PathInfo = '/SaldoxConveniosxRUT'
      OnAction = SaldoxConveniosxRUT
    end
    item
      Name = 'ValidacionCaptcha'
      PathInfo = '/ValidacionCaptcha'
      OnAction = wmConveniosWEBValidacionCaptchaAction
    end
    item
      Name = 'DescargarCSVDetalleTransitos'
      PathInfo = '/DescargarCSVDetalleTransitos'
      OnAction = DescargarCSVDetalleTransitos
    end
    item
      Name = 'DescargarArchivoAyudaExcel'
      PathInfo = '/DescargarArchivoAyudaExcel'
      OnAction = wmConveniosWEBDescargarArchivoAyudaExcelAction
    end
    item
      Name = 'RedirigirBancoChile'
      PathInfo = '/RedirigirBancoChile'
      OnAction = wmConveniosWEBRedirigirBancoChileAction
    end
    item
      Name = 'NotificacionBCH'
      PathInfo = '/NotificacionBCH'
      OnAction = wmConveniosWEBNotificacionBCHAction
    end
    item
      Name = 'SalidaBCH'
      PathInfo = '/SalidaBCH'
      OnAction = wmConveniosWEBSalidaBCHAction
    end
    item
      Name = 'ValidarInscripcionAraucoTAG'
      PathInfo = '/ValidarInscripcionAraucoTAG'
      OnAction = wmConveniosWEBValidarInscripcionAraucoTAGAction
    end
    item
      Name = 'RealizarInscripccionAraucoTAG'
      PathInfo = '/RealizarInscripccionAraucoTAG'
      OnAction = wmConveniosWEBRealizarInscripccionAraucoTAGAction
    end
    item
      Name = 'ReenviarInscripcionAraucoTAG'
      PathInfo = '/ReenviarInscripcionAraucoTAG'
      OnAction = wmConveniosWEBReenviarInscripcionAraucoTAGAction
    end
    item
      Name = 'DescargarCSVDetalleEstacionamientos'
      PathInfo = '/DescargarCSVDetalleEstacionamientos'
      OnAction = wmConveniosWEBDescargarCSVDetalleEstacionamientosAction
    end
    item
      Name = 'PagarDeudaOnline'
      PathInfo = '/PagarDeudaOnline'
      OnAction = PagarDeudaOnline
    end
    item
      Name = 'PagarUltimaNotaCobroOnline'
      PathInfo = '/PagarUltimaNotaCobroOnline'
      OnAction = PagarUltimaNotaCobroOnline
    end
    item
      Name = 'WebPayPrepagoOnline'
      PathInfo = '/WebPay/PrepagoOnline'
      OnAction = WebPayPrePagoOnline
    end
    item
      Name = 'WebPayExitoTBK'
      PathInfo = '/WebPay/ExitoTBK'
      OnAction = WebpayExitoTBK
    end
    item
      Name = 'WebPayFracasoTBK'
      PathInfo = '/WebPay/FracasoTBK'
      OnAction = WebpayFracasoTBK
    end
    item
      Name = 'DoPagarDeudaOnline'
      PathInfo = '/DoPagarDeudaOnline'
      OnAction = DoPagarDeudaOnline
    end
    item
      Name = 'VerNoFacturados'
      PathInfo = '/VerNoFacturados'
      OnAction = VerNoFacturados
    end>
  OnException = WebModuleException
  Height = 162
  Width = 181
end
