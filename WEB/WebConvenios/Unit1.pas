unit Unit1;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm1 = class(TForm)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses ComApp;

{$R *.DFM}

const
  CLASS_ComWebApp: TGUID = '{DFBD6E88-0F19-4D64-8163-E2A749A318C2}';

initialization
  TWebAppAutoObjectFactory.Create(Class_ComWebApp,
    'Convenios', 'Convenios Object');

end.
 