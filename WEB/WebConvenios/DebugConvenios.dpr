program DebugConvenios;

{$APPTYPE GUI}

{%File 'ComApp.obj'}

uses
  Forms,
  ComApp,
  Unit1 in 'Unit1.pas' {Form1},
  wmConvenios in 'wmConvenios.pas' {wmConveniosWEB: TWebModule},
  Combos in 'Combos.pas',
  dmConvenios in 'dmConvenios.pas' {dmConveniosWEB: TDataModule},
  FuncionesWEB in 'FuncionesWEB.pas',
  ManejoDatos in 'ManejoDatos.pas',
  ManejoErrores in 'ManejoErrores.pas',
  Parametros in 'Parametros.pas',
  Sesiones in 'Sesiones.pas',
  ReporteFactura in '..\..\Facturacion\ReporteFactura.pas' {frmReporteFactura},
  ConstParametrosGenerales in '..\..\Comunes\ConstParametrosGenerales.pas',
  PeaTypes in '..\..\Comunes\PeaTypes.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TwmConveniosWEB, wmConveniosWEB);
  Application.CreateForm(TfrmReporteFactura, frmReporteFactura);
  Application.Run;
end.
