unit Conexion;

interface

uses
  SysUtils, Classes, ADODB, Util, UtilDB, DB, IniFiles, Eventlog, HTTPApp;

type
    TDMConexion = class(TDataModule)
        BaseCAC: TADOConnection;

        procedure DataModuleCreate(Sender: TObject);
        procedure BaseCACBeforeConnect(Sender: TObject);
        procedure BaseCACWillConnect(Connection: TADOConnection;
            var ConnectionString, UserID, Password: WideString;
            var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    end;

    function GetBasePath: string;
    function GetServer: string;
    function GetBase: string;
    function GetUser: string;
    function GetPassword: string;
    function GetIniFileName: String;
    function GetAmbienteInfo: String;

var
    DMConexion: TDMConexion;
    ISAPICGIFile: String;

resourcestring
    CAPTION_ASEGURAR_BASE_DATOS = 'Asegurar Base de Datos';
    MSG_ASEGURAR_BASE_DATOS_COP = 'No ha podido asegurarse la base de datos COP.';
    MSG_ASEGURAR_BASE_DATOS_AUD_CAC = 'No ha podido asegurarse la base de datos de auditor�a del CAC.';

implementation

{$R *.dfm}
{$I Ambiente.inc}

function GetIniFileName: String;
begin
    Result := AmbienteArchivoINI;
end;

function GetBasePath: string;
var
    inifile: TIniFile;
begin
    iniFile := TIniFile.create(GetIniFileName);
    result := inifile.ReadString('General', 'BasePath', 'http://localhost/');
    inifile.Destroy;
end;

function GetServer: string;
var
    inifile: TIniFile;
begin
    iniFile := TIniFile.create(GetIniFileName);
    result := inifile.ReadString('General', 'Server', 'SRV-DATABASE\DBCOSTANERA');
    inifile.Destroy;
end;

function GetBase: string;
var
    inifile: TIniFile;
begin
    iniFile := TIniFile.create(GetIniFileName);
    result := inifile.ReadString('General', 'Base', 'OP_CAC');
    inifile.Destroy;
end;

function GetUser: string;
var
    inifile: TIniFile;
begin
    iniFile := TIniFile.create(GetIniFileName);
    result := inifile.ReadString('General', 'User', 'usr_op');
    inifile.Destroy;
end;

function GetPassword: string;
var
    inifile: TIniFile;
begin
    iniFile := TIniFile.create(GetIniFileName);
    result := inifile.ReadString('General', 'Password', 'usr_op');
    inifile.Destroy;
end;

procedure TDMConexion.DataModuleCreate(Sender: TObject);
begin
    BaseCAC.KeepConnection := true;
    try
        BaseCAC.ConnectionString :=
          'Provider=SQLOLEDB.1;' +
          'Password=' + GetPassword + ';' +
          'User ID=' + GetUser + ';' +
          'Initial Catalog=' + GetBase + ';' +
          'Data Source=' + GetServer;
    except
    end;
end;

procedure TDMConexion.BaseCACBeforeConnect(Sender: TObject);
begin
    BaseCAC.ConnectionString :=
      'Provider=SQLOLEDB.1;' +
      'User ID=sa;' +
      'Initial Catalog=' + GetBase + ';' +
      'Data Source=' + GetServer;
end;

procedure TDMConexion.BaseCACWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
Var
	DescriError: AnsiString;
begin
	if not SecureDatabase(BaseCAC, UserID, Password, DescriError, 'Parametros') then begin
        EventLogReportEvent(elError, 'Error al intentar asegurar la base de datos: ' + DescriError, '');
        EventStatus := esCancel;
	end;
end;

function GetAmbienteInfo: String;
    ResourceString
        AMBIENTE_INFO_DESARROLLO = 'Desarrollo';
        AMBIENTE_INFO_TEST = 'Test';
        AMBIENTE_INFO_AMBIENTE = ' Ambiente: ';
        AMBIENTE_INFO_FILE = 'ISAPI/CGI: ';
        AMBIENTE_INFO_FILE_VERSION = 'Ver.: ';
        AMBIENTE_INFO_INSTANCIA = ' SQL Server: ';
        AMBIENTE_INFO_BD = ' BD: ';
        AMBIENTE_INFO_USUARIO = ' User: ';
        AMBIENTE_INFO_PATH_PLANTILLAS = ' Path Plantillas: ';
        AMBIENTE_INFO_INI_FILE = ' Ini: ';
        ESTILO_10 = '<SPAN class=estiloDev1>%s</span>';
        ESTILO_3 = '<SPAN class=estiloDev2>%s</span>';
        AMBIENTE_INFO_GENERAL =
            '%s, %s, %s, %s<BR>%s, %s, %s, %s';
begin
    Result := '';
    {$IFDEF DESARROLLO}
        Result :=
            Format(
                AMBIENTE_INFO_GENERAL,
                [Format(ESTILO_10,[AMBIENTE_INFO_AMBIENTE]) + Format(ESTILO_3,[AMBIENTE_INFO_DESARROLLO]),
                 Format(ESTILO_10,[AMBIENTE_INFO_INI_FILE]) + Format(ESTILO_3,[GetIniFileName]),
                 Format(ESTILO_10,[AMBIENTE_INFO_FILE]) + Format(ESTILO_3,[ExtractFileName(ISAPICGIFile)]),
                 Format(ESTILO_10,[AMBIENTE_INFO_FILE_VERSION]) + Format(ESTILO_3,[GetFileVersionString(ISAPICGIFile, True)]),
                 Format(ESTILO_10,[AMBIENTE_INFO_INSTANCIA]) + Format(ESTILO_3,[GetServer]),
                 Format(ESTILO_10,[AMBIENTE_INFO_BD]) + Format(ESTILO_3,[GetBase]),
                 Format(ESTILO_10,[AMBIENTE_INFO_USUARIO]) + Format(ESTILO_3,[GetUser])]);
    {$ELSE}
        {$IFDEF TEST}
        Result :=
            Format(
                AMBIENTE_INFO_GENERAL,
                [Format(ESTILO_10,[AMBIENTE_INFO_AMBIENTE]) + Format(ESTILO_3,[AMBIENTE_INFO_TEST]),
                 Format(ESTILO_10,[AMBIENTE_INFO_INI_FILE]) + Format(ESTILO_3,[GetIniFileName]),
                 Format(ESTILO_10,[AMBIENTE_INFO_FILE]) + Format(ESTILO_3,[ExtractFileName(ISAPICGIFile)]),
                 Format(ESTILO_10,[AMBIENTE_INFO_FILE_VERSION]) + Format(ESTILO_3,[GetFileVersionString(ISAPICGIFile, True)]),
                 Format(ESTILO_10,[AMBIENTE_INFO_INSTANCIA]) + Format(ESTILO_3,[GetServer]),
                 Format(ESTILO_10,[AMBIENTE_INFO_BD]) + Format(ESTILO_3,[GetBase]),
                 Format(ESTILO_10,[AMBIENTE_INFO_USUARIO]) + Format(ESTILO_3,[GetUser])]);
        {$ENDIF}
    {$ENDIF}
end;

end.
