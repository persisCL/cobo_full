library wsCanalDePagoOnline;

uses
  ActiveX,
  ComObj,
  WebBroker,
  ISAPIApp,
  ISAPIThreadPool,
  wsCanalPagoOnline in 'wsCanalPagoOnline.pas' {WebModule1: TWebModule},
  wsCanalPagoOnlineImpl in 'wsCanalPagoOnlineImpl.pas',
  wsCanalPagoOnlineIntf in 'wsCanalPagoOnlineIntf.pas',
  Conexion in 'Conexion.pas' {DMConexion: TDataModule};

{$R *.res}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;

begin
  CoInitFlags := COINIT_MULTITHREADED;
  Application.Initialize;
  Application.CreateForm(TWebModule1, WebModule1);
  Application.Run;
end.
