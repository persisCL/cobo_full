{ Invokable implementation File for TwsCanalPagoOnline which implements IwsCanalPagoOnline }


{------------------------------------------------------------------------------

	Firma		:	SS_1278_MBE_20150525
    Description	:	La validación del RUT ahora se hace en el
    				SP WSObtenerSaldoxConveniosPorRut

                	Ya no tiene que grabar directamente en WsHistorico,
                    esto debe hacerlo el JOB.

    Firma       :   SS_1278_MBE_20150727
    Description :   Ya no devuelve error en caso de que no pueda consolidar el pago.

    Firma       :   SS_1278_MBE_20150728
    Description :   Se agrega el Codigo de Servicio en el pago.

------------------------------------------------------------------------------}

unit wsCanalPagoOnlineImpl;

interface

uses InvokeRegistry, Types, XSBuiltIns, wsCanalPagoOnlineIntf,
ADODB, Conexion, UtilDB, Util;

type

  { TwsCanalPagoOnline }
  TwsCanalPagoOnline = class(TInvokableClass, IwsCanalPagoOnline)
  private
  	FConexion : TDMConexion;
  public
  	function WS_ObtenerSaldoxConvenioPorRut(Consulta: TConsulta): TRespuestaConsulta; stdcall;
	function WS_GenerarPagoConvenioPorRut(Pago: TPago): TMensaje; stdcall;

  end;

implementation

uses sysUtils;

{ TwsCanalPagoOnline }

function TwsCanalPagoOnline.WS_ObtenerSaldoxConvenioPorRut(
  Consulta: TConsulta): TRespuestaConsulta;
resourcestring
	MSG_ERROR_RUT_INVALIDO		= 'Rut Invalido';
    MSG_ERROR_GUARDAR_HISTORICO = 'Se ha producido un error al conectarse a la Base de Datos, SP WSAgregarHistoricoMedioPagoOnLine: ';
    MSG_ERROR_TOTEM_NO_EXISTE	= 'El Totem no existe';							// SS_1278_MBE_20150525
    MSG_ERROR_GUARDAR_CONSULTA	= 'Error al guardar la consulta';				// SS_1278_MBE_20150525

    
    function ValidarRut(sCon: TDMConexion;  Numero : string) : Boolean;
    begin
        try
        	if (Length(trim(Numero))>10) or (Length(trim(Numero))<8)then begin result:=False; exit; end;
			numero:=strright('000000000000000'+trim(numero),10);
            Result := QueryGetValueInt(sCon.BaseCAC,format('EXEC VerificarRUT ''%s'',''%s''',[strleft(Numero,9),strright(numero,1)]))=1;
        except
            Result := False;
        end;
    end;

    function AgregarConsultaInvalida(sCon: TDMConexion; DatosConsulta: TConsulta): Integer;
    var
    spAuditoriaWS: TADOStoredProc;
    Retorno : Integer;
	begin
    	Retorno := 0;
    	try
        	try
               spAuditoriaWS := TADOStoredProc.Create(nil);

               with spAuditoriaWS do begin
               		Connection    := sCon.BaseCAC;
                    ProcedureName := 'WSAgregarHistoricoMedioPagoOnLine';
                    Parameters.Refresh;
                    with Parameters do begin
                        ParamByName('@IdComercio').Value		:= DatosConsulta.POS;
                        ParamByName('@NumeroDocumento').Value	:= DatosConsulta.RutCliente;
                        ParamByName('@RespuestaWS').Value		:= MSG_ERROR_RUT_INVALIDO;
                        ExecProc;
                    end;

               end;
            except
                on e: Exception do begin
                    raise Exception.Create(MSG_ERROR_GUARDAR_HISTORICO + e.Message);
                end;
            end;

        finally
            Result := Retorno;
        end;
	end;
var
	spWSObtenerSaldoxConveniosPorRut: TADOStoredProc;
    vCodigoRetorno : integer;
begin
    FConexion := TDMConexion.Create(nil);
    Result := TRespuestaConsulta.Create();
    Result.Mensaje := TMensaje.Create();
    try
    	try
            spWSObtenerSaldoxConveniosPorRut := TADOStoredProc.Create(nil);
//            if ValidarRUT(FConexion, Consulta.RutCliente) then						            // SS_1278_MBE_20150525
//            begin                                                                                 // SS_1278_MBE_20150525
                with spWSObtenerSaldoxConveniosPorRut do begin
                    Connection:= FConexion.BaseCAC;
                    ProcedureName := 'WSObtenerSaldoxConveniosPorRut';
                    Parameters.Refresh;
                    Parameters.ParamByName('@IdComercio').Value := Consulta.POS;
                    Parameters.ParamByName('@NumeroDocumento').Value := Consulta.RutCliente;
                    Open;
                    vCodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                    if not IsEmpty then begin
                        Result.IDConsulta   := FieldByName('IdConsultaOnline').AsInteger;
                        Result.RutCliente   := FieldByName('NumeroDocumento').AsString;
                        Result.SaldoVencido := FieldByName('SaldoVencido').AsInteger div 100;       // SS_1278_MBE_20150525
                        Result.SaldoxVencer := FieldByName('SaldoxVencer').AsInteger div 100;       // SS_1278_MBE_20150525
//                        Result.Mensaje.CodigoError := 0;                                          // SS_1278_MBE_20150525
                        Result.Mensaje.CodigoError := vCodigoRetorno;                               // SS_1278_MBE_20150525
                        Result.Mensaje.Mensaje := FieldByName('Observacion').AsString;
                    end;
                end;

                if spWSObtenerSaldoxConveniosPorRut.Active then begin                               // SS_1278_MBE_20150525
                	spWSObtenerSaldoxConveniosPorRut.Close;                                         // SS_1278_MBE_20150525
                end;                                                                                // SS_1278_MBE_20150525

//            end else begin                                                                        // SS_1278_MBE_20150525
//                Result.IDConsulta := AgregarConsultaInvalida(FConexion, Consulta);                // SS_1278_MBE_20150525
//                Result.Mensaje.CodigoError := -2;                                                 // SS_1278_MBE_20150525
//                Result.Mensaje.Mensaje := MSG_ERROR_RUT_INVALIDO;                                 // SS_1278_MBE_20150525
//            end;                                                                                  // SS_1278_MBE_20150525
		except
            on e: Exception do begin
                Result.Mensaje.CodigoError := -1;
                Result.Mensaje.Mensaje := e.Message;
            end;

        end;
    finally
     	if Assigned(spWSObtenerSaldoxConveniosPorRut) then FreeAndNil(spWSObtenerSaldoxConveniosPorRut);
    end;

end;

function TwsCanalPagoOnline.WS_GenerarPagoConvenioPorRut(
  	Pago: TPago): TMensaje;
resourcestring
	MSG_ERROR_GUARDAR_PAGO = 'Se ha producido un error al momento de generar el pago. SP WSGenerarPagoConvenioPorRut: ';
var
	spWSGenerarPagoConvenioPorRut: TADOStoredProc;
    spWSConsolidarPago: TADOStoredProc;
begin
	FConexion := TDMConexion.Create(nil);
    Result := TMensaje.Create();
    try
        try
        	spWSGenerarPagoConvenioPorRut := TADOStoredProc.Create(nil);
            with spWSGenerarPagoConvenioPorRut do begin
                Connection:= FConexion.BaseCAC;
                ProcedureName := 'WSGenerarPagoConvenioPorRut';
                Parameters.Refresh;
                Parameters.ParamByName('@IdComercio').Value := Pago.IDPos;
                Parameters.ParamByName('@IdConsulta').Value := Pago.IDConsulta;
                Parameters.ParamByName('@NumeroDocumento').Value := Pago.RutCliente;
                Parameters.ParamByName('@MontoCancelado').Value := Pago.MontoCancelado * 100;		// SS_1278_MBE_20150525
                Parameters.ParamByName('@TipoDeuda').Value := UpperCase(Pago.TipoMonto);            // SS_1278_MBE_20150527
                Parameters.ParamByName('@IdInterno').Value := Pago.IdOperacion;
                Parameters.ParamByName('@CodigoDeServicio').Value := Pago.CodigoDeServicio;         // SS_1278_MBE_20150728
                Parameters.ParamByName('@DescriError').Value := EmptyStr;
                Parameters.ParamByName('@CodigoError').Value := 0;
                Parameters.ParamByName('@IdPagoOnline').Value := 0;
                ExecProc;
                Result.IdPagoOnline := Parameters.ParamByName('@IdPagoOnline').Value;
                Result.Mensaje := Parameters.ParamByName('@DescriError').Value;
                Result.CodigoError := Parameters.ParamByName('@CodigoError').Value;

//                if Result.IdPagoOnline > 0 then begin                                         // SS_1278_MBE_20150727
                if (Result.IdPagoOnline > 0) and (Result.CodigoError = 0) then begin            // SS_1278_MBE_20150727

                    try
                        spWSConsolidarPago := TADOStoredProc.Create(nil);
                        with spWSConsolidarPago do begin
                            Connection := FConexion.BaseCAC;
                            ProcedureName := 'WSConsolidarPagos';
                            Parameters.Refresh;
                            Parameters.ParamByName('@IdPago').Value := Result.IdPagoOnline;
                            ExecProc;
                        end;
{                                                                                               // SS_1278_MBE_20150727
                    except
                        on e: Exception do begin
                            Result.IdPagoOnline := 0;
                            Result.CodigoError := 0;
                            Result.Mensaje := MSG_ERROR_GUARDAR_PAGO + e.Message ;
                            end ;
}                                                                                               // SS_1278_MBE_20150727
                    finally                                                                     // SS_1278_MBE_20150727
                    end;
                end;
            end;
        except
	         on e: Exception do begin
                    Result.CodigoError := 0;
                    Result.Mensaje := MSG_ERROR_GUARDAR_PAGO + e.Message ;
             end;

        end;
    finally
     	if Assigned(spWSGenerarPagoConvenioPorRut) then FreeAndNil(spWSGenerarPagoConvenioPorRut);
        if Assigned(spWSConsolidarPago) then FreeAndNil(spWSConsolidarPago);
    end;

end;


initialization
{ Invokable classes must be registered }
   InvRegistry.RegisterInvokableClass(TwsCanalPagoOnline);
end.

