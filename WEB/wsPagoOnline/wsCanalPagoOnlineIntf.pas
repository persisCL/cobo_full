{ Invokable interface IwsCanalPagoOnline

    Firma       :   SS_1278_MBE_20150728
    Description :   Se agrega el c�digo de servicio al pago, para diferenciar el
                    canal de banco Estado
}

unit wsCanalPagoOnlineIntf;

interface

uses InvokeRegistry, Types, XSBuiltIns;

type

 	TMensaje = class(TRemotable)
    	private
            FIdPago: Int64;
            FCodigoError : Integer;
            FMensaje : string;
    	published
            property IdPagoOnline : Int64 read FIdPago write FIdPago;
        	property CodigoError : Integer read FCodigoError write FCodigoError;
        	property Mensaje : string read FMensaje write FMensaje;
	end;

	TConsulta = class(TRemotable)
    	private
        	FRutCliente: string;
            FIDPOS: Integer;
        published
            property RutCliente: string read FRutCliente write FRutCliente;
      		property POS: Integer read FIDPOS write FIDPOS;
    end;

    TPago = class(TRemotable)
        private
            FIDConsulta: Integer;
            FRutCliente: string;
            FIDPOS: Integer;
            FMonto: LongInt;
            FTipoMonto: string;
            FIDOperacion:LongInt;
            FCodigoServicio : string;                                                               // SS_1278_MBE_20150728
        published
        	property IDConsulta: Integer read FIDConsulta write FIDConsulta;
            property RutCliente: string read FRutCliente write FRutCliente;
            property IDPos: integer read FIDPOS write FIDPOS;
            property MontoCancelado: LongInt read FMonto write FMonto;
            property TipoMonto: string read FTipoMonto write FTipoMonto;
        	property IdOperacion: LongInt read FIDOperacion write FIDOperacion;
            property CodigoDeServicio : string read FCodigoServicio write FCodigoServicio;          // SS_1278_MBE_20150728

    end;

  	TRespuestaConsulta = class(TRemotable)
        private
        	FID: Integer;
            FRutCliente: string;
            FSaldoVencido: LongInt;
            FSaldoxVencer: LongInt;
            FMensaje: TMensaje;
        published
        	property IDConsulta: Integer read FID write FID;
            property RutCliente: string read FRutCliente write FRutCliente;
            property SaldoVencido: LongInt read FSaldoVencido write FSaldoVencido;
            property SaldoxVencer: LongInt read FSaldoxVencer write FSaldoxVencer;
            property Mensaje: TMensaje read FMensaje write FMensaje;

    end;



  { Invokable interfaces must derive from IInvokable }
  IwsCanalPagoOnline = interface(IInvokable)
  ['{1B47E756-517F-4AB7-8C39-C6CB6983800A}']

    { Methods of Invokable interface must not use the default }
    { calling convention; stdcall is recommended }
    function WS_ObtenerSaldoxConvenioPorRut(Consulta: TConsulta): TRespuestaConsulta; stdcall;
	function WS_GenerarPagoConvenioPorRut(Pago: TPago): TMensaje; stdcall;
  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface(TypeInfo(IwsCanalPagoOnline));

end.
