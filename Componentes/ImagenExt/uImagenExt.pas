{ ****************************************************************** }
{                                                                    }
{   VCL component TImageExt                                             }
{                                                                    }
{   Autor: Malisia, Fabio Nazareno                                   }
{                                                                    }
{ ****************************************************************** }
         
unit uImagenExt;

interface

uses forms, windows, Messages, SysUtils, Classes, Controls, Graphics, Math,
     extctrls, Cursores;

type
    TScaleChangeEvent = procedure (Sender: TObject; Scale: Extended) of object;

    TImageExt = class(TCustomControl)
    private
      { Private fields of TImageExt }
        FCenter : Boolean;
        FOffsetX : Integer;
        FOffsetY : Integer;
        FPicture : TPicture;
        FScale : Extended;
        FStretch : Boolean;
        FModoZoom : Boolean;
        FOnScaleChange : TScaleChangeEvent;
        PuntoInicial : TPoint;
        UltimoPunto : TPoint;
        ZoomIniciado : Boolean;
        FOnPictureChange: TNotifyEvent;
        FOnMouseDown: TMouseEvent;
        FOnMouseMove: TMouseMoveEvent;
        FOnMouseUp: TMouseEvent;

      { Private methods of TImageExt }
        { Method to set variable and property values and create objects }
        procedure AutoInitialize;
        { Method to free any objects created by AutoInitialize }
        procedure AutoDestroy;
        procedure SetCenter(Value : Boolean);
        procedure SetOffsetX(Value : Integer);
        procedure SetOffsetY(Value : Integer);
        procedure SetPicture(Value : TPicture);
        procedure SetScale(Value : Extended);
        procedure SetStretch(Value : Boolean);
        procedure SetModoZoom(const Value: Boolean);
        function DestRect:TRect;
        procedure UpdatePlate;
        procedure dibujarRecuadro(puntoInicio, PuntoFin: TPoint);

    protected
      { Protected fields of TImageExt }

      { Protected methods of TImageExt }
        procedure Paint; override;
        procedure Resize; override;
        procedure PictureChanged(Sender: TObject); virtual;
        procedure IniciarZoom(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
        procedure DibujarZoom(Sender: TObject; Shift: TShiftState; X, Y: Integer); virtual;
        procedure CalcularZoom(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;

    public
      { Public methods of TImageExt }
        constructor Create(AOwner: TComponent); override;
        destructor Destroy; override;
        procedure ShowAll;

    published
      { Published properties of TImageExt }
        property OnClick;
        property OnDblClick;
        property OnDragDrop;
        property OnEnter;
        property OnExit;
        property OnKeyDown;
        property OnKeyPress;
        property OnKeyUp;
        property Align default alClient;
        property Center : Boolean read FCenter write SetCenter default False;
        property Color default clBtnFace;
        property visible;
        property OffsetX : Integer read FOffsetX write SetOffsetX default 0;
        property OffsetY : Integer read FOffsetY write SetOffsetY default 0;
        property Picture : TPicture read FPicture write SetPicture;
        property Scale : Extended read FScale write SetScale;
        property ModoZoom : Boolean read FModoZoom write SetModoZoom default False;
        property Stretch : Boolean read FStretch write SetStretch default False;
        property OnStartDrag;
        property OnDragOver;
        property OnPictureChange :TNotifyEvent read FOnPictureChange write FOnPictureChange;
        property OnMouseDown : TMouseEvent read FOnMouseDown write FOnMouseDown;
        property OnMouseMove : TMouseMoveEvent read FOnMouseMove write FOnMouseMove;
        property OnMouseUp : TMouseEvent read FOnMouseUp write FOnMouseUp;
        property OnResize;
        property OnScaleChange : TScaleChangeEvent read FOnScaleChange write FOnScaleChange;
    end;


implementation

//TImageExt
procedure TImageExt.PictureChanged(Sender:TObject);
begin
    UpdatePlate;
    if assigned (FOnPictureChange) then FOnPictureChange (Self);
end;

procedure TImageExt.AutoInitialize;
begin
//     Screen.Cursors[crPlate] := LoadCursor(HInstance, 'CRPLATE');
     Align := alClient;
     FCenter := False;
     FOffsetX := 0;
     FOffsetY := 0;
     FPicture := TPicture.Create;
     FPicture.OnChange := PictureChanged;
     FScale := 1.0;
     FStretch := False;
     ZoomIniciado := False;
     inherited onMouseDown := IniciarZoom;
     inherited onMouseMove := DibujarZoom;
     inherited onMouseUp := CalcularZoom;
end; { of AutoInitialize }

{ Method to free any objects created by AutoInitialize }
procedure TImageExt.AutoDestroy;
begin
     FPicture.Free;
end; { of AutoDestroy }

procedure TImageExt.SetCenter(Value : Boolean);
begin
   if not FStretch then begin
      FCenter := Value;
      if FCenter then begin
          FOffsetX:=(Width - trunc(FPicture.Width*FScale)) div 2;
          FOffsetY:=(Height - trunc(FPicture.Height*FScale)) div 2;
      end;
      if (FOffsetX < 0) or (FOffsetY < 0) then
          InvalidateRect(Handle, nil, False)
      else
          invalidate;
   end;
end;

procedure TImageExt.SetOffsetX(Value : Integer);
begin
    if not FCenter then begin
        FOffsetX := Value;
        if FOffsetX < 0 then
            InvalidateRect(Handle, nil, False)
        else
            invalidate;
    end;
end;


procedure TImageExt.SetOffsetY(Value : Integer);
begin
    if not FCenter then begin
        FOffsetY := Value;
        if FOffsetY < 0 then
            InvalidateRect(Handle, nil, False)
        else
            invalidate;
    end;
end;


procedure TImageExt.SetPicture(Value : TPicture);
begin
    FPicture.Assign(Value);
    UpdatePlate;
end;


procedure TImageExt.SetScale(Value : Extended);
begin
    if value<=0 then
        raise Exception.create('Scale must be greater that 0');
    if not FStretch then begin
        if assigned(FOnScaleChange) then
            FOnScaleChange(self, value);
        if FCenter then begin
            FOffsetX:=(Width - trunc(FPicture.Width*value)) div 2;
            FOffsetY:=(Height - trunc(FPicture.Height*value)) div 2;
        end;
        FScale := Value;
        if (FOffsetX <= 0) and (FOffsetY <= 0) then
            InvalidateRect(Handle, nil, False)
        else
            invalidate;
    end;
end;


procedure TImageExt.SetStretch(Value : Boolean);
begin
    FStretch := Value;
    FCenter:=FStretch;
    invalidate;
end;


constructor TImageExt.Create(AOwner: TComponent);
begin
     inherited Create(AOwner);
     AutoInitialize;
end;


function TImageExt.DestRect:TRect;
begin
   Result := Rect(FOffsetX, FOffsetY, FOffsetX+trunc(FPicture.Width*FScale),
                  FOffsetY+Trunc(FPicture.Height*FScale));
end;


destructor TImageExt.Destroy;
begin
     AutoDestroy;
     inherited Destroy;
end;

procedure TImageExt.Paint;
begin
  if csDesigning in ComponentState then
    with inherited Canvas do begin
      Pen.Style := psDash;
      Brush.Style := bsClear;
      Rectangle(0, 0, Width, Height);
    end;
    with inherited Canvas do
        if not FStretch then
            StretchDraw(DestRect, Picture.Graphic)
        else
            StretchDraw(Rect(0,0,width,Height), Picture.Graphic);
end;

procedure TImageExt.UpdatePlate;
begin
    if (FPicture.width>0) and (FPicture.height>0) then begin
        if not FStretch then
            if FCenter then begin
                FOffsetX:=(Width - trunc(FPicture.Width*FScale)) div 2;
                FOffsetY:=(Height - trunc(FPicture.Height*FScale)) div 2;
            end;
        if (FOffsetX <= 0) or (FOffsetY <= 0) then
            InvalidateRect(Handle, nil, False)
        else
            invalidate;
    end else invalidate;

end;

procedure TImageExt.Resize;
begin
  updatePlate;
  inherited resize;
end;

procedure TImageExt.SetModoZoom(const Value: Boolean);
begin
    FModoZoom := value;
    if modoZoom then
        Cursor := crLupa
    else
        cursor := crDefault;
end;


procedure TImageExt.CalcularZoom(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
    anchoArea, altoArea, OldPosX, OldPosY,
    margenVertical, margenHorizontal : Integer;
begin
    if modoZoom and ZoomIniciado then begin
//Libero el area de recorrido para el mouse.
        clipCursor(nil);
//Tapo el ultimo dibujo
        dibujarRecuadro(puntoInicial,ultimoPunto);
//Calculo la escala
        anchoArea := trunc(abs(puntoInicial.x - x) / scale);
        altoArea := trunc(abs(puntoInicial.y - y) / scale);
        if (anchoArea <> 0) and (altoArea <> 0) then begin
            OldPosX := trunc((min(puntoInicial.x,x) - offsetX) / scale);
            OldPosY := trunc((min(puntoInicial.y,y) - offsetY) / scale);
            if anchoArea > altoArea then
                Scale := width / anchoArea
            else
                Scale := height / altoArea;
            MargenHorizontal := (width - trunc(anchoArea * Scale)) div 2;
            offsetX := MargenHorizontal - trunc(OldPosX * Scale);
            MargenVertical := (Height - trunc(altoArea * Scale)) div 2;
            offsetY := MargenVertical - trunc(OldPosY * Scale) ;
        end;
        ZoomIniciado := False;
    end;
    if assigned(FOnMouseUp) then
        FOnMouseUp(Sender, Button, Shift, X, Y);
end;

procedure TImageExt.DibujarZoom(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
    if (ssLeft in shift) and modoZoom and ZoomIniciado then begin
// Tapo el dibujo anterior
        dibujarRecuadro(puntoInicial,ultimoPunto);
// Hago el nuevo dibujo
        dibujarRecuadro(puntoInicial,point(X,Y));
        ultimoPunto := point(x,y);
    end;
    if assigned(FOnMouseMove) then
        FOnMouseMove(Sender, Shift, X, Y);
end;

procedure TImageExt.IniciarZoom(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var pr: PRect;
begin
    if ModoZoom and ((x >= FOffsetX) and (x <= FOffSetX + picture.Width * FScale))
                and ((y >= FOffsetY) and (y <= FOffSetY + picture.Height * FScale)) then begin
//Defino el area de recorrido para el mouse
        new(pr);
        pr.left := ClientToScreen(Point(max(self.left,FOffsetX),0)).x;
        pr.top := ClientToScreen(Point(0,max(self.top,FOffsetY))).y;
        pr.Right := pr.left + min(self.Width,trunc(picture.Width * FScale));
        pr.Bottom := pr.top + min(self.Height,trunc(picture.Height * FScale));
        clipCursor(pr);
        dispose(pr);
//Inicializo Variables para calcular area de Zoom
        puntoInicial := point(x,y);
        ultimoPunto := point(x,y);
        with canvas do begin
            pen.style := psDot;
            pen.mode := pmXor;
        end;
        ZoomIniciado := True;
    end;
    if assigned(FOnMouseDown) then
        FOnMouseDown(Sender, Button, Shift, X, Y);
end;

procedure TImageExt.dibujarRecuadro(puntoInicio, PuntoFin: TPoint);
begin
    with canvas do begin
// Dibujo la linea de arriba
        moveTo(min(puntoInicio.x,puntoFin.x),min(puntoInicio.y,puntoFin.y));
        lineTo(max(puntoInicio.x,puntoFin.x),min(puntoInicio.y,puntoFin.y));
// Dibujo la linea vertical derecha
        moveTo(max(puntoInicio.x,puntoFin.x),min(puntoInicio.y,puntoFin.y));
        lineTo(max(puntoInicio.x,puntoFin.x),max(puntoInicio.y,puntoFin.y));
// Dibujo la linea de abajo
        moveTo(max(puntoInicio.x,puntoFin.x),max(puntoInicio.y,puntoFin.y));
        lineTo(min(puntoInicio.x,puntoFin.x),max(puntoInicio.y,puntoFin.y));
// Dibujo la linea vertical izquierda
        moveTo(min(puntoInicio.x,puntoFin.x),max(puntoInicio.y,puntoFin.y));
        lineTo(min(puntoInicio.x,puntoFin.x),min(puntoInicio.y,puntoFin.y));
    end;
end;

procedure TImageExt.ShowAll;
var
    Centrada : Boolean;
begin
    if (picture.width <> 0) and (not FStretch) then begin
        Centrada := FCenter;
        Center := true;
        scale := max((width / picture.width), (height / picture.Height));
        Center := Centrada;
    end;
end;



end.
