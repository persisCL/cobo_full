unit DPSPageControl;

interface

uses
  Windows, Messages, SysUtils, Classes, ComCtrls, Controls, Graphics, Dialogs, ExtCtrls;

type
  TDPSPageControl = class(TPageControl)
  private
	FColor: TColor;
	FPageColor: TColor;
	procedure SetColor(const value: TColor);
	procedure setPageColor(const value: TColor);
  protected
	procedure Loaded; override;
	procedure DrawTab(TabIndex: Integer; const Rect: TRect; Active: Boolean); override;
	procedure WMPaint(var message: TMessage); message WM_PAINT;
	procedure UpdateActivePage; override;
  public
	constructor Create(AOwner: TComponent); override;
  published
	property PageColor: TColor read FPageColor write SetPageColor;
	property Color: TColor read FColor write SetColor;
  end;

implementation

constructor TDPSPageControl.Create(AOwner: TComponent);
Begin
	inherited create(aOwner);
	OwnerDraw := True;
	FColor := clBtnFace;
	FPageColor := clBtnFace;
end;


procedure TDPSPageControl.DrawTab(TabIndex: Integer; const Rect: TRect; Active: Boolean);
var DesplX, DesplY: Integer;
begin
	inherited DrawTab(TabIndex, Rect, Active);
	if Active then
		 canvas.Font.Color := clNavy
	else
		 canvas.Font.Color := clBlack;
	DesplX := ((Rect.Right - Rect.Left - Canvas.TextWidth(Pages[tabIndex].caption)) div 2);
	DesplY := ((Rect.Bottom - Rect.top - Canvas.TextHeight(Pages[tabIndex].caption)) div 2) + 1;
	Canvas.TextRect(Rect, Rect.left + DesplX, rect.top + DesplY, Pages[tabIndex].caption);
end;

procedure TDPSPageControl.Loaded;
var i: Integer;
begin
	inherited;
	for i := 0 to PageCount - 1 do
		Pages[i].Brush.Color := FPageColor;
end;

procedure TDPSPageControl.setPageColor(const value: TColor);
var i: Integer;
begin
	FPageColor := value;
	for i := 0 to PageCount - 1 do
		Pages[i].Brush.Color := FPageColor;
	PaintTo(Canvas, 0, 0);
end;

procedure TDPSPageControl.setColor(const value: TColor);
begin
	FColor := value;
	Brush.Color := value;
	PaintTo(Canvas, 0, 0);
end;

procedure TDPSPageControl.WMPaint(var message: TMessage);
begin
	inherited;
end;

procedure TDPSPageControl.UpdateActivePage;
begin
	inherited;
	loaded;
end;

end.
