{******************************************************************************}
{ Componente para visualizar Im�genes.                                         }
{   Permite aplicar un zoom sobre la imagen y centrar esta en un punto         }
{   determinado y especialmente en el centro.                                   }
{																			   }
{******************************************************************************}

unit ImagePlus;

interface

uses
  Windows, Messages, Classes, Controls, Graphics, Forms;

type
  TImagePlus = class(TScrollingWinControl)
  private
	{ Private declarations }
	FPicture: TPicture;
	FStretch: Boolean;
	FScale: Double;
	procedure SetStretch(const Value: Boolean);
	procedure SetScale(const Value: Double);
    function  GetVirtualImageSize: TSize;
    procedure AdjustScrollbars;
    function  GetWindowCenter: TPoint;
  protected
	{ Protected declarations }
    property  VirtualImageSize: TSize read GetVirtualImageSize;
	procedure Resize; override;
	procedure PictureChanged(Sender: TObject);
    procedure CenterTo(aPoint: TPoint);
    procedure WMPaint (var Message: TWMPaint); message WM_PAINT;
    procedure WMEraseBkgnd(var Message: TWMEraseBkgnd); message WM_ERASEBKGND;
  public
	{ Public declarations }
	constructor Create(AOwner: TComponent); override;
	destructor Destroy; override;
    procedure  CenterToPoint(aPoint: TPoint);
    procedure  Center;
  published
	{ Published declarations }
	property Align;
	property Anchors;
	property AutoSize;
    property Color;
	property Constraints;
	property DragCursor;
	property DragKind;
	property DragMode;
	property Enabled;
	property ParentShowHint;
	property Picture: TPicture read FPicture;
	property PopupMenu;
	property ShowHint;
	property Stretch: Boolean read FStretch write SetStretch default False;
	property Visible;
    property Scale: Double read FScale write SetScale;
	property OnClick;
	property OnContextPopup;
	property OnDblClick;
	property OnDragDrop;
	property OnDragOver;
	property OnEndDock;
	property OnEndDrag;
	property OnMouseDown;
	property OnMouseMove;
	property OnMouseUp;
	property OnStartDock;
	property OnStartDrag;
	property OnResize;
	//
  end;


implementation

{ TImagePlus }

constructor TImagePlus.Create(AOwner: TComponent);
begin
	inherited Create(AOwner);
	ControlStyle   := ControlStyle - [csAcceptsControls];
	Height		   := 120;
	Width          := 120;
    AutoScroll     := False;
    //
    FPicture := TPicture.Create;
	FPicture.OnChange := PictureChanged;
	//
	FScale		    := 1;
    HorzScrollBar.Style := ssHotTrack;
end;

destructor TImagePlus.Destroy;
begin
	FPicture.Free;
	inherited Destroy;
end;

procedure TImagePlus.Resize;
begin
    if assigned(Picture.Graphic) and (not Picture.Graphic.Empty) then begin
        AdjustScrollbars;
    end;
    Invalidate;
 	inherited;
end;

procedure TImagePlus.SetStretch(const Value: Boolean);
begin
    // Si el valor no cambio no hacemos nada
	if Value = FStretch then Exit;
    // Actualizamos el Valor
	FStretch := Value;
    // Posicionamos y dibujamos la imagen
    if assigned(Picture.Graphic) and (not Picture.Graphic.Empty) then begin
        AdjustScrollbars;
    end;
    Invalidate;
end;

procedure TImagePlus.SetScale(const Value: Double);
var
    aPoint: TPoint;
begin
    // Si la escala del Zoom es 0, o es igual a la anterior, o esta en modo Stretch el Zoom no aplica
    if (FStretch) or (Value = FScale) or (Value <= 0) then Exit;
    aPoint := GetWindowCenter;
    // Guardamos el valor anterior y aplicamos la nueva escala
    FScale := Value;
    //
    AdjustScrollbars;
    centerToPoint(aPoint);
    Invalidate;
end;

procedure TImagePlus.CenterTo(aPoint: TPoint);
begin
    //
    LockWindowUpdate(Handle);
    if Width < HorzScrollBar.Range then begin
        // Ancho de la ventana Menor que ancho de la imagen
        HorzScrollBar.Position := aPoint.X - (width div 2);
    end;
    if Height < VertScrollBar.Range then begin
        // Alto de la ventana Menor que alto de la imagen
        VertScrollBar.Position := aPoint.Y - (height div 2);
    end;
    LockWindowUpdate(0);
end;

procedure TImagePlus.CenterToPoint(aPoint: TPoint);
begin
    if FStretch then exit;
    CenterTo(Point(trunc(aPoint.X * FScale), trunc(aPoint.Y * FScale)));
end;

procedure TImagePlus.PictureChanged(Sender: TObject);
begin
    AdjustScrollbars;
    Invalidate;
end;

procedure TImagePlus.Center;
begin
    if FStretch then exit;
    centerTo(Point(HorzScrollBar.Range div 2, VertScrollBar.Range div 2));
end;


procedure TImagePlus.WMPaint(var Message: TWMPaint);
Var
    R: TRect;
    Canvas: TControlCanvas;
begin
    if not Assigned(FPicture.Graphic) then begin
        inherited;
        Exit;
    end;
    Canvas := TControlCanvas.Create;
    Canvas.Control := Self;
    try
        R.Left   := -HorzScrollBar.Position;
        R.Top    := -VertScrollBar.Position;
        R.Right  := R.Left + VirtualImageSize.cx;
        R.Bottom := R.Top  + VirtualImageSize.cy;
        Canvas.StretchDraw(R, FPicture.Graphic);
        // Rellenamos los espacios a los costados
        Canvas.Brush.Color := Color;
        if R.Left > 0 then Canvas.FillRect(Rect(0, 0, R.Left - 1, ClientHeight));
        if R.Top  > 0 then Canvas.FillRect(Rect(0, 0, ClientWidth, R.Top - 1));
        if R.Right  < ClientWidth then Canvas.FillRect(Rect(R.Right, 0, ClientWidth, ClientHeight));
        if R.Bottom < ClientHeight then Canvas.FillRect(Rect(0, R.Bottom, ClientWidth, ClientHeight));
        ValidateRect(Handle, nil);
    finally
        Canvas.Free;
    end;
end;

procedure TImagePlus.WMEraseBkgnd(var Message: TWMEraseBkgnd);
begin
    if not Assigned(FPicture.Graphic) then begin
        inherited;
    end;
end;

function TImagePlus.GetVirtualImageSize: TSize;
begin
    if not Assigned(FPicture.Graphic) then begin
        Result.cx := 0;
        Result.cy := 0;
    end else if FStretch then begin
        Result.cx := ClientWidth;
        Result.cy := ClientHeight;
    end else if FScale = 1 then begin
        Result.cx := FPicture.Graphic.Width;
        Result.cy := FPicture.Graphic.Height;
    end else begin
        Result.cx := Round(FPicture.Graphic.Width * FScale);
        Result.cy := Round(FPicture.Graphic.Height * FScale);
    end;
end;

procedure TImagePlus.AdjustScrollbars;
Var
    Size: TSize;
begin
    if FStretch then begin
        HorzScrollBar.Range := 0;
        VertScrollBar.Range := 0;
    end else begin
        HorzScrollBar.Visible := True;
        VertScrollBar.Visible := True;
        Size := VirtualImageSize;
        HorzScrollBar.Range := Size.cx;
        VertScrollBar.Range := Size.cy;
    end;
end;



function TImagePlus.GetWindowCenter: TPoint;
begin
    // Si la imagen no esta asignada retornamos 0, 0
    if not assigned(FPicture.Graphic) then begin
        result := Point(0, 0);
        exit;
    end;
    // Si el scrollbar horizontal es visible
    if HorzScrollBar.Visible then begin
        result.X := Trunc((HorzScrollBar.Position + (Width / 2)) / FScale);
    end else
        // Si no esta visible retornamos el medio de la imagen
        result.X := (FPicture.Graphic.Width div 2);
    // Si la scrollbar vertical es visible
    if HorzScrollBar.Visible then begin
        result.y := Trunc((VertScrollBar.Position + (Height / 2)) / FScale);
    end else
        result.Y := (FPicture.Graphic.Height div 2);


  //  Result := Point(Trunc((HorzScrollBar.Position + (Width / 2)) * (1 / FScale)), Trunc((VertScrollBar.Position + (Height / 2)) * (1 / FScale)));
//    Result := Point(Trunc((Width / 2)), Trunc((Height / 2)));
end;

end.
