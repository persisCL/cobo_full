program IOPrueba;

uses
  Forms,
  PruebaIO in 'PruebaIO.pas' {Form1},
  DMConnection in '..\..\..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  Login in '..\..\..\Comunes\Login.pas' {LoginForm},
  OPButtons in '..\..\Buttons\OPButtons.pas';

{$R *.res}
begin
  Application.Initialize;
  Application.CreateForm(TDMConnections, DMConnections);
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TLoginForm, LoginForm);
  if not Form1.Inicializa then Form1.Close;
  Application.Run;
end.
