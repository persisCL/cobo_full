{ *****************************************************************************}
{ Componente para la carga y visualizaci�n de las im�genes overview de un      }
{ tr�nsito, m�s el esquema asociado a las mismas.                              }
{																			   }
{ *****************************************************************************}

unit OPImageOverview;
    
interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms, ExtCtrls, ADODB, StdCtrls,
  Graphics, ImgTypes, ImgProcs, ProcsOverview, Math, Util, UtilProc, constParametrosGenerales;

type
  TModoVisual = (vmSoloImage, vmSoloGrafico, vmAmbos);

Const
  MVDefault = vmAmbos;

Type
  TEscala = 0..1000;
  
  TImageOverview = class(TScrollBox)
  private
	{ Private declarations }
	FPanel: TPanel;
	FImagen: TImage;
    FPuntoCobro: Integer;
	FLastError: AnsiString;
	FEscalaVisual: TEscala;
	FVisualMode: TModoVisual;
	FADOConnection: TADOConnection;
	ESCALA_VISUAL: Integer;
	//
	FDibuAuto:      Array[0..MAX_VEHICULOS] of TShape;
	FAutos:   		Array[0..MAX_VEHICULOS] of TPanel;
	FTags: 			Array[0..MAX_VEHICULOS] of TLabel;
	FListaPatentes: Array[0..MAX_VEHICULOS] of TLabel;
	FDatosAutos:    TDatosAutos;
	FInteractiveMode: Boolean;
	FTagConflicto_CM: BYTE;
	FTagConflicto_SN: Double;
	FAutoOrigen, FAutoDestino, FAutoSugerido: Integer;
	FNumCorrCO: Integer;
    FExisteSugerido: Boolean;
	//
	procedure InicializarOverview;
	function  ArmarGrafico(DatosImg: TDataOverview; mmPixel: Integer): Boolean;
	procedure DibujarVehiculo(NumAuto: Integer; x1, x2, y1, y2: Double; DatosImg: TDataOverview; Actual: Boolean);
	//
	procedure SetADOConnection(const Value: TADOConnection);
	procedure SetVisualMode(const Value: TModoVisual);
	procedure SetEscala(const Value: TEscala);
	procedure DibujarEsquema(Imagen: TBitmap; DatosImg: TDataOverview);
	procedure SetInteractiveMode(const Value: Boolean);
  protected
	{ Protected declarations }
	procedure OcultarGrafico;
	procedure OcultarImagen;
	procedure MostrarImagenConGrafico;
	procedure AutoMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
	procedure AutoTagDragDrop(Sender, Source: TObject; X, Y: Integer); virtual;
	procedure AutoTagDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean); virtual;
	function  AsignarTag(MoverTag: Boolean; Destino: TShape): Boolean;
	function  QuitarTag(Origen: TShape): Boolean;
  public
	{ Public declarations }
	constructor Create(AOwner: TComponent); override;
	destructor  Destroy; override;
	procedure   Resize; override;
	//
	function  MoverTag: Boolean;
	function  GrabarMovimientoTAG: Boolean;
	function  LoadImage(NumCorrCO: Integer; FechaHora: TDateTime; PatenteEsperada: AnsiString; TipoImagen: TTipoImagen): Boolean; overload;						// SS_1091_MCA_20130711
    function  LoadImage(NumCorrCO: Integer; FechaHora: TDateTime; PatenteEsperada: AnsiString; TipoImagen: TTipoImagen; EsItaliano: Boolean): Boolean overload;	// SS_1091_MCA_20130711
	procedure Reset;
	property  LastError: AnsiString Read FLastError;
	property  ExisteSugerido: Boolean read FExisteSugerido;
  published
	{ Published declarations }
	property Width default 100;
	property Height default 120;
	property ADOConnection: TADOConnection read FADOConnection write SetADOConnection;
	property VisualMode: TModoVisual read FVisualMode write SetVisualMode default MVDefault;
	property EscalaImage: TEscala read FEscalaVisual write SetEscala default 100;
	property InteractiveMode: Boolean read FInteractiveMode write SetInteractiveMode;
	property OnResize;
	//
	property Align;
	property Anchors;
	property AutoSize;
	property Constraints;
	property DockSite;
	property DragCursor;
	property DragKind;
	property DragMode;
	property Enabled;
	property Ctl3D;
	property Font;
	property ParentBiDiMode;
	property ParentColor;
	property ParentCtl3D;
	property ParentFont;     
	property ParentShowHint;
	property PopupMenu;
	property ShowHint;
	property TabOrder;    
	property TabStop;
	property Visible;     
	//
	property OnCanResize;
	property OnClick;
	property OnConstrainedResize;
	property OnContextPopup;
	property OnDblClick;
	property OnDockDrop;
	property OnDockOver;
	property OnDragDrop;
	property OnDragOver;
	property OnEndDock;
	property OnEndDrag;
	property OnEnter;
	property OnExit;
	property OnGetSiteInfo;
	property OnMouseDown;
	property OnMouseMove;
	property OnMouseUp;
	property OnStartDock;
	property OnStartDrag;
	property OnUnDock;     
  end;


implementation


{ TImageOverview }

constructor TImageOverview.Create(AOwner: TComponent);
begin
	inherited Create(AOwner);
	ControlStyle := ControlStyle - [csAcceptsControls];
	//
	Width  		  := 100;
	Height 		  := 120;
	BevelWidth    := 1;
	BevelOuter    := bvRaised;
	FEscalaVisual := 100;
	ESCALA_VISUAL := 1;
	FVisualMode   := MVDefault;
	//
	FPanel := TPanel.Create(Self);
	FPanel.Parent       := Self;
	FPanel.BevelOuter   := bvNone;
	FPanel.Height 	   	:= Height - 25;
	FPanel.ControlStyle := ControlStyle - [csAcceptsControls];
	//
	FImagen := TImage.Create(Self);
	FImagen.Parent  := FPanel;
	FImagen.Top     := 0;
	FImagen.Left    := 0;
	FImagen.Width   := 1;
	FIMagen.Height  := 1;
	FImagen.Stretch := True;
	//
	InteractiveMode := True;
end;

destructor TImageOverview.Destroy;
var
	i: Integer;
begin
	for i := 0 to MAX_VEHICULOS do begin
		// Liberar Imagenes y Paneles
		FDibuAuto[i].Free;
		FListaPatentes[i].Free;
		FAutos[i].Free;
		FTags[i].Free;
	end;
	FImagen.Free;
	FPanel.Free;
	inherited Destroy;
end;

procedure TImageOverview.DibujarEsquema(Imagen: TBitmap; DatosImg: TDataOverview);
var
    mmPixel: Integer;
begin
	try
    	if Swap(DatosImg.FactorEscala) = 0 then Exit;
		// Las longitudes est�n en cm. El factor de escala en 0.1 mm/pixel. Convertirlos.
   		mmPixel := Round(Swap(DatosImg.FactorEscala) * 0.1);

		// Dimensionamos y asignamos la imagen
        FImagen.Width   := Round(Imagen.Width  * (FEscalaVisual / 100));
        FImagen.Height  := Round(Imagen.Height * (FEscalaVisual / 100));
        FImagen.Stretch := True;
        FImagen.Picture.Assign(Imagen);

        // Dimensiones del Panel que contiene la imagen
		FPanel.Width  := max(FImagen.Width + MARGEN_PX * 2, ClientWidth - 17);
		FPanel.Height := max(FImagen.Height + MARGEN_PX * 2, ClientHeight - 17);

        // Posicion de la imagen dentro del Panel
        FImagen.Left    := Max(MARGEN_PX, ((ClientWidth - 17 - FImagen.Width) div 2) + MARGEN_PX);
        FImagen.Top     := Max(MARGEN_PX, ((ClientHeight - 17 - FImagen.Height) div 2) + MARGEN_PX);

        // Posici�n del panel dentro de la imagen
        VertScrollBar.Range	:= FPanel.Height;
		HorzScrollBar.Range	:= FPanel.Width;
        VertScrollBar.Position := ((VertScrollBar.Range - Height) div 2);
        HorzScrollBar.Position := ((HorzScrollBar.Range - Width) div 2);

        // Finalmente nos aseguramos que el panel este visible y armamos el gr�fico
		FPanel.Visible := True;
		ArmarGrafico(DatosImg, mmPixel);
	except
	end;
end;

function TImageOverview.LoadImage(NumCorrCO: Integer; FechaHora: TDateTime;
  PatenteEsperada: AnsiString; TipoImagen: TTipoImagen): Boolean;
var
	miBmp: TBitmap;
    ImagenRotada : TBitMap;
	modoactual: TModoVisual;
	ImagePath, DescriError: AnsiString;
    Error: TTipoErrorImg;
	DataImage: TDataImage;
begin
    Result 	   := False;
	miBmp := TBitmap.Create;
    IMagenRotada := TBitMap.Create;
    try
        try
            FNumCorrCO      := NumCorrCO;
            modoactual  	:= FVisualMode;
            FVisualMode 	:= vmAmbos;
            InicializarOverview;
            FillChar(DataImage, SizeOf(DataImage), 0);
            // Cargar la Imagen en Pantalla
            if not Assigned(FADOConnection) then
                raise exception.Create('Falta el par�metro ADOConnection');
            //
            if not ObtenerParametroGeneral(FADOConnection, DIR_IMAGENES_TRANSITOS_PENDIENTES, ImagePath) then
                raise exception.Create('Error obteniendo el directorio de imagenes de tr�nsitos pendientes');
          	if ObtenerImagenTransitoPendiente(ImagePath, NumCorrCO, TipoImagen, miBmp, DataImage, DescriError, Error) then begin

                if not RotarImagen(miBmp, ImagenRotada, DescriError) then
                    raise exception.Create(DescriError);

                // Cargar los datos Adicionales del fin de archivo
                CargarDatosAutos(FADOConnection, DataImage.DataOverView, NumCorrCO, FechaHora, PatenteEsperada, FDatosAutos, FExisteSugerido);

                // Dibujar el esquema
                DibujarEsquema(ImagenRotada, DataImage.DataOverView);
            end else begin
                raise exception.Create(DescriError);
            end;
            SetVisualMode(modoActual);
            Result := True;
        except
            on e:exception do begin
                FLastError := e.message;
            end;
        end;
    finally
    	miBmp.Free;
        ImagenRotada.free;
    end;
end;


{
Autor: Manuel Cabello
Fecha: 11-07-2013
SS_1091_MCA_20130711
Funcion Overload LoadImage
Valida si la imagen no es italiana para rotarla, en caso contrario la muestra tal como viene. 
}
function TImageOverview.LoadImage(NumCorrCO: Integer; FechaHora: TDateTime;
  PatenteEsperada: AnsiString; TipoImagen: TTipoImagen; EsItaliano: Boolean): Boolean;
var
	miBmp: TBitmap;
    ImagenRotada : TBitMap;
	modoactual: TModoVisual;
	ImagePath, DescriError: AnsiString;
    Error: TTipoErrorImg;
	DataImage: TDataImage;
begin
    Result 	   := False;
	miBmp := TBitmap.Create;
    IMagenRotada := TBitMap.Create;
    try
        try
            FNumCorrCO      := NumCorrCO;
            modoactual  	:= FVisualMode;
            FVisualMode 	:= vmAmbos;
            InicializarOverview;
            FillChar(DataImage, SizeOf(DataImage), 0);
            // Cargar la Imagen en Pantalla
            if not Assigned(FADOConnection) then
                raise exception.Create('Falta el par�metro ADOConnection');
            //
            if not ObtenerParametroGeneral(FADOConnection, DIR_IMAGENES_TRANSITOS_PENDIENTES, ImagePath) then
                raise exception.Create('Error obteniendo el directorio de imagenes de tr�nsitos pendientes');
          	if ObtenerImagenTransitoPendiente(ImagePath, NumCorrCO, TipoImagen, miBmp, DataImage, DescriError, Error) then begin

                if not EsItaliano and not RotarImagen(miBmp, ImagenRotada, DescriError) then
                    raise exception.Create(DescriError);

                // Cargar los datos Adicionales del fin de archivo
                CargarDatosAutos(FADOConnection, DataImage.DataOverView, NumCorrCO, FechaHora, PatenteEsperada, FDatosAutos, FExisteSugerido);

                if not EsItaliano then
                // Dibujar el esquema
                  DibujarEsquema(ImagenRotada, DataImage.DataOverView)
                else
                  DibujarEsquema(miBmp, DataImage.DataOverView);
            end else begin
                raise exception.Create(DescriError);
            end;
            SetVisualMode(modoActual);
            Result := True;
        except
            on e:exception do begin
                FLastError := e.message;
            end;
        end;
    finally
    	miBmp.Free;
        ImagenRotada.free;
    end;
end;

procedure TImageOverview.SetADOConnection(const Value: TADOConnection);
begin
	FADOConnection := Value;
end;

procedure TImageOverview.SetEscala(const Value: TEscala);
begin
	FEscalaVisual := Value;
end;

procedure TImageOverview.SetVisualMode(const Value: TModoVisual);
begin
	FVisualMode := Value;
	// Cambiar el modo de visualizaci�n
	case FVisualMode of
	   vmSoloImage: OcultarGrafico;
	   vmSoloGrafico: OcultarImagen;
	   vmAmbos: MostrarImagenConGrafico;
	end;
end;

procedure TImageOverview.InicializarOverview;
var
	i: Integer;
begin
	if Assigned(FImagen.Picture) then FImagen.Picture.Assign(nil);
	//
	FAutoOrigen   := -1;
	FAutoDestino  := -1;
	FAutoSugerido := -1; 
	FTagConflicto_CM := 0;
	FTagConflicto_SN := 0;
	//
	FillChar(FDatosAutos, SizeOf(FDatosAutos), 0);
	for i := 0 to MAX_VEHICULOS do begin
		// Liberar Imagenes y Paneles
		if not Assigned(FDibuAuto[i]) then Exit;
		FDibuAuto[i].Free;
		FListaPatentes[i].Free;
		FAutos[i].Free;
		FTags[i].Free;
	end; 
end;

function TImageOverview.ArmarGrafico(DatosImg: TDataOverview; mmPixel: Integer): Boolean;

	procedure MostrarAuto(NumAuto: Integer; Auto: TCoordVehiculo; Actual: Boolean);
	Var
		x, y, w, h, l: Integer;
	begin
		// Tiramos un Shape para indicar la x del veh�culo
		w := Round(Swap(Auto.Ancho) / mmPixel * 10);
		h := Round(Swap(Auto.Largo) / mmPixel * 10);

        l := Round((Swap(DatosImg.upperLeftY) - Swap(Auto.FrontY)) / mmPixel * 10);
		x := Round(FImagen.width / (FEscalaVisual / 100)) - (l + (w div 2));
   		y := Round((Swap(Auto.FrontX) - Swap(DatosImg.upperLeftX)) / mmPixel * 10) - h;
		Dec(x, 2);
		Dec(y, 2);
		Inc(w, 4);
		Inc(h, 4);
		x := Round(x * FEscalaVisual / 100) + FImagen.left;
		y := Round(y * FEscalaVisual / 100) + FImagen.top;
		w := Round(w * FEscalaVisual / 100);
		h := Round(h * FEscalaVisual / 100);
		DibujarVehiculo(NumAuto, x, x + w, y, y + h, DatosImg, Actual);
	end;

var
	i: Integer;
begin
	Result := False;
	try
		// Calcular los recuadros seg�n la ubicaci�n de los autos.
		// Desplazamiento dado por el tipo de imagen que sea.
		MostrarAuto(0, DatosImg.Actual, True);
		for i := 1 to MAX_VEHICULOS do begin
			if (SwapDWORD(DatosImg.Vehiculos[i].RegistrationID) = 0)
			  or (SwapDWORD(DatosImg.Vehiculos[i].VehicleID) = 0) then break;
			MostrarAuto(i, DatosImg.Vehiculos[i], False);
		end;
		Result := True;
	except
		on e: exception do begin
			MsgBox('Error' + e.Message);
		end;
	end;
end;

//****************************************************************************//
// Imagen OverView: FAutos tiene para cada tr�nsito el Panel Vehiculo[1] y el //
//   Panel Tag[2]. Cada Panel tiene en su propiedad 'tag' el n�mero de indice.//			                              		   //
//   Ademas FDatosAuto tiene para cada mismo i-tr�nsito, los datos del mismo. //
//   En el lugar 0 (Cero) posee los datos del tr�nsito ACTUAL.				  //	
//   La lista de Patentes tiene para cada �ndice la patente del tr�nsito.     //
//****************************************************************************//

procedure TImageOverview.DibujarVehiculo(NumAuto: Integer; x1, x2, y1, y2: Double; DatosImg: TDataOverview; Actual: Boolean);

	procedure CrearPatente(i: Integer; Patente: AnsiString);
	begin
		FListaPatentes[i] 			 := TLabel.Create(nil);
		FListaPatentes[i].Parent 	 := FPanel;
		FListaPatentes[i].Color      := clCream;
		FListaPatentes[i].Font.Size  := 8;
		FListaPatentes[i].Font.Color := clBlack;
		FListaPatentes[i].Caption 	 := ' ' + Patente;
		FListaPatentes[i].Visible 	 := False;
	end;

	procedure CrearImagenVehiculo(i: Integer; Actual: Boolean);
	begin
		FDibuAuto[i] 			 := TShape.Create(nil);
		FDibuAuto[i].Parent 	 := FPanel;
		FDibuAuto[i].Shape     	 := stRoundRect; 
		FDibuAuto[i].Top    	 := FAutos[i].Top;
		FDibuAuto[i].Left   	 := FAutos[i].Left;
		FDibuAuto[i].Height 	 := FAutos[i].Height;
		FDibuAuto[i].Width  	 := FAutos[i].Width;
		FDibuAuto[i].Brush.Style := bsClear;
		FDibuAuto[i].Pen.Mode  	 := pmCopy;
		FDibuAuto[i].Pen.Style	 := psDot; 
		FDibuAuto[i].Pen.Width 	 := 1;
		FDibuAuto[i].Pen.Color   := COLOR_COMUN;
		if Actual then FDibuAuto[i].Pen.Color := COLOR_ACTUAL;  
		if FDatosAutos[i].Sugerido then FDibuAuto[i].Pen.Color := COLOR_SUGERIDO;
		FDibuAuto[i].Visible   	 := True;	 
		FDibuAuto[i].Tag    	 := i;
		FDibuAuto[i].BringToFront;
		// 
		if FInteractiveMode then begin
			FDibuAuto[i].OnMouseDown := AutoMouseDown;
			FDibuAuto[i].DragMode    := dmAutomatic;
			FDibuAuto[i].OnDragOver  := AutoTagDragOver;
			FDibuAuto[i].OnDragDrop  := AutoTagDragDrop; 
		end;
	end;                  

	procedure CrearPanelTAG(i: Integer);
	begin
		FTags[i] 			 := TLabel.Create(nil);
		FTags[i].Tag    	 := i;
		FTags[i].Parent 	 := FPanel;
		FTags[i].Color  	 := FDibuAuto[i].Pen.Color;
		FTags[i].Left   	 := FAutos[i].Left + Round(FAutos[i].Width /4);
		FTags[i].Top    	 := Round(FAutos[i].Top + (FAutos[i].Height / 2) - 16);
		FTags[i].Width  	 := Round(FAutos[i].Width - 10);
		if FTags[i].Width < 28 then FTags[i].Width := 28;
		FTags[i].Height 	 := 20;
		FTags[i].Font.Size 	 := 4;
		FTags[i].Font.Color  := FTags[i].Color;
		FTags[i].Font.Style  := [fsBold];
		FTags[i].Transparent := True;
		//
		FDibuAuto[i].BringToFront;
	end;
	
	procedure CrearPanelAuto(i: Integer; x1, x2, y1, y2: Double; Actual: Boolean);
	begin
		// Crear el Panel del veh�culo
		FAutos[i] 			  := TPanel.Create(nil);
		FAutos[i].Parent 	  := FPanel;
		FAutos[i].Top    	  := Round(y1);
		FAutos[i].Left   	  := Round(x1);
		FAutos[i].Width  	  := Round(x2 - x1) + 1;
		FAutos[i].Height   	  := Round(y2 - y1) + 1;
		FAutos[i].Visible     := False;
		FAutos[i].Color  	  := clGray;
		FAutos[i].BevelOuter  := bvNone;
		FAutos[i].BorderStyle := bsNone;
		FAutos[i].SendToBack;	
	end;

resourceString
	MSG_TRANSITOACTUAL = 'Tr�nsito Actual';
	MSG_TRANSITOSUGERIDO = 'Tr�nsito Sugerido: El TAG corresponde a �sta Patente';
var
	NTAG_CM: BYTE;
	NTAG_SN: Double;
begin
	NTAG_CM  := FDatosAutos[NumAuto].ContextMark;
	NTAG_SN  := FDatosAutos[NumAuto].SerialNumber;
	//
	CrearPanelAuto(NumAuto, x1, x2, y1, y2, Actual);
	CrearImagenVehiculo(NumAuto, Actual);
	CrearPanelTAG(NumAuto);
	CrearPatente(NumAuto, FDatosAutos[NumAuto].Patente);
	//
	if not FInteractiveMode then Exit;
	if Actual then begin
		// Si es el transito actual, otro color, y el Tag.
		FDibuAuto[NumAuto].Hint := MSG_TRANSITOACTUAL;
		if NTAG_SN > 0 then begin
			// Le asignamos TAG, si es la primera vez.
			AsignarTag(False, FDibuAuto[NumAuto]);
			FTagConflicto_SN := NTAG_SN;
			FTagConflicto_CM := NTAG_CM;
		end else begin
			// Si ya se lo habiamos quitamos, nada.
			FTags[NumAuto].Caption := DESCRI_SIN_TAG;
		end;
	end else if FDatosAutos[NumAuto].Sugerido then begin
		// Es el que deberia tener el tag que tiene (o 'tenia' si ya lo movimos) el transito actual
		FDibuAuto[NumAuto].Hint := MSG_TRANSITOSUGERIDO;
	   //	sb_ReAsignarTag.Enabled := True;
		FAutoSugerido 			:= NumAuto;
//		TimerTAG.Tag     		:= NumAuto;
		if NTAG_SN > 0 then begin
			AsignarTag(False, FDibuAuto[NumAuto]);
		end else begin
			// El Timer corre para �ste tr�nsito
//			TimerTAG.Enabled := True;
		end;
	end else begin
		// No es al Actual ni el Sugerido
		if (NTAG_SN > 0) then begin
			// Si no es el sugerido y tiene tag
			if (NTAG_CM <> FTagConflicto_CM) OR (NTAG_SN <> FTagConflicto_SN) then begin
				// Si tubiese su Tag propio, no permitir que acepte otro
				FDibuAuto[NumAuto].OnDragOver := nil;
				FDibuAuto[NumAuto].OnDragDrop := nil;
				FTags[NumAuto].Caption    	  := DESCRI_TAG_OK;
			end else begin
				// Si es el Tag en conflicto
				AsignarTag(False, FDibuAuto[NumAuto]);
//				sb_ReAsignarTag.Enabled := True;
			end;
		end else begin
//			sb_ReAsignarTag.Enabled := True;
		end;
	end;

end;

procedure TImageOverview.MostrarImagenConGrafico;
var
	i: Integer;
begin
	try
		if (FImagen.Picture.Width <= 0) then Exit;
		FImagen.Visible := True;
		for i:= 0 to MAX_VEHICULOS do begin
			if Assigned(FDibuAuto[i]) then begin
				FDibuAuto[i].Pen.Style    := psDot;
				FDibuAuto[i].Brush.Style  := bsClear;
				FTags[i].Visible 	      := True;
				FListaPatentes[i].Visible := FListaPatentes[i].Tag > 0;
				FDibuAuto[i].BringToFront;
			end;
		end;
	except
	end;
end;

procedure TImageOverview.OcultarGrafico;
var
	i: Integer;
begin
	// Ocultar el esquema, dejando s�lo la foto visible
	FImagen.Visible := True;
	for i:= 0 to MAX_VEHICULOS do begin
		if Assigned(FDibuAuto[i]) then begin
			FDibuAuto[i].Brush.Style  := bsClear;
			FDibuAuto[i].Pen.Style    := psClear;
			FTags[i].Visible 	      := False;
			FListaPatentes[i].Visible := False;
		end;
	end;	
end;

procedure TImageOverview.OcultarImagen;
var
	i: Integer;
begin
	// Ocultar la imagen, dejando s�lo visible el esquema 
	FImagen.Visible := False;
	for i:= 0 to MAX_VEHICULOS do begin
		if Assigned(FDibuAuto[i]) then begin
			FDibuAuto[i].Pen.Style    := psDot;
			FDibuAuto[i].Brush.Color  := clGray;
			FTags[i].Visible 	      := True;
			FListaPatentes[i].Visible := FListaPatentes[i].Tag > 0;
			FTags[i].BringToFront;
		end;
	end;
end;

procedure TImageOverview.Resize;
begin
	inherited;
	FPanel.Height := Max((Height - 25), FPanel.Height);
end;

procedure TImageOverview.SetInteractiveMode(const Value: Boolean);
begin
	FInteractiveMode := Value;
end;

procedure TImageOverview.AutoMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
	i: Integer;
begin
	if (Button <> mbRight) then Exit; 
	i := (Sender as TShape).Tag;
	if FListaPatentes[i].Visible then begin
		FlistaPatentes[i].Tag 	  := 0;
		FListaPatentes[i].Visible := False;
	end else begin
		FListaPatentes[i].Width   := 55;
		FListaPatentes[i].Height  := 16;
		FListaPatentes[i].Left    := FAutos[i].Left + FAutos[i].Width;
		FListaPatentes[i].Top     := FAutos[i].Top + Y;
		FListaPatentes[i].Visible := True;
		FlistaPatentes[i].Tag 	  := 1;
		FListaPatentes[i].BringToFront; 
	end; 
end;

procedure TImageOverview.AutoTagDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
	// Habilitar a seleccionar para mover solo si es el que tiene el TAG.
	if (Source is TShape) then begin
		if FDatosAutos[(Source as TShape).tag].TAGAsignado then Accept := True
		  else Accept := False;
	end else begin
		Accept := False
	end;
end;

procedure TImageOverview.AutoTagDragDrop(Sender, Source: TObject; X, Y: Integer);
var
	Origen, Destino: TShape;
begin
	// Si los dos son Paneles, y son diferentes
	if (Sender is TShape) and (Source is TShape) and (Sender <> Source) then begin
		Origen  := (Source as TShape);
		Destino := (Sender as TShape);
		// El Destino
		if AsignarTag(True, Destino) then begin
			// Si es el que deberia tener el TAG
			if (Destino.Tag = FAutoSugerido) then begin
//				TimerTag.Enabled := False;
			end;
			// El Origen
			if QuitarTag(Origen) then begin
				FAutoOrigen  := Origen.tag;
				FAutoDestino := Destino.tag;
				// Si le quit� el Tag al que deberia tenerlo, timer
				if (Origen.Tag = FAutoSugerido) then begin
//					TimerTag.Enabled := True;
				end;
			end;
		end;
	end;
end;

function TImageOverview.AsignarTag(MoverTag: Boolean; Destino: TShape): Boolean;
begin
	Result := True;
	FTags[Destino.Tag].Caption           := DESCRI_TAG;
	FDatosAutos[Destino.Tag].TAGAsignado := True;
end;

function TImageOverview.QuitarTag(Origen: TShape): Boolean;
begin
	Result := True;
	FTags[Origen.Tag].Caption    := '';
	FDatosAutos[Origen.Tag].TAGAsignado := False;
end;

function TImageOverview.GrabarMovimientoTAG: Boolean;
begin
	Result := True;
	if (not FInteractiveMode) then Exit;
	if (FAutoOrigen = -1) or (FAutoDestino = -1) then Exit;
	//
	if not ActualizarTransitosCambioTag(FADOConnection, FPuntoCobro,
	  FNumCorrCO, FAutoOrigen, FAutoDestino, FDatosAutos) then begin
		Result := False;
	end;
end;


function TImageOverview.MoverTag: Boolean;
var
	i: Integer;
begin
	Result := False;
	try 
		// Si lo estamos moviendo con bot�n, no con drag & drop.
		if FAutoSugerido < 0 then Exit;
		// Mover el Tag de donde est� a la ubicaci�n de TimerTag
		if not FDatosAutos[FAutoSugerido].TAGAsignado then begin
			//Asignar al que corresponde
			if AsignarTag(True, FDibuAuto[FAutoSugerido]) then begin
	//			TimerTag.Enabled := False;
				for i := 0 to MAX_VEHICULOS do begin
					// Vaciar el ex due�o del tag
					if (FDatosAutos[i].TAGAsignado) and (i <> FAutoSugerido) then begin
						if QuitarTag(FDibuAuto[i]) then begin
							FAutoOrigen  := i;
							FAutoDestino := FAutoSugerido;
						end;
						Break;
					end;
				end;
			end;
		end else begin
			// Volver al Tag al Tr�nsito Actual
	//		TimerTag.Enabled := True;
			if AsignarTag(True, FDibuAuto[0]) then begin
				if QuitarTag(FDibuAuto[FAutoSugerido]) then begin
					FAutoOrigen  := FAutoSugerido;
					FAutoDestino := 0;
				end;
			end;
		end;   
		Result := True;
	except
		Result := False;
	end;
end;

procedure TImageOverview.Reset;
begin
	if Assigned(FImagen.Picture) then FImagen.Picture.Assign(nil);
	//
	FAutoOrigen   := -1;
	FAutoDestino  := -1; 
	FAutoSugerido := -1; 
	FTagConflicto_CM := 0;
	FTagConflicto_SN := 0;
	//
	FillChar(FDatosAutos, SizeOf(FDatosAutos), 0); 
end;

end.
