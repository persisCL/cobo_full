unit ProcsOverview;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, JPeg, ADODB, ImgTypes, 
  UtilDB, DB;

const
	MAX_VEHICULOS 	= 25;				// Cantidad M�xima de Veh�culos en la Imagen
	MARGEN_PX		 = 6; 				// Margen en la vista (pixels).

	//
	COLOR_ACTUAL			= $001D25DA; 	//Rojo
	COLOR_SUGERIDO			= $0000C2ED;    //Amarillo
	COLOR_COMUN				= $0026C384;    //Verde 
	COLOR_FLASH				= $0018769E;
	DESCRI_SIN_TAG			= '';
	DESCRI_TAG 				= 'TAG';
	DESCRI_TAG_OK			= 'OK';

function RotarImagen(Imagen: TBitmap; ImagenRotada: TBitmap; var DescriError: AnsiString): Boolean;
function CargarDatosAutos(Conn: TADOConnection; DatosImg: TDataOverview; NumeroPuntoCobro: Integer;
  FechaHora: TDateTime; FPatenteEsperada: AnsiString; var FDatosAutos: TDatosAutos; var ExisteSugerido: Boolean): Boolean;
function ActualizarTransitosCambioTag(Conn: TAdoConnection; NumeroPuntoCobro, NumCorrCO: Integer;
 Origen, Destino: Integer; DatosAutos: TDatosAutos): Boolean;

implementation

function RotarImagen(Imagen: TBitmap; ImagenRotada: TBitmap; var DescriError: AnsiString): Boolean;
var
	i, j: Integer;
begin
	Result := False;
	try
		ImagenRotada.Width  := Imagen.Height;
		ImagenRotada.Height := Imagen.Width;
		//
		for i:=0 to (Imagen.Height - 1) do begin
			for j:=0 to (Imagen.Width - 1) do begin
				ImagenRotada.Canvas.Pixels[(Imagen.Height - 1 - i), j] := Imagen.Canvas.Pixels[j, i];
			end;
		end;
		//		
		Result := True;
	except
		DescriError := SysErrorMessage(GetLastError);
	end;
end;

function CargarDatosAutos(Conn: TADOConnection; DatosImg: TDataOverview; NumeroPuntoCobro: Integer;
  FechaHora: TDateTime; FPatenteEsperada: AnsiString; var FDatosAutos: TDatosAutos; var ExisteSugerido: Boolean): Boolean;
var
	i: integer;
	ObtenerTransitoOverview: TADOStoredProc;
begin
	Result := False;
	ObtenerTransitoOverview := TADOStoredProc.Create(nil);
	try
		// Crear el Stored
		ObtenerTransitoOverview.Connection	   := Conn;
		ObtenerTransitoOverview.Name		   := 'ObtenerTransitoOverview';
		ObtenerTransitoOverview.ProcedureName  := 'dbo.ObtenerTransitoOverview';
		ObtenerTransitoOverview.Parameters.CreateParameter('@NumeroPuntoCobro', ftInteger, pdInput, 0, NumeroPuntoCobro);
		ObtenerTransitoOverview.Parameters.CreateParameter('@FechaHora', ftDateTime, pdInput, 0, FechaHora);
		ObtenerTransitoOverview.Parameters.CreateParameter('@RegistrationID', ftInteger, pdInput, 0, 0);
		//
		for i:=0 to MAX_VEHICULOS do begin
			if (i > 0) and (DatosImg.Vehiculos[i].RegistrationID = 0) then Break;
			//
			ObtenerTransitoOverview.Close;
			if i = 0 then begin
				ObtenerTransitoOverview.Parameters.ParamByName('@RegistrationID').Value := DatosImg.Actual.RegistrationID
			end else begin
				ObtenerTransitoOverview.Parameters.ParamByName('@RegistrationID').Value := DatosImg.Vehiculos[i].RegistrationID
			end;
			ObtenerTransitoOverview.Parameters.ParamByName('@NumeroPuntoCobro').Value    := NumeroPuntoCobro;
			ObtenerTransitoOverview.Parameters.ParamByName('@FechaHora').Value			 := FechaHora;
			//
			if not OpenTables([ObtenerTransitoOverview]) then Exit;
			if ObtenerTransitoOverview.RecordCount > 0 then begin
				// Cargar los datos
				FDatosAutos[i].NumCorrCO := ObtenerTransitoOverview.FieldByName('NumCorrCO').AsInteger;
				FDatosAutos[i].Validado       := Trim(ObtenerTransitoOverview.FieldByName('Validador').AsString) <> '';
				FDatosAutos[i].ContextMark    := ObtenerTransitoOverview.FieldByName('ContextMark').AsInteger;
				FDatosAutos[i].SerialNumber   := ObtenerTransitoOverview.FieldByName('ContractSerialNumber').AsFloat;
				FDatosAutos[i].Categoria	  := ObtenerTransitoOverview.FieldByName('CategoriaDetectada').AsInteger;
				FDatosAutos[i].Patente		  := Trim(ObtenerTransitoOverview.FieldByName('PatenteDetectada').AsString);
				FDatosAutos[i].Sugerido 	  := Trim(ObtenerTransitoOverview.FieldByName('Patente').AsString) = FPatenteEsperada;
				ExisteSugerido 				  := FDatosAutos[i].Sugerido;
			end;
			ObtenerTransitoOverview.Close;
		end;
		ObtenerTransitoOverview.Free;
		//
		Result := True;
	except
		ObtenerTransitoOverview.Close;
		ObtenerTransitoOverview.Free;
	end;
end;

function ActualizarTransitosCambioTag(Conn: TAdoConnection; NumeroPuntoCObro, NumCOrrCO: Integer;
 Origen, Destino: Integer; DatosAutos: TDatosAutos): Boolean;
var
	MoverTagDeTransito: TADOStoredProc;
begin
	Result := True;
	MoverTagDeTransito := TADOStoredProc.Create(nil);
	try
		MoverTagDeTransito.Connection     := Conn;
		MoverTagDeTransito.Name		      := 'MoverTagDeTransito';
		MoverTagDeTransito.ProcedureName  := 'dbo.MoverTagDeTransito';
		MoverTagDeTransito.Parameters.CreateParameter('@NumeroPuntoCobro', ftWord, pdInput, 0, 0);
		MoverTagDeTransito.Parameters.CreateParameter('@TransitoOrigen', ftInteger, pdInput, 0, 0);
		MoverTagDeTransito.Parameters.CreateParameter('@TransitoDestino', ftInteger, pdInput, 0, 0);
		MoverTagDeTransito.Parameters.CreateParameter('@TransitoActual', ftInteger, pdInput, 0, 0);
		//
		MoverTagDeTransito.Parameters.ParamByName('@NumeroPuntoCobro').Value     := NumeroPuntoCObro;
		MoverTagDeTransito.Parameters.ParamByName('@TransitoOrigen').Value       := DatosAutos[Origen].NumCorrCO;
		MoverTagDeTransito.Parameters.ParamByName('@TransitoDestino').Value      := DatosAutos[Destino].NumCorrCO;
		MoverTagDeTransito.Parameters.ParamByName('@TransitoActual').Value       := NumCorrCO;
		// Movemos el TAG que tenia originalmente el tr�nsito que estamos validando = FTagConflicto;
		MoverTagDeTransito.ExecProc;
	except
		MoverTagDeTransito.Close;
		MoverTagDeTransito.Free;
	end;
end;


end.

