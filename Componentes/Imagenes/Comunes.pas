unit Comunes;

interface

uses
	Windows, Messages, SysUtils, Graphics, Dialogs;

Type
	tFiltro = (tfBrillo, tfContraste, tfRealce);
	
const
	BRILLO_PEND_OSC		= 0.064;
	BRILLO_DEZPL_OSC 	= -0.7;
	//
	BRILLO_PEND	 		= 0.016;	//0.01;		//0.005;
	BRILLO_DEZPL		= 0.2;		//0.5; 		//0.75;
	CONTRASTE_PEND 		= 0.012;
	CONTRASTE_DEZPL		= 0.4;


	
procedure Brillo(Bmp: TBitmap; Valor: Double);
procedure Contraste(Bmp: TBitmap; Valor: Double);
procedure Realce(Bmp: TBitmap; Valor: Double);

implementation

procedure ObtenerConfigImagen(Bmp: TBitmap; var Ancho: Integer);
begin
	if Bmp.PixelFormat = pf8bit then ancho := Bmp.Width-1
	  else ancho := (Bmp.Width * 3)-1;	
end;

procedure Brillo(Bmp: TBitmap; Valor: Double);
Var
	P: PByteArray;
	Factor: Double;
	i, j, val, ancho: Integer;
begin
	try
		Factor := Valor * BRILLO_PEND + BRILLO_DEZPL;
		ObtenerConfigImagen(Bmp, Ancho);
		//
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin          
				val := Round(P[j] * Factor);
				if val < 0 then val := 0;
				if val > 255 then val := 255;
				P[j]   := val;
				Inc(j);
			end;
		end;
	except
		ShowMEssage('Error');
	end;
end;

procedure Contraste(Bmp: TBitmap; Valor: Double);
var
	P: PByteArray;
	Factor: Double;
	i, j, val, Dev, ancho:Integer;
begin
	try
		Factor := Valor * CONTRASTE_PEND + CONTRASTE_DEZPL;
		ObtenerConfigImagen(Bmp, Ancho);
		//
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin   
				Dev := P[j] - 128;
				val := 128 + Round(Dev * Factor);
				if val < 0 then val := 0;
				if val > 255 then val := 255;	
				P[j]   := val;
				Inc(j);
			end;
		end;
	except
		ShowMessage('Error!!');
	end;
end;

procedure Realce(Bmp: TBitmap; Valor: Double);
Var
	P: PByteArray;
	Factor: Double;
	i, j, val, ancho: Integer;
begin
	try
		Factor := Valor * BRILLO_PEND_OSC + BRILLO_DEZPL_OSC;
		ObtenerConfigImagen(Bmp, Ancho);
		//
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin         
				val := Round(P[j] * Factor);
				if val < 0 then val := 0;
				if val > 255 then val := 255;
				P[j]   := val;
				Inc(j);
			end;
		end;
	except
		ShowMessage('Error!!');
	end;
end;

end.
 