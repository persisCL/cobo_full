{ *****************************************************************************}
{ Componente para la carga y visualizaci�n de las im�genes de un tr�nsito,     }
{  m�s el combo que permite seleccionar c/tipo.                                }
{																			   }
{ *****************************************************************************}

unit OPImageVehicle;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms, ExtCtrls, ADODB, StdCtrls,
  Graphics, ImgTypes, ImgProcs, VarsOverview, Util, OPImageOverview, 
  UtilProc;

Type
  TImageVehicle = class;

  TConfigurationOverview = class(TPersistent)
  private
	// Private declarations 
	FEscalaVisual: TEscala;
	FVisualMode: TModoVisual;
	FImagenVehiculo: TImageVehicle;
	procedure SetEscala(const Value: TEscala);
	procedure SetVisualMode(const Value: TModoVisual);
  protected
	// Protected declarations 
  public
	// Public declarations
	destructor Destroy; override;
  published
	// Published declarations 
	constructor Create;
	property VisualMode: TModoVisual read FVisualMode write SetVisualMode default MVDefault;
	property EscalaImage: TEscala read FEscalaVisual write SetEscala default 100;
  end;
  
  TImageVehicle = class(TCustomControl)
  private
	{ Private declarations }
	FVentana: TPanel;
	FCombo: TComboBox;
	FLastError: AnsiString;
	FADOConnection: TADOConnection;
	FImagenAlmacenable: Boolean;
	FConfigOverview: TConfigurationOverview;
	FTipoImagen: TTipoImagen;
    FOnClick: TNotifyEvent;
	procedure SetADOConnection(const Value: TADOConnection);
	procedure SetConfigOverview(const Value: TConfigurationOverview);
	procedure ComboOnClick(Sender: TObject);
  protected
	{ Protected declarations }
	FImagenComun: TImage;
	FImagenOverview: TImageOverview;
	FConcesionaria, FPuntoCobro, FNumeroTransito: Integer;
	function MostrarImagen(var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; 
  public
	{ Public declarations }
	constructor Create(AOwner: TComponent); override;
	destructor  Destroy; override;
	//
	property LastError: AnsiString Read FLastError;
	property ImagenAlmacenable: Boolean Read FImagenAlmacenable;
	function LoadImages(Concesionaria, PuntoCobro, NumeroTransito: Integer; TipoImagen: TTipoImagen = tiFrontal): Boolean; virtual;
  published
	{ Published declarations }
	property Width default 224;
	property ADOConnection: TADOConnection read FADOConnection write SetADOConnection;
	property ConfigOverview: TConfigurationOverview read FConfigOverview write SetConfigOverview;
	property TipoImagen: TTipoImagen read FTipoImagen;
	// Eventos
	property OnClick: TNotifyEvent read FOnClick write FOnClick;  
	property OnEnter;
  end;

	  
implementation

{ ImageVehicle }

constructor TImageVehicle.Create(AOwner: TComponent);
begin
	inherited Create(AOwner);
	ControlStyle := ControlStyle - [csAcceptsControls];
	Width  := 224;
	Height := 215; 	
	//
	FCombo := TComboBox.Create(Self);
	FCombo.Parent  := Self;
	FCombo.Width   := 180;
	FCombo.Style   := csDropDownList;
	FCombo.OnClick := ComboOnClick;
	//
	FVentana := TPanel.Create(Self);
	FVentana.Parent 	:= Self;
	FVentana.Top    	:= FCombo.Top + FCombo.Height + 5;
	FVentana.Left   	:= FCombo.Left;
	FVentana.Width  	:= Width - 2; 						
	FVentana.Height		:= Height - FVentana.Top - 2;		
	FVentana.BevelOuter := bvLowered;
	// Crear las dos im�genes posibles. Una visible por vez.
	FImagenComun := TImage.Create(Self);
	FImagenComun.Parent  := FVentana;
	FImagenComun.Visible := False;
	FImagenComun.Top 	 := 1;
	FImagenComun.Left	 := 1;
	FImagenComun.Width	 := FVentana.Width - 2;
	FImagenComun.Height  := FVentana.Height - 2;
	FImagenComun.Stretch := True;
	//
	FImagenOverview := TImageOverview.Create(Self);
	FImagenOverview.Parent  := FVentana;
	FImagenOverview.Visible := False;
	FImagenOverview.Top 	:= 0;
	FImagenOverview.Left	:= 0;
	FImagenOverview.Width	:= FVentana.Width;
	FImagenOverview.Height  := FVentana.Height;
	//
	FConfigOverview := TConfigurationOverview.Create;
	FConfigOverview.FImagenVehiculo := Self;
end;

destructor TImageVehicle.Destroy;
begin
	FConfigOverview.Free;
	FImagenOverview.Free;
	FImagenComun.Free;
	FCombo.Free;
	FVentana.Free; 
	//
	inherited Destroy;
end;

function TImageVehicle.LoadImages(Concesionaria, PuntoCobro,
  NumeroTransito: Integer; TipoImagen: TTipoImagen = tiFrontal): Boolean;
var
	Error: TTipoErrorImg;
	DescriError: AnsiString; 
begin
	try
		if not Assigned(FADOConnection) then 
			raise exception.Create('Falta el par�metro ADOConnection');
		//
		FConcesionaria  := Concesionaria;
		FPuntoCobro     := PuntoCobro;
		FNumeroTransito := NumeroTransito;
		CargarComboImgPendientes(FADOConnection, FCombo, FConcesionaria, FPuntoCobro, FNumeroTransito, TipoImagen);
		if not MostrarImagen(DescriError, Error) then raise exception.Create('Error');
		Result := True;
	except
		on e:exception do begin
			FLastError := e.message;
			Result := False;
		end;
	end;
end;

procedure TImageVehicle.SetADOConnection(const Value: TADOConnection);
begin
	FADOConnection := Value;
end;

procedure TImageVehicle.ComboOnClick(Sender: TObject);
var
	Error: TTipoErrorImg;
	DescriError, Extension: AnsiString; 
begin
	Extension   := Copy(FCombo.Text, 201, 4);
	FTipoImagen := TTipoImagen(IVal(Extension));
	if not MostrarImagen(DescriError, Error) then FlastError := DescriError;
	//
	if Assigned(FOnClick) then FOnClick(Self);
end;

function TImageVehicle.MostrarImagen(var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;
var
	TipoImg: TTipoImagen;
	ImagenActual: TBitmap;
    DataImage: TDataImage;
begin
	ImagenActual := TBitmap.Create;       
	try
		// Buscar y mostrar la imagen de la posicion seleccionada.
		TipoImg := FTipoImagen;
		if TipoImg in [tiOverview1, tiOverview2, tiOverview3] then begin
			FImagenComun.Visible	:= False;
			FImagenOverview.Visible := True;
			if not FImagenOverview.LoadImage(FADOConnection, FConcesionaria, FPuntoCobro, FNumeroTransito,
                now, '', TipoImg, etValidado) then begin
				FLastError := FImagenOverview.LastError;
			end;
		end else begin
			FImagenOverview.Visible := False;
			FImagenComun.Visible	:= True;
			FImagenComun.Picture.Assign(nil);
			if not ObtenerImagenTransitoPendiente(FADOConnection, FConcesionaria, FPuntoCobro, FNumeroTransito, TipoImg,
			  ImagenActual, DataImage, DescriError, Error) then raise exception.Create(DescriError);
			FImagenComun.Picture.Assign(ImagenActual);
		end;
		// Ver si esa imagen es almacenable. // Cambiar
		if (TipoImg = tiFrontal) or (TipoImg = tiPosterior) then FImagenAlmacenable := True;
		//
		Result := True;
	except 
		on e:exception do begin
			FLastError := e.message;
			Result := False;
		end;
	end;
	ImagenActual.Free;
end;

procedure TImageVehicle.SetConfigOverview(const Value: TConfigurationOverview);
begin
	FConfigOverview := Value;
end;


{ TConfigurationOverview }

constructor TConfigurationOverview.Create;
begin
	inherited Create;
	FEscalaVisual := 100;
	FVisualMode   := MVDefault;
end;

destructor TConfigurationOverview.Destroy;
begin
	inherited Destroy;
end;

procedure TConfigurationOverview.SetEscala(const Value: TEscala);
begin
	FEscalaVisual := Value;
	FImagenVehiculo.FImagenOverview.EscalaImage := FEscalaVisual;
end;

procedure TConfigurationOverview.SetVisualMode(const Value: TModoVisual);
begin
	FVisualMode := Value;
	FImagenVehiculo.FImagenOverview.VisualMode := FVisualMode;
end;


end.
 