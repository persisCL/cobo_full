{ *****************************************************************************}
{ Componente para la carga y visualizaci�n de las im�genes de un tr�nsito,     }
{  m�s el combo que permite seleccionar c/tipo. Con la componente de LEAD.     }
{	                                                                           }
{ *****************************************************************************}


{ OJO: ESTA CLASE DEBER�A EN REALIDAD SER DESC. DE TImagesVehicles             }
{ Y Sobreescribir el LoadImages, o esa clase desaparecer, segun que consigamos }
{ hacer con los Jpegs										   				   }											   


unit OPImagesVehicle12Bits;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms, ExtCtrls, ADODB, StdCtrls,
  Graphics, ImgTypes, ImgProcs, VarsOverview, Util, OPImageOverview, Math, 
  UtilProc, LEADVCL, LTVclTyp;

Type
  TImagesVehicle12B = class;

  TConfigurationOverview = class(TPersistent)
  private
	// Private declarations 
	FEscalaVisual: TEscala;
	FVisualMode: TModoVisual;
	FImagenVehiculo: TImagesVehicle12B;
	procedure SetEscala(const Value: TEscala);
	procedure SetVisualMode(const Value: TModoVisual);
  protected
	// Protected declarations 
  public
	// Public declarations
	destructor Destroy; override;
  published
	// Published declarations 
	constructor Create;
	property VisualMode: TModoVisual read FVisualMode write SetVisualMode default MVDefault;
	property EscalaImage: TEscala read FEscalaVisual write SetEscala default 100;
  end;
  
  TImagesVehicle12B = class(TCustomControl)
  private
	{ Private declarations }
	FVentana: TPanel;
	FCombo: TComboBox;
	FLastError: AnsiString;
	FADOConnection: TADOConnection;
	FImagenAlmacenable: Boolean;
	FConfigOverview: TConfigurationOverview;
	FTipoImagen: TTipoImagen;
    FOnClick: TNotifyEvent;
	procedure SetADOConnection(const Value: TADOConnection);
	procedure SetConfigOverview(const Value: TConfigurationOverview);
	procedure ComboOnClick(Sender: TObject);
  protected
	{ Protected declarations }
	FImagenComun: TLEADImage;
	FImagenOverview: TImageOverview;
	FConcesionaria, FPuntoCobro, FNumeroTransito: Integer;
	function MostrarImagen(var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; 
  public
	{ Public declarations }
	constructor Create(AOwner: TComponent); override;
	destructor  Destroy; override;
	procedure   Resize; override;
	//
	property LastError: AnsiString Read FLastError;
	property ImagenAlmacenable: Boolean Read FImagenAlmacenable;
	function LoadImages(Concesionaria, PuntoCobro, NumeroTransito: Integer; TipoImagen: TTipoImagen = tiFrontal): Boolean; virtual;
	procedure GammaCorrect(Value: Integer);
	procedure IntensityChange(Value: Integer);
	procedure ContrastChange(Value: Integer);
	procedure ResetImage;
  published
	{ Published declarations }
	property Width default 224;
	property ADOConnection: TADOConnection read FADOConnection write SetADOConnection;
	property ConfigOverview: TConfigurationOverview read FConfigOverview write SetConfigOverview;
	property TipoImagen: TTipoImagen read FTipoImagen;
	property OnClick: TNotifyEvent read FOnClick write FOnClick;  
	property OnResize;
	//
	property Align;
	property Anchors;
	property AutoSize;
	property Constraints;
	property DockSite;
	property DragCursor;
	property DragKind;
	property DragMode;
	property Enabled;
	property Ctl3D;
	property Font;
	property ParentBiDiMode;
	property ParentColor;
	property ParentCtl3D;
	property ParentFont;
	property ParentShowHint;
	property PopupMenu;
	property ShowHint;
	property TabOrder;
	property TabStop;
	property Visible;     
	//
	property OnCanResize;
	property OnConstrainedResize;
	property OnContextPopup;
	property OnDockDrop;
	property OnDockOver;
	property OnDblClick;
	property OnDragDrop;
	property OnDragOver;
	property OnEndDock;
	property OnEndDrag;
	property OnEnter;
	property OnExit;
	property OnGetSiteInfo;
	property OnMouseDown;
	property OnMouseMove;
	property OnMouseUp;
	property OnStartDock;
	property OnStartDrag;
	property OnUnDock;
  end;
	   
	  
implementation

{ ImageVehicle }

constructor TImagesVehicle12B.Create(AOwner: TComponent);
begin
	inherited Create(AOwner);
	ControlStyle := ControlStyle - [csAcceptsControls];
	Width  := 225;
	Height := 265; 	
	//
	FCombo := TComboBox.Create(Self);
	FCombo.Parent  := Self;
	FCombo.Width   := 180;
	FCombo.Style   := csDropDownList;
	FCombo.Font    := Font;
	FCombo.OnClick := ComboOnClick;
	//
	FVentana := TPanel.Create(Self);
	FVentana.Parent 	:= Self;
	FVentana.Top    	:= FCombo.Top + FCombo.Height + 5;
	FVentana.Left   	:= FCombo.Left;
	FVentana.Width  	:= Width - 2; 						
	FVentana.Height		:= Height - FVentana.Top - 2;		
	FVentana.BevelOuter := bvLowered;
	// Crear las dos im�genes posibles. Una visible por vez.
	FImagenComun := TLEADImage.Create(Self);
	FImagenComun.Parent  := FVentana;
	FImagenComun.Visible := False;
	FImagenComun.Top 	 := 1;
	FImagenComun.Left	 := 1;
	FImagenComun.Width	 := FVentana.Width - 2;
	FImagenComun.Height  := FVentana.Height - 2;
	//
	FImagenOverview := TImageOverview.Create(Self);
	FImagenOverview.Parent  := FVentana;
	FImagenOverview.Visible := False;
	FImagenOverview.Top 	:= 0;
	FImagenOverview.Left	:= 0;
	FImagenOverview.Width	:= FVentana.Width;
	FImagenOverview.Height  := FVentana.Height;
	//
	FConfigOverview := TConfigurationOverview.Create;
	FConfigOverview.FImagenVehiculo := Self;
end;

destructor TImagesVehicle12B.Destroy;
begin
	FConfigOverview.Free;
	FImagenOverview.Free;
	FImagenComun.Free;
	FCombo.Free;
	FVentana.Free; 
	//
	inherited Destroy;
end;

function TImagesVehicle12B.LoadImages(Concesionaria, PuntoCobro,
  NumeroTransito: Integer; TipoImagen: TTipoImagen = tiFrontal): Boolean;
var
	Error: TTipoErrorImg;
	DescriError: AnsiString; 
begin
	try
		if not Assigned(FADOConnection) then 
			raise exception.Create('Falta el par�metro ADOConnection');
		//
		FConcesionaria  := Concesionaria;
		FPuntoCobro     := PuntoCobro;
		FNumeroTransito := NumeroTransito;
		CargarComboImgPendientes(FADOConnection, FCombo, FConcesionaria, FPuntoCobro, FNumeroTransito, TipoImagen);
		if not MostrarImagen(DescriError, Error) then raise exception.Create('Error');
		Result := True;
	except
		on e:exception do begin
			FLastError := e.message;
			Result := False;
		end;
	end;
end;

procedure TImagesVehicle12B.SetADOConnection(const Value: TADOConnection);
begin
	FADOConnection := Value;
end;

procedure TImagesVehicle12B.ComboOnClick(Sender: TObject);
var
	Error: TTipoErrorImg;
	DescriError, Extension: AnsiString; 
begin
	Extension   := Copy(FCombo.Text, 201, 4);
	FTipoImagen := TTipoImagen(IVal(Extension));
	if not MostrarImagen(DescriError, Error) then FlastError := DescriError;
	//
	if Assigned(FOnClick) then FOnClick(Self);
end;

function TImagesVehicle12B.MostrarImagen(var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;
var
	TipoImg: TTipoImagen;
	ImagenActual: TBitmap;
	ArchivoTransito: AnsiString;
begin
	ImagenActual := TBitmap.Create;       
	try
		// Buscar y mostrar la imagen de la posicion seleccionada.
		TipoImg := FTipoImagen;
		if TipoImg in [tiOverview1, tiOverview2, tiOverview3] then begin
			FImagenComun.Visible	:= False;
			FImagenOverview.Visible := True;
			if not FImagenOverview.LoadImage(FADOConnection, FConcesionaria, FPuntoCobro, FNumeroTransito, TipoImg) then begin
				FLastError := FImagenOverview.LastError;
			end;
		end else begin
			FImagenOverview.Visible := False;
			FImagenComun.Visible	:= True;
//			FImagenComun.Picture.Assign(nil)Create;	
			{if not ObtenerImagenTransitoPendiente(FADOConnection, FConcesionaria, FPuntoCobro, FNumeroTransito, TipoImg, 
			  ImagenActual, DescriError, Error) then raise exception.Create(DescriError);
			FImagenComun.Picture.Assign(ImagenActual);}
			FImagenComun.PaintSizeMode := smStretch;
			if not ObtenerPathTransitoPendiente(FADOConnection, FConcesionaria, 
			  FPuntoCobro, FNumeroTransito, TipoImagen, ArchivoTransito, DescriError, Error) then begin
				raise exception.Create(DescriError);
			end;
			FImagenComun.Load(ArchivoTransito, 12, 0, 1);
		end;
		// Ver si esa imagen es almacenable. // Cambiar
		if (TipoImg = tiFrontal) or (TipoImg = tiPosterior) then FImagenAlmacenable := True;
		//
		Result := True;
	except 
		on e:exception do begin
			FLastError := e.message;
			Result := False;
		end;
	end;
	ImagenActual.Free;
end;

procedure TImagesVehicle12B.SetConfigOverview(const Value: TConfigurationOverview);
begin
	FConfigOverview := Value;
end;


{ TConfigurationOverview }

constructor TConfigurationOverview.Create;
begin
	inherited Create;
	FEscalaVisual := 100;
	FVisualMode   := MVDefault;
end;

destructor TConfigurationOverview.Destroy;
begin
	inherited Destroy;
end;

procedure TConfigurationOverview.SetEscala(const Value: TEscala);
begin
	FEscalaVisual := Value;
	FImagenVehiculo.FImagenOverview.EscalaImage := FEscalaVisual;
end;

procedure TConfigurationOverview.SetVisualMode(const Value: TModoVisual);
begin
	FVisualMode := Value;
	FImagenVehiculo.FImagenOverview.VisualMode := FVisualMode;
end;

procedure TImagesVehicle12B.Resize;
begin
	inherited;
	FVentana.Width  		:= Width - 2; 						
	FVentana.Height			:= Height - FVentana.Top - 2;
	FImagenComun.Width	 	:= FVentana.Width - 2;
	FImagenComun.Height  	:= FVentana.Height - 2;
	FImagenOverview.Width	:= FVentana.Width;
	FImagenOverview.Height  := FVentana.Height; 
	if Width <= FCombo.Width then FCombo.Width := Width - 2;
	if Width > (FCombo.Width * 0.5) then FCombo.Width := Min((Width - 20), 220);
end;

procedure TImagesVehicle12B.GammaCorrect(Value: Integer);
begin
	if (Value <= 0) or (Value > 100) then Exit;
	FImagenComun.GammaCorrect(Value);
end;

procedure TImagesVehicle12B.ContrastChange(Value: Integer);
begin
	if (Value < 0) or (Value > 100) then Exit;
	FImagenComun.Contrast(Value);
end;

procedure TImagesVehicle12B.IntensityChange(Value: Integer);
begin
	if (Value < 0) or (Value > 100) then Exit;
	FImagenComun.Intensity(Value);	
end;

procedure TImagesVehicle12B.ResetImage;
var
	Error: TTipoErrorImg;
	DescriError: AnsiString;
begin
	MostrarImagen(DescriError, Error);
end;

end.
 