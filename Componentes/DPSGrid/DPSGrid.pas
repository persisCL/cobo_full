unit DPSGrid;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Grids, DBGrids, Graphics, DB, Math;

type
  TDPSGrid = class;

  TOrderIndicator = (oiNone, oiDown, oiUp);
  TOnLinkClickEvent = procedure(Sender: TDPSGrid; Column: TColumn) of object;
  TOnWillClickTitleEvent = procedure(Sender: TDPSGrid; Column: TColumn; var ExecuteAction: Boolean) of object;
  TOnOrderChangeEvent = procedure(Sender: TDPSGrid; var ACol: Integer; var OrderIndicator: TOrderIndicator) of object;

  TDPSGrid = class(TDBGrid)
  private
	{ Private declarations }
	FOnLinkClick: TOnLinkClickEvent;
	FOnWillClickTitle: TOnWillClickTitleEvent;
	FOnOrderChange: TOnOrderChangeEvent;

	FMarkSelection: Boolean;
	FSelectingMouse: Boolean;
	FDirection: Integer;
	FOriginalRegister: TBookMark;
	FLastRow: Integer;
	FButtonPress: Integer;
	FOrderIndicator: TOrderIndicator;
	FColumnOrderly: Integer;
	FExecuteAction: Boolean;

	function GetColumnOrderly: Integer;
	function GetOrderIndicator: TOrderIndicator;
	Procedure DrawRectangle(ARect: TRect);
  protected
	procedure Paint; override;
	function CheckColumnDrag(var Origin, Destination: Integer; const MousePt: TPoint): Boolean; override;
	procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
	function EndColumnDrag(var Origin, Destination: Integer; const MousePt: TPoint): Boolean; override;
	procedure LinkActive(value: Boolean); override;
	procedure Notification(AComponent: TComponent; Operation: TOperation); override;
	procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
	procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
	procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
	procedure KeyDown(var Key: Word; Shift: TShiftState); override;
	procedure Scroll(distance: Integer); override;
	function  SelectCell(ACol, ARow: Longint): Boolean; override;
	procedure TitleClick(Column: TColumn); override;
	procedure WMKillFocus(var Msg: TWMKillFocus); message WM_KILLFOCUS;
  public
	constructor Create (Aowner: TComponent);override;
	Procedure MarcarTodo;
	Procedure InvertirMarcas;

	property ColumnOrderly: Integer read GetColumnOrderly;
	property OrderIndicator: TOrderIndicator read GetOrderIndicator;
  published
	property OnLinkClick: TOnLinkClickEvent Read FOnLinkClick Write FOnLinkClick;
	property OnWillClickTitle: TOnWillClickTitleEvent Read FOnWillClickTitle Write FOnWillClickTitle;
	property OnOrderChange: TOnOrderChangeEvent Read FOnOrderChange Write FOnOrderChange;
  end;

implementation

{ TDPSGrid }


function TDPSGrid.CheckColumnDrag(var Origin, Destination: Integer;
  const MousePt: TPoint): Boolean;
var R: TRect;
	OldPenMode: TPenMode;
	OldPenWidth: Integer;

begin
	result := inherited CheckColumnDrag(Origin, Destination, MousePt);
	if Result then begin
		paint;
		OldPenWidth := Canvas.Pen.Width;
		OldPenMode := Canvas.Pen.Mode;
		Canvas.Pen.Width := 5;
		Canvas.Pen.Mode := pmNotXor;
		if Destination <> Origin then begin
			if Destination < Origin then
				R := cellRect(Destination, 0)
			else
				R := cellRect(Destination + 1, 0);
			Canvas.MoveTo(R.Left + 1, R.Top);
			Canvas.LineTo(R.Left + 1, ClientHeight);
		end;
		Canvas.Pen.Width := OldPenWidth;
		Canvas.Pen.Mode := OldPenMode;
	end;
end;

constructor TDPSGrid.create(Aowner: TComponent);
begin
	Inherited;
	Options := Options - [dgColLines, dgRowLines, dgIndicator, dgEditing] + [dgRowSelect];
	FDirection := 0;
	FMarkSelection := False;
	FOriginalRegister := nil;
	FSelectingMouse := False;
	FLastRow := -1;
	FButtonPress := -1;
	FOrderIndicator := oiNone;
	FColumnOrderly := -1;
	FExecuteAction := False;
end;


procedure TDPSGrid.DrawCell(ACol, ARow: Integer; ARect: TRect; AState: TGridDrawState);
var ShiftCaption, Index: Integer;
	P1, P2, P3: TPoint;
	Borde: integer;
	OldPenColor: TColor;
begin
	inherited DrawCell(ACol, ARow, ARect, AState);
	// Dibujo las celdas fijas
	if (gdFixed in AState) then begin
		OldPenColor := Canvas.Pen.Color;
		Canvas.Pen.Color := clBlack;

		// Si esta activo el indicador pinto solo las columnas mayores a cero
		if (dgIndicator in Options) then begin
			if (ACol > 0) then
				Canvas.FillRect(ARect);
		end else
			Canvas.FillRect(ARect);

		// Dibujo los bordes del titulo y de la columna cero
		if FButtonPress = ACol then
			Borde := BDR_SUNKENINNER
		else
			Borde := BDR_RAISEDINNER;
		if ARect.Right > ClientWidth + 2 then ARect.Right := ClientWidth + 2;
		DrawEdge(Canvas.Handle, ARect, Borde, BF_BOTTOM or BF_RIGHT);
		DrawEdge(Canvas.Handle, ARect, Borde, BF_TOP or BF_LEFT);
		Dec(ARect.Right);
		Dec(Arect.Bottom);
		Canvas.MoveTo(ARect.Left, ARect.Bottom);
		Canvas.LineTo(ARect.Right, ARect.Bottom);
		Canvas.LineTo(ARect.Right, ARect.Top - 1);
		if FButtonPress = ACol then
			ShiftCaption := 3
		else
			ShiftCaption := 1;

		// Si esta activo el indicador y es la columna cero no se puede dibujar ningun indicador
		if (dgIndicator in Options) and (ACol = 0) then exit;
		// Actualizo el titulo que debo dibujar
		if (dgIndicator in Options) then
			index := ACol - 1
		else
			Index := ACol;

		// Si esta column� es la ordenada, dibujo el indicador
		if FColumnOrderly = ACol then begin
			// Dibujo el indicador de orden en el titulo
			case FOrderIndicator of
				oiUp :
					begin
						P1 := Point(ARect.Right - 14, ((ARect.Bottom - 8) div 2) + 8);
						P2 := Point(ARect.Right - 5, ((ARect.Bottom - 8) div 2) + 8);
						P3 := Point(ARect.Right - 10, (ARect.Bottom - 8) div 2);
						if FButtonPress = ACol then begin
							P1.X := P1.X + 2;
							P2.X := P2.X + 2;
							P3.X := P3.X + 2;
						end;
						Canvas.TextRect(Rect(ARect.left + 1, ARect.Top + 1,ARect.Right - 16, ARect.Bottom - 1),
						  ARect.Left + ShiftCaption, ARect.Top + 1, Columns[Index].Title.Caption);
						Canvas.Pen.Color := clGray;
						Canvas.MoveTo(P3.X, P3.Y);
						Canvas.LineTo(P1.X, P1.Y);
						Canvas.Pen.Color := clWhite;
						Canvas.LineTo(P2.X, P2.Y);
						Canvas.LineTo(P3.X, P3.Y);
					end;
				oiDown :
					begin
						P1 := Point(ARect.Right - 14, (ARect.Bottom - 8) div 2);
						P2 := Point(ARect.Right - 5, (ARect.Bottom - 8) div 2);
						P3 := Point(ARect.Right - 10, ((ARect.Bottom - 8) div 2) + 8);
						if FButtonPress = ACol then begin
							P1.X := P1.X + 2;
							P2.X := P2.X + 2;
							P3.X := P3.X + 2;
						end;
						Canvas.TextRect(Rect(ARect.left + 1, ARect.Top + 1,ARect.Right - 16, ARect.Bottom - 1),
						  ARect.Left + ShiftCaption, ARect.Top + 1, Columns[Index].Title.Caption);
						Canvas.Pen.Color := clGray;
						Canvas.MoveTo(P3.X, P3.Y);
						Canvas.LineTo(P1.X, P1.Y);
						Canvas.LineTo(P2.X, P2.Y);
						Canvas.Pen.Color := clWhite;
						Canvas.LineTo(P3.X, P3.Y);
					end;
				else begin
					Canvas.TextOut(ARect.left + ShiftCaption, ARect.Top + 1,Columns[Index].Title.Caption);
				end;
			end;
		end	else begin
			Canvas.TextOut(ARect.left + ShiftCaption, ARect.Top + 1, Columns[Index].Title.Caption);
		end;
		Canvas.Pen.Color := OldPenColor;
	end else begin
		// Dibujamos la fila seleccionada
		if dgIndicator in Options then
			dec(ACol);
		if Assigned(DataLink.DataSet) and (DataLink.Active) and not (csDesigning in ComponentState)
		  and ((DataLink.ActiveRecord = ARow - 1) or DataLink.dataset.IsEmpty)
		  and (dgRowSelect in Options) then begin
			if SelectedRows.Find(DataLink.DataSet.Bookmark, Index)
			  or (not (dgMultiSelect in Options) and (Focused or (dgAlwaysShowSelection in Options))) then begin
				Canvas.Brush.Color := clHighlight;
				Canvas.Font.Color := clHighlightText;
			end else begin
				Canvas.Brush.Color := Columns[ACol].Color;
				Canvas.Font.Color := Columns[ACol].Font.Color;
			end;
			// Dibujamos la celda en funcion de los eventos definidos por el usuario
			if not enabled then
				Canvas.Font.Color := clGrayText;
			DefaultDrawColumnCell(ARect,ACol,Columns[ACol],AState);
			if Columns.State = csDefault then
				DrawDataCell(ARect, Columns[ACol].Field, AState);
			DrawColumnCell(ARect, ACol, Columns[ACol], AState);
			// Dibujamos el recuadro
			if ((DataLink.ActiveRecord = ARow - 1) or DataLink.dataset.IsEmpty)
			  and (Focused or (dgAlwaysShowSelection in Options)) then 
    			drawRectangle(ARect);
		end;
	end;
end;

procedure TDPSGrid.DrawRectangle(ARect: TRect);
var
	PenStyle: TPenStyle;
	PenColor: TColor;
	BrushStyle: TBrushStyle;
begin
	PenStyle := Canvas.Pen.Style;
	PenColor := Canvas.Pen.Color;
	BrushStyle := Canvas.Brush.Style;
	Canvas.Pen.Style := psDot;
	Canvas.Pen.Color := clGray;
	Canvas.Brush.Style := bsClear;
	Canvas.Rectangle(Rect(0, ARect.Top, ClientWidth, ARect.Bottom));
	Canvas.Pen.Style := PenStyle;
	Canvas.Pen.Color := PenColor;
	Canvas.Brush.Style := BrushStyle;
end;

function TDPSGrid.EndColumnDrag(var Origin, Destination: Integer;
  const MousePt: TPoint): Boolean;
begin
	result := inherited EndColumnDrag(Origin, Destination, MousePt);
	if result then begin
		if FColumnOrderly = Origin then
			FColumnOrderly := Destination
		else if (Destination <= FColumnOrderly) and (FColumnOrderly < Origin) then
			inc(FColumnOrderly)
		else if (Destination >= FColumnOrderly) and (FColumnOrderly > Origin) then
			dec(FColumnOrderly);
		Invalidate;
	end;
end;

function TDPSGrid.GetColumnOrderly: Integer;
begin
	if dgIndicator in Options then
		result := FColumnOrderly - 1
	else
		result := FColumnOrderly;
end;

function TDPSGrid.GetOrderIndicator: TOrderIndicator;
begin
	Result := FOrderIndicator;
end;

procedure TDPSGrid.InvertirMarcas;
var bm: TBookMark;
begin
	if Assigned(DataLink.DataSet) and DataLink.DataSet.Active then begin
		with DataLink.DataSet do begin
			DisableControls;
			bm := getBookMark;
			first;
			while not eof do begin
				SelectedRows.CurrentRowSelected := not SelectedRows.CurrentRowSelected;
				next;
			end;
			gotoBookMark(bm);
			freeBookMark(bm);
			EnableControls;
		end;
	end;
end;

procedure TDPSGrid.KeyDown(var Key: Word; Shift: TShiftState);

  procedure Marcar(Direction: Integer);
  var bm: TbookMark;
  begin
	  // Si la direccion cambia y viene haciendo una seleccion o deseleccion
	  // Marco o desmarco el actual, luego muevo un lugar
	  if (FDirection <> Direction) and (FDirection <> 0) then begin
		  // Si cambie de direccion pero estoy parado donde empece no cambio la seleccion
		  bm := DataLink.dataset.getbookmark;
		  if DataLink.dataset.CompareBookmarks(FOriginalRegister,bm) <> 0 then
			  FMarkSelection := not FMarkSelection;
		  DataLink.dataset.Freebookmark(bm);
	  end;
	  SelectedRows.CurrentRowSelected := FMarkSelection;
	  DataLink.Dataset.moveBy(Direction);
	  // Si estoy marcando marco la nueva fila
	  if FMarkSelection then
		  SelectedRows.CurrentRowSelected := FMarkSelection;
	  FDirection := Direction;
	  // Si estoy parado donde comence a marcar cambio la seleccion
	  bm := DataLink.dataset.getbookmark;
	  if DataLink.dataset.CompareBookmarks(FOriginalRegister,bm) = 0 then
		  FMarkSelection := not FMarkSelection;
	  DataLink.dataset.Freebookmark(bm);
  end;

  procedure MarcarRegistros(Key: Word);
  var FMarcaCursor, FMarcaInicial, FmarcaFinal :TBookMark;
	  Sentido: Integer;
  begin
	  // Tomamos el actual, calculamos el sentido y el paso
	  FMarcaInicial := DataLink.DataSet.GetBookmark;
	  case Key of
		  VK_NEXT:
			  begin
				  Sentido := 1;
				  DataLink.dataset.MoveBy(VisibleRowCount - 1);
			  end;
		  VK_PRIOR:
			  begin
				  Sentido := -1;
				  DataLink.dataset.MoveBy(-(VisibleRowCount - 1));
			  end;
		  VK_HOME:
			  begin
				  Sentido := -1;
				  DataLink.DataSet.first;
			  end
		  else begin
			  Sentido := 1;
			  DataLink.DataSet.last;
		  end;
	  end;
	  // Tomamos el nuevo registro y recorremos desmarcando y marcando
	  DataLink.Dataset.DisableControls;
	  FMarcaFinal := DataLink.DataSet.GetBookmark;
	  FMarcaCursor := nil;
	  DataLink.DataSet.GotoBookmark(FMarcaInicial);
	  While DataLink.dataset.CompareBookmarks(FMarcaCursor,FMarcaFinal) <> 0 do begin
		  DataLink.Dataset.FreeBookmark(FMarcaCursor);
		  Marcar(Sentido);
		  FMarcaCursor := DataLink.DataSet.getBookmark;
	  end;
	  SelectedRows.CurrentRowSelected := true;
	  DataLink.DataSet.FreeBookmark(FMarcaCursor);
	  DataLink.DataSet.FreeBookmark(FMarcaFinal);
	  DataLink.DataSet.FreeBookmark(FMarcaInicial);
	  DataLink.Dataset.EnableControls;
  end;

begin
	if not (dgMultiSelect in Options) then begin
		inherited;
		exit;
	end;
	{ Si esta el shift esta presionado para algunas teclas definimos nosotros el comportamiento,
	  sino heredamos}
	if (ssShift in Shift) then begin
		if dgMultiselect in Options then begin
			if ((Key in [VK_UP, VK_DOWN, VK_NEXT, VK_PRIOR, VK_HOME, VK_END])) then begin
				if (FDirection = 0) then
					FOriginalRegister := DataLink.dataset.getBookMark;
				case Key of
					VK_UP:
						marcar(-1);
					VK_DOWN:
						marcar(1);
					VK_NEXT, VK_PRIOR, VK_HOME, VK_END:
						MarcarRegistros(Key);
				end;
			end;
		end;
		exit;
	end;
	{ Si esta el control aplicado y la barra espaciadora desmarcamos o marcamos la fila actual
	  Con el resto de las teclas navegamos sin desmarcar lo desmarcado }
	if (ssCtrl in Shift) then begin
		if dgMultiselect in Options then begin
			case Key of
				VK_SPACE:
					begin
						SelectedRows.CurrentRowSelected := not SelectedRows.CurrentRowSelected;
						invalidate;
					end;
				VK_HOME:
					DataLink.DataSet.First;
				VK_END:
					DataLink.DataSet.Last;
				VK_DOWN, VK_LEFT:
					DataLink.DataSet.Next;
				VK_UP, VK_RIGHT:
					DataLink.DataSet.Prior;
				VK_NEXT:
					DataLink.DataSet.MoveBy(VisibleRowCount);
				VK_PRIOR:
					DataLink.DataSet.MoveBy(-VisibleRowCount);
			end;
		end else begin
			if Key = VK_SPACE then begin
				SelectedRows.CurrentRowSelected := not SelectedRows.CurrentRowSelected;
				invalidate;
			end;
		end;
		exit;
	end;
	{ Setemaos la direccion en 0, empieza una nueva selecci�n la proxima vez que se presione shift}
	FDirection := 0;
	FOriginalRegister := DataLink.dataset.getBookMark;
	FMarkSelection := True;
	inherited;
	{ Marcamos el actual}
	SelectedRows.CurrentRowSelected := not SelectedRows.CurrentRowSelected;
	invalidate;
end;

procedure TDPSGrid.LinkActive(value: Boolean);
begin
	inherited LinkActive(value);
	if Assigned(FOnOrderChange) and value then begin
		FOnOrderChange(Self, FColumnOrderly, FOrderIndicator);
		if (dgIndicator in Options) and (FColumnOrderly > -1) then
			Inc(FColumnOrderly);
	end;
	if Value and Assigned(DataLink.DataSet) and DataLink.DataSet.Active then
		FOriginalRegister := DataLink.dataset.getBookMark
	else
		FOriginalRegister := nil;
end;

procedure TDPSGrid.MarcarTodo;
var bm: TBookMark;
begin
	if Assigned(DataLink.DataSet) and DataLink.DataSet.Active then begin
		with DataLink.DataSet do begin
			DisableControls;
			bm := getBookMark;
			first;
			while not eof do begin
				SelectedRows.CurrentRowSelected := true;
				next;
			end;
			gotoBookMark(bm);
			freeBookMark(bm);
			EnableControls;
		end;
	end;
end;

procedure TDPSGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
Var
	Coord: TGridCoord;
	MarkCursor, LastMark: TBookMark;
	ACol, LastRecord, InitialRecord: Integer;
begin
	inherited;
	invalidate;
	if Button <> mbLeft then Exit;
	coord := MouseCoord(X, Y);
	// Calculamos la columna
	if dgIndicator in Options then
		ACol := Coord.X - 1
	else
		ACol := Coord.X;

	// Nos fijamos si clicke� en una celda "link"
	if (Coord.Y > 0) and (fsUnderline in Columns[ACol].Font.Style) then begin
		if Assigned(FOnLinkClick) then begin
			FOnLinkClick(Self, Columns[ACol]);
			exit;
		end;
	end;


	if not (dgMultiSelect in Options) and (Coord.Y > 0) then exit;
	if (ssShift in Shift) and (mbLeft = Button) then begin
		// Limpiamos, y tomamos el actual para saber la direcci�n y que registros recorrer
		SelectedRows.Clear;
		LastMark := DataLink.DataSet.GetBookmark;
		LastRecord := DataLink.dataset.RecNo;
		MarkCursor := nil;
		DataLink.Dataset.DisableControls;
		DataLink.DataSet.GotoBookmark(FOriginalRegister);
		InitialRecord := DataLink.dataset.RecNo;
		FDirection := sign(LastRecord - InitialRecord);
		While (DataLink.dataset.CompareBookmarks(MarkCursor, LastMark) <> 0) do begin
			DataLink.Dataset.FreeBookmark(MarkCursor);
			SelectedRows.CurrentRowSelected := true;
			DataLink.Dataset.moveBy(FDirection);
			MarkCursor := DataLink.DataSet.getBookmark;
		end;
		SelectedRows.CurrentRowSelected := true;
		DataLink.DataSet.FreeBookmark(MarkCursor);
		DataLink.DataSet.FreeBookmark(LastMark);
		DataLink.Dataset.EnableControls;
		FMarkSelection := True;
		Invalidate;
	end else begin
		{ Setemaos la direccion en 0, empieza una nueva selecci�n la proxima vez que se presione shift }
		FOriginalRegister := DataLink.dataset.getBookMark;
		FMarkSelection := True;
		FDirection := 0;

		if mbLeft = Button then begin
			{ Si estamos presionando el bot�n (Y no cambiando el tama�o de la columna) preguntamos
			  si queremos ejecutar la accion asociada al bot�n e indico que boton fue presionado }
			if (X < (CellRect(Coord.X, Coord.Y).Right - 5)) and
			  (X > (CellRect(Coord.X, Coord.Y).Left + 5)) and
			  (Coord.Y = 0) then begin
				FButtonPress := -1;
				if Assigned(OnTitleClick) then begin
					if assigned(FOnWillClickTitle) then
						FOnWillClickTitle(Self, Columns[ACol], FExecuteAction);
					if FExecuteAction then begin
						FButtonPress := Coord.X
					end;
				end;
			end;
		end;
	end;
end;

procedure TDPSGrid.MouseMove(Shift: TShiftState; X, Y: Integer);
Var
	cr: TCursor;
	Coord: TGridCoord;
begin
	inherited;
	cr := crDefault;
	Coord := MouseCoord(X, Y);
	if (FGridState = gsColMoving) or not (dgColumnResize in Options) then
		FExecuteAction := False;
	if dgIndicator in Options then Dec(Coord.X);
	if (Coord.Y > 0) and (Coord.Y < RowCount) and (Coord.X >= 0)
	  and (Coord.X < Columns.Count) then begin
		if fsUnderline in Columns[Coord.X].Font.Style then begin
			cr := crHandPoint;
		end;
	end;
	Cursor := cr;
end;

procedure TDPSGrid.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
	inherited;
	if FButtonPress <> -1 then 
		FButtonPress := -1;
	invalidate;
end;

procedure TDPSGrid.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
	inherited;
end;

procedure TDPSGrid.Paint;
Var
	R: TRect;
	aCol, aRow, OldActive, Index: Integer;
begin
	inherited Paint;
	// Faltar�a dibujar el resto del header (lo que falte hasta el ancho del grid)
	// y lo mismo con el "Resaltado" de la celda actual
	R := CellRect(ColCount - 1, 0);
	if (R.Right <> R.Left) then begin

		// Header
		R := Rect(R.Right, 0, ClientWidth, R.Bottom);
		Canvas.Brush.Color := Columns[0].Title.Color;
		Canvas.FillRect(R);
		DrawEdge(Canvas.Handle, R, BDR_RAISEDINNER, BF_BOTTOM or BF_RIGHT);
		DrawEdge(Canvas.Handle, R, BDR_RAISEDINNER, BF_TOP or BF_LEFT);
		Canvas.Pen.Color := clBlack;
		Canvas.MoveTo(R.Left, R.Bottom - 1);
		Canvas.LineTo(R.Right - 1, R.Bottom - 1);
		Canvas.LineTo(R.Right - 1, R.Top - 1);

		// Celdas seleccionadas
		aRow := topRow;
		while aRow <= VisibleRowCount do begin
			R := CellRect(ColCount - 1, aRow);
			R := Rect(R.Right, R.Top, ClientWidth, R.Bottom);
			if not (csDesigning in ComponentState) and (dgRowSelect in Options)
			  and Assigned(DataLink.Dataset) and DataLink.Dataset.Active then begin
				if ((DataLink.ActiveRecord = ARow - 1) or DataLink.dataset.IsEmpty)
				  and (Focused or (dgAlwaysShowSelection in Options)) then begin
					// Dibujar un recuadro para mostrar que es la fila actual
					// Adem�s de ser la fila actual puede estar seleccionada
					if SelectedRows.Find(DataLink.DataSet.Bookmark, Index)
					  or not (dgMultiSelect in Options) then
						Canvas.Brush.Color := clHighlight
					else
						Canvas.Brush.Color := Color;
					Canvas.FillRect(R);
					drawRectangle(R);
				end else begin
					OldActive := DataLink.ActiveRecord;
					try
						DataLink.ActiveRecord := ARow - 1;
						if SelectedRows.Find(DataLink.DataSet.Bookmark, Index) then
							// Esta seleccionada
							Canvas.Brush.Color := clHighlight
						else
							// NO esta seleccionada ni tiene el foco
							Canvas.Brush.Color := color;
						Canvas.FillRect(R);
					finally
						DataLink.ActiveRecord := OldActive;
					end;
				end;
			end else begin
				// La grilla esta vacia (El datalink esta cerrado)
				Canvas.Brush.Color := color;
				Canvas.FillRect(R);
			end;
			Inc(aRow);
		end;
		// Pinto la parte de abajo siguiendo la configuracion de las columnas
		ACol := 0;
		While ACol < Columns.Count do begin
			if dgIndicator in Options then
				R := CellRect(ACol+1, VisibleRowCount)
			else
				R := CellRect(ACol, VisibleRowCount);
			R := Rect(R.Left, R.Bottom, R.Right, ClientHeight);
			Canvas.Brush.Color := Columns[ACol].Color;
			Canvas.FillRect(R);
			inc(ACol);
		end;
	end;
end;

procedure TDPSGrid.Scroll(distance: Integer);
begin
	inherited;
	invalidate;
end;

function TDPSGrid.SelectCell(ACol, ARow: Integer): Boolean;
begin
	Result := Inherited SelectCell(ACol, ARow);
	if Result and HandleAllocated then InvalidateRect(Handle, nil, False);
end;


procedure TDPSGrid.TitleClick(Column: TColumn);
begin
	if FExecuteAction and Assigned(FOnOrderChange) then begin
		inherited;
		FOnOrderChange(Self, FColumnOrderly, FOrderIndicator);
		if (dgIndicator in Options) and (FColumnOrderly > -1) then
			inc(FColumnOrderly);
	end;
end;

procedure TDPSGrid.WMKillFocus(var Msg: TWMKillFocus);
begin
	inherited;
	Invalidate;
end;

end.
