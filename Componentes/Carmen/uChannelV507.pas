{

 Carmen V 5.0.7 Delphi component of CARMEN(TM) Automatic Number Plate Recognition.

 Copyright (C) 1992-2000, by Adaptive Recognition Hungary.
 CARMEN is the Trade Mark of Adaptive Recognition Hungary.

 This code and information is only intended as a supplement to the CARMEN Automatic Number
 Plate Recognition Product.  For conditions of distribution  and use, see the accompanying
 README file.  This code and information is provided "AS IS" without warranty of any kind,
 either  expressed or implied, including  but not limited  to  the  implied  warranties of
 merchantability and/or fitness for a particular purpose.

}

unit uChannelV507;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, uConstantsV507;

type
  TChannel=class                                         //Logical Channel Class
  private
    function GetVideoFormat:TVideoFormat;
    function GetColorFormat:TColorFormat;
    function GetResolution:TResolution;
    procedure SetVideoFormat(Value:TVideoFormat);
    procedure SetColorFormat(Value:TColorFormat);
    procedure SetResolution(Value:TResolution);
  public
    CtrlVideo,CtrlFormat:integer;
    VideoSource:integer;                                       //Physical video source of the logical channel
    Bitmap:TBitmap;                                            //Bitmap assigned to the channel
    Live:Boolean;
    Timer:TTimer;
    Grabbing:Boolean;
    Adjust:TChannel_Param;
    RawImage:Pointer;
    ImageInfo:TCapture_Info;
    property VideoFormat:TVideoFormat read GetVideoFormat write SetVideoFormat default vfPal;
    property ColorFormat:TColorFormat read GetColorFormat write SetColorFormat default cfGray;
    property Resolution:TResolution read GetResolution write SetResolution default resF;
    property Contrast:integer read Adjust.Contrast write Adjust.Contrast default 0;
    property Brightness:integer read Adjust.Bright write Adjust.Bright default 0;
    property Saturation:integer read Adjust.Saturation write Adjust.Saturation default 0;
    property Hue:integer read Adjust.hue write Adjust.hue default 0;
    property Format:integer read CtrlFormat;
    constructor Create;
    destructor Destroy;Override;
  end;

implementation

constructor TChannel.Create;
begin
     VideoFormat:=vfPal;
     ColorFormat:=cfGray;
     Resolution:=resFr;
     Contrast:=0;
     Saturation:=0;
     Hue:=0;
     Brightness:=0;
     Timer:=TTimer.Create(Timer);
     Live:=false;
     Grabbing:=false;
     RawImage:=nil;
end;

destructor TChannel.Destroy;
begin
     if Assigned(RawImage) then FreeMem(RawImage);
     Timer.Free;
end;

procedure TChannel.SetVideoFormat(Value:TVideoFormat);
begin
     case Value of
     vfPal:CtrlVideo:=FXCAP_SIGNAL_PAL;
     end;
     Adjust.signal:=CtrlVideo;
end;

function TChannel.GetVideoFormat:TVideoFormat;
begin
     case Adjust.signal of
        FXCAP_SIGNAL_PAL:result:=vfPal;
     else
        result:=vfPal;
     end;
end;

procedure TChannel.SetColorFormat(Value:TColorFormat);
var
   tmp,tmpres:integer;
begin
     tmp:=0;
     tmpres:=CtrlFormat and FXCAP_VFMT_MASK;
     case Value of
        cfGray:tmp:=FXCAP_FORMAT_GRAYSCALE;
        cfHiColor:tmp:=FXCAP_FORMAT_HICOLOR15;
        cfYuv422:tmp:=FXCAP_FORMAT_YUV422;
        cfRGB24:tmp:=FXCAP_FORMAT_RGB24
     end;
     CtrlFormat:= tmpres or tmp;
end;

function TChannel.GetColorFormat:TColorFormat;
var tmp:integer;
begin
   tmp:=(CtrlFormat and FXCAP_CFMT_MASK);
   case tmp of
     FXCAP_FORMAT_GRAYSCALE:result:=cfGray;
     FXCAP_FORMAT_HICOLOR15:result:=cfHiColor;
     FXCAP_FORMAT_YUV422:result:=cfYuv422;
     FXCAP_FORMAT_RGB24:result:=cfRGB24;
     FXCAP_FORMAT_RGB32:result:=cfRGB32;
   else
     Result:=cfGray;
   end;
end;

procedure TChannel.SetResolution(Value:TResolution);
var
   tmp,tmpcol:integer;
begin
   tmpcol:=CtrlFormat and FXCAP_CFMT_MASK;
   tmp:=0;
   case Value of
      resHFE:tmp:=FXCAP_HALF_FIELD_EVEN;
      resHFO:tmp:=FXCAP_HALF_FIELD_ODD;
      resHF:tmp:=FXCAP_HALF_FIELD;
      resFE:tmp:=FXCAP_FIELD_EVEN;
      resFO:tmp:=FXCAP_FIELD_ODD;
      resF:tmp:=FXCAP_FIELD;
      resFrE:tmp:=FXCAP_FRAME_EVEN;
      resFrO:tmp:=FXCAP_FRAME_ODD;
      resFr:tmp:=FXCAP_FRAME;
   end;
   CtrlFormat:=tmpcol or tmp;
end;

function TChannel.GetResolution:TResolution;
var
   tmp:integer;
begin
   tmp:=CtrlFormat and FXCAP_VFMT_MASK;
   case tmp of
     FXCAP_HALF_FIELD_EVEN:result:=resHFE;
     FXCAP_HALF_FIELD_ODD:result:=resHFO;
     FXCAP_HALF_FIELD:result:=resHF;
     FXCAP_FIELD_EVEN:result:=resFE;
     FXCAP_FIELD_ODD:result:=resFO;
     FXCAP_FIELD:result:=resF;
     FXCAP_FRAME_EVEN:result:=resFrE;
     FXCAP_FRAME_ODD:result:=resFrO;
     FXCAP_FRAME:result:=resFr;
   else
     Result:=resF;
   end;
end;

end.
