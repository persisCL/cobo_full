program ImgBase64;

{$APPTYPE CONSOLE}

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Util,
  DTEControlDLL,
  IdCoder,
  IdCoderMIME,
  XMLDoc,
  XMLIntf,
  ActiveX,
  DatosEntrada in 'DatosEntrada.pas',
  ConstParametrosGenerales in '..\..\Comunes\ConstParametrosGenerales.pas';

const
        CONST_INVALID_PARAMETERS    = 'Too many or no parameters provided';
        CONST_ERROR_JPG             = 'Error creating the JPG File';
        CONST_INVALID_XML           = 'Invalid XML';

var
    Timbre : TTimbreArchivo;
    CodErrorDLL: Integer;
    MensajeError: string;
    RutaImg : string;
    Base64String: String;
    Datos: TDatosEntrada;
    EliminarJPG : string;

function DateFormat(fecha: string):TDateTime;
const
    CONST_FORMATO_FECHA_INVALIDO = 'Formato de fecha invalido';
var
  year, month, day: string;
begin

  if Length(fecha) <> 8 then
    raise Exception.Create(CONST_FORMATO_FECHA_INVALIDO);
  try
     year:= Copy(fecha,1, 4);
      month:= Copy(fecha, 5, 2);
      day:= Copy(fecha, 7, 2);

      Result:= EncodeDate(StrToInt(year), StrToInt(month), StrToInt(day));
  except
        on ex: Exception do
          raise Exception.Create(CONST_FORMATO_FECHA_INVALIDO);
  end;

end;

function JpgToBase64(path : string): string;
const
    CONST_FILE_NOT_FOUND = 'File not found';
var
    fileStm : TStream;
    Encoder: TIdEncoderMIME;
begin
    Result := '';

    if not FileExists(RutaImg) then
       raise Exception.Create(CONST_FILE_NOT_FOUND);

    Encoder := TIdEncoderMIME.Create;
    try
        try
            fileStm := TFileStream.Create(RutaImg, fmOpenReadWrite);

            filestm.Seek(0, soFromBeginning);

            Result := Encoder.Encode(fileStm);
        Except
            on ex: Exception do
                raise Exception.Create(ex.message);
        end;
    finally
        if Assigned(fileStm) then
            fileStm.Free;
        Encoder.Free;
    end;
end;

function CrearArchivo(Base64String: string; TipoDocumento, NumeroDocumento: string): Boolean;
var
    myFile : TextFile;
    EscribirXML, RutaXML, XMLToOutput: string;
begin
    Result:= False;

    RutaXML := InstallIni.ReadString('General', 'RutaXML', PARAM_DIR_EGATE_HOME + 'out\xml\');

    AssignFile(myFile, RutaXML + '\' + TipoDocumento + NumeroDocumento + '.xml');

    EscribirXML := InstallIni.ReadString('General', 'EscribirXML', '1');

    if EscribirXML = '1' then
    begin
        try
            try
            ReWrite(myFile);

            Write(myFile, '<Timbre><Data>' + Base64String + '</Data></Timbre>');

            writeln(Output, 'pathxml:' + RutaXML + '\' + TipoDocumento + NumeroDocumento + '.xml');

            except
                on ex: Exception do
                    raise Exception.Create(ex.message);
            end;
        finally
            CloseFile(myFile);
        end;
    end;

    XMLToOutput := InstallIni.ReadString('General', 'XMLToOutput', '0');

    if XMLToOutput = '1' then
    begin
         writeln(Output, 'xmlBase64: <Timbre><Data>' + Base64String + '</Data></Timbre>');
    end;

end;

function XmtToDatosEntrada(xml: string): TDatosEntrada;
var
    oXML: IXMLDocument;
    memStr: TMemoryStream;
    dataNode: IXMLNode;
    resultado: TDatosEntrada;
begin

    Result:= nil;
    memStr:= TMemoryStream.Create;
    oXML:=   TXMLDocument.Create(nil);
    try
        try

            xml := UTF8Encode(xml);

            memStr.Seek(0, soFromBeginning);
            memStr.WriteBuffer(Pointer(xml)^, Length(xml)*SizeOf(Char));

            oXML.LoadFromStream(memStr);
            dataNode := oXML.DocumentElement;

            if (dataNode.ChildNodes['RutEmisor'].Text = '') or
                (dataNode.ChildNodes['DigitoVerEmisor'].Text = '') or
                (dataNode.ChildNodes['TipoDocumento'].Text = '') or
                (dataNode.ChildNodes['Folio'].Text = '') or
                (dataNode.ChildNodes['FechaHoraEmision'].Text = '') or
                (dataNode.ChildNodes['Monto'].Text = '') or
                (dataNode.ChildNodes['GlosaItem1'].Text = '') or
                (dataNode.ChildNodes['RutReceptor'].Text = '') or
                (dataNode.ChildNodes['DigitoVerReceptor'].Text = '') or
                (dataNode.ChildNodes['RazonSocialReceptor'].Text = '') or
                (dataNode.ChildNodes['FechaHoraTimbre'].Text = '')
            then
                raise Exception.Create(CONST_INVALID_XML);

            resultado := TDatosEntrada.Create();
            resultado.RutEmisor := StrToInt(dataNode.ChildNodes['RutEmisor'].Text);
            resultado.DigitoVerEmisor := dataNode.ChildNodes['DigitoVerEmisor'].Text;
            resultado.TipoDocumento := StrToInt(dataNode.ChildNodes['TipoDocumento'].Text);
            resultado.Folio := StrToInt(dataNode.ChildNodes['Folio'].Text);
            resultado.FechaHoraEmision := DateFormat(dataNode.ChildNodes['FechaHoraEmision'].Text);
            resultado.Monto := StrToFloat(dataNode.ChildNodes['Monto'].Text);
            resultado.GlosaItem1 := dataNode.ChildNodes['GlosaItem1'].Text;
            resultado.RutReceptor := StrToInt(dataNode.ChildNodes['RutReceptor'].Text);
            resultado.DigitoVerReceptor := dataNode.ChildNodes['DigitoVerReceptor'].Text;
            resultado.RazonSocialReceptor := dataNode.ChildNodes['RazonSocialReceptor'].Text;
            resultado.FechaHoraTimbre := DateFormat(dataNode.ChildNodes['FechaHoraTimbre'].Text);

            Result := resultado;

        except
            on ex: Exception do
            begin
               raise Exception.Create(ex.Message);
            end;
        end;

    finally
        memStr.Free;
    end;

end;

begin
CoInitialize(nil);
    try
        try
            if (ParamCount <> 1) then raise Exception.Create(CONST_INVALID_PARAMETERS);

            
            {MensajeError := '<Data>' +
                                '<RutEmisor>96875230</RutEmisor>' +
                                '<DigitoVerEmisor>8</DigitoVerEmisor>' +
                                '<TipoDocumento>34</TipoDocumento>' +
                                '<Folio>2880010</Folio>' +
                                '<FechaHoraEmision>20151130</FechaHoraEmision>' +
                                '<Monto>279940000</Monto>' +
                                '<GlosaItem1>Per�odo</GlosaItem1>' +
                                '<RutReceptor>88147600</RutReceptor>' +
                                '<DigitoVerReceptor>2</DigitoVerReceptor>' +
                                '<RazonSocialReceptor>Hormigones Transex Ltda. </RazonSocialReceptor>' +
                                '<FechaHoraTimbre>20151130</FechaHoraTimbre>' +
                            '</Data>';

            Datos := XmtToDatosEntrada(MensajeError);}

            Datos := XmtToDatosEntrada(ParamStr(1));

            if not Assigned(datos) then raise Exception.Create(CONST_INVALID_XML);
            ConfigurarVariable_EGATE_HOME(nil, false, MensajeError, false);
            CodErrorDLL := CargarDLL();
                if CodErrorDLL < 0 then
                    raise Exception.Create(ObtieneErrorAlCargarDLL(CodErrorDLL));

            ObtenerTimbreDBNet(datos.RutEmisor, datos.DigitoVerEmisor, datos.TipoDocumento,
                                datos.Folio, datos.FechaHoraEmision, datos.Monto,
                                datos.GlosaItem1, datos.RutReceptor, datos.DigitoVerReceptor,
                                datos.RazonSocialReceptor, datos.FechaHoraTimbre,
                                True, Timbre);

            if Timbre.Codigo <> 'OK'#0'        ' then
                raise Exception.Create(CONST_ERROR_JPG);

            RutaImg := InstallIni.ReadString('General', 'RutaImg', PARAM_DIR_EGATE_HOME + 'out\html\');

            RutaImg := RutaImg + '\' + Timbre.Archivo + '.jpg';

            Base64String := JpgToBase64(RutaImg);

            CrearArchivo(Base64String, IntToStr(datos.TipoDocumento),IntToStr(datos.Folio));

            EliminarJPG := InstallIni.ReadString('General', 'EliminarJPG', '1');
            if EliminarJPG = '1' then
                deletefile(RutaImg);

            ExitCode := 0;

            //Readln;

        except
            on E:Exception do
            begin
                Writeln(E.Classname, ': ', E.Message);
                ExitCode := 255;
            end;
        end;
    finally
       LiberarDLL;
       CoUninitialize;
       if Assigned(Datos) then
          Datos.Free;
    end;             
end.






