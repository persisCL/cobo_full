unit ComboBuscarCalle;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
    StdCtrls,Util,ADODB, utildb,db;

type
    TComboBuscarCalle = class;

    TKeyUtilizada = procedure(Sender: TObject; Key: Word) of object;

    TListBoxVariantEx = Class(TListBox)
        private
            FKeyUtilizada:TKeyUtilizada;
            PressTecla:Boolean;
            function GetCodigoCalle:Integer;
            function GetCodigocomuna:AnsiString;
        protected
            procedure WMGetDlgCode(var Message: TMessage); message WM_GETDLGCODE;
            procedure KeyDownList(Sender: TObject; var Key: Word; Shift: TShiftState);
            procedure OnSalidaLista(Sender: TObject);
            procedure OnDoubleClickLista(Sender: TObject);
            procedure OnEnterLista(Sender: TObject);
        public
            constructor Create(AOwner: TComponent); override;
            function GrabarCodigoCalleComuna(CodigoCalle,CodigoComuna: Integer): TObject;
            property OnKeyUtilizada:TKeyUtilizada read FKeyUtilizada write FKeyUtilizada;
            property CodigoCalle:Integer read GetCodigoCalle;
            property CodigoComuna:AnsiString read GetCodigocomuna;
            property PresionoTecla:Boolean read PressTecla write PressTecla;
        end;

    TComboBuscarCalle = class(TEdit)
    private
        Vacio:Boolean;
        spBuscarCalles:TADOStoredProc;
        Lista:TListBoxVariantEx;
        FForm: TForm;
        Desplegado:Boolean;
        FCodigoRegion: AnsiString;
        FCodigoComuna: AnsiString;
        FCodigoCalle:Integer;
        FCodigoPais:AnsiString;
        fConn:TADOConnection;
        FDropCount: Integer;
        procedure Desplegar(Valor: Boolean=True);
        procedure KeyDownBuscador(Sender: TObject; var Key: Word; Shift: TShiftState);
        procedure KeyPressBuscardorprocedure (Sender: TObject; var Key: Char);
        procedure CambioTexto(Sender: TObject);
        procedure BuscarCalles;
        procedure SetCodigoComuna(const Value: AnsiString);
        procedure SetCodigoRegion(const Value: AnsiString);
        procedure ProcesarTecla(Sender: TObject; Key: Word);
        procedure ProcesarSalida(Sender: TObject);
        procedure ProcesarEntrada(Sender: TObject);
        procedure IralProximo;
        procedure IralAnterior;
        procedure LevantarCalle;
        procedure SetCodigoCalle(const Value: Integer);
        procedure SetCodgoPais(const Value: AnsiString);
        procedure OnSalida(Sender: TObject);
        function GetDropCount: Integer;
        procedure SetDropCount(const Value: Integer);
        function GetNobreCalle: String;
        procedure SetNombreCalle(const Value: String);
        function GetCalleNormalizada: Boolean;
    protected
        procedure WMGetDlgCode(var Message: TMessage); message WM_GETDLGCODE;
    public
        constructor Create(AOwner: TComponent); override;
        destructor Destroy; override;
    published
        property NombreCalle:String read GetNobreCalle write SetNombreCalle;
        property CodigoComuna:AnsiString read FCodigoComuna write SetCodigoComuna;
        property CodigoRegion:AnsiString read FCodigoRegion write SetCodigoRegion;
        property CodigoPais:AnsiString read FCodigoPais write SetCodgoPais;
        property CodigoCalle:Integer read FCodigoCalle write SetCodigoCalle;
        property Conn: TADOConnection read fConn write fConn;
        property DropCount: Integer read GetDropCount write SetDropCount;
        property CalleNormalizada:Boolean read GetCalleNormalizada;
        procedure ForzarBusqueda;
    end;


implementation


{ TComboBuscarCalle }

constructor TComboBuscarCalle.Create(AOwner: TComponent);

  function PadrePrincipal(Form:TwinControl):TwinControl;
  begin
        if form.Parent=nil then result:=form
        else result:=PadrePrincipal(form.parent);
  end;

begin

    inherited;

    Conn := nil;
    MaxLength := 50;
    Parent := TWinControl(AOwner);

    Desplegado := False;
    name := 'cb_ComboBuscarCalle';
    DropCount := 8;
    CharCase := ecUpperCase;
    OnKeyDown := KeyDownBuscador;
    OnKeyPress := KeyPressBuscardorprocedure;
    OnChange := CambioTexto;
    OnExit := OnSalida;
    FCodigoCalle := - 1;
    FCodigoRegion := '';
    FCodigoComuna := '';
    FCodigoPais := '';
    text := '';

    FForm := TForm.create(nil);
    name := self.Name + 'TForm';
    fform.BorderStyle := bsNone;
    Lista := TListBoxVariantEx.Create(fform);
    lista.Parent := fform;
    lista.Align := alClient;

    lista.OnKeyUtilizada := ProcesarTecla;
    fform.OnDeactivate := ProcesarSalida;
    FForm.OnActivate := ProcesarEntrada;

    spBuscarCalles := TADOStoredProc.Create(nil);
    spBuscarCalles.Name := self.name + 'spBuscarCalles';
    spBuscarCalles.ProcedureName := 'BuscarCalles';

end;

procedure TComboBuscarCalle.Desplegar(Valor: Boolean=True);
var
    aux:TPoint;
    acount: Integer;
begin
    lista := (FForm.controls[0] as TListBoxVariantEx);

    vacio:=False;
    aCount := DropCount;
    if Lista.Items.Count > DropCount then aCount := DropCount;
    if Lista.Items.Count < 1 then begin
        aCount := 1;
        vacio:=true;
    end;

    FForm.Height := (aCount * (lista.ItemHeight)) + 4;
    FForm.Width := self.Width;

    aux.X:=self.Left;
    aux.Y:=self.top+self.Height;
    if self.parent.ClientToScreen(aux).Y + lista.Height >= screen.Height then begin
        aux.Y := self.Top - lista.Height;
    end;
    aux := (self.parent.ClientToScreen(aux));
    FForm.Left:=aux.x;
    FForm.top:=aux.y;

    Desplegado:=Valor;

    if Valor then begin
        Lista.SetItemIndex(0);
        FForm.Show;
    end else
        FForm.Hide;
end;

destructor TComboBuscarCalle.Destroy;
begin
    spBuscarCalles.Destroy;
    FForm.Destroy;

  inherited;
end;


procedure TComboBuscarCalle.KeyDownBuscador(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if key = VK_RETURN then BuscarCalles;

    if (key = VK_DOWN) and (Desplegado) then begin
        Lista.SetItemIndex(0);
        Lista.SetFocus;
    end;

    if key = VK_TAB then begin
        Desplegar(false);
        IralProximo;
    end;

    if (key = VK_TAB) and (ssShift in Shift ) then begin
        Desplegar(false);
        IralAnterior;
    end;


    if key =VK_ESCAPE then Desplegar(false);

    if (key = VK_DELETE) OR (key = VK_BACK) then begin
        FCodigoCalle:=-1;
        FCodigoComuna:='';
        Desplegar(false);
    end;
end;

procedure TComboBuscarCalle.WMGetDlgCode(var Message: TMessage);
begin
    Message.Result := DLGC_WANTCHARS + DLGC_WANTARROWS + DLGC_WANTTAB + DLGC_WANTALLKEYS;
end;


procedure TComboBuscarCalle.BuscarCalles;
var
    aux_text:String;
begin
    aux_text := Text;
    Text := 'Buscando ...';
    self.update;
    Lista.Clear;
	try
        spBuscarCalles.Connection:=fConn;
        with spBuscarCalles.Parameters do begin
            Refresh;
            ParamByName('@Cadena').value := Aux_Text;
            ParamByName('@Region').value := self.CodigoRegion;
            ParamByName('@Comuna').value := self.CodigoComuna;
            spBuscarCalles.open;
            spBuscarCalles.First;
        end;
        //Parametros del SP: Pais, Region, Comuna, Numero Calle, Descripcion...
		While not spBuscarCalles.Eof do begin
			Lista.Additem(Trim(spBuscarCalles.FieldByName('Calle').AsString), lista.GrabarCodigoCalleComuna(spBuscarCalles.FieldByName('CodigoCalle').AsInteger,spBuscarCalles.FieldByName('CodigoComuna').AsInteger));
			spBuscarCalles.Next;
		end;
	finally
		spBuscarCalles.Close;
        Text := aux_Text;
        Desplegar();
	end;
end;

procedure TComboBuscarCalle.SetCodigoComuna(const Value: AnsiString);
begin
  FCodigoComuna := Value;
  LevantarCalle;
end;

procedure TComboBuscarCalle.SetCodigoRegion(const Value: AnsiString);
begin
  FCodigoRegion := Value;
  LevantarCalle;
end;

procedure TComboBuscarCalle.ProcesarTecla(Sender: TObject; Key: Word);
var
    AuxCodigoCalle: Integer;
begin
    if key = VK_TAB then begin
        FCodigoCalle:=lista.CodigoCalle;
        FCodigoComuna:=Lista.CodigoComuna;
       Desplegar(false);
      if not(vacio) then
            text:=Trim(QueryGetValue(Conn,
                FORMAT('SELECT DBO.ObtenerDescripcionCalle (''%s'', ''%s'', ''%s'', %d) ', [self.CodigoPais,self.CodigoRegion,self.CodigoComuna, self.CodigoCalle])));
        IralProximo;
    end;

    if key = VK_ESCAPE then begin
        FCodigoComuna := '';
        FCodigoCalle:=-1;
        Desplegar(false);
        self.SetFocus;
        self.SelStart:=length(Trim(self.text));
    end;

    if key = VK_RETURN then begin
        AuxCodigoCalle := lista.CodigoCalle;
        FCodigoCalle := AuxCodigoCalle;
        FCodigoComuna:=Lista.CodigoComuna;
        Desplegar(false);
        if not(vacio) then
            text:=Trim(QueryGetValue(Conn,
                FORMAT('SELECT DBO.ObtenerDescripcionCalle (''%s'', ''%s'', ''%s'', %d) ', [self.CodigoPais,self.CodigoRegion,self.CodigoComuna, self.CodigoCalle])));
        self.SetFocus;
        self.SelStart:=length(Trim(self.text));
        FCodigoCalle := AuxCodigoCalle;

    end;

    if key = 65535 then begin // eveto de salida de la lista
//        if not(self.Focused) then begin
            AuxCodigoCalle:=lista.CodigoCalle;
            FCodigoCalle := AuxCodigoCalle;
            FCodigoComuna:=Lista.CodigoComuna;
            Desplegar(false);
            if not(vacio) then
                text:=Trim(QueryGetValue(Conn,
                    FORMAT('SELECT DBO.ObtenerDescripcionCalle (''%s'', ''%s'', ''%s'', %d) ', [self.CodigoPais,self.CodigoRegion,self.CodigoComuna, self.CodigoCalle])));
            FCodigoCalle := AuxCodigoCalle;
//        end;
    end;

end;

procedure TComboBuscarCalle.IralProximo;
begin
    if TComboBuscarCalle(self.Parent).FindNextControl(self,true,true,false)<>nil then
        TComboBuscarCalle(self.Parent).FindNextControl(self,true,true,false).SetFocus;
end;

procedure TComboBuscarCalle.KeyPressBuscardorprocedure(Sender: TObject;
  var Key: Char);
begin
    if UpCase(key) in ['A'..'Z', '0'..'9',' ', '.', ',','�','!','"','�','$','%','&','/','(',')','=','?','�','<','>','_','-',':',';']
    then begin
        FCodigoCalle:=-1;
        FCodigoComuna:='';
        Desplegar(false);
    end;
end;

procedure TComboBuscarCalle.SetCodigoCalle(const Value: Integer);
begin
    FCodigoCalle := Value;
    LevantarCalle;
end;

procedure TComboBuscarCalle.LevantarCalle;
var
    aux: Integer;
begin
    if (self.CodigoComuna<>'') and (self.CodigoRegion<>'') and (self.CodigoPais<>'') and (self.CodigoCalle>-1) then begin
        aux := CodigoCalle;
        Text:=Trim(QueryGetValue(Conn,
            FORMAT('SELECT DBO.ObtenerDescripcionCalle (''%s'', ''%s'', ''%s'', %d) ', [self.CodigoPais,self.CodigoRegion,self.CodigoComuna, self.CodigoCalle])));
        fCodigoCalle := aux;
    end;

end;

procedure TComboBuscarCalle.SetCodgoPais(const Value: AnsiString);
begin
  FCodigoPais := Value;
  LevantarCalle;
end;

procedure TComboBuscarCalle.OnSalida(Sender: TObject);
begin
    if not(Lista.Focused) then
        Desplegar(false);
end;

procedure TComboBuscarCalle.IralAnterior;
begin
    if TComboBuscarCalle(self.Parent).FindNextControl(self,false,true,false)<>nil then
        TComboBuscarCalle(self.Parent).FindNextControl(self,false,true,false).SetFocus;
end;

function TComboBuscarCalle.GetDropCount: Integer;
begin
    result := FDropCount;
end;

procedure TComboBuscarCalle.SetDropCount(const Value: Integer);
begin
    if (FDropCount = Value) or (Value < 1) then exit;
    FDropCount := Value;
end;

procedure TComboBuscarCalle.ProcesarSalida(Sender: TObject);
var
    aux:TPoint;
    AuxCodigoCalle: Integer;
begin

    aux.X:=self.Left;
    aux.Y:=self.top;
    aux := (self.parent.ClientToScreen(aux));

    with mouse.CursorPos do begin
        if not(lista.PresionoTecla) and (x<aux.X) or (x>aux.X+self.Width) or (y<aux.Y) or (y>aux.Y+self.Height) then begin
            AuxCodigoCalle := lista.CodigoCalle;
            FCodigoCalle := AuxCodigoCalle;
            FCodigoComuna:=Lista.CodigoComuna;
            if not(vacio) then
                text:=Trim(QueryGetValue(Conn,
                    FORMAT('SELECT DBO.ObtenerDescripcionCalle (''%s'', ''%s'', ''%s'', %d) ', [self.CodigoPais,self.CodigoRegion,self.CodigoComuna, self.CodigoCalle])));
            FCodigoCalle := AuxCodigoCalle;
        end;
    end;
    FForm.Hide;
end;

procedure TComboBuscarCalle.ProcesarEntrada(Sender: TObject);
begin
    lista.PresionoTecla:=False;
end;

function TComboBuscarCalle.GetNobreCalle: String;
begin
    result:=Trim(self.Text);
end;

procedure TComboBuscarCalle.SetNombreCalle(const Value: String);
begin
    Text:=Value;
    self.CodigoCalle:=-1;
    LevantarCalle;
end;

function TComboBuscarCalle.GetCalleNormalizada: Boolean;
begin
    result := self.CodigoCalle > -1;
end;


procedure TComboBuscarCalle.CambioTexto(Sender: TObject);
begin
    Self.CodigoCalle := -1;
end;

procedure TComboBuscarCalle.ForzarBusqueda;
begin
    BuscarCalles;
end;

{ TListBoxVariantEx }


constructor TListBoxVariantEx.Create(AOwner: TComponent);
begin
    inherited;
    name := AOwner.Name+'Tlistboxcariant';
    OnKeyDown := KeyDownList;
    OnExit := OnSalidaLista;
    OnDblClick := OnDoubleClickLista;
    OnEnter := OnEnterLista;
end;

procedure TListBoxVariantEx.KeyDownList(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if Key = VK_SPACE then Key:=VK_RETURN;

    if (key = VK_RETURN) OR (Key = VK_TAB) or (key = VK_ESCAPE) then begin
        PressTecla:=True;
        if Assigned(FKeyUtilizada) then FKeyUtilizada(Self, key);
    end;

end;

procedure TListBoxVariantEx.WMGetDlgCode(var Message: TMessage);
begin
    Message.Result:= DLGC_WANTARROWS or DLGC_WANTTAB or DLGC_WANTALLKEYS;
end;

function TListBoxVariantEx.GrabarCodigoCalleComuna(CodigoCalle,CodigoComuna: Integer): TObject;
begin
    result := TObject(CodigoCalle * 1000 + CodigoComuna);
end;

function TListBoxVariantEx.GetCodigoCalle: Integer;
begin
    if Items.Count=0 then result:=-1
    else result:=(Integer(self.Items.Objects[self.ItemIndex])-strtoint(self.Codigocomuna)) div 1000;
end;

function TListBoxVariantEx.GetCodigocomuna:AnsiString;
begin
    if Items.Count=0 then result:=''
    else result:=StrRight(inttostr(integer(self.Items.Objects[self.ItemIndex])),3);
end;


procedure TListBoxVariantEx.OnSalidaLista(Sender: TObject);
begin
    if Assigned(FKeyUtilizada) and not(PressTecla)  then begin
        PressTecla:=True;
        FKeyUtilizada(Self, 65535);
    end;
end;

procedure TListBoxVariantEx.OnDoubleClickLista(Sender: TObject);
begin
    if Assigned(FKeyUtilizada) then FKeyUtilizada(Self, VK_RETURN);
end;

procedure TListBoxVariantEx.OnEnterLista(Sender: TObject);
begin
    PressTecla:=False;
end;

end.
