unit WFEditor;

interface

Uses
	Windows, Messages, Forms, Classes, Controls, StdCtrls, Scada, WorkflowComp,
	Menus, Dialogs, DesignIntf, DesignEditors, DesignMenus;

Type
	TWorkflowEditor = class(TComponentEditor)
	public
		function  GetVerbCount: Integer; override;
		function  GetVerb(Index: Integer): string; override;
		procedure ExecuteVerb(Index: Integer); override;
	end;

procedure Register;

implementation

procedure Register;
begin
	RegisterComponents('Oriente Poniente', [TWorkflowViewer]);
	RegisterComponents('Oriente Poniente', [TWorkflowDisplay]);
	RegisterNoIcon([TWorkflowTask]);
	RegisterClass(TWorkflowTask);
	RegisterComponentEditor(TWorkflowViewer, TWorkflowEditor);
end;


{ TWorkflowEditor }
procedure TWorkflowEditor.ExecuteVerb(Index: Integer);
Var
	Item: TScadaItem;
begin
	case Index of
		0: Item := TWorkflowTask.Create(Designer.Root);
		else Exit;
	end;
	Item.Name := Designer.UniqueName(Item.ClassName);
	Item.Viewer := TScadaViewer(Component);
end;

function TWorkflowEditor.GetVerb(Index: Integer): string;
begin
	Case Index of
		0: Result := 'New Task';
		else Result := '';
	end;
end;

function TWorkflowEditor.GetVerbCount: Integer;
begin
	Result := 1;
end;



end.
