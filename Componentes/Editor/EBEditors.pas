unit EBEditors;

interface

Uses
	SysUtils, Classes, Controls, Graphics, Forms, StdCtrls, ExtCtrls, ComCtrls,
	Buttons, Util, Dialogs;

Type
	TEBEditor = class(TCustomPanel)
	private
		FBarra: TPanel;
		FEditor: TRichEdit;
		FComboFont: TComboBox;
		FBtnBold: TSpeedButton;
		FBtnItalic: TSpeedButton;
		FComboFontSize: TComboBox;
		FBtnFontColor: TSpeedButton;
		FBtnUnderline: TSpeedButton;
		function GetHTMLText: AnsiString;
		function GetRTFText: AnsiString;
		procedure SetRTFText(const Value: AnsiString);
		procedure DoBoldClick(Sender: TObject);
		procedure DoItalicClick(Sender: TObject);
		procedure DoUnderlineClick(Sender: TObject);
		procedure DoFontColorClick(Sender: TObject);
		procedure UpdateStyleButtons(Sender: TObject);
		function  GetOnChange: TNotifyEvent;
		procedure SetOnChange(const Value: TNotifyEvent);
		procedure FontChanged(Sender: TObject);
		procedure FontSizeChanged(Sender: TObject);
	protected
		procedure CreateHandle; override;
	public
		constructor Create(AOwner: TComponent); override;
		property RTFText: AnsiString read GetRTFText write SetRTFText;
		property HTMLText: AnsiString read GetHTMLText;
	published
		property OnChange: TNotifyEvent read GetOnChange write SetOnChange;
		property Align;
		property Constraints;
		property Anchors;
	end;

function RTFToHtml(Const Text: AnsiString; UseStyleInsteadOfFont: boolean = false): AnsiString;
procedure Register;

implementation

{$R *.res}

procedure Register;
begin
	RegisterComponents('ExerciseBuilder', [TEBEditor]);
end;

function RTFToHtml(Const Text: AnsiString; UseStyleInsteadOfFont: boolean = false): AnsiString;
Var
	i, j: Integer;
	FontColor: TColor;
	s, color, font, fontnumber, FontName, c: AnsiString;
begin
	Result := Text;
	if Pos('{\RTF', UpperCase(Result)) = 0 then Exit;
        if UseStyleInsteadOfFont then begin
            // Font table
            j := pos('{\fonttbl', Result);
            if j <> 0 then begin
                    s := Copy(Result, j, 255);
                    s := Copy(s, 1, Pos('}}', s) + 1);
                    for i := 100 downto 1 do begin
                            font := ParseParamByNumber(s, i + 2, '{');
                            font := ParseParamByNumber(font, 1, '}');
                            if font <> '' then begin
                                    FontNumber := ParseParamByNumber(font, 2, '\');
                                    FontName   := Trim(ParseParamByNumber(font, 2, '\fcharset0'));
                                    FontName   := ParseParamByNumber(FontName, 1, ';');
                                    Result := StringReplace(Result, '\' + FontNumber + ' ', '<span style="font-family: ' + FontName + '">', [rfReplaceAll]);
                                    Result := StringReplace(Result, '\' + FontNumber + '\', '<span style="font-family: ' + FontName + '">\', [rfReplaceAll]);
                            end;
                    end;
                    Result := StringReplace(Result, s, '', []);
            end;
            // Caracteres especiales
            for i := 0 to 255 do begin
                    Result := StringReplace(Result, '\''' + IntToHex(i, 2), Chr(i), [rfReplaceAll, rfIgnoreCase]);
            end;
            // Font Sizes
            for i := 100 downto 0 do begin
                    Result := StringReplace(Result, '\fs' + IntToStr(i) + ' ', '<span style="font-size: ' + IntToStr(i div 2) + 'pt">', [rfReplaceAll, rfIgnoreCase]);
                    Result := StringReplace(Result, '\fs' + IntToStr(i)      , '<span style="font-size: ' + IntToStr(i div 2) + 'pt">', [rfReplaceAll, rfIgnoreCase]);
            end;
            // Color table
            j := pos('{\colortbl', Result);
            if j <> 0 then begin
                    s := Copy(Result, j, 255);
                    s := Copy(s, 1, Pos('}', s));
                    for i := 1 to 1000 do begin
                            Color := ParseParamByNumber(s, i + 1, ';');
                            if Color = '' then break;
                            //
                            FontColor := 0;
                            c := ParseParamByNumber(Color, 2, '\red');
                            c := ParseParamByNumber(c, 1, '\'); c := ParseParamByNumber(c, 1, ';');
                            FontColor := FontColor or (IVal(c) shl 16);
                            c := ParseParamByNumber(Color, 2, '\green');
                            c := ParseParamByNumber(c, 1, '\'); c := ParseParamByNumber(c, 1, ';');
                            FontColor := FontColor or (IVal(c) shl 8);
                            c := ParseParamByNumber(Color, 2, '\blue');
                            c := ParseParamByNumber(c, 1, '\'); c := ParseParamByNumber(c, 1, ';');
                            FontColor := FontColor or (IVal(c) shl 0);
                            //
                            Result := StringReplace(Result, '\cf' + IntToStr(i), '<span style="color: #' +
                              IntToHex(FontColor, 6) + '">', [rfReplaceAll]);
                    end;
                    Result := StringReplace(Result, s, '', []);
            end;
            Result := StringReplace(Result, '\cf0', '<span style="color: #000000">', [rfReplaceAll]);
        end else begin
            // Font table
            j := pos('{\fonttbl', Result);
            if j <> 0 then begin
                    s := Copy(Result, j, 255);
                    s := Copy(s, 1, Pos('}}', s) + 1);
                    for i := 100 downto 1 do begin
                            font := ParseParamByNumber(s, i + 2, '{');
                            font := ParseParamByNumber(font, 1, '}');
                            if font <> '' then begin
                                    FontNumber := ParseParamByNumber(font, 2, '\');
                                    FontName   := Trim(ParseParamByNumber(font, 2, '\fcharset0'));
                                    FontName   := ParseParamByNumber(FontName, 1, ';');
                                    Result := StringReplace(Result, '\' + FontNumber + ' ', '<font face="' + FontName + '">', [rfReplaceAll]);
                                    Result := StringReplace(Result, '\' + FontNumber + '\', '<font face="' + FontName + '">\', [rfReplaceAll]);
                            end;
                    end;
                    Result := StringReplace(Result, s, '', []);
            end;
            // Caracteres especiales
            for i := 0 to 255 do begin
                    Result := StringReplace(Result, '\''' + IntToHex(i, 2), Chr(i), [rfReplaceAll, rfIgnoreCase]);
            end;
            // Font Sizes
            for i := 100 downto 0 do begin
                    Result := StringReplace(Result, '\fs' + IntToStr(i) + ' ', '<font size="' + IntToStr(Round(i / 8)) + '">', [rfReplaceAll, rfIgnoreCase]);
                    Result := StringReplace(Result, '\fs' + IntToStr(i)      , '<font size="' + IntToStr(Round(i / 8)) + '">', [rfReplaceAll, rfIgnoreCase]);
            end;
            // Color table
            j := pos('{\colortbl', Result);
            if j <> 0 then begin
                    s := Copy(Result, j, 255);
                    s := Copy(s, 1, Pos('}', s));
                    for i := 1 to 1000 do begin
                            Color := ParseParamByNumber(s, i + 1, ';');
                            if Color = '' then break;
                            //
                            FontColor := 0;
                            c := ParseParamByNumber(Color, 2, '\red');
                            c := ParseParamByNumber(c, 1, '\'); c := ParseParamByNumber(c, 1, ';');
                            FontColor := FontColor or (IVal(c) shl 16);
                            c := ParseParamByNumber(Color, 2, '\green');
                            c := ParseParamByNumber(c, 1, '\'); c := ParseParamByNumber(c, 1, ';');
                            FontColor := FontColor or (IVal(c) shl 8);
                            c := ParseParamByNumber(Color, 2, '\blue');
                            c := ParseParamByNumber(c, 1, '\'); c := ParseParamByNumber(c, 1, ';');
                            FontColor := FontColor or (IVal(c) shl 0);
                            //
                            Result := StringReplace(Result, '\cf' + IntToStr(i), '<font color=#' +
                              IntToHex(FontColor, 6) + '>', [rfReplaceAll]);
                    end;
                    Result := StringReplace(Result, s, '', []);
            end;
            Result := StringReplace(Result, '\cf0', '<font color=#000000>', [rfReplaceAll]);
        end;
	// Quitamos la primera linea
	i := Pos(CRLF, Result);
	Result := Copy(Result, i + 2, Length(Result));
	// Quitamos la llave final
	Result := TrimRight(Result);
	SetLength(Result, Length(Result) - 1);
	Result := TrimRight(Result);
	// Si termina en "\par", lo quitamos
	if UpperCase(StrRight(Result, 4)) = '\PAR' then SetLength(Result, Length(Result) - 4);
	// Views
	for i := 10 downto 0 do begin
		Result := StringReplace(Result, '\viewkind' + IntToStr(i) + ' ', '', [rfReplaceAll, rfIgnoreCase]);
		Result := StringReplace(Result, '\viewkind' + IntToStr(i),       '', [rfReplaceAll, rfIgnoreCase]);
	end;
	// Fonts
	for i := 10 downto 0 do begin
		Result := StringReplace(Result, '\f' + IntToStr(i) + ' ', '', [rfReplaceAll, rfIgnoreCase]);
		Result := StringReplace(Result, '\f' + IntToStr(i),       '', [rfReplaceAll, rfIgnoreCase]);
	end;
	// UC
	for i := 10 downto 0 do begin
		Result := StringReplace(Result, '\uc' + IntToStr(i) + ' ', '', [rfReplaceAll, rfIgnoreCase]);
		Result := StringReplace(Result, '\uc' + IntToStr(i),       '', [rfReplaceAll, rfIgnoreCase]);
	end;
	// {\colortbl ;\red255\green0\blue0;}
	// Pard
	Result := StringReplace(Result, '\pard ', '', [rfReplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\pard', '', [rfReplaceAll, rfIgnoreCase]);
	// Saltos de p�rrafo
	Result := StringReplace(Result, CRLF + '\par ', '\par ' + CRLF, [rfreplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\par ', '<br>', [rfreplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\par',  '<br>', [rfreplaceAll, rfIgnoreCase]);
	// Bold
	Result := StringReplace(Result, '\b0 ', '</b>', [rfreplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\b0', '</b>', [rfreplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\b ', '<b>', [rfreplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\b', '<b>', [rfreplaceAll, rfIgnoreCase]);
	// Italic
	Result := StringReplace(Result, '\i0 ', '</i>', [rfreplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\i0', '</i>', [rfreplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\i ', '<i>', [rfreplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\i', '<i>', [rfreplaceAll, rfIgnoreCase]);
	// Underline
	Result := StringReplace(Result, '\ulnone ', '</u>', [rfreplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\ulnone', '</u>', [rfreplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\ul ', '<u>', [rfreplaceAll, rfIgnoreCase]);
	Result := StringReplace(Result, '\ul', '<u>', [rfreplaceAll, rfIgnoreCase]);
	// Removemos los "lang"
	Repeat
		i := Pos('\LANG', UpperCase(Result));
		if i = 0 then Break;
		j := i + 5;
		while Result[j] in ['0'..'9'] do inc(j);
		Delete(Result, i, j - i);
		if Result[i] = ' ' then Delete(Result, i, 1);
	Until False;
	// Espacios
	Result := StringReplace(Result, '  ', ' &nbsp', [rfReplaceAll]);
	// Cierres
	Result := Trim(Result) + '</b></i></u>' + CRLF;
end;


{ TEBEditor }

constructor TEBEditor.Create(AOwner: TComponent);
begin
	inherited;
	BorderStyle := bsSingle;
	Color := clWindow;
	//
	FBarra := TPanel.Create(Self);
	FBarra.Parent := Self;
	FBarra.Align := alTop;
	FBarra.Height := 25;
	FBarra.BevelInner := bvNone;
	FBarra.BevelOuter := bvRaised;
	//
	FEditor := TRichEdit.Create(Self);
	FEditor.Parent := Self;
	FEditor.BorderStyle := bsNone;
	FEditor.Align := alClient;
	FEditor.OnSelectionChange := UpdateStyleButtons;
	FEditor.DefAttributes.Name := 'Courier New';
	//
	FBtnBold := TSpeedButton.Create(Self);
	FBtnBold.Parent := FBarra;
	FBtnBold.Flat := True;
	FBtnBold.Left := 2;
	FBtnBold.Top  := 1;
	FBtnBold.Font.Name := 'Courier New';
	FBtnBold.Font.Size := 12;
	FBtnBold.Font.Style := [fsBold];
	FBtnBold.Caption := 'B';
	FBtnBold.GroupIndex := 1;
	FBtnBold.AllowAllUp := True;
	FBtnBold.OnClick := DoBoldClick;
	//
	FBtnItalic := TSpeedButton.Create(Self);
	FBtnItalic.Parent := FBarra;
	FBtnItalic.Flat := True;
	FBtnItalic.Left := 25;
	FBtnItalic.Top  := 1;
	FBtnItalic.Font.Name := 'Courier New';
	FBtnItalic.Font.Size := 12;
	FBtnItalic.Font.Style := [fsItalic];
	FBtnItalic.Caption := 'I';
	FBtnItalic.GroupIndex := 2;
	FBtnItalic.AllowAllUp := True;
	FBtnItalic.OnClick := DoItalicClick;
	//
	FBtnUnderline := TSpeedButton.Create(Self);
	FBtnUnderline.Parent := FBarra;
	FBtnUnderline.Flat := True;
	FBtnUnderline.Left := 48;
	FBtnUnderline.Top  := 1;
	FBtnUnderline.Font.Name := 'Courier New';
	FBtnUnderline.Font.Size := 10;
	FBtnUnderline.Font.Style := [fsUnderline];
	FBtnUnderline.Caption := 'U';
	FBtnUnderline.GroupIndex := 3;
	FBtnUnderline.AllowAllUp := True;
	FBtnUnderline.OnClick := DoUnderlineClick;
	//
	FComboFont := TComboBox.Create(Self);
	FComboFont.Parent := FBarra;
	FComboFont.Left := FBtnUnderline.BoundsRect.Right + 8;
	FComboFont.Width := 150;
	FComboFont.Top := 2;
	FComboFont.Style := csDropDownList;
	FComboFont.TabStop := False;
	//
	FComboFontSize := TComboBox.Create(Self);
	FComboFontSize.Parent := FBarra;
	FComboFontSize.Left := FComboFont.BoundsRect.Right + 8;
	FComboFontSize.Width := 60;
	FComboFontSize.Top := 2;
	FComboFontSize.Style := csDropDownList;
	FComboFontSize.TabStop := False;
	//
	FBtnFontColor := TSpeedButton.Create(Self);
	FBtnFontColor.Parent := FBarra;
	FBtnFontColor.Flat := True;
	FBtnFontColor.Left := FComboFontSize.BoundsRect.Right + 3;
	FBtnFontColor.Top  := 2;
	FBtnFontColor.Glyph.LoadFromResourceName(HInstance, 'EBEDITOR_FONTCOLOR');
	FBtnFontColor.Glyph.TransparentColor := clOlive;
	FBtnFontColor.OnClick := DoFontColorClick;
end;

procedure TEBEditor.CreateHandle;
Var
	i: Integer;
begin
	inherited;
	//
	FEditor.HideSelection := False;
	//
	for i := 0 to Screen.Fonts.Count - 1 do
	  FComboFont.Items.Add(Screen.Fonts[i]);
	FComboFont.ItemIndex := FComboFont.Items.IndexOf(FEditor.Font.Name);
	FComboFont.OnChange := FontChanged;
	//
	FComboFontSize.Items.Add('7');
	FComboFontSize.Items.Add('8');
	FComboFontSize.Items.Add('9');
	FComboFontSize.Items.Add('10');
	FComboFontSize.Items.Add('12');
	FComboFontSize.Items.Add('14');
	FComboFontSize.Items.Add('18');
	FComboFontSize.Items.Add('24');
	FComboFontSize.Items.Add('36');
	FComboFontSize.ItemIndex := FComboFontSize.Items.IndexOf(IntToStr(FEditor.Font.Size));
	FComboFontSize.OnChange := FontChanged;
	//
end;

procedure TEBEditor.DoBoldClick(Sender: TObject);
begin
	if FBtnBold.Down then FEditor.SelAttributes.Style := FEditor.SelAttributes.Style + [fsBold]
	  else FEditor.SelAttributes.Style := FEditor.SelAttributes.Style - [fsBold];
end;

procedure TEBEditor.DoFontColorClick(Sender: TObject);
Var
	Dialog: TColorDialog;
begin
	Dialog := TColorDialog.Create(nil);
	if Dialog.Execute then FEditor.SelAttributes.Color := Dialog.Color;
	Dialog.Free;
end;

procedure TEBEditor.DoItalicClick(Sender: TObject);
begin
	if FBtnItalic.Down then FEditor.SelAttributes.Style := FEditor.SelAttributes.Style + [fsItalic]
	  else FEditor.SelAttributes.Style := FEditor.SelAttributes.Style - [fsItalic];
end;

procedure TEBEditor.DoUnderlineClick(Sender: TObject);
begin
	if FBtnUnderline.Down then FEditor.SelAttributes.Style := FEditor.SelAttributes.Style + [fsUnderline]
	  else FEditor.SelAttributes.Style := FEditor.SelAttributes.Style - [fsUnderline];
end;

procedure TEBEditor.FontChanged(Sender: TObject);
begin
	FEditor.SelAttributes.Name := FComboFont.Text;
end;

procedure TEBEditor.FontSizeChanged(Sender: TObject);
begin
	FEditor.SelAttributes.Size := IVal(FComboFontSize.Text);
end;

function TEBEditor.GetHTMLText: AnsiString;
begin
	Result := RTFToHTML(GetRTFText);
end;

function TEBEditor.GetOnChange: TNotifyEvent;
begin
	Result := FEditor.OnChange;
end;

function TEBEditor.GetRTFText: AnsiString;
Var
	FileName: AnsiString;
begin
	FileName := GetTempDir + TempFile + '.rtf';
	FEditor.Lines.SaveToFile(FileName);
	Result := FileToString(FileName);
	DeleteFile(FileName);
end;

procedure TEBEditor.SetOnChange(const Value: TNotifyEvent);
begin
	FEditor.OnChange := Value;
end;

procedure TEBEditor.SetRTFText(const Value: AnsiString);
Var
	FileName: AnsiString;
begin
	FileName := GetTempDir + TempFile + '.rtf';
	StringToFile(Value, FileName);
	FEditor.Lines.LoadFromFile(FileName);
	DeleteFile(FileName);
	FEditor.SelectAll;
	FEditor.SelAttributes.Name := 'Courier New';
	FEditor.SelLength := 0;
end;

procedure TEBEditor.UpdateStyleButtons(Sender: TObject);
begin
	FComboFont.OnChange := nil;
	FComboFontSize.OnChange := nil;
	FBtnBold.Down := fsBold in FEditor.SelAttributes.Style;
	FBtnItalic.Down := fsItalic in FEditor.SelAttributes.Style;
	FBtnUnderline.Down := fsUnderline in FEditor.SelAttributes.Style;
	FComboFont.ItemIndex := FComboFont.Items.IndexOf(FEditor.SelAttributes.Name);
	FComboFontSize.ItemIndex := FComboFontSize.Items.IndexOf(IntToStr(FEditor.SelAttributes.Size));
	FComboFont.OnChange := FontChanged;
	FComboFontSize.OnChange := FontSizeChanged;
end;

end.
