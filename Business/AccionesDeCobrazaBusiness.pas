{-----------------------------------------------------------------------------
  Author:    aunanue
  Date:      09-03-2017
  Description: Clase con las reglas de negocio para las Acciones de Cobranza.
-----------------------------------------------------------------------------}
unit AccionesDeCobrazaBusiness;

interface

uses ADODB, SysUtils, VariantComboBox, Windows;

type TAccionDeCobranza = record
        Codigo: integer;
        Descripcion: string;
    end;

type TAccionesDeCobranzaBusiness = class
    private
        FCaption: String;
        FAccionesDeCobranza: array of TAccionDeCobranza;
    public
        constructor Create(caption: string);
        destructor Destroy; override;
        procedure ObtenerTodosLosTiposDeAccionesDeCobranza;
        procedure LlenarTiposDeAccionesDeCobranzaParaTipoDeCobranza(TipoDeCobranza: integer;
                                                    cbAccionesDeCobranzaOut: TVariantComboBox;
                                                    AgregarItemSeleccionar: boolean);
        function EsAccionParaEnvioPostalOEmail(TipoDeAccionDeCobranza: integer) : Boolean;
        function EsAccionParaLlamadoTelefonico(TipoDeAccionDeCobranza: integer): Boolean;
    end;

implementation

uses DMConnection, RStrings, UtilProc;

resourcestring
    SELECCIONAR = '(Seleccionar)';

{-----------------------------------------------------------------------------
  Function Name: Create
  Author:    aunanue
  Date:      09-03-2017
  Description: Inicializo cosas como por ejemplo el t�tulo que se muestra en caso de errores.
-----------------------------------------------------------------------------}
constructor TAccionesDeCobranzaBusiness.Create(caption: string);
begin
    FCaption := Caption;
end;

{-----------------------------------------------------------------------------
  Function Name: Destroy
  Author:    aunanue
  Date:      09-03-2017
  Description: Libero memoria.
-----------------------------------------------------------------------------}
destructor TAccionesDeCobranzaBusiness.Destroy;
begin
    //libero todos los items del arreglo
    SetLength(FAccionesDeCobranza, 0);
    inherited;
end;

{-----------------------------------------------------------------------------
  Function Name: EsAccionParaEnvioPostalOEmail
  Author:    aunanue
  Date:      09-03-2017
  Description: Verifica si el tipo de acci�n pasado es para llamado env�o postal o email.
-----------------------------------------------------------------------------}
function TAccionesDeCobranzaBusiness.EsAccionParaEnvioPostalOEmail(
  TipoDeAccionDeCobranza: integer): Boolean;
begin
    Result := (TipoDeAccionDeCobranza = 1) or //Aviso por e-mail
              (TipoDeAccionDeCobranza = 2) or //Aviso postal
              (TipoDeAccionDeCobranza = 5)//Aviso por carta certificada
end;

{-----------------------------------------------------------------------------
  Function Name: EsAccionParaLlamadoTelefonico
  Author:    aunanue
  Date:      09-03-2017
  Description: Verifica si el tipo de acci�n pasado es para llamado telef�nico.
-----------------------------------------------------------------------------}
function TAccionesDeCobranzaBusiness.EsAccionParaLlamadoTelefonico(
  TipoDeAccionDeCobranza: integer): Boolean;
begin
    Result := (TipoDeAccionDeCobranza = 3)//Aviso telef�nico
end;

{-----------------------------------------------------------------------------
  Function Name: LlenarTiposDeAccionesDeCobranzaParaTipoDeCobranza
  Author:    aunanue
  Date:      09-03-2017
  Description: Llena un combo de Tipos de acciones de cobranza, para el Tipo de cobranza
               pasado como par�metro.
-----------------------------------------------------------------------------}
procedure TAccionesDeCobranzaBusiness.LlenarTiposDeAccionesDeCobranzaParaTipoDeCobranza(
  TipoDeCobranza: integer; cbAccionesDeCobranzaOut: TVariantComboBox;
  AgregarItemSeleccionar: boolean);
var
    i: integer;
begin
    cbAccionesDeCobranzaOut.Clear;
    if AgregarItemSeleccionar then
        cbAccionesDeCobranzaOut.Items.Add(SELECCIONAR, -1);
    //
    if Assigned(FAccionesDeCobranza) and (Length(FAccionesDeCobranza) > 0) and (TipoDeCobranza > 0) then begin
        for i := 0 to Length(FAccionesDeCobranza) - 1 do begin
            if TipoDeCobranza = 1 then begin
                //para 1 no filtro ninguna acci�n
                cbAccionesDeCobranzaOut.Items.Add(FAccionesDeCobranza[i].Descripcion, FAccionesDeCobranza[i].Codigo);
            end else begin
                //para el resto solamente agrego acciones mayores al c�digo 5
                if FAccionesDeCobranza[i].Codigo > 5 then
                    cbAccionesDeCobranzaOut.Items.Add(FAccionesDeCobranza[i].Descripcion, FAccionesDeCobranza[i].Codigo);
            end;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerTodosLosTiposDeAccionesDeCobranza
  Author:    aunanue
  Date:      09-03-2017
  Description: Obtiene todos los tipos de acciones de cobranza cargados en el sistema
-----------------------------------------------------------------------------}
procedure TAccionesDeCobranzaBusiness.ObtenerTodosLosTiposDeAccionesDeCobranza;
var
    spObtenerTiposDeAccionesDeCobranza: TAdoStoredProc;
    i: integer;
begin
    spObtenerTiposDeAccionesDeCobranza := TAdoStoredProc.Create(nil);
    try
        try
            spObtenerTiposDeAccionesDeCobranza.Connection := DMConnections.BaseCAC;
            spObtenerTiposDeAccionesDeCobranza.ProcedureName := 'TiposDeAccionesDeCobranza_Listar';
            spObtenerTiposDeAccionesDeCobranza.Open;
            if spObtenerTiposDeAccionesDeCobranza.RecordCount > 0 then begin
                spObtenerTiposDeAccionesDeCobranza.First;
                for i := 1 to spObtenerTiposDeAccionesDeCobranza.RecordCount do begin
                    SetLength(FAccionesDeCobranza, spObtenerTiposDeAccionesDeCobranza.RecordCount);
                    FAccionesDeCobranza[i - 1].Codigo := spObtenerTiposDeAccionesDeCobranza.FieldByName('CodigoTipoDeAccionDeCobranza').AsInteger;
                    FAccionesDeCobranza[i - 1].Descripcion := spObtenerTiposDeAccionesDeCobranza.FieldByName('Descripcion').AsString;
                    spObtenerTiposDeAccionesDeCobranza.Next;
                end;
            end;
            spObtenerTiposDeAccionesDeCobranza.Close;
        except on e:Exception do begin
                SetLength(FAccionesDeCobranza, 0);
                MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[FCaption]), e.message, Format(MSG_CAPTION_ELIMINAR,[FCaption]), MB_ICONSTOP);
            end;
        end;
    finally
        FreeAndNil(spObtenerTiposDeAccionesDeCobranza);
    end;
end;

end.
