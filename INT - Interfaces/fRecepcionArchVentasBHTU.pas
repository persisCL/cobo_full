{***********************************************************************************
File Name       :   fRecepcionArchVentasBHTU.pas
Author          :   ndonadio
Date Created    :   05/10/2005
Language        :   ES-AR
Description     :   Procesa el archivo de rendicion de ventas de BHTU enviado por Servipag

Revision 1
Author          :   dcepeda
Date Created    :   24/03/2010
Description     :   SS-871, El objetivo es desplegar en el reporte los siguientes conceptos:
                    Suma Monto Archivo y Diferencia de Montos.
                    La variable FSumaMontoTotalArchivo almacenar� la suma de los montos del
                    archivo de carga, esta variable ser� registrada en Base de Datos para su
                    posterior despliegue en el reporte.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

 Firma        : SS_1313_NDR_20150615
 Descripcion  : Emitir Reporte Resumen DayPass al finalizar la importacion
                Se debe mostrar la patente, la cantidad de daypass comprados en el a�o en curso
                (si este valor excede de un parametro), siempre y cuando la patente tenga al menos un
                DayPAss comprado en los ultimos X dias (segun parametro)
*************************************************************************************}
unit fRecepcionArchVentasBHTU;

interface

uses
    //Recepcion de ventas de BHTU
    DMConnection,                //Base de Datos OP_CAC
    UtilDB,
    PeaProcs,
    PeaTypes,
    ComunesInterfaces,
    Util,
    UtilProc,
    StrUtils,
    DateUtils,
    ConstParametrosGenerales,    //Parametros Generales
    fReporteRecepcionVentasBHTU, //Reporte finalizacion
    frmRptDayPassCopec,
    //Otros
    Windows, Messages, SysUtils, Variants, Classes,  Graphics,  Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB, ppDBPipe,
  ppParameter, ppModule, raCodMod, ppBands, ppCtrls, ppPrnabl, ppClass, ppCache,
  ppComm, ppRelatv, ppProd, ppReport, UtilRB;

type
    TLateDayPassRecord  = record
    Patente             : AnsiString;
    Categoria           : byte;
    FechaHabilitacion   : TDateTime;
    Monto               : integer;
    FechaVenta          : TDateTime;
    FechaRendicion      : TDateTime;
    Identificador       : AnsiString;
  end;

type
  TfrmRecepcionArchVentasBHTU = class(TForm)
    btnProcesar: TButton;
    btnSalir: TButton;
    btnCancelar: TButton;
    pnlAvance: TPanel;
    Label1: TLabel;
    lblDetalle: TLabel;
    lbl: TLabel;
    pbProgreso: TProgressBar;
    txtOrigen: TPickEdit;
    Label2: TLabel;
    Bevel1: TBevel;
    OpenDialog: TOpenDialog;
    spAgregarDaypassTardio: TADOStoredProc;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    spRegistrarArchivoRecibidoVentaBHTU: TADOStoredProc;
    spActualizarLogOperacionesRegistrosProcesados   : TADOStoredProc;
    RBIResumen: TRBInterface;
    ppReportResumenDayPass: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    lbl_usuario: TppLabel;
    ppLine1: TppLine;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel10: TppLabel;
    ppLine6: TppLine;
    ppImage2: TppImage;
    ppFechaProcesamiento: TppLabel;
    pptFechaProcesamiento: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBTCantidad: TppDBText;
    ppDBTFechaUso: TppDBText;
    ppDBTFechaVenta: TppDBText;
    ppDBTPatente: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppLine4: TppLine;
    raCodeModule2: TraCodeModule;
    ppParameterList1: TppParameterList;
    ppDBReporteResumenDayPass: TppDBPipeline;
    dsResumenDayPass: TDataSource;
    spResumenDayPass: TADOStoredProc;  //REV.1:
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ImgAyudaClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure txtOrigenButtonClick(Sender: TObject);
    procedure RBIResumenExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FErrorLog,
    FAdvertenciasLog : TStringList; // SS 909
    FPasesDiariosTardios : TStringList;
    FErrors : Boolean;
    FCancelar : Boolean;
    FProcesando : Boolean;
    FBHTU_Directorio_Procesados : AnsiString;
    FBHTU_Directorio_Entrada : AnsiString;
    FCodigoOperacion : integer;
    FCantidadErrores : integer;
    FCantErroresW : integer;
    //FCantErroresE             : integer; //REV.1: Comentado porque nunca se ha usado.
    FCantidadBHTU : integer;
    FFechaArchivo : TDateTime;
	//REV.1: Nuevas variables
	FSumaMontoTotalArchivo      : Int64;
    FCantidadFallaParseo        : integer;
    //Fin REV.1
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure HabilitarBotones;
    procedure EliminarLineasEnBlanco( var PaseDiario : TStringList);
    function  VerificarCantidadDeRegistros(FPasesDiarios : TStringList; descError: AnsiString) : boolean;
    function  AnalizarArchivoPasesDiariosTardios(var descError: AnsiString): boolean;
    function  RegistrarOperacion(CodigoModulo: integer; sFileName: string): boolean;
    function  CargarPasesDiariosTardios: boolean;
    function  ObtenerCantidadErroresInterfaz(CodigoOperacion: integer; var Cantidad: integer): boolean;
    function  ActualizarLog(CantidadErrores: integer): boolean;
    function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase: integer): boolean;
    function  ParseLateDayPassLine(sline: string; var LateDayPassRecord: TLateDayPassRecord; var sParseError: string): boolean;
    procedure AddParsingErrors(CodigoOperacion: Integer);
    procedure RegistrarArchivo;
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): Boolean;
  end;

Const
    FILE_NAME_PREFIX        = 'MC';
    FILE_NAME_DATEFORMAT    = 'YYMMDD';
    FILE_EXTENSION          = '.OK';

var
  frmRecepcionArchVentasBHTU: TfrmRecepcionArchVentasBHTU;

implementation

{$R *.dfm}

{******************************************************************************
Function Name   :   Inicializar
Author          :   ndonadio
Date Created    :   30/09/2005
Description     :   Inicializa el form
*******************************************************************************}
function TfrmRecepcionArchVentasBHTU.Inicializar(Titulo: AnsiString): boolean;
resourcestring
  	MSG_INIT_ERROR                      = 'No se pudo inicializar la interfaz de Recepcion de Archivod e Ventas de BHTU';
    ERROR_P_GRAL                        = 'Error al cargar el Par�metro General "%s".' ;
Const
    DIR_ORIGEN_VENTAS_BHTU              = 'DIR_ORIGEN_VENTAS_BHTU';
    DIR_DESTINO_VENTAS_BHTU_PROCESADOS  = 'DIR_DESTINO_VENTAS_BHTU_PROCESADOS';
begin
    CenterForm(Self);
    Result              := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    Caption             := Titulo;
    FErrors             := False;
    FProcesando         := False;
    lblDetalle.Caption  := '';
  	// Obtiene el Directorio de Entrada de la Interfase
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_ORIGEN_VENTAS_BHTU, FBHTU_Directorio_Entrada)   then begin
        MsgBoxErr(MSG_INIT_ERROR, Format( ERROR_P_GRAL, [DIR_ORIGEN_VENTAS_BHTU]), Caption, MB_ICONSTOP);
        Exit;
    end;
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DESTINO_VENTAS_BHTU_PROCESADOS, FBHTU_Directorio_Procesados) then begin
        MsgBoxErr(MSG_INIT_ERROR, Format( ERROR_P_GRAL, [DIR_DESTINO_VENTAS_BHTU_PROCESADOS]), Caption, MB_ICONSTOP);
        Exit;
    end;
    HabilitarBotones;
    Result := True;
end;

{******************************************************************************
Function Name   :   ImgAyudaClick
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Muestra la ayuda
*******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_AYUDA   = ' ' + CRLF +
                  'El Archivo de Ventas de BHTU es ' + CRLF +
                  'utilizado por SERVIPAG para informar '+ CRLF +
                  'los BHTU que han sido vendidos' + CRLF +
                  'al ESTABLECIMIENTO' + CRLF +
                  ' ' + CRLF +
                  'Nombre del Archivo: MCYYMMDD.OK' + CRLF +
                  ' ';
begin
    //si esta procesando sale
    if PnlAvance.Visible = True then Exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_AYUDA, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************************************************************
Function Name   :   ImgAyudaMouseMove
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Cambia el icono al pasar por arriba de la imagen de la ayuda
*******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{******************************************************************************
Function Name   :   txtOrigenButtonClick
Author          :   ndonadio
Date Created    :   30/09/2005
Description     :   Abre y valida nombre del archivo de BHTU Vendidos
*******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.txtOrigenButtonClick(Sender: TObject);
resourcestring
    MSG_ERROR                       = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST   = 'El archivo %s no existe';
    DAYPASS_FILES_FILTER            = 'Pases Diarios Tard�os (BHTU) |' + FILE_NAME_PREFIX + '*' + FILE_EXTENSION;
begin
    OpenDialog.InitialDir   := FBHTU_Directorio_Entrada;
  	OpenDialog.FileName     := '';
    OpenDialog.Filter       := DAYPASS_FILES_FILTER;
    if OpenDialog.Execute then begin
        txtOrigen.text:=UpperCase( OpenDialog.filename );
        if not FileExists( txtOrigen.text ) then begin
            MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtOrigen.Text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;
    end;
    btnProcesar.Enabled := FileExists(txtOrigen.Text);
end;

{******************************************************************************
Function Name   :   HabilitarBotones
Author          :   ndonadio
Date Created    :   30/09/2005
Description     :   Habilita O No Los Botones Y Otros Controles Dependiendo De
                    El Valor De La Variable Fprocesando
*******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.HabilitarBotones;
begin
    lblDetalle.Caption      := '';
    pbProgreso.Position     := pbProgreso.Min;
    FErrors                 := False;
    FCancelar               := False;
    if FProcesando then begin
        btnCancelar.Enabled := True;
        btnProcesar.Enabled := False;
        btnSalir.Enabled    := False;
        txtOrigen.Enabled   := False;
        pnlAvance.Visible   := True;
    end
    else begin
        btnCancelar.Enabled := False;
        txtOrigen.Text      := ExtractFilePath(txtOrigen.Text);
        btnProcesar.Enabled := FileExists(txtOrigen.Text);
        txtOrigen.Enabled   := True;
        btnSalir.Enabled    := True;
        pnlAvance.Visible   := False;
    end;
end;

{******************************************************************************
Function Name   :   EliminarLineasEnBlanco
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Elimina las lineas en blanco
*******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.EliminarLineasEnBlanco( var PaseDiario : TStringList);
var
    i : integer;
begin
    i := PaseDiario.Count -1;
    while (PaseDiario[i] = '') do begin
        PaseDiario.Delete(i);
        Dec(i);
    end;
end;

{******************************************************************************
Function Name   :   VerificarCantidadDeRegistros
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Verifica la cantidad de registros. Hoy no se usa...
*******************************************************************************}
function TfrmRecepcionArchVentasBHTU.VerificarCantidadDeRegistros(FPasesDiarios : TStringList; descError : AnsiString) : boolean;
resourcestring
    MSG_INVALID_RECORD_NUMBER       = 'El n�mero de registros del %s ' +CRLF+ 'no coincide con el registro de control';
    MSG_VERIFYING_CONTROL_REGISTER  = 'Verficando registro de control del ';
var
    nLen    : integer;
    Archivo : AnsiString;
begin
    Result          := True;
    FCantidadBHTU   := FPasesDiarios.Count;
    Archivo         := ExtractFileName(txtOrigen.Text);
    Archivo         := ReplaceStr(Archivo, FILE_NAME_PREFIX, '');
    Archivo         := ReplaceStr(Archivo, FILE_EXTENSION, '');
    Archivo         := '20' + TRIM(Archivo);
    try
        FFechaArchivo := EncodeDate(	StrToInt(Copy(Archivo, 1, 4)), StrToInt(Copy(Archivo, 5, 2)), StrToInt(Copy(Archivo, 7, 2)));
    except
        on exception do begin
            FFechaArchivo := EncodeDate(1980,1,1)
        end;
    end;
    Exit;
  	nLen                := FPasesDiarios.Count;
  	lblDetalle.Caption  := MSG_VERIFYING_CONTROL_REGISTER + Archivo;
  	if (StrToIntDef(Copy(FPasesDiarios[nLen-1], 1, 10),0) <> nLen) then begin
        MsgBox(Format(MSG_INVALID_RECORD_NUMBER, [Archivo]), Caption, MB_ICONERROR);
        Result := False;
    end;
end;

{******************************************************************************
Function Name   :   AnalizarArchivoPasesDiariosTardios
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Analiza el Archivo de Pases Diarios Tard�os
*******************************************************************************}
function TfrmRecepcionArchVentasBHTU.AnalizarArchivoPasesDiariosTardios(var descError: AnsiString): boolean;
resourcestring
    MSG_ANALIZING_LATE_DAYPASS_FILE     = 'Analizando Archivo Pases Diarios Tard�os - Linea : %d';
    MSG_LATE_DAYPASS_FILE_HAS_ERRORS    = 'Error de Parseo: (L�nea : %d): ';
var
    nNroLineaScript : Integer;
    nLineasScript   : Integer;
    FDayPassRecord  : TLateDayPassRecord;
    sParseError     : String;
    sErrorMsg       : String;
begin
  	nLineasScript       := FPasesDiariosTardios.Count - 1;
    lblDetalle.Caption  := Format(MSG_ANALIZING_LATE_DAYPASS_FILE, [0]) + ' de ' + IntToStr(nLineasScript);
    pbProgreso.Position := 0;
  	pbProgreso.Max      := FPasesDiariosTardios.Count - 1;
    try
        nNroLineaScript := 0;
        while ( nNroLineaScript < nLineasScript ) and ( not FCancelar ) and (	sErrorMsg = '' ) do begin

            if not ParseLateDayPassLine( FPasesDiariosTardios[nNroLineaScript], FDayPassRecord, sParseError ) then begin
                sErrorMsg := Format( MSG_LATE_DAYPASS_FILE_HAS_ERRORS, [nNroLineaScript+1] ) + sParseError;
                FErrorLog.Add(sErrorMsg);
            end;

            lblDetalle.Caption  := Format(MSG_ANALIZING_LATE_DAYPASS_FILE, [nNroLineaScript]) + ' de ' + IntToStr(nLineasScript);;
            Inc( nNroLineaScript );
            pbProgreso.Position := nNroLineaScript;
            Application.ProcessMessages;
        end;
    finally
      	Result      := ( not FCancelar ) and ( sErrorMsg = '' );
        DescError   := sErrorMsg;
        pbProgreso.Position := 0;
        Application.ProcessMessages;
    end;
end;

{******************************************************************************
Function Name   :   RegistrarOperacion
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Registra la Operaci�n en el Log de Operaciones
*******************************************************************************}
function TfrmRecepcionArchVentasBHTU.RegistrarOperacion(CodigoModulo: integer; sFileName: string) : boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION    = 'No se pudo registrar la operaci�n';
var
    DescError   : string;
begin
    Result  := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, CodigoModulo, sFileName, UsuarioSistema, '', True, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);
    if not Result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, Caption, MB_ICONERROR);
end;

{******************************************************************************
Function Name   :   ParseLateDayPassLine
Author          :   dcepeda         (ndonadio)
Date Created    :   08-Abril-2010   (05/10/2005)
Description     :   Verificar cada l�nea del archivo ingresado de acuerdo a la
                    definici�n de archivo BHTU.
                    Devuelve un Valor True si est� correcto y False si el formato
                    de l�nea archivo no corresponde con el formato definido para
                    reporte BHTU.
*******************************************************************************}
function  TfrmRecepcionArchVentasBHTU.ParseLateDayPassLine(sline : string; var LateDayPassRecord : TLateDayPassRecord; var sParseError : string): boolean;
resourcestring
    MSG_ERROR_PLATE            = 'La patente es inv�lida.';
    MSG_ERROR_CATEGORY         = 'La categoria del BHTU es inv�lida';
    MSG_ERROR_FITTING_OUT_DATE = 'La fecha de habilitaci�n es inv�lida';
    MSG_ERROR_AMMOUNT          = 'El monto es inv�lido.';
  	MSG_ERROR_SALES_DATE       = 'La fecha de venta es inv�lida.';
  	MSG_ERROR_RENDITION_DATE   = 'La fecha de rendici�n es inv�lida.';
    MSG_ERROR_FILE_TYPE        = 'El tipo de archivo a procesar no corresponde con el archivo seleccionado';
    MSG_ERROR_IDENTIFIER       = 'No se encontr� el Identificador BHTU';
begin
  	Result      := False;
    sParseError := '';		//REV.1

    with LateDayPassRecord do begin
        try
        	//Patente
			sParseError         := MSG_ERROR_PLATE;
            Patente             := Trim(Copy( sline, 1, 10));
            if (Patente = '') then Exit;
            //Categoria
            sParseError         := MSG_ERROR_CATEGORY;
            //Categoria         := StrToIntDef(Trim(Copy( sline,  11, 1)), 0); {REV.1: Cambiado}
            Categoria           := StrToInt(Copy( sline,  11, 1));
            //Fecha Habilitacion
            sParseError         := MSG_ERROR_FITTING_OUT_DATE;
            FechaHabilitacion   := EncodeDate(StrToInt(Copy(sline, 12, 4)), StrToInt(Copy(sline, 16, 2)), StrToInt(Copy(sline, 18, 2)));
            //Monto
            sParseError         := MSG_ERROR_AMMOUNT;
            //Monto             := StrToIntDef(Trim(Copy( sline,  20, 10)), 0); {REV.1: Cambiado}
            Monto               := StrToInt(Copy( sline,  20, 10));
            //Fecha de Venta
            sParseError         := MSG_ERROR_SALES_DATE;
            FechaVenta          := EncodeDate(	StrToInt(Copy(sline, 30, 4)), StrToInt(Copy(sline, 34, 2)), StrToInt(Copy(sline, 36, 2)));
            //Fecha de Rendicion
            sParseError         := MSG_ERROR_RENDITION_DATE;
            FechaRendicion      := EncodeDate(	StrToInt(Copy(sline, 38, 4)), StrToInt(Copy(sline, 42, 2)), StrToInt(Copy(sline, 44, 2)));
            //Identificador
            sParseError         := MSG_ERROR_IDENTIFIER;
            Identificador       := Trim(Copy( sline, 46, 31));
            if Identificador    = '' then Exit;

            sParseError         := '';
            Result              := True;

            //REV.1
            if sParseError <> '' then begin
                Result  := False;
            end;
            //Fin REV.1

        except
            on Exception do begin
                //REV.1:
                Result  := False;
                //Exit;
                //Fin REV.1
            end;
        end;
    end;
end;

{******************************************************************************
Function Name   :   ObtenerCantidadErroresInterfaz
Author          :   ndonadio
Date Created    :   03/10/2005
Description     :   Pasa los errores de parseo al log de errores de interfases
*******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.AddParsingErrors(CodigoOperacion: Integer);
var
    i: integer;
begin
    // SS 909
    for i := 0 to FAdvertenciasLog.Count - 1 do
        if not AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacion, FAdvertenciasLog[i], 0) then exit;

    for i := 0 to FErrorLog.Count - 1 do
        if not AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacion, FErrorLog[i], 1) then exit;
end;

{******************************************************************************
Function Name   :   CargarPasesDiariosTardios
Author          :   dcepeda         (ndonadio)
Date Created    :   08-Abril-2010   (05/10/2005)
Description     :   Carga los Pases Diarios Tard�os
*******************************************************************************}
function TfrmRecepcionArchVentasBHTU.CargarPasesDiariosTardios : boolean;
resourcestring
    MSG_PROCESSING_LATE_DAYPASS_FILE        = 'Procesando archivo de Pases Diarios Tard�os - Linea : %d';
    MSG_ERROR_PROCESSING_LATE_DAYPASS_FILE	= 'Error procesando archivo de Pases Diarios Tard�os';
    MSG_ERROR                               = 'Error';
    //MSG_ERROR_LINEA_LARGO                   = 'La l�nea %d tiene un largo inferior a %d'; {REV.1: Cambio Mensaje}
    MSG_ERROR_LINEA_LARGO                   = 'La l�nea %d tiene un largo distinto a %d d�gitos';
    MSG_ERROR_PARSEO                        = ' error de parseo: ';

const
    LARGO_LINEA         = 76;
    STR_LINE            =  'Linea: ';
var
    nNroLineaScript     : Integer;
    nLineasScript       : Integer;
    FDayPassRecord      : TLateDayPassRecord;
  	sParseError         : String;
    sErrorMsg           : String;
    sDescripcionError,
    sDescripcionAdvertencia: String;
begin
  	nLineasScript           := FPasesDiariosTardios.Count - 1;
    lblDetalle.Caption      := Format(MSG_PROCESSING_LATE_DAYPASS_FILE, [0]) + ' de ' + IntToStr(nLineasScript);
    pbProgreso.Position     := 0;
    pbProgreso.Max          := FPasesDiariosTardios.Count - 1;

	FErrors                 := False;
	nNroLineaScript         := 0;

	//REV.1:
	FSumaMontoTotalArchivo  := 0;
    FCantidadFallaParseo    := 0;
    //Fin REV.1

    while ( nNroLineaScript <= nLineasScript ) and ( not FCancelar ) and (	sErrorMsg = '' ) do begin

        //if Length(FPasesDiariosTardios[nNroLineaScript]) < LARGO_LINEA then begin {REV.1: La Longitud de la l�nea puede ser mayor o menor}
		if Length(FPasesDiariosTardios[nNroLineaScript]) <> LARGO_LINEA then begin    //REV.1
            FErrorLog.Add( Format(MSG_ERROR_LINEA_LARGO, [nNroLineaScript + 1, LARGO_LINEA]));
            FErrors := True;
            //REV.1:
            Inc(FCantidadFallaParseo);
            //exit;
            //Fin REV.1
        end
        else if ParseLateDayPassLine( FPasesDiariosTardios[nNroLineaScript], FDayPassRecord, sParseError ) then begin

            with FDayPassRecord, spAgregarDayPassTardio, Parameters do begin

                FSumaMontoTotalArchivo := FSumaMontoTotalArchivo + Monto;   //REV.1

				try
                	//DMConnections.BaseCAC.BeginTrans; {REV.1}
                    DMConnections.BaseCAC.Execute('BEGIN TRAN AgregarDayPassTardio');
					Refresh;
					ParamByName( '@Patente' ).Value                 := Patente;
					ParamByName( '@FechaHabilitacion' ).Value       := FechaHabilitacion;
					ParamByName( '@Importe' ).Value                 := Monto * 100;
					ParamByName( '@FechaVenta' ).Value              := FechaVenta;
					ParamByName( '@FechaRendicion' ).Value          := FechaRendicion;
					ParamByName( '@Identificador' ).Value           := Identificador;
					ParamByName( '@Categoria').Value                := Categoria;
					ParamByName( '@CodigoOperacion' ).Value         := FCodigoOperacion;
					ParamByName( '@DescripcionError' ).Value        := '';
					ParamByName( '@DescripcionAdvertencia' ).Value := ''; // SS 909
					ParamByName( '@CodigoProveedorDaypass' ).Value  := 1; // SERVIPAG
					spAgregarDayPassTardio.CommandTimeout           := 500;
					spAgregarDayPassTardio.ExecProc;
					sDescripcionError := Trim( VarToStr( ParamByName( '@DescripcionError' ).Value ));
                    sDescripcionAdvertencia := Trim( VarToStr( ParamByName( '@DescripcionAdvertencia' ).Value ));
                    //DMConnections.BaseCAC.CommitTrans;  {REV.1}
                    DMConnections.BaseCAC.Execute('COMMIT TRAN AgregarDayPassTardio');
					if sDescripcionError <> '' then begin
                        FErrorLog.Add(sDescripcionError); // SS 909
						FErrors := True;
					end
                    else begin // SS 909
                        if sDescripcionAdvertencia <> '' then
                            FAdvertenciasLog.Add(sDescripcionAdvertencia);
                    end;

				except
					on E : Exception do begin
                        //DMConnections.BaseCAC.CommitTrans;    {REV.1}
						DMConnections.BaseCAC.Execute('ROLLBACK TRAN AgregarDayPassTardio');
						//Registro la excepcion en el log de operaciones
						AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, MSG_ERROR_PROCESSING_LATE_DAYPASS_FILE + ' - ' + STR_LINE + IntToStr(nNroLineaScript + 1) + ' - ' + FPasesDiariosTardios[nNroLineaScript] + ' - ' + E.Message);
						sErrorMsg := MSG_ERROR_PROCESSING_LATE_DAYPASS_FILE;
						FErrors:= True;
						//Informo que se produjo un error al procesar
						MsgBoxErr(MSG_ERROR_PROCESSING_LATE_DAYPASS_FILE + CRLF + STR_LINE + IntToStr(nNroLineaScript + 1) + CRLF + FPasesDiariosTardios[nNroLineaScript], e.Message, MSG_ERROR, MB_ICONERROR);
					end;
				end;
			end;
		end
		else begin
			sParseError := STR_LINE + IntToStr(nNroLineaScript + 1) + MSG_ERROR_PARSEO + sParseError;
			FErrorLog.Add(sParseError);
			FErrors     := True;
            Inc(FCantidadFallaParseo);  //REV.1
		end;

		//Informa qu� BHTU est� procesando
		lblDetalle.Caption  := Format(MSG_PROCESSING_LATE_DAYPASS_FILE, [nNroLineaScript])+ ' de ' + IntToStr(nLineasScript);
		Application.ProcessMessages;

		Inc( nNroLineaScript );
		pbProgreso.Position := nNroLineaScript;
		Application.ProcessMessages;
    end;

    pbProgreso.Position := 0;

    Result := ( not FCancelar ) and ( sErrorMsg = '' );
end;


{******************************************************************************
Function Name   :   ObtenerCantidadErroresInterfaz
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Obtiene la cantidad de errores que se produjeron en el proceso
*******************************************************************************}
Function TfrmRecepcionArchVentasBHTU.ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
resourcestring
    MSG_ERROR   = 'No se pudo obtener la cantidad de errores de la interfaz';
begin
    Cantidad    := 0;
    try
        Cantidad    := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
        Result      := true;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR,e.Message, Caption, MB_ICONERROR);
            Result := False;
        end;
    end;
end;

procedure TfrmRecepcionArchVentasBHTU.RBIResumenExecute(Sender: TObject;
  var Cancelled: Boolean);
var																					              	//SS_1313_NDR_20150615
    FError: AnsiString;																              	//SS_1313_NDR_20150615
begin																				              	//SS_1313_NDR_20150615
  ppFechaProcesamiento.Caption:=FormatDateTime('dd-mm-yyyy', NowBase(DMConnections.BaseCAC));		//SS_1313_NDR_20150615
  spResumenDayPass.Close;															                //SS_1313_NDR_20150615
  spResumenDayPass.Parameters.Refresh;                                                            	//SS_1313_NDR_20150615
  spResumenDayPass.Parameters.ParamByName('@CodigoProveedorDaypass').Value:=1;                     	//SS_1313_NDR_20150615
  spResumenDayPass.Open;															                //SS_1313_NDR_20150615
end;																				                //SS_1313_NDR_20150615

{******************************************************************************
Function Name   :   RegistrarArchivo
Author          :   dcepeda         (ndonadio)
Date Created    :   08-Abril-2010   (05/10/2005)
Description     :   Registra el archivo recibido
*******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.RegistrarArchivo;
begin
    try
        with spRegistrarArchivoRecibidoVentaBHTU do begin
            Parameters.ParamByName('@CodigoOperacionInterfase').Value   := FCodigoOperacion;
            Parameters.ParamByName('@FechaArchivo').Value               := FFechaArchivo;
            Parameters.ParamByName('@CantidadBHTU').Value               := FCantidadBHTU;
            //Parameters.ParamByName('@CantidadBHTUValidos').Value      := FCantidadBHTU - FCantErroresE;
            Parameters.ParamByName('@CantidadBHTUValidos').Value        := FCantidadBHTU - FCantidadFallaParseo;   //REV.1
            Parameters.ParamByName('@CantidadErroresW').Value           := FCantErroresW;
            //Parameters.ParamByName('@CantidadErroresE').Value         := FCantErroresE;
            Parameters.ParamByName('@CantidadErroresE').Value           := FCantidadFallaParseo;  //REV.1
            ExecProc;
        end;
    except
        on Exception do begin
            Exit;
        end;
    end;
end;

{******************************************************************************
Function Name   :   ActualizarLog
Author          :   dcepeda         (ndonadio)
Date Created    :   08-Abril-2010   (05/10/2005)
Description     :   Se Actualiza el log para marcar la hora de finalizaci�n del proceso
*******************************************************************************}
Function TfrmRecepcionArchVentasBHTU.ActualizarLog(CantidadErrores : Integer) : Boolean;
Resourcestring
    MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
Const
    STR_ERRORES = 'Finaliz� OK!, Errores: %d, Advertencias: %d ';
    STR_CON_ERROR = 'Finaliz� con Errores, Errores: %d, Advertencias: %d';
var
    DescError, Mensaje : String;
begin
    // Rev. 1 (SS 909)
    if FErrors then
        Mensaje := STR_ERRORES
    else Mensaje := STR_CON_ERROR;

    Mensaje := Format(Mensaje, [FErrorLog.Count, FAdvertenciasLog.Count]);

    try
		Result  := ActualizarLogOperacionesInterfaseAlFinal
                    //(DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError); {REV.1}
					(DMConnections.BaseCAC, FCodigoOperacion, FCantidadBHTU, FSumaMontoTotalArchivo, Mensaje, DescError);

    	//REV.1:
        if Result then begin
        	spActualizarLogOperacionesRegistrosProcesados.Close;
        	with  spActualizarLogOperacionesRegistrosProcesados do begin
                Parameters.ParamByName('@CodigoOperacionInterfase').Value   := FCodigoOperacion;
                Parameters.ParamByName('@RegistrosAProcesar').Value         := FCantidadBHTU - FCantidadFallaParseo;
                Parameters.ParamByName('@LineasArchivo').Value              := NULL;
                ExecProc;
            end;
        end;
        //Fin REV.1

    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
            Result  := False;
        end;
    end;
end;

{******************************************************************************
Function Name   :   GenerarReporteDeFinalizacion
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Muestra Reporte
*******************************************************************************}
Function TfrmRecepcionArchVentasBHTU.GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
Const
    STR_TITLE   = 'Recepci�n Archivo Ventas de BHTU';
var
        F: TRptDayPassForm;
begin
        try
            F := TRptDayPassForm.Create(Self);

            if F.Inicializar(STR_TITLE, FCodigoOperacion, True) then
                F.RBIListado.Execute;
        finally
            if Assigned(F) then FreeAndNil(F);
        end;
end;

{******************************************************************************
Function Name   :   btnProcesarClick
Author          :   dcepeda         (lgisuk)
Date Created    :   08-Abril-2010   (31/01/2006)
Description     :   Procesa el archivo de Ventas
******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.btnProcesarClick(Sender: TObject);
resourcestring
    MSG_FILE_ALREADY_PROCESSED              = 'El archivo %s ya fue procesado' + crlf + 'Desea reprocesarlo ?';
    MSG_ERROR_FILE_MAYBE_CORRUPTED          = 'El archivo no se puede interpretar correctamente.';
    MSG_ERROR_FILE_NOT_EXISTS               = 'El archivo indicado no existe';
    MSG_ERROR_LATE_DAYPASS_FILE_IS_EMPTY    = 'El archivo de pases diarios est� vac�o';
    MSG_ERROR_CANNOT_OPEN_LATE_DAYPASS_FILE = 'No se puede abrir el archivo de pases diarios tard�os';
    MSG_ERROR                               = 'Error';
    MSG_ERROR_DAYPASS_REPORT		= 'Error emitiendo informe resumen de Pases Diarios';               //SS_1313_NDR_20150615
const
    STR_PROCESS_SUCCEDED                    = 'El proceso finaliz� con �xito!';
    //STR_PROCESS_ENDED_WITH_ERRORS           = 'El proceso finaliz� con errores'; //REV.1
    STR_PROCESS_ENDED_WITH_ERRORS           = 'El proceso finaliz� con Advertencias o errores'; //REV.1
    STR_PROCESS_CANCELLED                   = 'El proceso ha sido Cancelado por el Usuario';
    STR_PROCESS_COULD_NOT_BE_COMPLETED      = 'El proceso no se pudo completar.';
var
    DescError   : AnsiString;
begin
    //verifica que el archivo sea valido
    if not ValidateControls( [txtOrigen], [FileExists(txtOrigen.Text)], caption, [MSG_ERROR_FILE_NOT_EXISTS]) then Exit;

    //Verifica que el Archivo no haya sido procesado
    if  VerificarArchivoProcesado(DMConnections.BaseCAC, txtOrigen.Text) then begin
        //informa que el archivo ya fue procesado, consulta al operador si desea continuar
        if MsgBox (Format (MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtOrigen.Text)]), Caption, MB_ICONWARNING or MB_YESNO) <> IDYES then Exit;
    end;

    //Crea stringlist
    FPasesDiariosTardios    := TStringList.Create;
    FErrorLog               := TStringList.Create;
    FAdvertenciasLog        := TStringList.Create;
    //Deshabilita y Habilita los botones
    FProcesando             := True;
    HabilitarBotones;

    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        try
            //Lee el archivo de pases diarios tardios
            FPasesDiariosTardios.LoadFromFile(txtOrigen.Text);

            //Elimina las lineas en blanco
            EliminarLineasEnBlanco(FPasesDiariosTardios);

            //Verifica si el Archivo Contiene alguna linea
            if (FPasesDiariosTardios.Count = 0) then begin

            //Informa que el archivo esta vacio
            MsgBox(MSG_ERROR_LATE_DAYPASS_FILE_IS_EMPTY, Caption, MB_ICONERROR)

            end else begin

                    if not VerificarCantidadDeRegistros(FPasesDiariosTardios, descError) then begin
                        MsgBoxErr(MSG_ERROR_FILE_MAYBE_CORRUPTED, descError, Caption, MB_ICONSTOP);
                        Exit;
                    end;

                    // Registra la operacion
                    if not RegistrarOperacion(RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO_TARDIO, ExtractFileName(txtOrigen.Text)) then Exit;

                    // Carga los pases diarios
                    if CargarPasesDiariosTardios then begin

                        //Verifica si hubo errores
                        if not FErrors then begin
                            //Informa que el proceso finaliz� con �xito
                            MsgBox(STR_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION);
                        end else begin
                            //Informa que el proceso finalizo con errores
                            MsgBox(STR_PROCESS_ENDED_WITH_ERRORS, Caption, MB_ICONINFORMATION);
                        end;
                        ObtenerCantidadErroresInterfaz(FCodigoOperacion, FCantidadErrores);
                        FCantErroresW   := FCantidadErrores;
                        AddParsingErrors(FCodigoOperacion);
                        //ObtenerCantidadErroresInterfaz(FCodigoOperacion, FCantidadErrores);   {REV.1: Comentado por estar repetido}
                        // Registra el Archivo recibido
                        RegistrarArchivo;
                        //Actualiza el log al Final
                        ActualizarLog(FCantidadErrores);
                        //Mueve el archivo procesado a otro directorio
                        MoverArchivoProcesado(Caption, txtOrigen.Text, GoodDir(FBHTU_Directorio_Procesados) + ExtractFileName(txtOrigen.Text));
                        FProcesando := False;
                        Self.Visible := False;
                        Application.ProcessMessages;
                        //Muestra el Reporte de Finalizacion del Proceso
                        GenerarReportedeFinalizacion(FCodigoOperacion);
                        try                                                                                   //SS_1313_NDR_20150615
                          RBIResumen.Execute(True);                                                           //SS_1313_NDR_20150615
                        except                                                                                //SS_1313_NDR_20150615
                          on e: Exception do begin                                                            //SS_1313_NDR_20150615
                            MsgBoxErr(MSG_ERROR_DAYPASS_REPORT, e.Message, 'Error', MB_ICONERROR);            //SS_1313_NDR_20150615
                          end;                                                                                //SS_1313_NDR_20150615
                        end;                                                                                  //SS_1313_NDR_20150615

                    end else begin
                        if FCancelar then begin
                            //registra que el proceso fue cancelado
                            RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacion);
                            //informa que el proceso fue cancelado
                            MsgBox(STR_PROCESS_CANCELLED, Caption, MB_ICONERROR);
                        end else begin
                            //informa que el proceso no se pudo completar
                            MsgBox(STR_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
                        end;
                    end;
                end;
        except
            on E : Exception do begin
                MsgBoxErr(MSG_ERROR_CANNOT_OPEN_LATE_DAYPASS_FILE, E.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        //Libera el stringlist
        FreeAndNil(FPasesDiariosTardios);
        FreeAndNil(FErrorLog);
        FreeAndNil(FAdvertenciasLog);
        //Deshabilita y Habilita los botones
        if not FProcesando then
            Self.Close
        else HabilitarBotones;

        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

{******************************************************************************
Function Name   :   btnCancelarClick
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Permite cancelar el proceso
*******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.btnCancelarClick(Sender: TObject);
begin
    FCancelar   := True;
end;

{******************************************************************************
Function Name   :   btnSalirClick
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Sale del form
*******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.btnSalirClick(Sender: TObject);
begin
    if not FProcesando then Close;
end;

{******************************************************************************
Function Name   :   FormCloseQuery
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Permite cerrar el form solo si no est� procesando
*******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

{******************************************************************************
Function Name   :   FormClose
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Libero el formulario de memoria
*******************************************************************************}
procedure TfrmRecepcionArchVentasBHTU.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
