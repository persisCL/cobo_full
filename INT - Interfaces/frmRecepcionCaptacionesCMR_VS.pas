{-------------------------------------------------------------------------------------------------------------
    File Name      :    frmRecepcionCaptacionesCMR_VS.pas
    Author         :    CQuezada
    Date Created   :    02-04-2015
    Language       :    ES-CL
    Firma          :    SS_1147_CQU_20150324
    Description    :    Modulo de la intefaz Falabella  - Recepci�n de Captaciones PARA VESPUCIO SUR
                        Se reflota la versi�n antigua de Captaciones de CN ya que Vespucio usa esa version.

    Firma       : SS_1147_NDR_20141216
    Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-------------------------------------------------------------------------------------------------------------}
unit frmRecepcionCaptacionesCMR_VS;

interface

uses
  //Recepcion Captaciones CMR-Falabella ParamGen Vespucio Sur
  DMConnection,
  Util,
  UtilProc,
  ComunesInterfaces,
  ConstParametrosGenerales,
  PeaProcs,
  PeaTypes,
  UtilDB,
  FrmRptRecepcionCaptacionesCMR,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, DPSControls, ppDB, ppTxPipe, StrUtils,
  ppParameter, ppModule, raCodMod, ppCtrls, ppBands, ppReport, ppPrnabl,
  ppClass, ppStrtch, ppSubRpt, ppCache, ppComm, ppRelatv, ppProd, UtilRB,
  RStrings, RBSetup,ppDBPipe, DmiCtrls,
  DBClient;


type
  TCaptacionCMRRecord = Record
    NumeroDocumento         :   String;
    CodigoPersona           :   Integer;
    Apellido                :   String;
    Nombre                  :   String;
    Patente1                :   String;
    RutPatente1             :   String;
    Patente2                :   String;
    RutPatente2             :   String;
    Patente3                :   String;
    RutPatente3             :   String;
  	CodigoConvenioPatente1  :   Integer;
  	CodigoConvenioPatente2  :   Integer;
  	CodigoConvenioPatente3  :   Integer;
    NumeroTarjeta           :   String;
    MandatoCMR              :   Integer;
    Direccion               :   String;
    FechaInicio             :   TDateTime;
    FechaTermino            :   TDateTime;
    CodigoRespuesta         :   Integer;
    CodigoRespuestaFalabella:   Integer;
  	ObsRespuesta            :   String;
  end;


  TfRecepcionCaptacionesCMR_VS = class(TForm)
    Bevel1: TBevel;
    btnAbrirArchivo: TSpeedButton;
    Label2: TLabel;
    edOrigen: TEdit;
    OpenDialog: TOpenDialog;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    spAgregarDetalleCaptacion: TADOStoredProc;
    spObtenerConvenioClientePorPatente: TADOStoredProc;
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edOrigenChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FCodigoOperacion : Integer;
    FCancelar,
  	FDetenerImportacion: Boolean;
  	FCaptacionesTXT : TStringList;
  	FErrorMsg : String;
    FErrorDesc : String;
    FTotalRegistros: Integer;
   	FCMR_Directorio_Origen_Captaciones : AnsiString;
    FCMR_Directorio_Procesados: AnsiString;
    FCodigoNativa : Integer;
    FCMR_CodigodeAutopista : Ansistring;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  	function  RegistrarOperacion : Boolean;
    function  ParseCaptacionCMRLine(Line : string; Linea : Integer; var CaptacionRecord : TCaptacionCMRRecord ) : Boolean;
    function  ActualizarCaptaciones(Bajas : Boolean) : Boolean;
  	Function  GenerarReporteFinal : Boolean;
  public
  	function  Inicializar(txtCaption : ANSIString; MDIChild : Boolean) : Boolean;
	{ Public declarations }
  end;

var
  fRecepcionCaptacionesCMR_VS : TfRecepcionCaptacionesCMR_VS;

Const
  RO_MOD_INTERFAZ_ENTRANTE_CAPTACIONES_CMR = 50;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 15/06/2005
  Description: Inicializa el Formulario
  Parameters: txtCaption: ANSIString; MDIChild:Boolean
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfRecepcionCaptacionesCMR_VS.Inicializar(txtCaption : ANSIString; MDIChild : Boolean) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 05/08/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        CMR_DIRECTORIO_ORIGEN_CAPTACIONES = 'CMR_Directorio_Origen_Captaciones';
        CMR_DIRECTORIO_PROCESADOS         = 'CMR_Directorio_Procesados';
        CMR_CODIGODEAUTOPISTA             = 'CMR_CodigodeAutopista';    // SS_1147_CQU_20150324
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la concesionaria nativa
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;
                // Obtengo el C�digo de la Concesionaria
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_CODIGODEAUTOPISTA , FCMR_CodigodeAutopista) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_CODIGODEAUTOPISTA;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_DIRECTORIO_ORIGEN_CAPTACIONES , FCMR_Directorio_Origen_Captaciones) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_DIRECTORIO_ORIGEN_CAPTACIONES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FCMR_Directorio_Origen_Captaciones := GoodDir(FCMR_Directorio_Origen_Captaciones);
                if  not DirectoryExists(FCMR_Directorio_Origen_Captaciones) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FCMR_Directorio_Origen_Captaciones;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,CMR_DIRECTORIO_PROCESADOS, FCMR_Directorio_Procesados);

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Defino el modo en que se visualizara la ventana
    if not MDIChild then begin
        FormStyle := fsNormal;
        Visible := False;
    end;
    //Centro el form
    CenterForm(Self);
  	try
        DMConnections.BaseCAC.Connected := True;
    		Result := DMConnections.BaseCAC.Connected and
                              VerificarParametrosGenerales;
  	except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Caption := AnsiReplaceStr(txtCaption, '&', '');
    btnCancelar.Enabled := False;
    btnProcesar.Enabled := False;
    pnlAvance.Visible := False;
    lblReferencia.Caption := '';
    FCodigoOperacion := 0;         //inicializo codigo de operacion
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfRecepcionCaptacionesCMR_VS.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_CAPTACIONES        = ' ' + CRLF +
                             'El Archivo de Captaciones es ' + CRLF +
                             'utilizado por FALABELLA para informar al ESTABLECIMIENTO' + CRLF +
                             'las altas de mandantes efectuadas en FALABELLA' + CRLF +
                             ' ' + CRLF +
                             'Nombre del Archivo: 03_CAPTACIONCMRE_DDMMAA.SS' + CRLF +
                             ' ' + CRLF +
                             'Se utiliza para actualizar los datos de' + CRLF +
                             'los mandantes en el sistema del ESTABLECIMIENTO.' + CRLF +
                             ' ';
begin
    //si esta procesando sale
    if PnlAvance.Visible = True then Exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_CAPTACIONES, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TfRecepcionCaptacionesCMR_VS.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.Visible = True then ImgAyuda.Cursor := crDefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoClick
  Author:    flamas
  Date Created: 15/06/2005
  Description: Obtiene el Archivo de Captaciones
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionCaptacionesCMR_VS.btnAbrirArchivoClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerFiltro
      Author:    lgisuk
      Date Created: 16/03/2005
      Description: Obtengo el Filtro
      Parameters: var Filtro: AnsiString
      Return Value: Boolean

      Revision 1
      lgisuk
      10/09/2007
      Cambio nombre archivo recibido de CMR

      Revision 2
      lgisuk
      11/10/2007
      Ahora acepta archivos con el nombre CAPTACIONCMRE o CAPTACIONEMPE
    -----------------------------------------------------------------------------}
    Function ObtenerFiltro(var Filtro : AnsiString) : Boolean;
    Const
        FILE_TITLE      = 'Recepci�n Captaciones CMR-Falabella|';
        FILE_NAME       = '_CAPTACIONCMRE_';
        FILE_TITLE_EMPE = 'Recepci�n Captaciones CMR-Falabella Empresas|';
        FILE_NAME_EMPE  = '_CAPTACIONEMPE_';
        FILE_EXTENSION  = '*';
    begin
    		Filtro := FILE_TITLE + FCMR_CodigodeAutopista + FILE_NAME + '*.' + FILE_EXTENSION + '|'
                    + FILE_TITLE_EMPE + FCMR_CodigodeAutopista + FILE_NAME_EMPE + '*.' + FILE_EXTENSION ;
        Result:= True;
    end;

    Function EsArchivoValido(Nombre : AnsiString) : Boolean;
    begin
        Result:=    ExistePalabra(Nombre, FCMR_CodigodeAutopista) and
                    ExistePalabra(UpperCase(Nombre), 'CAPTACIONCMRE');
    end;

resourcestring
    MSG_ERROR = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST    = 'El archivo %s no existe';
  	MSG_ERROR_INVALID_FILENAME       = 'El nombre del archivo %s es inv�lido';
  	MSG_ERROR_FILE_ALREADY_PROCESSED = 'El archivo %s ya fue procesado'+ #10#13 + 'Desea re-procesarlo ?';
var
    Filtro : String;
begin
    //obtengo el filtro a aplicar a este tipo de archivos
    ObtenerFiltro(Filtro);

    Opendialog.InitialDir := FCMR_Directorio_Origen_Captaciones;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if OpenDialog.Execute then begin

        edOrigen.text := UpperCase( OpenDialog.FileName );

        if not EsArchivoValido (opendialog.filename) then begin
            MsgBox(Format(MSG_ERROR_INVALID_FILENAME, [opendialog.filename]), self.Caption, MB_OK + MB_ICONINFORMATION);
            edOrigen.text := '';
        end else begin
            btnAbrirArchivo.Enabled := False;
            edOrigen.text := OpenDialog.FileName;
        end;

        if not FileExists( edOrigen.text ) then begin
          	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

      	if  VerificarArchivoProcesado( DMConnections.BaseCAC, edOrigen.text ) then begin
    	  		if  (MsgBox(Format(MSG_ERROR_FILE_ALREADY_PROCESSED, [ExtractFileName(edOrigen.text)]), Caption, MB_YESNO + MB_ICONQUESTION) = IDNO) then Exit;
        end;

        if FCMR_Directorio_Procesados <> '' then begin
            if RightStr(FCMR_Directorio_Procesados,1) = '\' then  FCMR_Directorio_Procesados := FCMR_Directorio_Procesados + ExtractFileName(edOrigen.text)
            else FCMR_Directorio_Procesados := FCMR_Directorio_Procesados + '\' + ExtractFileName(edOrigen.text);
        end;

        BtnProcesar.Enabled := True;
	end;

end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 15/06/2005
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfRecepcionCaptacionesCMR_VS.RegistrarOperacion : Boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
  	DescError : String;
begin
  	Result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRANTE_CAPTACIONES_CMR, ExtractFileName(edOrigen.text), UsuarioSistema, '', True, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError  );
    if not Result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: ParseCaptacionCMRLine
  Author:    flamas
  Date Created: 15/06/2005
  Description: Parsea un l�nea del Archivo de Captaciones de Falabella
  Parameters: 
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  24/09/2007
  Version intermedia SS 565
-----------------------------------------------------------------------------}

 	function TfRecepcionCaptacionesCMR_VS.ParseCaptacionCMRLine(Line : String; Linea : Integer; var CaptacionRecord : TCaptacionCMRRecord) : Boolean;
    {-----------------------------------------------------------------------------
      Function Name: BorrarRecord
      Author:    flamas
      Date Created: 15/06/2005
      Description: Borra el Contenido del Registro
      Parameters: var CaptacionRecord : TCaptacionCMRRecord
      Return Value: None
    -----------------------------------------------------------------------------}
    Procedure BorrarRecord( var CaptacionRecord : TCaptacionCMRRecord );
    begin
        with CaptacionRecord do begin
            //Leidos del archivo
            NumeroDocumento          := '';
            Apellido		         := '';
            Nombre			         := '';
            Patente1		         := '';
            RutPatente1              := '';
            Patente2		         := '';
            RutPatente2              := '';
            Patente3		         := '';
            RutPatente3              := '';
            NumeroTarjeta 	         := '';
            MandatoCMR               := 0;
            Direccion 		         := '';
            FechaInicio		         := NullDate;
            FechaTermino	         := NullDate;
            //Se obtienen de la base
            CodigoPersona	         := 0;
            CodigoConvenioPatente1 	 := -1;
            CodigoConvenioPatente2 	 := -1;
            CodigoConvenioPatente3 	 := -1;
            CodigoRespuesta	         := 0;
            CodigoRespuestaFalabella := 0;
            ObsRespuesta 	         := '';
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: RegistroVacio
      Author:    lgisuk
      Date Created: 24/09/2007
      Description: Verifica si se recibio una linea vacia
      Parameters:
      Return Value: integer
    -----------------------------------------------------------------------------}

    Function RegistroVacio (CaptacionRecord : TCaptacionCMRRecord) : Boolean;
    begin

        with CaptacionRecord do begin
            //Verifico si el registro esta vacio
            Result :=       (NumeroDocumento = '')
                      and   (Apellido = '')
                      and   (Nombre = '')
                      and   (Patente1 = '')
                      and   (RutPatente1 = '')
                      and   (NumeroTarjeta  = '');
        end;

    end;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCodigoPersonaPatente
      Author:    flamas
      Date Created: 15/06/2005
      Description: Obtiene el C�digo de la Persona Due�a de un veh�culo determinado
      Parameters: sPatente : string
      Return Value: integer
    -----------------------------------------------------------------------------}

    Function ObtenerCodigoPersonaPatente(Patente : String; var CodigoConvenio, CodigoCliente : Integer) : Boolean;
    resourcestring
        MSG_ERROR 						= 'Error';
        MSG_ERROR_GETTING_PLATE_DATA 	= 'Error obteniendo los datos de la Patente';
    begin
        Result := True;
        CodigoConvenio 	:= -1;
        CodigoCliente   := -1;

        if (Patente = '') then Exit;

        try
            with spObtenerConvenioClientePorPatente  do begin
                Close;
                CommandTimeout := 500;
                Parameters.ParamByName('@Patente').Value := Patente;
                Open;
                if not IsEmpty then begin
                    CodigoConvenio 	:= FieldByName('CodigoConvenio').AsInteger;
                    CodigoCliente   := FieldByName('CodigoCliente').AsInteger;
                end;
                Close;
            end;
        except
            on e: Exception do begin
                FErrorMsg 	:= MSG_ERROR_GETTING_PLATE_DATA;
                FErrorDesc	:= e.Message;
                Result 		  := False;
            end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCodigoConvenioPorPatente_Rut
      Author:    lgisuk
      Date Created: 07/10/2005
      Description: Obtiene el codigo de convenio que coincide con ese cliente y vehiculo
      Parameters:
      Return Value: integer
    -----------------------------------------------------------------------------}

    Function ObtenerCodigoConvenioPorPatente_Rut(Patente : String; Rut : String) : Integer;
    begin
        Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerConvenioporPatente_Rut( ' + QuotedStr(Patente) + ' , ' + QuotedStr(Rut) + ')')
    end;

resourcestring
  	STR_ERROR_WRONG_DOCUMENT 			            = 'Rut err�neo: %s';
  	STR_ERROR_NO_PLATES					            = 'No se informaron patentes';
    STR_WARNING_NEW_PERSON      		            = 'Se agregar� la persona %s, %s';
    STR_ERROR_PLATE_NOT_REGISTERED 		            = 'La Patente%d (%s) no est� registrada en Costanera Norte';
    STR_WARNING_MORE_THAN_ONE_CONTRACT	            = 'Las Patentes del Mandato est�n en m�s de un Convenio';
    STR_WARNING_OWNER_ERROR				            = 'El Mandande no es Cliente del Convenio';
    STR_ERROR_CREDITCARD_NOT_NUMERIC   	            = 'La Tarjeta de Cr�dito %s no es num�rica';
    STR_ERROR_WRONG_CREDITCARD			            = 'La Tarjeta de Cr�dito %s es Err�nea (estructura no corresponde)';
    STR_ERROR_WRONG_INIT_DATE			            = 'La Fecha de Inicio %s es err�nea';
    STR_ERROR_WRONG_END_DATE			            = 'La Fecha de T�rmino %s es err�nea';
    STR_THE_DOCUMENT_1_NOT_CORRESPOND_TO_PATENT_1   = 'Rut 1 no Corresponde a Patente 1';
    STR_THE_DOCUMENT_2_NOT_CORRESPOND_TO_PATENT_2   = 'Rut 2 no Corresponde a Patente 2';
    STR_THE_DOCUMENT_3_NOT_CORRESPOND_TO_PATENT_3   = 'Rut 3 no Corresponde a Patente 3';
    STR_SEPARATOR                                   = ' - ';
const
  	CODIGO_ERROR_CMR_FALABELLA = 7;
var
    CodigoPersonaPatente1 	: Integer;
    CodigoPersonaPatente2 	: Integer;
    CodigoPersonaPatente3 	: Integer;
    DescriError				: Ansistring;
    FechaI		            : String;
    FechaT                  : String;
    Mandato                 : String;
    EsRutValido             : Boolean;
    EsTarjetaNumerica       : Boolean;
begin
    Result := False;
    
    // Borra el contenido del Registro
    BorrarRecord( CaptacionRecord );
    CodigoPersonaPatente1 	:= -1;
    CodigoPersonaPatente2 	:= -1;
    CodigoPersonaPatente3 	:= -1;

    with CaptacionRecord do begin
		try
            //Obtengo los datos de una linea del archivo
            NumeroDocumento := Trim(Copy(line, 1, 10));    //Obtiene el RUT y el d�gito verificador; la primera posicion viene en cero 1;10
            if Copy(NumeroDocumento, 1, 2) = '00' then NumeroDocumento := Copy(NumeroDocumento, 2, 9);
            Apellido		:= Trim(Copy(line, 11, 36));
            Nombre			:= Trim(Copy(line, 11, 36));
            Patente1        := Trim(Copy(line, 47, 6));
            RutPatente1     := Trim(Copy(line, 54, 9));    //la primera posicion del numero viene en cero 53;10
            Patente2        := Trim(Copy(line, 63, 6));
            RutPatente2     := Trim(Copy(line, 70, 9));    //la primera posicion del numero viene en cero 69;10
            Patente3        := Trim(Copy(line, 79, 6));
            RutPatente3     := Trim(Copy(line, 86, 9));    //la primera posicion del numero viene en cero 85;10
            NumeroTarjeta   := Trim(Copy(line, 95, 16));
            Mandato         := Trim(Copy(line, 115, 8));
           	FechaI          := Trim(Copy(line, 159, 8));
    		FechaT          := Trim(Copy(line, 167, 8));
            CodigoRespuesta := 0;

            //si es un registro vacio lo ignoro
            if RegistroVacio (CaptacionRecord) then Exit;

            //Valida el RUT
            EsRutValido := True;
            if not ValidarRUT(DMConnections.BaseCAC, NumeroDocumento) then begin
                //Asigno el codigo de error
                EsRutValido := False;
              	CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                CodigoRespuestaFalabella := 13; //Otro Causal
                ObsRespuesta	:= 'E001;';
                //Registro el error en la tabla de errores interfaces
        		if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + Format(STR_ERROR_WRONG_DOCUMENT, [NumeroDocumento])) then
                begin
                    Exit;
                end;
            end;

            //Si el RUT es V�lido
            if EsRutValido then begin
                //Verifica si la persona esta en la base
  			    CodigoPersona := ObtenerCodigoPersona(DMConnections.BaseCAC, NumeroDocumento);
                //Si no esta la persona en la Base
                if (CodigoPersona = -1) then begin
                    //Registra como advertencia que una persona sera dada de alta
      				if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + Format(STR_WARNING_NEW_PERSON, [Apellido, Nombre])) then
                    begin
                        Exit;
                    end;
                end;
            end;

            //Valido si se informaron todas las patentes
            if (Patente1 = '') and (Patente2 = '') and (Patente3 = '') then begin
                //Asigno el codigo de error
              	CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                CodigoRespuestaFalabella := 13; //Otro Causal
                ObsRespuesta	:= 'E010;';
                 //Registro el error en la tabla de errores interfaces
        		if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + Format(STR_ERROR_NO_PLATES, [NumeroDocumento])) then
                begin
                    Exit;
                end;
            end;

            //Obtiene el codigo de convenio y titular de la patente 1
            if not ObtenerCodigoPersonaPatente( Patente1, CodigoConvenioPatente1, CodigoPersonaPatente1) then Exit;

            //Obtiene el codigo de convenio y titular de la patente 2
            if not ObtenerCodigoPersonaPatente( Patente2, CodigoConvenioPatente2, CodigoPersonaPatente2) then Exit;

            //Obtiene el codigo de convenio y titular de la patente 3
            if not ObtenerCodigoPersonaPatente( Patente3, CodigoConvenioPatente3, CodigoPersonaPatente3) then Exit;

            //Valido que exista un Convenio que incluya un vehiculo con la Patente 1
            if (Patente1 <> '') and (CodigoPersonaPatente1 = -1) then begin
                //Asigno el codigo de error
              	CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                CodigoRespuestaFalabella := 10; //Patente 1 Inexistente
                ObsRespuesta	:= ObsRespuesta + 'E002-1;';
                //Registro el error en la tabla de errores interfaces
         		if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + Format(STR_ERROR_PLATE_NOT_REGISTERED, [1, Patente1]))
                then Exit;
            end;

         		//Valido que exista un Convenio que incluya un vehiculo con la Patente 2
      			if (Patente2 <> '') and (CodigoPersonaPatente2 = -1) then begin
                //Asigno el codigo de error
              	CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                CodigoRespuestaFalabella := 11; //Patente 2 Inexistente
                ObsRespuesta	:= ObsRespuesta + 'E002-2;';
                //Registro el error en la tabla de errores interfaces
        		if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + Format(STR_ERROR_PLATE_NOT_REGISTERED, [2, Patente2]))
                then Exit;
            end;

            //Valido que exista un Convenio que incluya un vehiculo con la Patente 3
            if (Patente3 <> '') and (CodigoPersonaPatente3 = -1) then begin
                //Asigno el codigo de error
              	CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                CodigoRespuestaFalabella := 12; //Patente 3 Inexistente
                ObsRespuesta	:= ObsRespuesta + 'E002-3;';
                //Registro el error en la tabla de errores interfaces
        		if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + Format(STR_ERROR_PLATE_NOT_REGISTERED, [3, Patente3]))
                then Exit;
            end;

    		//Valida que la Tarjeta sea Num�rica
            EsTarjetaNumerica := True;
            try
                PermitirSoloDigitos(NumeroTarjeta);
            except
              	on exception do begin
                    //Asigno el codigo de error
                    EsTarjetaNumerica := false;
                    CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                    CodigoRespuestaFalabella := 13; //Otro Causal
                 	ObsRespuesta	:= ObsRespuesta + 'E003;';
                    //Registro el error en la tabla de errores interfaces
        	  		if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + Format(STR_ERROR_CREDITCARD_NOT_NUMERIC, [NumeroTarjeta]))
                    then Exit;
                end;
            end;

            //Valida el formato de la tarjeta sea correcto
            if EsTarjetaNumerica and not ValidarTarjetaCredito(DMConnections.BaseCAC,  TARJETA_CMR, NumeroTarjeta, DescriError) then begin
                //Asigno el codigo de error
                CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                CodigoRespuestaFalabella := 13; //Otro Causal
                ObsRespuesta	:= 'E004;';
                //Registro el error en la tabla de errores interfaces
                if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + Format(STR_ERROR_WRONG_CREDITCARD, [NumeroTarjeta]))
                then Exit;
            end;

            //Valido que el mandato sea numerico sino se asigna cero por default
            try
                MandatoCMR  := StrToInt(Mandato);
            except
            end;

            //Valida la Fecha de Inicio
            if (FechaI <> '00000000') then begin
                try
                    FechaInicio := EncodeDate( 	StrToInt(Copy(FechaI, 1, 4)), StrToInt(Copy(FechaI, 5, 2)), StrToInt(Copy(FechaI, 7, 2)));
                except
                    on exception do begin
                        //Asigno el codigo de error
                    	CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                        CodigoRespuestaFalabella := 13; //Otro Causal
                        ObsRespuesta	:= ObsRespuesta + 'E005;';
                        //Registro el error en la tabla de errores interfaces
                        if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + Format(STR_ERROR_WRONG_INIT_DATE, [FechaI]))
                        then Exit;
                    end;
                end;
            end;

            //Valida la Fecha de T�rmino
            if (FechaT <> '00000000') then begin
                try
                    FechaTermino := EncodeDate(	StrToInt(Copy(FechaT, 1, 4)), StrToInt(Copy(FechaT, 5, 2)), StrToInt(Copy(FechaT, 7, 2)));
                except
                    on exception do begin
                        //Asigno el codigo de error
                        CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                        CodigoRespuestaFalabella := 13; //Otro Causal
                        ObsRespuesta	:= ObsRespuesta + 'E006;';
                        //Registro el error en la tabla de errores interfaces
                        if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + Format(STR_ERROR_WRONG_END_DATE, [FechaT]))
                        then Exit;
                    end;
                end;
            end;

            //Verifica si las Patentes est�n en m�s de un Convenio
            //si existen las tres pantentes
            if 	(CodigoConvenioPatente1 <> -1) and (CodigoConvenioPatente2 <> -1) and (CodigoConvenioPatente3 <> -1) then begin

                //verifica si por los menos estan repartidas en dos convenios
                if (CodigoConvenioPatente1 <> CodigoConvenioPatente2) or (CodigoConvenioPatente1 <> CodigoConvenioPatente3) or (CodigoConvenioPatente2 <> CodigoConvenioPatente3) then begin
                    //Asigno observacion
                    ObsRespuesta	:= ObsRespuesta + 'W001;';
                    //Registro la observacion en la tabla ErroresInterfaces
                    if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + STR_WARNING_MORE_THAN_ONE_CONTRACT)
                    then Exit;
                end;

            //si existen solo las pantentes 1 y 2
            end else if	(CodigoConvenioPatente1 <> -1) and (CodigoConvenioPatente2 <> -1) then begin

                //verifica si la pantente 1 esta en un convenio diferente a la patente 2
                if 	(CodigoConvenioPatente1 <> CodigoConvenioPatente2) then begin
                    //Asigno observacion
                    ObsRespuesta	:= ObsRespuesta + 'W001;';
                    //Registro la observacion en la tabla ErroresInterfaces
                    if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + STR_WARNING_MORE_THAN_ONE_CONTRACT)
                    then Exit;
                end;

            //si existen solo las patentes 1  y 3
            end else if	(CodigoConvenioPatente1 <> -1) and (CodigoConvenioPatente3 <> -1) then begin

                //verifica si la pantente 1 esta en un convenio diferente a la patente 3
                if 	(CodigoConvenioPatente1 <> CodigoConvenioPatente3) then begin
                    //Asigno observacion
                    ObsRespuesta	:= ObsRespuesta + 'W001;';
                    //Registro la observacion en la tabla ErroresInterfaces
                    if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + STR_WARNING_MORE_THAN_ONE_CONTRACT)
                    then Exit;
                end;

            //si existen solo las patentes 2  y 3
            end else if	(CodigoConvenioPatente2 <> -1) and (CodigoConvenioPatente3 <> -1) then begin

                //verifica si la pantente 2 esta en un convenio diferente a la patente 3
                if 	(CodigoConvenioPatente2 <> CodigoConvenioPatente3) then begin
                      //Asigno observacion
                      ObsRespuesta	:= ObsRespuesta + 'W001;';
                      //Registro la observacion en la tabla ErroresInterfaces
                      if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + STR_WARNING_MORE_THAN_ONE_CONTRACT)
                      then Exit;
                end;
                    
            end;

            //Verifica si los vehiculos le pertenecen a la persona que paga la nota de cobro
            if	(CodigoPersona <> -1) and
                ((CodigoPersonaPatente1 <> -1) and (CodigoPersonaPatente1 <> CodigoPersona) or
                 (CodigoPersonaPatente2 <> -1) and (CodigoPersonaPatente2 <> CodigoPersona) or
                 (CodigoPersonaPatente3 <> -1) and (CodigoPersonaPatente3 <> CodigoPersona))
            then begin
                ObsRespuesta	:= ObsRespuesta + 'W002;';
        		if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + STR_WARNING_OWNER_ERROR)
                then Exit;
            end;

            //Obtengo el convenio asociado a la Patente y Rut 3 recibidos
            CodigoConvenioPatente3 := ObtenerCodigoConvenioPorPatente_Rut(Patente3, RutPatente3);
            //Si no existe Convenio para la Patente y Rut
            if (Patente3 <> '') and (CodigoConvenioPatente3 = -1) then begin
                //Asigno el codigo de error
                CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                CodigoRespuestaFalabella := 9; //Rut 3 no corresponde a Patente 3
                ObsRespuesta	:= ObsRespuesta + 'E011-3;';
                //Registro el error en la tabla de errores interfaces
                if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + STR_THE_DOCUMENT_3_NOT_CORRESPOND_TO_PATENT_3)
                then Exit;
            end;

            //Obtengo el convenio asociado a la Patente y Rut 2 recibidos
            CodigoConvenioPatente2 := ObtenerCodigoConvenioPorPatente_Rut(Patente2, RutPatente2);
            //Si no existe Convenio para la Patente y Rut
            if (Patente2 <> '') and (CodigoConvenioPatente2 = -1) then begin
                //Asigno el codigo de error
                CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                CodigoRespuestaFalabella := 8; //Rut 2 no corresponde a Patente 2
                ObsRespuesta	:= ObsRespuesta + 'E011-2;';
                //Registro el error en la tabla de errores interfaces
                if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + STR_THE_DOCUMENT_2_NOT_CORRESPOND_TO_PATENT_2)
                then Exit;
            end;

            //Obtengo el Convenio asociado a la Patente y Rut 1 recibidos
            CodigoConvenioPatente1 := ObtenerCodigoConvenioPorPatente_Rut(Patente1, RutPatente1);
            //Si no existe Convenio para la Patente y Rut
            if (Patente1 <> '') and (CodigoConvenioPatente1 = -1) then begin
                //Asigno el codigo de error
                CodigoRespuesta := CODIGO_ERROR_CMR_FALABELLA;
                CodigoRespuestaFalabella := 7; //Rut 1 no corresponde a Patente 1
                ObsRespuesta	:= ObsRespuesta + 'E011-1;';
                //Registro el error en la tabla de errores interfaces
                if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, NumeroDocumento + STR_SEPARATOR + STR_THE_DOCUMENT_1_NOT_CORRESPOND_TO_PATENT_1)
                then Exit;
            end;

            Result := True;

        except
            on Exception do begin
                Exit;
            end;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarCaptaciones
  Author:    FLamas
  Date Created: 16/06/2005
  Description: Actualiza las Captaciones - Primero actualiza las Bajas y Despu�s las Altas
  Parameters: bBajas : boolean (Actualiza s�lo las Bajas o las Altas)
  Return Value: boolean
-----------------------------------------------------------------------------}

function TfRecepcionCaptacionesCMR_VS.ActualizarCaptaciones(Bajas : Boolean) : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: CaptacionEsBaja
      Author:    FLamas
      Date Created: 16/06/2005
      Description: Devuelve true si la captaci�n es una Baja
      Parameters: sLinea : string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function CaptacionEsBaja(Linea : String) : Boolean;
    begin
        Result := (Trim(Copy(Linea, 167, 8)) <> '') and (Trim(Copy(Linea, 167, 8)) <> '00000000');
    end;
                     
resourcestring
    MSG_ERROR_PROCESSING_WARRANTS_FILE	= 'Error procesando archivo de Captaciones';
  	MSG_ERROR							              = 'Error';
const
    STR_PROCESSING			   			        = 'Procesando...';
var
  	NroLineaScript, LineasScript : Integer;
    CaptacionesRecord : TCaptacionCMRRecord;
    EsBaja : Boolean;
begin
  	Screen.Cursor := crHourglass;

  	LineasScript := FCaptacionesTXT.Count;
    FTotalRegistros := LineasScript;
    lblReferencia.Caption := STR_PROCESSING;
    pbProgreso.Position := 0;
    pbProgreso.Max := FCaptacionesTXT.Count;
    pnlAvance.Visible := True;

    NroLineaScript := 0;

    while ( NroLineaScript < LineasScript ) and ( not FDetenerImportacion ) and ( FErrorMsg = '' ) do begin

      	EsBaja := CaptacionEsBaja(FCaptacionesTXT[NroLineaScript]);
        //si Es baja y queremos procesar bajas procesamos el registro o si es alta y queremos procesar altas procesamos el registro
        if  (EsBaja and Bajas) or (not EsBaja and not Bajas) then begin

            if ParseCaptacionCMRLine(FCaptacionesTXT[NroLineaScript], NroLineaScript, CaptacionesRecord) then begin

                with spAgregarDetalleCaptacion, Parameters, CaptacionesRecord do begin
                    // Procesa las Bajas o las que no son bajas de acuerdo al par�metro
                    try
                        Refresh;
                        ParamByName('@CodigoOperacionInterfase' ).Value     := FCodigoOperacion;
                        ParamByName('@NumeroDocumento' ).Value				:= NumeroDocumento;
                        ParamByName('@Apellido' ).Value						:= Apellido;
                        ParamByName('@Nombre' ).Value						:= Nombre;
                        ParamByName('@CodigoCliente' ).Value				:= iif(CodigoPersona = -1, NULL, CodigoPersona);
                        ParamByName('@Patente1' ).Value						:= Patente1;
                        ParamByName('@RutPatente1' ).Value					:= RutPatente1;
                        ParamByName('@CodigoConvenio1' ).Value				:= iif(CodigoConvenioPatente1 = -1, NULL, CodigoConvenioPatente1);
                        ParamByName('@Patente2' ).Value						:= Patente2;
                        ParamByName('@RutPatente2' ).Value					:= RutPatente2;
                        ParamByName('@CodigoConvenio2' ).Value				:= iif(CodigoConvenioPatente2 = -1, NULL, CodigoConvenioPatente2);
                        ParamByName('@Patente3' ).Value					    := Patente3;
                        ParamByName('@RutPatente3' ).Value					:= RutPatente3;
                        ParamByName('@CodigoConvenio3' ).Value			    := iif(CodigoConvenioPatente3 = -1, NULL, CodigoConvenioPatente3);
                        ParamByName('@NumeroTarjeta' ).Value				:= NumeroTarjeta;
                        ParamByName('@MandatoCMR' ).Value					:= MandatoCMR;
                        ParamByName('@Direccion' ).Value					:= Direccion;
                        ParamByName('@FechaInicio' ).Value					:= iif( FechaInicio = NullDate, NULL, FechaInicio);
                        ParamByName('@FechaTermino' ).Value					:= iif( FechaTermino = NullDate, NULL, FechaTermino);
                        ParamByName('@CodigoRespuesta' ).Value			    := CodigoRespuesta;
                        ParamByName('@CodigoRespuestaFalabella' ).Value		:= CodigoRespuestaFalabella;
                        ParamByName('@ObservacionRespuesta' ).Value		  	:= ObsRespuesta;
                        DMConnections.BaseCAC.BeginTrans;
                        CommandTimeOut := 500;
                        ExecProc;
                        DMConnections.BaseCAC.CommitTrans;
                    except on E : Exception do
                        begin
                            //Si ya se habia producido la transaccion deshago la transacci�n
                            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                            //Guardo la descripci�n del error que ocurrio
                            FErrorMsg := MSG_ERROR_PROCESSING_WARRANTS_FILE;
                            //Isnformo que se produjo un error al procesar
                            MsgBoxErr(MSG_ERROR_PROCESSING_WARRANTS_FILE, e.Message, MSG_ERROR, MB_ICONERROR);
                        end;
                    end;
                end;  

            end;
        end;
        Inc( NroLineaScript );
        pbProgreso.Position := NroLineaScript;
        Application.ProcessMessages;
    end;
    pbProgreso.Position := pbProgreso.Max;
	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteFinal
  Author:    FLamas
  Date Created: 21/06/2005
  Description:	Genera el Reporte Final de la Interfaz
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  11/09/2007
  Cambio reporte por nuevo formato archivo
-----------------------------------------------------------------------------
  Revision 2
  lgisuk
  17/09/2007
  Ahora el reporte es independiente del proceso
-----------------------------------------------------------------------------}
function TfRecepcionCaptacionesCMR_VS.GenerarReporteFinal : Boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de finalizaci�n de proceso';
    MSG_ERROR = 'Error';
const
    REPORT_TITLE     = 'Recepci�n de Captaciones';
var
    F : TFRptRecepcionCaptacionesCMR;
begin
    Result := False;
    try
        //Muestro el reporte
        Application.CreateForm(TFRptRecepcionCaptacionesCMR, F);
        if not F.Inicializar(FCodigoOperacion) then f.Release;
        Result := True;
    except
       on e: Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    flamas
  Date Created: 15/06/2005
  Description: Procesa el archivo de Captaciones
  Parameters: Sender: TObject
  Return Value: None
------------------------------------------------------------------------------}
procedure TfRecepcionCaptacionesCMR_VS.btnProcesarClick(Sender: TObject);
    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/06/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion : Integer ; Var Cantidad : Integer) : Boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 01/07/2005
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog( CantidadErrores : Integer ) : Boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : String;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_SUCCEDED = 'El proceso finaliz� con �xito';
    MSG_PROCESS_COULD_NOT_BE_COMPLETED 	= 'El proceso no se pudo completar';
    MSG_ERROR_CANNOT_OPEN_WARRANTS_FILE = 'No se puede abrir el archivo de mandatos %s';
    MSG_ERRROR_WARRANTS_FILE_IS_EMPTY	= 'El archivo seleccionado est� vac�o';
    MSG_ERROR = 'Error';
var
    CantidadErrores : Integer;
    Buffer : String;
    Archivo : TextFile;
begin
    //Creo el StringList
    FCaptacionesTXT := TStringList.Create;
    FErrorMsg := '';

    // Deshabilita los botones
    btnCancelar.Enabled := True;
    btnProcesar.Enabled := False;
    btnSalir.Enabled := False;
    btnAbrirArchivo.Enabled := False;
    edOrigen.Enabled := False;

    FDetenerImportacion := False;

    try
        try
            //Lee el archivo del Mandatos
            FCaptacionesTXT.Clear;
            AssignFile(Archivo, edOrigen.text);
            Reset(Archivo);
            //Recorre cada linea del archivo
            while not Eof (Archivo) do begin
                ReadLn(Archivo, Buffer);         //Lee la linea
                FCaptacionesTXT.Add(Buffer);     //La agrega al StringList
            end;

            //Verifica si el Archivo Contiene alguna linea
            if ( FCaptacionesTXT.Count = 0 ) then begin

                //Informo que el archivo esta vacio
                MsgBox(MSG_ERRROR_WARRANTS_FILE_IS_EMPTY, Caption, MB_ICONERROR);

            end else begin

                if  RegistrarOperacion  and 				// Primero registra la Operaci�n para obtener el C�digo de la misma
                    ActualizarCaptaciones(True) and         // Primero Registra las Bajas
                    ActualizarCaptaciones(False)            // Despu�s Registra las Altas
                then begin
                    //Muestro mensaje que el proceso finalizo exitosamente
                    MsgBox(MSG_PROCESS_SUCCEDED, Self.Caption, MB_OK + MB_ICONINFORMATION);
                    //Obtengo la cantidad de errores
                    ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);
                    //Actualizo el log al Final
                    ActualizarLog(CantidadErrores);
                    //Muestro el reporte de finalizaci�n
                    GenerarReporteFinal;
                    //Muevo el archivo a otro directorio
                    MoverArchivoProcesado(Caption,edOrigen.Text, FCMR_Directorio_Procesados);
                end else begin
                    //Informo que el proceso no se pudo completar
                    MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
                end;
            end;
        except on E : Exception do
            begin
                FErrorMsg := MSG_ERROR_CANNOT_OPEN_WARRANTS_FILE;
                MsgBoxErr(Format( MSG_ERROR_CANNOT_OPEN_WARRANTS_FILE, [ExtractFilePath(edOrigen.Text)]), E.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        //Libera el TStringList
        FreeAndNil(FCaptacionesTXT);
        //Cierro el archivo
        try
            CloseFile(Archivo);
        except
        end;
        // Lo desactiva para poder Cerrar el Form
        lblreferencia.Caption := '';
        btnprocesar.Enabled := False;
        btnCancelar.Enabled := False;
        PnlAvance.Visible := False;
        btnSalir.Enabled := True;
        //Close;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    flamas
  Date Created: 07/12/2004
  Description: Detiene el proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionCaptacionesCMR_VS.btnCancelarClick(Sender: TObject);
resourcestring
  	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
	FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: Permito salir si no esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionCaptacionesCMR_VS.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
	CanClose := not btnCancelar.Enabled;
end;

procedure TfRecepcionCaptacionesCMR_VS.FormKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 15/03/2005
  Description:  Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionCaptacionesCMR_VS.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfRecepcionCaptacionesCMR_VS.edOrigenChange(Sender: TObject);
begin
	btnProcesar.Enabled := FileExists( edOrigen.Text);
end;
{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: Lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionCaptacionesCMR_VS.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
