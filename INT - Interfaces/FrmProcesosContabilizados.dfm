object FProcesosContabilizados: TFProcesosContabilizados
  Left = 17
  Top = 133
  Width = 981
  Height = 491
  Caption = 'Procesos Contabilizados'
  Color = clBtnFace
  Constraints.MinHeight = 491
  Constraints.MinWidth = 981
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PAbajo: TPanel
    Left = 0
    Top = 416
    Width = 973
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object PDerecha: TPanel
      Left = 872
      Top = 0
      Width = 101
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object SalirBTN: TButton
        Left = 17
        Top = 8
        Width = 75
        Height = 25
        Caption = '&Salir'
        TabOrder = 0
        OnClick = SalirBTNClick
      end
    end
  end
  object Parriba: TPanel
    Left = 0
    Top = 0
    Width = 973
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object LDesde: TLabel
      Left = 31
      Top = 5
      Width = 64
      Height = 13
      Caption = 'Desde Fecha'
    end
    object LHasta: TLabel
      Left = 135
      Top = 5
      Width = 61
      Height = 13
      Caption = 'Hasta Fecha'
    end
    object PAderecha: TPanel
      Left = 776
      Top = 0
      Width = 197
      Height = 49
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object AbrirBTN: TButton
        Left = 102
        Top = 16
        Width = 78
        Height = 25
        Caption = 'Consultar'
        TabOrder = 0
        OnClick = AbrirBTNClick
      end
    end
    object Edesde: TDateEdit
      Left = 31
      Top = 21
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = 37987.000000000000000000
    end
    object EHasta: TDateEdit
      Left = 135
      Top = 21
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 2
      Date = 37987.000000000000000000
    end
  end
  object DBListEx: TDBListEx
    Left = 0
    Top = 49
    Width = 973
    Height = 367
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 110
        Header.Caption = 'Numero Proceso'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'NumeroProcesoContabilizacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 110
        Header.Caption = 'Fecha Inicio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'FechaHoraInicio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 80
        Header.Caption = 'Usuario'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'codigousuario'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Fecha Jornada'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'FechaJornada'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 110
        Header.Caption = 'Fecha Finalizaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'FechaHoraFinalizacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 80
        Header.Caption = 'Fecha Cierre'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'FechaHoraCierre'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Supervisor'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'CodigoUsuarioCierre'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 120
        Header.Caption = 'Observaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'Observacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsUnderline]
        Width = 80
        Header.Caption = 'Informe'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clBlack
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = True
      end>
    DataSource = DataSource
    DragReorder = True
    ParentColor = False
    TabOrder = 2
    TabStop = True
    OnDrawText = DBListExDrawText
    OnLinkClick = DBListExLinkClick
  end
  object DataSource: TDataSource
    AutoEdit = False
    DataSet = spObtenerLogProcesosContabilizados
    Left = 48
    Top = 87
  end
  object spObtenerLogProcesosContabilizados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerLogProcesosContabilizados;1'
    Parameters = <>
    Left = 12
    Top = 88
  end
end
