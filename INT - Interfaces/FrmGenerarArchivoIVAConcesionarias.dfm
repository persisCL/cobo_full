object FGenerarArchivoIVAConcesionarias: TFGenerarArchivoIVAConcesionarias
  Left = 0
  Top = 0
  Caption = 'IVA Otras Concesionarias'
  ClientHeight = 293
  ClientWidth = 562
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnKeyDown = FormKeyDown
  DesignSize = (
    562
    293)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 3
    Width = 547
    Height = 233
    Anchors = [akLeft, akTop, akRight, akBottom]
    ExplicitHeight = 280
  end
  object lblFechaDesde: TLabel
    Left = 339
    Top = 17
    Width = 62
    Height = 13
    Caption = 'Fecha Desde'
  end
  object lblFechaHasta: TLabel
    Left = 445
    Top = 17
    Width = 60
    Height = 13
    Caption = 'Fecha Hasta'
  end
  object lblConcesionaria: TLabel
    Left = 16
    Top = 17
    Width = 67
    Height = 13
    Caption = 'Concesionaria'
  end
  object lblNombreArchivo: TLabel
    Left = 16
    Top = 61
    Width = 93
    Height = 13
    Caption = 'Nombre del Archivo'
  end
  object edFechaDesde: TDateEdit
    Left = 339
    Top = 36
    Width = 100
    Height = 21
    AutoSelect = False
    TabOrder = 1
    OnChange = edFechaDesdeChange
    Date = -693594.000000000000000000
  end
  object edFechaHasta: TDateEdit
    Left = 445
    Top = 36
    Width = 100
    Height = 21
    AutoSelect = False
    TabOrder = 2
    OnChange = edFechaHastaChange
    Date = -693594.000000000000000000
  end
  object btnSalir: TButton
    Left = 463
    Top = 248
    Width = 75
    Height = 25
    Caption = '&Salir'
    TabOrder = 5
    OnClick = btnSalirClick
  end
  object btnProcesar: TButton
    Left = 364
    Top = 248
    Width = 75
    Height = 25
    Caption = '&Procesar'
    TabOrder = 4
    OnClick = btnProcesarClick
  end
  object vcbConcesionarias: TVariantComboBox
    Left = 16
    Top = 36
    Width = 317
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = vcbConcesionariasChange
    Items = <>
  end
  object txtArchivo: TPickEdit
    Left = 16
    Top = 80
    Width = 530
    Height = 21
    Enabled = True
    TabOrder = 3
    EditorStyle = bteTextEdit
    OnButtonClick = txtArchivoButtonClick
  end
  object PnlAvance: TPanel
    Left = 16
    Top = 107
    Width = 530
    Height = 124
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = 'Presione la tecla escape [Esc] para cancelar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    VerticalAlignment = taAlignTop
    DesignSize = (
      526
      120)
    object lblMensaje: TLabel
      Left = 5
      Top = 47
      Width = 37
      Height = 13
      Caption = 'Estado:'
    end
    object lblProgreso: TLabel
      Left = 5
      Top = 24
      Width = 61
      Height = 17
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Progreso'
      Constraints.MaxHeight = 17
      Constraints.MaxWidth = 61
      Constraints.MinHeight = 17
      Constraints.MinWidth = 61
    end
    object pbProgreso: TProgressBar
      Left = 72
      Top = 24
      Width = 383
      Height = -4
      Anchors = [akLeft, akTop, akRight, akBottom]
      Smooth = True
      Step = 1
      TabOrder = 0
      ExplicitWidth = 387
      ExplicitHeight = 0
    end
  end
  object dlgSelArchivo: TOpenDialog
    Left = 16
    Top = 247
  end
  object qryConsulta: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 48
    Top = 248
  end
  object spObtenerIVAConcesionaria: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerIVAConcesionaria'
    Parameters = <>
    Left = 80
    Top = 248
  end
  object spAgregaHistoricoIVAConcesionaria: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregaHistoricoIVAConcesionaria'
    Parameters = <>
    Left = 112
    Top = 248
  end
end
