{-----------------------------------------------------------------------------
 File Name: frmRecepcionRendicionesPresto.pas
 Author:    flamas
 Date Created: 03/01/2005
 Language: ES-AR
 Description: Modulo de la interfaz Presto - Recepcion de Rendiciones
-----------------------------------------------------------------------------}
{Revision History
-----------------------------------------------------------------------------}
{Author: lgisuk
11/03/2005: Genero el Reporte de Finalizacion de Proceso. sirve para para verificar si el proceso se realizo segun parametros normales.
16/03/2005: Obtengo el Filtro
21/06/2005: Obtengo la cantidad de errores contemplados que se produjeron al procesar el archivo
21/06/2005: Ordene las rutinas por su orden de ejecuci�n.
01/07/2005: Actualizo el log al finalizar
14/07/2005: Mensaje de Ayuda, Defini tama�o de ventana minimo, Permito maximizar y ajustar los forms. Separe units del form de las generales.
26/07/2005: Quite la llamada al parametro general 'CodigodeComercio' en rutina obtener filtro porque no se utilizaba.
26/07/2005: Corregi Tabulaci�n. se perdio al migrar a la nueva version de Delphi.
26/07/2005: Obtengo los Parametros Generales que se utilizaran en el formulario al inicializar y verifico que los valores obtenidos sean validos.
31/03/2006: Cree un nuevo reporte de finalizacion, elimine los dos reportes que existian anteriormente.

Firma       :   SS_1060_CQU_20120731
Descripcion :   Se quitan los acentos del Header para el archivo de presto ya que vienen sin ellos desde el origen.

Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

Autor       :   Claudio Quezada
Fecha       :   25-03-2015
Firma       :   SS_1147_CQU_20150325
Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

  
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto
------------------------------------------------------------------------------}
unit frmRecepcionRendicionesPresto;

interface

uses
  //Recepcion de Rendiciones Presto
  ComunesInterfaces,
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  FrmRptRecepcionRendiciones,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, PeaTypes, UtilDB, DPSControls, ppDB, ppTxPipe, StrUtils,
  ppParameter, ppModule, raCodMod, ppCtrls, ppBands, ppReport, ppPrnabl,
  ppClass, ppStrtch, ppSubRpt, ppCache, ppComm, ppRelatv, ppProd, UtilRB,
  RStrings, RBSetup,ppDBPipe;

type
  TRendicionPrestoRecord = record
    EPS : string;
  	FechaCargo : TDateTime;
    GlosaCargo : string;
    NumeroTarjeta : string;
    NumeroConvenio : string;
    Debito : INT64;
    CodigoRespuesta : string;
    DescripcionRespuesta : string;
    NotaCobro : Int64;
  end;

  TfRecepcionRendicionesPresto = class(TForm)
    Bevel1: TBevel;
    btnAbrirArchivo: TSpeedButton;
    Label2: TLabel;
    edOrigen: TEdit;
    OpenDialog: TOpenDialog;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    SpObtenerResumenComprobantesRecibidos: TADOStoredProc;
    SpObtenerResumenComprobantesRecibidosMotivo: TStringField;
    SpObtenerResumenComprobantesRecibidosCantidad: TIntegerField;
    SpObtenerResumenComprobantesRecibidosMonto: TStringField;
    SPActualizarRendicionPresto: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
    { Private declarations }
    FCodigoOperacion : integer;
    FLineas: integer;
    FMontoArchivo: int64;
   	FDetenerImportacion: boolean;
  	FRendicionesTXT : TStringList;
    FErrores : TStringList;
  	FErrorMsg : string;
    FPresto_CodigodeComercio : AnsiString;
   	FPresto_Directorio_Origen_Rendicion : AnsiString;
    FPresto_Directorio_Errores : AnsiString;
    FPresto_DirectorioRendicionProcesada: AnsiString;
    FCodigoNativa : Integer;  // SS_1147_CQU_20150325
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  	function  ParsePrestoLine( sline : string; var PrestoRecord : TRendicionPrestoRecord; var sParseError : string ) : boolean;
  	function  AnalizarRendicionesTXT : boolean;
    function  CargarRendicionesPresto : boolean;
  	function  RegistrarOperacion : boolean;
  	//procedure GenerarReportesErrores;
    Function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
  public
	{ Public declarations }
  	function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
  end;

var
  fRecepcionRendicionesPresto: TfRecepcionRendicionesPresto;

Const
	RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_PRESTO		= 33;

implementation


{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 04/01/2005
  Description: Inicializa el Formulario de Recepci�n de Rendiciones Presto
  Parameters: txtCaption: ANSIString; MDIChild:Boolean
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesPresto.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 26/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        PRESTO_CODIGODECOMERCIO             = 'Presto_CodigodeComercio';
        PRESTO_DIRECTORIO_ORIGEN_RENDICION  = 'Presto_Directorio_Origen_Rendicion';
        PRESTO_DIRECTORIO_ERRORES           = 'Presto_Directorio_Errores';
        PRESTO_DIRECTORIORENDICIONPROCESADA = 'Presto_DirectorioRendicionProcesada';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la Concesionaria Nativa                  // SS_1147_CQU_20150325
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150325
                
                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_CODIGODECOMERCIO , FPresto_CodigodeComercio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_CODIGODECOMERCIO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FPresto_CodigodeComercio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + PRESTO_CODIGODECOMERCIO;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_DIRECTORIO_ORIGEN_RENDICION  , FPresto_Directorio_Origen_Rendicion) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_DIRECTORIO_ORIGEN_RENDICION;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FPresto_Directorio_Origen_Rendicion := GoodDir(FPresto_Directorio_Origen_Rendicion);
                if  not DirectoryExists(FPresto_Directorio_Origen_Rendicion) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FPresto_Directorio_Origen_Rendicion;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_DIRECTORIO_ERRORES , FPresto_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FPresto_Directorio_Errores := GoodDir(FPresto_Directorio_Errores);
                if  not DirectoryExists(FPresto_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FPresto_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,PRESTO_DIRECTORIORENDICIONPROCESADA,FPresto_DirectorioRendicionProcesada);


                ObtenerParametroGeneral(DMConnections.BaseCAC,'Presto_Directorio_Origen_Rendicion', FPresto_Directorio_Origen_Rendicion);


            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;


    //Elijo el modo en que si visualizara la ventana
  	if not MDIChild then begin
  		FormStyle := fsNormal;
  		Visible := False;
  	end;

    //Centro el form
  	CenterForm(Self);
  	try
        DMConnections.BaseCAC.Connected := True;
    		Result := DMConnections.BaseCAC.Connected and
                                          VerificarParametrosGenerales;
  	except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;

    //Resolver ac� lo que necesita este form para inicializar correctamente
   	Caption := AnsiReplaceStr(txtCaption, '&', '');
  	btnCancelar.Enabled := False;
  	btnProcesar.Enabled := False;
  	pnlAvance.Visible := False;
  	lblReferencia.Caption := '';

end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfRecepcionRendicionesPresto.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_RENDICION          = ' ' + CRLF +
                          'El Archivo de Respuesta a Debitos' + CRLF +
                          'es enviado por PRESTO al ESTABLECIMIENTO' + CRLF +
                          'por cada Archivo de Debitos recibido' + CRLF +
                          'y tiene por objeto dar a conocer al Establecimiento, ' + CRLF +
                          'el resultado del procesamiento de la informaci�n enviada.' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: RND1MMDD1D.GEN' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para registrar los pagos de los' + CRLF +
                          'comprobantes aceptados en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_RENDICION, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TfRecepcionRendicionesPresto.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoClick
  Author:    flamas
  Date Created: 23/12/2004
  Description: Abre el Archivo de Rendiciones, y verifica si no ha sido procesado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesPresto.btnAbrirArchivoClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerFiltro
      Author:    lgisuk
      Date Created: 16/03/2005
      Description: Obtengo el Filtro
      Parameters: var Filtro: AnsiString
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function ObtenerFiltro(var Filtro: AnsiString): Boolean;
    Const
        FILE_TITLE			= 'Rendiciones PRESTO|';
        FILE_NAME 			= 'RND1';
    begin
        //Creo el Filtro
    		Filtro := FILE_TITLE + FILE_NAME + '*';
        Result:= True;
    end;

    {-----------------------------------------------------------------------------
      Function Name: ValidarNombreArchivo
      Author:    flamas
      Date Created: 23/12/2004
      Description: Valida el nombre del archivo
      Parameters: sFileName : string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    function ValidarNombreArchivoPresto(sFileName, sTipoArchivo : string) : boolean;
    resourcestring
        MSG_INVALIDA_FILE_NAME = 'El nombre del archivo %s es inv�lido';
        MSG_ERROR  = 'Error';
    begin
        result :=	(Copy(ExtractFileName(sFileName), 1, 4) = sTipoArchivo) and (Copy(ExtractFileName(sFileName), 9, 2) = '1D');
        if not result then MsgBox (Format (MSG_INVALIDA_FILE_NAME, [ExtractFileName (sFileName)]), MSG_ERROR, MB_ICONERROR);
    end;

resourcestring
    MSG_ERROR = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST 	= 'El archivo %s no existe';
  	MSG_ERROR_INVALID_FILENAME 		= 'El nombre del archivo %s es inv�lido';
  	MSG_FILE_ALREADY_PROCESSED 		= 'El archivo %s ya fue procesado';
    MSG_FILE_WISH_TO_REPROCESS		= 'Desea volver a procesarlo ?';
  	FILE_NAME 						= 'RND1';
var
    Filtro: string;
begin
    //obtengo el filtro a aplicar a este tipo de archivos
    ObtenerFiltro(Filtro);

    Opendialog.InitialDir := FPresto_Directorio_Origen_Rendicion;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if opendialog.execute then begin

        edOrigen.text:=UpperCase( opendialog.filename );

     		if  not ValidarNombreArchivoPresto(edOrigen.text, FILE_NAME) then begin
           	MsgBox( Format ( MSG_ERROR_INVALID_FILENAME, [ExtractFileName(edOrigen.text) ]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

        if not FileExists( edOrigen.text ) then begin
        	  MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

      	if  VerificarArchivoProcesado( DMConnections.BaseCAC, edOrigen.text ) then begin
	      		if (MsgBox (Format (MSG_FILE_ALREADY_PROCESSED, [ExtractFileName (edOrigen.text) ]) + #13#10 + MSG_FILE_WISH_TO_REPROCESS, MSG_ERROR, MB_ICONQUESTION or MB_YESNO) <> IDYES) then Exit;
        end;

      if FPresto_DirectorioRendicionProcesada <> '' then begin
            if RightStr(FPresto_DirectorioRendicionProcesada,1) = '\' then  FPresto_DirectorioRendicionProcesada := FPresto_DirectorioRendicionProcesada + ExtractFileName(edOrigen.text)
            else FPresto_DirectorioRendicionProcesada := FPresto_DirectorioRendicionProcesada + '\' + ExtractFileName(edOrigen.text);
      end;

      btnProcesar.Enabled := True;

	  end;
end;

{-----------------------------------------------------------------------------
  Function Name: ParsePrestoLine
  Author:    flamas
  Date Created: 23/12/2004
  Description: Parsea la linea de Rendicion
  Parameters: sline : string; var PrestoRecord : TRendicionPrestoRecord
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  02/08/2007
  Cambie Debito STRtoINT a STRtoINT64
  Revision :1
      Author : vpaszkowicz
      Date : 19/11/2007
      Description : Agrego un control de l�nea y un control acerca de si la NK
      est� corrida a izquierda.

  Revision 2
  Author: mbecerra
  Date: 2-Junio-2008
  Description:  Se cambia el largo de la l�nea de 201 a 203 y la condici�n 
            	 if Length(sline)<> LARGO_LINEA_PRESTO then begin        por

                 if Length(sline) > LARGO_LINEA_PRESTO then begin
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesPresto.ParsePrestoLine( sline : string; var PrestoRecord : TRendicionPrestoRecord; var sParseError : string ) : boolean;
const
    LARGO_LINEA_PRESTO = 203;
resourcestring
  	MSG_ERROR_INVALID_COMPANY 			= 'La empresa prestadora de servicios es inv�lida';
    MSG_ERROR_DATE              		= 'La fecha de cargo es inv�lida';
    MSG_ERROR_INVALID_AMMOUNT   		= 'El monto del d�bito es inv�lido';
    MSG_ERROR_INVALID_INVOICE_NUMBER	= 'El n�mero de la nota de cobro es inv�lido';
    MSG_ERROR_INVALID_LENGTH            = 'El largo de la l�nea es incorrecto';
    MSG_ERROR_INVALID_INVOICE_POSITION  = 'El campo correspondiente a nota de cobro est� corrido a izquierda';
var
    UltimoCaracter: string;
    Caracter: char;
begin
  	Result := False;

    if Length(sline) > LARGO_LINEA_PRESTO then begin
        sParseError	:= MSG_ERROR_INVALID_LENGTH;
        Exit;
    end;
    
    with PrestoRecord do begin

		    try

            FPresto_CodigodeComercio := PADL( FPresto_CodigodeComercio, 6, '0');
            sParseError				:= MSG_ERROR_INVALID_COMPANY;
        		EPS 					:= Trim( Copy( sline, 11, 6 )); if ( EPS <>  FPresto_CodigodeComercio ) then Exit; //Codigo Empresa Prestadora
            sParseError				:= MSG_ERROR_DATE;
      			FechaCargo 				:= EncodeDate(	StrToInt('20' + Copy( sline, 17, 2 )), StrToInt(Copy( sline, 19, 2 )), StrToInt(Copy(sline, 21, 2)));
        		GlosaCargo 				:= Trim( Copy( sline, 23, 26 ));
        		NumeroTarjeta 			:= Trim( Copy( sline, 49, 19 ));
        		NumeroConvenio 			:= Trim( Copy( sline, 68, 17 ));
            //if ObtenerCodigoConcesionariaNativa=CODIGO_VS then                // SS_1147_CQU_20150325  //SS_1147Q_NDR_20141202[??]
			if FCodigoNativa = CODIGO_VS then 									// SS_1147_CQU_20150325
            begin                                                               //SS_1147Q_NDR_20141202[??]
              NumeroConvenio:=Copy(NumeroConvenio,6,12);                        //SS_1147Q_NDR_20141202[??]
            end;                                                                //SS_1147Q_NDR_20141202[??]
            sParseError				:= MSG_ERROR_INVALID_AMMOUNT;
        		Debito 					:= StrToInt64( Trim( Copy( sline, 85, 10 )));   //monto
        		CodigoRespuesta 	 	:= Trim( Copy( sline, 100, 1 )); // Toma el �ltimo digito del C�digo de Respuesta 00000'0'
            DescripcionRespuesta 	:= Trim( Copy( sline, 101, 94 ));
            UltimoCaracter := Copy(DescripcionRespuesta, Length(DescripcionRespuesta), 1);
            Caracter := UltimoCaracter[1];
            if (Caracter in ['0'..'9']) then begin
                sParseError := MSG_ERROR_INVALID_INVOICE_POSITION;
                Exit;
            end;
            sParseError				:= MSG_ERROR_INVALID_INVOICE_NUMBER;
            NotaCobro 				:= StrToInt64( Trim( Copy( sline, 195, 12 )));
            sParseError				:= '';
            result := True;

        except

        	  on exception do Exit;

        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AnalizarRendicionesTXT
  Author:    flamas
  Date Created: 06/01/2005
  Description: Antes de hacer nada Parsea todo el Archivo para verificar
  				que el parseo sea v�lido
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesPresto.AnalizarRendicionesTXT : boolean;
resourcestring
  	MSG_ANALIZING_RENDERINGS_FILE	= 'Analizando Archivo de Rendiciones - Cantidad de lineas : %d';
    //MSG_RENDERINGS_FILE_HAS_ERRORS 	= 'El archivo de Rendiciones contiene errores'#10#13'L�nea : %d';
    MSG_RENDERINGS_FILE_HAS_ERRORS 	= 'Error en L�nea : %d';
    MSG_ERROR					 	= 'Error';
var
  	nNroLineaScript, nLineasScript :integer;
    FRendicionRecord : TRendicionPrestoRecord;
    sParseError : string;
begin
    FMontoArchivo := 0;
    Screen.Cursor := crHourglass;
    Result := True;
    nLineasScript := FRendicionesTXT.Count - 2;
    lblReferencia.Caption := Format( MSG_ANALIZING_RENDERINGS_FILE, [nLineasScript] );
    pbProgreso.Position := 1;
    pbProgreso.Max := FRendicionesTXT.Count - 2;
    pnlAvance.Visible := True;

    nNroLineaScript := 1;

    while ( nNroLineaScript <= nLineasScript ) and ( not FDetenerImportacion ) do begin //Comentado//and (	FErrorMsg = '' ) 	do begin

           	if not ParsePrestoLine( FRendicionesTXT[nNroLineaScript], FRendicionRecord, sParseError ) then begin
    			      // Si encuentra un error termina
                FErrorMsg := Format( MSG_RENDERINGS_FILE_HAS_ERRORS, [nNroLineaScript] ) + ' ' + sParseError;
                {MsgBox(FErrorMsg, Caption, MB_ICONERROR);
        				Screen.Cursor := crDefault;}
                FErrores.Add(FErrorMsg);
              	result := False;
                //Comentado//Exit;
            end
            else
                FMontoArchivo := FMontoArchivo + FRendicionRecord.Debito;
		   	Inc( nNroLineaScript );
          	pbProgreso.Position := nNroLineaScript;
          	Application.ProcessMessages;
    end;
    FLineas := nNroLineaScript - 1;
    result := Result and (not FDetenerImportacion);
  	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 23/12/2004
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesPresto.RegistrarOperacion : boolean;
resourcestring
  	MSG_ERRORS 							= 'Errores : ';
  	MSG_COULD_NOT_REGISTER_OPERATION 	= 'No se pudo registrar la operaci�n';
    MSG_ERROR 							= 'Error';
var
	  sDescrip : string;
    DescError : string;
begin
    sDescrip := iif( FErrores.Count > 0, '', MSG_ERRORS + IntToStr( FErrores.Count ));
  	result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_PRESTO, ExtractFileName(edOrigen.text), UsuarioSistema, sDescrip, True, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, FLineas, FMontoArchivo, DescError);
    if not result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: CargarRendicionesPresto
  Author:    flamas
  Date Created: 23/12/2004
  Description: Carga
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  14/03/07
  Ahora al llamar a registrar el pago informo si se trata de un reintento
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesPresto.CargarRendicionesPresto : boolean;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerFechaRecepcion
      Author:    flamas
      Date Created: 05/01/2005
      Description: Obtiene la fecha de recepci�n del Nombre del archivo
      Parameters: None
      Return Value: TDateTime
    -----------------------------------------------------------------------------}
    function  ObtenerFechaRecepcion : TDateTime;
    var
        sFecha : string;
    begin
    	  sFecha := Copy(FRendicionesTXT[0], 58, 6);
    	  result :=	EncodeDate(	StrToInt( '20' + Copy(sFecha, 1, 2)), StrToInt(Copy(sFecha, 3, 2)), StrToInt(Copy(sFecha, 5, 2)));
    end;

resourcestring
    MSG_PROCESSING_RENDERINGS_FILE			= 'Procesando archivo de Rendiciones - Cantidad de lineas : %d';
    MSG_ERROR_SAVING_INVOICE            = 'Error al Asentar la Operaci�n Comprobante: %d  ';
  	MSG_ERROR								            = 'Error';
var
  	nNroLineaScript, nLineasScript : integer;
    FRendicionRecord : TRendicionPrestoRecord;
    sDescripcionError : string;
    sParseError : string;
    //
    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
begin
  	Screen.Cursor := crHourglass;

  	nLineasScript := FRendicionesTXT.Count - 2;
    lblReferencia.Caption := Format( MSG_PROCESSING_RENDERINGS_FILE, [nLineasScript] );
    pbProgreso.Position := 0;
    pbProgreso.Max := FRendicionesTXT.Count - 2;
    pnlAvance.Visible := True;

    nNroLineaScript := 1;

    while ( nNroLineaScript <= nLineasScript ) and ( not FDetenerImportacion ) and (	FErrorMsg = '' ) do begin

        //Intentamos registrar el pago de un comprobante.
        Reintentos  := 0;

        //Obtengo los datos de la rendicion
        if ParsePrestoLine( FRendicionesTXT[nNroLineaScript], FRendicionRecord, sParseError ) then begin

            while Reintentos < 3 do begin

                  with FRendicionRecord, spActualizarRendicionPresto, Parameters do begin
                      try
                          ParamByName( '@CodigoOperacionInterfase' ).Value 	:= FCodigoOperacion;
                          ParamByName( '@FechaRecepcion' ).Value 				:= ObtenerFechaRecepcion;
                          ParamByName( '@FechaCargo' ).Value 					:= FechaCargo;
                          ParamByName( '@NumeroTarjeta' ).Value 				:= NumeroTarjeta;
                          ParamByName( '@NumeroConvenio' ).Value 				:= NumeroConvenio;
                          ParamByName( '@Debito' ).Value 						:= Debito;
                          ParamByName( '@CodigoRespuesta' ).Value 			:= CodigoRespuesta;
                          ParamByName( '@DescripcionRespuesta' ).Value 		:= DescripcionRespuesta;
                          ParamByName( '@NumeroComprobante' ).Value 			:= NotaCobro;
                          ParamByName( '@Usuario' ).Value 					:= UsuarioSistema;
                          ParamByName( '@Reintento' ).Value 					:= Reintentos;
                          ParamByName( '@DescripcionError' ).Value  := '';
                          CommandTimeOut := 500;
                          ExecProc;

                          sDescripcionError := Trim( VarToStr( ParamByName('@DescripcionError' ).Value ));
                          if ( sDescripcionError <> '' ) then FErrores.Add( StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]));

                          //1/4 de segundo entre registracion de pago y otro
                          Sleep(25);
                          //Salgo del ciclo por Ok
                          Break;

                      except
                          on E : exception do begin
                              if (pos('deadlock', e.Message) > 0) then begin
                                  mensajeDeadlock := e.message;
                                  inc(Reintentos);
                                  sleep(2000);
                              end else begin
                                  FErrorMsg := Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]);
                                  MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]), e.Message, MSG_ERROR, MB_ICONERROR);
                                  Break;
                              end;
                          end;
                      end
                  end;

            end;

        end;

        //una vez que se reintento 3 veces x deadlock lo informo
        if Reintentos = 3 then begin
            FErrorMsg := Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]);
            MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]), mensajeDeadLock, MSG_ERROR, MB_ICONERROR);
        end;

        Inc( nNroLineaScript );
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

  	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Genero el Reporte de Finalizacion de Proceso. sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TfRecepcionRendicionesPresto.GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
const
    STR_TITLE = 'Comprobantes Recibidos';
var
    FRecepcion : TFRptRecepcionRendiciones;
begin
    Result:=false;
    try
        //muestro el reporte
        Application.CreateForm(TFRptRecepcionRendiciones, FRecepcion);
        if not FRecepcion.Inicializar(STR_TITLE, CodigoOperacionInterfase) then fRecepcion.Release;
        Result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    flamas
  Date Created: 04/01/2005
  Description: Procesa el archivo de d�bitos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesPresto.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/06/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name:  ObtenerResumen
      Author:    lgisuk
      Date Created: 04/04/2006
      Description:  Obtengo el resumen del proceso realizado
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    procedure ObtenerResumen (CodigoOperacionInterfase : Integer;Var CantRegs, Aprobados, Rechazados : Integer);
    resourcestring
      MSG_ERROR = 'Error';
      MSG_ERROR_QUERY = 'Error Obteniendo Resumen';
    begin
      try
          With spObtenerResumenComprobantesRecibidos, Parameters do begin
              Close;
              ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
              CommandTimeout := 500;
              Open;
              CantRegs := Parameters.ParamByName('@CantidadComprobantesRecibidos').Value;
              Aprobados := Parameters.ParamByName('@CantidadComprobantesAceptados').Value;
              Rechazados := Parameters.ParamByName('@CantidadComprobantesRechazados').Value;
              Close;
          end;
      except
          on E : Exception do begin
              MsgBoxErr(MSG_ERROR_QUERY, E.Message, MSG_ERROR, MB_ICONERROR);
          end;
      end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 01/07/2005
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog(CantidadErrores:integer):boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : string;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_SUCCEDED 					= 'El proceso finaliz� con �xito';
    MSG_PROCESS_FINISH_WITH_ERRORS	        = 'El proceso finaliz� con errores.';
    MSG_PROCESS_COULD_NOT_BE_COMPLETED 		= 'El proceso no se pudo completar';
    MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE 	= 'No se puede abrir el archivo de rendiciones';
    MSG_ERRROR_RENDERINGS_FILE_IS_EMPTY 	= 'El archivo seleccionado est� vac�o';
    //PRESTO_RENDERINGS_DESCRIPTION 			= 'Rendici�n Presto sobre Estado de D�bitos - ';  //Ok Revisado 16/03/05  // SS_1060_CQU_20120731
    PRESTO_RENDERINGS_DESCRIPTION 			= 'Rendicion Presto sobre Estado de Debitos - ';                              // SS_1060_CQU_20120731 se quitan los acentos
    MSG_ERROR = 'Error';
    MSG_PROCESS_COULD_NOT_BE_COMPLETED_ERROR_FILE = 'El proceso no se pudo completar porque el archivo contiene errores de sintaxis. Corrija los errores y vuelva a procesarlo';
    CONST_REPORTE_PRESTO = 'Presto - Procesar Archivo de Rendiciones';
Var
    //TotalRegistros: integer;
    CantidadErrores: integer;
    CantRegs, Aprobados, Rechazados : Integer;
begin
 	// Crea las listas
	FRendicionesTXT := TStringList.Create;
  FErrores := TStringList.Create;
	FErrorMsg := '';

	// Deshabilita los botones
	btnCancelar.Enabled := True;
	btnProcesar.Enabled := False;
	btnSalir.Enabled := False;
	btnAbrirArchivo.Enabled := False;
	edOrigen.Enabled := False;

	FDetenerImportacion := False;
	try
		try
      //Lee el archivo del Rendicones
			FRendicionesTXT.text:= FileToString(edOrigen.text);
			//Verifica si el Archivo Contiene alguna linea
			if ( FRendicionesTXT.Count = 0 ) then
				MsgBox(MSG_ERRROR_RENDERINGS_FILE_IS_EMPTY, Caption, MB_ICONERROR)
			else begin
                //TotalRegistros := FRendicionesTXT.Count - 2;
                FPresto_CodigodeComercio := PADL(FPresto_CodigodeComercio, 6, '0');
            	if ValidarHeaderFooterPresto( FRendicionesTXT, edOrigen.Text, FPresto_CodigodeComercio, PRESTO_RENDERINGS_DESCRIPTION ) and
                	AnalizarRendicionesTXT then begin
                    //Inicia una transacci�n para toda la Importaci�n
                    //DMConnections.BaseCAC.BeginTrans;                                       //SS_1385_NDR_20150922
                    DMConnections.BaseCAC.Execute('BEGIN TRAN frmRecepcionRendicionesPresto');     //SS_1385_NDR_20150922
                    if RegistrarOperacion and
                    	CargarRendicionesPresto then begin
                            //Acepto la Transaccion
                            //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
                            DMConnections.BaseCAC.Execute('COMMIT TRAN frmRecepcionRendicionesPresto');					//SS_1385_NDR_20150922
                            //Obtengo la cantidad de errores
                            ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);
                            //Actualizo el log al Final
                            ActualizarLog(CantidadErrores);
                            //Obtengo resumen
                            ObtenerResumen (FCodigoOperacion, CantRegs, Aprobados, Rechazados);
                            //Muestro cartel de Finalizacion
                            if MsgProcesoFinalizado(MSG_PROCESS_SUCCEDED,MSG_PROCESS_FINISH_WITH_ERRORS,Caption, CantRegs, Aprobados, Rechazados) then begin
                	  						//Muestro el Reporte de Finalizacion del Proceso
                              	GenerarReportedeFinalizacion(FCodigoOperacion);
                                //Agregado porque si voy a usar la lista para mostrar errores de
                                //sintaxis, debo limpiarla ac� porque los errores fueron de proceso.
                                FErrores.Clear;
                            end;
                            //Muevo el archivo procesado
                            MoverArchivoProcesado(Caption,edOrigen.Text, FPresto_DirectorioRendicionProcesada);
                    end else begin
                    	  //En caso de Error Vuelve todo atr�s
                        //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                        DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION frmRecepcionRendicionesPresto END');	//SS_1385_NDR_20150922
                        //Muestro cartel indicando que la operacion no se pudo completar
                        MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
                    end;
                end
                else
                    MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED_ERROR_FILE, Caption, MB_ICONERROR);
			end;
		except
			on E : Exception do begin
  				FErrorMsg := MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE;
  				MsgBoxErr(MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE, e.Message,  MSG_ERROR, MB_ICONERROR);
			end;
		end;
	finally
        //Si hubo errores los muestro en un reporte en comunesInterfaces
        if (Ferrores.Count > 0) then
            GenerarReporteErroresSintaxis(CONST_REPORTE_PRESTO,FErrores, FPresto_Directorio_Errores, RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_PRESTO);
        //Libera las listas
    	FreeAndNil(FRendicionesTXT);
    	FreeAndNil(FErrores);
    	//Lo desactiva para poder Cerrar el Form
        btnCancelar.Enabled := False;
        Close;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    flamas
  Date Created: 07/12/2004
  Description: Detiene el proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesPresto.btnCancelarClick(Sender: TObject);
resourcestring
  	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Permito salir si no esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesPresto.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
  	CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesPresto.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Lo libero de Memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesPresto.FormClose(Sender: TObject;var Action: TCloseAction);
begin
   Action := caFree;
end;

end.
