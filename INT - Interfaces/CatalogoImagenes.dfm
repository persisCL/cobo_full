object frmCatalogo: TfrmCatalogo
  Left = 106
  Top = 61
  Caption = 'Cat'#225'logo de im'#225'genes de imprenta'
  ClientHeight = 567
  ClientWidth = 809
  Color = clBtnFace
  Constraints.MinHeight = 605
  Constraints.MinWidth = 825
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 9
    Top = 20
    Width = 43
    Height = 13
    Caption = 'N'#250'mero :'
  end
  object Label3: TLabel
    Left = 9
    Top = 49
    Width = 36
    Height = 13
    Caption = 'Fecha :'
  end
  object GroupB: TPanel
    Left = 0
    Top = 461
    Width = 809
    Height = 67
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    ExplicitTop = 465
    ExplicitWidth = 817
    object Label7: TLabel
      Left = 10
      Top = 11
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 11
      Top = 38
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtNombre: TEdit
      Left = 88
      Top = 8
      Width = 385
      Height = 21
      TabOrder = 0
    end
    object txtDescripcion: TEdit
      Left = 88
      Top = 35
      Width = 385
      Height = 21
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 528
    Width = 809
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 532
    ExplicitWidth = 817
    object Notebook: TNotebook
      Left = 620
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 176
    Top = 2
    Width = 104
    Height = 27
    BevelOuter = bvNone
    TabOrder = 2
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 809
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbar1Close
    ExplicitWidth = 817
  end
  object ListaImagenes: TAbmList
    Left = 0
    Top = 33
    Width = 809
    Height = 428
    TabStop = True
    TabOrder = 4
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'217'#0'Nombre de la Im'#225'gen                                    '
      #0'64'#0'Descripci'#243'n')
    HScrollBar = True
    RefreshTime = 100
    Table = tblCatalogo
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaImagenesClick
    OnProcess = ListaImagenesProcess
    OnDrawItem = ListaImagenesDrawItem
    OnRefresh = ListaImagenesRefresh
    OnInsert = ListaImagenesInsert
    OnDelete = ListaImagenesDelete
    OnEdit = ListaImagenesEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
    ExplicitWidth = 817
    ExplicitHeight = 432
  end
  object tblCatalogo: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'CatalogoImagenesImprenta'
    Left = 49
    Top = 65
  end
  object spActualizarCatalogoImprenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarCatalogoImagenesImprenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Accion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Codigo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 84
    Top = 64
  end
end
