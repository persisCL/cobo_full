unit ABMInterfaces;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DB, DBTables, ExtCtrls, DbList, Util, Menus, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  Mask, ADODB, PeaTypes, ComCtrls, DMConnection,
  FrmRptInformeUsuario, Variants, PeaProcs, DPSControls, CheckLst, VariantComboBox;

type

  PConsVar = ^TConsVar;
  TConsVar = record
	VarName: String[40];
	VarDescri: String[60];
	VarType: String[1];
    VarQuery: String;
  end;

  TFormABMInterfaces = class(TForm)
	Panel2: TPanel;
	DBList1: TAbmList;
	AbmToolbar1: TAbmToolbar;
    PageControl: TPageControl;
    tab_General: TTabSheet;
    Label1: TLabel;
    txt_descripcion: TEdit;
    tab_Consulta: TTabSheet;
    qry_Interfaces: TADOQuery;
    Notebook: TNotebook;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    qry_Sistemas: TADOQuery;
    txt_Detalle: TMemo;
    Label5: TLabel;
    cb_Categoria: TComboBox;
    Label7: TLabel;
    txt_consulta: TMemo;
    tab_Sistemas: TTabSheet;
    ckSistemas: TVariantCheckListBox;
    tab_Archivo: TTabSheet;
    Label2: TLabel;
    cb_Tipo: TComboBox;
    txt_Directorio: TEdit;
    Label3: TLabel;
    btnDirArchivosImagenes: TSpeedButton;
    Label4: TLabel;
    cb_Interfaz: TComboBox;
    txt_Separador: TEdit;
    Label6: TLabel;
	function  DBList1Process(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure DBList1Insert(Sender: TObject);
	procedure DBList1Edit(Sender: TObject);
	procedure DBList1Delete(Sender: TObject);
	procedure DBList1Click(Sender: TObject);
	procedure DBList1Refresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
	  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure btnDirArchivosImagenesClick(Sender: TObject);
    procedure AbmToolbar1Print(Sender: TObject);
    procedure cb_TipoClick(Sender: TObject);
  private
	{ Private declarations }
    FMenuInterfaces: TMenuItem;
    procedure CargarSistemasInterfaz(Conn: TADOConnection; CodigoInterfaz: integer);
    procedure Limpiar_Campos;
  public
	{ Public declarations }
	Function Inicializar(MenuInterfaces: TMenuItem): boolean;
  end;

var
  FormABMInterfaces  : TFormABMInterfaces;

implementation

{$R *.DFM}

function TFormABMInterfaces.Inicializar(MenuInterfaces: TMenuItem): boolean;
var S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	Notebook.PageIndex := 0;
    PageControl.ActivePageIndex := 0;
    FMenuInterfaces := MenuInterfaces;
	if not OpenTables([qry_Interfaces, qry_Sistemas]) then
		Result := False
	else begin
   	    CargarCategoriasInterfaces(DMConnections.BaseCAC, cb_Categoria);
       	While not qry_Sistemas.Eof do begin
            ckSistemas.Items.Add(Trim(qry_Sistemas.FieldByName('Descripcion').AsString), qry_Sistemas.FieldByName('CodigoSistema').AsInteger);
     	    qry_Sistemas.Next;
        end;
		DbList1.Reload;
		Result := True;
	end;
end;

procedure TFormABMInterfaces.Limpiar_Campos;
var
    i: integer;
begin
	txt_Descripcion.Clear;
    txt_Detalle.Clear;
	txt_Consulta.Clear;
   	for i := 0 to ckSistemas.Count - 1 do begin
        ckSistemas.Checked[i] := False;
    end;
    txt_Directorio.Clear;
    cb_Tipo.ItemIndex := 0;
    txt_Separador.Clear;
    cb_Interfaz.ItemIndex := 0;
end;

function TFormABMInterfaces.DBList1Process(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	Texto := Tabla.FieldByName('CodigoInterfaz').AsString + ' ' +
      Tabla.FieldByName('Descripcion').AsString;
	Result := True;
end;

procedure TFormABMInterfaces.BtSalirClick(Sender: TObject);
begin
	 Close;
end;

procedure TFormABMInterfaces.DBList1Insert(Sender: TObject);
begin
	Tab_General.Enabled := True;
	Tab_Consulta.Enabled := True;
    Tab_Sistemas.Enabled := True;
    Tab_Archivo.Enabled := True;
	Limpiar_Campos;
    txt_Directorio.Text := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT DirArchivosInterfaces FROM Parametros WHERE ID = 1'));
	DbList1.Enabled := False;
	Notebook.PageIndex := 1;
	PageControl.ActivePage := Tab_General;
	txt_Descripcion.SetFocus;
end;

procedure TFormABMInterfaces.DBList1Edit(Sender: TObject);
begin
	Tab_General.Enabled := True;
	Tab_Consulta.Enabled := True;
    Tab_Sistemas.Enabled := True;
    Tab_Archivo.Enabled := True;
	DbList1.Enabled := False;
	Notebook.PageIndex := 1;
end;

procedure TFormABMInterfaces.DBList1Delete(Sender: TObject);
resourcestring
    MSG_ELIMINAR_CONSULTA     = '�Est� seguro de querer eliminar esta interfaz?';
    MSG_ERROR_ELIMINAR        = 'No se pudo eliminar la interfaz.';
    CAPTION_ELIMINAR_CONSULTA = 'Eliminar Interfaz Personalizada';
var
    CodigoInterfaz: LongInt;
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_ELIMINAR_CONSULTA, CAPTION_ELIMINAR_CONSULTA, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            DMConnections.BaseCAC.BeginTrans;
   			// Eliminamos los sistemas de la interfaz
			CodigoInterfaz := TDBList(Sender).Table.FieldByName('CodigoInterfaz').AsInteger;
			QueryExecute(DMConnections.BaseCAC, 'DELETE FROM InterfacesSistemas WHERE ' +
			  'CodigoInterfaz = ' + IntToStr(CodigoInterfaz));
            qry_Interfaces.Delete;
            DMConnections.BaseCAC.CommitTrans;
		Except
			On E: Exception do begin
                qry_Interfaces.Cancel;
                DMConnections.BaseCAC.RollbackTrans;
				MsgBoxErr(MSG_ERROR_ELIMINAR, e.message, CAPTION_ELIMINAR_CONSULTA, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
        FMenuInterfaces.Clear;
        CargarInterfaces(DMConnections.BaseCAC, SYS_GESTION_INTERFACES, FMenuInterfaces);
	end;
	DbList1.Estado := Normal;
	DbList1.Enabled := True;
	Tab_General.Enabled := False;
	Tab_Consulta.Enabled := False;
    Tab_Sistemas.Enabled := False;
    Tab_Archivo.Enabled := False;
	Notebook.PageIndex := 0;
	Screen.Cursor := crDefault;
end;

procedure TFormABMInterfaces.DBList1Click(Sender: TObject);
begin
	with qry_Interfaces do begin
		txt_Descripcion.Text := Trim(FieldByName('Descripcion').AsString);
   	    CargarCategoriasInterfaces(DMConnections.BaseCAC, cb_Categoria, FieldByName('CodigoCategoria').AsInteger);
        txt_Detalle.Text := Trim(FieldByName('Detalle').AsString);
		txt_Consulta.Text := Trim(FieldByName('TextoSQL').AsString);
        CargarSistemasInterfaz(DMConnections.BaseCAC, FieldByName('CodigoInterfaz').AsInteger);
        txt_Directorio.Text := Trim(FieldByName('Directorio').AsString);
        if (FieldByName('Tipo').AsString = 'TXT') then cb_Tipo.ItemIndex := 0 else cb_Tipo.ItemIndex := 1;
        if (FieldByName('Separador').IsNull = False) then txt_Separador.Text := FieldByName('Separador').AsString else txt_Separador.Clear;
        if (FieldByName('Interfaz').AsString = 'E') then cb_Interfaz.ItemIndex := 0 else cb_Interfaz.ItemIndex := 1;
        cb_TipoClick(Sender);
	end;
end;

procedure TFormABMInterfaces.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormABMInterfaces.AbmToolbar1Close(Sender: TObject);
begin
	Close;
end;

procedure TFormABMInterfaces.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Tabla.FieldByName('CodigoInterfaz').AsString);
		TextOut(Cols[1], Rect.Top, Tabla.FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormABMInterfaces.FormShow(Sender: TObject);
begin
	DBList1.Reload;
end;

procedure TFormABMInterfaces.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_ACTUALIZAR_INTERFAZ = 'No se pudieron actualizar los datos de la interfaz.';
Var
	i, CodigoInterfaz: Integer;
begin
	Screen.Cursor := crHourGlass;
	With qry_Interfaces do begin
		Try
			if DbList1.Estado = Alta then begin
				Append;
				FieldByName('CodigoInterfaz').AsString := QueryGetValue(DMConnections.BaseCAC,
    				  'SELECT ISNULL(MAX(CodigoInterfaz) + 1, 1) FROM Interfaces');
			end else begin
				Edit;
			end;
			FieldByName('Descripcion').AsString := txt_Descripcion.text;
            FieldByName('CodigoCategoria').AsInteger := Ival(StrRight(cb_Categoria.Text, 10));
			FieldByName('Detalle').Value := IIf((txt_Detalle.text = ''), NULL, txt_Detalle.text);
			FieldByName('TextoSQL').AsString := txt_Consulta.Text;
			FieldByName('Directorio').AsString := txt_Directorio.Text;
			FieldByName('Tipo').AsString := cb_Tipo.Text;
            if (cb_Tipo.Text = 'TXT') then FieldByName('Separador').AsString := txt_Separador.Text else FieldByName('Separador').Value := NULL;
			FieldByName('Interfaz').AsString := cb_Interfaz.Text;
			Post;
			// Eliminamos los sistemas de la interfaz
			CodigoInterfaz := FieldByName('CodigoInterfaz').AsInteger;
			QueryExecute(DMConnections.BaseCAC, 'DELETE FROM InterfacesSistemas WHERE ' +
			  'CodigoInterfaz = ' + IntToStr(CodigoInterfaz));
			// Agregamos los sistemas de la interfaz
			for i := 0 to ckSistemas.Items.Count - 1 do begin
				if (ckSistemas.Checked[i] = True) then begin
					QueryExecute(DMConnections.BaseCAC, 'INSERT INTO InterfacesSistemas ' +
					  '(CodigoInterfaz, CodigoSistema) ' +
					  'VALUES (' + IntToStr(CodigoInterfaz) + ', ' +
					  IntToStr(ckSistemas.Items[i].Value) +  ')');
				end;
			end;
		except
			On E: EDataBaseError do begin
				Cancel;
            	Screen.Cursor := crDefault;
				MsgBoxErr(MSG_ACTUALIZAR_INTERFAZ, E.message, Self.Caption, MB_ICONSTOP);
			end;
		end;
	end;
    FMenuInterfaces.Clear;
    CargarInterfaces(DMConnections.BaseCAC, SYS_GESTION_INTERFACES, FMenuInterfaces);
	DbList1.Estado := Normal;
	DbList1.Enabled := True;
	Tab_General.Enabled := False;
	Tab_Consulta.Enabled := False;
    Tab_Sistemas.Enabled := False;
    Tab_Archivo.Enabled := False;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	DbList1.SetFocus;
	Screen.Cursor := crDefault;
end;

procedure TFormABMInterfaces.BtnCancelarClick(Sender: TObject);
begin
	DbList1.Estado := Normal;
	DbList1.Enabled := True;
	DbList1.SetFocus;
	Tab_General.Enabled := False;
	Tab_Consulta.Enabled := False;
    Tab_Sistemas.Enabled := False;
    Tab_Archivo.Enabled := False;
	Notebook.PageIndex := 0;
end;

procedure TFormABMInterfaces.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormABMInterfaces.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormABMInterfaces.CargarSistemasInterfaz(Conn: TADOConnection; CodigoInterfaz: integer);
Var
	i: Integer;
	Qry: TADOQuery;
begin
   	for i := 0 to ckSistemas.Count - 1 do begin
        ckSistemas.Checked[i] := False;
    end;
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT CodigoSistema FROM InterfacesSistemas WHERE CodigoInterfaz = ' + IntToStr(CodigoInterfaz);
		Qry.Open;
		While not Qry.Eof do begin
        	for i := 0 to ckSistemas.Count - 1 do begin
                if (ckSistemas.Items[i].Value = Qry.FieldByName('CodigoSistema').AsInteger) then ckSistemas.Checked[i] := True;
            end;
			Qry.Next;
		end;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure TFormABMInterfaces.btnDirArchivosImagenesClick(Sender: TObject);
var
    DirArchivosImagenes: AnsiString;
begin
    DirArchivosImagenes := Trim(BrowseForFolder(Self));
    if not (DirArchivosImagenes = '') then txt_Directorio.Text := DirArchivosImagenes;
end;

procedure TFormABMInterfaces.AbmToolbar1Print(Sender: TObject);
begin
    EjecutarInterfaz(DMConnections.BaseCAC, qry_Interfaces.FieldByName('CodigoInterfaz').AsInteger);
end;

procedure TFormABMInterfaces.cb_TipoClick(Sender: TObject);
begin
    if (cb_Tipo.Text = 'TXT') then txt_Separador.Visible := true else txt_Separador.Visible := false;
    Label6.Visible := txt_Separador.Visible;
end;

end.
