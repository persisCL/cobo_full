unit ABMFirmasEmail;
{
Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls;

type
  TFormFirmasEmail = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    FirmasEmail: TADOTable;
    txt_CodigoFirma: TNumericEdit;
    Notebook: TNotebook;
    txtFirma: TMemo;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    FirmasEmailCodigoFirma: TAutoIncField;
    FirmasEmailFirma: TStringField;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormFirmasEmail: TFormFirmasEmail;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar esta Firma?';
    MSG_DELETE_ERROR		= 'No se puede eliminar esta Firma porque hay datos que dependen de ella.';
    MSG_DELETE_CAPTION		= 'Eliminar Firma';
    MSG_ACTUALIZAR_ERROR  	= 'No se pudieron actualizar los datos de la Firma.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Firma';


{$R *.DFM}

procedure TFormFirmasEmail.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
    txt_CodigoFirma.Enabled	:= True;
end;

function TFormFirmasEmail.Inicializa: boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Notebook.PageIndex := 0;
	if not OpenTables([FirmasEmail]) then
		Result := False
	else begin
		Result := True;
		DbList1.Reload;
	end;
end;

procedure TFormFirmasEmail.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormFirmasEmail.DBList1Click(Sender: TObject);
begin
	with FirmasEmail do begin
		txt_CodigoFirma.Value	:= FieldByName('CodigoFirma').AsInteger;
		txtFirma.lines.text	:= FieldByName('Firma').AsString;
	end;
end;

procedure TFormFirmasEmail.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 1 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormFirmasEmail.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado     := modi;
end;

procedure TFormFirmasEmail.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCampos;
    dblist1.Estado     := Alta;
end;

procedure TFormFirmasEmail.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormFirmasEmail.Limpiar_Campos;
begin
	txt_codigoFirma.Clear;
	txtFirma.Lines.Clear;
end;

procedure TFormFirmasEmail.AbmToolbar1Close(Sender: TObject);
begin
    close;
end;

procedure TFormFirmasEmail.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			FirmasEmail.Delete;
		Except
			On E: Exception do begin
				FirmasEmail.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	VolverCampos;
	Screen.Cursor      := crDefault;
end;

procedure TFormFirmasEmail.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_VERIFICAR_FIRMA   = 'Debe llenar la informaci�n de la firma';
//var
  //  FirmaAnt: string;
begin
    If ValidateControls([TxtFirma], [TRIM(TxtFirma.Text) <> ''],'Faltan Datos', [MSG_VERIFICAR_FIRMA]) then
    begin
        Screen.Cursor := crHourGlass;
        With FirmasEmail do begin
            Try
                if DbList1.Estado = Alta then begin
                    Append;
                end else Edit;
                FieldByName('Firma').AsString			:= txtFirma.text;
                Post;
            except
                On E: EDataBaseError do begin
                    Cancel;
                    MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                    Exit;
                end;
            end;
        end;
        VolverCampos;
        Screen.Cursor	:= crDefault;
    end;
end;

procedure TFormFirmasEmail.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormFirmasEmail.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormFirmasEmail.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormFirmasEmail.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    txt_CodigoFirma.Enabled	:= False;
    txtFirma.SetFocus;
end;

end.
