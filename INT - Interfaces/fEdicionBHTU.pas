unit fEdicionBHTU;

interface

uses
    PeaProcs, Util, UtilProc, PeaTypes, UtilDB, DMConnection, ADODB,  ComunesInterfaces,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Validate, DateEdit, ComCtrls, VariantComboBox, DB;

type
  TfrmEdicionBHTU = class(TForm)
    btnAceptar: TButton;
    btnCancelar: TButton;
    txtPatente: TEdit;
    deFechaVenta: TDateEdit;
    lblFechaUso: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    lblNumeroDayPass: TLabel;
    Bevel: TBevel;
    pbProgreso: TProgressBar;
    lblDetalle: TLabel;
    cbCategoria: TVariantComboBox;
    spLiberarBHTU: TADOStoredProc;
    spModificarBHTU: TADOStoredProc;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    FCategoria,
    FNumeroDaypass      : integer;
    FFechaUso,
    FFechaVenta         : TDateTime;
    FPatente            : AnsiString;
    FIdentificador      : AnsiString;
    FUsado              : Boolean;
    function CargarCategoriasBHTU(Conn: TADOConnection; Combo: TVariantComboBox; var Error: AnsiString): Boolean;
    function HayCambios: Boolean;
    function VerificarPatente(Patente: AnsiString): boolean;
    function ModificarBHTU(var CodigoHistoricoBHTU: Integer; var Error: AnsiString): boolean;
    function LiberarTransitos(CodigoHistoricoBHTU: Integer; var Error: AnsiString): boolean;
  public
    { Public declarations }
    function Inicializar(NumeroDaypass, Categoria : integer;
                          FechaVenta              : TDateTime;
                          Patente                 : AnsiString;
                          Identificador           : AnsiString;
                          Usado                   : Boolean;
                          FechaUso                : TDateTime = NULLDATE): boolean;
  end;

var
  frmEdicionBHTU: TfrmEdicionBHTU;

implementation

{$R *.dfm}

{ TfrmEdicionBHTU }

{******************************** Function Header ******************************
Function Name: CargarCategoriasBHTU
Author : ndonadio
Date Created : 10/10/2005
Description :   Carga el combo de categorias. Esta armada como para
                meter esta funcion en Comunes INterfases, PeaPRocs, o alguna
                unit de ese estilo...
Parameters : Conn: TADOConnection; Combo: TVariantComboBox; var Error: AnsiString
Return Value : Boolean



Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto


*******************************************************************************}
function TfrmEdicionBHTU.CargarCategoriasBHTU(Conn: TADOConnection; Combo: TVariantComboBox; var Error: AnsiString): Boolean;
Var
	Qry: TADOQuery;
begin
    Result := False;
	Qry := TAdoQuery.Create(nil);
	try
        try
            Qry.Connection := Conn;
            Qry.SQL.Text :=
              ' SELECT DISTINCT C.DESCRIPCION, C.CATEGORIA ' +
              ' FROM CATEGORIAS C (NOLOCK)' +
              ' INNER JOIN CATEGORIASDAYPASS CD (NOLOCK) ON CD.CODIGOCATEGORIA = C.CATEGORIA' +
              ' INNER JOIN TARIFASDAYPASS TD (NOLOCK) ON TD.CODIGOTARIFADAYPASS = CD.CODIGOTARIFADAYPASS' +
              ' WHERE TD.TIPODAYPASS = ''T'' ' +
              ' ORDER BY C.CATEGORIA ASC';
            Qry.Open;
            Combo.Clear;
            While not Qry.Eof do begin
                Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,Qry.FieldByName('Categoria').AsInteger);
                Qry.Next;
            end;
            Combo.ItemIndex := -1;
            Result := True;
        except
            on e:exception do begin
                Error := e.Message;
                Exit;
            end;
        end;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 10/10/2005
Description :   Inicializ el dialog, carga los valroes del BHTU en edicion
                y setea valores iniciales de variables...
Parameters : NumeroDaypass, Categoria: integer; FechaVenta: TDateTime; Patente, Identificador: AnsiString; Usado: Boolean; FechaUso: TDateTime
Return Value : boolean
*******************************************************************************}
function TfrmEdicionBHTU.Inicializar(NumeroDaypass, Categoria: integer; FechaVenta: TDateTime;
  Patente, Identificador: AnsiString; Usado: Boolean; FechaUso: TDateTime): boolean;
resourcestring
    MSG_USADO       = 'BHTU Usado el d�a %s';
    MSG_NRO_DAYPASS = 'BHTU N� %d, (%s)';

    ERROR_CANT_LOAD_CATEGORIES  = 'No se pueden cargar las categor�as de BHTU';
var
    descError: AnsiString;
begin
    Result := False;

    if not CargarCategoriasBHTU(DMConnections.BaseCAC, cbCategoria, descError) then begin
        MsgBoxErr( ERROR_CANT_LOAD_CATEGORIES, descError, Caption, MB_ICONERROR);
        Exit;
    end;

    FNumeroDaypass  := NumeroDayPass;
    FIdentificador  := TRIM(Identificador);
    lblNumeroDayPass.Caption := Format(MSG_NRO_DAYPASS, [FNumeroDaypass, FIdentificador]);

    FCategoria      := Categoria;
    // busco la categoria y pongo el combo en esa posicion
    cbCategoria.ItemIndex := cbCategoria.Items.IndexOfValue(FCategoria);

    FFechaVenta     := FechaVenta;
    deFechaVenta.Date := FFechaVenta;

    FPatente        := TRIM(Patente);
    txtPatente.Text := FPatente;

    FUsado          := Usado;
    lblFechaUso.Visible := Usado;
    if Usado then  begin
        FFechaUso    := FechaUso;
        lblFechaUso.Caption := Format(MSG_USADO, [FormatDateTime('dd/mm/yyyy',FechaUso)]);
    end;

    Result := True;

end;

{******************************** Function Header ******************************
Function Name: btnAceptarClick
Author : ndonadio
Date Created : 10/10/2005
Description :   Se aceptan los cambios...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmEdicionBHTU.btnAceptarClick(Sender: TObject);
resourcestring
    CAP_OPENING_TRX             = 'Iniciando...';
    CAP_CHANGING_BHTU_DATA      = 'Modificando BHTU';
    CAP_PROCESSING              = 'Liberando tr�nsitos';
    CAP_COMMITTING_TRX          = 'Aplicando cambios';

    ERROR_OPENING_TRX           = 'Error Abriendo una transacci�n';
    ERROR_CHANGING_BHTU_DATA    = 'Error modificando los datos del BHTU';
    ERROR_PROCESSING            = 'Error Liberando los tr�nsitos asignados al BHTU';
    ERROR_COMMITTING_TRX        = 'Error Aplicando cambios en la DB';

    PROCESS_ENDED_OK            = 'El Proceso de liberaci�n del BHTU se complet� con �xito.';
    PROCESS_FAILED              = 'El Proceso no pudo ser completado';
    INVALID_TAN                 = 'Valor no v�lido para una patente';
    MSG_CONFIRMATION            = 'Se van a cambiar los valores del BHTU seleccionado.' + CRLF + 'Desea continuar?';
var
    CodigoHistoricoBHTU     : integer;
    descError           : AnsiString;

begin
    if not HayCambios then begin
        ModalResult := mrCancel;
        Exit;
    end;

    if not ValidateControls([txtPatente], [VerificarPatente(TRIM(txtPatente.Text))], Caption, [INVALID_TAN]) then begin
        ModalResult := mrNone;
        Exit;
    end;

    if MsgBox(MSG_CONFIRMATION, Caption, MB_ICONQUESTION+MB_YESNO) = mrNo then begin
        ModalResult := mrNone;
        Exit;
    end;

    pbProgreso.Step := 1;
    pbProgreso.Min  := 0;
    pbProgreso.Max  := 5;
    pbProgreso.Position := pbProgreso.Min;

    // Proceso los cambios
    pbProgreso.Visible := True;
    lblDetalle.Caption := '';
    lblDetalle.Visible := True;

    try
        // Inicio una transaccion
        pbProgreso.StepIt;
        lblDetalle.Caption := CAP_OPENING_TRX;
        //DMConnections.BaseCAC.BeginTrans;                                     //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('BEGIN TRAN fEdicionBHTU');               //SS_1385_NDR_20150922

        // Ejecuto el sp de modificacion del BHTU
        pbProgreso.StepIt;
        lblDetalle.Caption := CAP_CHANGING_BHTU_DATA;
        if not ModificarBHTU(CodigoHistoricoBHTU, descError) then begin
            //DMConnections.BaseCAC.RollbackTrans;                                                                                //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fEdicionBHTU END');	    //SS_1385_NDR_20150922
            ModalResult := mrAbort;
            MsgBOxErr(ERROR_CHANGING_BHTU_DATA, descError, Caption, MB_ICONERROR);
            Exit;
        end;

        // Ejecuto el SP de liberacion de transitos
        pbProgreso.StepIt;
        lblDetalle.Caption := CAP_PROCESSING;
        if not LiberarTransitos(CodigoHistoricoBHTU, descError) then begin
            //DMConnections.BaseCAC.RollbackTrans;                                                                                //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fEdicionBHTU END');	    //SS_1385_NDR_20150922
            ModalResult := mrAbort;
            MsgBOxErr(ERROR_PROCESSING, descError, Caption, MB_ICONERROR);
            Exit;
        end;

        // Hago el Commit
        pbProgreso.StepIt;
        lblDetalle.Caption := CAP_COMMITTING_TRX;
        //DMConnections.BaseCAC.CommitTrans;                                                                //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('COMMIT TRAN fEdicionBHTU');					              //SS_1385_NDR_20150922

        // Muestro mensaje de Todo Ok!!!
        pbProgreso.Position := pbProgreso.Max;
        lblDetalle.Caption := '';
        MsgBox(PROCESS_ENDED_OK, Caption, MB_ICONINFORMATION);

    except
        on e:exception do begin
            // si estoy en Transaccion, Hago un rollback
            //if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;                  //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fEdicionBHTU END');	    //SS_1385_NDR_20150922


            MsgBoxErr( PROCESS_FAILED, e.Message, Caption, MB_ICONERROR);
            ModalResult := mrAbort; // Seteo ModalResult = mrAbort
            Exit; // Salimos...
        end;
    end;
    // Asigno el resultado modal
    ModalResult := mrOK;
end;

{******************************** Function Header ******************************
Function Name: HayCambios
Author : ndonadio
Date Created : 10/10/2005
Description : Verifica si hubo cambios en los valores...
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TfrmEdicionBHTU.HayCambios: Boolean;
begin
    Result :=   (FCategoria <> cbCategoria.Value )  OR
                (FPatente <> TRIM(txtPatente.Text)) OR
                (FFechaVenta <> deFechaVenta.Date);
end;

{******************************** Function Header ******************************
Function Name: VerificarPatente
Author : ndonadio
Date Created : 10/10/2005
Description : verifica que la patente tenga la combinacion de letras y numeros validos

Parameters : Patente: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmEdicionBHTU.VerificarPatente(Patente: AnsiString): boolean;
resourcestring
    FAILURE_VALIDATING_TAN  = 'Se ha poduciodo un error al intentar validar la patente';
begin
    Result := False;
    try
        try   // verifico que sean numericos los ultimos 4
            StrToInt(COPY(TRIM(Patente), 3,4))
        except
            Exit;
        end;

        Result := (QueryGetValueInt(DMConnections.BaseCAC,
          Format('SELECT dbo.VerificarFormatoPatenteChilena(''%s'')', [TRIM(Patente)])) = 1);
    except
        on e:exception do begin
            MsgBoxErr(FAILURE_VALIDATING_TAN, e.Message, Caption, MB_ICONERROR);
            ModalResult := mrAbort;
            Close;
        end;
    end;
end;

procedure TfrmEdicionBHTU.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    if ModalResult = mrNone then ModalResult := mrCancel;
end;

{******************************** Function Header ******************************
Function Name: ModificarBHTU
Author : ndonadio
Date Created : 11/10/2005
Description :   Aplica la modificacion al BHTU
Parameters : var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmEdicionBHTU.ModificarBHTU(var CodigoHistoricoBHTU: Integer; var Error: AnsiString): boolean;
resourcestring
    ERROR_INTERNO   = 'El proceso de modificaci�n del BHTU ha fallado.';
begin
    Result := False;
    try
        with spModificarBHTU, Parameters do begin
            Close;
            ParamByName('@NumeroDayPass').Value := FNumeroDaypass;
            ParamByName('@Patente').Value := TRIM(txtPatente.Text);
            ParamByName('@FechaVenta').Value := deFechaVenta.Date;
            ParamByName('@Usuario').Value := UsuarioSistema;
            ParamByName('@CodigoHistoricoBHTU').Value := 0;
            ExecProc;

            if ParamByName('@RETURN_VALUE').Value <> 0 then begin
                Error := ERROR_INTERNO;
                Exit;
            end;
            CodigoHistoricoBHTU :=  ParamByName('@CodigoHistoricoBHTU').Value ;
        end;
        Result := True;
    except
        on e:exception do Error := e.Message;
    end;
end;

{******************************** Function Header ******************************
Function Name: LiberarTransitos
Author : ndonadio
Date Created : 11/10/2005
Description : Libera los transitos ascociados al BHTU si los hubiera
Parameters : var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmEdicionBHTU.LiberarTransitos(CodigoHistoricoBHTU: Integer; var Error: AnsiString): boolean;
resourcestring
    ERROR_INTERNO   = 'El proceso de Liberaci�n de Tr�nsitos ha fallado.';
begin
    Result := False;
    try
        with spLiberarBHTU, Parameters do begin
            ParamByName('@NumeroDayPass').Value := FNumeroDayPass;
            ParamByName('@CodigoHistoricoBHTU').Value := CodigoHistoricoBHTU;
            ExecProc;

            if ParamByName('@RETURN_VALUE').Value <> 0 then begin
                Error := ERROR_INTERNO;
                Exit;
            end;
            Result := True;
        end;
    except
        on e:exception do Error := e.Message;
    end;

end;

end.
