{-----------------------------------------------------------------------------
 File Name: frmCierredeJornada.pas
 Author:    lgisuk
 Date Created: 27/07/2005
 Language: ES-AR
 Description: M�dulo de la interface Contable - Cierre de Jornada
-----------------------------------------------------------------------------}
unit frmCierredeJornada;

interface

uses
  //Cierre de Jornada
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  PeaTypes,
  UtilDB,
  ComunesInterfaces,
  FrmAutorizacionSupervisorContable, //Autorizaci�n de Supervisor Contable
  FrmRptVerificacionInterfazContable,//Reporte de Finalizacion de Proceso
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, BuscaTab, DmiCtrls, DPSControls, VariantComboBox, StrUtils;

type

  TFCierredeJornada = class(TForm)
	Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    edDestino: TEdit;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    spObtenerAsientosContables: TADOStoredProc;
    lblFechaInterface: TLabel;
    spObtenerProcesosContabilizados: TADOStoredProc;
    edInterfazEntrante: TBuscaTabEdit;
    buscaInterfaces: TBuscaTabla;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    SPValidarLogin: TADOStoredProc;
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    function  buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);
    procedure edInterfazEntranteChange(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  	FNumeroProceso : integer;
    FJornadaAbierta : Boolean;
    FEstadoContabilizacion: TEstadoContabilizacion;
    FCodigoOperacion : integer;
    FLista	: TStringList;
    FErrorMsg : Ansistring;
   	FDetenerImportacion: boolean;
   	FContable_Directorio_Destino : Ansistring;
    FFechaJornada : TDateTime;
    FFechaJornadaAnterior: TdateTime;
    function  ProcesoAnteriorEstaCerrado(NroProceso: Integer; var NroProcesoAnterior: Integer): Boolean;
  	Function  RegistrarOperacion : boolean;
  	Function  GenerarArchivoContable : boolean;
    Function  GenerarReporteFinalizacionProceso(NumeroProceso:Integer):boolean;
  public
    function Inicializar : Boolean;
	{ Public declarations }
  end;

var
  FCierredeJornada: TFCierredeJornada;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 02/08/2005
  Description: Inicializacion de este formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFCierredeJornada.Inicializar : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 02/08/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        CONTABILIDAD_DIRECTORIO_DESTINO   = 'Contabilidad_Directorio_Destino';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC,  CONTABILIDAD_DIRECTORIO_DESTINO, FContable_Directorio_Destino) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER +  CONTABILIDAD_DIRECTORIO_DESTINO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FContable_Directorio_Destino := GoodDir(FContable_Directorio_Destino);
                if  not DirectoryExists(FContable_Directorio_Destino) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FContable_Directorio_Destino;
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
	MSG_INVALID_DESTINATION_DIRECTORY = 'El directorio de env�os es inv�lido';
begin
	Result := False;

	CenterForm(Self);

	try
		DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and
                            VerificarParametrosGenerales;
	except
  		on e: Exception do begin
  			  MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
    			Exit;
  		end;
	end;

  spObtenerProcesosContabilizados.Open;

	btnCancelar.Enabled := False;
	pnlAvance.Visible := False;
	lblReferencia.Caption := '';

	result := True;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 02/08/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFCierredeJornada.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_CONTABLE           = ' ' + CRLF +
                             'El Archivo de Cierre de Jornada ' + CRLF +
                             'es utilizado por el ESTABLECIMIENTO para informar al SAP' + CRLF +
                             'los asientos contables de todos los eventos' + CRLF +
                             'ocurridos en ese dia calendario' + CRLF +
                             ' ' + CRLF +
                             'Nombre del Archivo: ContabilidadYYYYMMDD.txt' + CRLF +
                             ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_CONTABLE, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 02/08/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TFCierredeJornada.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: edFechaChange
  Author:    lgisuk
  Date Created: 02/08/2005
  Description: En caso de Reprocesar esta interfaz
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFCierredeJornada.edInterfazEntranteChange(Sender: TObject);
begin
     btnProcesar.Enabled := ( edInterfazEntrante.Text <> '' );
end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfasesProcess
  Author:    lgisuk
  Date Created: 02/08/2005
  Description: En caso de Reprocesar esta interfaz
  Parameters: Tabla: TDataSet; var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFCierredeJornada.buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
    Result := True;
  	with Tabla do begin
       	Texto := 'Jornada' + ' - ' +FieldByName('FechaJornada').AsString
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfacesSelect
  Author:    lgisuk
  Date Created: 02/08/2005
  Description:  Selecciona un proceso contabilizado
  Parameters: Sender: TObject; Tabla: TDataSet
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFCierredeJornada.buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);
Const
  STR_FILE_NAME = 'Contabilidad';
  STR_EXT = '.txt';
Var
  Year, Month, Day : Word;
  Mes, Dia : AnsiString;
begin
    //Obtengo el numero de proceso
    FNumeroProceso := Tabla.FieldByName('NumeroProcesoContabilizacion').Value;
    //Obtengo si es una jornada abierta o cerrada
    FJornadaAbierta :=  IIF (Tabla.FieldByName('FechaHoraCierre').IsNull, True , False );
    //Obtengo el estado del proceso contable
    case Tabla.FieldByName('Estado').Value of
        CONST_ESTADO_CONTABILIZACION_INICIADO       : FEstadoContabilizacion := ecIniciada;
        CONST_ESTADO_CONTABILIZACION_FINALIZADO     : FEstadoContabilizacion := ecFinalizada;
        CONST_ESTADO_CONTABILIZACION_DESHACIENDO    : FEstadoContabilizacion := ecDeshaciendo;
        CONST_ESTADO_CONTABILIZACION_DESHECHO       : FEstadoContabilizacion := ecDeshecha;
        else FEstadoContabilizacion := ecDesconocida;
    end;
    //Obtengo fecha jornada
    edInterfazEntrante.Text:= Tabla.FieldByName('FechaJornada').Value;
    FFechaJornada := Tabla.FieldByName('FechaJornada').Value;
    //Obtengo a�o, mes y dia
    DecodeDate(FFechaJornada, Year, Month, Day);
    if length(IntToStr(Month)) = 1 then Mes := '0'+ IntToStr(Month) else Mes := IntToStr(Month);
    if length(IntToStr(Day)) = 1 then Dia := '0'+ IntToStr(Day) else Dia := IntToStr(Day);
    //Creo el nombre del archivo a enviar
    edDestino.Text:= FContable_Directorio_Destino + STR_FILE_NAME + inttostr(Year) + Mes + Dia + STR_EXT;
end;

{******************************** Function Header ******************************
Function Name: ProcesoAnteriorEstaCerrado
Author : ggomez
Date Created : 12/08/2005
Description : Si el Proceso de Contabilizaci�n inmediato anterior
    a NroProceso est� Cerrado, retorna True. Si es as�, en NroProcesoAnterior
    queda el N� del proceso inmediato anterior encontrado.
Parameters : NroProceso: Integer; var NroProcesoAnterior: Integer
Return Value : Boolean
*******************************************************************************}
function TFCierredeJornada.ProcesoAnteriorEstaCerrado(NroProceso: Integer; var NroProcesoAnterior: Integer): Boolean;
var
    Estado: TEstadoContabilizacion;
    FechaHoraInicio,
    FechaHoraCierre: TDateTime;
    Usuario: AnsiString;
begin
    Result := False;
    NroProcesoAnterior := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerAnterioNumeroProcesoContabilizacionFinalizado(' + IntToStr(NroProceso) + ')');
    // Si el N� de Proceso a testear NO es v�lido, retornar True.
    if NroProcesoAnterior < 1 then begin
        Result := True;
        Exit;
    end;
    if NroProcesoAnterior > -1 then begin
        // Obtener los datos del Proceso anterior
        ObtenerDatosProcesoContabilizacion(DMConnections.BaseCAC, NroProcesoAnterior, FFechaJornadaAnterior, FechaHoraInicio, FechaHoraCierre, Usuario, Estado);
        Result := (FechaHoraCierre <> NullDate);
    end; // if
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    lgisuk
  Date Created: 02/08/2005
  Description: Registra la Operaci�n en el Log de Interfases
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFCierredeJornada.RegistrarOperacion : boolean;
resourcestring
	  MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
Const
    CODIGO_MODULO = 55;
var
	  DescError : string;
begin
    result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, CODIGO_MODULO, ExtractFileName(edDestino.text), UsuarioSistema, '', False, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError );
    if not result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarArchivoContable
  Author:    lgisuk
  Date Created: 02/08/2005
  Description: Genera el Archivo Contable
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFCierredeJornada.GenerarArchivoContable : boolean;

    {-----------------------------------------------------------------------------
      Function Name: ArmarLineaTXT
      Author:    lgisuk
      Date Created: 02/08/2005
      Description: Arma la l�nea del TXT
      Parameters: None
      Return Value: string
    -----------------------------------------------------------------------------}
    function ArmarLineaTXT : string;
    begin
        with spObtenerAsientosContables do begin
            result :=	  PADR(Trim(FieldByName('FechaContabilizacion').AsString), 10, ' ') +
                        PADR(Trim(FieldByName('Referencia').AsString), 10, ' ') +
                        PADR(Trim(FieldByName('Nombre').AsString), 25, ' ') +
                        PADR(Trim(FieldByName('Cliente').AsString), 10, ' ') +
                        PADL(Trim(FieldByName('Debe').AsString), 10, '0') +
                        PADL(Trim(FieldByName('Haber').AsString),10, '0') +
                        PADR(Trim(FieldByName('Descripcion').AsString), 255, ' ');

        end;
    end;


resourcestring
  	MSG_NO_PENDING_ANSWERS 	 			      = 'No se puedieron generar los asientos de la jornada';
  	MSG_COULD_NOT_CREATE_FILE			      = 'No se pudo crear el archivo de contable';
  	MSG_COULD_NOT_GET_PENDING_ANSWERS 	= 'No se pudieron obtener los asientos contables de era jornada';
    MSG_GENERATING_ANSWERS_FILE		    	= 'Generando Archivo Contable - linea : %d ';
    MSG_PROCESSING			 		          	= 'Procesando...';
    MSG_ERROR                           = 'Error';
begin
	try
    	Screen.Cursor := crHourglass;
  		result := False;
	    try
    		    spObtenerAsientosContables.Close;
            spObtenerAsientosContables.Parameters.refresh;
    	    	spObtenerAsientosContables.Parameters.ParamByName( '@NumeroProcesoContabilizacion' ).Value := FNumeroProceso;
        		spObtenerAsientosContables.Open;
            if spObtenerAsientosContables.IsEmpty then begin
                	MsgBox(MSG_NO_PENDING_ANSWERS, Caption, MB_ICONWARNING);
                  FErrorMsg := MSG_NO_PENDING_ANSWERS;
        	      	Screen.Cursor := crDefault;
                  Exit;
            end;
        except
          	on e: Exception do begin
	        		      MsgBoxErr(MSG_COULD_NOT_GET_PENDING_ANSWERS, e.Message, MSG_ERROR  , MB_ICONERROR);
                	  FErrorMsg := MSG_COULD_NOT_GET_PENDING_ANSWERS;
                    Exit;
            end;
        end;
        lblReferencia.Caption	:= Format( MSG_GENERATING_ANSWERS_FILE, [spObtenerAsientosContables.RecordCount] );
        pbProgreso.Position 	:= 0;
        pnlAvance.Visible 		:= True;
        lblReferencia.Caption := MSG_PROCESSING;

        with spObtenerAsientosContables do begin
          	pbProgreso.Max := spObtenerAsientosContables.RecordCount;
            while ( not Eof ) and ( not FDetenerImportacion ) do begin
                //Genero cada linea del txt
              	Flista.Add( ArmarLineaTXT );
                pbProgreso.StepIt;
                Application.ProcessMessages;
                Next;
            end;
        end;

        if ( not FDetenerImportacion ) and ( FErrorMsg = '' ) then begin
          	try
              	Flista.SaveToFile( edDestino.Text );
                result := True;
            except
              	on e: Exception do begin
                  	MsgBoxErr(MSG_COULD_NOT_CREATE_FILE, e.Message, MSG_ERROR, MB_ICONERROR);
                    FErrorMsg := MSG_COULD_NOT_CREATE_FILE;
                end;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarInformedelProceso
  Author:    lgisuk
  Date Created: 23/06/2005
  Description: Muestro un informe de finalizaci�n de proceso
  Parameters: CodigoOperacionInterfase:Integer; NombreReporte, CantidadRegistros, SumatoriaCuentas: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
Function  TFCierredeJornada.GenerarReporteFinalizacionProceso(NumeroProceso:Integer):boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de finalizaci�n de proceso';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Cierre de Jornada';
var
    F : TFRptVerificacionInterfazContable;
begin
    Result := False;
    try
        //Muestro el reporte
        Application.CreateForm(TFRptVerificacionInterfazContable, F);
        if not F.Inicializar(NumeroProceso, REPORT_TITLE, ExtractFileName(edDestino.text)) then f.Release;
        Result := True;
    except
       on e: Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    lgisuk
  Date Created: 02/08/2005
  Description: Genera el Archivo de Respuestas a Captaciones
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFCierredeJornada.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: SolicitarAutorizacionSupervisor
      Author:    lgisuk
      Date Created: 02/08/2005
      Description: Solicito autorizaci�n de supervisor contable para poder cerrar la jornada
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function SolicitarAutorizacionSupervisor: boolean;

        {-----------------------------------------------------------------------------
          Function Name: SolicitarAutorizacionSupervisor
          Author:    lgisuk
          Date Created: 03/08/2005
          Description: Verifico que sea un usuario valido
          Parameters:
          Return Value: boolean
        -----------------------------------------------------------------------------}
        Function ValidarLogin (Usuario, Password: AnsiString): boolean;
        Resourcestring
            MSG_ERROR_ACCESO		  = 'No se ha podido acceder a la Base de Datos.';
          	CAPTION_ERROR_ACCESO	= 'Ingreso al Sistema';
        begin
            result:=false;
            try
              with SPValidarLogin do begin
                  Parameters.ParamByName('@CodigoUsuario').Value  := Usuario;
                  Parameters.ParamByName('@Password').Value       := Password;
                  Parameters.ParamByName('@NombreCompleto').Value := NULL;
                  Parameters.ParamByName('@DescriError').Value 	 := NULL;
                  Parameters.ParamByName('@PedirCambiarPassword').Value:= NULL;
                  ExecProc;
              end;
            except
                on e: Exception do begin
                    Screen.Cursor := crDefault;
                    MsgBox(MSG_ERROR_ACCESO + CRLF + e.Message, CAPTION_ERROR_ACCESO, MB_ICONSTOP);
                    Exit;
                end;
            end;
            result := (spValidarLogin.Parameters.ParamByName('@RETURN_VALUE').Value = 0)
        end;

        {-----------------------------------------------------------------------------
        Function Name: AutorizarProcesoContabilizacion
        Author:    lgisuk
        Date Created: 03/08/2005
        Description: Autorizo el proceso dejo asentado a que hora se autorizo y quien lo autorizo
        Parameters:
        Return Value: boolean
        -----------------------------------------------------------------------------}
        Function  AutorizarProcesoContabilizacion(Conn: TAdoConnection;NumeroProceso:integer;CodigoUsuarioCierre:string;Var DescError:Ansistring):boolean;
        var
            sp : TADOStoredProc;
        begin
            result := false;
            sp :=TADOStoredProc.Create(Nil);
            try
                try
                    sp.Connection := Conn;
                    sp.ProcedureName := 'AutorizarProcesoContabilizacion';
                    with SP.Parameters do begin
                        Refresh;
                        ParamByName('@NumeroProceso').Value := NumeroProceso;
                        ParamByName('@CodigoUsuarioCierre').Value := CodigoUsuarioCierre;
                    end;
                    SP.ExecProc;                                                        //Ejecuto el procedimiento
                    result:=true;
                except
                    on e: Exception do begin
                        DescError := e.Message;
                    end;
                end;
            finally
                SP.Free;
            end;
        end;

    ResourceString
        MSG_USER_NOT_AUTHORISED = 'Usuario o clave incorrectos!';
    var
        f: TFAutorizacionSupervisorContable;
        Usuario, Password : AnsiString;
        DescError:Ansistring;
    begin
        Usuario := '';
        Password := '';
        try
            F := TFAutorizacionSupervisorContable.Create(Self);
       		  F.ShowModal;
            Usuario  := F.UserName;
            Password := F.Password;
            Result:= ValidarLogin(Usuario , Password);
            if Result then AutorizarProcesoContabilizacion(dmconnections.BaseCAC,FNumeroProceso,Usuario,DescError)
                      else MsgBox(MSG_USER_NOT_AUTHORISED);
        finally
            FreeAndNil(F);
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 02/08/2005
      Description: Actualizo el log al finalizar
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ActualizarLog:boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVACION = 'Finalizo OK!';
    var
         DescError : AnsiString;
    begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);
        except
            on e: Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

resourcestring
    MSG_CONTABILIZACION_INICIADA            = 'El Proceso de Contabilizaci�n ha sido iniciado pero no se ha finalizado.' + CRLF + 'No se permite cerrar el Proceso de Contabilizaci�n.';
    MSG_CONTABILIZACION_DESHACIENDO         = 'El Proceso de Contabilizaci�n se ha comenzado a deshacer pero no se ha deshecho.' + CRLF + 'No se permite cerrar el Proceso de Contabilizaci�n.';
    MSG_CONTABILIZACION_DESHECHA            = 'El Proceso de Contabilizaci�n se ha deshecho.' + CRLF + 'No se permite cerrar el Proceso de Contabilizaci�n.';
    MSG_CONTABILIZACION_DESCONOCIDA         = 'No se puede determinar el estado del Proceso de Contabilizaci�n.' + CRLF + 'No se permite cerrar el Proceso de Contabilizaci�n.';
    MSG_CONTABILIZACION_ANTERIOR_NO_CERRADA = 'El Proceso de Contabilizaci�n anterior, %s, no est� cerrado.' + CRLF + 'No se permite cerrar el Proceso de Contabilizaci�n %s.';
    MSG_FILE_ALREADY_EXISTS 			          = 'El archivo ya existe.'#10#13'� Desea continuar ?';
    MSG_PROCESS_SUCCEDED 				            = 'El proceso finaliz� con �xito!';
    MSG_PROCESS_COULD_NOT_BE_COMPLETED 	    = 'El proceso no se pudo completar';
var
    NumeroProcesoContabilizacionAnterior: Integer;
begin
    // Controlar que el proceso est� Finalizado.
    case FEstadoContabilizacion of
        ecIniciada:
            begin
                MsgBox(MSG_CONTABILIZACION_INICIADA, Self.Caption, MB_OK + MB_ICONINFORMATION);
                Exit;
            end;
        ecDeshaciendo:
            begin
                MsgBox(MSG_CONTABILIZACION_DESHACIENDO, Self.Caption, MB_OK + MB_ICONINFORMATION);
                Exit;
            end;
        ecDeshecha:
            begin
                MsgBox(MSG_CONTABILIZACION_DESHECHA, Self.Caption, MB_OK + MB_ICONINFORMATION);
                Exit;
            end;
        ecDesconocida:
            begin
                MsgBox(MSG_CONTABILIZACION_DESCONOCIDA, Self.Caption, MB_OK + MB_ICONINFORMATION);
                Exit;
            end;
    end; // case

    // Controlar que el proceso inmediato anterior ESTE cerrado.
    if not ProcesoAnteriorEstaCerrado(FNumeroProceso, NumeroProcesoContabilizacionAnterior) then begin
        MsgBox(Format(MSG_CONTABILIZACION_ANTERIOR_NO_CERRADA, [Datetostr(FFechaJornadaAnterior), Datetostr(FFechaJornada)]), Self.Caption, MB_OK + MB_ICONINFORMATION);
        Exit;
    end; // if

    //Si es una jornada abierta
    if FJornadaAbierta then begin
      //Solicito autorizaci�n supervisor contable para hacer el cierre de jornada
      if not SolicitarAutorizacionSupervisor then exit;
    end;

    //Verifico si existe el archivo
	  if FileExists( edDestino.text ) then begin
        //Informo que el archivo ya existe y le pregunto al operador si desea continuar
		    if not(MsgBox(MSG_FILE_ALREADY_EXISTS, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then Exit;
    end;

    //Crea la listas de asientos
    Flista := TStringList.Create;

    // Deshabilita los botones
  	btnCancelar.Enabled := True;
    btnProcesar.Enabled := False;
    FErrorMsg := '';

    FDetenerImportacion := False;

    try
        //Registro la operacion y Genero el archivo contable
        if RegistrarOperacion then begin
              GenerarArchivoContable;
        end;

        if ( FErrorMsg = '' ) then begin
            //Actualizo el log al Final
            ActualizarLog;
            //Informo que el proceso finalizo con exito
            MsgBox(MSG_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION);
            //Genero el reporte de finalizacion de proceso
            GenerarReporteFinalizacionProceso(FNumeroProceso);
        end else begin
            //Informo que el proceso no se pudo completar
           	MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
        end;

    finally
        //libero el stringlist
		    FreeAndNil( Flista );
        //oculto panel proceso
        PnlAvance.Visible := False;
        lblreferencia.Caption := '';
       	//Habilita los botones
    		btnCancelar.Enabled := False;
       	btnProcesar.Enabled := True;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 02/08/2005
  Description: Detiene el Procesamiento
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFCierredeJornada.btnCancelarClick(Sender: TObject);
resourcestring
	  MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 02/08/2005
  Description: Cierra el Form si es que es posible
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFCierredeJornada.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
	  CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 02/08/2005
  Description: Cierra el Formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFCierredeJornada.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 02/08/2005
  Description: Libera el Formulario de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFCierredeJornada.FormClose(Sender: TObject;var Action: TCloseAction);
begin
  	Action := caFree;
end;


end.
