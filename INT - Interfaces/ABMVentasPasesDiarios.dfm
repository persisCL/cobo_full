object frmABMVentasPasesDiarios: TfrmABMVentasPasesDiarios
  Left = 0
  Top = 0
  Width = 703
  Height = 520
  Caption = 'Ventas de Pases Diarios'
  Color = clBtnFace
  Constraints.MinHeight = 520
  Constraints.MinWidth = 703
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object abmtb_VentasPasesDiarios: TAbmToolbar
    Left = 0
    Top = 0
    Width = 695
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = abmtb_VentasPasesDiariosClose
  end
  object dbl_VentasPasesDiarios: TAbmList
    Left = 0
    Top = 33
    Width = 695
    Height = 301
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'75'#0'Fecha Venta  '
      #0'68'#0'Pase Diario  '
      #0'110'#0'Monto Total               '
      #0'95'#0'Usuario                 '
      #0'116'#0'Fecha Modificaci'#243'n     '
      #0'100'#0'N'#186' Contabilizaci'#243'n  ')
    HScrollBar = True
    RefreshTime = 100
    Table = tbl_VentasPasesDiarios
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dbl_VentasPasesDiariosClick
    OnDrawItem = dbl_VentasPasesDiariosDrawItem
    OnRefresh = dbl_VentasPasesDiariosRefresh
    OnInsert = dbl_VentasPasesDiariosInsert
    OnDelete = dbl_VentasPasesDiariosDelete
    OnEdit = dbl_VentasPasesDiariosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = abmtb_VentasPasesDiarios
  end
  object pnl_Datos: TPanel
    Left = 0
    Top = 334
    Width = 695
    Height = 113
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    object lbl_TipoPaseDiario: TLabel
      Left = 15
      Top = 62
      Width = 30
      Height = 13
      Caption = 'Tipo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_FechaVenta: TLabel
      Left = 15
      Top = 36
      Width = 92
      Height = 13
      Caption = 'Fecha de venta: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_MontoTotal: TLabel
      Left = 15
      Top = 88
      Width = 73
      Height = 13
      Caption = 'Monto Total:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_Vendedor: TLabel
      Left = 15
      Top = 10
      Width = 59
      Height = 13
      Caption = 'Vendedor:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cb_TipoPaseDiario: TVariantComboBox
      Left = 156
      Top = 58
      Width = 110
      Height = 21
      Hint = 'Tipo de pase diario'
      Style = vcsDropDownList
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Items = <>
    end
    object de_FechaVenta: TDateEdit
      Left = 156
      Top = 32
      Width = 110
      Height = 21
      Hint = 'Fecha de la venta de pases diarios'
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
    object ne_MontoTotal: TNumericEdit
      Left = 156
      Top = 84
      Width = 109
      Height = 21
      Hint = 'Monto Total de la venta'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Decimals = 2
    end
    object cb_Vendedor: TVariantComboBox
      Left = 156
      Top = 6
      Width = 110
      Height = 21
      Hint = 'Tipo de pase diario'
      Style = vcsDropDownList
      Enabled = False
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 447
    Width = 695
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 498
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btn_Salir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btn_SalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object btn_Aceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btn_AceptarClick
        end
        object btn_Cancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = btn_CancelarClick
        end
      end
    end
  end
  object tbl_VentasPasesDiarios: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'VentasPasesDiarios'
    Left = 182
    Top = 2
  end
  object sp_InsertarVentaPaseDiario: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarVentaPaseDiario'
    Parameters = <>
    Left = 224
    Top = 4
  end
  object sp_ActualizarVentaPaseDiario: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarVentaPaseDiario'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaVenta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TipoPaseDiario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoVendedorDayPass'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaVentaNueva'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TipoPaseDiarioNuevo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoVendedorDayPassNuevo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MontoTotal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@MontoProporcional'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 260
    Top = 4
  end
  object sp_EliminarVentaPaseDiario: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarVentapaseDiario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaVenta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TipoPaseDiario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoVendedorDayPass'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 296
    Top = 4
  end
end
