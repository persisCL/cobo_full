unit frmRptRecepcionMovimientosTBK;

interface

uses
  DMConnection,
  Util,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppProd, ppClass, ppReport, UtilRB, ppComm, ppRelatv, ppDB, ppDBPipe,
  DB, DBClient, ppParameter, ppBands, ppCtrls, ppPrnabl, ppStrtch, ppSubRpt,
  ppCache, ADODB, RBSetup, UtilProc, ConstParametrosGenerales, jpeg;                  

type
  TRptRecepcionMovimientosTBKForm = class(TForm)
    cdsErrores: TClientDataSet;
    dsErrores: TDataSource;
    ppdbErrores: TppDBPipeline;
    ppReporteRecepMovimientosTBK: TppReport;
    ppDetailBand1: TppDetailBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppHeaderBand2: TppHeaderBand;
    lbl_usuario: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppImage2: TppImage;
    ppDetailBand2: TppDetailBand;
    ppDBText3: TppDBText;
    ppParameterList1: TppParameterList;
    rbiListado: TRBInterface;
    spObtenerErroresInterfase: TADOStoredProc;
    lblTitArchivoRecibido: TppLabel;
    lblTitUsuarioProceso: TppLabel;
    lblArchivoRecibido: TppLabel;
    lblUsuarioProceso: TppLabel;
    lblFechaProceso: TppLabel;
    lblNroProceso: TppLabel;
    lblTitLineasArchivo: TppLabel;
    lblTitRegistrosExito: TppLabel;
    lblTitRegistrosError: TppLabel;
    lblLineasArchivo: TppLabel;
    lblRegistrosExito: TppLabel;
    lblRegistrosError: TppLabel;
    spObtenerDatosProcesoMovimientosTBK: TADOStoredProc;
    ppLabel1: TppLabel;
    private
    { Private declarations }
    FCodigoOperacion: Integer;
  public
    { Public declarations }
    procedure MostrarReporte(CodigoOperacion: Integer);
  end;

var
  RptRecepcionMovimientosTBKForm: TRptRecepcionMovimientosTBKForm;

implementation

{$R *.dfm}

procedure TRptRecepcionMovimientosTBKForm.MostrarReporte(CodigoOperacion: Integer);
resourcestring
    PROCESSING_ERRORS_FILE				= 'Movimientos Transbank - Errores Procesamiento ';
    REPORT_DIALOG_CAPTION 				= 'Movimientos Transbank - Errores';
    MSG_ERROR_SAVING_ERRORS_FILE 	    = 'Error generando archivo de errores';
    MSG_ERR_REPORT                      = 'El Reporte de Errores se guardo en: %s';
    TITLE_ERR_REPORT                    = 'Reporte de Errores';
    MSG_PROCESS_NUMBER                  = 'Proceso N� %d';
    MSG_PROCESS_DATETIME                = 'Fecha de Proceso: %s Hora: %s';
var
  	Config: TRBConfig;
    FechaProceso: TDateTime;
    RutaLogo: AnsiString;
begin
    Screen.Cursor := crHourglass;
    FCodigoOperacion := CodigoOperacion;

    try
        ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);
        try                                                                                                 
          ppImage2.Picture.LoadFromFile(Trim(RutaLogo));
        except
          On E: Exception do begin
            Screen.Cursor := crDefault;
            MsgBox(E.message, Self.Caption, MB_ICONSTOP);
            Screen.Cursor := crHourGlass;
          end;
        end;

        cdsErrores.CreateDataSet ;

        with spObtenerErroresInterfase do begin

            Close;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
            Open;

            if Eof then Exit;

            First;
            while not Eof do begin
                cdsErrores.InsertRecord([FieldByName('DescripcionError').AsString]);
                Next;
            end;
            Close;
        end;

        //Datos del Encabezado
        with spObtenerDatosProcesoMovimientosTBK do begin
            Close;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
            ExecProc;

            lblArchivoRecibido.Caption := Parameters.ParamByName('@NombreArchivo').Value;
            lblUsuarioProceso.Caption := Parameters.ParamByName('@UsuarioProceso').Value;
            FechaProceso := Parameters.ParamByName('@FechaHoraProceso').Value;
            lblLineasArchivo.Caption := Parameters.ParamByName('@LineasArchivo').Value;
            lblRegistrosExito.Caption := Parameters.ParamByName('@RegistrosExito').Value;
            lblRegistrosError.Caption := Parameters.ParamByName('@RegistrosError').Value;
        end;

        lblNroProceso.Caption := Format(MSG_PROCESS_NUMBER, [FCodigoOperacion]);
        lblFechaProceso.Caption := Format(MSG_PROCESS_DATETIME, [FormatDateTime(ShortDateFormat, FechaProceso),
                                                FormatDateTime('HH:nn', FechaProceso)]);
    finally
	    Screen.Cursor := crDefault;
    end;

    rbiListado.Caption := REPORT_DIALOG_CAPTION;

  	Config := rbiListado.GetConfig;
    if Config.ShowUser then begin
    	if UsuarioSistema <> '' then lbl_usuario.caption := 'Usuario: ' +
        	UsuarioSistema + ' ' else lbl_usuario.Caption := '';
    end else begin
    	lbl_usuario.Caption := '';
    end;

    if Config.ShowDateTime then begin
    	lbl_usuario.caption := lbl_usuario.caption +
        FormatShortDate(Date) + ' ' + FormatTime(Time);
    end;
    rbiListado.Execute(True);
end;


end.
