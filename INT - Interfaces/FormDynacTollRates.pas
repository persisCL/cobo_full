unit FormDynacTollRates;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, ExtCtrls, ComCtrls, StdCtrls, DBCtrls, ToolWin,
  ImgList, PuntosCobro, Provider, Grids, DBGrids, DB, VariantComboBox, Dynac, UtilProc, PeaProcs,
  InvokeRegistry, Rio, SOAPHTTPClient, DBClient, CategoriasClases, Util, GrillaListado, DMConnection, BosTollRates, XSBuiltIns,
  UtilDB;

type
  TfrmDynacTollRates = class(TForm)
    pnlSuperior: TPanel;
    dblTollRates: TDBListEx;
    pnlSalir: TPanel;
    ilActivos: TImageList;
    btnSalir: TButton;
    pnlFiltroGrilla: TPanel;
    pnlBuscar: TPanel;
    lblAnio: TLabel;
    txtAnio: TEdit;
    btnBuscar: TButton;
    btnEnviar: TButton;
    lbl1: TLabel;
    cbbCategoriaClases: TVariantComboBox;
    lblPuntoCobro: TLabel;
    cbbPuntosCobro: TVariantComboBox;
    lblInformacion: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnEnviarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure cbbCategoriaClasesChange(Sender: TObject);
    procedure cbbPuntosCobroChange(Sender: TObject);
    procedure txtAnioKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    oPuntosCobro: PuntosCobro.TPuntoCobro;
    oTollRates: TTollRates;
    oCategoriasClases : TCategoriaClase;
    fGrilla : TfrmGrilla;

    procedure FiltrandoPuntoCobro(filtrando: Boolean);
    procedure FiltrarTollRates();
    procedure MostrarGaps(MensajeError: string);
    procedure Enviar();
  public
    { Public declarations }
    function Inicializar(): Boolean;
  end;

var
  frmDynacTollRates: TfrmDynacTollRates;

implementation

{$R *.dfm}

procedure TfrmDynacTollRates.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if Assigned(dblTollRates.DataSource) then
        dblTollRates.DataSource.Free;
    FreeAndNil(oPuntosCobro);
    FreeAndNil(oTollRates);
    FreeAndNil(oCategoriasClases);
    Action := caFree;
end;

procedure TfrmDynacTollRates.FormShow(Sender: TObject);
begin
    btnEnviar.Enabled := False;
end;

function TfrmDynacTollRates.Inicializar(): Boolean;
begin
    btnEnviar.Enabled := False;
    
    oPuntosCobro := PuntosCobro.TPuntoCobro.Create;

    oPuntosCobro.Obtener_from_BO_Raiting(13);

    oCategoriasClases:= TCategoriaClase.Create;
    oCategoriasClases.Obtener;
    while not oCategoriasClases.ClientDataSet.Eof do
    begin
        cbbCategoriaClases.Items.InsertItem(oCategoriasClases.Descripcion, oCategoriasClases.Id_CategoriasClases);
        oCategoriasClases.ClientDataSet.Next;
    end;
    if oCategoriasClases.ClientDataSet.RecordCount > 0 then
    begin
        cbbCategoriaClases.ItemIndex := 0;   
        cbbCategoriaClasesChange(nil);
    end;

    Result:= True;
end;

procedure TfrmDynacTollRates.cbbCategoriaClasesChange(Sender: TObject);
begin
    try
        FiltrandoPuntoCobro(True);

        oPuntosCobro.ClientDataSet.Filter := ' IDCategoriaClase = ' + Chr(39) + IntToStr(cbbCategoriaClases.Value) + chr(39);
        oPuntosCobro.ClientDataSet.First;

        cbbPuntosCobro.Items.Clear;
        while not oPuntosCobro.ClientDataSet.Eof do
        begin
            cbbPuntosCobro.Items.InsertItem(oPuntosCobro.Descripcion, oPuntosCobro.NumeroPuntoCobro);
            oPuntosCobro.ClientDataSet.Next;
        end;

        if cbbPuntosCobro.Items.Count > 0  then
            cbbPuntosCobro.ItemIndex := 0;

        FiltrarTollRates();
    finally
        FiltrandoPuntoCobro(False);
    end;
end;

procedure TfrmDynacTollRates.cbbPuntosCobroChange(Sender: TObject);
begin
    if cbbPuntosCobro.Items.Count > 0 then
        FiltrarTollRates();
end;

procedure TfrmDynacTollRates.FiltrandoPuntoCobro(filtrando: Boolean);
begin
    cbbPuntosCobro.Enabled := not filtrando;
    btnBuscar.Enabled:= not filtrando;
    btnEnviar.Enabled:= not filtrando;
end;

procedure TfrmDynacTollRates.btnBuscarClick(Sender: TObject);
var
    MensajeError: string;
    FechaDesde, FechaHasta : TDateTime;
    formatoFecha: string;
begin                    
    pnlFiltroGrilla.Enabled := False;
    
    if not ValidateControls([txtAnio, txtAnio],
                        [Trim(txtAnio.Text) <> '', IsInteger(txtAnio.Text)],
                        'Validaci�n',
                        ['Ingrese un a�o',
                         'Ingrese un a�o v�lido']) then
    begin
        Exit;
    end;

    try
        try
            formatoFecha := ShortDateFormat;
            ShortDateFormat := 'yyyy/mm/dd';
            FechaDesde := StrToDate(txtAnio.Text+'/01/01');
            FechaHasta := StrToDate(txtAnio.Text+'/12/31');

            if not Assigned(oTollRates) then
                oTollRates := TTollRates.Create;

            if Assigned(dblTollRates.DataSource) then
                dblTollRates.DataSource.Free;

            dblTollRates.DataSource := TDataSource.Create(nil);

            dblTollRates.DataSource.DataSet := oTollRates.CrearTarifasDynac(FechaDesde, FechaHasta, UsuarioSistema, MensajeError, DMConnections.BaseBO_Dynac);

            if oTollRates.ClientDataSet.RecordCount = 0 then
                MsgBox('No se encontraron registros', 'Mensaje', MB_OK)
            else
            begin
                pnlFiltroGrilla.Enabled := True;
                
                if MensajeError <> '' then
                    MostrarGaps(MensajeError)
                else
                    btnEnviar.Enabled := True;
            end;
        except
            on e:Exception do
            begin
                if e.Message = 'Error al convertir una cadena de caracteres en fecha y/u hora.' then
                    MsgBox('Ingrese un a�o v�lido', 'Atenci�n', MB_ICONSTOP)
                else
                    MsgBoxErr('Error cargando tarifas', e.Message, 'Error de carga', MB_ICONSTOP);
            end;
        end;
    finally
        ShortDateFormat := formatoFecha;
    end;           
    
end;

procedure TfrmDynacTollRates.MostrarGaps(MensajeError: string);
var
    datos: TClientDataSet;
    datosString: TStrings;
    I: Integer;
begin      
    try
        datos := TClientDataSet.Create(nil);
        datos.FieldDefs.add('Registro', ftString, 100);
        datos.CreateDataSet;
        datosString := Split(';', MensajeError);
        for I := 0 to datosString.Count - 1 do
        begin
            datos.InsertRecord([datosString.Strings[i]]);
        end;

        Application.CreateForm(TfrmGrilla, fGrilla);

        fgrilla.Caption := 'Fecha con Gaps';
        fGrilla.lblMensaje.Caption := 'Utilice el modulo de tarificaci�n para modificar las tarifas.';
        fGrilla.FormStyle := fsStayOnTop;
        if fGrilla.Inicializar(datos, []) then
           fGrilla.Show;

    finally
        FreeAndNil(datosString);
        datos := nil;
    end;
end;

procedure TfrmDynacTollRates.txtAnioKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = Chr(13) then
        btnBuscarClick(nil);
end;

procedure TfrmDynacTollRates.FiltrarTollRates();
var
    filtro : string;
begin
    if Assigned(oTollRates) then
    if Assigned(oTollRates.ClientDataSet) then
    begin
        filtro := ' RoadSegment = ' + Chr(39) + cbbCategoriaClases.Text + Chr(39);

        if cbbPuntosCobro.Items.Count > 0 then
            filtro := filtro + ' and GantryID = ' + Chr(39) + InttoStr(cbbPuntosCobro.Value) + Chr(39);

        oTollRates.ClientDataSet.Filter := filtro;
    end;   
end;

procedure TfrmDynacTollRates.btnEnviarClick(Sender: TObject);
begin
    if Assigned(dblTollRates.DataSource) and (dblTollRates.DataSource.DataSet.RecordCount > 0) then
    begin
        if Msgbox('�Confirma envio de tarifas?', 'Confirmar', MB_YESNO) = mrYes then
        begin
            try
                Cursor:= crHourGlass;
                pnlSuperior.Enabled := False;
                dblTollRates.Enabled := False;

                if oTollRates.MarcarParaEnviar(DMConnections.BaseBO_Dynac) then
                begin
                    Enviar();
                end;

            finally
                dblTollRates.Enabled := True;
                pnlSuperior.Enabled := True;
                Cursor:= crDefault;
            end;
        end;

    end;
end;

procedure TfrmDynacTollRates.Enviar();
var
    rates: receiveTollRatesBOSData;
    gantries: ArrayOfGantryData;
    resultado : receiveTollRatesBOSDataResponse;
    i, Total, vecesReintento, tiempoEspera: Integer;
    temp2: TXSDecimal;
    oControl : TTollRatesControl;
    TarifasEnviar : TTollRates;
    WSCliente: IBosTollRates;
begin
    try
        try
            Total := oTollRates.ClientDataSet.RecordCount;
            oControl := TTollRatesControl.Create;
            TarifasEnviar := TTollRates.Create;
            rates := receiveTollRatesBOSData.Create;
            WSCliente := GetIBosTollRates();
            resultado := nil;

            vecesReintento := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerParametroGeneralInt(' + QuotedStr('DynacVecesReintentoEnvio') + ')');
            tiempoEspera := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerParametroGeneralInt(' + QuotedStr('DynacTiempoReintentoEnvio') + ')');

            oControl.ObtenerPuntosAEnviar(UsuarioSistema, DMConnections.BaseBO_Dynac);

            lblInformacion.Caption := '';
            if oControl.ClientDataSet.RecordCount > 0 then
            begin

                while not oControl.ClientDataSet.Eof do
                begin

                    TarifasEnviar.ObtenerDisponiblesEnvio(oControl.NumeroPuntoCobro, UsuarioSistema, DMConnections.BaseBO_Dynac);
                    if TarifasEnviar.ClientDataSet.RecordCount > 0 then
                    begin
                        
                        TarifasEnviar.ClientDataSet.First;

                        rates.GantryId := IntToStr(TarifasEnviar.NumeroPuntoCobro);
                        rates.NewYearRate := IIf(oControl.ID  = 0, True, False);

                        SetLength(gantries, TarifasEnviar.ClientDataSet.RecordCount); 
                        for i := 0 to TarifasEnviar.ClientDataSet.RecordCount - 1 do
                        begin
                            gantries[i] := gantryData.Create;
                            gantries[i].ID := TarifasEnviar.ID;
                            gantries[i].PlanID := TarifasEnviar.ID_PlanTarifarioVersion;
                            gantries[i].StartDate := DateTimeToXSDateTime(TarifasEnviar.FechaDesde);
                            gantries[i].EndDate := DateTimeToXSDateTime(TarifasEnviar.FechaHasta);
                            gantries[i].DayOfWeek := TarifasEnviar.DescripcionDiaTipo;
                            gantries[i].StartTime := DateTimeToXSDateTime(TarifasEnviar.HoraDesde);
                            gantries[i].EndTime := DateTimeToXSDateTime(TarifasEnviar.HoraHasta);
                            gantries[i].RoadSegment := TarifasEnviar.DescripcionCategoriaClase;
                            gantries[i].RateType := TarifasEnviar.DescripcionTarifaTipo;
                            gantries[i].CategoryType := TarifasEnviar.DescripcionCategoria;
                            temp2 := TXSDecimal.Create;
                            temp2.DecimalString := FloatToStr(TarifasEnviar.Importe);
                            gantries[i].Amount := temp2;
                            TarifasEnviar.ClientDataSet.Next;
                        end;
                        rates.GantryData := gantries;

                        for i := 0 to vecesReintento - 1 do
                        begin
                            lblInformacion.Caption := 'Intento:' + IntToStr(i+1) +' envio de tarifas del punto de cobro: ' + IntToStr(ocontrol.NumeroPuntoCobro);
                            try
                               resultado := WSCliente.ReceiveTollRates(rates);
                               if resultado.Status = '0' then
                                    Break;
                               Sleep(tiempoEspera);
                            except
                                on e: Exception do
                                begin
                                    if i = 3 then
                                       raise Exception.Create(e.Message);
                                    Sleep(tiempoEspera);
                                end;
                            end;
                        end;

                        if resultado.Status <> '0' then
                            raise Exception.Create(resultado.DescriptionError);

                    end;

                    oControl.ClientDataSet.Next;

                end;

                ocontrol.ActualizarEnviados(UsuarioSistema, DMConnections.BaseBO_Dynac);
                btnEnviar.Enabled := False;
                pnlFiltroGrilla.Enabled := False;
                FreeAndNil(oTollRates.ClientDataSet);
                dblTollRates.DataSource.Free;
                txtAnio.Clear;
                MsgBox(IntToStr(Total) + ' enviados correctamente', 'Dynac Response', MB_OK)

            end
            else
                MsgBox('No hay tarifas por enviar', 'Dynac Response', MB_OK)
        except
            on e: Exception do begin
                MsgBox(e.Message, 'Dynac Response', MB_OK);
            end;
        end;

    finally
        lblInformacion.Caption := '';
        FreeAndNil(oControl);
        FreeAndNil(rates);
        FreeAndNil(resultado);
        FreeAndNil(TarifasEnviar);
    end;

end;

procedure TfrmDynacTollRates.btnSalirClick(Sender: TObject);
begin
    Close;
end;

end.
