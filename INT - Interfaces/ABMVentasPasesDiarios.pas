{********************************** Unit Header ********************************
File Name : ABMVentasPasesDiarios.pas
Author : ggomez
Date Created: 22/08/2005
Language : ES-AR
Description : ABM de Ventas de Pases Diarios. Los datos ingresados desde este
              ABM, ser�n utilizados para realizar el asiento contable de Ventas de
              Diarios (PDU/BHTU).
              Como por ahora hay s�lo un vendedor de Day Pass (Servipag), est�
              deshabilitado el control para seleccionar el vendedor.
*******************************************************************************}
unit ABMVentasPasesDiarios;

interface

uses
  //ABM Ventas de Pases Diarios
  DMConnection,
  RStrings,
  UtilProc,
  UtilDB,
  PeaProcs,
  PeaTypes,
  StrUtils,
  Util,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ADODB, DbList, Abm_obj,  DmiCtrls, Validate,
  DateEdit, VariantComboBox;

type
  TfrmABMVentasPasesDiarios = class(TForm)
    abmtb_VentasPasesDiarios: TAbmToolbar;
    dbl_VentasPasesDiarios: TAbmList;
    tbl_VentasPasesDiarios: TADOTable;
    pnl_Datos: TPanel;
    lbl_TipoPaseDiario: TLabel;
    lbl_FechaVenta: TLabel;
    Panel2: TPanel;
    Notebook: TNotebook;
    btn_Salir: TButton;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    lbl_MontoTotal: TLabel;
    cb_TipoPaseDiario: TVariantComboBox;
    de_FechaVenta: TDateEdit;
    ne_MontoTotal: TNumericEdit;
    sp_InsertarVentaPaseDiario: TADOStoredProc;
    sp_ActualizarVentaPaseDiario: TADOStoredProc;
    sp_EliminarVentaPaseDiario: TADOStoredProc;
    lbl_Vendedor: TLabel;
    cb_Vendedor: TVariantComboBox;
    procedure dbl_VentasPasesDiariosClick(Sender: TObject);
    procedure dbl_VentasPasesDiariosDelete(Sender: TObject);
    procedure dbl_VentasPasesDiariosEdit(Sender: TObject);
    procedure dbl_VentasPasesDiariosInsert(Sender: TObject);
    procedure dbl_VentasPasesDiariosRefresh(Sender: TObject);
    procedure dbl_VentasPasesDiariosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure abmtb_VentasPasesDiariosClose(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure LimpiarCampos;
    procedure VolverCampos;
    procedure CargarComboTiposPasesDiarios;
    procedure CargarComboVendedores;
  public
    { Public declarations }
    function Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;

  end;

var
  frmABMVentasPasesDiarios: TfrmABMVentasPasesDiarios;

implementation

{$R *.dfm}

resourcestring
  	STR_MAESTRO_VENTAS_PASES_DIARIOS = 'Ventas de Pases Diarios';
    MSG_VENTA_CONTABILIZADA = 'La Venta ya ha sido contabilizada.' + CRLF + 'No se permite realizar la operaci�n.';

{ TfrmABMVentasPasesDiarios }

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ggomez
Date Created : 22/08/2005
Description : Iniciliza el form. Retorna True, si iniciliz� con exito.
Parameters : txtCaption: String; MDIChild: Boolean
Return Value : Boolean
*******************************************************************************}
function TfrmABMVentasPasesDiarios.Inicializar(txtCaption: String;MDIChild: Boolean): Boolean;
var
	S: TSize;
begin
	  Result := False;
  	if MDIChild then begin
        FormStyle := fsMDIChild;
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
  	end else begin
        FormStyle := fsNormal;
        Visible := False;
  	end;
  	Notebook.PageIndex := 0;
    CenterForm(Self);
  	Caption := AnsiReplaceStr(txtCaption, '&', '');
  	if not OpenTables([tbl_VentasPasesDiarios]) then Exit;
    //Cargar el combo con los Vendedores de Pase Diario.
    CargarComboVendedores;
    //Cargar el combo con los tipos de Pase Diario.
    CargarComboTiposPasesDiarios;
    //Limpiar controles de ingreso de datos.
    LimpiarCampos;
    KeyPreview := True;
  	dbl_VentasPasesDiarios.Reload;
  	Result := True;
end;

procedure TfrmABMVentasPasesDiarios.LimpiarCampos;
begin
    de_FechaVenta.Date              := NowBase(DMConnections.BaseCAC); 
    cb_TipoPaseDiario.Value         := TPD_NORMAL;
    ne_MontoTotal.Value             := 0;
end;

procedure TfrmABMVentasPasesDiarios.VolverCampos;
begin
  	dbl_VentasPasesDiarios.Estado   := Normal;
  	dbl_VentasPasesDiarios.Enabled  := True;
  	dbl_VentasPasesDiarios.SetFocus;
  	Notebook.PageIndex              := 0;
    pnl_Datos.Enabled     			    := False;
    de_FechaVenta.Enabled           := True;
    cb_TipoPaseDiario.Enabled       := True;
    ne_MontoTotal.Enabled           := True;
end;

{******************************** Function Header ******************************
Function Name: CargarComboVendedores
Author : ggomez
Date Created : 23/08/2005
Description : Carga el Combo de Vendedores de Pases Diarios.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMVentasPasesDiarios.CargarComboVendedores;
resourcestring
    MSG_ERROR_CARGAR_VENDEDORES = 'Ha ocurrido un error al obtener los datos de los vendedores.';
var
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(nil);
    try
        try
            sp.Connection := DMConnections.BaseCAC;
            sp.ProcedureName := 'ObtenerVendedoresDayPass';
            sp.Open;
            cb_Vendedor.Clear;
            while not sp.Eof do begin
                cb_Vendedor.Items.Add(sp.FieldByName('Nombre').AsString, sp.FieldByName('CodigoVendedorDayPass').AsInteger);
                sp.Next;
            end;
            cb_Vendedor.ItemIndex := 0;
        except
            on e: Exception do MsgBoxErr(MSG_ERROR_CARGAR_VENDEDORES, E.Message,Self.Caption, MB_ICONSTOP);
        end;
    finally
        sp.Free;
    end; // finally

end;

{******************************** Function Header ******************************
Function Name: CargarComboTiposPasesDiarios
Author : ggomez
Date Created : 22/08/2005
Description : Carga el Combo de Tipos de Pases Diarios.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMVentasPasesDiarios.CargarComboTiposPasesDiarios;
begin
    cb_TipoPaseDiario.Clear;
    cb_TipoPaseDiario.Items.Add(TPD_NORMAL_DESC, TPD_NORMAL);
    cb_TipoPaseDiario.Items.Add(TPD_TARDIO_DESC, TPD_TARDIO);
    cb_TipoPaseDiario.Value := TPD_NORMAL;
end;

procedure TfrmABMVentasPasesDiarios.FormShow(Sender: TObject);
begin
  	dbl_VentasPasesDiarios.Reload;
end;

procedure TfrmABMVentasPasesDiarios.dbl_VentasPasesDiariosClick(Sender: TObject);
begin
    with (Sender AS TDbList).Table do begin
       de_FechaVenta.Date           := FieldByName('FechaVenta').AsDateTime;
       cb_TipoPaseDiario.Value      := FieldByName('TipoPaseDiario').AsString;
       ne_MontoTotal.Value          := FieldByname('MontoTotal').AsFloat / 100;
    end;
end;

procedure TfrmABMVentasPasesDiarios.dbl_VentasPasesDiariosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;Cols: TColPositions);
begin
    with Sender.Canvas, Tabla  do begin
        FillRect(Rect);
        TextOut(Cols[0], Rect.Top, FieldbyName('FechaVenta').AsString);
        TextOut(Cols[1], Rect.Top, iif(Trim(FieldbyName('TipoPaseDiario').AsString) = TPD_NORMAL, TPD_NORMAL_DESC, TPD_TARDIO_DESC));
        TextOut(Cols[2], Rect.Top, Format('%f',[(FieldbyName('MontoTotal').AsFloat / 100)]));
        TextOut(Cols[3], Rect.Top, Trim(FieldbyName('CodigoUsuario').AsString));
        TextOut(Cols[4], Rect.Top, FieldbyName('FechaHora').AsString);
        TextOut(Cols[5], Rect.Top, FieldbyName('NumeroProcesoContabilizacion').AsString);
    end;
end;

procedure TfrmABMVentasPasesDiarios.dbl_VentasPasesDiariosRefresh(Sender: TObject);
begin
  	if dbl_VentasPasesDiarios.Empty then LimpiarCampos;
end;

procedure TfrmABMVentasPasesDiarios.dbl_VentasPasesDiariosInsert(Sender: TObject);
begin
  	LimpiarCampos;
    pnl_Datos.Enabled               := True;
  	dbl_VentasPasesDiarios.Enabled  := False;
  	Notebook.PageIndex              := 1;
  	ActiveControl                   := de_FechaVenta;
end;

procedure TfrmABMVentasPasesDiarios.dbl_VentasPasesDiariosEdit(Sender: TObject);
begin
    if not tbl_VentasPasesDiarios.FieldByName('NumeroProcesoContabilizacion').IsNull then begin
        MsgBox(MSG_VENTA_CONTABILIZADA, Self.Caption, MB_ICONINFORMATION);
        VolverCampos;
        Exit;
    end;
  	dbl_VentasPasesDiarios.Enabled  := False;
  	dbl_VentasPasesDiarios.Estado   := Modi;
  	Notebook.PageIndex              := 1;
  	pnl_Datos.Enabled               := True;
  	ActiveControl                   := de_FechaVenta;
end;

procedure TfrmABMVentasPasesDiarios.dbl_VentasPasesDiariosDelete(Sender: TObject);
begin
    if not tbl_VentasPasesDiarios.FieldByName('NumeroProcesoContabilizacion').IsNull then begin

        MsgBox(MSG_VENTA_CONTABILIZADA, Self.Caption, MB_ICONINFORMATION);

    end else begin

        // Mostrar mensaje de confirmaci�n de la eliminaci�n.
        if MsgBox(Format(MSG_QUESTION_ELIMINAR, [STR_MAESTRO_VENTAS_PASES_DIARIOS]),STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            try
                with sp_EliminarVentaPaseDiario do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@FechaVenta').Value             := dbl_VentasPasesDiarios.Table.FieldbyName('FechaVenta').Value;
                    Parameters.ParamByName('@TipoPaseDiario').Value         := dbl_VentasPasesDiarios.Table.FieldbyName('TipoPaseDiario').Value;
                    Parameters.ParamByName('@CodigoVendedorDayPass').Value  := dbl_VentasPasesDiarios.Table.FieldByName('CodigoVendedorDayPass').AsInteger;
                    ExecProc;
                end;
            except
                on E: Exception do begin
                    MsgBoxErr(Format(MSG_ERROR_ELIMINAR, [STR_MAESTRO_VENTAS_PASES_DIARIOS]), E.Message, Format(MSG_CAPTION_ELIMINAR, [STR_MAESTRO_VENTAS_PASES_DIARIOS]), MB_ICONSTOP);
                end;
            end;
        end;

    end;

  	dbl_VentasPasesDiarios.Reload;
  	dbl_VentasPasesDiarios.Estado   := Normal;
  	dbl_VentasPasesDiarios.Enabled  := True;
  	Notebook.PageIndex              := 0;
end;

procedure TfrmABMVentasPasesDiarios.btn_AceptarClick(Sender: TObject);
resourcestring
    FLD_FECHA_VENTA         = 'Fecha de Venta';
    FLD_TIPO_PASE_DIARIO    = 'Tipo de Pase Diario';
    MONTO_SUPERIOR_A_CERO   = 'El Monto debe ser superior a cero.';
    MSG_PASE_DIARIO_EXISTENTE = 'Ya existen datos para la Fecha de Venta, el Tipo de Pase Diario y Vendedor ingresados.';
    MSG_FECHA_VENTA_DEBE_SER_ANTERIOR_O_IGUAL_A_FECHA_ACTUAL = 'La Fecha de Venta debe ser anterior o igual a la Fecha Actual.';

begin
    if not ValidateControls([de_FechaVenta,
            de_FechaVenta,
            cb_TipoPaseDiario,
            ne_MontoTotal],
            [de_FechaVenta.Date <> NullDate,
            de_FechaVenta.Date <= NowBase(DMConnections.BaseCAC),
            cb_TipoPaseDiario.Value <> EmptyStr,
            ne_MontoTotal.Value > 0],
            Format(MSG_CAPTION_ACTUALIZAR, [STR_MAESTRO_VENTAS_PASES_DIARIOS]),
            [Format(MSG_VALIDAR_DEBE_EL, [FLD_FECHA_VENTA]),
            MSG_FECHA_VENTA_DEBE_SER_ANTERIOR_O_IGUAL_A_FECHA_ACTUAL,
            Format(MSG_VALIDAR_DEBE_EL, [FLD_TIPO_PASE_DIARIO]),
            MONTO_SUPERIOR_A_CERO]) then begin
      Exit;
    end;

    // Si estoy insertando, controlar que la
    // FechaVenta, TipoPaseDiario, CodigoVendedorDayPass NO exista en la tabla.
    if (dbl_VentasPasesDiarios.Estado = Alta) then begin
        if dbl_VentasPasesDiarios.Table.Locate('FechaVenta;TipoPaseDiario;CodigoVendedorDayPass', VarArrayOf([de_FechaVenta.Date, cb_TipoPaseDiario.Value, cb_Vendedor.Value]), []) then begin
            MsgBox(MSG_PASE_DIARIO_EXISTENTE, Self.Caption, MB_ICONSTOP);
            Exit;
        end;
    end;

    // Si estoy editando y NO se quiere modificar el mismo registro, controlar que la
    // FechaVenta, TipoPaseDiario, CodigoVendedorDayPass NO exista en la tabla.
    if (dbl_VentasPasesDiarios.Estado = Modi) and ( (de_FechaVenta.Date <> dbl_VentasPasesDiarios.Table.FieldByName('FechaVenta').AsDateTime) or (cb_TipoPaseDiario.Value <> dbl_VentasPasesDiarios.Table.FieldByName('TipoPaseDiario').AsString) or (cb_Vendedor.Value <> dbl_VentasPasesDiarios.Table.FieldByName('CodigoVendedorDayPass').AsInteger) ) then begin
        if dbl_VentasPasesDiarios.Table.Locate('FechaVenta;TipoPaseDiario;CodigoVendedorDayPass', VarArrayOf([de_FechaVenta.Date, cb_TipoPaseDiario.Value, cb_Vendedor.Value]), []) then begin
            MsgBox(MSG_PASE_DIARIO_EXISTENTE, Self.Caption, MB_ICONSTOP);
            Exit;
        end;
    end;

	with dbl_VentasPasesDiarios do begin

  		Screen.Cursor := crHourGlass;

	  	try
  	  		try
                case Estado of
                    Alta:   begin
                                with sp_InsertarVentaPaseDiario, Parameters do begin
                                    Refresh;
                                    ParamByName('@FechaVenta').Value        := de_FechaVenta.Date;
                                    ParamByName('@TipoPaseDiario').Value    := cb_TipoPaseDiario.Value;
                                    ParamByName('@CodigoVendedorDayPass').Value := cb_Vendedor.Value;
                                    ParamByName('@MontoTotal').Value        := ne_MontoTotal.Value * 100;
                                    ParamByName('@CodigoUsuario').Value     := UsuarioSistema;
                                    ExecProc;
                                end;
                            end;
                    Modi:   begin
                                with sp_ActualizarVentaPaseDiario, Parameters do begin
                                    Refresh;
                                    ParamByName('@FechaVenta').Value            := dbl_VentasPasesDiarios.Table.FieldByName('FechaVenta').AsDateTime;
                                    ParamByName('@TipoPaseDiario').Value        := dbl_VentasPasesDiarios.Table.FieldByName('TipoPaseDiario').AsString;
                                    ParamByName('@CodigoVendedorDayPass').Value := dbl_VentasPasesDiarios.Table.FieldByName('CodigoVendedorDayPass').AsInteger;
                                    ParamByName('@FechaVentaNueva').Value       := de_FechaVenta.Date;
                                    ParamByName('@TipoPaseDiarioNuevo').Value   := cb_TipoPaseDiario.Value;
                                    ParamByName('@CodigoVendedorDayPassNuevo').Value := cb_Vendedor.Value;
                                    ParamByName('@MontoTotal').Value            := ne_MontoTotal.Value * 100;
                                    ParamByName('@CodigoUsuario').Value         := UsuarioSistema;
                                    ExecProc;
                                end;
                            end;
                end; // case
		  	  except
			  	    on E: Exception do begin
  			  	    	MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR, [STR_MAESTRO_VENTAS_PASES_DIARIOS]), E.Message, Format(MSG_CAPTION_ACTUALIZAR, [STR_MAESTRO_VENTAS_PASES_DIARIOS]), MB_ICONSTOP);
				      end;
			    end;
      finally
          VolverCampos;
      		Screen.Cursor := crDefault;
          Reload;
		  end;
	end; // with
end;

procedure TfrmABMVentasPasesDiarios.btn_CancelarClick(Sender: TObject);
begin
  	VolverCampos;
end;

procedure TfrmABMVentasPasesDiarios.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMVentasPasesDiarios.abmtb_VentasPasesDiariosClose(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMVentasPasesDiarios.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	  Action := caFree;
end;

end.
