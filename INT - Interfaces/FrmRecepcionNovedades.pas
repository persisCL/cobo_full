{------------------------------------------------------------------------------
 File Name: FrmRecepcionNovedades.pas
 Author:    lgisuk
 Date Created: 17/06/2005
 Language: ES-AR
 Description: M�dulo de la interface Falabella -
                Procesar Respuesta recibida del archivo de Novedades Enviado
--------------------------------------------------------------------------------}
{Revision History
-----------------------------------------------------------------------------}
{Author: lgisuk
05/08/2005: Correcci�n de identaci�n
05/08/2005: Obtengo los Parametros Generales que se utilizaran en el formulario al inicializar y verifico que los valores obtenidos sean validos
------------------------------------------------------------------------------}
//Author		:Mcabello
//Firma			:SS_1061_MCA_20131118
//Descripcion	: se cambia posicion de la obtencion del codigo respuesta.
//				se comenta varia i ya que no es usada



//Firma       : SS_1147Q_NDR_20141202[??]
//Descripcion : Si es VS el convenio es de largo 12 no 17

{------------------------------------------------------------------------------
Autor       :   Claudio Quezada
Fecha       :   24-03-2015
Firma       :   SS_1147_CQU_20150324
Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Autor       :   Claudio Quezada
Fecha       :   28-04-2015
Firma       :   SS_1147_CQU_20150428
Descripcion :   Se ELIMINA parte del desarrollo con firma SS_1147_CQU_20150324
                ya que la unica diferencia entre CN y VS es el numero de convenio.
                Todas las posiciones son iguales.
------------------------------------------------------------------------------}

unit FrmRecepcionNovedades;

interface

uses
    //Recepcion de Novedades
  	DMConnection,                    //Coneccion a base de datos OP_CAC
    Util,                            //Stringtofile,padl..
    UtilProc,                        //Mensajes
    ConstParametrosGenerales,        //Obtengo Valores de la Tabla Parametros Generales;
    ComunesInterfaces,               //Procedimientos y Funciones Comunes a todos los formularios
    Peatypes,                        //Constantes
    PeaProcs,                        //NowBase
    FrmRptErroresSintaxis,           //Reporte de Errores de sintaxis
    FrmRptRecepcionNovedades,        //Reporte del Proceso
    //General
  	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  	Dialogs, StdCtrls, Buttons, DB, ADODB, ExtCtrls, Grids, DBGrids,
  	DPSControls, ListBoxEx, DBListEx, ComCtrls, StrUtils, UtilRB,
    ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd, ppReport, ppDB,
    ppComm, ppRelatv, ppTxPipe, UtilDB, ppDBPipe, ppModule, raCodMod, DBClient,
    Math;

type

  //Estructura donde voy a almacenar cada mandato
  TMandato = Record
        TipoOperacion        : String;
        NumeroConvenio       : String;
        NumeroDocumento      : String;
        NumeroTarjetaCredito : String;
        FechaVencimiento     : String;
        CodigoRespuesta      : String;
        GlosaRespuesta       : String;
        DiadeVencimiento     : String;
  end;

  TFRecepcionNovedades = class(TForm)
    OpenDialog: TOpenDialog;
    pnlOrigen: TPanel;
    btnAbrirArchivo: TSpeedButton;
    txtOrigen: TEdit;
    lblOrigen: TLabel;
    SPProcesarNovedadesCMR: TADOStoredProc;
    btnProcesar: TButton;
    btnSalir: TButton;
    Bevel: TBevel;
    lblMensaje: TLabel;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    cdsRespuestaCMR: TClientDataSet;
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtOrigenChange(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
    { Private declarations }
    FCodigoOperacion : Integer;
    FErrorGrave : Boolean;      //Indica que se produjo un error
    FCancelar : Boolean;        //Indica si ha sido cancelado por el usuario
    FTotalRegistros : Integer;
    FCantidadControl : Real;
    FCMR_CodigodeAutopista : Ansistring;
    FCMR_Directorio_Origen_Novedades : AnsiString;
    FCMR_Directorio_Procesados : AnsiString;
    FCMR_Directorio_Errores : AnsiString;
    FCodigoNativa : Integer;    // SS_1147_CQU_20150324
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  	Function   Abrir : Boolean;
    Function   Control : Boolean;
    Function   ConfirmaCantidades(Lineas: Integer): Boolean;
    function   ParseLineToRecord(Linea : String; var Mandato: TMandato; var DescError : String) : Boolean;
    Function   AnalizarMandatosTXT : Boolean;
    function   RegistrarOperacion : Boolean;
    Function   ActualizarMandatos : Boolean;
    Function   GenerarReporteErroresSintaxis : Boolean;
    Function   GenerarReporteFinalizacionProceso(CodigoOperacionInterfase : Integer;CantidadRegistros, CantidadRegistrosReal: AnsiString) : Boolean;
  public
    { Public declarations }
  	Function  Inicializar(txtCaption : ANSIString; MDIChild : Boolean) : Boolean;
  end;

var
  FRecepcionNovedades : TFRecepcionNovedades;
  FLista : TStringList;
  FErrores : TStringList;

Const
  RO_MOD_INTERFAZ_ENTRANTE_NOVEDADES_CMR = 49;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    Lgisuk
  Date Created: 17/06/2005
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean

  Revision 1
  Author: nefernandez
  Date: 07/05/2008
  Description: SS 673: Se crea la tabla cliente para cargar los datos del archivo de respuesta
-----------------------------------------------------------------------------}
function TFRecepcionNovedades.Inicializar(txtCaption : ANSIString; MDIChild : Boolean) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 25/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        CMR_CODIGODEAUTOPISTA             = 'CMR_CodigodeAutopista';
        CMR_DIRECTORIO_ORIGEN_NOVEDADES   = 'CMR_Directorio_Origen_Novedades';
        CMR_DIRECTORIO_ERRORES            = 'CMR_Directorio_Errores';
        CMR_DIRECTORIO_PROCESADOS         = 'CMR_Directorio_Procesados';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la concesionaria nativa                  // SS_1147_CQU_20150324
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150324

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_CODIGODEAUTOPISTA , FCMR_CodigodeAutopista) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_CODIGODEAUTOPISTA;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FCMR_CodigodeAutopista <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + CMR_CODIGODEAUTOPISTA;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_DIRECTORIO_ORIGEN_NOVEDADES , FCMR_Directorio_Origen_Novedades) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_DIRECTORIO_ORIGEN_NOVEDADES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FCMR_Directorio_Origen_Novedades := GoodDir(FCMR_Directorio_Origen_Novedades);
                if  not DirectoryExists(FCMR_Directorio_Origen_Novedades) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FCMR_Directorio_Origen_Novedades;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_DIRECTORIO_ERRORES , FCMR_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FCMR_Directorio_Errores := GoodDir(FCMR_Directorio_Errores);
                if  not DirectoryExists(FCMR_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FCMR_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,CMR_DIRECTORIO_PROCESADOS, FCMR_Directorio_Procesados);

            except
                on E : Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, E.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Centro el formulario
  	CenterForm(Self);
  	try
  		DMConnections.BaseCAC.Connected := True;
        //Revision 1
        cdsRespuestaCMR.CreateDataSet;
        cdsRespuestaCMR.LogChanges := False;

  		Result := DMConnections.BaseCAC.Connected and
                           VerificarParametrosGenerales;


  	except
        on E : Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
  	end;
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Caption := AnsiReplaceStr(txtCaption, '&', '');  //titulo
  	btnProcesar.enabled := False; //Procesar
  	lblmensaje.Caption := '';     //limpio el indicador
    PnlAvance.visible := False;    //Oculto el Panel de Avance
    FCodigoOperacion := 0;         //inicializo codigo de operacion
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 03/06/2005
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionNovedades.ImgAyudaClick(Sender: TObject);
Resourcestring
     MSG_RESPUESTANOVEDADES = ' ' + CRLF +
                              'El Archivo de Respuesta a Novedades' + CRLF +
                              'es la respuesta que env�a FALABELLA al ESTABLECIMIENTO' + CRLF +
                              'para cada Archivo de Novedades recibido' + CRLF +
                              'y tiene por objeto dar a conocer al ESTABLECIMIENTO,' + CRLF +
                              'el resultado del procesamiento de la informaci�n enviada.' + CRLF +
                              ' ' + CRLF +
                              'Nombre del Archivo: 04_CCNOVEDADES_DDMMAA.xx' + CRLF +
                              ' ' + CRLF +
                              'Se utiliza para actualizar los datos de' + CRLF +
                              'los mandantes en el sistema del ESTABLECIMIENTO.' + CRLF +
                               ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = True then Exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_RESPUESTANOVEDADES, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 03/06/2005
  Description: permito hacer click en la ayuda solo si no esta procesando
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionNovedades.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.Visible = True then ImgAyuda.Cursor := crDefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarArchivoBTNClick
  Author:    lgisuk
  Date Created: 03/01/2005
  Description: busco el archivo a procesar
  Parameters: Sender: TObject
  Return Value: None

  Revision 1
  nefernandez
  04/03/2008
  Se cambia el mensaje de error para indicar que es el Nombre del Archivo el
  que tiene el formato no v�lido
-----------------------------------------------------------------------------}
procedure TFRecepcionNovedades.btnAbrirArchivoClick(Sender: TObject);

    //Verifica si es un Archivo Valido para Esta Pantalla
    Function EsArchivoValido(Nombre : AnsiString) : Boolean;
    begin
        Result:= ExistePalabra(Nombre, FCMR_CodigodeAutopista) and //posee el codigo de Autopista
            	         //ExistePalabra(Nombre, 'CCNOVEDADES');            // SS_1147_CQU_20150324
                         ExistePalabra(UpperCase(Nombre), 'CCNOVEDADES');   // SS_1147_CQU_20150324
    end;

resourcestring
    MSG_ERROR = 'El nombre del archivo de Respuesta de Novedades No es V�lido!'; //Revision 1
begin
    OpenDialog.InitialDir := FCMR_Directorio_Origen_Novedades;
    if OpenDialog.execute then begin
        if not EsArchivoValido (opendialog.filename) then begin
            MsgBox(MSG_ERROR, self.Caption, MB_OK + MB_ICONINFORMATION);
            txtOrigen.text := '';
        end
        else begin
            BTNAbrirArchivo.Enabled := False;
            txtOrigen.text := OpenDialog.FileName;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EOrigenChange
  Author:    lgisuk
  Date Created: 01/07/2005
  Description: Controlo que abra un archivo valido
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionNovedades.txtOrigenChange(Sender: TObject);
begin
    //Controlo que abra un archivo valido
  	btnProcesar.Enabled := FileExists( txtOrigen.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: Abrir
  Author:    lgisuk
  Date Created: 16/12/2004
  Description: Abro el Archivo TXT
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFRecepcionNovedades.Abrir : Boolean;
resourcestring
  	MSG_OPEN_FILE_ERROR = 'Error al abrir el archivo';
begin
    Result := False;
    FLista.text := FileToString(txtOrigen.text);
    FTotalRegistros := FLista.count-1;
    if FLista.text <> '' then  begin
        Result := True;
        if FCMR_Directorio_Procesados <> '' then begin
            if RightStr(FCMR_Directorio_Procesados,1) = '\' then  FCMR_Directorio_Procesados := FCMR_Directorio_Procesados + ExtractFileName(txtOrigen.text)
            else FCMR_Directorio_Procesados := FCMR_Directorio_Procesados + '\' + ExtractFileName(txtOrigen.text);
        end;
    end else begin
        MsgBox(MSG_OPEN_FILE_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Control
  Author:    lgisuk
  Date Created: 22/06/2005
  Description: verifico la consistencia entre los valores reflejados
               en el archivo de control y los mismos conceptos calculados
               a partir de los registros de detalle.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFRecepcionNovedades.Control : Boolean;
resourcestring
    MSG_NOT_EQUAL_COUNT = 'La cantidad reflejada en el registro de control no coincide'+ CRLF + 'con la cantidad calculada!';
    MSG_ERROR           = 'No se pudo realizar el control del archivo!';
begin
    Result := False;
    try
        //Obtengo la cantidad y la suma del archivo de control
        FCantidadControl := StrToFloat(Trim(Copy(Flista.Strings[0], 9, 12)));
        //comparo la cantidad contra la calculada
        If FCantidadControl <> FTotalRegistros then begin
            MsgBoxErr(MSG_NOT_EQUAL_COUNT, '', self.text, 0);
            exit;
        end;
                    
        Result := True;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
    		end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ConfirmaCantidades
  Author:    Lgisuk
  Date Created: 17/06/2005
  Description:  Consulta al usuario si quiere continuar o no,
                presentandole un resumen de las cantidades a procesar
                y de las cantidades existentes.
  Parameters: Lineas: integer
  Return Value: Boolean

 Revision 1
 Author : Fsandi
 Date : 12-06-2007
 Description : Se modifica las consultas SQL en delphi, se las cambia para que llamen funciones de la Base de Datos

 Revision 1
 Author : Fsandi
 Date : 12-06-2007
 Description : Ahora se ponen mensajes de advertencia o error si la diferencia es mucha entre el total mandatos y el archivo

 Revision 2:
 Author : Fsandi
 Date : 12-06-2007
 Description :  Se eliminan los mensajes de advertencia por solicitud del cliente
-----------------------------------------------------------------------------}
function TFRecepcionNovedades.ConfirmaCantidades(Lineas : Integer): Boolean;
resourcestring
    MSG_TITLE        = 'Confirmar Recepci�n de Novedades PAT - Falabella';
    MSG_CONFIRMATION = 'Actualmente existen %d Mandatos Confirmados '+crlf+
                       'y %d Mandatos Pendientes, que hacen'+crlf+
                       'un total de %d Mandatos PAT - Falabella Existentes.'+crlf+crlf+
                       'El Archivo seleccionado contiene %d Mandatos a procesar.';
    MSG_KEEP_PROCESSING = 'Desea continuar con el procesamiento del archivo?';
var
    MandatosPendientes, MandatosConfirmados, MandatosExistentes : Integer;
    Mensaje : String;
    MensajeCompleto : String;
    CodigoEmisorTarjetaFalabella : Integer;
begin

    CodigoEmisorTarjetaFalabella := QueryGetValueInt(DMConnections.BaseCAC,'select dbo.CONST_EMISOR_TARJETA_FALABELLA()');
    //Obtengo la cantida de mandatos Existentes/Pendientes y Confirmados
    MandatosExistentes  :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[4,CodigoEmisorTarjetaFalabella]));
    MandatosPendientes  :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[5,CodigoEmisorTarjetaFalabella]));
    MandatosConfirmados :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[6,CodigoEmisorTarjetaFalabella]));

    //Armo el mensaje
    Mensaje := Format(MSG_CONFIRMATION,[MandatosConfirmados,MandatosPendientes,MandatosExistentes, Lineas]);
    //lo muestro
    //Armo el mensaje
    MensajeCompleto := Format(MSG_CONFIRMATION,[MandatosConfirmados,MandatosPendientes,MandatosExistentes, Lineas]);

    MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING;
    Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
end;


{-----------------------------------------------------------------------------
  Function Name: ParseLineToRecord
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: Arma un registro de mandato en base a la linea leida del TXT
  Parameters: sline : string; var MandatoRecord : TMandatoRecord
  Return Value: boolean
  Revision :2
      Author : nefernandez
      Date : 07/05/2008
      Description : SS 673: Despu�s a analizar correctamente un registro, se carga
      en la tabla cliente, en donde est�n ordenados primero las Bajas y despu�s las Altas
-----------------------------------------------------------------------------}
function TFRecepcionNovedades.ParseLineToRecord(Linea : String; var Mandato: TMandato; var DescError : String) : Boolean;
Resourcestring
  STR_CONVENIO               = 'Convenio: ';
  STR_SEPARADOR              = ' - ';
  MSG_ERROR_TIPO_OPERACION   = 'Tipo operacion no es A/B/M';
  MSG_ERROR_NUMERO_CONVENIO  = 'Tiene una longitud menor a 17 caracteres';
  MSG_ERROR_CODIGO_RESPUESTA = 'Campo codigo de respuesta viene vacio';
  MSG_ERROR                  = 'Error al leer el registro';
Const
  PREFIJO = '00100';
begin
    Result := False;
    DescError := '';
    try
        with Mandato do begin
            //Parseo la linea
            TipoOperacion       := Trim(Copy(Linea, 1,  2 ));
            //NumeroConvenio      := PREFIJO + Trim(Copy(Linea, 11, 12));       // SS_1147_CQU_20150428
            if FCodigoNativa = CODIGO_VS                                        // SS_1147_CQU_20150428
            then NumeroConvenio      := Trim(Copy(Linea, 11, 12))               // SS_1147_CQU_20150428
            else NumeroConvenio      := PREFIJO + Trim(Copy(Linea, 11, 12));    // SS_1147_CQU_20150428
            NumeroDocumento     := Trim(Copy(Linea, 31, 11));
            NumeroTarjetaCredito:= Trim(Copy(Linea, 42, 19));
            FechaVencimiento    := Trim(Copy(Linea, 61, 5 ));
            //CodigoRespuesta     := Trim(Copy(Linea, 78, 2 )); //77, 3//       //SS_1061_MCA_20131118
            CodigoRespuesta     := Trim(Copy(Linea, 77, 2 ));                   //SS_1061_MCA_20131118
            GlosaRespuesta      := Trim(Copy(Linea, 80, 20));
            DiaDeVencimiento    := Trim(copy(Linea, 100, 102));

            //Verifico el tipo de operacion
            if not((TipoOperacion = 'A') or (TipoOperacion = 'B') or (TipoOperacion = 'M')) then begin
                DescError := STR_CONVENIO + NumeroConvenio + STR_SEPARADOR +  MSG_ERROR_TIPO_OPERACION;
                Exit;
            end;
            //Verifico que la longitud del numero de convenio sea de 17 caracteres
            //if length(Numeroconvenio) < 17 then begin                                     // SS_1147_CQU_20150324
            if ((FCodigoNativa <> CODIGO_VS) and (length(Numeroconvenio) < 17)) then begin  // SS_1147_CQU_20150324
                DescError := STR_CONVENIO + NumeroConvenio + STR_SEPARADOR + MSG_ERROR_NUMERO_CONVENIO;
                Exit;
            end;
            //Verifico codigo de respuesta
            if CodigoRespuesta = '' then begin
                DescError := STR_CONVENIO + NumeroConvenio + STR_SEPARADOR + MSG_ERROR_CODIGO_RESPUESTA;
                Exit;
            end;

            //if ObtenerCodigoConcesionariaNativa=CODIGO_VS then                // SS_1147_CQU_20150324  //SS_1147Q_NDR_20141202[??]
            //begin                                                             // SS_1147_CQU_20150324  //SS_1147Q_NDR_20141202[??]
            //  NumeroConvenio := Copy( NumeroConvenio, 6, 12);                 // SS_1147_CQU_20150324  //SS_1147Q_NDR_20141202[??]
            //end;                                                              // SS_1147_CQU_20150324  //SS_1147Q_NDR_20141202[??]

            //Revision 2
            cdsRespuestaCMR.Append;
            if (TipoOperacion = 'B') then
                cdsRespuestaCMR.FieldByName('Orden').AsInteger := 1
            else if (TipoOperacion = 'A') then
                cdsRespuestaCMR.FieldByName('Orden').AsInteger := 2
            else
                cdsRespuestaCMR.FieldByName('Orden').AsInteger := 3;

            cdsRespuestaCMR.FieldByName('TipoOperacion').AsString := TipoOperacion;
            cdsRespuestaCMR.FieldByName('NumeroConvenio').AsString := NumeroConvenio;
            cdsRespuestaCMR.FieldByName('NumeroDocumento').AsString := NumeroDocumento;
            cdsRespuestaCMR.FieldByName('NumeroTarjetaCredito').AsString := NumeroTarjetaCredito;
            cdsRespuestaCMR.FieldByName('FechaVencimiento').AsString := FechaVencimiento;
            cdsRespuestaCMR.FieldByName('CodigoRespuesta').AsString := CodigoRespuesta;
            cdsRespuestaCMR.FieldByName('GlosaRespuesta').AsString := GlosaRespuesta;
            cdsRespuestaCMR.FieldByName('DiadeVencimiento').AsString := DiadeVencimiento;
            cdsRespuestaCMR.Post;

        end;
        Result := True;
    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, Self.Caption, MB_ICONERROR);
            FErrorGrave:=true;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: AnalizarMandatosTXT
  Author:    lgisuk
  Date Created: 01/07/2005
  Description: Antes de hacer nada Parsea todo el Archivo para verificar
  				que el parseo sea v�lido
  Parameters: None
  Return Value: Boolean

  Revision 1
  Author: nefernandez
  Date: 07/05/2008
  Description: SS 673: Se abre la tabla cliente para cargar los datos del archivo de respuesta
-----------------------------------------------------------------------------}
Function TFRecepcionNovedades.AnalizarMandatosTXT : Boolean;
Resourcestring
    MSG_ERROR_SYNTAXIS = 'El Archivo contiene errores de Sintaxis';
var
    I : Integer;
    Mandato : TMandato;
    DescError : String;
begin
    FCancelar := False;                //Permito que la importacion sea cancelada
    PbProgreso.Position := 0;          //Inicio la Barra de progreso
    PbProgreso.Max := FLista.Count-1;  //Establezco como maximo la cantidad de registros del memo
    FErrores.Clear;

    //Revision 1
    if not cdsRespuestaCMR.Active then cdsRespuestaCMR.Active := True;
    cdsRespuestaCMR.EmptyDataSet;

    I := 1;
    //recorro las lineas del archivo
    while I < FLista.count do begin

        //si cancelan la operacion
        if FCancelar then begin
            Result := False;             //El analisis fue cancelado por el usuario
            pbProgreso.Position := 0;  //Si la importacion fue detenida, Vuelvo a Comenzar
            Exit;                      //salgo de la rutina
        end;

        //Arma un registro de mandato en base a la linea leida del TXT
        //verifica que los valores recibidos sean validos sino devuelve una
        //Descripci�n del error.
        if not ParseLineToRecord(Flista.Strings[i], Mandato, DescError) then begin
            //lo inserto en el string list de errores
            FErrores.Add(DescError);
        end;

        pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso
        Application.ProcessMessages;                    //Refresco la pantalla

        i:= i + 1;
    end;

    Result:= (FErrores.count = 0);

    if (Result = False) then MsgBox(MSG_ERROR_SYNTAXIS, Self.Caption, MB_OK + MB_ICONINFORMATION);

end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    lgisuk
  Date Created: 01/07/2005
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfRecepcionNovedades.RegistrarOperacion : Boolean;
resourcestring
	  MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Procesar Respuesta A Novedades';
var
    NombreArchivo: String;
  	DescError    : String;
begin
   NombreArchivo := Trim(ExtractFileName(TxtOrigen.text));
   Result := RegistrarOperacionEnLogInterface (DMConnections.BaseCAC , RO_MOD_INTERFAZ_ENTRANTE_NOVEDADES_CMR , NombreArchivo, UsuarioSistema , STR_OBSERVACION , False , False, NowBase(DMConnections.BaseCAC) , 0 , FCodigoOperacion , DescError);
   if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarMandatos
  Author:    lgisuk
  Date Created: 21/06/2005
  Description: Proceso el archivo de Respuesta a Novedades
  Parameters: var Respuesta:integer
  Return Value: Boolean
-----------------------------------------------------------------------------
  Revision 3
  nefernandez
  07/05/2008
  Para actualizar el mandato en la base de datos, a partir de ahora se considera
  la tabla cliente que tiene los datos de la recepci�n ordenados por tipo de operaci�n
  (primero las bajas y despu�s las altas).
-----------------------------------------------------------------------------}
Function TFRecepcionNovedades.ActualizarMandatos : Boolean;

      {-----------------------------------------------------------------------------
        Function Name: ActualizarMandato
        Author:    lgisuk
        Date Created: 21/06/2005
        Description: Actualizo los datos del mandante con la informacion recibida
                     en el archivo
        Parameters: Mandato: TMandato; var DescError:string
        Return Value: boolean
      -----------------------------------------------------------------------------}
      Function ActualizarMandato (Mandato: TMandato; var DescError : String) : Boolean;
      resourcestring
        STR_CONVENIO  = 'Convenio: ';
        STR_SEPARADOR = ' - ';
        MSG_ERROR     = 'Error al actualizar el mandato';
      var
          Error : String;
      begin
          Result := False;
          DescError := '';
          Error := '';
          try
                with SPProcesarNovedadesCMR.Parameters do begin
                    Refresh;
                    ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                    ParamByName('@TipoOperacion').Value            := Mandato.TipoOperacion;
                    ParamByName('@NumeroConvenio').Value           := Mandato.NumeroConvenio;
                    ParamByName('@NumeroDocumentoCMR').Value       := Mandato.NumeroDocumento;
                    ParamByName('@NumeroTarjetaCreditoCMR').Value  := Mandato.NumeroTarjetaCredito;
                    ParamByName('@FechaVencimientoCMR').Value      := Mandato.FechaVencimiento;
                    ParamByName('@CodigoRespuesta').Value          := Mandato.CodigoRespuesta;
                    ParamByName('@GlosaRespuesta').Value           := Mandato.GlosaRespuesta;
                    ParamByName('@DiadeVencimiento').Value         := Mandato.DiadeVencimiento;
                    ParamByName('@DescripcionError').Value         := '';
                end;
                with SPProcesarNovedadesCMR do begin
                    CommandTimeout := 500;
                    ExecProc;
                    Error := Trim(Copy(VarToStr(Parameters.ParamByName('@DescripcionError').Value),1,100));
                    if Error <> '' then begin
                        DescError := STR_CONVENIO + Mandato.NumeroConvenio + STR_SEPARADOR + Error;
                        exit;
                    end;
                end;
                Result := True;
          except
              on  E : Exception do begin
                  FErrorGrave := True;
                  MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
              end;
          end;
      end;

var
    //i: Integer;                                                               //SS_1061_MCA_20131118
    Mandato: TMandato;
    DescError: String;
begin
    Result := False;
    FCancelar := False;                //Permito que la importacion sea cancelada
    pbProgreso.Position := 0;          //Inicio la Barra de progreso
    //Revision 3
    //pbProgreso.Max := FLista.Count-1;  //Establezco como maximo la cantidad de registros del memo
    pbProgreso.Max := cdsRespuestaCMR.RecordCount;  //Establezco como maximo la cantidad de registros del memo
    //i:=1;                                                                     //SS_1061_MCA_20131118
    //Revision 3
    cdsRespuestaCMR.First;
    ///while i < FLista.count do begin
    while (not cdsRespuestaCMR.Eof ) do begin

        //si cancelan la operacion
        if FCancelar then begin
             pbProgreso.Position := 0;
             Exit;
        end;

        //Obtengo el registro con los datos del mandante del archivo
        //Revision 3: Ya no se considera el arreglo de string, pues ahora ya se tienen los
        //  datos de la recepci�n en la tabla cliente
        //ParseLineToRecord(Flista.Strings[i], Mandato, DescError);

        //Revisi�n 3: Se toman los datos de la tabla cliente para actualizar el mandato en la base de datos
        Mandato.TipoOperacion := cdsRespuestaCMR.FieldByName('TipoOperacion').Value;
        Mandato.NumeroConvenio := cdsRespuestaCMR.FieldByName('NumeroConvenio').Value;
        Mandato.NumeroDocumento := cdsRespuestaCMR.FieldByName('NumeroDocumento').Value;
        Mandato.NumeroTarjetaCredito := cdsRespuestaCMR.FieldByName('NumeroTarjetaCredito').Value;
        Mandato.FechaVencimiento := cdsRespuestaCMR.FieldByName('FechaVencimiento').Value;
        Mandato.CodigoRespuesta := cdsRespuestaCMR.FieldByName('CodigoRespuesta').Value;
        Mandato.GlosaRespuesta := cdsRespuestaCMR.FieldByName('GlosaRespuesta').Value;
        Mandato.DiadeVencimiento := cdsRespuestaCMR.FieldByName('DiadeVencimiento').Value;

        //Actualizo los datos del mandante.
        ActualizarMandato (Mandato, DescError);

        pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso
        Application.ProcessMessages;                    //Refresco la pantalla
        //i:= i + 1;                                                            //SS_1061_MCA_20131118
        cdsRespuestaCMR.Next;
    end;

    Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: GenerarReporteErroresSintaxis
  Author:    lgisuk
  Date Created: 01/07/2005
  Description: Genero el reporte de errores de sintaxis
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFRecepcionNovedades.GenerarReporteErroresSintaxis : Boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de errores de Sintaxis';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Falabella - Procesar Archivo de Respuesta a Novedades';
var
    F : TFRptErroresSintaxis;
begin
    Result := False;
    try
        //muestro el reporte
        Application.CreateForm(TFRptErroresSintaxis, F);
        if not F.Inicializar( IntToStr(RO_MOD_INTERFAZ_ENTRANTE_NOVEDADES_CMR) , REPORT_TITLE , FCMR_Directorio_Errores ,FErrores) then f.Release;
        Result := True;
    except
       on E : Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarInformedelProceso
  Author:    lgisuk
  Date Created: 23/06/2005
  Description: Muestro un informe de finalizaci�n de proceso
  Parameters: CodigoOperacionInterfase:Integer; NombreReporte, CantidadRegistros, SumatoriaCuentas: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
Function TFRecepcionNovedades.GenerarReporteFinalizacionProceso(CodigoOperacionInterfase:Integer;CantidadRegistros, CantidadRegistrosReal: AnsiString):boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de finalizaci�n de proceso';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Recepci�n Novedades';
var
    F : TFRptRecepcionNovedades;
begin
    Result := False;
    try
        //muestro el reporte
        Application.CreateForm(TFRptRecepcionNovedades, f);
        if not f.Inicializar(CodigoOperacionInterfase, REPORT_TITLE, CantidadRegistros, CantidadRegistrosReal) then f.Release;
        Result := True;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    lgisuk
  Date Created: 04/01/2005
  Description: Procesa el archivo de respuesta a novedades
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionNovedades.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/06/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion : Integer ; var Cantidad : Integer) : Boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+IntToStr(CodigoOperacion)+')');
            Result := True;
        except
           on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 01/07/2005
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog (CantidadErrores : Integer) : Boolean;
   resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : String;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_CANCEL = 'Proceso cancelado por el usuario';
    MSG_PROCESS_NOT_COMPLETE = 'El proceso no se pudo completar';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
  	MSG_PROCESS_FINALLY_OK = 'Proceso finalizado con �xito';
Const
    STR_OPEN_FILE          = 'Abrir Archivo...';
    STR_CHECK_FILE         = 'Control del Archivo...';
    STR_CONFIRM            = 'Confirmar Cantidades...';
    STR_ANALYZING          = 'Analizando Mandatos...';
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
    STR_PROCESS            = 'Actualizando Mandatos...';
var
    CantidadErrores : Integer;
begin
   	btnProcesar.Enabled := False;
    PnlAvance.Visible := True;       //El Panel de Avance es visible durante el procesamiento
    KeyPreview := True;
  	try
        //Inicio la operacion

        //Abro el archivo
    		Lblmensaje.Caption := STR_OPEN_FILE;
    		if not Abrir then begin
            FErrorGrave := True;
            Exit;
        end;

        //Controlo el archivo
        Lblmensaje.Caption := STR_CHECK_FILE;
        if not Control then begin
            FErrorGrave := True;
            Exit;
        end;

        //El usuario acepta o no continuar con el proceso
        Lblmensaje.Caption := STR_CONFIRM;
        if not ConfirmaCantidades(FLista.Count-1) then begin
            FErrorGrave := True;
            Exit;
        end;

        //Analizo si hay errores de parseo o sintaxis en el archivo
        Lblmensaje.Caption := STR_ANALYZING;
        if not AnalizarMandatosTxt then begin
            FErrorGrave := True;
            Exit;
        end;

        //Registro la operaci�n en el Log
        Lblmensaje.Caption := STR_REGISTER_OPERATION;
        if not RegistrarOperacion then begin
            FErrorGrave := True;
            Exit;
        end;

        //Proceso el archivo de respuesta a novedades
    		Lblmensaje.Caption := STR_PROCESS;
    		if not ActualizarMandatos then begin
            FErrorGrave := True;
            Exit;
        end;

        //Obtengo la cantidad de errores
        ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);

        //Actualizo el log al Final
        ActualizarLog(CantidadErrores);

  	finally
        pbProgreso.Position := 0;  //Inicio la Barra de progreso
        PnlAvance.visible := False;  //Oculto el Panel de Avance
    		Lblmensaje.Caption := '';
        if FCancelar then begin
            //Si fue cancelado por el usuario muestro mensaje que el proceso fue cancelado
            MsgBox(MSG_PROCESS_CANCEL, self.Caption, MB_OK + MB_ICONINFORMATION);
        end else if FErrorGrave then begin
            //Si se produjo un error grave muestro un cartel indicando que hubo errores
            MsgBox(MSG_PROCESS_NOT_COMPLETE, self.Caption, MB_OK + MB_ICONINFORMATION);
            //Genero el Reporte de Error con el Report Builder
            if (Ferrores.Count > 0) then GenerarReporteErroresSintaxis;
        end else begin
            //Muestro Mensaje de Finalizaci�n
            MsgProcesoFinalizado(MSG_PROCESS_FINALLY_OK , MSG_PROCESS_FINALLY_WITH_ERROR, self.Caption,
                                FTotalRegistros, FTotalRegistros-CantidadErrores, CantidadErrores);
            //Genero el reporte de finalizacion de proceso
            GenerarReporteFinalizacionProceso(FCodigoOperacion, FloatToStr(FCantidadControl),FloatToStr(FTotalRegistros));
        end;
        KeyPreview := False;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: permito cancelar la operacion
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionNovedades.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionNovedades.btnSalirClick(Sender: TObject);
begin
  	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: impido salir si esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionNovedades.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando
    CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionNovedades.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

initialization
    FLista   := TStringList.Create;
    FErrores := TStringList.Create;
finalization
    FreeAndNil(Flista);
    FreeAndNil(FErrores);
end.


