{********************************** File Header ********************************
File Name : ComponerEMail.pas
Author : gcasais
Date Created: 19/07/2005
Language : ES-AR
Description :
*******************************************************************************}

unit ComponerEMail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, MensajesEMail, Util, UtilProc, DMMensaje;

type
  TfrmComponerMail = class(TForm)
    btnCerrar: TButton;
    Memo: TMemo;
    Bevel1: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    lblPara: TLabel;
    lblAsunto: TLabel;
    procedure btnCerrarClick(Sender: TObject);
  private
    procedure MostrarMail(Mensaje: TMensaje);
  public
    function Inicializar(DM: TDMMensajes; CodigoMensaje: Integer; var Mensaje: TMensaje; var Error: string): Boolean;
  end;

var
  frmComponerMail: TfrmComponerMail;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: TfrmComponerMail.Inicializar
Author : gcasais
Date Created : 19/07/2005
Description :
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TfrmComponerMail.Inicializar(DM: TDMMensajes; CodigoMensaje: Integer; var Mensaje: TMensaje; var Error: string): Boolean;
resourcestring
    MSG_ERROR = 'Error';
    MSG_TITLE = 'Componer e-Mail';
var
    Email: TMensaje;
    ErrorMsg: String;
begin
    Result := ObtenerMensaje(DM, CodigoMensaje, EMail, ErrorMsg);
    if Result then begin
        MostrarMail(Email);
    end else begin
        MsgBoxErr(MSG_ERROR, Error, MSG_TITLE, MB_ICONERROR);
    end;
end;

{******************************** Function Header ******************************
Function Name: TfrmComponerMail.MostrarMail
Author : gcasais
Date Created : 19/07/2005
Description :
Parameters : Mensaje: TMensaje
Return Value : None
*******************************************************************************}
procedure TfrmComponerMail.MostrarMail(Mensaje: TMensaje);
begin
    lblPara.Caption := Mensaje.Para;
    lblAsunto.Caption := Mensaje.Asunto;
    Memo.Text := Mensaje.Mensaje;
end;

procedure TfrmComponerMail.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

end.
