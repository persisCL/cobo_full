{------------------------------------------------------------------------------------
 File Name	: frmGeneracionXMLInfracciones.pas
 Author		: Nelson Droguett Sierra (ndonadio)
 Date		:	07-Abril-2010
 Description: Genera los archivos de infractores para enviar al MOPTT en formato XML.

 Revision	:	1
 Author		: 	Nelson Droguett Sierra
 Date		:	07-Abril-2010
 Descripcion:	Agregar un combo para elegir la concesionaria para la que se generara
                el disco XML. Podria elegir la opci�n "Todas las Concesionarias".

 Revision	:	2
 Author		: 	Nelson Droguett Sierra
 Date		:	18-Mayo-2010
 Descripcion:	(SS-746) Se separan los archivos para los no facturados o facturados
            	no pagados y los que estan facturados y pagados

 Revision	:	3
 Author		: 	pdominguez
 Date		:	23/06/2010
 Descripcion:	Infractores Fase 2
    - Se eliminaron los siguientes objetos:
        ObtenerCantidadInfraccionesAEnviar: TADOStoredProc,
        spObtenerUltimoEnvioInfracciones: TADOStoredProc,
        spObtenerConcesionarias: TADOStoredProc,
        spActualizarDetalleInfraccionesEnviadas: TADOStoredProc,
        spValidarImagenDeReferenciaInfraccion: TADOStoredProc

    - Se a�adieron los siguientes objetos:
        fnObtenerUltimoArchivoDiscoXML: TADOStoredProc,
        spObtenerDatosInfraccionesAEnviarMOP: TADOStoredProc,
        spConsolidarInfraccionesTransitosAEnviarMOP: TADOStoredProc,
        spCargarInfraccionesTransitosAEnviarMOP: TADOStoredProc,
        fnObtenerNombreCortoConcesionaria: TADOStoredProc,
        spActualizarInfraccionesTransitosAEnviarMOP: TADOStoredProc

    - se modificaron / a�adieron los siguientes procedimientos:
        Inicializar,
        cbConcesionariaChange,
        MostrarReporteFinalizacion,
        btnActualizarDatosProcesoClick,
        btnProcesarClick,
        ObtenerDatosInfraccionesAProcesar,
        InicializarDatosInfraccionesAProcesar,
        NombreCarpetaXMLConcesionaria,
        GenerarDiscoXML
            SubFunciones (
                MuestraAviso,
                ValidarCondiciones,
                InicializarInterfase,
                InicializarDiscoArchivos,
                CargarDatosDiscoXML,
                ActualizarDatosXMLTemp,
                ProcesarInfraccion
                    SubFunciones (
                        DatosInfraccionCompletos,
                        EsImagenValida,
                        AlmacenarInfraccionXML)
                GuardarArchivoXML)

 Revision	:	4
 Author		: 	ebaeza
 Date		:	14/06/2011
 firma      :   SS-377-EBA-20110613
 Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos

 Revision	:	5
 Author		: 	Alejandro Labra
 Date		:	08/07/2011
 firma      :   PAR00133-Fase 2-ALA-20110708
 Descripcion:   Se arreglan lo siguiente:
                - M�todo incializar:
                    * Se indenta correctamente
                    * Se comenta llamada a Store ObtenerNombreCortoConcesionaria
                    * Se comenta llamada al m�todo ObtenerDatosInfraccionesAProcesar.
                    * Se habilita bot�n btnActualizarDatosProceso
                - Metodo cbConcesionariaChange:
                    * Se comenta llamada a Store ObtenerNombreCortoConcesionaria
                    * Se comenta llamada al m�todo ObtenerDatosInfraccionesAProcesar.
                - Metodo cbProcesarFacturadosClick
                    * Se comenta llamada al m�todo ObtenerDatosInfraccionesAProcesar.
                - Metodo btnProcesarClick
                    * Se agrega llamada a Store ObtenerNombreCortoConcesionaria
                - Metodo ObtenerDatosInfraccionesAProcesar
                    * Se comenta deshabilitaci�n del bot�n btnActualizarDatosProceso

 Author		: 	Claudio Quezada Ib��ez
 Date		:	17/10/2012
 firma      :   SS_660_CQU_20121010
 Descripcion:   Se agrega el Tipo de Infraccion al XML y a los resultados devueltos por el SP
                Asicionalmente se genera un nuevo TipoDisco para que los archivos por morosidad
                queden en otra carpeta.

Author      :   Claudio Quezada Ib��ez
Date        :   16-Mayo-2013
Firma       :   SS_1091_CQU_20130516
Description :   Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.

Author      :   Claudio Quezada Ib��ez
Date        :   16-Agosto-2013
Firma       :   SS_1091_CQU_20130816
Description :   Se ELIMINAN las l�neas que evitaban enviar la imagen italiana al XML.
                Cuando una imagen es italiana se carga en un Jpeg luego de haberlo cargado como BMP.

Fecha       :   11/10/2013
Firma       :   SS_1138_CQU_20131009
Descripcion :   Se agrega llamada a la funci�n ObtenerTipoImagenPorRegistratioAccesibility
                que devuelve el TTipoImagen seg�n un RegistrationAccessibility indicado,
                de esta manera se podr� cargar la foto que seleccion� el operador
                al validar la Infracci�n.


Fecha           :   30/10/2013
Firma           :   SS_1135_CQU_20131030
Descripcion     :   Se deben obtener las imagenes de infracciones desde el directorio en el que se graban
                    y no desde el directorio de las imagenes de tr�nsitos ya que este �ltimo podr�a ser borrado,
                    a diferencia del de infracciones que debe ser mantenido en el tiempo.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Fecha           :   17/09/2015
Firma           :   SS_1386_CQU_20150917
Descripcion     :   Cuando es un XML de VS se deben enviar el nombre separado
                    por Nombre, Apellido y ApellidoMaterno para ello se agregan los valores.
                    Se agerga detecci�n de la ConcesionariaNativa
 -------------------------------------------------------------------------------------}
unit frmGeneracionXMLInfracciones;

interface

uses
  //Generacion de Archivos de Infracciones
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  PeaTypes,
  UtilDB,
  ComunesInterfaces,
  FrmRptEnvioInfraccionesXML,
  jpeg, // SS_1091_CQU_20130816
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, BuscaTab, DmiCtrls, DPSControls, VariantComboBox,
  StrUtils, Validate, DateEdit, ppPrnabl, ppClass, ppCtrls, ppBarCod,
  ppBands, ppCache, ppComm, ppRelatv, ppProd, ppReport, MOPInfBind,
  XMLDoc, XMLDom, XMLIntf, PeaProcsCN, JPEGPlus, ImgTypes, ImgProcs, SysUtilsCN;
type
  TfGeneracionXMLInfracciones = class(TForm)
    btnProcesar: TButton;
    btnSalir: TButton;
    Bevel1: TBevel;
    spObtenerInfraccionesAEnviarMOP: TADOStoredProc;
    spObtenerInfraccionesTransitosAEnviarMOP: TADOStoredProc;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    //GroupBox2: TGroupBox;		// TASK_161_MGO_20170330
    cbConcesionaria: TVariantComboBox;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    lblProcesadoHastaLaFecha: TLabel;
    lblUltimoDiscoNoRegularizadas: TLabel;
    lblUltimoDiscoRegularizadas: TLabel;
    GroupBox4: TGroupBox;
    Label5: TLabel;
    deFechaDesde: TDateEdit;
    Label3: TLabel;
    deFechaHasta: TDateEdit;
    Label12: TLabel;
    lblFechaMinimaInfraccion: TLabel;
    lblTotalInfraccionesAEnviar: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    lblInfraccionesSinRegularizar: TLabel;
    Label11: TLabel;
    lblInfraccionesRegularizadas: TLabel;
    GroupBox5: TGroupBox;
    peDirectorioImagenes: TPickEdit;
    Label1: TLabel;
    peDirectorioXML: TPickEdit;
    Label13: TLabel;
    GroupBox6: TGroupBox;
    chAnexarADisco: TCheckBox;
    cbProcesarFacturados: TCheckBox;
    gbEstado: TGroupBox;
    pbProgreso: TProgressBar;
    lblArchivosGenerados: TLabel;
    lblInfraccionesRestantes: TLabel;
    Label2: TLabel;
    spObtenerDatosInfraccionesAEnviarMOP: TADOStoredProc;
    btnActualizarDatosProceso: TButton;
    lsbxAvisos: TListBox;
    fnObtenerNombreCortoConcesionaria: TADOStoredProc;
    spCargarInfraccionesTransitosAEnviarMOP: TADOStoredProc;
    fnObtenerUltimoArchivoDiscoXML: TADOStoredProc;
    spActualizarInfraccionesTransitosAEnviarMOP: TADOStoredProc;
    spConsolidarInfraccionesTransitosAEnviarMOP: TADOStoredProc;
    lblInfraccionesMorosas: TLabel;                                 // SS_660_CQU_20121010
    lblInfraccionesMorosasTotal: TLabel;							// SS_660_CQU_20121010
    lblConcesionaria: TLabel;										// TASK_161_MGO_20170330                            
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ImgAyudaClick(Sender: TObject);
    procedure deFechaHastaChange(Sender: TObject);
    procedure peDirectorioXMLButtonClick(Sender: TObject);
    procedure peDirectorioXMLChange(Sender: TObject);
    procedure peDirectorioImagenesButtonClick(Sender: TObject);
    procedure peDirectorioImagenesChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnProcesarClick(Sender: TObject);
    procedure cbProcesarFacturadosClick(Sender: TObject);
    procedure btnActualizarDatosProcesoClick(Sender: TObject);
    procedure cbConcesionariaChange(Sender: TObject);
    procedure chAnexarADiscoClick(Sender: TObject);
  private
    { Private declarations }
    FDirectorioDestino : AnsiString;

    //FImagePath : AnsiString;      // SS-377-EBA-20110613
    FImagePathNFI : AnsiString;     // SS-377-EBA-20110613

    FProcesando : Boolean;
    FCancelar : Boolean;
    FCorrDisco : Integer;
    FCorrFile : Integer;
    FDiskSize : Int64;
    FCodigoOperacion : Integer;
    FPrimeraOperacion : Integer;
    FstrErrores : TStringList;
    FXMLDenuncios : IXMLDenunciosType;
    FCant_Max_Infracciones_Por_Archivo : Integer;

    FNombreCortoConcesionaria: String;
    FUltimoDiscoSinRegularizar,
    FUltimoDiscoRegularizadas,
    FUltimoDiscoMorosos,                // SS_660_CQU_20121010
    FCodigoConcesionaria: Integer;
    FSepararRegularizadas,
    FContinuarDisco: Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    FCodigoNativa : Integer;                                                           // SS_1386_CQU_20150917
    function MostrarReporteFinalizacion(var DescriError : AnsiString): Boolean;

    function ObtenerDatosInfraccionesAProcesar(FechaDesde, FechaHasta: TDateTime; CodigoConcesionaria: Integer; SepararRegularizadas: Boolean): Boolean;
    procedure InicializarDatosInfraccionesAProcesar(DatosGenerales: Boolean = False);
    function NombreCarpetaXMLConcesionaria(DirectorioDestino, NombreCortoConcesionaria: String; Regularizadas: Boolean): String;
    function NombreCarpetaXMLConcesionariaMorosidad(DirectorioDestino, NombreCortoConcesionaria: String): String;   // SS_660_CQU_20121010
    procedure GenerarDiscoXML(CodigoConcesionaria: Integer);
  public
    { Public declarations }
    function Inicializar : Boolean;
  end;

var
  fGeneracionXMLInfracciones: TfGeneracionXMLInfracciones;
  ProcesarFacturados : Integer;


implementation

{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name	: Inicializar
  Author		: Nelson Droguett Sierra (ndonadio)
  Date Created	: 05/07/2005
  Description	: Inicializaci�n de este formulario
  Parameters	: None
  Return Value	: Boolean

  Revision : 1
  Author   : pdominguez
  Date     : 23/06/2010
  Description : Infractores Fase 2
    - Se a�ade la carga del combo de concesionarias, la carga del valor de las
    variables FSepararRegularizadas, FContinuarDisco, FCodigoConcesionaria y
    FNombreCortoConcesionaria.

 Revision	:	4
 Author		: 	ebaeza
 Date		:	14/06/2011
 firma      :   SS-377-EBA-20110613
 Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos
-----------------------------------------------------------------------------}
function TFGeneracionXMLInfracciones.Inicializar : Boolean;

    {-----------------------------------------------------------------------------
      Function Name	: MBToBytes
      Author		: Nelson Droguett Sierra (ndonadio)
      Date Created	: 05/07/2005
      Description	: Convierte una expresion dad en MegaBytes a Bytes
      Parameters	: MB: integer
      Return Value	: int64
    -----------------------------------------------------------------------------}
    function MBToBytes(MB : Integer) : Int64;
    begin
        Result := MB * 1024 * 1024;
    end;

resourcestring
    MSG_ERROR_LOADING_GENERAL_PARAMETER = 'Error cargando par�metro general.';
    MSG_NOT_COULD_LOAD_GENERAL_PARAMETER = 'No se pudo obtener el par�metro general %s.';
  	MSG_INVALID_DESTINATION_DIRECTORY = 'El directorio de env�os es inv�lido';
  	MSG_INVALID_IMAGES_PATH = 'El directorio de im�genes de tr�nsitos es inv�lido';
    MSG_INVALID_DISK_SIZE = 'El tama�o de unidad de disco es incorrecto.';

    RS_VALIDACION_DIRECTORIOS = 'Validaci�n Directorios';
Const
    CANT_MAX_INFRACCIONES_POR_ARCHIVO = 'CANT_MAX_INFRACCIONES_POR_ARCHIVO';
var
    DiskSize : Integer;
begin
    try
        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

	    Result := False;

        //Inicializamos captions y variables locales

        //FImagePath          := '';   // SS-377-EBA-20110613
        FImagePathNFI         := '';   // SS-377-EBA-20110613

        FDirectorioDestino  := '';
        FstrErrores         := TStringList.Create;
        DiskSize := 0;

        {//Obtenemos los par�metros generales                                                                                                               // SS-377-EBA-20110613
        if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS, FImagePath) then begin                                                // SS-377-EBA-20110613
            MsgBoxErr(MSG_ERROR_LOADING_GENERAL_PARAMETER, format(MSG_NOT_COULD_LOAD_GENERAL_PARAMETER, [DIR_IMAGENES_TRANSITOS]), Caption, MB_ICONERROR);  // SS-377-EBA-20110613
        //     exit;                                                                                                                                         // SS-377-EBA-20110613
        end;  }                                                                                                                                             // SS-377-EBA-20110613

        //Obtenemos los par�metros generales
        //if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN, FImagePathNFI ) then begin                                             // SS_1135_CQU_20131010  // SS-377-EBA-20110613
        //    MsgBoxErr(MSG_ERROR_LOADING_GENERAL_PARAMETER, format(MSG_NOT_COULD_LOAD_GENERAL_PARAMETER, [DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN]), Caption, MB_ICONERROR);   // SS_1135_CQU_20131010  // SS-377-EBA-20110613
        ////      exit;                                                                                                                                                             // SS_1135_CQU_20131010    // SS-377-EBA-20110613
        //end;                                                                                                                                                                      // SS_1135_CQU_20131010  // SS-377-EBA-20110613

        if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_INFRACTORES, FImagePathNFI) then begin                                                         // SS_1135_CQU_20131030
            MsgBoxErr(MSG_ERROR_LOADING_GENERAL_PARAMETER, format(MSG_NOT_COULD_LOAD_GENERAL_PARAMETER, [DIR_IMAGENES_TRANSITOS_INFRACTORES]), Caption, MB_ICONERROR);              // SS_1135_CQU_20131010
        end;                                                                                                                                                                        // SS_1135_CQU_20131010

        if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DESTINO_XML_INFRACCIONES, FDirectorioDestino) then begin
            MsgBoxErr(MSG_ERROR_LOADING_GENERAL_PARAMETER, format(MSG_NOT_COULD_LOAD_GENERAL_PARAMETER, [DIR_DESTINO_XML_INFRACCIONES]), Caption, MB_ICONERROR);
        //      Exit;
        end;

        if not ObtenerParametroGeneral(DMConnections.BaseCAC, INFRACCIONES_XML_TAMANIO_CD, DiskSize) then begin
            MsgBoxErr(MSG_ERROR_LOADING_GENERAL_PARAMETER, format(MSG_NOT_COULD_LOAD_GENERAL_PARAMETER, [INFRACCIONES_XML_TAMANIO_CD]), Caption, MB_ICONERROR);
        //  	Exit;
        end;

        //Obtengo la cantidad maxima de infracciones por archivo
        if not ObtenerParametroGeneral(DMConnections.BaseCAC, CANT_MAX_INFRACCIONES_POR_ARCHIVO, FCant_Max_Infracciones_Por_Archivo) then begin
            MsgBoxErr(MSG_ERROR_LOADING_GENERAL_PARAMETER, format(MSG_NOT_COULD_LOAD_GENERAL_PARAMETER, [CANT_MAX_INFRACCIONES_POR_ARCHIVO]), Caption, MB_ICONERROR);
        //      Exit;
        end;

        // INICIO : TASK_161_MGO_20170330
        CargarComboConcesionarias(DMConnections.BaseCAC, cbConcesionaria, False);
        if not ObtenerParametroGeneralMulticoncesionHabilitada(DMConnections.BaseCAC) then begin
            cbConcesionaria.Value := ObtenerCodigoConcesionariaNativa(DMConnections.BaseCAC);
            lblConcesionaria.Visible := False;
            cbConcesionaria.Visible := False;
        end;
        FCodigoConcesionaria := cbConcesionaria.Value;
        // FIN : TASK_161_MGO_20170330

        {//Validamos si los directorios existen                                                             // SS-377-EBA-20110613
          if not DirectoryExists(FImagePath) then                                                           // SS-377-EBA-20110613
            ShowMsgBoxCN(RS_VALIDACION_DIRECTORIOS, MSG_INVALID_IMAGES_PATH, MB_ICONWARNING, Self); }       // SS-377-EBA-20110613

        //Validamos si los directorios existen                                                              // SS-377-EBA-20110613
        {
          if not DirectoryExists(FImagePathNFI) then                                                           // SS-377-EBA-20110613
            ShowMsgBoxCN(RS_VALIDACION_DIRECTORIOS, MSG_INVALID_IMAGES_PATH, MB_ICONWARNING, Self);         // SS-377-EBA-20110613
        }
        //Verifica si el directorio de destino es valido
        if not DirectoryExists(FDirectorioDestino) then
            ShowMsgBoxCN(RS_VALIDACION_DIRECTORIOS, MSG_INVALID_DESTINATION_DIRECTORY, MB_ICONWARNING, Self);

        //Validamos el tama�o de la unidad de grabaci�n...
        if DiskSize = 0 then begin
            MsgBox(MSG_INVALID_DISK_SIZE, Caption, MB_ICONERROR);
        //		Exit;
        end;

        //Nos aseguramos que el Directorio destino sea correcto y lo mostramos
        FDirectorioDestino := GoodDir(FDirectorioDestino);
        peDirectorioXML.Text := FDirectorioDestino;

        //peDirectorioImagenes.Text := FImagePath;     // SS-377-EBA-20110613
        peDirectorioImagenes.Text := FImagePathNFI;   // SS-377-EBA-20110613


        //Mostramos el tama�o de la unidad
        FDiskSize := MBtoBytes(DiskSize);
        //Rev.1
        //CargarComboConcesionarias(DMConnections.BaseCAC, cbConcesionaria, False);     // TASK_161_MGO_20170330
        //FCodigoConcesionaria := cbConcesionaria.Value;                                // TASK_161_MGO_20170330

        {PAR00133-Fase 2-ALA-20110708
        with fnObtenerNombreCortoConcesionaria do begin
            Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConcesionaria;
            ExecProc;
            FNombreCortoConcesionaria := Trim(Parameters.ParamByName('@RETURN_VALUE').Value);
        end;
        }
        FCodigoNativa := ObtenerCodigoConcesionariaNativa;                      // SS_1386_CQU_20150917

        FSepararRegularizadas := cbProcesarFacturados.Checked;
        FContinuarDisco := chAnexarADisco.Checked;

    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;

        Result := True; //ObtenerDatosInfraccionesAProcesar(NullDate, NullDate, FCodigoConcesionaria, FSepararRegularizadas); //PAR00133-Fase 2-ALA-20110708
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 19/01/2006
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfGeneracionXMLInfracciones.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_INFRACCIONES     = ' ' + CRLF +
                          'El Archivo de Infracciones es' + CRLF +
                          'utilizado por el ESTABLECIMIENTO para informar al MOPTT' + CRLF +
                          'las infracciones que se produjeron durante un periodo' + CRLF +
                          'y fueron detectadas por el ESTABLECIMIENTO' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: INGRESO_99999999_YYYYMMDD.XML' + CRLF +
                          ' ';
begin

    //si esta procesando sale
    if BtnProcesar.Enabled = False then Exit;  
    //Muestro el mensaje
    MsgBoxBalloon(MSG_INFRACCIONES, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 19/01/2006
  Description:
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfGeneracionXMLInfracciones.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if Btnprocesar.Enabled = False then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: deFechaHastaChange
  Author:    ndonadio
  Date Created: 05/07/2005
  Description:  Habilito el proceso cuando hay fechas valida
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfGeneracionXMLInfracciones.deFechaHastaChange(Sender: TObject);
begin
    InicializarDatosInfraccionesAProcesar(False);
end;

{-----------------------------------------------------------------------------
  Function Name: peDirectorioImagenesChange
  Author:    lgisuk
  Date Created: 17/01/2005
  Description: Obtiene el directorio de imagenes
  Parameters:
  Return Value:

 Revision	:	4
 Author		: 	ebaeza
 Date		:	14/06/2011
 firma      :   SS-377-EBA-20110613
 Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos
-----------------------------------------------------------------------------}
procedure TfGeneracionXMLInfracciones.peDirectorioImagenesChange(Sender: TObject);
begin
    //FImagePath := trim(peDirectorioImagenes.Text);     // SS-377-EBA-20110613
    FImagePathNFI := trim(peDirectorioImagenes.Text);     // SS-377-EBA-20110613

end;

{-----------------------------------------------------------------------------
  Function Name: peDirectorioImagenesChange
  Author:    lgisuk
  Date Created: 18/01/2005
  Description: Permite buscar el directorio de imagenes
  Parameters:
  Return Value:

 Revision	:	4
 Author		: 	ebaeza
 Date		:	14/06/2011
 firma      :   SS-377-EBA-20110613
 Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos
-----------------------------------------------------------------------------}
procedure TfGeneracionXMLInfracciones.peDirectorioImagenesButtonClick(Sender: TObject);
Const
    STR_SELECT_LOCATION = 'Seleccione una ubicaci�n para el archivo';
var
    Location : String;
begin
    Location := Trim(BrowseForFolder(STR_SELECT_LOCATION));
    if Location = '' then Exit;
    peDirectorioImagenes.Text := Location;

    //FImagePath := Location;      // SS-377-EBA-20110613
    FImagePathNFI := Location;       // SS-377-EBA-20110613
end;

{-----------------------------------------------------------------------------
  Function Name: peDirectorioXMLChange
  Author:    lgisuk
  Date Created: 18/01/2005
  Description: Obtiene el directorio de Destino
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TfGeneracionXMLInfracciones.peDirectorioXMLChange(Sender: TObject);
begin
    FDirectorioDestino := trim(peDirectorioXML.Text);
end;


{-----------------------------------------------------------------------------
  Function Name: peDirectorioXMLButtonClick
  Author:    lgisuk
  Date Created: 18/01/2005
  Description: Permite buscar el directorio de Destino
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TfGeneracionXMLInfracciones.peDirectorioXMLButtonClick(Sender: TObject);
Const
    STR_SELECT_LOCATION = 'Seleccione una ubicaci�n para el archivo';
var
    Location : String;
begin
    Location := Trim(BrowseForFolder(STR_SELECT_LOCATION));
    if Location = '' then Exit;
    peDirectorioXML.Text := Location;
    FDirectorioDestino  := Location;
end;

{
    Revision: 3
    Auhtor  : pdominguez
    Date    : 23/06/2010
    Description: Infractores Fase 2
        - Se captura el c�digo de la concesionaria, su Nombre Corto, as� como
        se obtienen los nuevos datos para la concesionaria seleccionada.
}
procedure TfGeneracionXMLInfracciones.cbConcesionariaChange(Sender: TObject);
begin

    FCodigoConcesionaria := cbConcesionaria.Value;
    {PAR00133-Fase 2-ALA-20110708
    with fnObtenerNombreCortoConcesionaria do begin
        Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConcesionaria;
        ExecProc;
        FNombreCortoConcesionaria := Parameters.ParamByName('@RETURN_VALUE').Value;
    end;
    }
    InicializarDatosInfraccionesAProcesar(True);
    //ObtenerDatosInfraccionesAProcesar(NullDate, NullDate, FCodigoConcesionaria, FSepararRegularizadas);                       //PAR00133-Fase 2-ALA-20110708
end;

procedure TfGeneracionXMLInfracciones.cbProcesarFacturadosClick(Sender: TObject);
begin
    FSepararRegularizadas := cbProcesarFacturados.Checked;
    //ObtenerDatosInfraccionesAProcesar(deFechaDesde.Date, deFechaHasta.Date, cbConcesionaria.Value, FSepararRegularizadas);     //PAR00133-Fase 2-ALA-20110708
end;

procedure TfGeneracionXMLInfracciones.chAnexarADiscoClick(Sender: TObject);
begin
    FContinuarDisco := chAnexarADisco.Checked;
end;

{
    Revision: 3
    Auhtor  : pdominguez
    Date    : 23/06/2010
    Description: Infractores Fase 2
        - Se cambia la llamada a la generaci�n del reporte de finalizaci�n.
}
function TfGeneracionXMLInfracciones.MostrarReporteFinalizacion(var DescriError: AnsiString): boolean;
var
    f: TfrmRptEnvioXMLInfracciones;
begin
        Result := False;
        DescriError := '';
        try
            Application.CreateForm(TfrmRptEnvioXMLInfracciones, f);
            Result := f.Inicializar(FCodigoOperacion, 0, Trim(cbConcesionaria.Text), DescriError);
        except
            on e: Exception do DescriError := e.Message;
        end;
end;

{
    Revision: 3
    Auhtor  : pdominguez
    Date    : 23/06/2010
    Description: Infractores Fase 2
        - Se validan los fechas de filtrado y se obtienen los nuevos datos en funci�n
        de las mismas si estas son correctas.
}
procedure TfGeneracionXMLInfracciones.btnActualizarDatosProcesoClick(Sender: TObject);
    resourcestring
        rsTituloValidaciones = 'Validaci�n de Fechas';
        rsErrorControl_deFechaDesde = 'La Fecha Desde NO puede ser mayor que la Fecha Hasta.';
        rsErrorControl_deFechaHasta = 'La Fecha Hasta NO puede ser mayor que la Fecha Actual.';

    function ValidarCondiciones: boolean;
        var
            vControles  : Array [1..2] of TControl;
            vCondiciones: Array [1..2] of Boolean;
            vMensajes   : Array [1..2] of String;
    begin
        vControles[1]   := deFechaDesde;
        vControles[2]   := deFechaHasta;

        vCondiciones[1] := (deFechaDesde.Date = NullDate) or ((deFechaHasta.Date = NullDate) or (deFechaDesde.Date <= deFechaHasta.Date));
        vCondiciones[2] := (deFechaHasta.Date = NullDate) or (deFechaHasta.Date <= NowBase(DMConnections.BaseCAC));

        vMensajes[1]    := rsErrorControl_deFechaDesde;
        vMensajes[2]    := rsErrorControl_deFechaHasta;

        Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
    end;
begin
    if ValidarCondiciones then
        ObtenerDatosInfraccionesAProcesar(deFechaDesde.Date, deFechaHasta.Date, cbConcesionaria.Value, FSepararRegularizadas);
end;

{
    Revision: 3
    Auhtor  : pdominguez
    Date    : 23/06/2010
    Description: Infractores Fase 2
        - Se implementa el nuvo sistema de mensajes para preguntar la
        confirmaci�n de la ejecuci�n del proceso.
}
procedure TfGeneracionXMLInfracciones.btnProcesarClick(Sender: TObject);
    resourcestring
        rsParametrosSeleccionados =
            'Par�metros Seleccionados:' + CRLF + CRLF +
            'Fecha Desde: %s' + CRLF +
            'Fecha Hasta: %s' + CRLF +
            'Directorio de Imagenes: %s' + CRLF +
            'Directorio Destino Archivos XML: %s' + CRLF +
            'Continuar Disco Anterior: %s' + CRLF +
            'Generar Disco Infracciones Regularizadas: %s' + CRLF + CRLF;
        rsMensajeConfirmacion = '� Est� seguro de lanzar el proceso con los par�metros especificados ?';
        rsTitulo = 'Generaci�n Disco XML Infractores';

begin
    //Obtenemos el nombre corto de la concesionaria utilizado para generar disco XML.
    with fnObtenerNombreCortoConcesionaria do begin
        Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConcesionaria;
        ExecProc;
        FNombreCortoConcesionaria := Trim(Parameters.ParamByName('@RETURN_VALUE').Value);
    end;

    if ShowMsgBoxCN(
                        rsTitulo,
                        Format(rsParametrosSeleccionados,
                                [FormatDateTime('dd/mm/yyyy',deFechaDesde.Date),
                                 FormatDateTime('dd/mm/yyyy',deFechaHasta.Date),
                                 peDirectorioImagenes.Text,
                                 peDirectorioXML.Text,
                                 iif(FContinuarDisco, 'S�', 'No'),
                                 iif(FSepararRegularizadas, 'S�', 'No')])
                            + rsMensajeConfirmacion ,
                        MB_ICONQUESTION,
                        Self) = mrOk then
        GenerarDiscoXML(FCodigoConcesionaria);
end;

procedure TfGeneracionXMLInfracciones.btnSalirClick(Sender: TObject);
begin
    if FProcesando then FCancelar := True else Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 17/01/2006
  Description: impido salir si esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfGeneracionXMLInfracciones.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
     //Permite salir solo si no esta procesando
     if FProcesando then CanClose := False;
end;

procedure TfGeneracionXMLInfracciones.FormClose(Sender: TObject;var Action: TCloseAction);
begin
     Action := caFree;
end;

{
    Revision: 3
    Auhtor  : pdominguez
    Date    : 23/06/2010
    Description: Infractores Fase 2
        - Visualiza los nuevos datos obtenidos en base a los par�metros pasados.
}
function TfGeneracionXMLInfracciones.ObtenerDatosInfraccionesAProcesar(FechaDesde, FechaHasta: TDateTime; CodigoConcesionaria: Integer; SepararRegularizadas: Boolean): Boolean;
begin
    try
        Result := False;
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        try
            with spObtenerDatosInfraccionesAEnviarMOP do begin
                if Active then Close;
                Parameters.ParamByName('@FechaDesde').Value := iif(FechaDesde = NullDate, NULL, FechaDesde);
                Parameters.ParamByName('@FechaHasta').Value := iif(FechaHasta = NullDate, NULL, FechaHasta);
                Parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;
                Parameters.ParamByName('@SepararRegularizadas').Value := SepararRegularizadas;
                Open;

                deFechaDesde.Date := FieldByName('FechaDesde').AsDateTime;
                deFechaHasta.Date := FieldByName('FechaHasta').AsDateTime;

                if FieldByName('UltimaFechaProcesada').Value <> Null then
                    lblProcesadoHastaLaFecha.Caption := FormatDateTime('dd/mm/yyyy', FieldByName('UltimaFechaProcesada').AsDateTime)
                else lblProcesadoHastaLaFecha.Caption := 'Fecha no disponible';

                lblUltimoDiscoNoRegularizadas.Caption := FloatToStrF(FieldByName('UltimoDiscoInfraccionesSinRegularizar').AsFloat, ffNumber, 15, 0);
                lblUltimoDiscoRegularizadas.Caption := FloatToStrF(FieldByName('UltimoDiscoInfraccionesRegularizadas').AsFloat, ffNumber, 15, 0);

                FUltimoDiscoSinRegularizar := FieldByName('UltimoDiscoInfraccionesSinRegularizar').AsInteger;
                FUltimoDiscoRegularizadas  := FieldByName('UltimoDiscoInfraccionesRegularizadas').AsInteger;
                FUltimoDiscoMorosos        := FieldByName('UltimoDiscoInfraccionesMorosas').AsInteger;                                              // SS_660_CQU_20121010
                //chAnexarADisco.Enabled := not ((FUltimoDiscoSinRegularizar = 0) and (FUltimoDiscoRegularizadas = 0));                             // SS_660_CQU_20121010
                chAnexarADisco.Enabled := not ((FUltimoDiscoSinRegularizar = 0) and (FUltimoDiscoRegularizadas = 0) and (FUltimoDiscoMorosos = 0)); // SS_660_CQU_20121010

                lblFechaMinimaInfraccion.Caption := FieldByName('FechaMinimaInfraccion').AsString;
                if FieldByName('FechaMinimaInfraccion').AsDateTime < FieldByName('FechaDesde').AsDateTime then begin
                    lblFechaMinimaInfraccion.Font.Style := lblFechaMinimaInfraccion.Font.Style + [fsBold];
                    lblFechaMinimaInfraccion.Font.Color := clRed;
                end
                else begin
                    lblFechaMinimaInfraccion.Font.Style := lblFechaMinimaInfraccion.Font.Style - [fsBold];
                    lblFechaMinimaInfraccion.Font.Color := clBlack;
                end;

                lblTotalInfraccionesAEnviar.Caption := FloatToStrF(FieldByName('TotalInfraccionesaEnviar').AsFloat, ffNumber, 15, 0);
                lblInfraccionesSinRegularizar.Caption := FloatToStrF(FieldByName('InfraccionesSinRegularizar').AsFloat, ffNumber, 15, 0);
                lblInfraccionesRegularizadas.Caption := FloatToStrF(FieldByName('InfraccionesRegularizadas').AsFloat, ffNumber, 15, 0);
                lblInfraccionesMorosasTotal.Caption :=  FloatToStrF(FieldByName('TotalInfraccionesMorosas').AsFloat, ffNumber, 15, 0);              // SS_660_CQU_20121010
            end;

            Result := True;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        btnProcesar.Enabled := Result and (spObtenerDatosInfraccionesAEnviarMOP.FieldByName('TotalInfraccionesaEnviar').AsInteger > 0);
        //btnActualizarDatosProceso.Enabled := Not Result;                      //PAR00133-Fase 2-ALA-20110708
        
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

{
    Revision: 3
    Auhtor  : pdominguez
    Date    : 23/06/2010
    Description: Infractores Fase 2
        - Inicializa los visualmente los datos de la pantalla.
}
procedure TfGeneracionXMLInfracciones.InicializarDatosInfraccionesAProcesar(DatosGenerales: Boolean = False);
begin
    if DatosGenerales then begin
        lblProcesadoHastaLaFecha.Caption      := 'Fecha no disponible';
        lblUltimoDiscoNoRegularizadas.Caption := '0';
        lblUltimoDiscoRegularizadas.Caption   := '0';
    end;

    lblFechaMinimaInfraccion.Font.Style := lblFechaMinimaInfraccion.Font.Style - [fsBold];
    lblFechaMinimaInfraccion.Font.Color := clBlack;
    lblFechaMinimaInfraccion.Caption := 'Fecha no disponible';

    lblTotalInfraccionesAEnviar.Caption   := '0';
    lblInfraccionesSinRegularizar.Caption := '0';
    lblInfraccionesRegularizadas.Caption  := '0';
    lblInfraccionesMorosasTotal.Caption   := '0';   // SS_660_CQU_20121010

    btnActualizarDatosProceso.Enabled := True;
    btnProcesar.Enabled := False;
end;

{
    Revision: 3
    Auhtor  : pdominguez
    Date    : 23/06/2010
    Description: Infractores Fase 2
        - Devuelve el nombre de la CarpetaXML a emplear con el Nombre Corto de la Concesionaria y su tipo.
}
function TfGeneracionXMLInfracciones.NombreCarpetaXMLConcesionaria(DirectorioDestino, NombreCortoConcesionaria: String; Regularizadas: Boolean): String;
begin
    case Regularizadas of
        False:
            Result := GoodDir(GoodDir(GoodDir(DirectorioDestino) + NombreCortoConcesionaria) + 'SinRegularizar');
        True:
            Result := GoodDir(GoodDir(GoodDir(DirectorioDestino) + NombreCortoConcesionaria) + 'Regularizadas');
    end;
end;

{
    Revision: 3
    Auhtor  : pdominguez
    Date    : 23/06/2010
    Description: Infractores Fase 2
        - Proceso que genera el Disco XML.

 Revision	:	4
 Author		: 	ebaeza
 Date		:	14/06/2011
 firma      :   SS-377-EBA-20110613
 Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos
}
procedure TfGeneracionXMLInfracciones.GenerarDiscoXML(CodigoConcesionaria: Integer);
    resourcestring
        rsTituloValidaciones = 'Validaci�n Directorios';
        rsTituloGeneracionXML = 'Generaci�n Disco XML';
        rsErrorControl_peDirectorioImagenes = 'No existe o No se tiene acceso al directorio de las Im�genes.';
        rsErrorControl_peDirectorioXML = 'No existe o No se tiene acceso al directorio de las Im�genes.';
        rsErrorGeneracionDiscoXML = 'Se produjo un error durante la generaci�n del Disco XML.' + CRLF + CRLF + 'Error: /s';

    var
        UnitSizeDisco,
        UnitSizeDiscoEnCursoSinRegularizar,
        UnitSizeDiscoEnCursoMorosos,                // SS_660_CQU_20121010
        UnitSizeDiscoEnCursoRegularizadas: Int64;

        DiscoEnCursoSinRegularizar,
        DiscoEnCursoRegularizadas,
        DiscoEnCursoMorosos,                        // SS_660_CQU_20121010
        ArchivoEnCursoSinRegularizar,
        ArchivoEnCursoRegularizadas,
        ArchivoEnCursoMorosos,                      // SS_660_CQU_20121010
        TipoDiscoEnCurso,
        RegistrosRefresco,
        ArchivosGenerados: Integer;

        Error: String;

    {
        Revision: 3
        Auhtor  : pdominguez
        Date    : 23/06/2010
        Description: Infractores Fase 2
            - Muestra un mensaje en el ListBox de Avisos.
    }
    procedure MuestraAviso(Aviso: String);
    begin
        lsbxAvisos.Items.Insert(0, FormatDateTime('dd/mm/yyyy hh:nn:ss - ', NowBase(DMConnections.BaseCAC)) + Aviso);
        lsbxAvisos.ItemIndex := 0;
        Application.ProcessMessages;
    end;

    {
        Revision: 3
        Auhtor  : pdominguez
        Date    : 23/06/2010
        Description: Infractores Fase 2
            - Valida las condiciones previas al proceso.
    }
    function ValidarCondiciones: boolean;
        var
            vControles  : Array [1..1] of TControl;
            vCondiciones: Array [1..1] of Boolean;
            vMensajes   : Array [1..1] of String;
    begin
//        vControles[1]   := peDirectorioImagenes;
        vControles[1]   := peDirectorioXML;

//        vCondiciones[1] := DirectoryExists(peDirectorioImagenes.Text);
        vCondiciones[1] := DirectoryExists(peDirectorioXML.Text);

//        vMensajes[1]    := rsErrorControl_peDirectorioImagenes;
        vMensajes[1]    := rsErrorControl_peDirectorioXML;

        Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
    end;

    {
        Revision: 3
        Auhtor  : pdominguez
        Date    : 23/06/2010
        Description: Infractores Fase 2
            - Inicializa la Interfase.
    }
    function InicializarInterfase: Boolean;
        resourcestring
            rsRegistrarOperacionTitulo = 'Registrar Operaci�n Interfase';
            rsInterfazRegistrada = 'Se registr� la Interfase con el c�digo %d';
        Var
            vError: String;
    begin
        try
            Result := RegistrarOperacionEnLogInterface(
                            DMConnections.BaseCAC,
                            RO_MOD_INTERFAZ_SALIENTE_XML_INFRACCIONES,
                            '',
                            UsuarioSistema,
                            '',
                            False,
                            False,
                            NowBase(DMConnections.BaseCAC),
                            0,
                            FCodigoOperacion,
                            vError);
        Finally
            if Not Result then
                ShowMsgBoxCN(rsRegistrarOperacionTitulo, vError, MB_ICONERROR, Self)
            else MuestraAviso(Format(rsInterfazRegistrada,[FCodigoOperacion]));
        end;
    end;

    {
        Revision: 3
        Auhtor  : pdominguez
        Date    : 23/06/2010
        Description: Infractores Fase 2
            - Inicializa las variables de control de los Discos a Generar.
    }
    function InicializarDiscoArchivos: boolean;
        resourcestring
            rsInicializarDiscosArchivosTitulo = 'Inicializaci�n de Discos';
            rsDiscosInicializados = 'Se Inicializaron los Discos Correctamente.';
            rsDiscoLLeno = 'No se puede continuar el Disco %d de %s. Se inicializa un Disco Nuevo.';
        function ObtenerUltimoArchivoXML(TipoDisco, Disco: Integer): Integer;
        begin
            with fnObtenerUltimoArchivoDiscoXML do begin
                Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConcesionaria;
                Parameters.ParamByName('@CodigoTipo').Value := TipoDisco;
                Parameters.ParamByName('@CodigoDisco').Value := Disco;
                ExecProc;

                Result := Parameters.ParamByName('@RETURN_VALUE').Value;
            end;
        end;
    begin
        Result := False;
        try
            if FUltimoDiscoSinRegularizar = 0 then begin
                UnitSizeDiscoEnCursoSinRegularizar := 0;
                DiscoEnCursoSinRegularizar         := 1;
                ArchivoEnCursoSinRegularizar       := 1;
            end
            else begin
                if FContinuarDisco then begin
                    UnitSizeDisco := VerificarTamanioUnidad(NombreCarpetaXML(NombreCarpetaXMLConcesionaria(FDirectorioDestino,FNombreCortoConcesionaria, False), FUltimoDiscoSinRegularizar, 0));

                    if UnitSizeDisco < FDiskSize then begin
                        UnitSizeDiscoEnCursoSinRegularizar := UnitSizeDisco;
                        DiscoEnCursoSinRegularizar         := FUltimoDiscoSinRegularizar;
                        ArchivoEnCursoSinRegularizar       := ObtenerUltimoArchivoXML(1, DiscoEnCursoSinRegularizar) + 1;
                    end
                    else begin
                        MuestraAviso(Format(rsDiscoLLeno,[FUltimoDiscoSinRegularizar, 'Infracciones Sin Regularizar']));
                        UnitSizeDiscoEnCursoSinRegularizar := 0;
                        DiscoEnCursoSinRegularizar         := FUltimoDiscoSinRegularizar + 1;
                        ArchivoEnCursoSinRegularizar       := 1;
                    end;
                end
                else begin
                    UnitSizeDiscoEnCursoSinRegularizar := 0;
                    DiscoEnCursoSinRegularizar         := FUltimoDiscoSinRegularizar + 1;
                    ArchivoEnCursoSinRegularizar       := 1;
                end;
            end;

            if FSepararRegularizadas then begin
                if FUltimoDiscoRegularizadas = 0 then begin
                    UnitSizeDiscoEnCursoRegularizadas := 0;
                    DiscoEnCursoRegularizadas         := 1;
                    ArchivoEnCursoRegularizadas       := 1;
                end
                else begin
                    if FContinuarDisco then begin
                        UnitSizeDisco := VerificarTamanioUnidad(NombreCarpetaXML(NombreCarpetaXMLConcesionaria(FDirectorioDestino,FNombreCortoConcesionaria, True), FUltimoDiscoRegularizadas, 0));

                        if UnitSizeDisco < FDiskSize then begin
                            UnitSizeDiscoEnCursoRegularizadas := UnitSizeDisco;
                            DiscoEnCursoRegularizadas         := FUltimoDiscoRegularizadas;
                            ArchivoEnCursoRegularizadas       := ObtenerUltimoArchivoXML(2, DiscoEnCursoRegularizadas) + 1;
                        end
                        else begin
                            MuestraAviso(Format(rsDiscoLLeno,[FUltimoDiscoRegularizadas, 'Infracciones Regularizadas']));
                            UnitSizeDiscoEnCursoRegularizadas := 0;
                            DiscoEnCursoRegularizadas         := FUltimoDiscoRegularizadas + 1;
                            ArchivoEnCursoRegularizadas       := 1;
                        end;
                    end
                    else begin
                        UnitSizeDiscoEnCursoRegularizadas := 0;
                        DiscoEnCursoRegularizadas         := FUltimoDiscoRegularizadas + 1;
                        ArchivoEnCursoRegularizadas       := 1;
                    end;
                end;
            end;

            if FUltimoDiscoMorosos = 0 then begin                                                           // SS_660_CQU_20121010
                UnitSizeDiscoEnCursoMorosos := 0;                                                           // SS_660_CQU_20121010
                DiscoEnCursoMorosos         := 1;                                                           // SS_660_CQU_20121010
                ArchivoEnCursoMorosos       := 1;                                                           // SS_660_CQU_20121010
            end                                                                                             // SS_660_CQU_20121010
            else begin                                                                                      // SS_660_CQU_20121010
                if FContinuarDisco then begin                                                               // SS_660_CQU_20121010
                    UnitSizeDisco := VerificarTamanioUnidad(NombreCarpetaXML(NombreCarpetaXMLConcesionariaMorosidad(FDirectorioDestino,FNombreCortoConcesionaria), FUltimoDiscoMorosos, 0));    // SS_660_CQU_20121010

                    if UnitSizeDisco < FDiskSize then begin                                                 // SS_660_CQU_20121010
                        UnitSizeDiscoEnCursoMorosos := UnitSizeDisco;                                       // SS_660_CQU_20121010
                        DiscoEnCursoMorosos         := FUltimoDiscoMorosos;                                 // SS_660_CQU_20121010
                        ArchivoEnCursoMorosos       := ObtenerUltimoArchivoXML(3, DiscoEnCursoMorosos) + 1; // SS_660_CQU_20121010
                    end                                                                                     // SS_660_CQU_20121010
                    else begin                                                                              // SS_660_CQU_20121010
                        MuestraAviso(Format(rsDiscoLLeno,[FUltimoDiscoMorosos, 'Infracciones Morosas']));   // SS_660_CQU_20121010
                        UnitSizeDiscoEnCursoMorosos     := 0;                                               // SS_660_CQU_20121010
                        DiscoEnCursoMorosos             := FUltimoDiscoMorosos + 1;                         // SS_660_CQU_20121010
                        ArchivoEnCursoMorosos           := 1;                                               // SS_660_CQU_20121010
                    end;                                                                                    // SS_660_CQU_20121010
                end                                                                                         // SS_660_CQU_20121010
                else begin                                                                                  // SS_660_CQU_20121010
                    UnitSizeDiscoEnCursoMorosos := 0;                                                       // SS_660_CQU_20121010
                    DiscoEnCursoMorosos         := FUltimoDiscoMorosos + 1;                                 // SS_660_CQU_20121010
                    ArchivoEnCursoMorosos       := 1;                                                       // SS_660_CQU_20121010
                end;                                                                                        // SS_660_CQU_20121010
            end;                                                                                            // SS_660_CQU_20121010

            MuestraAviso(rsDiscosInicializados);
            Result := True;
        except
            on e: Exception do ShowMsgBoxCN(rsInicializarDiscosArchivosTitulo, e.Message, MB_ICONERROR,Self);
        end;
    end;

    {
        Revision: 3
        Auhtor  : pdominguez
        Date    : 23/06/2010
        Description: Infractores Fase 2
            - Carga los datos de la Infracci�n y sus Tr�nsitos a procesar.
    }
    function CargarDatosDiscoXML: Boolean;
        resourcestring
            rsDatosCargados = 'Se Cargaron los Datos a procesar';
    begin
        try
            Result := False;
            with spCargarInfraccionesTransitosAEnviarMOP do begin
                Parameters.ParamByName('@CodigoConcesionaria').Value :=  FCodigoConcesionaria;
                Parameters.ParamByName('@FechaDesde').Value := deFechaDesde.Date;
                Parameters.ParamByName('@FechaHasta').Value := deFechaHasta.Date;
                Parameters.ParamByName('@SepararRegularizadas').Value := FSepararRegularizadas;
                ExecProc;

            end;
            MuestraAviso(rsDatosCargados);
            Result := True;
        except
            on e: Exception do ShowMsgBoxCN(e, Self);
        end;
    end;

    {
        Revision: 3
        Auhtor  : pdominguez
        Date    : 23/06/2010
        Description: Infractores Fase 2
            - Actualiza los datos en la tabla de trabajo
    }
    Function ActualizarDatosXMLTemp(TipoDisco, CodigoInfraccion, DiscoAsignado, ArchivoAsignado: Integer; Error: String): Boolean;
    begin
        try
            Result := False;
            with spActualizarInfraccionesTransitosAEnviarMOP do begin
                Parameters.ParamByName('@CodigoTipo').Value            := TipoDisco;
                Parameters.ParamByName('@CodigoDisco').Value           := 0;
                Parameters.ParamByName('@CodigoArchivo').Value         := 0;
                Parameters.ParamByName('@CodigoInfraccion').Value      := CodigoInfraccion;
                Parameters.ParamByName('@CodigoDiscoAsignado').Value   := iif(DiscoAsignado = 0, NULL, DiscoAsignado);
                Parameters.ParamByName('@CodigoArchivoAsignado').Value := iif(ArchivoAsignado = 0, NULL, ArchivoAsignado);
                Parameters.ParamByName('@Error').Value                 := iif(Trim(Error) = '', NULL, Trim(Error));
                ExecProc;
            end;
            Result := True;
        except
            raise;
        end;
    end;

    {
        Revision: 3
        Auhtor  : pdominguez
        Date    : 23/06/2010
        Description: Infractores Fase 2
            - Procesa y almacena los datos de la Infracci�n, sus Tr�nsitos e Imagen de Referencia
            en la estructura XML.
    }
    function ProcesarInfraccion: Boolean;

        {
            Revision: 3
            Auhtor  : pdominguez
            Date    : 23/06/2010
            Description: Infractores Fase 2
                - Valida la conformidad de los datos de la Infracci�n.
        }
        function DatosInfraccionCompletos: Boolean;
            resourcestring
                rsDatosIncompletos = 'C�digo Infracci�n: %d, Patente: %s. Error: Datos Incompletos.';
        begin
            with spObtenerInfraccionesTransitosAEnviarMOP do begin
                Result :=
                    (trim(FieldByName('Nombre').AsString) <> '') and
                    (trim(FieldByName('NumeroDocumento').AsString) <> '') and
                    (trim(FieldByName('Domicilio').AsString) <> '') and
                    (trim(FieldByName('Comuna').AsString) <> '') and
                    (trim(FieldByName('Patente').AsString) <> '') and
                    (trim(FieldByName('PatenteDV').AsString) <> '') and
                    (trim(FieldByName('TipoVehiculo').AsString) <> '') and
                    (trim(FieldByName('Marca').AsString) <> '');

                if not Result then
                    ActualizarDatosXMLTemp(
                                            TipoDiscoEnCurso,
                                            FieldByName('CodigoInfraccion').AsInteger,
                                            0,
                                            0,
                                            Format(rsDatosIncompletos,
                                                [FieldByName('CodigoInfraccion').AsInteger,
                                                 iif(FieldByName('Patente').IsNull, 'Sin Patente', Trim(FieldByName('Patente').AsString))]));
            end;
        end;

        {
            Revision: 3
            Auhtor  : pdominguez
            Date    : 23/06/2010
            Description: Infractores Fase 2
                - Verifica la validez de la Imagen de Referencia.
        }
        function EsImagenValida: boolean;
            resourcestring
                rsImagenNoValida = 'C�digo Infracci�n: %d, Patente: %s. Error: Imagen de Referencia NO V�lida.';
        begin
            with spObtenerInfraccionesTransitosAEnviarMOP do begin
                Result :=
                    (not FieldByName('RefNumCorrCA').IsNull);

                if not Result then
                    ActualizarDatosXMLTemp(
                                            TipoDiscoEnCurso,
                                            FieldByName('CodigoInfraccion').AsInteger,
                                            0,
                                            0,
                                            Format(rsImagenNoValida,
                                                [FieldByName('CodigoInfraccion').AsInteger,
                                                 iif(FieldByName('Patente').IsNull, 'Sin Patente', Trim(FieldByName('Patente').AsString))]));
            end;
        end;

        {
            Revision: 3
            Auhtor  : pdominguez
            Date    : 23/06/2010
            Description: Infractores Fase 2
                - Almacena los datos en la estructura XML.
                
             Revision	:	4
             Author		: 	ebaeza
             Date		:	14/06/2011
             firma      :   SS-377-EBA-20110613
             Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos
        }
        function AlmacenarInfraccionXML: boolean;
            resourcestring
                rsImagenNoValida = 'C�digo Infracci�n: %d, Patente: %s. No se pudo recuperar la Imagen de Referencia. Error: %s';
            var
                vImagenTransito: TJPEGPlusImage;
                vRefNumCorrCA: Int64;
                vTipoImagen: TTipoImagen;
                vDataImage: TDataImage;
                vTipoErrorImagen: TTipoErrorImg;
                vFechaHoraTrx: TDateTime;
                vStrStream: TStringStream;
                vStrImagenCodificada: AnsiString;
                vError: AnsiString;
                vImagenTransitoItaliano : TBitmap;              // SS_1091_CQU_20130516
                vEsItaliano : Boolean;                          // SS_1091_CQU_20130516
                vContinuar  : Boolean;                          // SS_1091_CQU_20130516
                vImagenJpeg : TJPEGImage;                       // SS_1091_CQU_20130816

        begin
            Result := False;

            try
                try
                    vImagenTransito := TJPEGPlusImage.Create;
                    vRefNumCorrCA   := spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('RefNumCorrCA').AsInteger;
                    vFechaHoraTrx   := spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('RefFechaHoraTransito').AsDateTime;

                    //if (spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('RefRegistrationAccessibility').asInteger = 1) then                                              // SS_1138_CQU_20131009
                    //    vTipoImagen := tiFrontal                                                                                                                              // SS_1138_CQU_20131009
                    //else vTipoImagen := tiFrontal2;                                                                                                                           // SS_1138_CQU_20131009
                    vTipoImagen := ObtenerTipoImagenPorRegistratioAccesibility(spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('RefRegistrationAccessibility').AsInteger); // SS_1138_CQU_20131009

                    vStrStream              := TStringStream.Create('');                                                            // SS_1091_CQU_20130516
                    vImagenTransitoItaliano := TBitmap.Create;                                                                      // SS_1091_CQU_20130516
                    vEsItaliano             := spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('EsItaliano').AsBoolean;        // SS_1091_CQU_20130516
                    // Valido previamente si es italiano y cargo la imagen que corresponda, coloco una variable para saber          // SS_1091_CQU_20130516
                    // si carg� o no la imagen.                                                                                     // SS_1091_CQU_20130516
                    if ObtenerImagenTransitoInfractor(FImagePathNFI, FNombreCortoConcesionaria, vRefNumCorrCA,                      // SS_1135_CQU_20131030
                    //if vEsItaliano and ObtenerImagenTransito(FImagePathNFI, FNombreCortoConcesionaria, vRefNumCorrCA,             // SS_1135_CQU_20131030 // SS_1091_CQU_20130516
                            vFechaHoraTrx, vTipoImagen, vImagenTransitoItaliano, vDataImage, vError, vTipoErrorImagen) then begin   // SS_1091_CQU_20130516
                                                                                                                                    // SS_1091_CQU_20130516
                        //vImagenTransitoItaliano.SaveToStream(vStrStream);                                                         // SS_1091_CQU_20130816 // SS_1091_CQU_20130516
                        vImagenJpeg := TJPEGImage.Create;                                                                           // SS_1091_CQU_20130816
                        vImagenJpeg.Assign(vImagenTransitoItaliano);                                                                // SS_1091_CQU_20130816
                        vImagenJpeg.SaveToStream(vStrStream);                                                                       // SS_1091_CQU_20130816
                        vContinuar := True;                                                                                         // SS_1091_CQU_20130516
                                                                                                                                    // SS_1091_CQU_20130516
                    end else                                                                                                        // SS_1091_CQU_20130516
                    //if ObtenerImagenTransito(FImagePathNFI, FNombreCortoConcesionaria, vRefNumCorrCA, vFechaHoraTrx,              // SS_1135_CQU_20131030
                    if ObtenerImagenTransitoInfractor(FImagePathNFI, FNombreCortoConcesionaria, vRefNumCorrCA, vFechaHoraTrx,       // SS_1135_CQU_20131030 // SS_1091_CQU_20130516
                            vTipoImagen, vImagenTransito, vDataImage, vError, vTipoErrorImagen) then begin                          // SS_1091_CQU_20130516
                                                                                                                                    // SS_1091_CQU_20130516
                        vImagenTransito.SaveToStream(vStrStream);                                                                   // SS_1091_CQU_20130516
                        vContinuar := True;                                                                                         // SS_1091_CQU_20130516
                                                                                                                                    // SS_1091_CQU_20130516
                    end else                                                                                                        // SS_1091_CQU_20130516
                        vContinuar := False;                                                                                        // SS_1091_CQU_20130516

                    if vContinuar then begin                                                                                        // SS_1091_CQU_20130516
                    //if ObtenerImagenTransito(FImagePathNFI, FNombreCortoConcesionaria, vRefNumCorrCA, vFechaHoraTrx, vTipoImagen, vImagenTransito, vDataImage, vError, vTipoErrorImagen) then begin         // SS-377-EBA-20110613    // SS_1091_CQU_20130516
//                    if ObtenerImagenTransito(FImagePath, vRefNumCorrCA, vFechaHoraTrx, vTipoImagen, vImagenTransito, vDataImage, vError, vTipoErrorImagen) then begin                                       // SS-377-EBA-20110613
//                    if ObtenerImagenTransito(FImagePath, 9840341, vFechaHoraTrx, vTipoImagen, vImagenTransito, vDataImage, vError, vTipoErrorImagen) then begin
                        //vStrStream := TStringStream.Create('');                                                                   // SS_1091_CQU_20130516
                        //vImagenTransito.SaveToStream(vStrStream);                                                                 // SS_1091_CQU_20130516
                        vStrImagenCodificada := ImageURLEncode(vStrStream.DataString);                                              // SS_1091_CQU_20130516

                        FXMLDenuncios.Add;

                        with FXMLDenuncios.Infraccion[FXMLDenuncios.Count - 1] do begin
                            Concesionaria := FCodigoConcesionaria;

                            with spObtenerInfraccionesTransitosAEnviarMOP do begin
                                NumInfraccion             := FieldByName('CodigoInfraccion').AsInteger;
                                TipoInfraccion            := FieldByName('CodigoTipoInfraccion').AsInteger; // SS_660_CQU_20121010

                                Infractor.Nombre          := Trim(FieldByName('Nombre').AsString);
                                { INICIO : TASK_161_MGO_20170330
                                if FCodigoNativa <> CODIGO_VS then begin
                                    Infractor.ApellidoPaterno := '';
                                    Infractor.ApellidoMaterno := '';
                                end else begin
                                    Infractor.ApellidoPaterno := Trim(FieldByName('Apellido').AsString);
                                    Infractor.ApellidoMaterno := Trim(FieldByName('ApellidoMaterno').AsString);
                                end;
                                }
                                Infractor.ApellidoPaterno := Trim(FieldByName('Apellido').AsString);
                                Infractor.ApellidoMaterno := Trim(FieldByName('ApellidoMaterno').AsString);
                                // FIN : TASK_161_MGO_20170330
                                Infractor.Rut             := Copy(Trim(FieldByName('NumeroDocumento').AsString), 1, Length(Trim(FieldByName('NumeroDocumento').AsString)) - 1);
                                Infractor.DV              := RightStr(Trim(FieldByName('NumeroDocumento').AsString), 1);
                                Infractor.Domicilio       := Trim(FieldByName('Domicilio').AsString);
                                Infractor.Comuna          := Trim(FieldByName('Comuna').AsString);

                                Vehiculo.Patente      := Trim(FieldByName('Patente').AsString);
                                Vehiculo.DV           := Trim(FieldByName('PatenteDV').AsString);
                                Vehiculo.TipoVehiculo := Trim(FieldByName('TipoVehiculo').AsString);
                                Vehiculo.Marca        := Trim(FieldByName('Marca').AsString);
                                Vehiculo.Modelo       := Trim(FieldByName('Modelo').AsString);
                                Vehiculo.Agno         := FieldByName('Anio').AsInteger;
                                Vehiculo.Color        := Trim(FieldByName('Color').AsString);

                                Fotografia.Imagen := vStrImagenCodificada;                    // SS_1091_CQU_20130516

                                while not Eof do begin
                                    Transaccion.Add;
                                    with Transaccion[Transaccion.Count - 1] do begin
                                        Ident_PC            := FieldByName('IdentPC').AsString;
                                        Num_Corr_Punto      := FieldByName('NumCorrPunto').AsInteger;
                                        Time_PC             := FieldByName('TimePC').AsInteger;
                                        Fecha_transaccion   := FormatDateTime('dd"/"mm"/"yyyy', FieldByName('FechaHoraTransito').AsDateTime);
                                        Hora_local          := FormatDateTime('hh:nn:ss', FieldByName('FechaHoraTransito').AsDateTime);
                                        //Num_Corr_CO         := FieldByName('NumCorrCO_CA').AsInteger; // SS_1138_CQU_20131009
                                        Num_Corr_CO         := FieldByName('NumCorrCO_CA').Value;       // SS_1138_CQU_20131009
                                        //Num_Corr_CA         := FieldByName('NumCorrCA').AsInteger;    // SS_1138_CQU_20131009
                                        Num_Corr_CA         := FieldByName('NumCorrCA').Value;          // SS_1138_CQU_20131009
                                        ConsolidacionManual := FieldByName('ConsolidacionManual').AsString;
                                    end;

                                    Next;
                                end;
                            end;
                        end;

                        Result := True;
                    end;
                except
                    on e: Exception do vError := e.Message;
                end;
            finally
                if Assigned(vStrStream) then FreeAndNil(vStrStream);
                if Assigned(vImagenTransito) then FreeAndNil(vImagenTransito);
                if Assigned(vImagenTransitoItaliano) then FreeAndNil(vImagenTransitoItaliano);  // SS_1091_CQU_20130516
                if Assigned(vImagenJpeg) then FreeAndNil(vImagenJpeg);                          // SS_1091_CQU_20130816

                if not Result then
                    ActualizarDatosXMLTemp(
                                            TipoDiscoEnCurso,
                                            spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('CodigoInfraccion').AsInteger,
                                            0,
                                            0,
                                            Format(rsImagenNoValida,
                                                [spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('CodigoInfraccion').AsInteger,
                                                 iif(spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('Patente').IsNull, 'Sin Patente', Trim(spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('Patente').AsString)),
                                                 vError]));
            end;
        end;

    begin
        try
            try
                Result := False;

                with spObtenerInfraccionesTransitosAEnviarMOP, spObtenerInfraccionesTransitosAEnviarMOP.Parameters do begin
                    if Active then Close;

                    ParamByName('@CodigoTipo').Value       := spObtenerInfraccionesAEnviarMOP.FieldByName('CodigoTipo').AsInteger;
                    ParamByName('@CodigoDisco').Value      := spObtenerInfraccionesAEnviarMOP.FieldByName('CodigoDisco').AsInteger;
                    ParamByName('@CodigoArchivo').Value    := spObtenerInfraccionesAEnviarMOP.FieldByName('CodigoArchivo').AsInteger;
                    ParamByName('@CodigoInfraccion').Value := spObtenerInfraccionesAEnviarMOP.FieldByName('CodigoInfraccion').AsInteger;
                    Open;
                end;

                if DatosInfraccionCompletos and EsImagenValida then Result := AlmacenarInfraccionXML;
            except
                raise;
            end;
        finally
        end;
    end;

    {
        Revision: 3
        Auhtor  : pdominguez
        Date    : 23/06/2010
        Description: Infractores Fase 2
            - Guarda los datos de la estructura XML al disco.
    }
    function GuardarArchivoXML: boolean;
        resourcestring
            rsErrorGuardarArchivoXML = 'Se produjo un Error';
        var
            vCarpetaDestino,
            vNombreArchivoTemp,
            vNombreArchivo,
            vMensajeTipoDisco: String;  // SS_660_CQU_20121010
            vDiscoEnCurso,
            vArchivoEnCurso,
            vCantidadInfracciones: Integer;
            vUnitSizeDiscoEnCurso: Int64;
    begin
        try
            Result := False;

            try
                case TipoDiscoEnCurso of
                    1: begin
                        vCarpetaDestino       := NombreCarpetaXMLConcesionaria(FDirectorioDestino, FNombreCortoConcesionaria, False);
                        vDiscoEnCurso         := DiscoEnCursoSinRegularizar;
                        vArchivoEnCurso       := ArchivoEnCursoSinRegularizar;
                        vUnitSizeDiscoEnCurso := UnitSizeDiscoEnCursoSinRegularizar;
                        vMensajeTipoDisco     := '"Infracciones Sin Regularizar"';                                                      // SS_660_CQU_20121010
                    end;
                    2: begin
                        vCarpetaDestino       := NombreCarpetaXMLConcesionaria(FDirectorioDestino, FNombreCortoConcesionaria, True);
                        vDiscoEnCurso         := DiscoEnCursoRegularizadas;
                        vArchivoEnCurso       := ArchivoEnCursoRegularizadas;
                        vUnitSizeDiscoEnCurso := UnitSizeDiscoEnCursoRegularizadas;
                        vMensajeTipoDisco     := '"Infracciones Regularizadas"';                                                        // SS_660_CQU_20121010
                    end;
                    3: begin                                                                                                            // SS_660_CQU_20121010
                        vCarpetaDestino       := NombreCarpetaXMLConcesionariaMorosidad(FDirectorioDestino, FNombreCortoConcesionaria); // SS_660_CQU_20121010
                        vDiscoEnCurso         := DiscoEnCursoMorosos;                                                                   // SS_660_CQU_20121010
                        vArchivoEnCurso       := ArchivoEnCursoMorosos;                                                                 // SS_660_CQU_20121010
                        vUnitSizeDiscoEnCurso := UnitSizeDiscoEnCursoMorosos;                                                           // SS_660_CQU_20121010
                        vMensajeTipoDisco     := '"Infracciones Por Morosidad"';                                                        // SS_660_CQU_20121010
                    end;                                                                                                                // SS_660_CQU_20121010
                end;

                if not DirectoryExists(vCarpetaDestino) then
                    if not ForceDirectories(vCarpetaDestino) then raise Exception.Create('No se pudo crear la Carpeta Temporal para el Disco XML.');

                vNombreArchivoTemp := GoodDir(vCarpetaDestino) + TempFile;

                if Guardar(FXMLDenuncios, vNombreArchivoTemp) then begin
                    UnitSizeDisco := Util.GetFileSize(vNombreArchivoTemp);

                    if (vUnitSizeDiscoEnCurso + UnitSizeDisco) > FDiskSize then begin
                        vDiscoEnCurso         := vDiscoEnCurso + 1;
                        vArchivoEnCurso       := 1;
                        vUnitSizeDiscoEnCurso := UnitSizeDisco;
                    end
                    else vUnitSizeDiscoEnCurso := vUnitSizeDiscoEnCurso + UnitSizeDisco;

                    vCarpetaDestino := NombreCarpetaXML(GoodDir(vCarpetaDestino), vDiscoEnCurso, 0);

                    if not DirectoryExists(vCarpetaDestino) then
                        if Not ForceDirectories(vCarpetaDestino) then
                            raise Exception.Create('No se pudo crear la Carpeta Destino para el Disco XML.')
                        //else MuestraAviso(Format('Se cre� un Disco Nuevo de %s. Disco Nro. %d',[iif(TipoDiscoEnCurso = 1, '"Infracciones Sin Regularizar"', '"Infracciones Regularizadas"'), vDiscoEnCurso]));   // SS_660_CQU_20121010
                        else MuestraAviso(Format('Se cre� un Disco Nuevo de %s. Disco Nro. %d',[vMensajeTipoDisco, vDiscoEnCurso]));                                                                               // SS_660_CQU_20121010


                    if not FileExists(GoodDir(vCarpetaDestino) + 'Denuncios.DTD') then
                        GenerarDTD(vCarpetaDestino);


                    vNombreArchivo  := NombreArchivoXML(vCarpetaDestino, vDiscoEnCurso, vArchivoEnCurso, NowBase(DMConnections.BaseCAC), FCodigoConcesionaria);

                    if FileExists(vNombreArchivo) then DeleteFile(vNombreArchivo);

                    if RenameFile(vNombreArchivoTemp, vNombreArchivo) then begin
                        for vCantidadInfracciones := 0 to FXMLDenuncios.Count - 1 do
                            ActualizarDatosXMLTemp(TipoDiscoEnCurso, FXMLDenuncios[vCantidadInfracciones].NumInfraccion, vDiscoEnCurso, vArchivoEnCurso, '');

                        FXMLDenuncios.Clear;

                        vArchivoEnCurso := vArchivoEnCurso + 1;

                        if TipoDiscoEnCurso <> spObtenerInfraccionesAEnviarMOP.FieldByName('CodigoTipo').AsInteger then
                            TipoDiscoEnCurso := spObtenerInfraccionesAEnviarMOP.FieldByName('CodigoTipo').AsInteger
                        else case TipoDiscoEnCurso of
                            1: begin
                                DiscoEnCursoSinRegularizar         := vDiscoEnCurso;
                                ArchivoEnCursoSinRegularizar       := vArchivoEnCurso;
                                UnitSizeDiscoEnCursoSinRegularizar := vUnitSizeDiscoEnCurso;
                            end;
                            2: begin
                                DiscoEnCursoRegularizadas         := vDiscoEnCurso;
                                ArchivoEnCursoRegularizadas       := vArchivoEnCurso;
                                UnitSizeDiscoEnCursoRegularizadas := vUnitSizeDiscoEnCurso;
                            end;
                            3: begin                                                        // SS_660_CQU_20121010
                                DiscoEnCursoMorosos               := vDiscoEnCurso;         // SS_660_CQU_20121010
                                ArchivoEnCursoMorosos             := vArchivoEnCurso;       // SS_660_CQU_20121010
                                UnitSizeDiscoEnCursoMorosos       := vUnitSizeDiscoEnCurso; // SS_660_CQU_20121010
                            end;                                                            // SS_660_CQU_20121010
                        end;

                        Inc(ArchivosGenerados);
                        lblArchivosGenerados.Caption := IntToStr(ArchivosGenerados);
                        Application.ProcessMessages;

                        Result := True;
                    end;

                    if FileExists(vNombreArchivoTemp) then DeleteFile(vNombreArchivoTemp);
                end;
            except
                raise;
            end;
        finally

        end;
    end;

    procedure ActualizarControles(Habilitar: Boolean);
    begin
        btnProcesar.Enabled := Habilitar;
        //GroupBox2.Enabled := Habilitar;           // TASK_161_MGO_20170330
        cbConcesionaria.Enabled := Habilitar;
        GroupBox4.Enabled := Habilitar;
        deFechaDesde.Enabled := Habilitar;
        deFechaHasta.Enabled := Habilitar;
        peDirectorioImagenes.Enabled := Habilitar;
        peDirectorioXML.Enabled := Habilitar;
        chAnexarADisco.Enabled := Habilitar;
        cbProcesarFacturados.Enabled := Habilitar;
    end;

begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        FProcesando := False;
        ActualizarControles(False);

        if ValidarCondiciones and CargarDatosDiscoXML and InicializarInterfase and InicializarDiscoArchivos then begin
            FXMLDenuncios := NewDenuncios;

            try
                FProcesando := True;

                with spObtenerInfraccionesAEnviarMOP do begin
                    if Active then Close;
                    Open;

                    TipoDiscoEnCurso := FieldByName('CodigoTipo').AsInteger;

                    pbProgreso.Max := RecordCount;
                    pbProgreso.Min := 1;

                    RegistrosRefresco  := RecordCount div 100;
                    if RegistrosRefresco = 0 then RegistrosRefresco := 1;

                    gbEstado.Caption := ' Estado del Proceso: En Curso ... ';
                    lblInfraccionesRestantes.Visible := True;
                    lblInfraccionesRestantes.Caption := Format('Infracciones Restantes por Procesar: %d', [RecordCount]);
                    MuestraAviso('Procesando Infracciones ...');

                    ArchivosGenerados := 0;

                    while not Eof do begin

                    if RecNo >= RegistrosRefresco then
                        if RecNo mod RegistrosRefresco = 0 then begin
                            pbProgreso.Position := RecNo;
                            lblInfraccionesRestantes.Caption := Format('Infracciones Restantes por Procesar: %d', [RecordCount - RecNo]);
                            Application.ProcessMessages;
                        end;

                        if (TipoDiscoEnCurso <> FieldByName('CodigoTipo').AsInteger) or (FXMLDenuncios.Count = FCant_Max_Infracciones_Por_Archivo) then
                            if FXMLDenuncios.Count > 0 then
                                GuardarArchivoXML
                            else begin
                                if TipoDiscoEnCurso <> FieldByName('CodigoTipo').AsInteger then
                                    TipoDiscoEnCurso := FieldByName('CodigoTipo').AsInteger;
                            end;

                        ProcesarInfraccion;
                        spObtenerInfraccionesAEnviarMOP.Next;
                    end;

                    if FXMLDenuncios.Count > 0 then GuardarArchivoXML;

                    pbProgreso.Position := RecordCount;
                    lblInfraccionesRestantes.Caption := 'Infracciones Restantes por Procesar: 0';
                end;

                MuestraAviso('Consolidando Datos del Proceso ...');

                with spConsolidarInfraccionesTransitosAEnviarMOP do begin
                    Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConcesionaria;
                    Parameters.ParamByName('@FechaDesde').Value := deFechaDesde.Date;
                    Parameters.ParamByName('@FechaHasta').Value := deFechaHasta.Date;
                    Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                        raise Exception.Create(Parameters.ParamByName('@DescError').Value);
                    
                end;

                MuestraAviso('Cerrando Interfase ...');

                Error := '';
                if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOperacion,'El proceso finaliz� Correctamente.',Error) then
                    raise Exception.Create(Error);

                MuestraAviso('Imprimiendo Reporte de Interfase ...');

                if not MostrarReporteFinalizacion(Error) then
                    raise Exception.Create(Error);

            except
                on e: Exception do ShowMsgBoxCN(rsTituloGeneracionXML, e.Message, MB_ICONERROR, Self);
            end;
        end;
    finally
        if FProcesando then begin
            FProcesando := False;
            gbEstado.Caption := ' Estado del Proceso: Finalizado ';
            MuestraAviso('Proceso Finalizado.');
        end
        else ActualizarControles(True);

        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

{
    Auhtor      : CQuezada
    Date        : 16/10/2012
    Firma       : SS_660_CQU_20121010
    Description : Devuelve el nombre de la CarpetaXML a emplear con el Nombre Corto de la Concesionaria
                  y su tipo para las infracciones Por Morosidad.
}
function TfGeneracionXMLInfracciones.NombreCarpetaXMLConcesionariaMorosidad(DirectorioDestino, NombreCortoConcesionaria: String): String;   // SS_660_CQU_20121010
begin                                                                                                                                       // SS_660_CQU_20121010
    Result := GoodDir(GoodDir(GoodDir(DirectorioDestino) + NombreCortoConcesionaria) + 'PorMorosidad');                                     // SS_660_CQU_20121010
end;                                                                                                                                        // SS_660_CQU_20121010


end.
