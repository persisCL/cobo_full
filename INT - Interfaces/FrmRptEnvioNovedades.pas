{-------------------------------------------------------------------------------
 File Name: FrmRptEnvioNoveadades.pas
 Author:    Lgisuk
 Date Created: 23/06/2005
 Language: ES-AR
 Description: M�dulo de la interface Falabella - Reporte de Envio de Novedades

 Revision : 1
     Author : pdominguez
     Date   : 24/07/2009
     Description : SS 812
        - Se a�adi� el par�metro @SumatoriaCuentas al SP SpObtenerReporteEnvioNovedadesCMR.
        - Se modificaron los siguientes procedimientos / funciones:
            Inicializar
            RBIListadoExecute



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
Unit FrmRptEnvioNovedades;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  Utildb,                    //querygetvalue
  UtilProc,                  //Mensajes
  //general
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup, ConstParametrosGenerales;                                            //SS_1147_NDR_20140710

type
  TFRptEnvioNovedades = class(TForm)
    rptEnvioNovedades: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppMaxFechaEmision: TppLabel;
    ppLabel1: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    ppParameterList1: TppParameterList;
	RBIListado: TRBInterface;
    SpObtenerReporteEnvioNovedadesCMR: TADOStoredProc;
    ppLine5: TppLine;
    Ltusuario: TppLabel;
    LtFechaHora: TppLabel;
    ppusuario: TppLabel;
    ppfechahora: TppLabel;
    ppNumeroProceso: TppLabel;
    ppIdentificacionArchivoGenerado: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppCantidadTotalRegistrosEnviados: TppLabel;
    ppCantidadAltasEnviadas: TppLabel;
    ppCantidadBajasEnviadas: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppCantidadDeRegistros: TppLabel;
    ppSumatoriadeCuentas: TppLabel;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FCodigoOperacionInterfase:Integer;
    FNombreReporte:string;
//    FCantidadRegistros:string;
//    FSumatoriaCuentas:string;
    { Private declarations }
  public
    Function Inicializar(CodigoOperacionInterfase:Integer; NombreReporte: AnsiString): Boolean;
    { Public declarations }
  end;

var
  FRptEnvioNovedades: TFRptEnvioNovedades;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 23/06/2005
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean

  Revision : 1
    Author : pdominguez
    Date   : 24/07/2009
    Description : SS 812
        - Se eliminaron los par�metros CantidadRegistros y SumatoriaCuentas
-----------------------------------------------------------------------------}
Function TFRptEnvioNovedades.Inicializar(CodigoOperacionInterfase:Integer; NombreReporte: AnsiString): Boolean;
var                                                                               //SS_1147_NDR_20140710
    RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710

begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    FCodigoOperacionInterfase:= CodigoOperacionInterfase;
    FNombreReporte:= NombreReporte;
//    FCantidadRegistros:= CantidadRegistros;
//    FSumatoriaCuentas:= SumatoriaCuentas;
    Result := RBIListado.Execute;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 23/06/2005
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None

  Revision : 1
    Author : pdominguez
    Date   : 24/07/2009
    Description : SS 812
        - Se a�ade la inicializaci�n del par�metro @SumatoriaCuentas.
        - Se a�aden las asignaciones de los valores de los par�metros
        @SumatoriaCuentas y @CantidadTotalRegistrosEnviados al reporte.
-----------------------------------------------------------------------------}
Procedure TFRptEnvioNovedades.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
Resourcestring
    MSG_ERROR_AL_GENERAR_REPORTE = 'Error al generar el reporte';
    MSG_ERROR = 'Error';
Const
    REPORT = 'Reporte';
begin
    try
        with SpObtenerReporteEnvioNovedadesCMR do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').value:= FCodigoOperacionInterfase;
            Parameters.ParamByName('@FechaProcesamiento').value:= Now;
            Parameters.ParamByName('@Usuario').value:= '';
            Parameters.ParamByName('@NombreArchivo').value:= '';
            Parameters.ParamByName('@CantidadTotalRegistrosEnviados').value:= 0;
            Parameters.ParamByName('@CantidadAltasEnviadas').value:= 0;
            Parameters.ParamByName('@CantidadBajasEnviadas').value:= 0;
            Parameters.ParamByName('@SumatoriaCuentas').value:= 0; // SS 812
            ExecProc;
        end;
    except
        on e: Exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
        end;
    end;
    try
        //Asigno los valores a los campos del reporte
        with SpObtenerReporteEnvioNovedadesCMR do begin
            //Encabezado
            ppFechaHora.Caption                      := DateTimeToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
            ppUsuario.Caption                        := Parameters.ParamByName('@Usuario').Value;
            ppNumeroProceso.Caption                  := InttoStr(FCodigoOperacionInterfase);
            ppIdentificacionArchivoGenerado.Caption  := ExtractFileName(Parameters.ParamByName('@NombreArchivo').Value);
            //Datos del Archivo
            ppCantidadTotalRegistrosEnviados.Caption := Parameters.ParamByName('@CantidadTotalRegistrosEnviados').Value;
            ppCantidadAltasEnviadas.Caption          := Parameters.ParamByName('@CantidadAltasEnviadas').Value;
            ppCantidadBajasEnviadas.Caption          := Parameters.ParamByName('@CantidadBajasEnviadas').Value;
            //Datos del Registro de Control
            ppCantidaddeRegistros.Caption            := Parameters.ParamByName('@CantidadTotalRegistrosEnviados').Value; // SS 812
            ppSumatoriadeCuentas.Caption             := Parameters.ParamByName('@SumatoriaCuentas').Value; // SS 812
        end;
        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FnombreReporte;
    except
        on E: exception do begin
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.
