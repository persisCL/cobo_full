{-------------------------------------------------------------------------------
 File Name: FrmConvertirNominaATabla.pas
 Author:    lgisuk
 Date Created: 25/11/2004
 Language: ES-AR
 Description:  M�dulo de la interface TransBank - Recepci�n de Rendiciones
--------------------------------------------------------------------------------
 Revision History
------------------------
--------------------------------------------------------
 Author: rcastro
 Date Created: 17/12/2004
 Description: Revisi�n Mensajes, resourcestring

 Revision 1
 Author: nefernandez
 Date Created: 09/03/2007
 Description: Se pasa el parametro CodigoUsuario al SP ProcesarRespuestaANomina
              (para Auditoria)
 Revision 2
 Author: vpaszkowicz
 Date Created: 27/06/2007
 Description: Se pasa a la forma de trabajo de presto. Se elimina la tabla
 tbk_nominas y los campos del archivo se cargan directamente al store que procesa.

 Revision 3
 Author: mbecerra
 Date: 01-Agosto-2008
 Description: Se cambia para que el n�mero de comprobante repetido sea correlativo
            para cada comprobante, y no un correlativo para todos, como ahora.

 Revision 4
 Author: mbecerra
 Date: 01-Junio-2009
 Description:		(Ref. Facturaci�n Electronica)
            	Se cambia la instrucci�n beginTrans por un BEGIN TRAN expl�cito
                para que el SAVE TRANSACTION no d� error.


Firma       : SS_1243_NDR_20150127
Descripcion : Revision del proceso de interfaces que bloquea otros procesos (transaccionalidad)

Autor       :   Claudio Quezada
Fecha       :   06-02-2015
Firma       :   SS_1147_CQU_20150316
Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1147_CQU_20150511
Descripcion : Se elimina el corte en el NumeroConvenio, ya no se recorta a 12.
              Esto, ademas, obliga a agergar el TipoComprobante a todo el proceso.
              Este formulario hay que modificarlo para que quede igual que las otras Rendiciones.

Firma		: CAC-CLI-066_MGO_20160218
Descripcion	: Se agrega registro a EMAIL_Correspondencia para enviar email informando el rechazo
-------------------------------------------------------------------------------}
unit FrmConvertirNominaATabla;

interface
(*$WARNINGS ON*)
uses
    //Gestion de Interfaces
  	DMConnection,
    Util,
    UtilDB,
    UtilProc,
    PeaProcs,
    PeaTypes,                                                                   //SS_1147Q_NDR_20141202[??]
    ConstParametrosGenerales,
    ComunesInterfaces,
    FrmRptRecepcionRendiciones,
    //General
  	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  	Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons,
  	DPSControls, ListBoxEx, DBListEx, ComCtrls,  StrUtils, UtilRB,
    ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd, ppReport, ppDB,
    ppComm, ppRelatv, ppTxPipe, Menus, CollPnl, DBClient, MidasLib,
	VariantComboBox;

type
  TFConvertirNominaATabla = class(TForm)
    OpenDialog: TOpenDialog;
    pnlAbajo: TPanel;
    SpObtenerResumenComprobantesRecibidos: TADOStoredProc;
    SpObtenerResumenComprobantesRecibidosMotivo: TStringField;
    SpObtenerResumenComprobantesRecibidosCantidad: TIntegerField;
    SpObtenerResumenComprobantesRecibidosMonto: TStringField;
    spPagar: TADOStoredProc;
    cpPanelTop: TCollapsablePanel;
    btnBuscarArchivo: TSpeedButton;
    txtOrigen: TEdit;
    lblOrigen: TLabel;
    Panel1: TPanel;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    PageControl1: TPageControl;
    tsValidas: TTabSheet;
    dblValidas: TDBListEx;
    cdsArchivo: TClientDataSet;
    dsRechazos: TDataSource;
    cdsRechazos: TClientDataSet;
    lblInfo: TLabel;
    spInsertarPagorechazado: TADOStoredProc;
    StringField1: TStringField;
    IntegerField1: TIntegerField;
    StringField2: TStringField;
    spAgregarDetalle: TADOStoredProc;
    StringField3: TStringField;
    IntegerField2: TIntegerField;
    StringField4: TStringField;
    spAgregarRendicionesInterfases: TADOStoredProc;
    StringField5: TStringField;
    IntegerField3: TIntegerField;
    StringField6: TStringField;
    spValidarRendiciones: TADOStoredProc;
    StringField7: TStringField;
    IntegerField4: TIntegerField;
    StringField8: TStringField;
    spEliminar: TADOStoredProc;
    btnReprocesar: TButton;
    btnPausa: TButton;
    lblProceso: TLabel;
    spCargarTablaRendicionesTBK: TADOStoredProc;
    spEMAIL_Correspondencia_Agregar: TADOStoredProc;		//CAC-CLI-066_MGO_20160218
    StringField9: TStringField;							//CAC-CLI-066_MGO_20160218
    IntegerField5: TIntegerField;						//CAC-CLI-066_MGO_20160218
    StringField10: TStringField;						//CAC-CLI-066_MGO_20160218
    procedure btnSalirClick(Sender: TObject);
	  procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBuscarArchivoClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure txtOrigenChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnReprocesarClick(Sender: TObject);
    procedure btnPausaClick(Sender: TObject);
  private
    FCodigoOperacion : Integer;
    FCancelar : Boolean;      //Indica si se cancelo la operacion
    FTotalRegistros :  Integer;
    FTBK_CodigodeComercio : AnsiString;
    FTBK_DirectorioDestino : AnsiString;
    FTBK_DirectorioRendicionesProcesadas : AnsiString;
    FProcesando: Boolean;
    FPausa : Boolean;
    FCodigoNativa : Integer;  // SS_1147_CQU_20150316
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
   	Function  AbrirArchivo : Boolean;
    Function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
    function RegistrarOperacion: Boolean;
    function CargarClientDataset: Boolean;
    procedure Mostrar(const Texto: string);
    procedure MostrarProceso(const NumeroProceso: Integer);
    //function AgregarPagoRechazado(const NumeroComprobante: Int64; CodigoInterface: Int64; CodigoEstadoDebito: Integer; Fecha: TDateTime;                                  // SS_1147_CQU_20150511
    function AgregarPagoRechazado(const NumeroComprobante: Int64; CodigoInterface: Int64; TipoComprobante: string; CodigoEstadoDebito: Integer; Fecha: TDateTime;           // SS_1147_CQU_20150511
        CantidadRechazos: Integer; NumeroConvenio: string; Descripcion: string; CodigoRespuesta: string; MontoTransaccion: int64; out DescriError:String): Boolean;
    function AgregarRendicionesInterfases(const CodigoInterface: Integer; NumeroComprobante: int64; NumeroConvenio: string;
      //MontoTransaccion: integer; NumeroComprobanteOriginal: int64; CodigoRespuesta: string; out DescriError: string):Boolean;                                             // SS_1147_CQU_20150511
      MontoTransaccion: integer; NumeroComprobanteOriginal: int64; CodigoRespuesta: string; out DescriError: string; TipoComprobante: string):Boolean;                      // SS_1147_CQU_20150511
    //function Descartar(const CodigoInterface: Int64; NumeroConvenio:string; NumeroComprobante: Int64; CodigoRespuesta, GlosaRespuesta: string;                            // SS_1147_CQU_20150511
    function Descartar(const CodigoInterface: Int64; NumeroConvenio:string; NumeroComprobante: Int64; TipoComprobante : string; CodigoRespuesta, GlosaRespuesta: string;    // SS_1147_CQU_20150511
    FechaProceso: TDateTime; Importe: Int64; out CodigoEstadoDebito: Integer; out MotivoDescarte, DescriError: string): Boolean;
    procedure RegistrarRendiciones;
    //function RegistrarPago(const NumeroComprobante: Int64; CodigoRespuesta: string; Importe: Int64; FechaExpiracion: String; FechaProceso: TDateTime; CodigoAutorizacion: String;CodigoOperacionInterface: Int64; out NumeroPago: Int64; out DescriError: string): Boolean;                           // SS_1147_CQU_20150511
    function RegistrarPago(const NumeroComprobante: Int64; CodigoRespuesta: string; Importe: Int64; FechaExpiracion: String; FechaProceso: TDateTime; CodigoAutorizacion: String; CodigoOperacionInterface: Int64; out NumeroPago: Int64; out DescriError: string; TipoComprobante : string): Boolean;  // SS_1147_CQU_20150511
    function RegistrarBaja(const NumeroConvenio: string; CodigoRespuesta: string; CodigoOperacionInterface: Int64; out CodigoError: Integer; Out DescripcionErrorBase: string; out DescriError: string): Boolean;
    procedure DoPausa;
    function CargarTablaTemporal: boolean;
    function ActualizarLog(CantidadErrores: integer): Boolean;
    { Private declarations }
  public
    Function  Inicializar(txtCaption : ANSIString; MDIChild : Boolean) : Boolean;
    { Public declarations }
  end;

var
  FConvertirNominaATabla : TFConvertirNominaATabla;
  FLista : TStringList;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 25/11/2004
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFConvertirNominaATabla.Inicializar(txtCaption : ANSIString; MDIChild : Boolean) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 25/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        TBK_CODIGODECOMERCIO                = 'TBK_CodigodeComercio';
        TBK_DIRECTORIODESTINO               = 'TBK_DirectorioDestino';
        TBK_DIRECTORIORENDICIONESPROCESADAS = 'TBK_DirectorioRendicionesProcesadas';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la Concesionaria Nativa                  // SS_1147_CQU_20150316
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150316
                
                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, TBK_CODIGODECOMERCIO , FTBK_CodigodeComercio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + TBK_CODIGODECOMERCIO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FTBK_CodigodeComercio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + TBK_CODIGODECOMERCIO;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, TBK_DIRECTORIODESTINO , FTBK_DirectorioDestino) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + TBK_DIRECTORIODESTINO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FTBK_DirectorioDestino := GoodDir(FTBK_DirectorioDestino);
                if  not DirectoryExists(FTBK_DirectorioDestino) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FTBK_DirectorioDestino;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,TBK_DIRECTORIORENDICIONESPROCESADAS,FTBK_DirectorioRendicionesProcesadas);

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerNombreArchivo
      Author:    lgisuk
      Date Created: 04/07/2005
      Description: obtengo el nombre del archivo tipo a buscar
      Parameters: var NombreArchivo:Ansistring
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerNombreArchivo(var NombreArchivo : Ansistring) : Boolean;
    const
        FILE_EXTENSION = '.txt';
    begin
        //Genero el Nombre del Archivo de Nomina
        if FCodigoNativa = CODIGO_VS then                                           // SS_1147_CQU_20150316
            NombreArchivo:= 'NOMI' + FTBK_CodigodeComercio + 'MM' + 'DD' + 'R.001'  // SS_1147_CQU_20150316
        else                                                                        // SS_1147_CQU_20150316
        NombreArchivo:= 'NOMI' + FTBK_CodigodeComercio + 'MM' + 'DD' + '.' + 'R' + '.' + '000' + FILE_EXTENSION;
        Result := True;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
var
    NombreArchivo : AnsiString;
begin
    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;
    
    FProcesando := False;
    FPausa := False;
    //Centro el formulario
    CenterForm(Self);
    try
        cdsArchivo.CreateDataSet;
        cdsArchivo.LogChanges := False;
        cdsRechazos.CreateDataSet;
        cdsRechazos.LogChanges := False;
        if not DMConnections.BaseCAC.Connected then begin
            DMConnections.BaseCAC.Connected := True;
        end;
        Result := (VerificarParametrosGenerales and ObtenerNombreArchivo(NombreArchivo));
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
  	btnProcesar.Enabled := False;
    FCodigoOperacion := 0;
end;

procedure TFConvertirNominaATabla.Mostrar(const Texto: string);
begin
    lblInfo.Caption := Texto;
    lblInfo.Update;
    Application.ProcessMessages;
end;

procedure TFConvertirNominaATabla.MostrarProceso(const NumeroProceso: Integer);
const
    MSG_PROCESO = 'N�mero de proceso de interface: %d';
begin
    lblProceso.Caption := Format(MSG_PROCESO, [NumeroProceso]);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 03/06/2005
  Description:  Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirNominaATabla.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_NOMINA          = ' ' + CRLF +
      'El archivo Archivo de Respuesta a N�mina' + CRLF +
      'es enviado por TRANSBANK al ESTABLECIMIENTO' + CRLF +
      'para cada Archivo de N�mina recibido' + CRLF +
      'y tiene por objeto dar a conocer al Establecimiento, ' + CRLF +
      'el resultado del procesamiento de la informaci�n enviada.' + CRLF +
      ' ' + CRLF +
      'Nombre del Archivo: NOMI99999999mmddY.sss' + CRLF +
      ' ' + CRLF +
      'Se utiliza para registrar los pagos de los' + CRLF +
      'comprobantes aceptados en el sistema del ESTABLECIMIENTO.' + CRLF +
      ' ';
begin
    if FProcesando then Exit;
    MsgBoxBalloon(MSG_NOMINA, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 03/06/2005
  Description:
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirNominaATabla.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if not FProcesando then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarArchivoBTNClick
  Author:    lgisuk
  Date Created: 03/01/2005
  Description: Busco el archivo a procesar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
function TFConvertirNominaATabla.AgregarPagoRechazado(
  const NumeroComprobante: Int64; CodigoInterface: Int64;
  TipoComprobante : string; // SS_1147_CQU_20150511
  CodigoEstadoDebito: Integer; Fecha: TDateTime; CantidadRechazos: Integer; NumeroConvenio: string ; Descripcion: string;
  CodigoRespuesta: string; MontoTransaccion: int64; out DescriError: String): Boolean;
const
    NOTA_COBRO = 'NK';
var
    Error: String;
    NumeroComprobanteModificado: Int64;
begin
    Result := False;



    //si es un rechazo por duplicado dentro del mismo archivo, escribimos el numero de NK original + 000 y en esos 000 agregamos el numero de repeticion
    //es para poder hacer el join con PagosRechazados y detalle de comprobantes recibidos.
    if CantidadRechazos > 0  then NumeroComprobanteModificado := NumeroComprobante * 1000 + CantidadRechazos else NumeroComprobanteModificado := NumeroComprobante;

    if not cdsRechazos.Active then cdsRechazos.Active := True;
    try
        //Mostramos el rechazo por pantalla
        //cdsRechazos.AppendRecord(['NOTA_COBRO', NumeroComprobante, CodigoInterface, CodigoEstadoDebito, Fecha, Descripcion, NumeroConvenio]);             // SS_1147_CQU_20150511
        cdsRechazos.AppendRecord([TipoComprobante, NumeroComprobante, CodigoInterface, CodigoEstadoDebito, Fecha, Descripcion, NumeroConvenio]);            // SS_1147_CQU_20150511
        cdsRechazos.Last;
        // Ejecutar el Stored de pago rechazado!
        if not DMConnections.BaseCAC.InTransaction then begin
            //REV.4 DMConnections.BaseCAC.BeginTrans;
            DMConnections.BaseCAC.Execute('BEGIN TRAN frmConvertirANomina');
        end else begin
            DescriError := 'No se pudo iniciar una transacci�n para registrar un pago rechazado' + CRLF +
              'Verifique que no haya otra interfaz ejecutando desde este sistema' + CRLF +
              'y vuelva a intentar la operaci�n';
            Exit;
        end;
        // Registramos el pago rechazado en la tabla pagos rechazados
        //spInsertarPagorechazado.Parameters.ParamByName('@TipoComprobante').Value :=  NOTA_COBRO;                      // SS_1147_CQU_20150511
        spInsertarPagorechazado.Parameters.ParamByName('@TipoComprobante').Value :=  TipoComprobante;                   // SS_1147_CQU_20150511

        spInsertarPagorechazado.Parameters.ParamByName('@NumeroComprobante').Value :=  NumeroComprobanteModificado;
        spInsertarPagorechazado.Parameters.ParamByName('@CodigoOperacionInterfase').Value :=  CodigoInterface;
        spInsertarPagorechazado.Parameters.ParamByName('@CodigoEstadoDebito').Value :=  CodigoEstadoDebito;
        spInsertarPagorechazado.ExecProc;

        // Se agrega registro a EMAIL_Correspondencia para enviar email informando el rechazo																							//CAC-CLI-066_MGO_20160218
        with spEMAIL_Correspondencia_Agregar do begin																																	//CAC-CLI-066_MGO_20160218
            Close;																																										//CAC-CLI-066_MGO_20160218
            Parameters.Refresh;																																							//CAC-CLI-066_MGO_20160218
            Parameters.ParamByName('@CodigoTipoCorrespondencia').Value := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_TIPO_CORRESPONDENCIA_PAGO_RECHAZADO()');			//CAC-CLI-066_MGO_20160218
            Parameters.ParamByName('@CodigoConvenio').Value := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerCodigoConvenio(%s)' , [QuotedStr(NumeroConvenio)]));	//CAC-CLI-066_MGO_20160218
            Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;																											//CAC-CLI-066_MGO_20160218
            ExecProc;																																									//CAC-CLI-066_MGO_20160218
        end;																																											//CAC-CLI-066_MGO_20160218

        // Registramos el detalle en la tabla detalle comprobantes recibidos
        spAgregarDetalle.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoInterface;
        //spAgregarDetalle.Parameters.ParamByName('@TipoComprobante').Value := NOTA_COBRO;                              // SS_1147_CQU_20150511
        spAgregarDetalle.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;                           // SS_1147_CQU_20150511
        spAgregarDetalle.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobanteModificado;
        spAgregarDetalle.Parameters.ParamByName('@NumeroPago').Value := NULL;
        spAgregarDetalle.Parameters.ParamByName('@MontoEnCtvs').Value := NULL;
        spAgregarDetalle.ExecProc;
        if CantidadRechazos > 0  then // solo se agrega desde aca en la tabla Rendiciones interfaces cuando se trata de un registo duplicado en el archivo
            //if not AgregarRendicionesInterfases(CodigoInterface , NumeroComprobanteModificado, NumeroConvenio, MontoTransaccion, NumeroComprobante, CodigoRespuesta, Error) then begin                // SS_1147_CQU_20150511
            if not AgregarRendicionesInterfases(CodigoInterface , NumeroComprobanteModificado, NumeroConvenio, MontoTransaccion, NumeroComprobante, CodigoRespuesta, Error, TipoComprobante) then begin // SS_1147_CQU_20150511
                raise Exception.Create('Error grave ejecutando Agregar Rendiciones Interfaces' + CRLF + Error);
            end;
        //REV.4 DMConnections.BaseCAC.CommitTrans;
        DMConnections.BaseCAC.Execute('COMMIT TRAN frmConvertirANomina');
        // Regio!!!
        Result := True;
    except
        on e: exception do begin
            //if DMConnections.BaseCAC.InTransaction then begin                                                             //SS_1243_NDR_20150127
            	 //REV.4 DMConnections.BaseCAC.RollbackTrans;                                                                 //SS_1243_NDR_20150127
               //DMConnections.BaseCAC.Execute('ROLLBACK TRAN frmConvertirANomina');                                        //SS_1243_NDR_20150127
               QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION frmConvertirANomina END');  //SS_1243_NDR_20150127
            //end;                                                                                                          //SS_1243_NDR_20150127
            DescriError := e.Message;
        end;
    end;
end;

function TFConvertirNominaATabla.AgregarRendicionesInterfases(const CodigoInterface: Integer; NumeroComprobante: int64; NumeroConvenio: string;
      //MontoTransaccion: integer; NumeroComprobanteOriginal: int64; CodigoRespuesta: string; out DescriError: string;):Boolean;                        // SS_1147_CQU_20150511
      MontoTransaccion: integer; NumeroComprobanteOriginal: int64; CodigoRespuesta: string; out DescriError: string; TipoComprobante: string):Boolean;  // SS_1147_CQU_20150511
const
    NOTA_COBRO = 'NK';
begin
      Result := False;
      try
        spAgregarRendicionesInterfases.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoInterface;
        //spAgregarRendicionesInterfases.Parameters.ParamByName('@TipoComprobante').Value := NOTA_COBRO;    // SS_1147_CQU_20150511
        spAgregarRendicionesInterfases.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante; // SS_1147_CQU_20150511
        spAgregarRendicionesInterfases.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante ;
        spAgregarRendicionesInterfases.Parameters.ParamByName('@NumeroConvenio').Value := NumeroConvenio;
        spAgregarRendicionesInterfases.Parameters.ParamByName('@MontoDeLaTransaccion').Value := MontoTransaccion;
        spAgregarRendicionesInterfases.Parameters.ParamByName('@Aceptado').Value := (CodigoRespuesta = '0');
        spAgregarRendicionesInterfases.Parameters.ParamByName('@NotaCobroOriginal').Value := NumeroComprobanteOriginal;
        spAgregarRendicionesInterfases.ExecProc;
        Result := True;
      except
          on e: exception do begin
              DescriError := e.Message;
          end;
      end;
end;


procedure TFConvertirNominaATabla.btnBuscarArchivoClick(Sender: TObject);
    {-----------------------------------------------------------------------------
      Function Name: EsArchivoValido
      Author:    lgisuk
      Date Created: 04/07/2005
      Description: Verifica si es un Archivo Valido para Esta Pantalla
      Parameters: Nombre:Ansistring
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function EsArchivoValido(Nombre : AnsiString) : Boolean;
    begin
        Result :=  ExistePalabra(Nombre, FTBK_CodigodeComercio) and //posee el codigo de comercio
          ExistePalabra(Nombre,'R.');//es archivo de Respuesta a Nomina
    end;

resourcestring
	MSG_ERROR = 'No es un archivo de Respuesta a Nomina Valido!'+ CRLF +
        			'Se esperaba un archivo con el formato NOMI99999999mmddR.sss';
begin
    OpenDialog.InitialDir:= FTBK_DirectorioDestino;
    if OpenDialog.Execute then begin
        if not EsArchivoValido(OpenDialog.FileName) then begin
            MsgBox(MSG_ERROR, self.Caption, MB_OK + MB_ICONINFORMATION);
      			txtOrigen.text := '';
        end else begin
            btnBuscarArchivo.Enabled := False;
            txtOrigen.text := OpenDialog.FileName;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EOrigenChange
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: Controlo que abra un archivo valido
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirNominaATabla.txtOrigenChange(Sender: TObject);
begin
    //Controlo que abra un archivo valido
  	btnProcesar.Enabled := FileExists(txtOrigen.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: AbrirArchivo
  Author:    lgisuk
  Date Created: 16/12/2004
  Description: Abrir Archivo TXT
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFConvertirNominaATabla.AbrirArchivo : Boolean;
resourcestring
	  MSG_OPEN_FILE_ERROR = 'Error al abrir el archivo';
begin
    Result := False;
    FLista.text := FileToString(txtOrigen.text);
  	if FLista.text <> '' then  begin
        FTotalRegistros := FLista.Count;
        if FTBK_DirectorioRendicionesProcesadas <> '' then begin
            if RightStr(FTBK_DirectorioRendicionesProcesadas,1) = '\' then  FTBK_DirectorioRendicionesProcesadas := FTBK_DirectorioRendicionesProcesadas + ExtractFileName(txtOrigen.text)
            else FTBK_DirectorioRendicionesProcesadas := FTBK_DirectorioRendicionesProcesadas + '\' + ExtractFileName(txtOrigen.text);
        end;
        // Cargamos el Dataset con el contenido del archivo y comenzamos la detecci�n
        // de registros duplicados en memoria registrando como pagos rechazados
        // todos aquellos registros del archivo duplicados o que no se puedan
        // parsear correctamente.
        Result:= CargarClientDataset;
    end else begin
        MsgBox(MSG_OPEN_FILE_ERROR);
        Exit;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Genero el Reporte de Finalizacion de Proceso. sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFConvertirNominaATabla.GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
const
    STR_TITLE = 'Comprobantes Recibidos';
var
    Recepcion : TFRptRecepcionRendiciones;
begin
    Application.CreateForm(TFRptRecepcionRendiciones, Recepcion);
    Result := Recepcion.Inicializar(STR_TITLE , CodigoOperacionInterfase);
end;

function TFConvertirNominaATabla.RegistrarBaja(const NumeroConvenio: string;
  CodigoRespuesta: string; CodigoOperacionInterface: Int64;
  out CodigoError: Integer; out DescripcionErrorBase,
  DescriError: string): Boolean;
var
    CodigoConvenio: Int64;
begin
    Result := False;
    try
        CodigoConvenio := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerCodigoConvenio(%s)' , [QuotedStr(NumeroConvenio)]));
        spEliminar.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        spEliminar.Parameters.ParamByName('@CodigoDeRespuesta').Value := CodigoRespuesta;
        spEliminar.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterface;
        spEliminar.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        spEliminar.Parameters.ParamByName('@CodigoError').Value := Null;
        spEliminar.Parameters.ParamByName('@DescripcionError').Value := Null;
        spEliminar.ExecProc;
        CodigoError := spEliminar.Parameters.ParamByName('@CodigoError').Value;
        DescripcionErrorBase := spEliminar.Parameters.ParamByName('@DescripcionError').Value;
        Result := (CodigoError = 0);                  
    except
        on e: Exception do begin
            DescriError := e.Message;    
        end;
    end;
end;

function TFConvertirNominaATabla.RegistrarOperacion: boolean;
resourcestring
  	MSG_ERRORS 							= 'Errores : ';
  	MSG_COULD_NOT_REGISTER_OPERATION 	= 'No se pudo registrar la operaci�n';
    MSG_ERROR 							= 'Error';
    STR_OBSERVACION                     =  'Procesar Respuesta A Nomina';
const
    COD_MODULO = 36;
var
    DescError : string;
    NombreArchivo : AnsiString;
begin
    NombreArchivo:= ExtractFileName(txtOrigen.text);
  	Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, COD_MODULO, NombreArchivo, UsuarioSistema, STR_OBSERVACION, False, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);
    if not result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

//function TFConvertirNominaATabla.RegistrarPago(const NumeroComprobante: Int64; CodigoRespuesta: string; Importe: Int64; FechaExpiracion: String; FechaProceso: TDateTime; CodigoAutorizacion: String; CodigoOperacionInterface: Int64; out NumeroPago: Int64; out DescriError: string): Boolean;                          // SS_1147_CQU_20150511
function TFConvertirNominaATabla.RegistrarPago(const NumeroComprobante: Int64; CodigoRespuesta: string; Importe: Int64; FechaExpiracion: String; FechaProceso: TDateTime; CodigoAutorizacion: String; CodigoOperacionInterface: Int64; out NumeroPago: Int64; out DescriError: string; TipoComprobante : string): Boolean;  // SS_1147_CQU_20150511
resourcestring                                                                                                            // SS_1147_CQU_20150511
    MSG_SIN_COMPROBANTE         = 'No se encontr� el comprobante asociado al tipo fiscal = %s, y al N�mero Fiscal = %d';  // SS_1147_CQU_20150511
    SQL_ACTUALIZAR_RENDICION    = 'UPDATE RendicionesInterfases SET NumeroPago = %d, Aceptado = 1 '                       // SS_1147_CQU_20150511
                                    + 'WHERE CodigoOperacionInterfase = %d AND NumeroComprobante = %d '                   // SS_1147_CQU_20150511
                                    + 'AND TipoComprobante = ''%s''';                                                     // SS_1147_CQU_20150511
var
    CanalPagoPAT, FormaPagoTarjetaCredito, CodigoTarjetaTransbank, CodigoEmisorTarjetaTransBank, CodigoConceptoPagoDebitoAutomatico, CodigoEstadoDebito: Integer;
    TipoComprobanteFiscal,          // SS_1147_CQU20150511
    TipoComprobanteTemp : string;   // SS_1147_CQU20150511
    NumeroComprobanteFiscal,        // SS_1147_CQU20150511
    NumeroComprobanteTemp : Int64;  // SS_1147_CQU20150511
begin
    Result := False;
    spPagar.Close;
    try
        CanalPagoPAT 						:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CANAL_PAGO_PAT()                        ' );
        FormaPagoTarjetaCredito 			:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_FORMA_PAGO_TARJETA_CREDITO()            ' );
        CodigoTarjetaTransbank 				:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_TARJETA_TRANSBANK()              ' );
        CodigoEmisorTarjetaTransBank 		:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_EMISOR_TARJETA_TRANSBANK()              ' );
{INICIO: TASK_005_JMA_20160418		
        CodigoConceptoPagoDebitoAutomatico 	:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_CONCEPTO_PAGO_DEBITO_AUTOMATICO()' );
}
		CodigoConceptoPagoDebitoAutomatico 	:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_CONCEPTO_PAGO_DEBITO_AUTOMATICO(dbo.ObtenerConcesionariaNativa())' );
{TERMINO: TASK_005_JMA_20160418}	
        CodigoEstadoDebito := QueryGetValueInt(DMConnections.BaseCAC, format ('SELECT dbo.ObtenerCodigoEstadoDebito(%s, %d)',[CodigoRespuesta, 1]));
        if FCodigoNativa = CODIGO_VS then begin                                                                         // SS_1147_CQU_20150511
            TipoComprobanteFiscal   := TipoComprobante;                                                                 // SS_1147_CQU_20150511
            NumeroComprobanteFiscal := NumeroComprobante;                                                               // SS_1147_CQU_20150511
            TipoComprobanteTemp     := '';                                                                              // SS_1147_CQU_20150511
            NumeroComprobanteTemp   := -1;                                                                              // SS_1147_CQU_20150511
            ObtenerDocumentoInterno(TipoComprobanteFiscal, NumeroComprobanteFiscal,                                     // SS_1147_CQU_20150511
                                    TipoComprobanteTemp, NumeroComprobanteTemp);                                        // SS_1147_CQU_20150511
            if NumeroComprobanteTemp <= 0 then                                                                          // SS_1147_CQU_20150511
                raise Exception.Create(Format(MSG_SIN_COMPROBANTE, [TipoComprobanteFiscal, NumeroComprobanteFiscal]));  // SS_1147_CQU_20150511
        end else begin                                                                                                  // SS_1147_CQU_20150511
            TipoComprobanteTemp     := 'NK';                                                                            // SS_1147_CQU_20150511
            NumeroComprobanteTemp   := NumeroComprobante;                                                               // SS_1147_CQU_20150511
        end;                                                                                                            // SS_1147_CQU_20150511
        if not DMConnections.BaseCAC.InTransaction then begin
            //REV.4 DMConnections.BaseCAC.BeginTrans;
            DMConnections.BaseCAC.Execute('BEGIN TRAN frmConvertirANomina');
            
            //spPagar.Parameters.ParamByName('@TipoComprobante').Value := 'NK';                                         // SS_1147_CQU_20150511
            //spPagar.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;                          // SS_1147_CQU_20150511
            spPagar.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobanteTemp;                            // SS_1147_CQU_20150511
            spPagar.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobanteTemp;                        // SS_1147_CQU_20150511
            spPagar.Parameters.ParamByName('@CodigoEstadoDebito').Value := CodigoEstadoDebito;
            spPagar.Parameters.ParamByName('@CodigoTipoMedioPago').Value := CanalPagoPAT;
            spPagar.Parameters.ParamByName('@CodigoFormaPago').Value := FormaPagoTarjetaCredito;
            spPagar.Parameters.ParamByName('@Importe').Value := Importe * 100;
            spPagar.Parameters.ParamByName('@PAT_CodigoTipoTarjetaCredito').Value := CodigoTarjetaTransbank;
            spPagar.Parameters.ParamByName('@PAT_FechaVencimiento').Value := FechaExpiracion;
            spPagar.Parameters.ParamByName('@PAT_CodigoEmisorTarjetaCredito').Value := CodigoEmisorTarjetaTransBank;
            spPagar.Parameters.ParamByName('@FechaHoraPago').Value := FechaProceso;
            spPagar.Parameters.ParamByName('@Autorizacion').Value := CodigoAutorizacion;
            spPagar.Parameters.ParamByName('@CodigoConceptoPago').Value := CodigoConceptoPagoDebitoAutomatico;
            spPagar.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
            spPagar.Parameters.ParamByName('@DescripcionPago').Value := CodigoRespuesta;
            spPagar.Parameters.ParamByName('@NumeroPago').Value := Null;
            spPagar.ExecProc;
            NumeroPago := spPagar.Parameters.ParamByName('@NumeroPago').Value;

            // Agrego tambien en la tabla DetalleComprobantesRecibidos
            spAgregarDetalle.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterface;
            //spAgregarDetalle.Parameters.ParamByName('@TipoComprobante').Value := 'NK';                                // SS_1147_CQU_20150511
            //spAgregarDetalle.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante ;                // SS_1147_CQU_20150511
            if FCodigoNativa = CODIGO_VS then begin                                                                     // SS_1147_CQU_20150511
                // Este SP en base al Tipo y Numero Fiscal busca el correspondiente Comprobante                         // SS_1147_CQU_20150511
                spAgregarDetalle.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobanteFiscal;             // SS_1147_CQU_20150511
                spAgregarDetalle.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobanteFiscal;         // SS_1147_CQU_20150511
            end else begin                                                                                              // SS_1147_CQU_20150511
                spAgregarDetalle.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobanteTemp;               // SS_1147_CQU_20150511
                spAgregarDetalle.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobanteTemp;           // SS_1147_CQU_20150511
            end;                                                                                                        // SS_1147_CQU_20150511
            spAgregarDetalle.Parameters.ParamByName('@NumeroPago').Value := NumeroPago;
            spAgregarDetalle.Parameters.ParamByName('@MontoEnCtvs').Value := Importe * 100;
            spAgregarDetalle.ExecProc;

            if NumeroPago > 0 then begin                                                                                // SS_1147_CQU_20150511
                QueryExecute(   DMConnections.BaseCAC,                                                                  // SS_1147_CQU_20150511
                                Format(SQL_ACTUALIZAR_RENDICION, [  NumeroPago, CodigoOperacionInterface,               // SS_1147_CQU_20150511
                                                                    NumeroComprobanteTemp, TipoComprobanteTemp]));      // SS_1147_CQU_20150511
            end;                                                                                                        // SS_1147_CQU_20150511

            //REV.4 DMConnections.BaseCAC.CommitTrans;
            DMConnections.BaseCAC.Execute('COMMIT TRAN frmConvertirANomina');
            Result := (NumeroPago > 0);
        end else begin       
            raise Exception.Create('No se pudo iniciar una transacci�n para registrar un pago en el CAC');
        end;
    except
        on e: Exception do begin
            //if DMConnections.BaseCAC.InTransaction then begin                                                             //SS_1243_NDR_20150127
            	 //REV.4 DMConnections.BaseCAC.RollbackTrans;                                                                 //SS_1243_NDR_20150127
               //DMConnections.BaseCAC.Execute('ROLLBACK TRAN frmConvertirANomina');                                        //SS_1243_NDR_20150127
               QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION frmConvertirANomina END');  //SS_1243_NDR_20150127
            //end;                                                                                                          //SS_1243_NDR_20150127
            DescriError := e.Message;
        end;
    end;
end;

procedure TFConvertirNominaATabla.RegistrarRendiciones;
const
    MSG_PAGANDO = 'Registrando rendiciones, paso 2 de 2: Procesando %d de %d : Pagos registrados: [%d] - Pagos fallidos: [%d] - Rechazos registrados: [%d] - Bajas independientes registradas: [%d]';
var
    EstadoDebito: Integer;
    Motivo, Error: String;
    NumeroPago: Int64;
    CodigoErrorBase: Integer;
    DescripcionBase: string;
    PagoActual, TotalPagos,
    PagosRegistrados, PagosFallidos,
    PagosRechazados, Bajas: Integer;
    HabilitarReprocesar: Boolean;
begin
    // Registramos todas las rendiciones que nos quedaron como buenas en la tabla en memoria en la base
    TotalPagos := cdsArchivo.RecordCount;
    PagoActual:= 0;
    PagosRegistrados := 0;
    PagosFallidos := 0;
    PagosRechazados := 0;
    Bajas := 0;
    HabilitarReprocesar := False;
    btnPausa.Enabled := True;
    cdsArchivo.First;
    try
        try
            while not cdsArchivo.Eof do begin
                FProcesando := True;
                Inc(PagoActual);
                PagosRechazados := cdsRechazos.RecordCount;
                Mostrar(Format(MSG_PAGANDO, [PagoActual, TotalPagos, PagosRegistrados, PagosFallidos, PagosRechazados, Bajas]));
                // Si no figura como registro pagado y no est� descaratdo entonces le metemos pata
                if (not cdsArchivo.FieldByName('EstadoPago').AsBoolean) and (not cdsArchivo.FieldByName('Descartado').AsBoolean) then begin
                    // Si el registro esta marcado para dar de baja lo hago antes que nada
                    if cdsArchivo.FieldByName('EsBajaDefinitiva').AsBoolean then begin
                        if not RegistrarBaja(cdsArchivo.FieldByName('NumeroConvenio').AsString, cdsArchivo.FieldByName('CodigoRespuesta').AsString, cdsArchivo.FieldByName('CodigoProcesoInterface').AsInteger, CodigoErrorBase, DescripcionBase, Error) then begin
                            raise Exception.Create('Error al procesar la baja de un medio de pago ' + DescripcionBase + Space(1) + Error + Space (1) + 'Convenio:' + cdsArchivo.FieldByName('NumeroConvenio').AsString);
                        end else begin
                            // La baja se efectu� correctamente, lo marcamos en memoria como realizado y continuamos con el pr�ximo paso
                            cdsArchivo.Edit;
                            cdsArchivo.FieldByName('BajaDefinitiva').AsBoolean := True;
                            Inc(Bajas);
                        end;
                    end;
                    // Este registro tiene toda la pinta de estar listo, verificamos entonces si hay que descartarlo
                    // registr�ndolo entonces como pago rechazado en el sistema o podemos continuar con el pago.
                    if Descartar(cdsArchivo.FieldByName('CodigoProcesoInterface').AsInteger, cdsArchivo.FieldByName('NumeroConvenio').AsString,
                      //cdsArchivo.FieldByName('NumeroComprobante').AsInteger, cdsArchivo.FieldByName('CodigoRespuesta').AsString,  // SS_1147_CQU_20150511
                      cdsArchivo.FieldByName('NumeroComprobante').AsInteger, cdsArchivo.FieldByName('TipoComprobante').AsString,    // SS_1147_CQU_20150511
                      cdsArchivo.FieldByName('CodigoRespuesta').AsString,                                                           // SS_1147_CQU_20150511
                      cdsArchivo.FieldByName('GlosaRespuesta').AsString, cdsArchivo.FieldByName('FechaProceso').AsDateTime,
                      cdsArchivo.FieldByName('MontoTransaccion').AsInteger,
                      EstadoDebito, Motivo, Error) then begin
                        // Esta recepci�n hay que descartarla lo que implica anotarla como un pago rechazado
                        if AgregarPagorechazado(cdsArchivo.FieldByName('NumeroComprobante').AsInteger, cdsArchivo.FieldByName('CodigoProcesoInterface').AsInteger,
                          cdsArchivo.FieldByName('TipoComprobante').AsString,                                                       // SS_1147_CQU_20150511                        
                          EstadoDebito, Now, 0, cdsArchivo.FieldByName('NumeroConvenio').AsString, Motivo, cdsArchivo.FieldByName('CodigoRespuesta').AsString,
                          cdsArchivo.FieldByName('MontoTransaccion').AsInteger, Error) then begin
                            //Marco  este registro como descartado lo que implica que origin� el rechazo de un pago.
                            cdsArchivo.Edit;
                            cdsArchivo.FieldByName('Descartado').AsBoolean := True;
                            // Listo con este, vamos por el siguiente
                            cdsArchivo.Next;
                        end else begin
                            raise Exception.Create('Error al registrar un pago rechazado ' + DescripcionBase + Space(1) + Error + Space (1) + 'Convenio:' + cdsArchivo.FieldByName('NumeroConvenio').AsString);
                        end;
                    end else begin
                        //Tal vez entramos por ac� porque Descartar dio un error.... ojo
                        if Error <> '' then begin
                            raise Exception.Create('Error al validar una linea del archivo ' + DescripcionBase + Space(1) + Error + Space (1) + 'Convenio:' + cdsArchivo.FieldByName('NumeroConvenio').AsString);
                        end;
                        // Este pago no hay que descartalo por lo que vamos a intentar registar el pago.
                        if RegistrarPago(cdsArchivo.FieldByName('NumeroComprobante').AsInteger, cdsArchivo.FieldByName('CodigoRespuesta').AsString,
                          cdsArchivo.FieldByName('MontoTransaccion').AsInteger, cdsArchivo.FieldByName('FechaExpiracion').AsString, cdsArchivo.FieldByName('FechaProceso').AsDateTime,
                          //cdsArchivo.FieldByName('CodigoAutorizacion').AsString, cdsArchivo.FieldByName('CodigoProcesoInterface').AsInteger, NumeroPago, Error) then begin    // SS_1147_CQU_20150511
                          cdsArchivo.FieldByName('CodigoAutorizacion').AsString, cdsArchivo.FieldByName('CodigoProcesoInterface').AsInteger, NumeroPago, Error,                 // SS_1147_CQU_20150511
                          cdsArchivo.FieldByName('TipoComprobante').AsString) then begin                                                                                        // SS_1147_CQU_20150511
                            // Lo marcamos como procesado exitosamente
                            cdsArchivo.Edit;
                            cdsArchivo.FieldByName('EstadoPago').AsBoolean := True;
                            // Vamos por el siguiente!
                            cdsArchivo.Next;
                            Inc(PagosRegistrados);
                        end else begin
                            //Algo no funciono, pero no importa, seguimos con el pr�ximo, despu�s podemos reintentar manualmente
                            // y ya habilitamos el bot�n reprocesar para cuando terminemos con todos los que nos queden
                            // lo anotamos en el clientdataset como pago que no entr� con su respectivo error
                            //cdsRechazos.AppendRecord(['NOTA_COBRO', cdsArchivo.FieldByName('NumeroComprobante').AsInteger,                                        // SS_1147_CQU_20150511
                            cdsRechazos.AppendRecord([cdsArchivo.FieldByName('TipoComprobante').AsString, cdsArchivo.FieldByName('NumeroComprobante').AsInteger,    // SS_1147_CQU_20150511
                              cdsArchivo.FieldByName('CodigoProcesoInterface').AsInteger, 0, Now, Error, cdsArchivo.FieldByName('NumeroConvenio').AsString]);
                            cdsRechazos.Last;
                            Inc(PagosFallidos);
                            cdsArchivo.Next;
                            HabilitarReprocesar := True;
                        end;
                    end;
                end else begin
                    // Si caemos en esta situaci�n el registro de pago que
                    // tenemos en el ClientDataSet ya fu� procesado entonces vamos a ver que pasa con el siguiente.
                    cdsArchivo.Next;
                end;
                // Por �ltimo revisamos si tenemos que parar por cancelaci�n del Usuario o tenemos que pausar
                PagosRechazados := cdsRechazos.RecordCount;
                Mostrar(Format(MSG_PAGANDO, [PagoActual, TotalPagos, PagosRegistrados, PagosFallidos, PagosRechazados, Bajas]));
                if FCancelar then Exit;
                if FPausa then DoPausa;
            end;
            // Revisamos como sali� todo hasta ac�
            if PagosFallidos = 0 then begin
                MsgBox('Proceso Finalizado. Todas las rendiciones fueron procesadas correctamente', Caption, MB_ICONINFORMATION);
            end else begin
                MsgBox('Proceso finalizado con errores. Pagos que no se pudieron registrar:'  + IntToStr(PagosFallidos) + CRLF +
                  'Puede intentar procesarlas nuevamente presionando el bot�n "Reprocesar"', Caption, MB_ICONWARNING);
            end;
        except
            on e: exception do begin
                MsgboxErr('Error procesando rendiciones', e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
        if FCancelar then begin
            if HabilitarReprocesar then begin
                MsgBox('Proceso cancelado por el usuario ' + UsuarioSistema + CRLF +
                  'Existen ' + IntToStr(PagosFallidos) + ' rendiciones que no se pudieron registrar correctamente.' + CRLF +
                  'Puede volver a intentar procesarlas a continuaci�n presionando el bot�n "Reprocesar"', Caption, MB_ICONWARNING);
            end else begin
                MsgBox('Proceso cancelado por el usuario ' + UsuarioSistema,  Caption, MB_ICONINFORMATION);
            end;
        end else begin
            // si el usuario no cancel� y no quedan pagos para procesar, generamos los reportes
            if PagosFallidos = 0 then begin
                ActualizarLog(PagosFallidos);
                GenerarReportedeFinalizacion(FCodigoOperacion);
            end;
        end;
        btnReprocesar.Enabled := HabilitarReprocesar;
        FProcesando := False;
        btnPausa.Enabled := False;
        btnCancelar.Enabled := False;
    end;
end;

procedure TFConvertirNominaATabla.btnPausaClick(Sender: TObject);
begin
    FPausa := not FPausa;
    if FPausa then begin
        btnPausa.Caption := 'Continuar';
    end else begin
        btnPausa.Caption := 'Pausar';
    end;
end;

procedure TFConvertirNominaATabla.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/06/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz( CodigoOperacion : Integer; var Cantidad : Integer ) : Boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name:  ObtenerResumen
      Author:    lgisuk
      Date Created: 31/03/2006
      Description:  Obtengo el resumen del proceso realizado
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    procedure ObtenerResumen (CodigoOperacionInterfase : Integer;Var CantRegs, Aprobados, Rechazados : Integer);
    resourcestring
      MSG_ERROR = 'Error';
      MSG_ERROR_QUERY = 'Error Obteniendo Resumen';
    begin
      try
          With spObtenerResumenComprobantesRecibidos, Parameters do begin
              Close;
              ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
              CommandTimeout := 500;
              Open;
              CantRegs := Parameters.ParamByName('@CantidadComprobantesRecibidos').Value;
              Aprobados := Parameters.ParamByName('@CantidadComprobantesAceptados').Value;
              Rechazados := Parameters.ParamByName('@CantidadComprobantesRechazados').Value;
              Close;
          end;
      except
          on E : Exception do begin
              MsgBoxErr(MSG_ERROR_QUERY, E.Message, MSG_ERROR, MB_ICONERROR);
          end;
      end;
    end;


begin
    if MsgBox('�Desea comenzar el proceso de registraci�n de rendiciones TransBank?', Caption,
      MB_ICONQUESTION or MB_YESNO) = IDYES then begin
  	    btnProcesar.Enabled := False;
        btnCancelar.Enabled := True;
        KeyPreview := True;
        if not RegistrarOperacion  then begin
            Exit;
        end;

        if not CargarTablaTemporal then begin
            Exit;
        end;

        MostrarProceso(FCodigoOperacion);
        if AbrirArchivo then begin
            RegistrarRendiciones;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: Permito cancelar el proceso
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  16/10/2007
  Ahora cancela si selecciona escape
-----------------------------------------------------------------------------}
procedure TFConvertirNominaATabla.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    if key = VK_ESCAPE then begin
        FCancelar := True;
        btnCancelar.Enabled := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: Permito salir del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirNominaATabla.btnSalirClick(Sender: TObject);
begin
    Close;
end;

function TFConvertirNominaATabla.CargarClientDataset: Boolean;

    function  ObtenerFechaProceso(FechaStr: string): TDateTime;
    begin
      	  Result :=	EncodeDate(	StrToInt(Copy(FechaStr, 5, 4)), StrToInt(Copy(FechaStr, 3, 2)), StrToInt(Copy(FechaStr, 1, 2)));
    end;

const
    SQL_BAJA_DEFINITIVA = 'SELECT dbo.EsRespuestaRendicionRechazoDefinitivo(%d, %d, %s)';
    MSG_CARGANDO_ARCHIVO = 'Analizando archivo - Paso 1 de 2: Analizando %d de %d rendiciones recibidas. Bajas existentes en el archivo [%d]. Rechazos en el archivo: [%d]';
    MSG_MOTIVO_DUPLICADO = 'Comprobante duplicado en el archivo de TransBank';
    MSG_ERROR_LINEA = 'Ha ocurrido un error procesando la l�nea %d' + CRLF + 'L�nea: %s';
var
    i: Integer;
    NumeroComprobante: Int64;
    NumeroConvenio: string;
    NumeroTarjeta: string;
    FechaExpiracion: string;
    CodigoAutorizacion: string;
    MontoTransaccion: Int64;
    CodigoRespuesta: string;
    FechaProceso: TDateTime;
    GlosaRespuesta: string;
    EsBajaDefinitiva: Boolean;
    CodigoTipoMedioPago, CodigoTipoTarjetaCredito: Integer;
    CodigoRechazoDuplicado: Integer;
    Error : string;
    CantidadBajas: Integer;
    CantidadRechazos: Integer;
    MotivoSalida: String;
    CanceladoPorUsuario: Boolean;

    {Revision 3}
    sComprobante : string;
    CantidadRepetido, IndiceRepetido : integer;
    lstComprobantesRepetidos : TVariantItems;
    sTipoComprobanteFiscal, TipoComprobante : string;  // SS_1147_CQU_20150316
    NumeroComprobanteFiscal : Integer;                 // SS_1147_CQU_20150316
begin
	lstComprobantesRepetidos := TVariantItems.Create(nil);
    
    CantidadBajas := 0;
    CantidadRechazos := 0;
    MotivoSalida := '';
    CanceladoPorUsuario := False;
    FCancelar := False;
    btnPausa.Enabled := True;
    // Preparamos los ClientDataSets para el archivo y los rechazos
    if not cdsArchivo.Active then cdsArchivo.Active := True;
    if not cdsArchivo.IsEmpty then cdsArchivo.EmptyDataSet;
    if not cdsRechazos.Active then cdsRechazos.Active := True;
    if not cdsRechazos.IsEmpty then cdsRechazos.EmptyDataSet;
    // Comenzamos la carga del ClientDataSet que representa al archivo y que no permite
    // ingresar un registro que contenga una nota de cobro que ya exista en �l.
    // De esta forma nos sacamos de encima todos los duplicados sin necesidad de consultar
    // la base de datos.
    try
         try
            // Obtenemos algunas constantes que vamos a necesitar luego s�lo una vez
            CodigoTipoMedioPago := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CANAL_PAGO_PAT()');
            CodigoTipoTarjetaCredito := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_TARJETA_TRANSBANK()');
            CodigoRechazoDuplicado := QueryGetValueInt(DMConnections.BaseCAC,
              'SELECT dbo.ObtenerCodigoEstadoDebito(dbo.CONST_CODIGO_ERROR_COMPROBANTE_REPETIDO(), dbo.CONST_TIPO_MEDIO_PAGO_AUTOMATICO_NINGUNO())');
            // Recorremos la StringList que contiene todo el archivo leido desde disco.
            for i := 0 to FLista.Count - 1 do begin
                FProcesando := True;
                // Parseamos el archivo
                //NumeroComprobante    := StrToInt64(ParseParamByNumber(FLista.strings[i],11,';'));                                 // SS_1147_CQU_20150316
                if FCodigoNativa = CODIGO_VS then begin															                    // SS_1147_CQU_20150316
                    sTipoComprobanteFiscal := Copy(ParseParamByNumber(FLista.strings[i],11,';'), 1, 2);				                // SS_1147_CQU_20150316
                    NumeroComprobanteFiscal:= StrToInt64(Copy(ParseParamByNumber(FLista.strings[i],11,';'), 3, 15));	            // SS_1147_CQU_20150316
                    //ObtenerDocumentoInterno(sTipoComprobanteFiscal, NumeroComprobanteFiscal, TipoComprobante, NumeroComprobante); // SS_1147_CQU_20150511  // SS_1147_CQU_20150316
                    case StrToInt(sTipoComprobanteFiscal) of                                                                    // SS_1147_CQU_20150511
                      33 : TipoComprobante := TC_FACTURA_AFECTA;                                                                // SS_1147_CQU_20150511
                      34 : TipoComprobante := TC_FACTURA_EXENTA;                                                                // SS_1147_CQU_20150511
                      39 : TipoComprobante := TC_BOLETA_AFECTA;                                                                 // SS_1147_CQU_20150511
                      41 : TipoComprobante := TC_BOLETA_EXENTA;                                                                 // SS_1147_CQU_20150511
                    end;                                                                                                        // SS_1147_CQU_20150511
                    NumeroComprobante   := NumeroComprobanteFiscal;                                                             // SS_1147_CQU_20150511
                //end else NumeroComprobante    := StrToInt64(ParseParamByNumber(FLista.strings[i],11,';'));;                       // SS_1147_CQU_20150511  // SS_1147_CQU_20150316
                end else begin                                                                                                      // SS_1147_CQU_20150511
                    NumeroComprobante    := StrToInt64(ParseParamByNumber(FLista.strings[i],11,';'));;                              // SS_1147_CQU_20150511
                    TipoComprobante := 'NK';                                                                                        // SS_1147_CQU_20150511
                end;                                                                                                                // SS_1147_CQU_20150511
                NumeroConvenio      := ParseParamByNumber(FLista.strings[i],9,';');
                //if ObtenerCodigoConcesionariaNativa=CODIGO_VS then                            // SS_1147_CQU_20150316  //SS_1147Q_NDR_20141202[??]
                //if FCodigoNativa = CODIGO_VS then                                             // SS_1147_CQU_20150511  // SS_1147_CQU_20150316
                //begin                                                                         // SS_1147_CQU_20150511  //SS_1147Q_NDR_20141202[??]
                //  NumeroConvenio:=Copy(NumeroConvenio,6,12);                                  // SS_1147_CQU_20150511  //SS_1147Q_NDR_20141202[??]
                //end;                                                                          // SS_1147_CQU_20150511  //SS_1147Q_NDR_20141202[??]
                NumeroTarjeta       := ParseParamByNumber(FLista.strings[i],3,';');
                FechaExpiracion     := ParseParamByNumber(FLista.strings[i],4,';');
                CodigoAutorizacion  := ParseParamByNumber(FLista.strings[i],12,';');
                MontoTransaccion    := StrToInt64(ParseParamByNumber(FLista.strings[i],2,';'));
                CodigoRespuesta     := ParseParamByNumber(FLista.strings[i],13,';');
                FechaProceso        := ObtenerFechaProceso(ParseParamByNumber(FLista.strings[i], 17,';'));
                GlosaRespuesta      := ParseParamByNumber(FLista.strings[i],14,';');
                //Calculamos si m�s tarde tambi�n tenemos que ejecutar el proceso de baja definitiva
                EsBajaDefinitiva    := (QueryGetValue(DMConnections.BaseCAC, Format(SQL_BAJA_DEFINITIVA,
                  [CodigoTipoMedioPago, CodigoTipoTarjetaCredito, QuotedStr(CodigoRespuesta)])) = 'True');
                if EsBajaDefinitiva then Inc(CantidadBajas);
                //  Verificamos si existe este comprobante en el archivo
                if cdsArchivo.FindKey([NumeroComprobante]) then begin
                    // Si encontramos el comprobante detectamos un repetido, lo registramos como tal
                    Inc(CantidadRechazos);

                    {Revision 3 : usaremos la lista para ver cuantas veces est� repetido el mismo comprobante}
                    sComprobante := IntToStr(NumeroComprobante);
                    IndiceRepetido := lstComprobantesRepetidos.IndexOf(sComprobante);
                    if IndiceRepetido >= 0 then begin
                        {dado que ya estaba contado como repetido, tomamos el contador anterior y le sumamos uno}
                        CantidadRepetido := lstComprobantesRepetidos[IndiceRepetido].Value;
                        Inc(CantidadRepetido);
                        lstComprobantesRepetidos[IndiceRepetido].Value := CantidadRepetido;

                    end
                    else begin    { si no estaba contado como repetido, lo agregamos}
                        CantidadRepetido := 1;
                        lstComprobantesRepetidos.Add(sComprobante, CantidadRepetido);
                    end;

                   {Revision 3 if not AgregarPagoRechazado(NumeroComprobante, FCodigoOperacion, CodigoRechazoDuplicado, Now, CantidadRechazos, NumeroConvenio, MSG_MOTIVO_DUPLICADO, CodigoRespuesta, MontoTransaccion, Error) then begin}
                   //if not AgregarPagoRechazado(NumeroComprobante, FCodigoOperacion, CodigoRechazoDuplicado, Now, CantidadRepetido, NumeroConvenio, MSG_MOTIVO_DUPLICADO, CodigoRespuesta, MontoTransaccion, Error) then begin                 // SS_1147_CQU_20150511
                   if not AgregarPagoRechazado(NumeroComprobante, FCodigoOperacion, TipoComprobante, CodigoRechazoDuplicado, Now, CantidadRepetido, NumeroConvenio, MSG_MOTIVO_DUPLICADO, CodigoRespuesta, MontoTransaccion, Error) then begin  // SS_1147_CQU_20150511
                        MotivoSalida := 'Error grave al crear un pago rechazado por duplicado en el archivo � error de parseo' +  CRLF + Error;
                        Exit;
                   end;
                end else begin
                    // Regio!, no lo encontramos, lo agregamos a la lista para procesar los pagos!
                    cdsArchivo.AppendRecord([NumeroComprobante, NumeroConvenio, NumeroTarjeta,
                      FechaExpiracion, CodigoAutorizacion, MontoTransaccion, CodigoRespuesta,
                      //FechaProceso, GlosaRespuesta, EsBajaDefinitiva, False, False, False, FCodigoOperacion, '']);                // SS_1147_CQU_20150511
                      FechaProceso, GlosaRespuesta, EsBajaDefinitiva, False, False, False, FCodigoOperacion, '', TipoComprobante]); // SS_1147_CQU_20150511
                      //Agregamos el detalle
                      //if not AgregarRendicionesInterfases(FCodigoOperacion, NumeroComprobante, NumeroConvenio, MontoTransaccion, NumeroComprobante, CodigoRespuesta, Error) then begin                // SS_1147_CQU_20150511
                      if not AgregarRendicionesInterfases(FCodigoOperacion, NumeroComprobante, NumeroConvenio, MontoTransaccion, NumeroComprobante, CodigoRespuesta, Error, TipoComprobante) then begin // SS_1147_CQU_20150511
                          MotivoSalida := 'Error registrando una rendici�n en el proceso de carga del archivo en memoria' + CRLF + Error;
                          Exit;
                      end;
                end;
                // Mostramos el progreso por pantalla
                Mostrar(Format(MSG_CARGANDO_ARCHIVO, [i + 1, FLista.Count, CantidadBajas, CantidadRechazos]));
                 // Nos pidieron pausar el proceso?
                if FPausa then DoPausa;
                // Nos pidieron Cancelar el proceso porque apretaron Escape?
                if FCancelar then begin
                    CanceladoPorUsuario := True;
                    Result := False;
                    Exit;
                end else begin
                    Result := True;
                end;
            end;

        except
            on e: Exception do begin
                MotivoSalida := e.Message;
            end;
        end;
    finally
        // La raaz�n por la cual se apaga el informe de "Warnings" del compilador ac� es debido a que
        // usar la variable del for fuera del lazo provoca valores incorrectos para la variable salvo que se salga
        // del for con Exit. Como esto es lo que se hace el Warning no me interesa verlo.
        // Sacar esto si se quiere ver alg�n otro temilla del c�digo.
         (*$WARNINGS OFF*)
        if MotivoSalida <> '' then begin
            // Algo sali� mal, mostramos el error.
            MsgBoxErr(Format(MSG_ERROR_LINEA, [i + 1, FLista.Strings[i]]), MotivoSalida, Caption, MB_ICONERROR);
        end else if CanceladoPorUsuario then begin
            {$MESSAGE HINT 'Ver si hace falta loguear esta situaci�n en la base de alguna manera? '}
            MsgBox('Proceso Cancelado Por el Usuario ' + UsuarioSistema,  Caption, MB_ICONINFORMATION);
        end;
        FProcesando := False;
        btnPausa.Enabled := False;

        {revision 3}
        lstComprobantesRepetidos.Free;
    end;
end;

function TFConvertirNominaATabla.CargarTablaTemporal: boolean;
begin
    Result := False;
    lblInfo.Caption := 'Cargando informaci�n en tabla temporal...';
    try
        spCargarTablaRendicionesTBK.ExecProc;
        Result := True;
    except
        on e: Exception do begin
            MsgBox('Se ha producido un error cargando tabla temporal ' + e.Message,  Caption, MB_ICONINFORMATION);
        end;
    end;
    lblInfo.Caption := '';

end;

function TFConvertirNominaATabla.Descartar(const CodigoInterface: Int64; NumeroConvenio:string; NumeroComprobante: Int64;
    TipoComprobante : string;   // SS_1147_CQU_20150511
    CodigoRespuesta, GlosaRespuesta: string; FechaProceso: TDateTime; Importe: Int64;
    out CodigoEstadoDebito: Integer; out MotivoDescarte, DescriError: string): Boolean;
begin
    // Esta funci�n llama a un SP que nos informa si esta pago debe ser rechazado
    // Por cualquiera de las razones posibles para luego registralo como pago rechazado
    // indic�ndonos adem�s la raz�n del rechazo.
    Result := False;
    spValidarRendiciones.Close;
    try
        spValidarRendiciones.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoInterface;
        spValidarRendiciones.Parameters.ParamByName('@NumeroConvenio').Value := NumeroConvenio;
        spValidarRendiciones.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
        spValidarRendiciones.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;   // SS_1147_CQU_20150511
        spValidarRendiciones.Parameters.ParamByName('@CodigoRespuesta').Value := CodigoRespuesta;
        spValidarRendiciones.Parameters.ParamByName('@GlosaRespuesta').Value := GlosaRespuesta;
        spValidarRendiciones.Parameters.ParamByName('@FechaProceso').Value := Now;
        spValidarRendiciones.Parameters.ParamByName('@Importe').Value := Importe;
        spValidarRendiciones.Parameters.ParamByName('@CodigoEstadoDebito').Value := Null;
        spValidarRendiciones.Parameters.ParamByName('@DescripcionError').Value := Null;
        spValidarRendiciones.ExecProc;
        CodigoEstadoDebito := spValidarRendiciones.Parameters.ParamByName('@CodigoEstadoDebito').Value;
        MotivoDescarte := Trim(spValidarRendiciones.Parameters.ParamByName('@DescripcionError').Value);
        // Listongo!
        Result := (MotivoDescarte <> '');
    except
        on e: Exception do begin
            DescriError := e.Message;
        end;
    end;
end;

procedure TFConvertirNominaATabla.DoPausa;
begin
    if MsgBox('�Desea pausar el proceso?' + CRLF + 'A continuaci�n podr� retomarlo presionando el bot�n "Continuar"', Caption,
      MB_ICONQUESTION or MB_YESNO) = IDNO then begin
        FPausa := False;
        btnPausa.Caption := 'Pausar';
        Exit;
    end;
    while FPausa do begin
        Mostrar('Proceso en pausa, presione "Continuar" para retomar el procesamiento');
        Application.ProcessMessages;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: impido salir si esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirNominaATabla.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando
    CanClose := not FProcesando;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirNominaATabla.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	  Action := caFree;
end;

procedure TFConvertirNominaATabla.btnCancelarClick(Sender: TObject);
begin
    if MsgBox('�Desea cancelar el proceso de registraci�n de rendiones TransBank?', Caption,
      MB_ICONQUESTION or MB_YESNO) = IDYES then begin
        FCancelar := True;
        btnCancelar.Enabled := False;
    end;
end;

procedure TFConvertirNominaATabla.btnReprocesarClick(Sender: TObject);
begin
    RegistrarRendiciones;
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarLog
  Author:    lgisuk
  Date Created: 01/07/2005
  Description: Actualizo el log al finalizar
  Parameters: CantidadErrores:integer
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFConvertirNominaATabla.ActualizarLog( CantidadErrores : integer ) : Boolean;
resourcestring
    MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
const
    STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
var
    DescError : string;
begin
    try
        Result := ActualizarLogOperacionesInterfaseAlFinal
                        (DMConnections.BaseCAC, FCodigoOperacion, STR_ERRORES + IntToStr(CantidadErrores),DescError);
    except
        on e: Exception do begin
           MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
           Result := False;
        end;
    end;
end;




initialization
    FLista   := TStringList.Create;
finalization
    FreeAndNil(Flista);
end.

