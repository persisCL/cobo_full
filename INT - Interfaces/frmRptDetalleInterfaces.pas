unit frmRptDetalleInterfaces;

interface

uses
  DMConnection,
  Util, UtilDB,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppProd, ppClass, ppReport, UtilRB, ppComm, ppRelatv, ppDB, ppDBPipe,
  DB, DBClient, ppParameter, ppBands, ppCtrls, ppPrnabl, ppStrtch, ppSubRpt,
  ppCache, ADODB, RBSetup, UtilProc, ConstParametrosGenerales, jpeg, ppVar;

type
  TRptDetalleInterfacesForm = class(TForm)
    spObtenerEncabezadoReporteInterfaces: TADOStoredProc;
    dsObtenerEncabezadoReporteInterfaces: TDataSource;
    dbppObtenerReporteInterfaces_Rendiciones: TppDBPipeline;
    ppRptInterfacesRendiciones: TppReport;
    ppParameterList1: TppParameterList;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    spObtenerReporteInterfaces: TADOStoredProc;
    dsObtenerReporteInterfaces: TDataSource;
    rbiReporte: TRBInterface;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    dbppObtenerReporteEncabezadoInterfaces: TppDBPipeline;
    ppImage1: TppImage;
    ppDBText1: TppDBText;
    ppLabel1: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppSystemVariable2: TppSystemVariable;
    ppSystemVariable3: TppSystemVariable;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppSystemVariable4: TppSystemVariable;
    ppSystemVariable5: TppSystemVariable;
    ppLabel6: TppLabel;
    ppDBText2: TppDBText;
    ppLabel7: TppLabel;
    ppDBText3: TppDBText;
    ppLabel8: TppLabel;
    ppDBText4: TppDBText;
    ppLabel9: TppLabel;
    ppDBText5: TppDBText;
    ppLabel10: TppLabel;
    ppDBText6: TppDBText;
    ppLabel11: TppLabel;
    ppDBText7: TppDBText;
    ppLabel12: TppLabel;
    ppDBText8: TppDBText;
    ppLabel13: TppLabel;
    ppDBText9: TppDBText;
    ppLabel14: TppLabel;
    ppDBText10: TppDBText;
    ppLabel15: TppLabel;
    ppDBText11: TppDBText;
    ppLabel16: TppLabel;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppLabel17: TppLabel;
    ppDBText14: TppDBText;
    ppLabel18: TppLabel;
    ppDBText15: TppDBText;
    ppLabel19: TppLabel;
    ppDBText16: TppDBText;
    ppLabel20: TppLabel;
    ppDBText17: TppDBText;
    ppLine1: TppLine;
    dbppObtenerReporteInterfaces_DebitosPAC: TppDBPipeline;
    ppRptInterfacesDebitosPAC: TppReport;
    ppHeaderBand2: TppHeaderBand;
    ppImage2: TppImage;
    ppDBText18: TppDBText;
    ppLabel21: TppLabel;
    ppSystemVariable6: TppSystemVariable;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppSystemVariable7: TppSystemVariable;
    ppSystemVariable8: TppSystemVariable;
    ppLabel24: TppLabel;
    ppDBText19: TppDBText;
    ppLabel25: TppLabel;
    ppDBText20: TppDBText;
    ppLabel26: TppLabel;
    ppDBText21: TppDBText;
    ppLabel27: TppLabel;
    ppDBText22: TppDBText;
    ppLabel28: TppLabel;
    ppDBText23: TppDBText;
    ppLabel29: TppLabel;
    ppDBText24: TppDBText;
    ppLabel30: TppLabel;
    ppDBText25: TppDBText;
    ppDetailBand3: TppDetailBand;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppLine2: TppLine;
    ppDetailBand4: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppFooterBand2: TppFooterBand;
    ppLabel39: TppLabel;
    ppLabel40: TppLabel;
    ppSystemVariable9: TppSystemVariable;
    ppSystemVariable10: TppSystemVariable;
    ppParameterList2: TppParameterList;
    pfldBanco: TppField;
    pfldEmpresa: TppField;
    pfldConvenio: TppField;
    pfldNumeroConvenio: TppField;
    pfldComprobante: TppField;
    pfldFacturacion: TppField;
    pfldVencimiento: TppField;
    pfldMonto: TppField;
    ppLabel31: TppLabel;
    ppDBText26: TppDBText;
    ppLabel32: TppLabel;
    ppDBText27: TppDBText;
    ppLabel33: TppLabel;
    ppDBText28: TppDBText;
    ppLabel34: TppLabel;
    ppDBText29: TppDBText;
    ppLabel35: TppLabel;
    ppDBText30: TppDBText;
    ppLabel36: TppLabel;
    ppDBText31: TppDBText;
    ppLabel37: TppLabel;
    ppDBText32: TppDBText;
    ppLabel38: TppLabel;
    ppDBText33: TppDBText;
    dbppObtenerReporteInterfaces_DebitosPAT: TppDBPipeline;
    ppRptInterfacesDebitosPAT: TppReport;
    ppHeaderBand3: TppHeaderBand;
    ppImage3: TppImage;
    ppDBText34: TppDBText;
    ppLabel41: TppLabel;
    ppSystemVariable11: TppSystemVariable;
    ppLabel42: TppLabel;
    ppLabel43: TppLabel;
    ppSystemVariable12: TppSystemVariable;
    ppSystemVariable13: TppSystemVariable;
    ppLabel44: TppLabel;
    ppDBText35: TppDBText;
    ppLabel45: TppLabel;
    ppDBText36: TppDBText;
    ppLabel46: TppLabel;
    ppDBText37: TppDBText;
    ppLabel47: TppLabel;
    ppDBText38: TppDBText;
    ppLabel48: TppLabel;
    ppDBText39: TppDBText;
    ppLabel49: TppLabel;
    ppDBText40: TppDBText;
    ppLabel50: TppLabel;
    ppDBText41: TppDBText;
    ppDetailBand5: TppDetailBand;
    ppSubReport3: TppSubReport;
    ppChildReport3: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppLabel51: TppLabel;
    ppLabel52: TppLabel;
    ppLabel53: TppLabel;
    ppLabel54: TppLabel;
    ppLabel55: TppLabel;
    ppLabel56: TppLabel;
    ppLabel57: TppLabel;
    ppLabel58: TppLabel;
    ppLine3: TppLine;
    ppDetailBand6: TppDetailBand;
    ppDBText42: TppDBText;
    ppDBText43: TppDBText;
    ppDBText44: TppDBText;
    ppDBText45: TppDBText;
    ppDBText47: TppDBText;
    ppDBText48: TppDBText;
    ppDBText49: TppDBText;
    ppDBText50: TppDBText;
    ppSummaryBand3: TppSummaryBand;
    ppFooterBand3: TppFooterBand;
    ppLabel59: TppLabel;
    ppLabel60: TppLabel;
    ppSystemVariable14: TppSystemVariable;
    ppSystemVariable15: TppSystemVariable;
    ppParameterList3: TppParameterList;
    dbppObtenerReporteInterfaces_DebitosSVP: TppDBPipeline;
    ppRptInterfacesDebitosSVP: TppReport;
    ppHeaderBand4: TppHeaderBand;
    ppImage4: TppImage;
    ppDBText51: TppDBText;
    ppLabel61: TppLabel;
    ppSystemVariable16: TppSystemVariable;
    ppLabel62: TppLabel;
    ppLabel63: TppLabel;
    ppSystemVariable17: TppSystemVariable;
    ppSystemVariable18: TppSystemVariable;
    ppLabel64: TppLabel;
    ppDBText52: TppDBText;
    ppLabel65: TppLabel;
    ppDBText53: TppDBText;
    ppLabel66: TppLabel;
    ppDBText54: TppDBText;
    ppLabel67: TppLabel;
    ppDBText55: TppDBText;
    ppLabel68: TppLabel;
    ppDBText56: TppDBText;
    ppLabel69: TppLabel;
    ppDBText57: TppDBText;
    ppLabel70: TppLabel;
    ppDBText58: TppDBText;
    ppDetailBand7: TppDetailBand;
    ppSubReport4: TppSubReport;
    ppChildReport4: TppChildReport;
    ppTitleBand4: TppTitleBand;
    ppLabel71: TppLabel;
    ppLabel72: TppLabel;
    ppLabel73: TppLabel;
    ppLabel74: TppLabel;
    ppLabel75: TppLabel;
    ppLabel76: TppLabel;
    ppLabel77: TppLabel;
    ppLine4: TppLine;
    ppDetailBand8: TppDetailBand;
    ppDBText59: TppDBText;
    ppDBText60: TppDBText;
    ppDBText61: TppDBText;
    ppDBText62: TppDBText;
    ppDBText64: TppDBText;
    ppDBText65: TppDBText;
    ppDBText66: TppDBText;
    ppSummaryBand4: TppSummaryBand;
    ppFooterBand4: TppFooterBand;
    ppLabel79: TppLabel;
    ppLabel80: TppLabel;
    ppSystemVariable19: TppSystemVariable;
    ppSystemVariable20: TppSystemVariable;
    ppParameterList4: TppParameterList;
    procedure rbiReporteExecute(Sender: TObject;
      var Cancelled: Boolean);
  private
    { Private declarations }       
    FCodigoOperacionInterfase: Integer;
  public
    { Public declarations }
    function Inicializar(CodigoOperacionInterfase, CodigoModulo: Integer): Boolean;
  end;

var
  RptDetalleInterfacesForm: TRptDetalleInterfacesForm;

implementation

{$R *.dfm}

function TRptDetalleInterfacesForm.Inicializar(CodigoOperacionInterfase, CodigoModulo: Integer): Boolean;
resourcestring
    QRY_Modulo_PAC_EnvioDebitos = 'SELECT dbo.CONST_CODIGO_MODULO_GENERACION_NOMINA_BANCOCHILE()';
    QRY_Modulo_PAC_RecepcionRendiciones = 'SELECT dbo.CONST_CODIGO_MODULO_RECEPCION_RENDICIONES_PAC_BANCOCHILE()';
    QRY_Modulo_PAT_EnvioDebitos = 'SELECT dbo.CONST_CODIGO_MODULO_GENERACION_NOMINA_TRANSBANK()';
    QRY_Modulo_PAT_RecepcionRendiciones = 'SELECT dbo.CONST_CODIGO_MODULO_RECEPCION_RENDICIONES_TRANSBANK()';
    QRY_Modulo_Servipag_EnvioDebitos = 'SELECT dbo.CONST_CODIGO_MODULO_GENERACION_NOMINA_SERVIPAG()';
    QRY_Modulo_Servipag_RecepcionRendiciones = 'SELECT dbo.CONST_CODIGO_MODULO_RECEPCION_RENDICIONES_SERVIPAG()';
    ERR_REPORTE_INVALIDO = 'Error obteniendo el reporte a mostrar';
    ERR_REPORTE_DATOS = 'Error obteniendo los datos del reporte';  
    ERR_REPORTE_CONFIG = 'Error obteniendo la configuraci�n del reporte';
var
    RutaLogo: AnsiString;
    Modulo_PAC_EnvioDebitos,
    Modulo_PAC_RecepcionRendiciones,
    Modulo_PAT_EnvioDebitos,
    Modulo_PAT_RecepcionRendiciones,
    Modulo_Servipag_EnvioDebitos,
    Modulo_Servipag_RecepcionRendiciones: Integer;
begin
    Screen.Cursor := crHourGlass;
    Result := False;

    try
        try
            // Se obtienen los c�digos de m�dulo de cada interfaz
            Modulo_PAC_EnvioDebitos := QueryGetValueInt(DMConnections.BaseCAC, QRY_Modulo_PAC_EnvioDebitos);
            Modulo_PAC_RecepcionRendiciones := QueryGetValueInt(DMConnections.BaseCAC, QRY_Modulo_PAC_RecepcionRendiciones);
            Modulo_PAT_EnvioDebitos := QueryGetValueInt(DMConnections.BaseCAC, QRY_Modulo_PAT_EnvioDebitos);
            Modulo_PAT_RecepcionRendiciones := QueryGetValueInt(DMConnections.BaseCAC, QRY_Modulo_PAT_RecepcionRendiciones);
            Modulo_Servipag_EnvioDebitos := QueryGetValueInt(DMConnections.BaseCAC, QRY_Modulo_Servipag_EnvioDebitos);
            Modulo_Servipag_RecepcionRendiciones := QueryGetValueInt(DMConnections.BaseCAC, QRY_Modulo_Servipag_RecepcionRendiciones);
        except
            on E: Exception do begin
                MsgBoxErr(ERR_REPORTE_CONFIG, E.message, Self.Caption, MB_ICONSTOP);
                Exit;
            end;
        end;

        FCodigoOperacionInterfase := CodigoOperacionInterfase;

        // Se valida el m�dulo de la interfaz consultada, para utilizar el pipeline y dise�o correspondiente

        if (CodigoModulo = Modulo_PAC_RecepcionRendiciones)
        or (CodigoModulo = Modulo_PAT_RecepcionRendiciones)
        or (CodigoModulo = Modulo_Servipag_RecepcionRendiciones) then begin
            // Si es una interfaz de rendiciones, se prepara dicho reporte
            dbppObtenerReporteInterfaces_Rendiciones.DataSource := dsObtenerReporteInterfaces;
            rbiReporte.Report := ppRptInterfacesRendiciones;
        end;

        if CodigoModulo = Modulo_PAC_EnvioDebitos then begin
            // Si es una interfaz de d�bitos PAC, se prepara dicho reporte
            dbppObtenerReporteInterfaces_DebitosPAC.DataSource := dsObtenerReporteInterfaces;
            rbiReporte.Report := ppRptInterfacesDebitosPAC;
        end;

        if CodigoModulo = Modulo_PAT_EnvioDebitos then begin
            // Si es una interfaz de d�bitos PAT, se prepara dicho reporte
            dbppObtenerReporteInterfaces_DebitosPAT.DataSource := dsObtenerReporteInterfaces;
            rbiReporte.Report := ppRptInterfacesDebitosPAT;
        end;

        if CodigoModulo = Modulo_Servipag_EnvioDebitos then begin
            // Si es una interfaz de d�bitos Servipag, se prepara dicho reporte
            dbppObtenerReporteInterfaces_DebitosSVP.DataSource := dsObtenerReporteInterfaces;
            rbiReporte.Report := ppRptInterfacesDebitosSVP;
        end;

        try
            // Se obtiene el logo del encabezado
            ObtenerParametroGeneral(DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);
            ppImage1.Picture.LoadFromFile(Trim(RutaLogo));

            // Se obtienen los datos del encabezado
            spObtenerEncabezadoReporteInterfaces.Close;
            spObtenerEncabezadoReporteInterfaces.Parameters.Refresh;
            spObtenerEncabezadoReporteInterfaces.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
            spObtenerEncabezadoReporteInterfaces.ExecProc;

            if spObtenerEncabezadoReporteInterfaces.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spObtenerEncabezadoReporteInterfaces.Parameters.ParamByName('@ErrorDescription').Value);

            // Se obtienen los registros del reporte. La estructura var�a seg�n m�dulo
            spObtenerReporteInterfaces.Close;
            spObtenerReporteInterfaces.Parameters.Refresh;
            spObtenerReporteInterfaces.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
            spObtenerReporteInterfaces.ExecProc;

            if spObtenerReporteInterfaces.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spObtenerReporteInterfaces.Parameters.ParamByName('@ErrorDescription').Value);
        except
            on E: Exception do begin
                MsgBoxErr(ERR_REPORTE_DATOS, E.message, Self.Caption, MB_ICONSTOP);
                Exit;
            end;
        end;

        try
            // Se ejecuta el reporte
            rbiReporte.Execute;
        except
            on E: Exception do begin
                MsgBoxErr(ERR_REPORTE_INVALIDO, E.message, Self.Caption, MB_ICONSTOP);
                Exit;
            end;
        end;

        Result := True;

    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRptDetalleInterfacesForm.rbiReporteExecute(
  Sender: TObject; var Cancelled: Boolean);
var
    Config: TRBConfig;
begin
    Config := rbiReporte.GetConfig;
    if Config.BinName <> '' then rbiReporte.Report.PrinterSetup.BinName := Config.BinName;
end;

end.
