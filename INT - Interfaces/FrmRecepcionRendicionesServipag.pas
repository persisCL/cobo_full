{------------------------------------------------------------------------------
 File Name: FrmRecepcionRendicionesServipag.pas
 Author:    lgisuk
 Date Created: 20/10/2005
 Language: ES-AR
 Description: M�dulo de la interface Servipag - Recepci�n de Rendiciones

 Revisi�n: 1
 Author: nefernandez
 Date: 04/07/2007
 Description: Se agreg� la funcionalidad para la Recepci�n de Rendiciones de
 Pagos Unificados

 Revision :2
     Author : vpaszkowicz
     Date : 20/03/2008
     Description : Agrego la cantidad de lineas del archivo y la suma de los
     montos recibidos en el log de operaciones.

 Revision 3
 Author:mbecerra
 Date: 18-Junio-2008
 Description:
                1.- Se agreg� la funcionalidad de Rendiciones ServiPag Por Rut
                FCodigoServicio = '08'

                2.- Se elimin� la tecla ESCAPE del Form y se agreg� el bot�n Cancelar
                pues es m�s intuitivo.

                3.- Se agreg� la variable privada FArchivo, que contiene el nombre del
                archivo a procesar. Adem�s se corrig� la llamada a las funciones
                		EsArchivoValidoCajasyPortal,
                        EsArchivoValidoExpres,
 						EsArchivoValidoCajasyPortalUnificados,
 						EsArchivoValidoExpressUnificados,
 						EsArchivoValidoServipagPorRut

                pues todas recib�an como par�metro el Path Completo. Y en el Path
                es posible que la validaci�n se cumpla, aun cuando el nombre del
                archivo no cumpla.

 Firma          :   SS_1060_CQU_20120731
 Descripcion    :   En la lectura de registro, actualmente en la posici�n 50 est� leyendo un espacio, cuando vienen 2.
                    Se desplazan una posici�n a partir de la indicada, para ello se crea la variable FPosicionRuaExpress.
                    Esta nueva variable ser� 1 cuando FCodigoServicio = 06 y 0 en caso contrario.

Autor       :   Claudio Quezada
Fecha       :   06-02-2015
Firma       :   SS_1147_CQU_20150316
Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato


Firma       :   SS_1147_MBE_20150413
Description :   Se agrega el tipo de comprobante, ya que la deuda puede originarse por NK, SD o TD impago.
                Adem�s, se corrigen errores de programaci�n

    2015-04-14
                Se agrega el tipo y numero Fiscal, para el registro de errores
                
Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1147_CQU_20150519
Descripcion : Se solicita Homologar los men�es de VS a los de CN, sin embargo, al revisar, �stos est�n bien.
              Lo que puede estar ocurriendo es que la funci�n "ExistePalabra" busque lo similar
              y como hay similitud entre algunos nombres de archivo, entonces, los detecta igual. Se agrega "_"
              Se quitan las Anulaciones para VS ya que son procesadas por otro men�
--------------------------------------------------------------------------------}
unit FrmRecepcionRendicionesServipag;

interface

uses
    //Recepcion de Rendiciones
  	DMConnection,                    //Coneccion a base de datos OP_CAC
    Util,                            //Stringtofile,padl..
    UtilProc,                        //Mensajes
    ConstParametrosGenerales,        //Obtengo Valores de la Tabla Parametros Generales
    ComunesInterfaces,               //Procedimientos y Funciones Comunes a todos los formularios
    Peatypes,                        //Constantes
    PeaProcs,                        //NowBase
    FrmRptErroresSintaxis,           //Reporte de Errores de sintaxis
    FrmRptRecepcionComprobantesServipag, //Reporte del proceso
    //General
  	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  	Dialogs, StdCtrls, Buttons, DB, ADODB, ExtCtrls, Grids, DBGrids,
  	DPSControls, ListBoxEx, DBListEx, ComCtrls, StrUtils, UtilRB,
    ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd, ppReport, ppDB,
    ppComm, ppRelatv, ppTxPipe, UtilDB, ppDBPipe, ppModule, raCodMod;

type

  //Estructura donde voy a almacenar cada Rendicion
  TRendicion = record
  	Fecha : TDateTime;
    EPS : string;
    Sucursal : integer;
    Terminal : string;
    Correlativo : INT64;         // en el archivo es string[10]
    FechaContable : TDateTime;
    CodigoServicio : string;
    TipoOperacion : string;
    IndicadorContable : string;
    Monto : INT64;
    MedioPago : string;
    CodigoBancoCheque : integer;
    CuentaCheque : string;
    SerieCheque : integer;
    PlazaCheque : integer;
    TipoTarjeta : string;
    MarcaTarjeta : string;
    NumeroTarjeta : string;
    ExpiracionTarjeta : string;
    TipoCuotas : string;
    NumeroCuotas : integer;
    Autorizacion : string;
    TipoComprobante : string;                   // SS_1147_MBE_20150413
//    NotaCobro : integer;                      // SS_1147_MBE_20150413
    NumeroComprobante : Int64;                  // SS_1147_MBE_20150413
    Identificador : string;
  end;

  TFRecepcionRendicionesServipag = class(TForm)
    OpenDialog: TOpenDialog;
    pnlOrigen: TPanel;
    btnAbrirArchivo: TSpeedButton;
    txtOrigen: TEdit;
    lblOrigen: TLabel;
    btnProcesar: TButton;
    btnSalir: TButton;
    Bevel: TBevel;
    lblMensaje: TLabel;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    SpObtenerResumenComprobantesRecibidos: TADOStoredProc;
    SpObtenerResumenComprobantesRecibidosMotivo: TStringField;
    SpObtenerResumenComprobantesRecibidosCantidad: TIntegerField;
    SpObtenerResumenComprobantesRecibidosMonto: TStringField;
    SPProcesarRendicionesServipag: TADOStoredProc;
    btnCancelar: TButton;
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtOrigenChange(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoOperacion : Integer;
    FErrorGrave : Boolean;      //Indica que se produjo un error
    FCancelar : Boolean;        //Indica si ha sido cancelado por el usuario
    FTotalRegistros : Integer;
    //FCantidadControl : Real;
    FCantidadControl: integer;
    //FSumaControl : Real;
    FSumaControl: int64;
    FSumaReal : Real;
    FServipag_Directorio_Rendiciones : AnsiString;
    FServipag_Directorio_Procesados : AnsiString;
    FServipag_Directorio_Errores : AnsiString;
    // Revision 1: Se utiliza el c�digo del servicion ('02', '03', '04'), en vez de indicar si es express o no
    //FExpres : Boolean;
    FCodigoServicio : AnsiString;
    FArchivoAProcesar : AnsiString;
    FCodigoNativa : Integer;        // SS_1147_CQU_20150316
    FCodigoServicioVS : AnsiString; // SS_1147_CQU_20150316
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  	Function   Abrir : Boolean;
    Function   Control : Boolean;
    function   ConfirmaCantidades: Boolean;
    function   ParseLineToRecord(Linea : String; var Rendicion : TRendicion) : Boolean;
    Function   AnalizarRendicionesTXT : Boolean;
    function   RegistrarOperacion : Boolean;
    Function   ActualizarRendiciones : Boolean;
    Function   GenerarReporteErroresSintaxis : Boolean;
    Function   GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
    procedure GrabarErrores;                                                        // SS_1147_MBE_20150413
  public
    { Public declarations }
  	//Function  Inicializar(txtCaption : ANSIString; Expres : Boolean) : Boolean;
  	Function  Inicializar(txtCaption : ANSIString; CodigoServicio : String) : Boolean; // Revision 1
  end;

var
  FRecepcionRendicionesServipag : TFRecepcionRendicionesServipag;
  FLista : TStringList;
  FErrores : TStringList;
  FPosicionRuaExpress : Integer;    // SS_1060_CQU_20120731

Const
  RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_SERVIPAG = 67;
  SERVICIO_CAJA = '02';
  SERVICIO_EXPRES = '03';
  SERVICIO_UNCAJA = '04';
  SERVICIO_UNEXPR = '06';
  SERVICIO_PORRUT = '08';

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean

  Revisi�n: 1
  Author: nefernandez
  Date: 04/07/2007
  Description: Se usaba un parametro para indicar si la Rendici�n es 'Expres' o no.
  Ahora se pasa un par�metro que indica el servicio: '02': Caja/Portal, '03': Expres, '04': UnCaja/UnPort
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesServipag.Inicializar(txtCaption, CodigoServicio : AnsiString) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 20/10/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        SERVIPAG_DIRECTORIO_RENDICIONES        = 'Servipag_Directorio_Rendiciones';
        SERVIPAG_DIRECTORIO_ERRORES            = 'Servipag_Directorio_Errores';
        SERVIPAG_DIRECTORIO_PROCESADOS         = 'Servipag_Directorio_Procesados';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la Concesionaria Nativa                  // SS_1147_CQU_20150316
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150316

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, SERVIPAG_DIRECTORIO_RENDICIONES , FServipag_Directorio_Rendiciones) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SERVIPAG_DIRECTORIO_RENDICIONES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FServipag_Directorio_Rendiciones := GoodDir(FServipag_Directorio_Rendiciones);
                if  not DirectoryExists(FServipag_Directorio_Rendiciones) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FServipag_Directorio_Rendiciones;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, SERVIPAG_DIRECTORIO_ERRORES , FServipag_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SERVIPAG_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FServipag_Directorio_Errores := GoodDir(FServipag_Directorio_Errores);
                if  not DirectoryExists(FServipag_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FServipag_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                if not ObtenerParametroGeneral(DMConnections.BaseCAC,SERVIPAG_DIRECTORIO_PROCESADOS, FServipag_Directorio_Procesados) then begin
                	DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SERVIPAG_DIRECTORIO_PROCESADOS;
                    Result := False;
                    Exit;
                end;

                 //Verifico que sea v�lido
                FServipag_Directorio_Procesados := GoodDir(FServipag_Directorio_Procesados);
                if  not DirectoryExists(FServipag_Directorio_Procesados) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FServipag_Directorio_Procesados;
                    Result := False;
                    Exit;
                end;

            except
                on E : Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, E.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Centro el formulario
  	CenterForm(Self);
  	try
  		DMConnections.BaseCAC.Connected := True;
  		Result := DMConnections.BaseCAC.Connected and
                           VerificarParametrosGenerales;


  	except
        on E : Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
  	end;
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Caption := AnsiReplaceStr(txtCaption, '&', ''); //titulo
  	btnProcesar.Enabled := False; 					//Procesar
    btnCancelar.Enabled := False;
  	lblmensaje.Caption := '';     					//limpio el indicador
    PnlAvance.visible := False;    					//Oculto el Panel de Avance
    FCodigoOperacion := 0;         					//inicializo codigo de operacion
    // Revision 1:
    //FExpres := Expres;
    FCodigoServicio := CodigoServicio;
    FPosicionRuaExpress := 0; //SS_1060_CQU_20120731
end;


{----------------------------------------------------------------------------- Inicio    SS_1147_MBE_20150413
            GrabarErrores

Author      :   mbecerra
Date        :   15-Abril-2015
Description :   Grabar los errores encontrados en el procesmiento de la interfaz

-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesServipag.GrabarErrores;
var
    i : Integer;
begin
    for i := 0 to FErrores.Count - 1 do begin
        AgregarErrorInterfase( DMConnections.BaseCAC, FCodigoOperacion, FErrores[i], -1);
    end;

end;                                                                            // Fin      SS_1147_MBE_20150413


{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None

  Revisi�n: 1
  Author: nefernandez
  Date: 04/07/2007
  Description: Se ajusta el mensaje para indicar el prefijo del archivo mediante del c�digo del servicio
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesServipag.ImgAyudaClick(Sender: TObject);
Var MSG_RENDICION : String;
begin
    //si esta procesando sale
    if PnlAvance.visible = True then Exit;

    //Obtengo el mensaje
    MSG_RENDICION      := ' ' + CRLF +
                          'El Archivo de Rendiciones' + CRLF +
                          'es enviado por SERVIPAG al ESTABLECIMIENTO' + CRLF +
                          'y tiene por objeto dar a conocer al Establecimiento, ' + CRLF +
                          'los comprobantes que fueron cobrados en SERVIPAG.' + CRLF +
                          ' ' + CRLF +
                          // Revision 1
                          //'Nombre del Archivo: '+ IIF(FExpres = False,'02','03') +'AAAAMMDD.txt' + CRLF +
                          'Nombre del Archivo: '+ FCodigoServicio +'AAAAMMDD.txt' + CRLF + // Revision 1
                          ' ' + CRLF +
                          'Se utiliza para registrar los pagos de los' + CRLF +
                          'comprobantes aceptados en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';

    //Muestro el                               mensaje
    MsgBoxBalloon(MSG_RENDICION, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: permito hacer click en la ayuda solo si no esta procesando
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesServipag.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.Visible = True then ImgAyuda.Cursor := crDefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarArchivoBTNClick
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: Busco el archivo a procesar
  Parameters: Sender: TObject
  Return Value: None

  Revisi�n: 1
  Author: nefernandez
  Date: 04/07/2007
  Description: Se setea la descripcion del servicio de acuerdo al codigo (prefijo): '02', '03', '04'

  Revision 2
  Author: mbecerra
  Date: 19-Junio-2008
  Description:
                Se agregan las validaciones para ServipagPorRut
                Adem�s, se comenta el c�digo que inhabilita al boton Cargar archivo,
                pues s�lo debe inhabilitarse una vez que se comience el proceso
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesServipag.btnAbrirArchivoClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: EsArchivoValidoCajasyPortal
      Author:    lgisuk
      Date Created: 11/07/2006
      Description: Verifica si es un Archivo Valido para Esta Pantalla
      Parameters:
      Return Value: None

      Revisi�n: 1
      Author: nefernandez
      Date: 04/07/2007
      Description: Se compara si el codigo del servicio coincide con la pantalla que va a realizar el proceso
    -----------------------------------------------------------------------------}
    Function EsArchivoValidoCajasyPortal(Nombre : AnsiString) : Boolean;
    begin
        // Revision 1
        //Result:= (not(FExpres)) and
        Result:=  (FCodigoServicio = '02') and
                    ExistePalabra(Nombre, '02') and
                        ExistePalabra(uppercase(Nombre), '.TXT');
    end;

    {-----------------------------------------------------------------------------
      Function Name: EsArchivoValidoExpres
      Author:    lgisuk
      Date Created: 11/07/2006
      Description: Verifica si es un Archivo Valido para Esta Pantalla
      Parameters:
      Return Value: None

      Revisi�n: 1
      Author: nefernandez
      Date: 04/07/2007
      Description: Se compara si el codigo del servicio coincide con la pantalla que va a realizar el proceso
    -----------------------------------------------------------------------------}
    Function EsArchivoValidoExpres(Nombre : AnsiString) : Boolean;
    begin
        // Revision 1
        //Result:=  FExpres and
        Result:=  iif(FCodigoServicio = '03', True, False) and
                    ExistePalabra(Nombre, '03') and
                        ExistePalabra(uppercase(Nombre), '.TXT');
    end;

    {-----------------------------------------------------------------------------
      Function Name: EsArchivoValidoCajasyPortalUnificados
      Author: nefernandez
      Date Created: 04/07/2007
      Description: Verifica si es un archivo v�lido para la pantalla de Pagos Caja/Portal Unificados
      Parameters:
      Return Value: None
    -----------------------------------------------------------------------------}
    Function EsArchivoValidoCajasyPortalUnificados(Nombre : AnsiString) : Boolean;
    begin
        Result:=  iif(FCodigoServicio = '04', True, False) and
                    ExistePalabra(Nombre, '04') and
                        ExistePalabra(uppercase(Nombre), '.TXT');
    end;

    {-----------------------------------------------------------------------------
      Function Name: EsArchivoValidoExpressUnificados
      Author: nefernandez
      Date Created: 06/09/2007
      Description: Verifica si es un archivo v�lido para la pantalla de Pagos Express Unificados
      Parameters:
      Return Value: None
    -----------------------------------------------------------------------------}
    Function EsArchivoValidoExpressUnificados(Nombre : AnsiString) : Boolean;
    begin
        Result:=  iif(FCodigoServicio = '06', True, False) and
                    ExistePalabra(Nombre, '06') and
                        ExistePalabra(uppercase(Nombre), '.TXT');
    end;

	{-----------------------------------------------------------------------------
      Function Name: EsArchivoValidoServipagPorRut
      Author: mbecerra
      Date: 19-Junio-2008
      Description: Verifica si es un archivo v�lido para la pantalla de Servipago Por Rut
    -----------------------------------------------------------------------------}
    function EsArchivoValidoServipagPorRut(Nombre : AnsiString ) : boolean;
    begin
        Result := (FCodigoServicio = '08') and (ExistePalabra(Nombre, 'CNR02') or ExistePalabra(Nombre, 'CNR03') ) and
                    ExistePalabra(UpperCase(Nombre), '.TXT');
    end;

	{-----------------------------------------------------------------------------                 // SS_1147_CQU_20150316
      Function Name : EsArchivoValidoVespucio                                                      // SS_1147_CQU_20150316
      Author        : CQuezadaI                                                                    // SS_1147_CQU_20150316
      Date          : 16-Febrero-2015                                                              // SS_1147_CQU_20150316
      Description   : Verifica si es un archivo v�lido para la pantalla de Servipag VespucioSur    // SS_1147_CQU_20150316
    -----------------------------------------------------------------------------}                 // SS_1147_CQU_20150316
    function EsArchivoValidoVespucio(Nombre : AnsiString ) : boolean;                              // SS_1147_CQU_20150316
    begin                                                                                          // SS_1147_CQU_20150316
        if ExistePalabra(UpperCase(Nombre), '.TXT') then                                           // SS_1147_CQU_20150316
        begin                                                                                      // SS_1147_CQU_20150316
            if  (FCodigoServicio = SERVICIO_CAJA) and                                              // SS_1147_CQU_20150316
                //(ExistePalabra(Nombre, 'SPG_CAJA') or                                            // SS_1147_CQU_20150519  // SS_1147_CQU_20150316
                //ExistePalabra(Nombre, 'SPG_PORTAL')) then                                        // SS_1147_CQU_20150519  // SS_1147_CQU_20150316
                (ExistePalabra(Nombre, 'SPG_CAJA_') or                                             // SS_1147_CQU_20150519
                ExistePalabra(Nombre, 'SPG_PORTAL_')) then                                         // SS_1147_CQU_20150519
            begin                                                                                  // SS_1147_CQU_20150316
                FCodigoServicioVS := 'CAJA';                                                       // SS_1147_CQU_20150316
                Result := True;                                                                    // SS_1147_CQU_20150316
            end else if (FCodigoServicio = SERVICIO_UNCAJA) and                                    // SS_1147_CQU_20150316
                        //ExistePalabra(Nombre, 'SPG_RUAPORTAL') then                              // SS_1147_CQU_20150519  // SS_1147_CQU_20150316
                        ExistePalabra(Nombre, 'SPG_RUAPORTAL_') then                               // SS_1147_CQU_20150519
            begin                                                                                  // SS_1147_CQU_20150316
                FCodigoServicioVS := 'UNCAJA';                                                     // SS_1147_CQU_20150316
                Result := True;                                                                    // SS_1147_CQU_20150316
            end else if (FCodigoServicio = SERVICIO_UNEXPR) and                                    // SS_1147_CQU_20150316
                        //ExistePalabra(Nombre, 'SPG_RUAEXP') then                                 // SS_1147_CQU_20150519  // SS_1147_CQU_20150316
                        ExistePalabra(Nombre, 'SPG_RUAEXP_') then                                  // SS_1147_CQU_20150519
            begin                                                                                  // SS_1147_CQU_20150316
                FCodigoServicioVS := 'UNEXPR';                                                     // SS_1147_CQU_20150316
                Result := True;                                                                    // SS_1147_CQU_20150316
            end else if (FCodigoServicio = SERVICIO_EXPRES) and                                    // SS_1147_CQU_20150316
                        //ExistePalabra(Nombre, 'SPG_EXP') then                                    // SS_1147_CQU_20150519  // SS_1147_CQU_20150316
                        ExistePalabra(Nombre, 'SPG_EXP_') then                                     // SS_1147_CQU_20150519
            begin                                                                                  // SS_1147_CQU_20150316
                FCodigoServicioVS := 'EXPRES';                                                     // SS_1147_CQU_20150316
                Result := True;                                                                    // SS_1147_CQU_20150316
            end else if (FCodigoServicio = SERVICIO_PORRUT) then                                   // SS_1147_CQU_20150316
            begin                                                                                  // SS_1147_CQU_20150316
                 //if ExistePalabra(Nombre, 'SPG_RUA')                                             // SS_1147_CQU_20150519  // SS_1147_CQU_20150316
                 if ExistePalabra(Nombre, 'SPG_RUA_')                                              // SS_1147_CQU_20150519
                 then begin                                                                        // SS_1147_CQU_20150316
                    FCodigoServicioVS := 'REXPRE';                                                 // SS_1147_CQU_20150316
                    Result := True;                                                                // SS_1147_CQU_20150316
                 //end else if ExistePalabra(Nombre, 'REC96410') then begin                        // SS_1147_CQU_20150519  // SS_1147_CQU_20150316
                 //   FCodigoServicioVS := 'RCAJA';                                                // SS_1147_CQU_20150519  // SS_1147_CQU_20150316
                 //   Result := True;                                                              // SS_1147_CQU_20150519  // SS_1147_CQU_20150316
                 end else Result := False;                                                         // SS_1147_CQU_20150316
            end else Result:= False;                                                               // SS_1147_CQU_20150316
        end else Result := False;                                                                  // SS_1147_CQU_20150316
    end;                                                                                           // SS_1147_CQU_20150316

resourcestring
    MSG_ERROR = 'No es un archivo v�lido de Rendiciones ';
const
    STR_EXPRESS = '[EXPRES]';
    STR_CAJA_PORTAL = '[CAJA/PORTAL]';
    STR_UN_CAJA_PORTAL = '[UNCAJA/UNPORT]'; // Revision 1
    STR_UN_EXPRESS = '[UNEXPR]';
    STR_SERVIPAG_RUT = '[SERVIPAG x RUT]';
    STR_SERVIPAG_VS  = '[SERVIPAG]'; // SS_1147_CQU_20150316
var
    str_servicio: String;
    vHabilitaBoton : boolean;                       // SS_1147_MBE_20150413
    
begin

    // Revision 1: Se setea la descripcion del servicio de acuerdo al codigo (prefijo): '02', '03', '04'
    if (FCodigoServicio = SERVICIO_CAJA) then
        str_servicio := STR_CAJA_PORTAL
    else if (FCodigoServicio = SERVICIO_EXPRES) then
        str_servicio := STR_EXPRESS
    else if (FCodigoServicio = SERVICIO_UNCAJA) then
        str_servicio := STR_UN_CAJA_PORTAL
    //else if (FCodigoServicio = SERVICIO_UNEXPR) then          // SS_1060_CQU_20120731
    //    str_servicio := STR_UN_EXPRESS;                       // SS_1060_CQU_20120731
    else if (FCodigoServicio = SERVICIO_UNEXPR) then begin      // SS_1060_CQU_20120731
        str_servicio := STR_UN_EXPRESS;                         // SS_1060_CQU_20120731
        FPosicionRuaExpress := 1;                               // SS_1060_CQU_20120731
    end                                                         // SS_1060_CQU_20120731
    else if FCodigoServicio = SERVICIO_PORRUT then
    	str_servicio := STR_SERVIPAG_RUT
    else if FCodigoNativa = CODIGO_VS then                      // SS_1147_CQU_20150316
        str_servicio := STR_SERVIPAG_VS                         // SS_1147_CQU_20150316
    else
        str_servicio := '';

    OpenDialog.InitialDir := FServipag_Directorio_Rendiciones;
    if OpenDialog.execute then begin
        FArchivoAProcesar := ExtractFileName(OpenDialog.FileName);

        if FCodigoNativa = CODIGO_VS then begin                                                     // SS_1147_MBE_20150413
            vHabilitaBoton := EsArchivoValidoVespucio(FArchivoAProcesar);                           // SS_1147_MBE_20150413
        end                                                                                         // SS_1147_MBE_20150413
        else begin                                                                                  // SS_1147_MBE_20150413
            vHabilitaBoton :=   (   EsArchivoValidoCajasyPortal(FArchivoAProcesar) 	or              // SS_1147_MBE_20150413
        	                        EsArchivoValidoExpres(FArchivoAProcesar) 			or          // SS_1147_MBE_20150413
                                    EsArchivoValidoCajasyPortalUnificados(FArchivoAProcesar) or     // SS_1147_MBE_20150413
                                    EsArchivoValidoExpressUnificados(FArchivoAProcesar) or          // SS_1147_MBE_20150413
                                    EsArchivoValidoServipagPorRut(FArchivoAProcesar)                // SS_1147_MBE_20150413
                                );                                                                  // SS_1147_MBE_20150413
        end;                                                                                        // SS_1147_MBE_20150413

  {
        //if	EsArchivoValidoCajasyPortal(FArchivoAProcesar) 	or                                      // SS_1147_CQU_20150316
        if ((FCodigoNativa <> CODIGO_VS) and                                                            // SS_1147_CQU_20150316
            EsArchivoValidoCajasyPortal(FArchivoAProcesar) 	or											// SS_1147_CQU_20150316
        	EsArchivoValidoExpres(FArchivoAProcesar) 			or
            EsArchivoValidoCajasyPortalUnificados(FArchivoAProcesar) or
            EsArchivoValidoExpressUnificados(FArchivoAProcesar) or
			//EsArchivoValidoServipagPorRut(FArchivoAProcesar) then begin								// SS_1147_CQU_20150316
            EsArchivoValidoServipagPorRut(FArchivoAProcesar)) or										// SS_1147_CQU_20150316
            ((FCodigoNativa = CODIGO_VS) and (EsArchivoValidoVespucio(FArchivoAProcesar))) then begin   // SS_1147_CQU_20150316
        	// revision 2 BTNAbrirArchivo.Enabled := False;
  }
        if vHabilitaBoton then begin                                                                // SS_1147_MBE_20150413
            txtOrigen.text := OpenDialog.FileName;
            btnProcesar.Enabled := True;
        end
        else begin
        	//informo que no es un archivo valido
            // Revision 1
            //MsgBox(MSG_ERROR + IIF(FExpres, STR_EXPRESS, STR_CAJA_PORTAL) + STR_VALID , self.Caption, MB_OK + MB_ICONINFORMATION);
            MsgBox(MSG_ERROR + str_servicio, Caption, MB_OK + MB_ICONINFORMATION); // Revision 1
            txtOrigen.text := '';
            btnProcesar.Enabled := False;
            FArchivoAProcesar := '';
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EOrigenChange
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: Controlo que abra un archivo valido
  Parameters: Sender: TObject
  Return Value: None

  Revision 1
  Author : mbecerra
  Date: 19-Junio-2008
  Description:	Esta instrucci�n es innecesaria.
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesServipag.txtOrigenChange(Sender: TObject);
begin
    //Controlo que abra un archivo valido
  	// Revision 1 btnProcesar.Enabled := FileExists( txtOrigen.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: Abrir
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: Abro el Archivo TXT
  Parameters: None
  Return Value: None
        REVISION 1 ddiaz
            Date: 09-01-2005
            Description: se modific� el valor que se le asigna a FTotalRegistros de "FLista.Count - 5" a
                            "FLista.Count - 2". lo de por que "-5" habria que consultarlo con leo, en este
                            momento de vacaciones.
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesServipag.Abrir : Boolean;
resourcestring
  	MSG_OPEN_FILE_ERROR = 'Error al abrir el archivo';
begin
    Result := False;
    FLista.text := FileToString(txtOrigen.text);
//    FTotalRegistros := FLista.Count - 2; //original "- 5"                     // SS_1147_MBE_20150413
    // se le quita 1 al COUNT, debido a que la primera l�nea es el HEADER       // SS_1147_MBE_20150413
    FTotalRegistros := FLista.Count - 1;                                        // SS_1147_MBE_20150413
    if FLista.text <> '' then  begin
        Result := True;
        if FServipag_Directorio_Procesados <> '' then begin
            if RightStr(FServipag_Directorio_Procesados,1) = '\' then  FServipag_Directorio_Procesados := FServipag_Directorio_Procesados + ExtractFileName(txtOrigen.text)
            else FServipag_Directorio_Procesados := FServipag_Directorio_Procesados + '\' + ExtractFileName(txtOrigen.text);
        end;
    end else begin
        MsgBox(MSG_OPEN_FILE_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Control
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: verifico la consistencia entre los valores reflejados
               en el Registro de Footer y los mismos conceptos calculados
               a partir de los registros de detalle.
  Parameters: None
  Return Value: boolean
        REVISION 1 ddiaz
        Date: 09-01-2005
        Description: se modific� valor de varialble de bucle "i" a inicializarla en 1 para que tome el primer registro
                        de la FLista ya que el indice de los mismos comienza a valor 0

 Revisi�n: 2
 Author: nefernandez
 Date: 22/08/2007
 Description: Se controla que el registro de HEADER de las rendiciones de pagos
 servipag tengan la descripci�n adecuada

 Revisi�n: 3
 Author: nefernandez
 Date: 30/08/2007
 Description: Se permite que la descripci�n del registro HEADER de las rendiciones de pagos
 servipag sea 'Pagos Realizados en Caja y Portal de Servipag' o 'Pagos Realizados en  la Caja y Portal de Servipag'

 Revisi�n: 4
 Author: nefernandez
 Date: 06/09/2007
 Description: Se agrega verificacion para descripcion de header de rendiciones Express Unificado

 Revision 5
 Author: mbecerra
 Date: 19-Junio-2008
 Description: Se agregan validaciones para servipago por rut.
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesServipag.Control : Boolean;
resourcestring
    MSG_INVALID_FOOTER 	= 'El pie de p�gina del archivo es inv�lido!';
    MSG_NOT_EQUAL_COUNT = 'La cantidad reflejada en el registro de Footer no coincide'+ CRLF + 'con la cantidad calculada!';
    MSG_CANT_NOT_NUMBER = 'La cantidad reflejada en el registro de Footer no es un n�mero v�lido';
    MSG_NOT_EQUAL_SUM   = 'La suma reflejada en el registro de Footer no coincide con la suma calculada!';
    MSG_SUM_NOT_NUMBER  = 'La suma reflejada en el registro de Footer no es un n�mero v�lido';
    MSG_CANT_CONTROL    = 'Cantidad Control: ';
    MSG_CANT_CALC       = 'Cantidad Calculada: ';
    MSG_SUM_CONTROL     = 'Suma Control: ';
    MSG_SUM_CALC        = 'Suma Calculada: ';
    MSG_DIF             = 'Diferencia: ';
    MSG_ERROR           = 'No se pudo realizar el control del archivo!';
    MSG_ERROR1          = 'Error en validaci�n';
    MSG_HEADER1         = 'Pagos Realizados en Caja y Portal de Servipag';
    MSG_HEADER2         = 'Pagos Realizados en la Caja y Portal de Servipag';
    MSG_HEADER_06       = 'Pagos Realizados en Agencias Servipag';
    MSG_INVALID_HEADER 	= 'En encabezado debe tener una de las siguientes descripciones: ';
    MSG_HEADER          = 'HEADER';
    MSG_COSNOR          = 'COSNOR';
    MSG_HEADER3         = 'Pagos Realizados en la caja de Servipag';
    MSG_ERROR_EN        = 'El error se produjo en la l�nea: %s, al validar el campo %s' + CRLF + 'Mensaje del sistema: %s';  // SS_1060_CQU_20120731
var
    i : Integer;
    Glosa, Valor, Mensaje: String;
    NombreCampo, MensajeError : string; // SS_1060_CQU_20120731
begin
    Result := False;
    Mensaje := '';
    try
        NombreCampo := 'Header';        // SS_1060_CQU_20120731

        //Verifico que el HEADER tenga la descripcion apropiada.
        if (FCodigoServicio = '06') then begin
            Glosa := Copy(Flista.Strings[0], 15, 62 + FPosicionRuaExpress); // SS_1060_CQU_20120731 Glosa := Copy(Flista.Strings[0], 15, 62);
            if ( (Pos( uppercase(MSG_HEADER_06), uppercase(trim(Glosa)) ) <> 1) ) then begin
                  Mensaje := MSG_HEADER_06;
            end;
        end
        else if FCodigoServicio = '08' then begin
            Glosa :=  Copy(FLista[0],1,6);
            if UpperCase(Glosa) <> MSG_HEADER  then begin
                Mensaje := MSG_HEADER_06 + CRLF;
            end;

            Glosa := Copy(FLista[0], 8,6);
            if UpperCase(Glosa) <> MSG_COSNOR  then begin
                Mensaje := Mensaje + MSG_COSNOR + CRLF;
            end;

            Glosa := Copy(FLista[0], 15, Length(MSG_HEADER3));
            if UpperCase(Glosa) <> UpperCase(MSG_HEADER3)  then begin
                Mensaje := Mensaje + MSG_HEADER3 + CRLF;
            end;
        end
        else begin
            Glosa := Copy(Flista.Strings[0], 15, 69 + FPosicionRuaExpress);  // SS_1060_CQU_20120731 Glosa := Copy(Flista.Strings[0], 15, 69);
            if ( (Pos( uppercase(MSG_HEADER1), uppercase(Glosa) ) <> 1) and (Pos( uppercase(MSG_HEADER2), uppercase(Glosa) ) <> 1) ) then begin
                  Mensaje := MSG_HEADER1 + CRLF + MSG_HEADER2;
            end;
        end;

        if Mensaje <> '' then begin
            MsgBox(MSG_INVALID_HEADER + CRLF + Mensaje, Caption, MB_ICONEXCLAMATION);
            Exit;
        end;

        NombreCampo := 'Footer';  // SS_1060_CQU_20120731
        //Verifico si existe el footer
        Glosa := FLista[FLista.Count - 1];
        if ( Pos( 'FOOTER', Glosa) <> 1 ) then begin
    	      MsgBox( MSG_INVALID_FOOTER, MSG_ERROR, MB_ICONERROR );
    	      Exit;
        end;

        NombreCampo := 'FCantidadControl';  // SS_1060_CQU_20120731
        //Obtengo la cantidad y la suma del archivo de control
        //FCantidadControl := StrToFloat(Trim(Copy(Flista.Strings[FLista.Count - 1], 8, 6)));
        //FCantidadControl := StrToInt(Trim(Copy(Flista.Strings[FLista.Count - 1], 8, 6)));
        Valor := Trim(Copy(Flista.Strings[FLista.Count - 1], 8, 6));
        if (Valor <> '') and (PermitirSoloDigitos(Valor)) then
            FCantidadControl := StrToInt64(Valor)
        else begin
        	MsgBox(MSG_CANT_NOT_NUMBER, Caption, MB_ICONERROR);
            exit;
        end;

        //FSumaControl := StrToFloat(Trim(Copy(Flista.Strings[FLista.Count - 1], 30, 43)));
        Valor := Trim(Copy(Flista.Strings[FLista.Count - 1], 30, 14));

        if (Valor <> '') and (PermitirSoloDigitos(Valor)) then
            FSumaControl := StrToInt64(Valor)
        else begin
        	MsgBox(MSG_SUM_NOT_NUMBER, Caption, MB_ICONERROR);
            exit;
        end;

        //comparo la cantidad contra la calculada
        If FCantidadControl <> FTotalRegistros then begin
            MsgBoxErr(MSG_NOT_EQUAL_COUNT + CRLF + MSG_CANT_CONTROL  + FloatToSTR(FCantidadControl) + CRLF + MSG_CANT_CALC  + FloatToSTR(FTotalRegistros) + CRLF + MSG_DIF + FloatToSTR(FCantidadControl - FTotalRegistros) ,'', self.text, 0);
            exit;
        end;
        
        NombreCampo := 'FSumaReal';  // SS_1060_CQU_20120731
        //sumo los registros del archivo
        i:=1; //tmp original 2
        FSumaReal:=0;
      	while i < FLista.count - 1 do begin
            //FSumaReal := FSumaReal + StrToFloat(Trim(Copy(Flista.Strings[i], 73, 10)));                              // SS_1060_CQU_20120731
            FSumaReal := FSumaReal + StrToFloat(Trim(Copy(Flista.Strings[i], 73 + FPosicionRuaExpress, 10)));          // SS_1060_CQU_20120731                                                                                                              // SS_1060_CQU_20120731
            inc(i);
        end;
        //comparo la suma contra la calculada
        if FSumacontrol <> FSumaReal then begin
            MsgBoxErr(MSG_ERROR1, MSG_NOT_EQUAL_SUM + CRLF + MSG_SUM_CONTROL  + FloatToSTR(FSumaControl) + CRLF + MSG_SUM_CALC  + FloatToSTR(FSumaReal) + CRLF + MSG_DIF + FloatToSTR(FSumaControl - FSumaReal), self.text, 0);
            exit;
        end;
        Result := True;
    except
        on E : Exception do begin
            if AnsiPos('valid floating point', E.Message) <> 0 then MensajeError := 'Se esperaba un campo num�rico' + CRLF  // SS_1060_CQU_20120731
            else MensajeError := E.Message + CRLF;                                                                          // SS_1060_CQU_20120731

            //MsgBoxErr(MSG_ERROR, E.Message + ' L�nea: ' + IntToStr(NumLinea), MSG_ERROR, MB_ICONERROR);                   // SS_1060_CQU_20120731
            MsgBoxErr(MSG_ERROR, Format(MSG_ERROR_EN, [IntToStr(I), NombreCampo, MensajeError]), MSG_ERROR, MB_ICONERROR);  // SS_1060_CQU_20120731
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ConfirmaCantidades
  Author:    lgisuk
  Date Created: 20/10/2005
  Description:  Consulta al usuario si quiere continuar o no,
                presentandole un resumen de las cantidades a procesar
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesServipag.ConfirmaCantidades: Boolean;
ResourceString
    MSG_TITLE        = 'Recepci�n de Rendiciones';
    MSG_CONFIRMATION = 'El Archivo seleccionado contiene %d rendiciones a procesar.'+crlf+crlf+
                       'Desea continuar con el procesamiento del archivo?';
var
    Mensaje : String;
begin
    //Armo el mensaje
    Mensaje := Format(MSG_CONFIRMATION,[FTotalRegistros]);
    //lo muestro
    Result := ( MsgBox(Mensaje,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
end;

{-----------------------------------------------------------------------------
  Function Name: ParseLineToRecord
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: Arma un registro de Rendicion en base a la linea leida del TXT
  Parameters:
  Return Value: boolean

  Revisi�n: 1
  Author: nefernandez
  Date: 04/07/2007
  Description: Se incluye el medio de pago 'TCTO' (Tarjeta de Cr�dito) en la verificaci�n

  Revisi�n: 2
  Author: nefernandez
  Date: 26/07/2007
  Description: Se verifica que si la rendici�n es de pagos unificados (04) el
  campo "correlativo" sea mayor a cero

  Revisi�n: 3
  Author: lgisuk
  Date: 02/08/2007
  Description: Cambie Monto de IntTostr a IntToStr64

  Revision 4
  Author: mbecerra
  Date: 19-Junio-2008
  Description:  Se mejor� la lectura de los datos.

  Revision 5
  Author: mbecerra
  Date: 04-Dic-2008
  Description:	A ra�z de la reapertura de la SS 705, se hace un parseo m�s
            	riguroso. Es decir, instrucciones del tipo:
                Numero := StrToInt(Trim(Texto));
                	se cambian por
                Numero := StrToInt(Texto)

                pues ocurre que si Texto trae blancos al comienzo o al final,
                el TRIM los elimina, pero esto NO es correcto si se espera un n�mero
                seg�n el formato del archivo. Debe venir relleno con 0. Pues un blanco
                puede indicar un desfase en la l�nea.

                Igualmente si se esperan 20 blancos, deben venir los 20 blancos, y no menos.
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesServipag.ParseLineToRecord;

    {-----------------------------------------------------------------------
      Function Name: InicializarRegistroRendicion
      Author:    lgisuk
      Date Created: 21/10/2005
      Description: inicializo el registro de rendicion
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    procedure InicializarRegistroRendicion( var Rendicion : TRendicion );
    begin
        with Rendicion do begin
            CodigoBancoCheque := 0;
            CuentaCheque := '';
            SerieCheque := 0;
            PlazaCheque := 0;
            TipoTarjeta := '';
            MarcaTarjeta := '';
            NumeroTarjeta := '';
            ExpiracionTarjeta := '';
            TipoCuotas := '';
            NumeroCuotas := 0;
            Autorizacion := '';
        end;
    end;

Resourcestring
    MSG_ERROR                   = 'Error al leer el registro';
Const
    STR_VOUCHER                 = 'Comprobante: ';
    STR_DIVIDER                 = ' - ';
  	STR_DATE_ERROR              = 'La fecha Trn es inv�lida.';
    STR_COMPANY_ERROR           = 'La empresa prestadora de servicios es inv�lida.';
    STR_BRANCH_ERROR            = 'La sucursal es inv�lida.';
    STR_CONTROL_NUMBER_ERROR    = 'El correlativo es inv�lido.';
    STR_CONTROL_CORRELATIVO     = 'El c�digo externo de identificaci�n de pagos debe ser mayor a 0 (cero).';
    STR_INVOICE_DATE_ERROR      = 'La fecha contable es inv�lida.';
    STR_OPERATION_TYPE_ERROR    = 'El tipo de operaci�n es inv�lido.';
    STR_ACCOUNT_INDICATOR_ERROR = 'El indicador contable es inv�lido.';
    STR_AMMOUNT_ERROR           = 'El monto es inv�lido.';
    STR_PAYMENT_METHOD_ERROR    = 'El tipo de pago es inv�lido.';
    STR_BANK_CODE_ERROR         = 'El c�digo de banco es inv�lido.';
    STR_CHECK_SERIAL_ERROR      = 'La serie del cheque es inv�lida.';
    STR_ORIGIN_CHECK_ERROR      = 'La Plaza del cheque es inv�lida.';
    STR_NUMBER_OF_QUOTES_ERROR  = 'El Numero de Coutas en inv�lido.';
    STR_INVOICE_NUMBER_ERROR    = 'El n�mero de la nota de cobro es inv�lido.';
Var
    //Rev 4 Ano, Mes, Dia : String;
    ano, mes, dia, hora, min, sec : smallint;
//    sCorrelativo : String;                                            // SS_1147_MBE_20150413
    {AnoC, MesC, DiaC : String;
    sMonto : String;
    sCodigoBancoCheque : String;
    sSerieCheque : String;
    sPlazaCheque : String;
    sNumeroCuotas : String;  }
    NumeroComprobante : String;
    DescError, Aux : AnsiString;
    sTipoComprobanteFiscal, TipoComprobante : string;   // SS_1147_CQU_20150316
//    NumeroComprobanteFiscal, NroComprobante : Int64;  // SS_1147_MBE_20150413  // SS_1147_CQU_20150316
    NumeroComprobanteFiscal : Int64;                    // SS_1147_MBE_20150413

begin
    Result := True;
    DescError := '';

    try

        //Inicializo el Registro de Rendici�n
        InicializarRegistroRendicion (Rendicion);

        //Obtengo los Datos de la Rendici�n  y Parseo la L�nea
        DescError := STR_DATE_ERROR;
        if FCodigoNativa = CODIGO_VS then begin                                                                 // SS_1147_CQU_20150316
            ano	 := StrToInt(Copy(Linea, 21, 4));   															// SS_1147_CQU_20150316
            mes  := StrToInt(Copy(Linea, 25, 2));   															// SS_1147_CQU_20150316
            dia  := StrToInt(Copy(Linea, 27, 2));   															// SS_1147_CQU_20150316
            hora := 0;                              															// SS_1147_CQU_20150316
            min  := 0;                              															// SS_1147_CQU_20150316
            sec  := 0;                              															// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            Rendicion.Fecha    := EncodeDate(ano, mes, dia) + EncodeTime(hora, min, sec, 0);					// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            //Rendicion.EPS      := Trim(Copy( Linea, 15, 6)); NoViene											// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            DescError := STR_BRANCH_ERROR;																		// SS_1147_CQU_20150316
            Rendicion.Sucursal := StrToInt(Copy(Linea,44,3));													// SS_1147_CQU_20150316
            Rendicion.Terminal := Trim(Copy(Linea, 47, 4));														// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            Aux := Trim(Copy( Linea, 53, 3 + FPosicionRuaExpress));												// SS_1147_CQU_20150316
            if Aux = '' then Aux := '0';																		// SS_1147_CQU_20150316
            DescError := STR_CONTROL_NUMBER_ERROR;																// SS_1147_CQU_20150316
            Rendicion.Correlativo := StrToInt64(Aux);															// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            DescError := STR_INVOICE_DATE_ERROR;																// SS_1147_CQU_20150316
            ano := StrToInt(Copy(Linea, 21 + FPosicionRuaExpress, 4));											// SS_1147_CQU_20150316
            mes := StrToInt(Copy(Linea, 25 + FPosicionRuaExpress, 2));											// SS_1147_CQU_20150316
            dia := StrToInt(Copy(Linea, 27 + FPosicionRuaExpress, 2));											// SS_1147_CQU_20150316
            Rendicion.FechaContable := EncodeDate(ano, mes, dia);												// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            Rendicion.CodigoServicio     := FCodigoServicioVS;													// SS_1147_CQU_20150316
            //Rendicion.TipoOperacion      := Trim(Copy(Linea, 43 + FPosicionRuaExpress, 1));					// SS_1147_CQU_20150316
            //Rendicion.IndicadorContable  := Trim(Copy(Linea, 72 + FPosicionRuaExpress, 1));					// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            DescError := STR_AMMOUNT_ERROR;																		// SS_1147_CQU_20150316
            Rendicion.Monto := StrToInt64(Copy(Linea,13 + FPosicionRuaExpress,8));								// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            if      Trim(Copy(Linea, 43 + FPosicionRuaExpress, 1)) = '0' then Rendicion.MedioPago := 'CASH'		// SS_1147_CQU_20150316
            else if Trim(Copy(Linea, 43 + FPosicionRuaExpress, 1)) = '1' then Rendicion.MedioPago := 'CHEQUE'	// SS_1147_CQU_20150316
            else if Trim(Copy(Linea, 43 + FPosicionRuaExpress, 1)) = '2' then Rendicion.MedioPago := 'TCTO'		// SS_1147_CQU_20150316
            else Rendicion.MedioPago := '';																		// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            DescError := STR_BANK_CODE_ERROR;																	// SS_1147_CQU_20150316
            Rendicion.CodigoBancoCheque := StrToInt(Copy(Linea,36 + FPosicionRuaExpress,3));					// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            //Rendicion.CuentaCheque := Trim(Copy(Linea, 92 + FPosicionRuaExpress, 11));						// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            DescError := STR_CHECK_SERIAL_ERROR;																// SS_1147_CQU_20150316
            Rendicion.SerieCheque := StrToInt(Copy(Linea,29 + FPosicionRuaExpress,7));							// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            DescError := STR_ORIGIN_CHECK_ERROR;																// SS_1147_CQU_20150316
            Rendicion.PlazaCheque := StrToInt(Copy(Linea,39 + FPosicionRuaExpress,4));							// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            //Rendicion.TipoTarjeta   := Trim(Copy(Linea, 113 + FPosicionRuaExpress, 6));						// SS_1147_CQU_20150316
            //Rendicion.MarcaTarjeta  := Trim(Copy(Linea, 119 + FPosicionRuaExpress, 6));						// SS_1147_CQU_20150316
            //Rendicion.NumeroTarjeta := Trim(Copy(Linea, 125 + FPosicionRuaExpress, 19));						// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            //Rendicion.ExpiracionTarjeta  := Trim(Copy(Linea, 144 + FPosicionRuaExpress, 4));					// SS_1147_CQU_20150316
            //Rendicion.TipoCuotas         := Trim(Copy(Linea, 148 + FPosicionRuaExpress, 6));					// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            DescError := STR_NUMBER_OF_QUOTES_ERROR;															// SS_1147_CQU_20150316
            //rendicion.NumeroCuotas := StrToInt(Copy(Linea,154 + FPosicionRuaExpress,2));						// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            //Rendicion.Autorizacion  := Trim(Copy(Linea, 156 + FPosicionRuaExpress, 8));						// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316
            DescError := STR_INVOICE_NUMBER_ERROR;																// SS_1147_CQU_20150316
            NumeroComprobanteFiscal := StrToInt64(Copy(Linea, 1 + FPosicionRuaExpress, 10));                    // SS_1147_CQU_20150316
            sTipoComprobanteFiscal  := Trim(Copy(Linea, 11 + FPosicionRuaExpress, 2));                          // SS_1147_CQU_20150316

//            ObtenerDocumentoInterno(sTipoComprobanteFiscal, NumeroComprobanteFiscal,                          // SS_1147_MBE_20150413     // SS_1147_CQU_20150316
//                                    TipoComprobante, NroComprobante);                                         // SS_1147_MBE_20150413     // SS_1147_CQU_20150316
//            Rendicion.NotaCobro := NroComprobante;                                                            // SS_1147_MBE_20150413     // SS_1147_CQU_20150316
            case StrToInt(sTipoComprobanteFiscal) of                                                            // SS_1147_MBE_20150413
                33 : TipoComprobante := TC_FACTURA_AFECTA;                                                      // SS_1147_MBE_20150413
                34 : TipoComprobante := TC_FACTURA_EXENTA;                                                      // SS_1147_MBE_20150413
                39 : TipoComprobante := TC_BOLETA_AFECTA;                                                       // SS_1147_MBE_20150413
                41 : TipoComprobante := TC_BOLETA_EXENTA;                                                       // SS_1147_MBE_20150413
            end;                                                                                                // SS_1147_MBE_20150413

            Rendicion.TipoComprobante := TipoComprobante;                                                       // SS_1147_MBE_20150413
            Rendicion.NumeroComprobante := NumeroComprobanteFiscal;                                             // SS_1147_MBE_20150413

            //Rendicion.Identificador := Trim(Copy(Linea, 173 + FPosicionRuaExpress, 91));						// SS_1147_CQU_20150316
																												// SS_1147_CQU_20150316

//---------------------------- esto est� malo, est� dem�s                                                       // SS_1147_MBE_20150413
//            {Otras validaciones}																				// SS_1147_MBE_20150413     // SS_1147_CQU_20150316
//            DescError := '';         {si llega ac� es porque no ha habido error}								 // SS_1147_MBE_20150413     // SS_1147_CQU_20150316
//            if DescError <> '' then begin																		// SS_1147_MBE_20150413     // SS_1147_CQU_20150316
//                AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, DescError);					// SS_1147_MBE_20150413     // SS_1147_CQU_20150316
//                Result := False;																				// SS_1147_MBE_20150413     // SS_1147_CQU_20150316
//            end;																								// SS_1147_MBE_20150413     // SS_1147_CQU_20150316
//-------------------------------------------------------------
        end                                                                                                     // SS_1147_CQU_20150316
        else begin                              															    // SS_1147_CQU_20150316
            ano	 := StrToInt(Copy(Linea, 1, 4));
            mes  := StrToInt(Copy(Linea, 5, 2));
            dia  := StrToInt(Copy(Linea, 7, 2));
            hora := StrToInt(Copy(Linea, 9, 2));
            min  := StrToInt(Copy(Linea, 11, 2));
            sec  := StrToInt(Copy(Linea, 13, 2));
            Rendicion.Fecha    := EncodeDate(ano, mes, dia) + EncodeTime(hora, min, sec, 0);

            Rendicion.EPS      := Trim(Copy( Linea, 15, 6));

            DescError := STR_BRANCH_ERROR;
            Rendicion.Sucursal := StrToInt(Copy(Linea,21,6));			// Rev 5  Rendicion.Sucursal := StrToInt(Trim(Copy(Linea, 21, 6)));
            Rendicion.Terminal := Trim(Copy(Linea, 27, 15));

            Aux := Trim(Copy( Linea, 42, 10 + FPosicionRuaExpress));      // SS_1060_CQU_20120731 Aux := Trim(Copy( Linea, 42, 10));
            if Aux = '' then Aux := '0';
            DescError := STR_CONTROL_NUMBER_ERROR;
            Rendicion.Correlativo := StrToInt64(Aux);

            DescError := STR_INVOICE_DATE_ERROR;
            ano := StrToInt(Copy(Linea, 52 + FPosicionRuaExpress, 4));    // SS_1060_CQU_20120731 ano := StrToInt(Copy(Linea, 52, 4));
            mes := StrToInt(Copy(Linea, 56 + FPosicionRuaExpress, 2));    // SS_1060_CQU_20120731 mes := StrToInt(Copy(Linea, 56, 2));
            dia := StrToInt(Copy(Linea, 58 + FPosicionRuaExpress, 2));    // SS_1060_CQU_20120731 dia := StrToInt(Copy(Linea, 58, 2));
            Rendicion.FechaContable := EncodeDate(ano, mes, dia);

            Rendicion.CodigoServicio     := Trim(Copy(Linea, 60 + FPosicionRuaExpress, 6)); // SS_1060_CQU_20120731  Rendicion.CodigoServicio     := Trim(Copy(Linea, 60, 6));
            Rendicion.TipoOperacion      := Trim(Copy(Linea, 66 + FPosicionRuaExpress, 6)); // SS_1060_CQU_20120731  Rendicion.TipoOperacion      := Trim(Copy(Linea, 66, 6));
            Rendicion.IndicadorContable  := Trim(Copy(Linea, 72 + FPosicionRuaExpress, 1)); // SS_1060_CQU_20120731  Rendicion.IndicadorContable  := Trim(Copy(Linea, 72, 1));

            DescError := STR_AMMOUNT_ERROR;
            //Rendicion.Monto := StrToInt64(Copy(Linea,73,10));			//Rev 5 Rendicion.Monto := StrToInt64(Trim(Copy(Linea, 73, 10)));  // SS_1060_CQU_20120731
            Rendicion.Monto := StrToInt64(Copy(Linea,73 + FPosicionRuaExpress,10));			                                               // SS_1060_CQU_20120731

            //Rendicion.MedioPago := Trim(Copy(Linea, 83, 6));         // SS_1060_CQU_20120731
            Rendicion.MedioPago := Trim(Copy(Linea, 83 + FPosicionRuaExpress, 6));           // SS_1060_CQU_20120731

            DescError := STR_BANK_CODE_ERROR;
            //Rendicion.CodigoBancoCheque := StrToInt(Copy(Linea,89,3));	// Rev 5 Rendicion.CodigoBancoCheque := StrToInt(Trim(Copy(Linea, 89, 3))); // SS_1060_CQU_20120731
            Rendicion.CodigoBancoCheque := StrToInt(Copy(Linea,89 + FPosicionRuaExpress,3));                                                            // SS_1060_CQU_20120731

            //Rendicion.CuentaCheque := Trim(Copy(Linea, 92, 11));                      // SS_1060_CQU_20120731
            Rendicion.CuentaCheque := Trim(Copy(Linea, 92 + FPosicionRuaExpress, 11));  // SS_1060_CQU_20120731

            DescError := STR_CHECK_SERIAL_ERROR;
            //Rendicion.SerieCheque := StrToInt(Copy(Linea,103,7));		//Rev 5 Rendicion.SerieCheque  := StrToInt(Trim(Copy(Linea, 103, 7)));          // SS_1060_CQU_20120731
            Rendicion.SerieCheque := StrToInt(Copy(Linea,103 + FPosicionRuaExpress,7));                                                                 // SS_1060_CQU_20120731

            DescError := STR_ORIGIN_CHECK_ERROR;
            //Rendicion.PlazaCheque := StrToInt(Copy(Linea,110,3));		//Rev 5 Rendicion.PlazaCheque  := StrToInt(Trim(Copy(Linea, 110, 3)));          // SS_1060_CQU_20120731
            Rendicion.PlazaCheque := StrToInt(Copy(Linea,110 + FPosicionRuaExpress,3));                                                                 // SS_1060_CQU_20120731

            Rendicion.TipoTarjeta   := Trim(Copy(Linea, 113 + FPosicionRuaExpress, 6));       // SS_1060_CQU_20120731 Rendicion.TipoTarjeta   := Trim(Copy(Linea, 113, 6));
            Rendicion.MarcaTarjeta  := Trim(Copy(Linea, 119 + FPosicionRuaExpress, 6));       // SS_1060_CQU_20120731 Rendicion.MarcaTarjeta  := Trim(Copy(Linea, 119, 6));
            Rendicion.NumeroTarjeta := Trim(Copy(Linea, 125 + FPosicionRuaExpress, 19));      // SS_1060_CQU_20120731 Rendicion.NumeroTarjeta := Trim(Copy(Linea, 125, 19));

            Rendicion.ExpiracionTarjeta  := Trim(Copy(Linea, 144 + FPosicionRuaExpress, 4));  // SS_1060_CQU_20120731 Rendicion.ExpiracionTarjeta  := Trim(Copy(Linea, 144, 4));
            Rendicion.TipoCuotas         := Trim(Copy(Linea, 148 + FPosicionRuaExpress, 6));  // SS_1060_CQU_20120731 Rendicion.TipoCuotas         := Trim(Copy(Linea, 148, 6));

            DescError := STR_NUMBER_OF_QUOTES_ERROR;
            //rendicion.NumeroCuotas := StrToInt(Copy(Linea,154,2));		// Rev 5 Rendicion.NumeroCuotas       := StrToInt(Trim( Copy(Linea, 154, 2))); // SS_1060_CQU_20120731
            rendicion.NumeroCuotas := StrToInt(Copy(Linea,154 + FPosicionRuaExpress,2));                                                                   // SS_1060_CQU_20120731

            Rendicion.Autorizacion  := Trim(Copy(Linea, 156 + FPosicionRuaExpress, 8));       // SS_1060_CQU_20120731 Rendicion.Autorizacion  := Trim(Copy(Linea, 156, 8));

            DescError := STR_INVOICE_NUMBER_ERROR;
            //Rendicion.NotaCobro := StrToInt(Copy(Linea, 164, 9));		// Rev 5 Rendicion.NotaCobro     := StrToInt(Trim(Copy(Linea, 164, 9)));           // SS_1060_CQU_20120731
//            Rendicion.NotaCobro := StrToInt(Copy(Linea, 164 + FPosicionRuaExpress, 9));                   // SS_1147_MBE_20150413                        // SS_1060_CQU_20120731
            Rendicion.NumeroComprobante := StrToInt(Copy(Linea, 164 + FPosicionRuaExpress, 9));             // SS_1147_MBE_20150413
            Rendicion.TipoComprobante := 'NK';                                                              // SS_1147_MBE_20150413

            Rendicion.Identificador := Trim(Copy(Linea, 173 + FPosicionRuaExpress, 91));      // SS_1060_CQU_20120731  Rendicion.Identificador := Trim(Copy(Linea, 173, 91));

            {Otras validaciones}
            DescError := '';         {si llega ac� es porque no ha habido error}
            if (Rendicion.EPS <> 'COSNOR') then begin
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER +  STR_COMPANY_ERROR;
            end
            else if (FCodigoServicio = SERVICIO_UNCAJA) and (Rendicion.Correlativo <= 0) then begin
                { Verifico que el numero "correlativo" (identificaci�n del pago en el sistema externo) sea
                  mayor a cero para la rendici�n de pagos unificados (04)}
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_CONTROL_CORRELATIVO;
            end
            else if (Rendicion.TipoOperacion <> 'INGRES') then begin
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_OPERATION_TYPE_ERROR;
            end
            else if (Rendicion.IndicadorContable <> 'H') then begin
                //Verifico que el indicador contable sea haber
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_ACCOUNT_INDICATOR_ERROR;
            end
            else if (Rendicion.MedioPago <> 'CASH') and (Rendicion.MedioPago <> 'CHEQUE') and (Rendicion.MedioPago <> 'TCTO') then begin
                //Verifico que el medio de pago sea efectivo o cheque
                // Revision 1
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_PAYMENT_METHOD_ERROR;
            end;

            if DescError <> '' then begin
                AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, DescError);
                Result := False;
            end;
        end;                                        															// SS_1147_CQU_20150316
    except on E: Exception do begin
            { Rev 4 MsgBoxErr(MSG_ERROR, E.Message, Self.Caption, MB_ICONERROR);
             FErrorGrave:=true;}
//            AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, MSG_ERROR + ' ' + DescError);
            FErrores.Add(MSG_ERROR + DescError + e.Message);                                                    // SS_1147_MBE_20150413
            Result := False;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: AnalizarRencionesTXT
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: Antes de hacer nada Parsea todo el Archivo para verificar
  				que el parseo sea v�lido
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesServipag.AnalizarRendicionesTXT : Boolean;
Resourcestring
    MSG_ERROR_SYNTAXIS = 'El Archivo contiene errores de Sintaxis';
var
    I : Integer;
    Rendicion : TRendicion;
    DescError : String;
begin
    FCancelar := False;                //Permito que la importacion sea cancelada
    PbProgreso.Position := 0;          //Inicio la Barra de progreso
    PbProgreso.Max := FLista.Count-1;  //Establezco como maximo la cantidad de registros del memo
    FErrores.Clear;
    I := 1;
    //recorro las lineas del archivo
    while I < FLista.Count - 1 do begin //antes -3

        //si cancelan la operacion
        if FCancelar then begin
            Result := False;             //El analisis fue cancelado por el usuario
            pbProgreso.Position := 0;  //Si la importacion fue detenida, Vuelvo a Comenzar
            Exit;                      //salgo de la rutina
        end;

        //Arma un registro de Rendicion en base a la linea leida del TXT
        //verifica que los valores recibidos sean validos sino devuelve una
        //Descripci�n del error.
        if not ParseLineToRecord(Flista.Strings[i], Rendicion) then begin
            //lo inserto en el string list de errores
            FErrores.Add(DescError);
        end;

        pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso
        Application.ProcessMessages;                    //Refresco la pantalla

        i:= i + 1;
    end;

    Result:= (FErrores.count = 0);

    if (Result = False) then MsgBox(MSG_ERROR_SYNTAXIS, Self.Caption, MB_OK + MB_ICONINFORMATION);

end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: boolean
  Revision : 1
      Author : vpaszkowicz
      Date : 20/03/2008
      Description :Le agrego la suma de control y la cantidad de registros.
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesServipag.RegistrarOperacion : Boolean;
resourcestring
    MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Procesar Archivo de Rendiciones';
var
    NombreArchivo: String;
  	DescError    : String;
begin
    NombreArchivo := Trim(ExtractFileName(TxtOrigen.text));
    Result := RegistrarOperacionEnLogInterface (DMConnections.BaseCAC , RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_SERVIPAG , NombreArchivo, UsuarioSistema , STR_OBSERVACION , False , False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, FCantidadControl, FSumaControl, DescError);
    if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarRenciones
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: Proceso el archivo de rendiciones
  Parameters: 
  Return Value: Boolean
------------------------------------------------------------------------------
  Revision 1
  ddiaz
    Date: 09-01-2005
    Description: se modific� el valor que se le asigna variable "I" para bucle while de -2 a -1
                    para empezar correctamente en el primer registro del la lista FLista
-----------------------------------------------------------------------------
  Revision 2
  lgisuk
  05/11/07
  Ahora envio por parametro el numero de linea a procesar.
----------------------------------------------------------------------------
  Revision 3
  lgisuk
  28/11/07
  Ahora si el registro es rechazado continuamos procesando el resto.

  Revision 4
  Author: mbecerra
  Date: 19-Junio-2008
  Description:
            	Se mejora la funcionalidad para que no rechace todo el archivo si
                existe alguna l�nea mala. Adem�s, se agregan las rendiciones
                servipag por rut

                * Se agrega que no grabe en ErroresInterfaces el resultado del stored
                pues ya lo graba en PagosRechazados y en el reporte aparece
                dos veces el mismo error.

  Revision 5
  Author: mbecerra
  Date: 04-Dic-2008
  Description:	Se cambia la validaci�n del largo de l�nea, en vez de validar que sea
            	>= Valor, se exige que sea exactamente igual a ese valor.
----------------------------------------------------------------------------}
Function TFRecepcionRendicionesServipag.ActualizarRendiciones : Boolean;

      {-----------------------------------------------------------------------------
        Function Name: ActualizarRendicion
        Author:    lgisuk
        Date Created: 20/10/2005
        Description: Registro el pago con la informacion recibida en el archivo
        Parameters:
        Return Value: boolean
      -----------------------------------------------------------------------------
        Revision 1
         Author: lgisuk
         Date Created: 14/12/2006
         Description: Escribo sentencias sin utilizar comando with,
                      no utilizo refresh.
       -----------------------------------------------------------------------------
        Revision 2
         Author: lgisuk
         Date Created: 13/03/2007
         Description: se informa por parametro si se trate de un reintento o no
      -----------------------------------------------------------------------------
       Revision 3
        lgisuk
        05/11/2007
        Muestro un mensaje descriptivo con numero de linea y linea del problema
        Registro en log si se produce una excepci�n.
      -----------------------------------------------------------------------------}
      Function ActualizarRendicion (Rendicion : TRendicion; nNroLineaScript : Integer) : Boolean;
      resourcestring
          MSG_ERROR_SAVING_INVOICE = 'Error Linea %d,  %s';
      var
          Error : String;
          //
          Reintentos : Integer;
          MensajeDeadlock : AnsiString;
      begin
          Result := False;
          Error := '';

          //Intentamos registrar el pago de un comprobante.
          Reintentos  := 0;

          while (Reintentos < 3) and (not FErrorGrave) do begin

                try
                      SPProcesarRendicionesServipag.Parameters.Refresh;                                                                         // SS_1147_MBE_20150413
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@Fecha').Value := Rendicion.Fecha;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@EPS').Value := Rendicion.EPS;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@Sucursal').Value := Rendicion.Sucursal;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@Terminal').Value := Rendicion.Terminal;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@Correlativo').Value := Rendicion.Correlativo;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@FechaContable').Value := Rendicion.FechaContable;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@CodigoServicio').Value := Rendicion.CodigoServicio;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@TipoOperacion').Value := Rendicion.TipoOperacion;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@IndicadorContable').Value := Rendicion.IndicadorContable;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@Monto').Value := Rendicion.Monto;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@MedioPago').Value := Rendicion.MedioPago;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@CodigoBancoCheque').Value := IIF( Rendicion.CodigoBancoCheque <> 0, Rendicion.CodigoBancoCheque, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@CuentaCheque').Value := IIF( Rendicion.CuentaCheque <> '', Rendicion.CuentaCheque, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@SerieCheque').Value := IIF( Rendicion.SerieCheque <> 0, Rendicion.SerieCheque, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@PlazaCheque').Value := IIF( Rendicion.PlazaCheque <> 0, Rendicion.PlazaCheque, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@TipoTarjeta').Value := IIF( Rendicion.TipoTarjeta <> '', Rendicion.TipoTarjeta, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@MarcaTarjeta').Value := IIF( Rendicion.MarcaTarjeta <> '', Rendicion.MarcaTarjeta, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@NumeroTarjeta').Value := IIF( Rendicion.NumeroTarjeta <> '', Rendicion.NumeroTarjeta, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@ExpiracionTarjeta').Value := IIF( Rendicion.ExpiracionTarjeta <> '', Rendicion.ExpiracionTarjeta, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@TipoCuotas').Value := IIF( Rendicion.TipoCuotas <> '', Rendicion.TipoCuotas, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@NumeroCuotas').Value := IIF( Rendicion.NumeroCuotas <> 0, Rendicion.NumeroCuotas, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@Autorizacion').Value := IIF( Rendicion.Autorizacion <> '', Rendicion.Autorizacion, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@TipoComprobante').Value := Rendicion.TipoComprobante;              // SS_1147_MBE_20150413
//                      SPProcesarRendicionesServipag.Parameters.ParamByName('@NotaCobro').Value := Rendicion.NotaCobro;                        // SS_1147_MBE_20150413
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@NumeroComprobante').Value := Rendicion.NumeroComprobante;          // SS_1147_MBE_20150413
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@Identificador').Value := IIF( Rendicion.Identificador <> '', Rendicion.Identificador, NULL );
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@Usuario').Value	:= UsuarioSistema;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@Reintento').Value	:= Reintentos;
                      SPProcesarRendicionesServipag.Parameters.ParamByName('@DescripcionError').Value := '';
                      SPProcesarRendicionesServipag.CommandTimeout := 500;
                      SPProcesarRendicionesServipag.ExecProc;
                      Error := Trim(Copy(VarToStr(SPProcesarRendicionesServipag.Parameters.ParamByName('@DescripcionError').Value),1,100));
                      if Error <> '' then begin                                                                                                 // SS_1147_MBE_20150413
                        FErrores.Add(Error);                                                                                                    // SS_1147_MBE_20150413
                      end;                                                                                                                      // SS_1147_MBE_20150413

                      { Revision 4
                      if Error <> '' then begin
                          AgregarErrorInterfase(DMConnections.BaseCAC,FCodigoOperacion, Format(MSG_ERROR_SAVING_INVOICE, [nNroLineaScript, Error]));
                      end;
                      }
                      Result := True;
                      //1/4 de segundo entre registracion de pago y otro
                      Sleep(25);
                      //Salgo del ciclo por Ok
                      Break;


                except
                    on  E : Exception do begin
                          if (pos('deadlock', e.Message) > 0) then begin
                              mensajeDeadlock := e.message;
                              inc(Reintentos);
                              sleep(2000);
                          end else begin
                              FErrorGrave := True;
                          	  //Registro la excepcion en el log de operaciones
                              AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_ERROR_SAVING_INVOICE, [nNroLineaScript, e.Message]));
                              MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [nNroLineaScript, ' ']), E.Message, Self.caption, MB_ICONERROR);
                          end;
                    end;
                end;

          end;

          //una vez que se reintento 3 veces x deadlock lo informo
          if Reintentos = 3 then begin
              FErrorGrave := True;
              //Registro la excepcion en el log de operaciones
              AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_ERROR_SAVING_INVOICE, [nNroLineaScript, MensajeDeadlock]));
              MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [nNroLineaScript, ' ']), MensajeDeadlock, Self.caption, MB_ICONERROR);
          end;

      end;

resourcestring
    MSG_CANCELAR  = '�Est� seguro de cancelar el proceso?';
    MSG_CANCELADO = 'Cancelado por el usuario';
    MSG_LARGO_LINEA = 'La L�nea %d, tiene largo %d, el cual no cumple el largo esperado: %d';

const
    LARGO_LINEA = 264;
    LARGO_LINEA_VS = 58;	                    // SS_1147_CQU_20150316
    LARGO_LINEA_VS_EXPRESS = 57;                // SS_1147_MBE_20150413

var
    i: Integer;
    Rendicion : TRendicion;
    LargoLinea : Integer;	// SS_1147_CQU_20150316
begin
    Result := False;
    FCancelar := False;                //Permito que la importacion sea cancelada
    FErrorGrave := False;
    pbProgreso.Position := 0;          //Inicio la Barra de progreso
    pbProgreso.Max := FLista.Count-1;  //Establezco como maximo la cantidad de registros del memo

//    if FCodigoNativa = CODIGO_VS        // SS_1147_CQU_20150316                                           //SS_1147_MBE_20150413
//    then LargoLinea := LARGO_LINEA_VS   // SS_1147_CQU_20150316                                           //SS_1147_MBE_20150413
//    else LargoLinea := LARGO_LINEA;     // SS_1147_CQU_20150316                                           //SS_1147_MBE_20150413

    if FCodigoNativa = CODIGO_VS then begin                                                                 //SS_1147_MBE_20150413
        FPosicionRuaExpress := 0;                                                                           //SS_1147_MBE_20150413
        if FCodigoServicioVS =  'EXPRES' then LargoLinea := LARGO_LINEA_VS_EXPRESS                          //SS_1147_MBE_20150413
        else LargoLinea := LARGO_LINEA_VS;                                                                  //SS_1147_MBE_20150413
    end                                                                                                     //SS_1147_MBE_20150413
    else LargoLinea := LARGO_LINEA;                                                                         //SS_1147_MBE_20150413


    FErrores.Clear;                                                                                         //SS_1147_MBE_20150413
    i := 1; // original 2
//    while (I < FLista.Count - 1) and (not FErrorGrave)  do begin                                          //SS_1147_MBE_20150413
    while (i < FLista.Count) and (not FErrorGrave) do begin                                                 //SS_1147_MBE_20150413


// Revision 5        if Length(FLista[i]) >= LARGO_LINEA then begin
        //if Length(FLista[i]) = (LARGO_LINEA) then begin                     // SS_1060_CQU_20120731
        //if Length(FLista[i]) = (LARGO_LINEA + FPosicionRuaExpress) then begin // SS_1147_CQU_20150316 // SS_1060_CQU_20120731
        if Length(FLista[i]) = (LargoLinea + FPosicionRuaExpress) then begin    // SS_1147_CQU_20150316
        	//Obtengo el registro con los datos de la rendicion del archivo

        	if ParseLineToRecord(Flista[i], Rendicion) then begin
        		//Registro el pago con la informaci�n de la rendicion
        		ActualizarRendicion(Rendicion, i);
        	end;
        end
        else FErrores.Add(Format(MSG_LARGO_LINEA, [i, Length(FLista[i]), (LargoLinea + FPosicionRuaExpress)]));
        	//AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_LARGO_LINEA, [i, Length(FLista[i]), (LARGO_LINEA)]));                       // SS_1060_CQU_20120731
        	//AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_LARGO_LINEA, [i, Length(FLista[i]), (LARGO_LINEA + FPosicionRuaExpress)])); // SS_1147_CQU_20150316 // SS_1060_CQU_20120731
//        	AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_LARGO_LINEA, [i, Length(FLista[i]), (LargoLinea + FPosicionRuaExpress)]));    //SS_1147_MBE_20150413      // SS_1147_CQU_20150316

        pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso
        Application.ProcessMessages;                    //Refresco la pantalla
        Inc(i);

        if FCancelar then begin
        	FCancelar := False;
        	if MsgBox(MSG_CANCELAR, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES then FErrorGrave := True;
        end;

    end;

    Result := True;
end;



{-----------------------------------------------------------------------------
  Function Name: GenerarReporteErroresSintaxis
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: Genero el reporte de errores de sintaxis
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesServipag.GenerarReporteErroresSintaxis : Boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de errores de Sintaxis';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Servipag - Procesar Archivo de Rendiciones';
var
    F : TFRptErroresSintaxis;
begin
    Result := False;
    try
        //muestro el reporte
        Application.CreateForm(TFRptErroresSintaxis, F);
        if not F.Inicializar( IntToStr(RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_SERVIPAG) , REPORT_TITLE , FServipag_Directorio_Errores ,FErrores) then f.Release;
        Result := True;
    except
       on E : Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Genero el Reporte de Finalizacion de Proceso sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesServipag.GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
Const
    STR_TITLE = 'Comprobantes Recibidos';
var
    FRecepcion : TFrmRptRecepcionComprobantesServipag;//TFRptRecepcionRendiciones;
begin
    Result := False;
    try
        //muestro el reporte
        Application.CreateForm(TFrmRptRecepcionComprobantesServipag{TFRptRecepcionRendiciones},FRecepcion);
        if not fRecepcion.Inicializar( STR_TITLE, CodigoOperacionInterfase) then fRecepcion.Release;
        Result := True;
    except
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: Procesa el archivo de Rendiciones
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  11/10/07
  Ahora se registra en el log que el usuario cancelo el proceso

  Revision 2
  Author: mbecerra
  Date: 19-Junio-2008
  Description:
            	1.- Se agrega el procesamiento de Servipag por Rut
                2.- Se elimina la llamada a ValidarRendicionesTXT, pues si una l�nea est� mal
                debe seguir procesando el resto.
                3.- Se agrega la llamada a VerificarArchivoProcesado para advertir al usuario que el archivo
                ya fue procesado

  Revision 3
  Author: mbecerra
  Date: 10-Julio-2008
  Description:
                1.- Se modifica para considere los errores de interfases, pues no los sumaba.
                2.- se agregaron los errores de interfases al reporte.
                3.- Se modifica para que siempre muestre el reporte.
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesServipag.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 20/10/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion : Integer ; var Cantidad : Integer) : Boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+IntToStr(CodigoOperacion)+')');
            Result := True;
        except
           on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

     {-----------------------------------------------------------------------------
      Function Name:  ObtenerResumen
      Author:    lgisuk
      Date Created: 04/04/2006
      Description:  Obtengo el resumen del proceso realizado
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    procedure ObtenerResumen (CodigoOperacionInterfase : Integer;Var CantRegs, Aprobados, Rechazados : Integer);
    resourcestring
      MSG_ERROR = 'Error';
      MSG_ERROR_QUERY = 'Error Obteniendo Resumen';
    begin
      try
          With spObtenerResumenComprobantesRecibidos, Parameters do begin
              Close;
              ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
              CommandTimeout := 500;
              //Open;                                                                           // SS_1147_MBE_20150413
              ExecProc;                                                                         // SS_1147_MBE_20150413
              CantRegs := Parameters.ParamByName('@CantidadComprobantesRecibidos').Value;
              Aprobados := Parameters.ParamByName('@CantidadComprobantesAceptados').Value;
              Rechazados := Parameters.ParamByName('@CantidadComprobantesRechazados').Value;
              Close;
          end;
      except
          on E : Exception do begin
              MsgBoxErr(MSG_ERROR_QUERY, E.Message, MSG_ERROR, MB_ICONERROR);
          end;
      end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 20/10/2005
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog (CantidadErrores : Integer) : Boolean;
   resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : String;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_CANCEL       = 'Proceso cancelado por el usuario';
    MSG_PROCESS_NOT_COMPLETE = 'El proceso no se pudo completar';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
  	MSG_PROCESS_FINALLY_OK   = 'Proceso finalizado con �xito';
    MSG_ARCHIVO_NO_EXISTE    = 'Error: No se encontr� el archivo';
    MSG_ARCHIVO_YA_PROCESADO = 'El archivo seleccionado ya fue procesado. �Desea Reprocesarlo?';
Const
    STR_OPEN_FILE          = 'Abrir Archivo...';
    STR_CHECK_FILE         = 'Control del Archivo...';
    STR_CONFIRM            = 'Confirmar Cantidades...';
    STR_ANALYZING          = 'Analizando Rendiciones...';
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
    STR_PROCESS            = 'Registrando los Pagos...';
var
    CantRegs, Aprobados, Rechazados : integer;
    CantidadErrores : Integer;
begin
	if not FileExists(txtOrigen.Text) then begin
    	MsgBox(MSG_ARCHIVO_NO_EXISTE, Caption, MB_ICONEXCLAMATION);
        Exit;
    end;

    btnProcesar.Enabled     := False;
    btnCancelar.Enabled     := True;
    btnAbrirArchivo.Enabled := False;
    PnlAvance.Visible := True;       //El Panel de Avance es visible durante el procesamiento

    //Inicio la operacion

    {advertimos si el archivo ya fue procesado anteriormente}
    if VerificarArchivoProcesado(DMConnections.BaseCAC, FArchivoAProcesar) and
    	(MsgBox(MSG_ARCHIVO_YA_PROCESADO, Caption, MB_ICONQUESTION + MB_YESNO) = IDNO)  then Exit;

  	try

        //Abro el archivo
        Lblmensaje.Caption := STR_OPEN_FILE;
        if not Abrir() then begin
            FErrorGrave := True;
            Exit;
        end;

        if FCodigoNativa <> CODIGO_VS then begin    // SS_1147_CQU_20150316
            //Controlo el archivo
            Lblmensaje.Caption := STR_CHECK_FILE;
            if not Control() then begin
                FErrorGrave := True;
                Exit;
            end;
        end                                        // SS_1147_CQU_20150316
        else begin                                 // SS_1147_MBE_20150413
            FCantidadControl := FLista.Count - 1;  // SS_1147_MBE_20150413
        end;                                       // SS_1147_MBE_20150413

        //El usuario acepta o no continuar con el proceso
        Lblmensaje.Caption := STR_CONFIRM;
        if not ConfirmaCantidades() then begin
            FErrorGrave := True;
            Exit;
        end;

        { Revision
        //Analizo si hay errores de parseo o sintaxis en el archivo
        Lblmensaje.Caption := STR_ANALYZING;
        if not AnalizarRendicionesTxt() then begin
            FErrorGrave := True;
            Exit;
        end;
        }

        //Registro la operaci�n en el Log
        Lblmensaje.Caption := STR_REGISTER_OPERATION;
        if not RegistrarOperacion() then begin
            FErrorGrave := True;
            Exit;
        end;

        //Proceso el archivo de Rendiciones
        Lblmensaje.Caption := STR_PROCESS;
        if not ActualizarRendiciones()then begin
            FErrorGrave := True;
            Exit;
        end;

        // grabar los errores encontrados                                           // SS_1147_MBE_20150413
        GrabarErrores();                                                            // SS_1147_MBE_20150413

        //Obtengo la cantidad de errores
        ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);

        //Actualizo el log al Final
        ActualizarLog(CantidadErrores);

  	finally
        pbProgreso.Position := 0;  //Inicio la Barra de progreso
        PnlAvance.visible := False;  //Oculto el Panel de Avance
        Lblmensaje.Caption := '';
        if FCancelar then begin

            //registro que el proceso fue cancelado
            RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacion);

            //Si fue cancelado por el usuario muestro mensaje que el proceso fue cancelado
            MsgBox(MSG_PROCESS_CANCEL, self.Caption, MB_OK + MB_ICONINFORMATION);
        end else if FErrorGrave then begin
            //Si se produjo un error grave muestro un cartel indicando que hubo errores
            MsgBox(MSG_PROCESS_NOT_COMPLETE, self.Caption, MB_OK + MB_ICONEXCLAMATION);
            //Genero el Reporte de Error con el Report Builder
            if (Ferrores.Count > 0) then GenerarReporteErroresSintaxis;
        end else begin

            //Obtengo Resumen del Proceso
            ObtenerResumen (FCodigoOperacion, CantRegs, Aprobados, Rechazados);

            //Muestro Mensaje de Finalizaci�n
            //if MsgProcesoFinalizado(MSG_PROCESS_FINALLY_OK , MSG_PROCESS_FINALLY_WITH_ERROR , Caption , CantRegs , Aprobados , Rechazados) then begin
            //Revisi�n 3
            MsgProcesoFinalizado(MSG_PROCESS_FINALLY_OK , MSG_PROCESS_FINALLY_WITH_ERROR , Caption , CantRegs , Aprobados , Rechazados );
                //Genero el reporte de finalizacion de proceso
                GenerarReportedeFinalizacion(FCodigoOperacion);
            //end;

        end;

    	btnProcesar.Enabled     := True;
    	btnCancelar.Enabled     := False;
        btnAbrirArchivo.Enabled := True;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesServipag.btnSalirClick(Sender: TObject);
begin
  	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: impido salir si esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesServipag.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando
    CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 20/10/2005
  Description: lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesServipag.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFRecepcionRendicionesServipag.btnCancelarClick(Sender: TObject);
begin
	FCancelar := True;
end;

initialization
    FLista   := TStringList.Create;
    FErrores := TStringList.Create;
finalization
    FreeAndNil(Flista);
    FreeAndNil(FErrores);
end.


