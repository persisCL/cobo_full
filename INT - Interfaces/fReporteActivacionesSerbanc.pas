
{********************************** Unit Header ********************************
File Name : fReporteActivacionesSerbanc.pas
Author : ndonadio
Date Created: 22/09/2005
Language : ES-AR
Description : Prepara y lanza el reporte de finalizacion de la interfaz
                de envio de Activaciones para Serbanc
*******************************************************************************}
unit fReporteActivacionesSerbanc;

interface

uses
 // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils,
    // Parametros GEnerales
    ConstParametrosGenerales,
   // DB
    UtilDB, DMConnection,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB, ppDBPipe, ppPrnabl, ppClass,
  ppCtrls, ppBands, ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB;


type
  TfrmReporteActivacionesSerbanc = class(TForm)
    ppReportGeneracionActivaciones: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    pplblUsuarioProceso: TppLabel;
    pplblProceso: TppLabel;
    pplblFechaProceso: TppLabel;
    ppLabel: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLine1: TppLine;
    ppDetailBand1: TppDetailBand;
    ppGroupCobranza: TppGroup;
    ppGroupHeaderBandCobranza: TppGroupHeaderBand;
    ppDBText1: TppDBText;
    ppGroupFooterBandCobranza: TppGroupFooterBand;
    ppDBCalcNOtas: TppDBCalc;
    ppDBCalcMonto: TppDBCalc;
    ppDBCalcPat: TppDBCalc;
    ppDBText6: TppDBText;
    ppLabel9: TppLabel;
    ppGroupDeudor: TppGroup;
    ppGroupHeaderBandDeudor: TppGroupHeaderBand;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppGroupFooterBandDeudor: TppGroupFooterBand;
    RBI: TRBInterface;
    ppDBPConsumos: TppDBPipeline;
    ppDBPConsumosppField1: TppField;
    ppDBPConsumosppField2: TppField;
    ppDBPConsumosppField3: TppField;
    ppDBPConsumosppField4: TppField;
    ppDBPConsumosppField5: TppField;
    ppDBPConsumosppField6: TppField;
    dsConsumos: TDataSource;
    spObtenerDatosReporteEnvioActivacionesSerbanc: TADOStoredProc;
    procedure RBIExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FTitulo: string;
    FCodigoOperacionInterfase: Integer;
    FError : AnsIString;
    function PrepararReporteActivacionesSerbanc(var Error: AnsiString): Boolean;
  public
    { Public declarations }
    function MostrarReporte(CodigoOperacionInterfase: Integer; Titulo: AnsiString; var Error: AnsiString): boolean;
  end;

var
  frmReporteActivacionesSerbanc: TfrmReporteActivacionesSerbanc;

implementation

{$R *.dfm}

{ TForm1 }

function TfrmReporteActivacionesSerbanc.MostrarReporte(CodigoOperacionInterfase: Integer; Titulo: AnsiString; var Error: AnsiString): boolean;
resourcestring
    MSG_CANCELED = 'Ejecuci�n del reporte cancelada por el usuario';
begin
    Result := False;
    FCodigoOperacionInterfase := CodigoOperacionInterfase;
    try
        RBI.Caption := 'Reporte de ' + Titulo;
        if not RBI.Execute(True) then begin
            Error := FError;
            Exit;
        end;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
            Exit;
        end;
    end;
end;

procedure TfrmReporteActivacionesSerbanc.RBIExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_THERE_IS_NOTHING_TO_REPORT  = 'No hay datos para generar el reporte';
var
    DescError: AnsiString;
    FechaProceso: TDateTime;
begin
    if not PrepararReporteActivacionesSerbanc(DescError) then begin
        if trim(DescError) = '' then FError := MSG_THERE_IS_NOTHING_TO_REPORT
        else FError := descError;
        Cancelled := True;
        Exit;
    end;
    CurrencyFormat := 2;
    CurrencyString := '$'; // para forzar el signo pesos... es local a la istancia de la aplicacion.
    ThousandSeparator := '.';
    pplblUsuarioProceso.Caption := 'Usuario Responsable: ' + QueryGetValue(DMConnections.BaseCAC, Format('SELECT Usuario FROM LogOperacionesInterfases (NOLOCK) WHERE CodigoOperacionInterfase = %d', [FCodigoOperacionInterfase]));
    FechaProceso := QueryGetValueDateTime(DMConnections.BaseCAC, Format('SELECT Fecha FROM LogOperacionesInterfases (NOLOCK) WHERE CodigoOperacionInterfase = %d', [FCodigoOperacionInterfase]));
    pplabel1.Caption := FTitulo;
    pplblProceso.Caption := Format( 'Proceso N� %d', [FCodigoOperacionInterfase]);
    pplblFechaProceso.Caption := Format( 'Fecha de Proceso: %s; Hora: %s',[FormatDateTime('dd-mm-yyyy', FechaProceso),FormatDateTime('HH:nn', FechaProceso)]);
end;

{******************************** Function Header ******************************
Function Name: PrepararReporte
Author : ndonadio
Date Created : 09/08/2005
Description :  Esta funcion levanta los datos para el reporte.
               En realidad aca podria haberse hecho algo mas eficiente,
                pero como el reporte debe ser levantado a posteriori de
                la ejecuci�n de interface, es mejor que se haga siempre igual.
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TfrmReporteActivacionesSerbanc.PrepararReporteActivacionesSerbanc(var Error: AnsiString): Boolean;
begin
    Result := False;
    FTitulo := 'Proceso de Asignaci�n de Cobranzas';
    try
        spObtenerDatosReporteEnvioActivacionesSerbanc.Close;
        spObtenerDatosReporteEnvioActivacionesSerbanc.Parameters.Refresh;
        spObtenerDatosReporteEnvioActivacionesSerbanc.Parameters.ParamByName('@CodigoInterfase').Value := FCodigoOperacionInterfase;
        spObtenerDatosReporteEnvioActivacionesSerbanc.Parameters.ParamByName('@NombreEmpresaRecaudadora').Value := NULL;
        spObtenerDatosReporteEnvioActivacionesSerbanc.Open;
        if Trim(spObtenerDatosReporteEnvioActivacionesSerbanc.Parameters.ParamByName('@NombreEmpresaRecaudadora').Value) <> '' then
            FTitulo := FTitulo + ' a ' + Trim(spObtenerDatosReporteEnvioActivacionesSerbanc.Parameters.ParamByName('@NombreEmpresaRecaudadora').Value);
        Result := not spObtenerDatosReporteEnvioActivacionesSerbanc.IsEmpty;
        if not Result then Error := '';
    except
        on e:exception do begin
            Error := e.Message;
        end;
    end;
end;
end.
