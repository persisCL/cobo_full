object FGenerarArchivoTransaccionesRechazadas: TFGenerarArchivoTransaccionesRechazadas
  Left = 0
  Top = 0
  Caption = 'Transacciones Rechazadas Otras Concesionarias'
  ClientHeight = 293
  ClientWidth = 722
  Color = clBtnFace
  Constraints.MinHeight = 331
  Constraints.MinWidth = 738
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnKeyDown = FormKeyDown
  DesignSize = (
    722
    293)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 7
    Top = 9
    Width = 707
    Height = 233
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object lblFechaDesde: TLabel
    Left = 203
    Top = 17
    Width = 62
    Height = 13
    Caption = 'Fecha Desde'
  end
  object lblFechaHasta: TLabel
    Left = 309
    Top = 17
    Width = 60
    Height = 13
    Caption = 'Fecha Hasta'
  end
  object lblConcesionaria: TLabel
    Left = 16
    Top = 17
    Width = 67
    Height = 13
    Caption = 'Concesionaria'
  end
  object lblNombreArchivo: TLabel
    Left = 16
    Top = 61
    Width = 93
    Height = 13
    Caption = 'Nombre del Archivo'
  end
  object edFechaDesde: TDateEdit
    Left = 203
    Top = 36
    Width = 100
    Height = 21
    AutoSelect = False
    TabOrder = 1
    OnChange = edFechaDesdeChange
    Date = -693594.000000000000000000
  end
  object edFechaHasta: TDateEdit
    Left = 309
    Top = 36
    Width = 100
    Height = 21
    AutoSelect = False
    TabOrder = 2
    OnChange = edFechaHastaChange
    Date = -693594.000000000000000000
  end
  object btnSalir: TButton
    Left = 639
    Top = 254
    Width = 75
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 6
    OnClick = btnSalirClick
  end
  object btnGenerar: TButton
    Left = 527
    Top = 254
    Width = 98
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = '&Generar Archivo'
    Enabled = False
    TabOrder = 5
    OnClick = btnGenerarClick
  end
  object vcbConcesionarias: TVariantComboBox
    Left = 16
    Top = 36
    Width = 181
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = vcbConcesionariasChange
    Items = <>
  end
  object txtArchivo: TPickEdit
    Left = 16
    Top = 80
    Width = 393
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    Enabled = True
    MaxLength = 255
    TabOrder = 7
    EditorStyle = bteTextEdit
    OnButtonClick = txtArchivoButtonClick
  end
  object PnlAvance: TPanel
    Left = 16
    Top = 107
    Width = 647
    Height = 124
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = 'Presione la tecla escape [Esc] para cancelar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    VerticalAlignment = taAlignTop
    DesignSize = (
      643
      120)
    object lblMensaje: TLabel
      Left = 5
      Top = 47
      Width = 37
      Height = 13
      Caption = 'Estado:'
    end
    object lblProgreso: TLabel
      Left = 5
      Top = 24
      Width = 61
      Height = 17
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Progreso'
      Constraints.MaxHeight = 17
      Constraints.MaxWidth = 61
      Constraints.MinHeight = 17
      Constraints.MinWidth = 61
    end
    object pbProgreso: TProgressBar
      Left = 72
      Top = 24
      Width = 468
      Height = 9
      Anchors = [akLeft, akTop, akRight, akBottom]
      Smooth = True
      Step = 1
      TabOrder = 0
      ExplicitWidth = 472
      ExplicitHeight = 13
    end
  end
  object rdgTipoTransaccion: TRadioGroup
    Left = 424
    Top = 17
    Width = 281
    Height = 88
    Anchors = [akTop, akRight]
    Caption = 'Tipo Transacci'#243'n'
    Columns = 2
    Items.Strings = (
      'Transitos'
      'Estacionamientos'
      'Movimientos Cuentas'
      'Saldo Inicial'
      'Cuotas')
    TabOrder = 3
    OnClick = rdgTipoTransaccionClick
  end
  object btnObtener: TButton
    Left = 424
    Top = 254
    Width = 89
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = '&Obtener Datos'
    TabOrder = 4
    OnClick = btnObtenerClick
  end
  object dblRechazados: TDBListEx
    Left = 16
    Top = 107
    Width = 689
    Height = 124
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <>
    DragReorder = True
    ParentColor = False
    TabOrder = 9
    TabStop = True
    Visible = False
  end
  object qryConsulta: TADOQuery
    Parameters = <>
    Left = 48
    Top = 248
  end
  object spObtenerEstacionamientosRechazados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerEstacionamientosRechazados'
    Parameters = <>
    Left = 80
    Top = 248
  end
  object spAgregaHistoricoEstacionamientosRechazadosEnviados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregaHistoricoEstacionamientosRechazadosEnviados'
    Parameters = <>
    Left = 112
    Top = 248
  end
  object spObtenerTransaccionesMovimientosRechazados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTransaccionesMovimientosRechazados'
    Parameters = <>
    Left = 144
    Top = 248
  end
  object dsFuenteDatosGrilla: TDataSource
    Left = 176
    Top = 248
  end
  object spRechazosGenerico: TADOStoredProc
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 208
    Top = 248
  end
  object dlgSelArchivo: TSaveDialog
    DefaultExt = 'txt'
    Filter = 'Archivo de Texto|*.txt'
    FilterIndex = 0
    Left = 16
    Top = 248
  end
end
