object formListaErrores: TformListaErrores
  Left = 303
  Top = 295
  BorderStyle = bsDialog
  Caption = 'Observaciones y Errores de las Consultas al RNVM'
  ClientHeight = 230
  ClientWidth = 712
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 207
    Width = 249
    Height = 13
    Caption = 'Si desea guardar este detalle de errores haga "click"'
  end
  object lblaca: TLabel
    Left = 266
    Top = 206
    Width = 18
    Height = 13
    Cursor = crHandPoint
    Caption = 'ac'#225
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentColor = False
    ParentFont = False
    OnClick = lblacaClick
  end
  object mem_ListaErrores: TMemo
    Left = 8
    Top = 12
    Width = 689
    Height = 181
    TabStop = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object btnCerrar: TButton
    Left = 624
    Top = 202
    Width = 75
    Height = 25
    Caption = '&Cerrar'
    TabOrder = 1
    OnClick = btnCerrarClick
  end
  object Save: TSaveDialog
    Filter = 'Archivos de Texto|*.txt|Todos los Archivos|*.*'
    InitialDir = '.'
    Title = 'Guardar Reporte de Errores'
    Left = 288
    Top = 200
  end
end
