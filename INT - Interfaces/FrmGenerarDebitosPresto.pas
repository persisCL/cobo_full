{-----------------------------------------------------------------------------
 File Name: FrmGenerarDebitosPresto.pas
 Author:    flamas
 Date Created: 03/01/2005
 Language: ES-AR
 Description: Modulo de la interfaz Presto - Generaci�n de Debitos

 	Revision 1
	Autor: sguastoni
	Fecha: 05/01/2007
	Descripcion: se agrega parametro a sp  spActualizarComprobantesEnviadosPresto

   Revision :
       Author : vpaszkowicz
       Date : 16/07/2007
       Description : Cambio la forma de proceso. En lugar de usar el store
       spActualizarComprobantesEnviadosSantander uso una funci�n implementada
       en ComunesInterfaces y la llamo por cada comprobante que voy insertando
       en el archivo, dado que ya los tengo calculados.

    Revision : 3
        Author : jconcheyro
        Date : 08/08/2007
        Description : Agrego la lista de Grupos de facturacion para pasarle como
        parametro al store un lista con los grupos a tener en cuenta

  Revision : 4
    Date: 03/03/2009
    Author: mpiazza
    Description:    Ref (SRX01226) se cambia" now " por nowbase

  Revision : 5
    Date: 13/04/2009
    Author: mpiazza
    Description: ss750 se actualiza el log de interfase con el monto
                 y cantidad de registros totales para el reporte

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       :   SS_1147_CQU_20150519
Descripcion :   Se agrega el TipoComprobante a la funci�n ActualizarDetalleComprobanteEnviado
  
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

-----------------------------------------------------------------------------}
unit FrmGenerarDebitosPresto;

interface

uses
  //Debitos Presto
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  PeaTypes,
  UtilDB,
  ComunesInterfaces,
  FrmRptEnvioComprobantes,       //Reporte de Comprobantes Enviados
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, BuscaTab, DmiCtrls, DPSControls, VariantComboBox, StrUtils,
  DBClient, ImgList;


type

  TFGeneracionDebitosPresto = class(TForm)
	Bevel1: TBevel;
    edDestino: TEdit;
    OpenDialog: TOpenDialog;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    spObtenerComprobantesPresto: TADOStoredProc;
    lblFechaInterface: TLabel;
    spObtenerInterfasesEjecutadas: TADOStoredProc;
    edFecha: TBuscaTabEdit;
    buscaInterfaces: TBuscaTabla;
    spObtenerInterfasesEjecutadasFecha: TDateTimeField;
    spObtenerInterfasesEjecutadasDescripcion: TStringField;
    spObtenerInterfasesEjecutadasCodigoOperacionInterfase: TIntegerField;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    lblNombreArchivo: TLabel;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    lblUltimosGruposFacturados: TLabel;
    dblGrupos: TDBListEx;
    dsGrupos: TDataSource;
    ilImages: TImageList;
    cdGrupos: TClientDataSet;
    spObtenerUltimosGruposFacturados: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    function  buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);
    procedure edFechaChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
      procedure dblGruposDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblGruposDblClick(Sender: TObject);
  private
    { Private declarations }
        FCodigoOperacion : integer;
        FCodigoOperacionAnterior : integer;
        FDetenerImportacion: boolean;
        FDebitosTXT	: TStringList;
        FNumeroEnvio : integer;
        FErrorMsg : string;
        FObservaciones : string;
        sFechaGlosa : string;
        FPresto_Directorio_Destino_Debitos : Ansistring;
        FPresto_CodigodeComercio : AnsiString;
        FMontoTotal     : Int64;
        FCantidadTotal  : Int64;
        FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
        function  GenerarDebitosPresto : boolean;
        function  RegistrarOperacion : boolean;
//        function  ActualizarDebitosPresto : boolean;
        Function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
        Function  CrearNombreArchivo(var NombreArchivo: AnsiString; dFecha : TDateTime): Boolean;
        function CargarGrupos: Boolean;
        function GenerarListaGruposAIncluir(var Lista: string): boolean;
        function ExistenGruposSeleccionados: boolean;
        function GruposSeleccionadosEstanTerminados: boolean;

    public

        function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean; bReprocesar : boolean = False ) : Boolean;
	{ Public declarations }
  end;

var
  FGeneracionDebitosPresto: TFGeneracionDebitosPresto;

Const
	RO_MOD_INTERFAZ_SALIENTE_DEBITOS_PRESTO			= 32;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 25/11/2004
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGeneracionDebitosPresto.Inicializar(txtCaption: ANSIString; MDIChild:Boolean; bReprocesar : boolean = False ) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 19/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        PRESTO_CODIGODECOMERCIO           = 'Presto_CodigodeComercio';
        PRESTO_DIRECTORIO_DESTINO_DEBITOS = 'Presto_Directorio_Destino_Debitos';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_CODIGODECOMERCIO , FPresto_CodigodeComercio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_CODIGODECOMERCIO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FPresto_CodigodeComercio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + PRESTO_CODIGODECOMERCIO;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_DIRECTORIO_DESTINO_DEBITOS , FPresto_Directorio_Destino_Debitos) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_DIRECTORIO_DESTINO_DEBITOS;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FPresto_Directorio_Destino_Debitos := GoodDir(FPresto_Directorio_Destino_Debitos);
                if  not DirectoryExists(FPresto_Directorio_Destino_Debitos) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FPresto_Directorio_Destino_Debitos;
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;


resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
	MSG_DEBITS_PROCESSING = 'Procesamiento de d�bitos';
	MSG_DEBITS_REPROCESSING = 'Re-procesamiento de d�bitos';
	MSG_ERROR_FILE_ALREADY_CREATED = 'El archivo %s ya fue generado';
var
    NombreArchivo: AnsiString;
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

	if not MDIChild then begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');
    lblReferencia.Caption := '';
	try
		DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and
                            VerificarParametrosGenerales;
	except
		on e: Exception do begin
			MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
			Exit;
		end;
	end;

	lblFechaInterface.Visible := bReprocesar;
	edFecha.Visible := bReprocesar;

    if not CargarGrupos then begin
        Result := False;
        Exit;
    end;

    if not bReprocesar then begin
    	lblNombreArchivo.Top := 13;
		edDestino.Top := 32;
        FObservaciones := MSG_DEBITS_PROCESSING;
		btnProcesar.Enabled := DirectoryExists( FPresto_Directorio_Destino_Debitos ); // Verifica si existe el Directorio de Pagomatico

        //Creo el nombre del archivo a enviar
        CrearNombreArchivo(NombreArchivo, Now);
        eddestino.Text:= NombreArchivo;

        if  VerificarArchivoProcesado( DMConnections.BaseCAC, edDestino.text ) then begin
    		MsgBox( Format ( MSG_ERROR_FILE_ALREADY_CREATED, [ExtractFileName(edDestino.text) ]), MSG_ERROR, MB_ICONERROR );
        	Exit;
        end;

	end else begin
    	FObservaciones := MSG_DEBITS_REPROCESSING;
        spObtenerInterfasesEjecutadas.Parameters.ParamByName( '@CodigoModulo' ).Value := RO_MOD_INTERFAZ_SALIENTE_DEBITOS_PRESTO;
          spObtenerInterfasesEjecutadas.CommandTimeout := 500;
        spObtenerInterfasesEjecutadas.Open;
	end;

	btnCancelar.Enabled := False;
	pnlAvance.Visible := False;


	result := True;
end;


{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFGeneracionDebitosPresto.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_DEBITOS         = ' ' + CRLF +
                          'El Archivo de Debitos es' + CRLF +
                          'utilizado por el ESTABLECIMIENTO para informar a PRESTO' + CRLF +
                          'el detalle de los pagos a recaudar' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: DEB1DDMM1D' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_DEBITOS, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TFGeneracionDebitosPresto.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;



// revision 3
{******************************** Function Header ******************************
Function Name: CargarGrupos
Author :
Date Created :
Description :
Parameters : none
Return Value : Boolean
-----------------------------------------------------------------------------
  Revision :1
  Date: 23/09/2009
  Author: mpiazza
  Description: ss750 se incrementa a 200 segundos  el comandTimeOut
                    de ObtenerUltimosGruposFacturados
*******************************************************************************}
function TFGeneracionDebitosPresto.CargarGrupos: Boolean;
begin
    Result := False;
    spObtenerUltimosGruposFacturados.Close;

    try
        cdGrupos.CreateDataSet;
        spObtenerUltimosGruposFacturados.CommandTimeout := 200;
        spObtenerUltimosGruposFacturados.Open;
        Result := not spObtenerUltimosGruposFacturados.IsEmpty;
        while not spObtenerUltimosGruposFacturados.Eof do begin
            // Insertamos nuestro registro
            cdGrupos.AppendRecord([ False,
                                    spObtenerUltimosGruposFacturados.FieldByName('CodigoGrupoFacturacion').AsInteger,
                                    spObtenerUltimosGruposFacturados.FieldByName('FechaProgramadaEjecucion').AsDateTime,
                                    spObtenerUltimosGruposFacturados.FieldByName('FechaCorte').AsDateTime,
                                    spObtenerUltimosGruposFacturados.FieldByName('FechaEmision').AsDateTime,
                                    spObtenerUltimosGruposFacturados.FieldByName('FechaVencimiento').AsDateTime,
                                    spObtenerUltimosGruposFacturados.FieldByName('NoTerminados').AsInteger
                                  ]);
            spObtenerUltimosGruposFacturados.Next;
        end;
        spObtenerUltimosGruposFacturados.Close;
        cdGrupos.First;
    except
        on e: Exception do begin
            MsgBoxErr('Error obteniendo los �ltimos grupos facturados', e.Message, Caption, MB_ICONERROR);
            Result:= False;
        end;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: CrearNombreArchivo
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: Creo el nombre del archivo a enviar
  Parameters: var NombreArchivo: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFGeneracionDebitosPresto.CrearNombreArchivo(var NombreArchivo: AnsiString; dFecha : TDateTime): Boolean;
Const
    FILE_NAME = 'DEB1';
    FILE_DATE_FORMAT = 'ddmm';
    FILE_EXTENSION = '';
    FILE_APPEND = '1D';
begin
    Result:= False;
    try
        NombreArchivo := GoodFileName( FPresto_Directorio_Destino_Debitos + FILE_NAME + FormatDateTime ( FILE_DATE_FORMAT , dFecha ) + FILE_APPEND, FILE_EXTENSION );
        sFechaGlosa := FormatDateTime('yymmdd', dFecha);
        Result:= True;
    except
    end;
end;

procedure TFGeneracionDebitosPresto.dblGruposDblClick(Sender: TObject);
begin
    cdGrupos.Edit;
    if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
        cdGrupos.FieldByName('Seleccionado').AsBoolean := False;
    end else begin
        if ( cdGrupos.FieldByName('NoTerminados').AsInteger > 0 ) then
        begin
            ShowMessage('Este Proceso de Facturacion tiene comprobantes no terminados');
            cdGrupos.FieldByName('Seleccionado').AsBoolean := False;
        end
        else
            cdGrupos.FieldByName('Seleccionado').AsBoolean := True;
    end;
    cdGrupos.Post;

end;

procedure TFGeneracionDebitosPresto.dblGruposDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    if Column.FieldName = 'Seleccionado' then begin
        if cdGrupos.FieldByName('Seleccionado').AsBoolean = True then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 1);
          end else if cdGrupos.FieldByName('Seleccionado').AsBoolean = False then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 0);
          end;
          DefaultDraw := False;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: edFechaChange
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: En caso de Reprocesar esta interfaz
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosPresto.edFechaChange(Sender: TObject);
begin
   btnProcesar.Enabled := ( edFecha.Text <> '' );
end;


// Revision 3
function TFGeneracionDebitosPresto.ExistenGruposSeleccionados: boolean;
begin
    Result := False;
    try
        cdGrupos.First;
        while not cdGrupos.Eof do begin
            if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
                Result := True;
                cdGrupos.First;
                Break;
            end;
            cdGrupos.Next;
        end;
        cdGrupos.First;
    except
        on E: Exception do begin
            MsgBoxErr('Error contando grupos para la generaci�n ', e.Message, Caption, MB_ICONERROR);
        end;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfasesProcess
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: En caso de Reprocesar esta interfaz
  Parameters: Tabla: TDataSet; var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGeneracionDebitosPresto.buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Result := True;
	with Tabla do
    	Texto := FieldByName('Descripcion').AsString + ' - ' + FieldByName('Fecha').AsString;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfasesSelect
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: En caso de Reprocesar esta Interfaz
  Parameters: Sender: TObject; Tabla: TDataSet
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosPresto.buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);

    {-----------------------------------------------------------------------------
      Function Name: InvertirFecha
      Author:    flamas
      Date Created: 09/12/2004
      Description: Invierte el formato de la fecha para generar el archivo de d�bbitos
      Parameters: sFecha : string
      Return Value: string
    -----------------------------------------------------------------------------}
    function InvertirFecha( sFecha : string ) : string;
    begin
    	result := Copy( sFecha, 7, 4 ) + Copy( sFecha, 4, 2 ) + Copy( sFecha, 1, 2);
    end;
var
    NombreArchivo:Ansistring;
begin
	edFecha.Text := FormatDateTime( 'dd-mm-yyyy', Tabla.FieldByName('Fecha').AsDateTime);
    FCodigoOperacionAnterior := Tabla.FieldByName('CodigoOperacionInterfase').Value;

    //Creo el nombre del archivo a enviar
    CrearNombreArchivo(NombreArchivo, Tabla.FieldByName('Fecha').AsDateTime);
    eddestino.Text:= NombreArchivo;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 03/12/2004
  Description: Registra la Operaci�n en el registro de Operaciones
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TFGeneracionDebitosPresto.RegistrarOperacion : boolean;
resourcestring
	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
	DescError : string;
begin
   result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_SALIENTE_DEBITOS_PRESTO, ExtractFileName(edDestino.text), UsuarioSistema, FObservaciones, False, edFecha.Visible, NowBase(DMConnections.BaseCAC), FNumeroEnvio, FCodigoOperacion, DescError );
   if not result then
    	MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarDebitosPresto
  Author:    flamas
  Date Created: 03/01/2005
  Description: Genera el archivo de d�bitos de Presto
  Parameters: None
  Return Value: boolean
  Revision : 2
      Author : vpaszkowicz
      Date : 16/07/2007
      Description : Subo a esta parte el registro del comprobante usado ya que
      tengo todos los comprobantes en el store y no es necesario volverlos a
      calcular.
-----------------------------------------------------------------------------
  Revision :3
  Date: 13/04/2009
  Author: mpiazza
  Description: ss750 se actualiza el log de interfase con el monto
               y cantidad de registros totales para el reporte
-----------------------------------------------------------------------------}
function TFGeneracionDebitosPresto.GenerarDebitosPresto : boolean;

    {-----------------------------------------------------------------------------
      Function Name: HeaderDebitos
      Author:    flamas
      Date Created: 03/01/2005
      Description: Arma el Encabezado de D�bitos Presto
      Parameters: None
      Return Value: string
    -----------------------------------------------------------------------------}
    function  HeaderDebitos : string;
    resourcestring
        //SENT_DEBITS_DESCRIPTION = 'D�bitos enviados por Costanera Norte - ';  //SS_1147_MCA_20140408 //OK  Revisado: 15/03/05
        SENT_DEBITS_DESCRIPTION = 'D�bitos enviados por %s - '; 	 			 //SS_1147_MCA_20140408
    var
    	NombreConcesionaria: string;
    begin
    	NombreConcesionaria := QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM Concesionarias WITH (NOLOCK) WHERE CodigoConcesionaria = dbo.ObtenerConcesionariaNativa()');			//SS_1147_MCA_20140408

        result := PadR('HEADER '+PADL(FPresto_CodigodeComercio, 6, '0')+' ' + //Codigo Empresa Prestadora
                  //SENT_DEBITS_DESCRIPTION + sFechaGlosa, 300, ' ');			//SS_1147_MCA_20140408
                  Format(SENT_DEBITS_DESCRIPTION, [NombreConcesionaria]) + sFechaGlosa, 300, ' ');	//SS_1147_MCA_20140408
    end;

    {-----------------------------------------------------------------------------
      Function Name: ArmarLineaTXT
      Author:    flamas
      Date Created: 04/01/2005
      Description:	Arma la l�nea del TXT
      Parameters: None
      Return Value: string
    -----------------------------------------------------------------------------}
    function ArmarLineaTXT : string;
    resourcestring
        DEBIT_DESCRIPTION = 'COST. NORTE N/C %d'; //OK  Revisado: 15/03/05
    begin
        with spObtenerComprobantesPresto do begin
            result := PADR( IntToStr( FNumeroEnvio ), 10, ' ' ) +
                        PADL(FPresto_CodigodeComercio, 6, '0') + //Codigo Empresa Prestadora
                        FormatDateTime( 'yymmdd', FieldByName( 'FechaEmision' ).AsDateTime ) +
                        PADR( Format( DEBIT_DESCRIPTION, [FieldByName( 'NumeroComprobante' ).AsInteger]), 26, ' ' ) +
                        PADR( Trim(FieldByName( 'NumeroTarjeta' ).AsString), 19, ' ' ) +
                        PADR( Trim(FieldByName( 'NumeroConvenio' ).AsString), 17, ' ' ) +
                        PADL( Trim(FieldByName( 'Importe' ).AsString), 10, '0' ) +
                        PADR( Trim(FieldByName( 'NumeroComprobante' ).AsString), 12, ' ' );
            result := 	PADR( result, 300, ' ');

            FMontoTotal     := FMontoTotal + FieldByName('Importe').AsInteger;
            inc( FCantidadTotal)  ;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: FooterDebitos
      Author:    flamas
      Date Created: 04/01/2005
      Description: Arma el Footer de D�bitos Presto
      Parameters: None
      Return Value: string
    -----------------------------------------------------------------------------}
    function  FooterDebitos : string;
    begin
        result := PADR('FOOTER ' + PadL(IntToStr(FDebitosTXT.Count - 1 ), 6, '0'), 300, ' ');
    end;

resourcestring
	MSG_NO_PENDING_DEBITS 	 			= 'No hay d�bitos pendientes de env�o';
	MSG_COULD_NOT_CREATE_FILE			= 'No se pudo crear el archivo de d�bitos';
	MSG_COULD_NOT_GET_PENDING_DEBITS 	= 'No se pudieron obtener los d�bitos pendientes';
    MSG_GENERATING_DEBITS_FILE			= 'Generando archivo de d�bitos - Cantidad de d�bitos : %d ';
    MSG_PROCESSING			 			= 'Procesando...';
    MSG_OBTAINING_DATA			 		= 'Obteniendo datos a enviar al archivo...';
var
    ListaGrupos: string;
begin
	try
    	Screen.Cursor := crHourglass;
		result := False;
        lblReferencia.Caption := MSG_OBTAINING_DATA;
        Application.ProcessMessages;
	    try

            if not GenerarListaGruposAIncluir( ListaGrupos ) then Exit ;
            spObtenerComprobantesPresto.Parameters.ParamByName( '@ListaGruposAIncluir' ).Value := ListaGrupos;

	    	spObtenerComprobantesPresto.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value := Iif (FCodigoOperacionAnterior = 0, NULL, FCodigoOperacionAnterior );
            spObtenerComprobantesPresto.CommandTimeout := 5000;
    		spObtenerComprobantesPresto.Open;
            if spObtenerComprobantesPresto.IsEmpty then begin
            	MsgBox(MSG_NO_PENDING_DEBITS, Caption, MB_ICONWARNING);
                FErrorMsg := MSG_NO_PENDING_DEBITS;
        		Screen.Cursor := crDefault;
                Exit;
            end;

        except
        	on e: Exception do begin
	        		MsgBoxErr(MSG_COULD_NOT_GET_PENDING_DEBITS, e.Message, 'Error', MB_ICONERROR);
                	FErrorMsg := MSG_COULD_NOT_GET_PENDING_DEBITS;
                    Exit;
            end;
        end;

    		// Obtiene el N�mero Consecutivo de D�bitos - Para Procesamiento o Re-Procesamiento
        if not edFecha.Visible then
        	FNumeroEnvio := QueryGetValueInt(DMConnections.BaseCAC,format('select dbo.ObtenerUltimoRegistroEnviadoInterfase (NULL, %d)',
            					[RO_MOD_INTERFAZ_SALIENTE_DEBITOS_PRESTO])) + 1
        else
        	FNumeroEnvio := QueryGetValueInt(DMConnections.BaseCAC,format('select dbo.ObtenerUltimoRegistroEnviadoInterfase (%d, %d)',
            					[FCodigoOperacionAnterior,RO_MOD_INTERFAZ_SALIENTE_DEBITOS_PRESTO]));

        // Agrega el encabezado al archivo de d�bitos
        FDebitosTXT.Add( HeaderDebitos );

        lblReferencia.Caption := Format( MSG_GENERATING_DEBITS_FILE, [spObtenerComprobantesPresto.RecordCount] );
        pbProgreso.Position := 0;
        pnlAvance.Visible := True;
        lblReferencia.Caption := MSG_PROCESSING;
        Application.ProcessMessages;
        with spObtenerComprobantesPresto do begin
        	pbProgreso.Max := spObtenerComprobantesPresto.RecordCount;
            try
            while ( not Eof ) and ( not FDetenerImportacion ) do begin
                //Genero cada linea del txt
            	FDebitosTXT.Add( ArmarLineaTXT );
                    if not ActualizarDetalleComprobanteEnviado(DMConnections.BaseCAC,
                        FCodigoOperacion, spObtenerComprobantesPresto.FieldByName('NumeroComprobante').Value,
                        spObtenerComprobantesPresto.FieldByName('TipoComprobante').AsString,    // SS_1147_CQU_20150519
                        FErrorMsg,spObtenerComprobantesPresto.FieldByName('Importe').Value) then
                            raise Exception.Create(FErrorMsg);
                pbProgreso.StepIt;
                Application.ProcessMessages;
                Next;
            end;
            except
        	    on e: Exception do begin
                    FErrorMsg := MSG_COULD_NOT_GET_PENDING_DEBITS;
                    MsgBoxErr(MSG_COULD_NOT_GET_PENDING_DEBITS, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        end;

        if ( not FDetenerImportacion ) and ( FErrorMsg = '' ) then begin
        	try
				FDebitosTXT.Add( FooterDebitos );
            	FDebitosTXT.SaveToFile( edDestino.Text );
                result := True;
            except
            	on e: Exception do begin
                	MsgBoxErr(MSG_COULD_NOT_CREATE_FILE, e.Message, 'Error', MB_ICONERROR);
                    FErrorMsg := MSG_COULD_NOT_CREATE_FILE;
                end;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;


// Revision 3
function TFGeneracionDebitosPresto.GenerarListaGruposAIncluir(
  var Lista: string): boolean;
var
    ListaTemporal :TStringList;
    I: integer;
begin
    Result := False;
    ListaTemporal := TStringList.Create;
    try
        try
            ListaTemporal.Duplicates := dupIgnore; // la lista ignora los duplicados
            cdGrupos.First;
            while not cdGrupos.Eof do begin
                if cdGrupos.FieldByName('Seleccionado').AsBoolean then
                    ListaTemporal.Add( IntToStr(cdGrupos.FieldByName('GrupoFacturacion').AsInteger));
                cdGrupos.Next;
            end;
            cdGrupos.First;
            for I := 0 to ListaTemporal.Count - 1 do begin
                Lista := Lista + ListaTemporal[I];
                if (I < (ListaTemporal.Count -1) ) then   // asi no me agrega una coma al final
                    Lista := Lista + ','
            end;
            Result := True;
        except
            on E: Exception do begin
                MsgBoxErr('Error generando par�metro con lista de grupos para el store', e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
            ListaTemporal.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarDebitosPresto
  Author:    flamas
  Date Created: 03/01/2005
  Description:
  Parameters: None
  Return Value: None

  Revision : 2
      Author : vpaszkowicz
      Date : 16/07/2007
      Description : Comento este c�digo porque de ahora en mas se hace por
      l�nea del archivo, ya que tengo los d�bitos dentro del primer store y
      no es necesario volver a calcularlos.
-----------------------------------------------------------------------------}
{function TFGeneracionDebitosPresto.ActualizarDebitosPresto : boolean;
resourcestring
	MSG_UPDATING_SENT_DEBITS = 'Actualizando comprobantes enviados';
    MSG_COULD_NOT_UPDATE_SENT_DEBITS = 'Los comprobantes no se pudieron actualizar';
    MSG_ERROR = 'Error';
    MSG_PROCESSING = 'Procesando...';
begin
    Result:= False;
	Screen.Cursor := crHourglass;
    lblReferencia.Caption := MSG_UPDATING_SENT_DEBITS;
    pbProgreso.Position := 0;
    pnlAvance.Visible := True;
    lblReferencia.Caption := MSG_PROCESSING;
    Application.ProcessMessages;
	try
    	spActualizarComprobantesEnviadosPresto.Parameters.ParamByName( '@CodigoOperacionAnterior' ).Value := iif(FCodigoOperacionAnterior=0, Null, FCodigoOperacionAnterior);
    	spActualizarComprobantesEnviadosPresto.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value := FCodigoOperacion;
        //revision 1
        spActualizarComprobantesEnviadosPresto.Parameters.ParamByName( '@Usuario' ).Value := UsuarioSistema;
      	spActualizarComprobantesEnviadosPresto.CommandTimeout := 5000;
    	spActualizarComprobantesEnviadosPresto.ExecProc;
        Result:=true;
    except
    	on e: Exception do begin
        	MsgBoxErr(MSG_COULD_NOT_UPDATE_SENT_DEBITS, e.Message, MSG_ERROR, MB_ICONERROR);
            FErrorMsg := MSG_COULD_NOT_UPDATE_SENT_DEBITS;
        end;
    end;
    pbProgreso.Position := pbProgreso.Max;
    Screen.Cursor := crDefault;
end;}

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Genero el Reporte de Finalizacion de Proceso sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFGeneracionDebitosPresto.GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
var FEnvio:TFRptEnvioComprobantes;
begin
    Result:=false;
    try
        Application.createForm(TFRptEnvioComprobantes,FEnvio);
        if not fEnvio.Inicializar('Comprobantes Enviados',CodigoOperacionInterfase) then fEnvio.Release;
        Result:=true;
    except
    end;
end;

function TFGeneracionDebitosPresto.GruposSeleccionadosEstanTerminados: boolean;
begin
    Result := False;
    try
        cdGrupos.First;
        while not cdGrupos.Eof do begin

            if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
                Result := True;
                cdGrupos.First;
                Break;
            end;
            cdGrupos.Next;
        end;
        cdGrupos.First;
    except
        on E: Exception do begin
            MsgBoxErr('Error contando grupos para la generaci�n ', e.Message, Caption, MB_ICONERROR);
        end;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    flamas
  Date Created: 03/12/2004
  Description: Busca los d�bitos y Genera un TXT con los mismos
  Parameters: Sender: TObject
  Return Value: None
  Revision : 2
      Author : vpaszkowicz
      Date : 16/07/2007
      Description : Subo la transacci�n porque voy a ir grabando los comproban-
      tes usados uno a uno por cada l�nea del archivo.
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosPresto.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Actualizo el log al finalizar
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ActualizarLog:boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVACION = 'Finalizo OK!';
    var
         DescError : String;
    begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);
        except
            on e: Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

resourcestring
	MSG_PROCESS_SUCCEDED 				= 'El proceso finaliz� con �xito';
	MSG_PROCESS_COULD_NOT_BE_COMPLETED 	= 'El proceso no se pudo completar';
	MSG_FILE_ALREADY_EXISTS 			= 'El archivo ya existe.'#10#13'� Desea continuar ?';
    MSG_COULD_NOT_UPDATE_SENT_DEBITS    = 'Los comprobantes no se pudieron actualizar';
    MSG_ERROR_DELETE_FILE               = 'No pudo eliminar el archivo';
    MSG_ERROR = 'Error';
    MSG_SIN_GRUPOS_SELECCIONADOS = 'No se seleccionaron grupos de facturacion a incluir';
    MSG_COMPROBANTES_SIN_TERMINAR = 'Hay comprobantes sin terminar en los grupos :';
begin
	if FileExists( edDestino.text ) then
		if not(MsgBox(MSG_FILE_ALREADY_EXISTS, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then Exit;


    if not ExistenGruposSeleccionados  then begin
        MsgBoxBalloon(MSG_SIN_GRUPOS_SELECCIONADOS, MSG_ERROR, MB_ICONQUESTION , dblGrupos );
        Exit;
    end;

    if not GruposSeleccionadosEstanTerminados then begin
        MsgBoxBalloon(MSG_COMPROBANTES_SIN_TERMINAR, MSG_ERROR, MB_ICONQUESTION , dblGrupos );
        Exit;
    end;



 	// Crea la listas de Debitos
    FDebitosTXT := TStringList.Create;

    // Deshabilita los botones
	btnCancelar.Enabled := True;
    btnProcesar.Enabled := False;
    BorderIcons := [];
    FErrorMsg := '';
    FMontoTotal     := 0 ;
    FCantidadTotal  := 0 ;

    FDetenerImportacion := False;

    try
        //DMConnections.BaseCAC.BeginTrans;                                       //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('BEGIN TRAN FrmGenerarDebitosPresto');     //SS_1385_NDR_20150922

        //Registra la operacion y Genera los debitos
        {if RegistrarOperacion and GenerarDebitosPresto then begin
            //Abro un Transacci�n
            DMConnections.BaseCAC.BeginTrans;}
            //Actualiza el estado de los debitos
        	if RegistrarOperacion and GenerarDebitosPresto then begin
            //if ActualizarDebitosPresto then begin
                //Acepto la Transaccion
              //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
              DMConnections.BaseCAC.Execute('COMMIT TRAN FrmGenerarDebitosPresto');					//SS_1385_NDR_20150922
            end else begin
                //Si hubo errores hace un RollBack
                //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION FrmGenerarDebitosPresto END');	//SS_1385_NDR_20150922
                try
                    //Elimino el archivo
                    DeleteFile(edDestino.Text);
                except
                    on e: Exception do begin
                       //Informo que no pudo eliminar el archivo
                       MsgBoxErr(MSG_ERROR_DELETE_FILE, e.Message, Self.caption, MB_ICONERROR);
                    end;
                end;
                //Muestro un Cartel informando que no pudo actualizar los debitos
                MsgBoxErr(MSG_COULD_NOT_UPDATE_SENT_DEBITS, FErrorMsg, MSG_ERROR, MB_ICONERROR);
           end;

        //ss750
        ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacion, FCantidadTotal, FMontoTotal, '', FErrorMsg);

        //Verifico si hubo errores
        if ( FErrorMsg = '' ) then begin

            //Actualizo el log al Final
            ActualizarLog;
            //Informo que finalizo con exito
            MsgBox(MSG_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION);
            //Muestro el Reporte de Finalizacion del Proceso
            GenerarReportedeFinalizacion(FCodigoOperacion);

        end else begin

            //Informo que finalizo con error
        	MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
            
        end;

    finally
        //Libero el stringlist
        FreeAndNil( FDebitosTXT );
    	//Habilita los botones
		btnCancelar.Enabled := False;
    	btnProcesar.Enabled := True;
        Close;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    flamas
  Date Created: 07/12/2004
  Description: Detiene el proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosPresto.btnCancelarClick(Sender: TObject);
resourcestring
	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Permito salir si no esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosPresto.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
	CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosPresto.btnSalirClick(Sender: TObject);
begin
   Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Lo libero de Memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosPresto.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	Action := caFree;
end;


end.
