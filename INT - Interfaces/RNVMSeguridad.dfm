object frmSeguridadRNVM: TfrmSeguridadRNVM
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Seguridad de Acceso al Servicio RNVM'
  ClientHeight = 179
  ClientWidth = 464
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 188
    Height = 13
    Caption = 'Ingrese La Clave de Acceso al Servicio:'
  end
  object Label2: TLabel
    Left = 17
    Top = 54
    Width = 252
    Height = 13
    Caption = 'Ingrese Nuevamente La Clave de Acceso al Servicio:'
  end
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 449
    Height = 113
  end
  object txtClave: TEdit
    Left = 280
    Top = 21
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 0
  end
  object txtValidacion: TEdit
    Left = 280
    Top = 48
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object btnAceptar: TButton
    Left = 288
    Top = 144
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    TabOrder = 2
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 368
    Top = 144
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 3
  end
end
