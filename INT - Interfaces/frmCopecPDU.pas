{********************************** Unit Header **************************************
File Name : frmCopecPDU.pas
Author : mbecerra
Date Created: 13-mayo-2008
Language : ES-CL
Description : Procesa los archivos de PDU Copec.

Revision : 1
    Author: pdominguez
    Date  : 05/08/2010
    Description: SS 909 - BHTU Copec - Rechazos
        - Se a�adieron/modificaron los siguientes objetos/variables/Constantes
            FCOPECAdvertencias : TStringList

        - Se a�adieron/modificaron los siguientes procedimientos/Funciones
            ActualizarLogAlFinal,
            ProcesarArchivoPDUT,
            AgregarErroresParseo,
            btnProcesarClick
 Firma        : SS_1313_NDR_20150615
 Descripcion  : Emitir Reporte Resumen DayPass al finalizar la importacion
                Se debe mostrar la patente, la cantidad de daypass comprados en el a�o en curso
                (si este valor excede de un parametro), siempre y cuando la patente tenga al menos un
                DayPAss comprado en los ultimos X dias (segun parametro)

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)


Firma       : SS_1256_NDR_20150505
Descripcion : Los nombres de los archivos deben considerar la ConcesionariaNativa
*************************************************************************************}
unit frmCopecPDU;

interface

uses
    //Recepcion de ventas de BHTU
    DMConnection,                //Base de Datos OP_CAC
    UtilDB,
    PeaProcs,
    PeaTypes,
    ComunesInterfaces,
    Util,
    UtilProc,
    StrUtils,
    DateUtils,
    ConstParametrosGenerales,    //Parametros Generales
    frmRptDayPassCopec,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, DB, ADODB, StdCtrls, DmiCtrls,
  PeaProcsCN, ppDB, ppDBPipe, ppParameter, ppModule, raCodMod, ppBands, ppCtrls,
  ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB;

type
  TCopecPDU = record
    Serial 				: Ansistring;
    Matricula 			: Ansistring;
    Relleno1 			: AnsiString;
    FechaHabilitacion 	: TDateTime;
    Relleno2 			: AnsiString;
    Categoria			: byte;
    Relleno3			: AnsiString;
    Distribuidor		: AnsiString;
    Relleno4			: AnsiString;
    TipoArchivo			: AnsiString;
    FechaVenta 			: TDateTime;
  end;

  TCopecPDUT = record
    Matricula			: AnsiString;
    Categoria			: byte;
    FechaHabilitacion	: TDateTime;
    Valor				: integer;
    FechaVenta			: TDateTime;
    FechaContable		: TDateTime;
    NumeroSerie			: AnsiString; 
  end;

  TCopecPDUForm = class(TForm)
    Label2: TLabel;
    txtArchivo: TPickEdit;
    btnProcesar: TButton;
    btnSalir: TButton;
    btnCancelar: TButton;
    dlgOpen: TOpenDialog;
    spAgregarPDUCopec: TADOStoredProc;
    pnlAvance: TPanel;
    lblDetalle: TLabel;
    lbl: TLabel;
    pbProgreso: TProgressBar;
    spAgregarPDUTCopec: TADOStoredProc;
    spActualizarLogOperacionesRegistrosProcesados: TADOStoredProc;
    RBIResumen: TRBInterface;
    ppReportResumenDayPass: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    lbl_usuario: TppLabel;
    ppLine1: TppLine;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel10: TppLabel;
    ppLine6: TppLine;
    ppImage2: TppImage;
    ppFechaProcesamiento: TppLabel;
    pptFechaProcesamiento: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBTCantidad: TppDBText;
    ppDBTFechaUso: TppDBText;
    ppDBTFechaVenta: TppDBText;
    ppDBTPatente: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppLine4: TppLine;
    raCodeModule2: TraCodeModule;
    ppParameterList1: TppParameterList;
    ppDBReporteResumenDayPass: TppDBPipeline;
    dsResumenDayPass: TDataSource;
    spResumenDayPass: TADOStoredProc;
    procedure btnSalirClick(Sender: TObject);
    procedure txtArchivoButtonClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure RBIResumenExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FEsPDU : boolean;
    FCodigoOperacion : integer;
    FErrores : boolean;
    FProcesando : boolean;
    FCancelar : boolean;
    FCantidadLineasBuenas : integer;
    FCantidadLineasFalla  : integer;
    FCantidadLineasTotal  : integer;
    FCOPEC_PDU_Directorio_Entrada : string;
    FCOPEC_PDU_Directorio_Procesados : string;
    FCOPECPDU : TStringList;
    FCOPECErrors,
    FCOPECAdvertencias : TStringList; // Rev. 1 (SS 909)
    FArchivoPrefijo : AnsiString;
    FArchivoVenta,
    FArchivoOtros : AnsiString;           {para mover si existe otro archivo, PQMCOPEC o PQTCOPEC}
    FFechaArchivoVentasIni,
    FFechaArchivoVentasFin : TDateTime;   {las fecha de inicio y fin de ventas, se toman del archivo de ventas}
	FSumaMontoTotalArchivo      : Int64; // SS 909
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    { Public declarations }
    function Inicializar(EsPDU : boolean; Titulo: AnsiString): boolean;
    function ValidaArchivo : boolean;
    function RegistrarOperacion(CodigoModulo: integer; sFileName: string) : boolean;
    function ProcesarArchivo : boolean;
    function ProcesarArchivoPDU : boolean;
    function ProcesarArchivoPDUT : boolean;
    //function ObtenerCantidadErroresInterfaz(CodigoOperacion : integer; var Total : integer): boolean;
    function ActualizarLogAlFinal(EsExito : boolean) : boolean; // Rev. 1 (SS 909)
    procedure HabilitarBotones;
    procedure AgregarErroresParseo;
    procedure GenerarReporteDeFinalizacion;
  end;


var
  CopecPDUForm: TCopecPDUForm;

implementation

{$R *.dfm}
resourcestring
	ARCHIVO_PREFIJO_PDU  = 'PHDCOPEC';
    ARCHIVO_PREFIJO_PDUT = 'PMCCOPEC';
    ARCHIVO_DATEFORMAT = 'YYMMDD';
    ARCHIVO_EXTENSION = '.TXT';

{-------------------------------------------------------------------------------
 Function GetItem
 Author : mbecerra
 Date : 26-Junio-2008
 Description : Function que recibe un string de la forma <campo1>sep<campo2>sep<campo3>
 				y devuelve el <campo i> indicado en el par�metro, o un string vac�o
                si no lo encuentra
-------------------------------------------------------------------------------}
function GetItem(Texto : string; NumeroItem : integer; Separador : Char) : string;
var
    Elemento : string;
    i, Cual : integer;
begin
    i := 1;
    Elemento := '';
    Cual := 0;
    Texto := Texto + Separador;  {para asegurarnos de encontrar el separador en el �ltimo elemento}
    while i <= Length(Texto) do begin
        if Texto[i] = Separador then begin
            Inc(Cual);
            if Cual = NumeroItem then   {es el elemento que se debe retornar}
            	i := Length(Texto) + 1
            else
                Elemento := '';         {no es el elemento que se debe retornar}

        end
        else
          Elemento := Elemento + Texto[i];

    	Inc(i);
    end;

    Result := Elemento;
end;

{ ********************************************************
 Function: EsNumero
 Author: mbecerra
 Date: 14-Mayo-2008
 Description: Verifica que una cadena contega s�lo n�meros
***********************************************************************}
function EsNumero(texto : AnsiString) : boolean;
var
   i : integer;
begin
    i := 1;
    Result := True;
    while Result and (i <= Length(texto)) do begin
    	Result := (texto[i] in ['0'..'9']);
        Inc(i);
    end;

end;

{ ********************************************************
 Function: EsPatente
 Author: mbecerra
 Date: 22-Mayo-2008
 Description: Verifica que la patente sea v�lida
***********************************************************************}
function EsPatente(Patente : AnsiString) : boolean;
var
   i : integer;
begin
    i := 1;
    Patente := UpperCase(Patente);
    Result := True;
    while Result and (i <= Length(Patente)) do begin
    	Result := (Patente[i] in ['0'..'9', 'A'..'Z']);
        Inc(i);
    end;

end;

{
    Revision: 1
        Author : pdominguez
        Date   : 05/08/2010
        Description : SS 909 - BHTU Copec - Rechazos
            - Se obtienen los Errores/Advertencias de las Listas Generadas.
}
function TCopecPDUForm.ActualizarLogAlFinal;
Resourcestring
    MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
Const
    STR_ERRORES = 'Finaliz� OK!, Errores: %d, Advertencias: %d ';
    STR_CON_ERROR = 'Finaliz� con Errores, Errores: %d, Advertencias: %d';
var
    DescError, Mensaje : String;
begin
    // Rev. 1 (SS 909)
    if EsExito then
        Mensaje := STR_ERRORES
    else Mensaje := STR_CON_ERROR;

    Mensaje := Format(Mensaje, [FCOPECErrors.Count, FCOPECAdvertencias.Count]);
    // Rev. 1 (SS 909)
    try
        Result := ActualizarLogOperacionesInterfaseAlFinal
                   (DMConnections.BaseCAC,FCodigoOperacion, FCantidadLineasTotal, FSumaMontoTotalArchivo, Mensaje, DescError);

        if Result then begin
        	spActualizarLogOperacionesRegistrosProcesados.Close;
        	with  spActualizarLogOperacionesRegistrosProcesados do begin
            	Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                Parameters.ParamByName('@RegistrosAProcesar').Value       := FCantidadLineasBuenas;
                Parameters.ParamByName('@LineasArchivo').Value            := FCantidadLineasTotal;
                ExecProc;
            end;

        end;
        
    except
        on E : Exception do begin
           MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
           Result := False;
        end;
    end;
end;

function TCopecPDUForm.ProcesarArchivo;
begin
    if FEsPDU then Result := ProcesarArchivoPDU()
    else Result := ProcesarArchivoPDUT();
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Genero el Reporte de Finalizacion de Proceso sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean

  Revision 1
  Author: mbecerra
  Date: 10-Junio-2008
  Description:
            	Se agreg� una llamada a la funci�n AgregarErrorInterfase con el objeto
                de registrar la cantidad de l�neas totales procesadas y la
                cantidad de l�neas con error. Ya que el reporte lo pide y cuando sea
                invocado desde frmLogOperaciones, estos valores vendr�n en cero.

-----------------------------------------------------------------------------}
Procedure TCopecPDUForm.GenerarReporteDeFinalizacion;
    var
        F: TRptDayPassForm;
        TituloReport: String;

begin
	{revision 1
        AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion,
        						Format('Totales: Registros: %d - Procesados: %d - Errores: %d',
                                	[FCantidadLineasTotal, FCantidadLineasBuenas, FCantidadLineasFalla]) );}
        //muestro el reporte
        try
            F := TRptDayPassForm.Create(Self);

            if FEsPDU then
                TituloReport := 'Copec DayPass'
            else TituloReport := 'Copec DayPass Tard�o';
            
//            if F.Inicializar(TituloReport, FCodigoOperacion, FCOPECPDU.Count - 1, FCantidadLineasFalla) then
            if F.Inicializar(TituloReport, FCodigoOperacion, Not FEsPDU) then
                F.RBIListado.Execute;

        finally
            if Assigned(F) then FreeAndNil(F);
        end;
end;

function TCopecPDUForm.ProcesarArchivoPDU;

	{ ********************************************************
	Function: ValidarLinea
 	Author: mbecerra
 	Date: 22-Mayo-2008
 	Description:
    		Realiza validaciones adicionales a la l�nea, de acuerdo al formato del
            archivo.

	***********************************************************************}
    function ValidarLinea(LineaPDU : TCopecPDU) : string;
    resourcestring
        SERIAL_NO_NUMERICA    = 'El n�mero serial no es num�rico';
        PATENTE_INVALIDA      = 'La patente es inv�lida';
        NO_ES_RELLENO         = 'El campo de relleno no es coherente con lo esperado';
        VALOR_RELLENO_1       = '99999999';
        VALOR_RELLENO_2       = '00';
        DISTRIBUIDOR_INVALIDO = 'El Distribuidor es inv�lido';
        TIPO_ARCHIVO_INVALIDO = 'El Tipo de Archivo no es v�lido';

    begin
    	Result := '';
		if (Result = '') and (not EsNumero(LineaPDU.Serial)) then Result := SERIAL_NO_NUMERICA;
        if (Result = '') and (not EsPatente(LineaPDU.Matricula)) then Result := PATENTE_INVALIDA;
        if (Result = '') and (LineaPDU.Relleno1 <> VALOR_RELLENO_1) then Result := NO_ES_RELLENO;
        if (Result = '') and (LineaPDU.Relleno2 <> VALOR_RELLENO_2) then Result := NO_ES_RELLENO;
        if (Result = '') and (not EsNumero(LineaPDU.Distribuidor)) then Result := DISTRIBUIDOR_INVALIDO;
        if (Result = '') and (Trim(LineaPDU.Relleno4) <> '') then Result := NO_ES_RELLENO;
        if (Result = '') and (LineaPDU.TipoArchivo <> 'H') then Result := TIPO_ARCHIVO_INVALIDO;
    end;

	{ ********************************************************
 	Function: ParsearLinea
 	Author: mbecerra
 	Date: 14-Mayo-2008
 	Description: Parsea una l�nea de acuerdo al formato definido en el archivo de entrada.

    Date: 26-Junio-2008
    Description: Se agrega la validaci�n de la fecha de venta dentro del rango
    			de fechas en el archivo de ventas asociado. (es un warning, no un error)
	***********************************************************************}
	function ParsearLineaPDU(NumeroLinea, LineaEntrada : string; var RegistroCopec : TCopecPDU) : boolean;
		resourcestring
    		LINEA_NO_ES_LARGA = 'Error: El largo de la Linea %s es diferente al requerido: %d';
            FECHA_VENTA_FUERA = ' fuera rango indicado en archivo ventas:';

        const
        	LARGO_LINEA = 134;

		var
			LineaAux, MensajeError, Dia, Mes, Anio : AnsiString;

	begin
		Result := True;
        LineaAux := IntToStr(StrToInt(NumeroLinea) + 1);
    	if Length(LineaEntrada) <> LARGO_LINEA then begin
    		FCOPECErrors.Add(Format(LINEA_NO_ES_LARGA, [LineaAux, LARGO_LINEA]));
            FErrores := True;
	        Result := False;
	        exit;
    	end;

		try
			MensajeError 					:= 'Serial';
			RegistroCopec.Serial 			:= Copy(LineaEntrada, 1, 18);
        	MensajeError 					:= 'Matr�cula';
			RegistroCopec.Matricula 		:= Trim(Copy(LineaEntrada, 19, 10));
        	RegistroCopec.Relleno1			:= Copy(LineaEntrada, 29, 8);
        	MensajeError 					:= 'Habilitaci�n';
        	Anio							:= Copy(LineaEntrada, 37, 4);
        	Mes								:= Copy(LineaEntrada, 41, 2);
        	Dia 							:= Copy(LineaEntrada, 43, 2);
        	RegistroCopec.FechaHabilitacion := EncodeDate(StrToInt(Anio), StrToInt(Mes), StrToInt(Dia));
        	RegistroCopec.Relleno2			:= Copy(LineaEntrada, 45, 2);
        	MensajeError					:= 'Categor�a';
        	RegistroCopec.Categoria 		:= StrToInt(Copy(LineaEntrada, 47, 1));
        	RegistroCopec.Relleno3			:= Copy(LineaEntrada, 48, 1);
        	MensajeError					:= 'Distribuidor';
        	RegistroCopec.Distribuidor 		:= Copy(LineaEntrada, 49, 10);
        	RegistroCopec.Relleno4			:= Copy(LineaEntrada, 59, 67);
        	MensajeError					:= 'Tipo Archivo';
        	RegistroCopec.TipoArchivo 		:= Copy(LineaEntrada, 126, 1);
        	MensajeError					:= 'Fecha Venta';
        	Anio							:= Copy(LineaEntrada, 127, 4);
        	Mes								:= Copy(LineaEntrada, 131, 2);
        	Dia 							:= Copy(LineaEntrada, 133, 2);
        	RegistroCopec.FechaVenta 		:= EncodeDate(StrToInt(Anio), StrToInt(Mes), StrToInt(Dia));
            MensajeError := ValidarLinea(RegistroCopec);
            if MensajeError <> '' then begin
                FCOPECErrors.Add('Error L�nea ' + LineaAux + ' , Motivo: ' + MensajeError);
        		Result := False;
            end;

            if (FFechaArchivoVentasIni > 0) and (
            	(RegistroCopec.FechaVenta < FFechaArchivoVentasIni)  or
                (RegistroCopec.FechaVenta > FFechaArchivoVentasFin)  ) then begin
                FCOPECAdvertencias.Add('Advertencia: L�nea ' + LineaAux + ' La fecha de venta: ' + // Rev. 1 (SS 909)
                				DateToStr(RegistroCopec.FechaVenta) + FECHA_VENTA_FUERA +
                                DateToStr(FFechaArchivoVentasIni) + ' - ' +
                                DateToStr(FFechaArchivoVentasFin));
            end;

    	except on e:exception do begin
				FCOPECErrors.Add('Error: L�nea ' + LineaAux + ' Campo Inv�lido: ' + MensajeError + ', Motivo: ' + e.message);
        		Result := False;
        	end;
  		end;

	end;


resourcestring
    MSG_LINEA 				= 'L�nea: ';
    MSG_PROCESANDO_ARCHIVO  = 'Procesando archivo COPEC PDU - Linea : %d';
    MSG_ERROR_PROCESO		= 'Linea : %d - Mensaje: %s';
    MSG_ERROR 				= 'Error';

var
    NroLinea : integer;
    rcdParseoLinea : TCopecPDU;
    CodigoProveedor : integer;
    HuboErrorStore, ErrorEnEjecucionStore: string;
begin
    {obtener el c�digo proveedor COPEC}
    CodigoProveedor := 2;   {ProveedoresDayPass = COPEC}
  	FErrores := False;
    NroLinea := 0;
    HuboErrorStore := '';
    ErrorEnEjecucionStore := '';
    FCantidadLineasBuenas := 0;
    FCantidadLineasFalla  := 0;
    FCantidadLineasTotal  := FCOPECPDU.Count - 1;

    pbProgreso.Position := 0;
    pbProgreso.Max      := FCantidadLineasTotal;

    { Nota: debe ser NroLinea menor que el Count - 1, ya que el �ltimo es registro de control y no debe procesarse}
    while ( NroLinea < FCantidadLineasTotal ) and ( not FCancelar ) and (ErrorEnEjecucionStore = '') do begin
    	lblDetalle.Caption  := Format(MSG_PROCESANDO_ARCHIVO, [NroLinea + 1]) + ' de ' + IntToStr(FCOPECPDU.Count - 1);
    	Refresh;
        Application.ProcessMessages;
        if ParsearLineaPDU(IntToStr(NroLinea), FCOPECPDU[NroLinea], rcdParseoLinea) then begin
        	try
                //DMConnections.BaseCAC.BeginTrans;
                spAgregarPDUCopec.Parameters.ParamByName('@CodigoOperacionInterfase').Value  := FCodigoOperacion;
                spAgregarPDUCopec.Parameters.ParamByName('@NumeroSerie').Value		:= rcdParseoLinea.Serial;
                spAgregarPDUCopec.Parameters.ParamByName('@Patente').Value			:= rcdParseoLinea.Matricula;
                spAgregarPDUCopec.Parameters.ParamByName('@CodigoCategoria').Value	:= rcdParseoLinea.Categoria;
                spAgregarPDUCopec.Parameters.ParamByName('@FechaVenta').Value		:= rcdParseoLinea.FechaVenta;
                spAgregarPDUCopec.Parameters.ParamByName('@FechaUso').Value			:= rcdParseoLinea.FechaHabilitacion;
                spAgregarPDUCopec.Parameters.ParamByName('@Distribuidor').Value		:= rcdParseoLinea.Distribuidor;
                spAgregarPDUCopec.Parameters.ParamByName('@TipoArchivo').Value		:= rcdParseoLinea.TipoArchivo;
                spAgregarPDUCopec.Parameters.ParamByName('@CodigoProveedorDayPass').Value	:= CodigoProveedor;
                spAgregarPDUCopec.ExecProc;
                HuboErrorStore := Trim(VarToStr(spAgregarPDUCopec.Parameters.ParamByName('@DescripcionError').Value));
                if HuboErrorStore <> '' then begin  {hubo error en el Store NOTA: el mensaje no necesariamente es un error.
                									 Esto induce problemas al conteo de la aplicaci�n.
                                                     La versi�n homologada debe retornar exito � fracaso}
                	//AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_ERROR_PROCESO, [NroLinea, HuboErrorStore]));
                    //DMConnections.BaseCAC.RollbackTrans;
                    FCOPECErrors.Add(Format(MSG_ERROR_PROCESO, [NroLinea + 1, HuboErrorStore]));
                    Inc(FCantidadLineasFalla);
                    FErrores := True;
                end
                else Inc(FCantidadLineasBuenas);  //DMConnections.BaseCAC.CommitTrans;

            except on e:exception do begin
                	//DMConnections.BaseCAC.RollbackTrans;
                    ErrorEnEjecucionStore := e.Message;
                    AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_ERROR_PROCESO, [NroLinea, ErrorEnEjecucionStore]));
                    FErrores:= True;
                    MsgBoxErr(Format(MSG_ERROR_PROCESO, [NroLinea + 1, '']) + CRLF + MSG_LINEA + IntToStr(NroLinea + 1) + CRLF + FCOPECPDU[NroLinea], ErrorEnEjecucionStore, MSG_ERROR, MB_ICONERROR);
            	end;

            end;

        end
        else begin
        	Inc(FCantidadLineasFalla);
            FErrores := True;
        end;

        Inc( NroLinea );
      	pbProgreso.Position := NroLinea + 1;
        Application.ProcessMessages;
    end;

    pbProgreso.Position := 0;

    Result := ( not FCancelar ) and ( ErrorEnEjecucionStore = '' );
end;


{
    Revision: 1
        Author : pdominguez
        Date   : 05/08/2010
        Description : SS 909 - BHTU Copec - Rechazos
            - Se a�ade la Gesti�n de las Advertencias
}
function TCopecPDUForm.ProcesarArchivoPDUT;
	{ ********************************************************
	Function: ValidarLinea
 	Author: mbecerra
 	Date: 22-Mayo-2008
 	Description:
    		Realiza validaciones adicionales a la l�nea, de acuerdo al formato del
            archivo.
	***********************************************************************}
    function ValidarLinea(LineaPDUT : TCopecPDUT) : string;
    resourcestring
        PATENTE_INVALIDA    = 'La patente es inv�lida';
        NO_ES_CATEGORIA     = 'La Categor�a es inv�lida';
        NUMSERIE_INCOMPLETO = 'El n�mero de Serie est� incompleto';
    begin
    	Result := '';
        if (Result = '') and (not EsPatente(LineaPDUT.Matricula)) then Result := PATENTE_INVALIDA;
        if (Result = '') and (not (LineaPDUT.Categoria in [1..4])) then Result := NO_ES_CATEGORIA;
        if (Result = '') and (Length(LineaPDUT.NumeroSerie) < 31) then Result := NUMSERIE_INCOMPLETO;
    end;

	{ ********************************************************
 	Function: ParsearLinea
 	Author: mbecerra
 	Date: 14-Mayo-2008
 	Description: Parsea una l�nea de acuerdo al formato definido en el archivo de entrada.
	***********************************************************************}
	function ParsearLineaPDUT(NumeroLinea, LineaEntrada : string; var RegistroCopec : TCopecPDUT) : boolean;
		resourcestring
    		LINEA_NO_ES_LARGA = 'Error: El largo de la Linea %s es diferente al requerido: %d';
            FECHA_VENTA_FUERA = ' fuera rango indicado en archivo ventas:';

        const
        	LARGO_LINEA = 76;

		var
			LineaAux, MensajeError, Dia, Mes, Anio : AnsiString;


	begin
		Result := True;
        LineaAux := IntToStr(StrToInt(NumeroLinea) + 1);
    	if Length(LineaEntrada) <> LARGO_LINEA then begin
    		FCOPECErrors.Add(Format(LINEA_NO_ES_LARGA, [LineaAux, LARGO_LINEA]));
            FErrores := True;
	        Result := False;
	        Exit;
    	end;

		try
        	MensajeError 					:= 'Matr�cula';
			RegistroCopec.Matricula 		:= Trim(Copy(LineaEntrada, 1, 10));
            MensajeError 					:= 'Categor�a';
        	RegistroCopec.Categoria			:= StrToInt(Copy(LineaEntrada, 11, 1));
        	MensajeError 					:= 'Habilitaci�n';
        	Anio							:= Copy(LineaEntrada, 12, 4);
        	Mes								:= Copy(LineaEntrada, 16, 2);
        	Dia 							:= Copy(LineaEntrada, 18, 2);
        	RegistroCopec.FechaHabilitacion := EncodeDate(StrToInt(Anio), StrToInt(Mes), StrToInt(Dia));
        	MensajeError					:= 'Valor';
        	RegistroCopec.Valor		 		:= StrToInt(Copy(LineaEntrada, 20, 10));
            MensajeError					:= 'Fecha Venta';
        	Anio							:= Copy(LineaEntrada, 30, 4);
        	Mes								:= Copy(LineaEntrada, 34, 2);
        	Dia 							:= Copy(LineaEntrada, 36, 2);
        	RegistroCopec.FechaVenta 		:= EncodeDate(StrToInt(Anio), StrToInt(Mes), StrToInt(Dia));
        	MensajeError					:= 'Fecha Contable';
        	Anio							:= Copy(LineaEntrada, 38, 4);
        	Mes								:= Copy(LineaEntrada, 42, 2);
        	Dia 							:= Copy(LineaEntrada, 44, 2);
        	RegistroCopec.FechaContable		:= EncodeDate(StrToInt(Anio), StrToInt(Mes), StrToInt(Dia));
        	MensajeError					:= 'Serie';
        	RegistroCopec.NumeroSerie 		:= Copy(LineaEntrada, 46, 31);
            MensajeError := ValidarLinea(RegistroCopec);
            if MensajeError <> '' then begin
                FCOPECErrors.Add('Error L�nea ' + LineaAux + ' Motivo: ' + MensajeError);
        		Result := False;
            end;

            if (FFechaArchivoVentasIni > 0) and (
            	 (RegistroCopec.FechaVenta < FFechaArchivoVentasIni)  or
               	(RegistroCopec.FechaVenta > FFechaArchivoVentasFin)   ) then begin
                FCOPECAdvertencias.Add('Advertencia: L�nea ' + LineaAux + ' La fecha de venta: ' + // Rev. 1 (SS 909)
                				DateToStr(RegistroCopec.FechaVenta) + FECHA_VENTA_FUERA +
                                DateToStr(FFechaArchivoVentasIni) + ' - ' +
                                DateToStr(FFechaArchivoVentasFin));
            end;
    	except on e:exception do begin
				FCOPECErrors.Add('Error L�nea ' + LineaAux + ' Campo Inv�lido: ' + MensajeError + ', Motivo: ' + e.message);
        		Result := False;
        	end;
  		end;

	end;

resourcestring
    MSG_LINEA 				= 'L�nea: ';
    MSG_PROCESANDO_ARCHIVO  = 'Procesando archivo COPEC PDUT - Linea : %d';
    MSG_ERROR_PROCESO		= 'Linea : %d - Mensaje: %s';
    MSG_ERROR 				= 'Error';

var
    NroLinea : integer;
    rcdParseoLinea : TCopecPDUT;
    CodigoProveedor : integer;
    HuboErrorStore, ErrorEnEjecucionStore, HuboAdvertenciaStore : string; // Rev. 1 (SS 909)
begin
    {obtener el c�digo proveedor COPEC}
    CodigoProveedor := 2;   {ProveedoresDayPass = COPEC}
  	FErrores := False;
    NroLinea := 0;
    HuboErrorStore := '';
    HuboAdvertenciaStore := '';
    ErrorEnEjecucionStore := '';
    FCantidadLineasBuenas := 0;
    FCantidadLineasFalla  := 0;
    FCantidadLineasTotal  := FCOPECPDU.Count;   {no hay registro de control}

    pbProgreso.Position := 0;
    pbProgreso.Max      := FCantidadLineasTotal;

    while ( NroLinea < FCantidadLineasTotal ) and ( not FCancelar ) and (ErrorEnEjecucionStore = '') do begin
    	lblDetalle.Caption  := Format(MSG_PROCESANDO_ARCHIVO, [NroLinea + 1]) + ' de ' + IntToStr(FCOPECPDU.Count);
    	Refresh;
        Application.ProcessMessages;
        if ParsearLineaPDUT(IntToStr(NroLinea), FCOPECPDU[NroLinea], rcdParseoLinea) then begin

            FSumaMontoTotalArchivo := FSumaMontoTotalArchivo + rcdParseoLinea.Valor;

        	try
                //DMConnections.BaseCAC.BeginTrans;
                spAgregarPDUTCopec.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                spAgregarPDUTCopec.Parameters.ParamByName('@Patente').Value				:= rcdParseoLinea.Matricula;
                spAgregarPDUTCopec.Parameters.ParamByName('@Categoria').Value			:= rcdParseoLinea.Categoria;
                spAgregarPDUTCopec.Parameters.ParamByName('@FechaHabilitacion').Value	:= rcdParseoLinea.FechaHabilitacion;
                spAgregarPDUTCopec.Parameters.ParamByName('@Importe').Value				:= rcdParseoLinea.Valor * 100;
                spAgregarPDUTCopec.Parameters.ParamByName('@FechaVenta').Value			:= rcdParseoLinea.FechaVenta;
                spAgregarPDUTCopec.Parameters.ParamByName('@FechaRendicion').Value		:= rcdParseoLinea.FechaContable;
                spAgregarPDUTCopec.Parameters.ParamByName('@Identificador').Value		:= rcdParseoLinea.NumeroSerie;
                spAgregarPDUTCopec.Parameters.ParamByName('@CodigoProveedorDayPass').Value   := CodigoProveedor;
                spAgregarPDUTCopec.Parameters.ParamByName('@DescripcionError').Value   :=  NULL;
                spAgregarPDUTCopec.Parameters.ParamByName('@DescripcionAdvertencia').Value   :=  NULL;
                spAgregarPDUTCopec.ExecProc;
                HuboErrorStore := Trim(VarToStr(spAgregarPDUTCopec.Parameters.ParamByName('@DescripcionError').Value));
                HuboAdvertenciaStore := Trim(VarToStr(spAgregarPDUTCopec.Parameters.ParamByName('@DescripcionAdvertencia').Value)); //Rev. 1 (SS 909)
                if HuboErrorStore <> '' then begin  {hubo error en el Store}
                //    DMConnections.BaseCAC.RollbackTrans;
                	//AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_ERROR_PROCESO, [NroLinea, HuboErrorStore]));
                    FCOPECErrors.Add(Format(MSG_ERROR_PROCESO, [NroLinea + 1, HuboErrorStore]));
                    Inc(FCantidadLineasFalla);
                    FErrores := True;
                end
                else begin  // Rev. 1 (SS 909)
                    Inc(FCantidadLineasBuenas);   //DMConnections.BaseCAC.CommitTrans;
                    if HuboAdvertenciaStore <> '' then begin
                        FCOPECAdvertencias.Add(Format(MSG_ERROR_PROCESO, [NroLinea + 1, HuboAdvertenciaStore]));
                    end;
                end; // Fin Rev. 1 (SS 909)

            except on e:exception do begin
                	//DMConnections.BaseCAC.RollbackTrans;
                    ErrorEnEjecucionStore := e.Message;
                    AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_ERROR_PROCESO, [NroLinea, ErrorEnEjecucionStore]));
                    FErrores:= True;
                    MsgBoxErr(Format(MSG_ERROR_PROCESO, [NroLinea + 1, '']) + CRLF + MSG_LINEA + IntToStr(NroLinea + 1) + CRLF + FCOPECPDU[NroLinea], ErrorEnEjecucionStore, MSG_ERROR, MB_ICONERROR);
            	end;

            end;

        end
        else begin
        	Inc(FCantidadLineasFalla);
            FErrores := True;
        end;

        Inc( NroLinea );
      	pbProgreso.Position := NroLinea + 1;
        Application.ProcessMessages;
    end;

    pbProgreso.Position := 0;

    Result := ( not FCancelar ) and ( ErrorEnEjecucionStore = '' );
end;

{
    Revision: 1
        Author : pdominguez
        Date   : 05/08/2010
        Description : SS 909 - BHTU Copec - Rechazos
            - Se a�ade la Gesti�n de las Advertencias al almacenamiento de los errores de la interfase
}
procedure TCopecPDUForm.AgregarErroresParseo;
var
    i: integer;
begin
	i := 0;
    while i < FCOPECErrors.Count do begin
    	if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, FCOPECErrors[i], 1) then exit; // Rev. 1 (SS 909)
        Inc(i)
    end;
	i := 0;
    while i < FCOPECAdvertencias.Count do begin
    	if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, FCOPECAdvertencias[i], 0) then exit; // Rev. 1 (SS 909)
        Inc(i)
    end;

end;

function TCopecPDUForm.Inicializar;
resourcestring
  	MSG_INIT_ERROR = 'No se pudo inicializar la interfaz COPEC PDU';
    ERROR_P_GRAL = 'Error al cargar el Par�metro General "%s".' ;
    DIR_ORIGEN_COPEC_PDU = 'DIR_ORIGEN_COPEC_PDU';
    DIR_DESTINO_COPEC_PDU_PROCESADOS = 'DIR_DESTINO_COPEC_PDU_PROCESADOS';
    DIR_ORIGEN_COPEC_PDUT = 'DIR_ORIGEN_COPEC_PDUT';
    DIR_DESTINO_COPEC_PDUT_PROCESADOS = 'DIR_DESTINO_COPEC_PDUT_PROCESADOS';
var
	DirectorioOrigen, DirectorioDestino : AnsiString;
begin
    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    FEsPDU  := EsPDU;    {True = indica que es PDU, False = indica que es PDUT}
    if FEsPDU then begin
    	DirectorioOrigen  := DIR_ORIGEN_COPEC_PDU;
        DirectorioDestino := DIR_DESTINO_COPEC_PDU_PROCESADOS;
    end
    else begin
    	DirectorioOrigen  := DIR_ORIGEN_COPEC_PDUT;
        DirectorioDestino := DIR_DESTINO_COPEC_PDUT_PROCESADOS;
    end;

    Caption := Titulo;
//    CenterForm(Self);
    FErrores 	:= False;
    FProcesando := False;
    lblDetalle.Caption 	:= '';
  	// Obtiene el Directorio de Entrada de la Interface
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DirectorioOrigen, FCOPEC_PDU_Directorio_Entrada)   then begin
        MsgBoxErr(MSG_INIT_ERROR, Format( ERROR_P_GRAL, [DirectorioOrigen]), Caption, MB_ICONSTOP);
        Exit;
    end;
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DirectorioDestino, FCOPEC_PDU_Directorio_Procesados) then begin
        MsgBoxErr(MSG_INIT_ERROR, Format( ERROR_P_GRAL, [DirectorioDestino]), Caption, MB_ICONSTOP);
        Exit;
    end;
    HabilitarBotones;
    Result := True;
end;

procedure TCopecPDUForm.RBIResumenExecute(Sender: TObject;
  var Cancelled: Boolean);
var																					                    //SS_1313_NDR_20150615
    FError: AnsiString;																                    //SS_1313_NDR_20150615
begin																				                    //SS_1313_NDR_20150615
  ppFechaProcesamiento.Caption:=FormatDateTime('dd-mm-yyyy', NowBase(DMConnections.BaseCAC));			//SS_1313_NDR_20150615
  spResumenDayPass.Close;															                    //SS_1313_NDR_20150615
  spResumenDayPass.Parameters.Refresh;                                                            		//SS_1313_NDR_20150615
  spResumenDayPass.Parameters.ParamByName('@CodigoProveedorDaypass').Value:=2;                     		//SS_1313_NDR_20150615
  spResumenDayPass.Open;															                    //SS_1313_NDR_20150615
end;																				                    //SS_1313_NDR_20150615

function TCopecPDUForm.RegistrarOperacion(CodigoModulo: integer; sFileName: string) : boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
var
    DescError : string;
begin
    Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, CodigoModulo, sFileName, UsuarioSistema, '', True, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);
    if not Result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, Caption, MB_OK + MB_ICONERROR);
end;

{***********************************************************************
File Name : ValidaArchivo
Author : mbecerra
Date Created: 14-mayo-2008
Language : ES-CL
Description : Realiza las siguientes validaciones sobre el archivo de entrada COPEC PDU:
            	* Que el nombre concuerde con el especificado en el documento (PHDCOPECyymmdd##.TXT)
                * Eliminar las l�neas en blanco
                * que el registro de control (el �ltimo) coincida con la cantidad de registros
*************************************************************************************}
function TCopecPDUForm.ValidaArchivo;
resourcestring
    MSG_ARCHIVO_FORMATO_PDU = 'El nombre del archivo %s no tiene un formato v�lido (PHDCOPECyymmdd##.TXT)';
    MSG_ARCHIVO_FORMATO_PDUT = 'El nombre del archivo %s no tiene un formato v�lido (PMCCOPECyymmdd##.TXT)';
    MSG_ARCHIVO_CANTIDAD_REGISTROS = 'La cantidad de registros en el archivo %s no coincide con la cantidad informada en el registro de control';
    MSG_ARCHIVO_REGISTRO_CONTROL = 'No se encontr� el registro de control en el archivo %s';
    MSG_ARCHIVO_VACIO = 'El archivo %s no contiene registros (est� vac�o)';
var
    ArchivoAux, ArchivoNombre, ArchivoFecha : AnsiString;
    i : integer;
begin
    Result := True;
    ArchivoNombre := ExtractFileName(txtArchivo.Text);
    ArchivoAux    := ExtractFileExt(ArchivoNombre);
    i := Pos(ArchivoAux, ArchivoNombre);
    if i > 0 then ArchivoNombre := Copy(ArchivoNombre,1, i - 1)
    else Result := False;

    if Result then begin
    	ArchivoNombre    := UpperCase(ArchivoNombre);
    	ArchivoAux       := UpperCase(ArchivoAux);
    {validar la extensi�n}
    	if ArchivoAux <> ARCHIVO_EXTENSION then Result := False;
    end;

    {validar el prefijo}
    if Result and (Copy(ArchivoNombre,1,8) <> FArchivoPrefijo ) then Result := False;

    {validar el formato de fecha}
    if Result then begin
    	ArchivoFecha := Copy(ArchivoNombre, Length(FArchivoPrefijo) + 1, 6);
    	if not EsNumero(ArchivoFecha) then	Result := False;

        if Result then begin
            try
                EncodeDate(StrToInt(Copy(ArchivoFecha,1,2)) + 2000, StrToInt(Copy(ArchivoFecha,3,2)), StrToInt(Copy(ArchivoFecha,5,2))); 
            except
            	Result := False;
            end;
        end;
    end;

    {secuencia num�rica}
    if Result and (not EsNumero(Copy(ArchivoNombre, Length(ArchivoNombre) -1, 2))) then Result := False;
     
	if not Result then begin
    	if FEsPDU then MsgBox(Format(MSG_ARCHIVO_FORMATO_PDU, [txtArchivo.Text]), Caption, MB_OK + MB_ICONEXCLAMATION)
        else MsgBox(Format(MSG_ARCHIVO_FORMATO_PDUT, [txtArchivo.Text]), Caption, MB_OK + MB_ICONEXCLAMATION);
    end
    else begin
        {quitar l�neas en blanco}
        i := 0;
        while i < FCOPECPDU.Count do begin
          	if FCOPECPDU[i] = '' then FCOPECPDU.Delete(i)
            else  Inc(i);
            
        end;

        if FCOPECPDU.Count = 0 then begin
        	MsgBox(Format(MSG_ARCHIVO_VACIO, [txtArchivo.Text]), Caption, MB_OK + MB_ICONEXCLAMATION);
        	Result := False;
        end;
    end;

    {registro de control}
    if Result and FEsPDU then begin
        ArchivoAux := FCOPECPDU[FCOPECPDU.Count- 1];
        if not EsNumero(ArchivoAux) then begin             {existencia}
        	MsgBox(Format(MSG_ARCHIVO_REGISTRO_CONTROL, [txtArchivo.Text]), Caption, MB_OK + MB_ICONEXCLAMATION);
        	Result := False;
        end;

        if Result and (FCOPECPDU.Count <> StrToInt(ArchivoAux)) then begin {cuadratura}
        	MsgBox(Format(MSG_ARCHIVO_CANTIDAD_REGISTROS, [txtArchivo.Text]), Caption, MB_OK + MB_ICONEXCLAMATION);
        	Result := False;
        end;

    end;
    
end;

procedure TCopecPDUForm.btnCancelarClick(Sender: TObject);
begin
	FCancelar := True;
    Application.ProcessMessages;
end;

{
    Revision : 1
        Author : pdominguez
        Date   : 05/08/2010
        Description : SS 909 BHTU Copec - Rechazos
            - Se a�ade la gesti�n de la lista de Advertencias.
}
procedure TCopecPDUForm.btnProcesarClick(Sender: TObject);
resourcestring
  	MSG_FILE_ALREADY_PROCESSED = 'El archivo %s ya fue procesado' + crlf + 'Desea reprocesarlo ?';
    MSG_ERROR_FILE_NOT_EXISTS = 'El archivo indicado no existe';
  	MSG_ERROR_CANNOT_OPEN_COPEC_PDU_FILE	= 'No se puede abrir el archivo COPEC PDU';
    MSG_ERROR = 'Error';
    MSG_ERROR_DAYPASS_REPORT		= 'Error emitiendo informe resumen de Pases Diarios';               //SS_1313_NDR_20150615
const
  	STR_PROCESS_SUCCEDED = 'El proceso finaliz� con �xito!';
  	STR_PROCESS_ENDED_WITH_ERRORS = 'El proceso finaliz� con errores';
    STR_PROCESS_CANCELLED = 'El proceso ha sido Cancelado por el Usuario';
  	STR_PROCESS_COULD_NOT_BE_COMPLETED = 'El proceso no se pudo completar.';
var
    InterfazUsada : integer;
begin
    //verifica que el archivo sea valido
    if not FileExists(txtArchivo.Text) then  begin
    	MsgBox(MSG_ERROR_FILE_NOT_EXISTS, Caption, MB_OK + MB_ICONEXCLAMATION);
        exit;
    end;

  	//Verifica que el Archivo no haya sido procesado
	  if  VerificarArchivoProcesado(DMConnections.BaseCAC, txtArchivo.Text) then begin
          //informo que el archivo ya fue procesado, consulto al operador si desea continuar
          if MsgBox (Format (MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtArchivo.Text)]), Caption, MB_ICONWARNING or MB_YESNO) <> IDYES then Exit;
    end;

    //Creo el stringlist
    FCOPECPDU := TStringList.Create;
    FCOPECErrors := TStringList.Create;
    FCOPECAdvertencias := TStringList.Create; // Rev. 1 (SS 909)
    FProcesando := True;
    HabilitarBotones();
    if FEsPDU then InterfazUsada := RO_MOD_INTERFAZ_ENTRADA_COPEC_PDU
    else  InterfazUsada := RO_MOD_INTERFAZ_ENTRADA_COPEC_PDUT;

	FSumaMontoTotalArchivo  := 0; // SS 909

    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        try
          FCOPECPDU.LoadFromFile(txtArchivo.Text);  //Leo el archivo COPEC PDU
          if ValidaArchivo() then begin
          	// Se registra la operacion
            if not RegistrarOperacion(InterfazUsada, ExtractFileName(txtArchivo.Text)) then begin
            	Exit;
            end;

            if ProcesarArchivo() then begin
                btnCancelar.Enabled := False;
                Application.ProcessMessages;

            	if not FErrores then begin
                    ShowMsgBoxCN(Caption, STR_PROCESS_SUCCEDED, MB_ICONINFORMATION, Self);
//                	MsgBox(STR_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION);
                end
                else begin
                    ShowMsgBoxCN(Caption, STR_PROCESS_ENDED_WITH_ERRORS, MB_ICONWARNING, Self);
//                	MsgBox(STR_PROCESS_ENDED_WITH_ERRORS, Caption, MB_ICONINFORMATION);
                end;

                lblDetalle.Caption := 'Cerrando Proceso. Espere por favor ...';
                Application.ProcessMessages;
                AgregarErroresParseo();
                ActualizarLogAlFinal(True); // Rev. 1 (SS 909)
                FProcesando := False;
                lblDetalle.Caption := 'Moviendo Archivos. Espere por favor ...';
                Application.ProcessMessages;

                MoverArchivoProcesado(Caption, txtArchivo.Text, GoodDir(FCOPEC_PDU_Directorio_Procesados) + ExtractFileName(txtArchivo.Text));
                if FArchivoVenta <> '' then begin
                    MoverArchivoProcesado(Caption, FArchivoVenta, GoodDir(FCOPEC_PDU_Directorio_Procesados) + ExtractFileName(FArchivoVenta), True);
                end;

                if FileExists(FArchivoOtros) then begin
                	MoverArchivoProcesado(Caption, FArchivoOtros, GoodDir(FCOPEC_PDU_Directorio_Procesados) + ExtractFileName(FArchivoOtros), True);
                end;

                Self.Visible := False;
                Application.ProcessMessages;

                GenerarReportedeFinalizacion;

                try                                                                                   //SS_1313_NDR_20150615
                  RBIResumen.Execute(True);                                                           //SS_1313_NDR_20150615
                except                                                                                //SS_1313_NDR_20150615
                  on e: Exception do begin                                                            //SS_1313_NDR_20150615
                    MsgBoxErr(MSG_ERROR_DAYPASS_REPORT, e.Message, 'Error', MB_ICONERROR);            //SS_1313_NDR_20150615
                  end;                                                                                //SS_1313_NDR_20150615
                end;                                                                                  //SS_1313_NDR_20150615

            end
            else begin
            	if FCancelar then begin
                	RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacion);
                    MsgBox(STR_PROCESS_CANCELLED, Caption, MB_ICONERROR);
                end
                else begin
                    ActualizarLogAlFinal(False); // Rev. 1 (SS 909)
                	MsgBox(STR_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
                end;
            end;
          end;
        except
            on E : Exception do begin
                MsgBoxErr(MSG_ERROR_CANNOT_OPEN_COPEC_PDU_FILE, E.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
      	if Assigned(FCOPECPDU) then FreeAndNil(FCOPECPDU); // Rev. 1 (SS 909)
        if Assigned(FCOPECErrors) then FreeAndNil(FCOPECErrors); // Rev. 1 (SS 909)
        if Assigned(FCOPECAdvertencias) then FreeAndNil(FCOPECAdvertencias); // Rev. 1 (SS 909)

      	if not FProcesando then
            Self.Close
        else HabilitarBotones;

        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

procedure TCopecPDUForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TCopecPDUForm.HabilitarBotones;
begin
    lblDetalle.Caption := '';
    pbProgreso.Position := pbProgreso.Min;
    FErrores:= False;
    FCancelar := False;
    if FProcesando then begin
        btnCancelar.Enabled := True;
        btnProcesar.Enabled := False;
        btnSalir.Enabled := False;
        txtArchivo.Enabled := False;
        pnlAvance.Visible := True;
    end
    else begin
        btnCancelar.Enabled := False;
        txtArchivo.Text := ExtractFilePath(txtArchivo.Text);
        btnProcesar.Enabled := FileExists(txtArchivo.Text);
        txtArchivo.Enabled := True;
        btnSalir.Enabled := True;
        pnlAvance.Visible := False;
    end;
end;

procedure TCopecPDUForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

procedure TCopecPDUForm.txtArchivoButtonClick(Sender: TObject);
resourcestring
    MSG_ERROR           = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST = 'El archivo %s no existe';
    MSG_ARCHIVO_VENTAS  = 'No se encontr� archivo de ventas asociado: %s ';
    MSG_NO_SE_HARA      = 'No se har� validaci�n sobre la fecha de venta de cada pase';
    MSG_VENTAS_INVALIDO = 'Se ha encontrado asociado un archivo de ventas [ %s ] que no es v�lido.';
    MSG_VENTAS_1        = 'Se ha encontrado asociado el archivo de ventas %s.';
    MSG_VENTAS_2        = 'Los pases ser�n validados entre las siguientes fechas:';
    MSG_ERROR_FILE_DOES_NOT_MATCH       = 'Identificador de Concesionaria "%s" del archivo no corresponde a Concesionaria Nativa';   //SS_1256_NDR_20150505

var
  FiltroArchivo : AnsiString;
  ArchivoVentas, PathArchivoVentas, Auxiliar : AnsiString;
  lstArchivoVentas : TStringList;
begin
    dlgOpen.InitialDir := FCOPEC_PDU_Directorio_Entrada;
  	dlgOpen.FileName := '';
    FFechaArchivoVentasIni := 0;
    FFechaArchivoVentasFin := 0;
    if FEsPDU then begin
        FArchivoPrefijo := ARCHIVO_PREFIJO_PDU;
        FiltroArchivo := 'Archivos COPEC PDU |';
    end
    else begin
    	FArchivoPrefijo := ARCHIVO_PREFIJO_PDUT;
        FiltroArchivo := 'Archivos COPEC PDUT |';
    end;

    dlgOpen.Filter := FiltroArchivo + FArchivoPrefijo + '*' + ARCHIVO_EXTENSION;
    if dlgOpen.Execute then begin
        txtArchivo.Text := UpperCase( dlgOpen.Filename );
        if not FileExists( txtArchivo.text ) then begin
          	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtArchivo.Text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;


        if (RightStr( Trim( ReplaceStr(txtArchivo.text,'.TXT','')) , 2 )<>                                                          //SS_1256_NDR_20150505
            ('0'+Trim(IntToStr(ObtenerCodigoConcesionariaNativa)))                                                                  //SS_1256_NDR_20150505
           ) then                                                                                                                   //SS_1256_NDR_20150505
        begin                                                                                                                       //SS_1256_NDR_20150505
        	MsgBox( Format( MSG_ERROR_FILE_DOES_NOT_MATCH,                                                                            //SS_1256_NDR_20150505
                          [RightStr( Trim( ReplaceStr(txtArchivo.text,ARCHIVO_EXTENSION,'')) , 2 )]                                            //SS_1256_NDR_20150505
                        ),                                                                                                          //SS_1256_NDR_20150505
                  MSG_ERROR,                                                                                                        //SS_1256_NDR_20150505
                  MB_ICONERROR                                                                                                      //SS_1256_NDR_20150505
                );                                                                                                                  //SS_1256_NDR_20150505
            Exit;                                                                                                                   //SS_1256_NDR_20150505
        end;                                                                                                                        //SS_1256_NDR_20150505


    end;

    btnProcesar.Enabled := FileExists(txtArchivo.Text);
    FArchivoVenta := '';
    if btnProcesar.Enabled then begin  {ver si existe el archivo de ventas}
    	ArchivoVentas     := ExtractFileName(txtArchivo.Text);
        PathArchivoVentas := ExtractFilePath(txtArchivo.Text);
        if FEsPDU then begin
            ArchivoVentas := 'PVU' + Copy(ArchivoVentas,4,Length(ArchivoVentas));
            FArchivoOtros := 'PQM' + Copy(ArchivoVentas,4,Length(ArchivoVentas));
        end
        else begin
        	ArchivoVentas := 'PVT' + Copy(ArchivoVentas,4,Length(ArchivoVentas));
            FArchivoOtros := 'PQT' + Copy(ArchivoVentas,4,Length(ArchivoVentas));
        end;

        ArchivoVentas := ChangeFileExt(ArchivoVentas, '.CSV');
        FArchivoOtros := ExtractFilePath(txtArchivo.Text) + FArchivoOtros;

        if FileExists(PathArchivoVentas + ArchivoVentas) then begin
            lstArchivoVentas := TStringList.Create;
            lstArchivoVentas.LoadFromFile(PathArchivoVentas + ArchivoVentas);
            if lstArchivoVentas.Count >= 2 then begin
                try
                	Auxiliar := GetItem(lstArchivoVentas[1], 5, ';');
                    FFechaArchivoVentasIni := EncodeDate(StrToInt(Copy(Auxiliar,1,4)), StrToInt(Copy(Auxiliar,5,2)),StrToInt(Copy(Auxiliar,7,2)));
                    Auxiliar := GetItem(lstArchivoVentas[1], 7, ';');
                    FFechaArchivoVentasFin := EncodeDate(StrToInt(Copy(Auxiliar,1,4)), StrToInt(Copy(Auxiliar,5,2)),StrToInt(Copy(Auxiliar,7,2)));
                	MsgBox(Format(MSG_VENTAS_1, [ArchivoVentas]) + CRLF + MSG_VENTAS_2 + CRLF + CRLF +
                    		'Inicio: ' + DateToStr(FFechaArchivoVentasIni) + CRLF +
                            'Fin: ' + DateToStr(FFechaArchivoVentasFin), Caption, MB_ICONINFORMATION);
                    FArchivoVenta := PathArchivoVentas + ArchivoVentas;
                except
                	MsgBox(Format(MSG_VENTAS_INVALIDO, [ArchivoVentas]), Caption, MB_ICONEXCLAMATION);
                    FFechaArchivoVentasIni := 0;
    				FFechaArchivoVentasFin := 0;
                end;
            end
            else
                MsgBox(Format(MSG_VENTAS_INVALIDO, [ArchivoVentas]), Caption, MB_ICONEXCLAMATION);

            lstArchivoVentas.Free;
        end
        else
            MsgBox(Format(MSG_ARCHIVO_VENTAS, [ArchivoVentas]) + CRLF + MSG_NO_SE_HARA, Caption, MB_ICONEXCLAMATION);
    end;

end;

end.
