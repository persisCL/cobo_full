{-------------------------------------------------------------------------------
 File Name: FrmLogOperaciones.pas
 Author:    lgisuk
 Date Created: 25/11/2004
 Language: ES-AR
 Description:  M�dulo de la interface TransBank - Log de Operaciones Realizadas
--------------------------------------------------------------------------------
 Revision History
--------------------------------------------------------------------------------
 Author: rcastro
 Date Created: 17/12/2004
 Description: revisi�n general

 Revision : 1
     Author : pdominguez
     Date   : 24/07/2009
     Description : SS 812
        - Se modificaron/crearon los siguientes procedimientos/funciones:
            TieneInformeAsociado
            DBListExLinkClick
            MostrarReporteEnvioNovedadesCMR

  Revision : 2
     Author : jjofre
     Date   : 03/Junio/2010
     Description : SS 884
        - Se modificaron/crearon los siguientes procedimientos/funciones:
            TFLogOperaciones class
            TieneInformeAsociado
            DBListExLinkClick
        - Se agrega la unit
           frmRptRecepcionUniversoBancoChile

  Revision : 3
     Author : pdominguez
     Date   : 13/08/2010
     Description : SS 909 - BHTU COPEC - Rechazos
        - Se modificaron/crearon los siguientes procedimientos/funciones:
            DBListExLinkClick,
            MostrarReporteBHTU

     Author : mcabello
     Date	: 15/01/2014
     Firma	: SS_1061_MCA_20140115
     Description : se agrega al log de operaciones el reporte de recepcion de novedades CMR


Firma       : SS_1156B_NDR_20140128
Description : Mostar las operaciones de baja forzada masiva y poder imprimir el reporte asociado

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1400_NDR_20151007
Description : Reporte de Raspuestas a Consultas RNVM

Etiqueta	: 20160531 MGO
Descripci�n	: Se agrega llamada a reporte de movimientos TBK

Etiqueta	: 20160615 MGO
Descripci�n : Se quita reporte obsoleto de d�bitos

Etiqueta    :   TASK_039_MGO_20160702
Descripci�n :   Se pintan de rojo los registros que no finalizaron OK.
                Se agrega filtro de m�dulo.

Etiqueta	:	TASK_037_MGO_20160711
Descripci�n	:	Se agrega reporte detallado
-------------------------------------------------------------------------------}

unit FrmLogOperaciones;

interface

uses
  //Log Operaciones
  DMConnection,                     // coneccion a base de datos OP_CAC
  Util,                             // IIF   .
  UtilProc,                         // Mensajes
  PeaTypes,
  PeaProcs,                             //Nowbase
  Rstrings,                         // STR_ERROR
  frmRptNominas,                    //generaci�n de N�minas
  FrmRptEnvioComprobantes,          // Reporte de Comprobantes Enviados
  FrmRptRecepcionRendiciones,      // Reporte de Comprobantes Recibidos
  FrmRptDayPass,                    // Reporte de Day Pass Recibidos
  fReporteActivacionesSerbanc ,     // Reporte de activaciones serbanc
  fReporteDesactivacionesSerbanc,   // Reporte de desactivaciones de serbanc
  fReporteAccionesSerbanc ,         // Reporte de Acciones serbanc
  fReportePagosSerbanc ,            // Reporte de Pagos serbanc
  fReporteCTCSerbanc ,              // Reporte de CTC serbanc
  fReporteNvsDirSerbanc ,           // Reporte de Nuevas Dir serbanc
  fReporteEnvioInfraccionesBHTU,    // Reporte BHTU - Envio Infraciones
  FrmRptEnvioDebitos,               // Reporte de Envio de Debitos
  FrmRptRecepcionComprobantesServipag, //
  FrmRptRecepcionComprobantesSantander,
  FrmRptRecepcionComprobantesMisCuentas,
  FrmRptRecepcionComprobantesBDP,
  frmRptRecepcionAnulacionesServipag, // Reporte de Anulaciones Servipag
  FrmRptRecepcionCaptacionesCMR,
  frmRptRecepcionUniversoSantander, // Reporte de errores de Recepci�n de Universo Santander PAC
  frmRptDayPassCopec,
  fReporteRecepcionVentasBHTU,
  FrmRptEnvioNovedades, // Reporte envio Novedades CMR (SS 812)
  frmRptRecepcionUniversoBancoChile, // REV.2
  FrmRptRecepcionMovimientosTBK,
  FrmRptRecepcionNovedades, 		 //SS_1061_MCA_20140115
  FrmRptBajasForzadas,                          //SS_1156B_NDR_20140128
  FrmRptRecepcionRespuestasConsultasRNVM,       //SS_1400_NDR_20151007
  frmRptDetalleInterfaces,                      // 
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, StdCtrls, ExtCtrls, DPSControls, ListBoxEx, DBListEx,
  Validate, DateEdit, StrUtils,UtilDB, VariantComboBox;     

type
  TFLogOperaciones = class(TForm)
    DataSource: TDataSource;
    PAbajo: TPanel;
    PDerecha: TPanel;
    Parriba: TPanel;
    PAderecha: TPanel;
    DBListEx: TDBListEx;
    Edesde: TDateEdit;
    EHasta: TDateEdit;
    LDesde: TLabel;
    LHasta: TLabel;
    SalirBTN: TButton;
    AbrirBTN: TButton;
    SPObtenerLogOperacionesTBK: TADOStoredProc;
    SPObtenerLogOperacionesTBKCodigoOperacionInterfase: TIntegerField;
    SPObtenerLogOperacionesTBKCodigoModulo: TWordField;
    SPObtenerLogOperacionesTBKFecha: TDateTimeField;
    SPObtenerLogOperacionesTBKNombreArchivo: TStringField;
    SPObtenerLogOperacionesTBKUsuario: TStringField;
    SPObtenerLogOperacionesTBKObservaciones: TMemoField;
    SPObtenerLogOperacionesTBKReprocesamiento: TBooleanField;
    SPObtenerLogOperacionesTBKFechaHoraRecepcion: TDateTimeField;
    SPObtenerLogOperacionesTBKUltimoRegistroEnviado: TIntegerField;
    SPObtenerLogOperacionesTBKFechaHoraFinalizacion: TDateTimeField;
    SPObtenerLogOperacionesTBKFinalizoOK: TBooleanField;
    SPObtenerLogOperacionesTBKDescripcion: TStringField;
    SPObtenerLogOperacionesTBKTieneComprobantesEnviados: TBooleanField;
    SPObtenerLogOperacionesTBKTieneComprobantesRecibidos: TBooleanField;
    SPObtenerLogOperacionesTBKTieneDayPassRecibidos: TBooleanField;
    SPObtenerLogOperacionesTBKTieneReporteSerbanc: TBooleanField;
    SPObtenerLogOperacionesTBKTieneReporteBHTU: TBooleanField;
    SPObtenerLogOperacionesTBKTieneReporteDebitos: TBooleanField;
    SPObtenerLogOperacionesTBKTieneReporteCaptaciones: TBooleanField;
    SPObtenerLogOperacionesTBKDiferencia: TDateTimeField;
    SPObtenerLogOperacionesTBKReporte: TStringField;
    SPObtenerLogOperacionesTBKTieneReporteUnivBancoChile: TBooleanField;
    blnfldSPObtenerLogOperacionesTBKTieneReporteRespuestaNovedadesCMR: TBooleanField;
    SPObtenerLogOperacionesTBKTieneRespuestasConsultasRNVM: TBooleanField;
    SPObtenerLogOperacionesTBKTieneReporteMovimientosTBK: TBooleanField;
    Label1: TLabel;
    vcbModulo: TVariantComboBox;
    procedure AbrirBTNClick(Sender: TObject);
    procedure SalirBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SPObtenerLogOperacionesTBKCalcFields(DataSet: TDataSet);
    procedure DBListExLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure DBListExColumns0HeaderClick(Sender: TObject);
    procedure DBListExDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
  private
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;
    Function TieneInformeAsociado(DataSet: TDataSet): boolean;
    function MostrarReporteSerbanc(DataSet: TDataSet; var Error: AnsiString): Boolean;
    function MostrarReporteBHTU(DataSet: TDataSet; var Error: AnsiString): Boolean;
    procedure MostrarReporteEnvioNovedadesCMR(CodigoOperacionInterfase: Integer);

    { Private declarations }
  public
    function Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
    { Public declarations }
  end;

var
  FLogOperaciones: TFLogOperaciones;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 25/11/2004
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFLogOperaciones.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
Var
	S: TSize;
    spObtenerModulosInterfaces: TADOStoredProc;
begin
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Result := False;
    
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         
    Color := FColorMenu;

    //Manejo el MdiChild
    if MDIChild then begin
    		S := GetFormClientSize(Application.MainForm);
    		SetBounds(0, 0, S.cx, S.cy);
  	end else begin
    		FormStyle := fsNormal;
    		Visible := False;
   	end;
  	CenterForm(Self);
  	Caption := AnsiReplaceStr(txtCaption, '&', '');
    //Fechas
    edesde.Date:=now;
    ehasta.Date:=now;
    try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected;
        // Combo de interfaces
        spObtenerModulosInterfaces := TADOStoredProc.Create(Self);
        try
            spObtenerModulosInterfaces.Connection := DMConnections.BaseCAC;
            spObtenerModulosInterfaces.CommandTimeout := 60;
            spObtenerModulosInterfaces.ProcedureName := 'ObtenerModulosInterfaces';
            spObtenerModulosInterfaces.Parameters.Refresh;
            spObtenerModulosInterfaces.Open;

            if spObtenerModulosInterfaces.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spObtenerModulosInterfaces.Parameters.ParamByName('@ErrorDescription').Value);

            vcbModulo.Items.Add(TODOS, 0);
            while not spObtenerModulosInterfaces.Eof do begin
                vcbModulo.Items.Add(spObtenerModulosInterfaces.FieldByName('Descripcion').AsString,
                                    spObtenerModulosInterfaces.FieldByName('CodigoModulo').AsInteger);
                spObtenerModulosInterfaces.Next;
            end;
            vcbModulo.ItemIndex := vcbModulo.Items.IndexOfValue(0);
        finally                              
            spObtenerModulosInterfaces.Close;
            spObtenerModulosInterfaces.Free;
        end;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AbrirBTNClick
  Author:    lgisuk
  Date Created: 10/03/2005
  Description: Abro la consulta
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFLogOperaciones.AbrirBTNClick(Sender: TObject);
Resourcestring
    MSG_ERROR_OPEN = 'Error al Abrir';
    MSG_ERROR = 'Error';
Const
    STR_FECHA_DESDE = 'Debe Indicar Fecha Desde';
    STR_FECHA_HASTA = 'Debe Indicar Fecha Hasta';
    STR_FECHA_MAYOR = 'La Fecha Hasta debe ser mayor a la Fecha Desde';
begin
    //Obligo a cargar fecha desde
    if edesde.Date = NULLDATE then begin
        MsgBoxBalloon(STR_FECHA_DESDE, STR_ERROR, MB_ICONSTOP, edesde);
        edesde.SetFocus;
        Exit;

    end;

    //Obligo a cargar fecha hasta
    if ehasta.Date = NULLDATE then begin
        MsgBoxBalloon(STR_FECHA_HASTA, STR_ERROR, MB_ICONSTOP, ehasta);
        ehasta.SetFocus;
        exit;
    end;

    //valido que la fecha hasta sea mayor a la fecha desde
    if edesde.date > ehasta.Date then begin
        MsgBoxBalloon(STR_FECHA_MAYOR, STR_ERROR, MB_ICONSTOP, ehasta);
        ehasta.SetFocus;
        exit;
    end;

    //Abro la consulta
    with SPObtenerLogOperacionesTbk.Parameters do begin
        Refresh;
        ParamByName('@DesdeFecha').Value:= Edesde.date;
        ParamByName('@HastaFecha').Value:= Ehasta.date;
        ParamByName('@CodigoModulo').Value := vcbModulo.Value;
    end;
    try
        SPObtenerLogOperacionesTbk.Close;
        SPObtenerLogOperacionesTbk.CommandTimeout := 500;
        SPObtenerLogOperacionesTbk.Open;

        if SPObtenerLogOperacionesTbk.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
            raise Exception.Create(SPObtenerLogOperacionesTbk.Parameters.ParamByName('@ErrorDescription').Value); 
        
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR, e.message, MSG_ERROR_OPEN, MB_ICONERROR); //el stored procedure podria fallar
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListExColumns0HeaderClick
  Author:    lgisuk
  Date Created: 03/06/2005
  Description: permito ordenar haciendo click en la columna
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFLogOperaciones.DBListExColumns0HeaderClick(Sender: TObject);
begin
	if SPObtenerLogOperacionesTbk.IsEmpty then Exit;

	if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   	else TDBListExColumn(sender).Sorting := csAscending;

	SPObtenerLogOperacionesTbk.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

{-----------------------------------------------------------------------------
  Function Name: TieneInformeAsociado
  Author:    lgisuk
  Date Created: 10/03/2005
  Description:
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  17/09/2007
  Verifico si tiene reporte de captaciones
-----------------------------------------------------------------------------
  Revision 2
  nefernandez
  02/06/2008
  Se verifica si tiene reporte de errores de Universo Santander PAC

  Revision 3
  Author: mbecerra
  Date: 23-Junio-2008
  Description: Modificado para que siempre visualice el reporte, al menos para los
  				interfaces mas usadas por ahora.

  Revision 4
  Author : pdominguez
  Date   : 24/07/2009
  Description : SS 812
        - Se incluye la constante RO_MOD_INTERFAZ_SALIENTE_NOVEDADES_CMR dentro
        del set de "reportes conocidos" para que se habilite la reimpresi�n
        del reporte para este tipo Interfaz.
-----------------------------------------------------------------------------}
Function TFlogOperaciones.TieneInformeAsociado(DataSet: TDataSet): boolean;
Resourcestring
    MSG_ERROR_OPEN = 'Error al';
    MSG_ERROR = 'Error';
begin
    Result:=False;
    try
        with SPObtenerLogOperacionesTBK do begin
            Result:= Fieldbyname('TieneComprobantesEnviados').AsBoolean
                        or Fieldbyname('TieneComprobantesRecibidos').AsBoolean
                            or Fieldbyname('TieneDayPassRecibidos').AsBoolean
                                or Fieldbyname('TieneReporteSerbanc').AsBoolean
                                    or FieldByName('TieneReporteBHTU').AsBoolean
                                      or FieldByName('TieneReporteDebitos').AsBoolean
                                        or FieldByName('TieneReporteCaptaciones').AsBoolean
                                            or FieldByName('TieneReporteUnivSantander').AsBoolean
                                                or FieldByName('TieneReporteUnivBancoChile').AsBoolean
                                                // INICIO : 20160531 MGO
                                                or FieldByName('TieneReporteMovimientosTBK').AsBoolean
                                                // FIN : 20160531 MGO
                                                	or FieldByName('TieneReporteRespuestaNovedadesCMR').AsBoolean
                                                    or FieldByName('TieneRespuestasConsultasRNVM').AsBoolean;
        end;

        if not Result then begin  {ver si es alg�n reporte conocido}
            Result := SPObtenerLogOperacionesTBK.FieldByName('CodigoModulo').AsInteger in [
            					RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO,         {DayPass}
                                RO_MOD_INTERFAZ_ENTRADA_COPEC_PDU,            {CopecPDU}
                                RO_MOD_INTERFAZ_ENTRADA_COPEC_PDUT,           {CopecPDUT}
                                RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO_TARDIO,  {DayPass tard�o}
                                RO_MOD_INTERFAZ_SALIENTE_GENERACION_NOMINAS,  {Generaci�n de N�minas}
                                RO_MOD_INTERFAZ_SALIENTE_NOVEDADES_CMR,       {GeneracionNovedades CMR}
                                RO_MOD_INTERFAZ_ENTRANTE_BAJAS_FORZADAS,        {Bajas Forzadas CAC}
                                RO_MOD_INTERFAZ_ENTRANTE_RESPUESTAS_CONSULTAS_RNVM {RespuestasConsultasRNVM} 
                                ]
        end;

    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, MSG_ERROR_OPEN, MB_ICONERROR); //el stored procedure podria fallar
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: SPObtenerLogOperacionesTBKCalcFields
  Author:    lgisuk
  Date Created: 10/03/2005
  Description: Muestro 'Ver Informe..' si el registro tiene un informe asociado
  Parameters: DataSet: TDataSet
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFLogOperaciones.SPObtenerLogOperacionesTBKCalcFields(DataSet: TDataSet);
const
    { INICIO : TASK_037_MGO_20160711
    STR_VIEW_REPORT = 'Ver Informe..';
    }
    STR_VIEW_REPORT = 'Ver...';
    // FIN : TASK_037_MGO_20160711
begin
    //si tiene asociado informe
    if TieneInformeAsociado(DataSet) then begin
        with dataset do begin
            Fieldbyname('Reporte').asstring := STR_VIEW_REPORT;
        end;
    end;

    // INICIO : TASK_037_MGO_20160711
    // si tiene detalle
    if SPObtenerLogOperacionesTBK.FieldByName('CodigoReporte').AsInteger > 0 then
        DataSet.Fieldbyname('ReporteXML').AsString := STR_VIEW_REPORT
    else
        DataSet.Fieldbyname('ReporteXML').AsString := '';
    // FIN : TASK_037_MGO_20160711
end;

{-----------------------------------------------------------------------------
  Function Name: DBListExDrawText
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: Asigno formato a la grilla
  Parameters: Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFLogOperaciones.DBListExDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
var Hour, Min, Sec, MSec: Word;
begin
    //Obtengo el tiempo que duro el proceso
    if Column.FieldName = 'Diferencia' then begin
        DecodeTime(SPObtenerLogOperacionesTBK.FieldByName('Diferencia').asDateTime,Hour, Min, Sec, MSec);
        Text := PadL(IntToStr(Hour),2,'0') + ':' + PadL(IntToStr(Min),2,'0') + ':'+ PadL(IntToStr(Sec),2,'0');
    end;
	// INICIO : TASK_039_MGO_20160702
    if not SPObtenerLogOperacionesTBK.FieldByName('FinalizoOK').AsBoolean then begin
        if odSelected in State then
                Sender.Canvas.Font.Color := clMaroon
            else
                Sender.Canvas.Font.Color := clRed;
    end;
	// FIN : TASK_039_MGO_20160702
end;

{-----------------------------------------------------------------------------
  Function Name: DBListExLinkClick
  Author:    lgisuk
  Date Created: 10/03/2005
  Description: Muestra el Reporte de Finalizacion de Proceso
  Parameters: Sender: TCustomDBListEx;Column: TDBListExColumn
  Return Value: None

  Revision 1:
      Author : ggomez
      Date : 12/07/2006
      Description : Si tiene Comprobantes Recibidos y el M�dulo es de Recepciones
        Servipag, mostrar el reporte de recepci�n Servipag.

  Revision 2:
      Author : ggomez
      Date : 13/07/2006
      Description : Si tiene Comprobantes Recibidos y el M�dulo es de Recepciones
        Santander, mostrar el reporte de recepci�n Santander.

  Revision 4:
      Author : nefernandez
      Date : 26/07/2007
      Description : Si tiene Comprobantes Recibidos y el M�dulo es de Anulaciones
        Servipag, mostrar el reporte de Anulaciones Servipag.

  Revision 5:
      Author : lgisuk
      Date : 17/09/2007
      Description : Si tiene captaciones recibidas, mostrar el reporte de Captaciones.

  Revision 7:
      Author : nefernandez
      Date : 02/06/2008
      Description : Inclusi�n para mostrar el reporte de errores del proceso de
      Recepci�n de Universo Santander PAC.

  Revision 8:
  Author: mbecerra
  Date: 23-Julio-2008
  Description: Se agrega el reporte de generaci�n de N�minas.

  Revision 9:
      Author : pdominguez
      Date   : 24/07/2009
      Description : SS 812
            - Se a�ade la reimpresi�n del reporte para la interfaz Envio de
            Novedades CMR.

  Revision 10:
      Author : pdominguez
      Date   : 13/08/2010
      Description : SS 909 - BHTU COPEC - Rechazos
            - Se cambia la forma de creaci�n del Form TRptDayPassForm y la
            llamada al reporte.
-----------------------------------------------------------------------------}
procedure TFLogOperaciones.DBListExLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
resourcestring
    MSG_COULD_NOT_OPEN_THE_REPORT = 'No se puede mostrar el reporte';
const
    STR_SEND_DOCUMENTS  = 'Comprobantes Enviados';
    STR_RECIVE_DOCUMENTS = 'Comprobantes Recibidos';
    STR_DAY_PASS = 'Day Pass';
    RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_PAC		   = 22;
    RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_BANCOCHILE   = 103;
var
    FEnvio:TFRptEnvioComprobantes;
    FRecepcion: TFRptRecepcionRendiciones;
    FDayPass: TRptDayPassForm;
    FDebitos: TFRptEnvioDebitos;
    CodigoOperacionInterfase:integer;
    descError: AnsiString;
    FReporteRecepcionServipag: TFrmRptRecepcionComprobantesServipag;
    FReporteRecepcionSantander: TFrmRptRecepcionComprobantesSantander;
    FReporteRecepcionMisCuentas: TFrmRptRecepcionComprobantesMisCuentas;
    FReporteRecepcionBDP: TFrmRptRecepcionComprobantesBDP;
    FReporteAnulacionServipag: TRptRecepcionAnulacionesServipagForm;
    FReporteRecepcionCaptacionesCMR : TFRptRecepcionCaptacionesCMR;
    FRptRecepcionUniversoSantanderForm : TRptRecepcionUniversoSantanderForm;
    frmRptRecepcionUniversoBancoChile : TRptRecepcionUniversoBancoChileForm;
    frmRptRecepcionNovedades : TFRptRecepcionNovedades;
    frmRptRecepcionRespuestasConsultasRNVM : TFRptRecepcionRespuestasConsultasRNVM;
    frmRptRecepcionMovimientosTBK: TRptRecepcionMovimientosTBKForm;
    frmRptDetalleInterfacesForm: TRptDetalleInterfacesForm;                     //TASK_037_MGO_20160711
    TituloCopec : AnsiString;
begin
    // INICIO : TASK_037_MGO_20160711
    //Obtengo el codigo de operacion interface
    CodigoOperacionInterfase := SpObtenerLogOperacionesTbk.FieldByName('CodigoOperacionInterfase').AsInteger;

    if Column.FieldName = 'ReporteXML' then begin
        Application.CreateForm(TRptDetalleInterfacesForm, frmRptDetalleInterfacesForm);
        if not frmRptDetalleInterfacesForm.Inicializar(CodigoOperacionInterfase, SpObtenerLogOperacionesTbk.FieldByName('CodigoModulo').AsInteger) then
            frmRptDetalleInterfacesForm.Release;
    end;
    // FIN : TASK_037_MGO_20160711
    if Column.FieldName = 'Reporte' then begin                                  //TASK_037_MGO_20160711
        //si el registo no tiene informe asociado salgo
        if SpObtenerLogOperacionesTbk.active = false then exit;
        if TieneInformeAsociado(SpObtenerLogOperacionesTbk) = false then exit;

        with SpObtenerLogOperacionesTbk do begin

            ////Obtengo el codigo de operacion interface                                         //TASK_037_MGO_20160711
            //CodigoOperacionInterfase:=FieldByName('CodigoOperacionInterfase').value;           //TASK_037_MGO_20160711

            // Revision 8
            if FieldByName('CodigoModulo').AsInteger = RO_MOD_INTERFAZ_SALIENTE_GENERACION_NOMINAS then begin
                Application.CreateForm(TRptNominasForm, RptNominasForm);
                try
                    RptNominasForm.Inicializar(CodigoOperacionInterfase);
                finally
                    RptNominasForm.Release;
                end;
            
            end;

            //Si esta asociado a un informe de comprobantes enviados
            if FieldByName('TieneComprobantesEnviados').value = True then begin

                //muestro el reporte
                Application.CreateForm(TFRptEnvioComprobantes,FEnvio);
                if not fEnvio.Inicializar(STR_SEND_DOCUMENTS , CodigoOperacionInterfase) then fEnvio.Release;

            end;

            //Si esta asociado a un informe de comprobantes recibidos
            if FieldByName('TieneComprobantesRecibidos').Value = True then begin

                // Agredado en Revision 1
                case FieldByName('CodigoModulo').AsInteger of
                    RO_MOD_INTERFAZ_ENTRADA_PAGOS_SERVIPAG:
                        begin
                            Application.CreateForm(TFrmRptRecepcionComprobantesServipag, FReporteRecepcionServipag);
                            try
                                FReporteRecepcionServipag.Inicializar(STR_RECIVE_DOCUMENTS, CodigoOperacionInterfase);
                            finally
                                FReporteRecepcionServipag.Release;
                            end;
                        end;
                    RO_MOD_INTERFAZ_ENTRADA_PAGOS_SANTANDER:
                        begin
                            Application.CreateForm(TFrmRptRecepcionComprobantesSantander, FReporteRecepcionSantander);
                            try
                                FReporteRecepcionSantander.Inicializar(STR_RECIVE_DOCUMENTS, CodigoOperacionInterfase);
                            finally
                                FReporteRecepcionSantander.Release;
                            end;
                        end;
                     RO_MOD_INTERFAZ_ENTRADA_PAGOS_MISCUENTAS:
                        begin
                            Application.CreateForm(TFrmRptRecepcionComprobantesMisCuentas, FReporteRecepcionMisCuentas);
                            try
                                FReporteRecepcionMisCuentas.Inicializar(STR_RECIVE_DOCUMENTS, CodigoOperacionInterfase);
                            finally
                                FReporteRecepcionMisCuentas.Release;
                            end;
                        end;
                     RO_MOD_INTERFAZ_ENTRADA_PAGOS_BDP:
                        begin
                            Application.CreateForm(TFrmRptRecepcionComprobantesBDP, FReporteRecepcionBDP);
                            try
                                FReporteRecepcionBDP.Inicializar(STR_RECIVE_DOCUMENTS, CodigoOperacionInterfase);
                            finally
                                FReporteRecepcionBDP.Release;
                            end;
                        end;
                    // Revision 4
                    RO_MOD_INTERFAZ_ENTRANTE_ANULACIONES_SERVIPAG:
                        begin
                            Application.CreateForm(TRptRecepcionAnulacionesServipagForm, FReporteAnulacionServipag);
                            try
                                FReporteAnulacionServipag.Inicializar(STR_RECIVE_DOCUMENTS, CodigoOperacionInterfase);
                            finally
                                FReporteAnulacionServipag.Release;
                            end;
                        end;
                    else
                        begin
                            //muestro el reporte
                            Application.createForm(TFRptRecepcionRendiciones,FRecepcion);
                            if not fRecepcion.Inicializar(STR_RECIVE_DOCUMENTS , CodigoOperacionInterfase) then begin
                                fRecepcion.Release;
                            end;
                        end;
                end; // case

            end;

            //Si esta asociado a un informe de DayPass Recibidos
            if FieldByName('TieneDayPassRecibidos').value  or
              (FieldByName('CodigoModulo').AsInteger in [ RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO,
                                                          RO_MOD_INTERFAZ_ENTRADA_COPEC_PDU,
                                                          RO_MOD_INTERFAZ_ENTRADA_COPEC_PDUT
                                                        ]) then begin

                //muestro el reporte
                if FieldByName('CodigoModulo').AsInteger = RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO then try
                    FDayPass := TRptDayPassForm.Create(Self);
                    if FDayPass.Inicializar(STR_DAY_PASS,CodigoOperacionInterfase) then
                        FDayPass.RBIListado.Execute;
                finally
                    if Assigned(FDayPass) then FreeAndNil(FDayPass);
                end
                else if FieldByName('CodigoModulo').AsInteger in [ RO_MOD_INTERFAZ_ENTRADA_COPEC_PDU, RO_MOD_INTERFAZ_ENTRADA_COPEC_PDUT] then begin
                    Try
                        RptDayPassForm := TRptDayPassForm.Create(Self);
                        if FieldByName('CodigoModulo').AsInteger = RO_MOD_INTERFAZ_ENTRADA_COPEC_PDU then
                            TituloCopec := 'Copec DayPass'
                        else TituloCopec := 'Copec DaypPass Tard�o';

                        if RptDayPassForm.Inicializar(TituloCopec,
                                FieldByName('CodigoOperacionInterfase').AsInteger, not (FieldByName('CodigoModulo').AsInteger = RO_MOD_INTERFAZ_ENTRADA_COPEC_PDU)) then RptDayPassForm.RBIListado.Execute;
                    Finally
                        if Assigned(RptDayPassForm) then FreeAndNil(RptDayPassForm);
                    End;
                end;

            end;

            //Si tiene un reporte serbanc asociado
            if FieldByName('TieneReporteSerbanc').value then begin
                //muestro el reporte
                if not MostrarReporteSerbanc(SPObtenerLogOperacionesTBK, descError) then begin
                    MsgBox(MSG_COULD_NOT_OPEN_THE_REPORT + CRLF + DescError);
                    Exit;
                end;
            end;
            // Si tiene reporte BHTU
            if FieldByName('TieneReporteBHTU').value or
              (FieldByName('CodigoModulo').AsInteger = RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO_TARDIO) then begin
                //muestro el reporte
                if not MostrarReporteBHTU(SPObtenerLogOperacionesTBK, descError) then begin
                    MsgBox(MSG_COULD_NOT_OPEN_THE_REPORT + CRLF + DescError);
                    Exit;
                end;
            end;

            //Si esta asociado a un informe de recepcion  de captaciones
            if FieldByName('TieneReporteCaptaciones').value = True then begin

                //muestro el reporte
                Application.createForm(TFRptRecepcionCaptacionesCMR, FReporteRecepcionCaptacionesCMR);
                if not  FReporteRecepcionCaptacionesCMR.Inicializar(CodigoOperacionInterfase) then  FReporteRecepcionCaptacionesCMR.Release;

            end;

            //Si esta asociado a un informe de errores de Recepci�n de Universo Santander PAC
            if FieldByName('TieneReporteUnivSantander').value = True then begin
                if SPObtenerLogOperacionesTBK.FieldByName('CodigoModulo').AsInteger = RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_PAC then begin
                    Application.CreateForm(TRptRecepcionUniversoSantanderForm, FRptRecepcionUniversoSantanderForm);
                    try
                        try
                            FRptRecepcionUniversoSantanderForm.MostrarReporte(CodigoOperacionInterfase);
                        except
                            on E: Exception do begin
                                MsgBoxErr(MSG_COULD_NOT_OPEN_THE_REPORT, E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    finally
                        FRptRecepcionUniversoSantanderForm.Release;
                    end;
                end;
            end;

            if FieldByName('TieneReporteUnivBancoChile').value = True then begin
                if SPObtenerLogOperacionesTBK.FieldByName('CodigoModulo').AsInteger = RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_BANCOCHILE then begin
                    Application.CreateForm(TRptRecepcionUniversoBancoChileForm, frmRptRecepcionUniversoBancoChile);
                    try
                        try
                            frmRptRecepcionUniversoBancoChile.MostrarReporte(CodigoOperacionInterfase);
                        except
                            on E: Exception do begin
                                MsgBoxErr(MSG_COULD_NOT_OPEN_THE_REPORT, E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    finally
                        frmRptRecepcionUniversoBancoChile.Release;
                    end;
                end;
            end;

            if FieldByName('TieneReporteMovimientosTBK').Value = True then begin
                Application.CreateForm(TRptRecepcionMovimientosTBKForm, frmRptRecepcionMovimientosTBK);
                try
                    try
                        frmRptRecepcionMovimientosTBK.MostrarReporte(CodigoOperacionInterfase);
                    except
                        on E: Exception do begin
                            MsgBoxErr(MSG_COULD_NOT_OPEN_THE_REPORT, E.Message, Caption, MB_ICONERROR);
                            Exit;
                        end;
                    end;
                finally
                    frmRptRecepcionMovimientosTBK.Release;
                end;
            end;

            if FieldByName('TieneReporteRespuestaNovedadesCMR').value = True then begin
                   Application.CreateForm(TFRptRecepcionNovedades, frmRptRecepcionNovedades);
                    if not frmRptRecepcionNovedades.Inicializar(CodigoOperacionInterfase, 'Recepci�n Novedades','0', '0') then frmRptRecepcionNovedades.Release;
            end;

            if FieldByName('CodigoModulo').AsInteger = RO_MOD_INTERFAZ_SALIENTE_NOVEDADES_CMR then
                MostrarReporteEnvioNovedadesCMR(FieldByName('CodigoOperacionInterfase').AsInteger);

            if FieldByName('CodigoModulo').AsInteger = RO_MOD_INTERFAZ_ENTRANTE_BAJAS_FORZADAS then
            begin
                Application.CreateForm(TFRptBajasForzadas, FRptBajasForzadas);
                try
                    FRptBajasForzadas.Inicializar(  CodigoOperacionInterfase,
                                                    QueryGetValueInt(DMconnections.BaseCAC, 'SELECT RegistrosProcesados FROM LogOperacionesInterfases WITH (NOLOCK) WHERE CodigoOperacionInterfase='+inttostr(CodigoOperacionInterfase)),
                                                    QueryGetValueInt(DMconnections.BaseCAC, 'SELECT COUNT(*) FROM ErroresInterfases WHERE CodigoOperacionInterfase='+inttostr(CodigoOperacionInterfase)),
                                                    'Proceso de Bajas Forzadas');
                finally
                    FRptBajasForzadas.Release;
                end;
            end;

            if FieldByName('TieneRespuestasConsultasRNVM').value = True then
            begin
                Application.CreateForm(TFRptRecepcionRespuestasConsultasRNVM, frmRptRecepcionRespuestasConsultasRNVM);
                    if not frmRptRecepcionRespuestasConsultasRNVM.Inicializar('Respuestas a Consultas RNVM Recibidas',CodigoOperacionInterfase) then
                begin
                  frmRptRecepcionRespuestasConsultasRNVM.Release;
                end;
            end;

        end;
    end;                                                                        // TASK_037_MGO_20160711

    // INICIO : TASK_037_MGO_20160711
    if Column.FieldName = 'ReporteXML' then begin

    end;
    // FIN : TASK_037_MGO_20160711
end;

{******************************** Function Header ******************************
Function Name: MostrarReporteSerbanc
Author : ndonadio
Date Created : 22/09/2005
Description :   Basado en el codigo de modulo, activa el reporte correspondiente
Parameters : DataSet: TDataSet; var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TFLogOperaciones.MostrarReporteSerbanc(DataSet: TDataSet; var Error: AnsiString): Boolean;
resourcestring
    MODULE_NOT_RECOGNIZED_AS_SERBANC    = 'La operaci�n de interfaz seleccionada no pertenece a Serbanc.';
    CAP_SERBANC_ACT                     = 'Envio de Activaciones para Serbanc';
    CAP_SERBANC_DES                     = 'Envio de Desactivaciones para Serbanc';
    CAP_SERBANC_PAG                     = 'Recepcion de Rendicion de Pagos Serbanc';
    CAP_SERBANC_ACC                     = 'Recepcion de Rendici�n de Acciones de Cobranza de Serbanc';
    CAP_SERBANC_DIR                     = 'Recepci�n de Archivo de Nuevas Direcciones de Serbanc';
    CAP_SERBANC_CTC                     = 'Recepci�n de Sugerencias de Cambio de tipo de Cobranza de Serbanc';
var
    fr: TForm;
begin
    Result := False;
    try // finally...
        case DataSet.FieldByName('CodigoModulo').AsInteger of
            //56	Interfaz Saliente Activaciones Serbanc	2
            56: begin
                    try
                        Application.Createform(TfrmReporteActivacionesSerbanc, fr);
                        if not TfrmReporteActivacionesSerbanc(fr).MostrarReporte(DataSet.FieldByName('CodigoOperacionInterfase').asInteger, CAP_SERBANC_ACT, Error) then
                                Exit;
                    except
                        on e:exception do begin
                            Error := e.Message;
                            Exit;
                        end;
                    end;
                end;
            //57	Interfaz Saliente Desactivaciones Serbanc	2
            57: begin
                    try
                        Application.Createform(TfrmReporteDesactivacionesSerbanc, fr);
                        if not TfrmReporteDesactivacionesSerbanc(fr).MostrarReporte(DataSet.FieldByName('CodigoOperacionInterfase').asInteger, CAP_SERBANC_DES, Error) then
                                Exit;
                    except
                        on e:exception do begin
                            Error := e.Message;
                            Exit;
                        end;
                    end;
                end;
            //58	Interfaz Entrante Rendicion Acciones Serbanc	4
            58: begin
                    try
                        Application.Createform(TfrmReporteAccionesSerbanc, fr);
                        if not TfrmReporteAccionesSerbanc(fr).MostrarReporte(DataSet.FieldByName('CodigoOperacionInterfase').asInteger, CAP_SERBANC_ACC, Error) then
                               Exit;
                    except
                        on e:exception do begin
                            Error := e.Message;
                            Exit;
                        end;
                    end;
                end;
            //59	Interfaz Entrante Rendicion de Pagos Recibidos Serbanc	4
            59: begin
                    try
                        Application.Createform(TfrmReportePagosSerbanc, fr);
                        if not TfrmReportePagosSerbanc(fr).MostrarReporte(DataSet.FieldByName('CodigoOperacionInterfase').asInteger, CAP_SERBANC_PAG, Error) then
                                Exit;
                    except
                        on e:exception do begin
                            Error := e.Message;
                            Exit;
                        end;
                    end;
                end;
            //60	Interfaz entrante archivo Cambio de Tipo de Cobranza Serbanc	4
            60: begin
                    try
                        Application.Createform(TfrmReporteCTCSerbanc, fr);
                        if not TfrmReporteCTCSerbanc(fr).MostrarReporte(DataSet.FieldByName('CodigoOperacionInterfase').asInteger, CAP_SERBANC_CTC, Error) then
                                Exit;
                    except
                        on e:exception do begin
                            Error := e.Message;
                            Exit;
                        end;
                    end;
                end;
            //61	Interfaz entrante archivo de Nuevas Direcciones Serbanc	4
            61: begin
                    try
                        Application.Createform(TfrmReporteNvsDirSerbanc, fr);
                        if not TfrmReporteNvsDirSerbanc(fr).MostrarReporte(DataSet.FieldByName('CodigoOperacionInterfase').asInteger, CAP_SERBANC_DIR, Error) then
                                Exit;
                    except
                        on e:exception do begin
                            Error := e.Message;
                            Exit;
                        end;
                    end;
                end;
        else
            Error := MODULE_NOT_RECOGNIZED_AS_SERBANC;
            Exit;
        end;
        Result := True;
    finally
        if Assigned(fr) then fr.Release;
    end;
end;

{******************************** Function Header ******************************
Function Name: MostrarReporteBHTU
Author : ndonadio
Date Created : 26/09/2005
Description :   Nuestra los reportes de BHTU....
Parameters : DataSet: TDataSet; var Error: AnsiString
Return Value : Boolean

  Revision 1:
      Author : pdominguez
      Date   : 13/08/2010
      Description : SS 909 - BHTU COPEC - Rechazos
            - Se cambia el reporte de la clase TfrmReporteRecepcionVentasBHTU por
            el reporte TRptDayPassForm el cual se hace com�n a los procesos de
            carga BHTU.
*******************************************************************************}
function TFLogOperaciones.MostrarReporteBHTU(DataSet: TDataSet; var Error: AnsiString): Boolean;
resourcestring
    MODULE_NOT_RECOGNIZED_AS_BHTU   = 'La operaci�n de interfaz seleccionada no pertenece a ninguna Interfaz de Clearing de BHTU.';
    CAP_BTHU_ENVIO_INF              = 'Clearing BHTU - Generaci�n de Archivo de Infracciones';
var
    fr: TForm;
    F: TRptDayPassForm;
begin
    Result := False;
    try // finally...
        case DataSet.FieldByName('CodigoModulo').AsInteger of
            //62	Interfaz Saliente Envio Infracciones Clearing BHTU
            62: begin
                    try
                        Application.Createform(TfrmReporteEnvioInfraccionesBHTU, fr);
                        if not TfrmReporteEnvioInfraccionesBHTU(fr).MostrarReporte(DataSet.FieldByName('CodigoOperacionInterfase').asInteger, DataSet.FieldByName('CodigoOperacionInterfase').asInteger, CAP_BTHU_ENVIO_INF, Error) then
                                Exit;
                    except
                        on e:exception do begin
                            Error := e.Message;
                            Exit;
                        end;
                    end;
                end;

            RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO_TARDIO :
                // Rev. 1 (SS 909)
            	try
                	F := TRptDayPassForm.Create(Self);
        			if F.Inicializar('Recepci�n Archivo Ventas de BHTU', DataSet.FieldByName('CodigoOperacionInterfase').asInteger, True) then
                        f.RBIListado.Execute;
                finally
                    if assigned(F) then FreeAndNil(F);
                end;
                // Fin Rev. 1 (SS 909)
        else
                MsgBox(MODULE_NOT_RECOGNIZED_AS_BHTU, Caption, MB_ICONERROR);
                Exit;
        end;
        Result := True;
    finally
        if Assigned(fr) then fr.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 10/03/2005
  Description: Cierro el Formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFLogOperaciones.SalirBTNClick(Sender: TObject);
begin
    close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 10/03/2005
  Description: Lo libero de Memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFLogOperaciones.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

{******************************** Procedure Header *****************************
Procedure Name: MostrarReporteEnvioNovedadesCMR
Author : pdominguez
Date Created : 24/07/2009
Parameters : CodigoOperacionInterfase: Integer
Description : SS 812
    - Lanza el Reporte de env�o de Novedades CMR
*******************************************************************************}
procedure TFLogOperaciones.MostrarReporteEnvioNovedadesCMR(CodigoOperacionInterfase: Integer);
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de finalizaci�n de proceso';
    MSG_REPORT_ERROR_INICIALIZAR = 'Se produjo un Error al inicializar el Reporte.';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Envio Novedades';
var
    F : TFRptEnvioNovedades;
begin
    try
        Application.CreateForm(TFRptEnvioNovedades, F);
        if not F.Inicializar(CodigoOperacionInterfase, REPORT_TITLE) then begin
            f.Release;
            MsgBoxErr(MSG_REPORT_ERROR, MSG_REPORT_ERROR_INICIALIZAR, MSG_ERROR, MB_ICONERROR);
        end;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

end.
