{********************************** Unit Header *****************************************
File Name : ABMDepositoEmprasasDeCobranza.pas
Author : lgisuk
Date Created: 05/09/2005
Language : ES-AR
Description : Los datos ingresados desde este ABM, serán utilizados para realizar
              el asiento contable de "Deposito por parte de las Empreasas de Cobranza".
****************************************************************************************}
unit ABMDepositoEmpresasDeCobranza;

interface

uses
  ///Deposito Emprasas de Cobranza
  DMConnection,
  UtilProc,
  UtilDB,
  PeaTypes,
  StrUtils,
  Util,
  RStrings,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ADODB, DbList, Abm_obj, DmiCtrls, Validate,
  DateEdit, VariantComboBox;

type
  TfrmABMDepositoEmprasasDeCobranza = class(TForm)
    abmtb_Depositos: TAbmToolbar;
    dbl_Depositos: TAbmList;
    tbl_DepositoEmpresasCobranza: TADOTable;
    pnl_Datos: TPanel;
    lbl_FechaDeposito: TLabel;
    pnl_botonera: TPanel;
    Notebook: TNotebook;
    btn_Salir: TButton;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    lbl_Importe: TLabel;
    de_FechaDeposito: TDateEdit;
    ne_Importe: TNumericEdit;
    sp_InsertarDepositoEmpresaCobranza: TADOStoredProc;
    sp_ActualizarDepositoEmpresaCobranza: TADOStoredProc;
    sp_EliminarDepositoEmpresaCobranza: TADOStoredProc;
    lbl_Vendedor: TLabel;
    cb_EmpresaDeCobranza: TVariantComboBox;
    procedure dbl_DepositosClick(Sender: TObject);
    procedure dbl_DepositosDelete(Sender: TObject);
    procedure dbl_DepositosEdit(Sender: TObject);
    procedure dbl_DepositosInsert(Sender: TObject);
    procedure dbl_DepositosRefresh(Sender: TObject);
    procedure dbl_DepositosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure abmtb_DepositosClose(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure LimpiarCampos;
    procedure VolverCampos;
    procedure CargarComboEmpreasasCobranza;
  public
    { Public declarations }
    function Inicializar(MDIChild: Boolean): Boolean;
  end;

var
  frmABMDepositoEmprasasDeCobranza: TfrmABMDepositoEmprasasDeCobranza;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: Inicializar
Author : lgisuk
Date Created : 05/09/2005
Description : Inicialización de este formulario
Parameters :
Return Value : Boolean
*******************************************************************************}
function TfrmABMDepositoEmprasasDeCobranza.Inicializar(MDIChild: Boolean): Boolean;
var
    S: TSize;
begin
    Result := False;
    //Defino de que modo si visualizara el form
    if MDIChild then begin
        FormStyle := fsMDIChild;
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
    end else begin
        FormStyle := fsNormal;
        Visible := False;
    end;
    //Centro el form
    CenterForm(Self);
    //Abro la tabla DepositoEmpreasasCobranza
    if not OpenTables([tbl_DepositoEmpresasCobranza]) then Exit;
    //Finalizo de inicializar el form
    CargarComboEmpreasasCobranza; //Cargar el combo con los Vendedores de Pase Diario.
    LimpiarCampos; //Limpiar controles de ingreso de datos.
    Notebook.PageIndex := 0;
    KeyPreview := True;
    dbl_Depositos.Reload;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: LimpiarCampos
Author : lgisuk
Date Created : 05/09/2005
Description : Limpio los campos de edición, para cargar nuevos valores.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.LimpiarCampos;
begin
    de_FechaDeposito.Date := Date;
    ne_Importe.Value := 0;
end;

{******************************** Function Header ******************************
Function Name: dbl_VolverCampos
Author : lgisuk
Date Created : 05/09/2005
Description : Permito volver al form al estado Normal
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.VolverCampos;
begin
  	dbl_Depositos.Enabled  := True;
  	dbl_Depositos.SetFocus;
  	Notebook.PageIndex := 0;
    pnl_Datos.Enabled := False;
    de_FechaDeposito.Enabled := True;
    ne_Importe.Enabled := True;
end;

{******************************** Function Header ******************************
Function Name: CargarComboEmpreasasCobranza
Author : lgisuk
Date Created : 05/09/2005
Description : Carga el Combo de Empreasas de Cobranza.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.CargarComboEmpreasasCobranza;
resourcestring
    MSG_ERROR_LOADING_COMPANIES_OF_COLLECTION = 'Ha ocurrido un error al obtener los datos de las empreasas de cobranza.';
var
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(nil);
    try
        try
            sp.Connection := DMConnections.BaseCAC;
            sp.ProcedureName := 'ObtenerEmpresasCobranza';
            sp.Open;
            cb_EmpresadeCobranza.Clear;
            while not sp.Eof do begin
                cb_EmpresadeCobranza.Items.Add(sp.FieldByName('Nombre').AsString,
                sp.FieldByName('CodigoEmpresaCobranza').AsInteger);
                sp.Next;
            end;
            cb_EmpresadeCobranza.ItemIndex := 0;
        except
            on e: Exception do begin
                //informo que se produjo un error
                MsgBoxErr(MSG_ERROR_LOADING_COMPANIES_OF_COLLECTION, E.Message, Self.Caption, MB_ICONSTOP);
            end;
         end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: FormShow
Author : lgisuk
Date Created : 05/09/2005
Description : Al activar el form actualizo la grilla
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.FormShow(Sender: TObject);
begin
    dbl_Depositos.Reload;
end;

{******************************** Function Header ******************************
Function Name: dbl_DepositosRefresh
Author : lgisuk
Date Created : 05/09/2005
Description : si al actualizar la grilla, esta queda vacia limpia los campos
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.dbl_DepositosRefresh(Sender: TObject);
begin
	  if dbl_Depositos.Empty then LimpiarCampos;
end;

{******************************** Function Header ******************************
Function Name: dbl_DepositosDrawItem
Author : lgisuk
Date Created : 05/09/2005
Description : cargo los datos del deposito en la grilla
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.dbl_DepositosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var
    Nombre : String;
begin
    with Sender.Canvas, Tabla  do begin
      FillRect(Rect);
      Nombre := QueryGetValue(DMConnections.BaseCAC,'SELECT dbo.ObtenerNombreEmpresaCobranza ('+FieldbyName('CodigoEmpresaCobranza').AsString+')');
      TextOut(Cols[0], Rect.Top, FieldbyName('FechaDeposito').AsString);
      TextOut(Cols[1], Rect.Top, Nombre);
      TextOut(Cols[2], Rect.Top, Format('%f',[(FieldbyName('Importe').AsFloat / 100)]));
      TextOut(Cols[3], Rect.Top, Trim(FieldbyName('CodigoUsuario').AsString));
      TextOut(Cols[4], Rect.Top, FieldbyName('FechaModificacion').AsString);
      TextOut(Cols[5], Rect.Top, FieldbyName('NumeroProcesoContabilizacion').AsString);
    end;
end;

{******************************** Function Header ******************************
Function Name: dbl_DepositosClick
Author : lgisuk
Date Created : 05/09/2005
Description : Al hacer click en la grilla muestro los datos del deposito realizado
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.dbl_DepositosClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
     cb_EmpresadeCobranza.value := FieldByName('CodigoEmpresaCobranza').Asinteger;
	   de_FechaDeposito.Date := FieldByName('FechaDeposito').AsDateTime;
	   ne_Importe.Value := FieldByname('Importe').AsFloat / 100;
	end;
end;

{******************************** Function Header ******************************
Function Name: dbl_DepositosInsert
Author : lgisuk
Date Created : 05/09/2005
Description : Alta de deposito
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.dbl_DepositosInsert(Sender: TObject);
begin
    LimpiarCampos;
    pnl_Datos.Enabled := True;
    dbl_Depositos.Enabled := False;
    Notebook.PageIndex := 1;
    ActiveControl := de_FechaDeposito;
end;

{******************************** Function Header ******************************
Function Name: dbl_DepositosDelete
Author : lgisuk
Date Created : 05/09/2005
Description : Baja de deposito
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.dbl_DepositosDelete(Sender: TObject);
Resourcestring
    MSG_DEPOSIT_ALREADY_ASSESSED =  'Deposito ya contabilizado!';
    MSG_DEPOSIT = 'Deposito';
begin
    if not tbl_DepositoEmpresasCobranza.FieldByName('NumeroProcesoContabilizacion').IsNull then begin
        //Informo que el deposito ya esta contabilizado y por eso no se puede eliminar
        MsgBox(MSG_DEPOSIT_ALREADY_ASSESSED, Self.Caption, MB_ICONINFORMATION);
    end else begin
        //Mostrar mensaje de confirmación de la eliminación.
        if MsgBox(Format(MSG_QUESTION_ELIMINAR, [MSG_DEPOSIT]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            try
                with sp_EliminarDepositoEmpresaCobranza do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoDepositoEmpresaCobranza').Value := dbl_Depositos.Table.FieldbyName('CodigoDepositoEmpresaCobranza').Value;
                    ExecProc;
                end;
            except
                on E: Exception do begin
                    MsgBoxErr(Format(MSG_ERROR_ELIMINAR, [MSG_DEPOSIT]), E.Message, Format(MSG_CAPTION_ELIMINAR, [MSG_DEPOSIT]), MB_ICONSTOP);
                end;
            end;
        end;
    end;
  	dbl_Depositos.Reload;
  	dbl_Depositos.Estado   := Normal;
  	dbl_Depositos.Enabled  := True;
  	Notebook.PageIndex     := 0;
end;

{******************************** Function Header ******************************
Function Name: dbl_DepositosDelete
Author : lgisuk
Date Created : 05/09/2005
Description : Modificación de deposito
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.dbl_DepositosEdit(Sender: TObject);
Resourcestring
    MSG_DEPOSIT = 'Deposito';
begin
    if not tbl_DepositoEmpresasCobranza.FieldByName('NumeroProcesoContabilizacion').IsNull then begin
        MsgBox(MSG_DEPOSIT, Self.Caption, MB_ICONINFORMATION);
        VolverCampos;
        Exit;
    end;
  	dbl_Depositos.Enabled := False;
  	dbl_Depositos.Estado := Modi;
  	Notebook.PageIndex := 1;
  	pnl_Datos.Enabled := True;
  	ActiveControl := de_FechaDeposito;
end;

{******************************** Function Header ******************************
Function Name: Btn_AceptarClick
Author : lgisuk
Date Created : 05/09/2005
Description : Inserto o Actualizo un Deposito
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.btn_AceptarClick(Sender: TObject);
resourcestring
    MSG_VALUE_BIGGER_OR_EQUAL_TO_CERO = 'El Monto debe ser superior o igual a cero.';
    MSG_EXISTING_DEPOSIT = 'Ya existen datos para la Fecha de Deposito y Empresa de Cobranza ingresada.';
    MSG_DATE_DEPOSIT_IS_BIGGER_THAN_CURRENT_DATE = 'La Fecha de Deposito es Mayor a la Fecha Actual.';
    MSG_DEPOSIT = 'Deposito';
Const
    FLD_FECHA_DEPOSITO = 'Fecha de Deposito';
    FLD_EMPRESA_DE_COBRANZA = 'Empresa de Cobranza';
begin
	if not ValidateControls([de_FechaDeposito,
            ne_Importe],
            [de_FechaDeposito.Date <> NullDate,
            ne_Importe.Value >= 0],
            Format(MSG_CAPTION_ACTUALIZAR, [MSG_DEPOSIT]),
            [Format(MSG_VALIDAR_DEBE_EL, [FLD_FECHA_DEPOSITO]),
            Format(MSG_VALIDAR_DEBE_EL, [FLD_EMPRESA_DE_COBRANZA]),
            MSG_VALUE_BIGGER_OR_EQUAL_TO_CERO]) then begin
		  Exit;
	end;

    //Verifico que la fecha no sea mayor al dia de hoy
    if de_FechaDeposito.Date > NowBase(DMConnections.BaseCAC) then begin
        MsgBox( MSG_DATE_DEPOSIT_IS_BIGGER_THAN_CURRENT_DATE , Self.Caption, MB_ICONSTOP);
        Exit;
    end;

    // Si estoy insertando, controlar que la
    // FechaDeposito, CodigoEmpresaCobranza, NO exista en la tabla.
    if (dbl_Depositos.Estado = Alta) then begin
        if dbl_Depositos.Table.Locate('FechaDeposito;CodigoEmpresaCobranza', VarArrayOf([de_FechaDeposito.Date, cb_EmpresadeCobranza.Value]), []) then begin
            MsgBox(MSG_EXISTING_DEPOSIT, Self.Caption, MB_ICONSTOP);
            Exit;
        end;
    end;

    // Si estoy editando y NO se quiere modificar el mismo registro, controlar que la
    // FechaDeposito, CodigoEmpresaCobranza NO exista en la tabla.
    if (dbl_Depositos.Estado = Modi) and ( (de_FechaDeposito.Date <> dbl_Depositos.Table.FieldByName('FechaDeposito').AsDateTime) or (cb_EmpresadeCobranza.Value <> dbl_Depositos.Table.FieldByName('CodigoEmpresaCobranza').AsInteger) ) then begin
        if dbl_Depositos.Table.Locate('FechaDeposito;CodigoEmpresaCobranza;Importe', VarArrayOf([de_FechaDeposito.Date, cb_EmpresaDeCobranza.Value, ne_Importe.Value * 100]), []) then begin
            MsgBox(MSG_EXISTING_DEPOSIT, Self.Caption, MB_ICONSTOP);
            Exit;
        end;
    end;

	with dbl_Depositos do begin
  		Screen.Cursor := crHourGlass;
  		try
    			try
             case Estado of
                    Alta:   begin
                                with sp_InsertarDepositoEmpresaCobranza, Parameters do begin
                                    Refresh;
                                    ParamByName('@FechaDeposito').Value := de_FechaDeposito.Date;
                                    ParamByName('@CodigoEmpresaCobranza').Value := cb_EmpresaDeCobranza.Value;
                                    ParamByName('@Importe').Value := ne_Importe.Value * 100;
                                    ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                                    ExecProc;
                                end;
                            end;
                    Modi:   begin
                                with sp_ActualizarDepositoEmpresaCobranza, Parameters do begin
                                    Refresh;
                                    ParamByName('@CodigoDepositoEmpresaCobranza').Value := dbl_Depositos.Table.FieldByName('CodigoDepositoEmpresaCobranza').AsInteger;
                                    ParamByName('@CodigoEmpresaCobranza').Value := dbl_Depositos.Table.FieldByName('CodigoEmpresaCobranza').AsInteger;
                                    ParamByName('@FechaDeposito').Value := dbl_Depositos.Table.FieldByName('FechaDeposito').AsDateTime;
                                    ParamByName('@Importe').Value := ne_Importe.Value * 100;
                                    ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                                    ExecProc;
                                end;
                            end;
             end;
  			except
	    			on E: Exception do begin
    					  MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR, [MSG_DEPOSIT]), E.Message, Format(MSG_CAPTION_ACTUALIZAR, [MSG_DEPOSIT]), MB_ICONSTOP);
    				end;
  			end;
		finally
        VolverCampos;
			  Screen.Cursor := crDefault;
        Reload;
      	dbl_Depositos.Reload;
      	dbl_Depositos.Estado   := Normal;
       	dbl_Depositos.Enabled  := True;
  	    Notebook.PageIndex     := 0;
		end;
	end;
end;

{******************************** Function Header ******************************
Function Name: btn_CancelarClick
Author : lgisuk
Date Created : 05/09/2005
Description : Permito cancelar la operación
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.btn_CancelarClick(Sender: TObject);
begin
	  VolverCampos;
    dbl_Depositos.Reload;
   	dbl_Depositos.Estado   := Normal;
   	dbl_Depositos.Enabled  := True;
    Notebook.PageIndex     := 0;
end;

{******************************** Function Header ******************************
Function Name: abmtb_DepositosClose
Author : lgisuk
Date Created : 05/09/2005
Description : Permito cerrar el formulario
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.abmtb_DepositosClose(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: btn_SalirClick
Author : lgisuk
Date Created : 05/09/2005
Description : Permito cerrar el formulario
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: btn_FormClose
Author : lgisuk
Date Created : 05/09/2005
Description : lo libero de memoria
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmABMDepositoEmprasasDeCobranza.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	  Action := caFree;
end;

end.
