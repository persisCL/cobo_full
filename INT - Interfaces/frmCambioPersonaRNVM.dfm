object FCambioPersonaRNVM: TFCambioPersonaRNVM
  Left = 158
  Top = 203
  Caption = 'Cambio de Persona RNVM'
  ClientHeight = 559
  ClientWidth = 954
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object gbRangoFechas: TGroupBox
    Left = 0
    Top = 0
    Width = 954
    Height = 146
    Align = alTop
    TabOrder = 0
    DesignSize = (
      954
      146)
    object Label2: TLabel
      Left = 216
      Top = 45
      Width = 34
      Height = 13
      Caption = 'Desde:'
    end
    object Label3: TLabel
      Left = 374
      Top = 45
      Width = 32
      Height = 13
      Caption = 'Hasta:'
    end
    object lblPatente: TLabel
      Left = 45
      Top = 45
      Width = 42
      Height = 13
      Caption = '&Patente:'
      FocusControl = txt_Patente
    end
    object lblRUT: TLabel
      Left = 63
      Top = 72
      Width = 24
      Height = 13
      Caption = '&RUT:'
      FocusControl = txt_rut
    end
    object lbl_nombre: TLabel
      Left = 46
      Top = 95
      Width = 41
      Height = 13
      Caption = 'Nombre:'
    end
    object Label1: TLabel
      Left = 40
      Top = 117
      Width = 47
      Height = 13
      Caption = 'Direcci'#243'n:'
    end
    object Label4: TLabel
      Left = 16
      Top = 16
      Width = 71
      Height = 13
      Caption = 'Concesionaria:'
      FocusControl = txt_Patente
    end
    object deFDesde: TDateEdit
      Left = 256
      Top = 42
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      OnChange = txt_PatenteChange
      Date = -693594.000000000000000000
    end
    object deFHasta: TDateEdit
      Left = 412
      Top = 42
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 2
      OnChange = txt_PatenteChange
      Date = -693594.000000000000000000
    end
    object txt_Patente: TEdit
      Left = 93
      Top = 42
      Width = 56
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      OnChange = txt_PatenteChange
    end
    object txt_rut: TEdit
      Left = 93
      Top = 69
      Width = 91
      Height = 21
      Hint = 'N'#250'mero de documento'
      CharCase = ecUpperCase
      MaxLength = 9
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnChange = txt_PatenteChange
    end
    object txt_nombre: TEdit
      Left = 93
      Top = 92
      Width = 409
      Height = 21
      TabStop = False
      Enabled = False
      MaxLength = 30
      TabOrder = 4
    end
    object btnBuscar: TButton
      Left = 869
      Top = 69
      Width = 75
      Height = 22
      Anchors = [akTop, akRight]
      Caption = '&Buscar'
      Default = True
      TabOrder = 5
      OnClick = btnBuscarClick
    end
    object txt_Direccion: TEdit
      Left = 93
      Top = 114
      Width = 409
      Height = 21
      TabStop = False
      Enabled = False
      MaxLength = 30
      TabOrder = 6
    end
    object cbConcesionaria: TVariantComboBox
      Left = 93
      Top = 13
      Width = 227
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 7
      OnChange = cbConcesionariaChange
      Items = <>
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 146
    Width = 954
    Height = 374
    Align = alClient
    TabOrder = 1
    object dbl_Infracciones: TDBListEx
      Left = 2
      Top = 15
      Width = 950
      Height = 357
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Fecha'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaInfraccion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Descripci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'EstadoCN'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Fecha Anulaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaAnulacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Motivo Anulaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'MotivoAnulacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Nombre'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Nombre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'R.U.T.'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroDocumento'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Precio Infracci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'Precioinfraccion'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'CodigoTarifaDayPassBALI'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'CodigoTarifaDayPassBALI'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'C'#243'digo Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'CodigoConvenio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'N'#250'mero Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroConvenio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Tipo Comprobante Fiscal'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TipoComprobanteFiscalDesc'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'N'#176' Comprobante Fiscal'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'NumeroComprobanteFiscal'
        end>
      DataSource = dsObtenerInfraccionesAFacturar
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDrawText = dbl_InfraccionesDrawText
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 520
    Width = 954
    Height = 39
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      954
      39)
    object btnActualizaRNVM: TButton
      Left = 855
      Top = 10
      Width = 89
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Actualiza RNVM'
      Default = True
      TabOrder = 0
      OnClick = btnActualizaRNVMClick
    end
  end
  object spObtenerInfraccionesAFacturar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerInfraccionesAFacturar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@SePuedeEmitirCertificado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@ValidarInfraccionesImpagas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 84
    Top = 172
  end
  object dsObtenerInfraccionesAFacturar: TDataSource
    DataSet = spObtenerInfraccionesAFacturar
    Left = 118
    Top = 172
  end
  object spCambioPersonaRNVMa: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CambioPersonaRNVMa'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConsultaRNVM'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 432
    Top = 276
  end
  object spObtenerDatosCambioPersonaRNVMa: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosCambioPersonaRNVMa'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 432
    Top = 224
  end
end
