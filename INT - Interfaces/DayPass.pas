{Author: gcasais}
{$A+,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{-------------------------------------------------------------------------------
 File Name: DayPass.pas
 Author:   Flamas
 Date Created: 11/03/2005
 Language: ES-AR
 Description: M�dulo de la interface Servipag -	DayPass

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)


Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1256_NDR_20150505
Descripcion : Los nombres de los archivos deben considerar la ConcesionariaNativa

 Firma        : SS_1313_NDR_20150615
 Descripcion  : Emitir Reporte Resumen DayPass al finalizar la importacion
                Se debe mostrar la patente, la cantidad de daypass comprados en el a�o en curso
                (si este valor excede de un parametro), siempre y cuando la patente tenga al menos un
                DayPAss comprado en los ultimos X dias (segun parametro) 
--------------------------------------------------------------------------------}
unit DayPass;

interface

uses
  //DayPass
  DMConnection,
  Util,
  UtilDB,
  UtilProc,
  PeaProcs,
  PeaTypes,
  ConstParametrosGenerales,
  ComunesInterfaces,
  FrmRptDayPass,       //Reporte de DayPass Recibidos
  frmRptDayPassCopec,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,StrUtils,
  Dialogs, ComCtrls, StdCtrls, DB, ADODB, Buttons, ExtCtrls, DateUtils, UtilRB,							//SS_1256_NDR_20150505
  ppParameter, ppBands, ppModule, raCodMod, ppCtrls, ppReport, ppStrtch,								//SS_1256_NDR_20150505
  ppSubRpt, ppPrnabl, ppClass, ppCache, ppProd, ppComm, ppRelatv, ppDB, ppDBPipe;

type
  TMasterDayPassRecord = record
  	NumeroSerie : string;
    Pin : string;
    CodigoMaterial : string;
    Monto : integer;
    ValidezDesde : TDateTime;
    ValidezHasta : TDateTime;
  end;

  TDayPassRecord = record
  	NumeroSerie : string;
    Patente : string;
    Pin : string;
    FechaUso : TDateTime;
    FechaVenta : TDateTime;
    FechaDevolucion : TDateTime;
    CodigoMaterial : string;
    Distribuidor : string;
    RUTContacto : string;
    TelefonoContacto : string;
    NombreContacto : string;
    TipoArchivo : string;
  end;

  TLateDayPassRecord = record
    Patente : string;
    FechaVenta : TDateTime;
    CodigoMaterial : string;
    ValorPagado : integer;
  end;

  TfrmDayPass = class(TForm)
    btnCerrar: TButton;
    PageControl1: TPageControl;
    tsArchivoPatentes: TTabSheet;
    tsRecibirPasesDiarios: TTabSheet;
    Label1: TLabel;
    btnGenerar: TButton;
    spPatentes: TADOStoredProc;
    edOrigenPD: TEdit;
    Label2: TLabel;
    btnAbrirArchivoPD: TSpeedButton;
    OpenDialog: TOpenDialog;
    lblReferenciaPD: TLabel;
    pnlAvancePD: TPanel;
    lblAvancePD: TLabel;
    pbProgresoPD: TProgressBar;
    btnCancelarPD: TButton;
    btnProcesarPD: TButton;
    spAgregarDayPass: TADOStoredProc;
    pbAvance: TProgressBar;
    lblProgreso: TLabel;
    lblDescri: TLabel;
    btnCancelarPlates: TButton;
    tsPasesDiariosTardios: TTabSheet;
    Label4: TLabel;
    btnAbrirArchivoPDT: TSpeedButton;
    lblReferenciaPDT: TLabel;
    edOrigenPDT: TEdit;
    pnlAvancePDT: TPanel;
    Label6: TLabel;
    pbProgresoPDT: TProgressBar;
    btnCancelarPDT: TButton;
    btnProcesarPDT: TButton;
    spAgregarDayPassTardio: TADOStoredProc;
    tsCambioFecha: TTabSheet;
    Label3: TLabel;
    edOrigenCF: TEdit;
    btnAbrirArchivoCF: TSpeedButton;
    lblReferenciaCF: TLabel;
    pnlAvanceCF: TPanel;
    lblAvanceCF: TLabel;
    pbProgresoCF: TProgressBar;
    btnProcesarCF: TButton;
    btnCancelarCF: TButton;
    tsMaestroPases: TTabSheet;
    Label5: TLabel;
    edOrigenMPD: TEdit;
    btnAbrirArchivoMPD: TSpeedButton;
    lblReferenciaMPD: TLabel;
    pnlAvanceMPD: TPanel;
    lblAvanceMPD: TLabel;
    pbProgresoMPD: TProgressBar;
    btnProcesarMPD: TButton;
    btnCancelarMPD: TButton;
    tsDevolucion: TTabSheet;
    Label7: TLabel;
    edOrigenDev: TEdit;
    btnAbrirArchivoDev: TSpeedButton;
    lblReferenciaDev: TLabel;
    pnlAvanceDev: TPanel;
    lblAvanceDev: TLabel;
    pbProgresoDev: TProgressBar;
    btnProcesarDev: TButton;
    btnCancelarDev: TButton;
    pnlAyuda: TPanel;
    ImgAyudaMaestro: TImage;
    Panel1: TPanel;
    ImgAyudaDiario: TImage;
    Panel2: TPanel;
    ImgAyudaCambioFecha: TImage;
    Panel3: TPanel;
    ImgAyudaDevolucion: TImage;
    Panel4: TPanel;
    ImgAyudaPaseDiarioTardio: TImage;
    spActualizarLogOperacionesRegistrosProcesados: TADOStoredProc;
    spResumenDayPass: TADOStoredProc;
    dsResumenDayPass: TDataSource;
    ppDBReporteResumenDayPass: TppDBPipeline;
    ppReportResumenDayPass: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    lbl_usuario: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    raCodeModule2: TraCodeModule;
    ppParameterList1: TppParameterList;
    RBIResumen: TRBInterface;
    ppLine1: TppLine;
    ppLine4: TppLine;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel10: TppLabel;
    ppLine6: TppLine;
    ppDBTPatente: TppDBText;
    ppDBTFechaVenta: TppDBText;
    ppDBTFechaUso: TppDBText;
    ppDBTCantidad: TppDBText;
    ppImage2: TppImage;
    ppFechaProcesamiento: TppLabel;
    pptFechaProcesamiento: TppLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnGenerarClick(Sender: TObject);
    procedure btnAbrirArchivoPDClick(Sender: TObject);
    procedure btnCerrarClick(Sender: TObject);
    procedure btnProcesarPDClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCancelarPDClick(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;var AllowChange: Boolean);
    procedure btnCancelarPlatesClick(Sender: TObject);
    procedure btnAbrirArchivoPDTClick(Sender: TObject);
    procedure btnProcesarPDTClick(Sender: TObject);
    procedure btnAbrirArchivoCFClick(Sender: TObject);
    procedure btnProcesarCFClick(Sender: TObject);
    procedure btnAbrirArchivoMPDClick(Sender: TObject);
    procedure btnProcesarMPDClick(Sender: TObject);
    procedure btnAbrirArchivoDevClick(Sender: TObject);
    procedure btnProcesarDevClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ImgAyudaMaestroClick(Sender: TObject);
    procedure ImgAyudaDiarioClick(Sender: TObject);
    procedure ImgAyudaCambioFechaClick(Sender: TObject);
    procedure ImgAyudaDevolucionClick(Sender: TObject);
    procedure ImgAyudaPaseDiarioTardioClick(Sender: TObject);
    procedure RBIResumenExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FCodigoOperacion: integer;
    FErrorLog: TStringList;
    FMaestroPasesDiarios : TStringList;
    FPasesDiariosUso: TStringList;
    FPasesDiariosVentas: TStringList;
    FPasesDiariosTardios: TStringList;
    FPasesDiariosCambioFecha: TStringList;
    FPasesDiariosDevolucion: TStringList;
    FErrors: Boolean;
    FProcesando : boolean;
    FDetenerImportacion: Boolean;
    FCancelar: Boolean;
    FUsoFileName : string;
    FCambioFechaFileName : string;
    FVentasFileName : string;
    FProcesarSinVentas : boolean;
    FCantidadErrores:integer;
    // Parametros Generales
    FServipag_Directorio_Entrada : AnsiString;
    FServipag_Directorio_Procesados : AnsiString;
    FHabilitar_Habilitacion_Tardia : integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    // Procediemientos utilizados por todos los procesos
    procedure CheckForError;
    Function  RegistrarOperacion(CodigoModulo: integer; sFileName: string): boolean;
    Function  ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
    Function  ActualizarLog(CantidadErrores : integer) :boolean;
    procedure EliminarLineasEnBlanco( var PaseDiario : TStringList);
    Function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
    // Procedimientos Importaci�n Pases Diarios
    function  ObtenerArchivoPaseDiario(sExtension: string): string;
    function  ObtenerArchivoVentasPaseDiario(var sArchivoVenta : string): integer;
    function  VerificarCantidadDeRegistros( FPasesDiarios : TStringList; sArchivo : string ) : boolean;
    function  AnalizarArchivoPasesDiarios(FPasesDiarios : TStringList; sMsgAnalizando, sMsgTieneErrores, sTipoArchivo: string): boolean;
    function  CargarPasesDiarios(FPasesDiarios : TStringList; sMsgProcesando, sMsgTieneErrores, sMsgProcesoOK, sTipoArchivo: string): boolean;
    function  ParseDayPassLine(sTipoArchivo, sline : string; var DayPassRecord : TDayPassRecord; var sParseError : string): boolean;
    function  AnalizarArchivoPasesDiariosTardios: boolean;
    function  ParseLateDayPassLine(sline : string; var LateDayPassRecord : TLateDayPassRecord; var sParseError : string): boolean;
    function  CargarPasesDiariosTardios: boolean;
    // Procedimientos Importaci�n Maestro Pases Diarios
    function  VerificarCantidadDeRegistrosMaestro( FPasesDiarios : TStringList; sArchivo : string ) : boolean;
    function  AnalizarArchivoMaestroPasesDiarios(FPasesDiarios : TStringList; sMsgAnalizando, sMsgTieneErrores : string): boolean;
    function  ParseMasterDayPassLine(sline : string; var MasterRecord : TMasterDayPassRecord; var sParseError : string): boolean;
    function  CargarMaestroPasesDiarios(FPasesDiarios : TStringList; sMsgProcesando, sMsgTieneErrores, sMsgProcesoOK : string): boolean;
    // Procedimientos Importaci�n Devoluciones
    function  ObtenerOtrosArchivosPaseDiario(sFileName: string): string;
  public
    function Inicializar: Boolean;
  end;

Const
    //FILE_NAME_PREFIX            = 'CN';                                       //SS_1256_NDR_20150505
    VENTAS_NAME_PREFIX          = 'PVESERVI';
    HABILITACION_NAME_PREFIX    = 'PHDSERVI';
    MAESTRO_NAME_PREFIX    		  = 'PMASERVI';
    DEVOLUCION_NAME_PREFIX 		  = 'PDESERVI';
    CAMBIO_FECHA_NAME_PREFIX 	  = 'PCFSERVI';


var
  frmDayPass: TfrmDayPass;
  sPrefijoNombre:string;                                                        //SS_1256_NDR_20150505

implementation

{$R *.dfm}

{ TfrmDayPass }

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 06/07/2005
  Description: Inicializacion de este formulario
  Parameters: None
  Return Value: Boolean

  Revision 1
  Author: mbecerra
  Date: 26-Mayo-2008
  Description: Se forz� a que si no encuentra un par�metro general, retorne False.
-----------------------------------------------------------------------------}
function TfrmDayPass.Inicializar: Boolean;
resourcestring
  	MSG_INIT_ERROR 	 = 'No se pudo inicializar la interfaz de Pase Diario';
    MSG_ERROR		 = 'Error';
    ERROR_P_GRAL     = 'Error al cargar el Par�metro General "%s".' ;
    DIR_ENTRADA      = 'SERVIPAG_DIRECTORIO_ENTRADA';
    DIR_PROCESADOS   = 'SERVIPAG_DIRECTORIO_PROCESADOS';
    DIR_HABILITACION = 'HABILITAR_HABILITACION_TARDIA';
Const
  	DAYPASS_EXTENSION 		  = '.TXT';
  	LATE_DAYPASS_EXTENSION	= '.MOK';
begin
    Result 		:= False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    sPrefijoNombre := ObtenerNombreCortoConcesionariaNativa;

    FErrorLog	:= TStringList.Create;
    FErrors 	:= False;
    FProcesando := False;
    lblReferenciaMPD.Caption 	:= '';
    lblReferenciaPD.Caption 	:= '';
    lblReferenciaCF.Caption 	:= '';
    lblReferenciaDev.Caption 	:= '';
    lblReferenciaPDT.Caption 	:= '';
    PageControl1.ActivePageIndex := 0;
    try
    	// Revision 1
    	if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_ENTRADA, FServipag_Directorio_Entrada) then begin
        	MsgBoxErr(MSG_INIT_ERROR, Format( ERROR_P_GRAL, [DIR_ENTRADA]), Caption, MB_ICONSTOP);
        	Exit;
        end;

      	if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_PROCESADOS, FServipag_Directorio_Procesados) then  begin
        	MsgBoxErr(MSG_INIT_ERROR, Format( ERROR_P_GRAL, [DIR_PROCESADOS]), Caption, MB_ICONSTOP);
        	Exit;
        end;

       { if not ObtenerParametroGeneral(DMCOnnections.BaseCAC, DIR_HABILITACION, FHabilitar_Habilitacion_Tardia) then begin
        	MsgBoxErr(MSG_INIT_ERROR, Format( ERROR_P_GRAL, [DIR_HABILITACION]), Caption, MB_ICONSTOP);
        	Exit;
        end;}

        // Habilita o no el Tab de DayPass Tard�os
        tsPasesDiariosTardios.TabVisible := False ;//(FHabilitar_Habilitacion_Tardia = 1);

        FServipag_Directorio_Entrada := GoodDir(FServipag_Directorio_Entrada);
        FServipag_Directorio_Procesados := GoodDir(FServipag_Directorio_Procesados);

    	edOrigenMPD.Text := ObtenerOtrosArchivosPaseDiario( MAESTRO_NAME_PREFIX );
        btnProcesarMPD.Enabled := (edOrigenMPD.Text <> '');

        edOrigenPD.Text := ObtenerArchivoPaseDiario(DAYPASS_EXTENSION);
        btnProcesarPD.Enabled := (edOrigenPD.Text <> '');

     	edOrigenCF.Text := ObtenerOtrosArchivosPaseDiario( CAMBIO_FECHA_NAME_PREFIX );
        btnProcesarCF.Enabled := (edOrigenCF.Text <> '');

     	edOrigenDev.Text := ObtenerOtrosArchivosPaseDiario( DEVOLUCION_NAME_PREFIX );
        btnProcesarDev.Enabled := (edOrigenDev.Text <> '');

        edOrigenPDT.Text := ObtenerArchivoPaseDiario(LATE_DAYPASS_EXTENSION);
        btnProcesarPDT.Enabled := (edOrigenPDT.Text <> '');

        Result := True;
    except on e: exception do begin
          	MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Result := False;
    	end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCreate
  Author:    flamas
  Date Created: 06/07/2005
  Description: 	La hace invisible de entrada para evitar el efecto
                de que el Tab desaparece al Inicializar el FORM
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.FormCreate(Sender: TObject);
begin
	// La hace invisible de entrada para evitar el efecto
    // de que el Tab desaparece al Inicializar el FORM
	tsPasesDiariosTardios.TabVisible := False;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmDayPass.ImgAyudaMaestroClick(Sender: TObject);
Resourcestring
    MSG_DAYPASS         = ' ' + CRLF +
                          'El Archivo de Maestro de Pases Diarios' + CRLF +
                          'es utilizado por SERVIPAG para informar al ESTABLECIMIENTO' + CRLF +
                          'la lista de pases diarios validos' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: PMASERVIYYMMDDSS.TXT' + CRLF +
                          ' ';
begin
    //Muestro el mensaje
    MsgBoxBalloon(MSG_DAYPASS, Caption, MB_ICONQUESTION, ImgAyudaMaestro);
end;

{******************************** Function Header ******************************
Function Name: Image1Click
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmDayPass.ImgAyudaDiarioClick(Sender: TObject);
Resourcestring
    MSG_DAYPASS         = ' ' + CRLF +
                          'El Archivo de Pases Diarios' + CRLF +
                          'es utilizado por SERVIPAG para informar al ESTABLECIMIENTO' + CRLF +
                          'la lista de pases diarios vendidos' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: PHDSERVIYYMMDDSS.TXT' + CRLF +
                          ' ';
begin
    //Muestro el mensaje
    MsgBoxBalloon(MSG_DAYPASS, Caption, MB_ICONQUESTION, ImgAyudaDiario);
end;

{******************************** Function Header ******************************
Function Name: Image2Click
Author : lgisuk
Date Created : 14/07/2005
Description :  Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmDayPass.ImgAyudaCambioFechaClick(Sender: TObject);
Resourcestring
    MSG_DAYPASS         = ' ' + CRLF +
                          'El Archivo de Cambio de Fecha' + CRLF +
                          'es utilizado por SERVIPAG para informar al ESTABLECIMIENTO' + CRLF +
                          'la lista de pases diarios pertenecientes a usuarios' + CRLF +
                          'que decidieron cambiar la fecha de uso del pase' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: PCFSERVIYYMMDDSS.TXT' + CRLF +
                          ' ';
begin
    //Muestro el mensaje
    MsgBoxBalloon(MSG_DAYPASS, Caption, MB_ICONQUESTION, ImgAyudaCambioFecha);
end;

{******************************** Function Header ******************************
Function Name: Image3Click
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmDayPass.ImgAyudaDevolucionClick(Sender: TObject);
Resourcestring
    MSG_DAYPASS         = ' ' + CRLF +
                          'El Archivo de Devoluci�n' + CRLF +
                          'es utilizado por SERVIPAG para informar al ESTABLECIMIENTO' + CRLF +
                          'la lista con los pases diarios que fueron devueltos y' + CRLF +
                          'por lo tanto ya no seran utilizados' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: PDESERVIYYMMDDSS.TXT' + CRLF +
                          ' ';
begin
    //Muestro el mensaje
    MsgBoxBalloon(MSG_DAYPASS, Caption, MB_ICONQUESTION, ImgAyudaDevolucion);
end;

{******************************** Function Header ******************************
Function Name: Image4Click
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmDayPass.ImgAyudaPaseDiarioTardioClick(Sender: TObject);
Resourcestring
    MSG_DAYPASS         = ' ' + CRLF +
                          'El Archivo de Pases Diarios Tardios' + CRLF +
                          'es utilizado por SERVIPAG para informar al ESTABLECIMIENTO' + CRLF +
                          'la lista con las patentes de los vehiculos' + CRLF +
                          'que adquieron un pase diario tardio' + CRLF +
                          ' ' + CRLF +   ' ';
begin
    //Muestro el mensaje
    MsgBoxBalloon(MSG_DAYPASS, Caption, MB_ICONQUESTION, ImgAyudaPaseDiarioTardio);
end;

{-----------------------------------------------------------------------------
  Function Name: CheckForError
  Author:    flamas
  Date Created: 06/07/2005
  Description: Muestra los errores lo utilizan todos los procesos
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.CheckForError;
{begin
    if ((FErrors) or (FErrorLog.Count > 0)) then begin
        FErrorLog.SaveToFile('C:\Errores.txt');
        FErrorLog.Clear;
        Run('Notepad C:\Errores.txt');
        DeleteFile('C:\Errores.txt');
    end;}
var
    i : integer;
begin
	for i := 0 to FErrorLog.Count - 1 do begin
		if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, FErrorLog[i], 1) then begin // (SS 909) 
        	break;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EliminarLineasEnBlanco
  Author:    flamas
  Date Created: 28/03/2005
  Description: Elimina las l�neas en Blanco que haya al final
  Parameters: var PaseDiario : TStringList
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.EliminarLineasEnBlanco( var PaseDiario : TStringList);
var
	i : integer;
begin
	i := PaseDiario.Count -1;
    while (PaseDiario[i] = '') do begin
    	PaseDiario.Delete(i);
        Dec(i);
    end;
end;

procedure TfrmDayPass.RBIResumenExecute(Sender: TObject; var Cancelled: Boolean);										//SS_1313_NDR_20150615
var																					                                    //SS_1313_NDR_20150615
    FError: AnsiString;																                            		//SS_1313_NDR_20150615
begin																				                                    //SS_1313_NDR_20150615
  ppFechaProcesamiento.Caption:=DateToStr(NowBase(DMConnections.BaseCAC));			  									//SS_1313_NDR_20150615
  spResumenDayPass.Close;															                            		//SS_1313_NDR_20150615
  spResumenDayPass.Parameters.Refresh;                                       											//SS_1313_NDR_20150615
  spResumenDayPass.Parameters.ParamByName('@CodigoProveedorDaypass').Value:=1;  										//SS_1313_NDR_20150615
  spResumenDayPass.Open;															                            		//SS_1313_NDR_20150615
end;																				                                    //SS_1313_NDR_20150615


{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 23/12/2004
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TfrmDayPass.RegistrarOperacion(CodigoModulo: integer; sFileName: string) : boolean;
resourcestring
	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
    DescError : string;
begin
	result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, CodigoModulo, sFileName, UsuarioSistema, '', True, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);
   if not Result then
    	MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;


{-----------------------------------------------------------------------------
  Function Name: ObtenerCantidadErroresInterfaz
  Author:    lgisuk
  Date Created: 21/06/2005
  Description: Obtengo la cantidad de errores contemplados que se produjeron
               al procesar el archivo
  Parameters: CodigoOperacion:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TfrmDayPass.ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
resourcestring
    MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
begin
    Cantidad := 0;
    try
        Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
        Result := true;
    except
       on e: Exception do begin
           MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
           Result := False;
       end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarLog
  Author:    lgisuk
  Date Created: 01/07/2005
  Description: Actualizo el log al finalizar
  Parameters: CantidadErrores:integer
  Return Value: boolean

  Revision 1
  Author: mbecerra
  Date: 27-Mayo-2008
  Description: Se modific� para que considere si el proceso finaliz� con �xito o con errores,
            	para que no almacene un mensaje que induce al error al Operador

  Revision 2
  Author: mbecerra
  Date: 25-Junio-2008
  Description: Se modific� para que guarde en la tabla LogOperacionesInterfases la cantidad
            	de registros procesados y la cantidad total de registros
-----------------------------------------------------------------------------}
Function TfrmDayPass.ActualizarLog;
Resourcestring
    MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
Const
    STR_FINALIZADO_OK   = 'Finalizado OK!.';
    STR_FINALIZADO_NOOK = 'Finalizado con Errores de validaci�n: %d';
var
    Mensaje, DescError : string;
begin
    try
    	//Revision 1
    	if FErrors then Mensaje := Format(STR_FINALIZADO_NOOK, [FCantidadErrores])
        else Mensaje := STR_FINALIZADO_OK;

        Result := ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacion, Mensaje, DescError);

        //Revision 2
        if Result then begin
        	spActualizarLogOperacionesRegistrosProcesados.Close;
        	with  spActualizarLogOperacionesRegistrosProcesados do begin
            	Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                Parameters.ParamByName('@RegistrosAProcesar').Value       := FPasesDiariosUso.Count - 1 - FCantidadErrores;
                Parameters.ParamByName('@LineasArchivo').Value            := FPasesDiariosUso.Count - 1;
                ExecProc;
            end;

        end;

    except
        on e: Exception do begin
           MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
           Result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Genero el Reporte de Finalizacion de Proceso sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean

-----------------------------------------------------------------------------}
Function TFrmDayPass.GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
    var
        F:TRptDayPassForm;
begin
    Result:=false;
    try
        //muestro el reporte
        F := TRptDayPassForm.Create(Self);
        if F.Inicializar('Day Pass',CodigoOperacionInterfase) then
            F.RBIListado.Execute;
        Result:=true;
    finally
        if Assigned(F) then FreeAndNil(F);
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: btnGenerarClick
  Author:    gcasais
  Date Created: 06/07/2005
  Description: Genera el archivo de patentes
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnGenerarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: CrearArchivo
      Author:    gcasais
      Date Created: 06/07/2005
      Description: Crea el archivo
      Parameters: Dir, Nombre: String; var Archivo: TextFile; var DescriError: String
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    function CrearArchivo(Dir, Nombre: String; var Archivo: TextFile; var DescriError: String): Boolean;
    var
        NArch: String;
    begin
        Result := False;
        nArch := GoodDir(Dir) + Nombre;
        if FileExists(NArch) then begin
            DescriError := Format('El archivo %s ya existe en el directorio de destino', [NArch]);
            Exit;
        end;
        try
            AssignFile(Archivo, NArch );
            Rewrite(Archivo);
            Result := True;
        except
            on e: Exception do begin
                DescriError := e.Message;
                Exit;
            end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: GenerarArchivoPatentes
      Author:   gcasais
      Date Created: 06/07/2005
      Description:     Genera archivo de patentes
      Parameters: var DescriError: String
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    function GenerarArchivoPatentes(var DescriError: String): Boolean;
    Const
        FILE_EXT = '.PLA';
        LEN      = 10;
    var
        FDir_Archivo_Patentes : AnsiString;
        ArchivoLP: TextFile;
        Error: String;
        Patente: String;
        Cant: Integer;
        Control: String;
    begin
        MoveToMyOwnDir;
        Result := ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_ARCHIVO_PATENTES', FDir_Archivo_Patentes);
        SpPatentes.CommandTimeout := 180;
        if Result then FDir_Archivo_Patentes := GoodDir(FDir_Archivo_Patentes) else Exit;
        try
            spPatentes.Open;
            if spPatentes.RecordCount <= 0 then
              raise Exception.Create('No se encontraron patentes para informar');
        except
            on e: Exception do begin
                DescriError := e.Message;
                Exit;
            end;
        end;
        pbAvance.Visible := False;
        pbAvance.Max := spPatentes.RecordCount;
        pbAvance.Min := 0;
        pbAvance.Position := 0;
        lblProgreso.Visible := False;
        lblDescri.VisiBle := False;
        // Tenemos las patentes, vamos a crear el archivo
        if not CrearArchivo(FDir_Archivo_Patentes,
          //FILE_NAME_PREFIX + FormatDateTime('yymmdd', Now) + FILE_EXT, ArchivoLP, Error) then begin        //SS_1256_NDR_20150505
          sPrefijoNombre + FormatDateTime('yymmdd', Now) + FILE_EXT, ArchivoLP, Error) then                  //SS_1256_NDR_20150505
        begin                                                                                                //SS_1256_NDR_20150505
          MsgBoxErr('Error', Error, 'Crear Archivos de Patentes', MB_ICONERROR);
          Result := False;
          Exit;
        end;
        Cant := 0;
        FProcesando := True;
        tsRecibirPasesDiarios.Enabled := False;
        lblProgreso.Visible := True;
        pbAvance.Visible := True;
        lblDescri.Caption := 'Informando ' + IntToStr(pbAvance.Max) + ' patentes prohibidas';
        lblDescri.Visible := True;
        //Listo, empezamos a escribir el archivo
        try
            while not (spPatentes.Eof) and (not FCancelar) do begin
                Patente := Copy(spPatentes.FieldByName('Patente').AsString, 1, 6);
                Patente := PadR(Patente, LEN, ' ');
                Inc(Cant);
                WriteLn(ArchivoLP, Patente);
                pbAvance.StepIt;
                spPatentes.Next;
                pbAvance.Update;
            end;
        except
            on e: Exception do begin
                FCancelar := True;
                FErrors:= True;
                DescriError := e.Message;
                FProcesando := False;
                Result := False;
            end;
        end;
        // Escribimos el registro de control que es la cantidad de registros en el archivo
        // m�s el mismo registro de control
        if not FCancelar then begin
            Inc(Cant);
            Control := PadL(IntToStr(Cant), LEN, '0');
            WriteLn(ArchivoLP, Control);
            CloseFile(ArchivoLP);
            CheckForError;
            tsRecibirPasesDiarios.Enabled := True;
            FErrors:= False;
            FProcesando := False;
            MsgBox('Proceso Finalizado', 'Patentes Prohibidas', MB_ICONINFORMATION);
        end else begin
            CloseFile(ArchivoLP);
            Result := False;
            DescriError := 'Proceso Cancelado por el usuario';
            FProcesando := False;
        end;
    end;

var
    Err: String;
begin
    //Generamos el Archivo de patentes
    FCancelar := False;
    lblDescri.Visible := True;
    lblDescri.Caption := 'Obteniendo patentes, aguarde por favor...';
    btnGenerar.Enabled := False;
    lblDescri.Update;
    if not GenerarArchivoPatentes(Err) then begin
        MsgBoxErr('Proceso Cancelado', Err, 'Patentes Prohibidas', MB_ICONERROR);
    end;
    btnGenerar.Enabled := True;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarPlatesClick
  Author:    gcasais
  Date Created: 06/07/2005
  Description: Permito cancelar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnCancelarPlatesClick(Sender: TObject);
begin
    FCancelar := True;
end;

{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}
{  FUNCIONES Y PROCEDIMIENTOS IMPORTACION PASE DIARIO						   }
{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}

{-----------------------------------------------------------------------------
  Function Name: ObtenerArchivoPaseDiario
  Author:    flamas
  Date Created: 18/02/2005
  Description: Obtiene el Archivo a Recibir busca el de la fecha
  				o el del d�a anterior
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
function TfrmDayPass.ObtenerArchivoPaseDiario(sExtension: string) : string;
var
	sArchivo 	: string;
    Hoy		 	: TDateTime;
    DiaAnterior	: TDateTime;
begin
	  result 		:= '';
    Hoy 		:= NowBase(DMConnections.BaseCAC);

	  // Busca para procesar el archivo del D�a y si no el del d�a Anterior
	  //sArchivo 	:= FServipag_Directorio_Entrada + HABILITACION_NAME_PREFIX + FormatDateTime('yymmdd', Hoy) + '01' + sExtension;            //SS_1256_NDR_20150505
	  sArchivo 	:=  FServipag_Directorio_Entrada +                                                                                           //SS_1256_NDR_20150505
                  HABILITACION_NAME_PREFIX +                                                                                               //SS_1256_NDR_20150505
                  FormatDateTime('yymmdd', Hoy) +                                                                                          //SS_1256_NDR_20150505
                  '0'+Trim(IntToStr(ObtenerCodigoConcesionariaNativa)) +                                                                   //SS_1256_NDR_20150505
                  sExtension;                                                                                                              //SS_1256_NDR_20150505
    if FileExists(sArchivo) then
      result := sArchivo
    else
    begin
		  // Busca el d�a anterior
    	DiaAnterior := iif(DayOfTheWeek(Hoy) = 1, Hoy-2, Hoy-1);
		  //sArchivo := FServipag_Directorio_Entrada + HABILITACION_NAME_PREFIX + FormatDateTime('yymmdd', DiaAnterior) + '01' + sExtension;   //SS_1256_NDR_20150505
		  sArchivo := FServipag_Directorio_Entrada +                                                                                           //SS_1256_NDR_20150505
                  HABILITACION_NAME_PREFIX +                                                                                               //SS_1256_NDR_20150505
                  FormatDateTime('yymmdd', DiaAnterior) +                                                                                  //SS_1256_NDR_20150505
                  '0'+Trim(IntToStr(ObtenerCodigoConcesionariaNativa)) +                                                                   //SS_1256_NDR_20150505
                  sExtension;                                                                                                              //SS_1256_NDR_20150505
    	if FileExists(sArchivo) then result := sArchivo;
    end;

	  btnProcesarPD.Enabled := (result <> '');
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerArchivoVentasPaseDiario
  Author:    flamas
  Date Created: 20/04/2005
  Description: Busca el Archivo de Ventas correspondiente al de Usos
  				devuelve la cantidad de archivos encontrados.
  Parameters: sArchivoUso: string; var sArchivoVenta : string
  Return Value: integer

  Revision 1
  Author: mbecerra
  Date: 03-Julio-2008
  Description: se modific� para que busque el archivo exacto de ventas, y no s�lo los que tengan
            	el prefijo*.txt.

                Y como no pueden existir dos archivos con el mismo nombre,
                s�lo se valida que exista.
-----------------------------------------------------------------------------}
function TfrmDayPass.ObtenerArchivoVentasPaseDiario;
{ Revision 1
var
 	searchRec : TSearchRec;
    sMascaraVentas : string;}
begin
    {Revision 1
	result := 0;
    sMascaraVentas := Trim(StringReplace(FUsoFileName, HABILITACION_NAME_PREFIX, VENTAS_NAME_PREFIX, []));
    sMascaraVentas := Copy(sMascaraVentas, 1, Length(sMascaraVentas) - 6) + '*.TXT';
	if FindFirst(sMascaraVentas, 0, searchRec) = 0 then begin
		sArchivoVenta := GoodDir(ExtractFilePath(FUsoFileName)) + searchRec.Name;
        result := 1;
        if FindNext(searchRec) = 0 then result := 2;
    end;       }

    sArchivoVenta := ExtractFileName(FUsoFileName);
    sArchivoVenta := Trim(StringReplace(sArchivoVenta, HABILITACION_NAME_PREFIX, VENTAS_NAME_PREFIX, []));
    sArchivoVenta := ExtractFilePath(FUsoFileName) + sArchivoVenta;
    if FileExists(sArchivoVenta) then Result := 1
    else Result := 0;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoPDClick
  Author:    flamas
  Date Created: 18/02/2005
  Description: Abre un archivo
  Parameters: Sender: TObject
  Return Value: None

  Revision 1
  Author: mbecerra
  Date: 03-Julio-2008
  Description: la invocaci�n de la funci�n  ObtenerArchivoVentasPaseDiario
  				ha cambiado.
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnAbrirArchivoPDClick(Sender: TObject);
resourcestring
    MSG_ERROR 							            = 'Error';
	  MSG_ERROR_FILE_DOES_NOT_EXIST 		  = 'El archivo %s no existe';
    MSG_ERROR_FILE_DOES_NOT_MATCH       = 'Identificador de Concesionaria "%s" del archivo no corresponde a Concesionaria Nativa';   //SS_1256_NDR_20150505
	  MSG_ERROR_SALES_FILE_DOES_NOT_EXIST = 'El archivo de ventas no existe';
	  MSG_ERROR_MORE_THAN_ONE_SALES_FILE	= 'Existe m�s de un archivo de ventas v�lido.'+crlf+
                      										'Verifique cu�l es el correcto y deje s�lo dicho archivo.';
    MSG_WISH_TO_PROCESS_ANYWAY			    = '� Desea procesar s�lo el archivo de usos ?';
    DAYPASS_FILES_FILTER				        = 'Pases Diarios|' + HABILITACION_NAME_PREFIX + '*.TXT';
var
	nCantArchivosVtas : integer;
begin
    OpenDialog.InitialDir := FServipag_Directorio_Entrada;
	OpenDialog.FileName := '';
    OpenDialog.Filter := DAYPASS_FILES_FILTER;
    if OpenDialog.Execute then
    begin
        edOrigenPD.text:=UpperCase( OpenDialog.filename );

        if not FileExists( edOrigenPD.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigenPD.Text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

        if (RightStr( Trim( ReplaceStr(edOrigenPD.text,'.TXT','')) , 2 )<>                                                          //SS_1256_NDR_20150505
            ('0'+Trim(IntToStr(ObtenerCodigoConcesionariaNativa)))                                                                  //SS_1256_NDR_20150505
           ) then                                                                                                                   //SS_1256_NDR_20150505
        begin                                                                                                                       //SS_1256_NDR_20150505
        	MsgBox( Format( MSG_ERROR_FILE_DOES_NOT_MATCH,                                                                            //SS_1256_NDR_20150505
                          [RightStr( Trim( ReplaceStr(edOrigenPD.text,'.TXT','')) , 2 )]                                            //SS_1256_NDR_20150505
                        ),                                                                                                          //SS_1256_NDR_20150505
                  MSG_ERROR,                                                                                                        //SS_1256_NDR_20150505
                  MB_ICONERROR                                                                                                      //SS_1256_NDR_20150505
                );                                                                                                                  //SS_1256_NDR_20150505
            Exit;                                                                                                                   //SS_1256_NDR_20150505
        end;                                                                                                                        //SS_1256_NDR_20150505

		  // Obtiene el nombre del Archivo de Ventas
    	FUsoFileName := UpperCase(edOrigenPD.Text);
        {Revision 1
        nCantArchivosVtas := ObtenerArchivoVentasPaseDiario(FUsoFileName, FVentasFileName );}
        nCantArchivosVtas := ObtenerArchivoVentasPaseDiario(FVentasFileName );
		  FProcesarSinVentas := False;

		  if (nCantArchivosVtas > 1) then
      begin
        	MsgBox( MSG_ERROR_MORE_THAN_ONE_SALES_FILE, MSG_ERROR, MB_ICONERROR );
            Exit;
      end
      else if (nCantArchivosVtas = 0) then
      begin
        	if MsgBox(MSG_ERROR_SALES_FILE_DOES_NOT_EXIST + CRLF +
            	MSG_WISH_TO_PROCESS_ANYWAY, Caption, MB_ICONWARNING or MB_YESNO) = IDYES then
                	FProcesarSinVentas := True
            else Exit;
      end;
    end;

    btnProcesarPD.Enabled := True
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarPDClick
  Author:    flamas
  Date Created: 18/02/2005
  Description: Procesa el Archivo de Pases Diarios
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  11/10/07
  Ahora se registra en el log que el usuario cancelo el proceso
-----------------------------------------------------------------------------
  Revision 2
  lgisuk
  05/11/07
  Ahora Muestro un mensaje descriptivo con numero de linea y linea del problema.
  Registro en log si se produce una excepci�n.

  Revision 3
  Author: mbecerra
  Date: 10-Junio-2008
  Description:
            	Se quit� la llamada a la funci�n AnalizarArchivoPasesDiarios
                pues es redundante: Hace un parseo de cada l�nea y almacena en el log los errores
                Es lo mismo que la funci�n CargarPasesDiarios.
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnProcesarPDClick(Sender: TObject);
resourcestring
    MSG_ERROR 								= 'Error';
	MSG_FILE_ALREADY_PROCESSED 				= 'El archivo %s ya fue procesado' + crlf + 'Desea reprocesarlo ?';
    MSG_ERRROR_DAYPASS_FILE_IS_EMPTY		= 'El archivo de pases diarios est� vac�o';
	MSG_ERROR_CANNOT_OPEN_DAYPASS_FILE 		= 'No se puede abrir el archivo de pases diarios';
	MSG_PROCESS_SUCCEDED 					= 'El proceso finaliz� con �xito';
	MSG_PROCESS_ENDED_WITH_ERRORS			= 'El proceso finaliz� con errores';
	MSG_PROCESS_COULD_NOT_BE_COMPLETED 		= 'El proceso no se pudo completar';
	MSG_ANALIZING_DAYPASS_FILE				= 'Analizando Archivo Pase Diario - Cantidad de lineas : %d';
    MSG_DAYPASS_FILE_HAS_ERRORS 			= 'El archivo de pases diarios contiene errores'#10#13'L�nea : %d';
	MSG_ANALIZING_DAYPASS_SALES_FILE		= 'Analizando archivo de Ventas de Pase Diario - Cantidad de lineas : %d';
    MSG_DAYPASS_SALES_FILE_HAS_ERRORS 		= 'El archivo de ventas de pases diarios contiene errores'#10#13'L�nea : %d';
    MSG_PROCESSING_DAYPASS_FILE				= 'Procesando archivo de Pases Diarios - Cantidad de lineas : %d';
    MSG_ERROR_PROCESSING_DAYPASS_FILE		= 'Error procesando archivo de Pases Diarios ';
    MSG_PROCESSING_DAYPASS_SALES_FILE		= 'Procesando archivo Ventas de Pases Diarios - Cantidad de lineas : %d';
    MSG_ERROR_PROCESSING_DAYPASS_SALES_FILE	= 'Error procesando archivo de Pases Diarios ';
    MSG_DAYPASS_SALES_SUCCEDED				= 'La importaci�n de Ventas de Pases Diarios finaliz� con �xito';
    MSG_DAYPASS_SUCCEDED					= 'La importaci�n de Pases Diarios finaliz� con �xito';
    MSG_DAYPASS_FILE						= 'Archivo de Pase Diario';
    MSG_DAYPASS_SALES_FILE					= 'Archivo de Ventas Pase Diario';
    MSG_ERROR_DAYPASS_REPORT		= 'Error emitiendo informe resumen de Pases Diarios';               //SS_1313_NDR_20150615

begin
	// Verifica que el Archivo no haya sido procesado
	if  VerificarArchivoProcesado(DMConnections.BaseCAC, edOrigenPD.Text) then begin
        if MsgBox (Format (MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(edOrigenPD.Text)]),
        	Caption, MB_ICONWARNING or MB_YESNO) <> IDYES then Exit;
    end;

    //Creo los StringList
    FPasesDiariosUso 	:= TStringList.Create;
    FPasesDiariosVentas := TStringList.Create;

	// Deshabilita y Habilita los botones
	btnCancelarPD.Enabled := True;
	btnProcesarPD.Enabled := False;
	btnCerrar.Enabled := False;
	btnAbrirArchivoPD.Enabled := False;
	edOrigenPD.Enabled := False;
	FDetenerImportacion := False;
    FProcesando := True;
    FErrors := False;
    FErrorLog.Clear;
    tsArchivoPatentes.Enabled := False;
    tsMaestroPases.Enabled := False;
    tsCambioFecha.Enabled := False;
    //tsPasesDiariosTardios.Enabled := False;

    try
		try
            //leo el archivo pases diarios
			FPasesDiariosUso.text:= FileToString(FUsoFileName);

            //Elimino las lineas en blanco
            EliminarLineasEnBlanco(FPasesDiariosUso);

            //Si procesa con ventas
            if not FProcesarSinVentas then begin
                //leo el archivo de pases diarios con ventas
                FPasesDiariosVentas.Text := FileToString(FVentasFileName);
            end;

			//Verifica si el Archivo Contiene alguna linea
			if (FPasesDiariosUso.Count = 0) then begin

                //Informo que el archivo esta vacio
				MsgBox(MSG_ERRROR_DAYPASS_FILE_IS_EMPTY, Caption, MB_ICONERROR);

			end else begin
            	{Revision 3
            	if VerificarCantidadDeRegistros(FPasesDiariosUso, MSG_DAYPASS_FILE) and
                   AnalizarArchivoPasesDiarios( FPasesDiariosUso, MSG_ANALIZING_DAYPASS_FILE, MSG_DAYPASS_FILE_HAS_ERRORS, 'H') and
					(FProcesarSinVentas or
                    ( 	VerificarCantidadDeRegistros(FPasesDiariosVentas, MSG_DAYPASS_SALES_FILE) and
                    	AnalizarArchivoPasesDiarios( FPasesDiariosVentas, MSG_ANALIZING_DAYPASS_SALES_FILE, MSG_DAYPASS_SALES_FILE_HAS_ERRORS, 'V')) ) then begin}
            	if VerificarCantidadDeRegistros(FPasesDiariosUso, MSG_DAYPASS_FILE) and
				   (FProcesarSinVentas or
                		VerificarCantidadDeRegistros(FPasesDiariosVentas, MSG_DAYPASS_SALES_FILE) ) then begin

                        FCantidadErrores := 0;
                    	if 	RegistrarOperacion(RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO, ExtractFileName(edOrigenPD.Text)) and
                        	CargarPasesDiarios(FPasesDiariosUso, MSG_PROCESSING_DAYPASS_FILE, MSG_ERROR_PROCESSING_DAYPASS_FILE, MSG_DAYPASS_SUCCEDED, 'H') and
                            (FProcesarSinVentas or
                             CargarPasesDiarios(FPasesDiariosVentas, MSG_PROCESSING_DAYPASS_SALES_FILE, MSG_ERROR_PROCESSING_DAYPASS_SALES_FILE, MSG_DAYPASS_SUCCEDED, 'V'))then begin

                                //Verifico si hubo errores
                                if not FErrors then begin
                                    //Muestro un cartel que finalizo con exito
                                    MsgBox(MSG_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION);
                                end else begin
                                    //Muestro un cartel que finalizo con error
                                    MsgBox(MSG_PROCESS_ENDED_WITH_ERRORS, Caption, MB_ICONINFORMATION);
                                end;

                                // Graba los Errores encontrados
        						            CheckForError();
                                //Obtengo la cantidad de errores
                                //ObtenerCantidadErroresInterfaz(FCodigoOperacion, FCantidadErrores);
                                //Actualizo el log al Final
                                ActualizarLog(0);
                                //Muestro el Reporte de Finalizacion del Proceso
                                GenerarReportedeFinalizacion(FCodigoOperacion);
                                //Muevo el archivo de pases diarios a otro directorio
                                MoverArchivoProcesado(Caption,FUsoFileName, FServipag_Directorio_Procesados + ExtractFileName(FUsoFileName));
                                //Si procesa con ventas
                                if not (FProcesarSinVentas) then begin
                                    //Muevo el archivo pases diarios con ventas a otro directorio
                                	MoverArchivoProcesado(Caption, FVentasFileName, FServipag_Directorio_Procesados + ExtractFileName(FVentasFileName));
                                end;
                                try                                                                                   //SS_1313_NDR_20150615
                                  RBIResumen.Execute(True);                                                           //SS_1313_NDR_20150615
                                except                                                                                //SS_1313_NDR_20150615
                                  on e: Exception do begin                                                            //SS_1313_NDR_20150615
                                    MsgBoxErr(MSG_ERROR_DAYPASS_REPORT, e.Message, 'Error', MB_ICONERROR);            //SS_1313_NDR_20150615
                                  end;                                                                                //SS_1313_NDR_20150615
                                end;                                                                                  //SS_1313_NDR_20150615

                        end else begin

                            if FDetenerImportacion then begin
                              //registro que el proceso fue cancelado
                              RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacion);
                            end;

                                //Informo que el proceso no se pudo completar
                    			MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);

                        end;
                end;
			end;
		except
			on e: Exception do begin
				MsgBoxErr(MSG_ERROR_CANNOT_OPEN_DAYPASS_FILE, e.Message, 'Error', MB_ICONERROR);
			end;
		end;
    finally
        //Libero los StringList
    	FPasesDiariosUso.Free;
    	FPasesDiariosVentas.Free;
		//Deshabilita y Habilita los botones
		btnCancelarPD.Enabled := False;
		btnProcesarPD.Enabled := False;
		btnCerrar.Enabled := True;
		btnAbrirArchivoPD.Enabled := True;
		edOrigenPD.Enabled := True;
    	tsArchivoPatentes.Enabled := True;
    	tsMaestroPases.Enabled := True;
    	tsCambioFecha.Enabled := True;
		//tsPasesDiariosTardios.Enabled := True;
    	FErrors:= False;
    	FProcesando := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: VerificarCantidadDeRegistrosPD
  Author:    flamas
  Date Created: 18/02/2005
  Description: Verifica que la cantidad de registros coincida con el
  				registro de control
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmDayPass.VerificarCantidadDeRegistros(FPasesDiarios : TStringList; sArchivo : string) : boolean;
resourcestring
	MSG_ERROR						= 'Error';
	MSG_INVALID_RECORD_NUMBER 		= 'El n�mero de registros del %s ' +CRLF+
    									'no coincide con el registro de control';
    MSG_VERIFYING_CONTROL_REGISTER 	= 'Verificando registro de control del ';
var
	nLen : integer;
begin
	Result 	:= True;
	lblReferenciaPD.Caption := MSG_VERIFYING_CONTROL_REGISTER + sArchivo;
	nLen 	:= FPasesDiarios.Count;
	if (StrToIntDef(Copy(FPasesDiarios[nLen-1], 1, 10),0) <> nLen) then begin
    	MsgBox(Format(MSG_INVALID_RECORD_NUMBER, [sArchivo]), 'Error', MB_ICONERROR);
		Result := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AnalizarArchivoPasesDiarios
  Author:    flamas
  Date Created: 18/02/2005
  Description: Analiza el la Validez del Archivo de pase Diario
  Parameters: None
  Return Value: boolean

  Revision 1:
  Author: mbecerra
  Date: 26-Mayo-2008
  Description:  Si hay errores en el parseo de alguna l�nea, el sistema
                contin�a procesando el resto, sin detener el proceso completo.
-----------------------------------------------------------------------------}
function TfrmDayPass.AnalizarArchivoPasesDiarios(FPasesDiarios : TStringList; sMsgAnalizando, sMsgTieneErrores, sTipoArchivo: string): boolean;
resourcestring
    MSG_ERROR= 'Error';
var
	nNroLineaScript, nLineasScript :integer;
    FDayPassRecord 	: TDayPassRecord;
	sParseError		: string;
    sErrorMsg   	: string;
begin
	Screen.Cursor := crHourglass;

	nLineasScript := FPasesDiarios.Count - 1;
	if (sTipoArchivo = 'C') then begin
        lblReferenciaCF.Caption := Format(sMsgAnalizando, [nLineasScript]);
        pbProgresoCF.Position 	:= 0;
        pbProgresoCF.Max 		:= FPasesDiarios.Count - 1;
        pnlAvanceCF.Visible 	:= True;
    end else if (sTipoArchivo = 'D') then begin
        lblReferenciaDev.Caption := Format(sMsgAnalizando, [nLineasScript]);
        pbProgresoDev.Position 	:= 0;
        pbProgresoDev.Max 		:= FPasesDiarios.Count - 1;
        pnlAvanceDev.Visible 	:= True;
    end else begin
        lblReferenciaPD.Caption := Format(sMsgAnalizando, [nLineasScript]);
        pbProgresoPD.Position 	:= 0;
        pbProgresoPD.Max 		:= FPasesDiarios.Count - 1;
        pnlAvancePD.Visible 	:= True;
    end;

    nNroLineaScript := 0;

    while ( nNroLineaScript < nLineasScript ) and
    	  ( not FDetenerImportacion ) {and              revision 1
          (	sErrorMsg = '' )} do begin

    	if not ParseDayPassLine( sTipoArchivo, FPasesDiarios[nNroLineaScript], FDayPassRecord, sParseError ) then begin
        	sErrorMsg := Format( sMsgTieneErrores, [nNroLineaScript] ) + ' ' + sParseError;
            //MsgBox(sErrorMsg, Caption, MB_ICONERROR);          Revision 1
            FErrorLog.Add(sErrorMsg);							{Revision 1}
            Screen.Cursor := crDefault;
            result := False;
            Exit;
        end;

        Inc( nNroLineaScript );
		if (sTipoArchivo = 'C') then pbProgresoCF.Position := nNroLineaScript
        else if (sTipoArchivo = 'D') then pbProgresoDev.Position := nNroLineaScript
        else pbProgresoPD.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

	if (sTipoArchivo = 'C') then begin
        lblReferenciaCF.Caption := '';
        pbProgresoCf.Position := 0;
    end else if (sTipoArchivo = 'D') then begin
        lblReferenciaDev.Caption := '';
        pbProgresoDev.Position := 0;
    end else begin
        lblReferenciaPD.Caption := '';
        pbProgresoPD.Position := 0;
    end;
    Application.ProcessMessages;

	result := ( not FDetenerImportacion ); // and ( sErrorMsg = '' );        Revision 1
	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarPasesDiarios
  Author:    flamas
  Date Created: 18/02/2005
  Description: Carga los Pases Diarios
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  05/11/2007
  Muestro un mensaje descriptivo con numero de linea y linea del problema.
  Registro en log si se produce una excepci�n.

  Revision 2
  Author: mbecerra
  Date: 27-Mayo-2008
  Description:
  				FErrors no puede ser configurado con valor inicial en False, ya que
                la funci�n es llamada dos veces: una para el archivo de uso y la otra para
                el archivo de ventas

                Se cambia el uso de la funci�n ActualizarObservacionLogOperaciones
                por AgregarErrorInterfase, ya que el error registrado por la primera
                funci�n es sobreescrito por la funci�n ActualizarLogInterfaseAlFinal


  Revision 3
  Author: mbecerra
  Date: 10-Junio-2008
  Description:
                Dado que se elimin� la llamada a la funci�n AnalizarArchivoPasesDiarios
                es necesario agregar los errores del parseo

  Revision 4
  Author: mbecerra
  Date: 25-Junio-2008
  Description:
                Se agrega la cantidad de errores para grabarse en LogOperacionesInterfases.
                (S�lo para el archivo de tipo 'H')

-----------------------------------------------------------------------------}
function TfrmDayPass.CargarPasesDiarios(FPasesDiarios : TStringList; sMsgProcesando, sMsgTieneErrores, sMsgProcesoOK, sTipoArchivo: string) : boolean;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerFechaArchivo
      Author:    flamas
      Date Created: 21/02/2005
      Description: Obtiene la Fecha del Archivo en funci�n del nombre del mismo
      Parameters: None
      Return Value: TDateTime
    -----------------------------------------------------------------------------}
    function  ObtenerFechaArchivo(sTipoArchivo :string) : TDateTime;
    var
        sFecha : string;
    begin
        if (sTipoArchivo = 'C') then sFecha := Copy( ExtractFileName(edOrigenCF.Text), 8, 8)
        else if (sTipoArchivo = 'D') then sFecha := Copy( ExtractFileName(edOrigenDev.Text), 8, 8)
        else sFecha := Copy( ExtractFileName(edOrigenPD.Text), 8, 8);

        try
            result := EncodeDate(StrToInt('20' + Copy(sFecha, 2, 2)), StrToInt(Copy(sFecha, 4, 2)), StrToInt(Copy(sFecha,6, 2)));
        except
            on exception do result := nulldate;
        end;
    end;

resourcestring
	MSG_ERROR           = 'Error';
    MSG_CANCELADO       = 'Proceso cancelado por el usuario';
    MSG_PROCESO_ERRORES = 'Proceso finalizado con errores';
const
	STR_LINE =  'Linea: ';
var
	nNroLineaScript, nLineasScript : integer;
    FDayPassRecord 		: TDayPassRecord;
	sParseError			: string;
    sErrorMsg   		: string;
    sDescripcionError 	: string;
    dFechaArchivo		: TDateTime;
begin
	Screen.Cursor := crHourglass;

	nLineasScript := FPasesDiarios.Count - 1;
	if (sTipoArchivo = 'C') then begin
        lblReferenciaCF.Caption := Format(sMsgProcesando, [nLineasScript]);
        pbProgresoCF.Position 	:= 0;
        pbProgresoCF.Max 		:= FPasesDiarios.Count - 1;
        pnlAvanceCF.Visible 	:= True;
    end else if (sTipoArchivo = 'D') then begin
        lblReferenciaDev.Caption := Format(sMsgProcesando, [nLineasScript]);
        pbProgresoDev.Position 	:= 0;
        pbProgresoDev.Max 		:= FPasesDiarios.Count - 1;
        pnlAvanceDev.Visible 	:= True;
    end else begin
        lblReferenciaPD.Caption := Format(sMsgProcesando, [nLineasScript]);
        pbProgresoPD.Position 	:= 0;
        pbProgresoPD.Max 		:= FPasesDiarios.Count - 1;
        pnlAvancePD.Visible 	:= True;
    end;

    dFechaArchivo := ObtenerFechaArchivo(sTipoArchivo);
   {	FErrors:= False;}
    nNroLineaScript := 0;

    while ( nNroLineaScript < nLineasScript ) and ( not FDetenerImportacion ) and (	sErrorMsg = '' )  do begin

    	if ParseDayPassLine( sTipoArchivo, FPasesDiarios[nNroLineaScript], FDayPassRecord, sParseError ) then begin
        	with FDayPassRecord, spAgregarDayPass, Parameters do begin

				try
                    ParamByName('@CodigoOperacionInterfase' ).Value	:= FCodigoOperacion;
                    ParamByName('@NumeroSerie' ).Value				:= NumeroSerie;
                    ParamByName('@Patente' ).Value					:= Patente;
                    ParamByName('@Pin' ).Value						:= Pin;
                    ParamByName('@ValidezDesde').Value				:= Null;
                    ParamByName('@ValidezHasta').Value				:= Null;
                    ParamByName('@Monto').Value						:= Null;
                    ParamByName('@FechaVenta' ).Value				:= iif(FechaVenta=nulldate, dFechaArchivo, FechaVenta);
                    ParamByName('@FechaUso' ).Value					:= iif(FechaUso=nulldate, Null, FechaUso);
                    ParamByName('@FechaDevolucion' ).Value 			:= iif(FechaDevolucion=nulldate, Null, FechaDevolucion);
                    ParamByName('@CodigoMaterial' ).Value			:= CodigoMaterial;
                    ParamByName('@Distribuidor' ).Value				:= Distribuidor;
                    ParamByName('@RUTContacto' ).Value				:= RUTContacto;
                    ParamByName('@TelefonoContacto' ).Value			:= TelefonoContacto;
                    ParamByName('@NombreContacto' ).Value			:= NombreContacto;
                    ParamByName('@TipoArchivo' ).Value				:= TipoArchivo;
                    ParamByName('@CodigoProveedorDaypass' ).Value   := 1; // SERVIPAG
                    ParamByName('@DescripcionError').Value          := NULL;
                    spAgregarDayPass.ExecProc;

                	sDescripcionError := Trim( VarToStr( ParamByName( '@DescripcionError' ).Value ));
                	if ( sDescripcionError <> '' ) then begin
                    	FErrorLog.Add( StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]));
                        FErrors:= True;
                        if sTipoArchivo = 'H' then Inc(FCantidadErrores);

                    end;
				except
                	on e: exception do begin
                        //Registro la excepcion en el log de operaciones
                        //ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacion, sMsgTieneErrores + ' - ' + STR_LINE + IntToStr(nNroLineaScript + 1) + ' - ' + FPasesDiarios[nNroLineaScript] + ' - ' + E.Message);
                        AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, sMsgTieneErrores + ' - ' + STR_LINE + IntToStr(nNroLineaScript + 1) + ' - ' + FPasesDiarios[nNroLineaScript] + ' - ' + E.Message);
                        //Informo que se produjo un error al procesar
                        MsgBoxErr(sMsgTieneErrores + CRLF + STR_LINE + IntToStr(nNroLineaScript + 1) + CRLF + FPasesDiarios[nNroLineaScript], e.Message, MSG_ERROR, MB_ICONERROR);
                        sErrorMsg := sMsgTieneErrores;
                        FErrors:= True;
					end;
              	end
            end;
        end
        else begin  {revision 3}
        	sErrorMsg := MSG_ERROR + ' ' + STR_LINE + IntToStr(nNroLineaScript + 1) + ' - ' + sParseError;
            FErrorLog.Add(sErrorMsg);
        	FErrors := True;
            sErrorMsg := '';  { o sino la funci�n retornar� Falso y no es la idea}
            if sTipoArchivo = 'H' then Inc(FCantidadErrores);
        end;

        Inc( nNroLineaScript );
		if (sTipoArchivo = 'C') then pbProgresoCF.Position := nNroLineaScript
        else if (sTipoArchivo = 'D') then pbProgresoDev.Position := nNroLineaScript
        else pbProgresoPD.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

	if (sTipoArchivo = 'C') then begin
        lblReferenciaCF.Caption := sMsgProcesoOK;
        pbProgresoCf.Position := 0;
    end else if (sTipoArchivo = 'D') then begin
        lblReferenciaDev.Caption := sMsgProcesoOK;
        pbProgresoDev.Position := 0;
    end else begin
    	if FCancelar then lblReferenciaPD.Caption := MSG_CANCELADO
        else if FErrors then lblReferenciaPD.Caption := MSG_PROCESO_ERRORES
        else lblReferenciaPD.Caption := sMsgProcesoOK;
        
        pbProgresoPD.Position := 0;
    end;

	Application.ProcessMessages;

	Screen.Cursor := crDefault;
    Result := ( not FDetenerImportacion ) and ( sErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: ParseDayPassLine
  Author:    flamas
  Date Created: 18/02/2005
  Description: Parsea la linea de Pase Diario
  Parameters: sline : string; var DayPassRecord : TDayPassRecord;
  				var sParseError : string
  Return Value: boolean

-----------------------------------------------------------------------------}
function TfrmDayPass.ParseDayPassLine(sTipoArchivo, sLine : string; var DayPassRecord : TDayPassRecord; var sParseError : string) : boolean;
resourcestring
    MSG_SERIAL_NUMBER_ERROR = 'El n�mero de serie es inv�lido.';
	MSG_USE_DATE_ERROR 		= 'La fecha de uso es inv�lida.';
    MSG_PLATE_ERROR 		= 'La patente es inv�lida.';
    MSG_PARSE_ERROR			= 'Error de formato.';
    MSG_FILE_TYPE_ERROR		= 'El tipo de archivo a procesar no corresponde con el archivo seleccionado';
begin
	Result := False;
    with DayPassRecord do begin
		try
			sParseError 		:= MSG_FILE_TYPE_ERROR;
			TipoArchivo 		:= Trim(Copy( sline, 126, 1));
            if (TipoArchivo <> sTipoArchivo) then Exit;
			sParseError 		:= MSG_SERIAL_NUMBER_ERROR;
 			NumeroSerie			:= Trim(Copy(sline, 1, 18));
			sParseError 		:= MSG_PLATE_ERROR;
			Patente				:= Trim(Copy( sline, 19, 10)); if (Patente = '') then Exit;
			sParseError 		:= MSG_USE_DATE_ERROR;
			if (sTipoArchivo = 'V') then begin
                FechaVenta 		:= EncodeDate(	StrToInt(Copy(sline, 37, 4)),
                								StrToInt(Copy(sline, 41, 2)),
                                                StrToInt(Copy(sline, 43, 2)));
            	FechaUso 		:= nulldate;
                FechaDevolucion := nulldate;
            end else if (sTipoArchivo = 'H') then begin
            	FechaUso 		:= EncodeDate(	StrToInt(Copy(sline, 37, 4)),
                								StrToInt(Copy(sline, 41, 2)),
                                                StrToInt(Copy(sline, 43, 2)));
                FechaVenta 		:= nulldate;
                FechaDevolucion := nulldate;
            end else if (sTipoArchivo = 'D') then begin
            	FechaUso 		:= nulldate;
                FechaVenta 		:= nulldate;
                FechaDevolucion := EncodeDate(	StrToInt(Copy(sline, 37, 4)),
                								StrToInt(Copy(sline, 41, 2)),
                                                StrToInt(Copy(sline, 43, 2)));
            end;

    		sParseError 		:= MSG_PARSE_ERROR;
			Pin					:= Trim(Copy( sline,  29,  8));
			CodigoMaterial 		:= Trim(Copy( sline,  45,  4));
			Distribuidor 		:= Trim(Copy( sline,  49, 10));
			RUTContacto 		:= Trim(Copy( sline,  59, 12));
			TelefonoContacto 	:= Trim(Copy( sline,  71, 15));
			NombreContacto 		:= Trim(Copy( sline,  86, 40));
            sParseError := '';
            Result := True;
        except on e: exception do begin
            	sParseError := sParseError + ' ' + e.Message;
        	end;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarPDClick
  Author:    flamas
  Date Created: 18/02/2005
  Description: Detiene el proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnCancelarPDClick(Sender: TObject);
resourcestring
	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}
{  FUNCIONES Y PROCEDIMIENTOS IMPORTACION PASE DIARIO TARDIO				   }
{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}

{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoPDTClick
  Author:    flamas
  Date Created: 17/03/2005
  Description:	Busca el archivo de Pases Diarios Tard�os
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnAbrirArchivoPDTClick(Sender: TObject);
resourcestring
    MSG_ERROR 						= 'Error';
	  MSG_ERROR_FILE_DOES_NOT_EXIST 	= 'El archivo %s no existe';
    //DAYPASS_FILES_FILTER			= 'Pases Diarios Tard�os|' + FILE_NAME_PREFIX + '*.MOK';   //SS_1256_NDR_20150505
    DAYPASS_FILES_FILTER			= 'Pases Diarios Tard�os|%s*.MOK';                           //SS_1256_NDR_20150505
begin
    OpenDialog.InitialDir := FServipag_Directorio_Entrada;
	  OpenDialog.FileName := '';
    //OpenDialog.Filter := DAYPASS_FILES_FILTER;                                          //SS_1256_NDR_20150505
    OpenDialog.Filter := Format ( DAYPASS_FILES_FILTER, [sPrefijoNombre]);                 //SS_1256_NDR_20150505
    if OpenDialog.Execute then begin
        edOrigenPDT.text:=UpperCase( OpenDialog.filename );

        if not FileExists( edOrigenPDT.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigenPDT.Text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;
    end;

    btnProcesarPDT.Enabled := True
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarPDTClick
  Author:    flamas
  Date Created: 17/03/2005
  Description: Procesa el Archivo de Pases Diarios Tard�os
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  11/10/07
  Ahora se registra en el log que el usuario cancelo el proceso
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnProcesarPDTClick(Sender: TObject);
resourcestring
    MSG_ERROR 								= 'Error';
  	MSG_FILE_ALREADY_PROCESSED 				= 'El archivo %s ya fue procesado' + crlf + 'Desea reprocesarlo ?';
    MSG_ERRROR_LATE_DAYPASS_FILE_IS_EMPTY	= 'El archivo de pases diarios est� vac�o';
  	MSG_ERROR_CANNOT_OPEN_LATE_DAYPASS_FILE	= 'No se puede abrir el archivo de pases diarios tard�os';
  	MSG_PROCESS_SUCCEDED 					= 'El proceso finaliz� con �xito';
  	MSG_PROCESS_ENDED_WITH_ERRORS			= 'El proceso finaliz� con errores';
  	MSG_PROCESS_COULD_NOT_BE_COMPLETED 		= 'El proceso no se pudo completar';
    MSG_LATE_DAYPASS_SUCCEDED				= 'La importaci�n de Pases Diarios Tard�os finaliz� con �xito';
    MSG_LATE_DAYPASS_FILE					= 'Archivo de Pase Diario Tard�o';
begin
	// Verifica que el Archivo no haya sido procesado
	if  VerificarArchivoProcesado(DMConnections.BaseCAC, edOrigenPDT.Text) then begin
        if MsgBox (Format (MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(edOrigenPDT.Text)]),
        	Caption, MB_ICONWARNING or MB_YESNO) <> IDYES then Exit;
    end;

    // Creo el stringlist
    FPasesDiariosTardios := TStringList.Create;

	// Deshabilita y Habilita los botones
	btnCancelarPDT.Enabled := True;
	btnProcesarPDT.Enabled := False;
	btnCerrar.Enabled := False;
	btnAbrirArchivoPDT.Enabled := False;
	edOrigenPDT.Enabled := False;
	FDetenerImportacion := False;
    FProcesando := True;
    tsArchivoPatentes.Enabled := False;
    tsMaestroPases.Enabled := False;
    tsCambioFecha.Enabled := False;
    tsRecibirPasesDiarios.Enabled := False;

    try
		try
            //Leo el archivo de pases diarios tardios
			FPasesDiariosTardios.text:= FileToString(edOrigenPDT.Text);

            //Elimino las lineas en blanco
            EliminarLineasEnBlanco(FPasesDiariosTardios);

			//Verifica si el Archivo Contiene alguna linea
			if (FPasesDiariosTardios.Count = 0) then begin

                //Informo que el archivo esta vacio
				MsgBox(MSG_ERRROR_LATE_DAYPASS_FILE_IS_EMPTY, Caption, MB_ICONERROR)

			end else begin
            	if VerificarCantidadDeRegistros(FPasesDiariosTardios, MSG_LATE_DAYPASS_FILE) and
                	AnalizarArchivoPasesDiariosTardios then begin
                    	if RegistrarOperacion(RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO_TARDIO, ExtractFileName(edOrigenPDT.Text))
                        	and CargarPasesDiariosTardios then begin

                                //Verifico si hubo errores
                                if not FErrors then begin
                                    //Informo que el proceso finalizo con exito
                                    MsgBox(MSG_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION);
                                end else begin
                                    //Informo que el proceso finalizo con errores
                                    MsgBox(MSG_PROCESS_ENDED_WITH_ERRORS, Caption, MB_ICONINFORMATION);
                                end;
                                //Obtengo la cantidad de errores
                                ObtenerCantidadErroresInterfaz(FCodigoOperacion, FCantidadErrores);
                                //Actualizo el log al Final
                                ActualizarLog(FCantidadErrores);
                                //chequeo si hubo errores
            						        CheckForError;
                                //Muestro el Reporte de Finalizacion del Proceso
                                GenerarReportedeFinalizacion(FCodigoOperacion);
                                //Muevo el archivo procesado a otro directorio
                                MoverArchivoProcesado(Caption, edOrigenPDT.Text, FServipag_Directorio_Procesados + ExtractFileName(edOrigenPDT.Text));

                        end else begin

                                if FDetenerImportacion = True then begin
                                  //registro que el proceso fue cancelado
                                  RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacion);
                                end;

                                //Informo que el proceso no se pudo completar
                    			      MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);

                        end;
                end;
			end;
		except
			on e: Exception do begin
				MsgBoxErr(MSG_ERROR_CANNOT_OPEN_LATE_DAYPASS_FILE, e.Message, 'Error', MB_ICONERROR);
			end;
		end;
    finally
        //Libero el stringlist
    	FPasesDiariosTardios.Free;
		//Deshabilita y Habilita los botones
		btnCancelarPDT.Enabled := False;
		btnProcesarPDT.Enabled := False;
		btnCerrar.Enabled := True;
		btnAbrirArchivoPDT.Enabled := True;
		edOrigenPDT.Enabled := True;
    	tsArchivoPatentes.Enabled := True;
    	tsMaestroPases.Enabled := True;
    	tsCambioFecha.Enabled := True;
    	tsRecibirPasesDiarios.Enabled := True;
    	FErrors:= False;
    	FProcesando := False;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: AnalizarArchivoPasesDiariosTardios
  Author:    flamas
  Date Created: 17/03/2005
  Description:	Analiza el Archivo de Pases Diarios Tard�os
  Parameters: None
  Return Value: boolean

  Revision 1
  Author: mbecerra
  Date: 26-Mayo-2008
  Description:  Si hay errores en el parseo de alguna l�nea, el sistema
  				contin�a procesando el resto, sin detener el proceso completo.
-----------------------------------------------------------------------------}
function TfrmDayPass.AnalizarArchivoPasesDiariosTardios: boolean;
resourcestring
	MSG_ANALIZING_LATE_DAYPASS_FILE		= 'Analizando Archivo Pases Diarios Tard�os - Cantidad de lineas : %d';
    MSG_LATE_DAYPASS_FILE_HAS_ERRORS 	= 'El archivo de pases diarios tard�os contiene errores'#10#13'L�nea : %d';
    MSG_ERROR= 'Error';
var
	nNroLineaScript, nLineasScript :integer;
    FDayPassRecord 	: TLateDayPassRecord;
	sParseError		: string;
    sErrorMsg   	: string;
begin
	Screen.Cursor := crHourglass;

  	nLineasScript := FPasesDiariosTardios.Count - 1;
    lblReferenciaPDT.Caption := Format(MSG_ANALIZING_LATE_DAYPASS_FILE, [nLineasScript]);
    pbProgresoPDT.Position := 0;
	pbProgresoPDT.Max := FPasesDiariosTardios.Count - 1;
  	pnlAvancePDT.Visible := True;

    nNroLineaScript := 0;

    while ( nNroLineaScript < nLineasScript ) and ( not FDetenerImportacion ) {and
          (	sErrorMsg = '' ) }do begin

    	if not ParseLateDayPassLine( FPasesDiariosTardios[nNroLineaScript], FDayPassRecord, sParseError ) then begin
        	sErrorMsg := Format( MSG_LATE_DAYPASS_FILE_HAS_ERRORS, [nNroLineaScript] ) + ' ' + sParseError;
            //MsgBox(sErrorMsg, Caption, MB_ICONERROR);            Revision 1
            FErrorLog.Add(sErrorMsg);
            Screen.Cursor := crDefault;
            result := False;
            Exit;
        end;

		Inc( nNroLineaScript );
    	pbProgresoPDT.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

    pbProgresoPDT.Position := 0;
	Application.ProcessMessages;
    
	result := ( not FDetenerImportacion ) and ( sErrorMsg = '' );
	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: ParseLateDayPassLine
  Author:    flamas
  Date Created: 17/03/2005
  Description: Parsea un al�nea del Pase diario Tard�o
  Parameters: sline : string; var LateDayPassRecord : TLateDayPassRecord; var sParseError : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function  TfrmDayPass.ParseLateDayPassLine(sline : string; var LateDayPassRecord : TLateDayPassRecord; var sParseError : string): boolean;
resourcestring
    MSG_PLATE_ERROR 		= 'La patente es inv�lida.';
	MSG_SALES_DATE_ERROR 	= 'La fecha de venta es inv�lida.';
	MSG_AMMOUNT_ERROR 		= 'El monto es inv�lido.';
    MSG_PARSE_ERROR			= 'Error de formato.';
    MSG_FILE_TYPE_ERROR		= 'El tipo de archivo a procesar no corresponde con el archivo seleccionado';
begin
	result := False;
    with LateDayPassRecord do begin
		try
			sParseError 		:= MSG_PLATE_ERROR;
			Patente				:= Trim(Copy( sline, 1, 10)); if (Patente = '') then Exit;
    		sParseError 		:= MSG_PARSE_ERROR;
			CodigoMaterial 		:= Trim(Copy( sline,  11,  4));
    		sParseError 		:= MSG_AMMOUNT_ERROR;
			ValorPagado			:= StrToIntDef(Trim(Copy( sline,  15, 10)), 0);
			sParseError 		:= MSG_SALES_DATE_ERROR;
            FechaVenta 			:= EncodeDate(	StrToInt(Copy(sline, 25, 4)),
            									StrToInt(Copy(sline, 29, 2)),
                                                StrToInt(Copy(sline, 31, 2)));
            sParseError := '';
            result := True;
        except
        	on exception do Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarPasesDiariosTardios
  Author:    flamas
  Date Created: 17/03/2005
  Description: Carga los Pases Diarios Tard�os
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  05/11/2007
  Muestro un mensaje descriptivo con numero de linea y linea del problema.
  Registro en log si se produce una excepci�n.
-----------------------------------------------------------------------------}
function TfrmDayPass.CargarPasesDiariosTardios : boolean;
resourcestring
    MSG_PROCESSING_LATE_DAYPASS_FILE		= 'Procesando archivo de Pases Diarios Tard�os - Cantidad de lineas : %d';
    MSG_ERROR_PROCESSING_LATE_DAYPASS_FILE	= 'Error procesando archivo de Pases Diarios Tard�os - Linea: %d';
	MSG_PROCESS_SUCCEDED 					= 'El proceso finaliz� con �xito';
	MSG_PROCESS_ENDED_WITH_ERRORS			= 'El proceso finaliz� con errores';
	MSG_ERROR								= 'Error';
const
  STR_LINE =  'Linea: ';
var
	nNroLineaScript, nLineasScript : integer;
    FDayPassRecord 		: TLateDayPassRecord;
	sParseError			: string;
    sErrorMsg   		: string;
    sDescripcionError 	: string;
begin
	Screen.Cursor := crHourglass;

	nLineasScript := FPasesDiariosTardios.Count - 1;
    lblReferenciaPDT.Caption := Format(MSG_PROCESSING_LATE_DAYPASS_FILE, [nLineasScript]);
    pbProgresoPDT.Position 	:= 0;
    pbProgresoPDT.Max 		:= FPasesDiariosTardios.Count - 1;
    pnlAvancePDT.Visible 	:= True;

	FErrors:= False;
    nNroLineaScript := 0;

    while ( nNroLineaScript < nLineasScript ) and
    	  ( not FDetenerImportacion ) and
          (	sErrorMsg = '' ) do begin

    	if ParseLateDayPassLine( FPasesDiariosTardios[nNroLineaScript], FDayPassRecord, sParseError ) then begin
        	with FDayPassRecord, spAgregarDayPassTardio, Parameters do begin
				try
                    ParamByName( '@CodigoOperacionInterfase' ).Value	:= FCodigoOperacion;
                    ParamByName( '@Patente' ).Value						:= Patente;
                    ParamByName( '@FechaVenta' ).Value					:= FechaVenta;
                    ParamByName( '@CodigoMaterial' ).Value				:= CodigoMaterial;
                    ParamByName( '@Importe' ).Value						:= ValorPagado * 100;
                    spAgregarDayPassTardio.ExecProc;

                	sDescripcionError := Trim( VarToStr( ParamByName( '@DescripcionError' ).Value ));
                	if ( sDescripcionError <> '' ) then begin
                    	FErrorLog.Add( StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]));
                        FErrors:= True;
                    end;
				except
                	on e: exception do begin
                        //Registro la excepcion en el log de operaciones
                        AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, MSG_ERROR_PROCESSING_LATE_DAYPASS_FILE + ' - ' + STR_LINE + IntToStr(nNroLineaScript + 1) + ' - ' + FPasesDiariosTardios[nNroLineaScript] + ' - ' + E.Message);
                        //Informo que se produjo un error al procesar
                        MsgBoxErr(MSG_ERROR_PROCESSING_LATE_DAYPASS_FILE + CRLF + STR_LINE + IntToStr(nNroLineaScript + 1) + CRLF + FPasesDiariosTardios[nNroLineaScript], e.Message, MSG_ERROR, MB_ICONERROR);
                        sErrorMsg := MSG_ERROR_PROCESSING_LATE_DAYPASS_FILE;
                        FErrors:= True;
					end;
              	end;
            end;
        end;

        Inc( nNroLineaScript );
		pbProgresoPDT.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

    lblReferenciaPDT.Caption := MSG_PROCESS_SUCCEDED;
    pbProgresoPDT.Position := 0;

	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( sErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoCFClick
  Author:    flamas
  Date Created: 20/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnAbrirArchivoCFClick(Sender: TObject);
resourcestring
    MSG_ERROR 							= 'Error';
	MSG_ERROR_FILE_DOES_NOT_EXIST 		= 'El archivo %s no existe';
	MSG_ERROR_SALES_FILE_DOES_NOT_EXIST = 'El archivo de ventas %s no existe';
    MSG_WISH_TO_PROCESS_ANYWAY			= '� Desea procesar s�lo el archivo de usos ?';
    DAYPASS_FILES_FILTER				= 'Cambio de Fecha|' + CAMBIO_FECHA_NAME_PREFIX + '*.TXT';
begin
    OpenDialog.InitialDir := FServipag_Directorio_Entrada;
	OpenDialog.FileName := '';
    OpenDialog.Filter := DAYPASS_FILES_FILTER;
    if OpenDialog.Execute then begin
        edOrigenCF.text:=UpperCase( OpenDialog.filename );

        if not FileExists( edOrigenCF.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigenCF.Text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

		// Obtiene el nombre del Archivo de Ventas
    	FCambioFechaFileName := UpperCase(edOrigenCF.Text);
    end;

    btnProcesarCF.Enabled := True
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarCFClick
  Author:    flamas
  Date Created: 20/04/2005
  Description: Procesa el archivo de Cambios de Fecha
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  11/10/07
  Ahora se registra en el log que el usuario cancelo el proceso
-----------------------------------------------------------------------------
  Revision 2
  lgisuk
  05/11/07
  Ahora muestro un mensaje descriptivo con numero de linea y linea del problema.
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnProcesarCFClick(Sender: TObject);
resourcestring
    MSG_ERROR 								= 'Error';
	MSG_FILE_ALREADY_PROCESSED 				= 'El archivo %s ya fue procesado' + crlf + 'Desea reprocesarlo ?';
    MSG_ERRROR_DATE_CHANGES_FILE_IS_EMPTY 	= 'El archivo de cambios de fecha de pases diarios est� vac�o';
	MSG_ERROR_CANNOT_OPEN_DATE_CHANGES_FILE = 'No se puede abrir el archivo de cambios de fecha de pases diarios';
	MSG_PROCESS_SUCCEDED 					= 'El proceso finaliz� con �xito';
	MSG_PROCESS_ENDED_WITH_ERRORS			= 'El proceso finaliz� con errores';
	MSG_PROCESS_COULD_NOT_BE_COMPLETED 		= 'El proceso no se pudo completar';
	MSG_ANALIZING_DATE_CHANGES_FILE			= 'Analizando Archivo de Cambios de Fecha de Pase Diario - Cantidad de lineas : %d';
    MSG_DATE_CHANGES_FILE_HAS_ERRORS 		= 'El archivo de cambios de fecha de pases diarios contiene errores'#10#13'L�nea : %d';
    MSG_PROCESSING_DATE_CHANGES_FILE   		= 'Procesando archivo de Cambios de Fecha de Pases Diarios - Cantidad de lineas : %d';
    MSG_ERROR_PROCESSING_DATE_CHANGES_FILE	= 'Error procesando archivo de Pases Diarios ';
    MSG_DATE_CHANGES_SUCCEDED				= 'La importaci�n de Cambios de Fecha de Pases Diarios finaliz� con �xito';
    MSG_DATE_CHANGES_FILE					= 'Archivo de Cambios de Fecha de Pase Diario';
begin
	// Verifica que el Archivo no haya sido procesado
	if  VerificarArchivoProcesado(DMConnections.BaseCAC, edOrigenCF.Text) then begin
        if MsgBox (Format (MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(edOrigenCF.Text)]),
        	Caption, MB_ICONWARNING or MB_YESNO) <> IDYES then Exit;
    end;

    // Creo el StringList
    FPasesDiariosCambioFecha := TStringList.Create;

	// Deshabilita y Habilita los botones
	btnCancelarCF.Enabled := True;
	btnProcesarCF.Enabled := False;
	btnCerrar.Enabled := False;
	btnAbrirArchivoCF.Enabled := False;
	edOrigenCF.Enabled := False;
	FDetenerImportacion := False;
    FProcesando := True;
    tsArchivoPatentes.Enabled := False;
    tsMaestroPases.Enabled := False;
	tsRecibirPasesDiarios.Enabled := False;
    //tsPasesDiariosTardios.Enabled := False;

    try
		try
            //leo el archivo de cambios de fecha
			FPasesDiariosCambioFecha.text:= FileToString(FCambioFechaFileName);

            //Elimino las lineas en blanco
            EliminarLineasEnBlanco(FPasesDiariosCambioFecha);

			// Verifica si el Archivo Contiene alguna linea
			if (FPasesDiariosCambioFecha.Count = 0) then begin

                //Informo que el archivo esta vacio
				MsgBox(MSG_ERRROR_DATE_CHANGES_FILE_IS_EMPTY, Caption, MB_ICONERROR);

			end else begin
            	if VerificarCantidadDeRegistros(FPasesDiariosCambioFecha, MSG_DATE_CHANGES_FILE) and
                	AnalizarArchivoPasesDiarios(FPasesDiariosCambioFecha, MSG_ANALIZING_DATE_CHANGES_FILE, MSG_DATE_CHANGES_FILE_HAS_ERRORS, 'C') then begin

                    	if 	RegistrarOperacion(RO_MOD_INTERFAZ_ENTRANTE_DAYPASS_CAMBIO_FECHA, ExtractFileName(edOrigenCF.Text)) and
                           CargarPasesDiarios(FPasesDiariosCambioFecha, MSG_PROCESSING_DATE_CHANGES_FILE, MSG_ERROR_PROCESSING_DATE_CHANGES_FILE, MSG_DATE_CHANGES_SUCCEDED, 'C') then begin

                                //Verifico si hubo errores
                                if not FErrors then begin
                                    //Informo que finalizo con exito
                                    MsgBox(MSG_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION);
                                end else begin
                                    //Informo que finalizo con errores
                                    MsgBox(MSG_PROCESS_ENDED_WITH_ERRORS, Caption, MB_ICONINFORMATION);
                                end;
                                //Obtengo la cantidad de errores
                                ObtenerCantidadErroresInterfaz(FCodigoOperacion, FCantidadErrores);
                                //Actualizo el log al Final
                                ActualizarLog(FCantidadErrores);
                                //Muestra los Errores
        						CheckForError;
                                //Muestro el Reporte de Finalizacion del Proceso
                                GenerarReportedeFinalizacion(FCodigoOperacion);
                                //Muevo el archivo a otro directorio
                                MoverArchivoProcesado(Caption, edOrigenCF.Text, FServipag_Directorio_Procesados + ExtractFileName(edOrigenCF.Text));

                        end else begin

                           if FDetenerImportacion then begin
                            //registro que el proceso fue cancelado
                            RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacion);
                           end;

                                //Informo que la operacion no se pudo completar
                    			MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
                                
                        end;
                end;
			end;
		except
			on e: Exception do begin
				MsgBoxErr(MSG_ERROR_CANNOT_OPEN_DATE_CHANGES_FILE, e.Message, 'Error', MB_ICONERROR);
			end;
		end;
    finally
        //Libero el StringList
    	FPasesDiariosCambioFecha.Free;
		// Deshabilita y Habilita los botones
		btnCancelarCF.Enabled := False;
		btnProcesarCF.Enabled := False;
		btnCerrar.Enabled := True;
		btnAbrirArchivoCF.Enabled := True;
		edOrigenCF.Enabled := True;
    	tsArchivoPatentes.Enabled := True;
    	tsMaestroPases.Enabled := True;
		//tsPasesDiariosTardios.Enabled := True;
		tsRecibirPasesDiarios.Enabled := True;
    	FErrors:= False;
    	FProcesando := False;
    end;
end;


{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}
{  FUNCIONES Y PROCEDIMIENTOS IMPORTACION DE MAESTRO PASE DIARIO			   }
{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}

{-----------------------------------------------------------------------------
  Function Name: ObtenerOtrosArchivosPaseDiario
  Author:    flamas
  Date Created: 10/05/2005
  Description: Levanta un Archivo de Pases Diarios
  Parameters: sFileName : string
  Return Value: string
-----------------------------------------------------------------------------}
function TfrmDayPass.ObtenerOtrosArchivosPaseDiario(sFileName : string ): string;
var
	sArchivo 	: string;
    Hoy		 	: TDateTime;
    DiaAnterior	: TDateTime;
begin
	result 		:= '';
    Hoy 		:= NowBase(DMConnections.BaseCAC);

	  // Busca para procesar el archivo del D�a y si no el del d�a Anterior
	  //sArchivo 	:= FServipag_Directorio_Entrada + sFileName + FormatDateTime('yymmdd', Hoy) + '01' + '.TXT';             //SS_1256_NDR_20150505
	  sArchivo 	:=  FServipag_Directorio_Entrada +                                                                         //SS_1256_NDR_20150505
                  sFileName +                                                                                            //SS_1256_NDR_20150505
                  FormatDateTime('yymmdd', Hoy) +                                                                        //SS_1256_NDR_20150505
                  '0'+Trim(IntToStr(ObtenerCodigoConcesionariaNativa)) +                                                 //SS_1256_NDR_20150505
                  '.TXT';                                                                                                //SS_1256_NDR_20150505
    if FileExists(sArchivo) then result := sArchivo
    else begin
		// Busca el d�a anterior
    	DiaAnterior := iif(DayOfTheWeek(Hoy) = 1, Hoy-2, Hoy-1);
		  //sArchivo := FServipag_Directorio_Entrada + sFileName + FormatDateTime('yymmdd', DiaAnterior) + '01' + '.TXT';   //SS_1256_NDR_20150505
		  sArchivo := FServipag_Directorio_Entrada +                                                                        //SS_1256_NDR_20150505
                  sFileName +                                                                                           //SS_1256_NDR_20150505
                  FormatDateTime('yymmdd', DiaAnterior) +                                                               //SS_1256_NDR_20150505
                  '0'+Trim(IntToStr(ObtenerCodigoConcesionariaNativa)) +                                                //SS_1256_NDR_20150505
                  '.TXT';                                                                                               //SS_1256_NDR_20150505
    	if FileExists(sArchivo) then result := sArchivo;
    end;

	btnProcesarPD.Enabled := (result <> '');
end;

{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoMPDClick
  Author:    flamas
  Date Created: 09/05/2005
  Description: Abre el archivo de Maestro de Pases Diarios
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnAbrirArchivoMPDClick(Sender: TObject);
resourcestring
    MSG_ERROR 							= 'Error';
	MSG_ERROR_FILE_DOES_NOT_EXIST 		= 'El archivo %s no existe';
    MSG_WISH_TO_PROCESS_ANYWAY			= '� Desea procesar s�lo el archivo de usos ?';
    DAYPASS_FILES_FILTER				= 'Maestro Pases Diarios|' + MAESTRO_NAME_PREFIX + '*.TXT';
begin
    OpenDialog.InitialDir := FServipag_Directorio_Entrada;
	OpenDialog.FileName := '';
    OpenDialog.Filter := DAYPASS_FILES_FILTER;
    if OpenDialog.Execute then begin
        edOrigenMPD.text:=UpperCase( OpenDialog.filename );

        if not FileExists( edOrigenMPD.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigenMPD.Text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;
    end;

    btnProcesarMPD.Enabled := True
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarMPDClick
  Author:    flamas
  Date Created: 09/05/2005
  Description: Procesamiento del archivo Maestro de Pases Diarios
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  11/10/07
  Ahora se registra en el log que el usuario cancelo el proceso
-----------------------------------------------------------------------------
  Revision 2
  lgisuk
  05/11/07
  Ahora Muestro un mensaje descriptivo con numero de linea y linea del problema.
  Registro en log si se produce una excepci�n.
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnProcesarMPDClick(Sender: TObject);
resourcestring
    MSG_ERROR 								= 'Error';
	MSG_FILE_ALREADY_PROCESSED 				= 'El archivo %s ya fue procesado' + crlf + 'Desea reprocesarlo ?';
    MSG_ERRROR_DAYPASS_FILE_IS_EMPTY		= 'El archivo de maestro de pases diarios est� vac�o';
	MSG_ERROR_CANNOT_OPEN_DAYPASS_FILE 		= 'No se puede abrir el archivo de maestro de pases diarios';
	MSG_PROCESS_SUCCEDED 					= 'El proceso finaliz� con �xito';
	MSG_PROCESS_ENDED_WITH_ERRORS			= 'El proceso finaliz� con errores';
	MSG_PROCESS_COULD_NOT_BE_COMPLETED 		= 'El proceso no se pudo completar';
	MSG_ANALIZING_DAYPASS_FILE				= 'Analizando archivo Maestro de Pases Diarios - Cantidad de lineas : %d';
    MSG_DAYPASS_FILE_HAS_ERRORS 			= 'El archivo de Maestro de Pases Diarios contiene errores'#10#13'L�nea : %d';
    MSG_PROCESSING_DAYPASS_FILE				= 'Procesando archivo Maestro de Pases Diarios - Cantidad de lineas : %d';
    MSG_ERROR_PROCESSING_DAYPASS_FILE		= 'Error procesando archivo Maestro de Pases Diarios ';
    MSG_DAYPASS_SUCCEDED					= 'La importaci�n del Maestro de Pases Diarios finaliz� con �xito';
    MSG_DAYPASS_FILE						= 'Archivo Maestro de Pase Diario';
    MSG_DAYPASS_SALES_FILE					= 'Archivo de Ventas Pase Diario';
begin
	// Verifica que el Archivo no haya sido procesado
	if  VerificarArchivoProcesado(DMConnections.BaseCAC, edOrigenMPD.Text) then begin
        if MsgBox (Format (MSG_FILE_ALREADY_PROCESSED
        , [ExtractFileName(edOrigenMPD.Text)]),
        	Caption, MB_ICONWARNING or MB_YESNO) <> IDYES then Exit;
    end;

    // Crea el StringList
    FMaestroPasesDiarios := TStringList.Create;

	// Deshabilita y Habilita los botones
	btnCancelarMPD.Enabled := True;
	btnProcesarMPD.Enabled := False;
	btnCerrar.Enabled := False;
	btnAbrirArchivoMPD.Enabled := False;
	edOrigenMPD.Enabled := False;
	FDetenerImportacion := False;
    FProcesando := True;
    tsArchivoPatentes.Enabled := False;
    tsRecibirPasesDiarios.Enabled := False;
    tsCambioFecha.Enabled := False;
    //tsPasesDiariosTardios.Enabled := False;

    try
		try
            //Lee el archivo de Maestro de pases diarios
			FMaestroPasesDiarios.text:= FileToString(edOrigenMPD.Text);

            //Elimina las lineas en blanco recibidas
            EliminarLineasEnBlanco(FMaestroPasesDiarios);

			//Verifica si el Archivo Contiene alguna linea
			if (FMaestroPasesDiarios.Count = 0) then begin

                //Informa que el archivo esta Vacio
				MsgBox(MSG_ERRROR_DAYPASS_FILE_IS_EMPTY, Caption, MB_ICONERROR);

			end else begin

            	if VerificarCantidadDeRegistrosMaestro(FMaestroPasesDiarios, MSG_DAYPASS_FILE) and
                	AnalizarArchivoMaestroPasesDiarios( FMaestroPasesDiarios, MSG_ANALIZING_DAYPASS_FILE, MSG_DAYPASS_FILE_HAS_ERRORS) then begin
                    	if 	RegistrarOperacion(RO_MOD_INTERFAZ_ENTRANTE_MAESTRO_PASE_DIARIO, ExtractFileName(edOrigenMPD.Text)) and
                        	    CargarMaestroPasesDiarios(FMaestroPasesDiarios, MSG_PROCESSING_DAYPASS_FILE, MSG_ERROR_PROCESSING_DAYPASS_FILE, MSG_DAYPASS_SUCCEDED) then begin

                                    //Verifico si hubo errores
                                    if not FErrors then begin
                                        //Muestro un cartel que finalizo con exito
                                        MsgBox(MSG_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION);
                                    end else begin
                                        //Muestro un cartel que finalizo con error
                                        MsgBox(MSG_PROCESS_ENDED_WITH_ERRORS, Caption, MB_ICONINFORMATION);
                                    end;
                                    //Obtengo la cantidad de errores
                                    ObtenerCantidadErroresInterfaz(FCodigoOperacion, FCantidadErrores);
                                    //Actualizo el log al Final
                                    ActualizarLog(FCantidadErrores);
                                    //Muestra los Errores
            						CheckForError;
                                    //Muestro el Reporte de Finalizacion del Proceso
                                    GenerarReportedeFinalizacion(FCodigoOperacion);
                                    //Muevo el archivo procesado a otro directorio
                                    MoverArchivoProcesado(Caption, edOrigenMPD.Text, FServipag_Directorio_Procesados + ExtractFileName(edOrigenMPD.Text));

                        end else begin

                            if FDetenerImportacion then begin
                              //registro que el proceso fue cancelado
                              RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacion);
                            end;

                            //Informo que la operacion no se pudo completar
                            MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);

                        end;
                end;
			end;
		except
			on e: Exception do begin
				MsgBoxErr(MSG_ERROR_CANNOT_OPEN_DAYPASS_FILE, e.Message, 'Error', MB_ICONERROR);
			end;
		end;
    finally
        //Libero el stringlist
    	FMaestroPasesDiarios.Free;
		// Deshabilita y Habilita los botones
		btnCancelarMPD.Enabled := False;
		btnProcesarMPD.Enabled := False;
		btnCerrar.Enabled := True;
		btnAbrirArchivoMPD.Enabled := True;
		edOrigenMPD.Enabled := True;
    	tsArchivoPatentes.Enabled := True;
    	tsRecibirPasesDiarios.Enabled := True;
    	tsCambioFecha.Enabled := True;
		//tsPasesDiariosTardios.Enabled := True;
    	FErrors:= False;
    	FProcesando := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: VerificarCantidadDeRegistrosPD
  Author:    flamas
  Date Created: 18/02/2005
  Description: Verifica que la cantidad de registros coincida con el
  				registro de control
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmDayPass.VerificarCantidadDeRegistrosMaestro(FPasesDiarios : TStringList; sArchivo : string) : boolean;
resourcestring
	MSG_ERROR						= 'Error';
	MSG_INVALID_RECORD_NUMBER 		= 'El n�mero de registros del %s ' +CRLF+
    									'no coincide con el registro de control';
    MSG_VERIFYING_CONTROL_REGISTER 	= 'Verficando registro de control del ';
var
	nLen : integer;
begin
	Result 	:= True;
	lblReferenciaMPD.Caption := MSG_VERIFYING_CONTROL_REGISTER + sArchivo;
	nLen 	:= FPasesDiarios.Count - 1;
	if (StrToIntDef(Copy(FPasesDiarios[nLen], 25, 4),0) <> nLen) then begin
    	MsgBox(Format(MSG_INVALID_RECORD_NUMBER, [sArchivo]), 'Error', MB_ICONERROR);
		Result := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AnalizarArchivoMaestroPasesDiarios
  Author:    flamas
  Date Created: 09/05/2005
  Description:
  Parameters: FPasesDiarios : TStringList; sMsgAnalizando, sMsgTieneErrores
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfrmDayPass.AnalizarArchivoMaestroPasesDiarios(FPasesDiarios : TStringList; sMsgAnalizando, sMsgTieneErrores : string): boolean;
resourcestring
    MSG_ERROR= 'Error';
var
	nNroLineaScript, nLineasScript :integer;
    FMasterRecord 	: TMasterDayPassRecord;
	sParseError		: string;
    sErrorMsg   	: string;
begin
	Screen.Cursor := crHourglass;

	nLineasScript := FPasesDiarios.Count - 1;
    lblReferenciaMPD.Caption := Format(sMsgAnalizando, [nLineasScript]);
    pbProgresoMPD.Position := 0;
    pbProgresoMPD.Max := FPasesDiarios.Count - 1;
    pnlAvanceMPD.Visible := True;

    nNroLineaScript := 0;

    while ( nNroLineaScript < nLineasScript ) and
    	  ( not FDetenerImportacion ) and
          (	sErrorMsg = '' ) do begin

    	if not ParseMasterDayPassLine( FPasesDiarios[nNroLineaScript], FMasterRecord, sParseError ) then begin
        	sErrorMsg := Format( sMsgTieneErrores, [nNroLineaScript] ) + #10#13 + sParseError;
            MsgBox(sErrorMsg, Caption, MB_ICONERROR);
            Screen.Cursor := crDefault;
            result := False;
            Exit;
        end;

		Inc( nNroLineaScript );
		pbProgresoMPD.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

	pbProgresoMPD.Position := 0;
	Application.ProcessMessages;

	result := ( not FDetenerImportacion ) and ( sErrorMsg = '' );
	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: ParseMasterDayPassLine
  Author:    flamas
  Date Created: 09/05/2005
  Description: Parsea una l�nea del Maestro de DayPass 
  Parameters: sLine : string; var MasterRecord : TDayPassRecord; var sParseError : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfrmDayPass.ParseMasterDayPassLine(sLine : string; var MasterRecord : TMasterDayPassRecord; var sParseError : string) : boolean;
resourcestring
    MSG_SERIAL_NUMBER_ERROR = 'El n�mero de serie es inv�lido.';
	MSG_START_DATE_ERROR 	= 'La fecha de inicio de vigencia es inv�lida.';
	MSG_END_DATE_ERROR 		= 'La fecha de fin de vigencia es inv�lida.';
	MSG_AMOUNT_ERROR 		= 'El monto es inv�lido.';
    MSG_PARSE_ERROR			= 'Error de formato.';
begin
	result := False;
    with MasterRecord do begin
		try
			sParseError 		:= MSG_SERIAL_NUMBER_ERROR;
 			NumeroSerie			:= Trim(Copy(sline, 1, 18));
			sParseError 		:= MSG_START_DATE_ERROR;
			ValidezDesde		:= EncodeDate(	StrToInt(Copy(sline,61,4)),
            									StrToInt(Copy(sline,65,2)),
                                        		StrToInt(Copy(sline,67,2)));
			sParseError 		:= MSG_END_DATE_ERROR;
			ValidezHasta		:= EncodeDate(	StrToInt(Copy(sline,69,4)),
            									StrToInt(Copy(sline,73,2)),
                                        		StrToInt(Copy(sline,75,2)));
    		sParseError 		:= MSG_AMOUNT_ERROR;
			Monto				:= StrToInt(Trim(Copy( sline, 38, 18)));
    		sParseError 		:= MSG_PARSE_ERROR;
			Pin					:= Trim(Copy( sline,  29,  5));
			CodigoMaterial 		:= Trim(Copy( sline,  34,  4));
            sParseError := '';
            result := True;
        except
        	on exception do Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarMaestroPasesDiarios
  Author:    flamas
  Date Created: 09/05/2005
  Description: Carga el Maestro de Pases Diarios
  Parameters: FPasesDiarios : TStringList; sMsgProcesando, sMsgTieneErrores, sMsgProcesoOK : string
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  05/11/2007
  Muestro un mensaje descriptivo con numero de linea y linea del problema.
  Registro en log si se produce una excepci�n.
-----------------------------------------------------------------------------}
function TfrmDayPass.CargarMaestroPasesDiarios(FPasesDiarios : TStringList; sMsgProcesando, sMsgTieneErrores, sMsgProcesoOK : string) : boolean;
resourcestring
	MSG_ERROR = 'Error';
const
  STR_LINE =  'Linea: ';
var
	nNroLineaScript, nLineasScript : integer;
    FDayPassRecord 		: TMasterDayPassRecord;
	sParseError			: string;
    sErrorMsg   		: string;
    sDescripcionError 	: string;
begin
	Screen.Cursor := crHourglass;

	nLineasScript := FPasesDiarios.Count - 1;
    lblReferenciaMPD.Caption := Format(sMsgProcesando, [nLineasScript]);
    pbProgresoMPD.Position 	:= 0;
    pbProgresoMPD.Max 		:= FPasesDiarios.Count - 1;
    pnlAvanceMPD.Visible 	:= True;

	FErrors:= False;
    nNroLineaScript := 0;

    while ( nNroLineaScript < nLineasScript ) and
    	  ( not FDetenerImportacion ) and
          (	sErrorMsg = '' ) do begin

    	if ParseMasterDayPassLine( FPasesDiarios[nNroLineaScript], FDayPassRecord, sParseError ) then begin
        	with FDayPassRecord, spAgregarDayPass, Parameters do begin
				try
                    ParamByName('@CodigoOperacionInterfase').Value	:= FCodigoOperacion;
                    ParamByName('@NumeroSerie').Value				:= NumeroSerie;
                    ParamByName('@Patente').Value					:= Null;
                    ParamByName('@Pin').Value						:= Pin;
                    ParamByName('@ValidezDesde').Value				:= ValidezDesde;
                    ParamByName('@ValidezHasta').Value				:= ValidezHasta;
                    ParamByName('@Monto').Value						:= Monto;
                    ParamByName('@FechaVenta').Value				:= NowBase(DMConnections.BaseCAC);
                    ParamByName('@FechaUso').Value					:= Null;
                    ParamByName('@FechaDevolucion' ).Value			:= Null;
                    ParamByName('@CodigoMaterial').Value			:= CodigoMaterial;
                    ParamByName('@Distribuidor').Value				:= Null;
                    ParamByName('@RUTContacto').Value				:= Null;
                    ParamByName('@TelefonoContacto').Value			:= Null;
                    ParamByName('@NombreContacto').Value			:= Null;
                    ParamByName('@TipoArchivo').Value				:= 'M';
                    spAgregarDayPass.ExecProc;

                	sDescripcionError := Trim( VarToStr( ParamByName( '@DescripcionError' ).Value ));
                	if ( sDescripcionError <> '' ) then begin
                    	FErrorLog.Add( StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]));
                        FErrors:= True;
                    end;
				except
                	on e: exception do begin
                        //Registro la excepcion en el log de operaciones
                        //ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacion, sMsgTieneErrores + ' - ' + STR_LINE + IntToStr(nNroLineaScript + 1) + ' - ' + FPasesDiarios[nNroLineaScript] + ' - ' + E.Message);
                        AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, sMsgTieneErrores + ' - ' + STR_LINE + IntToStr(nNroLineaScript + 1) + ' - ' + FPasesDiarios[nNroLineaScript] + ' - ' + E.Message);
                        //Informo que se produjo un error al procesar
                        MsgBoxErr(sMsgTieneErrores + CRLF + STR_LINE + IntToStr(nNroLineaScript + 1) + CRLF + FPasesDiarios[nNroLineaScript], e.Message, MSG_ERROR, MB_ICONERROR);
                        sErrorMsg := sMsgTieneErrores;
                        FErrors:= True;

					end;
              	end
            end;
        end;

        Inc( nNroLineaScript );
		pbProgresoMPD.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

    lblReferenciaMPD.Caption := sMsgProcesoOK;
    pbProgresoMPD.Position := 0;

	Application.ProcessMessages;

	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( sErrorMsg = '' );
end;

{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}
{  FUNCIONES Y PROCEDIMIENTOS IMPORTACION DE DEVOLUCION DE PASES DIARIOS	   }
{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}

{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoDevClick
  Author:    flamas
  Date Created: 10/05/2005
  Description: Abre un archivo de Devoluci�n de Pase Diario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnAbrirArchivoDevClick(Sender: TObject);
resourcestring
    MSG_ERROR 							= 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST 		= 'El archivo %s no existe';
  	MSG_ERROR_SALES_FILE_DOES_NOT_EXIST = 'El archivo de devoluciones %s no existe';
    DAYPASS_FILES_FILTER				= 'Devoluciones|' + DEVOLUCION_NAME_PREFIX + '*.TXT';
begin
    OpenDialog.InitialDir := FServipag_Directorio_Entrada;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := DAYPASS_FILES_FILTER;
    if OpenDialog.Execute then begin
        edOrigenDev.text:=UpperCase( OpenDialog.filename );

        if not FileExists( edOrigenDev.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigenDev.Text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;
    end;

    btnProcesarDev.Enabled := True
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarDevClick
  Author:    flamas
  Date Created: 10/05/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  11/10/07
  Ahora se registra en el log que el usuario cancelo el proceso
-----------------------------------------------------------------------------
  Revision 2
  lgisuk
  05/11/07
  Ahora Muestro un mensaje descriptivo con numero de linea y linea del problema.
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnProcesarDevClick(Sender: TObject);
resourcestring
    MSG_ERROR 								= 'Error';
  	MSG_FILE_ALREADY_PROCESSED 				= 'El archivo %s ya fue procesado' + crlf + 'Desea reprocesarlo ?';
    MSG_ERRROR_DATE_CHANGES_FILE_IS_EMPTY 	= 'El archivo de cambios de fecha de pases diarios est� vac�o';
  	MSG_ERROR_CANNOT_OPEN_DATE_CHANGES_FILE = 'No se puede abrir el archivo de cambios de fecha de pases diarios';
  	MSG_PROCESS_SUCCEDED 					= 'El proceso finaliz� con �xito';
  	MSG_PROCESS_ENDED_WITH_ERRORS			= 'El proceso finaliz� con errores';
  	MSG_PROCESS_COULD_NOT_BE_COMPLETED 		= 'El proceso no se pudo completar';
  	MSG_ANALIZING_DATE_CHANGES_FILE			= 'Analizando Archivo de Devoluciones de Pase Diario - Cantidad de lineas : %d';
    MSG_DATE_CHANGES_FILE_HAS_ERRORS 		= 'El archivo de Devoluciones de Pases Diarios contiene errores'#10#13'L�nea : %d';
    MSG_PROCESSING_DATE_CHANGES_FILE   		= 'Procesando archivo de Devoluciones de Pases Diarios - Cantidad de lineas : %d';
    MSG_ERROR_PROCESSING_DATE_CHANGES_FILE	= 'Error procesando archivo de Devoluciones de Pases Diarios ';
    MSG_DATE_CHANGES_SUCCEDED				= 'La importaci�n de de Devoluciones de Pases Diarios finaliz� con �xito';
    MSG_DATE_CHANGES_FILE					= 'Archivo de de Devoluciones de Pase Diario';
begin
	// Verifica que el Archivo no haya sido procesado
	if  VerificarArchivoProcesado(DMConnections.BaseCAC, edOrigenDev.Text) then begin
        if MsgBox (Format (MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(edOrigenDev.Text)]),
        	Caption, MB_ICONWARNING or MB_YESNO) <> IDYES then Exit;
    end;

    // Creo el StringList
    FPasesDiariosDevolucion := TStringList.Create;

	// Deshabilita y Habilita los botones
	btnCancelarDev.Enabled := True;
	btnProcesarDev.Enabled := False;
	btnCerrar.Enabled := False;
	btnAbrirArchivoDev.Enabled := False;
	edOrigenDev.Enabled := False;
	FDetenerImportacion := False;
    FProcesando := True;
    tsArchivoPatentes.Enabled := False;
    tsMaestroPases.Enabled := False;
    tsCambioFecha.Enabled := False;
	tsRecibirPasesDiarios.Enabled := False;
    //tsPasesDiariosTardios.Enabled := False;

    try
		try
            //Leo el arhivo de devoluciones
      			FPasesDiariosDevolucion.text:= FileToString(edOrigenDev.Text);

            //Elimino las lineas en blanco
            EliminarLineasEnBlanco(FPasesDiariosDevolucion);

	      		//Verifica si el Archivo Contiene alguna linea
       			if (FPasesDiariosDevolucion.Count = 0) then begin

            //Informo que el archivo esta vacio
	    			MsgBox(MSG_ERRROR_DATE_CHANGES_FILE_IS_EMPTY, Caption, MB_ICONERROR)

			end else begin
            	if VerificarCantidadDeRegistros(FPasesDiariosDevolucion, MSG_DATE_CHANGES_FILE) and
                	AnalizarArchivoPasesDiarios(FPasesDiariosDevolucion, MSG_ANALIZING_DATE_CHANGES_FILE, MSG_DATE_CHANGES_FILE_HAS_ERRORS, 'D') then begin
                    	if 	RegistrarOperacion(RO_MOD_INTERFAZ_ENTRANTE_DAYPASS_DEVOLUCION, ExtractFileName(edOrigenDev.Text)) and
                        	CargarPasesDiarios(FPasesDiariosDevolucion, MSG_PROCESSING_DATE_CHANGES_FILE, MSG_ERROR_PROCESSING_DATE_CHANGES_FILE, MSG_DATE_CHANGES_SUCCEDED, 'D') then begin

                                //Verifico si hubo errores
                                if not FErrors then begin
                                    //Informo que se proceso con exito
                                    MsgBox(MSG_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION);
                                end else begin
                                    //Informo que se proceso con error
                                    MsgBox(MSG_PROCESS_ENDED_WITH_ERRORS, Caption, MB_ICONINFORMATION);
                                end;
                                //Obtengo la cantidad de errores
                                ObtenerCantidadErroresInterfaz(FCodigoOperacion, FCantidadErrores);
                                //Actualizo el log al Final
                                ActualizarLog(FCantidadErrores);
                                //Muestra los Errores
                    						CheckForError;
                                //Muestro el Reporte de Finalizacion del Proceso
                                GenerarReportedeFinalizacion(FCodigoOperacion);
                                //Muevo el archivo a otro directorio
                                MoverArchivoProcesado(Caption, edOrigenDev.Text, FServipag_Directorio_Procesados + ExtractFileName(edOrigenDev.Text));
                                
                        end else begin

                                if FDetenerImportacion then begin

                                    //registro que el proceso fue cancelado
                                    RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacion);
                                end;

                                //Informo que la operaci�n no se pudo completar
                          			MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
                                
                        end;
                end;
			end;
		except
			on e: Exception do begin
				MsgBoxErr(MSG_ERROR_CANNOT_OPEN_DATE_CHANGES_FILE, e.Message, 'Error', MB_ICONERROR);
			end;
		end;
    finally
        // Libero el StringList
      	FPasesDiariosCambioFecha.Free;
    		// Deshabilita y Habilita los botones
    		btnCancelarDev.Enabled := False;
    		btnProcesarDev.Enabled := False;
    		btnCerrar.Enabled := True;
    		btnAbrirArchivoDev.Enabled := True;
    		edOrigenDev.Enabled := True;
       	tsArchivoPatentes.Enabled := True;
      	tsMaestroPases.Enabled := True;
        tsCambioFecha.Enabled := False;
    		//tsPasesDiariosTardios.Enabled := True;
    		tsRecibirPasesDiarios.Enabled := True;
      	FErrors:= False;
      	FProcesando := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: PageControl1Changing
  Author:    flamas
  Date Created: 22/02/2005
  Description: Inhabilita el cambio de p�gina durante el procesamiento
  Parameters: Sender: TObject; var AllowChange: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.PageControl1Changing(Sender: TObject; var AllowChange: Boolean);
begin
  	AllowChange := not FProcesando;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    flamas
  Date Created: 22/02/2005
  Description: Cierra la Ventana si no est� procesando
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.FormClose(Sender: TObject; var Action: TCloseAction);
resourcestring
	MSG_PROCESSING_CANT_CLOSE = 'Se est� procesando la interfaz.' + CRLF + 'Cancele el procesamiento para poder cerrar la ventana.';
begin
  	if FProcesando then begin
      	MsgBox(MSG_PROCESSING_CANT_CLOSE, Caption, MB_ICONWARNING);
        Action := caNone;
    end else Action := caFree;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCerrarClick
  Author:    flamas
  Date Created: 22/02/2005
  Description: Cierra la Ventana
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.btnCerrarClick(Sender: TObject);
begin
  	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    flamas
  Date Created: 22/02/2005
  Description: Libera la memoria
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmDayPass.FormDestroy(Sender: TObject);
begin
    FErrorLog.Free;
end;



end.
