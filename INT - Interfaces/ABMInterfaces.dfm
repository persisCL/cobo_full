object FormABMInterfaces: TFormABMInterfaces
  Left = 196
  Top = 223
  Width = 618
  Height = 440
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Interfaces'
  Color = clBtnFace
  Constraints.MinHeight = 440
  Constraints.MinWidth = 610
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 367
    Width = 610
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Notebook: TNotebook
      Left = 413
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 610
    Height = 180
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'41'#0'Codigo'
      #0'43'#0'Interfaz')
    HScrollBar = True
    RefreshTime = 100
    Table = qry_Interfaces
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnProcess = DBList1Process
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 610
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar, btImprimir]
    OnClose = AbmToolbar1Close
    OnPrint = AbmToolbar1Print
  end
  object PageControl: TPageControl
    Left = 0
    Top = 213
    Width = 610
    Height = 154
    ActivePage = tab_Archivo
    Align = alBottom
    TabOrder = 3
    object tab_General: TTabSheet
      Caption = 'General'
      Enabled = False
      DesignSize = (
        602
        126)
      object Label1: TLabel
        Left = 10
        Top = 10
        Width = 72
        Height = 13
        Caption = 'Descripci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 10
        Top = 59
        Width = 45
        Height = 13
        Caption = 'Detalle:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 10
        Top = 35
        Width = 61
        Height = 13
        Caption = 'Categor'#237'a:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_descripcion: TEdit
        Left = 90
        Top = 10
        Width = 503
        Height = 21
        Color = 16444382
        MaxLength = 60
        TabOrder = 0
      end
      object txt_Detalle: TMemo
        Left = 90
        Top = 59
        Width = 504
        Height = 62
        Anchors = [akLeft, akTop, akRight, akBottom]
        Color = 16444382
        MaxLength = 2000
        ScrollBars = ssVertical
        TabOrder = 2
      end
      object cb_Categoria: TComboBox
        Left = 90
        Top = 35
        Width = 252
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          'Ninguno'
          'Barras'
          'Lineas'
          'Torta')
      end
    end
    object tab_Consulta: TTabSheet
      Caption = 'Consulta'
      Enabled = False
      ImageIndex = 2
      object txt_consulta: TMemo
        Left = 0
        Top = 0
        Width = 602
        Height = 126
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 8000
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object tab_Sistemas: TTabSheet
      Caption = 'Sistemas'
      ImageIndex = 4
      DesignSize = (
        602
        126)
      object ckSistemas: TVariantCheckListBox
        Left = 0
        Top = 0
        Width = 601
        Height = 126
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelWidth = 0
        ItemHeight = 13
        Items = <>
        TabOrder = 0
      end
    end
    object tab_Archivo: TTabSheet
      Caption = 'Archivo'
      Enabled = False
      ImageIndex = 3
      object Label2: TLabel
        Left = 10
        Top = 59
        Width = 24
        Height = 13
        Caption = 'Tipo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 10
        Top = 10
        Width = 48
        Height = 13
        Caption = 'Directorio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object btnDirArchivosImagenes: TSpeedButton
        Left = 567
        Top = 10
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = btnDirArchivosImagenesClick
      end
      object Label4: TLabel
        Left = 10
        Top = 35
        Width = 38
        Height = 13
        Caption = 'Interfaz:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 10
        Top = 83
        Width = 52
        Height = 13
        Caption = 'Separador:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cb_Tipo: TComboBox
        Left = 90
        Top = 59
        Width = 47
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 2
        OnClick = cb_TipoClick
        Items.Strings = (
          'TXT'
          'XML')
      end
      object txt_Directorio: TEdit
        Left = 90
        Top = 10
        Width = 471
        Height = 21
        Color = 16444382
        MaxLength = 60
        TabOrder = 0
      end
      object cb_Interfaz: TComboBox
        Left = 90
        Top = 35
        Width = 63
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          'Entrada'
          'Salida')
      end
      object txt_Separador: TEdit
        Left = 90
        Top = 83
        Width = 23
        Height = 21
        Color = 16444382
        MaxLength = 1
        TabOrder = 3
      end
    end
  end
  object qry_Interfaces: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM Interfaces ORDER BY Descripcion')
    Left = 525
    Top = 59
  end
  object qry_Sistemas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM Sistemas ORDER BY Descripcion')
    Left = 494
    Top = 59
  end
end
