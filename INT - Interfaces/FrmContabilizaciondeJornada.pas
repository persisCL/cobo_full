{-------------------------------------------------------------------------------
 File Name: FrmContabilizaciondeJornada.pas
 Author:    lgisuk
 Date Created: 27/07/2005
 Language: ES-AR
 Description: M�dulo de la interface Contable - Contabilizaci�n de Jornada
--------------------------------------------------------------------------------
 Revision History
--------------------------------------------------------------------------------
 Author: ggomez
 22/08/2005: Agregu� la generaci�n del Asiento de Emisi�n Masiva de Notas de Cobro.
 24/08/2005: Agregu� la generaci�n del Asiento de Venta de Pases Diarios y el
    de Ventas de Boletos de Habilitaci�n Tard�a.
 12/09/2005: Agregu� que muestre una sugerencia cuando se quiere contabilizar
    una jornada de un mes y hay jornadas NO contabilizadas del mes anterior.
 09/11/2005: Agregu� la generaci�n del Asiento de Cobro de Deuda Diaria
    (Pagos en Ventanilla).
 11/11/2005: Agregu� la generaci�n del Asiento de Cambios de Estado de
    Validaci�n de Tr�nsitos.
-------------------------------------------------------------------------------}
unit FrmContabilizaciondeJornada;

interface

uses
  //Contabilizacion de Jornada
  DMConnection,                       //Coneccion a base de datos OP_CAC
  UtilProc,                           //Mensajes
  Util,                               //Stringtofile,padl..
  UtilDB,                             //Rutinas para base de datos
  ComunesInterfaces,                  //Procedimientos y Funciones Comunes a todos los formularios
  ConstParametrosGenerales,           //Obtengo Valores de la Tabla Parametros Generales
  PeaProcs,                           //NowBase
  FrmRptVerificacionInterfazContable, //Reporte de Finalizacion de Proceso
  //General
  DB, ADODB, ComCtrls, StdCtrls, Controls,ExtCtrls, Classes, Windows, Messages, SysUtils,
  Variants,  Graphics,  Forms, Dialogs, Grids, DBGrids,  DPSControls, ListBoxEx,
  DBListEx, StrUtils, UtilRB, ppProd, ppClass, ppReport, ppComm, ppRelatv,
  ppDB, ppTxPipe, ppPrnabl, ppStrtch, ppSubRpt, ppCache, ppBands, ppCtrls,
  Validate, DateEdit, PeaTypes;

type
  TFContabilizaciondeJornada = class(TForm)
    pnlAvance: TPanel;
    pbProgreso: TProgressBar;
    Bevel: TBevel;
    btnProcesar: TButton;
    SalirBTN: TButton;
    Lprocesogeneral: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlcontabilizar: TPanel;
    lblFechaInterfase: TLabel;
    edFecha: TDateEdit;
    SPRegistrarTransitosAContabilizar: TADOStoredProc;
    spMarcarTransitosContabilizados: TADOStoredProc;
    spMarcarTransitosComoNoContabilizados: TADOStoredProc;
    spMarcarComprobantesContabilizados: TADOStoredProc;
    spMarcarComprobantesComoNoContabilizados: TADOStoredProc;
    spMarcarVentasPasesDiariosComoNoContabilizados: TADOStoredProc;
    spMarcarPagosContabilizados: TADOStoredProc;
    SPMarcarPagosComoNoContabilizados: TADOStoredProc;
    lbl_PresioneEscape: TLabel;
    lblMensaje: TLabel;
    lbl_DescripcionPasoGeneral: TLabel;
    SPRegistrarComprobantesAContabilizar: TADOStoredProc;
    SPRegistrarMovimientosCuentasAContabilizar: TADOStoredProc;
    spMarcarAnulacionesComoNoContabilizadas: TADOStoredProc;
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SalirBTNClick(Sender: TObject);
    procedure edFechaChange(Sender: TObject);
  private
    { Private declarations }
    FNumeroProceso : Integer;
    FCancelar : Boolean;
    FErrorGrave : Boolean;
    FFechaDesde : String;
    FCantidadTransitosAContabilizar: Int64;
    Function  ObtenerUltimaJornadaValidacionCerrada(Conn: TADOConnection): TDateTime;
    Function  EstanCajasCerradas(Conn: TADOConnection; Fecha: TDateTime): Boolean;
    Function  ObtenerUltimoNumeroProcesoContabilizacion(Conn: TADOConnection): Integer;
    Procedure FinalizarContabilizacionInconclusa(NumeroProceso: Integer);
    Function  EsJornadaContabilizable(Conn: TADOConnection; Fecha: TDateTime): Boolean;
    Function  EsJornadaContabilizada(Conn: TADOConnection; Fecha: TDateTime; var NumeroProcesoContabilizacion: Integer; var EstaCerrada: Boolean): Boolean;
    Function  ObtenerUltimoNumeroProcesoContabilizacionNoDesecho(Conn: TADOConnection): Integer;
    Function  DeshacerProcesoContabilizacion(Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
    Function  RegistrarOperacion : Boolean;
    Function  ObtenerTransitosAContabilizar(Fecha : TDateTime): Boolean;
    Function  EliminarCambiosDeEstado(Conn: TAdoConnection; NumeroProceso : Integer; Var DescError : Ansistring) : Boolean;
    Function  Contabilizar_IngresoPeajeDiario(Conn: TAdoConnection;Fecha:TdateTime;NumeroProceso:integer;Var DescError:Ansistring):boolean;
    Function  MarcarTransitosContabilizados(Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
    Function  ObtenerComprobantesAContabilizar(Fecha : TDateTime): Boolean;
    Function  ObtenerMovimientosCuentasAContabilizar: Boolean;
    Function  Contabilizar_EmisionMasivaNotasCobro(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    Function  MarcarComprobantesContabilizados(Conn: TADOConnection; TipoComprobante: AnsiString; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
    Function  Contabilizar_CobrosEmpresaDeCobranzaExterna (Conn: TAdoConnection; Fecha: TdateTime; NumeroProceso: Integer; Var DescError:Ansistring) : Boolean;
    Function  MarcarPagosContabilizados(Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
    Function  Contabilizar_VentasPasesDiarios(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    Function  Contabilizar_VentasBoletosHabilitacionTardia(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_CobrosVentanilla(Conn: TADOConnection;Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_CambiosEstadoValidacionTransitos(Conn: TADOConnection;Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_CambiosEstadoValidacionTransitosFacturableANoFacturable(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer;var DescError: Ansistring): Boolean;
    function  Contabilizar_CambiosEstadoValidacionTransitosNoFacturableAFacturable(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer;var DescError: Ansistring): Boolean;
    function  Contabilizar_BoletasEmitidas(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_FacturasEmitidas(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_NotasDebitoEmitidas(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_NotasCreditoEmitidas(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_NotasCreditoEmitidasParaAnularNK (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_EmisionAPedidoNotasCobro(Conn: TADOConnection;Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_RegistroDocumentosDevueltos (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_NotasCobroPorUsoPDU (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_NotasCobroPorUsoBHT (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_AnulacionPorPagoDuplicado (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_DevolucionDeMontoPorPagoDuplicado (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_AplicacionDelAbono(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    function  Contabilizar_NotasDeCobroBonificadas(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
    Function  EliminarTablasTemporalesUtilizadas(Conn: TADOConnection): Boolean;
    Function  GenerarReporteFinalizacionProceso(NumeroProceso:Integer):boolean;
  public
    function Inicializar: Boolean;
    { Public declarations }
  end;

var
  FContabilizaciondeJornada: TFContabilizaciondeJornada;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    Lgisuk
  Date Created: 29/07/2005
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFContabilizaciondeJornada.Inicializar: Boolean;
resourcestring
  	MSG_INIT_ERROR = 'Error al Inicializar';
  	MSG_ERROR = 'Error';
begin
  	Result := False;
    //Centro el formulario
  	CenterForm(Self);
  	try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
    //Todo Ok, terminamos de inicializar
    lbl_DescripcionPasoGeneral.Caption := EmptyStr;
    lblMensaje.Caption := '';                     //no muestro ningun mensaje
    PnlAvance.visible := False;                   //Oculto el Panel de Avance
    FCancelar := False;                           //inicio cancelar en false
    FNumeroProceso := 0;                           //Inicializo el codigo de operacion
    FErrorGrave := False;                         //Inicio sin errores
    edFecha.Date := NULLDATE;                     //inicializo fecha en null
    btnProcesar.Enabled := False;                 //deshabilito boton procesar hasta que seleccionen una fecha
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 29/07/2005
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFContabilizaciondeJornada.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_CONTABLE        = ' ' + CRLF +
                          'El proceso de contabilizaci�n de jornada' + CRLF +
                          'confecciona los asientos contables de todos' + CRLF +
                          'los eventos ocurridos en un dia calendario' + CRLF +
                          'que se encuentra validado y cerrado' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_CONTABLE, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 29/07/2005
  Description: permito hacer click en la ayuda solo si no esta procesando
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFContabilizaciondeJornada.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{******************************** Function Header ******************************
Function Name: edFechaChange
Author : lgisuk
Date Created : 29/07/2005
Description : Habilito el proceso cuando hay una fecha valida
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFContabilizaciondeJornada.edFechaChange(Sender: TObject);
begin
	if ( edFecha.Date <> nulldate ) then begin
   		btnProcesar.Enabled := True;
   		FFechaDesde := DatetoStr(edFecha.Date);
    end else begin
   		btnProcesar.Enabled := False;
    end;
end;

{******************************** Function Header ******************************
Function Name: ObtenerUltimoNumeroProcesoContabilizacion
Author : ggomez
Date Created : 11/08/2005
Description : Retorna el N�mero del �ltimo Proceso de Contabilizaci�n.
Parameters : Conn: TADOConnection
Return Value : Integer
*******************************************************************************}
function TFContabilizaciondeJornada.ObtenerUltimoNumeroProcesoContabilizacion(Conn: TADOConnection): Integer;
begin
    Result := QueryGetValueInt(Conn, 'SELECT dbo.ObtenerUltimoNumeroProcesoContabilizacion()');
end;

{******************************** Function Header ******************************
Function Name: FinalizarContabilizacionInconclusa
Author : ggomez
Date Created : 11/08/2005
Description : Controla si un proceso de contabilizaci�n NO ha Finalizado (ya sea
    por Inicio o por Deshacer), de ser as� realiza los pasos necesarios para
    finalizarlo.
    Si ocurre un error o el operador responde NO continuar con los pasos, se
    lanza una excepci�n.
Parameters : NumeroProceso: Integer
Return Value : None
*******************************************************************************}
procedure TFContabilizaciondeJornada.FinalizarContabilizacionInconclusa(NumeroProceso: Integer);
resourcestring
    MSG_ERROR_OBTAIN_PROCESS_INFORMATION = 'Ha ocurrido un error al obtener los datos del Proceso de Contabilizaci�n.';
    MSG_PROCESS_INFORMATION              = 'El Proceso de Contabilizaci�n' + CRLF +
                                           '   N�: %d' + CRLF +
                                           '   Fecha de Jornada: %s' + CRLF +
                                           '   Iniciado: %s' + CRLF +
                                           '   Por el Usuario: %s';
    MSG_INITIATED_PROCESS                = 'Ha sido iniciado pero no se ha finalizado.' + CRLF + CRLF +
                                           '�Desea deshacerlo para comenzar nuevamente?';
    MSG_PROCESS_UNDOING                  = 'Se comenz� a deshacer pero no se desecho completamente.' + CRLF + CRLF +
                                           '�Desea continuar deshaciendolo?';
var
    Estado: TEstadoContabilizacion;
    FechaJornada,
    FechaHoraInicio,
    FechaHoraCierre: TDateTime;
    Usuario: AnsiString;
    DescriError: AnsiString;
begin
    // Si el N� de Proceso a testear NO es v�lido, no hacer nada.
    if NumeroProceso < 1 then begin
        Exit;
    end;

    try
        //Obtiene los datos que identifican a un proceso de contabilizacion
        ObtenerDatosProcesoContabilizacion(DMConnections.BaseCAC, NumeroProceso, FechaJornada, FechaHoraInicio, FechaHoraCierre, Usuario, Estado);
    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_OBTAIN_PROCESS_INFORMATION, E.Message, Self.Caption, MB_ICONERROR);
            Abort;
        end;
    end;

    // Seg�n el estado del Proceso, realizar los pasos necesarios para finalizarlo
    case Estado of
        ecIniciada:
            begin
                // Mostrar un mensaje indicando la situaci�n y preguntar si desea
                // deshacer lo hecho para luego comenzarlo nuevamente.
                if MsgBox(  Format(MSG_PROCESS_INFORMATION,
                                [NumeroProceso, DateToStr(FechaJornada),
                                DateTimeToStr(FechaHoraInicio), Trim(Usuario)])
                            + CRLF + CRLF + MSG_INITIATED_PROCESS,
                        Self.Caption, MB_YESNO + MB_DEFBUTTON2 + MB_ICONQUESTION) = mrYes then begin

                    // Deshacerlo
                    if not DeshacerProcesoContabilizacion(DMConnections.BaseCAC, NumeroProceso, DescriError) then begin

                        if DescriError <> EmptyStr then begin
                            MsgBox(DescriError, Self.Caption, MB_ICONERROR);
                            Abort;
                        end;
                        if FCancelar then begin
                            Exit;
                        end;
                    end;

                    // Colocar en la fecha de la jornada la fecha del proceso para
                    // que pueda iniciarlo nuevamente.
                    edFecha.Date := FechaJornada;
                end else begin
                    Abort;
                end; // else if

            end;
        ecDeshaciendo:
            begin
                // Mostrar un mensaje indicando la situaci�n y preguntar si desea
                // continuar deshaciendo el proceoso.
                if MsgBox(  Format(MSG_PROCESS_INFORMATION,
                                [NumeroProceso, DateToStr(FechaJornada),
                                DateTimeToStr(FechaHoraInicio), Trim(Usuario)])
                            + CRLF + CRLF + MSG_PROCESS_UNDOING,
                        Self.Caption, MB_YESNO + MB_DEFBUTTON2 + MB_ICONQUESTION) = mrYes then begin

                    // Deshacerlo
                    if not DeshacerProcesoContabilizacion(DMConnections.BaseCAC, NumeroProceso, DescriError) then begin

                        if DescriError <> EmptyStr then begin
                            MsgBox(DescriError, Self.Caption, MB_ICONERROR);
                            Abort;
                        end;
                        if FCancelar then begin
                            Exit;
                        end;
                    end;

                end else begin
                    Abort;
                end; // else if

            end;

    end; // case

end;

{******************************** Function Header ******************************
Function Name: EsJornadaContabilizable
Author : ggomez
Date Created : 10/08/2005
Description : Dada una fecha, retorna True, si es un d�a contable v�lido.
Parameters : Conn: TADOConnection; Fecha: TDateTime
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.EsJornadaContabilizable(Conn: TADOConnection;Fecha: TDateTime): Boolean;
begin
    Result := QueryGetValueInt(Conn, 'SELECT dbo.EsJornadaContabilizable(' + QuotedStr(FormatDateTime('YYYYMMDD',Fecha)) + ')') = 1;     //QuotedStr(DateToStr(Fecha))
end;

{******************************** Function Header ******************************
Function Name: ObtenerUltimaJornadaValidacionCerrada
Author : ggomez
Date Created : 19/08/2005
Description : Retorna la fecha de la �ltima jornada de validaci�n cerrada.
Parameters : None
Return Value : TDateTime
*******************************************************************************}
function TFContabilizaciondeJornada.ObtenerUltimaJornadaValidacionCerrada(Conn: TADOConnection): TDateTime;
begin
    Result := QueryGetValueDateTime(Conn, 'SELECT dbo.ObtenerUltimaJornadaCerrada()');
end;

{******************************** Function Header ******************************
Function Name: EstanCajasCerradas
Author : ggomez
Date Created : 19/08/2005
Description : Retorna True si las Cajas est�n cerradas.
Parameters : Conn: TADOConnection
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.EstanCajasCerradas(Conn: TADOConnection; Fecha: TDateTime): Boolean;
begin
    Result := QueryGetValueInt(Conn, 'SELECT dbo.EstanCajasCerradas(' + QuotedStr(FormatDateTime('YYYYMMDD',Fecha)) + ')') = 1;     //QuotedStr(DateToStr(Fecha))
end;

{******************************** Function Header ******************************
Function Name: EsJornadaContabilizada
Author : ggomez
Date Created : 10/08/2005
Description : Dada una fecha, retorna True si fue contabilizada. En este
    caso el N� de proceso de contabilizaci�n es retornado en
    NumeroProcesoContabilizacion, en EstaCerrada se indica si la Contabilizaci�n
    est� cerrada.
Parameters : Conn: TADOConnection; Fecha: TDateTime;
    var NumeroProcesoContabilizacion: Integer; var EstaCerrada: Boolean
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.EsJornadaContabilizada(Conn: TADOConnection; Fecha: TDateTime; var NumeroProcesoContabilizacion: Integer; var EstaCerrada: Boolean): Boolean;
var
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection       := Conn;
        sp.ProcedureName    := 'Contabilizar_EsJornadaContabilizada';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@Fecha').Value                           := Fecha;
        sp.Parameters.ParamByName('@NumeroProcesoContabilizacion').Value    := -1;
        sp.Parameters.ParamByName('@EstaCerrada').Value                     := 0;
        sp.ExecProc;

        NumeroProcesoContabilizacion    := sp.Parameters.ParamByName('@NumeroProcesoContabilizacion').Value;
        EstaCerrada                     := sp.Parameters.ParamByName('@EstaCerrada').Value;

        Result := (sp.Parameters.ParamByName('@NumeroProcesoContabilizacion').Value <> -1);

    finally
        sp.Free;
    end;

end;

{******************************** Function Header ******************************
Function Name: DeshacerProcesoContabilizacion
Author : ggomez
Date Created : 10/08/2005
Description : Dado un N� de Proceso de Contabilizaci�n, lo deshace.
    Si se deshace con exito, retorna True
    Sino retorna False y deja en DescriError una descripci�n de lo que ocurri�.
Parameters : Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.DeshacerProcesoContabilizacion(Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;

    {******************************** Function Header ******************************
    Function Name: ObtenerCantidadTransitosContabilizados
    Author : ggomez
    Date Created : 06/09/2005
    Description : Retorna la cantidad de Tr�nsitos involucrados en un Proceso de Contabilizaci�n.
    Parameters : Conn: TADOConnection; NumeroProcesoContabilizacion: Integer
    Return Value : Int64
    *******************************************************************************}
    function ObtenerCantidadTransitosContabilizados(Conn: TADOConnection; NumeroProcesoContabilizacion: Integer): Int64;
    begin
        Result := QueryGetValueInt(Conn, 'SELECT dbo.ObtenerCantidadTransitosContabilizados('+ IntToStr(NumeroProcesoContabilizacion)+ ')');
    end;
    
    {******************************** Function Header ******************************
    Function Name: MarcarTransitosComoNoContabilizados
    Author : ggomez
    Date Created : 11/08/2005
    Description : Marca como NO contabilizados los tr�nsitos involucrados en proceso
        de contabilizaci�n
    Parameters : Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString
    Return Value : Boolean
    *******************************************************************************}
    function MarcarTransitosComoNoContabilizados( Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
    resourcestring
        MSG_ERROR_TO_MARK_ASSESSED_RECORDS = 'Ha ocurrido un error al marcar los tr�nsitos como NO Contabilizados.';
    var
        CantRegistrosActualizados: Integer;
    begin
        // Inicializar variables
        CantRegistrosActualizados   := -1;
        DescriError                 := EmptyStr;

        // Inicializar la barra de progreso
        pbProgreso.Position := 1;
        // Incremento en 1 pues Position es inicilizado en 1.
        pbProgreso.Max      := ObtenerCantidadTransitosContabilizados(DMConnections.BaseCAC,
                                NumeroProcesoContabilizacion) + 1;

        while (CantRegistrosActualizados <> 0) and (DescriError = EmptyStr) and (not FCancelar) do begin
            try
                with spMarcarTransitosComoNoContabilizados do begin
                    Close;
                    CommandTimeout := 500;
                    Parameters.Refresh;
                    Parameters.ParamByName('@NumeroProcesoContabilizacion').Value   := NumeroProcesoContabilizacion;
                    Parameters.ParamByName('@CantidadRegistrosActualizados').Value  := 0;
                    // Ejecutar
                    ExecProc;
                    // Obtener la cantidad de registros actualizados
                    CantRegistrosActualizados := Parameters.ParamByName('@CantidadRegistrosActualizados').Value;
                end;
            except
                on E: Exception do begin
                    DescriError := MSG_ERROR_TO_MARK_ASSESSED_RECORDS + E.Message;
                    Break;
                end;
            end; // except

            // Incrementar la barra de progreso
            pbProgreso.StepIt;
            if pbProgreso.Position >= pbProgreso.Max then begin
                // Inicializar la barra de progreso
                pbProgreso.Position := 1;
            end;

            // Refrescar la pantalla
            Application.ProcessMessages;
        end; // while

        // Finalizar la barra de progreso
        pbProgreso.Position := pbProgreso.Max;
        Application.ProcessMessages;

        // Si sale porque se indic� Cancelar, retornar False
        if FCancelar then begin
            Result := False;
            Exit;
        end;

        // Si no hubo error, retornar True.
        Result := (DescriError = EmptyStr);

    end;

    {******************************** Function Header ******************************
    Function Name: ObtenerCantidadComprobantesContabilizados
    Author : ggomez
    Date Created : 07/09/2005
    Description : Retorna la cantidad de comprobantes contabilizados en un proceso de contabilizaci�n.
    Parameters : Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; TipoComprobante: AnsiString
    Return Value : Int64
    *******************************************************************************}
    function ObtenerCantidadComprobantesContabilizados(Conn: TADOConnection; NumeroProcesoContabilizacion: Integer;TipoComprobante: AnsiString): Int64;
    begin
        Result := QueryGetValueInt(Conn, 'SELECT dbo.ObtenerCantidadComprobantesContabilizados('+ IntToStr(NumeroProcesoContabilizacion) + ', ' + QuotedStr(TipoComprobante) +')');
    end;

    {******************************** Function Header ******************************
    Function Name: MarcarComprobantesComoNoContabilizados
    Author : ggomez
    Date Created : 22/08/2005
    Description : Marca como NO contabilizados los comprobantes involucrados en el
        proceso de contabilizaci�n.
    Parameters : Conn: TADOConnection; TipoComprobante: AnsiString;
      NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString
    Return Value : Boolean
    *******************************************************************************}
    function MarcarComprobantesComoNoContabilizados(Conn: TADOConnection; TipoComprobante: AnsiString; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
    resourcestring
        MSG_ERROR_MARKING_VOUCHERS = 'Ha ocurrido un error al marcar los comprobantes como NO Contabilizados.';
    var
        CantRegistrosActualizados: Integer;
    begin
        // Inicializar variables
        CantRegistrosActualizados   := -1;
        DescriError                 := EmptyStr;

        // Inicializar la barra de progreso
        pbProgreso.Position := 1;
        pbProgreso.Max      := ObtenerCantidadComprobantesContabilizados(DMConnections.BaseCAC,
                                NumeroProcesoContabilizacion, TipoComprobante);

        while (CantRegistrosActualizados <> 0) and (DescriError = EmptyStr) and (not FCancelar) do begin
            try
                with spMarcarComprobantesComoNoContabilizados do begin
                    Close;
                    CommandTimeout := 500;
                    Parameters.Refresh;
                    Parameters.ParamByName('@TipoComprobante').Value                := TipoComprobante;
                    Parameters.ParamByName('@NumeroProcesoContabilizacion').Value   := NumeroProcesoContabilizacion;
                    Parameters.ParamByName('@CantidadRegistrosActualizados').Value  := 0;
                    // Ejecutar
                    ExecProc;
                    // Obtener la cantidad de registros actualizados
                    CantRegistrosActualizados := Parameters.ParamByName('@CantidadRegistrosActualizados').Value;
                end;
            except
                on E: Exception do begin
                    DescriError := MSG_ERROR_MARKING_VOUCHERS + CRLF + E.Message;
                    Break;
                end;
            end; // except

            // Incrementar la barra de progreso
            pbProgreso.StepIt;
            if pbProgreso.Position >= pbProgreso.Max then begin
                // Inicializar la barra de progreso
                pbProgreso.Position := 1;
            end;

            // Refrescar la pantalla
            Application.ProcessMessages;
        end; // while

        // Finalizar la barra de progreso
        pbProgreso.Position := pbProgreso.Max;
        Application.ProcessMessages;

        // Si sale porque se indic� Cancelar, retornar False
        if FCancelar then begin
            Result := False;
            Exit;
        end;

        // Si no hubo error, retornar True.
        Result := (DescriError = EmptyStr);

    end;

    {******************************** Function Header ******************************
    Function Name: MarcarPagosComoNoContabilizados
    Author : lgisuk
    Date Created : 01/09/2005
    Description : Marca como NO contabilizados los Pagos involucrados en el proceso de contabilizaci�n.
    Parameters :
    Return Value : Boolean
    *******************************************************************************}
    function MarcarPagosComoNoContabilizados(Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
    resourcestring
        MSG_ERROR_MARKING_RECORDS = 'Ha ocurrido un error al marcar los pagos como NO Contabilizados.';
    var
        CantRegistrosActualizados: Integer;
    begin
        // Inicializar variables
        CantRegistrosActualizados   := -1;
        DescriError                 := EmptyStr;

        // Inicializar la barra de progreso
        pbProgreso.Position := 1;
        pbProgreso.Max      := 100;

        while (CantRegistrosActualizados <> 0) and (DescriError = EmptyStr) and (not FCancelar) do begin
            try
                with spMarcarPagosComoNoContabilizados do begin
                    Close;
                    CommandTimeout := 500;
                    Parameters.Refresh;
                    Parameters.ParamByName('@NumeroProcesoContabilizacion').Value   := NumeroProcesoContabilizacion;
                    Parameters.ParamByName('@CantidadRegistrosActualizados').Value  := 0;
                    // Ejecutar
                    ExecProc;
                    // Obtener la cantidad de registros actualizados
                    CantRegistrosActualizados := Parameters.ParamByName('@CantidadRegistrosActualizados').Value;
                end;
            except
                on E: Exception do begin
                    DescriError := MSG_ERROR_MARKING_RECORDS + CRLF + E.Message;
                    Break;
                end;
            end; // except

            // Incrementar la barra de progreso
            pbProgreso.StepIt;
            if pbProgreso.Position >= pbProgreso.Max then begin
                // Inicializar la barra de progreso
                pbProgreso.Position := 1;
            end;

            // Refrescar la pantalla
            Application.ProcessMessages;
        end; // while

        // Finalizar la barra de progreso
        pbProgreso.Position := pbProgreso.Max;
        Application.ProcessMessages;

        // Si sale porque se indic� Cancelar, retornar False
        if FCancelar then begin
            Result := False;
            Exit;
        end;

        // Si no hubo error, retornar True.
        Result := (DescriError = EmptyStr);

    end;

    {******************************** Function Header ******************************
    Function Name: MarcarVentasPasesDiariosComoNoContabilizados
    Author : ggomez
    Date Created : 24/08/2005
    Description : Marca como NO contabilizados las ventas de pases diarios
        (PDU y BHT) involucradas en el proceso de contabilizaci�n.
    Parameters : Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString
    Return Value : Boolean
    *******************************************************************************}
    function MarcarVentasPasesDiariosComoNoContabilizados(Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
    resourcestring
        MSG_ERROR_MARKING_SALES_DAY_PASS = 'Ha ocurrido un error al marcar las Ventas de Pases Diarios como NO Contabilizadas.';
    begin
        DescriError := EmptyStr;

        try
            with spMarcarVentasPasesDiariosComoNoContabilizados do begin
                Close;
                CommandTimeout := 500;
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroProcesoContabilizacion').Value := NumeroProcesoContabilizacion;
                // Ejecutar
                ExecProc;
            end;
        except
            on E: Exception do begin
                DescriError := MSG_ERROR_MARKING_SALES_DAY_PASS + CRLF + E.Message;
            end;
        end; // except

        // Si no hubo error, retornar True.
        Result := (DescriError = EmptyStr);
    end;

    {******************************** Function Header ******************************
    Function Name: MarcarAnulacionesComoNoContabilizadas
    Author : lgisuk
    Date Created : 21/09/2005
    Description : Marca como NO contabilizados las anulaciones de pagos
    Parameters : Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString
    Return Value : Boolean
    *******************************************************************************}
    function MarcarAnulacionesComoNoContabilizadas(Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
    resourcestring
        MSG_ERROR_MARKING_ANNULAMENTS_OF_PAYMENTS = 'Ha ocurrido un error al marcar las Ventas de Pases Diarios como NO Contabilizadas.';
    begin
        DescriError := EmptyStr;

        try
            with spMarcarAnulacionesComoNoContabilizadas do begin
                Close;
                CommandTimeout := 500;
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroProcesoContabilizacion').Value := NumeroProcesoContabilizacion;
                // Ejecutar
                ExecProc;
            end;
        except
            on E: Exception do begin
                DescriError := MSG_ERROR_MARKING_ANNULAMENTS_OF_PAYMENTS + CRLF + E.Message;
            end;
        end; // except

        // Si no hubo error, retornar True.
        Result := (DescriError = EmptyStr);
    end;

resourcestring
    MSG_UNDOING_PROCESS             = 'Deshaciendo el proceso de Contabilizaci�n N�: %d ...';
    MSG_UNDOING_TRAFFIC             = 'Marcando Tr�nsitos como No Contabilizados ...';
    MSG_UNDOING_NOTES_COLLECTION    = 'Marcando Comprobantes como No Contabilizados ...';
    MSG_UNDOING_PAYMENT             = 'Marcando Pagos como No Contabilizados ...';
    MSG_UNDOING_DAY_PASS            = 'Marcando Pases Diarios como No Contabilizados ...';
    MSG_UNDOING_BALLOTS             = 'Marcando Boletas como No Contabilizadas ...';
    MSG_UNDOING_INVOICE             = 'Marcando Facturas como No Contabilizadas ...';
    MSG_UNDOING_NOTES_DEBIT         = 'Marcando Notas de Debito como No Contabilizadas ...';
    MSG_UNDOING_NOTES_CREDIT        = 'Marcando Notas de Credito como No Contabilizadas ...';
    MSG_UNDOING_NOTES_CREDIT_FOR_NOTES_COLLECTION = 'Marcando Notas de Credito Para Notas de Cobro como No Contabilizadas ...';
    MSG_UNDOING_ANNULMENTS          = 'Marcando Anulaciones como No Contabilizadas ...';
    MSG_UNDOING_DIRECT_NOTES_COLLECTION = 'Marcando Notas de Cobro Directas No Contabilizadas ...';
var
    sp: TADOStoredProc;
begin
    Result := False;

    lbl_DescripcionPasoGeneral.Caption := Format(MSG_UNDOING_PROCESS, [NumeroProcesoContabilizacion]);
  	btnProcesar.Enabled := False;
    pnlAvance.Visible   := True;
  	Screen.Cursor       := crHourGlass;
    KeyPreview          := True;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            // Iniciar el proceso de deshacer
            sp.Connection       := Conn;
            sp.ProcedureName    := 'Contabilizar_IniciarDeshacerProceso';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@NumeroProcesoContabilizacion').Value := NumeroProcesoContabilizacion;
            sp.Parameters.ParamByName('@DescripcionError').Value := '';
            sp.ExecProc;

            //-----------------------------------------------------------------
            // Marcar como NO Contabilizados los registros que intervinieron
            // en el proceso de contabilizaci�n.
            //-----------------------------------------------------------------
            // Marcar los Tr�nsitos como NO Contabilizados.
            lblMensaje.Caption  := MSG_UNDOING_TRAFFIC;
            Application.ProcessMessages;
            if not MarcarTransitosComoNoContabilizados(DMConnections.BaseCAC, NumeroProcesoContabilizacion, DescriError) then begin
                Exit;
            end;

            // Marcar los Comprobantes como NO Contabilizados.
            lblMensaje.Caption  := MSG_UNDOING_NOTES_COLLECTION;
            Application.ProcessMessages;
            if not MarcarComprobantesComoNoContabilizados(DMConnections.BaseCAC, TC_NOTA_COBRO, NumeroProcesoContabilizacion, DescriError) then begin
                Exit;
            end;

            // Marcar los Pagos como NO Contabilizados.
            lblMensaje.Caption  := MSG_UNDOING_PAYMENT;
            Application.ProcessMessages;
            if not MarcarPagosComoNoContabilizados(DMConnections.BaseCAC, NumeroProcesoContabilizacion, DescriError) then begin
                Exit;
            end;

            // Marcar las Ventas de Pases Diarios como NO Contabilizados.
            lblMensaje.Caption  := MSG_UNDOING_DAY_PASS;
            Application.ProcessMessages;
            if not MarcarVentasPasesDiariosComoNoContabilizados(DMConnections.BaseCAC, NumeroProcesoContabilizacion, DescriError) then begin
                Exit;
            end;

            // Marcar los Comprobantes como NO Contabilizados.
            lblMensaje.Caption  := MSG_UNDOING_BALLOTS;
            Application.ProcessMessages;
            if not MarcarComprobantesComoNoContabilizados(DMConnections.BaseCAC, TC_BOLETA, NumeroProcesoContabilizacion, DescriError) then begin
                Exit;
            end;

            // Marcar los Comprobantes como NO Contabilizados.
            lblMensaje.Caption  := MSG_UNDOING_INVOICE;
            Application.ProcessMessages;
            if not MarcarComprobantesComoNoContabilizados(DMConnections.BaseCAC, TC_FACTURA, NumeroProcesoContabilizacion, DescriError) then begin
                Exit;
            end;

            // Marcar los Comprobantes como NO Contabilizados.
            lblMensaje.Caption  := MSG_UNDOING_NOTES_DEBIT;
            Application.ProcessMessages;
            if not MarcarComprobantesComoNoContabilizados(DMConnections.BaseCAC, TC_NOTA_DEBITO, NumeroProcesoContabilizacion, DescriError) then begin
                Exit;
            end;

            // Marcar los Comprobantes como NO Contabilizados.
            lblMensaje.Caption  := MSG_UNDOING_NOTES_CREDIT;
            Application.ProcessMessages;
            if not MarcarComprobantesComoNoContabilizados(DMConnections.BaseCAC, TC_NOTA_CREDITO, NumeroProcesoContabilizacion, DescriError) then begin
                Exit;
            end;

            // Marcar los Comprobantes como NO Contabilizados.
            lblMensaje.Caption  := MSG_UNDOING_NOTES_CREDIT_FOR_NOTES_COLLECTION;
            Application.ProcessMessages;
            if not MarcarComprobantesComoNoContabilizados(DMConnections.BaseCAC, TC_NOTA_CREDITO_A_COBRO, NumeroProcesoContabilizacion, DescriError) then begin
                Exit;
            end;

            // Marcar las Anulaciones como NO Contabilizadas.
            lblMensaje.Caption  := MSG_UNDOING_ANNULMENTS;
            Application.ProcessMessages;
            if not MarcarAnulacionesComoNoContabilizadas(DMConnections.BaseCAC, NumeroProcesoContabilizacion, DescriError) then begin
                Exit;
            end;

            // Marcar los Comprobantes como NO Contabilizados.
            lblMensaje.Caption  := MSG_UNDOING_DIRECT_NOTES_COLLECTION;
            Application.ProcessMessages;
            if not MarcarComprobantesComoNoContabilizados(DMConnections.BaseCAC, TC_NOTA_COBRO_DIRECTA, NumeroProcesoContabilizacion, DescriError) then begin
                Exit;
            end;

            // Finalizar el proceso de deshacer
            sp.ProcedureName    := 'Contabilizar_FinalizarDeshacerProceso';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@NumeroProcesoContabilizacion').Value := NumeroProcesoContabilizacion;
            sp.Parameters.ParamByName('@DescripcionError').Value := '';
            sp.ExecProc;

            Result := True;
        except
            on E: Exception do begin
                Result      := False;
                DescriError := E.Message;
                if Trim(sp.Parameters.ParamByName('@DescripcionError').Value) <> EmptyStr then begin
                    DescriError := sp.Parameters.ParamByName('@DescripcionError').Value
                                    + CRLF + DescriError;
                end; // if
            end;

        end; // except

    finally
        sp.Free;
        Screen.Cursor       := crDefault;
        pbProgreso.Position := 0;
        pnlAvance.Visible   := False;
        lbl_DescripcionPasoGeneral.Caption := EmptyStr;
        lblMensaje.Caption  := EmptyStr;
    end;
end;

{******************************** Function Header ******************************
Function Name: ObtenerUltimoNumeroProcesoContabilizacion
Author : ggomez
Date Created : 11/08/2005
Description : Retorna el N�mero del �ltimo Proceso de Contabilizaci�n NO Desecho.
Parameters : Conn: TADOConnection
Return Value : Integer
*******************************************************************************}
function TFContabilizaciondeJornada.ObtenerUltimoNumeroProcesoContabilizacionNoDesecho(Conn: TADOConnection): Integer;
begin
    Result := QueryGetValueInt(Conn, 'SELECT dbo.ObtenerUltimoNumeroProcesoContabilizacionNoDesecho()');
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    lgisuk
  Date Created: 29/07/2005
  Description: Registro la operacion en el log
  Parameters: var DescError:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFContabilizaciondeJornada.RegistrarOperacion: Boolean;

      {-----------------------------------------------------------------------------
        Function Name: InsertarProcesoContabilizacion
        Author:    lgisuk
        Date Created: 29/07/2005
        Description: Inserta el proceso de contabilizaci�n
        Parameters:
        Return Value: boolean
      -----------------------------------------------------------------------------}
      Function  InsertarProcesoContabilizacion(Conn: TAdoConnection;Fecha:TdateTime;Usuario:string;var NumeroProceso:integer;Var DescError:Ansistring):boolean;
      var
          sp : TADOStoredProc;
      begin
          NumeroProceso := 0;
          result := false;
          sp :=TADOStoredProc.Create(Nil);
          try
              try
                  sp.Connection := Conn;
                  sp.ProcedureName := 'InsertarProcesoContabilizacion';
                  with SP.Parameters do begin
                      Refresh;
                      ParamByName('@Fecha').Value         := Fecha;
                      ParamByName('@CodigoUsuario').Value := Usuario;
                      ParamByName('@NumeroProceso').Value := 0;
                  end;
                  SP.ExecProc;                                                        //Ejecuto el procedimiento
                  NumeroProceso := SP.Parameters.ParamByName('@NumeroProceso').Value; //Obtengo el Id
                  result:=true;
              except
                  on e: Exception do begin
                      DescError := e.Message;
                  end;
              end;
          finally
              SP.Free;
          end;
      end;

resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo insertar el proceso de contabilizaci�n';
    MSG_ERROR = 'Error';
var
    DescError : String;
begin
    //Registro la operacion en el Log
    Result :=  InsertarProcesoContabilizacion(DMConnections.BaseCAC, edFecha.Date,  UsuarioSistema, FNumeroProceso, DescError);
    if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerTransitosAContabilizar
  Author:    lgisuk
  Date Created: 08/08/2005
  Description: Obtengo los Transitos que se van a contabilizar
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFContabilizaciondeJornada.ObtenerTransitosAContabilizar(Fecha : TDateTime): Boolean;
Resourcestring
    MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS = 'No se pudieron obtener los transitos a contabilizar';
    MSG_ERROR = 'Error';
var
  	NumCorrCADesde : Variant;
    NumCorrCAHasta : Variant;
    FErrorMsg : AnsiString;
begin

    //Inicializo variables
    NumCorrCADesde := -1;
    NumCorrCAHasta := 0;
    FerrorMsg := '';

    //inicializo la barra de progreso
    pbProgreso.Position := 1;
    // Incremento en 1 pues Position es inicilizado en 1.
    pbProgreso.Max := FCantidadTransitosAContabilizar + 1;

    while (NumCorrCADesde <> NumCorrCAHasta) and (FErrorMsg = '') and (not FCancelar) do begin

        NumCorrCADesde := NumCorrCaHasta;

        try
            With SPRegistrarTransitosAContabilizar do begin
                Close;
                CommandTimeout := 500;
                Parameters.Refresh;
                Parameters.ParamByName('@Fecha').Value := Fecha;
                //Parameters.ParamByName('@NumeroProcesoContabilizacion').Value := NumeroProceso;
                Parameters.ParamByName('@NumCorrCADesde').Value := NumCorrCADesde;
                Parameters.ParamByName('@NumCorrCAHasta').Value := 0;
                //Ejecuto
                ExecProc;
                //Obtengo el ultimo numero de convenio procesado
                NumCorrCAHasta := Integer (Parameters.ParamByName( '@NumCorrCAHasta' ).Value);
            end;
        except
            on e: Exception do begin
                FErrorMsg := MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS;
                FErrorGrave := True;
                MsgBoxErr(FErrorMsg, e.Message, MSG_ERROR, MB_ICONERROR);
                Break;
            end;
        end;

        // Incrementar la barra de progreso
        pbProgreso.StepIt;
        if pbProgreso.Position >= pbProgreso.Max then begin
            // Inicializar la barra de progreso
            pbProgreso.Position := 1;
        end;

        //Refresco la pantalla
        Application.ProcessMessages;
    end;

    //finalizo la barra de progreso
    pbProgreso.Position := pbProgreso.Max;
    Application.ProcessMessages;

    //Si llego a procesar hasta el final y no hubo errores. Apruebo este paso.
    Result := (NumCorrCADesde = NumCorrCaHasta);
end;

{-----------------------------------------------------------------------------
  Function Name: EliminarCambiosDeEstado
  Author:    lgisuk
  Date Created: 06/12/2005
  Description: Elimino los transitos que ya estan en un cambio de estado
  Parameters:
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  TFContabilizaciondeJornada.EliminarCambiosDeEstado(Conn: TAdoConnection; NumeroProceso : Integer; Var DescError : Ansistring) : Boolean;
var
    sp: TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            with sp do begin
                Connection       := Conn;
                CommandTimeOut   := 500;
                ProcedureName    := 'ObtenerTransitosAContabilizar';
                Open;
                //inicializo la barra de progreso
                pbProgreso.Position := 1;
                //Incremento en 1 pues Position es inicilizado en 1.
                pbProgreso.Max := FieldCount;
                //Recorro todos los transitos a contabilizar
                while not Eof do begin
                    //Si esta en un cambio de estado
                    if QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.TieneCambiosdeEstado ( ' + Fieldbyname('NumCorrCA').asString + ' , ' + QuotedStr(FormatDateTime('YYYYMMDD',Fieldbyname('Fecha').asDateTime)) + ' , ' + IntToStr(NumeroProceso) + ')') = 'True' then begin
                        //Elimino el registro
                        Delete;
                    end;
                    //Incremento la barra de progreso
                    pbProgreso.Position := pbProgreso.Position + 1;
                    Application.ProcessMessages;
                    //siguente
                    Next;
                end;
                //finalizo la barra de progreso
                pbProgreso.Position := pbProgreso.Max;
                Application.ProcessMessages;
            end;
            Result := True;
        except
            on e: Exception do begin
                DescError := e.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Contabilizar_IngresoPeajeDiario
  Author:    lgisuk
  Date Created: 29/07/2005
  Description: Genera el Asiento de "Ingreso de Peaje Diario"
  Parameters:
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  TFContabilizaciondeJornada.Contabilizar_IngresoPeajeDiario(Conn: TAdoConnection;Fecha:TdateTime;NumeroProceso:integer;Var DescError:Ansistring):boolean;
var
    sp: TADOStoredProc;
begin
    Result := false;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.ProcedureName    := 'Contabilizar_IngresoPeajeDiario';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@NumeroProcesoContabilizacion').Value := NumeroProceso;
                ParamByName('@DescripcionError').Value             := EmptyStr;
            end;
            sp.ExecProc;                                                        //Ejecuto el procedimiento
            DescError := sp.Parameters.ParamByName('@DescripcionError').Value;  //Obtengo el error
            Result := True;
        except
            on e: Exception do begin
                DescError := e.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{----------------------------------------------------------------------------------
  Function Name: Contabilizar_CobrosEmpresaDeCobranzaExterna
  Author:    lgisuk
  Date Created: 08/11/2005
  Description: Genera los Asientos de Cobro de Empresas de Cobranza Externas
  Parameters:
  Return Value: boolean
----------------------------------------------------------------------------------}
Function  TFContabilizaciondeJornada.Contabilizar_CobrosEmpresaDeCobranzaExterna (Conn: TAdoConnection; Fecha: TdateTime; NumeroProceso: Integer; Var DescError:Ansistring) : Boolean;

    {------------------------------------------------------------------------------------
      Function Name: Contabilizar_CobroEmpresaDeCobranzaExterna
      Author:    lgisuk
      Date Created: 08/11/2005
      Description: Genera el Asiento de Cobros Empresa de Cobranza Externa
      Parameters:
      Return Value: boolean
    ------------------------------------------------------------------------------------}
    Function Contabilizar_CobroEmpresaDeCobranzaExterna(Conn: TAdoConnection; Fecha: TdateTime; CodigoTipoMedioPago, PAT_CodigoTipoTarjetaCredito: Integer ; NumeroProceso: Integer;Var DescError:Ansistring) : Boolean;
    var
        sp : TADOStoredProc;
    begin
        result := false;
        sp := TADOStoredProc.Create(Nil);
        try
            try
                sp.Connection := Conn;
                sp.ProcedureName := 'Contabilizar_CobrosEmpresaDeCobranzaExterna';
                sp.CommandTimeout := 500;
                with SP.Parameters do begin
                    Refresh;
                    ParamByName('@NumeroProcesoContabilizacion').Value := NumeroProceso;
                    ParamByName('@Fecha').Value := Fecha;
                    ParamByName('@CodigoTipoMedioPago').Value := CodigoTipoMedioPago;
                    ParamByName('@PAT_CodigoTipoTarjetaCredito').Value := IIF (PAT_CodigoTipoTarjetaCredito = 0, NULL, PAT_CodigoTipoTarjetaCredito);
                    ParamByName('@DescripcionError').Value := '';
                end;
                SP.ExecProc;                                                        //Ejecuto el procedimiento
                DescError := SP.Parameters.ParamByName('@DescripcionError').Value;  //Obtengo el error
                result:=true;
            except
                on e: Exception do begin
                    DescError := e.Message;
                end;
            end;
        finally
            SP.Free;
        end;
    end;

Resourcestring
    MSG_ERROR = '';
Const
    STR_ASSESING_COLLECTIONS_COMPANY  = 'Contabilizando Cobros Empresa ';
Var
    sp : TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        try
            with sp do begin
              Connection := Conn;
              ProcedureName := 'ObtenerEmpresasDeCobranzaExternas';
              CommandTimeout := 500;
              Open; //abro la consulta
              while not Eof do begin

                  //Genero el Asiento de Cobro de Empresa de Cobranza Externa
                  Lblmensaje.Caption := STR_ASSESING_COLLECTIONS_COMPANY + Fieldbyname('Descripcion').AsString + ' ...';
                  Application.ProcessMessages;
                  Result := Contabilizar_CobroEmpresaDeCobranzaExterna (Conn, Fecha, Fieldbyname('CodigoTipoMedioPago').AsInteger, Fieldbyname('PAT_CodigoTipoTarjetaCredito').AsInteger , NumeroProceso, DescError) and
                                MarcarPagosContabilizados(DMConnections.BaseCAC, FNumeroProceso, DescError);
                  if not Result then begin
                      exit;
                  end;

                  Next;
              end;
              Close;
            end;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        SP.Free;
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: MarcarTransitosContabilizados
Author : ggomez
Date Created : 10/08/2005
Description : Marca como contabilizados los tr�nsitos involucrados en el asiento
    de Registro de Peaje Diario.
Parameters : Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.MarcarTransitosContabilizados(Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
resourcestring
    MSG_ERROR_TO_MARK_ASSESSED_RECORDS   = 'Ha ocurrido un error al marcar los tr�nsitos como Contabilizados.';
var
  	NumCorrCADesde: Int64;
    NumCorrCAHasta: Int64;
begin
    REsult := False;

    // Inicializar variables
    NumCorrCADesde  := -1;
    NumCorrCAHasta  := 0;
    DescriError     := EmptyStr;

    // Inicializar la barra de progreso
    pbProgreso.Position := 1;
    // Incremento en 1 pues Position es inicilizado en 1.
    pbProgreso.Max      := FCantidadTransitosAContabilizar + 1;

    while (NumCorrCADesde <> NumCorrCAHasta) and (DescriError = EmptyStr) and (not FCancelar) do begin

        NumCorrCADesde := NumCorrCaHasta + 1 ;

        try
            with spMarcarTransitosContabilizados do begin
                Close;
                CommandTimeout := 1000;
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroProcesoContabilizacion').Value   := NumeroProcesoContabilizacion;
                Parameters.ParamByName('@NumCorrCADesde').Value                 := NumCorrCADesde;
                Parameters.ParamByName('@NumCorrCAHasta').Value                 := 0;
                // Ejecutar
                ExecProc;
                // Obtener el �ltimo n�mero de tr�nsito procesado
                NumCorrCAHasta := Parameters.ParamByName('@NumCorrCAHasta').Value;
            end; // with
        except
            on E: Exception do begin
                Result      := False;
                DescriError := MSG_ERROR_TO_MARK_ASSESSED_RECORDS + CRLF + E.Message;
                Break;
            end;
        end; // except

        // Incrementar la barra de progreso
        pbProgreso.StepIt;
        if pbProgreso.Position >= pbProgreso.Max then begin
            // Inicializar la barra de progreso
            pbProgreso.Position := 1;
        end;

        // Refrescar la pantalla
        Application.ProcessMessages;
    end; // while


    // Finalizar la barra de progreso
    pbProgreso.Position := pbProgreso.Max;
    Application.ProcessMessages;

    // Si no hubo error y lleg� a procesar todo, retornar True.
    if DescriError = EmptyStr then begin
        Result := (NumCorrCADesde = NumCorrCaHasta);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerComprobantesAContabilizar
  Author:    lgisuk
  Date Created: 07/09/2005
  Description: Obtengo los Comprobantes que se van a contabilizar
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFContabilizaciondeJornada.ObtenerComprobantesAContabilizar(Fecha : TDateTime): Boolean;
Resourcestring
    MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS = 'No se pudieron obtener los Comprobantes a contabilizar';
    MSG_ERROR = 'Error';
var
  	NumeroComprobanteDesde : Variant;
    NumeroComprobanteHasta : Variant;
    FErrorMsg : AnsiString;
begin

    //Inicializo variables
    NumeroComprobanteDesde := -1;
    NumeroComprobanteHasta := 0;
    FerrorMsg := '';

    //inicializo la barra de progreso
    pbProgreso.Position := 1;
    pbProgreso.Max := 100;

    while (NumeroComprobanteDesde <> NumeroComprobanteHasta) and (FErrorMsg = '') and (not FCancelar) do begin

        NumeroComprobanteDesde := NumeroComprobanteHasta;

        try
            With SPRegistrarComprobantesAContabilizar do begin
                Close;
                CommandTimeout := 500;
                Parameters.Refresh;
                Parameters.ParamByName('@Fecha').Value := Fecha;
                Parameters.ParamByName('@NumeroComprobanteDesde').Value := NumeroComprobanteDesde;
                Parameters.ParamByName('@NumeroComprobanteHasta').Value := 0;
                //Ejecuto
                ExecProc;
                //Obtengo el ultimo numero de comprobante procesado
                NumeroComprobanteHasta := Integer (Parameters.ParamByName( '@NumeroComprobanteHasta' ).Value);
            end;
        except
            on e: Exception do begin
                FErrorMsg := MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS;
                FErrorGrave := True;
                MsgBoxErr(FErrorMsg, e.Message, MSG_ERROR, MB_ICONERROR);
                Break;
            end;
        end;

        // Incrementar la barra de progreso
        pbProgreso.StepIt;
        if pbProgreso.Position >= pbProgreso.Max then begin
            // Inicializar la barra de progreso
            pbProgreso.Position := 1;
        end;

        //Refresco la pantalla
        Application.ProcessMessages;
    end;

    //finalizo la barra de progreso
    pbProgreso.Position := pbProgreso.Max;
    Application.ProcessMessages;

    //Si llego a procesar hasta el final y no hubo errores. Apruebo este paso.
    Result := (NumeroComprobanteDesde = NumeroComprobanteHasta);
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerMovimientosCuentasAContabilizar
  Author:    lgisuk
  Date Created: 08/09/2005
  Description: Obtengo los Movimientos Cuentas que se van a contabilizar
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFContabilizaciondeJornada.ObtenerMovimientosCuentasAContabilizar: Boolean;
Resourcestring
    MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS = 'No se pudieron obtener los Movimientos de Cuenta a contabilizar';
    MSG_ERROR = 'Error';
var
  	NumeroMovimientoDesde : Variant;
    NumeroMovimientoHasta : Variant;
    FErrorMsg : AnsiString;
begin

    //Inicializo variables
    NumeroMovimientoDesde := -1;
    NumeroMovimientoHasta := 0;
    FerrorMsg := '';

    //inicializo la barra de progreso
    pbProgreso.Position := 1;
    pbProgreso.Max := 100;

    while (NumeroMovimientoDesde <> NumeroMovimientoHasta) and (FErrorMsg = '') and (not FCancelar) do begin

        NumeroMovimientoDesde := NumeroMovimientoHasta;

        try
            With SPRegistrarMovimientosCuentasAContabilizar do begin
                Close;
                CommandTimeout := 500;
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroMovimientoDesde').Value := NumeroMovimientoDesde;
                Parameters.ParamByName('@NumeroMovimientoHasta').Value := 0;
                //Ejecuto
                ExecProc;
                //Obtengo el ultimo numero de comprobante procesado
                NumeroMovimientoHasta := Integer (Parameters.ParamByName( '@NumeroMovimientoHasta' ).Value);
            end;
        except
            on e: Exception do begin
                FErrorMsg := MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS;
                FErrorGrave := True;
                MsgBoxErr(FErrorMsg, e.Message, MSG_ERROR, MB_ICONERROR);
                Break;
            end;
        end;

        // Incrementar la barra de progreso
        pbProgreso.StepIt;
        if pbProgreso.Position >= pbProgreso.Max then begin
            // Inicializar la barra de progreso
            pbProgreso.Position := 1;
        end;

        //Refresco la pantalla
        Application.ProcessMessages;
    end;

    //finalizo la barra de progreso
    pbProgreso.Position := pbProgreso.Max;
    Application.ProcessMessages;

    //Si llego a procesar hasta el final y no hubo errores. Apruebo este paso.
    Result := (NumeroMovimientoDesde = NumeroMovimientoHasta);
end;


{******************************** Function Header ******************************
Function Name: Contabilizar_EmisionMasivaNotasCobro
Author : ggomez
Date Created : 22/08/2005
Description: Genera el Asiento de "Emisi�n Masiva de Notas de Cobro".
Parameters : Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_EmisionMasivaNotasCobro(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESING_EMISSION_MASSIVE_NOTES_COLLECTION = 'Ha ocurrido un error al contabilizar la Emisi�n Masiva de Notas de Cobro.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp      := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_EmisionMasivaNotasCobro';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError   := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result      := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESING_EMISSION_MASSIVE_NOTES_COLLECTION + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_AplicacionDelAbono
Author : lgisuk
Date Created : 16/11/2005
Description: Genera el Asiento de "Aplicaci�n del Abono".
Parameters : Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_AplicacionDelAbono(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESING_APPLICATION_OF_THE_CREDIT = 'Ha ocurrido un error al contabilizar la Aplicaci�n del Abono.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp      := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_AplicacionDelAbono';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError   := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result      := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESING_APPLICATION_OF_THE_CREDIT + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_NotasdeCobroBonificadas
Author : lgisuk
Date Created : 17/11/2005
Description: Genera el Asiento de "Notas de Cobro Bonificadas".
Parameters : Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_NotasDeCobroBonificadas(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESING_COLLECTION_NOTES_DISCOUNTED = 'Ha ocurrido un error al contabilizar las Notas de Cobro Bonificadas.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp      := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_NotasDeCobroBonificadas';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError   := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result      := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESING_COLLECTION_NOTES_DISCOUNTED + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;


{******************************** Function Header ******************************
Function Name: MarcarComprobantesContabilizados
Author : ggomez
Date Created : 22/08/2005
Description : Marca como contabilizados los comprobantes contabilizados.
Parameters : Conn: TADOConnection; TipoComprobante: AnsiString; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.MarcarComprobantesContabilizados(Conn: TADOConnection; TipoComprobante: AnsiString; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
resourcestring
    MSG_ERROR_TO_MARK_ASSESSED_RECORDS   = 'Ha ocurrido un error al marcar los comprobantes como Contabilizados.';
var
  	NumeroComprobanteDesde: Int64;
    NumeroComprobanteHasta: Int64;
begin
    Result := False;

    // Inicializar variables
    NumeroComprobanteDesde  := -1;
    NumeroComprobanteHasta  := 0;
    DescriError             := EmptyStr;

    // Inicializar la barra de progreso
    pbProgreso.Position := 1;
    pbProgreso.Max      := 100000;

    while (NumeroComprobanteDesde <> NumeroComprobanteHasta) and (DescriError = EmptyStr) and (not FCancelar) do begin

        NumeroComprobanteDesde := NumeroComprobanteHasta + 1;

        try
            with spMarcarComprobantesContabilizados do begin
                Close;
                CommandTimeout := 500;
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroProcesoContabilizacion').Value   := NumeroProcesoContabilizacion;
                Parameters.ParamByName('@TipoComprobante').Value                := TipoComprobante;
                Parameters.ParamByName('@NumeroComprobanteDesde').Value         := NumeroComprobanteDesde;
                Parameters.ParamByName('@NumeroComprobanteHasta').Value         := 0;
                // Ejecutar
                ExecProc;
                // Obtener el �ltimo n�mero de comprobante procesado
                NumeroComprobanteHasta := Parameters.ParamByName('@NumeroComprobanteHasta').Value;
            end; // with
        except
            on E: Exception do begin
                Result      := False;
                DescriError := MSG_ERROR_TO_MARK_ASSESSED_RECORDS
                                + CRLF + E.Message;
                Break;
            end;
        end; // except

        // Incrementar la barra de progreso
        pbProgreso.StepIt;
        if pbProgreso.Position >= pbProgreso.Max then begin
            // Inicializar la barra de progreso
            pbProgreso.Position := 1;
        end;

        // Refrescar la pantalla
        Application.ProcessMessages;
    end; // while


    // Finalizar la barra de progreso
    pbProgreso.Position := pbProgreso.Max;
    Application.ProcessMessages;

    // Si no hubo error y lleg� a procesar todo, retornar True.
    if DescriError = EmptyStr then begin
        Result := (NumeroComprobanteDesde = NumeroComprobanteHasta);
    end;
end;

{******************************** Function Header ******************************
Function Name: MarcarPagosContabilizados
Author : lgisuk
Date Created : 01/09/2005
Description : Marca los pagos como contabilizados.
Parameters : Conn: TADOConnection; TipoComprobante: AnsiString; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.MarcarPagosContabilizados(Conn: TADOConnection; NumeroProcesoContabilizacion: Integer; var DescriError: AnsiString): Boolean;
resourcestring
    MSG_ERROR_TO_MARK_ASSESSED_RECORDS = 'Ha ocurrido un error al marcar los pagos como Contabilizados';
Const
    STR_MARKING_ASSESSED_DEBTS =  'Marcando Pagos Contabilizados ...';
var
  	NumeroPagoDesde: Int64;
    NumeroPagoHasta: Int64;
begin
    Result := False;

    // Inicializar variables
    NumeroPagoDesde  := -1;
    NumeroPagoHasta  := 0;
    DescriError      := EmptyStr;

    // Inicializar la barra de progreso
    pbProgreso.Position := 1;
    pbProgreso.Max      := 100;

    Lblmensaje.Caption := STR_MARKING_ASSESSED_DEBTS;

    while (NumeroPagoDesde <> NumeroPagoHasta) and (DescriError = EmptyStr) and (not FCancelar) do begin

        NumeroPagoDesde := NumeroPagoHasta + 1 ;

        try
            with spMarcarPagosContabilizados do begin
                Close;
                CommandTimeout := 500;
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroProcesoContabilizacion').Value   := NumeroProcesoContabilizacion;
                Parameters.ParamByName('@NumeroPagoDesde').Value         := NumeroPagoDesde;
                Parameters.ParamByName('@NumeroPagoHasta').Value         := 0;
                // Ejecutar
                ExecProc;
                // Obtener el �ltimo n�mero de pago procesado
                NumeroPagoHasta := Parameters.ParamByName('@NumeroPagoHasta').Value;
            end; // with
        except
            on E: Exception do begin
                Result      := False;
                DescriError := MSG_ERROR_TO_MARK_ASSESSED_RECORDS + CRLF + E.Message;
                Break;
            end;
        end; // except

        // Incrementar la barra de progreso
        pbProgreso.StepIt;
        if pbProgreso.Position >= pbProgreso.Max then begin
            // Inicializar la barra de progreso
            pbProgreso.Position := 1;
        end;

        // Refrescar la pantalla
        Application.ProcessMessages;
    end; // while

    // Finalizar la barra de progreso
    pbProgreso.Position := pbProgreso.Max;
    Application.ProcessMessages;

    // Si no hubo error y lleg� a procesar todo, retornar True.
    if DescriError = EmptyStr then begin
        Result := (NumeroPagoDesde = NumeroPagoHasta);
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_VentasPasesDiarios
Author : ggomez
Date Created : 24/08/2005
Description: Genera el Asiento de "Ventas de Pases Diarios (PDU)".
Parameters : Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_VentasPasesDiarios(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer;var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESING_SALES_DAY_PASS = 'Ha ocurrido un error al contabilizar la Venta de Pases Diarios.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp      := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_VentasPasesDiarios';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError   := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result      := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESING_SALES_DAY_PASS + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_VentasBoletosHabilitacionTardia
Author : ggomez
Date Created : 24/08/2005
Description: Genera el Asiento de "Ventas de Boletos de Habilitaci�n Tard�a (BHT)".
Parameters : Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_VentasBoletosHabilitacionTardia(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESING_SALES_LATE_DAY_PASS = 'Ha ocurrido un error al contabilizar la Venta de Boletos de Habilitaci�n Tard�a.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp      := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_VentasBoletosHabilitacionTardia';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError   := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result      := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESING_SALES_LATE_DAY_PASS + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;



{******************************** Function Header ******************************
Function Name: Contabilizar_BoletasEmitidas
Author : lgisuk
Date Created : 15/09/2005
Description: Genera el Asiento de "Boletas Emitidas".
Parameters :
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_BoletasEmitidas(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESSED_THE_EMITTED_BALLOTS = 'Ha ocurrido un error al contabilizar las boletas emitidas.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_BoletasEmitidas';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESSED_THE_EMITTED_BALLOTS + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_FacturasEmitidas
Author : lgisuk
Date Created : 16/09/2005
Description: Genera el Asiento de "Facturas Emitidas".
Parameters :
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_FacturasEmitidas(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESSED_THE_EMITTED_INVOICES = 'Ha ocurrido un error al contabilizar las facturas emitidas.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_FacturasEmitidas';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESSED_THE_EMITTED_INVOICES + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;


{******************************** Function Header ******************************
Function Name: Contabilizar_NotasDebitoEmitidas
Author : lgisuk
Date Created : 16/09/2005
Description: Genera el Asiento de "Notas de Debito Emitidas".
Parameters :
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_NotasDebitoEmitidas(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESSED_THE_EMITTED_NOTES_OF_DEBIT = 'Ha ocurrido un error al contabilizar las Notas de Debito emitidas.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_NotasDebitoEmitidas';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result := True;
        except
            on E: Exception do begin
                DescError :=  MSG_ERROR_ASSESSED_THE_EMITTED_NOTES_OF_DEBIT + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_NotasCreditoEmitidas
Author : lgisuk
Date Created : 16/09/2005
Description: Genera el Asiento de "Notas de Credito Emitidas".
Parameters :
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_NotasCreditoEmitidas(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESSED_THE_EMITTED_NOTES_OF_CREDIT = 'Ha ocurrido un error al contabilizar las Notas de Credito emitidas.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_NotasCreditoEmitidas';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result := True;
        except
            on E: Exception do begin
                DescError :=  MSG_ERROR_ASSESSED_THE_EMITTED_NOTES_OF_CREDIT + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_NotasCreditoEmitidasParaAnularNK
Author : lgisuk
Date Created : 16/09/2005
Description: Genera el Asiento de "Notas de Credito Para Notas de Cobro Emitidas".
Parameters :
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_NotasCreditoEmitidasParaAnularNK (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESSED_THE_EMITTED_NOTES_OF_CREDIT = 'Ha ocurrido un error al contabilizar las Notas de Credito para notas de cobro emitidas.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_NotasCreditoEmitidasParaAnularNK';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result := True;
        except
            on E: Exception do begin
                DescError :=  MSG_ERROR_ASSESSED_THE_EMITTED_NOTES_OF_CREDIT + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_EmisionAPedidoNotasCobro
Author : ggomez
Date Created : 19/09/2005
Description: Genera el Asiento de "Emisi�n de Notas de Cobro a Pedido".
Parameters : Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer;
    var DescError: Ansistring
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_EmisionAPedidoNotasCobro(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESING_MANUAL_EMISSION_NOTES_COLLECTION = 'Ha ocurrido un error al contabilizar la Emisi�n de Notas de Cobro a Pedido.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp      := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_EmisionAPedidoNotasCobro';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := EmptyStr;
            end;
            sp.ExecProc;
            DescError   := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result      := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESING_MANUAL_EMISSION_NOTES_COLLECTION + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_NotasCreditoEmitidasParaAnularNK
Author : lgisuk
Date Created : 20/09/2005
Description: Genera el Asiento de "Registros de los Documentos Devueltos".
Parameters :
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_RegistroDocumentosDevueltos (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESSED_THE_RETURNED_DOCUMENTS = 'Ha ocurrido un error al contabilizar los Documentos Devueltos.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_RegistroDocumentosDevueltos';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result := True;
        except
            on E: Exception do begin
                DescError :=  MSG_ERROR_ASSESSED_THE_RETURNED_DOCUMENTS + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_NotasCobroPorUsoPDU
Author : lgisuk
Date Created : 28/09/2005
Description: Genera el Asiento de "Notas de Cobro por Uso de PDU".
Parameters :
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_NotasCobroPorUsoPDU (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESSED_COLLECTION_NOTES_FOR_USE_OF_DAILY_UNIFIED_PASS = 'Ha ocurrido un error al contabilizar las notas de cobro por uso de PDU.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_NotasCobroPorUsoPDU';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result := True;
        except
            on E: Exception do begin
                DescError :=  MSG_ERROR_ASSESSED_COLLECTION_NOTES_FOR_USE_OF_DAILY_UNIFIED_PASS + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_NotasCobroPorUsoBHT
Author : lgisuk
Date Created : 29/09/2005
Description: Genera el Asiento de "Notas de Cobro por Uso de BHT".
Parameters :
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_NotasCobroPorUsoBHT (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESSED_COLLECTION_NOTES_FOR_USE_OF_LATE_DAY_PASS = 'Ha ocurrido un error al contabilizar las notas de cobro por uso de BHT.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_NotasCobroPorUsoBHT';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result := True;
        except
            on E: Exception do begin
                DescError :=  MSG_ERROR_ASSESSED_COLLECTION_NOTES_FOR_USE_OF_LATE_DAY_PASS + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_AnulacionPorPagoDuplicado
Author : lgisuk
Date Created : 01/11/2005
Description: Genera el Asiento de "Anulaci�n por pago duplicado".
Parameters :
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_AnulacionPorPagoDuplicado (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESSED_ANNULMENT_FOR_DUPLICATED_PAYMENT = 'Ha ocurrido un error al contabilizar la anulaci�n por pago duplicado.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_AnulacionPorPagoDuplicado';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result := True;
        except
            on E: Exception do begin
                DescError :=  MSG_ERROR_ASSESSED_ANNULMENT_FOR_DUPLICATED_PAYMENT + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;


{******************************** Function Header ******************************
Function Name: Contabilizar_AnulacionPorPagoDuplicado
Author : lgisuk
Date Created : 01/11/2005
Description: Genera el Asiento de "Devoluci�n de Monto por pago duplicado".
Parameters :
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_DevolucionDeMontoPorPagoDuplicado (Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESSED_ANNULMENT_FOR_DUPLICATED_PAYMENT = 'Ha ocurrido un error al contabilizar la anulaci�n por pago duplicado.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_DevolucionDeMontoPorPagoDuplicado';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result := True;
        except
            on E: Exception do begin
                DescError :=  MSG_ERROR_ASSESSED_ANNULMENT_FOR_DUPLICATED_PAYMENT + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_CobrosVentanilla
Author : ggomez
Date Created : 09/11/2005
Description: Genera el Asiento de "Cobro de Deuda Diaria".
Parameters : Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_CobrosVentanilla(
  Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer;
  var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESING_COLLECTIONS_IN_WINDOW = 'Ha ocurrido un error al contabilizar los Pagos en Ventanilla.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp      := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_CobroDeudaDiaria';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError   := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result      := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESING_COLLECTIONS_IN_WINDOW + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: Contabilizar_CambiosEstadoValidacionTransitos
Author : ggomez
Date Created : 11/11/2005
Description: Genera el Asiento de "Cambios de Estado de Validaci�n de Tr�nsitos".
Parameters : Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring
Return Value : Boolean
*******************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_CambiosEstadoValidacionTransitos(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer;var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESING_CHANGE_VALIDATION_STATE  = 'Ha ocurrido un error al contabilizar los Cambios de Estado de Validaci�n.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp      := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_CambiosEstadoTransitos';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError   := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result      := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESING_CHANGE_VALIDATION_STATE + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header **************************************************************
Function Name: Contabilizar_CambiosEstadoValidacionTransitosFacturableANoFacturable
Author : lgisuk
Date Created : 21/11/2005
Description: Genera el Asiento de "Cambios de Estado de Validaci�n de Tr�nsitos de Facturable a No Facturable".
Parameters : Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring
Return Value : Boolean
****************************************************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_CambiosEstadoValidacionTransitosFacturableANoFacturable(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer;var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESING_CHANGE_VALIDATION_STATE_OF_INVOICEABLE_TO_NONINVOICEABLE   = 'Ha ocurrido un error al contabilizar los Cambios de Estado de Validaci�n.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp      := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_CambiosEstadoTransitosFacturableANoFacturable';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError   := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result      := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESING_CHANGE_VALIDATION_STATE_OF_INVOICEABLE_TO_NONINVOICEABLE + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header **************************************************************
Function Name: Contabilizar_CambiosEstadoValidacionTransitosFacturableANoFacturable
Author : lgisuk
Date Created : 21/11/2005
Description: Genera el Asiento de "Cambios de Estado de Validaci�n de Tr�nsitos de Facturable a No Facturable".
Parameters : Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer; var DescError: Ansistring
Return Value : Boolean
****************************************************************************************************************}
function TFContabilizaciondeJornada.Contabilizar_CambiosEstadoValidacionTransitosNoFacturableAFacturable(Conn: TADOConnection; Fecha: TDateTime; NumeroProceso: Integer;var DescError: Ansistring): Boolean;
resourcestring
    MSG_ERROR_ASSESING_CHANGE_VALIDATION_STATE_OF_NONINVOICEABLE_TO_INVOICEABLE   = 'Ha ocurrido un error al contabilizar los Cambios de Estado de Validaci�n.';
var
    sp: TADOStoredProc;
begin
    Result  := False;
    sp      := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection       := Conn;
            sp.CommandTimeout   := 500;
            sp.ProcedureName    := 'Contabilizar_CambiosEstadoTransitosNoFacturableAFacturable';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@FechaAContabilizar').Value            := Fecha;
                ParamByName('@NumeroProcesoContabilizacion').Value  := NumeroProceso;
                ParamByName('@DescripcionError').Value              := '';
            end;
            sp.ExecProc;
            DescError   := sp.Parameters.ParamByName('@DescripcionError').Value;
            Result      := True;
        except
            on E: Exception do begin
                DescError := MSG_ERROR_ASSESING_CHANGE_VALIDATION_STATE_OF_NONINVOICEABLE_TO_INVOICEABLE + CRLF + E.Message;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EliminarTablasTemporalesUtilizadas
  Author:    lgisuk
  Date Created: 08/09/2005
  Description: Elimina las tablas temporales utilizadas
  Parameters:
  Return Value: None
-----------------------------------------------------------------------------}
function TFContabilizaciondeJornada.EliminarTablasTemporalesUtilizadas(Conn: TADOConnection): Boolean;
var
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection    := Conn;
        sp.ProcedureName := 'EliminarTemporalesContabilidad';
        sp.ExecProc;
        Result := True;
    finally
        sp.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarInformedelProceso
  Author:    lgisuk
  Date Created: 23/06/2005
  Description: Muestro un informe de finalizaci�n de proceso
  Parameters: CodigoOperacionInterfase:Integer; NombreReporte, CantidadRegistros, SumatoriaCuentas: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
Function TFContabilizaciondeJornada.GenerarReporteFinalizacionProceso(NumeroProceso:Integer):boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de finalizaci�n de proceso';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Contabilizaci�n de Jornada';
var
    F : TFRptVerificacionInterfazContable;
begin
    Result := False;
    try
        //Muestro el reporte
        Application.CreateForm(TFRptVerificacionInterfazContable, F);
        if not F.Inicializar(NumeroProceso, REPORT_TITLE) then f.Release;
        Result := True;
    except
       on e: Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    lgisuk
  Date Created: 29/07/2005
  Description: Realiza el proceso de contabilizaci�n de jornada
  Parameters: Sender: TObject
  Return Value: None

  Revision 1:
      Author : ggomez
      Date : 14/12/2005
      Description : Cuando se sugiere contan�bilizar una jornada y el operador
        responde que no desea hacerlo, se limpia la fecha de jornada para evitar
        que contabilice una jornada no deseada.

-----------------------------------------------------------------------------}
procedure TFContabilizaciondeJornada.btnProcesarClick(Sender: TObject);

      {******************************** Function Header ******************************
      Function Name: ObtenerCantidadTransitosAContabilizar
      Author : ggomez
      Date Created : 06/09/2005
      Description : Retorna la cantidad de Tr�nsitos a Contabilizar.
      Parameters : Conn: TADOConnection; Fecha: TDateTime
      Return Value : Int64
      *******************************************************************************}
      function ObtenerCantidadTransitosAContabilizar(Conn: TADOConnection;Fecha: TDateTime): Int64;
      begin
          Result := QueryGetValueInt(Conn, 'SELECT dbo.ObtenerCantidadTransitosAContabilizar('+ QuotedStr(FormatDateTime('YYYYMMDD', Fecha)) + ')');
      end;

      {-----------------------------------------------------------------------------
      Function Name: CalcularTotalesCuentasContables
      Author:    lgisuk
      Date Created: 01/11/2005
      Description: Calculo los totales para cada cuenta utilizada en los asientos
      Parameters:
      Return Value: boolean
      -----------------------------------------------------------------------------}
      Function  CalcularTotalesCuentasContables(Conn: TAdoConnection; NumeroProceso : Integer) : Boolean;
      var
          SP : TADOStoredProc;
          DescError : AnsiString;
      begin
          Result := False;
          SP :=TADOStoredProc.Create(Nil);
          try
              try
                  SP.Connection := Conn;
                  SP.CommandTimeout := 500;
                  SP.ProcedureName := 'CalcularTotalesCuentasContables';
                  with SP.Parameters do begin
                      Refresh;
                      ParamByName('@NumeroProcesoContabilizacion').Value := NumeroProceso;
                  end;
                  SP.ExecProc;  //Ejecuto el procedimiento
                  Result := True;
              except
                  on e: Exception do begin
                      DescError := e.Message;
                  end;
              end;
          finally
              SP.Free;
          end;
      end;

    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 29/07/2005
      Description: Actualizo el log al finalizar
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ActualizarLog:boolean;

      {-----------------------------------------------------------------------------
      Function Name: FinalizarProcesoContabilizacion
      Author:    lgisuk
      Date Created: 29/07/2005
      Description: Actualizo el proceso al finalizar
      Parameters:
      Return Value: boolean
      -----------------------------------------------------------------------------}
      Function  FinalizarProcesoContabilizacion(Conn: TAdoConnection;NumeroProceso:integer;FinalizoOk:boolean;Observacion:string;Var DescError:Ansistring):boolean;
      var
          sp : TADOStoredProc;
      begin
          result := false;
          sp :=TADOStoredProc.Create(Nil);
          try
              try
                  sp.Connection := Conn;
                  sp.CommandTimeout := 500;
                  sp.ProcedureName := 'FinalizarProcesoContabilizacion';
                  with SP.Parameters do begin
                      Refresh;
                      ParamByName('@NumeroProceso').Value := NumeroProceso;
                      ParamByName('@FinalizoOk').Value    := FinalizoOk;
                      ParamByName('@Observacion').Value   := Observacion;
                  end;
                  SP.ExecProc;                                                        //Ejecuto el procedimiento
                  result:=true;
              except
                  on e: Exception do begin
                      DescError := e.Message;
                  end;
              end;
          finally
              SP.Free;
          end;
      end;

    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVATION = 'Finalizo OK!';
    var
         DescError : String;
    begin
        Result := FinalizarProcesoContabilizacion (DMConnections.BaseCAC, FNumeroProceso, True, STR_OBSERVATION , DescError);
        if not result then MsgBox (DescError);
    end;

resourcestring
    MSG_IS_NOT_VALID_DATE = 'La fecha de jornada a contabilizar no es valida!';
  	MSG_IS_NOT_VALID_ASSESSED_DAY = 'La jornada %s no es una jornada contable v�lida.';
    MSG_ASSESSED_DAY = 'La jornada %s ha sido contabilizada en el proceso de contabilizaci�n N�: %d.' + CRLF + CRLF + '�Desea contabilizarla nuevamente?';
    MSG_CLOSED_ASSESSED_DAY = 'La jornada %s ha sido contabilizada en el proceso de contabilizaci�n N�: %d.' + CRLF + 'No se permite contabilizarla nuevamente pues ya se ha cerrado el proceso mencionado.';
    MSG_ASSESSED_DAY_NOT_TO_RE_ASSESS = 'La jornada %s ha sido contabilizada en el proceso de contabilizaci�n N�: %d' + CRLF + 'No se permite contabilizarla nuevamente pues ya se ha contabilizado una jornada posterior.';
    MSG_DAY_SHOULD_BE_PREVIOUS_CURRENT_DATE = 'La jornada a contabilizar debe ser anterior a la fecha actual.';
    MSG_DAY_OPEN_VALIDATION  = 'No se permite contabilizar la jornada pues el Proceso de Validaci�n de Tr�nsitos no ha sido cerrado.';
    MSG_POINT_OF_SALE_NOT_CLOSED = 'No se permite contabilizar la jornada pues hay Puntos de Ventas que no han cerrado su Turno.';
    MSG_SUGGESTION_TO_ASSESS_FIRST_DAY = 'Deber�a realizar una contabilizaci�n para la jornada: %s.' + CRLF + CRLF + '�Desea continuar con la contabilizaci�n de la jornada: %s?';
    MSG_SUGGESTION_TO_ASSESS_DAY = 'Deber�a realizar una contabilizaci�n para la mayor jornada del mes: %s.' + CRLF + 'Esto se debe a que quedan jornadas de dicho mes, que no se han contabilizado.' + CRLF + CRLF + '�Desea continuar con la contabilizaci�n de la jornada: %s?';
    MSG_PROCESS_CANCEL = 'Proceso cancelado';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
  	MSG_PROCESS_FINALLY_OK = 'El proceso finaliz� con �xito';
    MSG_CANNOT_GET_GENERAL_PARAMETER = 'No se ha podido obtener el valor del par�metro general: %s.';
Const
    STR_REGISTER_OPERATION = 'Creando Proceso de Contabilizaci�n...';
    STR_GENERAL_STEP_NUMBER = 'Paso General %d de 22';
    STR_OBTAINING_RECORDS_TO_ASSESSING = 'Obteniendo Tr�nsitos a Contabilizar...';
    STR_ELIMINATING_RECORDS_INCLUDED_IN_CHANGUES_OF_STATE = 'Eliminando transitos incluidos en Cambios de Estado...';
    STR_ASSESSING_REVENUE_OF_DAILY_TOLL = 'Contabilizando Ingreso de Peaje Diario...';
    STR_MARKING_ASSESSED_RECORDS = 'Marcando Tr�nsitos Contabilizados...';
    STR_OBTAINING_NOTES_TO_ASSESSING = 'Obteniendo Comprobantes a Contabilizar...';
    STR_OBTAINING_ACCOUNT_MOVEMENTS_TO_ASSESSING = 'Obteniendo los Movimientos de Cuenta a Contabilizar...';
    STR_ASSESING_EMISSION_MASSIVE_NOTES_COLLECTION  = 'Contabilizando Emisi�n Masiva de Notas de Cobro ...';
    STR_MARKING_ASSESSED_NOTES_COLLECTION = 'Marcando Notas de Cobro Contabilizadas ...';
    STR_COLLECTIONS_OF_EXTERNAL_COMPANIES = 'Contabilizando Cobros Empresas de Cobranza Externas...';
    STR_ASSESING_SALES_DAY_PASS = 'Contabilizando Ventas de Pases Diarios ...';
    STR_ASSESING_SALES_LATE_DAY_PASS = 'Contabilizando Ventas de Boletos de Habilitaci�n Tard�a ...';
    STR_ASSESING_EMITTED_BALLOTS = 'Contabilizando Boletas Emitidas...';
    STR_ASSESING_EMITTED_INVOICES = 'Contabilizando Facturas Emitidas...';
    STR_ASSESING_EMITTED_NOTES_OF_DEBIT  = 'Contabilizando Notas de Debito Emitidas...';
    STR_ASSESING_EMITTED_NOTES_OF_CREDIT = 'Contabilizando Notas de Credito Emitidas...';
    STR_ASSESING_EMITTED_NOTES_OF_CREDIT_FOR_NOTES_COLLECTION = 'Contabilizando Notas de Credito para Notas de Cobro Emitidas...';
    STR_ASSESING_REQUESTED_NOTES_COLLECTION  = 'Contabilizando Notas de Cobro Emitidas a Pedido ...';
    STR_ASSESING_THE_RETURNED_DOCUMENTS = 'Contabilizando los Documentos Devueltos...';
    STR_ASSESING_COLLECTION_NOTES_FOR_USE_OF_DAILY_UNIFIED_PASS = 'Contabilizando las Notas de Cobro por uso de PDU...';
    STR_ASSESING_COLLECTION_NOTES_FOR_USE_OF_LATE_DAILY_PASS = 'Contabilizando las Notas de Cobro por uso de BHT...';
    STR_ASSESING_ANNULMENT_FOR_DUPLICATED_PAYMENT = 'Contabilizando Anulaci�n por Pago Duplicado...';
    STR_ASSESING_APLICATION_OF_THE_CREDIT = 'Contabilizando Aplicaci�n del Abono...';
    STR_ASSESING_COLLECTION_NOTES_DISCOUNTED = 'Contabilizando Notas de Credito Bonificadas...';
    STR_ASSESING_COLLECTIONS_IN_WINDOW = 'Contabilizando Cobros en Ventanilla ...';
    STR_ASSESING_CHANGE_VALIDATION_STATE = 'Contabilizando Cambios de Estado de Validaci�n ...';
    STR_ASSESING_CHANGE_VALIDATION_STATE_OF_INVOICEABLE_TO_NONINVOICEABLE =  'Contabilizando Cambios de Estado de Validaci�n de Facturable a No Facturable ...';
    STR_ASSESING_CHANGE_VALIDATION_STATE_OF_NONINVOICEABLE_TO_INVOICEABLE =  'Contabilizando Cambios de Estado de Validaci�n de No Facturable a Facturable ...';
    STR_CALCULATED_TOTAL_FOR_ACCOUNT = 'Calculando Totales por Cuenta...';
    STR_FINISHING_OPERATION = 'Finalizando Proceso de Contabilizaci�n...';
var
    DescError : AnsiString;
    NumeroProcesoContabilizacion,
    NumeroUltimoProcesoContabilizacion: Integer;
    EstaContabilizada,
    EstaCerrada: Boolean;
    UltimaJornadaValidacionCerrada: TDateTime;
    CajasEstanCerradas: Boolean;
    EstadoContabilizacion: TEstadoContabilizacion;
    FechaJornadaContabilizada,
    FechaHoraInicioContabilizacion,
    FechaHoraCierreContabilizacion: TDateTime;
    UsuarioContabilizacion: AnsiString;
    UltimoDiaDeMes,
    UltimoDiaDeMesSiguiente: TDate;
    FechaInicioOperacionAutopista: TDateTime;
begin

    FCancelar := False;

    //Verifico si es una fecha valida
    if edfecha.Date < EncodeDate(1900,1,1) then begin
        MsgBox(MSG_IS_NOT_VALID_DATE);
        Exit;
    end;

    //Obtengo el numero del ultimo proceso de contabilizaci�n
    NumeroUltimoProcesoContabilizacion := ObtenerUltimoNumeroProcesoContabilizacion(DMConnections.BaseCAC);

    try
        // Controlar si hay un proceso de Contabilizaci�n sin finalizar, y
        // realizar las acciones necesarias para finalizarlo.
        FinalizarContabilizacionInconclusa(NumeroUltimoProcesoContabilizacion);
        if FCancelar then Exit;
    except
        Exit;
    end; // except

    // Si la jornada a contabilizar NO es anterior a la de la BD, NO permitir
    // contabilizarla.
    if (edFecha.Date >= Trunc(NowBase(DMConnections.BaseCAC))) then begin
        MsgBox(MSG_DAY_SHOULD_BE_PREVIOUS_CURRENT_DATE, Self.Caption,
            MB_OK + MB_ICONINFORMATION);
        Exit;
    end;

    // Si la jornada no es contabilizable, mostrar un mensaje y salir.
    if not EsJornadaContabilizable(DMConnections.BaseCAC, edFecha.Date) then begin
        MsgBox(Format(MSG_IS_NOT_VALID_ASSESSED_DAY, [DateToStr(edFecha.Date)]),
            Self.Caption, MB_OK + MB_ICONINFORMATION);
        Exit;
    end; // if

    // Verificar que el proceso de validaci�n de tr�nsitos hay finalizado.
   UltimaJornadaValidacionCerrada := ObtenerUltimaJornadaValidacionCerrada(DMConnections.BaseCAC);
    if edFecha.Date > UltimaJornadaValidacionCerrada then begin
        MsgBox(MSG_DAY_OPEN_VALIDATION, Self.Caption, MB_OK + MB_ICONINFORMATION);
        Exit;
    end;

    // Verificar que las cajas de CN esten cerradas.
    CajasEstanCerradas := EstanCajasCerradas(DMConnections.BaseCAC, edFecha.Date);
    if not CajasEstanCerradas then begin
        MsgBox(MSG_POINT_OF_SALE_NOT_CLOSED, Self.Caption,
            MB_OK + MB_ICONINFORMATION);
        Exit;
    end;

    // Obtener si la jornada esta contabilizada y cerrada
    EstaContabilizada := EsJornadaContabilizada(DMConnections.BaseCAC,
                        edFecha.Date, NumeroProcesoContabilizacion, EstaCerrada);
    // Si la jornada ha sido Contabilizada y HA SIDO CERRADO el Proceso de
    // Contabilizaci�n, mostrar un mensaje indicando la situaci�n y salir.
    if EstaContabilizada and EstaCerrada then begin
        MsgBox(Format(MSG_CLOSED_ASSESSED_DAY,[DateToStr(edFecha.Date),
            NumeroProcesoContabilizacion]), Self.Caption, MB_OK + MB_ICONINFORMATION);
        Exit;
    end; // if

    // Si la jornada ha sido Contabilizada y NO pertenece al �ltimo proceso de
    // Contabilizaci�n, mostrar un mensaje indicando la situaci�n y salir.
    NumeroUltimoProcesoContabilizacion := ObtenerUltimoNumeroProcesoContabilizacionNoDesecho(DMConnections.BaseCAC);
    if EstaContabilizada
            and (NumeroProcesoContabilizacion < NumeroUltimoProcesoContabilizacion) then begin
        MsgBox(Format(MSG_ASSESSED_DAY_NOT_TO_RE_ASSESS,
            [DateToStr(edFecha.Date), NumeroProcesoContabilizacion]),
            Self.Caption, MB_OK + MB_ICONINFORMATION);
        Exit;
    end; // if

    //-------------------------------------------------------------------------
    // Verificar que las jornadas de los meses anteriores al mes de la jornada
    // ingresada, se hayan contabilizado. Si esto no se d� mostrar un mensaje
    // que sugiera realizar una contabilizaci�n para contabilizar las jornadas
    // no contabilizadas.
    //-------------------------------------------------------------------------
    if NumeroUltimoProcesoContabilizacion < 1 then begin
        // No se ha ejecutado nig�n Proceso de Contabilizaci�n.
        // Controlar que la fecha a Contabilizar sea la de Inicio de Operaciones
        // de la autopista
        if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'FECHA_INICIO_COBRANZA',
                FechaInicioOperacionAutopista) then begin
            MsgBox(Format(MSG_CANNOT_GET_GENERAL_PARAMETER, ['FECHA_INICIO_COBRANZA']),
                Self.Caption, MB_OK + MB_ICONINFORMATION);
            Exit;
        end;

        FechaInicioOperacionAutopista := Trunc(FechaInicioOperacionAutopista);
        if edFecha.Date > FechaInicioOperacionAutopista then begin
            if MsgBox(Format(MSG_SUGGESTION_TO_ASSESS_FIRST_DAY,
                    [DateToStr(FechaInicioOperacionAutopista),
                    DateToStr(edFecha.Date)]),
                    Self.Caption, MB_YESNO + MB_DEFBUTTON2 + MB_ICONQUESTION) <> mrYes then begin

                Exit;
            end; // if
        end; // if
    end else begin
        // Obtener la fecha del �ltimo proceso de contabilizaci�n.
        ObtenerDatosProcesoContabilizacion(DMConnections.BaseCAC,
            NumeroUltimoProcesoContabilizacion, FechaJornadaContabilizada,
            FechaHoraInicioContabilizacion, FechaHoraCierreContabilizacion,
            UsuarioContabilizacion, EstadoContabilizacion);

        // Si la fecha de la �lt. contab. es anterior a la fecha a contab.
        // y las fechas son de distinto Mes/A�o.
        if (FechaJornadaContabilizada < edFecha.Date)
                and ((Month(FechaJornadaContabilizada) <> Month(edFecha.Date))
                    or (Year(FechaJornadaContabilizada) <> Year(edFecha.Date))) then begin

            // Obtener la fecha mayor del mes de la Ult. Contabilizaci�n.
            UltimoDiaDeMes := IncMonth(EncodeDate(Year(FechaJornadaContabilizada),
                                Month(FechaJornadaContabilizada), 1), 1) - 1;

            // Si la fecha de la �lt. jornada contab. es anterior a la fecha
            // mayor del mes, mostrar una sugerencia de contabilizar la m�xima
            // jornada del mes.
            if FechaJornadaContabilizada < UltimoDiaDeMes then begin
                if MsgBox(Format(MSG_SUGGESTION_TO_ASSESS_DAY,
                        [PadL(IntToStr(Month(FechaJornadaContabilizada)), 2, '0')
                            + '/' + IntToStr(Year(FechaJornadaContabilizada)),
                        DateToStr(edFecha.Date)]),
                        Self.Caption, MB_YESNO + MB_DEFBUTTON2 + MB_ICONQUESTION) <> mrYes then begin

                    // Colocar la Fecha de Contabilizaci�n en blanco, ya que el
                    // respondi� que no desea contabilizar la jornada. Esto se
                    // hizo para que el operador no se equivoque y contabilice
                    // una jornada no deseada.
                    edFecha.Date := NullDate;
                    Exit;
                end; // if
            end else begin
                // FechaJornadaContabilizada = UltimoDiaDeMes

                // Si la fecha a contab. NO es del mes inmediato posterior a la
                // fecha de la �lt. jornada contab., mostrar sugerencia para
                // contabilizar la mayor jornada del mes inmediato posterior.
                UltimoDiaDeMesSiguiente := IncMonth(UltimoDiaDeMes, 1);
                if (UltimoDiaDeMesSiguiente < edFecha.Date) then begin
                    if MsgBox(Format(MSG_SUGGESTION_TO_ASSESS_DAY,
                            [PadL(IntToStr(Month(UltimoDiaDeMesSiguiente)), 2, '0')
                                + '/' + IntToStr(Year(UltimoDiaDeMesSiguiente)),
                            DateToStr(edFecha.Date)]),
                            Self.Caption, MB_YESNO + MB_DEFBUTTON2 + MB_ICONQUESTION) <> mrYes then begin

                        // Colocar la Fecha de Contabilizaci�n en blanco, ya que el
                        // respondi� que no desea contabilizar la jornada. Esto se
                        // hizo para que el operador no se equivoque y contabilice
                        // una jornada no deseada.
                        edFecha.Date := NullDate;
                        Exit;
                    end; // if
                end; // if
            end; // else if

        end; // if FechaJornadaContabilizada < edFecha.Date
    end; // if NumeroUltimoProcesoContabilizacion > 0


    // Si la jornada ha sido Contabilizada, mostrar un mensaje preguntando si
    // desea contabilizarla nuevamente.
    if EstaContabilizada then begin
        // Preguntar al operador si desea contabilizar la jornada nuevamente
        if MsgBox(Format(MSG_ASSESSED_DAY,
                [DateToStr(edFecha.Date), NumeroProcesoContabilizacion]),
                Self.Caption, MB_YESNO + MB_DEFBUTTON2 + MB_ICONQUESTION) = mrNo then begin
            Exit;
        end else begin
            // Deshacer el proceso de contabilizaci�n, para comenzarlo nuevamente.
            if not DeshacerProcesoContabilizacion(DMConnections.BaseCAC,
                    NumeroProcesoContabilizacion, DescError) then begin
                if DescError <> EmptyStr then begin
                    FErrorGrave := True;
                    Exit;
                end;
                if FCancelar then begin
                    Exit;
                end;
            end;
        end;
    end; // if  

    edFecha.Enabled     := False;
    btnprocesar.Enabled := False;
    PnlAvance.Visible   := True;
  	Screen.Cursor       := crHourGlass;
    KeyPreview          := True;
    DescError           := '';
    try
        // Eliminar las tablas temporales utilizadas, si es que existen en memoria.
        EliminarTablasTemporalesUtilizadas(DMConnections.BaseCAC);

        //Registro la operacion en el log
        Lblmensaje.Caption := STR_REGISTER_OPERATION;
        Application.ProcessMessages;
        if not RegistrarOperacion then begin
             FErrorGrave := True;
             Exit;
        end;

        //--------------------------------------------------------------------
        // Generar Asiento de Cambios de Estado de Validaci�n de Tr�nsitos.
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [1]);
        lblMensaje.Caption := STR_ASSESING_CHANGE_VALIDATION_STATE;
        Application.ProcessMessages;
  	    if not Contabilizar_CambiosEstadoValidacionTransitos(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        //-----------------------------------------------------------------------------------------------
        // Generar Asiento de Cambios de Estado de Validaci�n de Tr�nsitos de Facturable a No Facturable.
        //-----------------------------------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [2]);
        lblMensaje.Caption := STR_ASSESING_CHANGE_VALIDATION_STATE_OF_INVOICEABLE_TO_NONINVOICEABLE;
        Application.ProcessMessages;
  	    if not Contabilizar_CambiosEstadoValidacionTransitosFacturableANoFacturable(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        //-----------------------------------------------------------------------------------------------
        // Generar Asiento de Cambios de Estado de Validaci�n de Tr�nsitos de No Facturable a Facturable.
        //-----------------------------------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [3]);
        lblMensaje.Caption := STR_ASSESING_CHANGE_VALIDATION_STATE_OF_NONINVOICEABLE_TO_INVOICEABLE;
        Application.ProcessMessages;
  	    if not Contabilizar_CambiosEstadoValidacionTransitosNoFacturableAFacturable(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        //--------------------------------------------------------------------
        // Generar Asiento de Registro de Peaje Diario.
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [4]);
        //Obtengo los Transitos a Contabilizar
        Lblmensaje.Caption := STR_OBTAINING_RECORDS_TO_ASSESSING;
        Application.ProcessMessages;
        FCantidadTransitosAContabilizar := ObtenerCantidadTransitosAContabilizar(DMConnections.BaseCAC, edFecha.Date);
  	    if not ObtenerTransitosAContabilizar(edfecha.Date) then begin
            FErrorGrave := True;
            Exit;
        end;

        //Elimino los cambios de estado
        Lblmensaje.Caption := STR_ELIMINATING_RECORDS_INCLUDED_IN_CHANGUES_OF_STATE;
        Application.ProcessMessages;
  	    if not EliminarCambiosDeEstado(DMConnections.BaseCAC, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        //Contabilizo el ingreso de peaje diario
        Lblmensaje.Caption := STR_ASSESSING_REVENUE_OF_DAILY_TOLL;
        Application.ProcessMessages;
  	    if not Contabilizar_IngresoPeajeDiario(DMConnections.BaseCAC,
                edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        // Marcar como Contabilizados los Tr�nsitos que intervinieron en el asiento de Registro de Peaje Diario.
        Lblmensaje.Caption := STR_MARKING_ASSESSED_RECORDS;
        Application.ProcessMessages;
  	    if not MarcarTransitosContabilizados(DMConnections.BaseCAC, FNumeroProceso, DescError) then begin
            if DescError <> EmptyStr then begin
                FErrorGrave := True;
                Exit;
            end;
            if FCancelar then begin
                Exit;
            end;
        end;   

        //--------------------------------------------------------------------
        // Generar Asientos relacionados con Notas de Cobro
        // Emisi�n Masiva de NK, NK a Pedido, Aplicaci�n del Abono, NK Bonificadas
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [5]);

        //Obtengo los Comprobantes a Contabilizar
        Lblmensaje.Caption := STR_OBTAINING_NOTES_TO_ASSESSING;
        Application.ProcessMessages;
        if not ObtenerComprobantesAContabilizar(edfecha.Date) then begin
            FErrorGrave := True;
            Exit;
        end;

        //Obtengo los Movimientos de Cuenta a Contabilizar
        Lblmensaje.Caption := STR_OBTAINING_ACCOUNT_MOVEMENTS_TO_ASSESSING;
        Application.ProcessMessages;
        if not ObtenerMovimientosCuentasAContabilizar then begin
            FErrorGrave := True;
            Exit;
        end;

        // Generar Asiento de Emisi�n Masiva de Notas de Cobro.
        Lblmensaje.Caption := STR_ASSESING_EMISSION_MASSIVE_NOTES_COLLECTION;
        Application.ProcessMessages;
  	    if not Contabilizar_EmisionMasivaNotasCobro(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        // Generar Asiento de Emisi�n de Notas de Cobro a Pedido.
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [6]);
        lblMensaje.Caption := STR_ASSESING_REQUESTED_NOTES_COLLECTION;
        Application.ProcessMessages;
  	    if not Contabilizar_EmisionAPedidoNotasCobro(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end; 

        // Generar Asiento de Aplicaci�n del Abono.
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [7]);
        Lblmensaje.Caption := STR_ASSESING_APLICATION_OF_THE_CREDIT;
        Application.ProcessMessages;
  	    if not Contabilizar_AplicacionDelAbono(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

       // Generar Asiento de Notas de Cobro Bonificadas.
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [8]);
        Lblmensaje.Caption := STR_ASSESING_COLLECTION_NOTES_DISCOUNTED;
        Application.ProcessMessages;
  	    if not Contabilizar_NotasDeCobroBonificadas(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;   

        // Marcar como Contabilizados los Comprobantes que intervinieron en el asiento.
        Lblmensaje.Caption := STR_MARKING_ASSESSED_NOTES_COLLECTION;
        Application.ProcessMessages;
  	    if not MarcarComprobantesContabilizados(DMConnections.BaseCAC, TC_NOTA_COBRO, FNumeroProceso, DescError) then begin
            if DescError <> EmptyStr then begin
                FErrorGrave := True;
                Exit;
            end;
            if FCancelar then begin
                Exit;
            end;
        end;

        //-----------------------------------------------------------------------------------------------------------
        // Generar Asiento de Cobros Empresa de Cobranza Externa
        //-----------------------------------------------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [9]);
        lblMensaje.Caption := STR_COLLECTIONS_OF_EXTERNAL_COMPANIES;
        Application.ProcessMessages;
        if not Contabilizar_CobrosEmpresadeCobranzaExterna(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;  

        //--------------------------------------------------------------------
        // Generar Asiento de Ventas de Pases Diarios.
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [10]);
        lblMensaje.Caption := STR_ASSESING_SALES_DAY_PASS;
        Application.ProcessMessages;
  	    if not Contabilizar_VentasPasesDiarios(DMConnections.BaseCAC,
                edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        //--------------------------------------------------------------------
        // Generar Asiento de Ventas de Boletos de Habilitaci�n Tard�a.
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [11]);
        lblMensaje.Caption := STR_ASSESING_SALES_LATE_DAY_PASS;
        Application.ProcessMessages;
  	    if not Contabilizar_VentasBoletosHabilitacionTardia(DMConnections.BaseCAC,
                edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;


        //--------------------------------------------------------------------
        // Generar Asiento de Cobro de Deuda Diaria (Pagos en Ventanilla).
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [12]);
        lblMensaje.Caption := STR_ASSESING_COLLECTIONS_IN_WINDOW;
        Application.ProcessMessages;
  	    if not Contabilizar_CobrosVentanilla(DMConnections.BaseCAC,
                edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        //--------------------------------------------------------------------
        // Generar Asiento de Emisi�n de Boletas
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [13]);
        // Generar el asiento
        Lblmensaje.Caption := STR_ASSESING_EMITTED_BALLOTS;
        Application.ProcessMessages;
  	    if not Contabilizar_BoletasEmitidas(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        // Marcar como Contabilizados las boletas que intervinieron en el asiento.
        Lblmensaje.Caption := STR_MARKING_ASSESSED_NOTES_COLLECTION;
        Application.ProcessMessages;
  	    if not MarcarComprobantesContabilizados(DMConnections.BaseCAC, TC_BOLETA , FNumeroProceso, DescError) then begin
            if DescError <> EmptyStr then begin
                FErrorGrave := True;
                Exit;
            end;
            if FCancelar then begin
                Exit;
            end;
        end;

        //--------------------------------------------------------------------
        // Generar Asiento de Emisi�n de Facturas
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [14]);
        // Generar el asiento
        Lblmensaje.Caption := STR_ASSESING_EMITTED_INVOICES;
        Application.ProcessMessages;
  	    if not Contabilizar_FacturasEmitidas(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        // Marcar como Contabilizados las Facturas que intervinieron en el asiento.
        Lblmensaje.Caption := STR_MARKING_ASSESSED_NOTES_COLLECTION;
        Application.ProcessMessages;
  	    if not MarcarComprobantesContabilizados(DMConnections.BaseCAC, TC_FACTURA , FNumeroProceso, DescError) then begin
            if DescError <> EmptyStr then begin
                FErrorGrave := True;
                Exit;
            end;
            if FCancelar then begin
                Exit;
            end;
        end; 

        //--------------------------------------------------------------------
        // Generar Asiento de Emisi�n de Notas de Debito
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [15]);
        // Generar el asiento
        Lblmensaje.Caption := STR_ASSESING_EMITTED_NOTES_OF_DEBIT;
        Application.ProcessMessages;
  	    if not Contabilizar_NotasDebitoEmitidas(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        // Marcar como Contabilizados las Notas de Debito que intervinieron en el asiento.
        Lblmensaje.Caption := STR_MARKING_ASSESSED_NOTES_COLLECTION;
        Application.ProcessMessages;
  	    if not MarcarComprobantesContabilizados(DMConnections.BaseCAC, TC_NOTA_DEBITO , FNumeroProceso, DescError) then begin
            if DescError <> EmptyStr then begin
                FErrorGrave := True;
                Exit;
            end;
            if FCancelar then begin
                Exit;
            end;
        end;

        //--------------------------------------------------------------------
        // Generar Asiento de Emisi�n de Notas de Credito
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [16]);
        // Generar el asiento
        Lblmensaje.Caption := STR_ASSESING_EMITTED_NOTES_OF_CREDIT;
        Application.ProcessMessages;
  	    if not Contabilizar_NotasCreditoEmitidas(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;             
        end;

        // Marcar como Contabilizadas las Notas de Credito que intervinieron en el asiento.
        Lblmensaje.Caption := STR_MARKING_ASSESSED_NOTES_COLLECTION;
        Application.ProcessMessages;
  	    if not MarcarComprobantesContabilizados(DMConnections.BaseCAC, TC_NOTA_CREDITO , FNumeroProceso, DescError) then begin
            if DescError <> EmptyStr then begin
                FErrorGrave := True;
                Exit;
            end;
            if FCancelar then begin
                Exit;
            end;
        end;

        //--------------------------------------------------------------------
        // Generar Asiento de Emisi�n de Notas de Credito para Notas de Cobro
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [17]);
        // Generar el asiento
        Lblmensaje.Caption := STR_ASSESING_EMITTED_NOTES_OF_CREDIT_FOR_NOTES_COLLECTION;
        Application.ProcessMessages;
  	    if not Contabilizar_NotasCreditoEmitidasParaAnularNK(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        // Marcar como Contabilizadas las Notas de Credito para Notas de Cobro que intervinieron en el asiento.
        Lblmensaje.Caption := STR_MARKING_ASSESSED_NOTES_COLLECTION;
        Application.ProcessMessages;
  	    if not MarcarComprobantesContabilizados(DMConnections.BaseCAC, TC_NOTA_CREDITO_A_COBRO , FNumeroProceso, DescError) then begin
            if DescError <> EmptyStr then begin
                FErrorGrave := True;
                Exit;
            end;
            if FCancelar then begin
                Exit;
            end;
        end;    

        //--------------------------------------------------------------------
        // Generar Asiento de Registro de los Documentos Devueltos
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [18]);
        lblMensaje.Caption := STR_ASSESING_THE_RETURNED_DOCUMENTS;
        Application.ProcessMessages;
  	    if not Contabilizar_RegistroDocumentosDevueltos(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        //--------------------------------------------------------------------
        // Generar Asiento de Notas de Cobro por Uso de PDU
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [19]);
        lblMensaje.Caption := STR_ASSESING_COLLECTION_NOTES_FOR_USE_OF_DAILY_UNIFIED_PASS;
        Application.ProcessMessages;
  	    if not Contabilizar_NotasCobroPorUsoPDU(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        //--------------------------------------------------------------------
        // Generar Asiento de Notas de Cobro por Uso de BHT
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [20]);
        lblMensaje.Caption := STR_ASSESING_COLLECTION_NOTES_FOR_USE_OF_LATE_DAILY_PASS;
        Application.ProcessMessages;
  	    if not Contabilizar_NotasCobroPorUsoBHT(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        //--------------------------------------------------------------------
        // Generar Asiento de Anulaci�n Por pago duplicado
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [21]);
        lblMensaje.Caption := STR_ASSESING_ANNULMENT_FOR_DUPLICATED_PAYMENT;
        Application.ProcessMessages;
  	    if not Contabilizar_AnulacionPorPagoDuplicado(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        //--------------------------------------------------------------------
        // Generar Asiento de Devoluci�n de Monto Por pago duplicado
        //--------------------------------------------------------------------
        lbl_DescripcionPasoGeneral.Caption := Format(STR_GENERAL_STEP_NUMBER, [22]);
        lblMensaje.Caption := STR_ASSESING_ANNULMENT_FOR_DUPLICATED_PAYMENT;
        Application.ProcessMessages;
  	    if not Contabilizar_DevolucionDeMontoPorPagoDuplicado(DMConnections.BaseCAC, edFecha.Date, FNumeroProceso, DescError) then begin
            FErrorGrave := True;
            Exit;
        end;

        // Generar los siguientes asientos

        //Calculo los Totales por Cuenta
        Lblmensaje.Caption := STR_CALCULATED_TOTAL_FOR_ACCOUNT;
        Application.ProcessMessages;
        CalcularTotalesCuentasContables(DMConnections.BaseCAC , FNumeroProceso);  

        //Actualizo el log al Final
        Lblmensaje.Caption := STR_FINISHING_OPERATION;
        Application.ProcessMessages;
        ActualizarLog;

    finally
      	Screen.Cursor           := crDefault;
        pbProgreso.Position     := 0;
        PnlAvance.Visible       := False;
        lblMensaje.Caption      := '';
        edFecha.Enabled         := True;
        edfecha.Date            := NullDate;
        //Elimino las tablas temporales utilizadas,
        //me fijo si alguna quedo en memoria.
        EliminarTablasTemporalesUtilizadas(DMConnections.BaseCAC);
        //Muestro Mensaje de Finalizaci�n
        if FCancelar then begin
            // Muestro mensaje que el proceso fue cancelado (sin errores)
            MsgBox(MSG_PROCESS_CANCEL, Self.Caption, MB_OK + MB_ICONINFORMATION);
            FCancelar := False;
        end else if FErrorGrave then begin
            //si se produjo un error lo informo
            if DescError <> '' then MsgBox(DescError, Self.Caption, MB_ICONSTOP);
            //Muestro mensaje que finalizo por un error
            MsgBox(MSG_PROCESS_FINALLY_WITH_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
        end else begin
            //Muestro mensaje que el proceso finalizo exitosamente
            MsgBox(MSG_PROCESS_FINALLY_OK, Self.Caption, MB_OK + MB_ICONINFORMATION);
            //Genero el reporte de finalizacion de proceso
            GenerarReporteFinalizacionProceso(FNumeroProceso);
        end;
        KeyPreview := False;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 29/07/2005
  Description: Permito Cancelar
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFContabilizaciondeJornada.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created:  29/07/2005
  Description: Permite salir solo si no esta procesando.
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFContabilizaciondeJornada.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando.
	  CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created:  29/07/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFContabilizaciondeJornada.SalirBTNClick(Sender: TObject);
begin
	  Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created:  29/07/2005
  Description: Lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFContabilizaciondeJornada.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;


end.
