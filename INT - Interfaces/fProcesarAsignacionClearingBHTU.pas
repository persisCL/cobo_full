
{********************************** Unit Header ********************************
File Name : fProcesarAsignacionClearingBHTU.pas              
Author : ndonadio
Date Created: 06/10/2005
Language : ES-AR
Description : Asigna los BHTU a las infracciones del Clearing.
              Adicionalmente anula las infracciones reales y
              asigna los transitos afectados.

Firma       :   SS_1147_CQU_20140408
Comentario  :   Se cambia el nombre a los objetos: spObtenerTransitosCN y spAsignarTransitoCNaBHTU
                por spObtenerTransitosCN y spAsignarTransitoCNaBHTU, respectivamente.
                Tambien al nombre de los procedimientos almacenados de SQL

Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto


*******************************************************************************}
unit fProcesarAsignacionClearingBHTU;

interface


uses
   // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc,
    // Parametros GEnerales
    ConstParametrosGenerales,
   // DB
    UtilDB, DMConnection,
    // Reporte
    fReporteProcesoAsignacionBHTU,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, DateUtils, ppDB, ppDBPipe, UtilRB,
  ppCtrls, ppBands, ppClass, ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport, ppModule,
  raCodMod, ppParameter, Validate;

type
  TfrmProcesarAsignacionClearingBHTU = class(TForm)
    btnSalir: TButton;
    btnProcesar: TButton;
    Label1: TLabel;
    lblDetalle: TLabel;
    pbProgreso: TProgressBar;
    Bevel1: TBevel;
    spObtenerListaBHTU: TADOStoredProc;
    spAsignarBHTUAInfraccion: TADOStoredProc;
    spAsignarOtrasInfracciones: TADOStoredProc;
    spAsignarOtrosTransitos: TADOStoredProc;
    //spObtenerTransitosCN: TADOStoredProc;     // SS_1147_CQU_20140408
    //spAsignarTransitoCNaBHTU: TADOStoredProc; // SS_1147_CQU_20140408
    spObtenerTransitos: TADOStoredProc;         // SS_1147_CQU_20140408
    spAsignarTransitoaBHTU: TADOStoredProc;     // SS_1147_CQU_20140408
    spRegistrarProceso: TADOStoredProc;
    spCerrarProceso: TADOStoredProc;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ImgAyudaClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
    FCancelar,
    FPRocesando         : boolean;
    FCantidadAProcesar  : Integer;
    FCodigoOperacionInterfase    : Integer;
    Function ObtenerCantidadAProcesar: Boolean;
    Function RegistrarResumenProceso(var CodigoOperacion: Integer; var Error: AnsiString): boolean;
    Function CerrarResumenProceso(CodigoOperacion: Integer; Error: AnsiString): boolean;
    procedure MostrarReporte;
    procedure HabilitarBotones;
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): boolean;
  end;

resourcestring
      MSG_BHTU_QTY  = '%d BHTUs Pendientes de Asignaci�n';

var
  frmProcesarAsignacionClearingBHTU: TfrmProcesarAsignacionClearingBHTU;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : ndonadio
Date Created : 06/10/2005
Description :  Accion al presionar el btnSalir (Salir o Cancelar)
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmProcesarAsignacionClearingBHTU.btnSalirClick(Sender: TObject);
begin
    if FProcesando then FCancelar := True
    else Close;
end;

{******************************** Function Header ******************************
Function Name: ObtenerCantidadAProcesar
Author : ndonadio
Date Created : 06/10/2005
Description :
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TfrmProcesarAsignacionClearingBHTU.ObtenerCantidadAProcesar: Boolean;
resourcestring
    ERROR_CANT_OBTAIN_QTY   = 'Error al obtener la cantidad a procesar.';
begin
    Result := False;
    try
        FCantidadAProcesar := QueryGetValueInt(DMConnections.BaseCAC,
          'SELECT dbo.ObtenerCantidadBHTUAProcesar() ') ;
        lblDetalle.Caption := Format(MSG_BHTU_QTY, [ FCantidadAProcesar ]);
        Result := FCantidadAProcesar >= 0;

        if  Result then begin
            pbProgreso.Max := FCantidadAProcesar * 6;
        end;
        pbProgreso.Min := 0;
        pbProgreso.Position := pbProgreso.Min;
    except
        on e:exception do begin
            MsgBoxErr(ERROR_CANT_OBTAIN_QTY, e.Message, Caption, MB_ICONSTOP);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 06/10/2005
Description :
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmProcesarAsignacionClearingBHTU.HabilitarBotones;
resourcestring
    CAP_CANCEL  = 'Cancelar';
    CAP_QUIT    = 'Salir';
begin
    FCancelar := False;
    lblDetalle.Caption := '';
    pbProgreso.Position := pbProgreso.Min;
    if FProcesando then begin
        btnSalir.Caption := CAP_CANCEL;
        btnProcesar.Enabled := False;
    end
    else begin
        btnSalir.Caption := CAP_QUIT;
        btnProcesar.Enabled := (FCantidadAProcesar > 0);

    end;

end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 06/10/2005
Description :  Habilita o deshabilita los botones y cambia los captions
                de acuerdo a si se est� procesando o no.
Parameters : Titulo: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmProcesarAsignacionClearingBHTU.Inicializar(Titulo: AnsiString): boolean;
resourcestring
    MSG_NO_BHTU_AVAILABLE   = 'No hay BHTU''s disponibles para asignar.';
begin
    Result := False;
    Caption := TRIM(Titulo);
    CenterForm(Self);
    FProcesando := False;
    if not ObtenerCantidadAProcesar then Exit;   
    if FCantidadAProcesar = 0 then begin
        MsgBox(MSG_NO_BHTU_AVAILABLE, Caption, MB_ICONWARNING);
        Exit;
    end;
    HabilitarBotones;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : ndonadio
Date Created : 06/10/2005
Description : esto es para que el form se cierre realmente...
Parameters : Sender: TObject; var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TfrmProcesarAsignacionClearingBHTU.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : ndonadio
Date Created : 06/10/2005
Description : Impide cerrar si se est� procesando.
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmProcesarAsignacionClearingBHTU.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author : ndonadio
Date Created : 06/10/2005
Description : Proceso principal de asignacion
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmProcesarAsignacionClearingBHTU.btnProcesarClick(Sender: TObject);
resourcestring
    ERROR_RETRIEVING_NEXT_BHTU          = 'Error al Obtener los datos del BHTU';
    ERROR_OPENING_BHTU_LIST             = 'Error abriendo la lista de BHTU sin Asignar';
    ERROR_USING_BHTU                    = 'Error al Asignar el BHTU a la Infraccion seleccionada';
    ERROR_SEARCHING_MORE_INFRACTIONS    = 'Error al buscar otras Infracciones para el BHTU';
    ERROR_SEARCHING_LOST_TRX            = 'Error asignando BHTU a Tr�nsitos en franjas de Tolerancia';
    ERROR_RETRIEVING_CN_TRX             = 'Error recuperando Tr�nsitos de CN Asginados al BHTU';
    ERROR_CHECKING_CN_TRX               = 'Error al Asignar Tr�nsito de CN al BHTU';
    ERROR_CANT_REGISTER_OPERATION       = 'Error al registrar la operacion';
    ERROR_CLOSING_OPERATION             = 'Error al cerrar la operacion';
    MSG_PROCESS_ENDED_OK                = 'El proceso finaliz� con �xito';
    MSG_PROCESS_CANCELLED               = 'Proceso cancelado por el usuario';
    MSG_CANT_ASIGNACIONES               = 'Se asignaron %d BHTU.' ;
    MSG_NO_ASIGNACION                   = 'No se asignaron BHTU.';
var
    descError           : AnsiString;
    BHTUQty,
    CodigoAsignacion,
    NumeroDayPass,
    NumCorrCA,
    CantidadProcesados  : Integer;
    FechaInfraccion,
    FechaVenta          : TDateTime;
    Patente             : AnsiString;
begin
    FProcesando := True;
    HabilitarBotones;
    CantidadProcesados := 0;
    try
        // Preparar Proceso
        descError   := '';
        try
            // Obtener lista de BHTU a Asignar
            descError := ERROR_OPENING_BHTU_LIST;
            spObtenerListaBHTU.Close;
            spObtenerListaBHTU.Open;
            if  not spObtenerListaBHTU.Eof then begin
                // si no est� vacio, registro la operacion
                if not RegistrarResumenProceso(FCodigoOperacionInterfase, descError) then begin
                    MsgBoxErr(ERROR_CANT_REGISTER_OPERATION, descError, Caption, MB_ICONSTOP);
                    Exit;
                end;
            end;
            BHTUQty := FCantidadAProcesar;
            // Por cada BHTU sin Asignar
            descError := ERROR_RETRIEVING_NEXT_BHTU;
            while (not spObtenerListaBHTU.Eof) and (not FCancelar) do begin
                // Obtengo los datos del daypass(BHTU) seleccionado

                NumeroDaypass   := spObtenerListaBHTU.FieldByName('NumeroDayPass').AsInteger;
                FechaVenta      := spObtenerListaBHTU.FieldByName('FechaVenta').AsDateTime;
                Patente         := trim( spObtenerListaBHTU.FieldByName('Patente').AsString);

                // Inicio Transaccion
                //DMConnections.BaseCAC.BeginTrans;                                               //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('BEGIN TRAN fAsignacionClearingBHTU');              //SS_1385_NDR_20150922

                pbProgreso.StepIt;

                // Asigno BHTU A Infraccion
                descError := ERROR_USING_BHTU;
                spAsignarBHTUAInfraccion.Parameters.ParamByName('@CodigoOperacion').Value := FCodigoOperacionInterfase;
                spAsignarBHTUAInfraccion.Parameters.ParamByName('@NumeroDayPass').Value := NumeroDaypass;
                spAsignarBHTUAInfraccion.Parameters.ParamByName('@FechaVenta').Value := FechaVenta;
                spAsignarBHTUAInfraccion.Parameters.ParamByName('@Patente').Value := Patente;
                spAsignarBHTUAInfraccion.ExecProc;

                // si no asigno... sigo con el siguiente...
                if spAsignarBHTUAInfraccion.Parameters.ParamByName('@RETURN_VALUE').Value = -1 then begin
                    //DMConnections.BaseCAC.CommitTrans;                                                                //SS_1385_NDR_20150922
                    DMConnections.BaseCAC.Execute('COMMIT TRAN fAsignacionClearingBHTU');					              //SS_1385_NDR_20150922

                    // sigo con el siguiente BHTU
                    spObtenerListaBHTU.Next;
                    BHTUQty := BHTUQty - 1;
                    lblDetalle.Caption := Format(MSG_BHTU_QTY, [BHTUQty]);
                    pbProgreso.StepBy(5);
                    Application.ProcessMessages;

                    Continue;
                end;
                inc(CantidadProcesados);
                CodigoAsignacion    := spAsignarBHTUAInfraccion.Parameters.ParamByName('@CodigoAsignacion').Value;
                FechaInfraccion     := spAsignarBHTUAInfraccion.Parameters.ParamByName('@FechaInfraccion').Value;

                pbProgreso.StepIt;

                // Asigno Otras Infracciones misma Fecha
                descError := ERROR_SEARCHING_MORE_INFRACTIONS;
                spAsignarOtrasInfracciones.Parameters.ParamByName('@CodigoAsignacion').Value := CodigoAsignacion;
                spAsignarOtrasInfracciones.Parameters.ParamByName('@FechaInfraccion').Value := FechaInfraccion;
                spAsignarOtrasInfracciones.Parameters.ParamByName('@Patente').Value := Patente;
                spAsignarOtrasInfracciones.ExecProc;

                pbProgreso.StepIt;

                // Asigno posibles Transitos sueltos
                // (de otras infracciones pero que por tolerancias se puedan anular con este BHTU)
                descError := ERROR_SEARCHING_LOST_TRX;
                spAsignarOtrosTransitos.Parameters.ParamByName('@CodigoAsignacion').Value := CodigoAsignacion;
                spAsignarOtrosTransitos.Parameters.ParamByName('@FechaInfraccion').Value := FechaInfraccion;
                spAsignarOtrosTransitos.Parameters.ParamByName('@Patente').Value := Patente;
                spAsignarOtrosTransitos.ExecProc;

                // Obtengo los Trx de CN que Anule
                pbProgreso.StepIt;
                descError := ERROR_RETRIEVING_CN_TRX;
                //spObtenerTransitosCN.Close;                                                                   // SS_1147_CQU_20140408
                //spObtenerTransitosCN.Parameters.ParamByName('@CodigoAsignacion').Value := CodigoAsignacion;   // SS_1147_CQU_20140408
                //spObtenerTransitosCN.Open;                                                                    // SS_1147_CQU_20140408
                spObtenerTransitos.Close;                                                                       // SS_1147_CQU_20140408
                spObtenerTransitos.Parameters.ParamByName('@CodigoAsignacion').Value := CodigoAsignacion;       // SS_1147_CQU_20140408
                spObtenerTransitos.Open;                                                                        // SS_1147_CQU_20140408
                // Por Cada Uno
                descError := ERROR_RETRIEVING_CN_TRX;
                pbProgreso.StepIt;
                //while  (not spObtenerTransitosCN.Eof) do begin                                                // SS_1147_CQU_20140408
                while  (not spObtenerTransitos.Eof) do begin                                                    // SS_1147_CQU_20140408
                // no controlo FCancelar, porque cancelo por ciclo completo, es decir, termino la asignacion en curso antes de cancelar
                    //NumCorrCA := spObtenerTransitosCN.FieldByName('NumCorrCA').AsInteger;                     // SS_1147_CQU_20140408
                    NumCorrCA := spObtenerTransitos.FieldByName('NumCorrCA').AsInteger;                         // SS_1147_CQU_20140408
                    // Anulo Transito de Infraccion (real)
                    descError := ERROR_CHECKING_CN_TRX;
                    // Asigno Transito (real) al Daypass
                    //spAsignarTransitoCNaBHTU.Parameters.ParamByName('@NumCorrCA').Value := NUmCorrCA;         // SS_1147_CQU_20140408
                    //spAsignarTransitoCNaBHTU.Parameters.ParamByName('@NumeroDayPass').Value := NumeroDaypass; // SS_1147_CQU_20140408
                    //spAsignarTransitoCNaBHTU.ExecProc;                                                        // SS_1147_CQU_20140408
                    spAsignarTransitoaBHTU.Parameters.ParamByName('@NumCorrCA').Value := NUmCorrCA;             // SS_1147_CQU_20140408
                    spAsignarTransitoaBHTU.Parameters.ParamByName('@NumeroDayPass').Value := NumeroDaypass;     // SS_1147_CQU_20140408
                    spAsignarTransitoaBHTU.ExecProc;                                                            // SS_1147_CQU_20140408
                    // siguiente trx de CN...
                    descError := ERROR_RETRIEVING_CN_TRX;
                    //spObtenerTransitosCN.Next;                                                                // SS_1147_CQU_20140408
                    spObtenerTransitos.Next;                                                                    // SS_1147_CQU_20140408
                end;
                
                //DMConnections.BaseCAC.CommitTrans;                                                                //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('COMMIT TRAN fAsignacionClearingBHTU');					              //SS_1385_NDR_20150922
                // siguiente BHTU
                descError := ERROR_RETRIEVING_NEXT_BHTU;
                spObtenerListaBHTU.Next;
                BHTUQty := BHTUQty - 1;
                lblDetalle.Caption := Format(MSG_BHTU_QTY, [BHTUQty]);
                pbProgreso.StepIt;
                Application.ProcessMessages;
            end;

            if not CerrarResumenProceso(FCodigoOperacionInterfase, descError) then begin
                MsgBoxErr( ERROR_CLOSING_OPERATION, descError, Caption, MB_ICONERROR);
                Exit;
            end;
            pbProgreso.Position := pbProgreso.Max;

            if not FCancelar then begin
                if CantidadPRocesados > 0 then MsgBox(MSG_PROCESS_ENDED_OK + CRLF + Format(MSG_CANT_ASIGNACIONES, [CantidadProcesados]), Caption, MB_ICONINFORMATION)
                else MsgBox(MSG_PROCESS_ENDED_OK + CRLF + MSG_NO_ASIGNACION, Caption, MB_ICONINFORMATION)
            end
            else MsgBox(MSG_PROCESS_CANCELLED, Caption, MB_ICONINFORMATION);
        except
            on e:exception do begin
                //if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fAsignacionClearingBHTU END');	    //SS_1385_NDR_20150922

                MsgBoxErr( descError, e.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        if CantidadProcesados > 0 then begin
            // si procese algo, muestro el reporte...
             MostrarReporte;
        end;
        FProcesando := False;
        ObtenerCantidadAProcesar;
        HabilitarBotones;
    end;
end;

{******************************** Function Header ******************************
Function Name: RegistrarResumenProceso
Author : ndonadio
Date Created : 06/10/2005
Description :   Guarda un registro en la tabla ResuemnProcesoAsignacionBHTU
Parameters : CodigoOperacion: Integer; Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmProcesarAsignacionClearingBHTU.RegistrarResumenProceso(var CodigoOperacion: Integer;
var  Error: AnsiString): boolean;
begin
    Result := False;
    try
        spRegistrarProceso.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        spRegistrarProceso.Parameters.ParamByName('@CodigoProcesoAsignacionBHTU').Value := NULL;
        spRegistrarProceso.ExecProc;
        CodigoOperacion := spRegistrarProceso.Parameters.ParamByName('@CodigoProcesoAsignacionBHTU').Value;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: CerrarResumenProceso
Author : ndonadio
Date Created : 06/10/2005
Description :   Completa los datos en  un registro en la tabla ResuemnProcesoAsignacionBHTU
Parameters : CodigoOperacion: Integer; Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmProcesarAsignacionClearingBHTU.CerrarResumenProceso(CodigoOperacion: Integer;
  Error: AnsiString): boolean;
begin
    Result := False;
    try
        spCerrarProceso.Parameters.ParamByName('@CodigoProcesoAsignacionBHTU').Value := CodigoOperacion;
        spCerrarProceso.ExecProc;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: MostrarReporte
Author : ndonadio
Date Created : 06/10/2005
Description : Muestra el reporte
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmProcesarAsignacionClearingBHTU.MostrarReporte;
var
    f: TfrmReportePRocesoAsignacionBHTU;
begin
    Application.Createform(TfrmReportePRocesoAsignacionBHTU, f);
    if not f.Inicializar(FCodigoOperacionInterfase, Caption) then f.Release;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : ndonadio
Date Created : 10/10/2005
Description :Muestra la ayuda de la ventana.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmProcesarAsignacionClearingBHTU.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_AYUDA   = ' ' + CRLF +
                'El Proceso de Asignaci�n de BHTU' + CRLF +
                'asigna los tr�nsitos infractores enviados por  '+ CRLF +
                'las Concesionarias adheridas, anulando las ' + CRLF +
                'infracciones y marcando los BHTU como usados.' + CRLF +
                ' ' ;
begin
    //si esta procesando sale
    if FProcesando then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_AYUDA, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

procedure TfrmProcesarAsignacionClearingBHTU.ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
    if FProcesando then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;


end.
