{-----------------------------------------------------------------------------
 File Name: HashingMd5.pas
 Author:    Modificada y revisada por FLamas
 Date Created: 27/12/2004
 Language: ES-AR
 Description: Calcula el Hasshing MD5 de un string dado
 				funci�n a utilizar MD5string
-----------------------------------------------------------------------------}
// tabs = 2
// -----------------------------------------------------------------------------------------------
//
//                                 MD5 Message-Digest for Delphi 4
//
//                                 Delphi 4 Unit implementing the
//                      RSA Data Security, Inc. MD5 Message-Digest Algorithm
//
//                          Implementation of Ronald L. Rivest's RFC 1321
//
//                      Copyright � 1997-1999 Medienagentur Fichtner & Meyer
//                                  Written by Matthias Fichtner
//
// -----------------------------------------------------------------------------------------------
//               See RFC 1321 for RSA Data Security's copyright and license notice!
// -----------------------------------------------------------------------------------------------
//
//     14-Jun-97  mf  Implemented MD5 according to RFC 1321                           RFC 1321
//     16-Jun-97  mf  Initial release of the compiled unit (no source code)           RFC 1321
//     28-Feb-99  mf  Added MD5Match function for comparing two digests               RFC 1321
//     13-Sep-99  mf  Reworked the entire unit                                        RFC 1321
//     17-Sep-99  mf  Reworked the "Test Driver" project                              RFC 1321
//     19-Sep-99  mf  Release of sources for MD5 unit and "Test Driver" project       RFC 1321
//
// -----------------------------------------------------------------------------------------------
//                   The latest release of md5.pas will always be available from
//                  the distribution site at: http://www.fichtner.net/delphi/md5/
// -----------------------------------------------------------------------------------------------
//                       Please send questions, bug reports and suggestions
//                      regarding this code to: mfichtner@fichtner-meyer.com
// -----------------------------------------------------------------------------------------------
//                        This code is provided "as is" without express or
//                     implied warranty of any kind. Use it at your own risk.
// -----------------------------------------------------------------------------------------------

unit HashingMd5;

// -----------------------------------------------------------------------------------------------
INTERFACE
// -----------------------------------------------------------------------------------------------

uses
        Windows, SysUtils;

// Constants for MD5Transform routine. 
const
    S11 = 7;
    S12 = 12;
    S13 = 17;
    S14 = 22;
    S21 = 5;
    S22 = 9;
    S23 = 14;
    S24 = 20;
    S31 = 4;
    S32 = 11;
    S33 = 16;
    S34 = 23;
    S41 = 6;
    S42 = 10;
    S43 = 15;
    S44 = 21;

type
	MD5Count = array[0..1] of DWORD;
    MD5State = array[0..3] of DWORD;
    MD5Block = array[0..15] of DWORD;
    MD5CBits = array[0..7] of byte;
    MD5Digest = array[0..15] of byte;
    MD5Buffer = array[0..63] of byte;
    MD5Context = record
    	State: MD5State;
        Count: MD5Count;
        Buffer: MD5Buffer;
    end;

procedure MD5Init(var Context: MD5Context);
procedure MD5Update(var Context: MD5Context; Input: pChar; InputLen: longword);
procedure MD5Final(var Digest: MD5Digest; var Context: MD5Context);
procedure Encode(Output, Input: pointer; Count: longword);
procedure Decode(Output, Input: pointer; Count: longword);

function MD5String(const M: string): string;
function Digest2String( D : MD5Digest ) : string;

function MD5Match(D1, D2: MD5Digest): boolean;
procedure MD5Transform(var State: MD5State; Block: pointer );

// -----------------------------------------------------------------------------------------------
IMPLEMENTATION
// -----------------------------------------------------------------------------------------------

var
        PADDING: MD5Buffer = (
                $80, $00, $00, $00, $00, $00, $00, $00,
                $00, $00, $00, $00, $00, $00, $00, $00,
                $00, $00, $00, $00, $00, $00, $00, $00,
                $00, $00, $00, $00, $00, $00, $00, $00,
                $00, $00, $00, $00, $00, $00, $00, $00,
                $00, $00, $00, $00, $00, $00, $00, $00,
                $00, $00, $00, $00, $00, $00, $00, $00,
                $00, $00, $00, $00, $00, $00, $00, $00
        );

function F(x, y, z: DWORD): DWORD;
begin
        Result := (x and y) or ((not x) and z);
end;

function G(x, y, z: DWORD): DWORD;
begin
        Result := (x and z) or (y and (not z));
end;

function H(x, y, z: DWORD): DWORD;
begin
        Result := x xor y xor z;
end;

function I(x, y, z: DWORD): DWORD;
begin
        Result := y xor (x or (not z));
end;

procedure rot(var x: DWORD; n: BYTE);
begin
        x := (x shl n) or (x shr (32 - n));
end;

procedure FF(var a: DWORD; b, c, d, x: DWORD; s: BYTE; ac: DWORD);
begin
        inc(a, F(b, c, d) + x + ac);
        rot(a, s);
        inc(a, b);
end;

procedure GG(var a: DWORD; b, c, d, x: DWORD; s: BYTE; ac: DWORD);
begin
        inc(a, G(b, c, d) + x + ac);
        rot(a, s);
        inc(a, b);
end;

procedure HH(var a: DWORD; b, c, d, x: DWORD; s: BYTE; ac: DWORD);
begin
        inc(a, H(b, c, d) + x + ac);
        rot(a, s);
        inc(a, b);
end;

procedure II(var a: DWORD; b, c, d, x: DWORD; s: BYTE; ac: DWORD);
begin
        inc(a, I(b, c, d) + x + ac);
        rot(a, s);
        inc(a, b);
end;

// -----------------------------------------------------------------------------------------------
// Initialize given Context
procedure MD5Init(var Context: MD5Context);
begin
	with Context do begin
    	State[0] := $67452301;
        State[1] := $efcdab89;
        State[2] := $98badcfe;
        State[3] := $10325476;
        Count[0] := 0;
        Count[1] := 0;
        ZeroMemory(@Buffer, SizeOf(MD5Buffer));
    end;
end;

// -----------------------------------------------------------------------------------------------
// Update given Context to include Length bytes of Input
procedure MD5Update(var Context: MD5Context; Input: pChar; InputLen: longword);
var
	Index: longword;
    PartLen: longword;
    I: longword;
begin
	with Context do begin
    	Index := (Count[0] shr 3) and $3f;
        Inc(Count[0], InputLen shl 3);
        if Count[0] < (InputLen shl 3) then Inc(Count[1]);
        Inc(Count[1], InputLen shr 29);
    end;

    PartLen := 64 - Index;
    if ( InputLen >= PartLen ) then begin
    	CopyMemory(@Context.Buffer[Index], Input, PartLen);
        MD5Transform(Context.State, @Context.Buffer);
        I := PartLen;
        while I + 63 < InputLen do begin
        	MD5Transform(Context.State, @Input[I] );
            Inc(I, 64);
        end;
    	Index := 0;
    end else I := 0;

    CopyMemory(@Context.Buffer[Index], @Input[I], InputLen - I);
end;

// -----------------------------------------------------------------------------------------------
// Finalize given Context, create Digest and zeroize Context
procedure MD5Final(var Digest: MD5Digest; var Context: MD5Context);
var
	Bits: MD5CBits;
    Index: longword;
    PadLen: longword;
begin
	Encode(@Bits, @Context.Count, 8);
    Index := (Context.Count[0] shr 3) and $3f;
    if ( Index < 56 ) then PadLen := 56 - Index
    else PadLen := 120 - Index;
    MD5Update(Context, @PADDING, PadLen);

    // Append length (before padding)
    MD5Update(Context, @Bits, 8);

    // Store state in digest
    Encode(@Digest, @Context.State, 16);

  	// Zeroize sensitive information.
    ZeroMemory(@Context, SizeOf(MD5Context));
end;

// -----------------------------------------------------------------------------------------------
// MD5Transform State according to first 64 bytes at Buffer
procedure MD5Transform(var State: MD5State; Block: pointer );
var
        a, b, c, d: DWORD;
        x: MD5Block;
begin
    Decode(@x, Block, 64);
    a := State[0];
    b := State[1];
    c := State[2];
	d := State[3];

    // Round 1
    FF (a, b, c, d, x[ 0], S11, $d76aa478); // 1
    FF (d, a, b, c, x[ 1], S12, $e8c7b756); // 2
    FF (c, d, a, b, x[ 2], S13, $242070db); // 3
    FF (b, c, d, a, x[ 3], S14, $c1bdceee); // 4
    FF (a, b, c, d, x[ 4], S11, $f57c0faf); // 5
    FF (d, a, b, c, x[ 5], S12, $4787c62a); // 6
    FF (c, d, a, b, x[ 6], S13, $a8304613); // 7
    FF (b, c, d, a, x[ 7], S14, $fd469501); // 8
    FF (a, b, c, d, x[ 8], S11, $698098d8); // 9
    FF (d, a, b, c, x[ 9], S12, $8b44f7af); // 10
    FF (c, d, a, b, x[10], S13, $ffff5bb1); // 11
    FF (b, c, d, a, x[11], S14, $895cd7be); // 12
    FF (a, b, c, d, x[12], S11, $6b901122); // 13
    FF (d, a, b, c, x[13], S12, $fd987193); // 14
    FF (c, d, a, b, x[14], S13, $a679438e); // 15
    FF (b, c, d, a, x[15], S14, $49b40821); // 16

    // Round 2
    GG (a, b, c, d, x[ 1], S21, $f61e2562); // 17
    GG (d, a, b, c, x[ 6], S22, $c040b340); // 18
    GG (c, d, a, b, x[11], S23, $265e5a51); // 19
    GG (b, c, d, a, x[ 0], S24, $e9b6c7aa); // 20
    GG (a, b, c, d, x[ 5], S21, $d62f105d); // 21
    GG (d, a, b, c, x[10], S22,  $2441453); // 22
    GG (c, d, a, b, x[15], S23, $d8a1e681); // 23
    GG (b, c, d, a, x[ 4], S24, $e7d3fbc8); // 24
    GG (a, b, c, d, x[ 9], S21, $21e1cde6); // 25
    GG (d, a, b, c, x[14], S22, $c33707d6); // 26
    GG (c, d, a, b, x[ 3], S23, $f4d50d87); // 27
    GG (b, c, d, a, x[ 8], S24, $455a14ed); // 28
    GG (a, b, c, d, x[13], S21, $a9e3e905); // 29
    GG (d, a, b, c, x[ 2], S22, $fcefa3f8); // 30
    GG (c, d, a, b, x[ 7], S23, $676f02d9); // 31
    GG (b, c, d, a, x[12], S24, $8d2a4c8a); // 32

    // Round 3
    HH (a, b, c, d, x[ 5], S31, $fffa3942); // 33
    HH (d, a, b, c, x[ 8], S32, $8771f681); // 34
    HH (c, d, a, b, x[11], S33, $6d9d6122); // 35
    HH (b, c, d, a, x[14], S34, $fde5380c); // 36
    HH (a, b, c, d, x[ 1], S31, $a4beea44); // 37
    HH (d, a, b, c, x[ 4], S32, $4bdecfa9); // 38
    HH (c, d, a, b, x[ 7], S33, $f6bb4b60); // 39
    HH (b, c, d, a, x[10], S34, $bebfbc70); // 40
    HH (a, b, c, d, x[13], S31, $289b7ec6); // 41
    HH (d, a, b, c, x[ 0], S32, $eaa127fa); // 42
    HH (c, d, a, b, x[ 3], S33, $d4ef3085); // 43
    HH (b, c, d, a, x[ 6], S34,  $4881d05); // 44
    HH (a, b, c, d, x[ 9], S31, $d9d4d039); // 45
    HH (d, a, b, c, x[12], S32, $e6db99e5); // 46
    HH (c, d, a, b, x[15], S33, $1fa27cf8); // 47
    HH (b, c, d, a, x[ 2], S34, $c4ac5665); // 48

    // Round 4
    II (a, b, c, d, x[ 0], S41, $f4292244); // 49
    II (d, a, b, c, x[ 7], S42, $432aff97); // 50
    II (c, d, a, b, x[14], S43, $ab9423a7); // 51
    II (b, c, d, a, x[ 5], S44, $fc93a039); // 52
    II (a, b, c, d, x[12], S41, $655b59c3); // 53
    II (d, a, b, c, x[ 3], S42, $8f0ccc92); // 54
    II (c, d, a, b, x[10], S43, $ffeff47d); // 55
    II (b, c, d, a, x[ 1], S44, $85845dd1); // 56
    II (a, b, c, d, x[ 8], S41, $6fa87e4f); // 57
 	II (d, a, b, c, x[15], S42, $fe2ce6e0); // 58
 	II (c, d, a, b, x[ 6], S43, $a3014314); // 59
 	II (b, c, d, a, x[13], S44, $4e0811a1); // 60
 	II (a, b, c, d, x[ 4], S41, $f7537e82); // 61
 	II (d, a, b, c, x[11], S42, $bd3af235); // 62
 	II (c, d, a, b, x[ 2], S43, $2ad7d2bb); // 63
 	II (b, c, d, a, x[ 9], S44, $eb86d391); // 64

    Inc(State[0], a);
    Inc(State[1], b);
    Inc(State[2], c);
    Inc(State[3], d);
end;

// -----------------------------------------------------------------------------------------------
// Encode Count DWORDs at Source into (Count * 4) Bytes at Target
procedure Encode(Output, Input: pointer; Count: longword);
var
	InputPtr: PDWORD;
    OutputPtr: PByte;
    i: longword;
begin
    InputPtr := Input;
    OutputPtr := Output;
    for i := 1 to Count div 4 do begin
    	OutputPtr^ := InputPtr^ and $ff;
        Inc(OutputPtr);
        OutputPtr^ := (InputPtr^ shr 8) and $ff;
        Inc(OutputPtr);
        OutputPtr^ := (InputPtr^ shr 16) and $ff;
        Inc(OutputPtr);
        OutputPtr^ := (InputPtr^ shr 24) and $ff;
        Inc(InputPtr);
        Inc(OutputPtr);
    end;
end;

// -----------------------------------------------------------------------------------------------
// Decode Count bytes at Source into (Count / 4) DWORDs at Target
procedure Decode(Output, Input: pointer; Count: longword);
var
	InputPtr: PByte;
    OutputPtr: PDWORD;
    i: longword;
begin
    InputPtr := Input;
    OutputPtr := Output;
    for i := 1 to Count div 4 do begin
    	OutputPtr^ := InputPtr^;
        inc(InputPtr);
        OutputPtr^ := OutputPtr^ or (InputPtr^ shl 8);
        inc(InputPtr);
        OutputPtr^ := OutputPtr^ or (InputPtr^ shl 16);
        inc(InputPtr);
        OutputPtr^ := OutputPtr^ or (InputPtr^ shl 24);
        inc(InputPtr);
        inc(OutputPtr);
    end;
end;

// -----------------------------------------------------------------------------------------------

// Create digest of given Message
function MD5String(const M: string): string;
var
	Context: MD5Context;
    D : MD5Digest;
begin
        MD5Init(Context);
        MD5Update(Context, pChar(M), length(M));
        MD5Final(D, Context);
        result := UpperCase( Trim(Digest2String( D )));
end;

// Create hex representation of given Digest
function Digest2String( D : MD5Digest ) : string;
var
        I: byte;
const
        Digits: array[0..15] of char =
                ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
begin
        Result := '';
        for I := 0 to 15 do
        	Result := Result + Digits[(D[I] shr 4) and $0f] + Digits[D[I] and $0f];
end;

// -----------------------------------------------------------------------------------------------

// Compare two Digests
function MD5Match(D1, D2: MD5Digest): boolean;
var
        I: byte;
begin
        I := 0;
        Result := TRUE;
        while Result and (I < 16) do begin
                Result := D1[I] = D2[I];
                inc(I);
        end;
end;

end.

