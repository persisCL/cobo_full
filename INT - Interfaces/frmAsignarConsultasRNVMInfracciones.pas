{-------------------------------------------------------------
            	frmAsignarConsultasRNVMInfracciones

Autor		:	Nelson Droguett
Fecha		:	22-10-2015
Firma		:	SS_1406_NDR_20151022
Descripcion	:	Asigna ConsultasRNVM a Infracciones

Firma       : SS_1406_NDR_20151028
Decripcion  : Poner mensaje de proceso finalizado
---------------------------------------------------------------------------------}

unit frmAsignarConsultasRNVMInfracciones;

interface

uses
	DMConnection,
  Util,
	UtilProc,
  UtilDB,
  PeaTypes,
  PeaProcs,
  PeaProcsCN,
  ConstParametrosGenerales,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, DB, ADODB, DateEdit,
  DBListEx, DmiCtrls, ExtCtrls, ListBoxEx, Validate;

type
  TfrmAsignarConsultasRNVMInfraccionesForm = class(TForm)
    btnProcesar: TButton;
    btnSalir: TButton;
    spAsignarConsultasRNVMaInfracciones: TADOStoredProc;
    dsAsignarConsultasRNVMaInfracciones: TDataSource;
    lblFechaMaxima: TLabel;
    deFechaMaxima: TDateEdit;
    dblInfraccionesSinConsulta: TDBListEx;
    pnlBotones: TPanel;
    pnlParametros: TPanel;
    lblTituloGrilla: TLabel;
    txtDirectorioDestino: TPickEdit;
    lblDirectorio: TLabel;
    lblNombreArchivo: TLabel;
    edNombreArchivo: TEdit;
    procedure btnProcesarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FDestino,FNombreArchivo:AnsiString;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;
  public
    { Public declarations }
    function Inicializar() : boolean;
    procedure Procesar;
  end;

var
  frmAsignarConsultasRNVMInfraccionesForm: TfrmAsignarConsultasRNVMInfraccionesForm;

implementation

{$R *.dfm}

{----------------------------------------------------------
  					btnSalirClick
  Author: Nelson Droguett
  Date: 22-Octubre-2015
  Description: Cierra el formulario
------------------------------------------------------------}
procedure TfrmAsignarConsultasRNVMInfraccionesForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

{----------------------------------------------------------
  					btnProcesar
  Author: Nelson Droguett
  Date: 22-Octubre-2015
  Description:  Llama al proceso de Asignar las ConsultasRNVM a las infracciones
------------------------------------------------------------}
procedure TfrmAsignarConsultasRNVMInfraccionesForm.btnProcesarClick(Sender: TObject);
resourcestring
	MSG_ERROR = 'Error en el proceso de asignacion de consultas RNVM a infracciones';
  MSG_FIN   = 'El proceso ha finalizado';                                                     //SS_1406_NDR_20151028
begin
  btnProcesar.Enabled:=False;
  btnSalir.Enabled:=False;
  Screen.Cursor := crHourGlass;
	try
    if ShowMsgBoxCN(Caption, 'Esta seguro de asignar Consultas RNVM a Infracciones ?', MB_ICONQUESTION, Self) = mrOk then
    begin
    	Procesar();
      MsgBox(MSG_FIN, Caption, MB_ICONINFORMATION);                                           //SS_1406_NDR_20151028
    end;
	except on e:exception do
      begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
	end;
	Screen.Cursor := crDefault;
  btnProcesar.Enabled:=True;
  btnSalir.Enabled:=True;

end;

{----------------------------------------------------------
  					Procesar
  Author: Nelson Droguett
  Date: 22-Octubre-2015
  Description: Ejecuta el SP que asigna las consultas rnvm a las infracciones y muestra el reporte
------------------------------------------------------------}
procedure TfrmAsignarConsultasRNVMInfraccionesForm.Procesar;
resourcestring
    MSG_FINALIZACION_EXITOSA = 'El archivo "%s" se ha generado exitosamente';
var
    Archivo: TextFile;
begin
    with spAsignarConsultasRNVMaInfracciones do
    begin
      Close;
      Parameters.Refresh;
      Parameters.ParamByName('@FechaMaximaInfraccion').Value:=deFechaMaxima.Date;
      Parameters.ParamByName('@CodigoUsuario').Value:=UsuarioSistema;
      Open;
      if not IsEmpty then
      begin
        AssignFile(Archivo, (GoodDir(txtDirectorioDestino.Text) + Trim(edNombreArchivo.Text)));
        Rewrite(Archivo);

        first;
        while not eof do
        begin
          WriteLn(Archivo,Trim(FieldByName('Patente').AsString)+','+FieldByName('FechaInfraccion').AsString);
          Next;
        end;
        CloseFile(Archivo);
        MsgBox(Format(MSG_FINALIZACION_EXITOSA,
                      [GoodDir(txtDirectorioDestino.Text) + Trim(edNombreArchivo.Text)]),
                      Caption,
                      MB_ICONINFORMATION);
      end;
    end;
end;

{----------------------------------------------------------
  					FormClose
  Author: Nelson Droguett
  Date: 22-Octubre-2015
  Description: Libera el formulario cuando este se cierra
------------------------------------------------------------}
procedure TfrmAsignarConsultasRNVMInfraccionesForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;


{----------------------------------------------------------
             Inicializar
  Author: Nelson Droguett
  Date: 22-Octubre-2015
  Description:
            	Inicializa la interfaz de generaci�n de N�minas.
------------------------------------------------------------}
function TfrmAsignarConsultasRNVMInfraccionesForm.Inicializar;
resourcestring
    ERROR_CANNOT_INITIALIZE     = 'No se puede inicializar la interfaz.';
    ERROR_CANNOT_GET_PARAMETER  = 'Error obteniendo el parametro general %s';
const
    DIR_DESTINO_INFRACCION_SIN_CONSULTA = 'DIR_DESTINO_INFRACCION_SIN_CONSULTA';
    PREFIJ_ARCH_INFRACCION_SIN_CONSULTA = 'PREFIJ_ARCH_INFRACCION_SIN_CONSULTA';
begin
	Result := False;
  FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
  FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
  FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
  FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
  Color := FColorMenu;

  if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DESTINO_INFRACCION_SIN_CONSULTA, FDestino) then
  begin
    MsgBoxErr(ERROR_CANNOT_INITIALIZE, Format(ERROR_CANNOT_GET_PARAMETER, [DIR_DESTINO_INFRACCION_SIN_CONSULTA]), Caption, MB_ICONSTOP);
    Exit;
  end;
  txtDirectorioDestino.Text := GoodDir(FDestino);

  if not ObtenerParametroGeneral(DMConnections.BaseCAC, PREFIJ_ARCH_INFRACCION_SIN_CONSULTA, FNombreArchivo) then
  begin
    MsgBoxErr(ERROR_CANNOT_INITIALIZE, Format(ERROR_CANNOT_GET_PARAMETER, [PREFIJ_ARCH_INFRACCION_SIN_CONSULTA]), Caption, MB_ICONSTOP);
    Exit;
  end;
  edNombreArchivo.Text := Trim(FNombreArchivo)+'_'+FormatDateTime('yyyymmdd_hhnnss', NowBase(DMConnections.BaseCAC))+'.txt';

  deFechaMaxima.Date := NowBase(DMConnections.BaseCAC);

  Result:=True;
end;


end.
