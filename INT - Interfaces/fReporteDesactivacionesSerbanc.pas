{********************************** Unit Header ********************************
File Name : fReporteDesactivacionesSerbanc.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision : 1
    Author : pdominguez
    Date : 18/05/2009
    Description : SS 803
            - S� modific� la procedure RBIExecute.
*******************************************************************************}

unit fReporteDesactivacionesSerbanc;

interface

uses
 // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils,
    // Parametros GEnerales
    ConstParametrosGenerales,
   // DB
    UtilDB, DMConnection,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB, ppDBPipe, ppPrnabl, ppClass,
  ppCtrls, ppBands, ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB, ppParameter;


type
  TfrmReporteDesactivacionesSerbanc = class(TForm)
    spObtenerDatosReporteDesactivacionSerbanc: TADOStoredProc;
    dsReporteDesactivaciones: TDataSource;
    ppDBReporteDesactivaciones: TppDBPipeline;
    ppReportGeneracionDesactivaciones: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    pplbl_Usuario: TppLabel;
    pplblProceso: TppLabel;
    pplblFechaProceso: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppGroupCobranza: TppGroup;
    ppGroupHeaderBandCobranza: TppGroupHeaderBand;
    ppDBText4: TppDBText;
    ppLabel: TppLabel;
    ppLabel2: TppLabel;
    ppLabel8: TppLabel;
    ppLabel7: TppLabel;
    ppLabel5: TppLabel;
    ppLabel3: TppLabel;
    ppGroupFooterBandCobranza: TppGroupFooterBand;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppLabel4: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppDBText1: TppDBText;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBCalcNOtas: TppDBCalc;
    ppDBCalcMonto: TppDBCalc;
    ppDBText6: TppDBText;
    ppLabel9: TppLabel;
    ppGroupDeudor: TppGroup;
    ppGroupHeaderBandDeudor: TppGroupHeaderBand;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText5: TppDBText;
    ppGroupFooterBandDeudor: TppGroupFooterBand;
    ppParameterList1: TppParameterList;
    RBI: TRBInterface;
    procedure ppDBText4Print(Sender: TObject);
    procedure RBIExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FCodigoOperacion: Integer;
    FError : AnsIString;
    FTitulo: string;
    function PrepararReporteDesactivacionesSerbanc(var Error: AnsiString): Boolean;
  public
    { Public declarations }
    function MostrarReporte(CodigoOperacionInterfase: Integer; Titulo: AnsiString; var Error: AnsiString): boolean;
  end;

var
  frmReporteDesactivacionesSerbanc: TfrmReporteDesactivacionesSerbanc;

implementation

{$R *.dfm}
{******************************** Function Header ******************************
Function Name: PrepararReporteDesactivacionesSerbanc
Author : ndonadio
Date Created : 18/08/2005
Description :  Obtiene los datos para el reporte. Verifica que haya datos para reportar.
Parameters : var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmReporteDesactivacionesSerbanc.PrepararReporteDesactivacionesSerbanc(
  var Error: AnsiString): Boolean;
var
    Empresa: string;
begin
    Result := False;
    try
        FTitulo := 'Proceso de Desactivaci�n de Cobranzas';
        spObtenerDatosReporteDesactivacionSerbanc.CommandTimeOut := 5000;
        spObtenerDatosReporteDesactivacionSerbanc.Parameters.ParamByName('@CodigoOperacion').Value := FCodigoOperacion;
        spObtenerDatosReporteDesactivacionSerbanc.Parameters.ParamByName('@NombreEmpresaRecaudadora').Value := NULL;
        spObtenerDatosReporteDesactivacionSerbanc.Open;
        if Trim(spObtenerDatosReporteDesactivacionSerbanc.Parameters.ParamByName('@NombreEmpresaRecaudadora').Value) <> '' then
            FTitulo := FTitulo + ' para ' + Trim(spObtenerDatosReporteDesactivacionSerbanc.Parameters.ParamByName('@NombreEmpresaRecaudadora').Value);
        Result := not spObtenerDatosReporteDesactivacionSerbanc.IsEmpty;
        if not Result then Error := '';
    except
        on e:exception do begin
            Error := e.Message;
        end;
    end;
end;

function TfrmReporteDesactivacionesSerbanc.MostrarReporte(CodigoOperacionInterfase: Integer;
  Titulo: AnsiString; var Error: AnsiString): boolean;
resourcestring
    MSG_CANCELED = 'Ejecuci�n del reporte cancelada por el usuario';
begin
    Result := False;
    FCodigoOperacion := CodigoOperacionInterfase;
    try
        RBI.Caption := 'Reporte de ' + Titulo;
        if not RBI.Execute(True) then begin
            Error := FError;
            Exit;
        end;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
            Exit;
        end;
    end;
end;

{******************************** Procedure Header *****************************
Procedure Name: RBIExecute
Author :
Date Created :
Parameters : Sender: TObject; var Cancelled: Boolean
Description :

Revision : 1
    Author : pdominguez
    Date : 18/05/2009
    Description : SS 803
            - Se arregl� el Bug encontrado en el label del usuario, el cual
            mostraba al usuario del sistema, en vez del usuario responsable
            del proceso.
*******************************************************************************}
procedure TfrmReporteDesactivacionesSerbanc.RBIExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_CANT_GATHER_DATA_FOR_REPORT = 'No se pueden obtener los datos para el reporte';
    MSG_THERE_IS_NOTHING_TO_REPORT  = 'No hay datos para generar el reporte';
var
    DescError: AnsiString;
    FechaProceso: TDateTime;
begin
    if not PrepararReporteDesactivacionesSerbanc(DescError) then begin
        if trim(DescError) = '' then
            MsgBox(MSG_THERE_IS_NOTHING_TO_REPORT, Caption, MB_ICONWARNING)
        else
            MsgBoxErr(MSG_CANT_GATHER_DATA_FOR_REPORT, DescError, Caption, MB_ICONERROR);
        Cancelled := True;
        Exit;
    end;
    CurrencyString := '$';
    pplabel1.Caption := FTitulo;
    pplbl_Usuario.Caption := 'Usuario Responsable: ' + QueryGetValue(DMConnections.BaseCAC, Format('SELECT Usuario FROM LogOperacionesInterfases (NOLOCK) WHERE CodigoOperacionInterfase = %d', [FCodigoOperacion]));;
    FechaProceso := QueryGetValueDateTime(DMConnections.BaseCAC, Format('SELECT Fecha FROM LogOperacionesInterfases (NOLOCK) WHERE CodigoOperacionInterfase = %d', [FCodigoOperacion]));
    pplblProceso.Caption := Format( 'Proceso N� %d', [FCodigoOperacion]);
    pplblFechaProceso.Caption := Format( 'Fecha de Proceso: %s; Hora: %s',[FormatDateTime('dd-mm-yyyy', FechaProceso),FormatDateTime('HH:nn', FechaProceso)]);
end;

procedure TfrmReporteDesactivacionesSerbanc.ppDBText4Print(Sender: TObject);
begin
    if (spObtenerDatosReporteDesactivacionSerbanc.FieldByName('CodigoMotivoDesactivacion').asInteger = 75) or
        (spObtenerDatosReporteDesactivacionSerbanc.FieldByName('CodigoMotivoDesactivacion').asInteger = 85) then
    ppLabel8.Caption := 'Cambiado' else   ppLabel8.Caption := 'Desactivado';

end;

end.
