unit fReporteProcesoAsignacionBHTU;

interface

uses
 // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils,
    // Parametros GEnerales
    ConstParametrosGenerales,
   // DB
    UtilDB, DMConnection,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB, ppDBPipe, ppPrnabl, ppClass,
  ppCtrls, ppBands, ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB, ppParameter;


type
  TfrmReportePRocesoAsignacionBHTU = class(TForm)
    spObtenerProceso: TADOStoredProc;
    spResumenProceso: TADOStoredProc;
    dsReporte: TDataSource;
    ppDBReporte: TppDBPipeline;
    RBI: TRBInterface;
    ppReport2: TppReport;
    ppHeaderBand2: TppHeaderBand;
    ppDetailBand2: TppDetailBand;
    ppFooterBand2: TppFooterBand;
    ppShape2: TppShape;
    lbl_usuario: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    lblFechaProceso: TppLabel;
    lblNroProceso: TppLabel;
    lblUsuarioProceso: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    procedure RBIExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FCodigoOperacion: Integer;
  public
    { Public declarations }
    function Inicializar(CodigoOperacion: Integer; Titulo: AnsiString): boolean;
  end;

var
  frmReportePRocesoAsignacionBHTU: TfrmReportePRocesoAsignacionBHTU;

implementation

{$R *.dfm}

{ TfrmReportePRocesoAsignacionBHTU }

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 06/10/2005
Description : El usuario ordena la ejecucion
Parameters : CodigoOperacion: Integer; Titulo: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmReportePRocesoAsignacionBHTU.Inicializar(CodigoOperacion: Integer; Titulo: AnsiString): boolean;
begin
    Result := False;
    FCodigoOperacion := CodigoOperacion;
     try
        RBI.Caption := 'Reporte de ' + Titulo;
        if not RBI.Execute(True) then Exit;
        Result := True;
    except
        on e:exception do begin
            MsgBoxErr( 'No se puede mostrar el reporte', e.Message, Caption, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: RBIExecute
Author : ndonadio
Date Created : 06/10/2005
Description : Cargo los datos del reporte
Parameters : Sender: TObject; var Cancelled: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmReportePRocesoAsignacionBHTU.RBIExecute(Sender: TObject; var Cancelled: Boolean);
begin
    try
        with   spObtenerProceso, Parameters do begin
            ParamByName('@CodigoProcesoAsignacionBHTU').Value := FCodigoOperacion;
            Open;
            // cargo los labels
            lblFechaProceso.Caption   := FormatDateTime('dd/mm/yyyy', FieldByName('FechaHoraInicio').AsDateTime) + ' ;  Hora: '
              + FormatDateTime('hh:nn', FieldByName('FechaHoraInicio').AsDateTime);
            lblNroProceso.Caption     :=  IntToStr(FCodigoOperacion);
            lblUsuarioProceso.Caption :=  FieldByName('Usuario').asString;
            Close;
        end;

        // Abro el resumen del proceso
        spResumenProceso.Parameters.ParamByName('@CodigoProcesoAsignacionBHTU').Value := FCodigoOperacion;
        spResumenProceso.Open;

    except
        on e:exception do begin
            Cancelled := True;
            MsgBoxErr( 'No se puede mostrar el reporte', e.MEssage, Caption, MB_ICONERROR);
            Exit;
        end;
    end;
end;

end.
