{------------------------------------------------------------------------------
 File Name: frmRptRecepcionUniversoBancoChile.pas
 Author: jjofre
 Date Created: 02 de Junio de 2010
 Description: Reporte Universo Banco de Chile



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


--------------------------------------------------------------------------------}
unit frmRptRecepcionUniversoBancoChile;

interface

uses
  DMConnection,
  Util,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppProd, ppClass, ppReport, UtilRB, ppComm, ppRelatv, ppDB, ppDBPipe,
  DB, DBClient, ppParameter, ppBands, ppCtrls, ppPrnabl, ppStrtch, ppSubRpt,
  ppCache, ADODB, RBSetup, UtilProc, ConstParametrosGenerales, jpeg;                  //SS_1147_NDR_20140710

type
  TRptRecepcionUniversoBancoChileForm = class(TForm)
    cdsErrores: TClientDataSet;
    dsErrores: TDataSource;
    ppdbErrores: TppDBPipeline;
    ppReporteRecepUniversoBancoChile: TppReport;
    ppDetailBand1: TppDetailBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppHeaderBand2: TppHeaderBand;
    ppLabel1: TppLabel;
    lbl_usuario: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppImage2: TppImage;
    ppDetailBand2: TppDetailBand;
    ppDBText3: TppDBText;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppLine2: TppLine;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppParameterList1: TppParameterList;
    rbiListado: TRBInterface;
    spObtenerErroresInterfase: TADOStoredProc;
    qryBanco: TADOQuery;
    lblTitArchivoRecibido: TppLabel;
    lblTitUsuarioProceso: TppLabel;
    lblArchivoRecibido: TppLabel;
    lblUsuarioProceso: TppLabel;
    lblFechaProceso: TppLabel;
    lblNroProceso: TppLabel;
    lblTitLineasArchivo: TppLabel;
    lblTitConveniosDuplicados: TppLabel;
    lblTitRegistrosExito: TppLabel;
    lblTitRegistrosError: TppLabel;
    lblLineasArchivo: TppLabel;
    lblConveniosDuplicados: TppLabel;
    lblRegistrosExito: TppLabel;
    lblRegistrosError: TppLabel;
    spObtenerDatosProcesoUniversoBancoChile: TADOStoredProc;
    private
    { Private declarations }
    FCodigoOperacion: Integer;
  public
    { Public declarations }
    procedure MostrarReporte(CodigoOperacion: Integer);
  end;

var
  RptRecepcionUniversoBancoChileForm: TRptRecepcionUniversoBancoChileForm;

implementation

{$R *.dfm}

procedure TRptRecepcionUniversoBancoChileForm.MostrarReporte(CodigoOperacion: Integer);
resourcestring
    PROCESSING_ERRORS_FILE				= 'Universo Banco Chile - Errores Procesamiento ';
    REPORT_DIALOG_CAPTION 				= 'Universo Banco Chile - Errores';
    MSG_ERROR_SAVING_ERRORS_FILE 	    = 'Error generando archivo de errores';
    MSG_ERR_REPORT                      = 'El Reporte de Errores se guardo en: %s';
    TITLE_ERR_REPORT                    = 'Reporte de Errores';
    MSG_THERE_IS_NO_BANK                = 'Banco Desconocido';
    MSG_PROCESS_NUMBER                  = 'Proceso N� %d';
    MSG_PROCESS_DATETIME                = 'Fecha de Proceso: %s Hora: %s';
var
  	Config: TRBConfig;
    HayBanco: boolean;
    iCodSBEI : Integer;
    sNOmbreBanco: string;
    FechaProceso: TDateTime;
    RutaLogo: AnsiString;                                                                               //SS_1147_NDR_20140710
begin


    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710



    FCodigoOperacion := CodigoOperacion;

    //Manda los errores al cdsErrores
    qrybanco.Open;

    ///Mover esto a la pag�na de reporte
    cdsErrores.CreateDataSet ;

    spObtenerErroresInterfase.Close;
    spObtenerErroresInterfase.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
    spObtenerErroresInterfase.Open;
    if (spObtenerErroresInterfase.Eof) then
        Exit;

	Screen.Cursor := crHourglass;
    spObtenerErroresInterfase.First;
    while (not spObtenerErroresInterfase.Eof) do begin
        HayBanco := qryBanco.Locate('CodigoBancoSBEI',iVal(Copy(spObtenerErroresInterfase.FieldByName('DescripcionError').AsString,1,4)),[]);
        sNombreBanco := iif(HayBanco,qryBanco.FieldByName('Descripcion').asString,MSG_THERE_IS_NO_BANK);
        iCodSBEI := iif(HayBanco, iVal( Copy(spObtenerErroresInterfase.FieldByName('DescripcionError').AsString,1,4) ), 0);
        cdsErrores.InsertRecord([iCodSBEI, sNombreBanco, Copy(spObtenerErroresInterfase.FieldByName('DescripcionError').AsString,5,Length(spObtenerErroresInterfase.FieldByName('DescripcionError').AsString)) ] );

        spObtenerErroresInterfase.Next;
    end;
    spObtenerErroresInterfase.Close;

    //Datos del Encabezado
    spObtenerDatosProcesoUniversoBancoChile.Close;
    spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
    spObtenerDatosProcesoUniversoBancoChile.ExecProc;

    lblArchivoRecibido.Caption := spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@NombreArchivo').Value;
    lblUsuarioProceso.Caption := spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@UsuarioProceso').Value;
    FechaProceso := spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@FechaHoraProceso').Value;
    lblLineasArchivo.Caption := spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@LineasArchivo').Value;
    lblConveniosDuplicados.Caption := spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@ConveniosDuplicados').Value;
    lblRegistrosExito.Caption := spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@RegistrosExito').Value;
    lblRegistrosError.Caption := spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@RegistrosError').Value;

    lblNroProceso.Caption := Format( MSG_PROCESS_NUMBER, [FCodigoOperacion]);
    lblFechaProceso.Caption := Format( MSG_PROCESS_DATETIME, [FormatDateTime(ShortDateFormat, FechaProceso),
        FormatDateTime('HH:nn', FechaProceso)]);

	Screen.Cursor := crDefault;

	// Asigna los Pipelines del reporte
	//ppErroresPipeline.FileName := sArchivoErroresProcesamiento;

    rbiListado.Caption := REPORT_DIALOG_CAPTION;

	// Carga la Configuraci�n de Usuario
    // la funci�n del componente RBInterface no funciona en este caso
    // porque el reporte tiene subreportes y el label est� dentro de un subreporte
    // Adem�s existen 3 labes uno en cada subreporte y no se pueden llamar igual
  	Config := rbiListado.GetConfig;
    if Config.ShowUser then begin
    	if UsuarioSistema <> '' then lbl_usuario.caption := 'Usuario: ' +
        	UsuarioSistema + ' ' else lbl_usuario.Caption := '';
    end else begin
    	lbl_usuario.Caption := '';
    end;

    if Config.ShowDateTime then begin
    	lbl_usuario.caption := lbl_usuario.caption +
        FormatShortDate(Date) + ' ' + FormatTime(Time);
    end;
    rbiListado.Execute(True);
end;


end.
