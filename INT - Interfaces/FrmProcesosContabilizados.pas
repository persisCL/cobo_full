{-------------------------------------------------------------------------------
 File Name: FrmProcesosContabilizados.pas
 Author:    lgisuk
 Date Created: 12/09/2005
 Language: ES-AR
 Description:  M�dulo de la interface contable - Ver Procesos Contabilizados
-------------------------------------------------------------------------------}
unit FrmProcesosContabilizados;

interface

uses
  //Log Operaciones
  DMConnection,                      //Coneccion a base de datos OP_CAC
  Util,                              //IIF
  UtilProc,                          //Mensajes
  Rstrings,                          //STR_ERROR
  FrmRptVerificacionInterfazContable,//Reporte de Finalizacion de Proceso
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, StdCtrls, ExtCtrls, DPSControls, ListBoxEx,
  DBListEx, Validate, DateEdit, StrUtils;

type
  TFProcesosContabilizados = class(TForm)
    DataSource: TDataSource;
    PAbajo: TPanel;
    PDerecha: TPanel;
    Parriba: TPanel;
    PAderecha: TPanel;
    DBListEx: TDBListEx;
    Edesde: TDateEdit;
    EHasta: TDateEdit;
    LDesde: TLabel;
    LHasta: TLabel;
    SalirBTN: TButton;
    AbrirBTN: TButton;
    spObtenerLogProcesosContabilizados: TADOStoredProc;
    procedure AbrirBTNClick(Sender: TObject);
    procedure SalirBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBListExLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure DBListExColumns0HeaderClick(Sender: TObject);
    procedure DBListExDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
  private
    Function TieneInformeAsociado(DataSet: TDataSet): boolean;
    { Private declarations }
  public
    function Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
    { Public declarations }
  end;

var
  FProcesosContabilizados: TFProcesosContabilizados;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 12/09/2005
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFProcesosContabilizados.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
Var
  	S: TSize;
begin
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Result := False;
    //Manejo el MdiChild
    if MDIChild then begin
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
    end else begin
        FormStyle := fsNormal;
        Visible := False;
    end;
  	CenterForm(Self);
  	Caption := AnsiReplaceStr(txtCaption, '&', '');
    //Fechas
    edesde.Date:=now;
    ehasta.Date:=now;
    try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AbrirBTNClick
  Author:    lgisuk
  Date Created: 12/09/2005
  Description: Abro la consulta
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFProcesosContabilizados.AbrirBTNClick(Sender: TObject);
Resourcestring
    STR_FECHA_DESDE = 'Debe Indicar Fecha Desde';
    STR_FECHA_HASTA = 'Debe Indicar Fecha Hasta';
    STR_FECHA_MAYOR = 'La Fecha Hasta debe ser mayor a la Fecha Desde';
begin
    //Obligo a cargar fecha desde
    if edesde.Date = NULLDATE then begin
        MsgBoxBalloon(STR_FECHA_DESDE, STR_ERROR, MB_ICONSTOP, edesde);
        edesde.SetFocus;
        Exit;
    end;

    //Obligo a cargar fecha hasta
    if ehasta.Date = NULLDATE then begin
        MsgBoxBalloon(STR_FECHA_HASTA, STR_ERROR, MB_ICONSTOP, ehasta);
        ehasta.SetFocus;
        exit;
    end;

    //valido que la fecha hasta sea mayor a la fecha desde
    if edesde.date > ehasta.Date then begin
        MsgBoxBalloon(STR_FECHA_MAYOR, STR_ERROR, MB_ICONSTOP, ehasta);
        ehasta.SetFocus;
        exit;
    end;

    //Abro la consulta
    with SPObtenerLogProcesosContabilizados.Parameters do begin
        Refresh;
        ParamByName('@DesdeFecha').Value:= Edesde.date;
        ParamByName('@HastaFecha').Value:= Ehasta.date;
    end;
    try
        SPObtenerLogProcesosContabilizados.Close;
        SPObtenerLogProcesosContabilizados.Open;
    except
        on e: Exception do begin
            MsgBoxErr('Error', e.message, 'Error Al', MB_ICONERROR); //el stored procedure podria fallar
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListExColumns0HeaderClick
  Author:    lgisuk
  Date Created: 12/09/2005
  Description: permito ordenar haciendo click en la columna
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFProcesosContabilizados.DBListExColumns0HeaderClick(Sender: TObject);
begin
	if SPObtenerLogProcesosContabilizados.IsEmpty then Exit;
	if TDBListExColumn(sender).Sorting = csAscending then begin
      TDBListExColumn(sender).Sorting := csDescending;
  end else begin
      TDBListExColumn(sender).Sorting := csAscending;
  end;
	SPObtenerLogProcesosContabilizados.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

{-----------------------------------------------------------------------------
  Function Name: TieneInformeAsociado
  Author:    lgisuk
  Date Created: 12/09/2005
  Description: Verifica si el proceso tiene informe asociado
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFProcesosContabilizados.TieneInformeAsociado(DataSet: TDataSet): boolean;
Const
    FINALIZO_OK = 2;
begin
    Result:=False;
    try
        with SPObtenerLogProcesosContabilizados do begin
            result:= Fieldbyname('Estado').AsInteger = FINALIZO_OK;
        end;
    except
        on e: Exception do begin
            MsgBoxErr('Error', e.message, 'Error Al', MB_ICONERROR); //el stored procedure podria fallar
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListExDrawText
  Author:    lgisuk
  Date Created: 12/09/2005
  Description: Asigno formato a la grilla
  Parameters: Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFProcesosContabilizados.DBListExDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
begin
    //Muestro 'Ver Informe..' si el registro tiene un informe asociado
    if Column.Index = 8 then begin
        //si tiene asociado informe
        if TieneInformeAsociado(SPObtenerLogProcesosContabilizados) then begin
              text:='Ver Informe..';
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListExLinkClick
  Author:    lgisuk
  Date Created: 12/09/2005
  Description: Muestra el Reporte de Finalizacion de Proceso
  Parameters: Sender: TCustomDBListEx;Column: TDBListExColumn
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFProcesosContabilizados.DBListExLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
Const
    STR_TITLE = 'Reporte de Finalizaci�n';
var
    F: TFRptVerificacionInterfazContable;
    NumeroProcesoContabilizacion:integer;
begin
    //si el registo no tiene informe asociado salgo
    if SPObtenerLogProcesosContabilizados.active = false then exit;
    if TieneInformeAsociado(SPObtenerLogProcesosContabilizados) = false then exit;

    with SPObtenerLogProcesosContabilizados do begin

        //Obtengo el codigo de operacion interface
        NumeroProcesoContabilizacion:=FieldByName('NumeroProcesoContabilizacion').value;

        //muestro el reporte
        Application.CreateForm(TFRptVerificacionInterfazContable,F);
        if not f.Inicializar(NumeroProcesoContabilizacion,STR_TITLE) then f.Release;

    end;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 12/09/2005
  Description: Cierro el Formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFProcesosContabilizados.SalirBTNClick(Sender: TObject);
begin
    close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 12/09/2005
  Description: Lo libero de Memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFProcesosContabilizados.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
