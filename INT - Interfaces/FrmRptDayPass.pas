{-------------------------------------------------------------------------------
 File Name: FrmRptDayPass.pas
 Author:    Flamas, lgisuk
 Date Created: 11/03/2005
 Language: ES-AR
 Description: Gestion de Interfases - Reporte de DayPass
              Informa la cantidad de DayPass recibidos , el importe total y
              otra informacion util para verificar si el proceso de recepcion
              se realizo segun parametros normales.

  Revision 1
  Author : mpiazza
  Date: 16-07-2009
  Description: ss-653  se agregan al reporte el importe unitario por categoria importeunitarioCategoria1#



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
Unit FrmRptDayPass;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  UtilProc,                  //Mensajes
  //general
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,RBSetup, ConstParametrosGenerales;          //SS_1147_NDR_20140710

type
  TFRptDayPass = class(TForm)
    DataSource: TDataSource;
    SpObtenerReporteRecepcionDayPass: TADOStoredProc;
    ppReporte: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppModulo: TppLabel;
    pptFechaProcesamiento: TppLabel;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    ppMaxFechaEmision: TppLabel;
    ppLabel1: TppLabel;
    label1: TppLabel;
    ppFechaProcesamiento: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppCantidadComprobantes: TppLabel;
    ppLine3: TppLine;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppTipoComprobante: TppLabel;
    ppLine4: TppLine;
    ppLine5: TppLine;
    ppLabel14: TppLabel;
    ppImporteTotal: TppLabel;
    pptImporteTotal: TppLabel;
    ppLine6: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    ppParameterList1: TppParameterList;
    ppDBText1: TppDBText;
    ppDBPipeline1: TppDBPipeline;
    RBIListado: TRBInterface;
    ppLabel15: TppLabel;
    ppCantidadAceptados: TppLabel;
    ppLabel17: TppLabel;
    ppCantidadRechazados: TppLabel;
    ppLabel20: TppLabel;
    ppCantidadTotal: TppLabel;
    ppLabel16: TppLabel;
    importeunitarioCategoria1: TppLabel;
    importeunitarioCategoria2: TppLabel;
    importeunitarioCategoria3: TppLabel;
    importeunitarioCategoria4: TppLabel;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FNombreReporte:string;
    FCodigoOperacionInterfase:integer;
    FCantidadTotal, FCantidadErrores : integer;
    { Private declarations }
  public
    function Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase, CantidadComprobantes, CantidadErrores : integer): Boolean;
    { Public declarations }
  end;

var
  FRptDayPass: TFRptDayPass;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 13/04/2005
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRptDayPass.Inicializar;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin

    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    FNombreReporte            := NombreReporte;
    FCodigoOperacionInterfase := CodigoOperacionInterfase;
    FCantidadTotal            := CantidadComprobantes;
    FCantidadErrores          := CantidadErrores;
    RBIListado.Execute;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 13/04/2005
  Description:  Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None

  Revision 1
  Author : mbecerra
  Date: 10-Junio-2008
  Description:
                Se modificó para recibir los parámetros de cantidad de registros procesados
                y cantidad de registros con errores.

                Y estos dos debiesen cuadrar con el parámetro retornado por el stored:
                @CantidadComprobantes = cantidad registros aceptados

  Revision 2
  Author : mpiazza
  Date: 16-07-2009
  Description: ss-653 se agregan al reporte el importe unitario por categoria importeunitarioCategoria1#

-----------------------------------------------------------------------------}
procedure TFRptDayPass.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
resourcestring
	MSG_ERROR_AL_GENERAR_REPORTE = 'Error al generar reporte';
    MSG_ERROR = 'Error';
var
    Config: TRBConfig;
begin
    //configuración del reporte
    Config := rbiListado.GetConfig;
    if Config.BinName <> '' then rbiListado.Report.PrinterSetup.BinName := Config.BinName;
    try
        //Abro la consulta
        with SpObtenerReporteRecepcionDayPass do begin

            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := Fcodigooperacioninterfase;
            Parameters.ParamByName('@Modulo').Value := NULL;
            Parameters.ParamByName('@FechaProcesamiento').Value := NULL;
            Parameters.ParamByName('@NombreArchivo').Value := NULL;
            Parameters.ParamByName('@Usuario').Value := NULL;
            Parameters.ParamByName('@CantidadComprobantes').Value := NULL;
            Parameters.ParamByName('@ImporteTotal').Value := NULL;
            Parameters.ParamByName('@CantidadCategoria1').Value := NULL;
	        Parameters.ParamByName('@ImporteCategoria1').Value := NULL;
	        Parameters.ParamByName('@CantidadCategoria2').Value := NULL;
	        Parameters.ParamByName('@ImporteCategoria2').Value := NULL;
	        Parameters.ParamByName('@CantidadCategoria3').VALUE := NULL;
	        Parameters.ParamByName('@ImporteCategoria3').Value := NULL;
	        Parameters.ParamByName('@CantidadCategoria4').Value := NULL;
	        Parameters.ParamByName('@ImporteCategoria4').Value := NULL;
            Parameters.ParamByName('@TotalRegistros').Value := NULL;
            Parameters.ParamByName('@TotalProcesados').Value := NULL;
            Parameters.ParamByName('@importeunitarioCategoria1').Value := NULL;
            Parameters.ParamByName('@importeunitarioCategoria2').Value := NULL;
            Parameters.ParamByName('@importeunitarioCategoria3').Value := NULL;
            Parameters.ParamByName('@importeunitarioCategoria4').Value := NULL;
            CommandTimeOut := 500;
            Open;
            if SpObtenerReporteRecepcionDayPass.IsEmpty then begin
              	Close;
                Exit;
            end;

            //Asigno los valores a los campos del reporte
            ppmodulo.Caption:= Parameters.ParamByName('@Modulo').Value;
            ppFechaProcesamiento.Caption:= DateToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
            ppNombreArchivo.Caption:= copy(Parameters.ParamByName('@NombreArchivo').Value,1,40);
            ppUsuario.Caption:= Parameters.ParamByName('@Usuario').Value;
            { Revision 1}
            ppCantidadComprobantes.Caption := Parameters.ParamByName('@CantidadComprobantes').Value;
            ppCantidadTotal.Caption        := Parameters.ParamByName('@TotalRegistros').Value;
            ppCantidadAceptados.Caption    := Parameters.ParamByName('@TotalProcesados').Value;
            ppCantidadRechazados.Caption   := Parameters.ParamByName('@TotalRegistros').Value - Parameters.ParamByName('@TotalProcesados').Value;
            ppImporteTotal.Caption:= Parameters.ParamByName('@ImporteTotal').Value;
            pplabel6.Caption := inttostr(Parameters.ParamByName('@CantidadCategoria1').Value);
            pplabel7.Caption := inttostr(Parameters.ParamByName('@CantidadCategoria2').Value);
            pplabel8.Caption := inttostr(Parameters.ParamByName('@CantidadCategoria3').Value);
            pplabel9.Caption := inttostr(Parameters.ParamByName('@CantidadCategoria4').Value);
            pplabel10.Caption := Parameters.ParamByName('@ImporteCategoria1').Value;
            pplabel11.Caption := Parameters.ParamByName('@ImporteCategoria2').Value;
            pplabel12.Caption := Parameters.ParamByName('@ImporteCategoria3').Value;
            pplabel13.Caption := Parameters.ParamByName('@ImporteCategoria4').Value;
            importeunitarioCategoria1.Caption := Parameters.ParamByName('@importeunitarioCategoria1').Value;
            importeunitarioCategoria2.Caption := Parameters.ParamByName('@importeunitarioCategoria2').Value;
            importeunitarioCategoria3.Caption := Parameters.ParamByName('@importeunitarioCategoria3').Value;
            importeunitarioCategoria4.Caption := Parameters.ParamByName('@importeunitarioCategoria4').Value;
        end;

        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption :=  FNombreReporte;



    except
        on E: exception do begin
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.
