{-----------------------------------------------------------------------------
 File Name: frmRespuestasAConsultasRNVM.pas
 Author   : Nelson Droguett
 Date Created: 07/10/2015
 Description: Modulo de la interfaz Respuestas a Consultas RNVM

Firma       : SS_1400_NDR_20151007
Descripcion : Se crea la interfaz entrante de respuestas a consultas RNVM
------------------------------------------------------------------------------}
unit frmRespuestasAConsultasRNVM;

interface

uses
  //Recepcion de Rendiciones ConsultaRNVM
  ComunesInterfaces,
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  FrmRptRecepcionRespuestasConsultasRNVM,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, PeaTypes, UtilDB, DPSControls, ppDB, ppTxPipe, StrUtils,
  ppParameter, ppModule, raCodMod, ppCtrls, ppBands, ppReport, ppPrnabl,
  ppClass, ppStrtch, ppSubRpt, ppCache, ppComm, ppRelatv, ppProd, UtilRB,
  RStrings, RBSetup,ppDBPipe;

type
  TRespuestaAConsultaRNVMRecord = record
    PATENTE     : string;
  	DV_PATENTE  : string;
    MARCA       : string;
    MODELO      : string;
    CATEGORIA   : string;
    FEC_COMPRA  : string;
    COLOR       : string;
    COLOR2      : string;
    RUT         : string;
    NOMBRE      : string;
    AP_MATERNO  : string;
    AP_PATERNO  : string;
    ANO         : string;
    COMUNA      : string;
    REGION      : string;
    CALLE       : string;
    NUMERO      : string;
    RESTO_DIRE  : string;

  end;

  TfRespuestasAConsultasRNVM = class(TForm)
    Bevel1: TBevel;
    btnAbrirArchivo: TSpeedButton;
    Label2: TLabel;
    edOrigen: TEdit;
    OpenDialog: TOpenDialog;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    SpObtenerResumenRespuestasRNVMRecibidas: TADOStoredProc;
    SPActualizarRespuestaAConsultaRNVM: TADOStoredProc;
    SpObtenerResumenRespuestasRNVMRecibidasDescripcionError: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
    { Private declarations }
    FCodigoOperacion : integer;
    FLineas: integer;
   	FDetenerImportacion: boolean;
  	FRespuestasConsultasRNVMTXT : TStringList;
    FErrores : TStringList;
  	FErrorMsg : string;


    FRNVM_DIR_ARCHIVO_RESPUESTAS_MASIVAS : AnsiString;
   	FRNVM_DIR_ARCHIVO_RESP_PROCESADOS : AnsiString;
    FRNVM_DIR_ARCHIVO_RESP_RECHAZADOS : AnsiString;
   	FRNVM_DIR_ARCHIVO_RESP_ERRORES : AnsiString;


  	function  ParseRespuestaConsultaRNVMLine( sline : string; var ConsultaRNVMRecord : TRespuestaAConsultaRNVMRecord; var sParseError : string ) : boolean;
  	function  AnalizarRespuestasConsultasRNVMTXT : boolean;
    function  CargarRespuestasConsultasRNVM : boolean;
  	function  RegistrarOperacion : boolean;

    Function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
  public
	{ Public declarations }
  	function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
  end;

var
  fRespuestasAConsultasRNVM: TfRespuestasAConsultasRNVM;

Const
	RO_MOD_INTERFAZ_ENTRANTE_RESPUESTAS_CONSULTAS_RNVM		= 111;

implementation


{$R *.dfm}
function TfRespuestasAConsultasRNVM.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;

    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF;
    const
        RNVM_DIR_ARCHIVO_RESPUESTAS_MASIVAS = 'RNVM_DIR_ARCHIVO_RESPUESTAS_MASIVAS';
        RNVM_DIR_ARCHIVO_RESP_PROCESADOS    = 'RNVM_DIR_ARCHIVO_RESP_PROCESADOS';
        RNVM_DIR_ARCHIVO_RESP_RECHAZADOS    = 'RNVM_DIR_ARCHIVO_RESP_RECHAZADOS';
        RNVM_DIR_ARCHIVO_RESP_ERRORES       = 'RNVM_DIR_ARCHIVO_RESP_ERRORES';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, RNVM_DIR_ARCHIVO_RESPUESTAS_MASIVAS  , FRNVM_DIR_ARCHIVO_RESPUESTAS_MASIVAS) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + RNVM_DIR_ARCHIVO_RESPUESTAS_MASIVAS;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FRNVM_DIR_ARCHIVO_RESPUESTAS_MASIVAS := GoodDir(FRNVM_DIR_ARCHIVO_RESPUESTAS_MASIVAS);
                if  not DirectoryExists(FRNVM_DIR_ARCHIVO_RESPUESTAS_MASIVAS) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FRNVM_DIR_ARCHIVO_RESPUESTAS_MASIVAS;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, RNVM_DIR_ARCHIVO_RESP_PROCESADOS  , FRNVM_DIR_ARCHIVO_RESP_PROCESADOS) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + RNVM_DIR_ARCHIVO_RESP_PROCESADOS;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FRNVM_DIR_ARCHIVO_RESP_PROCESADOS := GoodDir(FRNVM_DIR_ARCHIVO_RESP_PROCESADOS);
                if  not DirectoryExists(FRNVM_DIR_ARCHIVO_RESP_PROCESADOS) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FRNVM_DIR_ARCHIVO_RESP_PROCESADOS;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, RNVM_DIR_ARCHIVO_RESP_RECHAZADOS  , FRNVM_DIR_ARCHIVO_RESP_RECHAZADOS) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + RNVM_DIR_ARCHIVO_RESP_RECHAZADOS;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FRNVM_DIR_ARCHIVO_RESP_RECHAZADOS := GoodDir(FRNVM_DIR_ARCHIVO_RESP_RECHAZADOS);
                if  not DirectoryExists(FRNVM_DIR_ARCHIVO_RESP_RECHAZADOS) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FRNVM_DIR_ARCHIVO_RESP_RECHAZADOS;
                    Result := False;
                    Exit;
                end;


                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, RNVM_DIR_ARCHIVO_RESP_ERRORES  , FRNVM_DIR_ARCHIVO_RESP_ERRORES) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + RNVM_DIR_ARCHIVO_RESP_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FRNVM_DIR_ARCHIVO_RESP_ERRORES := GoodDir(FRNVM_DIR_ARCHIVO_RESP_ERRORES);
                if  not DirectoryExists(FRNVM_DIR_ARCHIVO_RESP_ERRORES) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FRNVM_DIR_ARCHIVO_RESP_ERRORES;
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
  	Result := False;

    //Elijo el modo en que si visualizara la ventana
  	if not MDIChild then begin
  		FormStyle := fsNormal;
  		Visible := False;
  	end;

    //Centro el form
  	CenterForm(Self);
  	try
        DMConnections.BaseCAC.Connected := True;
    		Result := DMConnections.BaseCAC.Connected and VerificarParametrosGenerales;
  	except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;

    //Resolver ac� lo que necesita este form para inicializar correctamente
   	Caption := AnsiReplaceStr(txtCaption, '&', '');
  	btnCancelar.Enabled := False;
  	btnProcesar.Enabled := False;
  	pnlAvance.Visible := False;
  	lblReferencia.Caption := '';
end;





procedure TfRespuestasAConsultasRNVM.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_RENDICION          = ' ' + CRLF +
                          'Explicaci�n al usuario sobre el uso de la interfaz';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_RENDICION, Caption, MB_ICONQUESTION, IMGAYUDA);
end;





procedure TfRespuestasAConsultasRNVM.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;






procedure TfRespuestasAConsultasRNVM.btnAbrirArchivoClick(Sender: TObject);



    Function ObtenerFiltro(var Filtro: AnsiString): Boolean;
    Const
        FILE_TITLE			= 'Resuestas a Consultas RNVM|';
    begin
        //Creo el Filtro
    		Filtro := FILE_TITLE + 'VSUR' + '*';     // CN para cnorte VS para VespucioSur ??
        Result:= True;
    end;



    function ValidarNombreArchivoConsultaRNVM(sFileName, sTipoArchivo : string) : boolean;
    resourcestring
        MSG_INVALIDA_FILE_NAME = 'El nombre del archivo %s es inv�lido';
        MSG_ERROR  = 'Error';
    begin
        result :=	( Copy(ExtractFileName(sFileName), 1, 4) = sTipoArchivo)
                  and
                  (Copy(ExtractFileName(sFileName), 13, 8) = '_RES.TXT');
    end;

resourcestring
    MSG_ERROR                     = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST = 'El archivo %s no existe';
  	MSG_ERROR_INVALID_FILENAME 		= 'El nombre del archivo %s es inv�lido';
  	MSG_FILE_ALREADY_PROCESSED 		= 'El archivo %s ya fue procesado';
    MSG_FILE_WISH_TO_REPROCESS		= 'Desea volver a procesarlo ?';
var
    Filtro: string;
begin
    //obtengo el filtro a aplicar a este tipo de archivos
    ObtenerFiltro(Filtro);

    Opendialog.InitialDir := FRNVM_DIR_ARCHIVO_RESPUESTAS_MASIVAS;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if opendialog.execute then
    begin

        edOrigen.text:=UpperCase( opendialog.filename );

     		if  not ValidarNombreArchivoConsultaRNVM( UpperCase(edOrigen.text),
                                                  'VSUR') then begin             // CN para cnorte VS para VespucioSur ??
           	MsgBox( Format ( MSG_ERROR_INVALID_FILENAME, [ExtractFileName(edOrigen.text) ]),
                    MSG_ERROR,
                    MB_ICONERROR
                  );
            Exit;
        end;

        if not FileExists( edOrigen.text ) then begin
        	  MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

      	if  VerificarArchivoProcesado( DMConnections.BaseCAC, edOrigen.text ) then begin
        	  MsgBox( Format ( MSG_FILE_ALREADY_PROCESSED, [edOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

        if FRNVM_DIR_ARCHIVO_RESP_PROCESADOS <> '' then begin
            if RightStr(FRNVM_DIR_ARCHIVO_RESP_PROCESADOS,1) = '\' then  FRNVM_DIR_ARCHIVO_RESP_PROCESADOS := FRNVM_DIR_ARCHIVO_RESP_PROCESADOS + ExtractFileName(edOrigen.text)
            else FRNVM_DIR_ARCHIVO_RESP_PROCESADOS := FRNVM_DIR_ARCHIVO_RESP_PROCESADOS + '\' + ExtractFileName(edOrigen.text);
        end;

      btnProcesar.Enabled := True;

	  end;
end;





function TfRespuestasAConsultasRNVM.ParseRespuestaConsultaRNVMLine( sline : string; var ConsultaRNVMRecord : TRespuestaAConsultaRNVMRecord; var sParseError : string ) : boolean;
const
    LARGO_LINEA_ConsultaRNVM = 498;
resourcestring
  	MSG_ERROR_INVALID_COMPANY 			    = 'La empresa prestadora de servicios es inv�lida';
    MSG_ERROR_DATE              		    = 'La fecha de cargo es inv�lida';
    MSG_ERROR_INVALID_AMMOUNT   		    = 'El monto del d�bito es inv�lido';
    MSG_ERROR_INVALID_INVOICE_NUMBER  	= 'El n�mero de la nota de cobro es inv�lido';
    MSG_ERROR_INVALID_LENGTH            = 'El largo de la l�nea es incorrecto';
    MSG_ERROR_INVALID_INVOICE_POSITION  = 'El campo correspondiente a nota de cobro est� corrido a izquierda';
var
    UltimoCaracter: string;
    Caracter: char;
begin
  	Result := False;

    if Length(sline) > LARGO_LINEA_ConsultaRNVM then begin
        sParseError	:= MSG_ERROR_INVALID_LENGTH;
        Exit;
    end;

    with ConsultaRNVMRecord do begin

		    try
          //                                                                                                                                                                                                 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
          //111111233333333333333333333333333333333333333334444444444444444444444444444444444444444555555555555555555555556666666677777777777777777777777777777788888888888888888888888888888888888999999999900000000000000000000000000000000000000000000000000000000000011111111111111111111111111111111111111112222222222222222222222222222222222222222333344444444444444444444444444444444444444444444444444556666666666666666666666666666666666666666666667777777777778888888888888888888888888888888888888888888888888888
          //         1         2         3         4         5         6         7         8         9         10        11        12        13        14        15        16         17       18        19        20        21        22        23        24        25        26        27        28         29       30        31        32        33        34        35        36        37        38        39        40        41        42        43        44        45        46        47        48        49        50
          //12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
          PATENTE     :=Copy(sline,1,6);      //1
          DV_PATENTE  :=Copy(sline,7,1);      //2
          MARCA       :=Copy(sline,8,40);     //3
          MODELO      :=Copy(sline,48,40);    //4
          CATEGORIA   :=Copy(sline,88,23);    //5
          FEC_COMPRA  :=Copy(sline,111,8);    //6
          COLOR       :=Copy(sline,119,30);   //7
          COLOR2      :=Copy(sline,149,35);   //8
          RUT         :=Copy(sline,184,10);   //9
          NOMBRE      :=Copy(sline,194,60);   //10
          AP_PATERNO  :=Copy(sline,254,40);   //11
          AP_MATERNO  :=Copy(sline,294,40);   //12
          ANO         :=Copy(sline,334,4);    //13
          COMUNA      :=Copy(sline,338,50);   //14
          REGION      :=Copy(sline,388,2);    //15
          CALLE       :=Copy(sline,390,45);   //16
          NUMERO      :=Copy(sline,435,12);   //17
          RESTO_DIRE  :=Copy(sline,447,52);   //18

          if FEC_COMPRA='00000000' then FEC_COMPRA:='20000101';
          if Length(RUT)>9 then RUT := RightStr(RUT,9);



          sParseError				:= '';
          result := True;
        except

        	on exception do Exit;

        end;
    end;
end;





function TfRespuestasAConsultasRNVM.AnalizarRespuestasConsultasRNVMTXT : boolean;
resourcestring
  	MSG_ANALIZING_RENDERINGS_FILE	= 'Analizando Archivo de Respuestas a Consultas RNVM - Cantidad de lineas : %d';
    MSG_RENDERINGS_FILE_HAS_ERRORS 	= 'El archivo de Respuestas a Consultas RNVM contiene errores'#10#13'L�nea : %d';
    MSG_ERROR					 	= 'Error';
var
  	nNroLineaScript, nLineasScript :integer;
    FRespuestaAConsultaRNVMRecord : TRespuestaAConsultaRNVMRecord;
    sParseError : string;
begin
    Screen.Cursor := crHourglass;
    Result := True;
    nLineasScript := FRespuestasConsultasRNVMTXT.Count;
    lblReferencia.Caption := Format( MSG_ANALIZING_RENDERINGS_FILE, [nLineasScript] );
    pbProgreso.Position := 1;
    pbProgreso.Max := FRespuestasConsultasRNVMTXT.Count;
    pnlAvance.Visible := True;

    nNroLineaScript := 1;

    while ( nNroLineaScript <= nLineasScript ) and ( not FDetenerImportacion ) do
    begin

      if not ParseRespuestaConsultaRNVMLine(  FRespuestasConsultasRNVMTXT[nNroLineaScript-1],
                                              FRespuestaAConsultaRNVMRecord,
                                              sParseError ) then begin
    	    // Si encuentra un error termina
          FErrorMsg := Format(  MSG_RENDERINGS_FILE_HAS_ERRORS,
                                [nNroLineaScript] ) + ' ' + sParseError;

          FErrores.Add(FErrorMsg);
        	result := False;
      end;
		  Inc( nNroLineaScript );
      pbProgreso.Position := nNroLineaScript;
      Application.ProcessMessages;
    end;
    FLineas := nNroLineaScript - 1;
    result := Result and (not FDetenerImportacion);
  	Screen.Cursor := crDefault;
end;





function TfRespuestasAConsultasRNVM.RegistrarOperacion : boolean;
resourcestring
  	MSG_ERRORS 							          = 'Errores : ';
  	MSG_COULD_NOT_REGISTER_OPERATION 	= 'No se pudo registrar la operaci�n';
    MSG_ERROR 							          = 'Error';
var
	  sDescrip : string;
    DescError : string;
begin
    sDescrip := iif( FErrores.Count > 0, '', MSG_ERRORS + IntToStr( FErrores.Count ));
  	result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC,
                                                RO_MOD_INTERFAZ_ENTRANTE_RESPUESTAS_CONSULTAS_RNVM,
                                                ExtractFileName(edOrigen.text),
                                                UsuarioSistema,
                                                sDescrip,
                                                True,
                                                False,
                                                NowBase(DMConnections.BaseCAC),
                                                0,
                                                FCodigoOperacion,
                                                FLineas,
                                                0,
                                                DescError);
    if  not result then
        MsgBoxErr(  MSG_COULD_NOT_REGISTER_OPERATION,
                    DescError,
                    MSG_ERROR,
                    MB_ICONERROR);
end;





function TfRespuestasAConsultasRNVM.CargarRespuestasConsultasRNVM : boolean;



    function  ObtenerFechaRecepcion : TDateTime;
    var
        sFecha : string;
    begin
    	  sFecha := Copy(FRespuestasConsultasRNVMTXT[0], 58, 6);
    	  result :=	EncodeDate(	StrToInt( '20' + Copy(sFecha, 1, 2)), StrToInt(Copy(sFecha, 3, 2)), StrToInt(Copy(sFecha, 5, 2)));
    end;

resourcestring
    MSG_PROCESSING_RENDERINGS_FILE			= 'Procesando archivo de Rendiciones - Cantidad de lineas : %d';
    MSG_ERROR_SAVING_RESPONSE           = 'Error al Registrar la Respuesta a Consulta RNVM: RUT:%s PATENTE:%s  ';
  	MSG_ERROR								            = 'Error';
var
  	nNroLineaScript, nLineasScript : integer;
    FRespuestaAConsultaRNVMRecord : TRespuestaAConsultaRNVMRecord;
    sDescripcionError : string;
    sParseError : string;

    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
begin
  	Screen.Cursor := crHourglass;

  	nLineasScript := FRespuestasConsultasRNVMTXT.Count;
    lblReferencia.Caption := Format( MSG_PROCESSING_RENDERINGS_FILE, [nLineasScript] );
    pbProgreso.Position := 0;
    pbProgreso.Max := FRespuestasConsultasRNVMTXT.Count;
    pnlAvance.Visible := True;

    nNroLineaScript := 1;

    while ( nNroLineaScript <= nLineasScript ) and ( not FDetenerImportacion ) and (	FErrorMsg = '' ) do begin

        Reintentos  := 0;

        if ParseRespuestaConsultaRNVMLine( FRespuestasConsultasRNVMTXT[nNroLineaScript-1], FRespuestaAConsultaRNVMRecord, sParseError ) then
        begin
            with FRespuestaAConsultaRNVMRecord, spActualizarRespuestaAConsultaRNVM, Parameters do
            begin
                try
                    Refresh;
                    ParamByName( '@CodigoOperacionInterfase' ).Value 	:= FCodigoOperacion;
                    ParamByName( '@Patente' ).Value 	                := PATENTE;
                    ParamByName( '@DVPatente' ).Value 	              := DV_PATENTE;
                    ParamByName( '@Marca' ).Value 	                  := MARCA;
                    ParamByName( '@Modelo' ).Value 	                  := MODELO;
                    ParamByName( '@TipoVehiculo' ).Value 	            := CATEGORIA;
                    ParamByName( '@FechaCompra' ).Value             	:= FEC_COMPRA;
                    ParamByName( '@Color' ).Value 	                  := COLOR;
                    ParamByName( '@Color2' ).Value 	                  := COLOR2;
                    ParamByName( '@Rut' ).Value 	                    := RUT;
                    ParamByName( '@Nombre' ).Value 	                  := NOMBRE;
                    ParamByName( '@Apellido' ).Value 	                := AP_PATERNO;
                    ParamByName( '@ApellidoMaterno' ).Value 	        := AP_MATERNO;
                    ParamByName( '@AnoVeh' ).Value 	                  := ANO;
                    ParamByName( '@Comuna' ).Value 	                  := COMUNA;
                    ParamByName( '@Region' ).Value 	                  := REGION;
                    ParamByName( '@Calle' ).Value 	                  := CALLE;
                    ParamByName( '@Numero' ).Value 	                  := NUMERO;
                    ParamByName( '@RestoDireccion' ).Value 	          := RESTO_DIRE;
                    ParamByName( '@Usuario' ).Value                   := UsuarioSistema;
                    ParamByName( '@DescripcionError' ).Value          := '';
                    ParamByName( '@NumeroLinea' ).Value               := nNroLineaScript;

                    CommandTimeOut := 500;
                    ExecProc;

                    sDescripcionError := Trim( VarToStr( ParamByName('@DescripcionError' ).Value ));
                    if  ( sDescripcionError <> '' ) then
                        FErrores.Add( StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]));
                except
                    on E : exception do
                    begin
                      FErrorMsg := Format(MSG_ERROR_SAVING_RESPONSE, [FRespuestaAConsultaRNVMRecord.RUT,FRespuestaAConsultaRNVMRecord.PATENTE]);
                      MsgBoxErr(Format(MSG_ERROR_SAVING_RESPONSE, [FRespuestaAConsultaRNVMRecord.RUT,FRespuestaAConsultaRNVMRecord.PATENTE]), e.Message, MSG_ERROR, MB_ICONERROR);
                      Break;
                    end;
                end
            end;
        end
        else
        begin
        end;

        Inc( nNroLineaScript );
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

  	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;





Function TfRespuestasAConsultasRNVM.GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
const
    STR_TITLE = 'Respuestas a Consultas RNVM Recibidas';
var
    FRecepcion : TFRptRecepcionRespuestasConsultasRNVM;
begin
    Result:=false;
    try
        Application.CreateForm(TFRptRecepcionRespuestasConsultasRNVM, FRecepcion);
        if not FRecepcion.Inicializar(STR_TITLE, CodigoOperacionInterfase) then fRecepcion.Release;
        Result:=true;
    except
    end;
end;





procedure TfRespuestasAConsultasRNVM.btnProcesarClick(Sender: TObject);



    Function ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;




    procedure ObtenerResumen (CodigoOperacionInterfase : Integer;Var CantRegs, Aprobados, Rechazados : Integer);
    resourcestring
      MSG_ERROR = 'Error';
      MSG_ERROR_QUERY = 'Error Obteniendo Resumen';
    begin
      try
          With SpObtenerResumenRespuestasRNVMRecibidas, Parameters do begin
              Close;
              Refresh;
              ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
              Parameters.ParamByName('@FechaProcesamiento').Value           := NULL;
              Parameters.ParamByName('@NombreArchivo').Value                := NULL;
              Parameters.ParamByName('@Modulo').Value                       := NULL;
              Parameters.ParamByName('@Usuario').Value                      := NULL;
              Parameters.ParamByName('@CantidadRespuestasRecibidas').Value  := NULL;
              Parameters.ParamByName('@CantidadRespuestasAceptadas').Value  := NULL;
              Parameters.ParamByName('@CantidadRespuestasRechazadas').Value := NULL;
              Parameters.ParamByName('@LineasArchivo').Value                := NULL;
              CommandTimeout := 500;
              Open;
              CantRegs := Parameters.ParamByName('@CantidadRespuestasRecibidas').Value;
              Aprobados := Parameters.ParamByName('@CantidadRespuestasAceptadas').Value;
              Rechazados := Parameters.ParamByName('@CantidadRespuestasRechazadas').Value;
              Close;
          end;
      except
          on E : Exception do begin
              MsgBoxErr(MSG_ERROR_QUERY, E.Message, MSG_ERROR, MB_ICONERROR);
          end;
      end;
    end;



   Function ActualizarLog(CantidadErrores:integer):boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : string;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal( DMConnections.BaseCAC,
                                                                FCodigoOperacion,
                                                                STR_ERRORES+ IntToStr(CantidadErrores),
                                                                DescError);
        except
            on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_SUCCEDED 					          = 'El proceso finaliz� con �xito';
    MSG_PROCESS_FINISH_WITH_ERRORS	        = 'El proceso finaliz� con errores.';
    MSG_PROCESS_COULD_NOT_BE_COMPLETED 		  = 'El proceso no se pudo completar';
    MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE 	= 'No se puede abrir el archivo de rendiciones';
    MSG_ERRROR_RENDERINGS_FILE_IS_EMPTY 	  = 'El archivo seleccionado est� vac�o';
    ConsultaRNVM_RENDERINGS_DESCRIPTION 		= 'Rendicion ConsultaRNVM sobre Estado de Debitos - ';
    MSG_ERROR                               = 'Error';
    CONST_REPORTE_RESPUESTAS_CONSULTAS_RNVM = 'Respuestas Consultas RNVM - Procesar Archivo';
    MSG_PROCESS_COULD_NOT_BE_COMPLETED_ERROR_FILE = 'El proceso no se pudo completar porque el archivo contiene errores de sintaxis. Corrija los errores y vuelva a procesarlo';
Var
    //TotalRegistros: integer;
    CantidadErrores: integer;
    CantRegs, Aprobados, Rechazados : Integer;
begin
 	// Crea las listas
	FRespuestasConsultasRNVMTXT := TStringList.Create;
  FErrores := TStringList.Create;
	FErrorMsg := '';

	// Deshabilita los botones
	btnCancelar.Enabled := True;
	btnProcesar.Enabled := False;
	btnSalir.Enabled := False;
	btnAbrirArchivo.Enabled := False;
	edOrigen.Enabled := False;

	FDetenerImportacion := False;
	try
		try
      //Lee el archivo del Rendicones
			FRespuestasConsultasRNVMTXT.text:= FileToString(edOrigen.text);
			//Verifica si el Archivo Contiene alguna linea
			if ( FRespuestasConsultasRNVMTXT.Count = 0 ) then
				MsgBox(MSG_ERRROR_RENDERINGS_FILE_IS_EMPTY, Caption, MB_ICONERROR)
			else
      begin
        //TotalRegistros := FRespuestasConsultasRNVMTXT.Count - 2;
        if AnalizarRespuestasConsultasRNVMTXT then
        begin
          DMConnections.BaseCAC.Execute('BEGIN TRAN frmRespuestasAConsultasRNVM');
          if RegistrarOperacion and CargarRespuestasConsultasRNVM then
          begin
            DMConnections.BaseCAC.Execute('COMMIT TRAN frmRespuestasAConsultasRNVM');

            ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);

            ActualizarLog(CantidadErrores);

            ObtenerResumen (FCodigoOperacion, CantRegs, Aprobados, Rechazados);

            if MsgProcesoFinalizado(  MSG_PROCESS_SUCCEDED,
                                      MSG_PROCESS_FINISH_WITH_ERRORS,
                                      Caption,
                                      CantRegs,
                                      Aprobados,
                                      Rechazados) then
            begin
              	GenerarReportedeFinalizacion(FCodigoOperacion);

                FErrores.Clear;
            end;
            MoverArchivoProcesado(Caption,edOrigen.Text, FRNVM_DIR_ARCHIVO_RESP_PROCESADOS);
          end else
          begin
              DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION frmRespuestasAConsultasRNVM END');
              MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
          end;
        end
        else
        begin
          MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED_ERROR_FILE, Caption, MB_ICONERROR);
        end;
			end;
		except
			on E : Exception do begin
  				FErrorMsg := MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE;
  				MsgBoxErr(MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE, e.Message,  MSG_ERROR, MB_ICONERROR);
			end;
		end;
	finally
    if (Ferrores.Count > 0) then
        GenerarReporteErroresSintaxis(CONST_REPORTE_RESPUESTAS_CONSULTAS_RNVM,
                                      FErrores,
                                      FRNVM_DIR_ARCHIVO_RESP_ERRORES,
                                      RO_MOD_INTERFAZ_ENTRANTE_RESPUESTAS_CONSULTAS_RNVM);

    FreeAndNil(FRespuestasConsultasRNVMTXT);
    FreeAndNil(FErrores);
    btnCancelar.Enabled := False;
    Close;
	end;
end;





procedure TfRespuestasAConsultasRNVM.btnCancelarClick(Sender: TObject);
resourcestring
  	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;





procedure TfRespuestasAConsultasRNVM.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
  	CanClose := not btnCancelar.Enabled;
end;





procedure TfRespuestasAConsultasRNVM.btnSalirClick(Sender: TObject);
begin
    Close;
end;





procedure TfRespuestasAConsultasRNVM.FormClose(Sender: TObject;var Action: TCloseAction);
begin
   Action := caFree;
end;

end.
