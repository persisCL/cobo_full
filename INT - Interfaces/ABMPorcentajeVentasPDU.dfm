object FAbmPorcentajeVentasPDU: TFAbmPorcentajeVentasPDU
  Left = 516
  Top = 165
  Width = 583
  Height = 401
  Caption = 'Porcentaje por Ventas de PDU'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object AbmList: TAbmList
    Left = 0
    Top = 35
    Width = 575
    Height = 234
    Ctl3D = True
    ParentCtl3D = False
    TabStop = True
    TabOrder = 1
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'128'#0'Fecha                              '
      
        #0'245'#0'Porcentaje                                                 ' +
        '             ')
    RefreshTime = 300
    Table = QryPorcentajeVentasDayPass
    Style = lbOwnerDrawFixed
    OnClick = AbmListClick
    OnDrawItem = AbmListDrawItem
    OnRefresh = abmListRefresh
    OnInsert = AbmListInsert
    OnDelete = AbmListDelete
    OnEdit = AbmListEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 575
    Height = 35
    Habilitados = [btAlta, btBaja, btModi]
  end
  object pnlEditing: TPanel
    Left = 0
    Top = 269
    Width = 575
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 2
    object LPorcentaje: TLabel
      Left = 236
      Top = 23
      Width = 12
      Height = 16
      Caption = '%'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LfechadeInicio: TLabel
      Left = 9
      Top = 8
      Width = 30
      Height = 13
      Caption = 'Fecha'
    end
    object Lbl_porcentaje: TLabel
      Left = 126
      Top = 8
      Width = 51
      Height = 13
      Caption = 'Porcentaje'
    end
    object nePorcentaje: TNumericEdit
      Left = 124
      Top = 20
      Width = 101
      Height = 21
      TabOrder = 0
      Decimals = 2
    end
    object deFechaInicio: TDateEdit
      Left = 8
      Top = 20
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
  end
  object pnlButtons: TPanel
    Left = 0
    Top = 317
    Width = 575
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    DesignSize = (
      575
      50)
    object Notebook: TNotebook
      Left = 365
      Top = 8
      Width = 201
      Height = 38
      Anchors = [akRight, akBottom]
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'Salir'
        object btnSalir: TButton
          Left = 120
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Salir'
          TabOrder = 0
          OnClick = btnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        Caption = 'Editing'
        object btnAceptar: TButton
          Left = 45
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btnAceptarClick
        end
        object btnCancelar: TButton
          Left = 120
          Top = 8
          Width = 75
          Height = 25
          Cancel = True
          Caption = 'Cancelar'
          TabOrder = 1
          OnClick = btnCancelarClick
        end
      end
    end
  end
  object QryPorcentajeVentasDayPass: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT  *'
      'FROM    PorcentajeVentasDayPass WITH (NOLOCK)'
      'WHERE TipoPaseDiario = '#39'N'#39)
    Left = 56
    Top = 152
  end
end
