{------------------------------------------------------------------------------
 File Name: FrmRecepcionRendicionesSantander.pas
 Author:    lgisuk
 Date Created: 12/07/2006
 Language: ES-AR
 Description: M�dulo de la interface Santander - Recepci�n de Rendiciones
 
Revision 1
Author: lgisuk
Date Created: 13/03/07
Description: Ahora al llamar a registrar el pago, informo si se trata de un reintento

Revision : 2
    Author : vpaszkowicz
    Date : 20/03/2008
    Description : Guardo en el log la cantidad de registros del archivo y el
    total recibido en el mismo.

Revision 3
Author: mbecerra
Date: 23-Junio-2009
Description:	Se corrige un bug que hac�a que se procesaran comprobantes
            	err�neos. Es decir, la NK debe ser num�rica entera,
                sin hacer un Trim(xxxx)

                Adicionalmente se quita la regla de que si el parseo
                de una l�nea es incorrecto, detenga toda la rendici�n.

                En el reporte de errores, se agrega el n�mero de la l�nea
                mal parseada.

                Se le quitan los try excesivos a la funci�n ParseLineToRecord
                y se cambian por llamadas a la funcion PermitirSoloDigitos del
                PeaProcs

                Se agrega la llamada a la funci�n PermitirSoloDigitos en vez de
                un try...except..

                Se despliega el reporte del resultado, independiente si fue
                exitoso o no.

                Se modifica la funci�n de Control para que despliegue
                un mensaje m�s apropiado si alg�n monto o la cantidad

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Fecha       : 24-04-2015
Firma       : SS_1147_CQU_20150423
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
              Se corrigen y realizan las modificaciones necesarias para concesionaria Nativa
--------------------------------------------------------------------------------}
unit FrmRecepcionRendicionesSantander;

interface

uses
    //Recepcion de Rendiciones
  	DMConnection,                         //Coneccion a base de datos OP_CAC
    Util,                                 //Stringtofile,padl..
    UtilProc,                             //Mensajes
    ConstParametrosGenerales,             //Obtengo Valores de la Tabla Parametros Generales
    ComunesInterfaces,                    //Procedimientos y Funciones Comunes a todos los formularios
    Peatypes,                             //Constantes
    PeaProcs,                             //NowBase
    FrmRptErroresSintaxis,                //Reporte de Errores de sintaxis
    FrmRptRecepcionComprobantesSantander, //Reporte del proceso
    //General
  	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  	Dialogs, StdCtrls, Buttons, DB, ADODB, ExtCtrls, Grids, DBGrids,
  	DPSControls, ListBoxEx, DBListEx, ComCtrls, StrUtils, UtilRB,
    ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd, ppReport, ppDB,
    ppComm, ppRelatv, ppTxPipe, UtilDB, ppDBPipe, ppModule, raCodMod;

type

  //Estructura donde voy a almacenar cada Rendicion
  TRendicion = record
    //NotaCobro : integer;      // SS_1147_CQU_20150423
    Monto : INT64;
  	Fecha : TDateTime;
    CodigoServicio : Integer;
    MedioPago : Integer;
    NumeroComprobante : Int64;  // SS_1147_CQU_20150423
    TipoComprobante : string;   // SS_1147_CQU_20150423
  end;

  TFRecepcionRendicionesSantander = class(TForm)
    OpenDialog: TOpenDialog;
    pnlOrigen: TPanel;
    btnAbrirArchivo: TSpeedButton;
    txtOrigen: TEdit;
    lblOrigen: TLabel;
    btnProcesar: TButton;
    btnSalir: TButton;
    Bevel: TBevel;
    lblMensaje: TLabel;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    SpObtenerResumenComprobantesRecibidos: TADOStoredProc;
    SpObtenerResumenComprobantesRecibidosMotivo: TStringField;
    SpObtenerResumenComprobantesRecibidosCantidad: TIntegerField;
    SpObtenerResumenComprobantesRecibidosMonto: TStringField;
    SPProcesarRendicionesSantander: TADOStoredProc;
    SPAgregarResumenComprobantesRecibidos: TADOStoredProc;
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtOrigenChange(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
    { Private declarations }
    FCodigoOperacion : Integer;
    FErrorGrave : Boolean;      //Indica que se produjo un error
    FCancelar : Boolean;        //Indica si ha sido cancelado por el usuario
    FTotalRegistros : Integer;
    FCantidadControl, FSumaControl : Real;
    FSantander_Directorio_Rendiciones : AnsiString;
    FSantander_Directorio_Procesados : AnsiString;
    FSantander_Directorio_Errores : AnsiString;
    FMontoArchivo: int64;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    FCodigoNativa : Integer;        													// SS_1147_CQU_20150316
  	Function   Abrir : Boolean;
    Function   Control : Boolean;
    function   ConfirmaCantidades : Boolean;
    function   ParseLineToRecord(Linea : String; var Rendicion : TRendicion; var DescError : String) : Boolean;
    Function   AnalizarRendicionesTXT : Boolean;
    function   RegistrarOperacion : Boolean;
    Function   ActualizarRendiciones : Boolean;
    Function   GenerarReporteErroresSintaxis : Boolean;
    Function   GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
  public
    { Public declarations }
  	Function  Inicializar(txtCaption : ANSIString) : Boolean;
    procedure GrabarEnErroresInterfaces;
  end;

var
  FRecepcionRendicionesSantander : TFRecepcionRendicionesSantander;
  FLista : TStringList;
  FErrores : TStringList;

Const
  RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_SANTANDER = 71;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesSantander.Inicializar(txtCaption : ANSIString) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 12/07/2006
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        SANTANDER_DIRECTORIO_RENDICIONES        = 'Santander_Rendiciones_PEC_POC';
        SANTANDER_DIRECTORIO_ERRORES            = 'Santander_Directorio_Errores';
        SANTANDER_DIRECTORIO_PROCESADOS         = 'STD_DirectorioRendicionesProcesadas';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la Concesionaria Nativa                  // SS_1147_CQU_20150423
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150423

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, SANTANDER_DIRECTORIO_RENDICIONES , FSantander_Directorio_Rendiciones) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SANTANDER_DIRECTORIO_RENDICIONES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FSantander_Directorio_Rendiciones := GoodDir(FSantander_Directorio_Rendiciones);
                if  not DirectoryExists(FSantander_Directorio_Rendiciones) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FSantander_Directorio_Rendiciones;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, SANTANDER_DIRECTORIO_ERRORES , FSantander_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SANTANDER_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FSantander_Directorio_Errores := GoodDir(FSantander_Directorio_Errores);
                if  not DirectoryExists(FSantander_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FSantander_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,SANTANDER_DIRECTORIO_PROCESADOS, FSantander_Directorio_Procesados);

            except
                on E : Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, E.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Centro el formulario
  	CenterForm(Self);
  	try
  		DMConnections.BaseCAC.Connected := True;
  		Result := DMConnections.BaseCAC.Connected and
                           VerificarParametrosGenerales;
  	except
        on E : Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
  	end;
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Caption := AnsiReplaceStr(txtCaption, '&', '');  //titulo
  	btnProcesar.enabled := False; //Procesar
  	lblmensaje.Caption := '';     //limpio el indicador
    PnlAvance.visible := False;    //Oculto el Panel de Avance
    FCodigoOperacion := 0;         //inicializo codigo de operacion
end;

{-----------------------------------------------------------------------------
            	GrabarEnErroresInterfaces
  Author: mbecerra
  Date: 23-Junio-2009
  Description:		Graba los errores en la tabla ErroresInterfases
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesSantander.GrabarEnErroresInterfaces;
var
	i : integer;
begin
    for i := 0 to FErrores.Count - 1 do begin
    	AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, FErrores[i])
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesSantander.ImgAyudaClick(Sender: TObject);
ResourceString
     MSG_RENDICION      = ' ' + CRLF +
                          'El Archivo de Rendiciones' + CRLF +
                          'es enviado por SANTANDER al ESTABLECIMIENTO' + CRLF +
                          'y tiene por objeto dar a conocer al Establecimiento, ' + CRLF +
                          'los comprobantes que fueron cobrados en SANTANDER.' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: RENCNESSAAAAMMDD.TXT' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para registrar los pagos de los' + CRLF +
                          'comprobantes aceptados en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = True then Exit;

    //Muestro el mensaje
    MsgBoxBalloon(MSG_RENDICION, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: permito hacer click en la ayuda solo si no esta procesando
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesSantander.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.Visible = True then ImgAyuda.Cursor := crDefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarArchivoBTNClick
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Busco el archivo a procesar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesSantander.btnAbrirArchivoClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: EsArchivoValido
      Author:    lgisuk
      Date Created: 12/07/2006
      Description: Verifica si es un Archivo Valido para Esta Pantalla
      Parameters:
      Return Value: None
    -----------------------------------------------------------------------------}
    Function EsArchivoValido(Nombre : AnsiString) : Boolean;
    resourcestring                                          // SS_1147_CQU_20150423
        PREFIJO_CN = 'RENCNESS';                            // SS_1147_CQU_20150423
        PREFIJO_VS = 'SNS_CAJA';                            // SS_1147_CQU_20150423
    var                                                     // SS_1147_CQU_20150423
        Prefijo : string;                                   // SS_1147_CQU_20150423
    begin
        if FCodigoNativa = CODIGO_VS                        // SS_1147_CQU_20150423
        then Prefijo := PREFIJO_VS                          // SS_1147_CQU_20150423
        else Prefijo := PREFIJO_CN;                         // SS_1147_CQU_20150423
        //Result:=   ExistePalabra(Nombre, 'RENCNESS') and  // SS_1147_CQU_20150423
        Result:=   ExistePalabra(Nombre, Prefijo) and       // SS_1147_CQU_20150423
                        ExistePalabra(uppercase(Nombre), '.TXT');
    end;

resourcestring
    MSG_ERROR = 'No es un archivo de Rendiciones Valido! ';
begin
    OpenDialog.InitialDir := FSantander_Directorio_Rendiciones;
    if OpenDialog.execute then begin
        if not (EsArchivoValido (OpenDialog.Filename)) then begin
            //inform que no es un archivo valido
            MsgBox(MSG_ERROR , self.Caption, MB_OK + MB_ICONINFORMATION);
            txtOrigen.text := '';
        end
        else begin
            BTNAbrirArchivo.Enabled := False;
            txtOrigen.text := OpenDialog.FileName;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EOrigenChange
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Controlo que abra un archivo valido
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesSantander.txtOrigenChange(Sender: TObject);
begin
    //Controlo que abra un archivo valido
  	btnProcesar.Enabled := FileExists( txtOrigen.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: Abrir
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Abro el Archivo TXT
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesSantander.Abrir : Boolean;
resourcestring
  	MSG_OPEN_FILE_ERROR = 'Error al abrir el archivo';
begin
    Result := False;
    FLista.text := FileToString(txtOrigen.text);
    FTotalRegistros := FLista.Count - 1;
    if FLista.text <> '' then  begin
        Result := True;
        if FSantander_Directorio_Procesados <> '' then begin
            if RightStr(FSantander_Directorio_Procesados,1) = '\' then  FSantander_Directorio_Procesados := FSantander_Directorio_Procesados + ExtractFileName(txtOrigen.text)
            else FSantander_Directorio_Procesados := FSantander_Directorio_Procesados + '\' + ExtractFileName(txtOrigen.text);
        end;
    end else begin
        MsgBox(MSG_OPEN_FILE_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Control
  Author:    lgisuk
  Date Created: 19/07/2006
  Description: verifico la consistencia entre los valores reflejados
               en el Registro del encabezado y los mismos conceptos calculados
               a partir de los registros de detalle.
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesSantander.Control : Boolean;
resourcestring
	{REV.3
    MSG_NOT_EQUAL_COUNT = 'La cantidad reflejada en el registro de encabezado no coincide'+ CRLF + 'con la cantidad calculada!';
    MSG_NOT_EQUAL_SUM   = 'La suma reflejada en el registro de encabezado no coincide con la suma calculada!';
    MSG_CANT_CONTROL    = 'Cantidad Control: ';
    MSG_CANT_CALC       = 'Cantidad Calculada: ';
    MSG_SUM_CONTROL     = 'Suma Control: ';
    MSG_SUM_CALC        = 'Suma Calculada: ';
    MSG_DIF             = 'Diferencia: ';
    MSG_ERROR           = 'No se pudo realizar el control del archivo!';
    }

    //REV.3
    MSG_CAPTION			= 'Validar Encabezado';
    MSG_MONTO			= 'El Monto';
    MSG_CANTIDAD		= 'La Cantidad';
    MSG_SUMA			= 'La Suma';
    MSG_ENCABEZADO		= 'el encabezado';
    MSG_LINEA			= 'la linea %d';
    MSG_NO_NUMERICO		= '%s no es num�rico(a) en ';
    MSG_CANTIDAD_ENC	= '%s informada en el encabezado [%.0n] no coincide con la calculada en el archivo [%.0n]';
    MSG_ERROR			= 'Ocurri� un error al realizar el control del encabezado del archivo';
var
    i : Integer;
    FSumaReal: Real;
    //REV.3
    sAux, Mensaje : string;
begin
    Result := False;
    try
        //Obtengo la cantidad y la suma del archivo de control
        {REV.3
        FCantidadControl := StrToFloat(Trim(Copy(Flista.Strings[0], 9, 7)));
        FSumaControl := StrToFloat(Trim(Copy(Flista.Strings[0], 17, 17)));
        }

        Mensaje := '';
        sAux := Copy(FLista[0],9,7);
        if PermitirSoloDigitos(sAux) then FCantidadControl := StrToFloat(sAux)
        else Mensaje := Format(MSG_NO_NUMERICO + MSG_ENCABEZADO, [MSG_CANTIDAD]);

        if Mensaje = '' then begin
        	sAux := Copy(FLista[0], 16, 17);
            if PermitirSoloDigitos(sAux) then FSumaControl := StrToFloat(sAux)
            else Mensaje := Format(MSG_NO_NUMERICO + MSG_ENCABEZADO, [MSG_SUMA]);
        end;

        //comparo la cantidad contra la calculada
        If (Mensaje = '') and (FCantidadControl <> FTotalRegistros) then begin
            {REV.3
            MsgBoxErr(MSG_NOT_EQUAL_COUNT + CRLF + MSG_CANT_CONTROL  + FloatToSTR(FCantidadControl) + CRLF + MSG_CANT_CALC  + FloatToSTR(FTotalRegistros) + CRLF + MSG_DIF + FloatToSTR(FCantidadControl - FTotalRegistros) ,'', self.text, 0);
            exit;   }
            Mensaje := Format(MSG_CANTIDAD_ENC, [MSG_CANTIDAD, FCantidadControl, 1.0 * FTotalRegistros]);
        end;

        //sumo los registros del archivo
        //REV.3   Se detiene por cada l�nea que est� mala.. (rqui�ones...)
        i := 1;
        FSumaReal := 0;
      	while (Mensaje = '') and (i < FLista.Count) do begin
        	//sAux := Copy(FLista[i], 10, 10);      // SS_1147_CQU_20150423
            if FCodigoNativa = CODIGO_VS            // SS_1147_CQU_20150423
            then sAux := Copy(FLista[i], 13, 8)     // SS_1147_CQU_20150423
            else sAux := Copy(FLista[i], 10, 10);   // SS_1147_CQU_20150423
            if PermitirSoloDigitos(sAux) then FSumaReal := FSumaReal + StrToFloat(sAux)
            else Mensaje := Format(MSG_NO_NUMERICO + MSG_LINEA, [MSG_MONTO, i]);

        	Inc(i);
        end;

        //comparo la suma contra la calculada
        if (Mensaje = '') and (FSumacontrol <> FSumaReal) then begin
            {REV.3
            MsgBoxErr(MSG_NOT_EQUAL_SUM  + CRLF + MSG_SUM_CONTROL  + FloatToSTR(FSumaControl) + CRLF + MSG_SUM_CALC  + FloatToSTR(FSumaReal) + CRLF + MSG_DIF + FloatToSTR(FSumaControl - FSumaReal), '', self.text, 0);
            exit;}
            Mensaje := Format(MSG_CANTIDAD_ENC, [MSG_SUMA, FSumaControl, FSumaReal]);
        end;

        //REV.3
        if Mensaje <> '' then MsgBox(Mensaje, MSG_CAPTION, MB_ICONERROR);
        Result := (Mensaje = '');

    except on E : Exception do begin
    		MsgBoxErr(MSG_ERROR, E.Message, MSG_CAPTION, MB_ICONERROR);
    	end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ConfirmaCantidades
  Author:    lgisuk
  Date Created: 12/07/2006
  Description:  Consulta al usuario si quiere continuar o no,
                presentandole un resumen de las cantidades a procesar
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesSantander.ConfirmaCantidades: Boolean;
ResourceString
    MSG_TITLE        = 'Recepci�n de Rendiciones';
    MSG_CONFIRMATION = 'El Archivo seleccionado contiene %d rendiciones a procesar.'+crlf+crlf+
                       'Desea continuar con el procesamiento del archivo?';
var
    Mensaje : String;
begin
    //Armo el mensaje
    Mensaje := Format(MSG_CONFIRMATION,[FTotalRegistros]);
    //lo muestro
    Result := ( MsgBox(Mensaje,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
end;

{-----------------------------------------------------------------------------
  Function Name: ParseLineToRecord
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Arma un registro de Rendicion en base a la linea leida del TXT
  Parameters:
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  02/07/2007
  Cambie Monto STRtoINT a STRtoINT64
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesSantander.ParseLineToRecord(Linea : String; var Rendicion : TRendicion; var DescError : String) : Boolean;
Resourcestring
    MSG_ERROR                   = 'Error al leer el registro';
Const
    STR_VOUCHER                 = 'Comprobante: ';
    STR_DIVIDER                 = ' - ';
    STR_INVOICE_NUMBER_ERROR    = 'El n�mero de la nota de cobro es inv�lido.';
    STR_AMMOUNT_ERROR           = 'El monto es inv�lido.';
  	STR_DATE_ERROR              = 'La fecha es inv�lida.';
    STR_PAYMENT_METHOD_ERROR    = 'El tipo de pago es inv�lido.';
    STR_INVALID_SERVICE         = 'El canal de pago es incorrecto';
    STR_INVOICE_TYPE_ERROR      = 'El tipo de comprobante es inv�lido o no se indic�';    // SS_1147_CQU_20150423
Var
    Ano, Mes, Dia : String;
    sMonto : String;
    sMedioPago : String;
    sCodigoServicio : String;
    NumeroComprobante : String;
    sTipoComprobanteFiscal, TipoComprobante : string;       // SS_1147_CQU_20150423
begin
    DescError := '';

    try
        if FCodigoNativa = CODIGO_VS then begin             // SS_1147_CQU_20150423
            NumeroComprobante       := Copy(Linea, 1, 10);  // SS_1147_CQU_20150423
            sTipoComprobanteFiscal  := Copy(Linea, 11, 2);  // SS_1147_CQU_20150423
            case StrToInt(sTipoComprobanteFiscal) of        // SS_1147_CQU_20150423
                33 : TipoComprobante := TC_FACTURA_AFECTA;  // SS_1147_CQU_20150423
                34 : TipoComprobante := TC_FACTURA_EXENTA;  // SS_1147_CQU_20150423
                39 : TipoComprobante := TC_BOLETA_AFECTA;   // SS_1147_CQU_20150423
                41 : TipoComprobante := TC_BOLETA_EXENTA;   // SS_1147_CQU_20150423
            end;                                            // SS_1147_CQU_20150423
            sMonto				    := Copy(Linea, 13, 8);  // SS_1147_CQU_20150423
            Ano					    := Copy(Linea, 21, 4);  // SS_1147_CQU_20150423
            Mes					    := Copy(Linea, 25, 2);  // SS_1147_CQU_20150423
            Dia					    := Copy(Linea, 27, 2);  // SS_1147_CQU_20150423
            sMedioPago			    := '0';                 // SS_1147_CQU_20150423
            sCodigoServicio		    := Copy(Linea, 29, 29); // SS_1147_CQU_20150423
        end else begin                                      // SS_1147_CQU_20150423
            // (Aumenta tabulado)                           // SS_1147_CQU_20150423
            NumeroComprobante	:= Copy(Linea, 1, 9 );		//REV.3 Trim( Copy(Linea, 1, 9 ));
            sMonto				:= Copy(Linea, 10, 10);		//REV.3 Trim(Copy(Linea, 10, 10));
            Ano					:= Copy(Linea, 20, 4);
            Mes					:= Copy(Linea, 24, 2);
            Dia					:= Copy(Linea, 26, 2);
            sMedioPago			:= Copy(Linea, 28, 1);      //REV.3 Copy(Linea, 29, 1);
            sCodigoServicio		:= Copy(Linea, 29, 2);      //REV.3 Copy(Linea, 30, 1);
            TipoComprobante     := TC_NOTA_COBRO;           // SS_1147_CQU_20150423
        end;                                                // SS_1147_CQU_20150423

        if DescError = '' then begin
        	//if PermitirSoloDigitos(NumeroComprobante) then Rendicion.NotaCobro := StrToInt(NumeroComprobante)         // SS_1147_CQU_20150423
        	if PermitirSoloDigitos(NumeroComprobante) then Rendicion.NumeroComprobante := StrToInt(NumeroComprobante)   // SS_1147_CQU_20150423
            else DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_INVOICE_NUMBER_ERROR;
        end;

        if DescError = '' then begin
            if PermitirSoloDigitos(sMonto) then Rendicion.Monto := StrToInt64(sMonto)
            else DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_AMMOUNT_ERROR;
        end;

        if DescError = '' then begin
            if PermitirSoloDigitos(Ano) and PermitirSoloDigitos(Mes) and PermitirSoloDigitos(Dia) then Rendicion.Fecha := EncodeDate(StrToInt(Ano), StrToInt(Mes), StrToInt(Dia))
            else DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER +  STR_DATE_ERROR;
        end;

        if DescError = '' then begin
            if PermitirSoloDigitos(sMedioPago) then begin
            	Rendicion.MedioPago := StrToInt(sMedioPago );
                if Rendicion.MedioPago <> 0 then begin
                	DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_PAYMENT_METHOD_ERROR;
                end;
            end
            else DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_PAYMENT_METHOD_ERROR;
        end;

        if DescError = '' then begin
            if PermitirSoloDigitos(sCodigoServicio) then Rendicion.CodigoServicio := StrToInt(sCodigoServicio )
            else DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_INVALID_SERVICE;
        end;

        if DescError = '' then begin                                                                // SS_1147_CQU_20150423
            if TipoComprobante <> '' then Rendicion.TipoComprobante := TipoComprobante              // SS_1147_CQU_20150423
            else DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER                         // SS_1147_CQU_20150423
                              + Format(STR_INVOICE_TYPE_ERROR, [TipoComprobante]);                  // SS_1147_CQU_20150423
        end;                                                                                        // SS_1147_CQU_20150423

        Result := (DescError = '');
        
//-------------------------Comentado en REV.3
        //Obtengo los Datos de la Rendici�n
//        with Rendicion do begin

            //Parseo la linea

            //Verifico que el numero de comprobante sea numerico
{            try
                NotaCobro := StrToInt(NumeroComprobante);
            except
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_INVOICE_NUMBER_ERROR;
                Exit;
            end;
}
            //Verifico que el Monto sea numerico
{            try
                Monto := StrToInt64(sMonto);
            except
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_AMMOUNT_ERROR;
                Exit;
            end;
}
            //Verifico que sea una fecha valida
{            try
                Fecha := EncodeDate(StrToInt(Ano), StrToInt(Mes), StrToInt(Dia));
            except
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER +  STR_DATE_ERROR;
                Exit;
            end;
}
            //verifico que el valor de medio de pago sea numerico
{            try
                MedioPago := StrToInt(sMedioPago );
            except
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_PAYMENT_METHOD_ERROR;
                Exit;
            end;
}
            //Verifico que el medio de pago sea efectivo
{            if (MedioPago <> 0)  then begin
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_PAYMENT_METHOD_ERROR;
                Exit;
            end;
}
            //canal de pago
{            try
                CodigoServicio := StrToInt(sCodigoServicio );
            except
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_INVALID_SERVICE;
                Exit;
            end;

        end;

        Result := True;
}
    except on E: Exception do begin
            //MsgBoxErr(MSG_ERROR, E.Message, Self.Caption, MB_ICONERROR);                          // SS_1147_CQU_20150423
            //FErrorGrave:=true;                                                                    // SS_1147_CQU_20150423
            FErrores.Add(MSG_ERROR + DescError + e.Message);                                        // SS_1147_CQU_20150423
            Result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AnalizarRencionesTXT
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Antes de hacer nada Parsea todo el Archivo para verificar
  				que el parseo sea v�lido
  Parameters: None
  Return Value: Boolean
  Revision : 1
      Author : vpaszkowicz
      Date : 20/03/2008
      Description : Sumo el total recibido.
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesSantander.AnalizarRendicionesTXT : Boolean;
Resourcestring
    MSG_ERROR_SYNTAXIS = 'El Archivo contiene errores de Sintaxis';
var
    I : Integer;
    Rendicion : TRendicion;
    DescError : String;
begin
    FCancelar := False;                //Permito que la importacion sea cancelada
    PbProgreso.Position := 0;          //Inicio la Barra de progreso
    PbProgreso.Max := FLista.Count;  //Establezco como maximo la cantidad de registros del memo
    FErrores.Clear;
    FMontoArchivo := 0;
    I := 1;
    //recorro las lineas del archivo
    while I < FLista.Count  do begin

        //si cancelan la operacion
        if FCancelar then begin
            Result := False;             //El analisis fue cancelado por el usuario
            pbProgreso.Position := 0;  //Si la importacion fue detenida, Vuelvo a Comenzar
            Exit;                      //salgo de la rutina
        end;

        //Arma un registro de Rendicion en base a la linea leida del TXT
        //verifica que los valores recibidos sean validos sino devuelve una
        //Descripci�n del error.
        if not ParseLineToRecord(Flista.Strings[i], Rendicion, DescError) then begin
            //lo inserto en el string list de errores
            FErrores.Add(DescError);
        end
        else FMontoArchivo := FMontoArchivo + Rendicion.Monto;

        pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso
        Application.ProcessMessages;                    //Refresco la pantalla

        I := I + 1;
    end;

    Result:= (FErrores.count = 0);

    if (Result = False) then MsgBox(MSG_ERROR_SYNTAXIS, Self.Caption, MB_OK + MB_ICONINFORMATION);

end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: boolean
  Revision :1
      Author : vpaszkowicz
      Date : 20/03/2008
      Description : Agrego el total del archivo y la cantidad de lineas.
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesSantander.RegistrarOperacion : Boolean;
resourcestring
	  MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Procesar Archivo de Rendiciones';
var
    NombreArchivo: String;
  	DescError    : String;
begin
   NombreArchivo := Trim(ExtractFileName(TxtOrigen.text));
   Result := RegistrarOperacionEnLogInterface (DMConnections.BaseCAC , RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_SANTANDER , NombreArchivo, UsuarioSistema , STR_OBSERVACION , False , False, NowBase(DMConnections.BaseCAC) , 0 , FCodigoOperacion , FTotalRegistros, FMontoArchivo, DescError);
   if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarRenciones
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Proceso el archivo de rendiciones
  Parameters: 
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesSantander.ActualizarRendiciones : Boolean;

      {-----------------------------------------------------------------------------
        Function Name: ActualizarRendicion
        Author:    lgisuk
        Date Created: 12/07/2006
        Description: Registro el pago con la informacion recibida en el archivo
        Parameters:
        Return Value: boolean
      -----------------------------------------------------------------------------
        Revision 1
        lgisuk
        13/03/07
        Ahora al llamar a registrar el pago, informo si se trata de un reintento
      -----------------------------------------------------------------------------}
      Function ActualizarRendicion (Rendicion : TRendicion; var DescError : String) : Boolean;
      resourcestring
          MSG_ERROR_SAVING_INVOICE = 'Error al Asentar la Operaci�n Comprobante: %d  ';
          MSG_ERROR = 'Error al actualizar la Rendicion';
      Const
          STR_COMPROBANTE  = 'Comprobante: ';
          STR_SEPARADOR = ' - ';
      var
          Error : String;
          //
          Reintentos : Integer;
          mensajeDeadlock : AnsiString;
      begin
          Result := False;
          DescError := '';
          Error := '';

          //Intentamos registrar el pago de un comprobante.
          Reintentos  := 0;

          while Reintentos < 3 do begin

              try
                    with SPProcesarRendicionesSantander.Parameters, Rendicion do begin
                        Refresh;
                        ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                        //ParamByName('@NotaCobro').Value := NotaCobro;                 // SS_1147_CQU_20150423
                        ParamByName('@NumeroComprobante').Value := NumeroComprobante;   // SS_1147_CQU_20150423
                        ParamByName('@TipoComprobante').Value := TipoComprobante;       // SS_1147_CQU_20150423
                        ParamByName('@Monto').Value := Monto;
                        ParamByName('@Fecha').Value := Fecha;
                        ParamByName('@MedioPago').Value := MedioPago;
                        ParamByName('@CodigoServicio').Value := CodigoServicio;
                        ParamByName('@Usuario').Value	:= UsuarioSistema;
                        ParamByName('@Reintento').Value	:= Reintentos;
                        ParamByName('@DescripcionError').Value := '';
                    end;
                    with SPProcesarRendicionesSantander do begin
                        CommandTimeout := 500;
                        ExecProc;
                        Error := Trim(Copy(VarToStr(Parameters.ParamByName('@DescripcionError').Value),1,100));
                        if Error <> '' then begin
                            //DescError := STR_COMPROBANTE + IntToStr(Rendicion.NotaCobro) + STR_SEPARADOR + Error;         // SS_1147_CQU_20150423
                            FErrores.Add(STR_COMPROBANTE + IntToStr(Rendicion.NumeroComprobante) + STR_SEPARADOR + Error);  // SS_1147_CQU_20150423
                            //exit;                                                                                         // SS_1147_CQU_20150423
                        end;
                    end;
                    Result := True;
                    //1/4 de segundo entre registracion de pago y otro
                    Sleep(25);
                    //Salgo del ciclo por Ok
                    Break;
              except
                  on  E : Exception do begin
                      if (pos('deadlock', e.Message) > 0) then begin
                          mensajeDeadlock := e.message;
                          inc(Reintentos);
                          sleep(2000);
                      end else begin
                          FErrorGrave := True;
                          //MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [Rendicion.NotaCobro]), E.Message, Self.caption, MB_ICONERROR);                      // SS_1147_CQU_20150423
                          MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [Rendicion.NumeroComprobante]), E.Message, Self.caption, MB_ICONERROR);                // SS_1147_CQU_20150423
                          AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_ERROR_SAVING_INVOICE, [Rendicion.NumeroComprobante]));  // SS_1147_CQU_20150423
                          //Break;                                                                                                                          // SS_1147_CQU_20150423
                      end;
                  end;
              end;

          end; 

          //una vez que se reintento 3 veces x deadlock lo informo
          if Reintentos = 3 then begin
              FErrorGrave := True;
              //MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [Rendicion.NotaCobro]), MensajeDeadLock, MSG_ERROR, MB_ICONERROR);       // SS_1147_CQU_20150423
              MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [Rendicion.NumeroComprobante]), MensajeDeadLock, MSG_ERROR, MB_ICONERROR); // SS_1147_CQU_20150423
              AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_ERROR_SAVING_INVOICE, [MensajeDeadlock]));  // SS_1147_CQU_20150423
          end;

      end;

var
    i: Integer;
    Rendicion : TRendicion;
    DescError: String;
begin
    Result := False;
    FCancelar := False;					//Permito que la importacion sea cancelada
    pbProgreso.Position := 0;			//Inicio la Barra de progreso
    pbProgreso.Max := FLista.Count;		//Establezco como maximo la cantidad de registros del memo
    I := 1;
    while I < FLista.Count  do begin

        //si cancelan la operacion
        if FCancelar then begin
             pbProgreso.Position := 0;
             Exit;
        end;

        //Obtengo el registro con los datos de la rendicion del archivo
        //REV.3 ParseLineToRecord(Flista.Strings[i], Rendicion, DescError);
        if ParseLineToRecord(Flista.Strings[i], Rendicion, DescError) then begin
        	//Registro el pago con la informaci�n de la rendicion
        	ActualizarRendicion (Rendicion, DescError);
        end
        else
        	FErrores.Add('[' + IntToStr(i) + '] ' + DescError);

        pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso
        Application.ProcessMessages;                    //Refresco la pantalla
        i:= i + 1;
    end;

    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteErroresSintaxis
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Genero el reporte de errores de sintaxis
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesSantander.GenerarReporteErroresSintaxis : Boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de errores de Sintaxis';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Santander - Procesar Archivo de Rendiciones';
var
    F : TFRptErroresSintaxis;
begin
    Result := False;
    try
        //muestro el reporte
        Application.CreateForm(TFRptErroresSintaxis, F);
        if not F.Inicializar( IntToStr(RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_SANTANDER) , REPORT_TITLE , FSantander_Directorio_Errores ,FErrores) then f.Release;
        Result := True;
    except
       on E : Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Genero el Reporte de Finalizacion de Proceso sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesSantander.GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
Const
    STR_TITLE = 'Comprobantes Recibidos';
var
    FRecepcion : TFrmRptRecepcionComprobantesSantander;
begin
    Result := False;
    try
        //muestro el reporte
        Application.CreateForm(TFrmRptRecepcionComprobantesSantander,FRecepcion);
        if not fRecepcion.Inicializar( STR_TITLE, CodigoOperacionInterfase) then fRecepcion.Release;
        Result := True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Procesa el archivo de Rendiciones
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesSantander.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 12/07/2006
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion : Integer ; var Cantidad : Integer) : Boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+IntToStr(CodigoOperacion)+')');
            Result := True;
        except
           on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

     {-----------------------------------------------------------------------------
      Function Name:  ObtenerResumen
      Author:    lgisuk
      Date Created: 12/07/2006
      Description:  Obtengo el resumen del proceso realizado
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    procedure ObtenerResumen (CodigoOperacionInterfase : Integer;Var CantRegs, Aprobados, Rechazados : Integer);
    resourcestring
      MSG_ERROR = 'Error';
      MSG_ERROR_QUERY = 'Error Obteniendo Resumen';
    begin
      try
          With spObtenerResumenComprobantesRecibidos, Parameters do begin
              Close;
              ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
              CommandTimeout := 500;
              Open;
              CantRegs := Parameters.ParamByName('@CantidadComprobantesRecibidos').Value;
              Aprobados := Parameters.ParamByName('@CantidadComprobantesAceptados').Value;
              Rechazados := Parameters.ParamByName('@CantidadComprobantesRechazados').Value;
              Close;
          end;
      except
          on E : Exception do begin
              MsgBoxErr(MSG_ERROR_QUERY, E.Message, MSG_ERROR, MB_ICONERROR);
          end;
      end;
    end;

     {-----------------------------------------------------------------------------
      Function Name:  ObtenerResumen
      Author:    lgisuk
      Date Created: 18/07/2006
      Description:  Registro el resumen de comprobantes recibidos
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    procedure RegistrarResumen (CodigoOperacionInterfase : Integer; Cantidad, Monto : Real);
    resourcestring
      MSG_ERROR = 'Error';
      MSG_ERROR_INSERT = 'Error guardando Resumen';
    begin
      try
          With spAgregarResumenComprobantesRecibidos, Parameters do begin
              Close;
              ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
              ParamByName('@Cantidad').Value := Cantidad;
              ParamByName('@Monto').Value := Monto;
              CommandTimeout := 500;
              ExecProc;
          end;
      except
          on E : Exception do begin
              MsgBoxErr(MSG_ERROR_INSERT, E.Message, MSG_ERROR, MB_ICONERROR);
          end;
      end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 12/07/2006
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog (CantidadErrores : Integer) : Boolean;
   resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : String;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_CANCEL			= 'Proceso cancelado por el usuario';
    MSG_PROCESS_NOT_COMPLETE	= 'El proceso no se pudo completar';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
  	MSG_PROCESS_FINALLY_OK		= 'Proceso finalizado con �xito';
    MSG_FINALIZADO				= 'Proceso finalizado:';		//REV.3
    MSG_RESUMEN					= 	'Total Registros: %.0n' + CRLF +
                                    'L�neas con Errores: %.0n' + CRLF +
                                    'Pagos Recibidos: %.0n' + CRLF +
                                    'Pagos Aceptados: %.0n' + CRLF +
                                    'Pagos Rechazados: %.0n';
Const
    STR_OPEN_FILE          = 'Abrir Archivo...';
    STR_CHECK_FILE         = 'Control del Archivo...';
    STR_CONFIRM            = 'Confirmar Cantidades...';
    STR_ANALYZING          = 'Analizando Rendiciones...';
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
    STR_PROCESS            = 'Registrando los Pagos...';
var
    CantRegs, Aprobados, Rechazados : integer;
    CantidadErrores : Integer;
begin
    BtnProcesar.Enabled := False;
    PnlAvance.Visible := True;       //El Panel de Avance es visible durante el procesamiento
    KeyPreview := True;
  	try
        //Inicio la operacion
        FErrores.Clear;                     // SS_1147_CQU_20150423
        //Abro el archivo
    		Lblmensaje.Caption := STR_OPEN_FILE;
    		if not Abrir then begin
            FErrorGrave := True;
            Exit;
        end;

        //Controlo el archivo
        Lblmensaje.Caption := STR_CHECK_FILE;
        if not Control then begin
            FErrorGrave := True;
            Exit;
        end;

        //El usuario acepta o no continuar con el proceso
        Lblmensaje.Caption := STR_CONFIRM;
        if not ConfirmaCantidades then begin
            FErrorGrave := True;
            Exit;
        end;

        {Comentado en REV.3
        //Analizo si hay errores de parseo o sintaxis en el archivo
        Lblmensaje.Caption := STR_ANALYZING;
        if not AnalizarRendicionesTxt then begin
            FErrorGrave := True;
            Exit;
        end;              }

        //Registro la operaci�n en el Log
        Lblmensaje.Caption := STR_REGISTER_OPERATION;
        if not RegistrarOperacion then begin
            FErrorGrave := True;
            Exit;
        end;

        //Proceso el archivo de Rendiciones
    		Lblmensaje.Caption := STR_PROCESS;
    		if not ActualizarRendiciones then begin
            FErrorGrave := True;
            Exit;
        end;

        //Registro el resumen de lo que recibi
        RegistrarResumen (FCodigoOperacion, FCantidadControl, FSumaControl);

        //Obtengo la cantidad de errores
        //REV.3 ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);
        CantidadErrores := FErrores.Count;

        //REV.3
        GrabarEnErroresInterfaces();

        //Actualizo el log al Final
        ActualizarLog(CantidadErrores);

  	finally
        pbProgreso.Position := 0;  //Inicio la Barra de progreso
        PnlAvance.visible := False;  //Oculto el Panel de Avance
    		Lblmensaje.Caption := '';
        if FCancelar then begin
            //Si fue cancelado por el usuario muestro mensaje que el proceso fue cancelado
            MsgBox(MSG_PROCESS_CANCEL, self.Caption, MB_OK + MB_ICONINFORMATION);
        end else if FErrorGrave then begin
            //Si se produjo un error grave muestro un cartel indicando que hubo errores
            MsgBox(MSG_PROCESS_NOT_COMPLETE, self.Caption, MB_OK + MB_ICONINFORMATION);
            //Genero el Reporte de Error con el Report Builder
            if (Ferrores.Count > 0) then GenerarReporteErroresSintaxis;
        end else begin

            //Obtengo Resumen del Proceso
            ObtenerResumen (FCodigoOperacion, CantRegs, Aprobados, Rechazados);

            //Muestro Mensaje de Finalizaci�n
            {REV.3 if MsgProcesoFinalizado(MSG_PROCESS_FINALLY_OK , MSG_PROCESS_FINALLY_WITH_ERROR , Caption , CantRegs , Aprobados , Rechazados) then begin}
            //REV.3 MsgProcesoFinalizado(MSG_PROCESS_FINALLY_OK , MSG_PROCESS_FINALLY_WITH_ERROR , Caption , CantRegs , Aprobados , Rechazados);

            MsgBox(MSG_FINALIZADO + CRLF + Format(MSG_RESUMEN, [	1.0 * FTotalRegistros,
            														1.0 * CantidadErrores,
                                                                    1.0 * CantRegs,
                                                                    1.0 * Aprobados,
                                                                    1.0 * Rechazados]), Caption, MB_ICONINFORMATION);
                //Genero el reporte de finalizacion de proceso
                GenerarReportedeFinalizacion(FCodigoOperacion);
            {end;}

        end;
        KeyPreview := False;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: permito cancelar la operacion
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesSantander.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesSantander.btnSalirClick(Sender: TObject);
begin
  	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: impido salir si esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesSantander.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando
    CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesSantander.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

initialization
    FLista   := TStringList.Create;
    FErrores := TStringList.Create;
finalization
    FreeAndNil(Flista);
    FreeAndNil(FErrores);
end.


