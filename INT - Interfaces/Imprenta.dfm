object frmImprenta: TfrmImprenta
  Left = 137
  Top = 142
  Caption = 'Proceso masivo de impresi'#243'n'
  ClientHeight = 394
  ClientWidth = 712
  Color = clBtnFace
  Constraints.MinHeight = 421
  Constraints.MinWidth = 720
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  DesignSize = (
    712
    394)
  PixelsPerInch = 96
  TextHeight = 13
  object imgTimbre: TImage
    Left = 208
    Top = 357
    Width = 105
    Height = 105
    Stretch = True
  end
  object lblProcesoInterrumpido: TLabel
    Left = 48
    Top = 352
    Width = 272
    Height = 13
    Caption = 'PROCESO INTERRUMPIDO, REVISAR DATOS.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object nb: TNotebook
    Left = 0
    Top = 0
    Width = 712
    Height = 351
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    object TPage
      Left = 0
      Top = 0
      Caption = 'Paso1'
      DesignSize = (
        712
        351)
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 423
        Height = 13
        Caption = 
          'Paso 1 - Seleccionar los comprobantes existentes para enviar a i' +
          'mpresi'#243'n '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 19
        Top = 56
        Width = 244
        Height = 13
        Caption = 'Presione "Todos los Comprobantes" para comenzar'
        Visible = False
      end
      object lblCantidad: TLabel
        Left = 16
        Top = 271
        Width = 500
        Height = 30
        Anchors = [akLeft, akBottom]
        AutoSize = False
        Caption = 'Se han encontrado'
        Constraints.MaxHeight = 30
        Constraints.MaxWidth = 500
        Constraints.MinHeight = 30
        Constraints.MinWidth = 500
        WordWrap = True
      end
      object Label4: TLabel
        Left = 522
        Top = 270
        Width = 169
        Height = 13
        Anchors = [akRight, akBottom]
        Caption = 'Presione "Siguiente" para continuar'
      end
      object Label3: TLabel
        Left = 24
        Top = 315
        Width = 331
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 
          'Limitar el Proceso de Impresi'#243'n a los Comprobantes desde el N'#250'me' +
          'ro  '
      end
      object Label5: TLabel
        Left = 440
        Top = 315
        Width = 77
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'hasta el N'#250'mero'
      end
      object Bevel1: TBevel
        Left = 8
        Top = 11
        Width = 696
        Height = 335
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
      object DBListEx1: TDBListEx
        Left = 17
        Top = 72
        Width = 676
        Height = 195
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 60
            Header.Caption = 'N'#250'mero'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'NumeroComprobante'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Fecha Emision'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaEmision'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Per'#237'odo Inicial'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'PeriodoInicial'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Per'#237'odo Final'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'PeriodoFinal'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Vencimiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaVencimiento'
          end>
        DataSource = DS
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
      end
      object btnSiguiente: TButton
        Left = 618
        Top = 310
        Width = 78
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Siguiente'
        Enabled = False
        TabOrder = 0
        OnClick = btnSiguienteClick
      end
      object btnObtenerComprobantes: TButton
        Left = 560
        Top = 42
        Width = 134
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Comprobantes Pendientes'
        TabOrder = 2
        OnClick = btnObtenerComprobantesClick
      end
      object neComprobanteInicial: TNumericEdit
        Left = 360
        Top = 311
        Width = 73
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 12
        TabOrder = 3
      end
      object neComprobanteFinal: TNumericEdit
        Left = 525
        Top = 311
        Width = 73
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 12
        TabOrder = 4
      end
      object btnBuscarProceso: TButton
        Left = 427
        Top = 42
        Width = 134
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Procesos de Impresi'#243'n'
        TabOrder = 5
        OnClick = btnBuscarProcesoClick
      end
      object btnProcesosFacturacion: TButton
        Left = 293
        Top = 42
        Width = 134
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Procesos de Facturaci'#243'n'
        TabOrder = 6
        OnClick = btnProcesosFacturacionClick
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Paso2'
      DesignSize = (
        712
        351)
      object Bevel2: TBevel
        Left = 8
        Top = 8
        Width = 697
        Height = 335
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
      object Label6: TLabel
        Left = 24
        Top = 24
        Width = 322
        Height = 13
        Caption = 'Paso 2 - Creaci'#243'n del archivo de interface para imprenta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 23
        Top = 54
        Width = 397
        Height = 13
        Caption = 
          'Seleccione la ubicaci'#243'n en d'#243'nde desea crear el archivo de inter' +
          'face'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 24
        Top = 104
        Width = 395
        Height = 13
        Caption = 
          'Si desea incluir una imagen del cat'#225'logo de la imprenta seleccio' +
          'nela aqu'#237' (opcional)'
      end
      object lblForzandoReimpresion: TLabel
        Left = 24
        Top = 310
        Width = 112
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'lblForzandoReimpresion'
      end
      object lblRangoSeleccionado: TLabel
        Left = 24
        Top = 296
        Width = 107
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'lblRangoSeleccionado'
      end
      object lblImprimiendoElectronicos: TLabel
        Left = 24
        Top = 326
        Width = 124
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'lblImprimiendoElectronicos'
      end
      object btnAnterior: TButton
        Left = 544
        Top = 310
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Anterior'
        TabOrder = 0
        OnClick = btnAnteriorClick
      end
      object btnFinalizar: TButton
        Left = 624
        Top = 310
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Finalizar'
        Enabled = False
        TabOrder = 1
        OnClick = btnFinalizarClick
      end
      object pnlprogreso: TPanel
        Left = 152
        Top = 158
        Width = 379
        Height = 118
        Anchors = [akLeft, akRight, akBottom]
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        Visible = False
        DesignSize = (
          379
          118)
        object Label8: TLabel
          Left = 16
          Top = 19
          Width = 80
          Height = 13
          Anchors = [akLeft, akRight, akBottom]
          Caption = 'Progreso general'
        end
        object lblDescri: TLabel
          Left = 160
          Top = 0
          Width = 3
          Height = 13
        end
        object labelProgreso: TLabel
          Left = 16
          Top = 87
          Width = 341
          Height = 13
          Alignment = taCenter
          Anchors = [akLeft, akRight, akBottom]
          AutoSize = False
          Caption = 'labelProgreso'
        end
        object Label77: TLabel
          Left = 16
          Top = 64
          Width = 345
          Height = 13
          Alignment = taCenter
          Anchors = [akLeft, akRight, akBottom]
          AutoSize = False
          Caption = 'Presione Escape para cancelar '
        end
        object pbProgreso: TProgressBar
          Left = 16
          Top = 42
          Width = 341
          Height = 17
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 0
        end
      end
      object bteImagen: TBuscaTabEdit
        Left = 24
        Top = 120
        Width = 433
        Height = 21
        Enabled = True
        ReadOnly = True
        TabOrder = 3
        EditorStyle = bteTextEdit
        BuscaTabla = btCatalogo
      end
      object txtUbicacion: TPickEdit
        Left = 24
        Top = 72
        Width = 433
        Height = 21
        Enabled = True
        ReadOnly = True
        TabOrder = 4
        EditorStyle = bteTextEdit
        OnButtonClick = txtUbicacionButtonClick
      end
      object dbgDatosJORDAN: TDBGrid
        Left = 608
        Top = 24
        Width = 82
        Height = 60
        DataSource = dsObtenerDatosJORDAN
        Options = [dgTitles, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 5
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Visible = False
      end
    end
  end
  object btnSalir: TButton
    Left = 629
    Top = 363
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 0
    OnClick = btnSalirClick
  end
  object chbLogSucesos: TCheckBox
    Left = 387
    Top = 288
    Width = 113
    Height = 17
    Caption = 'Log del Proceso'
    TabOrder = 2
  end
  object DS: TDataSource
    DataSet = spObtenerComprobantesImprentaPreview
    Left = 8
    Top = 357
  end
  object btCatalogo: TBuscaTabla
    Caption = 'Seleccionar Im'#225'gen'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = tblCatalogoImagenesImprenta
    OnProcess = btCatalogoProcess
    OnSelect = btCatalogoSelect
    Left = 39
    Top = 357
  end
  object tblCatalogoImagenesImprenta: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'CatalogoImagenesImprenta'
    Left = 71
    Top = 357
  end
  object spObtenerComprobantesImprentaPreview: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerComprobantesImprentaPreview'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ForzarReimpresion'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ImprimirElectronicos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 104
    Top = 357
  end
  object spActualizarComprobantesEnviadosAImprenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ActualizarComprobantesEnviadosAImprenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraImpreso'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 138
    Top = 358
  end
  object Update: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 172
    Top = 357
  end
  object spPrepararProcesoInterfazImprenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 3000
    ProcedureName = 'PrepararProcesoInterfazImprenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ComprobanteInicial'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@ComprobanteFinal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@ForzarReimpresion'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoOperacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImprimirElectronicos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 328
    Top = 358
  end
  object spObtenerLineasInterfazImprenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 3000
    ProcedureName = 'ObtenerLineasInterfazImprenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Img'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@FechaProceso'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@SoloNK'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@TotalComprobantesFAC'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@TotalComprobantesBOL'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 360
    Top = 358
  end
  object qryEliminarTemporales: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'DROP TABLE ##ComprobantesAImprimir'
      'DROP TABLE ##Lineas')
    Left = 296
    Top = 358
  end
  object qryLineas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    CursorType = ctStatic
    LockType = ltReadOnly
    CommandTimeout = 60
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      '##Lineas.TipoComprobante,'
      '##Lineas.NumeroComprobante,'
      '##Lineas.Linea, '
      '##Lineas.TipoComprobanteFiscal, '
      '##Lineas.NumeroComprobanteFiscal, '
      '##Lineas.GrupoImprenta, '
      '##Lineas.FechaEmision, '
      '##Lineas.ImporteTotal, '
      '##Lineas.PrimeraLineaDetalle, '
      '##Lineas.RutReceptor, '
      '##Lineas.RazonSocialReceptor, '
      '##Lineas.FechaHoraTimbre, '
      '##Lineas.CodigoConcepto'
      'FROM  '
      '##Lineas  WITH (NOLOCK)  '
      'ORDER BY '
      '##Lineas.Comuna, '
      '##Lineas.TipoComprobante, '
      '##Lineas.NumeroComprobante, '
      '##Lineas.Patente, '
      '##Lineas.GrupoImprenta,'
      '##Lineas.CodigoConcepto')
    Left = 392
    Top = 358
    object qryLineasTipoComprobante: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object qryLineasNumeroComprobante: TLargeintField
      FieldName = 'NumeroComprobante'
    end
    object qryLineasLinea: TStringField
      FieldName = 'Linea'
      Size = 255
    end
    object qryLineasTipoComprobanteFiscal: TStringField
      FieldName = 'TipoComprobanteFiscal'
      FixedChar = True
      Size = 2
    end
    object qryLineasNumeroComprobanteFiscal: TLargeintField
      FieldName = 'NumeroComprobanteFiscal'
    end
    object qryLineasGrupoImprenta: TIntegerField
      FieldName = 'GrupoImprenta'
    end
    object qryLineasFechaEmision: TDateTimeField
      FieldName = 'FechaEmision'
    end
    object qryLineasImporteTotal: TLargeintField
      FieldName = 'ImporteTotal'
    end
    object qryLineasPrimeraLineaDetalle: TStringField
      FieldName = 'PrimeraLineaDetalle'
      Size = 100
    end
    object qryLineasRutReceptor: TStringField
      FieldName = 'RutReceptor'
      FixedChar = True
      Size = 11
    end
    object qryLineasRazonSocialReceptor: TStringField
      FieldName = 'RazonSocialReceptor'
      Size = 100
    end
    object qryLineasFechaHoraTimbre: TDateTimeField
      FieldName = 'FechaHoraTimbre'
    end
    object qryLineasCodigoConcepto: TIntegerField
      FieldName = 'CodigoConcepto'
      ReadOnly = True
    end
  end
  object spObtenerOperacionesImpresionMasiva: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOperacionesImpresionMasiva;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 202
    Top = 357
    object spObtenerOperacionesImpresionMasivaCodigoOperacionInterfase: TIntegerField
      FieldName = 'CodigoOperacionInterfase'
    end
    object spObtenerOperacionesImpresionMasivaFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object spObtenerOperacionesImpresionMasivaUsuario: TStringField
      FieldName = 'Usuario'
      Size = 50
    end
    object spObtenerOperacionesImpresionMasivaCantidad: TIntegerField
      FieldName = 'Cantidad'
      ReadOnly = True
    end
  end
  object buscaOperaciones: TBuscaTabla
    Caption = 'Procesos de Impresi'#243'n Masiva'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = spObtenerOperacionesImpresionMasiva
    OnProcess = buscaOperacionesProcess
    OnSelect = buscaOperacionesSelect
    Left = 234
    Top = 357
  end
  object spActualizarFechaImpresionComprobantesEnviadosAImprenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarFechaImpresionComprobantesEnviadosAImprenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraImpreso'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 265
    Top = 357
  end
  object ActionList1: TActionList
    Left = 600
    Top = 360
    object AConfig_EGATE_HOME_Manual: TAction
      Caption = 'AConfig_EGATE_HOME_Manual'
      OnExecute = AConfig_EGATE_HOME_ManualExecute
    end
    object AConfig_EGATE_HOME_Parametrico: TAction
      Caption = 'AConfig_EGATE_HOME_Parametrico'
      OnExecute = AConfig_EGATE_HOME_ParametricoExecute
    end
    object ACargarDLL: TAction
      Caption = 'ACargarDLL'
      OnExecute = ACargarDLLExecute
    end
    object ATimbrarDinamico: TAction
      Caption = 'ATimbrarDinamico'
    end
    object ATimbraryObtenerImagen: TAction
      Caption = 'ATimbraryObtenerImagen'
    end
    object ALiberarDLL: TAction
      Caption = 'ALiberarDLL'
      OnExecute = ALiberarDLLExecute
    end
  end
  object spObtenerDatosParaTimbreElectronico: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 3000
    ProcedureName = 'ObtenerDatosComprobanteParaTimbreElectronico'
    Parameters = <>
    Left = 440
    Top = 360
  end
  object dsObtenerDatosJORDAN: TDataSource
    DataSet = ObtenerDatosJORDAN
    Left = 656
    Top = 88
  end
  object ObtenerDatosJORDAN: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltReadOnly
    CommandTimeout = 6000
    ProcedureName = 'ObtenerDatosJORDAN'
    Parameters = <>
    Left = 608
    Top = 88
  end
end
