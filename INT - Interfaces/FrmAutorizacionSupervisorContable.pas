{-------------------------------------------------------------------------------
 File Name: FrmAutorizacionSupervisorContable.pas
 Author:    lgisuk
 Date Created: 27/07/2005
 Language: ES-AR
 Description: Módulo de la interface Contable - Autorización de Supervisor Contable
-------------------------------------------------------------------------------}
unit FrmAutorizacionSupervisorContable;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TFAutorizacionSupervisorContable = class(TForm)
    btnAceptar: TButton;
    btnCancelar: TButton;
    Bevel1: TBevel;
    txtUserName: TEdit;
    txtClave: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Image1: TImage;
    procedure FormShow(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    FUserName,
    FPassWord: String;
    function GetUserName: String;
    procedure SetUserName(const Value: String);
    function GetPassword: String;
    procedure SetPassword(const Value: String);
  public
    { Public declarations }
  published
    property UserName: String read GetUserName write SetUserName;
    property Password: String read GetPassword write SetPassword;
  end;

var
  FAutorizacionSupervisorContable: TFAutorizacionSupervisorContable;

implementation

{$R *.dfm}

function TFAutorizacionSupervisorContable.GetPassword: String;
begin
    Result := FPassword;
end;

function TFAutorizacionSupervisorContable.GetUserName: String;
begin
    Result := FUserName;
end;

procedure TFAutorizacionSupervisorContable.SetPassword(const Value: String);
begin
    if Value = FPassword then Exit;
    FPassword := Value;
end;

procedure TFAutorizacionSupervisorContable.SetUserName(const Value: String);
begin
    if Value = FUserName then Exit;
    FUserName := Value;
end;

procedure TFAutorizacionSupervisorContable.FormShow(Sender: TObject);
begin
    ActiveControl := txtUserName;
end;

procedure TFAutorizacionSupervisorContable.btnAceptarClick(Sender: TObject);
begin
    UserName := trim(txtUserName.Text);
    Password := trim(txtClave.Text);
end;

procedure TFAutorizacionSupervisorContable.btnCancelarClick(Sender: TObject);
begin
    close;
end;

end.
