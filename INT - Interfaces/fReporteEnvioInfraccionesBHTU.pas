
{********************************** Unit Header ********************************
File Name : fReporteEnvioInfraccionesBHTU.pas
Author : ndonadio
Date Created: 27/09/2005
Language : ES-AR
Description : Reporte para la generacion del archivo de infracciones para el
                clearing de BHTU...
*******************************************************************************}
unit fReporteEnvioInfraccionesBHTU;

interface

uses
    // Db
    DMConnection,
    // comunes
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppProd, ppClass, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, DB, ADODB, ppCtrls,
  ppPrnabl, ppBands, ppCache;

type
  TfrmReporteEnvioInfraccionesBHTU = class(TForm)
    spObtenerDatosReporteEnvioInfraccionesBHTU: TADOStoredProc;
    dsReporte: TDataSource;
    ppDBReporte: TppDBPipeline;
    ppReportEnvioInfraccionesBHTU: TppReport;
    RBI: TRBInterface;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppGroupOperacion: TppGroup;
    ppGroupHeaderBand: TppGroupHeaderBand;
    ppGroupFooterBand: TppGroupFooterBand;
    ppLabel: TppLabel;
    ppLabel1: TppLabel;
    lbl_usuario: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppLine1: TppLine;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppDBText: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppShape1: TppShape;
    ppShape2: TppShape;
    ppLabel11: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppDBCalc: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    ppLabel12: TppLabel;
    ppSummaryBand: TppSummaryBand;
    procedure RBIExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FcodigoInicial,
    FCodigoFinal    : integer;
    FError          : AnsiString;
  public
    { Public declarations }
    function MostrarReporte( OperacionInicial, OperacionFinal: Integer; Titulo: AnsiString; var Error: AnsiString): Boolean;
  end;

var
  frmReporteEnvioInfraccionesBHTU: TfrmReporteEnvioInfraccionesBHTU;

implementation

{$R *.dfm}

{ TfrmReporteEnvioInfraccionesBHTU }

{******************************** Function Header ******************************
Function Name: MostrarReporte
Author : ndonadio
Date Created : 23/09/2005
Description : Inicializa y lanza el reporte...
Parameters : OperacionInicial, OperacionFinal: Integer; var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmReporteEnvioInfraccionesBHTU.MostrarReporte(OperacionInicial, OperacionFinal: Integer;
  Titulo: AnsiString; var Error: AnsiString): Boolean;
begin
    Result := False;
    FCodigoInicial := OperacionInicial;
    FCodigoFinal   := OperacionFinal;
    try
        RBI.Caption := 'Reporte de ' + Titulo;
        if not RBI.Execute(True) then begin
            Error := FError;
            Exit;
        end;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
            Exit;
        end;
    end;
end;

procedure TfrmReporteEnvioInfraccionesBHTU.RBIExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_NOTHING_TO_REPORT   = 'No hay datos para mostrar.';
begin
    try
        with spObtenerDatosReporteEnvioInfraccionesBHTU do begin
            Parameters.ParamByName('@PrimeraOperacion').Value := FCodigoInicial;
            Parameters.ParamByName('@UltimaOperacion').Value := FCodigoFinal;
            Open;
        end;
        if  spObtenerDatosReporteEnvioInfraccionesBHTU.IsEmpty then begin
            FError := MSG_NOTHING_TO_REPORT;
            Exit;
        end;
    except
        on e:exception do begin
            FError := e.Message;
        end;
    end;
end;

end.
