{-----------------------------------------------------------------------------
 File Name: ComunesInterfases.pas
 Author:    lgisuk
 Date Created: 25/11/2004
 Language: ES-AR
 Description:  Procedimientos y Funciones Comunes a todos los formularios
-----------------------------------------------------------------------------}
unit ComunesInterfases;

interface

uses //Comunes Interfases
     DMConnection,
     UtilProc,
     UtilDB,
     Util,
     ConstParametrosGenerales,
     //General
     Classes, ADODB, SysUtils, VariantComboBox, Windows, Variants;
     //Author: lgisuk
     Function  RegistrarOperacionEnLogInterfase(Conn: TAdoConnection;CodigoModulo:Integer;Fecha:tdatetime;NombreArchivo,Usuario,Observaciones:string;recibido, Reprocesamiento:boolean;FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer;var CodigoOperacionInterfase:integer;Var DescError:Ansistring):boolean; overload;
     Function  ActualizarObservacionesEnLogInterfase(Base:TADOConnection;SP:tadostoredproc;CodigoOperacionInterfase:Integer;Observaciones:string;Agrega:boolean):boolean;
     Function  ExistePalabra(frase,buscar:string):boolean;
     //Author: flamas
     Function  CargarFechasDeInterfasesEjecutadas( sp : TADOStoredProc; CodigoModulo:Integer; Combo : TVariantComboBox ) : boolean;
     function  ValidarNombreArchivoPrestoLider( sFileName, sTipoArchivo, sExtension : string ) : boolean;
     function  ValidarFechaAAAAMMDD( sFecha : string ) : boolean;
     function  VerificarArchivoProcesado( Base:TADOConnection; sFileName : string ) : boolean;
     function  ValidarHeaderFooterPerstoLider( var sArchivo : TStringList; sNombreArchivo, sEPS, sGlosa : string ) : boolean;
     //Author: mtraversa
     function  RegistrarComprobanteEnviadoEnLog(Conn: TAdoConnection; TipoComprobante: ShortString;NumeroComprobante: LongInt; CodigoOperacionInterfase: Integer; var DescError: AnsiString): Boolean;


implementation

resourcestring
	MSG_ERROR = 'Error';

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacionEnLogInterfase
  Author:    flamas
  Date Created: 12/01/2005
  Description: Registra la Operacion en el Log :
  				Agrega el Ultimo registro procesado en la interfaz y
                tambi�n si la interfaz fue reprocesada
  Parameters: Conn: TAdoConnection;CodigoModulo:Integer;Fecha:tdatetime;NombreArchivo,Usuario,Observaciones:string;recibido, Reprocesamiento:boolean;FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer;var CodigoOperacionInterfase:integer;Var DescError:Ansistring
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  RegistrarOperacionEnLogInterfase(Conn: TAdoConnection;CodigoModulo:Integer;Fecha:tdatetime;NombreArchivo,Usuario,Observaciones:string;recibido, Reprocesamiento:boolean;FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer;var CodigoOperacionInterfase:integer;Var DescError:Ansistring):boolean; overload;
var sp:TADOStoredProc;
begin
	CodigoOperacionInterfase:=0;
    result:=false;
    sp:=TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'AgregarLogOperacionesInterfases';
            with SP.Parameters do begin
                Refresh;
                ParamByName('@CodigoModulo').Value:= CodigoModulo;
                ParamByName('@Fecha').Value:= Fecha;
                ParamByName('@NombreArchivo').Value:= NombreArchivo;
                ParamByName('@Usuario').Value:= Usuario;
                ParamByName('@Observaciones').Value:= Observaciones;
                ParamByName('@Reprocesamiento').Value:= Reprocesamiento;
                ParamByName('@FechaHoraRecepcion').Value:= FechaHoraRecepcion;
                ParamByName('@UltimoRegistroEnviado').Value:= Iif( UltimoRegistroEnviado = 0, null, UltimoRegistroEnviado );
                ParamByName('@CodigoOperacionInterfase').Value := 0;
            end;
            SP.ExecProc;                                                                             //Ejecuto el procedimiento
            CodigoOperacionInterfase:= SP.Parameters.ParamByName('@CodigoOperacionInterfase').Value; //Obtengo el Id
            result:=true;
		except
            on e: Exception do begin
              DescError:=e.Message;
            end;
        end;
    finally
    	SP.Free;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarObservacionesEnLogInterfase
  Author:    lgisuk
  Date Created: 13/12/2004
  Description: Permite Actualizar la Observacion de una operacion del log
  Parameters: SP:tadostoredproc;CodigoOperacionInterfase:Integer;Observaciones:string
  Return Value: Integer
-----------------------------------------------------------------------------}
Function ActualizarObservacionesEnLogInterfase(Base:TADOConnection;SP:tadostoredproc;CodigoOperacionInterfase:Integer;Observaciones:string;Agrega:boolean):boolean;
var sqlquery,value:string;
begin
    result:=false;
    sqlquery:='SELECT Observaciones FROM LogOperacionesInterfases WITH (NOLOCK) WHERE CodigoOperacionInterfase = '+inttostr(CodigoOperacionInterfase);
 	value := QueryGetValue(Base, SqlQuery);
    if Agrega = true then begin
     value := value + Observaciones;
    end
	else begin
     value := observaciones;
    end;
    with SP.Parameters do begin
        ParamByName('@CodigoOperacionInterfase').Value:= CodigoOperacionInterfase;
        ParamByName('@Observaciones').Value:= Value;
    end;
    try
        SP.ExecProc;
        result:=true;                                                                             //Ejecuto el procedimiento
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR, e.message, 'Error Al', 0);                                        //el stored procedure podria fallar
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ExistePalabra
  Author:    lgisuk
  Date Created: 07/12/2004
  Description: verifica si existe una palabra dentro de una frase
  Parameters: frase,buscar:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function ExistePalabra(frase,buscar:string):boolean;
var posi:integer;
	bandera:boolean;
begin
     posi:=pos(buscar,frase);//esta funcion delphi hace lo mismo que la substring que cree
     if posi = 0 then begin
        bandera:=false;
     end
     else begin
        bandera:=true;
     end;
     result:=bandera;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarFechasDeInterfasesEjecutadas
  Author:    flamas
  Date Created: 09/12/2004
  Description: Carga un VariantComoBox con las Fechas en que se ejecutaron las interfases
  				de un m�dulo determinado y sus correspondientes c�digos de operaci�n
                sirve para reprocesar la interfase de una fecha determinada
  Parameters: sp : TADOStoreProcedure; CodigoModulo:Integer; Combo : TVariantComboBox
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  CargarFechasDeInterfasesEjecutadas( sp : TADOStoredProc; CodigoModulo:Integer; Combo : TVariantComboBox ) : boolean;
resourcestring
	MSG_COULD_NOT_LOAD_INTERFASES = 'No se pudieron cargar las interfases anteriores';
begin
	result := False;
    try
    	with SP do begin
    		Parameters.ParamByName( '@CodigoModulo' ).Value := CodigoModulo;
    		try
    			Open;
        		Combo.Clear;
        		while not Eof do begin
                	Combo.Items.Add( FormatDateTime( 'dd-mm-yyyy', FieldByName( 'FECHA' ).AsDateTime),
                					FieldByName( 'CODIGOOPERACIONINTERFASE' ).AsInteger );
        			Next;
        		end;
        		result := True;
    		except
    			on e: Exception do
        			MsgBoxErr(MSG_COULD_NOT_LOAD_INTERFASES, e.Message, MSG_ERROR, MB_ICONERROR);
    		end;
    	end;
    finally
    	SP.Close;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarNombreArchivo
  Author:    flamas
  Date Created: 23/12/2004
  Description: Valida el nombre del archivo
  Parameters: sFileName : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function ValidarNombreArchivoPrestoLider( sFileName, sTipoArchivo, sExtension : string ) : boolean;
resourcestring
	MSG_INVALIDA_FILE_NAME = 'El nombre del archivo %s es inv�lido';
Var
    CodigodeComercio:AnsiString;
begin
	if ( Pos( '.', sExtension ) = 0 ) then sExtension := '.' + sExtension;

    //Obtengo el codigo de comercio
    ObtenerParametroGeneral(DMConnections.BaseCAC,'Presto_CodigodeComercio',CodigodeComercio);
    CodigodeComercio := Copy(CodigodeComercio, 4, 3);
    
    result := ( Copy( ExtractFileName( sFileName ), 1, 7 ) = CodigodeComercio + sTipoArchivo ) and
				ValidarFechaAAAAMMDD( Copy( ExtractFileName( sFileName ), 8, 8 )) and
				( ExtractFileExt(sFileName) = sExtension );

	if not result then MsgBox (Format (MSG_INVALIDA_FILE_NAME, [ExtractFileName (sFileName)]), MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarFecha
  Author:    flamas
  Date Created: 23/12/2004
  Description: Valida una fecha dada en formato AAAMMDD
  Parameters: sFecha : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function ValidarFechaAAAAMMDD( sFecha : string ) : boolean;
begin
	try
    		StrToDate( Copy( sFecha, 7, 2 ) + DateSeparator +
            			Copy( sFecha, 5, 2 ) + DateSeparator +
            			Copy( sFecha, 1, 4 ) );
        	result := true;
    except
    	on exception do
        	result := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: VerificarArchivoProcesado
  Author:    flamas
  Date Created: 23/12/2004
  Description: Verifica si el Archivo ha sido procesado en una interfase
  Parameters: sFileName : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function VerificarArchivoProcesado ( Base:TADOConnection; sFileName : string ) : boolean;
resourcestring
	MSG_FILE_ALREADY_PROCESSED = 'El archivo %s ya fue procesado';
begin
	Result := ( QueryGetValue(Base,
				'SELECT dbo.VerificarArchivoInterfaseProcesado(''' +
				ExtractFileName( sFileName ) + ''')') = 'True' );
end;


{-----------------------------------------------------------------------------
  Function Name: ValidarHeaderFooterPerstoLider
  Author:    flamas
  Date Created: 23/12/2004
  Description: Valida el encabezado, pie de p�gina y cantidad de registros del Archivo
  Parameters: var sArchivo : TStringList
  Return Value: boolean
-----------------------------------------------------------------------------}
function  ValidarHeaderFooterPerstoLider( var sArchivo : TStringList; sNombreArchivo, sEPS, sGlosa : string ) : boolean;
resourcestring
	MSG_INVALID_HEADER 			= 'El encabezado del archivo es inv�lido';
	MSG_INVALID_DESCRIPTION 	= 'La glosa es incorrecta';
	MSG_INVALID_COMPANY 		= 'El c�digo de empresa prestadora de servicios es inv�lido';
	MSG_INVALID_FOOTER 			= 'El pie de p�gina del archivo es inv�lido';
	MSG_INVALID_REGISTER_COUNT 	= 'La cantidad de registros del archivo'#10#13 +
    							  	'no coincide con la cantidad reportada';
var
	nLineas : integer;
    nRegs	: integer;
begin
	nLineas := sArchivo.Count - 1;

    result := False;

	if ( Pos( 'HEADER', sArchivo[0] ) <> 1 ) then begin
    	MsgBox( MSG_INVALID_HEADER, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

    if ( Pos( sEPS , sArchivo[0] ) <> 8 ) then begin
    	MsgBox( MSG_INVALID_COMPANY, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

	if  (Pos( Trim( sGlosa ) + ' ' + Copy( ExtractFileName( sNombreArchivo ), 10, 6 ), sArchivo[0]) <> 15)  then begin
    	MsgBox( MSG_INVALID_DESCRIPTION, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

    if ( Pos( 'FOOTER', sArchivo[nLineas] ) <> 1 ) then begin
    	MsgBox( MSG_INVALID_FOOTER, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

	try
    	nRegs := StrToInt( trim( Copy( sArchivo[nLineas], 8, 6 )));
    except
    	on exception do begin
    		MsgBox( MSG_INVALID_FOOTER, MSG_ERROR, MB_ICONERROR );
    		Exit;
		end;
    end;

    if ( nRegs <> nLineas - 1 ) then begin
    	MsgBox( MSG_INVALID_REGISTER_COUNT, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

    result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarComprobanteEnviadoEnLog
  Author:    mtraversa
  Date Created: 28/12/2004
  Description: Crea un nuevo registro en la tabla DetalleComprobantesEnviados
  Parsmeters: Conn: TAdoConnection; TipoComprobante: ShortString; NumeroComprobante: LongInt; CodigoOperacionInterfase: Integer
  Return Value:None
-----------------------------------------------------------------------------}
function RegistrarComprobanteEnviadoEnLog(Conn: TAdoConnection; TipoComprobante: ShortString;
    NumeroComprobante: LongInt; CodigoOperacionInterfase: Integer; var DescError: AnsiString): Boolean;
begin
    Result := False;
    try
        with TADOStoredProc.Create(Nil) do
            try
                Connection := Conn;
                ProcedureName := 'AgregarDetalleComprobantesEnviados';
                with Parameters do begin
                    Refresh;
                    ParamByName('@TipoComprobante').Value          := TipoComprobante;
                    ParamByName('@NumeroComprobante').Value        := NumeroComprobante;
                    ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                end;
                ExecProc;
                Result := True;
            finally
                Free;
            end;
    except
        on E: Exception do
			DescError := E.Message;
    end;
end;


end.
