
{********************************** Unit Header ********************************
File Name : fGenerarArchivoAsignacionesBHTU.pas
Author : ndonadio
Date Created: 12/10/2005
Language : ES-AR
Description :

Autor       :   Claudio Quezada Ib��ez
Firma       :   SS_1147_CQU_20140408
Fecha       :   23-04-2014
Descripcion :   Se agrega variable y llamada a funci�n para obtener la Concesionaria Nativa


Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto


*******************************************************************************}
unit fGenerarArchivoAsignacionesBHTU;

interface

  uses
  // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils,
   // Parametros GEnerales
    ConstParametrosGenerales,
   // DB
    UtilDB, DMConnection,
   // Reporte
    fReporteGeneracionArchivoAsignacionBHTU,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB, ppDBPipe, ppPrnabl, ppClass,
  ppCtrls, ppBands, ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB;

type
  TfrmGenerarArchivoAsignacionesBHTU = class(TForm)
    Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    btnProcesar: TButton;
    btnSalir: TButton;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    txtDestinoArchivo: TPickEdit;
    spObtenerLineas: TADOStoredProc;
    spMarcarAsignaciones: TADOStoredProc;
    Label2: TLabel;
    Label1: TLabel;
    spLimpiarTemporalesEnvioAsignacionesBHTU: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ImgAyudaClick(Sender: TObject);
    procedure txtDestinoArchivoButtonClick(Sender: TObject);
  private
    { Private declarations }
    FProcesando,
    FCancelar   : boolean;
    FArchivo,
    FDestino    : AnsiString;
    FCodigoConcesionariaNativa : Integer;                                           // SS_1147_CQU_20140408
    function CrearNombresArchivos(Directorio: AnsiString): boolean;
    procedure BorrarArchivo;
    procedure MostrarReporte(CodigoOperacion: Integer);
    function VerificarDestino: boolean;
    procedure LimpiarTablas;
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): Boolean;
  end;

var
  frmGenerarArchivoAsignacionesBHTU: TfrmGenerarArchivoAsignacionesBHTU;

implementation

{$R *.dfm}

procedure TfrmGenerarArchivoAsignacionesBHTU.txtDestinoArchivoButtonClick(Sender: TObject);
resourcestring
    MSG_SELECT_LOCATION = 'Seleccione ubicaci�n donde se generar�n los archvos.';
var
    Location: String;
begin
    btnProcesar.Enabled := False;
    // abre el form de seleccion de directorio
    Location := Trim(BrowseForFolder(MSG_SELECT_LOCATION));
    // valida el directio elegido
    if Location = '' then Location := GoodDir(TRIM(txtDestinoArchivo.Text));
    txtDestinoArchivo.Text := GoodDir(Location);
   // lo carga en las variables, edits, etc.
    btnProcesar.Enabled := VerificarDestino;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : ndonadio
Date Created : 06/10/2005
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGenerarArchivoAsignacionesBHTU.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_GENERAR_ARCHIVO  = ' ' + CRLF +
      'Genera el Archivo de las Asignaciones del Ultimo Proceso '+ CRLF +
      ' '+ CRLF +
      'Nombre del Archivo: %s'+ CRLF +
      ' ';
begin
    //si esta procesando sale
    if FProcesando then exit;
    //Muestro el mensaje
    MsgBoxBalloon(Format(MSG_GENERAR_ARCHIVO, [FArchivo]), Caption, MB_ICONQUESTION, IMGAYUDA);
end;


{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : ndonadio
Date Created : 06/10/2005
Description :
Parameters : Sender: TObject; Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TfrmGenerarArchivoAsignacionesBHTU.ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
    ImgAyuda.Cursor := crHandPoint;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 06/10/2005
Description :     Inicializa el form
Parameters : Titulo: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmGenerarArchivoAsignacionesBHTU.Inicializar(Titulo: AnsiString): Boolean;
resourcestring
    ERROR_CANT_INIT = 'No se puede inicializar el formulario';
    DIR_DESTINO_ASIGN_CLEARING_BHTU = 'DIR_DESTINO_ASIGN_CLEARING_BHTU';
    ERROR_CANT_RETRIEVE_PARAMETER = 'Error al obtener el Par�metro General " %s "';
    ERROR_CANT_CREATE_FILENAME = 'No es posible crear el nombre de archivo en el directorio: ' + CRLF + '%s';
begin
    Result := False;
    CenterForm(Self);
    caption := Titulo;
    // esto hay que cambiarlo por lo del p.gral.
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DESTINO_ASIGN_CLEARING_BHTU, FDestino) then begin
        MsgBoxErr(ERROR_CANT_INIT, Format(ERROR_CANT_RETRIEVE_PARAMETER, [DIR_DESTINO_ASIGN_CLEARING_BHTU]) , Caption, MB_ICONERROR);
        Exit;
    end;
    if (not CrearNombresArchivos(FDestino)) then begin
        MsgBoxErr(ERROR_CANT_INIT, Format(ERROR_CANT_CREATE_FILENAME, [FDestino]) , Caption, MB_ICONERROR);
        Exit;
    end;
    FCodigoConcesionariaNativa := ObtenerCodigoConcesionariaNativa;                 // SS_1147_CQU_20140408
    txtDestinoArchivo.Text := GoodDir(FDestino);
    btnProcesar.Enabled := True;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: CrearNombresArchivos
Author : ndonadio
Date Created : 06/10/2005
Description : Crea el nombre de archivo
Parameters : Directorio: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmGenerarArchivoAsignacionesBHTU.CrearNombresArchivos(Directorio: AnsiString): boolean;
const
    FILE_PREFIX = 'anulaciones';
    FILE_SEP    = '_';
    FILE_EXT    = '.dat' ;
    FILE_DATEFORMAT = 'yyyymmdd' ;
var
    FechaBase: TDateTime;
    FullName: ansiString;
begin
    result := False;
    try
        FechaBase := NowBase(DMConnections.BaseCAC);
        FArchivo := '';
        //FArchivo := FILE_PREFIX + FILE_SEP + TRIM(IntToStr(CODIGO_CN)) + FILE_SEP;                // SS_1147_CQU_20140408
        FArchivo := FILE_PREFIX + FILE_SEP + TRIM(IntToStr(FCodigoConcesionariaNativa)) + FILE_SEP; // SS_1147_CQU_20140408
        FArchivo := FArchivo + FormatDateTime(FILE_DATEFORMAT, FechaBase) + FILE_EXT;

        FullName := GoodDir(Directorio) + GoodFileName(FArchivo, FILE_EXT);
        //txtDestinoArchivo.Text := FullName;
        lblNombreArchivo.Caption := FArchivo;
        Result := True;
    except
        on exception do exit;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author : ndonadio
Date Created : 06/10/2005
Description : Proceso principal
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGenerarArchivoAsignacionesBHTU.btnProcesarClick(Sender: TObject);
resourcestring
    NOTHING_TO_SAVE     = 'No hay nada para procesar';
    ERROR_PROCESO       = 'El proceso ha fallado';
    ERROR_LOG           = 'Error al registrar la operaci�n';
    ERROR_LOG_END       = 'Error al Registrar el Fin de la Operaci�n';
    PROCESS_OK          = 'El proceso finaliz� con �xito';
    MSG_QTY_TAN         = 'Cantidad de Patentes en el Archivo: %d';
    CAP_CANCEL          = 'Cancelar';
    CAP_CLOSE           = 'Salir';
    PROCESS_CANCELLED   = 'Proceso cancelado por el usuario';
    ERROR_INVALID_PATH  = 'El Directorio de destino es inv�lido.';
    ASK_FOR_OVERWRITE   = 'El Archivo "%s" ya existe en la carpeta %s. '+ CRLF + 'Desea sobreescribirlo?';
var
    Lista: TStringList;
    CodigoOperacion: Integer;
    descError: AnsiString;
    qtyPatente: Integer;
begin
    if not ValidateControls( [txtDestinoArchivo], [VerificarDestino], Caption, [ERROR_INVALID_PATH]) then Exit;
    if FileExists(GoodDir(txtDestinoArchivo.text)+FArchivo) then begin

        if MsgBox(Format(ASK_FOR_OVERWRITE, [FArchivo, GoodDir(txtDestinoArchivo.text)]),
          Caption, MB_ICONQUESTION + MB_YESNO) = mrNo then Exit;
    end;
    FCancelar := False;
    pbProgreso.Min := 0;
    Lista := TStringLIst.Create;
    btnProcesar.Enabled := False;
    btnSalir.Caption := CAP_CANCEL;
    FProcesando := True;
    pnlAvance.Visible := True;
    try
        try
            spObtenerLineas.Open;
            pbProgreso.Max := spObtenerLineas.RecordCount + 5;
            while (not spObtenerLineas.Eof) and (not FCancelar) do begin
                Lista.Add( TRIM(spObtenerLineas.FieldByName('Campo').AsString) );
                spObtenerLineas.Next;
                pbProgreso.StepIt;
            end;
            if FCancelar then begin
                MsgBox(PROCESS_CANCELLED, Caption, MB_ICONINFORMATION);
                Exit;
            end;
            pbProgreso.StepIt;
            if Lista.Count > 0 then begin
                // genere algo...
                // registro la operacion
                pbProgreso.StepIt;
                if not RegistrarOperacionEnLogInterface(DMCOnnections.BaseCAC, RO_MOD_INTERFAZ_SALIDA_ASIGNACIONES_CLEARING_BHTU,
                    FArchivo,UsuarioSistema,'',False,False,0,0, CodigoOperacion, descError) then begin
                    MsgBoxErr(ERROR_LOG, descError, Caption, MB_ICONERROR);
                    Exit;
                end;
                // Guardo el archivo...
                pbProgreso.StepIt;
                Lista.SaveToFile(GoodDir(txtDestinoArchivo.Text)+FArchivo);

                // Obtengo la cantidad de patentes para guardarla en el archivo.
                pbProgreso.StepIt;
                qtyPatente := QueryGetValueInt( DMConnections.BaseCAC,
                  'SELECT COUNT( DISTINCT Patente ) FROM ##AsignacionesEnviadas (NOLOCK)');

                // Inicio una transaccion por las dudas...
                //DMConnections.BaseCAC.BeginTrans;                                                       //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('BEGIN TRAN fGenerarAsignacionesBHTU');              //SS_1385_NDR_20150922

                // Marco las asignaciones que inclu� en el archivo como enviadas
                spMarcarAsignaciones.Parameters.ParamByName('@CodigoOperacion').Value := CodigoOperacion;
                spMarcarAsignaciones.ExecProc;
                pbProgreso.StepIt;

                // Cierro el log de operaciones
                if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, CodigoOPeracion, format(MSG_QTY_TAN, [QtyPatente]), descError) then begin
                    //if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;                              //SS_1385_NDR_20150922
                    DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fGenerarAsignacionesBHTU END');	    //SS_1385_NDR_20150922
                    BorrarArchivo;
                    MsgBoxErr(ERROR_LOG_END, descError, Caption, MB_ICONERROR);
                    Exit;
                end;
                // hago el commit
                //DMConnections.BaseCAC.CommitTrans;                                                                //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('COMMIT TRAN fGenerarAsignacionesBHTU');					              //SS_1385_NDR_20150922
                MsgBox(PROCESS_OK, Caption, MB_ICONINFORMATION);
                MostrarReporte(CodigoOperacion);
            end
            else begin
                // no habia nada para enviar...
                MsgBox(NOTHING_TO_SAVE, Caption, MB_ICONINFORMATION);
            end;

        except
            // Fall� algo...
            on e: exception do begin
                //if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fGenerarAsignacionesBHTU END');	    //SS_1385_NDR_20150922
                BorrarArchivo;
                MsgBoxErr(ERROR_PROCESO, e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
        LimpiarTablas;
        pnlAvance.Visible := False;
        spObtenerLineas.Close;
        Lista.Free;
        FProcesando := False;
        FCancelar := False;
        btnProcesar.Enabled := True;
        btnSalir.Caption := CAP_CLOSE;
    end;
end;

{******************************** Function Header ******************************
Function Name: BorrarArchivo
Author : ndonadio
Date Created : 06/10/2005
Description : Si existe, borra el archivo...
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmGenerarArchivoAsignacionesBHTU.BorrarArchivo;
begin
    try
        if FileExists(txtDestinoarchivo.Text) then DeleteFile(txtDestinoarchivo.Text);
    except
        on exception do exit;
    end;
end;

{******************************** Function Header ******************************
Function Name: MostrarReporte
Author : ndonadio
Date Created : 06/10/2005
Description : Muestra el reporte de finalizacion
Parameters : CodigoOperacion: Integer
Return Value : None
*******************************************************************************}
procedure TfrmGenerarArchivoAsignacionesBHTU.MostrarReporte(CodigoOperacion: Integer);
var
    f:TfrmReporteGeneracionArchivoAsignacionBHTU;
begin
    Application.CreateForm(  TfrmReporteGeneracionArchivoAsignacionBHTU, f);
    if not f.Inicializar(Caption, CodigoOperacion) then f.Release;
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : ndonadio
Date Created : 06/10/2005
Description : Sale de la aplicacion o cancela si esta procesando.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGenerarArchivoAsignacionesBHTU.btnSalirClick(Sender: TObject);
begin
    if FProcesando then FCancelar := True else Close;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : ndonadio
Date Created : 06/10/2005
Description : Para que se cierre el form...
Parameters : Sender: TObject; var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TfrmGenerarArchivoAsignacionesBHTU.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

{******************************** Function Header ******************************
Function Name: VerificarDestino
Author : ndonadio
Date Created : 12/10/2005
Description : Verifica que lo que est� escrito en el selector de directorios
              sea un directorio v�lido...
Parameters : None
Return Value : boolean
*******************************************************************************}
function TfrmGenerarArchivoAsignacionesBHTU.VerificarDestino: boolean;
var
    Location: AnsiString;
begin
    Result := False;
    if TRIM(GoodDir(txtDestinoArchivo.Text)) <> TRIM(GoodDir(FDestino)) then begin
        Location := GoodDir(TRIM(txtDestinoArchivo.Text));
        if not DirectoryExists(Location) then Exit;
        // lo carga en las variables, edits, etc.
        FDestino := GoodDir(Location);
        txtDestinoArchivo.Text := GoodDir(Location);
        // Habilita el boton procesar si puede crear nombres de archivos...
        Result := CrearNombresArchivos(FDestino);
    end
    else Result := True;
end;

{******************************** Function Header ******************************
Function Name: LimpiarTablas
Author : ndonadio
Date Created : 12/10/2005
Description : Limpia las tablas temporales si aun estan abiertas...
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmGenerarArchivoAsignacionesBHTU.LimpiarTablas;
resourcestring
    ERROR_DELETING_TEMP_TABLES = 'No se pueden eliminar las tablas temporales.' + CRLF + 'La aplicaci�n se cerrar� para completar el proceso de eliminaci�n.' +
      CRLF + CRLF + 'Puede volver a entrar de inmediato si es necesario.';
begin
    try
        spLimpiarTemporalesEnvioAsignacionesBHTU.ExecProc
    except
         on e:exception do begin
            MsgBoxErr(ERROR_DELETING_TEMP_TABLES, e.Message, Caption, MB_ICONWARNING);
            Application.Terminate;
        end;
    end;
end;

end.
