object frmRendicionPAC: TfrmRendicionPAC
  Left = 237
  Top = 257
  Anchors = [akLeft]
  Caption = 'Recepci'#243'n de Rendici'#243'n Santander'
  ClientHeight = 294
  ClientWidth = 570
  Color = clBtnFace
  Constraints.MinHeight = 328
  Constraints.MinWidth = 578
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    570
    294)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 6
    Width = 555
    Height = 239
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object btnAbrirArchivo: TSpeedButton
    Left = 446
    Top = 30
    Width = 23
    Height = 22
    Anchors = [akTop, akRight]
    Caption = '...'
    OnClick = btnAbrirArchivoClick
  end
  object Label2: TLabel
    Left = 18
    Top = 14
    Width = 208
    Height = 13
    Caption = 'Ubicaci'#243'n del archivo de Rendici'#243'n:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblReferencia: TLabel
    Left = 98
    Top = 194
    Width = 76
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'lblReferencia'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edOrigen: TEdit
    Left = 16
    Top = 30
    Width = 425
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
  end
  object btnProcesar: TButton
    Left = 322
    Top = 257
    Width = 75
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = '&Procesar'
    Enabled = False
    TabOrder = 1
    OnClick = btnProcesarClick
  end
  object btnCancelar: TButton
    Left = 402
    Top = 257
    Width = 75
    Height = 26
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    Enabled = False
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object btnSalir: TButton
    Left = 482
    Top = 257
    Width = 75
    Height = 26
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 3
    OnClick = btnSalirClick
  end
  object pnlAvance: TPanel
    Left = 91
    Top = 59
    Width = 408
    Height = 104
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    Caption = 'Presione bot'#243'n para cancelar'
    TabOrder = 4
    DesignSize = (
      408
      104)
    object lblprocesoGeneral: TLabel
      Left = 8
      Top = 79
      Width = 80
      Height = 13
      Anchors = [akLeft, akRight, akBottom]
      Caption = 'Progreso general'
    end
    object pbProgreso: TProgressBar
      Left = 98
      Top = 78
      Width = 259
      Height = 17
      Anchors = [akLeft, akRight, akBottom]
      Smooth = True
      Step = 1
      TabOrder = 0
    end
  end
  object pnlAyuda: TPanel
    Left = 520
    Top = 14
    Width = 25
    Height = 23
    Anchors = [akTop, akRight]
    BevelInner = bvRaised
    TabOrder = 5
    object ImgAyuda: TImage
      Left = 2
      Top = 2
      Width = 21
      Height = 19
      Cursor = crHandPoint
      Align = alClient
      Picture.Data = {
        07544269746D617036100000424D361000000000000036000000280000002000
        0000200000000100200000000000001000000000000000000000000000000000
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6
        C600B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
        B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
        B500B5B5B500B5B5B500B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636B63006363
        6300636363006363630063636300636363006363630063636300636363006363
        6300636363006363630063636300636363006363630063636300636363006363
        63006363630063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006363630094E7
        FF008CE7FF009CE7FF009CE7FF009CE7FF009CE7FF00A5EFFF00ADEFFF00ADEF
        FF00ADEFFF00ADEFFF00B5EFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00C6F7FF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        630063636300EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        630063636300EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF009CD6
        FF00B5DEFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF009CD6
        FF00B5DEFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        63004AADFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        63000084FF00DEEFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00B5DE
        FF0063636300108CFF00DEEFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF009CD6FF0063636300108CFF00DEF7FF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00DEEFFF0063636300108CFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00C6EFFF00ADDEFF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF0084C6FF0063636300A5D6FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF006363630063636300DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00A5D6FF0063636300A5D6FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF0042A5FF006363630094D6FF00E7FF
        FF00E7FFFF00DEEFFF00108CFF0063636300DEF7FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00C6EFFF00108CFF0063636300319C
        FF004AADFF000084FF00636363006BBDFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00CEEFFF0042A5FF006363
        6300636363006363630084C6FF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0063636300BDEF
        FF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636B63006363
        6300636363006363630063636300636363006363630063636300636363006363
        6300636363006363630063636300636363006363630063636300636363006363
        63006363630063636300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00}
      Stretch = True
      Transparent = True
      OnClick = ImgAyudaClick
      OnMouseMove = ImgAyudaMouseMove
    end
  end
  object OpenDialog: TOpenDialog
    Filter = 'Archivos de Texto|*.TXT;*.ASC'
    Left = 10
    Top = 255
  end
  object spProcesarRendicion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ProcesarRendicionPAC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoEstadoDebito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PAC_CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PAC_CodigoTipoCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAC_NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PAC_Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PAC_Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@PAT_CodigoTipoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAT_NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@PAT_FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@PAT_CodigoEmisorTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAT_Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeCodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ChequeNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeTitular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeCodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@ChequeNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Cupon'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cuotas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraPago'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoAutorizacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConceptoPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = Null
      end>
    Left = 78
    Top = 256
  end
  object spRegistrarOperacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarLogOperacionesInterfases;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoModulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Fecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NombreArchivo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@recibido'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@FechaHoraRecepcion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 111
    Top = 256
  end
  object spActualizaObservacionesLOG: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarObservacionesLogOperacionesInterfase;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 143
    Top = 256
  end
  object SpObtenerResumenComprobantesRecibidos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerResumenComprobantesRecibidos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaProcesamiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = 36526d
      end
      item
        Name = '@NombreArchivo'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = #39#39
      end
      item
        Name = '@Modulo'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = #39#39
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = #39#39
      end
      item
        Name = '@CantidadComprobantesRecibidos'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end
      item
        Name = '@ImporteTotalRecibido'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 12
        Value = '0'
      end
      item
        Name = '@CantidadComprobantesAceptados'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end
      item
        Name = '@ImporteTotalAceptado'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 12
        Value = '0'
      end
      item
        Name = '@CantidadComprobantesRechazados'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end
      item
        Name = '@ImporteTotalRechazado'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 12
        Value = '0'
      end>
    Left = 176
    Top = 256
    object SpObtenerResumenComprobantesRecibidosMotivo: TStringField
      FieldName = 'Motivo'
      Size = 50
    end
    object SpObtenerResumenComprobantesRecibidosCantidad: TIntegerField
      FieldName = 'Cantidad'
      ReadOnly = True
    end
    object SpObtenerResumenComprobantesRecibidosMonto: TStringField
      FieldName = 'Monto'
      ReadOnly = True
    end
  end
  object SPAgregarRendicionPAC: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarRendicionPAC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoBancoSBEI'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 17
        Value = Null
      end
      item
        Name = '@Monto'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@RutSubscriptor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@NumeroConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaDebito'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EstadoDebito'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Reintento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 48
    Top = 256
  end
end
