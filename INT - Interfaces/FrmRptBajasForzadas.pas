{-------------------------------------------------------------------------------
Firma       : SS_1156B_NDR_20140128
Description : Reporte de bajas forzadas masivas
-------------------------------------------------------------------------------}
Unit FrmRptBajasForzadas;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  Utildb,                    //querygetvalue
  UtilProc,                  //Mensajes
  //general
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup;

type
  TFRptBajasForzadas = class(TForm)
    rptBajasForzadas: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppLCodigoOperacionInterfase: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    ppParameterList1: TppParameterList;
	  RBIListado: TRBInterface;
    SpObtenerReporteBajasForzadas: TADOStoredProc;
    ppLine5: TppLine;
    ppLUsuario: TppLabel;
    ppLFechaHora: TppLabel;
    ppUsuario: TppLabel;
    ppFechaHora: TppLabel;
    ppCodigoOperacion: TppLabel;
    ppLabel6: TppLabel;
    ppLabel8: TppLabel;
    ppLineasArchivo: TppLabel;
    ppRegistrosProcesados: TppLabel;
    ppLabel1: TppLabel;
    ppRegistrosErroneos: TppLabel;
    ppDBReporteBajasForzadas: TppDBPipeline;
    dsReporteBajasForzadas: TDataSource;
    ppErrorBajaForzada: TppDBText;
    ppLabel3: TppLabel;
    pplblArchivo: TppLabel;
    ppArchivo: TppLabel;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FCodigoOperacionInterfase : Integer;
    FNombreReporte : String;
    FCantidadRegistros : Integer;
    FCantidadErrores : Integer;
    { Private declarations }
  public
    Function Inicializar( CodigoOperacionInterfase,
                          CantidadRegistros,
                          CantidadErrores:Integer;
                          NombreReporte: AnsiString) : Boolean;
    { Public declarations }
  end;

var
  FRptBajasForzadas: TFRptBajasForzadas;

implementation

{$R *.dfm}






Function TFRptBajasForzadas.Inicializar(CodigoOperacionInterfase,
                                        CantidadRegistros,
                                        CantidadErrores:Integer;
                                        NombreReporte: AnsiString
                                        ) : Boolean;
begin
    FCodigoOperacionInterfase := CodigoOperacionInterfase;
    FNombreReporte := NombreReporte;
    FCantidadRegistros := CantidadRegistros;
    FCantidadErrores := CantidadErrores;
    Result := RBIListado.Execute;
end;






Procedure TFRptBajasForzadas.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
Resourcestring
    MSG_ERROR_AL_GENERAR_REPORTE = 'Error al generar el reporte';
    MSG_ERROR = 'Error';
Const
    REPORT = 'Reporte';
begin
    try
        with SpObtenerReporteBajasForzadas do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
            Parameters.ParamByName('@FechaProcesamiento').Value       := Now;
            Parameters.ParamByName('@Usuario').Value                  := '';
            Parameters.ParamByName('@NombreArchivo').Value            := '';
            Parameters.ParamByName('@LineasArchivo').Value            := 0;
            Parameters.ParamByName('@RegistrosProcesados').Value      := 0;
            Parameters.ParamByName('@RegistrosErroneos').Value        := 0;
            Open;
        end;
    except
        on e: Exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
        end;
    end;
    try
        //Asigno los valores a los campos del reporte
        with SpObtenerReporteBajasForzadas do begin
            //Encabezado
            ppFechaHora.Caption           := DateTimeToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
            ppCodigoOperacion.Caption     := InttoStr(FCodigoOperacionInterfase);
            ppUsuario.Caption             := Parameters.ParamByName('@Usuario').Value;
            ppLineasArchivo.Caption       := Parameters.ParamByName('@LineasArchivo').Value;
            ppRegistrosProcesados.Caption := Parameters.ParamByName('@RegistrosProcesados').Value;
            ppRegistrosErroneos.Caption   := Parameters.ParamByName('@RegistrosErroneos').Value;
            ppArchivo.Caption             := Parameters.ParamByName('@NombreArchivo').Value;
        end;
        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FnombreReporte;
    except
        on E: exception do begin
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.
