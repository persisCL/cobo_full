object ReEmisionAnulacionDiscoXMLInfraccionesForm: TReEmisionAnulacionDiscoXMLInfraccionesForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'ReEmisi'#243'n / Anulaci'#243'n de Disco XML Infractores'
  ClientHeight = 668
  ClientWidth = 1121
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    1121
    668)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 4
    Top = 4
    Width = 1111
    Height = 657
    Anchors = [akLeft, akTop, akRight, akBottom]
    ExplicitWidth = 883
    ExplicitHeight = 616
  end
  object GroupBox2: TGroupBox
    Left = 12
    Top = 8
    Width = 1094
    Height = 53
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Concesionaria: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object cbConcesionaria: TVariantComboBox
      Left = 20
      Top = 19
      Width = 332
      Height = 21
      Style = vcsDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      OnChange = cbConcesionariaChange
      Items = <>
    end
  end
  object GroupBox1: TGroupBox
    Left = 12
    Top = 65
    Width = 1094
    Height = 261
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Discos Emitidos: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    DesignSize = (
      1094
      261)
    object Label2: TLabel
      Left = 370
      Top = 22
      Width = 70
      Height = 13
      Caption = 'Tipo Disco :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 20
      Top = 22
      Width = 67
      Height = 13
      Caption = 'Mostrar desde'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 203
      Top = 22
      Width = 37
      Height = 13
      Caption = 'hasta el'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 17
      Top = 240
      Width = 142
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Total Discos Mostrados :'
      ExplicitTop = 199
    end
    object lblDiscosMostrados: TLabel
      Left = 161
      Top = 240
      Width = 6
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ExplicitTop = 199
    end
    object cbTiposDisco: TVariantComboBox
      Left = 445
      Top = 19
      Width = 180
      Height = 21
      Style = vcsDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 2
      OnChange = cbTiposDiscoChange
      Items = <>
    end
    object dblDiscos: TDBListEx
      Left = 17
      Top = 50
      Width = 1061
      Height = 175
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 70
          Header.Caption = 'Nro. Disco'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'CodigoDisco'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 135
          Header.Caption = 'Fecha/Hora'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'FechaHora'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 75
          Header.Caption = 'Desde'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'FechaDesde'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 75
          Header.Caption = 'Hasta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'FechaHasta'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Tot. Archivos'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'TotalArchivos'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Tot. Infracciones'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'TotalInfracciones'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'ReEmisiones'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'TotalReEmisiones'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 60
          Header.Caption = 'Anulado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'Anulado'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 65
          Header.Caption = 'Interfase'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'CodigoOperacionInterfase'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Usuario'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'Usuario'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 125
          Header.Caption = 'Interfase Anulaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'CodigoOperacionInterfaseAnulacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Usuario Anulaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          Color = clWindow
          IsLink = False
          FieldName = 'UsuarioAnulacion'
        end>
      Color = clWhite
      DataSource = dsObtenerDatosDiscoXMLInfractoresEmitidos
      DragReorder = True
      ParentColor = False
      TabOrder = 4
      TabStop = True
      OnDrawBackground = dblDiscosDrawBackground
      OnDrawText = dblDiscosDrawText
    end
    object deFechaDesde: TDateEdit
      Left = 95
      Top = 19
      Width = 97
      Height = 21
      AutoSelect = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = deFechaDesdeChange
      Date = -693594.000000000000000000
    end
    object deFechaHasta: TDateEdit
      Left = 255
      Top = 19
      Width = 97
      Height = 21
      AutoSelect = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = deFechaHastaChange
      Date = -693594.000000000000000000
    end
    object btnActualizarDatosDiscos: TButton
      Left = 1003
      Top = 17
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Actualizar'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = btnActualizarDatosDiscosClick
    end
  end
  object GroupBox5: TGroupBox
    Left = 12
    Top = 329
    Width = 1094
    Height = 110
    Anchors = [akLeft, akRight, akBottom]
    Caption = ' Opciones Directorios '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    DesignSize = (
      1094
      110)
    object Label1: TLabel
      Left = 17
      Top = 63
      Width = 190
      Height = 13
      Caption = 'Directorio destino archivos XML :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 17
      Top = 20
      Width = 121
      Height = 13
      Caption = 'Directorio im'#225'genes :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object peDirectorioImagenes: TPickEdit
      Left = 17
      Top = 36
      Width = 1061
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Enabled = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 0
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      OnChange = peDirectorioImagenesChange
      EditorStyle = bteTextEdit
      OnButtonClick = peDirectorioImagenesButtonClick
    end
    object peDirectorioXML: TPickEdit
      Left = 17
      Top = 79
      Width = 1061
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Enabled = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = peDirectorioXMLChange
      EditorStyle = bteTextEdit
      OnButtonClick = peDirectorioXMLButtonClick
    end
  end
  object gbEstado: TGroupBox
    Left = 12
    Top = 445
    Width = 1094
    Height = 175
    Anchors = [akLeft, akRight, akBottom]
    Caption = ' Estado del Proceso: Sin Iniciar. '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    DesignSize = (
      1094
      175)
    object lblArchivosGenerados: TLabel
      Left = 153
      Top = 40
      Width = 6
      Height = 13
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblInfraccionesRestantes: TLabel
      Left = 834
      Top = 42
      Width = 244
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = 'Procesando 9999 de 9999 infracciones'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
      ExplicitLeft = 499
    end
    object Label3: TLabel
      Left = 24
      Top = 40
      Width = 123
      Height = 13
      Caption = 'Archivos Generados :'
    end
    object pbProgreso: TProgressBar
      Left = 17
      Top = 19
      Width = 1061
      Height = 17
      Anchors = [akLeft, akTop, akRight]
      Smooth = True
      Step = 1
      TabOrder = 0
    end
    object lsbxAvisos: TListBox
      Left = 16
      Top = 61
      Width = 1062
      Height = 101
      Anchors = [akLeft, akTop, akRight, akBottom]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
    end
  end
  object btnReEmitir: TButton
    Left = 951
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&ReEmitir'
    Enabled = False
    TabOrder = 5
    OnClick = btnReEmitirClick
  end
  object btnSalir: TButton
    Left = 1030
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 6
    OnClick = btnSalirClick
  end
  object btnAnular: TButton
    Left = 872
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Anular'
    Enabled = False
    TabOrder = 4
    OnClick = btnAnularClick
  end
  object spObtenerDatosDiscoXMLInfractoresEmitidos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterScroll = spObtenerDatosDiscoXMLInfractoresEmitidosAfterScroll
    ProcedureName = 'ObtenerDatosDiscoXMLInfractoresEmitidos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Concesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = 1
      end
      item
        Name = '@CodigoTipo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = 1
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 40148d
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 40354d
      end>
    Left = 368
    Top = 192
  end
  object dsObtenerDatosDiscoXMLInfractoresEmitidos: TDataSource
    DataSet = spObtenerDatosDiscoXMLInfractoresEmitidos
    Left = 312
    Top = 176
  end
  object spCargarInfraccionesTransitosDiscoXMLEmitido: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CargarInfraccionesTransitosDiscoXMLEmitido'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoDisco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 248
    Top = 544
  end
  object spObtenerInfraccionesTransitosAEnviarMOP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerInfraccionesTransitosAEnviarMOP'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoDisco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoArchivo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 384
    Top = 528
  end
  object spObtenerInfraccionesAEnviarMOP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerInfraccionesAEnviarMOP'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 432
    Top = 552
  end
  object fnObtenerNombreCortoConcesionaria: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerNombreCortoConcesionaria'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdReturnValue
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 584
    Top = 520
  end
  object spActualizarInfraccionesTransitosAEnviarMOP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarInfraccionesTransitosAEnviarMOP'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoDisco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoArchivo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDiscoAsignado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoArchivoAsignado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Error'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 624
    Top = 520
  end
  object spActualizarDiscoXMLInfractoresReEmisiones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDiscoXMLInfractoresReEmisiones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoDisco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 600
    Top = 560
  end
  object spActualizarDiscoXMLInfractoresReEmisionesErrores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDiscoXMLInfractoresReEmisionesErrores'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 488
    Top = 480
  end
  object fnSePuedeAnularDiscoXML: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'SePuedeAnularDiscoXML'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdReturnValue
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoDisco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 760
    Top = 496
  end
  object spAnularDiscoXMLInfracciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AnularDiscoXMLInfracciones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoDisco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end>
    Left = 792
    Top = 552
  end
end
