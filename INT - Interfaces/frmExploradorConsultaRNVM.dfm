object ExploradorConsultaRNVMForm: TExploradorConsultaRNVMForm
  Left = 0
  Top = 0
  ActiveControl = deFechaDesde
  Caption = 'Explorador de Consultas RNVM'
  ClientHeight = 479
  ClientWidth = 695
  Color = clBtnFace
  Constraints.MinHeight = 513
  Constraints.MinWidth = 703
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object cbBusquedas: TCollapsablePanel
    Left = 0
    Top = 0
    Width = 695
    Height = 97
    Align = alTop
    Animated = False
    Caption = 'B'#250'squedas'
    BarColorStart = 14071199
    BarColorEnd = 10646097
    RightMargin = 0
    LeftMargin = 0
    ArrowLocation = cpaRightTop
    Style = cpsWinXP
    InternalSize = 74
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    object Label1: TLabel
      Left = 36
      Top = 51
      Width = 34
      Height = 13
      Caption = 'Desde:'
    end
    object Label2: TLabel
      Left = 293
      Top = 51
      Width = 32
      Height = 13
      Caption = 'Hasta:'
    end
    object deFechaDesde: TDateEdit
      Left = 75
      Top = 48
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
    object teHoraDesde: TTimeEdit
      Left = 171
      Top = 48
      Width = 68
      Height = 21
      AutoSelect = False
      TabOrder = 2
      AllowEmpty = False
      ShowSeconds = False
    end
    object deFechaHasta: TDateEdit
      Left = 331
      Top = 48
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 3
      Date = -693594.000000000000000000
    end
    object teHoraHasta: TTimeEdit
      Left = 424
      Top = 48
      Width = 68
      Height = 21
      AutoSelect = False
      TabOrder = 4
      AllowEmpty = False
      ShowSeconds = False
    end
    object btnLimpiar: TButton
      Left = 611
      Top = 45
      Width = 75
      Height = 25
      Caption = '&Limpiar'
      TabOrder = 5
      OnClick = btnLimpiarClick
    end
    object btnBuscar: TButton
      Left = 520
      Top = 45
      Width = 75
      Height = 25
      Caption = '&Buscar'
      Default = True
      TabOrder = 6
      OnClick = btnBuscarClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 438
    Width = 695
    Height = 41
    Align = alBottom
    TabOrder = 1
    object btnCerrar: TButton
      Left = 611
      Top = 9
      Width = 75
      Height = 25
      Caption = '&Cerrar'
      TabOrder = 0
      OnClick = btnCerrarClick
    end
  end
  object lstConsultaRNVM: TDBListEx
    Left = 0
    Top = 97
    Width = 695
    Height = 241
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'Fecha'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Patente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'Patente'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Usuario'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'CodigoUsuario'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 70
        Header.Caption = 'Resultado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'Resultado'
      end>
    DataSource = dsObtenerConsultaRNVM
    DragReorder = True
    ParentColor = False
    TabOrder = 2
    TabStop = True
  end
  object Panel2: TPanel
    Left = 0
    Top = 338
    Width = 695
    Height = 100
    Align = alBottom
    TabOrder = 3
    object dbmRespuesta: TDBMemo
      Left = 1
      Top = 1
      Width = 693
      Height = 98
      Align = alClient
      DataField = 'Respuesta'
      DataSource = dsObtenerConsultaRNVM
      TabOrder = 0
    end
  end
  object spObtenerConsultaRNVM: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerConsultaRNVM;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 488
    Top = 112
  end
  object dsObtenerConsultaRNVM: TDataSource
    DataSet = spObtenerConsultaRNVM
    Left = 520
    Top = 112
  end
end
