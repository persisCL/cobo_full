{-----------------------------------------------------------------------------
 File Name: fRecepcionXMLRespuestaInfraccion.pas
 Author:    ndonadio
 Date Created: 15/06/2005
 Language: ES-AR
 Description: Procesa los archivos XML de respuestas de infracciones del MOPTT

Autor       :   Claudio Quezada Ib��ez
Firma       :   SS_1147_CQU_20140408
Fecha       :   23-04-2014
Descripcion :   Se agrega variable y llamada a funci�n para obtener la Concesionaria Nativa

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

  
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto
-----------------------------------------------------------------------------}
unit fRecepcionXMLRespuestaInfraccion;

interface

uses
    // Base de Datos
  DMConnection, UtilDB,
    // Interfases
  ComunesInterfaces,
  // XML
  MOPRespuestasDenunciosBind, MOPInfBind,
  // Otros
  ConstParametrosGenerales,UtilProc, PeaTypes, PeaProcs, strUtils, Util,
  // Generales
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, StdCtrls, ComCtrls, DmiCtrls, DBClient,
  ppCtrls, ppBands, ppReport, ppStrtch, ppSubRpt, ppPrnabl, ppClass,
  ppCache, ppDB, UtilRB, ppProd, ppComm, ppRelatv, ppTxPipe, ppModule,
  raCodMod, ppParameter, DPSControls, ppDBPipe;


type
  TfrmRecepcionXMLRespuestaInfraccion = class(TForm)
    pedOrigen: TPickEdit;
    Label1: TLabel;
    pnlAvance: TPanel;
    lblAvance: TLabel;
    pbProgreso: TProgressBar;
    btnProcesar: TButton;
    btnSalir: TButton;
    Bevel1: TBevel;
    spObtenerCodigoEstadoEnvioInfraccionPorDescripcion: TADOStoredProc;
    spActualizarDetalleInfraccionesEnviadas: TADOStoredProc;
    lblArchivoenProceso: TLabel;
    txtPLERROR: TppTextPipeline;
    ppReport1: TppReport;
    RBInterface1: TRBInterface;
    btnReporte: TButton;
    ppParameterList1: TppParameterList;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLine1: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLabel3: TppLabel;
    ppDetailBand2: TppDetailBand;
    raCodeModule1: TraCodeModule;
    ppFooterBand1: TppFooterBand;
    Descripcion: TppField;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBText9: TppDBText;
    ppShape5: TppShape;
    spActualizarInfraccionesPorXMLInvalido: TADOStoredProc;
    spResumenRespuestaXMLInfracciones: TADOStoredProc;
    cdsResumen: TClientDataSet;
    dsResumen: TDataSource;
    ppDBResumen: TppDBPipeline;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailBand3: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppLabel6: TppLabel;
    ppLabel9: TppLabel;
    ppShape2: TppShape;
    lblEstadoArchivo: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppDBText4: TppDBText;
    ppDBText7: TppDBText;
    ppDBText6: TppDBText;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel8: TppLabel;
    ppLabel10: TppLabel;
    ppLabel13: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    ppLine2: TppLine;
    ppLine3: TppLine;
    procedure pedOrigenButtonClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnReporteClick(Sender: TObject);
    procedure ppDBText3GetText(Sender: TObject; var Text: String);
  private
    { Private declarations }
    strError,
    FOperaciones  : TStringList;
    FErrorGrave,
    FProcesando,
    FCancelar   : Boolean;
    FDirectorioArchivosProcesados: AnsiString;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function IsValidFileName( fname, SourcePath: AnsiString): Boolean;
    function ObtenerDiscoArchivo(fn: AnsiString; var CorrDisco: integer;var CorrFile: Integer): Boolean;
    function ProcesarArchivoXML(sr: TSearchRec; DirOrigen: AnsiString; var DescriError: AnsiString): Boolean;
    Function ActualizarLog(FCodigoOperacion: integer; Observ: AnsiString; Var DescError: AnsiString):boolean;
  public
    { Public declarations }
    function  Inicializar: Boolean;
  end;


const
    CONST_XML_INFRACCION_ACEPTADO_MOP   =   6;
    CONST_FORMATO_INVALIDO_ARCHIVO_XML  =   1;
    // Para evaluar formato del nombre de archivo
    PREFIJO_ARCHIVO_CN      =   'ingreso';
    PREFIJO_ARCHIVO_MOP     =   'salida';
    SEPARADOR               =   '_';
    // Para obtener Disco y Archivo del Nombre de archivo
    COD_CN_LARGO    =   2;
    DISCO_LARGO     =   4;
    ARCHIVO_LARGO   =   4;
    FECHA_LARGO     =   8;

var
  frmRecepcionXMLRespuestaInfraccion: TfrmRecepcionXMLRespuestaInfraccion;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    ndonadio
  Date Created: 15/06/2005
  Description:
  Parameters: txtCaption: ANSIString; MDIChild:Boolean
  Return Value: Boolean
-----------------------------------------------------------------------------}
function  TfrmRecepcionXMLRespuestaInfraccion.Inicializar: Boolean;
resourcestring
 	MSG_INIT_ERROR              = 'No se pudieron obtener los parametros generales. No se puede inicializar.';
    MSG_ERROR_FOLDER_NOT_EXIST  = 'El directorio de origen de Respuestas no existe.';
var
    DirOrigenArchivos: AnsiString;
begin

    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    if not ObtenerParametroGeneral(DMCOnnections.BaseCAC, 'DIR_Respuestas_XML_Infractores', DirOrigenArchivos) then begin
        MsgBox(MSG_INIT_ERROR, Caption, mb_ICONSTOP);
        Exit;
    end;
    if not ObtenerParametroGeneral(DMCOnnections.BaseCAC, 'DIR_Resp_XML_Inf_Procesadas', FDirectorioArchivosProcesados) then begin
        MsgBox(MSG_INIT_ERROR, Caption, mb_ICONSTOP);
        Exit;
    end;

    DirOrigenArchivos := GoodDir(DirOrigenArchivos);
    FDirectorioArchivosProcesados := GoodDir(FDirectorioArchivosProcesados);

    if (not DirectoryExists(FDirectorioArchivosProcesados)) or (not DirectoryExists(DirOrigenArchivos))  then begin
        MsgBox(MSG_ERROR_FOLDER_NOT_EXIST,Caption,MB_ICONERROR);
        Exit;
    end;
    FErrorGrave := False;
    pedOrigen.Text :=  DirOrigenArchivos;
    btnReporte.Enabled := False;
    Result := True;

end;

{-----------------------------------------------------------------------------
  Function Name: pedOrigenButtonClick
  Author:    ndonadio
  Date Created: 15/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmRecepcionXMLRespuestaInfraccion.pedOrigenButtonClick(
  Sender: TObject);
resourcestring
    MSG_SELECT_LOCATION = 'Seleccione la ubicaci�n de los archivos XML';
var
    Location: String;
begin
    Location := Trim(BrowseForFolder(MSG_SELECT_LOCATION));
    if Location = '' then exit;
    pedOrigen.Text := GoodDir(Location);
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    ndonadio
  Date Created: 15/06/2005
  Description:  recupera las respuestas de las infracciones
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmRecepcionXMLRespuestaInfraccion.btnProcesarClick(Sender: TObject);
resourcestring
    CAP_CANCEL  =   '&Cancelar';
    CAP_SALIR   =   '&Salir';
    MSG_INVALID_FILENAME    =   'El nombre de archivo "%s" no es v�lido';
    MSG_PROCESS_CANCELED    =   'El proceso ha sido cancelado por el usuario.';
    MSG_THERE_IS_NO_FILE    =   'No hay archivos para procesar.';
    MSG_PROCESS_OK          =   'El proceso ha finalizado.';
    MSG_ERROR               =   'Error. Proceso cancelado.';
const
    FILE_PATTERN    =   '*.XML';
var
    DirOrigen,
    FullPath   : AnsiString;
    DescError   : AnsiString;
    sr          : TSearchRec;
    EndOfDir       : boolean;
    NadaAProcesar  : boolean;

begin
// acomodo botones, seteo valores iniciales...
    FProcesando         := True;
    NadaAProcesar       := True;
    FCancelar           := False;
    EndOfDir            := False;
    FErrorGrave         := False;
    btnSalir.Caption    := CAP_CANCEL;
    btnProcesar.Enabled := False;
    btnReporte.Enabled  := False;

    strError            := TStringList.Create;
    FOperaciones        := TStringList.Create;

    spObtenerCodigoEstadoEnvioInfraccionPorDescripcion.Close;
    spActualizarDetalleInfraccionesEnviadas.Close;

    DirOrigen := GoodDir(pedOrigen.Text);
    FullPath := GoodDir(DirOrigen) + FILE_PATTERN;
    // encuentro el primer XML de la lista...
    lblArchivoEnProceso.Caption := '';
    lblArchivoenProceso.Visible := True;

    if FindFirst(FullPath, faAnyFile, sr) = 0 then begin
        NadaAProcesar := False;
        while (NOT FCancelar) AND (NOT EndOfDir) AND (NOT FErrorGrave)do begin
            if IsValidFileName(sr.Name, DirOrigen) then begin
                // Proceso el archivo...
                if not ProcesarArchivoXML(sr, DirOrigen, DescError) then begin
                     // hubo algun error...
                     if not FErrorGrave then  begin
                        //si no es grave, lo guardo en un string list para mostrar al final
                        strError.Add(descError)
                     end
                     else begin
                        //Si es grave, muestro el mensaje...
                        MsgBoxErr(MSG_ERROR, DescError,caption, MB_ICONERROR);
                        // sale solo porque no valida el while...
                     end;
                end
                // si se proceso, lo muevo
                else MoverArchivoProcesado(caption, GoodDir(DirOrigen)+ sr.name, GoodDir(FDirectorioArchivosProcesados)+ sr.name, True);
            end
            else begin
                strError.Add(Format(MSG_INVALID_FILENAME, [sr.Name]))
            end;
            Application.ProcessMessages;
            if (FindNext(sr) <> 0 ) then EndOfDir := True;
        end; //while(1)
        pbProgreso.Position := pbProgreso.Max ;
        FindClose(sr);
    end;
//finally...
    if FCancelar then MsgBox(MSG_PROCESS_CANCELED, caption, MB_ICONWARNING)
    else if NadaAProcesar then MsgBox(MSG_THERE_IS_NO_FILE, caption, MB_ICONINFORMATION)
         else if EndOfDir then begin
            MsgBox(MSG_PROCESS_OK, caption, MB_ICONINFORMATION);
            btnReporte.Enabled := True;
            btnReporte.SetFocus;
         end;
    FProcesando := False;
    btnSalir.Caption := CAP_SALIR;
    btnProcesar.Enabled := True;
    pbProgreso.Position := pbProgreso.Min;
end;

{-----------------------------------------------------------------------------
  Function Name: IsValidFileName
  Author:    ndonadio
  Date Created: 15/06/2005
  Description:  Valida que el "formato" del nombre del archivo sea correcto.
                No valida que los valores contenidos en el nombre del archivo lo sean, solo
                el Codigo de Concesionaria
  Parameters: fname: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmRecepcionXMLRespuestaInfraccion.IsValidFileName( fname, SourcePath: AnsiString): Boolean;
var
    Aux         : AnsiString;
    Pos,
    CodError    : Integer;
    AuxB        : Int64;
    FCodigoConcesionariaNativa : Integer;                                           // SS_1147_CQU_20140408
begin
    FCodigoConcesionariaNativa := ObtenerCodigoConcesionariaNativa;                 // SS_1147_CQU_20140408
// salida_0100010001_20050624_20050522.XML
    Pos := 1;
    // el fname debe empezar con "salida_"
    Aux := Copy(fname,Pos,Length(PREFIJO_ARCHIVO_MOP)+LENGTH(SEPARADOR));
    Result := (UpperCase(Aux) = UpperCase(PREFIJO_ARCHIVO_MOP+SEPARADOR));
    if not Result then Exit;
    Pos := Length(PREFIJO_ARCHIVO_MOP)+Length(SEPARADOR)+1;
    // valido que el codigo de concesionaria sea valido.
    //Result := (Copy(fname, Pos, 2) = FormatFloat('00',COD_CONCESIONARIA));        // SS_1147_CQU_20140408
    Result := (Copy(fname, Pos, 2) = FormatFloat('00',FCodigoConcesionariaNativa)); // SS_1147_CQU_20140408
    // si la concesionaria no es la esperada, salgo...
    if not Result then Exit;
    Pos := Pos + 2;
    // Ahora lo esperado son dos numeros seguidos, de cuatro digitos cada uno
    // que representan el Num Correlativo de Disco y de Archivo. NO se validan aca...
    Aux := Copy(fname, pos, 8);
    Val(Aux, AuxB, CodError);
    Result := ((CodError = 0) AND (AuxB > 0));
    // si hubo error salgo...
    if not Result then Exit;
    Pos := Pos  + 8;
    //Lo siguiente es un separador...
    Aux := Copy(fname, Pos,Length(SEPARADOR));
    Result := ( UpperCase(Aux) = UpperCase(SEPARADOR));
    // si no lo era... salgo
    if not Result then Exit;
    Pos := Pos  + 1;
    // Ahora deberia venir una fecha (la de creacion del archivo enviado al MOPTT)
    Aux := Copy(fname, pos, 8) ;
    Result := IsValidDate( Copy(Aux,7,2)+'/'+Copy(Aux,5,2)+'/'+Copy(Aux,1,4));
    // Si no es fecha salgo...
    if not Result then Exit;
    Pos := Pos + 8;
    // Un separador...
    Aux := Copy(fname, Pos,Length(SEPARADOR));
    Result := ( UpperCase(Aux) = UpperCase(SEPARADOR));
    // si no lo era... salgo
    if not Result then Exit;
    Pos := Pos  + 1;
    // Ahora deberia venir otra fecha
    Aux := Copy(fname, pos, 8);
    Result := IsValidDate( Copy(Aux,7,2)+'/'+Copy(Aux,5,2)+'/'+Copy(Aux,1,4));
    // Si no es fecha salgo...
    if not Result then Exit;
    Pos := Pos + 8;
    // finalmente, validamos que la extensi�n sea la correcta: ".XML"
    // le pongo que lea de mas para asegurarme de que ah� termine el nombre de archivo...
    Aux := Copy(fname, Pos,10);
    Result := ( UpperCase(Aux) = '.XML');
end;


{-----------------------------------------------------------------------------
  Function Name: ObtenerDiscoArchivo
  Author:    ndonadio
  Date Created: 30/06/2005
  Description: Obtiene el numero correlativo de disco y el numero correlativo
                de archivo a partir del nombre del archivo
  Parameters: fn: AnsiString; var CorrDisco: integer;var CorrFile: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmRecepcionXMLRespuestaInfraccion.ObtenerDiscoArchivo(fn: AnsiString; var CorrDisco: integer;var CorrFile: Integer): Boolean;
var
    sDisco, sArchivo    : AnsiString;
    parseString: String;
begin

    Result := True;
    parseString := StringReplace(fn, PREFIJO_ARCHIVO_CN, '', [rfReplaceAll]);
    parseString := StringReplace(parseString, SEPARADOR, '', [rfReplaceAll]);

    Delete(parseString, 1, COD_CN_LARGO);
    // extraigo los numeros como strings
    sDisco   := Copy(parseString, 1, DISCO_LARGO);
    sArchivo := Copy(parseString, DISCO_LARGO + 1, ARCHIVO_LARGO);

    // verifico que sean numeros...
    try
        CorrDisco   := StrToInt(sDisco);
        CorrFile    := StrToInt(sArchivo);
    except
        // si fall� es porque no son numeros...
        Result := False;
    end;

end;

{-----------------------------------------------------------------------------
  Function Names: btnSalirClick, FormCloseQuery, FormClose
  Author:    ndonadio
  Date Created: 15/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmRecepcionXMLRespuestaInfraccion.btnSalirClick(
  Sender: TObject);
begin
    if NOT FProcesando then Close else FCancelar := True;
end;
procedure TfrmRecepcionXMLRespuestaInfraccion.FormCloseQuery(
  Sender: TObject; var CanClose: Boolean);
begin
    if FProcesando then CanClose := False;
end;
procedure TfrmRecepcionXMLRespuestaInfraccion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;


{******************************** Function Header ******************************
Function Name: ProcesarArchivoXML
Author : ndonadio
Date Created : 28/07/2005
Description :    Procesa el archivo de respuestas enviado por el MOPTT
Parameters : sr: TSearchRec; DirOrigen: AnsiString; var DescriError: AnsiString
Return Value : Boolean
*******************************************************************************}
Function TfrmRecepcionXMLRespuestaInfraccion.ProcesarArchivoXML(sr: TSearchRec; DirOrigen: AnsiString; var DescriError: AnsiString): Boolean;
resourcestring
    MSG_ERROR_INVALID_XML_FORMAT                = 'El archivo XML no tiene el formato esperado.';
    MSG_ERROR_FILE_ISNOT_VALID_XML              = 'No es un Archivo XML v�lido.';
    MSG_ERROR_INFRACTION_COULD_NOT_BE_UPDATED   = 'La infraccion %d no puede ser actualizada. No pertenece al archivo o no se encuentra el registro del envio.';
    MSG_ERROR_COULDNOT_REGISTER_UPDATE          = 'Error al Actualizar registro de infraccion Nro. %d ';
    MSG_ERROR_INFRACTION_IS_NOT_PENDING         = 'La infraccion %d no puede ser actualizada. Ya tiene un codigo de respuesta.';
    MSG_ERROR_INFRACTION_IS_ANULATED            = 'La infraccion %d est� ANULADA';
    MSG_ERROR_WRONG_VERSION                     = '%s : no es la version correcta del Archivo. Tiene reprocesos.';
    MSG_ERROR_FILE_NOT_SENT                     = '%s : No se encuentran Envios para este archivo.';
    MSG_ERROR_COULDNOT_REGISTER_OP              = 'No se puede registrar operacion en Log de Interfaces. ';
    MSG_ERROR_OBTAINING_DISK_FILE_NUMBERS       = 'No se pueden obtener los n�meros de disco y/o archivo a procesar.';
    MSG_ERROR_CONVERSION                        = 'Error al leer Datos.';

    MSG_PROCESS_OK          = 'Proceso OK';
    MSG_PROCESS_ABORTED     = 'Proceso Terminado Anormalmente';
    MSG_PROCESS_CANCELED    = 'Proceso Cancelado por el Usuario';
    CAP_PROCESSING_FILE     = 'Procesando: ';
var
    XMLRespuesta            : IXMLRespuestasDenunciosType;
    CorrD, CorrA,
    CodigoInterface,
    iNumDen                 : Integer;
    regError                : AnsiString;
    CodigoOperacionRespuesta: Integer;
begin
    Result := False;
    DescriError := '';
    lblArchivoenProceso.caption := CAP_PROCESSING_FILE + sr.name;
    lblArchivoEnProceso.Repaint;

    Cursor := crHourglass;
    try
        try
            // intento cargar el archiv de respuestas...
            XMLRespuesta := LoadRespuestasDenuncios(DirOrigen + sr.name);  //esto puede fallar... con excepcion
        except
            // si fall�, registro el error...
            on e: exception do begin
                if e.ClassName = 'EIntfCastError' then DescriError := ( sr.name + ' - ' + MSG_ERROR_INVALID_XML_FORMAT) // el contenido es XML pero no en el formato esperado...
                else if e.ClassName = 'EDOMParseError' then DescriError := (sr.name + ' - ' + MSG_ERROR_FILE_ISNOT_VALID_XML) //el contenido NO es XML
                    else DescriError := (sr.name + ' - ' + e.Message); // otro error..
                // Salgo...
                Exit;
            end;
        end;
    finally
        Cursor := crDefault;
    end;

    pbProgreso.Position := pbProgreso.Min;
    pbProgreso.Max := XMLRespuesta.Archivo.Denuncio.Count;
    //verifico que el archivo informado sea correcto...
    if not ObtenerDiscoArchivo(XMLRespuesta.Archivo.Nombre, CorrD, CorrA) then begin
        // si fallo... devuelvo la descripcion del error, y salgo, para que pase al siguiente archivo...
        DescriError := sr.Name +' - '+ MSG_ERROR_OBTAINING_DISK_FILE_NUMBERS;
        Exit;
    end;
    // obtengo el codigo de envio para ese archivo
    CodigoInterface := QueryGetValueInt(DMCOnnections.BaseCAC, Format('SELECT dbo.ObtenerCodigoOperacionXMLInfraccion( %d, %d, %s )',
                        [CorrD, CorrA, QuotedStr(XMLRespuesta.Archivo.Nombre)]));
    if CodigoInterface > 0 then begin
        // encontre el envio... es la version correcta...
        // Agrego esta Operacion en el Log Operaciones Interfases...
        if RegistrarOperacionEnLogInterface(DMCOnnections.BaseCAC,RO_MOD_INTERFAZ_ENTRANTE_XML_INFRACCIONES, sr.Name,UsuarioSistema,XMLRespuesta.Archivo.Observaciones,True,False,NowBase(DMConnections.BaseCAC),0,CodigoOperacionRespuesta,regError) then begin

            try
                XMLRespuesta.Archivo.CodigoEstado;
            except
                DescriError := sr.name + ' - ' + XMLRespuesta.Archivo.Nombre + ' - ' + MSG_ERROR_CONVERSION;
                FErrorGrave := False;
                if not ActualizarLog(CodigoOperacionRespuesta, MSG_PROCESS_ABORTED, regError) then DescriError := DescriError + CrLf + RegError;
                Exit;
            end;

            //DMConnections.BaseCAC.BeginTrans;                                             //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('BEGIN TRAN fRecepcionXMLRespuesta');     //SS_1385_NDR_20150922

            if XMLRespuesta.Archivo.CodigoEstado <> CONST_FORMATO_INVALIDO_ARCHIVO_XML then begin
                // Archivo PROCESADO... puede ser Aceptado (FIRMADO) o RECHAZADO:
                //Actualizo los denuncios (infracciones)
                iNumDen := 0;
                While (iNumDen < XMLRespuesta.Archivo.Denuncio.Count) and (not FCancelar) do begin
                    pbProgreso.StepIt ;
                    // Actualizo Denuncio ( iNumDen )
                    // completo info en DetalleInfraccionesEnviadas
                    with XMLRespuesta.Archivo do begin
                        try 
                            // Actualiza las infracciones...
                            spActualizarDetalleInfraccionesEnviadas.Parameters.ParamByName('@CodigoInfraccion').Value := Denuncio[iNumDen].NumInfraccion;
                            spActualizarDetalleInfraccionesEnviadas.Parameters.ParamByName('@CodigoEstado').Value := Denuncio[iNumDen].CodigoEstado;
                            spActualizarDetalleInfraccionesEnviadas.Parameters.ParamByName('@DetalleInfraccion').Value := Trim(Denuncio[iNumDen].Observaciones);
                            spActualizarDetalleInfraccionesEnviadas.Parameters.ParamByName('@CodigoOperacionInterface').Value := CodigoInterface;
                            spActualizarDetalleInfraccionesEnviadas.Parameters.ParamByName('@CodigoOperacionRecepcion').Value := CodigoOperacionRespuesta;
                            spActualizarDetalleInfraccionesEnviadas.ExecProc;
                        except
                            on e: EVariantTypeCastError do begin
                                // si la exceptcion es poir un error de conversion, entonces salgo pero no es un error
                                // grave. Se infroma que el archivo no se puede procesar y se pasa al siguiente.
                                //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fRecepcionXMLRespuesta END');	//SS_1385_NDR_20150922
                                DescriError := sr.name + ' - ' + XMLRespuesta.Archivo.Nombre + ' - ' + MSG_ERROR_CONVERSION ;
                                FErrorGrave := False;
                                if not ActualizarLog(CodigoOperacionRespuesta, MSG_PROCESS_ABORTED, regError) then DescriError := DescriError + CrLf + RegError;
                                Exit;
                            end;
                            on e: Exception do begin
                                // fallo la actualizacion del envio de infraccion... es un error grave.
                                //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fRecepcionXMLRespuesta END');	//SS_1385_NDR_20150922
                                DescriError := sr.name + ' - '+ XMLRespuesta.Archivo.Nombre + ' - ' + e.Message;
                                if not ActualizarLog(CodigoOperacionRespuesta, MSG_PROCESS_ABORTED, regError) then DescriError := DescriError + CrLf + RegError;
                                FErrorGrave := True;
                                Exit;
                            end;
                        end;
                        //segun la respuesta...
                        case spActualizarDetalleInfraccionesEnviadas.Parameters.ParamByName('@RETURN_VALUE').Value OF
                            -1: begin // no se puede registrar la actualizacion
                                    //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                                    DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fRecepcionXMLRespuesta END');	//SS_1385_NDR_20150922
                                    DescriError := (sr.name +' - '+ XMLRespuesta.Archivo.Nombre +' - '+ Format( MSG_ERROR_COULDNOT_REGISTER_UPDATE, [Denuncio[iNumDen].NumInfraccion] ));
                                    if not ActualizarLog(CodigoOperacionRespuesta, MSG_PROCESS_ABORTED, regError) then DescriError := DescriError + CrLf + RegError;
                                    FErrorGrave := True;
                                    Exit;
                                end;
                            // por estos casos debemos insertar en "ErroresInterfaces"
                            -2: begin // si la infraccion ya tiene una respuesta...
                                    AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacionRespuesta, sr.name +','+ XMLRespuesta.Archivo.Nombre +','+FORMAT(MSG_ERROR_INFRACTION_IS_NOT_PENDING,[Denuncio[iNumDen].NumInfraccion]));
                                end;
                            -3: begin   // si la infraccion estaba anulada
                                    AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacionRespuesta, sr.name +','+ XMLRespuesta.Archivo.Nombre +','+FORMAT(MSG_ERROR_INFRACTION_IS_ANULATED,[Denuncio[iNumDen].NumInfraccion]));
                                end;
                             0: begin   // no se actualizo la infraccion...
                                    AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacionRespuesta, sr.name +','+ XMLRespuesta.Archivo.Nombre +','+FORMAT(MSG_ERROR_INFRACTION_COULD_NOT_BE_UPDATED,[Denuncio[iNumDen].NumInfraccion]));
                                end;
                        end;
                    end;
                    inc(iNumDen);
                    Application.ProcessMessages;
                end; //While (2)...
                // si hay transaccion es que todo ejecuto bien, sino hubiera hecho el rollback...y de hecho no llegaria hasta aca...
                //Cierro la transaccion...
                //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('COMMIT TRAN fRecepcionXMLRespuesta');					//SS_1385_NDR_20150922
                // Actualizar Operacion en el Log de Interfases
                // si falla salgo, con FErrorGrave en True y la desc, del error en DescriError...
                if not FCancelar then begin
                    if not ActualizarLog(CodigoOperacionRespuesta, MSG_PROCESS_OK, DescriError) then Exit;
                end
                else begin
                    if not ActualizarLog(CodigoOperacionRespuesta, MSG_PROCESS_CANCELED, DescriError) then Exit;
                end;
            end
            else begin
                // Archivo RECHAZADO...  POR FORMATO INVALIDO
                // Actualizo el detalle de infracciones enviadas con el Estado de ARCHIVO RECHAZADO
                try
                    spActualizarInfraccionesPorXMLInvalido.Parameters.ParamByName('@CodigoEstado').Value := XMLRespuesta.Archivo.CodigoEstado;
                    spActualizarInfraccionesPorXMLInvalido.Parameters.ParamByName('@CodigoOperacionEnvio').Value := CodigoInterface;
                    spActualizarInfraccionesPorXMLInvalido.Parameters.ParamByName('@CodigoOperacionRespuesta').Value := CodigoOperacionRespuesta;
                    spActualizarInfraccionesPorXMLInvalido.Prepared := True;
                    spActualizarInfraccionesPorXMLInvalido.ExecProc;
                except
                    //si fallo, hago el rollback y salgo...
                    //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                    DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fRecepcionXMLRespuesta END');	//SS_1385_NDR_20150922
                    ActualizarLog(CodigoOperacionRespuesta, MSG_PROCESS_ABORTED, DescriError);
                    // es un error grave, se corta el proceso, por eso independientemente del resultado
                    // de ActualizarLog, se marca FErrorGrave = True
                    FErrorGrave := True;
                    Exit;
                end;
                // si llegue aca, salio todo bien, asi que hago el commit...
                //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('COMMIT TRAN fRecepcionXMLRespuesta');					//SS_1385_NDR_20150922
                // si no puedo actualizar el log... salgo con error grave...
                if not ActualizarLog(CodigoOperacionRespuesta, MSG_PROCESS_OK, DescriError) then  Exit;
            end;
        end
        else begin
            // NO PUDE REGISTRAR LA OPERACION EN EL LOG DE INTERFACES...
            DescriError := (sr.name +' - '+ XMLRespuesta.Archivo.Nombre +' - '+ MSG_ERROR_COULDNOT_REGISTER_OP + regError);
            Exit;
        end;
    end
    else begin
         // NO encontr� el envio
         if CodigoInterface = 0 then  DescriError := (sr.name +' - '+ XMLRespuesta.Archivo.Nombre +' - '+ Format( MSG_ERROR_FILE_NOT_SENT,[XMLRespuesta.Archivo.Nombre]))
         // lo encontre pero la version del archivo es incorrecta
         else   DescriError := (sr.name +' - '+ XMLRespuesta.Archivo.Nombre +' - '+ Format( MSG_ERROR_WRONG_VERSION,[XMLRespuesta.Archivo.Nombre]));
         Exit;
    end;
    // finalmente, si todo salio bien, agrego el Codigo de Operacion de Interfazen mi lista
    // de interfaces procesadas... esto lo usare luego para obtener los datos del Reporte de Finalizacion
    FOperaciones.Add ( (IntToStr(CodigoOperacionRespuesta)) );
    //devuelvo OK!
    Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: btnReporteClick
  Author:    ndonadio
  Date Created: 07/07/2005
  Description:   ejecuta el reporte de finalizacion...
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmRecepcionXMLRespuestaInfraccion.btnReporteClick(
  Sender: TObject);
resourcestring
    MSG_ERROR_RETRIEVING_DATA =  'Error al recuperar datos de Resumen';
var
    archE, stdFile: AnsiString;
    i: integer;
begin
    // guardamos la StringList Error en un archivo temporal
    stdFile :=  TempFile;
    ArchE   := GetTempDir + stdFile;
    strError.SaveToFile(ArchE);
    // asignamos el tmp file creado al pipeline
    txtPLError.FileName := ArchE;
    txtPLError.Open;

    if cdsResumen.Active then  cdsResumen.Close;
    cdsResumen.CreateDataSet;
    
    for i := 0 to FOperaciones.Count-1 do begin
        // levanto los datos...
        try
            spResumenRespuestaXMLInfracciones.Parameters.ParamByName('@Codigo').Value :=  IVal(FOperaciones[i]);
            spResumenRespuestaXMLInfracciones.Open;
        except
            on e:Exception do begin
                //si fallo tiro error y salgo...
                spResumenRespuestaXMLInfracciones.Close;
                MsgBoxErr(MSG_ERROR_RETRIEVING_DATA, e.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
        // inserto en el ClientDataSet
        with cdsResumen do begin
            Insert;
            FieldByName('OperacionRecepcion').Value := spResumenRespuestaXMLInfracciones.FieldByName('OperacionRecepcion').AsInteger;
            FieldByName('OperacionEnvio').Value := spResumenRespuestaXMLInfracciones.FieldByName('OperacionEnvio').asInteger;
            FieldByName('InfraccionesRechazadas').Value := spResumenRespuestaXMLInfracciones.FieldByName('InfraccionesRechazadas').AsInteger;
            FieldByName('InfraccionesFirmadas').Value := spResumenRespuestaXMLInfracciones.FieldByName('InfraccionesFirmadas').AsInteger ;
            FieldByName('InfraccionesConError').Value := spResumenRespuestaXMLInfracciones.FieldByName('InfraccionesConError').AsInteger;
            FieldByName('ArchivoRespuesta').Value := spResumenRespuestaXMLInfracciones.FieldByName('ArchivoRespuesta').asString;
            FieldByName('ArchivoEnviado').Value := spResumenRespuestaXMLInfracciones.FieldByName('ArchivoEnviado').asString;
            FieldByName('EstadoRespuesta').Value := spResumenRespuestaXMLInfracciones.FieldByName('EstadoRespuesta').asString;
            Post;
        end;
        spResumenRespuestaXMLInfracciones.Close;
    end;

    // Ejecutamos el reporte...
    rbInterface1.Execute(True);

    // borramos el archivo temporal de Errores...
    txtPLError.Close;
    DeleteFile(ArchE);
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarLog
  Author:    ndonadio
  Date Created: 06/07/2005
  Description: actualiza el log de operaciones con la fecha hora de finalizacion...
  Parameters: Observ: AnsiString
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TfrmRecepcionXMLRespuestaInfraccion.ActualizarLog(FCodigoOperacion: integer; Observ: AnsiString; var DescError : AnsiString): boolean;
begin
    Result := ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOperacion,Observ,DescError);
    FErrorGrave := NOT Result;
end;



{******************************** Function Header ******************************
Function Name: ppDBText3GetText
Author : ndonadio
Date Created : 14/07/2005
Description : Actualizo la visualizacion del estado de respuesta
Parameters : Sender: TObject; var Text: String
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionXMLRespuestaInfraccion.ppDBText3GetText(
  Sender: TObject; var Text: String);
begin
    if ppDBResumen.FieldValues['EstadoRespuesta'] = 'A' then begin
        lblEstadoArchivo.Font.Style := [];
        lblEstadoArchivo.Caption := 'SI' ;
    end
    else begin
        lblEstadoArchivo.Font.Style := [fsBold];
        lblEstadoArchivo.Caption := 'NO'  ;
    end;
end;

end.
