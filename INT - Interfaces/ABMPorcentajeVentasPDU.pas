{********************************** File Header ********************************
File Name : ABMPorcentajeVentasPDU.pas
Author: lgisuk
Date Created: 21/11/2005
Language : ES-AR
Description : ABM Porcentaje que Correponde a CN por Ventas de PDU

Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
*******************************************************************************}
unit ABMPorcentajeVentasPDU;

interface

uses
  //ABM Porcentaje Ventas PDU
  DMConnection,
  Util,
  UtilProc,
  UtilDB,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Validate, DateEdit, StdCtrls, DmiCtrls, ExtCtrls, DbList,
  Abm_obj, ADODB;

type
  TFAbmPorcentajeVentasPDU = class(TForm)
    AbmToolbar: TAbmToolbar;
    AbmList: TAbmList;
    pnlEditing: TPanel;
    LPorcentaje: TLabel;
    LfechadeInicio: TLabel;
    Lbl_porcentaje: TLabel;
    nePorcentaje: TNumericEdit;
    deFechaInicio: TDateEdit;
    pnlButtons: TPanel;
    Notebook: TNotebook;
    btnSalir: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    QryPorcentajeVentasDayPass: TADOQuery;
    procedure btnSalirClick(Sender: TObject);
    procedure abmListClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure abmListInsert(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure abmListDelete(Sender: TObject);
    procedure abmListEdit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure abmListDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure abmListRefresh(Sender: TObject);
  private
    { Private declarations }
    FInicializando: Boolean;
    Procedure CambiarAModo(Modo : Integer);
  public
    { Public declarations }
    Function Inicializar : Boolean;
  end;

const
    CONSULTA  = 0; //Indica que la ventana esta en modo normal
    EDICION = 1;   //Indica que la ventana esta en modo edicion

var
  FFAbmTasasInteres: TFAbmPorcentajeVentasPDU;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: Inicializar
Author : lgisuk
Date Created : 21/11/2005
Description :  inicializacion de este formulario
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TFAbmPorcentajeVentasPDU.Inicializar : Boolean;
resourcestring
    MSG_ERROR_INIT = 'Error de Inicializacion';
    MSG_ERROR = 'ERROR';
Const
    STR_EXIT = 'Salir';
Var
    S: TSize;
begin
    FInicializando := True;
    try
        FormStyle := fsMDIChild;
      	S := GetFormClientSize(Application.MainForm);
      	SetBounds(0, 0, S.cx, S.cy);
        CenterForm(Self);
        try
            QryPorcentajeVentasDayPass.Open;
            deFechaInicio.Enabled := False;
            nePorcentaje.Enabled := False;
            abmList.Enabled := True;
            Notebook.ActivePage := STR_EXIT;
            abmList.Reload;
            Result := True;
        except
            on e:exception do begin
                MsgBoxErr(MSG_ERROR_INIT, e.Message, MSG_ERROR, MB_ICONSTOP) ;
                Result := False;
            end;
        end;
    finally
       FInicializando := False;
       abmListclick(self);
    end;
end;

{******************************** Function Header ******************************
Function Name: CambiarAModo
Author : lgisuk
Date Created : 21/11/2005
Description : Permite cambiar a Modo Edici�n o Modo Normal
Parameters : None
Return Value : Boolean
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.CambiarAModo(Modo: integer);
Const
    STR_EXIT = 'Salir';
    STR_EDITING = 'Editing';
begin
    if (Modo = CONSULTA) then begin
        deFechaInicio.Enabled := False;
        nePorcentaje.Enabled := False;
        abmList.Enabled := True;
        Notebook.ActivePage := STR_EXIT;
        abmList.Estado := Normal;
        abmList.Reload;
        abmListClick(Self);
    end
    else begin
        if abmList.Estado = Alta then begin
            deFechaInicio.Date := NowBase(DMConnections.BaseCAC);
            nePorcentaje.Value := 0;
            deFechaInicio.Enabled := True;
        end else begin
            deFechaInicio.Enabled := False;
        end;
        nePorcentaje.Enabled := True;
        abmToolbar.Habilitados := [];
        abmList.Access := [] ;
        abmList.Enabled := False;
        Notebook.ActivePage := STR_EDITING;
        nePorcentaje.SetFocus ;
    end;
end;

{******************************** Function Header ******************************
Function Name: abmListRefresh
Author : lgisuk
Date Created : 21/11/2005
Description : Permito Actualizar la grilla
Parameters : None
Return Value : Boolean
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.abmListRefresh(Sender: TObject);
begin
  	if QryPorcentajeVentasDayPass.IsEmpty then begin
        deFechaInicio.Date := Date;
        nePorcentaje.Value := 0;
    end;
end;

{******************************** Function Header ******************************
Function Name: abmListDrawItem
Author : lgisuk
Date Created : 21/11/2005
Description : Muestro los datos en la grilla
Parameters : None
Return Value : Boolean
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.abmListDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
    With Sender.Canvas, Tabla  do begin
        FillRect(Rect);
      	TextOut(Cols[0], Rect.Top, FormatDateTime('dd/mm/yyyy',Tabla.FieldByName('FechaInicio').AsDateTime));
        TextOut(Cols[1], Rect.Top, Tabla.FieldByName('Porcentaje').AsString + ' %' );
    end;
end;

{******************************** Function Header ******************************
Function Name: abmListClick
Author : lgisuk
Date Created : 21/11/2005
Description :
Parameters : None
Return Value : Boolean
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.abmListClick(Sender: TObject);
begin
    if not FInicializando then begin
        deFechaInicio.Date := QryPorcentajeVentasDayPass.FieldByName('FechaInicio').AsDateTime;
        nePorcentaje.Value := (QryPorcentajeVentasDayPass.FieldByName('Porcentaje').AsInteger);
        abmList.Access := [accAlta, accBaja, accModi] ;
        abmToolbar.Habilitados := [btAlta, btBaja, btModi];
    end;
end;

{******************************** Function Header ******************************
Function Name: abmListEdit
Author : lgisuk
Date Created : 21/11/2005
Description : Permito editar un registro
Parameters : None
Return Value : Boolean
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.abmListEdit(Sender: TObject);
begin
    CambiarAModo(EDICION);
end;

{******************************** Function Header ******************************
Function Name: abmListInsert
Author : lgisuk
Date Created : 21/11/2005
Description : Alta Nuevo Registro
Parameters : None
Return Value : Boolean
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.abmListInsert(Sender: TObject);
begin
    CambiarAModo(EDICION);
end;

{******************************** Function Header ******************************
Function Name: abmListDelete
Author : lgisuk
Date Created : 21/11/2005
Description : Permito Eliminar un Registro
Parameters : None
Return Value : Boolean
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.abmListDelete(Sender: TObject);
resourcestring
    ERROR_DELETE = 'Error al intentar eliminar el registro';
    MSG_DELETE =   'Desea eliminar el porcentaje?';
    TITLE_DELETE = 'Confirme Eliminaci�n';
const
    MSG_ERROR = 'ERROR';
begin
    try
        try
            // Solicito al usuario confirmacion de lo que quiere borrar
            if (MsgBox(Format(MSG_DELETE,[ (floattostr((QryPorcentajeVentasDayPass.FieldByName('Porcentaje').asInteger)) + '%') , QryPorcentajeVentasDayPass.FieldByName('FechaInicio').asString ]),TITLE_DELETE,MB_ICONQUESTION+MB_YESNO) = mrYes) then begin
                QryPorcentajeVentasDayPass.DeleteRecords(arCurrent);
                abmList.Repaint;
                abmListClick(Sender);
            end;
        except
            on E : Exception do begin
                MsgBoxErr(ERROR_DELETE, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    finally
        CambiarAModo(CONSULTA);
    end;
end;

{******************************** Function Header ******************************
Function Name: btnAceptarClick
Author : lgisuk
Date Created : 21/11/2005
Description : Permito Realizar una Alta o Modificaci�n
Parameters : None
Return Value : Boolean

Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR_INSERT = 'No se ha insertado ning�n registro';
    MSG_ERROR_MODIFY = 'No se ha modificado el registro';
    MSG_ERROR_TITLE = 'ERROR';
    MSG_TITLE_OVERWRITE = 'Confirme sobreescritura';
    MSG_CONFIRM_OVERWRITE = 'Ya existe un porcentaje cargado para esa fecha. Desea modificarlo?';
    MSG_ERROR_INCORRECT_DATA = 'Datos Incorrectos';
    MSG_ERROR_INVALID_DATE = 'La Fecha no es v�lida!';
    MSG_ERROR_INVALID_PERCENTAGE = 'el porcentaje debe ser mayor que cero y menor o igual a 100%';
    MSG_ERROR_EXIST_PERCENTAGE_FOR_THAT_DATE = 'Ya existe un porcentaje para la fecha seleccionada!';
var
    sErrorCaption : String;
    fFechaActual  : TDateTime;
begin

     // Si estoy insertando, controlar que la fecha no exista en la tabla.
    if (abmlist.Estado = Alta) then begin
        if (QueryGetValueINT(DMConnections.BaseCAC,'SELECT 1 FROM PorcentajeVentasDayPass WITH (NOLOCK) WHERE tipopasediario = '''+'N'+''' and dbo.PrincipioDia(FechaInicio) = '+ QuotedStr(FormatDateTime('YYYYMMDD',deFechaInicio.Date))) = 1) then begin
            MsgBox(MSG_ERROR_EXIST_PERCENTAGE_FOR_THAT_DATE , Self.Caption, MB_ICONSTOP);
            Exit;
        end;
    end;

    try
        // Fijo la fache actual (toma la fecha del server, con mlisegundos...)
        fFechaActual := NowBase(DMConnections.BaseCAC);
        // Valido los controles
        if ValidateControls([deFechaInicio, nePorcentaje],
           [(deFechaInicio.Date >= EncodeDate(1900,1,1)),
            ((nePorcentaje.Value > 0 ) AND (nePorcentaje.Value <= 100 ))],
             MSG_ERROR_INCORRECT_DATA,
                [MSG_ERROR_INVALID_DATE,
                  MSG_ERROR_INVALID_PERCENTAGE]) then begin
                    // Si los datos son validos
                    if abmList.Estado = Alta then begin
                        // Antes de aceptar el alta, debo controlar
                        // que la fecha ingresada no concuerde con otro registro...
                        if (QryPorcentajeVentasDayPass.Locate( 'FechaInicio',deFechaInicio.Date,[])) AND (deFechaInicio.Date > FFechaActual) then begin
                            // Si existe pregunto si quiere sobreescribir...
                            if MsgBox(MSG_CONFIRM_OVERWRITE,MSG_TITLE_OVERWRITE,MB_ICONQUESTION+MB_YESNO) = mrYes then begin

                                sErrorCaption := MSG_ERROR_MODIFY;
                                QryPorcentajeVentasDayPass.Edit;

                            end
                            else begin

                                // Si no quiere, salgo...
                                CambiarAModo(CONSULTA);
                                Exit;

                            end;
                        end
                        else begin

                            // Si estaba haciendo un alta, sigo...
                            sErrorCaption := MSG_ERROR_INSERT;
                            QryPorcentajeVentasDayPass.Insert;

                        end;
                    end
                    else begin
                        // Sino estaba haciendo una modificacion
                        sErrorCaption := MSG_ERROR_MODIFY;
                        QryPorcentajeVentasDayPass.Edit;
                    end;

                    //Inserto o Actualizo los Datos
                    with QryPorcentajeVentasDayPass do begin

                        FieldByName('FechaInicio').value := iif( (TRUNC(deFechaInicio.Date) = TRUNC(FFechaActual) ), FFechaActual,deFechaInicio.Date);
                        FieldByName('Porcentaje').Value := nePorcentaje.Value;
                        FieldByName('TipoPaseDiario').Value := 'N';
                        // Blanqueo los errores de la connection
                        Connection.Errors.Clear;
                        // Guardo los cambios
                        Post;

                    end;

                    CambiarAModo(CONSULTA);
                    
        end else begin

                // Reseteo los valores porque no fueron validados...
                if (neporcentaje.Value > 100) then nePorcentaje.Value := 0;
                
        end;

    except
        on E : Exception do begin
            MsgBoxErr(sErrorCaption,e.Message,MSG_ERROR_TITLE,MB_ICONSTOP);
            if QryPorcentajeVentasDayPass.State <> dsBrowse then begin
                QryPorcentajeVentasDayPass.Cancel;
            end;
            CambiarAModo(CONSULTA);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnCancelarClick
Author : lgisuk
Date Created : 21/11/2005
Description : Permito Cancelar la Operaci�n
Parameters : None
Return Value : Boolean
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.btnCancelarClick(Sender: TObject);
begin
    // Estaba editando y decido cancelar
    QryPorcentajeVentasDayPass.Cancel;
    CambiarAModo(CONSULTA);
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : lgisuk
Date Created : 21/11/2005
Description :  Permito Salir del Formulario
Parameters : None
Return Value : Boolean
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : lgisuk
Date Created : 21/11/2005
Description : Permito Salir si no esta editando
Parameters : None
Return Value : Boolean
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    CanClose := Not ( nePorcentaje.Enabled );
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : lgisuk
Date Created : 21/11/2005
Description : Libero el formulario de Memoria
Parameters : None
Return Value : Boolean
*******************************************************************************}
procedure TFAbmPorcentajeVentasPDU.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
