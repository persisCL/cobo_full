{-----------------------------------------------------------------------------
 Unit Name: CancelarFacturacionMasiva.pas
 Author:    mtraversa
 Date Created: 29/12/2004
 Language: ES-AR
 Description: M�dulo para la anulacion de procesos masivos de impresion
-----------------------------------------------------------------------------}

unit CancelarFacturacionMasiva;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BuscaTab, DB, ADODB, DmiCtrls, StdCtrls, ComCtrls, ExtCtrls, UtilProc,
  PeaProcs, PeaTypes, RStrings, DMConnection, ComunesInterfaces, Util;

type
  TFrmCancelarFacturacionMasiva = class(TForm)
	Bevel1: TBevel;
    btnProcesar: TButton;
    btnSalir: TButton;
    spObtenerOperacionesFacturacionMasiva: TADOStoredProc;
    spAnularProcesoImpresionMasivo: TADOStoredProc;
    lblFechaInterfase: TLabel;
    edProceso: TBuscaTabEdit;
    buscaOperaciones: TBuscaTabla;
    spObtenerOperacionesFacturacionMasivaCodigoOperacionInterfase: TIntegerField;
    spObtenerOperacionesFacturacionMasivaFecha: TDateTimeField;
    spObtenerOperacionesFacturacionMasivaUsuario: TStringField;
    spObtenerOperacionesFacturacionMasivaCantidad: TIntegerField;
    procedure btnProcesarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    function buscaOperacionesProcess(Tabla: TDataSet;
      var Texto: String): Boolean;
    procedure buscaOperacionesSelect(Sender: TObject; Tabla: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FCodigoOperacionInterfase: LongInt;
    FProcesando: Boolean;
    { Private declarations }
  public
    function Inicializar: Boolean;
    { Public declarations }
  end;

var
  FrmCancelarFacturacionMasiva: TFrmCancelarFacturacionMasiva;

implementation

{$R *.dfm}

function TFrmCancelarFacturacionMasiva.Inicializar: Boolean;
begin
    FProcesando := False;
    FCodigoOperacionInterfase := -1;
    with spObtenerOperacionesFacturacionMasiva, Parameters do begin
		//ParamByName( '@CodigoModulo' ).Value :=  RO_MOD_INTERFAZ_SALIENTE_IMPRESION;
		//ParamByName( '@TraerTodas' ).Value := 1;
    	Open;
    end;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: TFrmCancelarFacturacionMasiva.btnProcesarClick
  Author:    mtraversa
  Date Created: 29/12/2004
  Description: Se pone en Null la FechaHoraImpreso de los comprabantes involucrados
               en el proc. de interfase con codigo FCodigoOperacionInterfase
  Parsmeters: Sender: TObject
  Return Value:None
-----------------------------------------------------------------------------}
procedure TFrmCancelarFacturacionMasiva.btnProcesarClick(Sender: TObject);
resourcestring
	MSG_PROCESO_FINALIZO_CON_EXITO   = 'El proceso finaliz� con �xito';
	MSG_PROCESO_NO_SE_PUDO_COMPLETAR = 'El proceso no se pudo completar';
    MSG_ANULACION_FACTURACION        = 'Anulaci�n Impresi�n Masiva - Cod. Op.: %d';
var
    NuevoCodigoOperacionInterfase: Integer;
    DescError: String;
begin
    btnProcesar.Enabled := False;
    FProcesando := True;
    try
        try
            DMConnections.BaseCAC.BeginTrans;

            with spAnularProcesoImpresionMasivo do begin
                Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
                ExecProc;
            end;

            if not RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_CANCELAR_INTERFAZ_SALIENTE_IMPRESION,
                                                    Now, EmptyStr, UsuarioSistema,
                                                    Format(MSG_ANULACION_FACTURACION, [FCodigoOperacionInterfase]),
                                                    False, Now, NuevoCodigoOperacionInterfase, DescError) then
                Raise(Exception.Create(DescError));

            DMConnections.BaseCAC.CommitTrans;

            spObtenerOperacionesFacturacionMasiva.Close;
            spObtenerOperacionesFacturacionMasiva.Open;

            MsgBox(MSG_PROCESO_FINALIZO_CON_EXITO, Self.Caption, MB_OK + MB_ICONINFORMATION);

            edProceso.Clear;
        except
            on E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_PROCESO_NO_SE_PUDO_COMPLETAR, MSG_PROCESO_NO_SE_PUDO_COMPLETAR, MSG_ERROR_ACTUALIZAR, MB_ICONERROR);
                btnProcesar.Enabled := True;
            end;
        end;
    finally
        FProcesando := False;
    end;
end;

procedure TFrmCancelarFacturacionMasiva.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    spObtenerOperacionesFacturacionMasiva.Close;
    Action := caFree;
end;

procedure TFrmCancelarFacturacionMasiva.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: TFrmCancelarFacturacionMasiva.buscaOperacionesProcess
  Author:    mtraversa
  Date Created: 29/12/2004
  Description: Para cada proceso se muetra Fecha de realzacion y usuario que lo
               realizo
  Parsmeters: Tabla: TDataSet; var Texto: String
  Return Value:Boolean
-----------------------------------------------------------------------------}
function TFrmCancelarFacturacionMasiva.buscaOperacionesProcess(
  Tabla: TDataSet; var Texto: String): Boolean;
begin
	Result := True;
	with Tabla do
    	Texto := Format('Fecha Realizaci�n: %s  -  Usuario: %s - %d Comprobantes Procesados',
                        [FieldByName('Fecha').AsString, FieldByName('Usuario').AsString, FieldByName('Cantidad').asInteger]);

end;

procedure TFrmCancelarFacturacionMasiva.buscaOperacionesSelect(
  Sender: TObject; Tabla: TDataSet);
begin
    with Tabla do begin
        FCodigoOperacionInterfase := FieldByName('CodigoOperacionInterfase').AsInteger;
        edProceso.Text := FieldByName('Fecha').AsString;
    end;
    btnProcesar.Enabled := True;
end;

procedure TFrmCancelarFacturacionMasiva.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

end.
