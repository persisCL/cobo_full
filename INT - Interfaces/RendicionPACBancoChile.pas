{-------------------------------------------------------------------------------
File Name   : RendicionPACBancoChile.pas
Author      : jjofre
Date Created:  20/05/2010
Description : M�dulo de la interface Banco Chile - Recepci�n de Rendiciones

 Revision : 1
     Author : jjofre
     Date : 28 de mayo de 2010
     Description : - se modifica la funcion ParseLine para adaptarlo a nuevo
                    requerimiento de banco de chile.
                - se modifica AnalizarRendicionesTXT para el nuevo largo de linea

  Revision : 2
     Author : jjofre
     Date : 2 de Junio de 2010
     Description :  - se modifica el record RecordRendicionPACBC; EstadoDebito a string
                    - se modifica la funcion ParseLine para adaptarlo el EstadoDebito
                    alfanumerico.
  Revision : 3
     Author : jjofre
     Date : 14 de Junio de 2010
     Descripcion : se modifican las siguientes funciones para para validar los
                registros de control, nombres de archivos.
                -Inicializar
                -ParseLine
                -AnalizarRendicionesTXT
                -btnProcesarClick
  Revision : 4
     Author : jjofre
     Date : 06 de Julio de 2010
     Descripcion : se modifican las siguientes funciones para para validar los
                registros de control, nombres de archivos.
                -AnalizarRendicionesTXT


Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

Autor       :   Claudio Quezada
Fecha       :   06-02-2015
Firma       :   SS_1147_CQU_20150316
Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato
                Se relizan correcciones segun lo indicado por PVergara de VespucioSur
                El Tipo de Comprobante y el Numero de Comprobante no vienen
                por lo cual se obtiene el �ltimo comprobante impago del convenio.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Autor       :   Claudio Quezada
Fecha       :   20-04-2015
Firma       :   SS_1147_CQU_20150420
Descripcion :   Se corrige un par de posiciones
                Se obtiene el primer comprobante impago para asociar a la rendicion si no existe
                se busca el ultimo comprobante pagado.
                Se corrige el TOP del boton Aceptar (dfm)
                Se corrige un error al seleccionar otro archivo, siempre concatenaba el archivo anterior.

Autor       :   Claudio Quezada
Fecha       :   11-05-2015
Firma       :   SS_1147_CQU_20150511
Descripcion :   BancoChile, para Vespucio, no envia el numero de comprobante por ello que en Delphi se hacia
                la obtencion del comprobante en base al numero de convenio. Dicha validacion no estaba funcionando
                ya que el error se limpia antes de salir de la funcion que parsea. Dado lo anterior es que se cambio a SQL
                la obtencion del comprobante, esto permite controlar mejor el error y evitar los descuadres en el reporte.
-------------------------------------------------------------------------------}
unit RendicionPACBancoChile;

interface

uses
 DMConnection,
 Util,
 Utildb,
 UtilProc,
 StrUtils,
 ConstParametrosGenerales,
 PeaProcs,
 ComunesInterfaces,
 FrmRptRecepcionRendiciones,
 Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
 Dialogs, StdCtrls, DB, ADODB, ComCtrls, ExtCtrls, Buttons, PeaTypes,
 DPSControls,  ppParameter,ppCtrls, ppBands, ppReport, ppPrnabl, ppClass,
 ppStrtch, ppSubRpt, ppCache, ppProd, UtilRB, ppDB, ppComm, ppRelatv,
 ppTxPipe, RBSetup;


type

  //Registro de Rendici�n
  RecordRendicionPACBC = record
    NumeroComprobante: int64;
    EstadoDebito: string;
    TotalAPagar: int64;
    CodigoTipoMedioPago: string;
    CodigoBancoSBEI: smallint;
    PAC_CodigoBanco: integer;
    NroCuentaBancaria: string;
    PAC_CodigoTipoCuentaBancaria: word;
    MontoFijo: int64;
    RutSubscriptor: string;
    PAC_Sucursal: string;
    NumeroConvenio: string;
    FechaDebito: TDateTime;
    FechaDebitoFormateada: TDateTime;
    DescripcionErrorValidacion: string;
    CodigoConceptoPago: integer;
    iTotalMontoAceptado: int64;
    iTotalMontoRechazado: int64;
    iTotalCantidadAceptados: int64;
    iTotalCantidadRechazados: int64;
    TipoComprobante : string;   // SS_1147_CQU_20150420
  end;


  TfrmRendicionPACBancoChile = class(TForm)
    btnAbrirArchivo: TSpeedButton;
    Label2: TLabel;
    edOrigen: TEdit;
    OpenDialog: TOpenDialog;
    spProcesarRendicion: TADOStoredProc;
    spRegistrarOperacion: TADOStoredProc;
    spActualizaObservacionesLOG: TADOStoredProc;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    Bevel1: TBevel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    SpObtenerResumenComprobantesRecibidos: TADOStoredProc;
    SpObtenerResumenComprobantesRecibidosMotivo: TStringField;
    SpObtenerResumenComprobantesRecibidosCantidad: TIntegerField;
    SpObtenerResumenComprobantesRecibidosMonto: TStringField;
    spAgregarRendicionPACBC: TADOStoredProc;
    procedure btnSalirClick(Sender: TObject);
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    private
    { Private declarations }
      FCodigoOperacion: integer;
      FCancelar: Boolean;
      iTotalLines: Integer;   //Total de lineas en el archivo
      iInvalidLines: Integer; //Lineas Con Debitos Invalidos
      //REV.3
      FBancoChile_Codigo_Convenio: AnsiString;
      FBancoChile_Codigo_Empresa: AnsiString;
      FBancoChile_Codigo_Convenio_aux: AnsiString;
      FBancoChile_Codigo_Empresa_aux: AnsiString;
      FErrores: TStringList;
      txtRendicionesPAC: TStringList;
      FBancoChile_Directorio_Rendiciones: AnsiString;
      FBancoChile_Directorio_Errores : AnsiString;
      FBCO_DirectorioRendicionesProcesadas: AnsiString;
      FLineasArchivo: integer;
      FMontoArchivo: int64;
      FCodigoNativa : Integer;  // SS_1147_CQU_20150316
      FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
      FArchivoDestino : AnsiString; // SS_1147_CQU_20150420
      function  ParseLine(sLine: string; var RecRendiPAC: RecordRendicionPACBC; NroLinea:integer): String;
      function  AnalizarRendicionesTXT : boolean;
      function  RegistrarOperacion(var CodigoOperacion: integer): boolean;
      Function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
    public
    { Public declarations }
      function Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
  end;

var
  frmRendicionPACBancoChile: TfrmRendicionPACBancoChile;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
 Function Name: Inicializar
 Author:  jjofre
 Date Created: 20/05/2010
 Description: Inicializaci�n de este formulario
-----------------------------------------------------------------------------}
function TfrmRendicionPACBancoChile.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;


    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : jjofre
    Date Created : 20/05/2010
    Description : Se obtienen los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        BANCOCHILE_DIRECTORIO_RENDICIONES    = 'BancoChile_Directorio_Rendiciones';
        BANCOCHILE_DIRECTORIO_ERRORES        = 'BancoChile_Directorio_Errores';
        BCO_DIRECTORIORENDICIONESPROCESADAS  = 'BCO_DirectorioRendicionesProcesado';
        BANCODECHILE_CODIGO_CONVENIO         = 'BancoChile_Codigo_Convenio';
        BANCODECHILE_CODIGO_EMPRESA          = 'BancoChile_Codigo_Empresa';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la Concesionaria Nativa                  // SS_1147_CQU_20150316
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150316

                //Se Obtiene el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BANCOCHILE_DIRECTORIO_RENDICIONES , FBancoChile_Directorio_Rendiciones) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BANCOCHILE_DIRECTORIO_RENDICIONES;
                    Result := False;
                    Exit;
                end;
                //Se Verifica que sea v�lido
                FBancoChile_Directorio_Rendiciones := GoodDir(FBancoChile_Directorio_Rendiciones);
                if  not DirectoryExists(FBancoChile_Directorio_Rendiciones) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FBancoChile_Directorio_Rendiciones;
                    Result := False;
                    Exit;
                end;

                //Se Obtiene el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BANCOCHILE_DIRECTORIO_ERRORES , FBancoChile_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BANCOCHILE_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Se Verifica que sea v�lido
                FBancoChile_Directorio_Errores := GoodDir(FBancoChile_Directorio_Errores);
                if  not DirectoryExists(FBancoChile_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FBancoChile_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Se Obtiene el Parametro General
                ObtenerParametroGeneral(DMConnections.BaseCAC, BCO_DIRECTORIORENDICIONESPROCESADAS, FBCO_DirectorioRendicionesProcesadas);

                 //Se obtiene el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BANCODECHILE_CODIGO_CONVENIO , FBancoChile_Codigo_Convenio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BANCODECHILE_CODIGO_CONVENIO;
                    Result := False;
                    Exit;
                end;
                //Se Verifica que sea v�lido
                if not (FBancoChile_Codigo_Convenio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + BANCODECHILE_CODIGO_CONVENIO;
                   Result := False;
                   Exit;
                end;

                //Se obtiene el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BANCODECHILE_CODIGO_EMPRESA , FBancoChile_Codigo_Empresa) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BANCODECHILE_CODIGO_EMPRESA;
                    Result := False;
                    Exit;
                end;
                //Se Verifica que sea v�lido
                if not (FBancoChile_Codigo_Empresa <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + BANCODECHILE_CODIGO_EMPRESA;
                   Result := False;
                   Exit;
                end;

                FBancoChile_Codigo_Convenio_aux := PadL(FBancoChile_Codigo_Convenio,3,'0');
                FBancoChile_Codigo_Empresa_aux := PadL(FBancoChile_Codigo_Empresa,3,'0');

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            if (Result = False) then begin
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_ERROR_INIT = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    if not MDIChild then begin
  		FormStyle := fsNormal;
  		Visible := False;
  	end;

    CenterForm(Self);
    try
        Result := DMConnections.BaseCAC.Connected and
                          VerificarParametrosGenerales;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;

    Caption := AnsiReplaceStr(txtCaption, '&', '');
   	pnlAvance.Visible := False;
    lblReferencia.Caption := '';
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : jjofre
Date Created : 17/05/2010
Description : Mensaje de Ayuda
*******************************************************************************}
procedure TfrmRendicionPACBancoChile.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_RENDICION          = ' ' + CRLF +
                          'El Archivo de Respuesta a Debitos' + CRLF +
                          'es enviado por BANCO DE CHILE al ESTABLECIMIENTO' + CRLF +
                          'por cada Archivo de Debitos recibido' + CRLF +
                          'y tiene por objeto dar a conocer al Establecimiento, ' + CRLF +
                          'el resultado del procesamiento de la informaci�n enviada.' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: PACR_CEM_YYYY-MM-DD.asc' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para registrar los pagos de los' + CRLF +
                          'comprobantes aceptados en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';
begin
    if PnlAvance.Visible = True then Exit;
    MsgBoxBalloon(MSG_RENDICION, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : jjofre
Date Created : 17/05/2010
Description :
*******************************************************************************}
procedure TfrmRendicionPACBancoChile.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
 Function Name: btnAbrirArchivoClick
 Author:  jjofre
 Date Created: 17/05/2010
 Description: Busca el Archivo del Rendiciones PACR_CEM
-----------------------------------------------------------------------------}
procedure TfrmRendicionPACBancoChile.btnAbrirArchivoClick(Sender: TObject);
resourcestring
  	MSG_FILE_ALREADY_PROCESSED 			  = 'El archivo %s ya fue procesado';
    MSG_FILE_WISH_TO_REPROCESS			  = '�Desea volver a procesarlo?';
  	ERROR_RENDERINGS_FILE_NOT_EXISTS 	= 'El archivo de rendiciones %s no existe';
    MSG_ERROR = 'Error';
begin
    //Define un directorio de origen
    opendialog.InitialDir:=FBancoChile_Directorio_Rendiciones;

  	if opendialog.execute then begin
        edOrigen.text:= opendialog.filename;

        if FileExists( edOrigen.text ) then begin
            btnProcesar.Enabled := True;
        end else begin
            btnProcesar.Enabled := False;
            Exception.Create( Format ( ERROR_RENDERINGS_FILE_NOT_EXISTS, [edOrigen.text]));
        end;

        //Verifica que el nombre del archivo procesado sea correcto
        if  VerificarArchivoProcesado( DMConnections.BaseCAC, edOrigen.text ) then begin
			  if (MsgBox (Format (MSG_FILE_ALREADY_PROCESSED, [ExtractFileName (edOrigen.text) ]) + #13#10 + MSG_FILE_WISH_TO_REPROCESS, MSG_ERROR, MB_ICONQUESTION or MB_YESNO) <> IDYES) then
            Exit;
        end;
        FArchivoDestino := GoodDir(FBCO_DirectorioRendicionesProcesadas) + ExtractFileName(edOrigen.text);                                                                                  // SS_1147_CQU_20150420
        //if FBCO_DirectorioRendicionesProcesadas <> '' then begin                                                                                                                          // SS_1147_CQU_20150420
        //    if RightStr(FBCO_DirectorioRendicionesProcesadas,1) = '\' then  FBCO_DirectorioRendicionesProcesadas := FBCO_DirectorioRendicionesProcesadas + ExtractFileName(edOrigen.text) // SS_1147_CQU_20150420
        //    else FBCO_DirectorioRendicionesProcesadas := FBCO_DirectorioRendicionesProcesadas + '\' + ExtractFileName(edOrigen.text);                                                     // SS_1147_CQU_20150420
        //end;                                                                                                                                                                              // SS_1147_CQU_20150420
    end;
end;

{-----------------------------------------------------------------------------
 Function Name: ParseLine
 Author:  jjofre
 Date Created: 17/05/2010
 Description: Parsea la linea de entrada y almacena los datos en un registro
-----------------------------------------------------------------------------}
Function TfrmRendicionPACBancoChile.ParseLine(sLine: string; var RecRendiPAC: RecordRendicionPACBC; NroLinea:Integer): String;
resourcestring
    ERROR_PARSING          =  'Error de Parseo...';
    ERROR_SAVING_LINE      =  'Error al Asentar la Operaci�n en Linea: %d. ';
    ERROR_CRITICAL         =  'Valor inv�lido: ';
    STR_BANK_CODE          =  'C�digo de Banco';
    STR_AMOUNT             =  'Importe del D�bito';
    STR_AGREEMENT_NUMBER   =  'N�mero de Convenio';
    STR_DEBIT_STATE        =  'Estado del D�bito';
    STR_INVOICE_NUMBER     =  'Numero de Comprobante';
    STR_DEBIT_DATE         =  'Fecha de D�bito';
    MSG_EMPRESA_ERROR 		    = 'El c�digo de La Empresa es inv�lido.';
    MSG_CONVENIO_ERROR          = 'El c�digo de Convenio es inv�lido.';
    //SQL_NUMERO_COMPROBANTE =   'SELECT dbo.ObtenerUltimoComprobanteImpago(dbo.ObtenerCodigoConvenio(''%s''), ''NK'')';                                        // SS_1147_CQU_20150420
    SQL_NUMERO_COMPROBANTE        =   'SELECT dbo.ObtenerPrimerComprobanteImpago(dbo.ObtenerCodigoConvenio(dbo.QuitarCerosIzquierdaTarjeta(''%s'')), ''%s'')';  // SS_1147_CQU_20150420
    SQL_NUMERO_COMPROBANTE_PAGADO =   'SELECT dbo.ObtenerUltimoComprobantePagado(dbo.ObtenerCodigoConvenio(dbo.QuitarCerosIzquierdaTarjeta(''%s'')), ''%s'')';  // SS_1147_CQU_20150420
    MSG_INVOICE_ERROR             =  'No se encontr� Comprobante para Asociar, Numero de Convenio ''%s''';                                                      // SS_1147_CQU_20150420

Var
  	Rta    : string;
    sFecha : string;
    Flag : byte;
    //NumeroDocumento : Int64;    // SS_1147_CQU_20150420 //SS_1147_CQU_20150316
Begin
    Rta := ERROR_PARSING;
    try

    With RecRendiPAC do begin
            CodigoBancoSBEI := 0;
            MontoFijo := 0;
            NumeroConvenio := '';
            EstadoDebito := '';
            NumeroComprobante := 0;
            FechaDebito := nulldate;
            CodigoTipoMedioPago := '';
            PAC_CodigoBanco:=0;
            PAC_CodigoTipoCuentaBancaria := 0;
                                                                                                                                                                                                                                                                                                                                                                                                                         PAC_Sucursal := '';
            FechaDebitoFormateada := Now;
            DescripcionErrorValidacion := '';
            TotalAPagar := 0;
        end;
        Flag := 0;
        try
            With RecRendiPAC  do begin
                //Valido el Codigo Empresa
                if ( (Trim(Copy(sline,4,3))) <> FBancoChile_Codigo_Empresa_aux ) then begin
                    Rta := MSG_EMPRESA_ERROR;
                    Exit;
                end;

                //Valido el Codigo Convenio
                if ( (Trim(Copy(sline,7,3))) <> FBancoChile_Codigo_Convenio_aux) then begin
                    Rta := MSG_CONVENIO_ERROR;
                    Exit;
                end;

                inc(Flag);
                CodigoBancoSBEI := strToInt(trim(copy(sLine, 1, 3)));
                inc(Flag);
                MontoFijo := strtoint64(trim(copy(sLine, 34, 11)));
                inc(Flag);
                //NumeroConvenio := '001' + trim(copy(sLine, 12, 14));                                                                          // SS_1147_CQU_20150420 //REV. 1
                if FCodigoNativa = CODIGO_VS then                                                                                               // SS_1147_CQU_20150420
                    NumeroConvenio := trim(copy(sLine, 12, 14))                                                                                 // SS_1147_CQU_20150420
                else                                                                                                                            // SS_1147_CQU_20150420
                    NumeroConvenio := '001' + trim(copy(sLine, 12, 14));                                                                        // SS_1147_CQU_20150420
                inc(Flag);
                EstadoDebito := trim(copy(sLine, 61, 3));
                inc(Flag);
                //NumeroComprobante:=StrToInt64(Trim(copy(sLine,65,10)));                                                                                       // SS_1147_CQU_20150420 // REV. 1
                if FCodigoNativa = CODIGO_VS then begin                                                                                                         // SS_1147_CQU_20150420
                    // Podria venir el numero comprobante en la rendicion                                                                                       // SS_1147_CQU_20150420
                    if not (Trim(copy(sLine,65,10)) = '') then NumeroComprobante := StrToInt64(Trim(copy(sLine,65,10)));                                        // SS_1147_CQU_20150420
                    {   ESTA VALIDACION SE TRASPASA AL PROCEDIMIENTO ALMACENADO PARA PODER REGISTRAR EL ERROR YA QUE ACA NO FUNCIONA  }                         // SS_1147_CQU_20150511
                    //// Si no viene busco un comprobante                                                                                                       // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //if NumeroComprobante = 0 then begin                                                                                                       // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    // Busco la Primera SD Impaga (Las mas antigua)                                                                                       // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    TipoComprobante := 'SD';                                                                                                              // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    NumeroComprobante := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_NUMERO_COMPROBANTE, [NumeroConvenio, TipoComprobante]));      // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //end;                                                                                                                                      // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //if NumeroComprobante = 0 then begin                                                                                                       // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    // No hay SD vieja impaga, busco la Primera NK Impaga (Las mas antigua)                                                               // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    TipoComprobante := 'NK';                                                                                                              // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    NumeroComprobante := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_NUMERO_COMPROBANTE, [NumeroConvenio, TipoComprobante]));      // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //end;                                                                                                                                      // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //if NumeroComprobante = 0 then begin                                                                                                       // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    // No hay SD ni NK vieja impaga, busco la ultima SD pagada (Las mas nueva)                                                            // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    TipoComprobante := 'SD';                                                                                                              // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    NumeroComprobante := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_NUMERO_COMPROBANTE_PAGADO, [NumeroConvenio, TipoComprobante]));// SS_1147_CQU_20150511 // SS_1147_CQU_20150420
                    //end;                                                                                                                                      // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //if NumeroComprobante = 0 then begin                                                                                                       // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    // No hay SD pagada, busco la ultima NK pagada (Las mas nueva)                                                                        // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    TipoComprobante := 'NK';                                                                                                              // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    NumeroComprobante := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_NUMERO_COMPROBANTE_PAGADO, [NumeroConvenio, TipoComprobante]));// SS_1147_CQU_20150511 // SS_1147_CQU_20150420
                    //end;                                                                                                                                      // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //if NumeroComprobante = 0 then begin                                                                                                       // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    // No hay comprobante para asociar, error                                                                                             // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    Rta := Format(MSG_INVOICE_ERROR, [NumeroConvenio]);                                                                                   // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //    TipoComprobante := '';                                                                                                                // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                    //end;                                                                                                                                      // SS_1147_CQU_20150511  // SS_1147_CQU_20150420
                end else begin                                                                                                                                  // SS_1147_CQU_20150420
                    NumeroComprobante:=StrToInt64(Trim(copy(sLine,65,10)));                                                                                     // SS_1147_CQU_20150420
                    TipoComprobante := 'NK';                                                                                                                    // SS_1147_CQU_20150420
                end;                                                                                                                                            // SS_1147_CQU_20150420
                inc(Flag);
    		      	sFecha := trim(copy(sLine, 53, 8));
			        	if ( sFecha = '00000000' ) then FechaDebito := now else FechaDebito := EncodeDate(	StrToInt(Copy(sFecha, 1, 4)), StrToInt(Copy(sFecha, 5, 2)), StrToInt(Copy(sFecha, 7, 2)));

                //Suma el Monto y la cantidad de pagos Aceptados
                //asi luego compararlos con el registro de totales de la Rendicion
                if (EstadoDebito='300') then begin
                    iTotalMontoAceptado:= iTotalMontoAceptado + MontoFijo;
                    inc(iTotalCantidadAceptados);
                end else begin
                    iTotalMontoRechazado:= iTotalMontoRechazado + MontoFijo;
                    Inc(iTotalCantidadRechazados);
                end;

            end;
            Rta := '';
        except
            on e : Exception do begin
                Rta := ERROR_CRITICAL;
                Case Flag of
                    1: Rta := Rta + STR_BANK_CODE  ;
                    2: Rta := Rta + STR_AMOUNT  ;
                    3: Rta := Rta + STR_AGREEMENT_NUMBER  ;
                    4: Rta := Rta + STR_DEBIT_STATE  ;
                    5: Rta := Rta + STR_INVOICE_NUMBER ;
                    6: Rta := Rta + STR_DEBIT_DATE  ;
                else
                    Rta := e.Message;
                end;
		    end;
        end;
    finally
	    ParseLine := Rta;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: AnalizarRendicionesTXT
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Hace el Parseo para verificar que el archivo sea v�lido
-----------------------------------------------------------------------------}
function TfrmRendicionPACBancoChile.AnalizarRendicionesTXT : boolean;
resourcestring
	  MSG_REF_ANALYZING_LINE            = 'Analizando linea %d';
	  MSG_ERROR_LINE                    = 'Error de formato del Archivo - L�nea: %d '#10#13;
      MSG_ERROR_LINE_REGISTRO_TOTALES   = 'Registro de totales No cuadra con archivo ';
      MSG_NO_TIENE_CANTIDAD_CARACTERES = 'La linea no tiene la cantidad de caracteres que corresponde, Linea: ';

const
    CANTIDAD_CARACTERES_VALIDACION = 100 ;    //REV.4

var
    rRPAC: RecordRendicionPACBC;
    DescError,ParseResult,Totalresult : string;
    NroLineaProc: integer;

begin

	result := True;
    FMontoArchivo := 0;
    pbProgreso.Max := txtRendicionesPAC.Count-1;
	pbProgreso.Position := 0;
	NroLineaProc := 0;
    Totalresult:= ' ';
	While ( NroLineaProc < txtRendicionesPAC.Count-1) and not FCancelar do Begin
        Application.ProcessMessages;
        pbProgreso.StepIt;
        lblReferencia.Caption := Format(MSG_REF_ANALYZING_LINE, [NroLineaProc+1]);

        if Length(Trim( txtRendicionesPAC.strings[NroLineaProc])) <> CANTIDAD_CARACTERES_VALIDACION then begin    //REV.4
            MsgBox(MSG_NO_TIENE_CANTIDAD_CARACTERES + IntToStr(NroLineaProc + 1) , Caption, MB_ICONERROR);
            result := False;
            ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,MSG_NO_TIENE_CANTIDAD_CARACTERES + ' Linea: ' + IntToStr(NroLineaProc + 1),DescError);
            Break;
        end;

        ParseResult := ParseLine(txtRendicionesPAC.strings[NroLineaProc], rRPAC, NroLineaProc+1);
        if not (ParseResult = '') then begin
            MsgBox( Format( MSG_ERROR_LINE + ParseResult,[(NroLineaProc+1)] ),Caption,MB_ICONERROR);
            result := False;
            ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,Format( MSG_ERROR_LINE + ParseResult,[(NroLineaProc+1)]),DescError);
            Break;
        end
        else FMontoArchivo := FMontoArchivo + rRPAC.MontoFijo;
        Inc(NroLineaProc);
    end;
    FLineasArchivo := NroLineaProc;
    //Se validan los totales del registro de control
    if (rRPAC.iTotalMontoAceptado <> StrToInt64(Copy(txtRendicionesPAC[txtRendicionesPAC.Count-1],25,13))) then  begin
        Totalresult:= 'Total Monto Aceptado diferente a monto total registros; '
    end;

    if (rRPAC.iTotalCantidadAceptados <> StrToInt64(Copy(txtRendicionesPAC[txtRendicionesPAC.Count-1],19,6))) then begin
        Totalresult:= Totalresult + 'Total Cantidad registros Aceptados diferente a registros aceptados; ';
    end;

    if (rRPAC.iTotalMontoRechazado <> StrToInt64(Copy(txtRendicionesPAC[txtRendicionesPAC.Count-1],44,13))) then begin
        Totalresult:= Totalresult + 'Total Monto rechazado diferente a monto rechazado registros; ';
    end;

    if (rRPAC.iTotalCantidadRechazados <> StrToInt64(Copy(txtRendicionesPAC[txtRendicionesPAC.Count-1],38,6))) then begin
        Totalresult:= Totalresult + 'Total Cantidad registros rechazados diferente a registros rechazados';
    end;

    if (Trim(Copy(txtRendicionesPAC[txtRendicionesPAC.Count-1],4,3)) <> FBancoChile_Codigo_Empresa_aux ) then begin
        Totalresult:= Totalresult + 'Codigo de Empresa Inv�lido en Registro de Control';
    end;

    if (Trim(Copy(txtRendicionesPAC[txtRendicionesPAC.Count-1],7,3)) <> FBancoChile_Codigo_Convenio_aux ) then begin
        Totalresult:= Totalresult + 'Codigo de Convenio Inv�lido en Registro de Control';
    end;

    if Totalresult <> ' ' then begin
        MsgBox(Totalresult,Caption,MB_ICONERROR);
        ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,Totalresult,DescError);
        result := False;
    end;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                        end;

{-----------------------------------------------------------------------------
 Function Name: RegistrarOperacion
 Author:  jjofre
 Date Created: 20/05/2010
 Description: Registra la operacion en el log de interfaces
-----------------------------------------------------------------------------}
function TfrmRendicionPACBancoChile.RegistrarOperacion(var CodigoOperacion: integer):boolean;
resourcestring
    MSG_RESULT = '( %d / %d )';
    MSG_INIT = 'Proceso Iniciado ( %s ) - ';
const
    RO_MOD_INTERFAZ_RENDICIONES_PAC_BC = 104 ;
var
    sDescrip : string;
    rta: boolean;
    CodOperacion: integer;
    DescError: string;
begin
    rta := False;
    if CodigoOperacion = 0 then begin
        sDescrip := Format( MSG_INIT, [DateToStr(now)]);
        rta := RegistrarOperacionEnLogInterface(dmconnections.BaseCAC, RO_MOD_INTERFAZ_RENDICIONES_PAC_BC, ExtractFileName(edOrigen.text), UsuarioSistema, sDescrip, True, false, NowBase(DMConnections.BaseCAC), 0, CodOperacion, FLineasArchivo, (FMontoArchivo div 100), DescError);
        CodigoOperacion := CodOperacion;
    end
    else begin
        sDescrip := Format( MSG_RESULT, [iTotalLines, iInvalidLines]);
        ActualizarObservacionesEnLogInterface(DMCOnnections.BaseCAC , spActualizaObservacionesLOG , CodigoOperacion, sDescrip, True);
    end;
    RegistrarOperacion := rta;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Genera el Reporte de Finalizacion de Proceso.
-----------------------------------------------------------------------------}
Function  TfrmRendicionPACBancoChile.GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
Const
    STR_TITLE = 'Comprobantes Recibidos';
var
    FRecepcion : TFRptRecepcionRendiciones;
begin
    Result:=false;
    try
        Application.CreateForm(TFRptRecepcionRendiciones, FRecepcion);
        if not fRecepcion.Inicializar(STR_TITLE, CodigoOperacionInterfase) then fRecepcion.Release;
        Result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
 Function Name: btnProcesarClick
 Author:  jjofre
 Date Created: 20/05/2010
 Description: Procesa el pago de las rendiciones.
-----------------------------------------------------------------------------}
procedure TfrmRendicionPACBancoChile.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    jjofre
      Date Created: 20/05/2010
      Description: Se obtienen la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name:  ObtenerResumen
      Author:    jjofre
      Date Created: 20/05/2010
      Description:  Se Obtiene el resumen del proceso realizado
    -----------------------------------------------------------------------------}
    procedure ObtenerResumen (CodigoOperacionInterfase : Integer;Var CantRegs, Aprobados, Rechazados : Integer);
    resourcestring
      MSG_ERROR = 'Error';
      MSG_ERROR_QUERY = 'Error Obteniendo Resumen';
    begin
        try
            With spObtenerResumenComprobantesRecibidos, Parameters do begin
                Close;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                CommandTimeout := 500;
                Open;
                CantRegs := Parameters.ParamByName('@CantidadComprobantesRecibidos').Value;
                Aprobados := Parameters.ParamByName('@CantidadComprobantesAceptados').Value;
                Rechazados := Parameters.ParamByName('@CantidadComprobantesRechazados').Value;
                Close;
            end;
        except
            on E : Exception do begin
                MsgBoxErr(MSG_ERROR_QUERY, E.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    jjofre
      Date Created: 20/05/2010
      Description: Se Actualiza el log al finalizar
   -----------------------------------------------------------------------------}
   Function ActualizarLog(CantidadErrores:integer):boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : string;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on e : Exception do begin
                MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
                Result := False;
            end;
        end;
   end;

    {-----------------------------------------------------------------------------
                GrabarErrores

    Author      :   CQuezadaI
    Date        :   11-Mayo-2015
    Firma       :   SS_1147_CQU_20150511
    Description :   Grabar los errores encontrados en el procesmiento de la interfaz

    -----------------------------------------------------------------------------}
    procedure GrabarErrores(FErrores: TStringList);
    var
        i : Integer;
    begin
        for i := 0 to FErrores.Count - 1 do begin
            AgregarErrorInterfase( DMConnections.BaseCAC, FCodigoOperacion, FErrores[i], -1);
        end;

    end;

resourcestring
    MSG_ERROR_FILE_NOT_EXISTS	               = 'El archivo %s no existe';
    MSG_ERROR_CANNOT_OPEN_FILE 	               = 'No se puede abrir el archivo %s';
    MSG_ERROR_LINE                             = 'Error al Importar L�nea: %d - ';
    MSG_ERROR_IMPORTING_LINE                   = 'Error al Importar L�nea: %d - Comprobante %d , ';
    MSG_ERROR_INVALID_DATE                     = 'Error al Importar L�nea: %d - Fecha Invalida. Reemplazada por fecha actual ';
    MSG_ERROR_VALIDATION                       = 'Error de Validaci�n L�nea: %d - Comprobante %d , ';
    MSG_ERROR_SAVING_INVOICE                   = 'Error al Asentar la Operaci�n Comprobante: %d  ';
    MSG_PROCESSING_FILE 		               = 'Procesando Archivo de Rendiciones - Cantidad de lineas : %d';
    MSG_PROCESSING				               = 'Procesando...';
    MSG_PROCESS_ENDS_OK                        = 'El proceso finaliz� con �xito';
    MSG_PROCESS_ENDS_WITH_ERRORS               = 'El proceso finaliz� con errores';
    MSG_PROCESS_CANNOT_COMPLETE                = 'El proceso no se pudo completar';
    MSG_PROCESS_USER_CANCELED                  = 'El Proceso ha sido Cancelado por el Usuario';
    MSG_REF_STARTING                           = 'Iniciando el proceso de importaci�n del archivo %s';
    MSG_REF_ANALYZING_LINE                     = 'Analizando linea %d';
    MSG_REF_VALIDATING                         = 'Validando valores importados';
    MSG_REF_REGISTERING_PAYMENT                = 'Registrando Informaci�n del Pago...';
    MSG_REF_REGISTERING_ERROR                  = 'Registrando Informaci�n de Error ';
    MSG_ERROR_RENDICION_NAME_FILE            = 'El nombre del archivo contiene un c�digo de Empresa inv�lido ';
    MSG_ERRROR_RENDICION_FILE_IS_EMPTY 		= 'El archivo de rendiciones seleccionado est� vac�o';
const
    CONST_TIPO_COMPROBANTE_DEBITO = 'NK';
var
    rRPAC: RecordRendicionPACBC;
    NroLineaProc: integer;
  	DescError,sDescripcionError: String;
    FErrorGrave : boolean;
    CantidadErrores : integer;
    CantRegs, Aprobados, Rechazados : Integer;

    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
begin
    FErrores := TStringList.Create;

    FErrores.Clear;

    btnProcesar.Enabled := False;
    btnCancelar.Enabled := True;
    pnlAvance.Visible := True;
    try
        lblReferencia.Caption := Format(MSG_REF_STARTING, [edOrigen.text]);
        Screen.Cursor := crHourglass;

		//Lee el archivo de Rendiciones ...
		try
            txtRendicionesPAC := TStringList.Create;
            txtRendicionesPAC.LoadFromFile(edOrigen.text);
		except
            on E : Exception do begin
                MsgBoxErr(Format(MSG_ERROR_CANNOT_OPEN_FILE, [edOrigen.text]), e.Message, 'Error', MB_ICONERROR);
                Screen.Cursor := crDefault;
                Exit;
            end;
		end;

		NroLineaProc := 0;
		FCancelar := False;
        FCodigoOperacion := 0;
		pbProgreso.Max := txtRendicionesPAC.Count;
		pbProgreso.Position := 0;
		iTotalLines:=txtRendicionesPAC.Count;
		iInvalidLines := 0;
		pbProgreso.Step := 1;
        FErrorGrave := False;
        FLineasArchivo := iTotalLines - 1;

        //Registro la operaci�n en el log
        RegistrarOperacion(FCodigoOperacion);

        if FCodigoNativa <> CODIGO_VS then begin   // SS_1147_CQU_20150316
        //Valida el Codigo de Empresa del nombre del archivo
        if (Trim(Copy(ExtractFileName(edOrigen.text),6,3)) <> FBancoChile_Codigo_Empresa_aux ) then begin
        //Muestra un cartel indicando que el nombre del archivo no contiene el codigo de convenio Valido
            MsgBox(MSG_ERROR_RENDICION_NAME_FILE, Caption, MB_ICONERROR);
            ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOperacion,MSG_ERROR_RENDICION_NAME_FILE,DescError);
            Exit;
        end;
        end;                                       // SS_1147_CQU_20150316

        if ( txtRendicionesPAC.Count = 0 ) then begin
            //Muestra un cartel indicando que el archivo esta vacio
            MsgBox(MSG_ERRROR_RENDICION_FILE_IS_EMPTY, Caption, MB_ICONERROR);
            ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOperacion,MSG_ERRROR_RENDICION_FILE_IS_EMPTY,DescError);
            Exit;
        end;


        // Hace el Parseo y si �ste funciona ok, lo procesa
		if AnalizarRendicionesTXT then begin

            //Realiza la operacion de registrar los pagos
            While ( NroLineaProc < txtRendicionesPAC.Count-1) and not FCancelar do Begin
                Application.ProcessMessages;
                pbProgreso.StepIt;
                lblReferencia.Caption := Format(MSG_REF_ANALYZING_LINE, [NroLineaProc+1]);

                ParseLine(txtRendicionesPAC.strings[NroLineaProc], rRPAC, NroLineaProc+1);

                lblReferencia.Caption := MSG_REF_REGISTERING_PAYMENT;

                Reintentos  := 0;
                while Reintentos < 3 do begin

                    // Registra el pago del comprobante
                    with spAgregarRendicionPACBC, Parameters do begin

                        ParamByName('@CodigoOperacionInterfase').Value	:= FCodigoOperacion;
                        ParamByName('@CodigoBancoSBEI').Value  			:= rrpac.CodigoBancoSBEI;
                        ParamByName('@Monto').Value     				:= rrpac.MontoFijo;
                        ParamByName('@NumeroConvenio').Value   			:= trim(rrpac.NumeroConvenio);
                        ParamByName('@NumeroComprobante').Value 		:= rrpac.NumeroComprobante;
                        ParamByName('@FechaDebito').Value    			:= rrpac.FechaDebito;
                        ParamByName('@EstadoDebito').Value    			:= rrpac.EstadoDebito;
                        ParamByName('@Usuario').Value    				:= UsuarioSistema;
                        ParamByName('@Reintento').Value    				:= Reintentos;
                        ParamByName('@DescripcionError' ).Value         := '';                      // SS_1147_CQU_20150420
                        ParamByName('@TipoComprobante' ).Value          := rRPAC.TipoComprobante;   // SS_1147_CQU_20150420

                        try
                            CommandTimeOut := 500;
                            ExecProc;

                            sDescripcionError := Trim( VarToStr( ParamByName( '@DescripcionError' ).Value ));
                            if ( sDescripcionError <> '' ) then FErrores.Add( StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]));
                            //1/4 de segundo entre registracion de pago y otro
                            sleep(25);
                            Break;

                        except
                            on E : Exception do begin
                                if (pos('deadlock', e.Message) > 0) then begin
                                    mensajeDeadlock := e.message;
                                    inc(Reintentos);
                                    sleep(2000);
                                end else begin
                                    MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [rrpac.NumeroComprobante]), e.Message, caption, MB_ICONSTOP);
                                    ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOperacion,Format(MSG_ERROR_SAVING_INVOICE, [rrpac.NumeroComprobante]),DescError);
                                    FErrorGrave := True;
                                    Break;
                                end;
                            end;
                        end;
                    end;

                end;

                //en caso de excepcion de no se continua con proximo registro
                if FErrorGrave then Break;

                //una vez que se reintento 3 veces x deadlock se informa
                if Reintentos = 3 then begin
                    MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [rrpac.NumeroComprobante]), mensajeDeadLock, caption, MB_ICONSTOP);
                    FErrorGrave := True;
                    Break;
                end;

                INC(NroLineaProc);
            end;
            Screen.Cursor := crDefault;

            lblReferencia.Caption := '';
            pbProgreso.Position := 0;

            //Si el proceso no se cancelo o arrojo errores graves
            if ( not FCancelar ) and ( not FErrorGrave ) then begin

                GrabarErrores(FErrores);    // SS_1147_CQU_20150511

                //Se Obtienen la cantidad de errores
                ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);

                //Se Actualiza el log al Final
                ActualizarLog(CantidadErrores);

                //Se Obtiene el resumen
                ObtenerResumen (FCodigoOperacion, CantRegs, Aprobados, Rechazados);

                //Se muestra el cartel de finalizacion
                if MsgProcesoFinalizado( MSG_PROCESS_ENDS_OK, MSG_PROCESS_ENDS_WITH_ERRORS, Caption, CantRegs, Aprobados, Rechazados) then begin
                    //Se Muestra el Reporte de Finalizacion del Proceso
                    GenerarReportedeFinalizacion(FCodigoOperacion);
                end;

                //Se Mueve el archivo procesado a otro directorio
                //MoverArchivoProcesado(Caption,edOrigen.Text, FBCO_DirectorioRendicionesProcesadas);   // SS_1147_CQU_20150420
                MoverArchivoProcesado(Caption,edOrigen.Text, FArchivoDestino);                          // SS_1147_CQU_20150420
            end
            else if ( FCancelar ) then begin
                //Se Muestra el cartel que fue cancelado por el usuario
                MsgBox(MSG_PROCESS_USER_CANCELED);
            end else begin
                //Se informa que el proceso no pudo finalizar porque existieron errores
                MsgBox(MSG_PROCESS_CANNOT_COMPLETE);
            end;
        end;
	finally
        pnlAvance.Visible := False;
        pbProgreso.Position := 0;
        lblReferencia.Caption := '';
        Screen.Cursor := crDefault;
        FreeAndNil( txtRendicionesPAC );
        FreeAndNil(FErrores);
        btnProcesar.Enabled := False;
        btnCancelar.Enabled := False;
        FCancelar := False;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Permite cancelar el proceso
-----------------------------------------------------------------------------}
procedure TfrmRendicionPACBancoChile.btnCancelarClick(Sender: TObject);
begin
    FCancelar := True;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Cierro el formulario
-----------------------------------------------------------------------------}
procedure TfrmRendicionPACBancoChile.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Se libera de Memoria
-----------------------------------------------------------------------------}
procedure TfrmRendicionPACBancoChile.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
