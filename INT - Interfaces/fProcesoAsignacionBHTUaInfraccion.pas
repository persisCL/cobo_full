{********************************** Unit Header ********************************
File Name : fProcesoAsignacionBHTUaInfraccion.pas
Author : lgisuk
Date Created: 03/03/2006
Language : ES-AR
Description :  Obtiene los BHTU que no estan utilizados y para cada BHTU
               busca si hay una infraccion que coincida por patente y fecha.
               si es asi Asigna los BHTU a las infraccion. Adicionalmente anula
               las infracciones  y asigna los transitos afectados.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
*******************************************************************************}
unit fProcesoAsignacionBHTUaInfraccion;

interface

uses
    //Proceso de Asignacion BHTU a Infraccion
    DMConnection,
    PeaProcs,
    PeaTypes,
    ComunesInterfaces,
    Util,
    UtilProc,
    ConstParametrosGenerales,
    UtilDB,
    fReporteProcesoAsignacionBHTU,
    //Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, DateUtils, ppDB, ppDBPipe, UtilRB,
    ppCtrls, ppBands, ppClass, ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport, ppModule,
    raCodMod, ppParameter, Validate, DateEdit;

type
  TfrmProcesoAsignacionBHTUaInfraccion = class(TForm)
    btnSalir: TButton;
    btnProcesar: TButton;
    Bevel1: TBevel;
    spObtenerListaBHTU: TADOStoredProc;
    spAsignarBHTUAInfraccion: TADOStoredProc;
    spAsignarOtrasInfracciones: TADOStoredProc;
    spAsignarOtrosTransitos: TADOStoredProc;
    spObtenerTransitosCN: TADOStoredProc;
    spAsignarTransitoCNaBHTU: TADOStoredProc;
    spRegistrarProceso: TADOStoredProc;
    spCerrarProceso: TADOStoredProc;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    lbl: TLabel;
    Label2: TLabel;
    deFechaDesdeInfracciones: TDateEdit;
    Label3: TLabel;
    deFechaHastaInfracciones: TDateEdit;
    spObtenerInfraccionesAProcesarClearingBHTU: TADOStoredProc;
    pnlAvance: TPanel;
    Label1: TLabel;
    lblDetalle: TLabel;
    Label4: TLabel;
    pbProgreso: TProgressBar;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    deFechaDesdeBHTU: TDateEdit;
    deFechaHastaBHTU: TDateEdit;
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ImgAyudaClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
    FCancelar : Boolean;
    FPRocesando : Boolean;
    FCantidadAProcesar : Integer;
    FCodigoOperacion : Integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function  RegistrarOperacion : boolean;
    Function  ObtenerInfraccionesAProcesar(FechaDesde, FechaHasta : TDateTime) : Boolean;
    Function  ActualizarLog : Boolean;
    Function  ObtenerCantidadAProcesar : Boolean;
    Function  RegistrarResumenProceso(var CodigoOperacion : Integer; var Error : AnsiString) : Boolean;
    Function  CerrarResumenProceso(CodigoOperacion : Integer; Error : AnsiString) : Boolean;
    procedure MostrarReporte;
    procedure HabilitarBotones;
  public
    { Public declarations }
    function Inicializar(Titulo : AnsiString) : Boolean;
  end;

resourcestring
    MSG_BHTU_QRY  = '%d BHTUs Pendientes de Asignaci�n';

var
  frmProcesoAsignacionBHTUaInfraccion: TfrmProcesoAsignacionBHTUaInfraccion;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: Inicializar
Author : lgisuk
Date Created : 03/03/2006
Description :  Inicializaci�n de este formulario
Parameters : Titulo: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmProcesoAsignacionBHTUaInfraccion.Inicializar(Titulo : AnsiString) : Boolean;
//resourcestring
    //MSG_NO_BHTU_AVAILABLE   = 'No hay BHTU''s disponibles para asignar.';
begin
    //Result := False;
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    Caption := TRIM(Titulo);
    CenterForm(Self);
    FProcesando := False;
    {if not ObtenerCantidadAProcesar then Exit;
    if FCantidadAProcesar = 0 then begin
        MsgBox(MSG_NO_BHTU_AVAILABLE, Caption, MB_ICONWARNING);
        Exit;
    end;}
    HabilitarBotones;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 03/03/2006
Description :Muestra la ayuda de la ventana.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmProcesoAsignacionBHTUaInfraccion.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_AYUDA   = ' ' + CRLF +
                'El Proceso de Asignaci�n de BHTU' + CRLF +
                'permite al ESTABLECIMIENTO asignar los' + CRLF +
                'BHTU a las infracciones seleccionadas por '+ CRLF +
                'el ESTABLECIMIENTO, anulando las ' + CRLF +
                'infracciones y marcando los BHTU como utilizados.' + CRLF +
                ' '  + CRLF +
                'Solo se asignaran los BHTU seleccionados.' + CRLF +
                ' ';
begin
    //si esta procesando sale
    if FProcesando then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_AYUDA, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 03/03/2006
Description : Permite ver la ayuda si no esta procesando
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmProcesoAsignacionBHTUaInfraccion.ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState;X, Y: Integer);
begin
    if FProcesando then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{******************************** Function Header ******************************
Function Name: ObtenerCantidadAProcesar
Author : lgisuk
Date Created : 03/03/2006
Description :
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TfrmProcesoAsignacionBHTUaInfraccion.ObtenerCantidadAProcesar: Boolean;
resourcestring
    MSG_ERROR_CANT_OBTAIN_QRY = 'Error al obtener la cantidad a procesar.';
begin
    Result := False;
    try
        FCantidadAProcesar := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCantidadBHTUAProcesar(' + QuotedStr(FormatDateTime('YYYYMMDD',deFechaDesdeBHTU.Date)) + ',' + QuotedStr(FormatDateTime('YYYYMMDD',deFechaHastaBHTU.Date)) +')') ;
        lblDetalle.Caption := Format(MSG_BHTU_QRY, [ FCantidadAProcesar ]);
        Result := FCantidadAProcesar >= 0;
        if Result then begin
            pbProgreso.Max := FCantidadAProcesar * 10;
        end;
        pbProgreso.Min := 0;
        pbProgreso.Position := pbProgreso.Min;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_CANT_OBTAIN_QRY, E.Message, Caption, MB_ICONSTOP);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : lgisuk
Date Created : 03/03/2006
Description :
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmProcesoAsignacionBHTUaInfraccion.HabilitarBotones;
const
    STR_CANCEL = 'Cancelar';
    STR_QUIT = 'Salir';
begin
    FCancelar := False;
    lblDetalle.Caption := '';
    pbProgreso.Position := pbProgreso.Min;
    if FProcesando then begin
        pnlAvance.Visible := True;
        btnSalir.Caption := STR_CANCEL;
        btnProcesar.Enabled := False;
    end
    else begin
        pnlAvance.Visible := False;
        btnSalir.Caption := STR_QUIT;
        btnProcesar.Enabled := True; //(FCantidadAProcesar > 0);
    end;
    Application.ProcessMessages;
end;

{******************************** Function Header ******************************
Function Name: RegistrarResumenProceso
Author : lgisuk
Date Created : 03/03/2006
Description :   Guarda un registro en la tabla ResuemnProcesoAsignacionBHTU
Parameters : CodigoOperacion: Integer; Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmProcesoAsignacionBHTUaInfraccion.RegistrarResumenProceso(var CodigoOperacion: Integer; var  Error: AnsiString): boolean;
begin
    Result := False;
    try
        spRegistrarProceso.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        spRegistrarProceso.Parameters.ParamByName('@CodigoProcesoAsignacionBHTU').Value := NULL;
        spRegistrarProceso.CommandTimeOut := 500;
        spRegistrarProceso.ExecProc;
        CodigoOperacion := spRegistrarProceso.Parameters.ParamByName('@CodigoProcesoAsignacionBHTU').Value;
        Result := True;
    except
        on E : Exception do begin
            Error := E.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: CerrarResumenProceso
Author : lgisuk
Date Created : 03/03/2006
Description :   Completa los datos en  un registro en la tabla ResuemnProcesoAsignacionBHTU
Parameters : CodigoOperacion: Integer; Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmProcesoAsignacionBHTUaInfraccion.CerrarResumenProceso(CodigoOperacion : Integer; Error : AnsiString) : Boolean;
begin
    Result := False;
    try
        spCerrarProceso.Parameters.ParamByName('@CodigoProcesoAsignacionBHTU').Value := CodigoOperacion;
        spCerrarProceso.CommandTimeOut := 500;
        spCerrarProceso.ExecProc;
        Result := True;
    except
        on E : Exception do begin
            Error := E.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: MostrarReporte
Author : lgisuk
Date Created : 03/03/2006
Description : Muestra el reporte
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmProcesoAsignacionBHTUaInfraccion.MostrarReporte;
var
    F : TfrmReportePRocesoAsignacionBHTU;
begin
    Application.Createform(TfrmReportePRocesoAsignacionBHTU, F);
    if not F.Inicializar(FCodigoOperacion, Caption) then F.Release;
end;

{******************************** Function Header ******************************
Function Name: RegistrarOperacion
Author : ndonadio
Date Created : 05/10/2005
Description : Registra la Operaci�n en el Log de Operaciones
Parameters : CodigoModulo: integer; sFileName: string
Return Value : boolean
*******************************************************************************}
function TfrmProcesoAsignacionBHTUaInfraccion.RegistrarOperacion : boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
var
    DescError : string;
begin
    Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, 63, '', UsuarioSistema, '', True, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);
    if not Result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, Caption, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerInfraccionesAProcesar
  Author:    lgisuk
  Date Created: 07/03/2006
  Description: Obtengo las infracciones a Procesar
  Parameters: None
  Return Value: None

  Revision 1:
      Author : ggomez
      Date : 30/03/2006
      Description : En el llamado al SP spObtenerInfraccionesAProcesarClearingBHTU
        sete� el par�metro CodigoOperacionInterfase con Null en lugar de -1.

-----------------------------------------------------------------------------}
Function TfrmProcesoAsignacionBHTUaInfraccion.ObtenerInfraccionesAProcesar(FechaDesde, FechaHasta : TDateTime) : Boolean;
Resourcestring
    MSG_COULD_NOT_OBTAIN_INFRACTIONS_TO_PROCESS = 'No se pudieron obtener las infracciones a procesar';
    MSG_ERROR = 'Error';
var
  	Desde : Variant;
    Hasta : Variant;
    FErrorMsg : AnsiString;
begin
    //Informo la tarea
    lbldetalle.Caption := 'Obteniendo infracciones a procesar..';

    //Inicializo variables
    Desde := -1;
    Hasta := 0;
    FerrorMsg := '';

    //Inicializo la barra de progreso
    pbProgreso.Position := 1;
    //Establezco el maximo
    pbProgreso.Max := 1000;

    while (Desde <> Hasta) and (FErrorMsg = '') and (not FCancelar) do begin

        Desde := Hasta;

        try
            With spObtenerInfraccionesAProcesarClearingBHTU do begin
                Close;
                CommandTimeout := 500;
                Parameters.Refresh;
                Parameters.ParamByName('@FechaDesde').Value := FechaDesde;
                Parameters.ParamByName('@FechaHasta').Value := FechaHasta;
                Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                Parameters.ParamByName('@Desde').Value := Desde;
                Parameters.ParamByName('@Hasta').Value := 0;
                //Ejecuto
                ExecProc;
                //Obtengo el ultimo numero de convenio procesado
                Hasta := Integer (Parameters.ParamByName( '@Hasta' ).Value);
            end;

        except
            on E : Exception do begin
                FErrorMsg := MSG_COULD_NOT_OBTAIN_INFRACTIONS_TO_PROCESS;
                MsgBoxErr(FErrorMsg, e.Message, MSG_ERROR, MB_ICONERROR);
                Break;
            end;
        end;

        // Incrementar la barra de progreso
        pbProgreso.StepIt;
        if pbProgreso.Position >= pbProgreso.Max then begin
            // Inicializar la barra de progreso
            pbProgreso.Position := 1;
        end;

        //Refresco la pantalla
        Application.ProcessMessages;
        
    end;

    //finalizo la barra de progreso
    pbProgreso.Position := pbProgreso.Max;
    Application.ProcessMessages;

    //Si llego a procesar hasta el final y no hubo errores. Apruebo este paso.
    Result := (Desde = Hasta);
end;

{******************************** Function Header ******************************
Function Name: ActualizarLog
Author : lgisuk
Date Created : 07/04/2006
Description : Actualizo el log para marcar la hora de finalizaci�n del proceso  
Parameters : CantidadErrores:integer
Return Value : boolean
*******************************************************************************}
Function TfrmProcesoAsignacionBHTUaInfraccion.ActualizarLog : Boolean;
Resourcestring
    MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
Const
    STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
var
    DescError : String;
begin
    try
        Result := ActualizarLogOperacionesInterfaseAlFinal
                   (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(0),DescError);
    except
        on E : Exception do begin
           MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
           Result := False;
        end;
    end;
end;


{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author : lgisuk
Date Created : 03/03/2006
Description : Proceso principal de asignacion
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmProcesoAsignacionBHTUaInfraccion.btnProcesarClick(Sender: TObject);

    {******************************** Function Header ******************************
    Function Name: ValidarFecha
    Author : lgisuk
    Date Created : 09/03/2006
    Description : Valido que sea una fecha valida
    Parameters : Sender: TObject
    Return Value : None
    *******************************************************************************}
    function ValidarFecha(Control : TDateEdit): Boolean;
    begin
        Result := False;
        if Control.IsEmpty then Exit;
        try
            if StrToDate(Control.Text) > EncodeDate(1900,1,1) then begin
                Result := True;
            end;
        except
            on exception do exit;
        end;
    end;

resourcestring
    MSG_ERROR_VALUE_FROM = 'La Fecha Desde no es una fecha v�lida.';
    MSG_ERROR_VALUE_TO = 'La Fecha Hasta no es una fecha v�lida.';
    MSG_ERROR_VALUE_RANGE = 'La Fecha Hasta debe ser mayor o igual a la Fecha Desde';
    MSG_ERROR_RETRIEVING_NEXT_BHTU = 'Error al Obtener los datos del BHTU';
    MSG_ERROR_OPENING_BHTU_LIST = 'Error abriendo la lista de BHTU sin Asignar';
    MSG_ERROR_USING_BHTU = 'Error al Asignar el BHTU a la Infraccion seleccionada';
    MSG_ERROR_SEARCHING_MORE_INFRACTIONS = 'Error al buscar otras Infracciones para el BHTU';
    MSG_ERROR_SEARCHING_LOST_TRX = 'Error asignando BHTU a Tr�nsitos en franjas de Tolerancia';
    MSG_ERROR_RETRIEVING_CN_TRX = 'Error recuperando Tr�nsitos de CN Asginados al BHTU';
    MSG_ERROR_CHECKING_CN_TRX = 'Error al Asignar Tr�nsito de CN al BHTU';
    MSG_ERROR_CANT_REGISTER_OPERATION = 'Error al registrar la operacion';
    MSG_ERROR_CLOSING_OPERATION = 'Error al cerrar la operacion';
    MSG_PROCESS_ENDED_OK = 'El proceso finaliz� con �xito!';
    MSG_PROCESS_CANCELLED = 'El proceso fue cancelado por el usuario!';
const
    STR_CANT_ASIGNACIONES = 'Se asignaron %d BHTU.' ;
    STR_NO_ASIGNACION = 'No se asignaron BHTU.';
var
    DescError : AnsiString;
    BHTUQry : Integer;
    CodigoAsignacion : Integer;
    NumeroDayPass : Integer;
    NumCorrCA : Integer;
    CantidadProcesados : Integer;
    FechaUso : TDateTime;
    Patente : AnsiString;
begin

    //Valido las fechas seleccionadas para las infracciones
    if not ValidateControls( [deFechaDesdeInfracciones, deFechaHastaInfracciones, deFechaHastaInfracciones], [(ValidarFecha(deFechaDesdeInfracciones)), (ValidarFecha(deFechaHastaInfracciones)), (deFechaDesdeInfracciones.Date <= deFechaHastaInfracciones.Date)], Caption, [MSG_ERROR_VALUE_FROM, MSG_ERROR_VALUE_TO,  MSG_ERROR_VALUE_RANGE]) then Exit;
    //Valido las fechas selecionadas para los BHTU
    if not ValidateControls( [deFechaDesdeBHTU, deFechaHastaBHTU, deFechaHastaBHTU], [(ValidarFecha(deFechaDesdeBHTU)), (ValidarFecha(deFechaHastaBHTU)), (deFechaDesdeBHTU.Date <= deFechaHastaBHTU.Date)], Caption, [MSG_ERROR_VALUE_FROM, MSG_ERROR_VALUE_TO,  MSG_ERROR_VALUE_RANGE]) then Exit;

    FProcesando := True;
    HabilitarBotones;
    CantidadProcesados := 0;
    try
        //Preparar Proceso
        DescError := '';
        try
            //Registro la operacion de Transferenciar de Infracciones a Procesar
            if not RegistrarOperacion then begin
                Exit;
            end;

            //Obtengo lista infracciones a procesar, Selecciono las infracciones a asignar a boletos de habilitacion tardia
            if not ObtenerInfraccionesAProcesar(defechadesdeInfracciones.Date, defechahastaInfracciones.Date) then begin
                if FCancelar then MsgBox(MSG_PROCESS_CANCELLED);
                Exit;
            end;

            //Actualizo el log para dejar registro de a que hora finalizo el proceso
            if not ActualizarLog then begin
                Exit;
            end;

            //Obtengo la cantidad de BHTU a procesar
            ObtenerCantidadAProcesar;

            // Obtener lista de BHTU a Asignar
            DescError := MSG_ERROR_OPENING_BHTU_LIST;
            spObtenerListaBHTU.Close;
            spObtenerListaBHTU.Parameters.Refresh;
            spObtenerListaBHTU.Parameters.ParamByName('@FechaDesde').Value := deFechaDesdeBHTU.Date;
            spObtenerListaBHTU.Parameters.ParamByName('@FechaHasta').Value := deFechaHastaBHTU.Date;
            spObtenerListaBHTU.CommandTimeOut := 500;
            spObtenerListaBHTU.Open;
            if  not spObtenerListaBHTU.Eof then begin
                // si no est� vacio, registro la operacion
                if not RegistrarResumenProceso(FCodigoOperacion, DescError) then begin
                    MsgBoxErr(MSG_ERROR_CANT_REGISTER_OPERATION, DescError, Caption, MB_ICONSTOP);
                    Exit;
                end;
            end;
            BHTUQry := FCantidadAProcesar;
            // Por cada BHTU sin Asignar
            DescError := MSG_ERROR_RETRIEVING_NEXT_BHTU;
            while (not spObtenerListaBHTU.Eof) and (not FCancelar) do begin
                // Obtengo los datos del daypass(BHTU) seleccionado

                NumeroDaypass   := spObtenerListaBHTU.FieldByName('NumeroDayPass').AsInteger;
                FechaUso      := spObtenerListaBHTU.FieldByName('FechaUso').AsDateTime;
                Patente         := trim( spObtenerListaBHTU.FieldByName('Patente').AsString);

                // Inicio Transaccion
                DMConnections.BaseCAC.BeginTrans;

                pbProgreso.StepIt;

                // Asigno BHTU A Infraccion
                DescError := MSG_ERROR_USING_BHTU;
                spAsignarBHTUAInfraccion.Parameters.Refresh;
                spAsignarBHTUAInfraccion.Parameters.ParamByName('@CodigoOperacion').Value := FCodigoOperacion;
                spAsignarBHTUAInfraccion.Parameters.ParamByName('@NumeroDayPass').Value := NumeroDaypass;
                spAsignarBHTUAInfraccion.Parameters.ParamByName('@FechaUso').Value := FechaUso;
                spAsignarBHTUAInfraccion.Parameters.ParamByName('@Patente').Value := Patente;
                spAsignarBHTUAInfraccion.Parameters.ParamByName('@CodigoAsignacion').Value := NULL;
                spAsignarBHTUAInfraccion.Parameters.ParamByName('@FechaInfraccion').Value := NULL;
                spAsignarBHTUAInfraccion.CommandTimeOut := 500;
                spAsignarBHTUAInfraccion.ExecProc;

                // si no asigno... sigo con el siguiente...
                if spAsignarBHTUAInfraccion.Parameters.ParamByName('@RETURN_VALUE').Value = -1 then begin
                    DMConnections.BaseCAC.CommitTrans; // hay que cerrar la transaccion...
                    // sigo con el siguiente BHTU
                    spObtenerListaBHTU.Next;
                    BHTUQry := BHTUQry - 1;
                    lblDetalle.Caption := Format(MSG_BHTU_QRY, [BHTUQry]);
                    pbProgreso.StepBy(5);
                    Application.ProcessMessages;

                    Continue;
                end;
                inc(CantidadProcesados);
                CodigoAsignacion    := spAsignarBHTUAInfraccion.Parameters.ParamByName('@CodigoAsignacion').Value;

                pbProgreso.StepIt;

                // Obtengo los Trx de CN que Anule
                DescError := MSG_ERROR_RETRIEVING_CN_TRX;
                spObtenerTransitosCN.Close;
                spObtenerTransitosCN.Parameters.ParamByName('@CodigoAsignacion').Value := CodigoAsignacion;
                spObtenerTransitosCN.CommandTimeOut := 500;
                spObtenerTransitosCN.Open;
                //Por Cada Uno
                DescError := MSG_ERROR_RETRIEVING_CN_TRX;
                pbProgreso.StepIt;
                while  (not spObtenerTransitosCN.Eof) do begin
                //no controlo FCancelar, porque cancelo por ciclo completo, es decir, termino la asignacion en curso antes de cancelar
                    NumCorrCA := spObtenerTransitosCN.FieldByName('NumCorrCA').AsInteger;
                    // Anulo Transito de Infraccion (real)
                    DescError := MSG_ERROR_CHECKING_CN_TRX;
                    // Asigno Transito (real) al Daypass
                    spAsignarTransitoCNaBHTU.Parameters.ParamByName('@NumCorrCA').Value := NUmCorrCA;
                    spAsignarTransitoCNaBHTU.Parameters.ParamByName('@NumeroDayPass').Value := NumeroDaypass;
                    spAsignarTransitoCNaBHTU.CommandTimeOut := 500;
                    spAsignarTransitoCNaBHTU.ExecProc;
                    // siguiente trx de CN...
                    DescError := MSG_ERROR_RETRIEVING_CN_TRX;
                    spObtenerTransitosCN.Next;
                end;
                DMConnections.BaseCAC.CommitTrans;
                // siguiente BHTU
                DescError := MSG_ERROR_RETRIEVING_NEXT_BHTU;
                spObtenerListaBHTU.Next;
                BHTUQry := BHTUQry - 1;
                lblDetalle.Caption := Format(MSG_BHTU_QRY, [BHTUQry]);
                pbProgreso.StepIt;
                Application.ProcessMessages;
            end;

            if not CerrarResumenProceso(FCodigoOperacion, DescError) then begin
                MsgBoxErr( MSG_ERROR_CLOSING_OPERATION, DescError, Caption, MB_ICONERROR);
                Exit;
            end;
            pbProgreso.Position := pbProgreso.Max;

            if not FCancelar then begin
                if CantidadProcesados > 0 then MsgBox(MSG_PROCESS_ENDED_OK + CRLF + Format(STR_CANT_ASIGNACIONES, [CantidadProcesados]), Caption, MB_ICONINFORMATION)
                else MsgBox(MSG_PROCESS_ENDED_OK + CRLF + STR_NO_ASIGNACION, Caption, MB_ICONINFORMATION)
            end
            else MsgBox(MSG_PROCESS_CANCELLED, Caption, MB_ICONINFORMATION);
        except
            on E : Exception do begin
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr( DescError, E.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        //si procese algo...
        if CantidadProcesados > 0 then begin
             //muestro el reporte
             MostrarReporte;
        end;
        FProcesando := False;
        ObtenerCantidadAProcesar;
        HabilitarBotones;
    end;
end;



{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : lgisuk
Date Created : 03/03/2006
Description :  Accion al presionar el btnSalir (Salir o Cancelar)
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmProcesoAsignacionBHTUaInfraccion.btnSalirClick(Sender: TObject);
begin
    if FProcesando then FCancelar := True else Close;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : lgisuk
Date Created : 03/03/2006
Description : Impide cerrar si se est� procesando.
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmProcesoAsignacionBHTUaInfraccion.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : lgisuk
Date Created : 03/03/2006
Description : esto es para que el form se cierre realmente...
Parameters : Sender: TObject; var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TfrmProcesoAsignacionBHTUaInfraccion.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;



end.
