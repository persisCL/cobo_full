{********************************** Unit Header ********************************
File Name : fActivacionesSerbanc.pas
Author : ndonadio
Date Created: 05/08/2005 - revision 16/08/2005
Language : ES-AR
Description : Interfaz de Salida. Envio de Archivo de Activaciones a Serbanc
              (Gestion de Cobranza a Morosos)
              Genera dos archivos: Notas de Cobro y Patentes (detalles de consumos
              por patente de esas notas de cobro)

Revision 1
Author: pdominguez
Date: 11/05/2009
Description: SS 803
       - Se a�aden los objetos:
            cbEmpresasRecaudaadoras: TVariantComboBox
            lblEmpresa: TLabel
            spPrepararComprobantesPendientesActivacionSerbanc: TADOStoredProc
       - Se modificaron las siguientes Procedimientos / Funciones:
            Inicializar
            CrearNombresArchivos
            PrepararComprobantesPendientes
            ObtenerComprobantesPendientes
            GenerarArchivoComprobantes
            GenerarArchivoPatentes
            RegistrarNotasEnLog
            btnProcesarClick

Revision 2
Author: mbecerra
Date: 18-Mayo-2009
Description:	Con el fin de acelerar el procesamiento, se agrega una DBGrid.

Revision 3
  Author: mpiazza
  Date: 04/06/2009
  Description:	ss-803 Se agrega un parametro para el filtro del combo de empresa

Revision 3
Author: mpiazza
Date: 22/06/2009
Description:	ss-803 se cambia a la funcion CargarEmpresaRecaudacionPreJudicial
            por una que filtre directamente prejudicial

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

  
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

*******************************************************************************}
unit fActivacionesSerbanc;

interface

uses
    //Activaciones Serbanc
    Util,
    UtilProc,
    StrUtils,
    PeaProcs,
    PeaTypes,
    ComunesInterfaces,
    ConstParametrosGenerales,
    UtilDB,
    DMConnection,
    fReporteActivacionesSerbanc,
    //Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB, ppDBPipe, ppPrnabl, ppClass,
    ppCtrls, ppBands, ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB, VariantComboBox,
  Grids, DBGrids;

type
  TfrmActivacionesSerbanc = class(TForm)
    Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    txtDestinoArchivo: TPickEdit;
    spObtenerComprobantesPendientesActivacionSerbanc: TADOStoredProc;
    spObtenerConsumosPorPatenteSerbanc: TADOStoredProc;
    SQLQuery: TADOQuery;
    spActualizarComprobantesEnviadosSerbanc: TADOStoredProc;
    spPrepararComprobantesPendientesActivacionSerbanc: TADOStoredProc;
    spObtenerComprobantesPendientesActivacionSerbancDomPpal: TADOStoredProc;
    spObtenerComprobantesPendientesActivacionSerbancDomFact: TADOStoredProc;
    spLimpiarTemporales: TADOStoredProc;
    spInsertarErroresSerbanc: TADOStoredProc;
    spPrepararTablasTemporalesActivacionesSerbanc: TADOStoredProc;
    lblReferencia: TLabel;
    ObtenerTotalEnviado: TADOStoredProc;
    cbEmpresasRecaudadoras: TVariantComboBox;
    Label1: TLabel;
    lblEmpresa: TLabel;
    Label2: TLabel;
    dbgActivaciones: TDBGrid;
    dsActivaciones: TDataSource;
    dbgPatentes: TDBGrid;
    dsPatentes: TDataSource;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtDestinoArchivoButtonClick(Sender: TObject);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    FDestino: AnsiString;
    FNombreArchivoNK,
    FNombreArchivoConsumos: AnsiString;
    FProcesando: Boolean;
    FFechaProceso: TDateTime;
    FCancelar: Boolean;
    FCodigoOperacionInterfase: Integer;
    FMontoTotalPatentes: Int64;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    Function  CrearNombresArchivos(Path: AnsiString): Boolean;
    Function  RegistrarNotasEnLog(CodOperacion: Integer; var Error: AnsiString; CodigoEmpresaRecaudadora: Integer; var RegistrosActualizados: Integer): Boolean;
    Function  PrepararComprobantesPendientes(var Error: AnsiString; var Cantidad: Integer; CodigoEmpresaRecaudadora: Integer): Boolean;
    Function  GenerarArchivoComprobantes(var Error: AnsiString; CodigoEmpresaRecaudadora: Integer): Boolean;
    Function  GenerarArchivoPatentes(var Error: AnsiString; CodigoEmpresaRecaudadora: Integer): Boolean;
    Function  ObtenerComprobantesPendientes(var Error: AnsiString; CodigoEmpresaRecaudadora: Integer): boolean;
    Procedure MaximoBarra(Cantidad: Integer);
    Procedure MostrarReporteFinalizacion(CodigoOperacionInterfase: Integer);
    Procedure LiberarDBRes;
//    Procedure EliminarArchivos;
    Procedure HabilitarBotones;
    function  DevuelveNombreArchivo(MascaraArchivo: String; CodigoEmpresa: Integer): String;
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): boolean;
  end;

var
  frmActivacionesSerbanc: TfrmActivacionesSerbanc;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 05/08/2005
Description :   Valida que la tabla de origen de datos tenga algo para procesar
                e intenta obtener el destino de los archivos.
Parameters : None
Return Value : boolean

Revision 1
Author: pdominguez
Date: 11/05/2009
Description: SS 803
       - Se a�ade la carga de los datos en el combo de EmpresasRecaudadoras.

Revision 2
Author: mpiazza
Date: 04/06/2009
Description:	ss-803 Se agrega un parametro para el filtro del combo de empresa

Revision 3
Author: mpiazza
Date: 22/06/2009
Description:	ss-803 se cambia a la funcion CargarEmpresaRecaudacionPreJudicial
            por una que filtre directamente prejudicial


*******************************************************************************}
function TfrmActivacionesSerbanc.Inicializar(Titulo: AnsiString): boolean;
resourcestring
    ERROR_CANNOT_INITIALIZE     = 'No se puede inicializar la interfaz.';
    MSG_NOTHING_TO_PROCESS      = 'No hay Activaciones Pendientes.';
    ERROR_CANNOT_GET_PARAMETER  = 'Error obteniendo el parametro general %s';
const
    DIR_DEST_ACTIVACIONES_SERBANC = 'DIR_DEST_ACTIVACIONES_SERBANC' ;
begin
    Result := False;
    
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    // Le pongo de caption lo que trae de parametro...
    Caption :=  Titulo;
    //Centro el form
  	CenterForm(Self);
    // Cargar el parametro general de directorio de destino
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DEST_ACTIVACIONES_SERBANC, FDestino) then begin
        MsgBoxErr(ERROR_CANNOT_INITIALIZE, Format(ERROR_CANNOT_GET_PARAMETER, [DIR_DEST_ACTIVACIONES_SERBANC]), Caption, MB_ICONSTOP);
        Exit;
    end;
    FProcesando := False;
    FCancelar := False;
    // muestra el directorio de destino
    txtDestinoArchivo.Text := FDestino;
    HabilitarBotones;
    // Revisi�n 1/2 (SS 803)
    CargarEmpresaRecaudacionPreJudicial( DMConnections.BaseCAC,cbEmpresasRecaudadoras,True,0);
    cbEmpresasRecaudadoras.Items[0].Caption := 'Todas las Empresas';
    cbEmpresasRecaudadoras.ItemIndex := 0;
    // Fin Revisi�n 1 (SS 803)

    // los nombres de los archivos los creo aca, para poder mostrarlos en el hint de ayuda...
    // al iniciar el proceso estos nombres se recrearan usando la fecha que corresponda a ese momento.
    Result := CrearNombresArchivos(FDestino);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : ndonadio
Date Created : 05/08/2005
Description :   Muestra un hint con info acerca de la interfaz.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmActivacionesSerbanc.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_ACTIVACIONES_SERBANC  = ' ' + CRLF +
                                'El Archivo de Activaciones es' + CRLF +
                                'utilizado por el ESTABLECIMIENTO para informar a SERBANC' + CRLF +
                                'las Notas de Cobro de las cuales debe encargarse de la ' + CRLF +
                                'Gest�n de Cobranza. Adicionalmente se env�a un segundo ' + CRLF +
                                'Archivo con los detalles de Consumos por Patente. ' + CRLF +
                                ' ' + CRLF +
                                'Nombre del Archivo de Notas de Cobro: %s' + CRLF +
                                'Nombre del Archivo de Consumos por Patente: %s' + CRLF +
                                ' ';
begin
    //si esta procesando sale
    if FProcesando then exit;
    //Muestro el mensaje
    MsgBoxBalloon(Format(MSG_ACTIVACIONES_SERBANC,[FNombreArchivoNK,FNombreArchivoConsumos]), Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : ndonadio
Date Created : 05/08/2005
Description :   permito hacer click si no esta procesando
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmActivacionesSerbanc.ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
    if FProcesando then ImgAyuda.Cursor := crHourglass else ImgAyuda.Cursor := crHandPoint;
end;

{******************************** Function Header ******************************
Function Name: txtDestinoArchivoButtonClick
Author : ndonadio
Date Created : 05/08/2005
Description :  Permite seleccionar un directorio de destino para los archivos.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmActivacionesSerbanc.txtDestinoArchivoButtonClick(Sender: TObject);
resourcestring
    MSG_SELECT_LOCATION = 'Seleccione ubicaci�n donde se generar�n los archvos.';
var
    Location: String;
begin
    btnProcesar.Enabled := False;
    // abre el form de seleccion de directorio
    Location := Trim(BrowseForFolder(MSG_SELECT_LOCATION));
    // valida el directio elegido
    if Location = '' then Location := ExtractFilePath(txtDestinoArchivo.Text);
   // lo carga en las variables, edits, etc.
    txtDestinoArchivo.Text := Location;
    FDestino := GoodDir(txtDestinoArchivo.Text);
    // Habilita el boton procesar si puede crear nombres de archivos...
    btnProcesar.Enabled := CrearNombresArchivos(GoodDir(txtDestinoArchivo.Text));
end;

{******************************** Function Header ******************************
Function Name: CrearNombresArchivos
Author : ndonadio
Date Created : 05/08/2005
Description : Crea el nombre de archivo basandose en el Path recibido.
Parameters : Path: AnsiString
Return Value : AnsiString

Revision : 1
    Author : pdominguez
    Date   : 18/05/2009
    Description : SS 803
           - Se modifican las constantes PREFIJO_ARCHIVO_NKS y PREFIJO_ARCHIVO_CONSUMOS para
           adaptar la nomenclatura de los ficheros para indicar para que empresa se generan.
           - Se a�ade la ruta completa de los ficheros a almacenar en las propiedades del
           Form, FNombreArchivoNK y FNombreArchivoConsumos.
*******************************************************************************}
function TfrmActivacionesSerbanc.CrearNombresArchivos(Path: AnsiString): Boolean;
const
    PREFIJO_ARCHIVO_NKS      = 'ASG_COB_%s_';
    PREFIJO_ARCHIVO_CONSUMOS = 'ASG_PTN_%s_';
    EXTENSION_ARCHIVOS       = '.TXT';
    FORMATO_FECHA            = 'YYYYMMDD';
begin
    // Obtengo la fecha del proceso
    FFechaProceso := NowBase(DMConnections.BaseCAC);
    // el nombre del archivo de notas de cobro (activaciones)
    FNombreArchivoNK := GoodDir(Path) + PREFIJO_ARCHIVO_NKS + FormatDateTime(FORMATO_FECHA, FFechaProceso) + EXTENSION_ARCHIVOS;
    // el nombre del archivo de los detalles de consumos (patentes)
    FNombreArchivoConsumos := GoodDir(Path) + PREFIJO_ARCHIVO_CONSUMOS + FormatDateTime(FORMATO_FECHA, FFechaProceso) + EXTENSION_ARCHIVOS;
    btnProcesar.Enabled := True;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 05/08/2005
Description : Habilita o deshabilita botones basandose en si est� procesando o no.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmActivacionesSerbanc.HabilitarBotones;
begin
    txtDestinoArchivo.Enabled := Not FProcesando;
    btnSalir.Enabled := Not FProcesando;
    btnProcesar.Enabled := Not FProcesando;
    pnlAvance.Visible := FProcesando;
    btnCancelar.Enabled := FProcesando;
    lblReferencia.Visible := FProcesando;
    lblReferencia.Caption := '';
    pbProgreso.Position := 0;
    FCancelar := False;
    cbEmpresasRecaudadoras.Enabled := Not FProcesando;
    Cursor := iif(FProcesando, crHourglass, crDefault);
end;

{******************************** Function Header ******************************
Function Name: MaximoBarra
Author : ndonadio
Date Created : 25/08/2005
Description :   Calcula el maximo de la barra y el delta porcentual...
Parameters : None
Return Value : Integer
*******************************************************************************}
procedure TfrmActivacionesSerbanc.MaximoBarra(Cantidad: Integer);
var
    Resultado: Integer;
begin
    // la cantidad dfe comprobantes para ciclar de a uno
    Resultado := Cantidad; //(Cantidad div 500) + 2; // esto es para la obtencion del comprobantes (con y sin domicilio)
    Resultado := Resultado + Cantidad div 3; // para el proceso de cada comprobante (actualizo cada 3)
    Resultado := Resultado + (Cantidad div 250) + 1; // esto es para los ciclos de consumos por patente
    Resultado := Resultado + 20; //otras actualizaciones... creo que sobran algunas, pero al final lo completa...
    pbProgreso.Max := Resultado;
    pbProgreso.Smooth := True;
    pbProgreso.Min := 0;
end;

{******************************** Function Header ******************************
Function Name: PrepararComprobantesPendientes
Author : ndonadio
Date Created : 24/08/2005
Description :   Obtiene los comprobantes pendientes de activacion
Parameters : var Error: AnsiString; var Cantidad: Integer;
Return Value : Boolean

Revision : 1
    Author : pdominguez
    Date   : 18/05/2009
    Description : SS 803
            - Se a�ade a la funci�n el par�metro CodigoEmpresaRecaudadora.
            - Se a�ade a la ejecuci�n del SP spPrepararComprobantesPendientesActivacionSerbanc
            el par�metro @CodigoEmpresasRecaudadoras.
    Revision : 2
    Author : vpaszkowicz
    Date   : 06/01/2010
    Description : Aumento el commandtimeout del sp preparar
*******************************************************************************}
function TfrmActivacionesSerbanc.PrepararComprobantesPendientes(var Error: AnsiString; var Cantidad: Integer; CodigoEmpresaRecaudadora: Integer): Boolean;
resourcestring
    MSG_LOADING_QTY = 'Preparando para Procesar  : %s Comprobantes cargados...';
var
    CantTotal, Qty : Integer;
    MaxComprobante : Int64;
begin
    Result := False;
    CantTotal := 0;
    spPrepararComprobantesPendientesActivacionSerbanc.CommandTimeout := 5000;
    // Llamada a un SP que al final me deja una tabla Temp ##...
    // Para poder usarlar desde el SP que trae los Detalles Consumos.
    try
        // mato las temporales si existen...esto es para asegurarme que
        // todo va a andar bien...
        spLimpiarTemporales.ExecProc;
        // Creo las temporales que voy a necesitar
        spPrepararTablasTemporalesActivacionesSerbanc.ExecProc;
        Qty := 1;
        MaxComprobante := 0;
        while (Qty > 0) AND (not FCancelar) do begin
            spPrepararComprobantesPendientesActivacionSerbanc.Parameters.ParamByName('@Cantidad').Value := 0;
            spPrepararComprobantesPendientesActivacionSerbanc.Parameters.ParamByName('@MaximoComprobante').Value := MaxComprobante;
            spPrepararComprobantesPendientesActivacionSerbanc.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value := CodigoEmpresaRecaudadora;

            spPrepararComprobantesPendientesActivacionSerbanc.ExecProc;

            Qty := spPrepararComprobantesPendientesActivacionSerbanc.Parameters.ParamByName('@Cantidad').Value;
            MaxComprobante := spPrepararComprobantesPendientesActivacionSerbanc.Parameters.ParamByName('@MaximoComprobante').Value;

            CantTotal := CantTotal + Qty;
            lblReferencia.Caption := Format(MSG_LOADING_QTY, [PadL(IntToStr(CantTotal), 7, ' ')]);
            Application.ProcessMessages;
        end;
    except
        on e:exception do begin
            Error :=  e.Message;
            Exit;
        end;
    end;
    Cantidad := cantTotal;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: ObtenerComprobantesPendientes
Author : ndonadio
Date Created : 25/08/2005
Description : Obtiene los comprobantes a procesar, realizando ciclos...
Parameters : var Error: AnsiString
Return Value : boolean

Revision : 1
    Author : pdominguez
    Date   : 18/05/2009
    Description : SS 803
        - Se a�adi� el par�metro @CodigoEmpresasRecaudadoras a los SP's
        spObtenerComprobantesPendientesActivacionSerbancDomFact y
        spObtenerComprobantesPendientesActivacionSerbancDomPpal.
        - Se a�adi� el par�metro @CodigoEmpresasRecaudadoras al SP
        spObtenerComprobantesPendientesActivacionSerbanc
*******************************************************************************}
function TfrmActivacionesSerbanc.ObtenerComprobantesPendientes(var Error: AnsiString; CodigoEmpresaRecaudadora: Integer): boolean;
resourcestring
    MSG_CANCELADO = 'Proceso cancelado por el usuario.';
    CAP_COMPROBANTED_OBTENIDOS = 'Procesando: %d Comprobantes Obtenidos';
    CAP_PLUS_DOM_FACT   = ' (con Dom. de Facturaci�n)';
    CAP_PLUS_DOM_PPAL   = ' (con Dom. Principal)';
var
    Cantidad:   Int64;
    Total:      Integer;
begin
    Result := False;
    try
        Total := 0;
        // Proceso los que van con Dom. de facturacion
        // en ciclos de a 1000
        Cantidad := -1;
        while (Cantidad <> 0)  and (not FCancelar) do begin
            // levanto maximo 1000 de lo que quede... on dom. de fact.
            spObtenerComprobantesPendientesActivacionSerbancDomFact.CommandTimeout := 0;
            spObtenerComprobantesPendientesActivacionSerbancDomFact.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value := CodigoEmpresaRecaudadora;
            spObtenerComprobantesPendientesActivacionSerbancDomFact.ExecProc;
            Cantidad := spObtenerComprobantesPendientesActivacionSerbancDomFact.Parameters.ParamByName('@Cantidad').Value;
            Total := Total + Cantidad;
            lblReferencia.Caption := format(CAP_COMPROBANTED_OBTENIDOS, [Total]) + CAP_PLUS_DOM_FACT;
            pbProgreso.StepBy(Cantidad);
            pbProgreso.Refresh;
            Application.ProcessMessages;
        end;
        if FCancelar then Exit;
        // Proceso los de Domicilio Principal
        // en ciclos de a 1000 comprobantes
        Cantidad := -1; // esta asignacion podria obviarse, pero asi me parece mas claro el cambio de ciclo...
        while (Cantidad <> 0) and (not FCancelar) do begin
            // levanto maximo 500 de lo que quede... on dom. ppal.
            spObtenerComprobantesPendientesActivacionSerbancDomPpal.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value := CodigoEmpresaRecaudadora;
            spObtenerComprobantesPendientesActivacionSerbancDomPpal.ExecProc;
            Cantidad := spObtenerComprobantesPendientesActivacionSerbancDomPpal.Parameters.ParamByName('@Cantidad').Value;
            Total := Total + Cantidad;
            lblReferencia.Caption := format(CAP_COMPROBANTED_OBTENIDOS, [Total]) + CAP_PLUS_DOM_PPAL;
            pbProgreso.StepBy(Cantidad);
            pbProgreso.Refresh;
            Application.ProcessMessages;
        end;
        if FCancelar then Exit;
        //Obtener totales.
//        ObtenerTotalEnviado.ExecProc;
        // Obtener Listos para Activar
        Result := True;
    except
        on e:exception do begin
            Error := e.Message + CRLF + lblReferencia.Caption;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: GenerarArchivoComprobantes
Author : ndonadio
Date Created : 16/08/2005
Description : Genera el archivo de comprobantes
Parameters : var Error: AnsiString
Return Value : Boolean

Revision : 1
    Author : pdominguez
    Date   : 19/05/2009
    Description : SS 803
            - Se modificaron las funciones CargarRegistroControlNotas,
            CargarComprobantesAlStringList
            - Se revis� y adapto la funci�n para el proceso masivo de generaci�n de Ficheros.

*******************************************************************************}
function TfrmActivacionesSerbanc.GenerarArchivoComprobantes(var Error: AnsiString; CodigoEmpresaRecaudadora: Integer): Boolean;

    {******************************** Function Header ******************************
    Function Name: CargarResgistroControlNotas
    Author : ndonadio
    Date Created : 08/08/2005
    Description :   Carga en el String List pasado un registro de control al principio.
    Parameters : var strComprobantes: TStringList; var Error: AnsiString
    Return Value : Boolean
    Revision : 1
        Author : vpaszkowicz
        Date : 28/02/2008
        Description : Obtengo el total enviado desde otro sp para poder usar el
        sp a recorrer con el CursorLocation en Server ya que de esta forma consume
        menos memoria.

    Revision : 2
        Author : pdominguez
        Date   : 19/05/2009
        Description : SS 803
                - Se a�ande al SP ObtenerTotalEnviado los par�metos @CodigoEmpresasRecaudadoras,
                @TotalComprobantes, @CantidadComprobantes.
    *******************************************************************************}
   //REV.2 function CargarRegistroControlNotas(var Archivo : TextFile; var Error: AnsiString): Boolean;
   function CargarRegistroControlNotas(var Linea : string; var Error: AnsiString): Boolean;
    ResourceString
        MSG_ERROR_OBTAIN_REGISTRY_OF_CONTROL = 'Error Obteniendo el Registro de Control de Comprobantes';
        MSG_ERROR_WRITE_REGISTRY_OF_CONTROL =  'Error al escribir el Registro de Control en el Archivo de Comprobantes';
    var
        MontoTotal: Int64;
        //REV.2 Linea: AnsiString;
    begin
        Result := False;
        //Obtengo el Registro de Control
        try
                //MontoTotal := Parameters.ParamByName('@TotalComprobantes').Value;

                ObtenerTotalEnviado.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value := CodigoEmpresaRecaudadora;
                ObtenerTotalEnviado.ExecProc;

                MontoTotal := ObtenerTotalEnviado.Parameters.ParamByName('@TotalComprobantes').Value;
                Linea := '';
                Linea := Linea + FormatDateTime('yyyymmddhhnnss', FFechaProceso);
                Linea := Linea + PadL(IntToStr(ObtenerTotalEnviado.Parameters.ParamByName('@CantidadComprobantes').Value),7,'0');
                Linea := Linea + PadL(IntToStr(MontoTotal),16,'0');
        except
            on e : exception do begin
                Error:= MSG_ERROR_OBTAIN_REGISTRY_OF_CONTROL + CRLF + e.Message;
                Exit;
            end;
        end;
        //Escribo el registro de control en el Archivo
        {REV.2
        try
            WriteLn(Archivo, Linea);
        except
            on e : exception do begin
                Error:= MSG_ERROR_WRITE_REGISTRY_OF_CONTROL + CRLF + e.Message;
                Exit;
            end;
        end;  }
        Result := True;
    end;

    {******************************** Function Header ******************************
    Function Name: CargarComprobantesAlStringList
    Author : ndonadio
    Date Created : 05/08/2005
    Description : Se encarga de para cada comprobante armar el string completo
                    y mandarlo al string list
    Parameters : String list para devolver los datos, String ERROR para devolver descripcion
                    de error si la hubiese....
    Return Value : Boolean

    Revision 1:
        Author : ggomez
        Date : 07/09/2006
        Description : Cambi� la forma de generar el valor para incorporar en el
            archivo para el campo TotalAPagar. Esto se hizo pues el campo
            TotalAPagar ahora es bigint.
            Antes se hac�a Campo := IntToStr(FieldByName('TotalAPagar').AsInteger);
            ahora se hace Campo := FloatToStrF(FieldByName('TotalAPagar').AsFloat, ffGeneral, 18, 0);

    Revision :
        Author : pdominguez
        Date   : 19/05/2009
        Description : SS 803
                - Se revis� y adapto la funci�n para el proceso masivo de generaci�n de Ficheros.

    *******************************************************************************}
    //REV.2 function CargarComprobantesAlStringList(var Archivo : TextFile; var Error: AnsiString): Boolean;
    function CargarComprobantesAlStringList(Archivo, LineaControl : string; var Error: AnsiString): Boolean;
    resourcestring
        MSG_ERROR_PROCESS       = 'Error al procesar los Comprobantes';
        MSG_ERROR_WRITE_LINE    = 'Error al escribir el registro en el Archivo de Comprobantes';
        MSG_PROCES_CANCELLED    = 'Proceso cancelado por el usuario';
        MSG_DOMFACT             = 'El Comprobante %s fue enviado, pero el convenio NO tiene domicilio principal. Se envio el de Facturaci�n.';
        CAP_PROC_X_DE_Y         = 'Procesando el comprobante %d de %d';
    const
        CONST_TAMANO_BYTES_STRINGLIST = 25 * 1024 * 1024;			{25 MB}    // SS_1027_PDO_20120404
    var
        fsArchivoComprobantes: TFileStream;                     // SS_1027_PDO_20120404
        TamanoStringList : Cardinal;	{hasta 4 mil millones}  // SS_1027_PDO_20120404
        Linea, Campo: AnsiString;
        i, Total: Integer;
        ListaRegistros : TStringList;		//REV.2
    begin
        try        // SS_1027_PDO_20120404
            // Actualizo la barra de progreso
            ListaRegistros := TStringList.Create;
            Result := False;
            i:= 0;
            Total :=  spObtenerComprobantesPendientesActivacionSerbanc.RecordCount;
            pbProgreso.Max := Total;

            //Total :=  ObtenerTotalEnviado.Parameters.ParamByName('@TotalRegistros').Value;
            Error := '';
            try
                // controlar si vale la pena el try...

                {---------------------------------------------------
                    Agregado en revision 2
                --------------------------------------------------------
                spObtenerComprobantesPendientesActivacionSerbanc.First;
                with spObtenerComprobantesPendientesActivacionSerbanc do begin}

                fsArchivoComprobantes := TFileStream.Create(Trim(Archivo), fmCreate);       // SS_1027_PDO_20120404

                ListaRegistros.Add(LineaControl);                                           // SS_1027_PDO_20120404
                TamanoStringList := Length(LineaControl);                                   // SS_1027_PDO_20120404


                with dbgActivaciones.DataSource.DataSet do begin
                    DisableControls;
                    First;
                    while  (not EOF) and (not FCancelar) do begin

                        Linea := '';
                        Campo := '';
                        // CodigoTipoCobranza - C2
                        Campo := FieldByName('CodigoTipoCobranza').AsString;
                        Campo := PadL(UpperCase(trim(Campo)),2,' ') ;
                        Linea := Linea + Campo;
                        Campo := '';
                        // NumeroComprobante - N12
                        Campo := FieldByName('NumeroComprobante').AsString;
                        Campo := PadL(UpperCase(trim(Campo)),12,'0');
                        Linea := Linea + Campo;
                        Campo := '';
                        // TipoComprobante - C2
                        Campo := FieldByName('TipoComprobante').AsString;
                        Campo := PadL(UpperCase(trim(Campo)),2,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // MontoComprobante - N16 -- TotalComprobante
                        Campo := FloatToStr(FieldByName('Importe').AsFloat);
                        Campo := PadL(UpperCase(trim(Campo)),16,'0');
                        Linea := Linea + Campo;
                        Campo := '';
                        // CodigoTipoDeudor - N1
                        Campo := FieldByName('CodigoTipoDeudor').AsString;
                        Linea := Linea + Campo;
                        Campo := '';
                        // NumeroDocumento - C10 - (RUT)
                        Campo := FieldByName('NumeroDocumento').AsString;
                        Campo := PadR(UpperCase(trim(Campo)),10,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // NumeroConvenio - C17
                        Campo := FieldByName('NumeroConvenio').AsString;
                        Campo := PadL(UpperCase(trim(Campo)),17,' ') ;
                        Linea := Linea + Campo;
                        Campo := '';
                        // Nombre - C30 (Razon Social...)
                        if FieldByName('Personeria').AsString <> 'J' then
                            Campo := LeftStr(FieldByName('Nombre').AsString,30)
                        else
                            Campo := LeftStr(FieldByName('Apellido').AsString,30);
                        Campo := PadL(Campo,30,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // Apellido - C20
                        if FieldByName('Personeria').AsString <> 'J' then
                            Campo := LeftStr(FieldByName('Apellido').AsString,20)
                        else
                            Campo := '';
                        Campo := PadL(Campo,20,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // ApellidoMaterno - C20
                        if FieldByName('Personeria').AsString <> 'J' then
                            Campo := LeftStr(FieldByName('ApellidoMaterno').AsString,20)
                        else
                            Campo := '';
                        Campo := PadL(Campo,20,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // Dir_Calle - C50
                        Campo := LeftStr(FieldByName('Dir_Calle').AsString,50);
                        Campo := PadR(trim(Campo),50,' ') ;
                        Linea := Linea + Campo;
                        Campo := '';
                        // Dir_Numero - C10
                        Campo := FieldByName('Dir_Numero').AsString;
                        Campo := PadL(trim(Campo),10,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // Dir_Detalle - C30
                        Campo := FieldByName('Dir_Detalle').AsString;
                        Campo := PadR(trim(Campo),30,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // CodigoPostal - C7
                        Campo := FieldByName('CodigoPostal').AsString;
                        Campo := PadL(Campo,7,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // Comuna - C20
                        Campo := FieldByName('Comuna').AsString;
                        Campo := PadR(Campo,20,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // Region - C3
                        Campo := FieldByName('Region').AsString;
                        Campo := PadL(UpperCase(trim(Campo)),3,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // Ciudad - C20
                        Campo := FieldByName('Ciudad').AsString;
                        Campo := PadR(UpperCase(trim(Campo)),20,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // Telefono1 - C15
                        Campo := FieldByName('Telefono1').AsString;
                        Campo := PadL(UpperCase(trim(Campo)),15,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // Telefono2 - C15
                        Campo := FieldByName('Telefono2').AsString;
                        Campo := PadL(UpperCase(trim(Campo)),15,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // Celular - C15
                        Campo := FieldByName('Celular').AsString;
                        Campo := PadL(UpperCase(trim(Campo)),15,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // email - C40
                        Campo := FieldByName('email').AsString;
                        Campo := PadR(Campo,40,' ');
                        Linea := Linea + Campo;
                        Campo := '';
                        // FechaEmision - F8 yyyymmdd
                        Campo := FormatDateTime('yyyymmdd',FieldByName('FechaEmision').AsDateTime);
                        Campo := PadR(UpperCase(trim(Campo)),8,'0');
                        Linea := Linea + Campo;
                        Campo := '';
                        // FechaVencimiento - F8 yyyymmdd
                        Campo := FormatDateTime('yyyymmdd',FieldByName('FechaVencimiento').AsDateTime);
                        Campo := PadR(UpperCase(trim(Campo)),8,'0');
                        Linea := Linea + Campo;
                        Campo := '';
                        // AjusteSencilloAnterior - N8
                        Campo := IntToStr(FieldByName('AjusteSencilloAnterior').AsInteger);
                        Campo := PadL(UpperCase(trim(Campo)),8,'0');
                        Linea := Linea + Campo;
                        Campo := '';
                        // AjusteSencilloActual - N8
                        Campo := IntToStr(FieldByName('AjusteSencilloActual').AsInteger);
                        Campo := PadL(UpperCase(trim(Campo)),8,'0');
                        Linea := Linea + Campo;
                        Campo := '';
                        // TotalAPagar - N16
                        Campo := FloatToStrF(FieldByName('TotalAPagar').AsFloat, ffGeneral, 18, 0);
                        Campo := PadL(UpperCase(trim(Campo)),16,'0');
                        Linea := Linea + Campo;
                        Campo := '';

                        {if  FieldByName('DomicilioFacturacion').AsInteger = 1 then begin
                            if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacionInterfase, Format(MSG_DOMFACT,[TRIM(FieldByName('NumeroComprobante').AsString)])) then begin
                                    Error := 'Error al asentar detalle de envio con domicilion de facturacion'+CRLF+'en el log de errores de interfase.';
                                    Exit;
                            end;
                        end;}
                        inc(i);
                        if ((i mod 11) = 1 )then begin
                            lblReferencia.Caption := Format(CAP_PROC_X_DE_Y, [i,Total]);
                            pbProgreso.Position := i;
                            pbProgreso.Refresh;
                        end;

                        Application.ProcessMessages ;
                        ListaRegistros.Add(Linea);

                        TamanoStringList := TamanoStringList + Length(Linea);               // SS_1027_PDO_20120404
                        if TamanoStringList >= CONST_TAMANO_BYTES_STRINGLIST then begin     // SS_1027_PDO_20120404
                            ListaRegistros.SaveToStream(fsArchivoComprobantes);             // SS_1027_PDO_20120404
                            ListaRegistros.Clear;                                           // SS_1027_PDO_20120404
                            TamanoStringList := 0;                                          // SS_1027_PDO_20120404
                        end;                                                                // SS_1027_PDO_20120404


                        //Escribo la linea en el archivo
                        {REV.2
                        try
                            //REV.2 WriteLn(Archivo, Linea);

                        except
                            on e:exception do begin
                                Error := MSG_ERROR_WRITE_LINE + CRLF + e.Message;
                                Exit;
                            end;
                        end;}

                        Next;
                    end; // while
                    lblReferencia.Caption := Format(CAP_PROC_X_DE_Y, [i,Total]);
                    lblReferencia.Refresh;
                end; // with

                if not FCancelar then begin
                    try
                        if ListaRegistros.Count > 0 then begin                      // SS_1027_PDO_20120404
                            ListaRegistros.SaveToStream(fsArchivoComprobantes);     // SS_1027_PDO_20120404
                        end;                                                        // SS_1027_PDO_20120404
    //            		ListaRegistros.SaveToFile(Archivo);                         // SS_1027_PDO_20120404
                    except on e:exception do begin
                                Error := MSG_ERROR_WRITE_LINE + CRLF + e.Message;
                                Exit;
                        end;
                    end;

                    Result := True;
                end
            except
                // si fall� sale e informa el error...
                on E : Exception do begin
                    Error := MSG_ERROR_PROCESS + CRLF + e.Message;
                    Exit;
                end;
            end;

    //        ListaRegistros.Free;                                                          // SS_1027_PDO_20120404
        finally                                                                             // SS_1027_PDO_20120404
            if Assigned(ListaRegistros) then FreeAndNil(ListaRegistros);                    // SS_1027_PDO_20120404
            if Assigned(fsArchivoComprobantes) then FreeAndNil(fsArchivoComprobantes);      // SS_1027_PDO_20120404
        end;                                                                                // SS_1027_PDO_20120404
    end;

resourcestring
    MSG_ERROR_SAVING_FILES = 'Error al grabar el Archivo de Comprobantes';
const
    STR_PROCESANDO_NK = 'Procesando Notas de Cobro';
    STR_CARGANDO_CONTROL_NK = 'Generando registro de control de Notas de Cobro';
var
    //REV.2 Archivo : TextFile;
    Archivo, Linea : string;
    IndiceEmpresa: Integer;
begin
    Result := False;

    try
        spObtenerComprobantesPendientesActivacionSerbanc.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value := CodigoEmpresaRecaudadora;
        spObtenerComprobantesPendientesActivacionSerbanc.Open;


        {REV.2
        AssignFile(Archivo, DevuelveNombreArchivo(FNombreArchivoNK,CodigoEmpresaRecaudadora));
        Rewrite(Archivo); }

        Archivo := DevuelveNombreArchivo(FNombreArchivoNK,CodigoEmpresaRecaudadora);
        try
            // Cargar Registro de Control
            lblReferencia.Caption := STR_CARGANDO_CONTROL_NK;
            if CargarRegistroControlNotas(Linea, Error) then begin
                // Cargar registros en el string list de salida
                lblReferencia.Caption := STR_PROCESANDO_NK;
                Result := CargarComprobantesAlStringList(Archivo, Linea, Error);
            end;
        except
            on e : exception do begin
                Error := MSG_ERROR_SAVING_FILES + CRLF + e.Message;
            end;
        end;
    Finally
        {REV.2 CloseFile(Archivo);}

        if spObtenerComprobantesPendientesActivacionSerbanc.Active then
            spObtenerComprobantesPendientesActivacionSerbanc.Close;

   end;
end;

{******************************** Function Header ******************************
Function Name: GenerarArchivoPatentes
Author : ndonadio
Date Created : 16/08/2005
Description :
Parameters : var Error: AnsiString
Return Value : Boolean

Revision : 1
    Author : pdominguez
    Date   : 19/05/2009
    Description : SS 803
            - Se modificaron las funciones CargarRegistroControlDetalle,
            CargarDetalleConsumos
            - Se revis� y adapto la funci�n para el proceso masivo de generaci�n de Ficheros.

*******************************************************************************}
function TfrmActivacionesSerbanc.GenerarArchivoPatentes(var Error: AnsiString; CodigoEmpresaRecaudadora: Integer): Boolean;

    {******************************** Function Header ******************************
    Function Name: CargarRegistroControlDetalle
    Author : ndonadio
    Date Created : 09/08/2005
    Description :  Agrega el registro de control en el string list  del Detalle de consumos
    Parameters : var strConsumos: TStringList; var Error: AnsiString
    Return Value : Boolean

    Revision : 1
        Author : pdominguez
        Date   : 19/05/2009
        Description : SS 803
                - Se a�ade al SP spObtenerConsumosPorPatenteSerbanc los par�metros
                @CodigoEmpresasRecaudadoras, @TotalCantidad, @TotalImporte.
    *******************************************************************************}
    //REV.2 function CargarRegistroControlDetalle(var Archivo : TextFile; var Error: AnsiString): Boolean;
    function CargarRegistroControlDetalle(var Linea : string; var Error: AnsiString): Boolean;
    ResourceString
        MSG_ERROR_OBTAIN_REGISTRY_OF_CONTROL = 'Error Obteniendo el Registro de Control de Patentes';
        MSG_ERROR_WRITE_REGISTRY_OF_CONTROL =  'Error al escribir el Registro de Control en el Archivo de Patentes';
    {REV.2 var
        Linea: AnsiString;}
    begin
        Result := False;
        //Obtengo el registro de Control
        try
            With spObtenerConsumosPorPatenteSerbanc do begin
                //REV.2 CommandTimeOut := 500;
                Close;
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroComprobante').Value := 0 ;
                Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value := CodigoEmpresaRecaudadora;
                Parameters.ParamByName('@TotalCantidad').Value := NULL;
                Parameters.ParamByName('@TotalImporte').Value := NULL;
                Open;
                Linea := '';
                Linea := Linea + FormatDateTime('yyyymmddhhnnss', FFechaProceso);
                Linea := Linea + PadL(VarToStr(Parameters.ParamByName('@TotalCantidad').Value ),7,'0');
                Linea := Linea + PadL(VarToStr(Parameters.ParamByName('@TotalImporte').Value),16,'0');
                Close;
            end;
        except
            on e : exception do begin
                Error := MSG_ERROR_OBTAIN_REGISTRY_OF_CONTROL + CRLF + e.Message;
                Exit;
            end;
        end;
        //Escribo el registro de control en el archivo de patentes.
        {REV.2
        try
            WriteLn(Archivo, Linea);
        except
            on e : exception do begin
                Error := MSG_ERROR_WRITE_REGISTRY_OF_CONTROL + CRLF + e.Message;
                Exit;
            end;
        end;   }

        Result := True;
    end;

    {******************************** Function Header ******************************
    Function Name: CargarDetalleConsumos
    Author : ndonadio
    Date Created : 09/08/2005
    Description : Carga el detalle de consumos (patentes) en el string list
    Parameters : var strConsumos:TStringList; var Error:AnsiString
    Return Value : boolean

    Revision 1:
        Author : nefernandez
        Date : 23/08/2007
        Description : SS 548: Cambio del AsInteger a AsVariant para el campo Monto
        del sp ObtenerConsumosPorPatenteSerbanc

    Revision : 2
        Author : pdominguez
        Date   : 19/05/2009
        Description : SS 803
                - Se a�ade al SP spObtenerConsumosPorPatenteSerbanc el par�metro
                @CodigoEmpresasRecaudadoras.
    *******************************************************************************}
    //REV.2 Function CargarDetalleConsumos(var Archivo : TextFile; var Error:AnsiString):boolean;
    Function CargarDetalleConsumos(Archivo, LineaControl : string; var Error:AnsiString):boolean;
    resourcestring
        MSG_ERROR_PROCESS       = 'Error al procesar las Patentes';
        MSG_ERROR_WRITE_LINE    = 'Error al escribir el registro en el Archivo de Patentes';
        MSG_PROCES_CANCELLED    = 'Proceso cancelado por el usuario';
        CAP_PROC_X_DE_Y         = 'Procesando el comprobante %d de %d';
    const
        CONST_TAMANO_BYTES_STRINGLIST = 25 * 1024 * 1024;			{25 MB}    // SS_1027_PDO_20120404
    var
        fsArchivoPatentes: TFileStream;                         // SS_1027_PDO_20120404
        TamanoStringList : Cardinal;	{hasta 4 mil millones}  // SS_1027_PDO_20120404
        Linea, Campo: AnsiString;
        ListaLineas :TStringList;		//REV.2
    begin
        try
    
            Result := False;
            Error := '';
            FMontoTotalPatentes := 0;
            //REV.2

            fsArchivoPatentes := TFileStream.Create(Trim(Archivo), fmCreate);       // SS_1027_PDO_20120404            
            
            ListaLineas := TStringList.Create;
            ListaLineas.Add(LineaControl);
            TamanoStringList := Length(LineaControl);                               // SS_1027_PDO_20120404            
            try
                // controlar si vale la pena el try...

                if spObtenerConsumosPorPatenteSerbanc.Active then spObtenerConsumosPorPatenteSerbanc.Close;
                //REV.2 with spObtenerConsumosPorPatenteSerbanc do begin
                spObtenerConsumosPorPatenteSerbanc.Parameters.ParamByName('@NumeroComprobante').Value := 0 ;
                spObtenerConsumosPorPatenteSerbanc.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value := CodigoEmpresaRecaudadora;
                While (StrToInt64(VarToStr(spObtenerConsumosPorPatenteSerbanc.Parameters.ParamByName('@NumeroComprobante').Value)) >= 0) and (not FCancelar) do begin
                    //CommandTimeOut := 500;
                    spObtenerConsumosPorPatenteSerbanc.Close;
                    spObtenerConsumosPorPatenteSerbanc.Open;
                    //REV.2 pbProgreso.Max := spObtenerConsumosPorPatenteSerbanc.RecordCount;
                    pbProgreso.Max := dbgPatentes.DataSource.DataSet.RecordCount;
                    with dbgPatentes.DataSource.DataSet do begin
                        First;
                        DisableControls;
                        while  (not EOF) AND (not FCancelar) do begin
                            Linea := '';
                            Campo := '';
                            // CodigoTipoCobranza - C2
                            Campo := FieldByName('CodigoTipoCobranza').AsString;
                            Campo := PadL(UpperCase(trim(Campo)),2,' ') ;
                            Linea := Linea + Campo;
                            Campo := '';
                            // CodigoTipoDeudor - N1
                            Campo := FieldByName('CodigoTipoDeudor').AsString;
                            Linea := Linea + Campo;
                            Campo := '';
                            // NumeroDocumento - C10 - (RUT)
                            Campo := FieldByName('NumeroDocumento').AsString;
                            Campo := PadR(UpperCase(trim(Campo)),10,' ');
                            Linea := Linea + Campo;
                            Campo := '';
                            // NumeroConvenio - C17
                            Campo := FieldByName('NumeroConvenio').AsString;
                            Campo := PadL(UpperCase(trim(Campo)),17,' ') ;
                            Linea := Linea + Campo;
                            Campo := '';
                            // Patente C10
                            Campo := FieldByName('Patente').AsString;
                            Campo := PadR(UpperCase(trim(Campo)),10,' ') ;
                            Linea := Linea + Campo;
                            Campo := '';
                            // TAG     C36
                            Campo := FieldByName('TAG').AsString;
                            Campo := PadL(UpperCase(trim(Campo)),36,'0') ;
                            Linea := Linea + Campo;
                            Campo := '';
                            // NumeroComprobante - N12
                            Campo := FieldByName('NumeroComprobante').AsString;
                            Campo := PadL(UpperCase(trim(Campo)),12,'0');
                            Linea := Linea + Campo;
                            Campo := '';
                            // TipoComprobante - C2
                            Campo := FieldByName('TipoComprobante').AsString;
                            Campo := PadL(UpperCase(trim(Campo)),2,' ');
                            Linea := Linea + Campo;
                            Campo := '';
                            // Monto - N16
                            Campo :=  FieldByName('Monto').asString;
                            Campo := PadL(trim(Campo),16 , '0');
                            Linea := Linea + Campo;
                            Campo := '';
                            // Concesionaria Due�a del TAG  - CHAR(20)
                            Campo := FieldByName('ConcesionariaDuenaTag').AsString;
                            Campo := PadR(UpperCase(trim(Campo)),20,' ');
                            Linea := Linea + Campo;
                            Campo := '';

                            FMontoTotalPatentes :=  FMontoTotalPatentes +  FieldByName('Monto').AsVariant;

                            ListaLineas.Add(Linea);

                            TamanoStringList := TamanoStringList + Length(Linea);            // SS_1027_PDO_20120404
                            if TamanoStringList >= CONST_TAMANO_BYTES_STRINGLIST then begin  // SS_1027_PDO_20120404
                                ListaLineas.SaveToStream(fsArchivoPatentes);                 // SS_1027_PDO_20120404
                                ListaLineas.Clear;                                           // SS_1027_PDO_20120404
                                TamanoStringList := 0;                                       // SS_1027_PDO_20120404
                            end;                                                             // SS_1027_PDO_20120404                            
                            {REV.2
                            try
                                WriteLn(Archivo, Linea);
                            except
                                on e : exception do begin
                                    Error := MSG_ERROR_WRITE_LINE + CRLF + e.Message;
                                    Exit;
                                end;
                            end;  }

                            pbProgreso.Position := spObtenerConsumosPorPatenteSerbanc.RecNo;
                            pbProgreso.Refresh;

                            if ((spObtenerConsumosPorPatenteSerbanc.RecNo mod 11) = 1 )then
                                lblReferencia.Caption := Format(CAP_PROC_X_DE_Y, [spObtenerConsumosPorPatenteSerbanc.RecNo,spObtenerConsumosPorPatenteSerbanc.RecordCount]);
                            Application.ProcessMessages ;

                            Next;
                        end; // while
                    
                        EnableControls;
                    end; //with       REV.2

                    lblReferencia.Caption := Format(CAP_PROC_X_DE_Y, [spObtenerConsumosPorPatenteSerbanc.RecordCount,spObtenerConsumosPorPatenteSerbanc.RecordCount]);
                    Application.ProcessMessages;
                end; //while
                //REV.2 end; //with

                try
                    if ListaLineas.Count > 0 then begin                      // SS_1027_PDO_20120404
                        ListaLineas.SaveToStream(fsArchivoPatentes);         // SS_1027_PDO_20120404
                    end;                                                     // SS_1027_PDO_20120404
                    // ListaLineas.SaveToFile(Archivo);                      // SS_1027_PDO_20120404
                except on e : exception do begin
                        Error := MSG_ERROR_WRITE_LINE + CRLF + e.Message;
                        Exit;
                    end;
                end;

                if not FCancelar then Result := True;
            except
                on e : exception do begin
                    Error := MSG_ERROR_PROCESS + CRLF + e.Message;
                    Exit;
                end;
            end; //try

            //ListaLineas.Free;
        finally                                                                   // SS_1027_PDO_20120404
            if Assigned(ListaLineas) then FreeAndNil(ListaLineas);                // SS_1027_PDO_20120404
            if Assigned(fsArchivoPatentes) then FreeAndNil(fsArchivoPatentes);    // SS_1027_PDO_20120404
        end;                                                                      // SS_1027_PDO_20120404 
    end;

resourcestring
    MSG_ERROR_SAVING_FILES = 'Error al grabar el Archivo de Patentes';
Const
    STR_CARGANDO_CONTROL_PT = 'Generando registro de control de archivo de Patentes';
    STR_CARGANDO_CONSUMOS = 'Procesando Detalle de consumos';
var
    //REV.2 pi: Integer;
    //REV.2 Archivo : TextFile;
    Archivo, Linea : string;

begin
    Result := False;
    //REV.2 try
        {REV.2
        AssignFile(Archivo, DevuelveNombreArchivo(FNombreArchivoConsumos,CodigoEmpresaRecaudadora));
        Rewrite(Archivo);}
        Archivo := DevuelveNombreArchivo(FNombreArchivoConsumos,CodigoEmpresaRecaudadora);
        try
            // Cargar Registro de Control
//            lblReferencia.Caption := STR_CARGANDO_CONTROL_PT;
            //REV.2 if CargarRegistroControlDetalle(Archivo, Error) then begin
            if CargarRegistroControlDetalle(Linea, Error) then begin

                // Cargar registros en el string list de salida
//                lblReferencia.Caption := STR_CARGANDO_CONSUMOS;
                Result := CargarDetalleConsumos(Archivo, Linea, Error);
            end;
        except
            on e : exception do begin
                Error := MSG_ERROR_SAVING_FILES + CRLF + e.Message;
            end;
        end;
    {REV.2
    Finally
        CloseFile(Archivo);
   end;}



{    Result := False;
    pi := pbProgreso.Position ;
    try
        try
            //Creo un archivo para guardar las Patentes
            AssignFile(Archivo, GoodDir(txtDestinoArchivo.Text) + FNombreArchivoConsumos);
            Rewrite(Archivo);

            //Cargando registro de control de consumos
            lblReferencia.Caption := STR_CARGANDO_CONTROL_PT;
            if not CargarRegistroControlDetalle( Archivo, Error ) then begin
                Exit;
            end;
            pbProgreso.StepIt;
            pbProgreso.Refresh ;

            //Cargar los detalles de Consumo en la string list
            lblReferencia.Caption := STR_CARGANDO_CONSUMOS;
            if not CargarDetalleConsumos(Archivo, Error) then begin
                Exit;
            end;
            pbProgreso.StepIt;
            pbProgreso.Refresh ;

            //Cierro el archivo
            CloseFile(Archivo);
        except
            on e : exception do begin
                Error := MSG_ERROR_SAVING_FILES +CRLF + e.Message;
                Exit;
            end;
        end;
        Result := True;
    finally
        pbProgreso.Position := pi + 3;;
        pbProgreso.Refresh ;
    end;}
end;

{******************************** Function Header ******************************
Function Name: EliminarArchivos
Author : ndonadio
Date Created : 09/08/2005
Description : Elimina los archivos creados
Parameters : None
Return Value : None
*******************************************************************************}
{procedure TfrmActivacionesSerbanc.EliminarArchivos;
begin
    try
        DeleteFile( GoodDir(txtDestinoArchivo.Text) + FNombreArchivoNK);
        DeleteFile( GoodDir(txtDestinoArchivo.Text) + FNombreArchivoConsumos );
    except
    end;
end;}

{******************************** Function Header ******************************
Function Name: RegistrarNotasEnLog
Author : ndonadio
Date Created : 09/08/2005
Description :   Registra las notas enviadas en el log de operaciones.
Parameters : CodOperacion: Integer; var Error: AnsiString
Return Value : Boolean

Revision : 1
    Author : pdominguez
    Date   : 19/05/2009
    Description : SS 803
            - Se a�adi� los par�metros CodigoEmpresaRecaudadora y RegistrosActualizados.
            - Con el par�metro RegistrosActualizados informamos del total de registros actualizados
            en el proceso, ya que si el proceso no devuelve un error y no procesa ning�n registro,
            se informa de ello.
            - Se a�adi� al SP spActualizarComprobantesEnviadosSerbanc el par�metro
            @CodigoEmpresasRecaudadoras.
*******************************************************************************}
function TfrmActivacionesSerbanc.RegistrarNotasEnLog(CodOperacion: Integer; var Error: AnsiString; CodigoEmpresaRecaudadora: Integer; var RegistrosActualizados: Integer): Boolean;
resourcestring
    ERROR_UPDATING   = 'Error (%d) al ejecutar la actualizacion de commprobantes enviados';
    CANT_ACTUALIZADA = '%d Comprobantes Enviados Actualizados';
var
   CantAct, Cantidad : Integer;
begin
    Result := False;
    try
        //DMConnections.BaseCAC.BeginTrans;                                             //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('BEGIN TRAN fActivacionesSerbanc');     //SS_1385_NDR_20150922
        CantAct  := 0;
        Cantidad := 1;
        while Cantidad > 0 do begin
            with spActualizarComprobantesEnviadosSerbanc do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoOperacionInterface').Value := CodOperacion;
                Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value := CodigoEmpresaRecaudadora;
                ExecProc ;
                Cantidad := Parameters.ParamByName('@RETURN_VALUE').Value;
            end;
            if Cantidad < 0 then Raise Exception.Create(Format(ERROR_UPDATING, [Cantidad]));
            CantAct := CAntAct + Cantidad;
            lblReferencia.Caption := Format( CANT_ACTUALIZADA, [CantAct]);
            lblReferencia.Refresh;
            application.ProcessMessages;
        end;

        RegistrosActualizados := CantAct;

        spInsertarErroresSerbanc.Parameters.ParamByName('@CodigoOperacionInterface').Value := CodOperacion;
        spInsertarErroresSerbanc.ExecProc;

        //if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;      //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('COMMIT TRAN fActivacionesSerbanc');					//SS_1385_NDR_20150922

        Result := True;
    except
        on E : Exception do begin
            //if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;                        //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fActivacionesSerbanc END');	//SS_1385_NDR_20150922

            Error := e.Message;
            Exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: MostrarReporteFinalizacion
Author : ndonadio
Date Created : 16/08/2005
Description : Lanza el reporte de finalizaci�n...
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmActivacionesSerbanc.MostrarReporteFinalizacion(CodigoOperacionInterfase: Integer);
resourcestring
    ERROR_REPORT    = 'No se puede mostrar el reporte';
var
    fr: TfrmReporteActivacionesSerbanc;
    descError: AnsiString;
begin
    try
        Application.Createform(TfrmReporteActivacionesSerbanc, fr);
        if not fr.MostrarReporte(CodigoOperacionInterfase, Caption, descError) then begin
            if TRIM(descError) <> '' then
                MsgBoxErr(ERROR_REPORT, descError, Caption, MB_ICONERROR);
            fr.Release;
            Exit;
        end else begin
            fr.Release;
        end;
    except
        on e:exception do begin
            MsgBoxErr(ERROR_REPORT, e.Message, Caption, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: LiberarDBRes
Author : ndonadio
Date Created : 24/08/2005
Description :   Elimina la tabla temporal y cierra los SP...
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmActivacionesSerbanc.LiberarDBRes;
resourcestring
    WARNING_CANNOT_DROP_TEMP_TABLE  = 'La tabla de trabajo temporal usada para este proceso no pudo ser eliminada.'+CRLF+
                                      'Por favor, cierre la aplicaci�n para completar este paso.'  ;
begin
    // Cierro los SP...
    spObtenerComprobantesPendientesActivacionSerbanc.Close;
    spObtenerConsumosPorPatenteSerbanc.Close;
    spActualizarComprobantesEnviadosSerbanc.Close;
    SQLQuery.Close;
    // libero las temporales si existe...
    // como puede fallar, lo pongo en un try...except para poder avisar que no se pudo hacer si falla...
    try
        spLimpiarTemporales.ExecProc;
    except
        on e:exception do begin
            MsgBox(WARNING_CANNOT_DROP_TEMP_TABLE + CRLF + '('+e.MEssage+')', Caption, MB_ICONWARNING);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author : ndonadio
Date Created : 05/08/2005
Description : Genera los archivos de salida para enviar a Serbanc.
Parameters : Sender: TObject
Return Value : None
******************************************************************************
Revision : 3
    Author : pdominguez
    Date   : 19/05/2009
    Description : SS 803
            - Se adapta todo el proceso para generar un fichero por empresa
            recaudadora.
            - Se implementa la posibilidad de generar los archivos de manera
            masiva.
*******************************************************************************}
procedure TfrmActivacionesSerbanc.btnProcesarClick(Sender: TObject);
resourcestring
    ERROR_REGISTERING_LOG           = 'Error al registrar operaci�n en log de interfaces para la empresa de Cobranza "%s".';
    ERROR_REGISTERING_ENDLOG        = 'Error al registrar cierre de operaci�n en log de interfaces para la empresa de Cobranza "%s".';
    ERROR_REGISTERING_NOTAS         = 'Error al registrar Comprobantes enviados para la empresa de Cobranza "%s".';
    ERROR_PROCESANDO_NOTAS          = 'Error al generar el archivo %s de Comprobantes para la empresa de Cobranza "%s".';
    ERROR_PROCESANDO_CONSUMOS       = 'Error al procesar el archivo %s de consumos por patente para la empresa de Cobranza "%s".';
    PROCESO_ARCHIVO_OK              = 'El proceso del archivo %s finaliz� con �xito';
    ERROR_FINISHING_PROCESS         = 'Error al intentar finalizar el proceso';
    ERROR_LOADING_INVOICES          = 'Error al cargar la lista de comprobantes pendientes para la empresa de Cobranza "%s".';
    MSG_NOTHING_TO_PROCESS          = 'No hay Notas de Cobro pendientes de procesar en este momento para la empresa de Cobranza "%s".';
    WARNING_CANNOT_DROP_TEMP_TABLE  = 'No se pudo eliminar la tabla temporal ##ComprobantesAActivarSerbanc. ';
    MSG_PROCESO_CANCELADO           = 'El Proceso ha sido cancelado por el usuario';
    MSG_PREPARANDO                  = 'Preparando para Procesar.'; //para el panel de avance...
    PROCESS_FINISHED_OK             = 'El proceso finaliz� con �xito';
    MSG_WARNING_FILE_ALREADY_EXISTS = 'IMPORTANTE: '+CRLF+' '+CRLF+ 'Los archivos ya existen en el directorio seleccionado.'+CRLF+ 'Si continua se sobreescribir�n. '+CRLF+ 'Seleccione otro directorio de destino si no desea sobreescribirlos.'; // por si existen los archivos
    MSG_SELECCIONE_EMPRESA          = 'Selecione una Empresa para lanzar el Proceso.';
    MSG_TOTAL_REGISTROS_ERRORES     = 'No se proces� ning�n registro de la selecci�n de Comprobantes a Enviar de la empresa de Cobranza "%s". ' + CRLF + 'Revise la tabla de Errores de Interfases. C�digo de Interfase: %d';
var
    DescError: AnsiString;
    Cantidad,
    IndiceEmpresa,
    IndiceCodigosOperacionesInterfases,
    RegistrosActualizados: Integer;
    ErroresEnProceso: Boolean;
    ListaCodigosOperacionesInterfases: TStringList;

{******************************** Function Header ******************************
Function Name: ExistenArchivos
Author : pdominguez
Date Created : 19/05/2009
Parameters : None
Return Value : Boolean
Description : SS 803
        - Comprueba si existe alg�n archivo ya generado de cualquiera de las
        empresas cargadas en el TVariantComboBox cbEmpresasRecaudadoras.
*******************************************************************************}
function ExistenArchivos: Boolean;
   Var
       Indice: Integer;
begin
   Result := False;
   case cbEmpresasRecaudadoras.ItemIndex of
       0: begin
           for Indice := 1 to cbEmpresasRecaudadoras.Items.Count - 1 do
               Result :=
                   Result or FileExists(DevuelveNombreArchivo(FNombreArchivoNK,cbEmpresasRecaudadoras.Items[Indice].Value));
       end;
   else
       Result := FileExists(DevuelveNombreArchivo(FNombreArchivoNK,cbEmpresasRecaudadoras.Items[cbEmpresasRecaudadoras.ItemIndex].Value));
  end;
end;

{******************************** Procedure Header *****************************
Procedure Name: EliminarArchivos
Author : pdominguez
Date Created : 19/05/2009
Parameters : CodigoEmpresaRecaudadora: Integer
Description : SS 803
        - Elimina los archivos existentes de la Empresa Recaudadora especificada
        en el par�metro CodigoEmpresaRecaudadora.
*******************************************************************************}
procedure EliminarArchivos(CodigoEmpresaRecaudadora: Integer);
   var
       Indice: Integer;
begin
    if FileExists(DevuelveNombreArchivo(FNombreArchivoNK,CodigoEmpresaRecaudadora)) then
        DeleteFile(DevuelveNombreArchivo(FNombreArchivoNK,CodigoEmpresaRecaudadora));
    if FileExists(DevuelveNombreArchivo(FNombreArchivoConsumos,CodigoEmpresaRecaudadora)) then
        DeleteFile(DevuelveNombreArchivo(FNombreArchivoConsumos,CodigoEmpresaRecaudadora));
end;

{******************************** Procedure Header *****************************
Procedure Name: GenerarArchivos
Author : pdominguez
Date Created : 19/05/2009
Parameters : CodigoEmpresaRecaudadora: Integer; DescripcionEmpresaRecaudadora: String
Description : SS 803
        - Genera los archivos de comprobantes y patentes de la Empresa Recaudadora
        especificada en el par�metro CodigoEmpresaRecaudadora.
*******************************************************************************}
procedure GenerarArchivos(CodigoEmpresaRecaudadora: Integer; DescripcionEmpresaRecaudadora: String);
begin
    // Insertar inicio de proceso en Log de Operaciones
    lblReferencia.Caption := 'Registrando operaci�n en Log de Interfases.';
    Application.ProcessMessages;
    if not RegistrarOperacionEnLogInterface(
        DMConnections.BaseCac,
        RO_MOD_INTERFAZ_SALIENTE_ACTIVACION_SERBANC,
        ExtractFileName(DevuelveNombreArchivo(FNombreArchivoNK, CodigoEmpresaRecaudadora)) + '; ' + ExtractFileName(DevuelveNombreArchivo(FNombreArchivoConsumos,CodigoEmpresaRecaudadora)),
        UsuarioSistema,'',False,False,NowBase(DMConnections.BaseCAC), 0, FCodigoOperacionInterfase, DescError) then begin
        MsgBoxErr(Format(ERROR_REGISTERING_LOG,[DescripcionEmpresaRecaudadora]), DescError, Caption, MB_ICONERROR);
        ErroresEnProceso := True;
        Exit;
    end;


    lblReferencia.Caption := 'Preparando Comprobantes Pendientes ...';
    Application.ProcessMessages;
    if not PrepararComprobantesPendientes(descError, Cantidad, CodigoEmpresaRecaudadora) then begin
        // si fallo... vuelvo todo al estado inicial y salgo
        FProcesando := False;
        HabilitarBotones;
        MsgBoxErr(Format(ERROR_LOADING_INVOICES,[DescripcionEmpresaRecaudadora]), DescError, Caption, MB_ICONERROR);
        ErroresEnProceso := True;
        Exit;
    end
    else begin
        if Cantidad = 0 then begin
            MsgBox(format(MSG_NOTHING_TO_PROCESS,[DescripcionEmpresaRecaudadora]), Caption, MB_ICONINFORMATION);
            Exit;
        end
        else begin
            lblReferencia.Caption := 'Obteniendo Comprobantes Pendientes ...';
            Application.ProcessMessages;
            EliminarArchivos(CodigoEmpresaRecaudadora);
            // Llamo a los otros stores para obtener la data a procesar
            if not ObtenerComprobantesPendientes(descError, CodigoEmpresaRecaudadora) then begin
                if FCancelar then Exit;
                MsgBoxErr(Format(ERROR_LOADING_INVOICES,[DescripcionEmpresaRecaudadora]), descError, Caption, MB_ICONSTOP);
                ErroresEnProceso := True;
            end
            else begin
                lblReferencia.Caption := 'Generando Archivo de Comprobantes ...';
                pbProgreso.Position := 0;
                Application.ProcessMessages;
                if not GenerarArchivoComprobantes(descError, CodigoEmpresaRecaudadora) then begin
                    EliminarArchivos(CodigoEmpresaRecaudadora);
                    if FCancelar then Exit;
                    MsgBoxErr(Format(ERROR_PROCESANDO_NOTAS,[ExtractFileName(FNombreArchivoNK),DescripcionEmpresaRecaudadora]), descError, Caption, MB_ICONSTOP);
                    ErroresEnProceso := True;
                end
                else begin
                    lblReferencia.Caption := 'Generando Archivo de Patentes ...';
                    pbProgreso.Position := 0;
                    Application.ProcessMessages;
                    if not GenerarArchivoPatentes(descError, CodigoEmpresaRecaudadora) then begin
                        EliminarArchivos(CodigoEmpresaRecaudadora);
                        if FCancelar then Exit;
                        MsgBoxErr(Format(ERROR_PROCESANDO_CONSUMOS,[ExtractFileName(FNombreArchivoConsumos),DescripcionEmpresaRecaudadora]), descError, Caption, MB_ICONSTOP);
                        ErroresEnProceso := True;
                    end
                    //Registro la finalizaci�n Correcta de archivo en el log de operaciones
                    else begin

                        // Inserto los registros en el Detalle de Activaciones Enviadas (Log)
                        lblReferencia.Caption := 'Actualizando detalle de Activaciones Enviadas ...';
                        Application.ProcessMessages;
                        RegistrosActualizados := 0;
                        if not RegistrarNotasEnLog(FCodigoOperacionInterfase, DescError, CodigoEmpresaRecaudadora, RegistrosActualizados) then begin
                            MsgBoxErr(Format(ERROR_REGISTERING_NOTAS,[DescripcionEmpresaRecaudadora]), DescError, Caption, MB_ICONERROR);
                            EliminarArchivos(CodigoEmpresaRecaudadora);
                            ErroresEnProceso := True;
                        end
                        else begin
                            if FCancelar then Exit;
                            if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacionInterfase, 'Finalizado OK!', DescError) then begin
                                MsgBoxErr(Format(ERROR_REGISTERING_ENDLOG,[DescripcionEmpresaRecaudadora]), DescError, Caption, MB_ICONERROR);
                                ErroresEnProceso := True;
                            end;
                            if RegistrosActualizados = 0 then
                                MsgBoxErr(Format(MSG_TOTAL_REGISTROS_ERRORES,[DescripcionEmpresaRecaudadora, FCodigoOperacionInterfase]), DescError, Caption, MB_ICONERROR)
                            else ListaCodigosOperacionesInterfases.Add(IntToStr(FCodigoOperacionInterfase));
                        end;
                    end;
                end;
            end;
        end;
    end;
end;

begin
    // Validar los nombres de archivos
    CrearNombresArchivos(FDestino);

    if ExistenArchivos then
        if MsgBox(MSG_WARNING_FILE_ALREADY_EXISTS, Caption, MB_ICONWARNING+MB_YESNO) = mrNo then Exit;
    // seteo valores
    FProcesando := True;
    ErroresEnProceso := False;
    HabilitarBotones;
    lblEmpresa.Caption := 'Iniciando Procesos. Espere Por Favor ...';
    Application.ProcessMessages;

    ListaCodigosOperacionesInterfases := TStringList.Create;
    // Muestro el panel de avance y seteo los limites iniciales del progressbar
    // Obtengo los comprobantes a procesar
    try
        if FCancelar then Exit;
        case cbEmpresasRecaudadoras.ItemIndex of
            0:
                for IndiceEmpresa := 1 to cbEmpresasRecaudadoras.Items.Count - 1 do begin
                    lblEmpresa.Caption := Format('Procesando empresa "%s".',[Trim(cbEmpresasRecaudadoras.Items[IndiceEmpresa].Caption)]);
                    Application.ProcessMessages;
                    GenerarArchivos(cbEmpresasRecaudadoras.Items[IndiceEmpresa].Value, Trim(cbEmpresasRecaudadoras.Items[IndiceEmpresa].Caption));
               end;
        else
            lblEmpresa.Caption := Format('Procesando empresa "%s".',[Trim(cbEmpresasRecaudadoras.Items[cbEmpresasRecaudadoras.ItemIndex].Caption)]);
            Application.ProcessMessages;
            GenerarArchivos(cbEmpresasRecaudadoras.Items[cbEmpresasRecaudadoras.ItemIndex].Value, Trim(cbEmpresasRecaudadoras.Items[cbEmpresasRecaudadoras.ItemIndex].Caption));
        end;

    finally
        if FCancelar then begin

              //registro que el proceso fue cancelado
              RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacionInterfase);

              MsgBox(MSG_PROCESO_CANCELADO, Caption, MB_ICONSTOP);
        end
        else if not ErroresEnProceso then begin
            if ListaCodigosOperacionesInterfases.Count > 0 then
                for IndiceEmpresa := 0 to ListaCodigosOperacionesInterfases.Count - 1 do
                    MostrarReporteFinalizacion(StrToInt(ListaCodigosOperacionesInterfases[IndiceEmpresa]));
            
            MsgBox(PROCESS_FINISHED_OK, Caption, MB_ICONINFORMATION);
        end;
        FProcesando := False; // aviso que ya no estoy procesando
        HabilitarBotones; // re habilito botones y reinicio el progressbar
        // libero los recursos de DB...(sp's, temps, etc)
        LiberarDBRes;
        if Assigned(ListaCodigosOperacionesInterfases) then FreeAndNil(ListaCodigosOperacionesInterfases);
        Close;
    end;

end;

{******************************** Function Header ******************************
Function Name: btnCancelarClick
Author : ndonadio
Date Created : 09/08/2005
Description :  Que hacemos cuando se presiona el boton cancelar
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmActivacionesSerbanc.btnCancelarClick(Sender: TObject);
begin
    FCancelar := True;
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : ndonadio
Date Created : 05/08/2005
Description : Cierra el form.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmActivacionesSerbanc.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : ndonadio
Date Created : 05/08/2005
Description : Inhibe el cierre del form si esta procesando
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmActivacionesSerbanc.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    // Si est� procesando no deja cerrar...
    CanClose := not FProcesando;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : ndonadio
Date Created : 05/08/2005
Description : lo libero de memoria
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmActivacionesSerbanc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

function TfrmActivacionesSerbanc.DevuelveNombreArchivo(MascaraArchivo: String; CodigoEmpresa: Integer): String;
begin
   Result :=
       Format(MascaraArchivo, [StringOfChar('0', 2 - Length(IntToStr(CodigoEmpresa))) + IntToStr(CodigoEmpresa)]);
end;

end.
