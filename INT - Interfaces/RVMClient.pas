{-----------------------------------------------------------------------------
 File Name: RVMClient.pas
 Author:    gcasais
 Date Created: 22/02/2005
 Language: ES-AR
 Description: Componente Cliente para conexi�n al RNVM
-----------------------------------------------------------------------------}
unit RVMClient;

interface

uses
  Windows, Messages, SysUtils, Classes, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, XMLDoc, XMLIntf, XMLDOM, RVMTypes,
  RVMBind, Util, UtilProc, Dialogs; 
Const
    CLIENT_ID = 'DPS RVM Client 1.0';
type
  TRVMClient = class(TObject)
  private
    FUserName: AnsiString;
    FPassword: AnsiString;
    FServiceID: AnsiString;
    FServiceURL: AnsiString;
    FSaveDirectory: AnsiString;
    FSaveFileName: AnsiString;
    FServicePort: Integer;
    FHttpClient: TIdHTTP;
    FUseProxy: Boolean;
    FProxyServer: AnsiString;
    FProxyServerPort: Integer;
    FContentType: AnsiString;
    function GetUserName: String;
    procedure SetUsername(const Value: String);
    function GetPassWord: String;
    procedure SetPassWord(const Value: String);
    function GetServiceID: String;
    procedure SetServiceID(const Value: String);
    function GetServiceURL: String;
    procedure SetServiceURL(const Value: String);
    function GetServicePort: Integer;
    procedure SetServicePort(const Value: Integer);
    function GetSaveDirectory: String;
    procedure SetSaveDirectory(const Value: String);
    function GetSaveFileName: String;
    procedure SetSaveFileName(const Value: String);
    function GetProxyServer: String;
    procedure SetProxyServer(const Value: String);
    function GetProxyServerPort: Integer;
    procedure SetProxyServerPort(const Value: Integer);
    function GetContentType: String;
    procedure SetContentType(const Value: String);
  protected
    procedure ConfigureClient;
    function FillData(RVMResp: TXMLDocument): TRVMInformation;
  public
    constructor Create;
    destructor  Destroy; override;
    function RequestLPData(LicensePlate: String; var RVMInformation: TRVMInformation): Boolean;
  published
    property Username: String read GetUserName write SetUsername;
    property Password: String read GetPassWord write SetPassWord;
    property ServiceID: String read GetServiceID write SetServiceID;
    property ServiceURL: String read GetServiceURL write SetServiceURL;
    property ServicePort: Integer read GetServicePort write SetServicePort;
    property SaveDirectory: String read GetSaveDirectory write SetSaveDirectory;
    property FileName: String read GetSaveFileName write SetSaveFileName;
    property RequestUseProxy: Boolean read FUseproxy write FUseProxy;
    property RequestProxyServer: String read GetProxyServer write SetProxyServer;
    property RequestProxyServerPort: Integer read GetProxyServerPort write SetProxyServerPort;
    property RequestContentType: String read GetContentType write SetContentType;
  end;


implementation

{ TRVMClient }
{-----------------------------------------------------------------------------
  Function Name: ConfigureClient
  Author:    gcasais
  Date Created: 25/02/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TRVMClient.ConfigureClient;
begin
    FHttpClient.Request.Username    := UserName;
    FHttpClient.Request.Password    := Password;
    FHttpClient.Host                := ServiceUrl;
    FHttpClient.Request.ContentType := RequestContentType; //'application/x-www-form-urlencoded';
    if RequestUseProxy then begin
        FHttpClient.Request.ProxyServer := RequestProxyServer; //'172.16.1.125';
        FHttpClient.Request.ProxyPort   := RequestProxyServerPort; //8080
    end else begin
        FHttpClient.Request.ProxyServer := '';
        FHttpClient.Request.ProxyPort   := 0;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: Create
  Author:    gcasais
  Date Created: 25/02/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
constructor TRVMClient.Create;
begin
    FHttpClient := TIdHTTP.Create(nil);
end;
{-----------------------------------------------------------------------------
  Function Name: Destroy
  Author:    gcasais
  Date Created: 25/02/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
destructor TRVMClient.Destroy;
begin
    FreeAndNil(FHttpClient);
    inherited;
end;
{-----------------------------------------------------------------------------
  Function Name: GetPassWord
  Author:    gcasais
  Date Created: 25/02/2005
  Description:
  Parameters: None
  Return Value: String
-----------------------------------------------------------------------------}
function TRVMClient.GetPassWord: String;
begin
    Result := FPassWord;
end;

function TRVMClient.GetServiceID: String;
begin
    Result := FServiceID;
end;

function TRVMClient.GetServicePort: Integer;
begin
    Result := FServicePort;
end;

function TRVMClient.GetServiceURL: String;
begin
    Result := FServiceURL;
end;

function TRVMClient.GetUserName: String;
begin
    Result := FUserName;
end;

{-----------------------------------------------------------------------------
  Function Name: RequestLPData
  Author:    gcasais
  Date Created: 22/02/2005
  Description: Obtiene la informacion del RVM para una patente dada
  Parameters: LicensePlate: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TRVMClient.RequestLPData(LicensePlate: String; var RVMInformation: TRVMInformation):Boolean;
resourcestring
    HARD_XML =
      '<srcei><datos><user>%s</user><tipo>RVMDIR</tipo><param>%s</param></datos></srcei>';
var
    AskXML: String;
    Response: TStringStream;
    RespXML: TXMLDocument;
    TempString: TStringList;
    Buff: String;
begin
    Result := False;
    if Trim(LicensePlate) = '' then begin
        raise exception.Create('Debe especificarse una patente');
        Exit;
    end;
    LicensePlate := UpperCase(LicensePlate);
    TempString  := TStringList.Create;
    Response    := TStringStream.Create('');
    ConfigureClient;
    AskXML := Format(HARD_XML, [ServiceID, LicensePlate]);
    TempString.Add(AskXML);
    RespXML := TXMLDocument.Create(nil);
    RespXML.DOMVendor := GetDOMVendor('MSXML');
    RespXML.Options := RespXML.Options + [doNodeAutoIndent];
    try
        try
            try
                RespXML.Active := True;
            except
                on e: exception do begin
                    raise exception.Create('Error activando el contenedor XML ' + e.Message);
                    Exit;
                end;
            end;
            FHttpClient.Post(ServiceURL, TempString, Response);
            // Esta es una manganeta para determinar si la patente
            // por la que se pregunto no existe en la BD del RVM
            Buff:= Response.DataString;
            if Pos(LicensePlate, Buff) <= 0 then begin
               MsgBoxErr('Error', buff, 'RNVM', MB_ICONWARNING);
               Exit;
            end;
            Buff := '';
            // Listo, seguimos cargando la respuesta a un Doc XML
            RespXML.LoadFromStream(Response);
            //Guardamos el XML si nos pidieron hacerlo
            if DirectoryExists(SaveDirectory) then begin
                try
                    RespXML.SaveToFile(SaveDirectory + FileName);
                except
                    on e: Exception do begin
                        MsgBoxErr('Error guardando el XML de respuesta ' + CRLF +
                          SaveDirectory + FileName, e.Message, 'Error', MB_ICONERROR);
                    end;
                end;
            end;
            //Lo parseamos..
            RVMInformation := FillData(RespXML);
            //Listo!
            Result := True;
        except
            on e: Exception do begin
                raise Exception.Create('Error sending request ' + e.Message);
            end;
        end;
    finally
        FreeAndNil(TempString);
        FreeAndNil(Response);
    end;
end;


procedure TRVMClient.SetPassWord(const Value: String);
begin
    if Value = FPassWord then Exit;
    FPassWord := Value;
end;

procedure TRVMClient.SetServiceID(const Value: String);
begin
    if Value  = FServiceID then Exit;
    FServiceID := Value;
end;

procedure TRVMClient.SetServicePort(const Value: Integer);
begin
    if Value = FServicePort then Exit;
    FServicePort := Value;
end;

procedure TRVMClient.SetServiceURL(const Value: String);
begin
    if Value = FServiceURL then Exit;
    FServiceURL := Value;
end;

procedure TRVMClient.SetUsername(const Value: String);
begin
    if Value = FUserName then exit;
    FUsername := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: FillData
  Author:    gcasais
  Date Created: 22/02/2005
  Description: Convierte la respuesta en formato XML al record con la data
  Parameters: RVMResponse: TXMLDocument
  Return Value: TRVMInformation
-----------------------------------------------------------------------------}
function TRVMClient.FillData(RVMResp: TXMLDocument): TRVMInformation;
var
    Doc: IXMLCertxmlType;
begin
    if not RVMResp.Active then RVMResp.Active := True;
    //Hacemos el bind al documento (DOM)
    Doc := Getcertxml(RVMResp);
    //Llenamos nuestro record
    // Datos del veh�culo
    try
        Result.RequestedLicensePlate                := Doc.Patente.Fieldname1;
        Result.VehicleInformation.Category          := Doc.Auto.Fieldname1;
        Result.VehicleInformation.Manufacturer      := Doc.Auto.Fieldname2;
        Result.VehicleInformation.Model             := Doc.Auto.Fieldname3;
        Result.VehicleInformation.Color             := Doc.Auto.Fieldname4;
        Result.VehicleInformation.EngineID          := Doc.Auto.fieldname5;
        Result.VehicleInformation.ChassisID         := Doc.Auto.Fieldname6;
        Result.VehicleInformation.Serial            := Doc.Auto.Fieldname7;
        Result.VehicleInformation.VINNumber         := Doc.Auto.Fieldname8;
        Result.VehicleInformation.Year              := Doc.Auto.Fieldname9;
    except
        raise exception.Create('Error obteniendo los datos del veh�culo');
    end;
    // Datos del seguro, si los hubiera
    try
        Result.InsuranceInformation.Entity          := Doc.ConSeguro.Fieldname1;
        Result.InsuranceInformation.ContractID      := Doc.ConSeguro.Fieldname2;
        Result.InsuranceInformation.ExpirationDate  := Doc.ConSeguro.Fieldname3;
    except
        on e: Exception do begin
            Result.InsuranceInformation.Entity  := 'No existen datos referidos al seguro';
        end;
    end;
    // Datos del titular
    try
        Result.OwnerInformation.OwnerID1            := Doc.Amo.Fieldname1;
        Result.OwnerInformation.OwnerName1          := Doc.Amo.Fieldname2;
    except
        Result.OwnerInformation.OwnerID1 := 'No se pudieron obtener todos los datos del titular';
    end;
    // Datos del domicilio
    try
        Result.AddressInformation.Street     := Doc.Dire.Fieldname1;
        Result.AddressInformation.DoorNumber := Doc.Dire.FieldName2;
        Result.AddressInformation.Letter     := Doc.Dire.Fieldname3;
        Result.AddressInformation.OtherInfo  := Doc.Dire.Fieldname4;
        Result.AddressInformation.Commune    := Doc.Dire.Fieldname5;
        Result.AddressInformation.Field6     := doc.Dire.Fieldname6;
    except
        on e: Exception do begin
            Result.AddressInformation.Street := 'No se pudieron obtener los datos del domicilio' + CRLF +
            e.Message;
        end;
    end;
    // Anotaciones del RVM Para este veh�culo
    try
        Result.VehicleNotes := doc.TituloAnot;
    except
        Result.VehicleNotes := 'No hay anotaciones para el t�tulo de este veh�culo';
    end;

    // Detalle de las anotaciones, si las hubiera.
    try
        Result.DomainConstraints.Notas[1]:= Doc.Anotacion1.Fieldname1;
        Result.DomainConstraints.Notas[2]:= Doc.Anotacion1.Fieldname2;
        Result.DomainConstraints.Notas[3]:= Doc.Anotacion1.Fieldname3;
        Result.DomainConstraints.Notas[4]:= Doc.Anotacion1.Fieldname4;
        Result.DomainConstraints.Notas[5]:= Doc.Anotacion1.Fieldname5;
        Result.DomainConstraints.Notas[6]:= Doc.Anotacion1.Fieldname6;
        Result.DomainConstraints.Notas[7]:= Doc.Anotacion1.Fieldname7;
        Result.DomainConstraints.Notas[8]:= Doc.Anotacion1.Fieldname8;
        Result.DomainConstraints.Notas[9]:= Doc.Anotacion1.Fieldname9;
        Result.DomainConstraints.Notas[10]:= Doc.Anotacion1.Fieldname10;
        Result.DomainConstraints.Notas[11]:= Doc.Anotacion1.Fieldname11;

    except
    end;

    try
        Result.DomainRequests.Nota1 := doc.SinSolicitudes.Fieldname1;
        Result.DomainRequests.Nota2 := doc.TituloAmos;
        Result.OthersOwnersInformation.OwnerID1 := doc.AmoAnt.Fieldname1;
        Result.OthersOwnersInformation.OwnerName1 := doc.AmoAnt.Fieldname2;
    except
    end;
end;

function TRVMClient.GetSaveDirectory: String;
begin
    Result := GoodDir(FSaveDirectory);
end;

procedure TRVMClient.SetSaveDirectory(const Value: String);
begin
    if Value = FSaveDirectory then Exit;
    FSaveDirectory := Value;
end;

function TRVMClient.GetSaveFileName: String;
begin
    Result := FSaveFilename;
end;

procedure TRVMClient.SetSaveFileName(const Value: String);
begin
    if Value = FSaveFileName then Exit;
    FSaveFileName := Value;
end;

function TRVMClient.GetProxyServer: String;
begin
    Result := FProxyServer;
end;

procedure TRVMClient.SetProxyServer(const Value: String);
begin
    if Value = FProxyServer then Exit;
    FProxyServer := Trim(Value);
end;

function TRVMClient.GetProxyServerPort: Integer;
begin
    Result := FProxyServerPort;
end;

procedure TRVMClient.SetProxyServerPort(const Value: Integer);
begin
    if Value <= 0 then begin
        raise Exception.Create('El port debe ser mayor que cero');
        Exit;
    end;
    if Value = FProxyServerPort then Exit;
    FProxyServerPort := Value;
end;

function TRVMClient.GetContentType: String;
begin
    Result := FContentType;
end;

procedure TRVMClient.SetContentType(const Value: String);
begin
    if Trim (Value) = '' then begin
        raise Exception.Create('Debe especificarse un Content Type para el Request');
        Exit;
    end;
    if Value = FContentType then Exit;
    FContentType := Value;
end;
end.
