﻿{-----------------------------------------------------------------------------
  Firma             : SS-1015-NDR-20120425
  Description       : Generacion de Arhchivos de IVA otras concesionarias

  Fecha         : 29/08/2012
  Firma         : SS_1006_1015_CQU_20120829
  Description   : Se modifica la obtención del código de del Modulo, ahora se usa una función y no es necesaria
                  la consulta SQL.
                  Se modifica la función que graba el Log al final de la operación para que cuando no encuentre datos lo indique.

  Fecha         : 27/09/2012
  Firma         :  SS_1006_1015_CQU_20120926
  Description   : Se debe guardar sólo el nombre de archivo cuando se guarda el histórico

  Fecha         : 28/09/2012
  Firma         : SS_1006_1015_CQU_20120928
  Descripcion   : Se modifica el comportamiento cuando no encuentra registros, ahora se muestra sólo una advertencia
                  y no un mensaje de error.

  Firma         : SS_1128_NDR_20131030
  Descripcion   : Guarda primero los registros y despues el archivo.
                  Si el archivo ya existe y el usuario NO lo desea reemplazar, no borrar el archivo existente

  Firma       : SS_1147_NDR_20141216
  Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

}

unit FrmGenerarArchivoIVAConcesionarias;

interface

uses
    StrUtils, Util, UtilProc, PeaProcs, DMConnection, ConstParametrosGenerales,
    DmiCtrls, ComunesInterfaces, UtilDB, PeaTypes,
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, ExtCtrls, StdCtrls, Validate, DateEdit, VariantComboBox, DB, ADODB,
    ComCtrls, PeaProcsCN;   // SS_1006_1015_CQU_20120829

type
  TFGenerarArchivoIVAConcesionarias = class(TForm)
    Bevel1: TBevel;
    edFechaDesde: TDateEdit;
    edFechaHasta: TDateEdit;
    lblFechaDesde: TLabel;
    lblFechaHasta: TLabel;
    btnSalir: TButton;
    btnProcesar: TButton;
    lblConcesionaria: TLabel;
    vcbConcesionarias: TVariantComboBox;
    lblNombreArchivo: TLabel;
    dlgSelArchivo: TOpenDialog;
    txtArchivo: TPickEdit;
    PnlAvance: TPanel;
    qryConsulta: TADOQuery;
    pbProgreso: TProgressBar;
    lblMensaje: TLabel;
    lblProgreso: TLabel;
    spObtenerIVAConcesionaria: TADOStoredProc;
    spAgregaHistoricoIVAConcesionaria: TADOStoredProc;
    procedure btnSalirClick(Sender: TObject);
    procedure txtArchivoButtonClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure vcbConcesionariasChange(Sender: TObject);
    procedure edFechaDesdeChange(Sender: TObject);
    procedure edFechaHastaChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    RutaTransaccion: AnsiString;
    NombreArchivo: AnsiString;
    FCodigoConcesionaria: AnsiString;
    FDirectorioIVAOtrasConc : AnsiString;
    FCancelar : Boolean;
    FCodigoOperacion : Integer;
    FNombreArchivo : AnsiString;
    FFechaDesde : Variant;
    FFechaHasta : Variant;
    TipoInterface : string;
    FErrorGrave : Boolean;
    //CodigoModulo : string; // Obtengo el Codigo del Modulo a Ejecutar // SS_1006_1015_CQU_20120829
    CodigoModulo : Integer;                                             // SS_1006_1015_CQU_20120829
    DirectorioSeleccionado : string;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    Function GenerarNombreArchivo(FechaInicio, FechaTermino : TDateEdit) : string;
    Function RegistrarOperacion : Boolean;
    Function GuardarArchivo : Boolean;
    Function ActualizarLog : Boolean;
    function GuardarHistorico : Boolean;
    function ObtenerIVAConcesionaria: Boolean;
    Procedure CambiarNombreArchivo();
    Function VerificarParametrosGenerales : boolean;
  public
    { Public declarations }
    function Inicializar(txtCaption: ANSIString; MDIChild:Boolean; TipoInterfaceIndicada : string) : Boolean;

  end;

const
    DIRECTORIO_DESTINO = 'CARPETA_SALIDA_IVA_OTRAS_CONC';
    MSG_ERROR_CONCESION = 'Debe seleccionar una concesionaria';
    MSG_ERROR_DIRECTORIO = 'El directorio indicado en los parámetros no existe, verifique Parametros Generales';
    MSG_ERROR_TITULO = 'Atención';
var
    FGenerarArchivoIVAConcesionarias: TFGenerarArchivoIVAConcesionarias;
    bCargando : boolean;
    FLista : TStringList;
    SinDatos : String; // SS_1006_1015_CQU_20120829


implementation

{$R *.dfm}
function TFGenerarArchivoIVAConcesionarias.Inicializar(txtCaption: ANSIString; MDIChild:Boolean; TipoInterfaceIndicada: string): Boolean;

    {-----------------------------------------------------------------------------
      Function Name: CargarCombo
      Author:    NDR
      Date Created: 25-04-2012
      Description: Elimina la concesionaria Costanera Norte del Combo.
      Parameters: None
      Return Value: None
    -----------------------------------------------------------------------------}
    procedure CargarCombo;
    var indice : Integer;
    begin
        CargarComboOtrasConcesionarias(DMConnections.BaseCAC, vcbConcesionarias, True, True);
        vcbConcesionarias.ItemIndex := 0;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';

begin
    Result := false;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    TipoInterface := TipoInterfaceIndicada;
    bCargando := true;
    if not MDIChild then begin
		  FormStyle := fsNormal;
		  Visible := False;
    end;
    CenterForm(Self);
    try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected And VerificarParametrosGenerales;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
    FCancelar := False;                             //inicio cancelar en false
    FCodigoOperacion := 0;                          //Inicializo el codigo de operacion
    CargarCombo;                                    // Cargo los datos en el Combo Concesionarias
    btnProcesar.Enabled := Result;                  // Si está todo bien (Result) habilito el botón
    // Obtengo el Codigo del Módulo
    //CodigoModulo := QueryGetValue(DMConnections.BaseCAC, 'SELECT CodigoModulo FROM Modulos WITH (NOLOCK) WHERE Descripcion = ''Interfaz Saliente Generar Archivo IVA Otras Concesionarias''');    // SS_1006_1015_CQU_20120829
    CodigoModulo :=  ObtenerCodigoModuloGenerarArchivoIvaOC(DMConnections.BaseCAC); // SS_1006_1015_CQU_20120829
    FNombreArchivo := GenerarNombreArchivo(edFechaDesde, edFechaHasta); // Genero el nombre del archivo
    txtArchivo.Text := FNombreArchivo;              // Coloco el nombre del archivo en el texto
    PnlAvance.Visible := False;
    Caption := AnsiReplaceStr (txtCaption, '&', '');
    // Terminó de Cargar
    bCargando := False;
end;


{-----------------------------------------------------------------------------
  Function Name: VerificarParametrosGenerales
  Author:    NDR
  Date Created: 25-04-2012
  Description: Verifica que el parametro de directorio exista en la BD y físicamente.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFGenerarArchivoIVAConcesionarias.VerificarParametrosGenerales : boolean;
resourcestring
    MSG_ERROR_CHECK_GENERAL   = 'Error al verificar parametro ';
    MSG_ERROR                 = 'Error';
    STR_NOT_EXISTS_GENERAL    = 'No existe parametro: ';
    STR_EMPTY_GENERAL         = 'Parametro vacio: ';
    STR_DIRECTORY_NOT_EXISTS  = 'No existe el directorio indicado en parametros generales: ' + CRLF;
var
    DescError : AnsiString;
    sParametroDirectorio : string;
begin
    Result    := True;
    DescError := '';
    try
        sParametroDirectorio := DIRECTORIO_DESTINO;

        try
            //Obtengo el Parametro General
            if not ObtenerParametroGeneral(DMConnections.BaseCAC, sParametroDirectorio , FDirectorioIVAOtrasConc) then begin
                DescError := STR_DIRECTORY_NOT_EXISTS + sParametroDirectorio;
                Result := False;
                Exit;
            end;
            //Verifico que sea válido
            FDirectorioIVAOtrasConc := GoodDir(FDirectorioIVAOtrasConc);
            if  not DirectoryExists(FDirectorioIVAOtrasConc) then begin
                DescError := STR_DIRECTORY_NOT_EXISTS + FDirectorioIVAOtrasConc;
                Result := False;
                Exit;
            end;
        except
            on e: Exception do begin
                Result := False;
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL, e.Message, MSG_ERROR, MB_ICONWARNING);
            end;
        end;

    finally
        //si no pasó la verificación de parametros generales
        if not (Result) then begin
            //informo la situación
            MsgBoxErr(MSG_ERROR_CHECK_GENERAL, DescError, MSG_ERROR, MB_ICONWARNING);
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: CambiarNombreArchivo
  Author:    NDR
  Date Created: 25-04-2012
  Description: Crea un nuevo nombre de archivo en base a los datos del formulario
                y actualiza el campo de texto que lo despliega.
  Parameters: FechaInicio, FechaTermino: TDateEdit
  Return Value: string
-----------------------------------------------------------------------------}
function TFGenerarArchivoIVAConcesionarias.GenerarNombreArchivo(FechaInicio, FechaTermino : TDateEdit) : string;
resourcestring
    MSG_FILE_NAME_ERROR = 'Error al crear el nombre del archivo';
    MSG_DIRECTORY_ERROR = 'El directorio de salida %s no existe.' + CRLF +
                          'Verifique los parámetros generales.';
var
    NombreArchivo : string;
begin
    try
        NombreArchivo := 'IVA_'+'01_'+'0'+FCodigoConcesionaria
        + '_'+FormatDateTime ('yyyymmdd', FechaInicio.Date)
        + '_' + FormatDateTime ('yyyymmdd', FechaTermino.Date)
        + '_' + FormatDateTime ('yyyymmddHHnnss', NowBase(DMConnections.BaseCAC))
        + '.txt';
        // Verifico que el directorio seleccionado no haya cambiado
        // por el directorio de los parámetros
        if (NOT (DirectorioSeleccionado = '') AND (DirectorioSeleccionado <> FDirectorioIVAOtrasConc)) then
            FDirectorioIVAOtrasConc := DirectorioSeleccionado;
        Result := GoodFileName(FDirectorioIVAOtrasConc + NombreArchivo, 'txt');
    except
        raise Exception.Create(MSG_FILE_NAME_ERROR);
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: CambiarNombreArchivo
  Author:    NDR
  Date Created: 25-04-2012
  Description: Cambia el nombre de archivo y actualiza el campo de texto que lo despliega.
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoIVAConcesionarias.CambiarNombreArchivo();
begin
    FNombreArchivo := GenerarNombreArchivo(edFechaDesde, edFechaHasta);
    txtArchivo.Text := FNombreArchivo;
    if (NOT btnProcesar.Enabled) then btnProcesar.Enabled := True;

    txtArchivo.Refresh;
end;


{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    NDR
  Date Created: 25-04-2012
  Description: Comienza el proceso de generación del archivo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoIVAConcesionarias.btnProcesarClick(Sender: TObject);
  function ValidarEntrada: Boolean;
  resourcestring
      MSG_ERROR                 = 'Validación';
  var
      DescError :string;
  begin
      DescError := '';
      Result := True;

      if (Integer(vcbConcesionarias.Value) <= 0) then
          DescError := DescError + 'Debe seleccionar una concesionaria' + #13 + #10;

      if (edFechaDesde.IsEmpty OR edFechaHasta.IsEmpty) then
          DescError := DescError + 'Debe ingresar una fecha válida' + #13 + #10
      else if edFechaDesde.Date > edFechaHasta.Date then
          DescError := DescError + 'Fecha Desde debe ser menor o igual a Fecha Hasta' + #13 + #10;

      if DescError <> '' then
      begin
          MsgBox(DescError, MSG_ERROR, MB_ICONWARNING);
          Result := False;
      end;
  end;

resourcestring
    MSG_PROCESS_CANCEL = 'Proceso cancelado';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finalizó con Errores';
  	MSG_PROCESS_FINALLY_OK = 'El proceso finalizó con éxito';
Const
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
    STR_OBTAINING_DATA     = 'Obteniendo los Datos...';
    STR_SAVE_FILE          = 'Guardando el Archivo...';
    STR_SAVE_HISTORIC      = 'Guardando en Historico...';

begin

    if (ValidarEntrada) then
    begin
        btnProcesar.Enabled := False;
        PnlAvance.visible := True;
  	    Screen.Cursor := crHourGlass;
        KeyPreview := True;
        FErrorGrave := False;

        FFechaDesde := edFechaDesde.Date;
        FFechaHasta := edFechaHasta.Date;
        FNombreArchivo := txtArchivo.Text;

        try
            //Registro la operación en el Log
            lblMensaje.Caption := STR_REGISTER_OPERATION;          //Informo Tarea
            Application.ProcessMessages;                           //Refresco la pantalla
            if not RegistrarOperacion then begin
                FErrorGrave := True;
                Exit;
            end;

            // Obtengo los datos y genero el archivo
            lblMensaje.Caption := STR_OBTAINING_DATA;               //Informo Tarea
            Application.ProcessMessages;                            //Refresco la pantalla
            if not ObtenerIVAConcesionaria then begin
              FErrorGrave := True;
              Exit;
            end;

            //BEGIN:SS_1128_NDR_20131030---------------------------------------------
            //Guardo los registros que se agregaron al archivo
            lblmensaje.Caption := STR_SAVE_HISTORIC;                 //Informo Tarea
            Application.ProcessMessages;
            if not GuardarHistorico then begin
                FErrorGrave := true;
                Exit;
            end;
            //END:SS_1128_NDR_20131030---------------------------------------------

            // Guardo el archivo
            lblMensaje.Caption := STR_SAVE_FILE;                    //Informo Tarea
            Application.ProcessMessages;                            //Refresco la pantalla
  	        if not GuardarArchivo then begin
                FErrorGrave := True;
                Exit;
            end;

            //Actualizo el log al Final
            ActualizarLog;

        finally
            if SinDatos <> '' then ActualizarLog;    // SS_1006_1015_CQU_20120829

            Screen.Cursor := crDefault;
            pbProgreso.Position := 0;
            PnlAvance.visible := False;
            lblMensaje.Caption := '';
            if FCancelar then begin
                //Muestro mensaje que el proceso fue cancelado  (sin errores)
                MsgBox(MSG_PROCESS_CANCEL, Self.Caption, MB_OK + MB_ICONINFORMATION);
            end else if FErrorGrave then begin
                //Muestro mensaje que finalizo por un error
                //MsgBox(MSG_PROCESS_FINALLY_WITH_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);                             // SS_1006_1015_CQU_20120928
                if SinDatos = EmptyStr then MsgBox(MSG_PROCESS_FINALLY_WITH_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);   // SS_1006_1015_CQU_20120928
            end else begin
                //Muestro mensaje que el proceso finalizo exitosamente
                MsgBox(MSG_PROCESS_FINALLY_OK, Self.Caption, MB_OK + MB_ICONINFORMATION);
                btnProcesar.Enabled := True;
            end;
            KeyPreview := False;
            // Dejo el Botón Habilitado
            btnProcesar.Enabled := True;
            SinDatos := EmptyStr;                                                                                               // SS_1006_1015_CQU_20120928
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Cierra el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoIVAConcesionarias.btnSalirClick(Sender: TObject);
begin
    Close;
end;


{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Libera el formulario de memoria.
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoIVAConcesionarias.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;


{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Permite salir solo si no esta procesando.
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoIVAConcesionarias.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando.
  	CanClose := not PnlAvance.visible;
end;


{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Permito Cancelar
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoIVAConcesionarias.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE:
            begin
                FCancelar := True;
                PnlAvance.Visible := False;
            end;
    else
        FCancelar := False;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: edFechaDesdeChange
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Cambia la fecha y también cambia el nombre del archivo a generar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoIVAConcesionarias.edFechaDesdeChange(Sender: TObject);
begin
    if not bCargando then begin
        CambiarNombreArchivo;
        FFechaDesde := edFechaDesde.Date;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: edFechaHastaChange
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Cambia la fecha y también cambia el nombre del archivo a generar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoIVAConcesionarias.edFechaHastaChange(Sender: TObject);
begin
    if not bCargando then begin
        CambiarNombreArchivo;
        FFechaHasta := edFechaHasta.Date;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: txtArchivoButtonClick
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Permite al usuario colocar su propia ruta y nombre de archivo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoIVAConcesionarias.txtArchivoButtonClick(Sender: TObject);
resourcestring
    MSG_ERROR           = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST = 'El archivo %s ya existe';
begin
    dlgSelArchivo.Filter := '*.txt';
    dlgSelArchivo.InitialDir := FDirectorioIVAOtrasConc;
  	dlgSelArchivo.FileName := FNombreArchivo;
    if dlgSelArchivo.Execute then begin
        txtArchivo.Text := UpperCase( dlgSelArchivo.Filename );
        if FileExists( txtArchivo.text ) then begin
          	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtArchivo.Text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;
    end;
    // Si existe el Archivo se habilita el botón Procesar
    if not (FileExists(txtArchivo.Text)) then
    begin
        btnProcesar.Enabled := true;
        FNombreArchivo         := ExtractFileName(txtArchivo.Text);
        FDirectorioIVAOtrasConc:= ExtractFilePath(txtArchivo.Text);
        DirectorioSeleccionado := ExtractFilePath(txtArchivo.Text);
    end
    else begin
        MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtArchivo.Text]), MSG_ERROR, MB_ICONERROR );
        Exit;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: vcbConcesionariasChange
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Cambia el nombre de archivo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoIVAConcesionarias.vcbConcesionariasChange(Sender: TObject);
begin
    if not bCargando then
    begin
        if (vcbConcesionarias.ItemIndex = 0) then
        begin
            MsgBox(MSG_ERROR_CONCESION, MSG_ERROR_TITULO);
        end
        else
        begin
            FCodigoConcesionaria := vcbConcesionarias.Items[vcbConcesionarias.ItemIndex].Value;
            CambiarNombreArchivo;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: ObtenerIVAConcesionaria
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Obtiene los IVA de otras concesionarias
  Parameters: None
  Return Value: True or False
-----------------------------------------------------------------------------}
function TFGenerarArchivoIVAConcesionarias.ObtenerIVAConcesionaria: Boolean;
Resourcestring
    MSG_COULD_NOT_OBTAIN_IVA_CONCESIONARIA = 'No se pudo obtener IVA de Concesionaria';
    //MSG_EMPTY_QUERY_ERROR = 'No hay registros para informar!';                                               // SS_1006_1015_CQU_20120928
    MSG_EMPTY_QUERY_ERROR = 'No se han encontrado registros para los criterios de búsqueda seleccionados.';    // SS_1006_1015_CQU_20120928
    MSG_ERROR = 'Error';
var
    i : Integer;
    Linea : AnsiString;
    ErrorMsg : String;
    nProgreso : Integer;
begin
    Result := False;
    //Inicializo variables
    FLista.Clear;
    FCancelar := False;
    //inicializo la barra de progreso
    pbProgreso.Position := 0;
    pbProgreso.Max := 0; //debo colocar la cantidad de registros para el final del progreso

    //Obtengo los IVAs de otras concesionarias
    try
        spObtenerIVAConcesionaria.Close;
        spObtenerIVAConcesionaria.Connection := DMConnections.BaseCAC;
        spObtenerIVAConcesionaria.Parameters.Refresh;
        spObtenerIVAConcesionaria.Parameters.ParamByName('@FechaDesde').Value := FFechaDesde;
        spObtenerIVAConcesionaria.Parameters.ParamByName('@FechaHasta').Value := FFechaHasta;
        spObtenerIVAConcesionaria.Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConcesionaria;
        spObtenerIVAConcesionaria.Parameters.ParamByName('@NombreArchivo').Value := ExtractFileName(FNombreArchivo); // FNombreArchivo; // SS_1006_1015_CQU_20120926
        spObtenerIVAConcesionaria.CommandTimeout := 500;
        spObtenerIVAConcesionaria.Open;
        if not spObtenerIVAConcesionaria.IsEmpty then
        begin
            pbProgreso.Max := spObtenerIVAConcesionaria.RecordCount;
            nProgreso := 0;
            while not spObtenerIVAConcesionaria.EOF and (not FCancelar) do begin
                nProgreso := nProgreso + 1;
                i := 1;
                Linea := '';
                //mientras halla registros y no sea cancelado
                while (i <= spObtenerIVAConcesionaria.FieldCount) and (not FCancelar) do begin
                    //genero las lineas del archivo
                    Linea := Linea + spObtenerIVAConcesionaria.Fields.FieldByNumber(i).AsString + ';';
                    Inc(i);
                end;
                FLista.Add(Linea);
                spObtenerIVAConcesionaria.Next;

                //Refresco la pantalla
                if nProgreso <= spObtenerIVAConcesionaria.RecordCount then pbProgreso.Position:= nProgreso;  //Muestro el progreso
                Application.ProcessMessages;

            end;
            Result := (not FCancelar);
        end
        else begin                              // SS_1006_1015_CQU_20120829
            //MsgBoxErr(MSG_COULD_NOT_OBTAIN_IVA_CONCESIONARIA                          // SS_1006_1015_CQU_20120928
            //, MSG_EMPTY_QUERY_ERROR, MSG_ERROR, MB_ICONERROR);                        // SS_1006_1015_CQU_20120928
            MsgBox(MSG_EMPTY_QUERY_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);    // SS_1006_1015_CQU_20120928
            SinDatos := MSG_EMPTY_QUERY_ERROR;  // SS_1006_1015_CQU_20120829
        end;                                    // SS_1006_1015_CQU_20120829

    except
        on e: Exception do begin
            ErrorMsg := MSG_COULD_NOT_OBTAIN_IVA_CONCESIONARIA;
            MsgBoxErr(ErrorMsg , e.Message, MSG_ERROR, MB_ICONERROR);
            //Break;
        end;
    end;      
end;



{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Registra la Operación en el Log de Operaciones
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoIVAConcesionarias.RegistrarOperacion : Boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operación';
    MSG_ERROR = 'Error';
var
	DescError : String;
    STR_OBSERVACION : string;
    //CodigoDelModulo : integer;    // SS_1006_1015_CQU_20120829
begin
    STR_OBSERVACION := 'Generar Archivo IVA Otras Concesionarias';
    //CodigoDelModulo := StrToInt(CodigoModulo); // SS_1006_1015_CQU_20120829
    //Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, CodigoDelModulo, ExtractFileName(FNombreArchivo), UsuarioSistema, STR_OBSERVACION, False, False , NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError); // SS_1006_1015_CQU_20120829
    Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, CodigoModulo, ExtractFileName(FNombreArchivo), UsuarioSistema, STR_OBSERVACION, False, False , NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);      // SS_1006_1015_CQU_20120829
    if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;




{-----------------------------------------------------------------------------
  Function Name: GuardarArchivo
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Proceso que guarda el archivo que se generó
  Parameters: None
  Return Value: True or False
-----------------------------------------------------------------------------}
function TFGenerarArchivoIVAConcesionarias.GuardarArchivo : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: GuardarArchivoRechazados
      Author:    NDR
      Date Created: 25-Abril-2012
      Description: Guarda el archivo que se generó
      Parameters: var DescError:AnsiString
      Return Value: True or False
    -----------------------------------------------------------------------------}
    Function GuardarArchivoIVAConcesionaria(var DescError : AnsiString) : Boolean;
    Resourcestring
        MSG_ERROR  = 'No se pudo guardar archivo';
        MSG_CANCEL = 'Se ha cancelado el proceso debido a la existencia de un ' + CRLF +
                     'archivo idéntico en el directorio de destino';
    Const
        FILE_TITLE   = ' El archivo:  ';
        FILE_EXISTS  = ' ya existe en el directorio de destino. ¿Desea reemplazarlo?';
        FILE_WARNING = ' Atención';
    begin
        //Verificamos que no exista uno con el mismo nombre
        if FileExists(FNombreArchivo) then begin
            if (MsgBox( FILE_TITLE + FNombreArchivo + FILE_EXISTS, FILE_WARNING, MB_YESNO + MB_ICONQUESTION) = IDNO) then begin
                DescError := MSG_CANCEL; //informo que fue cancelado
                Result := False; //fallo
                Exit; //y salgo
            end;
        end;

        try
            //creo el archivo
            FLista.SaveToFile(FNombreArchivo);
            if not Result then
                DescError := MSG_ERROR
        except
            raise Exception.Create(MSG_ERROR);
        end;
    end;

Resourcestring
    MSG_SAVE_FILE_ERROR = 'No se pudo Guardar el Archivo: ';
    MSG_ERROR = 'Error';
var
    DescError : Ansistring;
begin
    Result := False;
    if not Assigned(FLista) or (FLista.Count = 0) then Exit;
    try
        Result :=  (//Grabo el archivo,
                    GuardarArchivoIVAConcesionaria(DescError)
                   );
        if not Result then begin
            //DeleteFile(FNombreArchivo);           //SS_1128_NDR_20131030
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR , DescError, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    except
        on e: Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR , e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarLog
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Obtengo la fecha del ultimo proceso de esta interfaz
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFGenerarArchivoIVAConcesionarias.ActualizarLog : Boolean;
Resourcestring
     MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
Const
     STR_OBSERVACION = 'Finalizo OK!';
var
     DescError : String;
     Observacion : String;                                                                  // SS_1006_1015_CQU_20120829
begin                                                                                             
    try
        if SinDatos <> '' then Observacion := STR_OBSERVACION + ' - No Existen Datos'       //SS_1006_1015_CQU_20120829
        else Observacion := STR_OBSERVACION;                                                //SS_1006_1015_CQU_20120829

        //Result := ActualizarLogOperacionesInterfaseAlFinal                                //SS_1006_1015_CQU_20120829
        //          (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError); //SS_1006_1015_CQU_20120829

        Result := ActualizarLogOperacionesInterfaseAlFinal                                  //SS_1006_1015_CQU_20120829
                  (DMConnections.BaseCAC, FCodigoOperacion, Observacion , DescError);       //SS_1006_1015_CQU_20120829
        //SinDatos := EmptyStr;                                                             //SS_1006_1015_CQU_20120829 // SS_1006_1015_CQU_20120928
    except
        on e : Exception do begin
           Result := False;
           MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
           //SinDatos := EmptyStr;                                                          //SS_1006_1015_CQU_20120829 // SS_1006_1015_CQU_20120928
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: GuardarHistorico
  Author:    NDR
  Date Created: 25-Abril-2012
  Description: Guarda los registros que se guardaron en el archivo que se genero
                con los IVAs de otras concesionarias
  Parameters: None
  Return Value: True or False
-----------------------------------------------------------------------------}
function TFGenerarArchivoIVAConcesionarias.GuardarHistorico : Boolean;
Resourcestring
    MSG_SAVE_FILE_ERROR = 'No se pudo Guardar el Historico: ';
    MSG_ERROR = 'Error';
var
    DescError : Ansistring;
    i : Integer;
    Linea : AnsiString;
    ErrorMsg : String;
    nProgreso : Integer;

    ValorRetorno : smallint;
    Mensaje : string;
begin
    Result := False;
    try
        if not spObtenerIVAConcesionaria.IsEmpty then
        begin
            pbProgreso.Max := spObtenerIVAConcesionaria.RecordCount;
            Application.ProcessMessages;

            //aqui llamo al sp que insertara los registro
            spAgregaHistoricoIVAConcesionaria.Close;
            spAgregaHistoricoIVAConcesionaria.Connection := DMConnections.BaseCAC;
            spAgregaHistoricoIVAConcesionaria.Parameters.Refresh;
            spAgregaHistoricoIVAConcesionaria.Parameters.ParamByName('@MensajeError').Value := null;
            spAgregaHistoricoIVAConcesionaria.ExecProc;
            ValorRetorno := spAgregaHistoricoIVAConcesionaria.Parameters.ParamByName('@RETURN_VALUE').Value;
            Mensaje := IIF(spAgregaHistoricoIVAConcesionaria.Parameters.ParamByName('@MensajeError').Value = null, '', spAgregaHistoricoIVAConcesionaria.Parameters.ParamByName('@MensajeError').Value);

            pbProgreso.Position := spObtenerIVAConcesionaria.RecordCount;
            if ValorRetorno < 0 then
            begin
                IIf( Mensaje = '', 'Error al Agregar registro a la tabla HistoricoIVAConcesionaria', Mensaje);
                raise EConvertError.Create(Mensaje);
            end;

            //Refresco la pantalla
            Application.ProcessMessages;
            Result := (not FCancelar);
        end;
    except
        on e: Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR , e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;

    end;

end;


initialization
    FLista   := TStringList.Create;
finalization
    FreeAndNil(Flista);

end.
