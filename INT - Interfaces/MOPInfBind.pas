{-----------------------------------------------------------------------------
 File Name: RVMBind.pas
 Author:    dcalani
 Date Created: 07/06/2005
 Language: ES-AR
 Description: Bind Unit para el XML al MOP

 Author 		: 	Claudio Quezada Ib��ez
 Date		    :	17/10/2012
 firma          :   SS_660_CQU_20121010
 Descripcion    :   Se agrega el Tipo de Infraccion al XML y a los resultados devueltos por el SP

 Author 		: 	Claudio Quezada Ib��ez
 Date		    :	14/10/2012
 firma          :   SS_1138_CQU_20131009
 Descripcion    :   Se cambia el tipo de datos de los campos Num_Corr_CA y Num_Corr_CO
-----------------------------------------------------------------------------}
unit MOPInfBind;

interface

uses SysUtils, xmldom, XMLDoc, XMLIntf, Util;

type

{ Forward Decls }

  IXMLDenunciosType = interface;
  IXMLInfraccionType = interface;
  IXMLInfractorType = interface;
  IXMLVehiculoType = interface;
  IXMLFotografiaType = interface;
  IXMLTransaccionType = interface;
  IXMLTransaccionTypeList = interface;

{ IXMLDenunciosType }

  IXMLDenunciosType = interface(IXMLNodeCollection)
    ['{7096D33D-780A-4C5D-85ED-D2213DF29A29}']
    { Property Accessors }
    function Get_Infraccion(Index: Integer): IXMLInfraccionType;
    { Methods & Properties }
    function Add: IXMLInfraccionType;
    function Insert(const Index: Integer): IXMLInfraccionType;
    property Infraccion[Index: Integer]: IXMLInfraccionType read Get_Infraccion; default;
  end;

{ IXMLInfraccionType }

  IXMLInfraccionType = interface(IXMLNode)
    ['{35F4F539-CDC1-4E72-88D7-92A49231CA9B}']
    { Property Accessors }
    function Get_Concesionaria: Integer;
    function Get_NumInfraccion: Integer;
    function Get_TipoInfraccion : Integer;          // SS_660_CQU_20121010
    function Get_Infractor: IXMLInfractorType;
    function Get_Vehiculo: IXMLVehiculoType;
    function Get_Fotografia: IXMLFotografiaType;
    function Get_Transaccion: IXMLTransaccionTypeList;
    procedure Set_Concesionaria(Value: Integer);
    procedure Set_NumInfraccion(Value: Integer);
    procedure Set_TipoInfraccion(Value: Integer);   // SS_660_CQU_20121010
    { Methods & Properties }
    property Concesionaria: Integer read Get_Concesionaria write Set_Concesionaria;
    property NumInfraccion: Integer read Get_NumInfraccion write Set_NumInfraccion;
    property TipoInfraccion: Integer read Get_TipoInfraccion write Set_TipoInfraccion; // SS_660_CQU_20121010
    property Infractor: IXMLInfractorType read Get_Infractor;
    property Vehiculo: IXMLVehiculoType read Get_Vehiculo;
    property Fotografia: IXMLFotografiaType read Get_Fotografia;
    property Transaccion: IXMLTransaccionTypeList read Get_Transaccion;
  end;

{ IXMLInfractorType }

  IXMLInfractorType = interface(IXMLNode)
    ['{16FD3784-1627-48A9-95D1-9D0C971478AE}']
    { Property Accessors }
    function Get_Nombre: WideString;
    function Get_ApellidoPaterno: WideString;
    function Get_ApellidoMaterno: WideString;
    function Get_Rut: WideString;
    function Get_DV: WideString;
    function Get_Domicilio: WideString;
    function Get_Comuna: WideString;
    procedure Set_Nombre(Value: WideString);
    procedure Set_ApellidoPaterno(Value: WideString);
    procedure Set_ApellidoMaterno(Value: WideString);
    procedure Set_Rut(Value: WideString);
    procedure Set_DV(Value: WideString);
    procedure Set_Domicilio(Value: WideString);
    procedure Set_Comuna(Value: WideString);
    { Methods & Properties }
    property Nombre: WideString read Get_Nombre write Set_Nombre;
    property ApellidoPaterno: WideString read Get_ApellidoPaterno write Set_ApellidoPaterno;
    property ApellidoMaterno: WideString read Get_ApellidoMaterno write Set_ApellidoMaterno;
    property Rut: WideString read Get_Rut write Set_Rut;
    property DV: WideString read Get_DV write Set_DV;
    property Domicilio: WideString read Get_Domicilio write Set_Domicilio;
    property Comuna: WideString read Get_Comuna write Set_Comuna;
  end;

{ IXMLVehiculoType }

  IXMLVehiculoType = interface(IXMLNode)
    ['{D15A493B-BA3E-48BF-92EE-AA6C87B2DED2}']
    { Property Accessors }
    function Get_Patente: WideString;
    function Get_DV: WideString;
    function Get_TipoVehiculo: WideString;
    function Get_Marca: WideString;
    function Get_Modelo: WideString;
    function Get_Agno: Integer;
    function Get_Color: WideString;
    procedure Set_Patente(Value: WideString);
    procedure Set_DV(Value: WideString);
    procedure Set_TipoVehiculo(Value: WideString);
    procedure Set_Marca(Value: WideString);
    procedure Set_Modelo(Value: WideString);
    procedure Set_Agno(Value: Integer);
    procedure Set_Color(Value: WideString);
    { Methods & Properties }
    property Patente: WideString read Get_Patente write Set_Patente;
    property DV: WideString read Get_DV write Set_DV;
    property TipoVehiculo: WideString read Get_TipoVehiculo write Set_TipoVehiculo;
    property Marca: WideString read Get_Marca write Set_Marca;
    property Modelo: WideString read Get_Modelo write Set_Modelo;
    property Agno: Integer read Get_Agno write Set_Agno;
    property Color: WideString read Get_Color write Set_Color;
  end;

{ IXMLFotografiaType }

  IXMLFotografiaType = interface(IXMLNode)
    ['{0975BA51-AE52-4BD6-9CEF-25D1C5AE252D}']
    { Property Accessors }
    function Get_Imagen: WideString;
    procedure Set_Imagen(Value: WideString);
    { Methods & Properties }
    property Imagen: WideString read Get_Imagen write Set_Imagen;
  end;

{ IXMLTransaccionType }

  IXMLTransaccionType = interface(IXMLNode)
    ['{80F4D10D-FFE6-488E-BBEB-3A7FDE6125D7}']
    { Property Accessors }
    function Get_Ident_PC: WideString;
    function Get_Num_Corr_Punto: Integer;
    function Get_Time_PC: Integer;
    function Get_Fecha_transaccion: WideString;
    function Get_Hora_local: WideString;
    //function Get_Num_Corr_CO: Integer;                                        // SS_1138_CQU_20131009
    function Get_Num_Corr_CO: Int64;                                            // SS_1138_CQU_20131009
    //function Get_Num_Corr_CA: Integer;                                        // SS_1138_CQU_20131009
    function Get_Num_Corr_CA: Int64;                                            // SS_1138_CQU_20131009
    function Get_ConsolidacionManual: WideString;
    procedure Set_Ident_PC(Value: WideString);
    procedure Set_Num_Corr_Punto(Value: Integer);
    procedure Set_Time_PC(Value: Integer);
    procedure Set_Fecha_transaccion(Value: WideString);
    procedure Set_Hora_local(Value: WideString);
    //procedure Set_Num_Corr_CO(Value: Integer);                                // SS_1138_CQU_20131009
    procedure Set_Num_Corr_CO(Value: Int64);                                    // SS_1138_CQU_20131009
    //procedure Set_Num_Corr_CA(Value: Integer);                                // SS_1138_CQU_20131009
    procedure Set_Num_Corr_CA(Value: Int64);                                    // SS_1138_CQU_20131009
    procedure Set_ConsolidacionManual(Value: WideString);
    { Methods & Properties }
    property Ident_PC: WideString read Get_Ident_PC write Set_Ident_PC;
    property Num_Corr_Punto: Integer read Get_Num_Corr_Punto write Set_Num_Corr_Punto;
    property Time_PC: Integer read Get_Time_PC write Set_Time_PC;
    property Fecha_transaccion: WideString read Get_Fecha_transaccion write Set_Fecha_transaccion;
    property Hora_local: WideString read Get_Hora_local write Set_Hora_local;
    //property Num_Corr_CO: Integer read Get_Num_Corr_CO write Set_Num_Corr_CO; // SS_1138_CQU_20131009
    property Num_Corr_CO: Int64 read Get_Num_Corr_CO write Set_Num_Corr_CO;     // SS_1138_CQU_20131009
    //property Num_Corr_CA: Integer read Get_Num_Corr_CA write Set_Num_Corr_CA; // SS_1138_CQU_20131009
    property Num_Corr_CA: Int64 read Get_Num_Corr_CA write Set_Num_Corr_CA;     // SS_1138_CQU_20131009
    property ConsolidacionManual: WideString read Get_ConsolidacionManual write Set_ConsolidacionManual;
  end;

{ IXMLTransaccionTypeList }

  IXMLTransaccionTypeList = interface(IXMLNodeCollection)
    ['{2648E1FB-8600-49EB-99F2-9E2BC4571E75}']
    { Methods & Properties }
    function Add: IXMLTransaccionType;
    function Insert(const Index: Integer): IXMLTransaccionType;
    function Get_Item(Index: Integer): IXMLTransaccionType;
    property Items[Index: Integer]: IXMLTransaccionType read Get_Item; default;
  end;

{ Forward Decls }

  TXMLDenunciosType = class;
  TXMLInfraccionType = class;
  TXMLInfractorType = class;
  TXMLVehiculoType = class;
  TXMLFotografiaType = class;
  TXMLTransaccionType = class;
  TXMLTransaccionTypeList = class;

{ TXMLDenunciosType }

  TXMLDenunciosType = class(TXMLNodeCollection, IXMLDenunciosType)
  protected
    { IXMLDenunciosType }
    function Get_Infraccion(Index: Integer): IXMLInfraccionType;
    function Add: IXMLInfraccionType;
    function Insert(const Index: Integer): IXMLInfraccionType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfraccionType }

  TXMLInfraccionType = class(TXMLNode, IXMLInfraccionType)
  private
    FTransaccion: IXMLTransaccionTypeList;
  protected
    { IXMLInfraccionType }
    function Get_Concesionaria: Integer;
    function Get_NumInfraccion: Integer;
    function Get_TipoInfraccion : Integer;          // SS_660_CQU_20121010
    function Get_Infractor: IXMLInfractorType;
    function Get_Vehiculo: IXMLVehiculoType;
    function Get_Fotografia: IXMLFotografiaType;
    function Get_Transaccion: IXMLTransaccionTypeList;
    procedure Set_Concesionaria(Value: Integer);
    procedure Set_NumInfraccion(Value: Integer);
    procedure Set_TipoInfraccion(Value: Integer);   // SS_660_CQU_20121010
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfractorType }

  TXMLInfractorType = class(TXMLNode, IXMLInfractorType)
  protected
    { IXMLInfractorType }
    function Get_Nombre: WideString;
    function Get_ApellidoPaterno: WideString;
    function Get_ApellidoMaterno: WideString;
    function Get_Rut: WideString;
    function Get_DV: WideString;
    function Get_Domicilio: WideString;
    function Get_Comuna: WideString;
    procedure Set_Nombre(Value: WideString);
    procedure Set_ApellidoPaterno(Value: WideString);
    procedure Set_ApellidoMaterno(Value: WideString);
    procedure Set_Rut(Value: WideString);
    procedure Set_DV(Value: WideString);
    procedure Set_Domicilio(Value: WideString);
    procedure Set_Comuna(Value: WideString);
  end;

{ TXMLVehiculoType }

  TXMLVehiculoType = class(TXMLNode, IXMLVehiculoType)
  protected
    { IXMLVehiculoType }
    function Get_Patente: WideString;
    function Get_DV: WideString;
    function Get_TipoVehiculo: WideString;
    function Get_Marca: WideString;
    function Get_Modelo: WideString;
    function Get_Agno: Integer;
    function Get_Color: WideString;
    procedure Set_Patente(Value: WideString);
    procedure Set_DV(Value: WideString);
    procedure Set_TipoVehiculo(Value: WideString);
    procedure Set_Marca(Value: WideString);
    procedure Set_Modelo(Value: WideString);
    procedure Set_Agno(Value: Integer);
    procedure Set_Color(Value: WideString);
  end;

{ TXMLFotografiaType }

  TXMLFotografiaType = class(TXMLNode, IXMLFotografiaType)
  protected
    { IXMLFotografiaType }
    function Get_Imagen: WideString;
    procedure Set_Imagen(Value: WideString);
  end;

{ TXMLTransaccionType }

  TXMLTransaccionType = class(TXMLNode, IXMLTransaccionType)
  protected
    { IXMLTransaccionType }
    function Get_Ident_PC: WideString;
    function Get_Num_Corr_Punto: Integer;
    function Get_Time_PC: Integer;
    function Get_Fecha_transaccion: WideString;
    function Get_Hora_local: WideString;
    //function Get_Num_Corr_CO: Integer;                                        // SS_1138_CQU_20131009
    function Get_Num_Corr_CO: Int64;                                            // SS_1138_CQU_20131009
    //function Get_Num_Corr_CA: Integer;                                        // SS_1138_CQU_20131009
    function Get_Num_Corr_CA: Int64;                                            // SS_1138_CQU_20131009
    function Get_ConsolidacionManual: WideString;
    procedure Set_Ident_PC(Value: WideString);
    procedure Set_Num_Corr_Punto(Value: Integer);
    procedure Set_Time_PC(Value: Integer);
    procedure Set_Fecha_transaccion(Value: WideString);
    procedure Set_Hora_local(Value: WideString);
    //procedure Set_Num_Corr_CO(Value: Integer);                                // SS_1138_CQU_20131009
    procedure Set_Num_Corr_CO(Value: Int64);                                    // SS_1138_CQU_20131009
    //procedure Set_Num_Corr_CA(Value: Integer);                                // SS_1138_CQU_20131009
    procedure Set_Num_Corr_CA(Value: Int64);                                    // SS_1138_CQU_20131009
    procedure Set_ConsolidacionManual(Value: WideString);
  end;

{ TXMLTransaccionTypeList }

  TXMLTransaccionTypeList = class(TXMLNodeCollection, IXMLTransaccionTypeList)
  protected
    { IXMLTransaccionTypeList }
    function Add: IXMLTransaccionType;
    function Insert(const Index: Integer): IXMLTransaccionType;
    function Get_Item(Index: Integer): IXMLTransaccionType;
  end;



CONST
    DENUNCIO_FILE = '<?xml version="1.0" encoding="UTF-8"?> ' + CRLF +
                    '<!ELEMENT Denuncios (Infraccion+) > ' + CRLF +
                    //'<!ELEMENT Infraccion (Concesionaria, NumInfraccion, Infractor, Vehiculo, Fotografia, ' + CRLF +                  // SS_660_CQU_20121010
                    '<!ELEMENT Infraccion (Concesionaria, NumInfraccion, TipoInfraccion, Infractor, Vehiculo, Fotografia, ' + CRLF +    // SS_660_CQU_20121010
                    'Transaccion+)> ' + CRLF +
                    '<!ELEMENT Infractor (Nombre, ApellidoPaterno, ApellidoMaterno, Rut, DV, Domicilio, Comuna)> ' + CRLF +
                    '<!ELEMENT Vehiculo (Patente, DV, TipoVehiculo, Marca, Modelo, Agno, Color)> ' + CRLF +
                    '<!ELEMENT Transaccion (Ident_PC, Num_Corr_Punto, Time_PC, Fecha_transaccion,Hora_local, ' + CRLF +
                    'Num_Corr_CO, Num_Corr_CA, ConsolidacionManual)> ' + CRLF +
                    '<!ELEMENT Fotografia (Imagen,FimaImagen?)> ' + CRLF +
                    '<!ELEMENT Concesionaria (#PCDATA) > ' + CRLF +
                    '<!ELEMENT NumInfraccion (#PCDATA) > ' + CRLF +
                    '<!ELEMENT TipoInfraccion (#PCDATA) > ' + CRLF +    // SS_660_CQU_20121010
                    '<!ELEMENT Nombre (#PCDATA) > ' + CRLF +
                    '<!ELEMENT ApellidoPaterno (#PCDATA) > ' + CRLF +
                    '<!ELEMENT ApellidoMaterno (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Rut (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Domicilio (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Comuna (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Patente (#PCDATA) > ' + CRLF +
                    '<!ELEMENT DV (#PCDATA) > ' + CRLF +
                    '<!ELEMENT TipoVehiculo (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Marca (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Modelo (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Agno (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Color (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Ident_PC (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Num_Corr_Punto (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Time_PC (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Fecha_transaccion (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Hora_local (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Num_Corr_CO (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Num_Corr_CA (#PCDATA) > ' + CRLF +
                    '<!ELEMENT ConsolidacionManual (#PCDATA) > ' + CRLF +
                    '<!ELEMENT Imagen (#PCDATA) > ' + CRLF +
                    '<!ELEMENT FimaImagen (#PCDATA) > ';

{ Global Functions }
function GetDenuncios(Doc: IXMLDocument): IXMLDenunciosType;
function LoadDenuncios(const FileName: WideString): IXMLDenunciosType;
function NewDenuncios: IXMLDenunciosType;
function Guardar(Const XML: IXMLDenunciosType; Archivo: String): Boolean;
Procedure GenerarDTD(Path: AnsiString);


implementation

{ Global Functions }

function GetDenuncios(Doc: IXMLDocument): IXMLDenunciosType;
begin
  Result := Doc.GetDocBinding('Denuncios', TXMLDenunciosType) as IXMLDenunciosType;
end;
function LoadDenuncios(const FileName: WideString): IXMLDenunciosType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Denuncios', TXMLDenunciosType) as IXMLDenunciosType;
end;

function NewDenuncios: IXMLDenunciosType;
var
    xmldoc: IXMLDocument;
begin
    xmldoc := NewXMLDocument('1.0');
    xmlDoc.Encoding := 'UTF-8';
    xmlDoc.StandAlone := 'yes';
    result := xmldoc.GetDocBinding('Denuncios', TXMLDenunciosType) as IXMLDenunciosType;
end;

function Guardar(Const XML: IXMLDenunciosType; Archivo: String): Boolean;
var
    xmldoc: IXMLDocument;
begin
    try
        xmldoc := NewXMLDocument('1.0');
        xmlDoc.LoadFromXML('<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Denuncios SYSTEM "Denuncios.dtd">' + xml.XML);
        xmlDoc.Encoding := 'UTF-8';
        xmlDoc.SaveToFile(Archivo);
        result := true
    except
        result := false;
    end;
end;

procedure GenerarDTD(Path: AnsiString);
begin
    if not FileExists(GoodDir(trim(path)) + 'Denuncios.DTD') then
        StringToFile(DENUNCIO_FILE, GoodDir(trim(path)) + 'Denuncios.DTD');
end;

{ TXMLDenunciosType }

procedure TXMLDenunciosType.AfterConstruction;
begin
  RegisterChildNode('Infraccion', TXMLInfraccionType);
  ItemTag := 'Infraccion';
  ItemInterface := IXMLInfraccionType;
  inherited;
end;

function TXMLDenunciosType.Get_Infraccion(Index: Integer): IXMLInfraccionType;
begin
  Result := List[Index] as IXMLInfraccionType;
end;

function TXMLDenunciosType.Add: IXMLInfraccionType;
begin
  Result := AddItem(-1) as IXMLInfraccionType;
end;

function TXMLDenunciosType.Insert(const Index: Integer): IXMLInfraccionType;
begin
  Result := AddItem(Index) as IXMLInfraccionType;
end;

{ TXMLInfraccionType }

procedure TXMLInfraccionType.AfterConstruction;
begin
  RegisterChildNode('Infractor', TXMLInfractorType);
  RegisterChildNode('Vehiculo', TXMLVehiculoType);
  RegisterChildNode('Fotografia', TXMLFotografiaType);
  RegisterChildNode('Transaccion', TXMLTransaccionType);
  FTransaccion := CreateCollection(TXMLTransaccionTypeList, IXMLTransaccionType, 'Transaccion') as IXMLTransaccionTypeList;
  inherited;
end;

function TXMLInfraccionType.Get_Concesionaria: Integer;
begin
  Result := ChildNodes['Concesionaria'].NodeValue;
end;

procedure TXMLInfraccionType.Set_Concesionaria(Value: Integer);
begin
  ChildNodes['Concesionaria'].NodeValue := Value;
end;

function TXMLInfraccionType.Get_NumInfraccion: Integer;
begin
  Result := ChildNodes['NumInfraccion'].NodeValue;
end;

procedure TXMLInfraccionType.Set_NumInfraccion(Value: Integer);
begin
  ChildNodes['NumInfraccion'].NodeValue := Value;
end;

function TXMLInfraccionType.Get_TipoInfraccion: Integer;            // SS_660:CQU_20121010
begin                                                               // SS_660:CQU_20121010
  Result := ChildNodes['TipoInfraccion'].NodeValue;                 // SS_660:CQU_20121010
end;                                                                // SS_660:CQU_20121010

procedure TXMLInfraccionType.Set_TipoInfraccion(Value: Integer);    // SS_660_CQU_20121010
begin                                                               // SS_660_CQU_20121010
  ChildNodes['TipoInfraccion'].NodeValue := Value;                  // SS_660_CQU_20121010
end;                                                                // SS_660_CQU_20121010

function TXMLInfraccionType.Get_Infractor: IXMLInfractorType;
begin
  Result := ChildNodes['Infractor'] as IXMLInfractorType;
end;

function TXMLInfraccionType.Get_Vehiculo: IXMLVehiculoType;
begin
  Result := ChildNodes['Vehiculo'] as IXMLVehiculoType;
end;

function TXMLInfraccionType.Get_Fotografia: IXMLFotografiaType;
begin
  Result := ChildNodes['Fotografia'] as IXMLFotografiaType;
end;

function TXMLInfraccionType.Get_Transaccion: IXMLTransaccionTypeList;
begin
  Result := FTransaccion;
end;

{ TXMLInfractorType }

function TXMLInfractorType.Get_Nombre: WideString;
begin
  Result := ChildNodes['Nombre'].Text;
end;

procedure TXMLInfractorType.Set_Nombre(Value: WideString);
begin
  ChildNodes['Nombre'].NodeValue := Value;
end;

function TXMLInfractorType.Get_ApellidoPaterno: WideString;
begin
  Result := ChildNodes['ApellidoPaterno'].Text;
end;

procedure TXMLInfractorType.Set_ApellidoPaterno(Value: WideString);
begin
  ChildNodes['ApellidoPaterno'].NodeValue := Value;
end;

function TXMLInfractorType.Get_ApellidoMaterno: WideString;
begin
  Result := ChildNodes['ApellidoMaterno'].Text;
end;

procedure TXMLInfractorType.Set_ApellidoMaterno(Value: WideString);
begin
  ChildNodes['ApellidoMaterno'].NodeValue := Value;
end;

function TXMLInfractorType.Get_Rut: WideString;
begin
  Result := ChildNodes['Rut'].Text;
end;

procedure TXMLInfractorType.Set_Rut(Value: WideString);
begin
  ChildNodes['Rut'].NodeValue := Value;
end;

function TXMLInfractorType.Get_DV: WideString;
begin
  Result := ChildNodes['DV'].NodeValue;
end;

procedure TXMLInfractorType.Set_DV(Value: WideString);
begin
  ChildNodes['DV'].NodeValue := Value;
end;

function TXMLInfractorType.Get_Domicilio: WideString;
begin
  Result := ChildNodes['Domicilio'].Text;
end;

procedure TXMLInfractorType.Set_Domicilio(Value: WideString);
begin
  ChildNodes['Domicilio'].NodeValue := Value;
end;

function TXMLInfractorType.Get_Comuna: WideString;
begin
  Result := ChildNodes['Comuna'].Text;
end;

procedure TXMLInfractorType.Set_Comuna(Value: WideString);
begin
  ChildNodes['Comuna'].NodeValue := Value;
end;

{ TXMLVehiculoType }

function TXMLVehiculoType.Get_Patente: WideString;
begin
  Result := ChildNodes['Patente'].Text;
end;

procedure TXMLVehiculoType.Set_Patente(Value: WideString);
begin
  ChildNodes['Patente'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_DV: WideString;
begin
  Result := ChildNodes['DV'].NodeValue;
end;

procedure TXMLVehiculoType.Set_DV(Value: WideString);
begin
  ChildNodes['DV'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_TipoVehiculo: WideString;
begin
  Result := ChildNodes['TipoVehiculo'].Text;
end;

procedure TXMLVehiculoType.Set_TipoVehiculo(Value: WideString);
begin
  ChildNodes['TipoVehiculo'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_Marca: WideString;
begin
  Result := ChildNodes['Marca'].Text;
end;

procedure TXMLVehiculoType.Set_Marca(Value: WideString);
begin
  ChildNodes['Marca'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_Modelo: WideString;
begin
  Result := ChildNodes['Modelo'].NodeValue;
end;

procedure TXMLVehiculoType.Set_Modelo(Value: WideString);
begin
  ChildNodes['Modelo'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_Agno: Integer;
begin
  Result := ChildNodes['Agno'].NodeValue;
end;

procedure TXMLVehiculoType.Set_Agno(Value: Integer);
begin
  ChildNodes['Agno'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_Color: WideString;
begin
  Result := ChildNodes['Color'].Text;
end;

procedure TXMLVehiculoType.Set_Color(Value: WideString);
begin
  ChildNodes['Color'].NodeValue := Value;
end;

{ TXMLFotografiaType }

function TXMLFotografiaType.Get_Imagen: WideString;
begin
  Result := ChildNodes['Imagen'].Text;
end;

procedure TXMLFotografiaType.Set_Imagen(Value: WideString);
begin
  ChildNodes['Imagen'].NodeValue :=  Value;
end;

{ TXMLTransaccionType }

function TXMLTransaccionType.Get_Ident_PC: WideString;
begin
  Result := ChildNodes['Ident_PC'].Text;
end;

procedure TXMLTransaccionType.Set_Ident_PC(Value: WideString);
begin
  ChildNodes['Ident_PC'].NodeValue := Value;
end;

function TXMLTransaccionType.Get_Num_Corr_Punto: Integer;
begin
  Result := ChildNodes['Num_Corr_Punto'].NodeValue;
end;

procedure TXMLTransaccionType.Set_Num_Corr_Punto(Value: Integer);
begin
  ChildNodes['Num_Corr_Punto'].NodeValue := Value;
end;

function TXMLTransaccionType.Get_Time_PC: Integer;
begin
  Result := ChildNodes['Time_PC'].NodeValue;
end;

procedure TXMLTransaccionType.Set_Time_PC(Value: Integer);
begin
  ChildNodes['Time_PC'].NodeValue := Value;
end;

function TXMLTransaccionType.Get_Fecha_transaccion: WideString;
begin
  Result := ChildNodes['Fecha_transaccion'].Text;
end;

procedure TXMLTransaccionType.Set_Fecha_transaccion(Value: WideString);
begin
  ChildNodes['Fecha_transaccion'].NodeValue := Value;
end;

function TXMLTransaccionType.Get_Hora_local: WideString;
begin
  Result := ChildNodes['Hora_local'].Text;
end;

procedure TXMLTransaccionType.Set_Hora_local(Value: WideString);
begin
  ChildNodes['Hora_local'].NodeValue := Value;
end;

//function TXMLTransaccionType.Get_Num_Corr_CO: Integer;                        // SS_1138_CQU_20131009
function TXMLTransaccionType.Get_Num_Corr_CO: Int64;                            // SS_1138_CQU_20131009
begin
  Result := ChildNodes['Num_Corr_CO'].NodeValue;
end;

//procedure TXMLTransaccionType.Set_Num_Corr_CO(Value: Integer);                // SS_1138_CQU_20131009
procedure TXMLTransaccionType.Set_Num_Corr_CO(Value: Int64);                    // SS_1138_CQU_20131009
begin
  ChildNodes['Num_Corr_CO'].NodeValue := Value;
end;

//function TXMLTransaccionType.Get_Num_Corr_CA: Integer;                        // SS_1138_CQU_20131009
function TXMLTransaccionType.Get_Num_Corr_CA: Int64;                            // SS_1138_CQU_20131009
begin
  Result := ChildNodes['Num_Corr_CA'].NodeValue;
end;

//procedure TXMLTransaccionType.Set_Num_Corr_CA(Value: Integer);                // SS_1138_CQU_20131009
procedure TXMLTransaccionType.Set_Num_Corr_CA(Value: Int64);                    // SS_1138_CQU_20131009
begin
  ChildNodes['Num_Corr_CA'].NodeValue := Value;
end;

function TXMLTransaccionType.Get_ConsolidacionManual: WideString;
begin
  Result := ChildNodes['ConsolidacionManual'].Text;
end;

procedure TXMLTransaccionType.Set_ConsolidacionManual(Value: WideString);
begin
  ChildNodes['ConsolidacionManual'].NodeValue := Value;
end;

{ TXMLTransaccionTypeList }

function TXMLTransaccionTypeList.Add: IXMLTransaccionType;
begin
  Result := AddItem(-1) as IXMLTransaccionType;
end;

function TXMLTransaccionTypeList.Insert(const Index: Integer): IXMLTransaccionType;
begin
  Result := AddItem(Index) as IXMLTransaccionType;
end;

function TXMLTransaccionTypeList.Get_Item(Index: Integer): IXMLTransaccionType;
begin
  Result := List[Index] as IXMLTransaccionType;
end;

end.
