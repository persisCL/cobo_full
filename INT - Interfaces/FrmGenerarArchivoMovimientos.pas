{-------------------------------------------------------------------------------
 File Name: FrmGenerarArchivoMovimientos.pas
 Author:    lgisuk
 Date Created: 25/11/2004
 Language: ES-AR
 Description: M�dulo de la interface TransBank - Generar Archivo de Movimientos
-------------------------------------------------------------------------------
 Revision History
--------------------------------------------------------------------------------
 Author: gcasais
 Date Created: 31/12/2004
 Description: revisi�n general

 Autor       :   Claudio Quezada
 Fecha       :   30-03-2015
 Firma       :   SS_1147_CQU_20150324
 Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato

 Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-------------------------------------------------------------------------------}
unit FrmGenerarArchivoMovimientos;

interface

uses
  //Gestion de Interfaces
  DMConnection,              //Coneccion a base de datos OP_CAC
  UtilProc,                  //Mensajes
  Util,                      //stringtofile,padl..
  UtilDB,                    //Rutinas para base de datos
  PeaProcs,                  //NowBase
  ComunesInterfaces,         //Procedimientos y Funciones Comunes a todos los formularios
  ConstParametrosGenerales,  //Obtengo Valores de la Tabla Parametros Generales
  PeaTypes,                 // SS_1147_CQU_20150324
  //General
  DB, ADODB, ComCtrls, StdCtrls, Controls,ExtCtrls, Classes, Windows, Messages, SysUtils,
  Variants,  Graphics,  Forms, Dialogs, Grids, DBGrids,  DPSControls, ListBoxEx,
  DBListEx, StrUtils, UtilRB, ppProd, ppClass, ppReport, ppComm, ppRelatv,
  ppDB, ppTxPipe, ppPrnabl, ppStrtch, ppSubRpt, ppCache, ppBands, ppCtrls,
  Validate, DateEdit;

type
  TFGenerarArchivoMovimientos = class(TForm)
    SPObtenerMandatosNovedadesTBK: TADOStoredProc;
    lblCTitulo: TLabel;
    pnlAvance: TPanel;
    pbProgreso: TProgressBar;
    Bevel: TBevel;
    btnProcesar: TButton;
    btnSalir: TButton;
    lblProcesoGeneral: TLabel;
    lblMensaje: TLabel;
    lblTitulo: TMemo;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlReprocesar: TPanel;
    lblNombreArchivo: TLabel;
    lblArchivoInterface: TLabel;
    edFecha: TDateEdit;
    lblFechaInterfase: TLabel;
    spObtenerUltimoCodigoConvenio: TADOStoredProc;
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure btnSalirClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure edFechaChange(Sender: TObject);
  private
    { Private declarations }
    FCodigoOperacion : Integer;
    FCancelar : Boolean;
    FErrorGrave : Boolean;
    FNombreArchivoMovimientos : AnsiString;
    FNumeroSecuencia : Integer;
    FFechaDesde : Variant;
    FTBK_CodigodeComercio  : Ansistring;
    FTBK_DirectorioDestino : Ansistring;
    FCodigoNativa : Integer;    // SS_1147_CQU_20150324
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    Function  CrearNombreArchivoMovimientos(var NombreArchivo : AnsiString; Fecha : TDateTime) : Boolean;
    Function  RegistrarOperacion : Boolean;
    Function  ObtenerMandatos : Boolean;
    Function  GuardarArchivo : Boolean;
  public
    Function  Inicializar ( Reprocesar : Boolean = False ): Boolean;
    { Public declarations }
  end;

var
  FGenerarArchivoMovimientos : TFGenerarArchivoMovimientos;
  FLista : TStringList;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 25/11/2004
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoMovimientos.Inicializar ( Reprocesar : Boolean = False ) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 18/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        TBK_CODIGODECOMERCIO  = 'TBK_CodigodeComercio';
        TBK_DIRECTORIODESTINO = 'TBK_DirectorioDestino';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la concesionaria nativa                  // SS_1147_CQU_20150324
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150324

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, TBK_CODIGODECOMERCIO , FTBK_CodigodeComercio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + TBK_CODIGODECOMERCIO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FTBK_CodigodeComercio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + TBK_CODIGODECOMERCIO;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, TBK_DIRECTORIODESTINO , FTBK_DirectorioDestino) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + TBK_DIRECTORIODESTINO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FTBK_DirectorioDestino := GoodDir(FTBK_DirectorioDestino);
                if  not DirectoryExists(FTBK_DirectorioDestino) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FTBK_DirectorioDestino;
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Centro el formulario
	CenterForm(Self);
	try
        DMConnections.BaseCAC.Connected := true;
        Result := DMConnections.BaseCAC.Connected and
                        VerificarParametrosGenerales and
                            CrearNombreArchivoMovimientos(FNombreArchivoMovimientos, Now);
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
    // Todo Ok, terminamos de inicializar
    lblTitulo.text := FNombreArchivoMovimientos;  //indico que archivo voy a generar
    lblMensaje.Caption := '';                     //no muestro ningun mensaje
    PnlAvance.visible := False;                   //Oculto el Panel de Avance
    FCancelar := False;                           //inicio cancelar en false
    FCodigoOperacion := 0;                        //Inicializo el codigo de operacion
    FErrorGrave := False;                         //Inicio sin errores

    //Si es re-proceso
    if Reprocesar = True then begin
        //hago visible el panel de busqueda de fecha
        pnlreprocesar.Visible := True;
        //inicializo fecha en null
        edFecha.Date := NULLDATE;
        //deshabilito boton procesar hasta que seleccionen una fecha
        btnProcesar.Enabled := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 03/06/2005
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoMovimientos.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_MOVIMIENTOS     = ' ' + CRLF +
                          'El Archivo de Movimientos es' + CRLF +
                          'utilizado por el ESTABLECIMIENTO para informar a TRANSBANK' + CRLF +
                          'las altas, bajas y modificaciones de mandantes' + CRLF +
                          'efectuadas por el ESTABLECIMIENTO' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: 99999999_MOVIMIENTOSIC_YYYYMMDD.S' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = True then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_MOVIMIENTOS, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 03/06/2005
  Description:
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoMovimientos.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: CrearNombreArchivoMovimientos
  Author:    lgisuk
  Date Created: 07/07/2005
  Description: creo el nombre del archivo de movimientos
  Parameters: var NombreArchivo: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoMovimientos.CrearNombreArchivoMovimientos(var NombreArchivo: AnsiString; Fecha : TDateTime ): Boolean;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerNumeroSecuencia
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Obtengo el numero de secuencia
      Parameters: var NumeroSecuencia:AnsiString
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerNumeroSecuencia(var NumeroSecuencia : AnsiString) : Boolean;
    resourcestring
        MSG_ERROR = 'Error al crear el numero de secuencia';
    Var
        SQLNumeroSecuenciaTBK : String;
        Valor : Integer;
    begin
        try
            //Obtengo el numero de secuencia
            SQLNumeroSecuenciaTBK := 'Select dbo.ObtenerNumeroSecuenciaTBK (23)';
            Valor := QueryGetValueInt(DMConnections.BaseCAC,SQLNumeroSecuenciaTBK);
            //el numero de secuencia debe ocupar 3 posiciones
            NumeroSecuencia := IStr0(Valor,3);
            Result := True;
        except
            raise Exception.Create(MSG_ERROR);
        end;
    end;

resourcestring
    MSG_FILE_NAME_ERROR = 'Error al crear el nombre del archivo';
    MSG_DIRECTORY_ERROR = 'El directorio de salida %s no existe.' + CRLF +
                          'Verifique los par�metros generales.';
const
    FILE_NAME = 'MOVIMIENTOSIC';
    FILE_EXTENSION = '.txt';
var
    Year, Month, Day : Word;
    Mes, Dia : AnsiString;
    NumeroSecuencia : AnsiString;
begin
    try
        //Obtengo a�o, mes y dia actual
        DecodeDate(Fecha, Year, Month, Day);
        if length(IntToStr(Month)) = 1 then Mes := '0'+ IntToStr(Month) else Mes := IntToStr(Month);
        if length(IntToStr(Day)) = 1 then Dia := '0'+ IntToStr(Day) else Dia := IntToStr(Day);
        //Obtengo Numero de Secuencia
        ObtenerNumeroSecuencia(NumeroSecuencia);
        //Genero el Nombre del Archivo de Movimientos
        NombreArchivo := FTBK_CodigodeComercio + '_' + FILE_NAME + '_';
        //NombreArchivo := NombreArchivo + IntToStr(Year) + Mes + Dia;                                          // SS_1147_CQU_20150324
        //NombreArchivo := FTBK_DirectorioDestino + NombreArchivo + '.' + NumeroSecuencia + FILE_EXTENSION;     // SS_1147_CQU_20150324
        if FCodigoNativa = CODIGO_VS then begin                                                                 // SS_1147_CQU_20150324
            NombreArchivo := NombreArchivo + Dia + Mes + IntToStr(Year);                                        // SS_1147_CQU_20150324
            NombreArchivo := FTBK_DirectorioDestino + NombreArchivo + '.001';                                   // SS_1147_CQU_20150324
        end else begin                                                                                          // SS_1147_CQU_20150324
            NombreArchivo := NombreArchivo + IntToStr(Year) + Mes + Dia;                                        // SS_1147_CQU_20150324
            NombreArchivo := FTBK_DirectorioDestino + NombreArchivo + '.' + NumeroSecuencia + FILE_EXTENSION;   // SS_1147_CQU_20150324
        end;                                                                                                    // SS_1147_CQU_20150324
        //guardo el numero de secuencia
        FNumeroSecuencia := StrToInt(NumeroSecuencia);
        Result := True;
    except
        raise Exception.Create(MSG_FILE_NAME_ERROR);
    end;
end;

{******************************** Function Header ******************************
Function Name: edFechaChange
Author : lgisuk
Date Created : 11/07/2005
Description : Permito Re-Procesar el Envio de Mandatos
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
Procedure TFGenerarArchivoMovimientos.edFechaChange(Sender: TObject);
Resourcestring
    STR_FILE_NAME = '99999999_MOVIMIENTOSIC_YYYYMMDD.S';
begin
	if ( edFecha.Date <> nulldate ) then begin
   		btnProcesar.Enabled := True;
   		FFechaDesde := edFecha.Date;

        //Creo el nombre del archivo a enviar
        CrearNombreArchivoMovimientos(FNombreArchivoMovimientos, edFecha.Date);
        lblNombreArchivo.Caption := ExtractFileName(FNombreArchivoMovimientos);

    end else begin
   		btnProcesar.Enabled := False;
   		lblNombreArchivo.Caption := STR_FILE_NAME;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    lgisuk
  Date Created: 07/07/2005
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoMovimientos.RegistrarOperacion : Boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Generar Archivo Movimientos';
    COD_MODULO = 23;
var
	DescError : String;
begin
   Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, COD_MODULO, ExtractFileName(FNombreArchivoMovimientos), UsuarioSistema, STR_OBSERVACION, False, False , NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);
   if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: Abrir
  Author:    lgisuk
  Date Created: 16/12/2004
  Description: Abro la Consulta
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFGenerarArchivoMovimientos.ObtenerMandatos: Boolean;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerUltimoConvenio
      Author:    lgisuk
      Date Created: 13/07/2005
      Description: Obtengo el ultimo numero de convenio
      Parameters: None
      Return Value: Longint
    -----------------------------------------------------------------------------}
    function ObtenerUltimoConvenio : Longint;
    begin
        try
            spObtenerUltimoCodigoConvenio.Open;
            result := spObtenerUltimoCodigoConvenio.FieldByNAme( 'UltimoConvenio' ).AsInteger;
            spObtenerUltimoCodigoConvenio.Close;
        except
            on exception do result := 1000000
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: QueryToTxt
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: convierte el bloque recibido a txt
      Parameters: Query:TCustomAdoDataSet
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    function QueryToTxt(Query : TCustomAdoDataSet) : Boolean;
    resourcestring
        MSG_PROCESS_ERROR = 'Error procesando los datos de los movimientos';
        MSG_ERROR = 'Error';
    var
        i : Integer;
        Linea : AnsiString;
    begin
        Result := False;

        try
            with Query do begin
                First;
                while not Eof and (not FCancelar) do begin
                    i:= 1;
                    Linea:= '';
                    //mientras halla registros y no sea cancelado
                    while (i <= Query.FieldCount) and (not FCancelar) do begin
                        //genero las lineas del archivo
                        Linea := Linea + Fields.FieldByNumber(i).AsString + ';';
                        Inc(i);
                    end;
                    FLista.Add(Linea);
                    Next;
                end;
            end;
            Result := (not FCancelar) and (FLista.Count > 0);
        except
            on e: Exception do begin
                FErrorGrave := True;
                MsgBoxErr(MSG_PROCESS_ERROR , e.Message, MSG_ERROR, MB_ICONERROR);
                Exit;
            end;
        end;
    end;

Resourcestring
    MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS = 'No se pudieron obtener los mandatos pendientes';
    MSG_EMPTY_QUERY_ERROR = 'No hay Mandatos PAT con novedades para informar!';
    MSG_ERROR = 'Error';
var
  	NroConvenio : Variant;
    ErrorMsg : String;
    Year, Month, Day : Word;
    Mes,Dia : AnsiString;
    CantidadTotal : Integer;
begin
    Result := False;

    //Inicializo variables
    FLista.Clear;
    FCancelar := False;
    NroConvenio := 0;
    ErrorMsg := '';
    CantidadTotal := 0;

    //inicializo la barra de progreso
    pbProgreso.Position := 0;
    pbProgreso.Max := ObtenerUltimoConvenio;

    //Obtengo las novedades y las escribo en el archivo
    while (NroConvenio <> null) and (ErrorMsg = '') and (not FCancelar) do begin

        With SPObtenerMandatosNovedadesTBK do begin
            try
                Close;
                Parameters.ParamByName('@FechaInterfase').Value:=FFechaDesde;
                Parameters.ParamByName('@CodigoConvenio').Value:=NroConvenio;
                CommandTimeout := 500;
                Open;
            except
                on e: Exception do begin
                    ErrorMsg := MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS;
                    MsgBoxErr(ErrorMsg , e.Message, MSG_ERROR, MB_ICONERROR);
                    Break;
                end;
            end;
            //Acumulo la cantidad total de registros
            CantidadTotal := CantidadTotal + RecordCount;
            //Creo el bloque en el archivo
            QueryToTxt(SPObtenerMandatosNovedadesTBK);
            //Obtengo el ultimo numero de convenio procesado
            NroConvenio := Parameters.ParamByName( '@CodigoConvenio' ).Value;
        end;

        //Refresco la pantalla
        if CantidadTotal <> 0 then begin
            if NroConvenio <> NULL then pbProgreso.Position:= NroConvenio;  //Muestro el progreso
            Application.ProcessMessages;
        end;

    end;

    //finalizo la barra de progreso
    pbProgreso.Position := pbProgreso.Max;
    Application.ProcessMessages;

    //Genero en Encabezado del archivo y lo inserto al principio del archivo
    DecodeDate(Now, Year, Month, Day);

    //Si esta en modo Re-proceso
    if pnlReprocesar.Visible = true then DecodeDate(edFecha.Date, Year, Month, Day);

    if Length(IntToStr(Month)) = 1 then Mes := '0'+ IntToStr(Month) else Mes := IntToStr(Month);
    if Length(IntToStr(Day)) = 1 then Dia := '0'+ IntToStr(Day) else Dia := IntToStr(Day);
    Flista.Insert(0, IntToStr(Year) + Mes + Dia + ';' + IntToStr(CantidadTotal));

    //Verifico que halla novedades para informar
    if CantidadTotal = 0 then begin
        FErrorGrave := True;
        MsgBox(MSG_EMPTY_QUERY_ERROR);
        Exit;
    end;

    //Si llego a procesar hasta el final y no hubo errores
    if (NroConvenio = NULL) then begin
        //Apruebo este paso
        Result := True;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: Guardar
  Author:    lgisuk
  Date Created: 16/12/2004
  Description: Guardar Archivo
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TFGenerarArchivoMovimientos.GuardarArchivo : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: GuardarArchivoMovimientos
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Guarda el archivo de movimientos
      Parameters: var DescError:AnsiString
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function GuardarArchivoMovimientos(var DescError : AnsiString) : Boolean;
    Resourcestring
        MSG_ERROR  = 'No se pudo guardar archivo de movimientos';
        MSG_CANCEL = 'Se ha cancelado el proceso debido a la existencia de un ' + CRLF +
                     'archivo id�ntico en el directorio de destino';
    Const
        FILE_TITLE   = ' El archivo:  ';
        FILE_EXISTS  = ' ya existe en el directorio de destino. �Desea reemplazarlo?';
        FILE_WARNING = ' Atenci�n';
    begin
        //Verificamos que no exista uno con el mismo nombre
        if FileExists(FNombreArchivoMovimientos) then begin
            if (MsgBox( FILE_TITLE + FNombreArchivoMovimientos + FILE_EXISTS,FILE_WARNING , MB_YESNO + MB_ICONQUESTION) = IDNO) then begin
                DescError := MSG_CANCEL; //informo que fue cancelado
                Result := False; //fallo
                Exit; //y salgo
            end;
        end;
        //creo el archivo
        try
            Result := StringToFile(FLista.text, FNombreArchivoMovimientos);
            if not Result then DescError := MSG_ERROR;
        except
            raise Exception.Create(MSG_ERROR);
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: GuardarNumeroSecuencia
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Guardo el numero de secuencia
      Parameters: CodigoOperacionInterfase:integer;NumeroSecuencia:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function GuardarNumeroSecuencia(CodigoOperacionInterfase:integer;NumeroSecuencia:integer):boolean;
    resourcestring
        MSG_ERROR = 'Error al guardar el numero de secuencia: ';
    var
        DescError : AnsiString;
    begin
        try
            //Actualizo el numero de secuencia
            ActualizarNumeroSecuencia(DMConnections.BaseCAC,
                                         CodigoOperacionInterfase,
                                         NumeroSecuencia,
                                         DescError
                                        );
            Result := True;
        except
            raise Exception.Create(MSG_ERROR + descerror );
        end;
    end;

Resourcestring
    MSG_SAVE_FILE_ERROR = 'No se pudo Guardar el Archivo: ';
    MSG_ERROR = 'Error';
var
    DescError : Ansistring;
begin
    Result := False;
    if not Assigned(FLista) or (FLista.Count = 0) then Exit;
    try
        Result :=  (//Grabo el archivo,
                    GuardarArchivoMovimientos(DescError) and
                    //Y Guardo el numero de secuencia
                    GuardarNumeroSecuencia(FCodigoOperacion,FNumeroSecuencia)
                   );
        if not Result then begin
            DeleteFile(FNombreArchivoMovimientos);
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR , DescError, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    except
        on e: Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR , e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    lgisuk
  Date Created: 04/01/2005
  Description: Genera el Archivo de Movimientos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoMovimientos.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerUltimoEnvioInterfaz
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Obtengo la fecha en la que se proceso la ultima interfaz
      Parameters: None
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerUltimoEnvioInterfaz : Boolean;
    Resourcestring
        MSG_ERROR = 'No se pudo obtener ultimo envio interfaz';
    var
        SQLFechaDesde : string;
    begin
        //si esta en modo re-proceso
        if pnlReprocesar.Visible = true then begin
            Result := True;
            Exit;
        end;
        //Obtengo la fecha en la que se ejecuto la ultima interfaz
        try
            SQLFechaDesde := Format('SELECT dbo.UltimoEnvioInterfaz (%d)', [23]);
            FFechaDesde := QueryGetValueDateTime(DMConnections.BaseCAC, SQLFechaDesde);
            Result := True;
        except
            on e: Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Actualizo el log al finalizar
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ActualizarLog : Boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVACION = 'Finalizo OK!';
    var
         DescError : String;
    begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);
        except
            on e : Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

resourcestring
    MSG_PROCESS_CANCEL = 'Proceso cancelado';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
  	MSG_PROCESS_FINALLY_OK = 'El proceso finaliz� con �xito';
Const
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
    STR_OBTAINING_DATA     = 'Obteniendo los Datos...';
    STR_SAVE_FILE          = 'Guardando el Archivo...';
begin
  	btnprocesar.Enabled := False;
    PnlAvance.visible := True;
  	Screen.Cursor := crHourGlass;
    KeyPreview := True;
    try

        //Obtengo la fecha en la que se proceso la ultima interfaz
        if not ObtenerUltimoEnvioInterfaz then begin
            FErrorGrave := True;
            Exit;
        end;

        //Registro la operaci�n en el Log
        Lblmensaje.Caption := STR_REGISTER_OPERATION;          //Informo Tarea
        Application.ProcessMessages;                           //Refresco la pantalla
        if not RegistrarOperacion then begin
            FErrorGrave := True;
            Exit;
        end;

        // Obtengo los datos y genero el archivo
        Lblmensaje.Caption := STR_OBTAINING_DATA;               //Informo Tarea
        Application.ProcessMessages;                           //Refresco la pantalla
  	    if not ObtenerMandatos then begin
            FErrorGrave := True;
            Exit;
        end;

        // Guardo el archivo
        Lblmensaje.Caption := STR_SAVE_FILE;                    //Informo Tarea
        Application.ProcessMessages;                           //Refresco la pantalla
  	    if not GuardarArchivo then begin
            FErrorGrave := True;
            Exit;
        end;

        //Actualizo el log al Final
        ActualizarLog;

    finally
      	Screen.Cursor := crDefault;
        pbProgreso.Position := 0;
        PnlAvance.visible := False;
    		lblMensaje.Caption := '';
        if FCancelar then begin
            //Muestro mensaje que el proceso fue cancelado  (sin errores)
            MsgBox(MSG_PROCESS_CANCEL, Self.Caption, MB_OK + MB_ICONINFORMATION);
        end else if FErrorGrave then begin
            //Muestro mensaje que finalizo por un error
            MsgBox(MSG_PROCESS_FINALLY_WITH_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
        end else begin
            //Muestro mensaje que el proceso finalizo exitosamente
            MsgBox(MSG_PROCESS_FINALLY_OK, Self.Caption, MB_OK + MB_ICONINFORMATION);
        end;
        KeyPreview := False;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 25/04/2005
  Description: Permito Cancelar
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoMovimientos.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 25/04/2005
  Description: Permite salir solo si no esta procesando.
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoMovimientos.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando.
  	CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 25/04/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoMovimientos.btnSalirClick(Sender: TObject);
begin
	  Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 25/04/2005
  Description: Lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoMovimientos.FormClose(Sender: TObject;var Action: TCloseAction);
begin
  	Action := caFree;
end;

initialization
    FLista   := TStringList.Create;
finalization
    FreeAndNil(Flista);
end.
