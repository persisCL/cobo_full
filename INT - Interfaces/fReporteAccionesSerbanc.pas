unit fReporteAccionesSerbanc;

interface

uses
 // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils,
    // Parametros GEnerales
    ConstParametrosGenerales,
   // DB
    UtilDB, DMConnection,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB, ppDBPipe, ppPrnabl, ppClass,
  ppCtrls, ppBands, ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB,
  ppStrtch, ppSubRpt, ppModule, raCodMod;


type
  TfrmReporteAccionesSerbanc = class(TForm)
    spObtenerDatosReporteAccionesSerbanc: TADOStoredProc;
    dsReporte: TDataSource;
    ppReportAcciones: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLblFechaProceso: TppLabel;
    ppLblNombreArchivo: TppLabel;
    ppLblNumeroProceso: TppLabel;
    ppLblRecibidos: TppLabel;
    ppLblRechazados: TppLabel;
    ppLblValidos: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLine1: TppLine;
    lbl_usuario: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    RBI: TRBInterface;
    ppDBReporte: TppDBPipeline;
    ppDBReporteppField1: TppField;
    ppDBReporteppField2: TppField;
    ppLabel12: TppLabel;
    ppLblErroneos: TppLabel;
    spErrores: TADOStoredProc;
    dsErrores: TDataSource;
    PipelineErrores: TppDBPipeline;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailBand3: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppLabel8: TppLabel;
    ppLine2: TppLine;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLine3: TppLine;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLabel13: TppLabel;
    ppLine4: TppLine;
    ppLine5: TppLine;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    raCodeModule1: TraCodeModule;
    raCodeModule2: TraCodeModule;
    ppLabel11: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppLabel16: TppLabel;
    ppDBCalc2: TppDBCalc;
    raCodeModule3: TraCodeModule;
    ppLblUsuario: TppLabel;
    procedure RBIExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigoOperacionInterfase: Integer;
    FError: AnsiString;
    function MostrarReporte(CodigoOperacionInterfase: Integer; Titulo: AnsiString; var Error: AnsiString): boolean;
  end;

var
  frmReporteAccionesSerbanc: TfrmReporteAccionesSerbanc;

implementation

{$R *.dfm}
function TfrmReporteAccionesSerbanc.MostrarReporte(CodigoOperacionInterfase: Integer; Titulo: AnsiString; var Error: AnsiString): boolean;
resourcestring
    MSG_CANCELED = 'Ejecuci�n del reporte cancelada por el usuario';
begin
    Result := False;
    FCodigoOperacionInterfase := CodigoOperacionInterfase;
    try
        RBI.Caption := 'Reporte de ' + Titulo;
        if not RBI.Execute(True) then begin
            Error := FError;
            Exit;
        end;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
            Exit;
        end;
    end;
end;

procedure TfrmReporteAccionesSerbanc.RBIExecute(Sender: TObject; var Cancelled: Boolean);
const
    FORMATO_CANTIDAD     = '#,##0' ;
    FORMATO_FECHA        = 'dd-mm-yyyy';
var
    FechaProceso: TDateTime;
    UsuarioProceso, NombreArchivo: AnsiString;
    RegValidos, RegRecibidos, RegRechazados, RegErroneos: Integer;
begin
    // intento abrir el ssp de datos del reporte

    try
        spObtenerDatosReporteAccionesSerbanc.Close;
        spObtenerDatosReporteAccionesSerbanc.Parameters.ParamByName('@CodigoOperacionInterfase').Value :=  FCodigoOperacionInterfase;
        spObtenerDatosReporteAccionesSerbanc.Open;

        spErrores.Close;
        spErrores.Parameters.ParamByName('@CodigoOperacionInterfase').Value :=  FCodigoOperacionInterfase;
        spErrores.Open;

        FechaProceso    :=  spObtenerDatosReporteAccionesSerbanc.Parameters.ParamByName('@FechaProceso').Value;
        UsuarioProceso  :=  spObtenerDatosReporteAccionesSerbanc.Parameters.ParamByName('@Usuario').Value;
        NombreArchivo   :=  spObtenerDatosReporteAccionesSerbanc.Parameters.ParamByName('@Archivo').Value;
        RegValidos      :=  spObtenerDatosReporteAccionesSerbanc.Parameters.ParamByName('@RegistrosValidos').Value;
        RegRechazados   :=  spObtenerDatosReporteAccionesSerbanc.Parameters.ParamByName('@RegistrosRechazados').Value;
        RegErroneos     :=  spObtenerDatosReporteAccionesSerbanc.Parameters.ParamByName('@RegistrosErroneos').Value;
        RegRecibidos    :=  spObtenerDatosReporteAccionesSerbanc.Parameters.ParamByName('@RegistrosRecibidos').Value;
    except
        on e:exception do begin
            FError := e.Message;
            Cancelled := True;
            Exit;
        end;
    end;

    CurrencyString := '$';

    pplblNumeroProceso.Caption  := Format( 'Proceso N� %d', [FCodigoOperacionInterfase]);
    pplblFechaProceso.Caption   := Format( 'Fecha de Proceso: %s; Hora: %s',[FormatDateTime(FORMATO_FECHA, FechaProceso),FormatDateTime('HH:nn', FechaProceso)]);
    ppLblUsuario.Caption        := TRIM(UsuarioProceso);
    ppLblRecibidos.Caption      := FormatFloat(FORMATO_CANTIDAD,RegRecibidos);
    ppLblRechazados.Caption     := FormatFloat(FORMATO_CANTIDAD,RegRechazados);
    ppLblValidos.Caption        := FormatFloat(FORMATO_CANTIDAD,RegValidos);
    ppLblErroneos.Caption       := FormatFloat(FORMATO_CANTIDAD,RegErroneos);
    ppLblNombreArchivo.Caption  := TRIM(NombreArchivo);
end;


end.
