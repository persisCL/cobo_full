{-------------------------------------------------------------------------------
 File Name: frmRptDayPassCopec.pas
 Author:    
 Date Created: 
 Language: ES-AR
 Description: Gestion de Interfases - Reporte de DayPass Copec
              Informa la cantidad de DayPass recibidos , el importe total y
              otra informacion util para verificar si el proceso de recepcion
              se realizo segun parametros normales.

  Revision 1
  Author : mpiazza
  Date: 16-07-2009
  Description: ss-653 se agregan al reporte el importe unitario por categoria importeunitarioCategoria1#

  Revision 2
  Author : pdominguez
  Date   : 05/08/2010
  Description : SS 909 - BHTU Copec - Rechazos
    - Se a�aden/modifican los siguientes objetos:
        spObtenerErroresInterfase, spObtenerAdvertenciasInterfase: TADOStoredProc.
        dsObtenerErroresInterfase, dsObtenerAdvertenciasInterfase: TDataSource.
        ppDBPipeline2: TppDBPipeline.
        Se a�aden los par�metros @TotalErrores y @TotalAdvertencias al objeto
        SpObtenerReporteRecepcionDayPass: TADOStoredProc.
        ppReporte: TppReport.
        ppSubReport1, ppSubReport2: TppSubReport.
        ppCantidadAdvertencias: TppLabel.

    - Se a�adieron/modificaron los siguientes Procedimientos/Funciones
        RBIListadoExecute



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
unit frmRptDayPassCopec;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  UtilProc,                  //Mensajes
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppParameter, ppBands, ppCtrls, ppPrnabl, ppClass, ppCache,
  ppProd, ppReport, ppDB, ppComm, ppRelatv, ppDBPipe, DB, ADODB, ppStrtch,
  ppSubRpt, ConstParametrosGenerales;                                           //SS_1147_NDR_20140710;

type
  TRptDayPassForm = class(TForm)
    dsObtenerErroresInterfase: TDataSource;
    SpObtenerReporteRecepcionDayPass: TADOStoredProc;
    ppDBPipeline1: TppDBPipeline;
    //RBIListado: TRBInterface;
    ppReporte: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppModulo: TppLabel;
    pptFechaProcesamiento: TppLabel;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    pptCantidadComprobantes: TppLabel;
    ppFechaProcesamiento: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppCantidadComprobantes: TppLabel;
    ppLine3: TppLine;
    ppLine1: TppLine;
    ppLabel15: TppLabel;
    ppLabel17: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppParameterList1: TppParameterList;
    ppCantidadAceptados: TppLabel;
    ppCantidadRechazados: TppLabel;
    ppLabel16: TppLabel;
    ppCantidadTotal: TppLabel;
    ppCantidadAdvertencias: TppLabel;
    ppLabel20: TppLabel;
    spObtenerErroresInterfase: TADOStoredProc;
    spObtenerAdvertenciasInterfase: TADOStoredProc;
    ppDBPipeline2: TppDBPipeline;
    dsObtenerAdvertenciasInterfase: TDataSource;
    ppSummaryBand1: TppSummaryBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLabel14: TppLabel;
    ppLine4: TppLine;
    ppLine5: TppLine;
    ppDetailBand2: TppDetailBand;
    ppDBText1: TppDBText;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppLabel19: TppLabel;
    ppLine7: TppLine;
    ppLine8: TppLine;
    ppDetailBand3: TppDetailBand;
    ppDBText2: TppDBText;
    ppSubReport4: TppSubReport;
    ppChildReport4: TppChildReport;
    ppTitleBand4: TppTitleBand;
    ppDetailBand5: TppDetailBand;
    ppLabel1: TppLabel;
    ppLine6: TppLine;
    ppLabel2: TppLabel;
    ppDBText4: TppDBText;
    ppLabel3: TppLabel;
    ppDBText5: TppDBText;
    ppLabel4: TppLabel;
    ppDBText6: TppDBText;
    ppLabel5: TppLabel;
    ppDBText7: TppDBText;
    RBIListado: TRBInterface;
    ppImporteTotal: TppLabel;
    pptImporteTotal: TppLabel;
    ppDiferenciaMontos: TppLabel;
    pplblDiferenciaMontos: TppLabel;
    pplblMontoArchivo: TppLabel;
    ppMontoArchivo: TppLabel;
    ppLabel6: TppLabel;
    ppLine2: TppLine;
    dsObtenerReporteRecepcionDayPass: TDataSource;
    ppDBPipeline3: TppDBPipeline;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FNombreReporte: Ansistring;
    FCodigoOperacionInterfase: integer;
    FTotalRegistros, FTotalErrores : integer;
  public
    { Public declarations }
    function Inicializar (NombreReporte: AnsiString; CodigoOperacionInterfase:integer; ReporteBHTU: Boolean = False): Boolean;
  end;

var
  RptDayPassForm: TRptDayPassForm;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    rharris
  Date Created: 15/05/2008
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean

  Revision 1
  Author: mbecerra
  Date: 10-Junio-2008
  Description:
            	Se modifica para agregar dos par�metros solicitados por el cliente:
                  * El total de registros en el archivo
                  * El total de registros rechazados

                * El total de registros aceptados corresponde ahora al par�metro
                retornado por el stored.

                Y debiese coincidir que: Total Registros = Total Aceptados + Total con Errores

    Revision 2
    Author: mbecerra
    Date: 26-Junio-2008
    Description:
                	Ahora los valores son tomados de la tabla LogOperacionesInterfases:
                    	* Total Aceptados = RegistrosProcesados
                        * Total Procesados = LineasArchivo
                        * Total Rechazados = LineasArchivo - RegistrosProcesados

-----------------------------------------------------------------------------}

function TRptDayPassForm.Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase:integer; ReporteBHTU: Boolean = False): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin

    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    FNombreReporte            := NombreReporte;
    FCodigoOperacionInterfase := CodigoOperacionInterfase;
    Result := True;

    if not ReporteBHTU then begin
        pplblDiferenciaMontos.Visible := False;
        pplblMontoArchivo.Visible     := False;
        ppMontoArchivo.Visible        := False;
        ppDiferenciaMontos.Visible    := False;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute(
  Author:
  Date Created:
  Description: Ejecuta y carga el reporte
  Parameters: Sender: TObject; Cancelled: Boolean
  Return Value:

  Revision 1
  Author : mpiazza
  Date: 16-07-2009
  Description: ss-653 se agregan al reporte el importe unitario por categoria importeunitarioCategoria1#

  Revision 2
  Author : pdominguez
  Date   : 05/08/2010
  Description : SS 909 - BHTU Copec - Rechazos
    - Se a�aden los par�metros @TotalErrores y @TotalAdvertencias al SP
    SpObtenerReporteRecepcionDayPass.
    - Se a�ade la obtenci�n de los Errores y Advertencias.
-----------------------------------------------------------------------------}
procedure TRptDayPassForm.RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
	MSG_ERROR_AL_GENERAR_REPORTE = 'Error al generar reporte';
    MSG_ERROR = 'Error';
//var
    //Config: TRBConfig;
begin
    //configuraci�n del reporte
//    Config := rbiListado.GetConfig;
//    if Config.BinName <> '' then rbiListado.Report.PrinterSetup.BinName := Config.BinName;

    try
    	with SpObtenerReporteRecepcionDayPass do begin
          if Active then Close;
        	Parameters.Refresh;
        	Parameters.ParamByName('@CodigoOperacionInterfase').Value	:= FCodigoOperacionInterfase;
        	Parameters.ParamByName('@Modulo').Value 					:= NULL;
        	Parameters.ParamByName('@FechaProcesamiento').Value 		:= NULL;
        	Parameters.ParamByName('@NombreArchivo').Value				:= NULL;
        	Parameters.ParamByName('@Usuario').Value 					:= NULL;
        	Parameters.ParamByName('@CantidadComprobantes').Value		:= NULL;
        	Parameters.ParamByName('@ImporteTotal').Value				:= NULL;
        	Parameters.ParamByName('@CantidadCategoria1').Value			:= NULL;
        	Parameters.ParamByName('@ImporteCategoria1').Value			:= NULL;
        	Parameters.ParamByName('@CantidadCategoria2').Value			:= NULL;
        	Parameters.ParamByName('@ImporteCategoria2').Value			:= NULL;
        	Parameters.ParamByName('@CantidadCategoria3').VALUE			:= NULL;
        	Parameters.ParamByName('@ImporteCategoria3').Value			:= NULL;
        	Parameters.ParamByName('@CantidadCategoria4').Value			:= NULL;
        	Parameters.ParamByName('@ImporteCategoria4').Value			:= NULL;
			Parameters.ParamByName('@importeunitarioCategoria1').Value := NULL;
			Parameters.ParamByName('@importeunitarioCategoria2').Value := NULL;
			Parameters.ParamByName('@importeunitarioCategoria3').Value := NULL;
			Parameters.ParamByName('@importeunitarioCategoria4').Value := NULL;
			Parameters.ParamByName('@TotalRegistros').Value             := NULL;
			Parameters.ParamByName('@TotalProcesados').Value            := NULL;
			Parameters.ParamByName('@TotalErrores').Value := NULL; // Rev. 2 (SS 909)
			Parameters.ParamByName('@TotalAdvertencias').Value := NULL; // Rev. 2 (SS 909)
        	CommandTimeOut := 500;
        	ExecProc;

        	//Asigno los valores a los campos del reporte
        	ppLtitulo.Caption				:= 'Reporte ' + FNombreReporte;
        	ppmodulo.Caption				:= Parameters.ParamByName('@Modulo').Value;
        	ppFechaProcesamiento.Caption	:= DateToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
        	ppNombreArchivo.Caption			:= Copy(Parameters.ParamByName('@NombreArchivo').Value,1,40);
        	ppUsuario.Caption				:= Parameters.ParamByName('@Usuario').Value;
            {Revision 1
        	ppCantidadComprobantes.Caption	:= IntToStr(Parameters.ParamByName('@CantidadComprobantes').Value);}
            {Revision 2
            ppCantidadComprobantes.Caption  := IntToStr(FTotalRegistros);
            ppCantidadRechazados.Caption    := IntToStr(FTotalErrores);}
            {Nota: @CantidadComprobantes es tomado de la tabla DaypassRecibidos y
              debiese tener el mismo valor que @TotalProcesados}
            ppCantidadComprobantes.Caption  := Parameters.ParamByName('@CantidadComprobantes').Value;
            ppCantidadTotal.Caption         := Parameters.ParamByName('@TotalRegistros').Value;
            ppCantidadAceptados.Caption     := Parameters.ParamByName('@TotalProcesados').Value;
            ppCantidadRechazados.Caption    := Parameters.ParamByName('@TotalErrores').Value; // Rev. 2 (SS 909)
            ppCantidadAdvertencias.Caption  := Parameters.ParamByName('@TotalAdvertencias').Value; // Rev. 2 (SS 909)

            ppDiferenciaMontos.Caption      := Parameters.ParamByName('@DiferenciaMontos').Value; // Rev. 2 (SS 909)
            ppMontoArchivo.Caption          := Parameters.ParamByName('@MontoArchivo').Value; // Rev. 2 (SS 909)

            { ** fin Revision 1 *** }
        	ppImporteTotal.Caption			:= Parameters.ParamByName('@ImporteTotal').Value;
          // SS 1080 - MDV - 20130124 Se suprimen lo campos de categor�as que se mostraban desde variable
          // estos ser�n desplegados via recordset desde el sp  ObtenerReporteRecepcionDayPass
        	{pplabel6.Caption 				:= IntToStr(Parameters.ParamByName('@CantidadCategoria1').Value);
        	pplabel7.Caption 				:= IntToStr(Parameters.ParamByName('@CantidadCategoria2').Value);
        	pplabel8.Caption 				:= IntToStr(Parameters.ParamByName('@CantidadCategoria3').Value);
        	pplabel9.Caption 				:= IntToStr(Parameters.ParamByName('@CantidadCategoria4').Value);
        	pplabel10.Caption 				:= Parameters.ParamByName('@ImporteCategoria1').Value;
        	pplabel11.Caption 				:= Parameters.ParamByName('@ImporteCategoria2').Value;
        	pplabel12.Caption 				:= Parameters.ParamByName('@ImporteCategoria3').Value;
        	pplabel13.Caption 				:= Parameters.ParamByName('@ImporteCategoria4').Value;
          importeunitarioCategoria1.Caption := Parameters.ParamByName('@importeunitarioCategoria1').Value;
          importeunitarioCategoria2.Caption := Parameters.ParamByName('@importeunitarioCategoria2').Value;
          importeunitarioCategoria3.Caption := Parameters.ParamByName('@importeunitarioCategoria3').Value;
          importeunitarioCategoria4.Caption := Parameters.ParamByName('@importeunitarioCategoria4').Value;  }
          // FIN SS 1080
        	//Configuro el Titulo del Report Builder
        	rbiListado.Caption :=  FNombreReporte;
    	end;

        // Rev. 2 (SS 909)
        with spObtenerErroresInterfase do begin
            if Active then Close;
        	Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        	Parameters.ParamByName('@CodigoTipoErrorInterfase').Value := 1;
            Open;
        end;

        with spObtenerAdvertenciasInterfase do begin
            if Active then Close;
        	Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        	Parameters.ParamByName('@CodigoTipoErrorInterfase').Value := 0;
            Open;
        end;
        // Fin Rev. 2 (SS 909)

    except
        on E: exception do begin
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;

end;

end.
