{-------------------------------------------------------------------------------
 File Name: FrmRptLibroVentas.pas
 Author:    lgisuk
 Date Created: 02/11/2005
 Language: ES-AR
 Description: M�dulo de la interface Contable - Libro de Ventas

 Revision: 1
 Author: nefernandez
 Date: 07/05/2007
 Description: No incluir los comprobantes 'NC' en este reporte

 
Revision 2
Author : FSandi
Date : 19-06-2007
Description : Se unifican los diferentes tipos de comprobantes en una sola consulta.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-------------------------------------------------------------------------------}
unit FrmRptLibroVentas;

interface

uses
  //Libro de Ventas
  DMConnection,
  PeaTypes,
  Util,
  UtilDB,
  UtilProc,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StrUtils, ExtCtrls, ComCtrls, StdCtrls, Validate, DateEdit,
  DB, ADODB, UtilRB, ppProd, ppClass, ppReport, ppComm, ppRelatv, ppDB,
  ppDBPipe, ppPrnabl, ppCtrls, ppBands, ppCache, ppModule, daDataModule,
  ppParameter, ppStrtch, ppSubRpt, raCodMod, ExtDlgs, Buttons;

type
  TFRptLibroVentas = class(TForm)
    de_FechaDesde: TDateEdit;
    de_FechaHasta: TDateEdit;
    btn_Imprimir: TButton;
    btn_Salir: TButton;
    lbl_FechaDesde: TLabel;
    lbl_FechaHasta: TLabel;
    pnl_Avance: TPanel;
    lbl_ProgesoGeneral: TLabel;
    pb_ProgresoGeneral: TProgressBar;
    Bevel: TBevel;
    lbl_Mensaje: TLabel;
    ppLibroVentas: TppReport;
    RbiLibroVentas: TRBInterface;
    ppParameterList1: TppParameterList;
    sp_LibroVentas_ObtenerDatosComprobantes: TADOStoredProc;
    ds_ObtenerDatosReporteLibroVentas_Comprobantes: TDataSource;
    pp_ObtenerDatosReporteLibroVentas_Comprobantes: TppDBPipeline;
    sp_LibroVentas_ProcesarComprobantes: TADOStoredProc;
    gr: TppHeaderBand;
    ppLabel6: TppLabel;
    ppLblPeriodo: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppSubReportBoletas: TppSubReport;
    ppChildReport3: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppDetailBand4: TppDetailBand;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppSummaryBand4: TppSummaryBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppShape2: TppShape;
    ppLabel21: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    ppDBCalc4: TppDBCalc;
    ppDBCalc5: TppDBCalc;
    raCodeModule4: TraCodeModule;
    ppSummaryBand1: TppSummaryBand;
    ppLabel3: TppLabel;
    ppValorAfectoAIVATotal: TppLabel;
    ppValorExcentodeIvaTotal: TppLabel;
    ppIVATotal: TppLabel;
    ppDocumentosTotal: TppLabel;
    ppLine5: TppLine;
    ppLine6: TppLine;
    ppCantidadTotal: TppLabel;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppLine3: TppLine;
    ppLine13: TppLine;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    ppLabel32: TppLabel;
    ppLabel35: TppLabel;
    ppLabel36: TppLabel;
    ppGroupFooterBand2: TppGroupFooterBand;
    raCodeModule2: TraCodeModule;
    chkGuardar: TCheckBox;
    DlgGuardar: TSaveTextFileDialog;
    txtArchivo: TEdit;
    BTNSeleccionar: TSpeedButton;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppShape1: TppShape;
    ppDBText20: TppDBText;
    ppLabel22: TppLabel;
    ppLabel2: TppLabel;
    ppDBText19: TppDBText;
    procedure BTNSeleccionarClick(Sender: TObject);
    procedure chkGuardarClick(Sender: TObject);
    procedure RbiLibroVentasExecute(Sender: TObject;var Cancelled: Boolean);
    procedure btn_ImprimirClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_SalirClick(Sender: TObject);
  private
    { Private declarations }
    FCancelar : Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure CrearTablasTemporales;
    function  ObtenerCantidadComprobantes(Conn : TADOConnection ; FechaDesde, FechaHasta : TDateTime; TipoCompro: AnsiString) : Int64;
    procedure ProcesarComprobantes;
    procedure EliminarTablasTemporales;
    procedure ImprimirPipeline(unPipeline: TppDataPipeline; var unArchivo: TextFile);
    procedure GuardarComoTexto;
  public
    { Public declarations }
    function Inicializar : Boolean;
  end;

var
  FRptLibroVentas : TFRptLibroVentas;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 02/11/2005
  Description: Inicializaci�n de este formulario
  Parameters:
  Return Value: None
  Revision :
      Author : vpaszkowicz
      Date : 20/09/2006
      Description : Arranco con las componentes para grabar deshabilitadas
-----------------------------------------------------------------------------}
function TFRptLibroVentas.Inicializar : Boolean;
begin
    try
        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
        pnl_Avance.Hide;
        lbl_Mensaje.Caption := EmptyStr;
        CenterForm(Self);
        Visible := False;
        de_FechaDesde.Date := Date;
        de_FechaHasta.Date := de_FechaDesde.Date;
    	  Result := True;
        txtArchivo.Enabled := chkGuardar.Checked;
        BtnSeleccionar.Enabled := chkGuardar.Checked;
    except
        Result := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CrearTablasTemporales
  Author:    lgisuk
  Date Created: 02/11/2005
  Description: Crea las tablas temporales que se utilizan en el Libro de Ventas
  Parameters:
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptLibroVentas.CrearTablasTemporales;
var
    sp : TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection       := DMConnections.BaseCAC;
        sp.ProcedureName    := 'LibroVentas_CrearTablasTemporales';
        sp.ExecProc;
    finally
        sp.Free;
    end;
end;

{-----------------------------------------------------------------------------
Function Name: ObtenerCantidadComprobantes
Author : lgisuk
Date Created : 02/11/2005
Description : Retorna la cantidad de comprobantes de un Tipo que han sido emitidos entre dos fechas.
Parameters : FechaDesde, FechaHasta: TDateTime; TipoCompro: AnsiString
Return Value : Int64
-----------------------------------------------------------------------------}
function TFRptLibroVentas.ObtenerCantidadComprobantes(Conn: TADOConnection;FechaDesde, FechaHasta: TDateTime; TipoCompro: AnsiString): Int64;
var
    codigolibro : integer;
begin
    CodigoLibro := QueryGetValueInt(Conn, 'SELECT DBO.CONST_LIBRO_VENTAS()');
    Result := QueryGetValueInt(Conn, 'SELECT dbo.LibroAuxVentasPeajes_CantidadComprobantes(' + QuotedStr(FormatDateTime('YYYYMMDD', FechaDesde)) + ', ' + QuotedStr(FormatDateTime('YYYYMMDD', FechaHasta)) + ', '+ InttoStr(CodigoLibro) +')');
end;


{-----------------------------------------------------------------------------
  Function Name: ProcesarBoletas
  Author:    lgisuk
  Date Created: 03/11/2005
  Description: Procesa las Boletas a mostrar en el libro de ventas.
  Parameters:
  Return Value: None

  Revision 2:
  Author:  FSandi
  Date: 19-06-2007
  Description: Se cambia el nombre a la funcion, dado que ahora procesara todos los comprobantes
  Revision : 3
      Author : vpaszkowicz
      Date : 14/08/2007
      Description : Comento el while y quito los top en los storeds porque se
      quedaba loco el store cuando tenia que procesar el ultimo grupo de 100.
-----------------------------------------------------------------------------}
procedure TFRptLibroVentas.ProcesarComprobantes;
resourcestring
    MSG_ERROR_PROCESSING = 'Ha ocurrido un error al procesar los Comprobantes.';
var
    CantidadComprobantes: Int64;
    NumeroComprobanteDesde,
    NumeroComprobanteHasta: Int64;
begin
    // Inicializar las variables para que entre al menos una vez al ciclo.
    NumeroComprobanteDesde := -2;
    NumeroComprobanteHasta := -1;

    // Inicializar la barra de progreso
    CantidadComprobantes := ObtenerCantidadComprobantes(DMConnections.BaseCAC, de_FechaDesde.Date, de_FechaHasta.Date, TC_BOLETA);
    pb_ProgresoGeneral.Position := 1;
    pb_ProgresoGeneral.Max      := CantidadComprobantes + 2;

    //Le quito esta parte para que procese todos juntos
    //while (NumeroComprobanteDesde <> NumeroComprobanteHasta) and (not FCancelar) do begin

        NumeroComprobanteDesde := NumeroComprobanteHasta + 1;

        try
            with sp_LibroVentas_ProcesarComprobantes do begin
                Close;
                CommandTimeout := 500;

                Parameters.Refresh;
                Parameters.ParamByName('@FechaDesde').Value := de_FechaDesde.Date;
                Parameters.ParamByName('@FechaHasta').Value := de_FechaHasta.Date;
                Parameters.ParamByName('@NumeroComprobanteDesde').Value := NumeroComprobanteDesde;
                Parameters.ParamByName('@NumeroComprobanteHasta').Value := 0;

                ExecProc;

                // Obtener el �ltimo Numero de comprobante procesado.
                NumeroComprobanteHasta := Parameters.ParamByName('@NumeroComprobanteHasta').Value;
            end;
        except
            on E: Exception do begin
                raise Exception.Create(MSG_ERROR_PROCESSING + CRLF + E.Message);
            end;
        end;

        // Incrementar la barra de progreso
        pb_ProgresoGeneral.StepIt;
        if pb_ProgresoGeneral.Position >= pb_ProgresoGeneral.Max then begin
            // Inicializar la barra de progreso
            pb_ProgresoGeneral.Position := 1;
        end;

        // Refrescar la pantalla
        Application.ProcessMessages;
    //end;

    // Finalizar la barra de progreso.
    pb_ProgresoGeneral.Position := pb_ProgresoGeneral.Max;
    Application.ProcessMessages;

end;


{-----------------------------------------------------------------------------
  Function Name: EliminarTablasTemporales
  Author:    lgisuk
  Date Created: 02/11/2005
  Description: Elimino las tablas temporales creadas
  Parameters:
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptLibroVentas.EliminarTablasTemporales;
var
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection       := DMConnections.BaseCAC;
        sp.ProcedureName    := 'LibroVentas_EliminarTablasTemporales';
        sp.ExecProc;
    finally
        sp.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 02/11/2005
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
  Revision :
      Author : vpaszkowicz
      Date : 20/09/2006
      Description :Le agrego la posibilidad de expoertar como txt desde c�digo
      ya que como contiene subreportes el report builder no puede hacerlo auto-
      m�ticamente
-----------------------------------------------------------------------------}
procedure TFRptLibroVentas.RbiLibroVentasExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_ERROR_ON_GENERATED_THE_REPORT = 'Ha ocurrido un error al generar el reporte.';
var
    ValorAfectoAIVATotal : Real;
    ValorExcentoDeIVATotal : Real;
    IVATotal : Real;
    DocumentosTotal : Real;
    CantidadTotal : Real;
begin
    try
        //Titulo
        ppLblPeriodo.caption := 'Periodo Desde: '+ DateToStr(De_FechaDesde.Date) +' Hasta: '+ DateToStr(De_FechaHasta.Date);


        //Boletas
        with sp_LibroVentas_ObtenerDatosComprobantes, Parameters do begin
            Refresh;
            ParamByname('@ValorAfectoAIVA').Value := 0;
            ParamByname('@ValorExcentoDeIVA').Value := 0;
            ParamByname('@TotalIVA').Value := 0;
            ParamByname('@TotalDelDocumento').Value := 0;
            ParamByname('@CantidadTotal').Value := 0;
            Close;
            CommandTimeout := 1000;
            Open;
            //Sub Total Notas de Boletas
            ValorAfectoAIVATotal := ParamByname('@ValorAfectoAIVA').Value;
            ValorExcentoDeIVATotal := ParamByname('@ValorExcentoDeIVA').Value;
            IVATotal := ParamByname('@TotalIVA').Value;
            DocumentosTotal := ParamByname('@TotalDelDocumento').Value;
            CantidadTotal := ParamByname('@CantidadTotal').Value;
        end;

        //Total del libro de Ventas
        ppValorAfectoAIVATotal.Caption := FloatToStr(ValorAfectoAIVATotal);
        ppValorExcentoDeIVATotal.Caption := FloatToStr(ValorExcentoDeIVATotal);
        ppIVATotal.Caption := FloatToStr(IVATotal);
        ppDocumentosTotal.Caption := FloatToStr(DocumentosTotal);
        ppCantidadTotal.Caption := FloatToStr(CantidadTotal);
        if chkGuardar.Checked and (txtArchivo.Text <> '') then
            GuardarComoTexto;
    except
        on E: Exception do begin
            Cancelled := True;
            raise Exception.Create(MSG_ERROR_ON_GENERATED_THE_REPORT + CRLF + E.Message);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_ImprimirClick
  Author:    lgisuk
  Date Created: 02/11/2005
  Description: Permito Imprimir el libro de Ventas
  Parameters:
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptLibroVentas.btn_ImprimirClick(Sender: TObject);
resourcestring
    MSG_ERROR_TO_OBTAIN_DATA = 'Ha ocurrido un error al obtener los datos para el reporte.';
    MSG_PROCESS_CANCEL   = 'Proceso cancelado.';
    MSG_INVALID_DATE     = 'La fecha ingresada no es v�lida.';
    MSG_DATE_FROM_MUST_BE_PREVIOUS_OR_EQUAL_TO_DATE_EVEN = 'La Fecha Desde debe ser anterior o igual a la Fecha Hasta.';
    MSG_ERROR_PATH = 'Debe indicar un directorio de exportaci�n';
const
    STR_DELETE_TEMPORARY_TABLES = 'Eliminando tablas temporales ...';
    STR_CREATE_TEMPORARY_TABLES = 'Creando tablas temporales ...';
//Revision 2
    STR_PROCESAR_COMPROBANTES = 'Procesando Comprobantes ...';
//Fin de Revision 2
    STR_PRINTING_REPORT = 'Imprimiendo el reporte ...';
begin
    FCancelar := False;
    if chkGuardar.Checked and (txtArchivo.Text = '') then
    begin
       MsgBoxBalloon(MSG_ERROR_PATH, 'Error', MB_ICONSTOP, txtArchivo);
       Exit;
    end;
    // Controlar que se hayan ingresado las fechas.
    // Controlar que la Fecha Desde sea anterior o igual a la Fecha Hasta
    if not ValidateControls([de_FechaDesde,
            de_FechaHasta,
            de_FechaDesde],
            [de_FechaDesde.Date >= 0,
            de_FechaHasta.Date >= 0,
            de_FechaDesde.Date <= de_FechaHasta.Date],
            Self.Caption,
            [MSG_INVALID_DATE,
            MSG_INVALID_DATE,
            MSG_DATE_FROM_MUST_BE_PREVIOUS_OR_EQUAL_TO_DATE_EVEN]) then begin
        Exit;
    end;

    de_FechaDesde.Enabled   := False;
    de_FechaHasta.Enabled   := False;
    btn_Imprimir.Enabled    := False;
    pnl_Avance.Show;
  	Screen.Cursor   := crHourGlass;
    KeyPreview      := True;
    try
        try
            lbl_Mensaje.Caption := STR_DELETE_TEMPORARY_TABLES;
            Application.ProcessMessages;
            EliminarTablasTemporales;

            lbl_Mensaje.Caption := STR_CREATE_TEMPORARY_TABLES;
            Application.ProcessMessages;
            CrearTablasTemporales;


            if FCancelar then Exit;
            lbl_Mensaje.Caption := STR_PROCESAR_COMPROBANTES;   //Revision 2
            Application.ProcessMessages;
            ProcesarComprobantes;

            if FCancelar then Exit;
            lbl_Mensaje.Caption := STR_PRINTING_REPORT;
            Application.ProcessMessages;
            RbiLibroVentas.Execute;

            if FCancelar then Exit;
            EliminarTablasTemporales;

            // Cerrar el form, porque al ser Dialog, queda sobre el reporte.
            Close;
        except
            on E: Exception do begin
                MsgBoxErr(MSG_ERROR_TO_OBTAIN_DATA, E.Message, Self.Caption, MB_ICONSTOP);
            end;
        end;
    finally
      	Screen.Cursor   := crDefault;
        pnl_Avance.Hide;
        pb_ProgresoGeneral.Position := 0;
        lbl_Mensaje.Caption     := EmptyStr;
        de_FechaDesde.Enabled   := True;
        de_FechaHasta.Enabled   := True;
        btn_Imprimir.Enabled    := True;

        if FCancelar then begin
            // Eliminar las tablas temporales
            EliminarTablasTemporales;
            FCancelar := False;
            // Mostrar el mensaje de proceso cancelado (sin errores).
            MsgBox(MSG_PROCESS_CANCEL, Self.Caption, MB_OK + MB_ICONINFORMATION);
        end
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 02/11/2005
  Description: Permito Cancelar
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptLibroVentas.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created:  02/11/2005
  Description: Permite salir solo si no esta procesando.
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptLibroVentas.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    // Permitir salir s�lo si no esta procesando.
    CanClose := not pnl_Avance.Visible;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_SalirClick
  Author:    lgisuk
  Date Created:  02/11/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptLibroVentas.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created:  02/11/2005
  Description: Lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptLibroVentas.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

{******************************** Function Header ******************************
Function Name: chkGuardarClick
Author : vpaszkowicz
Date Created : 20/09/2006
Description : Habilita/Deshabilita las componentes para grabar archivos de texto.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFRptLibroVentas.chkGuardarClick(Sender: TObject);
begin
    txtArchivo.Enabled := chkGuardar.Checked;
    BtnSeleccionar.Enabled := chkGuardar.Checked;
    if chkGuardar.Checked and (txtArchivo.Text = '') then BtnSeleccionarClick(Sender);
end;

{******************************** Function Header ******************************
Function Name: ImprimirPipeline
Author : vpaszkowicz
Date Created : 20/09/2006
Description : Imprime todos los campos de un pipeline pasado como parametro en un
archivo tambien pasado como parametro.
Parameters : unPipeline: TppDataPipeline; var unArchivo: TextFile
Return Value : None
*******************************************************************************}

procedure TFRptLibroVentas.ImprimirPipeline(unPipeline: TppDataPipeline; var unArchivo: TextFile);
var
    unaLinea, ultLinea: string;
    i: integer;
begin
    with unPipeline do begin
        First;
        while not Eof do begin
            unaLinea := '';
            ultLinea := '';
            i := FieldCount;
            for i := 0 to i - 1 do begin
                if (ultLinea <> '') and (Trim(unPipeline.Fields[i].asstring) <> '') then unaLinea := unaLinea + ',';
                unaLinea := unaLinea + Trim(unPipeline.Fields[i].asstring);
                ultLinea := Trim(unPipeline.Fields[i].asstring);
            end;
            Writeln(unArchivo, unaLinea);
            Next;
            ultLinea := '';
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: GuardarComoTexto
Author : vpaszkowicz
Date Created : 20/09/2006
Description : Guarda el reporte en un archivo de texto en un camino previamente
seleccionado en la componente dlgGuardar
Parameters : None
Return Value : None
*******************************************************************************}

procedure TFRptLibroVentas.GuardarComoTexto;
var
    unArchivo: TextFile;
    cabecera: string;
begin
    try
        //Verifica que se halla seleccionado un directorio de exportacion
        if Trim(txtArchivo.Text) = '' then begin
            MsgBoxBalloon('Debe indicar un directorio de exportaci�n', 'Error', MB_ICONSTOP, txtArchivo);
            Exit;
        end;
        AssignFile(unArchivo, DlgGuardar.FileName);
        Rewrite(unArchivo);
        //Le asigno la cabecera a mano porque tiene l�neas desparejas
        Cabecera := QuotedStr('Tipo de documento') + ',' ;
        Cabecera := Cabecera + QuotedStr('Fecha de emisi�n') + ',';
        Cabecera := Cabecera + QuotedStr('N�mero') + ',';
        Cabecera := Cabecera + QuotedStr('Nombre del Cliente') + ',';
        Cabecera := Cabecera + QuotedStr('RUT') + ',';
        Cabecera := Cabecera + QuotedStr('Valor Afecto') + ',';
        Cabecera := Cabecera + QuotedStr('Valor Exento') + ',';
        Cabecera := Cabecera + QuotedStr('Total IVA') + ',';
        Cabecera := Cabecera + QuotedStr('Total del Documento') + ',';
        Cabecera := Cabecera + QuotedStr('C�digo Impresora Fiscal') + ',';
        Cabecera := Cabecera + QuotedStr('N�mero de Serie') + ',';
        Cabecera := Cabecera + QuotedStr('Descripci�n Impresora Fiscal');
        Writeln(unArchivo, Cabecera);
{        ImprimirPipeline(pp_ObtenerDatosReporteLibroVentas_Facturas, unArchivo);}
        ImprimirPipeline(pp_ObtenerDatosReporteLibroVentas_Comprobantes, unArchivo);
{        ImprimirPipeline(pp_ObtenerDatosReporteLibroVentas_NotasCredito, unArchivo);}
        CloseFile(unArchivo);
    except
        ShowMessage('No se pudo crear el archivo de texto');
    end;
end;

{******************************** Function Header ******************************
Function Name: BTNSeleccionarClick
Author : vpaszkowicz
Date Created : 20/09/2006
Description : Permite cambiar la ubicaci�n del archivo de texto y crear uno nuevo
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}

procedure TFRptLibroVentas.BTNSeleccionarClick(Sender: TObject);
begin
    //Abre una ventana para seleccionar un directorio
    DlgGuardar.Execute;
    if DlgGuardar.FileName <> '' then txtArchivo.Text := DlgGuardar.FileName;
end;

end.
