unit fRececpcionArchivoInfraccionesClearingBHTU;
{----------------------------------------------------------------------------------------------------------------------------

Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

----------------------------------------------------------------------------------------------------------------------------}
interface

uses
  // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils, DateUtils,
    // Parametros Generales
    ConstParametrosGenerales,
    // DB
    UtilDB, DMConnection,
    // Reporte finalizacion
    fReporteRecepcionInfraccionesBHTU,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes,  Graphics,  Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB,     ppDBPipe, ppPrnabl, ppClass,
    ppCtrls, ppBands,  ppCache,  ppComm,   ppRelatv, ppProd,    ppReport, UtilRB;


type
   TInfraccion = record
        Concesionaria   : byte;
        FechaInfraccion : TDateTime;
        Patente         : string;
        Categoria       : byte;
        Portico         : string;
   end;

   TTransito = record
        NumCorrCA       : int64;
        Concesionaria   : byte;
        Fecha           : TDateTime;
        Patente         : string;
        Categoria       : byte;
        Portico         : string;
   end;

   TTipoTolerancia = (INFERIOR, SUPERIOR);

  TfrmRecepcionArchivoInfraccionesClearingBHTU = class(TForm)
    Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    txtOrigen: TPickEdit;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    OpenDialog: TOpenDialog;
    Label1: TLabel;
    txtOrigenTransitos: TPickEdit;
    spPrepararRecepcionArchivosClearingBHTU: TADOStoredProc;
    spAgregarInfraccionClearingBHTU: TADOStoredProc;
    spAgregarTransitoClearingBHTU: TADOStoredProc;
    spEliminarTemporalesInfraccionesClearingBHTU: TADOStoredProc;
    lblEsc: TLabel;
    lblDetalle: TLabel;
    spRegistrarArchivo: TADOStoredProc;
    spObtenerUltimoArchivo: TADOStoredProc;
    spSeleccionarTransitoInfraccionClearingBHTU: TADOStoredProc;
    spRegistrarErrorTransitosDuplicados: TADOStoredProc;
    spEncontrarTransitosDuplicadosClearingBHTU: TADOStoredProc;
    procedure txtOrigenTransitosButtonClick(Sender: TObject);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure txtOrigenButtonClick(Sender: TObject);
  private
    { Private declarations }
    FProcesando,
    FCancelar       : boolean;
    FOrigen         : AnsiString; // Directorio de orgien propuesto desde P.Gral.
    FListaErroresInfracciones: TStringList;
    FListaErroresTransitos: TStringList;
    FCantInf, FCantTrx: Integer;
    FFechaArchivo   : TDateTime;
    FConcesionaria   : byte;
    Reproceso       : Integer;
    function  CargarInfracciones( NombreArchivo: AnsiString; var Lista: TStringList; var Error: AnsiString): boolean;
    function  CargarTransitos( var Lista: TStringList; var Error: AnsiString): boolean;
    function  ValidarFechaInfraccionArchivo( Fecha, FechaArchivo: TDateTime): boolean;
    function  ValidarFechaTransitoArchivo( Fecha, FechaArchivo: TDateTime): boolean;
    function  ObtenerToleranciaBHTU( Concesionaria: Byte; Fecha: TDateTime; Tipo: TTIpoTolerancia ): Integer;
    function  AgregarErroresValidacion(ListaErrores: TStringList; CodigoOperacion: Integer; var Error: AnsiString): boolean;
    function  AgregarRegistroArchivo( Fecha: TDateTime; ArchivoInfracciones, ArchivoTransitos: AnsiString; CodigoOperacionInterfase: Integer; Reproceso: Integer; CantidadInfracciones, CantidadTransitos: Integer; var Error: AnsiString): Boolean;
    function  ValidarSecuenciaArchivo(FechaArchivo: TDateTime; var Mensaje: AnsiString): Boolean;
    function  MostrarReporte(CodigoOperacion: Integer): boolean;
    procedure EliminarTemporales;
    procedure HabilitarBotones;
    procedure ActualizarProgreso(Delta: Integer = 1; Mensaje: AnsiString = '');
    function  ProcesarInfraccion(CodigoOperacion: Integer; Infraccion: TInfraccion; var descError: AnsiString): boolean;
    function  ParsearInfraccion(Linea: AnsiString; var Infraccion: TInfraccion;
      descError: AnsiString): boolean;
    function  AbrirArchivoTransitos(NombreArchivo: AnsiString; var Lista: TStringList;
      var descError: AnsiString): Boolean;
    function ValidarDatosArchivoInfracion(NombreArchivo: AnsiString;
      var Error: AnsiString): Boolean;
  public
    { Public declarations }
    function Inicializar( Titulo: AnsiString) : Boolean;
  end;

const
    SEP             = ';';
    MAX_LARGO_PATENTE   = 10;
    MIN_LARGO_PATENTE   = 6;
var
  frmRecepcionArchivoInfraccionesClearingBHTU: TfrmRecepcionArchivoInfraccionesClearingBHTU;

implementation

{$R *.dfm}

procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.txtOrigenButtonClick(Sender: TObject);
    {******************************** Function Header ******************************
    Function Name: ObtenerFiltro
    Author : ndonadio
    Date Created : 10/08/2005
    Description :
    Parameters : None
    Return Value : AnsiString
    *******************************************************************************}
    Function ObtenerFiltro: AnsiString;
    Const
        FILE_TITLE	   = 'Archivo de Infracciones Clearing BHTU|';
        FILE_NAME 	   = 'infractores_';
        FILE_EXTENSION = '.dat';
    begin
            Result:= FILE_TITLE + FILE_NAME + '*' + FILE_EXTENSION;
    end;

resourcestring
    MSG_ERROR = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST   = 'El archivo %s no existe';
  	MSG_ERROR_INVALID_FILENAME 		= 'El nombre del archivo %s es inv�lido';
  	MSG_FILE_ALREADY_PROCESSED 		= 'El archivo %s ya fue procesado';
var
    Filtro: string;
begin
    //obtengo el filtro a aplicar a este tipo de archivos
    Filtro := ObtenerFiltro;
    btnProcesar.Enabled := False;

    Opendialog.InitialDir := txtOrigen.Text;;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if opendialog.execute then begin
        txtOrigen.text := UpperCase( opendialog.filename );

        if not FileExists( txtOrigen.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

    	if  VerificarArchivoProcesado( DMConnections.BaseCAC, txtOrigen.text ) then begin
    			MsgBox (Format(MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtOrigen.text)]),Caption, MB_ICONSTOP);
                Exit;
    	end;

        // En tanto no haya que elegir otro archivo para transitos...
        txtOrigenTransitos.Text := ReplaceStr(UPPERCASE(txtOrigen.text), '.DAT', '.TRX');
        // si se eleigi� un archivo v�lido, se peude procesar...
        btnProcesar.Enabled := FileExists(txtOrigen.text) AND FileExists(txtOrigenTransitos.text) ;
	end;

end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : ndonadio
Date Created : 27/09/2005
Description : Cerrar el formulario
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author : ndonadio
Date Created : 27/09/2005
Description : Procesar los archivos seleccionados
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.btnProcesarClick(Sender: TObject);
resourcestring
    ERROR_FILES_NOT_EXISTS              = 'El Archivo de Infracciones seleccionado no existe.';
    ERROR_LOADING_INFRACTIONS           = 'Error al cargar las Infracciones';
    ERROR_LOADING_TRANSACTIONS          = 'Error al cargar los tr�nsitos';
    ERROR_CREATING_TEMP_TABLES          = 'Error al crear las tablas temporales del proceso.';
    ERROR_INSERTING_VALIDATION_ERRORS   = 'No se pueden almacenar los errores. El proceso es cancelado.';
    ERROR_PROCESSING_INFRACTIONS        = 'Error al procesar las infracciones.';
    ERROR_PROCESSING_TRANSACTIONS       = 'Error al procesar los Tr�nsitos.';
    ERROR_REGISTERING_OPERATION         = 'Ha ocurrido un error al registrar la operaci�n en el Log de Interfases';
    ERROR_SAVING_FILE_DATA              = 'Error al guardar los datos del archivo recibido.';
    ERROR_CLOSING_OPERATION             = 'Ha ocurrido un error al intentar cerrar la Operaci�n.';
    ERROR_CANT_VALIDATE_FILE_SEQUENCE   = 'Error al validar la secuencia de archivo.';
    ERROR_CANT_VALIDATE_FILE_DATA       = 'No se puede obtener la informaci�n necesaria para validar la secuencia de archivo.';


    PROCESS_END_OK                  = 'El proceso termin� con �xito.';
    PROCESS_FAILED                  = 'El proceso fall�';
    PROCESS_CANCELLED               = 'Proceso cancelado por el usuario.';

    PROGRESS_REGISTER_LOG           = 'Registrando Inicio de la Operaci�n';
    PROGRESS_CREATING_TABLES        = 'Creando Tablas temporales auxiliares';
    PROGRESS_LOADING_INF            = 'Cargando Infracciones';
    PROGRESS_LOADING_TRX            = 'Cargando Tr�nsitos';
    PROGRESS_REGISTERING_VAL_ERRORS = 'Registrando Errores de Validaci�n';
    PROGRESS_REGISTERING_DUP_ERRORS = 'Registrando Errores de Duplicaci�n';
    PROGRESS_PROCESSING_INF         = 'Procesando Infracciones';
    PROGRESS_PROCESSING_TRX         = 'Procesando Tr�nsitos';
    PROGRESS_CLOSING_LOG            = 'Cerrando Operaci�n';
    PROGRESS_CREATING_FILE_REGISTER = 'Creando Registro de Archivo Recibido';
    ERROR_OPENING_TRANSACTIONS      = 'Error al abrir archivo de Tr�nsitos';
const
    EXT_INF     = '.dat';
    EXT_TRX     = '.trx';
var
    ArchivoInfracciones,
    ArchivoTransitos,
    Mensaje,
    descError               : AnsiString;
    CodigoOperacionInterfase: integer;
    IndexInfraccion         : Integer;
    CurrentInfraccion       : TInfraccion;
    ListaInfracciones       : TStringList;
    ListaTransitos          : TStringList;

begin
    pbProgreso.Min := 0;
    pbProgreso.Max := 10000;
    FProcesando := True;
    HabilitarBotones;
    FListaErroresInfracciones   := TStringList.Create;
    FListaErroresTransitos      := TStringList.Create;
    ListaInfracciones           := TStringList.Create;
    ListaTransitos              := TStringList.Create;
    Cursor := crHourGlass;
    try
        // Verifico los nombres de los arhcivos
        ArchivoInfracciones := txtOrigen.Text;
        ArchivoTransitos    := txtOrigenTransitos.Text;
        if not ValidateControls( [txtOrigen],
          [FileExists(ArchivoInfracciones) AND FileExists(ArchivoTransitos)],
          Caption, []) then Exit;
        if not ValidarDatosArchivoInfracion(ArchivoInfracciones, descError) then begin
                if MsgBox( Format(ERROR_CANT_VALIDATE_FILE_DATA, [ExtractFileName(ArchivoInfracciones)])+CRLF+CRLF+descError , Caption, MB_ICONERROR+MB_YESNO) = mrNO then  Exit;
        end;

        // Verifico que el archivo sea el que se espera
        try
            if (not ValidarSecuenciaArchivo(FFechaArchivo, Mensaje)) then
                if ( MsgBox( Mensaje, Caption, MB_ICONWARNING+MB_YESNO) = mrNo ) then Exit;
        except
            on e:exception do begin
                MsgBoxErr( Format(ERROR_CANT_VALIDATE_FILE_SEQUENCE, [ExtractFileName(ArchivoInfracciones)]), e.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;


        // Registro el inicio de operaci�n...
        if not RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRADA_INFRACCIONES_CLEARING_BHTU, ArchivoInfracciones, UsuarioSistema, '',
            True, False, NowBase(DMConnections.BaseCAC), 0, CodigoOperacionInterfase, descError) then begin
                MsgBoxErr( ERROR_REGISTERING_OPERATION, descError, Caption, MB_ICONERROR);
                Exit;
        end;
        // Creo las temporales para procesar
        try
            spPrepararRecepcionArchivosClearingBHTU.ExecProc;
        except
            on e:exception do begin
                MsgBoxErr( ERROR_CREATING_TEMP_TABLES, e.Message, caption, MB_ICONERROR);
                Exit;
            end;
        end;
        if not AbrirArchivoTransitos(ArchivoTransitos, ListaTransitos, descError) then begin
            MsgBoxErr( ERROR_OPENING_TRANSACTIONS, descError, caption, MB_ICONERROR);
            Exit;
        end;
        // Abro archivo de infracciones...
        if not CargarInfracciones(ArchivoInfracciones, ListaInfracciones, descError) then begin
            MsgBoxErr( ERROR_LOADING_INFRACTIONS, descError, caption, MB_ICONERROR);
            Exit;
        end;
        ActualizarProgreso( 1, PROGRESS_CREATING_FILE_REGISTER);
        if not AgregarRegistroArchivo(FFechaArchivo, ArchivoInfracciones, ArchivoTransitos,
          CodigoOperacionInterfase, Reproceso, FCantInf, FCantTrx, descError) then begin
            MsgBoxErr(ERROR_SAVING_FILE_DATA, descError, Caption, MB_ICONERROR);
            Exit;
        end;
        // Cargo los transitos
        ActualizarProgreso( 1, PROGRESS_LOADING_TRX);
        if not CargarTransitos(ListaTransitos, descError) then begin
            MsgBoxErr( ERROR_LOADING_TRANSACTIONS, descError, caption, MB_ICONERROR);
            Exit;
        end;
        // por cada infraccion
        // proceso las infracciones BHTU. de a 1
        IndexInfraccion := 0;
        while (IndexInfraccion < ListaInfracciones.Count ) and (not FCancelar) do begin
            if ParsearInfraccion(ListaInfracciones[IndexInfraccion], CurrentInfraccion, descError) then begin
                if not ProcesarInfraccion(CodigoOperacionInterfase, CurrentInfraccion, descError ) then begin
                    MsgBoxErr( ERROR_PROCESSING_INFRACTIONS, descError, caption, MB_ICONERROR);
                    Exit;
                end;
            end;
            inc(IndexInfraccion);
        end; // while

        // Registro los errores de las infracciones
        if not AgregarErroresValidacion(FListaErroresInfracciones, CodigoOperacionInterfase, descError) then begin
            MsgBoxErr(ERROR_INSERTING_VALIDATION_ERRORS, descError, Caption, MB_ICONERROR);
            Exit;
        end;
        // Registro los errores de los transitos
        ActualizarProgreso(1, PROGRESS_REGISTERING_VAL_ERRORS);
        if not AgregarErroresValidacion(FListaErroresTransitos, CodigoOperacionInterfase, descError) then begin
            MsgBoxErr(ERROR_INSERTING_VALIDATION_ERRORS, descError, Caption, MB_ICONERROR);
            Exit;
        end;
        // si no cancel�, termino Ok...
        if not FCancelar then begin
            // Cierro el log...
            ActualizarProgreso(1, PROGRESS_CLOSING_LOG);
            if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, CodigoOperacionInterfase, PROCESS_END_OK , descError) then begin
                //DMConnections.BaseCAC.RollbackTrans;                                                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fInfraccionesClearingBHTU END');	    //SS_1385_NDR_20150922
                MsgBoxErr(ERROR_CLOSING_OPERATION, descError, Caption, MB_ICONERROR);
                Exit;
            end;
            pbProgreso.Position := 10000;
            // Commit de la transaccion
            MsgBox(PROCESS_END_OK, Caption, MB_ICONINFORMATION);
            // Muestro el reporte
            MostrarReporte( CodigoOperacionInterfase );
        end
        else begin
            // Si en alg�n punto cancel�... hago un rollback
            pbProgreso.Position := 10000;
            if not ActualizarObservacionesEnLogInterface(DMCOnnections.BaseCAC, CodigoOperacionInterfase, PROCESS_CANCELLED, True, descError) then
                // No pude actualizar asi que muestro un mensaje de error...
                MsgBoxErr( ERROR_CLOSING_OPERATION + CRLF + CRLF + PROCESS_CANCELLED, descError, Caption, MB_ICONWARNING)
            else // y muestro el mensaje de proceso cancelado...
                MsgBox(PROCESS_CANCELLED, Caption, MB_ICONWARNING);
        end;
    finally
        Cursor := crDefault;
        FProcesando := False;
        FCancelar := False;
        FListaErroresInfracciones.Free;
        EliminarTemporales;
        HabilitarBotones;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnCancelarClick
Author : ndonadio
Date Created : 27/09/2005
Description : Cancelar el procesamiento
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.btnCancelarClick(Sender: TObject);
begin
    FCancelar := True;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : ndonadio
Date Created : 27/09/2005
Description :   Cuando el usuario quiere cerrar el form, se verifica que no se
                est� procesando
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
resourcestring
    MSG_CANT_CLOSE_ON_PROCESSING =  'No se puede cerrar la ventana mientras est� procesando.' + CRLF +
                                    'Por favor, Cancele el proceso si desea Cerrar, o espere a que termine normalmente.';
begin
    CanClose := (NOT FProcesando);
    if FProcesando then   MsgBoxBalloon(MSG_CANT_CLOSE_ON_PROCESSING, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : ndonadio
Date Created : 27/09/2005
Description : Al cerrar el form....
Parameters : Sender: TObject; var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : ndonadio
Date Created : 27/09/2005
Description : cambia el cursor cuando pasa sobre el pnlAyuda
Parameters : Sender: TObject; Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.ImgAyudaMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
    if FProcesando then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : ndonadio
Date Created : 27/09/2005
Description : Mietrsa una breve ayuda de la pantalla
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_AYUDA  = ' ' + CRLF +
                          'Los archivos de Infracciones son enviados' + CRLF +
                          'por las concesionarias adheridas ' + CRLF +
                          'como parte del proceso de Clearing de BHTU.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if FProcesando then exit;
    //Muestro el mensaje
        MsgBoxBalloon(MSG_AYUDA, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: CargarInfracciones
Author : ndonadio
Date Created : 27/09/2005
Description : Levanta el archivo de infracciones a un string list, lo parsea,
                valida y guarda en un temporal.
Parameters : NombreArchivo: AnsiString; var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.CargarInfracciones(NombreArchivo: AnsiString;
  var Lista: TStringList;
  var Error: AnsiString): boolean;
resourcestring
    ERROR_FILENAME_INVALID_DATE         = 'Infracci�n con Error - ' + 'El nombre del archivo de infracciones no contiene una fecha v�lida (%s)';
    ERROR_FILENAME_INVALID_HIGHWAY      = 'Infracci�n con Error - ' + 'El nombre del archivo de infracciones no contiene un valor que pueda interpretarse como C�digo de Concesionaria. (%s)';
    ERROR_PARSE_INVALID_TAN             = 'Infracci�n con Error - ' + 'El valor %s no puede interpretarse como una Patente. (%s)';
    ERROR_PARSE_INVALID_DATE            = 'Infracci�n con Error - ' + 'El valor %s no puede interpretarse como una Fecha. (%s)';
    ERROR_PARSE_INVALID_CATEGORY        = 'Infracci�n con Error - ' + 'El valor %s no puede interpretarse como una Categor�a. (%s)';
    WARNING_DATE_NOT_IN_RANGE           = 'Infracci�n con Error - ' + 'La fecha de infracci�n %s no es correcta para el archivo con fecha %s (Registro: %s)';
    ERROR_NO_INFRACTION_ON_FILE         = 'El archivo est� vacio.';
var
    i               : Integer;
    Infraccion      : TInfraccion;
    descError       : AnsiString;
begin
    Result := False;
    try
        // Cargo el archivo en la lista
        Lista.LoadFromFile(NombreArchivo);
        if Lista.Count = 0 then  begin
            Error := ERROR_NO_INFRACTION_ON_FILE;
            Exit;
        end;
        FCantInf := Lista.Count;
        // calculo el maximo para la barra de progreso
        pbProgreso.Max := FCantInf * 2 + FCantTRx + 7;
        pbProgreso.StepIt;
        // Ciclo por la lista
        i := 0;
        while  (i < Lista.Count) and (not FCancelar) do begin
            if not ParsearInfraccion(Lista[i], Infraccion, descError) then begin
                FListaErroresInfracciones.Add(descError);
                Continue;
            end;
            // Paso a la siguiente infraccion en el archivo...
            inc(i);
            ActualizarProgreso(1, lblDetalle.Caption);
        end;
        Result := True;
    finally
        pbProgreso.Position := FCantInf + 1;
    end;
end;

{******************************** Function Header ******************************
Function Name: CargarTransitos
Author : ndonadio
Date Created : 27/09/2005
Description : Parsea, valida y guarda en un temporal el archivo de transitos
Parameters : NombreArchivo: AnsiString; var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.CargarTransitos(var Lista: TStringList;
  var Error: AnsiString): boolean;
resourcestring
    ERROR_FILENAME_INVALID_DATE     = 'Tr�nsito con Error - ' + 'El nombre del archivo de tr�nsitos no contiene una fecha v�lida (%s)';
    ERROR_FILENAME_INVALID_HIGHWAY  = 'Tr�nsito con Error - ' + 'El nombre del archivo de tr�nsitos no contiene un valor que pueda interpretarse como C�digo de Concesionaria. (%s)';
    ERROR_PARSE_INVALID_TAN         = 'Tr�nsito con Error - ' + 'El valor %s no puede interpretarse como una Patente. (%s)';
    ERROR_PARSE_INVALID_NUMCORRCA   = 'Tr�nsito con Error - ' + 'El valor %s no puede interpretarse como un NumCorrCA. (%s)';
    ERROR_PARSE_INVALID_DATE        = 'Tr�nsito con Error - ' + 'El valor %s no puede interpretarse como una Fecha. (%s)';
    ERROR_PARSE_INVALID_CATEGORY    = 'Tr�nsito con Error - ' + 'El valor %s no puede interpretarse como una Categor�a. (%s)';
    WARNING_DATE_NOT_IN_RANGE       = 'Tr�nsito con Error - ' + 'La fecha de tr�nsito %s no es correcta para el archivo con fecha %s (Registro: %s)';
var
    i               : Integer;
    Concesionaria   : byte;
    Transito        : TTransito;
    strFecha        : AnsiString;
    descError       : AnsiString;
    Origen          : Integer;
    FechaArchivo    : TDateTime;
begin
    Result := False;
    Origen := pbProgreso.Position;
    concesionaria := FConcesionaria;
    try
        FechaArchivo := FFechaArchivo;
       // Ciclo por la lista
        i := 0;
        while  (i < Lista.Count) and (not FCancelar) do begin
            // Parseo
            Transito.Concesionaria := Concesionaria;

            // El NumCorrCA
            try
                Transito.NumCorrCA  :=  StrToInt(TRIM(ParseParamByNumber(Lista.Strings[i], 1, SEP)));
            except
                FListaErroresTransitos.Add(Format(ERROR_PARSE_INVALID_NUMCORRCA, [TRIM(ParseParamByNumber(Lista.Strings[i], 1, SEP)), Lista.Strings[i]]));
                inc(i);
                Continue;
            end;
            // La Patente
            Transito.Patente := TRIM(ParseParamByNumber(Lista.Strings[i], 2, SEP));
            if (Length(Transito.Patente) > MAX_LARGO_PATENTE)
                or (Length(Transito.Patente) < MIN_LARGO_PATENTE) then begin
                // No se agrega porque el largo de patente es invalido...
                FListaErroresTransitos.Add(Format(ERROR_PARSE_INVALID_TAN, [Transito.Patente, Lista.Strings[i]]));
                inc(i);
                Continue;
            end;

            // La Fecha
            strFecha := TRIM(ParseParamByNumber(Lista.Strings[i], 3, SEP));
            strFecha := COPY(strFEcha, 5,4) + COPY(strFEcha, 3,2) + COPY(strFecha, 1,2) + COPY(strFEcha, 9,14);
            if not FechaHoraAAAAMMDDToDateTime(DMConnections.BaseCac, strFecha, Transito.Fecha, descError) then begin
                FListaErroresTransitos.Add(Format(ERROR_PARSE_INVALID_DATE, [strFecha, Lista.Strings[i]]));
                inc(i);
                Continue;
            end;
            // si la fecha de la infraccion esta fuera del intervalo de fechas del archivo,
            // reporto el error, pero la cargo igual
            if not ValidarFechaTransitoArchivo(Transito.Fecha, FechaArchivo) then
               FListaErroresTransitos.Add(Format(WARNING_DATE_NOT_IN_RANGE, [TRIM(ParseParamByNumber(Lista.Strings[i], 3, SEP)), DateToStr(FechaArchivo), Lista.Strings[i]]));

            // La Categoria
            try
                Transito.Categoria := StrToInt(TRIM(ParseParamByNumber(Lista.Strings[i], 4, SEP)));
            except
                FListaErroresTransitos.Add(Format(ERROR_PARSE_INVALID_CATEGORY, [TRIM(ParseParamByNumber(Lista.Strings[i], 4, SEP)), Lista.Strings[i]]));
                inc(i);
                Continue;
            end;
            Transito.Portico := TRIM(ParseParamByNumber(Lista.Strings[i], 5, SEP));

            // Cargo en la temporal
            try
                with spAgregarTransitoClearingBHTU do begin
                    Parameters.ParamByName('@Concesionaria').Value  := Transito.Concesionaria ;
                    Parameters.ParamByName('@NumCorrCA').Value      := Transito.NumCorrCA ;
                    Parameters.ParamByName('@Patente').Value        := Transito.Patente ;
                    Parameters.ParamByName('@Fecha').Value          := Transito.Fecha;
                    Parameters.ParamByName('@Portico').Value        := Transito.Portico ;
                    Parameters.ParamByName('@Categoria').Value      := Transito.Categoria;
                    ExecProc;
                end;
            except
                // Error lo suficientemente grave como para abortar el proceso...
                on e:exception do begin
                    Error := e.Message;
                    Exit;
                end;
            end;
            // Paso a la siguiente infraccion en el archivo...
            inc(i);
            ActualizarProgreso(1, lblDetalle.Caption);
        end;
        Result := True;
    finally
        // libero el string list
        Lista.Free;
        pbProgreso.Position := Origen + FCantTrx;
    end;
end;

{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 27/09/2005
Description :   Habilita o dehabilita los botones de acuerdo a FProcesando
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.HabilitarBotones;
begin
    FCancelar := False;
    if not FProcesando then begin           // Si no estoy procesando
        txtOrigen.Text := FOrigen;          // blanqueo los txt de origen
        txtOrigenTransitos.Text := FOrigen;
    end;
    btnSalir.Enabled    := NOT FProcesando;
    btnProcesar.Enabled := NOT FProcesando AND ( ExtractFileName(txtOrigen.Text) <> '' ) AND ( FileExists(txtOrigen.text) );
    btnCancelar.Enabled := FProcesando;
    lblDetalle.Caption  := '';
    pnlAvance.Visible   := FProcesando;
    pbProgreso.Position := pbProgreso.Min;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 28/09/2005
Description : Inicializa el formulario, cargar paramteros generales, etc.
Parameters : Titulo: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.Inicializar(Titulo: AnsiString): Boolean;
resourcestring
    CANNOT_INITIALIZE   = 'No se puede inicializar la interfaz';
    ERROR_P_GRAL        = 'Error al cargar el Par�metro General "%s".' ;
const
    DIR_ORIGEN_INFRACCIONES_BHTU = 'DIR_ORIGEN_INFRAC_CLEARING_BHTU';
begin
    Result := False;
    lblDetalle.Caption := '';
    // Acomodo el form
    Caption := Titulo;
    CenterForm(Self);
    // Cargo el par�metro general de directorio donde buscar los archivos
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_ORIGEN_INFRACCIONES_BHTU, FOrigen) then begin
        MsgBoxErr(CANNOT_INITIALIZE, Format( ERROR_P_GRAL, [DIR_ORIGEN_INFRACCIONES_BHTU]), Caption, MB_ICONSTOP);
        Exit;
    end;
    txtOrigen.text :=  FOrigen;
    txtOrigenTransitos.Text := FOrigen;
    FProcesando := False;
    HabilitarBotones;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: txtOrigenTransitosButtonClick
Author : ndonadio
Date Created : 30/09/2005
Description : Selecciona un nuevo arhcivo para procesar...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.txtOrigenTransitosButtonClick(
  Sender: TObject);
    {******************************** Function Header ******************************
    Function Name: ObtenerFiltro
    Return Value : AnsiString
    *******************************************************************************}
    Function ObtenerFiltro: AnsiString;
    Const
        FILE_TITLE	   = 'Archivo de Tr�nsitos Clearing BHTU|';
        FILE_NAME 	   = 'infractores_';
        FILE_EXTENSION = '.trx';
    begin
            Result:= FILE_TITLE + FILE_NAME + '*' + FILE_EXTENSION;
    end;

resourcestring
    MSG_ERROR = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST   = 'El archivo %s no existe';
  	MSG_ERROR_INVALID_FILENAME 		= 'El nombre del archivo %s es inv�lido';
  	MSG_FILE_ALREADY_PROCESSED 		= 'El archivo %s ya fue procesado';
var
    Filtro: string;
begin
    //obtengo el filtro a aplicar a este tipo de archivos
    Filtro := ObtenerFiltro;
    btnProcesar.Enabled := False;

    Opendialog.InitialDir := txtOrigen.Text;;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if opendialog.execute then begin
        txtOrigenTransitos.text:=UpperCase( opendialog.filename );

        if not FileExists( txtOrigenTransitos.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

    	if  VerificarArchivoProcesado( DMConnections.BaseCAC, txtOrigenTransitos.text ) then begin
    			MsgBox (Format(MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtOrigenTransitos.text)]),Caption, MB_ICONSTOP);
                Exit;
    	end;
        // si se eleigi� un archivo v�lido, se peude procesar...
        btnProcesar.Enabled := (ExtractFileName(txtOrigen.Text) <> '') AND FileExists(txtOrigen.text) AND FileExists(txtOrigenTransitos.text) ;
	end;
end;

{******************************** Function Header ******************************
Function Name: EliminarTemporales
Author : ndonadio
Date Created : 28/09/2005
Description : Elimina las tablas temporales utilizadas por el proceso.
                Si falla muestra un mensaje solicitando al operador que
                ciere la aplicaci�n para eliminar estas tablas.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.EliminarTemporales;
resourcestring
    MSG_CANT_DROP_TEMP_TABLES = 'No se han podido eliminar las tablas temporales' + CRLF +
                                'utilizadas en el proceso de carga de infracciones. ' + CRLF +
                                'Es recomendable que salga de la aplicaci�n ahora,' + CRLF +
                                'de forma que estas tablas se eliminen. ' + CRLF +
                                'Puede volver a entrar de inmediato si lo necesita.';
begin
    try
        spEliminarTemporalesInfraccionesClearingBHTU.ExecProc;
    except
        // solo muesrto un mensaje. Es un warning, no algo muy grave,
        // el proce puede haber terminado Ok o fallado.
        // No tiene nada que ver con esto.
        msgBox(MSG_CANT_DROP_TEMP_TABLES, Caption, MB_ICONWARNING);
    end;
end;

{******************************** Function Header ******************************
Function Name: ValidarFechaInfraccionArchivo
Author : ndonadio
Date Created : 28/09/2005
Description : Valida si una fecha se corresponde con la del archivo
                segunb la tabla de relacion de fechas...
Parameters : Fecha, FechaArchivo
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.ValidarFechaInfraccionArchivo(Fecha,
  FechaArchivo: TDateTime): boolean;
var
    DiaArchivo: Integer;
begin
    DiaArchivo := DayOfTheWeek(FechaArchivo); // 1: Lunes...
    Case DiaArchivo of
        // lunes -> lunes o martes anterios
        1: Result := ( Fecha = (FechaArchivo - 7) ) OR  ( Fecha = (FechaArchivo - 6));
        // martes -> miercoles anterior
        2: Result := ( Fecha = (FechaArchivo - 6) ) ;
        // miercoles -> jueves anterior
        3: Result := ( Fecha = (FechaArchivo - 6) ) ;
        // jueves -> viernes anterior
        4: Result := ( Fecha = (FechaArchivo - 6) ) ;
        // viernes -> sabado o domingo anterio
        5: Result := ( Fecha = (FechaArchivo - 6) ) OR  ( Fecha = (FechaArchivo - 5));
        6:  Result := False;  // no se hacen archivos los sabados
        7:  Result := False;  // no se hacen archivos los domingos;
    else Result := False;
    end;

end;

{******************************** Function Header ******************************
Function Name: ValidarFechaTransitoArchivo
Author : ndonadio
Date Created : 28/09/2005
Description : Valida que la fecha de los transitos caiga dentro de lo especificado
                por la tabla de relacion de fechas treniendo en cuenta los margenes
                de tolerancia para esa autopista y esa fecha...
Parameters : Fecha, FechaArchivo: TDateTime
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.ValidarFechaTransitoArchivo(Fecha,
  FechaArchivo: TDateTime): boolean;
var
    ToleranciaSuperior,
    ToleranciaInferior  : Integer;
    FechaHoraInferior,
    FechaHoraSuperior   : TDateTime;
    DiaArchivo          : byte;
begin
    Result := False;
    // Obtenemos las tolerancias para esa fecha y esa concesionaria
    ToleranciaSuperior  :=  ObtenerToleranciaBHTU(FConcesionaria, Fecha, SUPERIOR);
    ToleranciaInferior  :=  ObtenerToleranciaBHTU(FConcesionaria, Fecha, INFERIOR) * (-1); // la hacemos negativa para que reste...

    // Calculamos el rango de fechas/horas permitidas...
    DiaArchivo := DayOfTheWeek(FechaArchivo); // 1: Lunes...
    Case DiaArchivo of
        // lunes -> lunes o martes anterios
        1:  begin
                FechaHoraInferior := (FechaArchivo - 6);
                FechaHoraSuperior := (FechaArchivo - 6);
            end;
        // martes -> miercoles anterior
        2:  begin
                FechaHoraInferior := (FechaArchivo - 6);
                FechaHoraSuperior := (FechaArchivo - 6);
            end;
        // miercoles -> jueves anterior
        3: begin
                FechaHoraInferior := (FechaArchivo - 6);
                FechaHoraSuperior := (FechaArchivo - 6);
            end;
        // jueves -> viernes anterior
        4: begin
                FechaHoraInferior := (FechaArchivo - 6);
                FechaHoraSuperior := (FechaArchivo - 6);
            end;
        // viernes -> sabado o domingo anterio
        5: begin
                FechaHoraInferior := (FechaArchivo - 5);
                FechaHoraSuperior := (FechaArchivo - 6);
            end;
    else begin
            // si la fecha del archivo es sabado o domingo est� mal,
            // NO se generan archivos los fines de semana...
            Exit;
        end;
    end;
    // llevamos la fecha hora superior al ULTIMO segundo del d�a... 23:59:59
    FechaHoraSuperior :=  IncSecond( IncDay(FechaHoraSuperior,1), -1);
    // Ahora agregamos las tolerancias...
    FechaHoraSuperior := IncMinute(FechaHoraSuperior, ToleranciaSuperior);
    FechaHoraInferior := IncMinute(FechaHoraInferior, ToleranciaInferior);

    Result := ( Fecha >= FEchaHoraInferior ) AND ( Fecha <= FechaHoraSuperior );
end;

{******************************** Function Header ******************************
Function Name: ObtenerToleranciaBHTU
Author : ndonadio
Date Created : 28/09/2005
Description : Obtiene la tolerancia...
Parameters : Concesionaria: int; Fecha: TDateTime; Tipo: TTIpoTolerancia
Return Value : Integer
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.ObtenerToleranciaBHTU(Concesionaria: byte;
  Fecha: TDateTime; Tipo: TTIpoTolerancia): Integer;
const
    SQL_SUP = 'SELECT dbo.ObtenerToleranciaSuperiorBHTU( %d, ''%s'' )';
    SQL_INF = 'SELECT dbo.ObtenerToleranciaInferiorBHTU( %d, ''%s'' )';
var
    Tolerancia : Integer;
    strSQL: AnsiString;
begin

    if Tipo = SUPERIOR then
            strSQL := SQL_SUP
    else    strSQL := SQL_INF;

    try
        Tolerancia := QueryGetValueInt(DMCOnnections.BaseCAC,
          Format( strSQL,[Concesionaria, FormatDateTime('yyyy-mm-dd', Fecha)]));
        Result := Tolerancia;
    except
        Result := 0;
        Exit;
    end;

end;

{******************************** Function Header ******************************
Function Name: AgregarErroresValidacion
Author : ndonadio
Date Created : 28/09/2005
Description : Los errores cargados en la string list se cargan en la
                tabla ErroresInterfases
Parameters : ListaErrores: TStringList; CodigoOperacion: Integer; var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.AgregarErroresValidacion(
  ListaErrores: TStringList; CodigoOperacion: Integer; var Error: AnsiString): boolean;
var
    i: integer;
begin
    Result := False;
    i := 0;
    try
        // si tengo algo en la LIsta de Errores
        if ListaErrores.Count > 0 then begin
            ActualizarProgreso(1, lblDetalle.Caption );
            // por cada item...
            while (i < ListaErrores.Count) and (not FCancelar) do begin
                // lo agrego...
                AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacion, ListaErrores.Strings[i]);
                inc(i);     // paso al siguiente
            end;
        end;
        Result := True;
    except
        on e:exception do begin
            // si fallo, devuelvo el error...
            Error := e.Message;
            Exit;
        end;
    end;
end;


{******************************** Function Header ******************************
Function Name: ActualizarProgreso
Author : ndonadio
Date Created : 29/09/2005
Description : Actualiza la barra de progreso...
Parameters : Mensaje: AnsiString
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoInfraccionesClearingBHTU.ActualizarProgreso(Delta: Integer = 1; Mensaje: AnsiString = '');
begin
    if Delta = 0 then Delta := 1;
    pbProgreso.StepBy(Delta);
    lblDetalle.Caption := Mensaje;
    Application.ProcessMessages;    // hago que la app procese los mensajes...
end;

{******************************** Function Header ******************************
Function Name: AgregarRegistroArchivo
Author : ndonadio
Date Created : 29/09/2005
Description : registra el archivo recibido colgado al log de operaciones...
Parameters : Fecha: TDateTime; ArchivoInfracciones, ArchivoTransitos: AnsiString; Concesionaria: Byte; CodigoOperacionInterfase, Integer; Reproceso: Integer; var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.AgregarRegistroArchivo(Fecha: TDateTime;
  ArchivoInfracciones, ArchivoTransitos: AnsiString;
  CodigoOperacionInterfase: Integer; Reproceso: Integer; CantidadInfracciones, CantidadTransitos: Integer;
  var Error: AnsiString): Boolean;
resourcestring
    ERROR_SP = 'No se pudo guardar el registro del archivo en la base de datos.';
begin
    Result := False;
    try
        with spRegistrarArchivo do begin
            // Cargo los datos del archivo como parametro
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
            Parameters.ParamByName('@Concesionaria').Value := FConcesionaria;
            Parameters.ParamByName('@FechaArchivo').Value := Fecha;
            Parameters.ParamByName('@FechaRecepcion').Value := NowBase(DMConnections.BaseCAC);
            Parameters.ParamByName('@ArchivoInfracciones').Value := ExtractFileName(ArchivoInfracciones);
            Parameters.ParamByName('@ArchivoTransitos').Value := ExtractFileName(ArchivoTransitos);
            Parameters.ParamByName('@Reproceso').Value := Reproceso;
            Parameters.ParamByName('@CantidadInfracciones').Value := CantidadInfracciones;
            Parameters.ParamByName('@CantidadTransitos').Value := CantidadTransitos;
            // ejecuto el sp
            ExecProc;
            Result := ( Parameters.ParamByName('@RETURN_VALUE').Value = 0);
            if not Result then Error := ERROR_SP; // si devolvio error, devuelvo un mensaje de error...
        end;
    except
        on e:exception do begin
            // si explot� informo el por qu�...
            Error := e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ValidarSecuenciaArchivo
Author : ndonadio
Date Created : 29/09/2005
Description : Valida el archivo para saber si es el siguiente en secuencia o no
            OJO! No est� nada protegido porque si falla, no debe devolver FALSE,
            sino que debe Explotar... la explocion la agarra mas arriba...
Parameters : FechaArchivo: TDateTime; Concesionaria: Byte
Return Value : Boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.ValidarSecuenciaArchivo(
  FechaArchivo: TDateTime; var Mensaje: AnsiString): Boolean;
resourcestring
    MSG_INVALID_SEQUENCE = 'El archivo no es el siguiente en la secuencia de recepci�n. Desea continuar igualmente?';
    MSG_FILE_ALREADY_PROCESSED = 'El Archivo ya ha sido procesado. Desea reprocesarlo?';
begin
    // Como uso el resultado True o False para definir
    // si es el archivo correcto o no, dejo que explote
    // si algo falla, y lo agarro arriba en un try..except
    Result := False;
    // Obtengo el ultimo archivo cargado para esa concesionaria (el mas nuevo, sin contar reprocesos)
    spObtenerUltimoArchivo.Close;
    spObtenerUltimoArchivo.Parameters.ParamByName('@Concesionaria').Value := FConcesionaria;
    spObtenerUltimoArchivo.Open;
    // por ahora digo que el mensaje a devolver es el error de secuencia...
    Mensaje := MSG_INVALID_SEQUENCE;
    if spObtenerUltimoArchivo.IsEmpty then begin
        // si no encontr� le archivo, es que el que estoy procesando ahora es el primero
        Result := True;
        Exit;
    end;
    // verifico que sea el siguiente en la secuencia... teniendo en cuenta que
    // no se generan archivos ni lunes ni domingo...
    case DayOfTheWeek(spObtenerUltimoArchivo.FieldByName('FechaArchivo').asDateTime) of
        1..4: Result := (FechaArchivo = (spObtenerUltimoArchivo.FieldByName('FechaArchivo').asDateTime + 1));
        5   : Result := (FechaArchivo = (spObtenerUltimoArchivo.FieldByName('FechaArchivo').asDateTime + 3));
        6..7: Result := False; // no hay archivos de sabado y domingo
    end;

    // si no es el siguiente de la secuencia,
    // verifico si no esta intentando reprocesar...
    if (not Result) then begin
       //Obtengo el indice de reproceso que corresponderia a este archivo
        Reproceso := QueryGetValueInt( DMConnections.BaseCAC, Format(' SELECT ISNULL( (SELECT ISNULL(MAX(Reproceso) + 1, 0)'+
          ' FROM ArchivosInfraccionesRecibidosClearingBHTU (NOLOCK) WHERE Concesionaria = %d AND FechaArchivo = CONVERT(DATETIME, ''%s'', 112)), 0)',
          [FConcesionaria, FormatDateTime('yyyymmdd', FechaArchivo)]) );
        // si es un reproceso cambio el mensaje por Archivo Ya Procesado...
        if Reproceso > 0 then Mensaje := MSG_FILE_ALREADY_PROCESSED;
    end;
end;

{******************************** Function Header ******************************
Function Name: MostrarReporte
Author : ndonadio
Date Created : 29/09/2005
Description :   Muestra el reporte de finalizacion...
Parameters : CodigoOperacion: Integer
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.MostrarReporte(
  CodigoOperacion: Integer): boolean;
var
    f: TFrmReporteRecepcionInfraccionesBHTU;
begin
    Application.CreateForm(TFrmReporteRecepcionInfraccionesBHTU, f);
    Result := f.MostrarReporte(CodigoOperacion, Caption);
    f.Release;
end;


{******************************** Function Header ******************************
Function Name:  ProcesarInfraccion
Author : ndonadio
Date Created : 04/10/2005
Description : Procesa una infraccion de la lista...
Parameters : Not available
Return Value : Not available
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.ProcesarInfraccion (CodigoOperacion: Integer; Infraccion: TInfraccion ;var descError: AnsiString): boolean;
resourcestring
    ALL_TRANSACTIONS_WERE_DUPS  = 'Infraccion de Fecha %s, Patente %s  No Cargada porque todos sus tr�nsitos ya est�n cargados.';
    ERRROR_DUP_INFRACTION       = 'Infraccion Duplicada  %s, %s ';
    ERROR_TAN_HAS_CONTRACT_IN_CN= 'La Patente %s tiene convenio de Costanera Norte.( %s )';
    ERROR_NO_TRANSACTIONS       = 'Infracci�n de Fecha %s, Patente %s  No Cargada porque No tiene tr�nsitos asociados.';
    ERROR_CANT_ADD_ERROR        = 'Error al agregar Error de Interfaz.';

    SP_VERIFYING_DUP_INF    = ' (Verificando Infracciones Duplicadas)';
    SP_VERIFYING_TAN        = ' (Verificnado exietencia de convenio)';
    SP_SELECTING_TRX        = ' (Seleccionando los Tr�nsitos de la Infracci�n)';
    SP_SEARCH_FOR_DUPS      = ' (Buscando Tr�nsitos Duplicados)';
    SP_REGISTERING_DUP_TRX  = ' (Registrando los Tr�nsitos Duplicados)';
    SP_ADDING_INFRACTION    = ' (Agregando la Infracci�n)';
var
    UbicacionError: AnsiString;
begin
    // Salgo con Result = FALSE solo si se requiere abortar el proceso, sino
    // cambio el Result a True antes de Salir...
    Result := False;
    try
        UbicacionError := SP_VERIFYING_DUP_INF;
        // si la infraccion es duplicada, salgo
        if QueryGetValueInt( DMConnections.BaseCAC,Format('SELECT dbo.VerificarInfraccionDuplicadaClearinhBHTU( %d, CONVERT(SMALLDATETIME, ''%s'', 112) , ''%s'')',
            [Infraccion.Concesionaria , FormatDateTime('yyyymmdd',Infraccion.FechaInfraccion), Infraccion.Patente ])) = 1 then begin
            descError :=  ERROR_CANT_ADD_ERROR;
            Result := AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacion, FORMAT(ERRROR_DUP_INFRACTION, [FormatDatetIme('dd/mm/yyyy',INfraccion.FechaInfraccion), Infraccion.Patente ]));
            Exit;
        end;
        // Si la patente pertenece a un convenio, la agrego como Error de Interfase
        UbicacionError := SP_VERIFYING_TAN;
        if QueryGetValueInt( DMConnections.BaseCAC,Format('SELECT dbo.VerificarPatenteEnConvenio(''%s'', CONVERT(SMALLDATETIME, ''%s'', 112) )',[TRIM(Infraccion.Patente), FormatDateTime('yyyymmdd', Infraccion.FechaInfraccion)])) <> 0 then begin
            // si no se pueda agregar el error...salgo con error grave
            if not AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacion, FORMAT(ERROR_TAN_HAS_CONTRACT_IN_CN, [Infraccion.Patente, FormatDatetIme('dd/mm/yyyy',INfraccion.FechaInfraccion)])) then begin
                descError :=  ERROR_CANT_ADD_ERROR;
                Exit;
            end;
        end;

        // Selecciono los transitos de esta infraccion
        UbicacionError := SP_SELECTING_TRX;
        spSeleccionarTransitoInfraccionClearingBHTU.Parameters.ParamByName('@Concesionaria').Value := Infraccion.Concesionaria;
        spSeleccionarTransitoInfraccionClearingBHTU.Parameters.ParamByName('@Fecha').Value := Infraccion.FechaInfraccion ;
        spSeleccionarTransitoInfraccionClearingBHTU.Parameters.ParamByName('@Patente').Value := Infraccion.Patente;
        spSeleccionarTransitoInfraccionClearingBHTU.Parameters.ParamByName('@Cantidad').Value := 0;
        spSeleccionarTransitoInfraccionClearingBHTU.ExecProc;

        if  spSeleccionarTransitoInfraccionClearingBHTU.Parameters.ParamByName('@Cantidad').Value = 0 then begin
            // si no hay transitos agrego error y salgo...
             Result := AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacion, FORMAT(ERROR_NO_TRANSACTIONS, [FormatDatetIme('dd/mm/yyyy',INfraccion.FechaInfraccion), Infraccion.Patente ]));
             Exit;
        end;

        // Ubico los duplicados
        UbicacionError := SP_SEARCH_FOR_DUPS;
        spEncontrarTransitosDuplicadosClearingBHTU.Parameters.ParamByName('@Concesionaria').Value := Infraccion.Concesionaria;
        spEncontrarTransitosDuplicadosClearingBHTU.ExecProc;

        // Re verifico transitos no duplicados. Si no quedan, agrego error y salgo
        if (QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT(*) FROM ##TransitosInfraccionClearingBHTU') = 0) then begin
            // registro los transitos duplicados
                if AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacion, FORMAT(ALL_TRANSACTIONS_WERE_DUPS, [ FormatDatetIme('dd/mm/yyyy',INfraccion.FechaInfraccion), Infraccion.Patente ])) then begin
                    spRegistrarErrorTransitosDuplicados.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacion;
                    spRegistrarErrorTransitosDuplicados.Parameters.ParamByName('@DetalleInfraccion').Value := '';
                    spRegistrarErrorTransitosDuplicados.ExecProc;
                    Result := True;
                end
                else begin
                    descError := ERROR_CANT_ADD_ERROR;
                    Result := False;
                end;
                Exit;
        end;

        //DMConnections.BaseCAC.BeginTrans;                                               //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('BEGIN TRAN fInfraccionesClearingBHTU');            //SS_1385_NDR_20150922


        // Agrego la infraccion...
        UbicacionError := SP_ADDING_INFRACTION;
        with spAgregarInfraccionClearingBHTU do begin
            Parameters.ParamByName('@Concesionaria').Value  := Infraccion.Concesionaria ;
            Parameters.ParamByName('@FechaInfraccion').Value := Infraccion.FechaInfraccion ;
            Parameters.ParamByName('@Patente').Value        := Infraccion.Patente ;
            Parameters.ParamByName('@Categoria').Value      := Infraccion.Categoria;
            Parameters.ParamByName('@Portico').Value        := Infraccion.Portico ;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacion;
            ExecProc;

            // Si se produjo algun error, salgo con error grave
            if  Parameters.ParamByName('@RETURN_VALUE').Value = -1 then begin
                    //DMConnections.BaseCAC.RollbackTrans;                                                                              //SS_1385_NDR_20150922
                    DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fInfraccionesClearingBHTU END');	    //SS_1385_NDR_20150922
                    descError :=  Parameters.ParamByName('@descError').Value;
                    Exit;
            end;
        end;
        // Si habia transitos duplicados, los grabo como errores de interfases
        UbicacionError := SP_REGISTERING_DUP_TRX;
        spRegistrarErrorTransitosDuplicados.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacion;
        spRegistrarErrorTransitosDuplicados.Parameters.ParamByName('@DetalleInfraccion').Value := '';
        spRegistrarErrorTransitosDuplicados.ExecProc;

        //DMConnections.BaseCAC.CommitTrans;                                                                //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('COMMIT TRAN fInfraccionesClearingBHTU');					              //SS_1385_NDR_20150922
        Result := True;
    except
        // Error lo suficientemente grave como para abortar el proceso...
        on e:exception do begin
            //if DMConnections.BaseCAC.intransaction then DMConnections.BaseCAC.RollbackTrans;                              //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fInfraccionesClearingBHTU END');	    //SS_1385_NDR_20150922

            descError := e.Message + UbicacionError; ;
            Exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: AbrirArchivoTransitos
Author : ndonadio
Date Created : 04/10/2005
Description :
Parameters : var ListaTransitos: TStringList; var descError: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.AbrirArchivoTransitos(NombreArchivo: AnsiString; var Lista: TStringList;
     var descError: AnsiString): Boolean;
resourcestring
    ERROR_FILENAME_INVALID_DATE     = 'Tr�nsito con Error - ' + 'El nombre del archivo de tr�nsitos no contiene una fecha v�lida (%s)';
    ERROR_FILENAME_INVALID_HIGHWAY  = 'Tr�nsito con Error - ' + 'El nombre del archivo de tr�nsitos no contiene un valor que pueda interpretarse como C�digo de Concesionaria. (%s)';
    ERROR_PARSE_INVALID_TAN         = 'Tr�nsito con Error - ' + 'El valor %s no puede interpretarse como una Patente. (%s)';
    ERROR_PARSE_INVALID_DATE        = 'Tr�nsito con Error - ' + 'El valor %s no puede interpretarse como una Fecha. (%s)';
    ERROR_PARSE_INVALID_CATEGORY    = 'Tr�nsito con Error - ' + 'El valor %s no puede interpretarse como una Categor�a. (%s)';
    WARNING_DATE_NOT_IN_RANGE       = 'Tr�nsito con Error - ' + 'La fecha de tr�nsito %s no es correcta para el archivo con fecha %s (Registro: %s)';
var
    FechaArchivo    : TDateTime;
    strFecha        : AnsiString;
begin
        Result := False;
        strFecha := RightStr(TRIM(REPLACESTR(UPPERCASE(ExtractFileName(NombreArchivo)),'.TRX','')), 8);
        // Valido y recupero datos del nombre del archivo...
        if ValidarFechaAAAAMMDD(strFecha) then
            FechaArchivo := EncodeDate( StrToInt(Copy(strFecha, 1, 4)),
            			                StrToInt(Copy(strFecha, 5, 2)),
                                        StrToInt(Copy(strFecha, 7, 2)))
        else begin
            descError := Format(ERROR_FILENAME_INVALID_DATE, [ExtractFileName(NombreArchivo)]);
            Exit;
        end;
        try
            FConcesionaria := StrToInt( Copy(ExtractFileName(NombreArchivo),Pos('_', ExtractFileName(NombreArchivo))+1, 1));
        except
            descError := Format(ERROR_FILENAME_INVALID_HIGHWAY, [ExtractFileName(NombreArchivo)]);
            Exit;
        end;
        FFechaArchivo :=  FechaArchivo;
        // Cargo el archivo en la lista
        Lista.LoadFromFile(NombreArchivo);
        fCantTrx := Lista.Count;
        Result := True;
end;

{******************************** Function Header ******************************
Function Name: ParsearInfraccion
Author : ndonadio
Date Created : 04/10/2005
Description :   Parsea la linea para obetener los datos de infraccion
Parameters :
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.ParsearInfraccion(Linea: AnsiString; var Infraccion: TInfraccion; descError: AnsiString): boolean;
resourcestring
    ERROR_FILENAME_INVALID_DATE         = 'Infracci�n con Error - ' + 'El nombre del archivo de infracciones no contiene una fecha v�lida (%s)';
    ERROR_FILENAME_INVALID_HIGHWAY      = 'Infracci�n con Error - ' + 'El nombre del archivo de infracciones no contiene un valor que pueda interpretarse como C�digo de Concesionaria. (%s)';
    ERROR_CANT_VALIDATE_FILE_SEQUENCE   = 'Error al validar la secuencia de archivo.';
    ERROR_PARSE_INVALID_TAN             = 'Infracci�n con Error - ' + 'El valor %s no puede interpretarse como una Patente. (%s)';
    ERROR_PARSE_INVALID_DATE            = 'Infracci�n con Error - ' + 'El valor %s no puede interpretarse como una Fecha. (%s)';
    ERROR_PARSE_INVALID_CATEGORY        = 'Infracci�n con Error - ' + 'El valor %s no puede interpretarse como una Categor�a. (%s)';
    WARNING_DATE_NOT_IN_RANGE           = 'Infracci�n con Error - ' + 'La fecha de infracci�n %s no es correcta para el archivo con fecha %s (Registro: %s)';
    ERROR_NO_INFRACTION_ON_FILE         = 'El archivo est� vacio.';
var
    strFecha        : AnsiString;
begin
    Result := False;
    // Parseo
    Infraccion.Concesionaria := FConcesionaria;
    // La Patente
    Infraccion.Patente := TRIM(ParseParamByNumber(Linea, 1, SEP));
    if (Length(Infraccion.Patente) > MAX_LARGO_PATENTE)
        or (Length(Infraccion.Patente) < MIN_LARGO_PATENTE) then begin
        // No se agrega porque el largo de patente es invalido...
        FListaErroresInfracciones.Add(Format(ERROR_PARSE_INVALID_TAN, [Infraccion.Patente, Linea]));
        Exit;
    end;

    // La Fecha
    strFecha := TRIM(ParseParamByNumber(Linea, 2, SEP));
    strFecha := COPY(strFecha, 5, 4)+COPY(strFecha, 3, 2)+COPY(strFecha, 1, 2);
    if  not ValidarFechaAAAAMMDD(strFecha) then begin
        FListaErroresInfracciones.Add(Format(ERROR_PARSE_INVALID_DATE, [TRIM(ParseParamByNumber(Linea, 2, SEP)), Linea]));
        Exit;
    end;
    Infraccion.FechaInfraccion := EncodeDate( StrToInt(COPY(strFecha, 1, 4)), StrToInt(COPY(strFecha, 5, 2)), StrToInt(COPY(strFecha, 7, 2)) );
    // si la fecha de la infraccion esta fuera del intervalo de fechas del archivo,
    // reporto el error, pero la cargo igual
    if not ValidarFechaInfraccionArchivo(Infraccion.FechaInfraccion, FFechaArchivo) then
       FListaErroresInfracciones.Add(Format(WARNING_DATE_NOT_IN_RANGE, [TRIM(ParseParamByNumber(Linea, 2, SEP)), DateToStr(FFechaArchivo), Linea]));

    // La Categoria
    try
        Infraccion.Categoria := StrToInt(TRIM(ParseParamByNumber(Linea, 3, SEP)));
    except
        FListaErroresInfracciones.Add(Format(ERROR_PARSE_INVALID_CATEGORY, [TRIM(ParseParamByNumber(Linea, 3, SEP)), Linea]));
        Exit;
    end;
    Infraccion.Portico := TRIM(ParseParamByNumber(Linea, 4, SEP));
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: ValidarDatosArchivoInfracion
Author : ndonadio
Date Created : 07/10/2005
Description :
Parameters : NombreArchivo: AnsiString; var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmRecepcionArchivoInfraccionesClearingBHTU.ValidarDatosArchivoInfracion(NombreArchivo: AnsiString; var Error: AnsiString): Boolean;
resourcestring
    ERROR_FILENAME_INVALID_DATE         = 'Infracci�n con Error - ' + 'El nombre del archivo de infracciones no contiene una fecha v�lida (%s)';
    ERROR_FILENAME_INVALID_HIGHWAY      = 'Infracci�n con Error - ' + 'El nombre del archivo de infracciones no contiene un valor que pueda interpretarse como C�digo de Concesionaria. (%s)';
var
    strFecha        : AnsiString;
begin
    Result := False;
    strFecha := RightStr(TRIM(REPLACESTR(UPPERCASE(ExtractFileName(NombreArchivo)),'.DAT','')), 8);
    // Valido y recupero datos del nombre del archivo...
    if ValidarFechaAAAAMMDD(strFecha) then
        FFechaArchivo := EncodeDate( StrToInt(Copy(strFecha, 1, 4)),
                                    StrToInt(Copy(strFecha, 5, 2)),
                                    StrToInt(Copy(strFecha, 7, 2)))
    else begin
        Error := Format(ERROR_FILENAME_INVALID_DATE, [ExtractFileName(NombreArchivo)]);
        Exit;
    end;
    try
        FConcesionaria := StrToInt( Copy(ExtractFileName(NombreArchivo),Pos('_', ExtractFileName(NombreArchivo))+1, 1));
    except
        Error := Format(ERROR_FILENAME_INVALID_HIGHWAY, [ExtractFileName(NombreArchivo)]);
        Exit;
    end;
    Result := True;
end;

end.
