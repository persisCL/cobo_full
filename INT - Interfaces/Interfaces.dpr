program Interfaces;
uses
  Forms,
  OpenOnce,
  Controls,
  FrmMain in 'FrmMain.pas' {MainForm},
  DMConnection in '..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  MaskCombo in '..\Componentes\MaskCombo\MaskCombo.pas',
  PeaTypes in '..\Comunes\PeaTypes.pas',
  PeaProcs in '..\Comunes\PeaProcs.pas',
  Login in '..\Comunes\Login.pas' {LoginForm},
  CambioPassword in '..\Comunes\CambioPassword.pas' {FormCambioPassword},
  Mensajes in '..\Comunes\Mensajes.pas' {FormMensajes},
  RStrings in '..\Comunes\RStrings.pas',
  UtilReportes in '..\Comunes\UtilReportes.pas',
  ConstParametrosGenerales in '..\Comunes\ConstParametrosGenerales.pas',
  FrmRptInformeUsuario in '..\Comunes\FrmRptInformeUsuario.pas' {FormRptInformeUsuario},
  FrmRptInformeUsuarioConfig in '..\Comunes\FrmRptInformeUsuarioConfig.pas' {FormRptInformeUsuarioConfig},
  FrmStartup in 'FrmStartup.pas' {FormStartup},
  FrmGenerarArchivoMovimientos in 'FrmGenerarArchivoMovimientos.pas' {FGenerarArchivoMovimientos},
  Constantes in 'Constantes.pas',
  FrmGenerarArchivoNomina in 'FrmGenerarArchivoNomina.pas' {FGenerarArchivoNomina},
  FrmConvertirMovimientosATabla in 'FrmConvertirMovimientosATabla.pas' {FConvertirMovimientosATabla},
  FrmConvertirNominaATabla in 'FrmConvertirNominaATabla.pas' {FConvertirNominaATabla},
  FrmLogOperaciones in 'FrmLogOperaciones.pas' {FLogOperaciones},
  FrmGenerarDebitosSantander in 'FrmGenerarDebitosSantander.pas' {FGeneracionDebitosSantander},
  BuscaClientes in '..\Comunes\BuscaClientes.pas' {FormBuscaClientes},
  FrmRecepcionUniversoSantander in 'FrmRecepcionUniversoSantander.pas' {FRecepcionUniversoSantander},
  RendicionPAC in 'RendicionPAC.pas' {frmRendicionPAC},
  frmRecepcionRendicionesLider in 'frmRecepcionRendicionesLider.pas' {fRecepcionRendicionesLider},
  CatalogoImagenes in 'CatalogoImagenes.pas' {frmCatalogo},
  EnvioMandatosPresto in 'EnvioMandatosPresto.pas' {FEnvioMandatosPresto},
  frmRecepcionMandatosPresto in 'frmRecepcionMandatosPresto.pas' {fRecepcionMandatosPresto},
  CancelarImpresionMasiva in 'CancelarImpresionMasiva.pas' {FrmCancelarImpresionMasiva},
  FrmGenerarDebitosPresto in 'FrmGenerarDebitosPresto.pas' {FGeneracionDebitosPresto},
  frmRecepcionRendicionesPresto in 'frmRecepcionRendicionesPresto.pas' {fRecepcionRendicionesPresto},
  frmGenerarUniversoPresto in 'frmGenerarUniversoPresto.pas' {FUniversoPresto},
  HashingMD5 in 'HashingMD5.pas',
  CargaDevolucionesCorreo in 'CargaDevolucionesCorreo.pas' {frmCargaDevolucionesCorreo},
  DayPass in 'DayPass.pas' {frmDayPass},
  FrmRvm in 'FrmRvm.pas' {FRvm},
  ComunesInterfaces in 'ComunesInterfaces.pas',
  RVMTypes in '..\Comunes\RVMTypes.pas',
  RNVMConfig in '..\Comunes\RNVMConfig.pas',
  RVMBind in '..\Comunes\RVMBind.pas',
  RVMClient in '..\Comunes\RVMClient.pas',
  RVMLogin in '..\Comunes\RVMLogin.pas',
  FrmConsultaRNVMTransitosValidados in 'FrmConsultaRNVMTransitosValidados.pas' {FormConsultaRNVMTransitosValidados},
  ActualizarDatosInfractor in '..\Comunes\ActualizarDatosInfractor.pas' {DMActualizarDatosInfractor: TDataModule},
  frmListaErrores in 'frmListaErrores.pas' {formListaErrores},
  frmInterfazImprentaInfractores in 'frmInterfazImprentaInfractores.pas' {FormInterfazImprentaInfractores},
  frmCancelarOperacionesImprentaInfractores in 'frmCancelarOperacionesImprentaInfractores.pas' {FormCancelarNotificacionMasivaInfractores},
  frmGeneracionXMLInfracciones in 'frmGeneracionXMLInfracciones.pas' {fGeneracionXMLInfracciones},
  ImagenesTransitos in '..\Comunes\ImagenesTransitos.pas' {frmImagenesTransitos},
  ImgProcs in '..\Comunes\ImgProcs.pas',
  ImgTypes in '..\Comunes\ImgTypes.pas',
  utilImg in '..\Comunes\utilImg.pas',
  adminImg in '..\Comunes\adminImg.pas',
  Filtros in '..\Componentes\Imagenes\Filtros.pas',
  MOPInfBind in 'MOPInfBind.pas',
  fRecepcionXMLRespuestaInfraccion in 'fRecepcionXMLRespuestaInfraccion.pas' {frmRecepcionXMLRespuestaInfraccion},
  FrmGenerarArchivoNovedades in 'FrmGenerarArchivoNovedades.pas' {FGenerarArchivoNovedades},
  frmRecepcionCaptacionesCMR in 'frmRecepcionCaptacionesCMR.pas' {fRecepcionCaptacionesCMR},
  MOPRespuestasDenunciosBind in 'MOPRespuestasDenunciosBind.pas',
  FrmRecepcionNovedades in 'FrmRecepcionNovedades.pas' {FRecepcionNovedades},
  frmEnvioRespuestasCaptaciones in 'frmEnvioRespuestasCaptaciones.pas' {FEnvioRespuestasCaptaciones},
  frmGeneracionDebitosCMR in 'frmGeneracionDebitosCMR.pas' {FGeneracionDebitosCMR},
  frmRecepcionRendicionesCMR in 'frmRecepcionRendicionesCMR.pas' {fRecepcionRendicionesCMR},
  FrmRptEnvioNovedades in 'FrmRptEnvioNovedades.pas' {FRptEnvioNovedades},
  FrmRptRecepcionNovedades in 'FrmRptRecepcionNovedades.pas' {FRptRecepcionNovedades},
  SeleccionProcFacturacion in 'SeleccionProcFacturacion.pas' {dlgSeleccionProcFacturacion},
  FrmRptErroresSintaxis in 'FrmRptErroresSintaxis.pas' {FRptErroresSintaxis},
  FrmRptEnvioInfraccionesXML in 'FrmRptEnvioInfraccionesXML.pas' {frmRptEnvioXMLInfracciones},
  MensajesEmail in '..\Comunes\MensajesEmail.pas',
  dmMensaje in '..\Comunes\dmMensaje.pas' {dmMensajes: TDataModule},
  ABMFirmasEmail in 'ABMFirmasEmail.pas' {FormFirmasEmail},
  ABMPlantillasEmail in 'ABMPlantillasEmail.pas' {FormPlantillasEmail},
  ExplorarEMails in '..\Comunes\ExplorarEMails.pas' {frmExplorarMails},
  ParamGen in '..\Comunes\ParamGen.pas' {FormParam},
  frmEditarVal in '..\Comunes\frmEditarVal.pas' {frmValueEdit},
  ComponerEMail in '..\Comunes\ComponerEMail.pas' {frmComponerMail},
  FrmContabilizaciondeJornada in 'FrmContabilizaciondeJornada.pas' {FContabilizaciondeJornada},
  frmCierredeJornada in 'frmCierredeJornada.pas' {FCierredeJornada},
  FrmAutorizacionSupervisorContable in 'FrmAutorizacionSupervisorContable.pas' {FAutorizacionSupervisorContable},
  FrmRptVerificacionInterfazContable in 'FrmRptVerificacionInterfazContable.pas' {FRptVerificacionInterfazContable},
  frmConfiguracionEnviosAutomaticos in 'frmConfiguracionEnviosAutomaticos.pas' {formConfiguracionEnviosAutomaticos},
  fActivacionesSerbanc in 'fActivacionesSerbanc.pas' {frmActivacionesSerbanc},
  fDesactivacionesSerbanc in 'fDesactivacionesSerbanc.pas' {frmDesactivacionesSerbanc},
  fAccionesSerbanc in 'fAccionesSerbanc.pas' {frmAccionesSerbanc},
  fPagosSerbanc in 'fPagosSerbanc.pas' {frmRecepcionArchivoPagosSerbanc},
  ABMVentasPasesDiarios in 'ABMVentasPasesDiarios.pas' {frmABMVentasPasesDiarios},
  fCambiosTipoCobranzaSerbanc in 'fCambiosTipoCobranzaSerbanc.pas' {frmRecepcionCambioTipoCobranzaSerbanc},
  FReporteLibroVentasPeajes in 'FReporteLibroVentasPeajes.pas' {frmReporteLibroVentasPeajes},
  fRecepcionDireccionesSerbanc in 'fRecepcionDireccionesSerbanc.pas' {frmRecpcionNuevasDireccionesSerbanc},
  LoginSetup in '..\Comunes\LoginSetup.pas' {frmLoginSetup},
  LoginValuesEditor in '..\Comunes\LoginValuesEditor.pas' {frmLoginValuesEditor},
  FrmProcesosContabilizados in 'FrmProcesosContabilizados.pas' {FProcesosContabilizados},
  fEnvioInfraccionesClearingBHTU in 'fEnvioInfraccionesClearingBHTU.pas' {frmEnvioInfraccionesClearingBHTU},
  DMComunicacion in '..\Comunes\DMComunicacion.pas' {DMComunicaciones: TDataModule},
  RNVMProgress in 'RNVMProgress.pas' {frmRNVMProgress},
  fReporteActivacionesSerbanc in 'fReporteActivacionesSerbanc.pas' {frmReporteActivacionesSerbanc},
  fReporteDesactivacionesSerbanc in 'fReporteDesactivacionesSerbanc.pas' {frmReporteDesactivacionesSerbanc},
  fReporteAccionesSerbanc in 'fReporteAccionesSerbanc.pas' {frmReporteAccionesSerbanc},
  fReportepagosSerbanc in 'fReportepagosSerbanc.pas' {frmReportePagosSerbanc},
  fReporteCTCSerbanc in 'fReporteCTCSerbanc.pas' {frmReporteCTCSerbanc},
  fReporteNvsDirSerbanc in 'fReporteNvsDirSerbanc.pas' {frmReporteNvsDirSerbanc},
  fReporteEnvioInfraccionesBHTU in 'fReporteEnvioInfraccionesBHTU.pas' {frmReporteEnvioInfraccionesBHTU},
  fReprocesarEnvioInfraccionesClearingBHTU in 'fReprocesarEnvioInfraccionesClearingBHTU.pas' {frmReprocesarEnvioInfraccionesClearingBHTU},
  fRececpcionArchivoInfraccionesClearingBHTU in 'fRececpcionArchivoInfraccionesClearingBHTU.pas' {frmRecepcionArchivoInfraccionesClearingBHTU},
  fReporteRecepcionInfraccionesBHTU in 'fReporteRecepcionInfraccionesBHTU.pas' {frmReporteRecepcionInfraccionesBHTU},
  FrmRptDayPass in 'FrmRptDayPass.pas' {FRptDayPass},
  RNVMSeguridad in 'RNVMSeguridad.pas' {frmSeguridadRNVM},
  FrmGenerarArchivoCuadratura in 'FrmGenerarArchivoCuadratura.pas' {FGenerarArchivoCuadratura},
  FrmRptEnvioCuadratura in 'FrmRptEnvioCuadratura.pas' {FRptEnvioCuadratura},
  fRecepcionArchVentasBHTU in 'fRecepcionArchVentasBHTU.pas' {frmRecepcionArchVentasBHTU},
  fReporteRecepcionVentasBHTU in 'fReporteRecepcionVentasBHTU.pas' {frmReporteRecepcionVentasBHTU},
  fProcesarAsignacionClearingBHTU in 'fProcesarAsignacionClearingBHTU.pas' {frmProcesarAsignacionClearingBHTU},
  fReporteProcesoAsignacionBHTU in 'fReporteProcesoAsignacionBHTU.pas' {frmReportePRocesoAsignacionBHTU},
  fGenerarArchivoAsignacionesBHTU in 'fGenerarArchivoAsignacionesBHTU.pas' {frmGenerarArchivoAsignacionesBHTU},
  fReporteGeneracionArchivoAsignacionBHTU in 'fReporteGeneracionArchivoAsignacionBHTU.pas' {frmReporteGeneracionArchivoAsignacionBHTU},
  fConsultaBHTU in 'fConsultaBHTU.pas' {frmConsultaBHTU},
  fEdicionBHTU in 'fEdicionBHTU.pas' {frmEdicionBHTU},
  FrmRecepcionRendicionesServipag in 'FrmRecepcionRendicionesServipag.pas' {FRecepcionRendicionesServipag},
  CustomAssistance in '..\Comunes\CustomAssistance.pas' {frmLocalAssistance},
  FrmRptLibroVentas in 'FrmRptLibroVentas.pas' {FRptLibroVentas},
  ABMPorcentajeVentasPDU in 'ABMPorcentajeVentasPDU.pas' {FAbmPorcentajeVentasPDU},
  ABMPorcentajeVentasBHT in 'ABMPorcentajeVentasBHT.pas' {FAbmPorcentajeVentasBHT},
  FrmRptRecepcionRendiciones in 'FrmRptRecepcionRendiciones.pas' {FRptRecepcionRendiciones},
  FrmRptEnvioDebitos in '..\Comunes\FrmRptEnvioDebitos.pas' {FRptEnvioDebitos},
  FrmRecepcionRendicionesMisCuentas in 'FrmRecepcionRendicionesMisCuentas.pas' {FRecepcionRendicionesMisCuentas},
  FrmRptRecepcionComprobantesMisCuentas in 'FrmRptRecepcionComprobantesMisCuentas.pas',
  FrmRptRecepcionComprobantesServipag in 'FrmRptRecepcionComprobantesServipag.pas',
  FrmWebAuditoria in '..\Comunes\FrmWebAuditoria.pas' {FWebAuditoria},
  frmExploradorConsultaRNVM in 'frmExploradorConsultaRNVM.pas' {ExploradorConsultaRNVMForm},
  FrmRptRecepcionCaptacionesCMR in 'FrmRptRecepcionCaptacionesCMR.pas' {FRptRecepcionCaptacionesCMR},
  frmRptRecepcionUniversoSantander in 'frmRptRecepcionUniversoSantander.pas' {RptRecepcionUniversoSantanderForm},
  frmCopecPDU in 'frmCopecPDU.pas' {CopecPDUForm},
  frmRptDayPassCopec in 'frmRptDayPassCopec.pas' {RptDayPassForm},
  frmGeneracionDeNominas in 'frmGeneracionDeNominas.pas' {GeneracionDeNominasForm},
  ComunInterfaces in '..\Comunes\ComunInterfaces.pas',
  frmRptNominas in 'frmRptNominas.pas' {RptNominasForm},
  FrmRecepcionRendicionesSantander in 'FrmRecepcionRendicionesSantander.pas' {FRecepcionRendicionesSantander},
  DTEControlDLL in '..\FAC - Facturacion\DBNet\DTEControlDLL.pas',
  ImprentaJORDAN in 'ImprentaJORDAN.pas' {frmImprentaJORDAN},
  frmCambioPersonaRNVM in 'frmCambioPersonaRNVM.pas' {FCambioPersonaRNVM},
  frmVerLibroDePeajes in 'frmVerLibroDePeajes.pas' {VerLibroDePeajesForm},
  EncriptaRijandel in '..\Comunes\EncriptaRijandel.pas',
  AES in '..\Comunes\AES.pas',
  base64 in '..\Comunes\base64.pas',
  FrmGenerarDebitosBcoChile in 'FrmGenerarDebitosBcoChile.pas' {FGeneracionDebitosBcoChile},
  RendicionPACBancoChile in 'RendicionPACBancoChile.pas' {frmRendicionPACBancoChile},
  FrmRecepcionUniversoBcoChile in 'FrmRecepcionUniversoBcoChile.pas' {FRecepcionUniversoBcoChile},
  frmRptRecepcionUniversoBancoChile in 'frmRptRecepcionUniversoBancoChile.pas' {RptRecepcionUniversoBancoChileForm},
  PeaProcsCN in '..\Comunes\PeaProcsCN.pas',
  MsgBoxCN in '..\Comunes\MsgBoxCN.pas' {frmMsgBoxCN},
  SysUtilsCN in '..\Comunes\SysUtilsCN.pas',
  frmMuestraMensaje in '..\Comunes\frmMuestraMensaje.pas' {PanelMensajesForm},
  frmBloqueosSistema in '..\Comunes\frmBloqueosSistema.pas' {BloqueosSistemaForm},
  frmReEmisionAnulacionDiscoXMLInfracciones in 'frmReEmisionAnulacionDiscoXMLInfracciones.pas' {ReEmisionAnulacionDiscoXMLInfraccionesForm},
  frmVentanaAviso in '..\Comunes\Avisos\frmVentanaAviso.pas' {VentanaAvisoForm},
  Aviso in '..\Comunes\Avisos\Aviso.pas',
  TipoFormaAtencion in '..\Comunes\Avisos\TipoFormaAtencion.pas',
  Notificacion in '..\Comunes\Avisos\Notificacion.pas',
  Diccionario in '..\Comunes\Diccionario.pas',
  ClaveValor in '..\Comunes\ClaveValor.pas',
  AvisoTagVencidos in '..\Comunes\Avisos\AvisoTagVencidos.pas',
  NotificacionImpresionReporte in '..\Comunes\Avisos\NotificacionImpresionReporte.pas' {FormNotificacionImpresionReporte},
  NotificacionImpresionDoc in '..\Comunes\Avisos\NotificacionImpresionDoc.pas',
  frmMuestraPDF in '..\Comunes\frmMuestraPDF.pas' {MuestraPDFForm},
  FactoryINotificacion in '..\Comunes\Avisos\FactoryINotificacion.pas',
  SysUtils,
  FrmGenerarArchivoIVAConcesionarias in 'FrmGenerarArchivoIVAConcesionarias.pas' {FGenerarArchivoIVAConcesionarias},
  frmAcercaDe in '..\Comunes\frmAcercaDe.pas' {AcercaDeForm},
  frmRecepcionCaptacionesCMR_VS in 'frmRecepcionCaptacionesCMR_VS.pas' {fRecepcionCaptacionesCMR_VS},
  FrmRptRecepcionComprobantesSantander in 'FrmRptRecepcionComprobantesSantander.pas',
  frmRecepcionAnulacionesServipag in 'frmRecepcionAnulacionesServipag.pas' {RecepcionAnulacionesServipagForm},
  frmRespuestasAConsultasRNVM in 'frmRespuestasAConsultasRNVM.pas' {fRespuestasAConsultasRNVM},
  FrmRptRecepcionRespuestasConsultasRNVM in 'FrmRptRecepcionRespuestasConsultasRNVM.pas' {FRptRecepcionRespuestasConsultasRNVM},
  ImagePlus in '..\Componentes\Imagenes\ImagePlus.pas',
  frmAsignarConsultasRNVMInfracciones in 'frmAsignarConsultasRNVMInfracciones.pas' {frmAsignarConsultasRNVMInfraccionesForm},
  frmMensajeACliente in '..\Comunes\frmMensajeACliente.pas' {FormMensajeACliente},
  frmABMTiposCorrespondencia in 'frmABMTiposCorrespondencia.pas' {FormABMTiposCorrespondencia},
  Crypto in '..\Comunes\Crypto.pas',
  frmEjecutarInterfaz in 'frmEjecutarInterfaz.pas' {FormEjecutarInterfaz},
  frmRptRecepcionMovimientosTBK in 'frmRptRecepcionMovimientosTBK.pas' {RptRecepcionMovimientosTBKForm},
  frmRptDetalleInterfaces in 'frmRptDetalleInterfaces.pas' {RptDetalleInterfacesForm},
  ApplicationGlobalVariables in '..\ComunesNew\ApplicationGlobalVariables.pas',
  ApplicationUpdateRequest in '..\ComunesNew\ApplicationUpdateRequest.pas',
  WritingToLog in '..\ComunesNew\WritingToLog.pas',
  FormDynacTollRates in 'FormDynacTollRates.pas' {frmDynacTollRates},
  PuntosCobro in '..\Comunes\Clases\PuntosCobro.pas',
  ClaseBase in '..\Comunes\Clases\ClaseBase.pas',
  Dynac in '..\Comunes\Clases\Dynac.pas',
  CategoriasClases in '..\Comunes\Clases\CategoriasClases.pas',
  GrillaListado in '..\Comunes\GrillaListado.pas' {frmGrilla},
  BosTollRates in '..\DYNAC\ServicioEnvioTransitos\BosTollRates.pas';

{$R *.res}

begin

    Application.Initialize;
    Application.Title := 'Gesti�n de Interfaces';

    if not ActualizadorUpdateNeeded then begin
        if not MidasUpdateNeeded then begin
            if not ApplicationINIUPdateNeeded then begin
                if not ApplicationUpdateNeeded then begin

                    GetOtherFilesCheckUpdate;


                    if not AlreadyOpen then begin

                        Screen.Cursor := crAppStart;
                        Application.ProcessMessages;
                        FormStartup := TFormStartup.Create(nil);
                        FormStartup.Show;
                        FormStartup.Update;
                        Application.ProcessMessages;

                        Application.CreateForm(TDMConnections, DMConnections);
                        Application.CreateForm(TMainForm, MainForm);
                        Application.CreateForm(TfrmDynacTollRates, frmDynacTollRates);
                        Application.CreateForm(TfrmGrilla, frmGrilla);
                        MainForm.Show;

                        if not MainForm.Inicializar then begin

                            Application.Terminate;
                        end;

                        Screen.Cursor := crDefault;
                        Application.ProcessMessages;

                        ShortDateFormat := 'dd/mm/yyyy';
                        LongDateFormat  := 'dd/mm/yyyy';
                        Application.Run;
                    end
                    else Application.Terminate;
                end
                else begin
                    ApplicationUpdateExecute;
                    Application.Terminate;
                end;
            end
            else begin
                ApplicationINIUpdateExecute;
                Application.Terminate;
            end;
        end
        else begin
            MidasUpdateExecute;
            Application.Terminate;
        end;
    end
    else begin
        ActualizadorUpdateExecute;
        Application.Terminate;
    end;
end.
