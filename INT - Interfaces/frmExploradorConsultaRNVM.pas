{-------------------------------------------------------------------------------
File Name: frmExploradorConsultaRNVM.pas
Author: nefernandez
Date Created: 22/06/2007
Language: ES-AR
Description:  Explorar las consultas realizadas al RNVM

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

--------------------------------------------------------------------------------}
unit frmExploradorConsultaRNVM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CollPnl, ListBoxEx, DBListEx, ExtCtrls, StdCtrls, TimeEdit, Validate,
  DateEdit, DB, ADODB, Util, UtilProc, DateUtils, DBCtrls;

type
  TExploradorConsultaRNVMForm = class(TForm)
    cbBusquedas: TCollapsablePanel;
    deFechaDesde: TDateEdit;
    teHoraDesde: TTimeEdit;
    deFechaHasta: TDateEdit;
    teHoraHasta: TTimeEdit;
    Label1: TLabel;
    Label2: TLabel;
    btnLimpiar: TButton;
    btnBuscar: TButton;
    Panel1: TPanel;
    btnCerrar: TButton;
    lstConsultaRNVM: TDBListEx;
    spObtenerConsultaRNVM: TADOStoredProc;
    dsObtenerConsultaRNVM: TDataSource;
    Panel2: TPanel;
    dbmRespuesta: TDBMemo;
    procedure btnLimpiarClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCerrarClick(Sender: TObject);
  private
    { Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure ObtenerConsultaRNVM (FechaDesde: TDateTime; FechaHasta: TDateTime);
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

var
  ExploradorConsultaRNVMForm: TExploradorConsultaRNVMForm;

resourcestring
    MSG_TITLE = 'Explorador de Consultas RNVM';
    MSG_ATTENTION = 'Atenci�n';

implementation

uses DMConnection;

{$R *.dfm}

function TExploradorConsultaRNVMForm.Inicializar: Boolean;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    deFechaDesde.Date := IncDay(Date, -15);
    deFechaHasta.Date := Date;
    teHoraDesde.Time   := StrToTime('00' + TIMESEPARATOR + '00');
    teHoraHasta.Time   := StrToTime('23' + TIMESEPARATOR + '59');
    Result := True;
end;

procedure TExploradorConsultaRNVMForm.ObtenerConsultaRNVM (FechaDesde: TDateTime; FechaHasta: TDateTime);
resourcestring
    MSG_ERROR = 'Ha ocurrido un error al abrir el Procedimiento Almacenado: %s';
begin
    try
        spObtenerConsultaRNVM.Close;
        spObtenerConsultaRNVM.Parameters.ParamByName('@FechaDesde').Value := FechaDesde;
        spObtenerConsultaRNVM.Parameters.ParamByName('@FechaHasta').Value := FechaHasta;
        spObtenerConsultaRNVM.Open;
    except
        on E: Exception do begin
            MsgBoxErr(Format(MSG_ERROR, [spObtenerConsultaRNVM.Name]), E.Message, MSG_TITLE, MB_ICONERROR);
        end;
    end;
end;

procedure TExploradorConsultaRNVMForm.btnBuscarClick(Sender: TObject);
resourcestring
    MSG_RECORDSET_IS_EMPTY = 'No hay registros disponibles para la b�squeda seleccionada';
var
    FechaHoraDesde, FechaHoraHasta: TDateTime;
begin
    Screen.Cursor := crHourGlass;
    FechaHoraDesde := deFechaDesde.Date + teHoraDesde.Time;
    FechaHoraHasta := deFechaHasta.Date + teHoraHasta.Time;
    ObtenerConsultaRNVM(FechaHoraDesde, FechaHoraHasta);
    if spObtenerConsultaRNVM.IsEmpty then MsgBox(MSG_RECORDSET_IS_EMPTY);
    Screen.Cursor := crDefault;
end;

procedure TExploradorConsultaRNVMForm.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

procedure TExploradorConsultaRNVMForm.btnLimpiarClick(Sender: TObject);
begin
    deFechaDesde.Date := NullDate;
    deFechaHasta.Date := NullDate;
    teHoraDesde.Time := StrToTime('00' + TimeSeparator + '00');
    teHoraHasta.Time := StrToTime('00' + TimeSeparator + '00');
end;

procedure TExploradorConsultaRNVMForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    spObtenerConsultaRNVM.Close;
    Action := caFree;
end;

end.
