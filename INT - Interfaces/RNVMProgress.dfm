object frmRNVMProgress: TfrmRNVMProgress
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Consultando al RNVM'
  ClientHeight = 167
  ClientWidth = 373
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 353
    Height = 89
  end
  object lblInfo: TLabel
    Left = 16
    Top = 24
    Width = 321
    Height = 13
    Caption = 
      'Realizando la(s) %d Consulta(s) solicitada(s), aguarde por favor' +
      '...'
  end
  object btnCancelar: TButton
    Left = 152
    Top = 128
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    TabOrder = 0
    OnClick = btnCancelarClick
  end
  object pbConsultas: TProgressBar
    Left = 16
    Top = 48
    Width = 337
    Height = 17
    TabOrder = 1
  end
end
