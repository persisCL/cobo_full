{-------------------------------------------------------------------------------
 File Name: FrmRptEnvioCuadratura.pas
 Author:    Lgisuk
 Date Created: 04/10/2005
 Language: ES-AR
 Description: M�dulo de la interface Falabella - Reporte de Envio de Cuadratura



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
Unit FrmRptEnvioCuadratura;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  Utildb,                    //querygetvalue
  UtilProc,                  //Mensajes
  //general
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup, ConstParametrosGenerales;                                            //SS_1147_NDR_20140710

type
  TFRptEnvioCuadratura = class(TForm)
    rptEnvioCuadratura: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppMaxFechaEmision: TppLabel;
    ppLabel1: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    ppParameterList1: TppParameterList;
	  RBIListado: TRBInterface;
    SpObtenerReporteEnvioCuadraturaCMR: TADOStoredProc;
    ppLine5: TppLine;
    Ltusuario: TppLabel;
    LtFechaHora: TppLabel;
    ppusuario: TppLabel;
    ppfechahora: TppLabel;
    ppNumeroProceso: TppLabel;
    ppIdentificacionArchivoGenerado: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppCantidadTotalRegistrosEnviados: TppLabel;
    ppCantidadAltasEnviadas: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppCantidadDeRegistros: TppLabel;
    ppSumatoriadeCuentas: TppLabel;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FCodigoOperacionInterfase : Integer;
    FNombreReporte : String;
    FCantidadRegistros : String;
    FSumatoriaCuentas : String;
    { Private declarations }
  public
    Function Inicializar(CodigoOperacionInterfase : Integer; NombreReporte, CantidadRegistros, SumatoriaCuentas: AnsiString) : Boolean;
    { Public declarations }
  end;

var
  FRptEnvioCuadratura: TFRptEnvioCuadratura;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRptEnvioCuadratura.Inicializar(CodigoOperacionInterfase : Integer; NombreReporte, CantidadRegistros, SumatoriaCuentas: AnsiString) : Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    FCodigoOperacionInterfase := CodigoOperacionInterfase;
    FNombreReporte := NombreReporte;
    FCantidadRegistros := CantidadRegistros;
    FSumatoriaCuentas := SumatoriaCuentas;
    Result := RBIListado.Execute;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
Procedure TFRptEnvioCuadratura.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
Resourcestring
    MSG_ERROR_AL_GENERAR_REPORTE = 'Error al generar el reporte';
    MSG_ERROR = 'Error';
Const
    REPORT = 'Reporte';
begin
    try
        with SpObtenerReporteEnvioCuadraturaCMR do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
            Parameters.ParamByName('@FechaProcesamiento').Value := Now;
            Parameters.ParamByName('@Usuario').Value := '';
            Parameters.ParamByName('@NombreArchivo').Value := '';
            Parameters.ParamByName('@CantidadTotalRegistrosEnviados').Value := 0;
            Parameters.ParamByName('@CantidadAltasEnviadas').Value := 0;
            ExecProc;
        end;
    except
        on e: Exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
        end;
    end;
    try
        //Asigno los valores a los campos del reporte
        with SpObtenerReporteEnvioCuadraturaCMR do begin
            //Encabezado
            ppFechaHora.Caption                      := DateTimeToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
            ppUsuario.Caption                        := Parameters.ParamByName('@Usuario').Value;
            ppNumeroProceso.Caption                  := InttoStr(FCodigoOperacionInterfase);
            ppIdentificacionArchivoGenerado.Caption  := ExtractFileName(Parameters.ParamByName('@NombreArchivo').Value);
            //Datos del Archivo
            ppCantidadTotalRegistrosEnviados.Caption := Parameters.ParamByName('@CantidadTotalRegistrosEnviados').Value;
            ppCantidadAltasEnviadas.Caption          := Parameters.ParamByName('@CantidadAltasEnviadas').Value;
            //Datos del Registro de Control
            ppCantidaddeRegistros.Caption            := FCantidadRegistros;
            ppSumatoriadeCuentas.Caption             := FSumatoriaCuentas;
        end;
        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FnombreReporte;
    except
        on E: exception do begin
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.
