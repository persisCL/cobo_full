{----------------------------------------------------------------------------
 File Name: Imprenta.pas
 Author:    gcasais
 Date Created: 04/12/2004
 Language: ES-AR
 Description:  M�dulo de interface de impresi�n masiva para imprenta

 Revision 1:
 Author:       sguastoni
 Date Created: 05/01/2006
 Description:  Agregado campo @Usuario a spActualizarFechaImpresionComprobantesEnviadosAImprenta

 Revision 2:
    Date: 03/03/2009
    Author: mpiazza
    Description:    Ref (SRX01226) se cambia" now " por nowbase

 Revision 3:
    Date: 04/03/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura


 Revision 4:
    Date : 11-03-2009
    Author : Nelson Droguett Sierra
    Description : Separar la generacion de los archivos de interface agrupando
                  TipoComprobanteFiscal
 Revision 5:
    Date : 19-03-2009
    Author : Nelson Droguett Sierra
    Description : Se integra la DLL para el timbre electronico.
                  Se agrega el XML de timbre electronico a los archivos planos


  Revision 6:
    Date : 02-04-2009
    Author : Nelson Droguett Sierra
    Description : En el caso de existir un archivo con el mismo nombre en el
    directorio seleccionado, debe preguntar si la reescribe o no.

  Revision 7:
    Date : 11-06-2009
    Author : Nelson Droguett Sierra
    Description : Se cambia la obtencion del timbre electronico llamando primero
                  a una funcion que obtiene los datos del comprobante electronico
                  para el timbre.

Revision 8:
Author: mbecerra
Date: 27-Junio-2009
Description:    1.-		Se agrega un TFileStream de modo de no cargar todo en memoria
                    	en TStringList y que colapse por memoria insuficiente. Para
                        eso se crea una constante que define la cantidad de bytes
                        que una vez que el tama�o de la TStringList alcanza ese valor,
                        se graba en disco usando el FileStream.

Revision    :9
Author      : Nelson Droguett Sierra
Fecha       : 09-07-2009
Descripcion : Importa desde el servidor la estructura de directorios de la DLL
                para el timbrado electronico a una carpeta local especificada
                como parametrogeneral DIR_JORDAN_TIMBRE_ELECTRONICO. Esto porque
                el timbrado electronico falla en trabajos masivos si pierde la
                conexion.
              El vaciado desde los stringList a disco, se hace cada 1 MB de informacion
              se escriben los fileStream, y ademas se descarga y carga nuevamente la DLL

-----------------------------------------------------------------------------}
unit Imprenta;

interface

uses
    //Imprenta
    // Rev.5 / 22-03-2009 / Nelson Droguett Sierra
    DTEControlDLL ,               // DLL Para Facturacion Electronica
    DMConnection,                 //Coneccion a base de datos OP_CAC
    Util,                         //stringtofile,padl..
    UtilProc,                     //Mensajes
    UtilDB,                       //Rutinas para base de datos
    //ComunesInterfaces,            //Procedimientos y Funciones Comunes a todos los formularios
    ComunInterfaces,            //Procedimientos y Funciones Comunes a todos los formularios
    PeaTypes,                     //Constantes
    PeaProcs,                     //NowBase
    ConstParametrosGenerales,     //Constantes
    SeleccionProcFacturacion,     //Selecciona el proceso de Facturaci�n
    //Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs,  StdCtrls, ExtCtrls, DB, ADODB, ListBoxEx, DBListEx, Buttons,
    DmiCtrls, DPSControls, ComCtrls, DateUtils, DBTables, BuscaTab, Grids,
    DBGrids, Mask, DBCtrls, EventLog, ActnList, StrUtils, inifiles, Math, PsAPI,
    ShellApi;

type
    TfrmImprenta = class(TForm)
        nb: TNotebook;
        Label1: TLabel;
        Bevel1: TBevel;
        Bevel2: TBevel;
        DBListEx1: TDBListEx;
        Label2: TLabel;
        DS: TDataSource;
        lblCantidad: TLabel;
        Label4: TLabel;
        Label6: TLabel;
        Label7: TLabel;
        btnSiguiente: TButton;
        btnObtenerComprobantes: TButton;
        btnAnterior: TButton;
        btnFinalizar: TButton;
        btnSalir: TButton;
        pnlprogreso: TPanel;
        Label8: TLabel;
        pbProgreso: TProgressBar;
        bteImagen: TBuscaTabEdit;
        Label9: TLabel;
        btCatalogo: TBuscaTabla;
        tblCatalogoImagenesImprenta: TADOTable;
        lblDescri: TLabel;
        spObtenerComprobantesImprentaPreview: TADOStoredProc;
        spActualizarComprobantesEnviadosAImprenta: TADOStoredProc;
        Update: TADOQuery;
        spPrepararProcesoInterfazImprenta: TADOStoredProc;
        spObtenerLineasInterfazImprenta: TADOStoredProc;
        qryEliminarTemporales: TADOQuery;
        labelProgreso: TLabel;
        neComprobanteInicial: TNumericEdit;
        neComprobanteFinal: TNumericEdit;
        Label3: TLabel;
        Label5: TLabel;
        qryLineas: TADOQuery;
        btnBuscarProceso: TButton;
        spObtenerOperacionesImpresionMasiva: TADOStoredProc;
        spObtenerOperacionesImpresionMasivaCodigoOperacionInterfase: TIntegerField;
        spObtenerOperacionesImpresionMasivaFecha: TDateTimeField;
        spObtenerOperacionesImpresionMasivaUsuario: TStringField;
        spObtenerOperacionesImpresionMasivaCantidad: TIntegerField;
        buscaOperaciones: TBuscaTabla;
        btnProcesosFacturacion: TButton;
        lblForzandoReimpresion: TLabel;
        lblRangoSeleccionado: TLabel;
        lblImprimiendoElectronicos: TLabel;
        Label77: TLabel;
        txtUbicacion: TPickEdit;
        spActualizarFechaImpresionComprobantesEnviadosAImprenta: TADOStoredProc;
    ActionList1: TActionList;
    AConfig_EGATE_HOME_Manual: TAction;
    AConfig_EGATE_HOME_Parametrico: TAction;
    ACargarDLL: TAction;
    ATimbrarDinamico: TAction;
    ATimbraryObtenerImagen: TAction;
    ALiberarDLL: TAction;
    imgTimbre: TImage;
    chbLogSucesos: TCheckBox;
    lblProcesoInterrumpido: TLabel;
    spObtenerDatosParaTimbreElectronico: TADOStoredProc;
    qryLineasTipoComprobante: TStringField;
    qryLineasNumeroComprobante: TLargeintField;
    qryLineasLinea: TStringField;
    qryLineasTipoComprobanteFiscal: TStringField;
    qryLineasNumeroComprobanteFiscal: TLargeintField;
    qryLineasGrupoImprenta: TIntegerField;
    qryLineasFechaEmision: TDateTimeField;
    qryLineasImporteTotal: TLargeintField;
    qryLineasPrimeraLineaDetalle: TStringField;
    qryLineasRutReceptor: TStringField;
    qryLineasRazonSocialReceptor: TStringField;
    qryLineasFechaHoraTimbre: TDateTimeField;
    qryLineasCodigoConcepto: TIntegerField;
    dbgDatosJORDAN: TDBGrid;
    dsObtenerDatosJORDAN: TDataSource;
    ObtenerDatosJORDAN: TADOStoredProc;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnSiguienteClick(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnObtenerComprobantesClick(Sender: TObject);
    procedure btnFinalizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    function  btCatalogoProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure btCatalogoSelect(Sender: TObject; Tabla: TDataSet);
    procedure btnBuscarProcesoClick(Sender: TObject);
    function  buscaOperacionesProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure buscaOperacionesSelect(Sender: TObject; Tabla: TDataSet);
    procedure btnProcesosFacturacionClick(Sender: TObject);
    procedure txtUbicacionButtonClick(Sender: TObject);
    // Rev.5 / 22-03-2009 / Nelson Droguett Sierra
    procedure AConfig_EGATE_HOME_ManualExecute(Sender: TObject);
    procedure AConfig_EGATE_HOME_ParametricoExecute(Sender: TObject);
    procedure ACargarDLLExecute(Sender: TObject);
    procedure ALiberarDLLExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    private
        FCodigoOperacion : Integer;
        FDestino : AnsiString;
        FOutFile : TextFile;
        FNombreArchivoSimple : AnsiString;
        FNombreArchivoControlSimple : AnsiString;
        FNombreArchivoMultiple : AnsiString;
        FNombreArchivoControlMultiple : AnsiString;

        // Rev.4 / 11-03-2009 / Nelson Droguett Sierra ------------------------
        FOutFileFAC : TextFile;
        FNombreArchivoSimpleFAC : AnsiString;
        FNombreArchivoControlSimpleFAC : AnsiString;
        FNombreArchivoMultipleFAC : AnsiString;
        FNombreArchivoControlMultipleFAC : AnsiString;

        FOutFileBOL : TextFile;
        FNombreArchivoSimpleBOL : AnsiString;
        FNombreArchivoControlSimpleBOL : AnsiString;
        FNombreArchivoMultipleBOL : AnsiString;
        FNombreArchivoControlMultipleBOL : AnsiString;
        //---------------------------------------------------------------------
        //---------------------------------------------------------------------


        FCancel : Boolean;
        FOnProcess : Boolean;
        FComprobantesAfectados : Integer;
        //FComprobantesExitosos : Integer;
        //FCantidadRegistros : Integer;
        FNombreImagenCatalogo : AnsiString;
        //FTotalComprobantes : Int64;
        FFechaProceso : TDateTime;
        FDetalleTransitosaparte : Boolean;
        FReproceso : Boolean;
        FPorFacturacion : boolean;
        FFechaProcesoAnterior : TDateTime;
        FFechaProcFacturacion : AnsiString;
        FNumeroProcesoAnterior : Longint;
        FNumeroProceso : Integer;
        FCantidadFacturados : Integer;
        FCantSinImprimir : Integer;
        FComprobanteInicial : Int64;
        FComprobanteFinal : Int64;
        FNoPrnFinal : Int64;
        FNoPrnInicial : Int64;
        FForzarReimpresion : Boolean;
        FImprimirElectronicos : Boolean;
        FHayDatosArchivoSimple : Boolean;
        FHayDatosArchivoMultiple : Boolean;

        // Rev.4 / 11-03-2009 / Nelson Droguett Sierra ------------------------
        FHayDatosArchivoSimpleFAC : Boolean;
        FHayDatosArchivoMultipleFAC : Boolean;
        FHayDatosArchivoSimpleBOL : Boolean;
        FHayDatosArchivoMultipleBOL : Boolean;
        FComprobantesExitososFAC : Integer;
        FCantidadRegistrosFAC : Integer;
        FTotalComprobantesFAC : Int64;
        FComprobantesExitososBOL : Integer;
        FCantidadRegistrosBOL : Integer;
        FTotalComprobantesBOL : Int64;
        FCantidadTimbresFAC : Integer;
        FCantidadTimbresBOL : Integer;
        //---------------------------------------------------------------------
        MSG_SQL_SQL:String;
        //---------------------------------------------------------------------

        procedure CrearNombresArchivos(Path: String);
        function  GenerarArchivo (NombreArchivoFAC, NombreArchivoBOL : AnsiString) : Boolean;
        function  CrearArchivo(Archivo : String; var DescriError : AnsiString; var FExist : Boolean) : Boolean;
        function  EscribirArchivoControl (NombreArchivoControlFAC,NombreArchivoControlBOL : AnsiString) : Boolean;
        function  RegistrarOperacion: boolean;
        function  CerrarOperacion : Boolean;
        function  ActualizarComprobantesEnviados : Boolean;
        function  ActualizarComprobantes : Boolean;
        procedure HabilitarControles(Habilitado : Boolean);
        procedure CargarComprobantesPorFacturacion;
        Function  ObtenerCantidadComprobantesAImprimir : int64;
        Function  ObtenerCantidadComprobantesAImprimirFAC : int64;
        Function  ObtenerCantidadComprobantesAImprimirBOL : int64;
        Function  GenerarArchivoSimple : Boolean;
        Function  GenerarArchivoMultiple : Boolean;
        procedure EscribirArchivoLog(ArchLog, Texto: AnsiString);
        function  ObtieneTipoDocumento(sTipoComprobanteFiscal:String) : integer;
        function  EscribeLog(sMensaje:String):Boolean;
        function  GenerarArchivoGrilla(NombreArchivoFAC, NombreArchivoBOL : AnsiString) : Boolean;


      public
        FLogBuffer : TStringList;
        function Inicializar : Boolean;
        function CurrentMemoryUsage : Cardinal;
        Function DelTree(DirName : string): Boolean;
    end;

var
  frmImprenta: TfrmImprenta;

const
    //REV.8 tama�o en bytes a procesar en la TStringList
    CONST_TAMANO_BYTES_STRINGLIST = 5 * 1024 * 1024;			{1 MB}

implementation

{$R *.dfm}

const
    TOP_SP_OBTENER_LINEAS = 1000;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 03/12/2004
  Description:  Inicializaci�n de este form
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmImprenta.Inicializar : Boolean;
Resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR_LOADING_PARAMETERS    = 'Error cargando los par�metros generales.';
    MSG_ERROR_THE_GENERAL_PARAMETER_DOES_NOT_EXISTS = 'El par�metro DIR_SALIDA_IMPRENTA no existe en la base de daros.';
    MSG_ERROR = 'Error';
Const
    HINT_BTN_TODOS = 'Todo los Comprobantes pendientes de impresi�n';
    HINT_BTN_PROC_IMPRESION = 'Seleccionar un Proceso de Impresi�n masiva anterior';
    HINT_BTN_PROC_FACTURACION = 'Seleccionar los Comprobantes de un proceso de Facturaci�n espec�fico';
begin
    lblCantidad.Caption := '';
    btnProcesosFacturacion.Hint     := HINT_BTN_PROC_FACTURACION;
    btnProcesosFacturacion.ShowHint := True;
    btnBuscarProceso.Hint           := HINT_BTN_PROC_IMPRESION;
    btnBuscarProceso.ShowHint       := True;
    btnObtenerComprobantes.Hint     := HINT_BTN_TODOS;
    btnObtenerComprobantes.ShowHint := True;
    Result := False;
    MSG_SQL_SQL		:= 'SELECT dbo.CONST_IH_INTERFACES_ERROR_SQL()';
    try
        Result := (DMConnections.BaseCAC.Connected) and (OpenTables([tblCatalogoImagenesImprenta]));
        if Result then begin
            nb.PageIndex := 0;
            FNombreImagenCatalogo := '';
            result := ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_SALIDA_IMPRENTA', FDestino);
            if not result then begin
                msgBoxErr(MSG_ERROR_LOADING_PARAMETERS , MSG_ERROR_THE_GENERAL_PARAMETER_DOES_NOT_EXISTS, caption, MB_ICONERROR);
                exit;
            end;
            if spObtenerComprobantesImprentaPreview.Active then begin
                spObtenerComprobantesImprentaPreview.Close;
            end;
            FOnProcess := False;
            FReProceso := False;
            FFechaProcesoAnterior := nulldate;
            FFechaProcFacturacion := '';
            FNumeroProcesoAnterior := 0;
            FNumeroProceso := 0;
            FCantidadFacturados := 0;
            FCantSinImprimir := 0;
            FComprobanteInicial := 0;
            FComprobanteFinal := 0;
            FNoPrnFinal := 0;
            FNoPrnInicial := 0;
            FForzarReimpresion := False;
            FImprimirElectronicos := False;
            neComprobanteInicial.Clear;
            neComprobanteFinal.Clear;
            neComprobanteInicial.Enabled := False;
            neComprobanteFinal.Enabled := False;
            FFechaProceso := NowBase(DMConnections.BaseCAC );
            CrearNombresArchivos(FDestino);
            FDetalleTransitosAparte := False;
            FLogBuffer:= TStringList.Create;
            FLogBuffer.Clear;
        end;
    except
        on E: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSiguienteClick
  Author:    ndonadio
  Date Created: 14/03/2005
  Description:  Paso a la pagina Siguiente
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.btnSiguienteClick(Sender: TObject);
Resourcestring
   MSG_ERROR_THE_ENTERED_DATA_ARE_INCORRECT = 'Los datos ingresados son incorrectos';
   MSG_ERROR_INITIAL_NUMBER_INCORRECT = 'El N� Inicial debe ser mayor que cero y menor que el N� Final.';
   MSG_ERROR_FINAL_NUMBER_INCORRECT = 'El N� Final debe ser mayor que cero y mayor que el N� Inicial';
Const
   STR_SELECTED_PRINTING_RANGE  = 'Comprobantes seleccionados entre el Nro. %d y el Nro. %d';
   STR_FORCING_REPRINTING = 'Se incluyen comprobantes ya impresos anteriormente.';
   STR_PRINTIG_ELECTRONICS = 'Se incluyen comprobantes marcados para envio electr�nico';
begin
    //Valido los campos antes de pasar de pagina
    if neComprobanteInicial.Enabled then
        if not ValidateControls(  [neComprobanteInicial,neComprobanteFinal],
           [((neComprobanteInicial.ValueInt > 0) AND (neComprobanteInicial.ValueInt <= neComprobanteFinal.ValueInt ) )
           ,((neComprobanteFinal.ValueInt > 0) AND (neComprobanteInicial.ValueInt <= neComprobanteFinal.ValueInt ))],
           MSG_ERROR_THE_ENTERED_DATA_ARE_INCORRECT,[MSG_ERROR_INITIAL_NUMBER_INCORRECT,
           MSG_ERROR_FINAL_NUMBER_INCORRECT]) then Exit;
    nb.PageIndex := nb.PageIndex + 1;
    btnFinalizar.Enabled := True;
    //Seteo las labels que "recuerdan" que se ha seleccionado en la sig. ventana...
    lblRangoSeleccionado.Caption := Format(STR_SELECTED_PRINTING_RANGE, [neComprobanteInicial.ValueInt, neComprobanteFinal.ValueInt]);
    //indica si fuerza o no reimpresion
    if FForzarReimpresion then begin
        lblForzandoReimpresion.Caption := STR_FORCING_REPRINTING;
    end else begin
        lblForzandoReimpresion.Caption := '';
    end;
    //indica si se imprimen electronicos
    if FImprimirElectronicos then begin
        lblImprimiendoElectronicos.Caption := STR_PRINTIG_ELECTRONICS;
    end else begin
        lblImprimiendoElectronicos.Caption := '';
    end;
    //Si el label de Forzar Reimpresion esta vacio, reemplazo por el de Imprimiendo electronicos
    //para que no quede el rengl�n en blanco...
    if ( TRIM(lblForzandoReimpresion.Caption) = '' ) then begin
        lblForzandoReimpresion.Caption := lblImprimiendoElectronicos.Caption;
        lblImprimiendoElectronicos.Caption := '';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAnteriorClick
  Author:    lgisuk
  Date Created: 06/12/2005
  Description:  Vuelvo a la pagina Anterior
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.btnAnteriorClick(Sender: TObject);
begin
    nb.PageIndex := nb.PageIndex - 1;
end;
{-----------------------------------------------------------------------------
  Function Name: btnObtenerComprobantesClick
  Author:    gcasais
  Date Created: 12/01/2005
  Description: Genera la "vista previa" de comprobantes a imprimir
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.btnObtenerComprobantesClick(Sender: TObject);
resourcestring
    MSG_INVOICE_ERROR = 'Error al obtener los comprobantes';
    MSG_ERROR = 'Error';
const
    STR_CAPTION = 'Proceso masivo de impresi�n';
const
    STR_THEY_HAVE_BEEN_RECORDS_TO_SEND = 'Se han encontrado %d comprobantes para enviar a imprimir (se visualizan hasta los 100 primeros)';
begin
    neComprobanteFinal.Enabled := True;
    neComprobanteInicial.Enabled := True;
    Caption := STR_CAPTION;
    FReProceso := True;
    FNumeroProceso := 0;
    FNumeroProcesoAnterior := 0;
    FForzarReimpresion := False;
    FImprimirElectronicos := False;
    try
        with spObtenerComprobantesImprentaPreview do begin
            Close;
            Parameters.ParamByName('@CodigoOperacion').Value := NULL;
            Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NULL;
            CommandTimeOut := 5000;
            Open;
        end;
        FComprobantesAfectados := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT (*) FROM COMPROBANTES WITH (NOLOCK) '+ ' WHERE ( FECHAHORAIMPRESO IS NULL)  AND ( TIPOCOMPROBANTE = ' + QuotedStr('NK') + ' ) '+ ' AND ( ENVIOELECTRONICO = 0 ) AND ( ESTADOPAGO <> ''A'') '  );
        FComprobanteInicial := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT MIN (NumeroComprobante) FROM COMPROBANTES WITH (NOLOCK) '+ ' WHERE ( FECHAHORAIMPRESO IS NULL)  AND ( TIPOCOMPROBANTE = ' + QuotedStr('NK') + ' ) '+ ' AND ( ENVIOELECTRONICO = 0 ) AND ( ESTADOPAGO <> ''A'') '  );
        neComprobanteInicial.ValueInt := FComprobanteInicial;
        FComprobanteFinal := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT MAX (NumeroComprobante) FROM COMPROBANTES WITH (NOLOCK) '+ ' WHERE ( FECHAHORAIMPRESO IS NULL)  AND ( TIPOCOMPROBANTE = ' + QuotedStr('NK') + ' ) '+ ' AND ( ENVIOELECTRONICO = 0 ) AND ( ESTADOPAGO <> ''A'') '  );
        neComprobanteFinal.ValueInt := FComprobanteFinal;
        btnSiguiente.Enabled := (FComprobantesAfectados > 0);
        lblCantidad.Caption := Format(STR_THEY_HAVE_BEEN_RECORDS_TO_SEND ,[FComprobantesAfectados]);
    except
        on E: Exception do begin
            MsgBoxErr(MSG_INVOICE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end
end;

{-----------------------------------------------------------------------------
  Function Name: btCatalogoProcess
  Author:    gcasais
  Date Created: 24/12/2004
  Description: Dibujamos la tabla de b�squeda
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmImprenta.btCatalogoProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
    Texto  := Tabla.FieldByName('Nombre').AsString + Space(10) + '(' + Tabla.FieldByName('Descripcion').AsString + ')';
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: btCatalogoSelect
  Author:    gcasais
  Date Created: 24/12/2004
  Description: Cargamos el nombre de la imagen que se seleccion�
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.btCatalogoSelect(Sender: TObject; Tabla: TDataSet);
begin
    FNombreImagenCatalogo := Trim(Tabla.FieldByName('Nombre').AsString);
    bteImagen.Text := FNombreImagenCatalogo;
end;

{-----------------------------------------------------------------------------
  Function Name: btnBuscarProcesoClick
  Author:    ndonadio
  Date Created: 02/05/2005
  Description: Setea todo para reprocesar un preceso de facturacion anterior
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.btnBuscarProcesoClick(Sender: TObject);
resourcestring
    MSG_INVOICE_ERROR = 'Error al obtener los comprobantes';
    MSG_ERROR = 'Error';
const
    STR_CAPTION = 'Proceso masivo de impresi�n';
const
    STR_THEY_HAVE_BEEN_RECORDS_TO_SEND_TO_PRINT = 'Se han encontrado %d comprobantes para enviar a imprimir (se visualizan hasta los 100 primeros)';
    STR_REPROCESS_OPERATION = ' - Reprocesando operaci�n N� %d del %s';
begin
    //Obtengo las operaciones de impresion masiva
    with spObtenerOperacionesImpresionMasiva do begin
        CommandTimeOut := 5000;
       	Open;
    end;
    FForzarReimpresion := False;
    FImprimirElectronicos := False;
    FNumeroProcesoAnterior := 0;
    FNumeroProceso := 0;
    buscaOperaciones.Activate ;
    if FNumeroProcesoAnterior > 0 then  begin
        try
            //Obtengo el preview
            with spObtenerComprobantesImprentaPreview, Parameters do begin
                Close;
                ParamByName('@CodigoOperacion').Value := FNumeroProcesoAnterior;
                ParamByName('@NumeroProcesoFacturacion').Value := NULL;
                CommandTimeOut := 5000;
                Open;
            end;
            FComprobantesAfectados := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT (*) FROM DetalleComprobantesEnviados WITH (NOLOCK) WHERE  DetalleComprobantesEnviados.CodigoOperacionInterfase = ' + IntToStr(FNumeroProcesoAnterior));
            FComprobanteInicial := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT MIN (NumeroComprobante) FROM DetalleComprobantesEnviados WITH (NOLOCK) WHERE  DetalleComprobantesEnviados.CodigoOperacionInterfase = ' + IntToStr(FNumeroProcesoAnterior));
            neComprobanteInicial.ValueInt := FComprobanteInicial;
            FComprobanteFinal := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT MAX (NumeroComprobante) FROM DetalleComprobantesEnviados WITH (NOLOCK) WHERE  DetalleComprobantesEnviados.CodigoOperacionInterfase = ' + IntToStr(FNumeroProcesoAnterior));
            neComprobanteFinal.ValueInt := FComprobanteFinal;
            btnSiguiente.Enabled := (FComprobantesAfectados > 0);
            lblCantidad.Caption := Format(STR_THEY_HAVE_BEEN_RECORDS_TO_SEND_TO_PRINT,[FComprobantesAfectados]);
            neComprobanteFinal.Enabled := False;
            neComprobanteInicial.Enabled := False;
            // cargo la fecha del proceso anterior
            FFechaProcesoAnterior := QueryGetValueDateTime( DMConnections.BaseCAC, Format('SELECT Fecha FROM LogOperacionesInterfases (NOLOCK) WHERE CodigoOperacionInterfase = %d',[FNumeroProcesoAnterior]));
            FReProceso := True;
            Caption := STR_CAPTION + Format(STR_REPROCESS_OPERATION,[FNUmeroProcesoAnterior, FormatDateTime('dd/mm/yyyy', FFechaProcesoAnterior)]);
        except
            on E: Exception do begin
                MsgBoxErr(MSG_INVOICE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                neComprobanteFinal.Enabled := True;
                neComprobanteInicial.Enabled := True;
                FReProceso := False;
            end;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaOperacionesProcess
  Author:    lgisuk
  Date Created: 06/12/2005
  Description:  veo los procesos realizados
  Parameters:  None
  Return Value: None
-----------------------------------------------------------------------------}
function TfrmImprenta.buscaOperacionesProcess(Tabla: TDataSet;var Texto: String): Boolean;
const
    STR_PROCESS = 'Fecha Realizaci�n: %s  -  Usuario: %s - %d Comprobantes Procesados';
begin
	  with Tabla do begin
      	Texto := Format(STR_PROCESS, [FieldByName('Fecha').AsString, FieldByName('Usuario').AsString, FieldByName('Cantidad').asInteger]);
    end;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaOperacionesSelect
  Author:    lgisuk
  Date Created: 06/12/2005
  Description:  selecciono un proceso
  Parameters:  None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.buscaOperacionesSelect(Sender: TObject; Tabla: TDataSet);
begin
    with Tabla do begin
        FNumeroProcesoAnterior := FieldByName('CodigoOperacionInterfase').AsInteger;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesosFacturacionClick
  Author:    lgisuk
  Date Created: 06/12/2005
  Description:  
  Parameters:  None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.btnProcesosFacturacionClick(Sender: TObject);
resourcestring
    MSG_INVOICE_ERROR = 'Error al obtener los comprobantes';
const
    STR_DOING_PROC_OF_INVOINCIG = ' - Procesando Proc. de Facturaci�n N� %d del %s';
    STR_CAPTION = 'Proceso masivo de impresi�n';
var
    f: TdlgSeleccionProcFacturacion;
begin
    Application.CreateForm(TdlgSeleccionProcFacturacion, f);
    if not f.Inicializar then exit;
    if ( f.ShowModal <> mrOK ) then begin
        f.Release;
        Exit;
    end;
    FNumeroProcesoAnterior := 0;
    FNumeroProceso:=  f.FNumeroProceso;
    FCantSinImprimir := f.FCantNoPrn;
    FComprobantesAfectados := f.FSelTotal;
    FCantidadFacturados := f.FCantTotal;
    FComprobanteInicial := f.FSelFirst;
    FComprobanteFinal := f.FSelLast;
    FNoPrnFinal     := f.FMaxNoPrn;
    FNoPrnInicial   := f.FMinNoPrn ;
    FFechaProcFacturacion := f.FFechaProceso;
    neComprobanteFinal.Enabled := True;
    neComprobanteInicial.Enabled := True;
    FForzarReimpresion := not f.chkSoloPendientes.Checked;
    FImprimirElectronicos := f.chkImprimirElectronicas.Checked;
    FPorFacturacion := True;
    f.Release;
    Caption := STR_CAPTION + Format(STR_DOING_PROC_OF_INVOINCIG,[FNumeroProceso, FFechaProcFacturacion]);
    CargarComprobantesPorFacturacion;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarComprobantesPorFacturacion
  Author:    lgisuk
  Date Created: 06/12/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.CargarComprobantesPorFacturacion;
resourcestring
    MSG_INVOICE_ERROR = 'Error al obtener los comprobantes';
    MSG_ERROR = 'Error';
const
    STR_THEY_HAVE_FOUND_VOUCHERS_FOR_THE_PROCESS_OF_INVOICING = 'Se han encontrado %d comprobantes pertenecientes al Proceso de Facturacion N� %d del d�a %s (se visualizan hasta los 100 primeros)';
begin
    try
        with spObtenerComprobantesImprentaPreview, Parameters do begin
            Close;
            ParamByName('@CodigoOperacion').Value := NULL;
            ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProceso;
            ParamByName('@ForzarReimpresion').Value := IIF(FForzarReimpresion = True, 1, 0); //Forzar Reimpresion
            ParamByName('@ImprimirElectronicos').Value := IIF(FImprimirElectronicos = True, 1, 0); //Imprimir Electronicos
            CommandTimeOut := 5000;
            Open;
        end;
    except
        on E: Exception do begin
            MsgBoxErr(MSG_INVOICE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            neComprobanteFinal.Enabled := True;
            neComprobanteInicial.Enabled := True;
            FPorFacturacion := False;
            Exit;
        end;
    end;
    lblCantidad.Caption := Format(STR_THEY_HAVE_FOUND_VOUCHERS_FOR_THE_PROCESS_OF_INVOICING, [FComprobantesAfectados, FNumeroProceso, FFechaProcFacturacion]);
    neComprobanteInicial.ValueInt := FComprobanteInicial;
    neComprobanteFinal.ValueInt := FComprobanteFinal;
    btnSiguiente.Enabled := (FComprobantesAfectados > 0);
end;

{******************************** Function Header ******************************
  Function Name: btnBrowseForFolderClick
  Author:    gcasais
  Date Created: 06/12/2004
  Description: Seleccionar la ubicaci�n para depositar el archivo creado
  Parameters: None
  Return Value: None
********************************************************************************}
procedure TfrmImprenta.txtUbicacionButtonClick(Sender: TObject);
const
    STR_SELECT_LOCATION = 'Seleccione una ubicaci�n para el archivo';
var
    Location: String;
begin
    Location := Trim(BrowseForFolder(STR_SELECT_LOCATION));
    if Location = '' then exit;
    FNombreArchivoSimple := '';
    txtUbicacion.Text := Location;
    FDestino := GoodDir(txtUbicacion.Text);
    CrearNombresArchivos(FDestino);
    btnFinalizar.Enabled := True;
end;

{-----------------------------------------------------------------------------
  Function Name: CrearNombresArchivos
  Author:    lgisuk
  Date Created: 06/12/2005
  Description: Crea el nombre de los archivos
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.CrearNombresArchivos(Path: String);
const
    CONTROL_FILE_PREFIX = 'Control-';
    TYPE_VOUCHER  = 'NC';
    FILE_PREFIX = TYPE_VOUCHER;
    DATE_FORMAT = 'ddmmyyyy';
    FILE_EXTENSION = '.txt';
    TYPE_SIMPLE = 'S';
    TYPE_MULTIPLE = 'M';
    // Rev.4 / 11-03-2009 / Nelson Droguett Sierra.
    FILE_PREFIXFAC = 'FA';
    FILE_PREFIXBOL = 'BO';
    //---------------------------------------------------------------------
begin
    //FNombreArchivoControlSimple := GoodDir(Path) + CONTROL_FILE_PREFIX + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;
    FNombreArchivoSimple := GoodDir(Path) + FILE_PREFIX + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;
    //FNombreArchivoControlMultiple := GoodDir(Path) + CONTROL_FILE_PREFIX + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;
    //FNombreArchivoMultiple := GoodDir(Path) + FILE_PREFIX + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;
    //txtUbicacion.Text := FNombreArchivoSimple;
    // Rev.4 / 11-03-2009 / Nelson Droguett Sierra ---------------------------

    FNombreArchivoControlSimpleFAC := GoodDir(Path) + CONTROL_FILE_PREFIX + FILE_PREFIXFAC + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;
    FNombreArchivoSimpleFAC := GoodDir(Path) + FILE_PREFIXFAC  + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;

    FNombreArchivoControlMultipleFAC := GoodDir(Path) + CONTROL_FILE_PREFIX + FILE_PREFIXFAC + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;
    FNombreArchivoMultipleFAC := GoodDir(Path) + FILE_PREFIXFAC  + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;

    FNombreArchivoControlSimpleBOL := GoodDir(Path) + CONTROL_FILE_PREFIX + FILE_PREFIXBOL  + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;
    FNombreArchivoSimpleBOL := GoodDir(Path) + FILE_PREFIXBOL + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;

    FNombreArchivoControlMultipleBOL := GoodDir(Path) + CONTROL_FILE_PREFIX  + FILE_PREFIXBOL + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;
    FNombreArchivoMultipleBOL := GoodDir(Path) + FILE_PREFIXBOL + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;

    txtUbicacion.Text := FNombreArchivoSimple;
    // -----------------------------------------------------------------------
end;


{-----------------------------------------------------------------------------
  Function Name: CrearArchivo
  Author:    gcasais
  Date Created: 21/12/2004
  Description: Creaci�n del Archivo de interface
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmImprenta.CrearArchivo(Archivo: String; var DescriError: AnsiString; var FExist: boolean):Boolean;
Resourcestring
    MSG_ERROR_PROCESS_CANCELLED = 'Se ha cancelado el proceso debido a la existencia de un archivo id�ntico en el directorio';
    MSG_ERROR_OF_DESTINY = 'de destino';
const
    STR_THE_FILE = 'El archivo ';
    STR_ALREADY_EXISTS = ' ya existe en el directorio de destino. �Desea reemplazarlo?';
    STR_ATTENTION = 'Atenci�n';
begin
    Result := False;
    //Verificamos que no exista uno con el mismo nombre
    if  FileExists(Archivo) and (MsgBox(STR_THE_FILE + Archivo + STR_ALREADY_EXISTS, STR_ATTENTION, MB_YESNO + MB_ICONQUESTION) = IDNO) then begin
        DescriError := MSG_ERROR_PROCESS_CANCELLED + CRLF + MSG_ERROR_OF_DESTINY;
        //devuelvo FExists en True para que no informe de Error...
        FExist := True;
        Exit;
    end;
    // Listo, intentamos crearlo
    try
        AssignFile(FOutFileFAC, Trim(Archivo));
        Rewrite(FOutFileFAC);
        CloseFile(FOutFileFAC);
        // Rev.4 / 11-03-2009 / Nelson Droguett Sierra
        Erase(FOutFileFAC);
        Result := True;
    except
        on e : Exception do begin
            DescriError := e.Message;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: HabilitarControles
  Author:    lgisuk
  Date Created: 06/12/2005
  Description: habilita los controles
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.HabilitarControles(Habilitado: Boolean);
begin
    txtUbicacion.Enabled := Habilitado;
    bteImagen.Enabled := Habilitado;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 18/01/2005
  Description: Actualiza el LogOperacionesInterface
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmImprenta.RegistrarOperacion: boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
    DescError : string;
begin
    Result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_SALIENTE_IMPRESION, ExtractFileName(FNombreArchivoSimple), UsuarioSistema, '', True, False, FFechaProceso, 0, FCodigoOperacion, DescError);
    if not Result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarComprobante
  Author:    gcasais
  Date Created: 13/01/2005
  Description: Actualiza un comprobante como impreso  (fechaHoraImpreso)
  Parameters: None
  Return Value: Boolean

  Revision : 1
      Author : ggomez
      Date : 06/12/2006
      Description : Quit� el uso de la transacci�n pues esta hace que se bloque�
        la tabla Comprobantes. Optamos por quitarla pues es poco probable de que
        falle esta actualizaci�n.
        Asi mismo agregu� que en caso de error, deje un registro en la tabla
        ErroresInterfaces y si este registro no se realiza con exito, entonces
        deja el registro en un archivo que est� en el mismo
        lugar donde se encuentra el ejecutable de la app, en la que se indica
        que fall� un determinado proceso. Esto lo hice porque si ocurre un error
        que adem�s de las pantallas de error que el operador capture, queden m�s
        datos a�n.
-----------------------------------------------------------------------------}
function TfrmImprenta.ActualizarComprobantes: Boolean;
resourcestring
    MSG_ERROR_UPDATING_NK = 'Error al actualizar un comprobante';
    MSG_ERROR = 'Impresi�n Comprobantes. Error actualizando Fecha Impresi�n de Comprobante. C�d. Op. Interface: %d. Fecha para actualizar: %s. �ltimo N� Comprobante actualizado: %d.';
const
    FILE_NAME_ERRORS = '.\Errors.txt';
var
    NumeroComp: int64;
    CodigoErrorSQL	:integer;
begin
    NumeroComp := 0;
    try
//        DMConnections.BaseCAC.BeginTrans;
        with spActualizarFechaImpresionComprobantesEnviadosAImprenta, Parameters do begin
            Close;
            ParamByName('@FechaHoraImpreso').Value  := FFechaProceso;
            ParamByName('@NumeroComprobante').Value := 0;
            //revision 1
            ParamByName('@Usuario').Value := UsuarioSistema;
            CommandTimeOut := 5000;
            while  NumeroComp >= 0 do begin;
                ExecProc;
                NumeroComp := ParamByName('@NumeroComprobante').Value;
            end;
        end;
//        DMConnections.BaseCAC.CommitTrans;
        Result := True;
    except
        on E: Exception do begin
//            DMConnections.BaseCAC.RollbackTrans;
            //if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion,
            //        Format(MSG_ERROR,
            //            [FCodigoOperacion,
            //            FormatDateTime('dd/mm/yyyy hh:mm:ss', FFechaProceso),
            //            NumeroComp])) then begin
            CodigoErrorSQL				:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_SQL);
            if AgregarEnErroresInterfaces( DMConnections.BaseCAC,FCodigoOperacion,CodigoErrorSQL,E.Message) then
                EscribirArchivoLog(FILE_NAME_ERRORS,
                    Format(MSG_ERROR,
                        [FCodigoOperacion,
                        FormatDateTime('dd/mm/yyyy hh:mm:ss', FFechaProceso),
                        NumeroComp]));


            raise Exception.Create( MSG_ERROR_UPDATING_NK + CRLF + E.Message);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarComprobantesEnviados
  Author:    flamas
  Date Created: 18/01/2005
  Description: Actualiza el Detalle de Comprobantes enviados por la interfaz
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmImprenta.ActualizarComprobantesEnviados : Boolean;
resourcestring
    MSG_ERROR_UPDATING_SENT_INVOICES = 'Error actualizando facturas enviadas';
    MSG_ERROR = 'Error';
begin
  	try
    	with spActualizarComprobantesEnviadosAImprenta, Parameters do begin
          	ParamByName( '@CodigoOperacionInterfase' ).Value := FCodigoOperacion;
      			ParamByName( '@FechaHoraImpreso' ).Value := FFechaProceso;
            ParamByName( '@Cantidad' ).Value := 1;
            CommandTimeOut := 5000;
            while ParamByName( '@Cantidad' ).Value > 0 do begin
                ExecProc;
            end;
            Result := True;
        end;
    except
        on E : Exception do begin
              Result := False;
              MsgBoxErr(MSG_ERROR_UPDATING_SENT_INVOICES, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EscribirArchivoControl
  Author:    gcasais
  Date Created: 27/12/2004
  Description: Genera el Archivo de Control
  Parameters: None
  Return Value: Boolean

Revision 1:
    Author : ggomez
    Date : 13/09/2006
    Description :
        - A la l�nea con la que se genera el archivo, le concaten� CRLF
        para que los archivos se puedan ver correctamete en el Notepad.
        Esto se hizo pues algunos archivos generados no se pod�an ver correctamente
        en el Notepad, aunque si se ve�an bien en otros editores de texto como
        Wordpad, UltraEdit32, ConTEXT, Crimson.
        Este cambio se pidi� por la raz�n expresada anteriormente, no porque la
        generaci�n del archivo haya tenido error.
        - Coment� la l�nea SetLength(Linea, Largo); pues no tiene sentido hacerlo.

-----------------------------------------------------------------------------}
function TfrmImprenta.EscribeLog(sMensaje: String): Boolean;
begin
  if chbLogSucesos.Checked then
  begin
     FLogBuffer.Add(FormatDateTime('yyyy/mm/dd HH:nn:ss:zzz',Now)+'   '+sMensaje);
  end;
  Result := True;
end;

function TfrmImprenta.EscribirArchivoControl (NombreArchivoControlFAC,
                                              NombreArchivoControlBOL : AnsiString): Boolean;
Resourcestring
    MSG_WRITE_FAIL = 'Error de escritura';
const
    PREFIJO = '2000';
    SEPARADOR = '|';
var
//    Largo :integer;
    Li_1, Li_2, Li_3: String;
    Linea : String;
begin
    Linea := '';
    try
{        FCantidadRegistros := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT(*) FROM ##Lineas WITH (NOLOCK)');
        Li_1 := IntToStr(FCantidadRegistros) + SEPARADOR;
        Li_2 := IntToStr(FComprobantesExitosos) + SEPARADOR;
        Li_3 := IntToStr(Round(FTotalComprobantes / 100)) + SEPARADOR;
//        Largo := Length(PREFIJO + SEPARADOR) + Length(Li_1) + Length(Li_2) + Length(Li_3);
//        SetLength(Linea, Largo);
        Linea := PREFIJO + SEPARADOR + Li_1 + Li_2 + Li_3;
        Linea := Linea + CRLF;
        StringToFile(Linea, NombreArchivoControl);
        Result := True;
}
        // Rev.4 / 11-03-2009 / Nelson Droguett Sierra -------------------------
        FCantidadRegistrosFAC := FCantidadTimbresFAC + QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT(*) FROM ##Lineas WITH (NOLOCK) WHERE TipoComprobanteFiscal IN ('+''''+TC_FACTURA+''''+','+''''+TC_FACTURA_AFECTA+''''+','+''''+TC_FACTURA_EXENTA+''''+ ')');
        Li_1 := IntToStr(FCantidadRegistrosFAC) + SEPARADOR;
        Li_2 := IntToStr(FComprobantesExitososFAC) + SEPARADOR;
        Li_3 := IntToStr(Round(FTotalComprobantesFAC / 100)) + SEPARADOR;
        Linea := PREFIJO + SEPARADOR + Li_1 + Li_2 + Li_3;
        Linea := Linea + CRLF;
        StringToFile(Linea, NombreArchivoControlFAC);

        FCantidadRegistrosBOL := FCantidadTimbresBOL + QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT(*) FROM ##Lineas WITH (NOLOCK) WHERE TipoComprobanteFiscal IN ('+''''+TC_BOLETA+''''+','+''''+TC_BOLETA_AFECTA+''''+','+''''+TC_BOLETA_EXENTA+''''+ ')');
        Li_1 := IntToStr(FCantidadRegistrosBOL) + SEPARADOR;
        Li_2 := IntToStr(FComprobantesExitososBOL) + SEPARADOR;
        Li_3 := IntToStr(Round(FTotalComprobantesBOL / 100)) + SEPARADOR;
        Linea := PREFIJO + SEPARADOR + Li_1 + Li_2 + Li_3;
        Linea := Linea + CRLF;
        StringToFile(Linea, NombreArchivoControlBOL);
        //----------------------------------------------------------------------


        Result := True;



    except
        on E: Exception do begin
            raise Exception.Create(MSG_WRITE_FAIL + Space(1) + E.Message );
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: EscribirArchivoLog
Author : ggomez
Date Created : 06/12/2006
Description : Dado un nombre de archivo escribe en �l el texto pasado como par�metro.
    Si ocurre un error al escribir en el archivo, genera un evento de windows
    (Visor de sucesos de windows).
Parameters : ArchLog, Texto: AnsiString
Return Value : None
*******************************************************************************}
procedure TfrmImprenta.EscribirArchivoLog(ArchLog, Texto: AnsiString);
resourcestring
    MSG_FECHA = 'Fecha Log: %s. %s.';
    MSG_ERROR_REGISTRAR_LOG = 'Error registrar log: %s. Texto: %s. Error: %s.';
var
    f: TextFile;
begin
    try
        MoveToMyOwnDir;
        AssignFile(F, ArchLog);
        try
            // Si el archivo no existe, entonces crearlo.
            if not FileExists(ArchLog) then begin
                ReWrite(F);
            end else begin
                // El archivo existe, entonces abrirlo.
                Append(F);
            end;
            // Escribir en el archivo la informaci�n
            Writeln(F, Format(MSG_FECHA,
                [FormatDateTime('yyyymmdd hh:mm:ss', Now), Texto]));
        finally
            CloseFile(F);
        end;
    except
        on E: Exception do begin
            EventLogReportEvent(elError, Format(MSG_ERROR_REGISTRAR_LOG,
                [Archlog, Texto, E.Message]), '');
        end;
    end; // except
end;

{-----------------------------------------------------------------------------
  Function Name: EscribirLinea
  Author:    gcasais
  Date Created: 27/12/2004
  Description: Escribe y contabiliza los registros escritos a la interface
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmImprenta.GenerarArchivo (NombreArchivoFAC, NombreArchivoBOL : AnsiString) : Boolean;
resourcestring
    MSG_ERROR_GENERATING_TEMP_FILE  = 'Error generando el archivo temporal.';
    MSG_ERROR_OBTENER_RUT           = 'Ha ocurrido un error al obtener el RUT de la concesionaria.';
    MSG_ERROR_TIMBRE	              = 'No se pudo timbrar el documento electr�nico';
Const
    STR_WRITING_FILE_IN_DISC = 'Escribiendo los datos al disco.';
var


  SLArchivoFacturas : TStringList;
  SLArchivoBoletas : TStringList;




  //REV.8
  //		la grabaci�n se la StringList se hace hasta un tope de bytes.
  TamanoStringListFacturas : Cardinal;	//hasta 4 mil millones
  TamanoStringListBoletas : Cardinal; //hasta 4 mil millones
  fsFacturas : TFileStream;
  fsBoletas : TFileStream;

  Timbre : TTimbreArchivo;
  // Rev.5 / 20-03-2009 / Nelson Droguett Sierra
  // Datos para el timbre electronico
  RutConcesionaria : String;
  CodigoErrorSQL:integer;
  bTimbreVacio:Boolean;
begin
     // Rev.5 / 20-03-2009 / Nelson Droguett Sierra ----------------------------
     try
            ObtenerParametroGeneral(qryLineas.Connection, 'RUT_EMPRESA', RutConcesionaria);
     except
            on E: Exception do begin
                // Mensja de error
                MsgBoxErr(MSG_ERROR_OBTENER_RUT, E.Message, Self.Caption, MB_ICONSTOP);
                // Como ocurri� un error, salir con False.
                Result := False;
                Exit;
            end;
     end; // except
     //-------------------------------------------------------------------------
    bTimbreVacio:=False;
    labelProgreso.Caption := STR_WRITING_FILE_IN_DISC;
    try


        //AssignFile(FOutFileFAC, Trim(NombreArchivoFAC));
        //Rewrite(FOutFileFAC);
        //AssignFile(FOutFileBOL, Trim(NombreArchivoBOL));
        //Rewrite(FOutFileBOL);




        // Rev.5 / 20-03-2009 / Nelson Droguett Sierra ----------------------------
        // SE ELIMINO LA ESCRITURA DIRECTA A DISCO... SE AGREAGN LINEAS A UN
        // STRINGLIST, EL QUE SE ESCRIBE A DISCO POR COMPLETO EN UN SOLO PASO
        // AL FINAL DEL PROCESO
        //
        //AssignFile(FOutFile, Trim(NombreArchivo));
        //Rewrite(FOutFile);
        //
        //qryLineas.Open;
        //while not qryLineas.Eof do begin
        //    WriteLn( FOutFile, qryLineas.FieldByName('Linea').asString);
        //    qryLineas.Next;
        //end;
        //qryLineas.Close;
        //CloseFile(FOutFile);


        EscribeLog( 'BEGIN Generacion del Archivo');
        // Rev.4 / 11-03-2009 / Nelson Droguett Sierra -------------------------

        // Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
        ACargarDLL.Execute;

        SLArchivoFacturas := TstringList.Create;
        SLArchivoBoletas := TstringList.Create;
        SLArchivoFacturas.Clear;
        SLArchivoBoletas.Clear;



        //REV.8  Crear el FileStream
        fsFacturas := TFileStream.Create(Trim(NombreArchivoFAC), fmCreate);
        fsBoletas := TFileStream.Create(Trim(NombreArchivoBOL), fmCreate);
        TamanoStringListBoletas := 0;
        TamanoStringListFacturas := 0;


        qryLineas.CursorType:=ctOpenForwardOnly;
        qryLineas.Open;
        EscribeLog( 'BEGIN Termina de hacer Open de las lineas');
        Timbre.Timbre:='';
        Timbre.Archivo:='';
        while not qryLineas.Eof do begin
            if (qryLineas.FieldByName('GrupoImprenta').AsInteger = 1100) then
            begin
              // Llamada a la DLL que "timbra electronicamente" el documento.
              // El resultado es una cadena XML con el timbre
              //EscribeLog( '       BEGIN Obtener Timbre para el documento' + qryLineas.FieldByName('TipoComprobanteFiscal').AsString+qryLineas.FieldByName('NumeroComprobanteFiscal').AsString);
              {ObtenerTimbreDBNet(	StrToInt(AnsiReplaceStr(StrLeft(RutConcesionaria,10),'.','')),
                              StrRight(Trim(RutConcesionaria),1),
                              ObtieneTipoDocumento(qryLineas.FieldByName('TipoComprobanteFiscal').AsString),
                              qryLineas.FieldByName('NumeroComprobanteFiscal').AsInteger,
                              qryLineas.FieldByName('FechaEmision').AsDateTime,
                              qryLineas.FieldByName('ImporteTotal').AsInteger,
                              qryLineas.FieldByName('PrimeraLineaDetalle').AsString,
                              StrToInt( StrLeft( Trim( qryLineas.FieldByName('RutReceptor').AsString ),Length(Trim(qryLineas.FieldByName('RutReceptor').AsString))-1)),
                              StrRight(Trim(qryLineas.FieldByName('RutReceptor').AsString),1) ,
                              qryLineas.FieldByName('RazonSocialReceptor').AsString,
                              qryLineas.FieldByName('FechaHoraTimbre').AsDateTime,False,
                              Timbre);
              }


              // Rev.7 / 11-06-2009 / Nelson Droguett Sierra
              with spObtenerDatosParaTimbreElectronico do begin
                  Parameters.Refresh;
                  Parameters.ParamByName('@TipoComprobante').Value := qryLineas.FieldByName('TipoComprobante').AsString;
                  Parameters.ParamByName('@NumeroComprobante').Value := qryLineas.FieldByName('NumeroComprobante').AsInteger;
                  ExecProc;
                  //No timbramos si el documento es una NK antigua
                  //if TipoDocumentoElectronico <> FTipoNotaCobro then begin
                    if ObtenerTimbreDBNet(	Parameters.ParamByName('@RutEmisor').Value,
                                            Parameters.ParamByName('@DVRutEmisor').Value,
                                            Parameters.ParamByName('@CodigoTipoDocumentoElectronico').Value,
                                            Parameters.ParamByName('@NumeroComprobanteFiscal').Value,
                                            Parameters.ParamByName('@FechaEmision').Value,
                                            Parameters.ParamByName('@TotalComprobante').Value,
                                            Parameters.ParamByName('@DescripcionPrimerItem').Value,
                                            Parameters.ParamByName('@RutReceptor').Value,
                                            Parameters.ParamByName('@DVRutReceptor').Value,
                                            Parameters.ParamByName('@RazonSocialONombrePersona').Value,
                                            Parameters.ParamByName('@FechaTimbre').Value,
                                            False, Timbre
                              ) then begin
                    end
                    else MsgBox(MSG_ERROR_TIMBRE, Caption, MB_ICONERROR);

                  //end;
              end;
              EscribeLog( '       Obtuvo Timbre para el documento' + qryLineas.FieldByName('TipoComprobanteFiscal').AsString+qryLineas.FieldByName('NumeroComprobanteFiscal').AsString);

              if Trim(Timbre.Timbre) ='' then
              begin
                EscribeLog( FormatFloat('Memory used: ,.# K', CurrentMemoryUsage / 1024)) ;
                EscribeLog( '!!! TIMBRE VACIO !!! ' +  qryLineas.FieldByName('TipoComprobante').AsString + ' '+qryLineas.FieldByName('NumeroComprobante').AsString + ' ' +qryLineas.FieldByName('TipoComprobanteFiscal').AsString + ' '+qryLineas.FieldByName('NumeroComprobanteFiscal').AsString );
                if (  Pos(qryLineas.FieldByName('TipoComprobanteFiscal').AsString,TC_FACTURA+','+TC_FACTURA_AFECTA+','+TC_FACTURA_EXENTA+','+'FM')>0 )  then
                  SLArchivoFacturas.Delete(SLArchivoFacturas.Count-1)
                else if ( Pos(qryLineas.FieldByName('TipoComprobanteFiscal').AsString,TC_BOLETA+','+TC_BOLETA_AFECTA+','+TC_BOLETA_EXENTA+','+'BM')>0 ) then
                  SLArchivoBoletas.Delete(SLArchivoBoletas.Count-1);
                bTimbreVacio:=True;
                EscribeLog( 'TMS:'+Timbre.Tms+' '+'Codigo:'+Timbre.Codigo+' '+'Mensaje:'+Timbre.Mensaje  );
                EscribeLog( '!!! TIMBRE VACIO !!! HACE UN BREAK' );
                Break;
              end;
            end;

            if (  Pos(qryLineas.FieldByName('TipoComprobanteFiscal').AsString,TC_FACTURA+','+TC_FACTURA_AFECTA+','+TC_FACTURA_EXENTA+','+'FM')>0 )  then
            begin
              SLArchivoFacturas.Add(qryLineas.FieldByName('Linea').asString);
              if (qryLineas.FieldByName('GrupoImprenta').AsInteger = 1100) then
              begin
                SLArchivoFacturas.Add('1150'+'|'+Trim(timbre.Timbre)+'|');
                FCantidadTimbresFAC := FCantidadTimbresFAC + 1 ;
              end;

              TamanoStringListFacturas := TamanoStringListFacturas +
                                          Length(qryLineas.FieldByName('Linea').asString)+
                                          IfThen(qryLineas.FieldByName('GrupoImprenta').AsInteger = 1100,
                                                  Length('1150'+'|'+Trim(timbre.Timbre)+'|')   // +Trim(timbre.Timbre)+
                                                ,
                                                  0
                                                );
              if TamanoStringListFacturas >= CONST_TAMANO_BYTES_STRINGLIST then
              begin
                  EscribeLog( '!!!HACE LA GRABACION DEL STRINGLIST FACTURAS AL STREAM' );
                  EscribeLog( FormatFloat('Memory used: ,.# K', CurrentMemoryUsage / 1024)) ;
                 SLArchivoFacturas.SaveToStream(fsFacturas);
                 SLArchivoFacturas.Clear;
                 // Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
                 SLArchivoFacturas.Free;
                 SLArchivoFacturas := TstringList.Create;
                 SLArchivoFacturas.Clear;
                 ALiberarDLL.Execute;
                 ACargarDLL.Execute;
                 EscribeLog( FormatFloat('Memory used: ,.# K', CurrentMemoryUsage / 1024)) ;
                 //-----------------------------------------------------------------------
                 TamanoStringListFacturas := 0;
              end;

            end
            else if ( Pos(qryLineas.FieldByName('TipoComprobanteFiscal').AsString,TC_BOLETA+','+TC_BOLETA_AFECTA+','+TC_BOLETA_EXENTA+','+'BM')>0 ) then
            begin

              SLArchivoBoletas.Add(qryLineas.FieldByName('Linea').asString);
              if (qryLineas.FieldByName('GrupoImprenta').AsInteger = 1100) then
              begin
                SLArchivoBoletas.Add('1150'+'|'+Trim(timbre.Timbre)+'|');
                FCantidadTimbresBOL := FCantidadTimbresBOL + 1 ;
              end;
              TamanoStringListBoletas := TamanoStringListBoletas +
                                          Length(qryLineas.FieldByName('Linea').asString)+
                                          IfThen(qryLineas.FieldByName('GrupoImprenta').AsInteger = 1100,
                                                  Length('1150'+'|'+Trim(timbre.Timbre)+'|') // +Trim(timbre.Timbre)+
                                                ,
                                                  0
                                                );
              if TamanoStringListBoletas >= CONST_TAMANO_BYTES_STRINGLIST then
              begin
                  EscribeLog( '!!!HACE LA GRABACION DEL STRINGLIST BOLETAS AL STREAM' );
                  EscribeLog( FormatFloat('Memory used: ,.# K', CurrentMemoryUsage / 1024)) ;
                 SLArchivoBoletas.SaveToStream(fsBoletas);
                 SLArchivoBoletas.Clear;
                 // Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
                 SLArchivoBoletas.Free;
                 SLArchivoBoletas := TstringList.Create;
                 SLArchivoBoletas.Clear;
                 ALiberarDLL.Execute;
                 ACargarDLL.Execute;
                 EscribeLog( FormatFloat('Memory used: ,.# K', CurrentMemoryUsage / 1024)) ;
                 //---------------------------------------------------------------------
                 TamanoStringListBoletas := 0;
              end;

            end;
            qryLineas.Next;
        end;
        qryLineas.Close;
        EscribeLog( '!!!CIERRA LA CONSULTA' );

        EscribeLog( FormatFloat('Memory used: ,.# K', CurrentMemoryUsage / 1024)) ;

        // Graba los StringList a disco.
        EscribeLog( 'BEGIN Graba desde el String List al Archivo de Texto de las Facturas');
        //SLArchivoFacturas.SaveToFile(Trim(NombreArchivoFAC));
        EscribeLog( '!!!HACE LA ULTIMA GRABACION DEL STRINGLIST FACTURAS AL STREAM' );

        SLArchivoFacturas.SaveToStream(fsFacturas);
        SLArchivoFacturas.Free;
        fsFacturas.Free;
        EscribeLog( 'END Graba desde el String List al Archivo de Texto de las Facturas');
        EscribeLog( FormatFloat('Memory used: ,.# K', CurrentMemoryUsage / 1024)) ;

        EscribeLog( 'BEGIN Graba desde el String List al Archivo de Texto de las Boletas');
        //SLArchivoBoletas.SaveToFile(Trim(NombreArchivoBOL));
        EscribeLog( '!!!HACE LA ULTIMA GRABACION DEL STRINGLIST BOLETAS AL STREAM' );
        SLArchivoBoletas.SaveToStream(fsBoletas);
        SLArchivoBoletas.Free;
        fsBoletas.Free;

        // Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
        ALiberarDLL.Execute;

        EscribeLog( 'END Graba desde el String List al Archivo de Texto de las Boletas');
        EscribeLog( FormatFloat('Memory used: ,.# K', CurrentMemoryUsage / 1024)) ;
        // ---------------------------------------------------------------------
        if bTimbreVacio then
        begin
          EscribeLog( FormatFloat('Memory used: ,.# K', CurrentMemoryUsage / 1024)) ;
          EscribeLog( '!!!ESTA ACA POR EL TIMBRE VACIO' );

          lblProcesoInterrumpido.Visible:=True;
          Result:= False
        end
        else
          Result := True;
    except
        on E : Exception do begin
            Result := false;
            //Registro la excepcion en el log de operaciones
            //ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacion, MSG_ERROR_GENERATING_TEMP_FILE + ' ' + e.Message);
            CodigoErrorSQL				:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_SQL);
            AgregarEnErroresInterfaces( DMConnections.BaseCAC,FCodigoOperacion,CodigoErrorSQL,MSG_ERROR_GENERATING_TEMP_FILE + ' '+E.Message);
            msgBoxErr(MSG_ERROR_GENERATING_TEMP_FILE, E.Message, Caption, MB_ICONERROR);
        end;
    end;
    labelProgreso.Caption := '';
end;

function TfrmImprenta.GenerarArchivoGrilla(NombreArchivoFAC,  NombreArchivoBOL: AnsiString): Boolean;
resourcestring
    MSG_ERROR_GENERATING_TEMP_FILE  = 'Error generando el archivo temporal.';
    MSG_ERROR_OBTENER_RUT           = 'Ha ocurrido un error al obtener el RUT de la concesionaria.';
    MSG_ERROR_TIMBRE	              = 'No se pudo timbrar el documento electr�nico';
Const
    STR_WRITING_FILE_IN_DISC = 'Escribiendo los datos al disco.';
var
  SLArchivoFacturas : TStringList;
  SLArchivoBoletas : TStringList;
  TamanoStringListFacturas : Cardinal;	//hasta 4 mil millones
  TamanoStringListBoletas : Cardinal; //hasta 4 mil millones
  fsFacturas : TFileStream;
  fsBoletas : TFileStream;
  Timbre : TTimbreArchivo;
  RutConcesionaria : String;
  CodigoErrorSQL,iRegistro:integer;
  bTimbreVacio:Boolean;
begin


  FCantidadTimbresFAC:=0;
  FCantidadTimbresBOL:=0;
  try
    ObtenerParametroGeneral(ObtenerDatosJordan.Connection, 'RUT_EMPRESA', RutConcesionaria);
  except
    on E: Exception do begin
        MsgBoxErr(MSG_ERROR_OBTENER_RUT, E.Message, Self.Caption, MB_ICONSTOP);
        Result := False;
        Exit;
    end;
  end;
  bTimbreVacio:=False;
  labelProgreso.Caption := STR_WRITING_FILE_IN_DISC;
  try
        ACargarDLL.Execute;

        SLArchivoFacturas := TstringList.Create;
        SLArchivoBoletas := TstringList.Create;
        SLArchivoFacturas.Clear;
        SLArchivoBoletas.Clear;
        fsFacturas := TFileStream.Create(Trim(NombreArchivoFAC), fmCreate);
        fsBoletas := TFileStream.Create(Trim(NombreArchivoBOL), fmCreate);
        TamanoStringListBoletas := 0;
        TamanoStringListFacturas := 0;

        Timbre.Timbre:='';
        Timbre.Archivo:='';

        dbgDatosJORDan.DataSource.DataSet.DisableControls;
        ObtenerDatosJORDAN.Close;
        ObtenerDatosJORDAN.CommandTimeout:=10000;
        ObtenerDatosJORDAN.Open;

        pbProgreso.Min:=0;
        pbProgreso.Max:=ObtenerDatosJORDAN.RecordCount;

        if Not(ObtenerDatosJORDAN.IsEmpty) then
        begin
            pbProgreso.Position:=0;
            dbgDatosJORDan.DataSource.DataSet.First;
            With dbgDatosJORDAN.DataSource.DataSet do
            begin
              while not Eof do
              begin
                iRegistro := dbgDatosJORDan.DataSource.DataSet.RecNo;
                if FieldByName('GrupoImprenta').AsInteger = 1100 then
                begin
                  with spObtenerDatosParaTimbreElectronico do begin
                        Parameters.Refresh;
                        Parameters.ParamByName('@TipoComprobante').Value := dbgDatosJORDan.DataSource.DataSet.FieldByName('TipoComprobante').AsString;
                        Parameters.ParamByName('@NumeroComprobante').Value := dbgDatosJORDan.DataSource.DataSet.FieldByName('NumeroComprobante').AsInteger;
                        ExecProc;
                        if Not (ObtenerTimbreDBNet(Parameters.ParamByName('@RutEmisor').Value,
                                              Parameters.ParamByName('@DVRutEmisor').Value,
                                              Parameters.ParamByName('@CodigoTipoDocumentoElectronico').Value,
                                              Parameters.ParamByName('@NumeroComprobanteFiscal').Value,
                                              Parameters.ParamByName('@FechaEmision').Value,
                                              Parameters.ParamByName('@TotalComprobante').Value,
                                              Parameters.ParamByName('@DescripcionPrimerItem').Value,
                                              Parameters.ParamByName('@RutReceptor').Value,
                                              Parameters.ParamByName('@DVRutReceptor').Value,
                                              Parameters.ParamByName('@RazonSocialONombrePersona').Value,
                                              Parameters.ParamByName('@FechaTimbre').Value,
                                              False, Timbre
                                )) then
                        begin
                          MsgBox(MSG_ERROR_TIMBRE, Caption, MB_ICONERROR);
                        end
                  end;

                  if Trim(Timbre.Timbre) ='' then
                  begin
                    if ( Pos(FieldByName('TipoComprobanteFiscal').AsString, TC_FACTURA+','+
                                                                            TC_FACTURA_AFECTA+','+
                                                                            TC_FACTURA_EXENTA)>0 )  then
                          SLArchivoFacturas.Delete(SLArchivoFacturas.Count-1)
                    else if ( Pos(FieldByName('TipoComprobanteFiscal').AsString,  TC_BOLETA+','+
                                                                                  TC_BOLETA_AFECTA+','+
                                                                                  TC_BOLETA_EXENTA)>0 ) then
                          SLArchivoBoletas.Delete(SLArchivoBoletas.Count-1);

                    bTimbreVacio:=True;
                    Break;
                  end;
                end;


                if (  Pos(FieldByName('TipoComprobanteFiscal').AsString,TC_FACTURA+','+
                                                                        TC_FACTURA_AFECTA+','+
                                                                        TC_FACTURA_EXENTA)>0 )  then
                begin
                  SLArchivoFacturas.Add(FieldByName('Linea').asString);
                  if (FieldByName('GrupoImprenta').AsInteger = 1100) then
                  begin
                    SLArchivoFacturas.Add('1150'+'|'+Trim(timbre.Timbre)+'|');
                    FCantidadTimbresFAC := FCantidadTimbresFAC + 1 ;
                  end;
                  TamanoStringListFacturas := TamanoStringListFacturas +
                                              Length(FieldByName('Linea').asString)+
                                              IfThen(FieldByName('GrupoImprenta').AsInteger = 1100,Length('1150'+'|'+Trim(timbre.Timbre)+'|'),0);
                  if TamanoStringListFacturas >= CONST_TAMANO_BYTES_STRINGLIST then
                  begin
                     SLArchivoFacturas.SaveToStream(fsFacturas);
                     SLArchivoFacturas.Clear;
                     SLArchivoFacturas.Free;
                     SLArchivoFacturas := TstringList.Create;
                     SLArchivoFacturas.Clear;
                     TamanoStringListFacturas := 0;
                     pbProgreso.Position:=iRegistro;
                     Application.ProcessMessages;
                  end;
                end
                else if ( Pos(FieldByName('TipoComprobanteFiscal').AsString,TC_BOLETA+','+
                                                                            TC_BOLETA_AFECTA+','+
                                                                            TC_BOLETA_EXENTA)>0 ) then
                begin
                  SLArchivoBoletas.Add(FieldByName('Linea').asString);
                  if (FieldByName('GrupoImprenta').AsInteger = 1100) then
                  begin
                    SLArchivoBoletas.Add('1150'+'|'+Trim(timbre.Timbre)+'|');
                    FCantidadTimbresBOL := FCantidadTimbresBOL + 1 ;
                  end;
                  TamanoStringListBoletas := TamanoStringListBoletas +
                                             Length(FieldByName('Linea').asString)+
                                             IfThen(FieldByName('GrupoImprenta').AsInteger = 1100,Length('1150'+'|'+Trim(timbre.Timbre)+'|'),0);
                  if TamanoStringListBoletas >= CONST_TAMANO_BYTES_STRINGLIST then
                  begin
                     SLArchivoBoletas.SaveToStream(fsBoletas);
                     SLArchivoBoletas.Clear;
                     SLArchivoBoletas.Free;
                     SLArchivoBoletas := TstringList.Create;
                     SLArchivoBoletas.Clear;
                     TamanoStringListBoletas := 0;
                     pbProgreso.Position:=iRegistro;
                     Application.ProcessMessages;
                  end;
                end;
                Next;
              end;
            end;
        end;
        SLArchivoFacturas.SaveToStream(fsFacturas);
        SLArchivoFacturas.Free;
        fsFacturas.Free;
        SLArchivoBoletas.SaveToStream(fsBoletas);
        SLArchivoBoletas.Free;
        fsBoletas.Free;
        ObtenerDatosJORDAN.Close;
        dbgDatosJORDan.DataSource.DataSet.EnableControls;
        ALiberarDLL.Execute;
        if bTimbreVacio then
        begin
          lblProcesoInterrumpido.Visible:=True;
          Result:= False
        end
        else
          Result := True;
    except
        on E : Exception do begin
            Result := false;
            CodigoErrorSQL				:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_SQL);
            AgregarEnErroresInterfaces( DMConnections.BaseCAC,FCodigoOperacion,CodigoErrorSQL,MSG_ERROR_GENERATING_TEMP_FILE + ' '+E.Message);
            msgBoxErr(MSG_ERROR_GENERATING_TEMP_FILE, E.Message, Caption, MB_ICONERROR);
        end;
    end;
    labelProgreso.Caption := '';
end;

{-----------------------------------------------------------------------------
  Function Name: CerrarOperacion
  Author:    lgisuk
  Date Created: 06/12/2005
  Description: Actualizo el log de operaciones al final
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TfrmImprenta.CerrarOperacion: boolean;
Resourcestring
    MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
Const
    STR_OBSERVATION = 'Finalizo OK!';
var
    DescError : String;
begin
    Result := ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVATION , DescError);
    if not Result then begin
        MsgBoxErr(MSG_ERROR, DescError, Self.caption, MB_ICONERROR);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerCantidadComprobantesAImprimir
  Author:    lgisuk
  Date Created: 22/12/2005
  Description: Obtengo la cantidad de comprobantes a imprimir
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TfrmImprenta.ObtenerCantidadComprobantesAImprimir : int64;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT COUNT(NumeroComprobante) FROM ##ComprobantesAImprimir WITH (NOLOCK) ' + ' WHERE FechaHoraImpreso IS NULL ');
end;


// Rev.4 / 18-03-2009 / Nelson Droguett Sierra
function TfrmImprenta.ObtenerCantidadComprobantesAImprimirBOL: int64;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT COUNT(NumeroComprobante) FROM ##ComprobantesAImprimir WITH (NOLOCK) ' + ' WHERE (FechaHoraImpreso IS NULL) AND TipoComprobanteFiscal IN ('+''''+TC_BOLETA+''''+','+''''+TC_BOLETA_AFECTA+''''+','+''''+TC_BOLETA_EXENTA+''''+','+''''+'BM'+''''+ ')');

end;



// Rev.4 / 18-03-2009 / Nelson Droguett Sierra
function TfrmImprenta.ObtenerCantidadComprobantesAImprimirFAC: int64;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT COUNT(NumeroComprobante) FROM ##ComprobantesAImprimir WITH (NOLOCK) ' + ' WHERE (FechaHoraImpreso IS NULL) AND  TipoComprobanteFiscal IN ('+''''+TC_FACTURA+''''+','+''''+TC_FACTURA_AFECTA+''''+','+''''+TC_FACTURA_EXENTA+''''+','+''''+'FM'+''''+ ')');
end;


// Rev.4 / 18-03-2009 / Nelson Droguett Sierra
// Obtiene el tipo de documento electronico a partir del TipoComprobante
function TfrmImprenta.ObtieneTipoDocumento(sTipoComprobanteFiscal:String): integer;
begin
  if ((sTipoComprobanteFiscal = TC_FACTURA_AFECTA) OR  (sTipoComprobanteFiscal = TC_FACTURA)) then
     Result:=33
  else if sTipoComprobanteFiscal = TC_FACTURA_EXENTA  then
     Result:=34
  else if ( (sTipoComprobanteFiscal = TC_BOLETA) OR
  (sTipoComprobanteFiscal = TC_BOLETA_AFECTA) ) then
     Result:=39
  else if sTipoComprobanteFiscal = TC_BOLETA_EXENTA  then
     Result:=41

end;
//----------------------------------------------------------------------------



{-----------------------------------------------------------------------------
  Function Name: GenerarArchivoSimple
  Author:    lgisuk
  Date Created: 20/12/2005
  Description: Genero el archivo con Notas de Cobro de una Hoja
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TfrmImprenta.GenerarArchivoSimple : Boolean;
resourcestring
    MSG_ERROR = 'Error';
const
    STR_INIT_PROCESS     = 'Iniciando proceso generaci�n Archivo Simple...';
    STR_PROCESS_INVOICES = 'Quedan %d comprobantes por procesar ... ';
    STR_COPYING_FILE     = 'Copiando archivo resultado ... ';
var
    //Cantidad : int64;
    // Rev.4 / 18-03-2009 / Nelson Droguett Sierra
    CantidadFAC : int64;
    CantidadBOL : int64;
    CodigoErrorSQL:integer;
begin
    Result := False;
    //Inicializo variables
    Screen.Cursor := crHourGlass;
    FCancel := False;
    KeyPreview := True;
    //Inicializo la barra de progreso
    //FComprobantesExitosos := 0;
    //FCantidadRegistros  := 0;
    //FTotalComprobantes  := 0;
    // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------
    FComprobantesExitososFAC := 0;
    FComprobantesExitososBOL := 0;

    FCantidadRegistrosFAC  := 0;
    FCantidadRegistrosBOL  := 0;

    FTotalComprobantesFAC  := 0;
    FTotalComprobantesBOL  := 0;

    FCantidadTimbresFAC  := 0;
    FCantidadTimbresBOL  := 0;

    CantidadFAC := 0;
    CantidadBOL := 0;

    //--------------------------------------------------------------------------
    pnlProgreso.Visible := True;
    //Comienzo a Procesar
    FOnProcess := True;
    try
        try
            //Informo que inicio el proceso
            labelProgreso.Caption := STR_INIT_PROCESS;
            labelProgreso.Update;

            //Obtenego los comprobantes a enviar a imprenta
            EscribeLog( 'BEGIN spPrepararProcesoInterfazImprenta');
            with spPrepararProcesoInterfazImprenta, Parameters do begin
                Refresh;
                ParamByName('@ComprobanteInicial').Value := neCOmprobanteInicial.ValueInt;
                ParamByName('@ComprobanteFinal').Value := neCOmprobanteFinal.ValueInt;
                ParamByName('@ForzarReimpresion').Value := IIF(FForzarReimpresion = True, 1 , 0); //Forzar Reimpresion
                ParamByName('@ImprimirElectronicos').Value := IIF(FImprimirElectronicos = True, 1 , 0); //Imprimir Electronicos
                ParamByName('@CodigoOperacion').Value := IIF(FReproceso = True, FNumeroProcesoAnterior, NULL); //Reproceso
                ParamByName('@NumeroProcesoFacturacion').Value := IIF(FPorFacturacion = True, FNumeroProceso, NULL); //Proceso Facturacion
                ParamByName('@SoloNK').Value := 1; //Simple
                CommandTimeOut := 50000;
                ExecProc;
            end;
            EscribeLog('      EJECUTO spPrepararProcesoInterfazImprenta');

            //informo cuantos comprobantes hay para imprimir
            //Cantidad := ObtenerCantidadComprobantesAImprimir;
            //labelProgreso.Caption := FORMAT(STR_PROCESS_INVOICES,[cantidad]);
            // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------
            CantidadBol := ObtenerCantidadComprobantesAImprimirBOL;
            CantidadFac := ObtenerCantidadComprobantesAImprimirFAC;


            FComprobantesExitososFAC := CantidadFac;
            FComprobantesExitososBOL := CantidadBol;

            labelProgreso.Caption := FORMAT(STR_PROCESS_INVOICES,[CantidadBOL + CantidadFAC]);
            labelProgreso.Update;

            pbProgreso.Min:=0;
            pbProgreso.Max:=CantidadBOL + CantidadFAC;
            pbProgreso.Position:=0;

            self.Repaint;

            //Verifico si hay datos para generar el archivo
            FHayDatosArchivoSimple := True;
            //if Cantidad = 0 then begin
            // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------
            if (CantidadBOL + CantidadFAC) = 0 then begin
                FHayDatosArchivoSimple := False; //No hay datos para generar el archivo
                Result := True;
                Exit;
            end;

            //Mientras Halla lineas para procesar
            //while (Cantidad > 0) and (not FCancel) do begin
            // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------
            while ((CantidadBOL + CantidadFAC) > 0) and (not FCancel) do begin

                // Genero tabla temporal con los comprobantes a enviar
                // a la imprenta en formato jordan de a bloques
                EscribeLog( 'BEGIN spObtenerLineasInterfazImprenta');
                with spObtenerLineasInterfazImprenta, Parameters do begin
                    if (State <> dsInactive) then  Close;
                    Refresh;
                    ParamByName('@Img').Value := FNombreImagenCatalogo;
                    ParamByName('@FechaProceso').Value := FFechaProceso;
                    ParamByName('@SoloNK').Value := 1; //Simple
                    //ParamByName('@TotalComprobantes').Value := 0;
                    // Rev.4 / 18-03-2009 / Nelson Droguett Sierra.
                    ParamByName('@TotalComprobantesFAC').Value := 0;
                    ParamByName('@TotalComprobantesBOL').Value := 0;
                    CommandTimeOut := 50000;
                    ExecProc;
                end;
               EscribeLog( '      EJECUTO spObtenerLineasInterfazImprenta');

                //Actualizo la barra de progreso
                //FComprobantesExitosos := IIF(Cantidad > 5000, FComprobantesExitosos + 5000, FComprobantesExitosos + Cantidad);
                //pbProgreso.Position := FComprobantesExitosos;
               // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------
                //pbProgreso.Position := FComprobantesExitosos;

                Application.ProcessMessages;

                //Informo cuantos comprobantes quedan por procesar
                //Cantidad := ObtenerCantidadComprobantesAImprimir;
                //labelProgreso.Caption := FORMAT(STR_PROCESS_INVOICES,[cantidad]);
                // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------
                CantidadBol := ObtenerCantidadComprobantesAImprimirBOL;
                CantidadFac := ObtenerCantidadComprobantesAImprimirFAC;
                labelProgreso.Caption := FORMAT(STR_PROCESS_INVOICES,[CantidadFAC + CantidadBOL]);
                labelProgreso.Update;

                pbProgreso.Position:=pbProgreso.Max - (CantidadBol+CantidadFAC);
                self.Repaint;
            end;

            //Obtengo el total de comprobantes procesados
            with spObtenerLineasInterfazImprenta, Parameters do begin
                  //FTotalComprobantes := StrToInt64(IIF(VarToStr(ParamByName('@TotalComprobantes').Value) = '', 0, VarToStr(ParamByName('@TotalComprobantes').Value)));
                  // Rev.4 / 18-03-2009 / Nelson Droguett Sierra
                  FTotalComprobantesFAC := StrToInt64(IIF(VarToStr(ParamByName('@TotalComprobantesFAC').Value) = '', 0, VarToStr(ParamByName('@TotalComprobantesFAC').Value)));
                  FTotalComprobantesBOL := StrToInt64(IIF(VarToStr(ParamByName('@TotalComprobantesBOL').Value) = '', 0, VarToStr(ParamByName('@TotalComprobantesBOL').Value)));
            end;

            //Si cancelaron el proceso
            if FCancel then begin
                Exit;
            end;

            //Informo que Comienzo a crear los archivos
            labelProgreso.Caption := STR_COPYING_FILE;
            labelProgreso.Update;
            self.Repaint;


            //Genero el archivo con los comprobantes
            EscribeLog( 'BEGIN Generar el Archivo con los comprobantes');
            if not GenerarArchivoGrilla(FNombreArchivoSimpleFAC,FNombreArchivoSimpleBOL) then begin
                Exit;
            end;
            EscribeLog( 'END Generar el Archivo con los comprobantes');

            //Genero el archivo de control
            EscribeLog( 'BEGIN Generar el Archivo de Control');
            if not EscribirArchivoControl(FNombreArchivoControlSimpleFAC,FNombreArchivoControlSimpleBOL) then begin
                Exit;
            end;
            EscribeLog( 'BEGIN Generar el Archivo de Control');


            //Marco los comprobantes como enviados a imprenta
            EscribeLog( 'BEGIN Marcar los comprobantes como enviados a imprenta');
            if not ActualizarComprobantes then begin
                Exit;
            end;
            EscribeLog( 'END Marcar los comprobantes como enviados a imprenta');


            //Informo que el proceso finalizo con exito
            Result := True;

        except
            on E : Exception do begin
                //Registro la excepcion en el log de operaciones
                //ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacion, MSG_ERROR + ' ' + e.Message);
                CodigoErrorSQL				:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_SQL);
                AgregarEnErroresInterfaces( DMConnections.BaseCAC,FCodigoOperacion,CodigoErrorSQL,MSG_ERROR + ' '+E.Message);
                MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        qryEliminarTemporales.ExecSQL;
        FOnProcess := False;
        KeyPreview := False;
        pbProgreso.Position := pbProgreso.Max; //0
        pnlprogreso.Visible := False;
        bteImagen.Clear;
        lblDescri.Caption := '';
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarArchivoMultiple
  Author:    lgisuk
  Date Created: 20/12/2005
  Description: Genero el archivo con Notas de Cobro de mas de una Hoja
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TfrmImprenta.GenerarArchivoMultiple : Boolean;
resourcestring
    MSG_ERROR = 'Error';
const
    STR_INIT_PROCESS     = 'Iniciando proceso generaci�n Archivo Multiple...';
    STR_PROCESS_INVOICES = 'Quedan %d comprobantes por procesar ... ';
    STR_COPYING_FILE     = 'Copiando archivo resultado ... ';
var
    //Cantidad : int64;
    // Rev.4 / 18-03-2009 / Nelson Droguett Sierra
    CantidadFAC : int64;
    CantidadBOL : int64;
    CodigoErrorSQL:integer;
begin
    Result := False;
    //Inicializo variables
    Screen.Cursor := crHourGlass;
    FCancel := False;
    KeyPreview := True;
    //Inicializo la barra de progreso
    //FComprobantesExitosos := 0;
    //FCantidadRegistros  := 0;
    //FTotalComprobantes  := 0;
    // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------
    FComprobantesExitososFAC := 0;
    FComprobantesExitososBOL := 0;

    FCantidadRegistrosFAC  := 0;
    FCantidadRegistrosBOL  := 0;

    FTotalComprobantesFAC  := 0;
    FTotalComprobantesBOL  := 0;

    FCantidadTimbresFAC  := 0;
    FCantidadTimbresBOL  := 0;

    CantidadFAC := 0;
    CantidadBOL := 0;

    //--------------------------------------------------------------------------
    pnlProgreso.Visible := True;
    //Comienzo a Procesar
    FOnProcess := True;
    try
        try
            //Informo que inicio el proceso
            labelProgreso.Caption := STR_INIT_PROCESS;
            labelProgreso.Update;

            //Obtenego los comprobantes a enviar a imprenta
            EscribeLog( 'BEGIN spPrepararProcesoInterfazImprenta');
            with spPrepararProcesoInterfazImprenta, Parameters do begin
                Refresh;
                ParamByName('@ComprobanteInicial').Value := neCOmprobanteInicial.ValueInt;
                ParamByName('@ComprobanteFinal').Value := neCOmprobanteFinal.ValueInt;
                ParamByName('@ForzarReimpresion').Value := IIF(FForzarReimpresion = True, 1 , 0); //Forzar Reimpresion
                ParamByName('@ImprimirElectronicos').Value := IIF(FImprimirElectronicos = True, 1 , 0); //Imprimir Electronicos
                ParamByName('@CodigoOperacion').Value := IIF(FReproceso = True, FNumeroProcesoAnterior, NULL); //Reproceso
                ParamByName('@NumeroProcesoFacturacion').Value := IIF(FPorFacturacion = True, FNumeroProceso, NULL); //Proceso Facturacion
                ParamByName('@SoloNK').Value := 0; //Multiple
                CommandTimeOut := 50000;
                ExecProc;
            end;
            EscribeLog( '      EJECUTO spPrepararProcesoInterfazImprenta');

            //informo cuantos comprobantes hay para imprimir
            //Cantidad := ObtenerCantidadComprobantesAImprimir;
            //labelProgreso.Caption := FORMAT(STR_PROCESS_INVOICES,[cantidad]);
            // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------
            CantidadBol := ObtenerCantidadComprobantesAImprimirBOL;
            CantidadFac := ObtenerCantidadComprobantesAImprimirFAC;
            labelProgreso.Caption := FORMAT(STR_PROCESS_INVOICES,[CantidadFAC + CantidadBOL]);





            FComprobantesExitososFAC := CantidadFac;
            FComprobantesExitososBOL := CantidadBol;

            labelProgreso.Update;
            self.Repaint;

            //Verifico si hay datos para generar el archivo
            FHayDatosArchivoMultiple := True;
            //if Cantidad = 0 then begin
            // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------
            if (CantidadBOL + CantidadFAC) = 0 then begin
                FHayDatosArchivoMultiple := False; //No hay datos para generar el archivo
                Result := True;
                Exit;
            end;

            //Mientras Halla lineas para procesar
            //while (Cantidad > 0) and (not FCancel) do begin
            // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------
            while ((CantidadBOL + CantidadFAC) > 0) and (not FCancel) do begin
                // Genero tabla temporal con los comprobantes a enviar
                // a la imprenta en formato jordan de a bloques
                EscribeLog( 'BEGIN spObtenerLineasInterfazImprenta');

                with spObtenerLineasInterfazImprenta, Parameters do begin
                    if (State <> dsInactive) then  Close;
                    Refresh;
                    ParamByName('@Img').Value := FNombreImagenCatalogo;
                    ParamByName('@FechaProceso').Value := FFechaProceso;
                    ParamByName('@SoloNK').Value := 0; //Multiple
                    //ParamByName('@TotalComprobantes').Value := 0;
                    // Rev.4 / 18-03-2009 / Nelson Droguett Sierra.
                    ParamByName('@TotalComprobantesFAC').Value := 0;
                    ParamByName('@TotalComprobantesBOL').Value := 0;
                    CommandTimeOut := 50000;
                    ExecProc;
                end;
                EscribeLog( '      EJECUTO spObtenerLineasInterfazImprenta');




                //Actualizo la barra de progreso
                //FComprobantesExitosos := IIF(Cantidad > 5000, FComprobantesExitosos + 5000, FComprobantesExitosos + Cantidad);
                //pbProgreso.Position := FComprobantesExitosos;
                // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------

                Application.ProcessMessages;

                //Informo cuantos comprobantes quedan por procesar
                //Cantidad := ObtenerCantidadComprobantesAImprimir;
                //labelProgreso.Caption := FORMAT(STR_PROCESS_INVOICES,[cantidad]);
                // Rev.11 / 18-03-2009 / Nelson Droguett Sierra ----------------------------
                CantidadBol := ObtenerCantidadComprobantesAImprimirBOL;
                CantidadFac := ObtenerCantidadComprobantesAImprimirFAC;
                labelProgreso.Caption := FORMAT(STR_PROCESS_INVOICES,[CantidadFAC + CantidadBOL]);
                labelProgreso.Update;
                self.Repaint;

            end;

            //Obtengo el total de comprobantes procesados
            with spObtenerLineasInterfazImprenta, Parameters do begin
                  //FTotalComprobantes := StrToInt64(IIF(VarToStr(ParamByName('@TotalComprobantes').Value) = '', 0, VarToStr(ParamByName('@TotalComprobantes').Value)));
                  // Rev.4 / 18-03-2009 / Nelson Droguett Sierra
                  FTotalComprobantesFAC := StrToInt64(IIF(VarToStr(ParamByName('@TotalComprobantesFAC').Value) = '', 0, VarToStr(ParamByName('@TotalComprobantesFAC').Value)));
                  FTotalComprobantesBOL := StrToInt64(IIF(VarToStr(ParamByName('@TotalComprobantesBOL').Value) = '', 0, VarToStr(ParamByName('@TotalComprobantesBOL').Value)));
            end;


            //Si cancelaron el proceso
            if FCancel then begin
                Exit;
            end;

            //Informo que Comienzo a crear los archivos
            labelProgreso.Caption := STR_COPYING_FILE;
            labelProgreso.Update;
            self.Repaint;

            //Marco los comprobantes en la tabla temporal como enviados a imprenta
            EscribeLog( 'BEGIN Actualizar Comprobantes');
            if not ActualizarComprobantes then begin
                Exit;
            end;
            EscribeLog( 'END Actualizar Comprobantes');

            //Genero el archivo con los comprobantes
            EscribeLog( 'BEGIN Generar Archivo comprobantes Multiples');
            if not GenerarArchivoGrilla(FNombreArchivoMultipleFAC,FNombreArchivoMultipleBOL) then begin
                Exit;
            end;
            EscribeLog( 'END Generar Archivo comprobantes Multiples');

            //Genero el archivo de control
            EscribeLog( 'BEGIN Escribir archivo de control');
            if not EscribirArchivoControl(FNombreArchivoControlMultipleFAC,FNombreArchivoControlMultipleBOL) then begin
                Exit;
            end;
            EscribeLog( 'END Escribir archivo de control');

            //Informo que el proceso finalizo con exito
            Result := True;

        except
            on E : Exception do begin
                //Registro la excepcion en el log de operaciones
                CodigoErrorSQL				:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_SQL);
                if AgregarEnErroresInterfaces( DMConnections.BaseCAC,FCodigoOperacion,CodigoErrorSQL,E.Message) then
                    MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        qryEliminarTemporales.ExecSQL;
        FOnProcess := False;
        KeyPreview := False;
        pbProgreso.Position := pbProgreso.Max; //0
        pnlprogreso.Visible := False;
        bteImagen.Clear;
        lblDescri.Caption := '';
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnFinalizarClick
  Author:    gcasais
  Date Created: 21/12/2004
  Description: Ejecuci�n del proceso
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.btnFinalizarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerMensajeFinalizacionDeProceso
      Author:    lgisuk
      Date Created: 23/01/2006
      Description: Obtengo el mensaje para mostrar al finalizar el proceso
      Parameters: None
      Return Value: None
    -----------------------------------------------------------------------------}
    Function ObtenerMensajeFinalizacionDeProceso : String;
    Resourcestring
        MSG_PROCESS_FINISHED  = 'El proceso finaliz� correctamente!';
    Const
        STR_SIMPLE_FILE = 'Archivo Simple : ';
        STR_MULTIPLE_FILE = 'Archivo M�ltiple : ';
        STR_GENERATED =  'Generado.';
        STR_NOT_GENERATED = 'No Generado.';
        STR_THERE_ARE_NO_SIMPLE_COLLECTION_NOTES_TO_SEND_IN_THIS_PROCESES = 'No hay notas de cobro simples' + CRLF + 'para enviar en este proceso.';
        STR_THERE_ARE_NO_COLLECTION_NOTES_OF_MULTIPLE_PAGES_TO_SEND_IN_THIS_PROCESES = 'No hay notas de cobro de m�ltiples p�ginas' + CRLF + 'para enviar en este proceso.';
    begin
        Result := MSG_PROCESS_FINISHED + CRLF +
                  CRLF +
                  STR_SIMPLE_FILE + IIF(FHayDatosArchivoSimple = True, STR_GENERATED, STR_NOT_GENERATED + CRLF + STR_THERE_ARE_NO_SIMPLE_COLLECTION_NOTES_TO_SEND_IN_THIS_PROCESES + CRLF) + CRLF +
                  STR_MULTIPLE_FILE + IIF(FHayDatosArchivoMultiple = True, STR_GENERATED, STR_NOT_GENERATED + CRLF + STR_THERE_ARE_NO_COLLECTION_NOTES_OF_MULTIPLE_PAGES_TO_SEND_IN_THIS_PROCESES) + CRLF;
    end;

resourcestring
    MSG_ERROR_CANNOT_CREATEFILE = 'Error al crear el archivo';
    MSG_ERROR = 'Error';
    //
    MSG_PROCESS_CANCELLED = 'Proceso cancelado';
var
    Error: String;
    FileAlreadyExist: boolean;
begin

    chbLogSucesos.Checked := installini.ReadString('LOGJORDAN','REGISTRARLOG','SI')='SI';

    EscribeLog('Inicio del Proceso');
    //Intentamos crear el archivo
    FileAlreadyExist := False;
    //intento crear el archivo
    //if not CrearArchivo(FNombreArchivoSimple, Error, FileAlreadyExist) then begin
        // si devolvio False porque el archivo ya existia y el usuario dijo que NO a reescribirlo
    //    if FileAlreadyExist then begin
    //        Exit; // Salimos sin mostrar ning�n mensaje
    //    end;
        // mostramos el mensaje de error
    //    MsgBoxErr(MSG_ERROR, Error, MSG_ERROR_CANNOT_CREATEFILE, MB_ICONERROR);
        // y salimos
    //    Exit;
    //end;



  // Rev.6 / 02-04-2009 / Nelson Droguett Sierra.
    EscribeLog('BEGIN Validacion de los Archivos');
    if not ( EscribeLog( 'BEGIN Valida Archivo de Factura Simple') and
             CrearArchivo(FNombreArchivoSimpleFAC, Error, FileAlreadyExist) and
             EscribeLog( 'END Valida Archivo de Factura Simple')) then begin
        if FileAlreadyExist then begin
            Exit; // Salimos sin mostrar ning�n mensaje
        end;
        MsgBoxErr(MSG_ERROR, Error, MSG_ERROR_CANNOT_CREATEFILE, MB_ICONERROR);
        Exit;
    end
    else if not ( EscribeLog( 'BEGIN Valida Archivo de Factura Multiple') and
                  CrearArchivo(FNombreArchivoMultipleFAC, Error, FileAlreadyExist) and
                  EscribeLog( 'END Valida Archivo de Factura Multiple')
                )   then begin
        if FileAlreadyExist then begin
            Exit; // Salimos sin mostrar ning�n mensaje
        end;
        MsgBoxErr(MSG_ERROR, Error, MSG_ERROR_CANNOT_CREATEFILE, MB_ICONERROR);
        Exit;
    end
    else if not ( EscribeLog( 'BEGIN Valida Archivo de Boleta Simple') and
                  CrearArchivo(FNombreArchivoSimpleBOL, Error, FileAlreadyExist) and
                  EscribeLog( 'END Valida Archivo de Boleta Simple')
                ) then begin
        if FileAlreadyExist then begin
            Exit; // Salimos sin mostrar ning�n mensaje
        end;
        MsgBoxErr(MSG_ERROR, Error, MSG_ERROR_CANNOT_CREATEFILE, MB_ICONERROR);
        Exit;
    end
    else if not ( EscribeLog( 'BEGIN Valida Archivo de Boleta Multiple') and
                  CrearArchivo(FNombreArchivoMultipleBOL, Error, FileAlreadyExist) and
                  EscribeLog( 'END Valida Archivo de Boleta Multiple')
                )   then begin
        if FileAlreadyExist then begin
            Exit; // Salimos sin mostrar ning�n mensaje
        end;
        MsgBoxErr(MSG_ERROR, Error, MSG_ERROR_CANNOT_CREATEFILE, MB_ICONERROR);
        Exit;
    end ;
    EscribeLog('END Validacion de los Archivos');
    // ------------------------------------------------------------------------------------
    //Inicializo
    pbProgreso.Step     := 1;
    pbProgreso.Min      := 0;
    pbprogreso.Max      := neComprobanteFinal.ValueInt - neComprobanteInicial.ValueInt;
    pbProgreso.Smooth   := True;
    pbProgreso.Position := 0;


    //Deshabilito los botones
    btnAnterior.Enabled := False;
    btnFinalizar.Enabled := False;
    btnSalir.Enabled    := False;
    HabilitarControles(False);

    //Obtener la fecha del proceso
    FFechaProceso := NowBase(DMConnections.BaseCAC);

    // Facturacion Electronica
    // Rev.5 / 19-03-2009 / Nelson Droguett Sierra
    AConfig_EGATE_HOME_Parametrico.Execute;
    //ACargarDLL.Execute;
    //--------------------------------------------------------




    //Registro la Operacion en Log
    EscribeLog('BEGIN Proceso General');
    if ( EscribeLog( 'BEGIN Registro de Operacion') and
         RegistrarOperacion and
         EscribeLog( 'END Registro de Operacion')
       )
        //Genero el archivo con Notas de Cobro de una Hoja
        and ( EscribeLog( 'BEGIN Generacion del Archivo Simple ') and
              GenerarArchivoSimple and
              EscribeLog( 'END Generacion del Archivo Simple')
            )
          //Genero el archivo con Notas de Cobro de mas de una Hoja
          and ( EscribeLog( 'BEGIN Generacion del Archivo Multiple') and
                GenerarArchivoMultiple and
                EscribeLog( 'END Generacion del Archivo Multiple')
              )
              //Marco los comprobantes como enviados a imprenta
              and ( EscribeLog( 'BEGIN Actualizacion de Comprobantes Enviados') and
                    ActualizarComprobantesEnviados and
                    EscribeLog( 'END Actualizacion de Comprobantes Enviados')
                 ) then begin

                //Marco en el log la fecha y hora de finalizacion
                if FCancel = False then begin
                    EscribeLog( 'BEGIN Cierre de la Operacion');
                    CerrarOperacion;
                    EscribeLog( 'END Cierre de la Operacion');
                end;

                //Informo que el proceso finalizo con exito
                MsgBox(ObtenerMensajeFinalizacionDeProceso);

    end else begin

        //si el proceso fue cancelado
        if FCancel then begin

            //Registro que el proceso fue cancelado
            RegistrarCancelacionInterface(DMConnections.BaseCAC, FCodigoOperacion,Error);

            //informo que el proceso fuen cancelado
            labelProgreso.Caption := MSG_PROCESS_CANCELLED;
            //Muestro cartel informando que el proceso fue cancelado
            MsgBox(MSG_PROCESS_CANCELLED);
        end;

       //Elimino los archivos creados
       //DeleteFile(FNombreArchivoControlSimple);
       //DeleteFile(FNombreArchivoSimple);
       //DeleteFile(FNombreArchivoControlMultiple);
       //DeleteFile(FNombreArchivoMultiple);

       // Rev.4 / 11-03-2009 / Nelson Droguett Sierra
       DeleteFile(FNombreArchivoControlSimpleFAC);
       DeleteFile(FNombreArchivoSimpleFAC);
       DeleteFile(FNombreArchivoControlMultipleFAC);
       DeleteFile(FNombreArchivoMultipleFAC);

       DeleteFile(FNombreArchivoControlSimpleBOL);
       DeleteFile(FNombreArchivoSimpleBOL);
       DeleteFile(FNombreArchivoControlMultipleBOL);
       DeleteFile(FNombreArchivoMultipleBOL);
       //-----------------------------------------------------------------------


    end;


    EscribeLog('BEGIN Liberacion de la DLL');
    // Rev.5 / 19-03-2009 / Nelson Droguett Sierra
    //ALiberarDLL.Execute;
    //--------------------------------------------------------
    EscribeLog('END Liberacion de la DLL');



    //Borro los mensajes
    lblRangoSeleccionado.Caption := '';
    lblForzandoReimpresion.Caption := '';
    lblImprimiendoElectronicos.Caption := '';
    //Habilito botones
    btnAnterior.Enabled := True;
    btnSalir.Enabled := True;
    HabilitarControles(True);
    //Vuelvo a la primea pagina para iniciar un nuevo proceso
    nb.PageIndex := nb.PageIndex - 1;
    if chbLogSucesos.Checked then
       FLogBuffer.SaveToFile(GoodDir(InstallIni.ReadString('LOGJORDAN','CARPETALOG','C:'))+'Log Jordan '+DateToStr(Now)+'.log');
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    gcasais
  Date Created: 23/12/2004
  Description: Permito cancelar la operacion
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE : FCancel := True;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 06/12/2005
  Description: Permito salir del formulario
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 06/12/2005
  Description: Permito salir si no esta procesando
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando.
  	CanClose := not FOnProcess;
end;


// Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
procedure TfrmImprenta.FormCreate(Sender: TObject);
var
  CarpetaDBNet:String;
begin
    ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_JORDAN_TIMBRE_ELECTRONICO', CarpetaDBNet);
    Deltree(ExpandFileName(GoodDir(CarpetaDBNet)+'..'));
end;

// Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
procedure TfrmImprenta.FormDestroy(Sender: TObject);
var
  CarpetaDBNet:String;
begin
    ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_JORDAN_TIMBRE_ELECTRONICO', CarpetaDBNet);
    Deltree(ExpandFileName(GoodDir(CarpetaDBNet)+'..'));
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    gcasais
  Date Created: 27/12/2004
  Description: Libero el form de memoria
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprenta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

// Rev.5 / 22-03-2009 / Nelson Droguett Sierra
// Carga la DLL
procedure TfrmImprenta.ACargarDLLExecute(Sender: TObject);
var
	Cod : integer;
begin
    EscribeLog( 'Carga de la DLL En Memoria');
    Cod := CargarDLL();
    //EscribeLog( 'END Carga de la DLL En Memoria');
    //if Cod = 0 then mmoLog.Lines.Add('DLL OK')
    //else LogueaError();
end;


// Rev.5 / 22-03-2009 / Nelson Droguett Sierra
// Configurar la variable de entorno en forma manual (directorio local)
procedure TfrmImprenta.AConfig_EGATE_HOME_ManualExecute(Sender: TObject);
const
	ValorPrueba = 'D:\SUITE\';
    //ValorPrueba = '\\pino\op_test\Aplicaciones\DBNET\suite\';
var
    Valor : string;
begin
    Valor := GetEnvironmentVariable('EGATE_HOME');
    SetEnvironmentVariable(PChar('EGATE_HOME'), PChar(ValorPrueba));
    Valor := GetEnvironmentVariable('EGATE_HOME');
    if ValorPrueba = Valor then PARAM_DIR_EGATE_HOME := Valor;
end;



// Rev.5 / 22-03-2009 / Nelson Droguett Sierra
// Configurar la variable de entorno en forma parametrica.
procedure TfrmImprenta.AConfig_EGATE_HOME_ParametricoExecute(Sender: TObject);
var
    Mensaje : string;
begin
	//if not DMConnections.BaseCAC.Connected then ConectarBase();

    //if ConfigurarVariable_EGATE_HOME(Mensaje) then
    //  mmoLog.Lines.Add('EGATE_HOME Exitosa: ' + PARAM_DIR_EGATE_HOME)
    //else mmoLog.Lines.Add('EGATE_HOME Error: ' + Mensaje);
    ConfigurarVariable_EGATE_HOME(DMConnections.BaseCAC, False, Mensaje, True);
end;


// Rev.5 / 22-03-2009 / Nelson Droguett Sierra
// Liberar la DLL de memoria.
procedure TfrmImprenta.ALiberarDLLExecute(Sender: TObject);
begin
  EscribeLog( 'Liberar la DLL de Memoria');
  LiberarDLL();
  //EscribeLog( 'END Liberar la DLL de Memoria');
    //if LiberarDLL() then mmoLog.Lines.Add('DLL Liberada OK')
    //else mmoLog.Lines.Add('Error al Liberar DLL');
end;





{
PageFaultCount - the number of page faults.
PeakWorkingSetSize - the peak working set size, in bytes.
WorkingSetSize - the current working set size, in bytes.
QuotaPeakPagedPoolUsage - The peak paged pool usage, in bytes.
QuotaPagedPoolUsage - The current paged pool usage, in bytes.
QuotaPeakNonPagedPoolUsage - The peak nonpaged pool usage, in bytes.
QuotaNonPagedPoolUsage - The current nonpaged pool usage, in bytes.
PagefileUsage - The current space allocated for the pagefile, in bytes. Those pages may or may not be in memory.
PeakPagefileUsage - The peak space allocated for the pagefile, in bytes.
}

// Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
function TfrmImprenta.CurrentMemoryUsage: Cardinal;
var
  pmc: TProcessMemoryCounters;
begin
  pmc.cb := SizeOf(pmc) ;
  if GetProcessMemoryInfo(GetCurrentProcess, @pmc, SizeOf(pmc)) then
    Result := pmc.WorkingSetSize
  else
    RaiseLastOSError;
end;

// Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
function TfrmImprenta.DelTree(DirName: string): Boolean;
var
  SHFileOpStruct : TSHFileOpStruct;
  DirBuf : array [0..255] of char;
begin
  try
   Fillchar(SHFileOpStruct,Sizeof(SHFileOpStruct),0) ;
   FillChar(DirBuf, Sizeof(DirBuf), 0 ) ;
   StrPCopy(DirBuf, DirName) ;
   with SHFileOpStruct do begin
    Wnd := 0;
    pFrom := @DirBuf;
    wFunc := FO_DELETE;
    fFlags := FOF_ALLOWUNDO;
    fFlags := fFlags or FOF_NOCONFIRMATION;
    fFlags := fFlags or FOF_SILENT;
   end;
    Result := (SHFileOperation(SHFileOpStruct) = 0) ;
   except
    Result := False;
  end;
end;

end.
