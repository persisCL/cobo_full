{-------------------------------------------------------------------------------
 File Name: FrmRptRecepcionNovedades.pas
 Author:    Lgisuk
 Date Created: 23/06/2005
 Language: ES-AR
 Description: Módulo de la interface Falabella -
                    Reporte de Recepción de Novedades



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
Unit FrmRptRecepcionNovedades;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  Utildb,                    //querygetvalue
  UtilProc,                  //Mensajes
  //general
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup, ppStrtch, ppSubRpt, ConstParametrosGenerales;                        //SS_1147_NDR_20140710

type
  TFRptRecepcionNovedades = class(TForm)
    rptRecepcionNovedades: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppMaxFechaEmision: TppLabel;
    ppLabel1: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    ppParameterList1: TppParameterList;
	RBIListado: TRBInterface;
    SpObtenerReporteRecepcionNovedadesCMR: TADOStoredProc;
    ppLine5: TppLine;
    Ltusuario: TppLabel;
    LtFechaHora: TppLabel;
    ppusuario: TppLabel;
    ppfechahora: TppLabel;
    ppNumeroProceso: TppLabel;
    ppIdentificacionArchivoGenerado: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppCantidadTotalRegistrosRecibidos: TppLabel;
    ppCantidadTotalMandatosAprobados: TppLabel;
    ppCantidadTotalMandatosRechazados: TppLabel;
    ppCantidadDeRegistros: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel7: TppLabel;
    ppDiferenciaCantidad: TppLabel;
    ppLine3: TppLine;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppCantidadTotalBajasRechazadas: TppLabel;
    ppCantidadTotalAltasRechazadas: TppLabel;
    ppCantidadTotalBajasAprobadas: TppLabel;
    ppCantidadTotalAltasAprobadas: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppRegistrosDiferencia: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppRegistrosRecibidos: TppLabel;
    ppRegistrosEnviados: TppLabel;
    ppLabel27: TppLabel;
    ppAltasDiferencia: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppAltasRecibidas: TppLabel;
    ppAltasEnviadas: TppLabel;
    ppLabel33: TppLabel;
    ppBajasDiferencia: TppLabel;
    ppLabel35: TppLabel;
    ppLabel36: TppLabel;
    ppBajasRecibidas: TppLabel;
    ppBajasEnviadas: TppLabel;
    ppLine4: TppLine;
    SPObtenerMandatosEnviadosNoRecibidosCMR: TADOStoredProc;
    SPObtenerMandatosRecibidosNoEnviadosCMR: TADOStoredProc;
    ppObtenerMandatosEnviadosNoRecibidosCMR: TppDBPipeline;
    ppObtenerMandatosRecibidosNoEnviadosCMR: TppDBPipeline;
    dsObtenerMandatosRecibidosNoEnviadosCMR: TDataSource;
    dsObtenerMandatosEnviadosNoRecibidosCMR: TDataSource;
    ppSubReportEnviadoNoRecibido: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppLabel22: TppLabel;
    ppLabel25: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppLabel26: TppLabel;
    ppLabel28: TppLabel;
    ppSubReportRecibidoNoEnviado: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailBand3: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    ppLabel31: TppLabel;
    ppLabel32: TppLabel;
    ppLabel34: TppLabel;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppLabel37: TppLabel;
    ppDBText5: TppDBText;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    SPObtenerErroresInterfase: TADOStoredProc;
    DSObtenerErroresInterfase: TDataSource;
    ppObtenerErroresInterfase: TppDBPipeline;
    ppSubReportErroresInterface: TppSubReport;
    ppChildReport3: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppDetailBand4: TppDetailBand;
    ppSummaryBand4: TppSummaryBand;
    ppLabel38: TppLabel;
    ppDBText6: TppDBText;
    ppLabel39: TppLabel;
    ppImage1: TppImage;
    ppLabel40: TppLabel;
    ppImage3: TppImage;
    ppLabel41: TppLabel;
    ppImage4: TppImage;
    ppLabel42: TppLabel;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FCodigoOperacionInterfase:Integer;
    FNombreReporte:string;
    FCantidadRegistros:string;
    FCantidadRegistrosReal:string;
    { Private declarations }
  public
    Function Inicializar(CodigoOperacionInterfase:Integer; NombreReporte, CantidadRegistros, CantidadRegistrosReal: AnsiString): Boolean;
    { Public declarations }
  end;

var
  FRptRecepcionNovedades: TFRptRecepcionNovedades;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 23/06/2005
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRptRecepcionNovedades.Inicializar(CodigoOperacionInterfase:Integer; NombreReporte, CantidadRegistros, CantidadRegistrosReal: AnsiString): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
      ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
      try                                                                                                 //SS_1147_NDR_20140710
        ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
      except                                                                                              //SS_1147_NDR_20140710
        On E: Exception do begin                                                                          //SS_1147_NDR_20140710
          Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
          MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
          Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
        end;                                                                                              //SS_1147_NDR_20140710
      end;                                                                                                //SS_1147_NDR_20140710

    FCodigoOperacionInterfase:= CodigoOperacionInterfase;
    FNombreReporte:= NombreReporte;
    FCantidadRegistros:= CantidadRegistros;
    FCantidadRegistrosReal:= CantidadRegistrosReal;
    Result := RBIListado.Execute;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 23/06/2005
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
Procedure TFRptRecepcionNovedades.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
Resourcestring
    MSG_ERROR_AL_GENERAR_REPORTE = 'Error al generar el reporte';
    MSG_ERROR = 'Error';
Const
    REPORT = 'Reporte';

var
    CantidadTotalRegistrosRecibidos: string;
    DiferenciaCantidadCuentas: Real;
begin
    try
        //Encabezado, Control, Archivo y Validación
        with SpObtenerReporteRecepcionNovedadesCMR do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').value:= FCodigoOperacionInterfase;
            Parameters.ParamByName('@FechaProcesamiento').value:= Now;
            Parameters.ParamByName('@Usuario').value:= '';
            Parameters.ParamByName('@NombreArchivo').value:= '';
            Parameters.ParamByName('@CantidadTotalRegistrosRecibidos').value:= 0;
            Parameters.ParamByName('@CantidadTotalAltasRecibidas').value:= 0;
            Parameters.ParamByName('@CantidadTotalBajasRecibidas').value:= 0;
            Parameters.ParamByName('@CantidadTotalAltasAprobadas').value:= 0;
            Parameters.ParamByName('@CantidadTotalBajasAprobadas').value:= 0;
            Parameters.ParamByName('@CantidadTotalMandatosAprobados').value:= 0;
            Parameters.ParamByName('@CantidadTotalAltasRechazadas').value:= 0;
            Parameters.ParamByName('@CantidadTotalBajasRechazadas').value:= 0;
            Parameters.ParamByName('@CantidadTotalMandatosRechazados').value:= 0;
            Parameters.ParamByName('@CantidadTotalRegistrosEnviados').value:= 0;
            Parameters.ParamByName('@CantidadTotalAltasEnviadas').value:= 0;
            Parameters.ParamByName('@CantidadTotalBajasEnviadas').value:= 0;
            Parameters.ParamByName('@RegistrosDiferencia').value:= 0;
            Parameters.ParamByName('@AltasDiferencia').value:= 0;
            Parameters.ParamByName('@BajasDiferencia').value:= 0;
            ExecProc;
        end;
    except
        on e: Exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
        end;
    end;

    //Detalle de Mandatos Enviados no Recibidos
    try
        with SpObtenerMandatosEnviadosNoRecibidosCMR do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').value:= FCodigoOperacionInterfase;
            Open;
        end;
    except
        on e: Exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
        end;
    end;

    //Detalle de Mandatos Recibidos no enviados
    try
        with SpObtenerMandatosRecibidosNoEnviadosCMR do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').value:= FCodigoOperacionInterfase;
            Open;
        end;
    except
        on e: Exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
        end;
    end;

    //Detalle de Errores de Validación
    try
        //Obtengo los errores que registro la interfaz
        with SPObtenerErroresInterfase do begin
            close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').value:=FCodigoOperacionInterfase;
            open;
        end;
      except
        on e: Exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
        end;
    end;

    try
        //Asigno los valores a los campos del reporte
        with SpObtenerReporteRecepcionNovedadesCMR do begin
            //Encabezado
            ppFechaHora.Caption                        := DateTimeToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
            ppUsuario.Caption                          := Parameters.ParamByName('@Usuario').Value;
            ppNumeroProceso.Caption                    := InttoStr(FCodigoOperacionInterfase);
            ppIdentificacionArchivoGenerado.Caption    := ExtractFileName(Parameters.ParamByName('@NombreArchivo').Value);
            //Datos del Registro de Control
            ppCantidaddeRegistros.Caption              := FCantidadRegistros;
            CantidadTotalRegistrosRecibidos            := FCantidadRegistrosReal;
            ppCantidadTotalRegistrosRecibidos.Caption  := CantidadTotalRegistrosRecibidos;
            //obtengo la diferencia de cantidad y sumatoria
            DiferenciaCantidadCuentas                  := strtofloat(FCantidadRegistros) - strtofloat(CantidadTotalRegistrosRecibidos);
            ppdiferenciacantidad.Caption               := floattostr(DiferenciaCantidadCuentas);
            //Datos del Archivo
            ppCantidadTotalAltasAprobadas.Caption      := Parameters.ParamByName('@CantidadTotalAltasAprobadas').Value;
            ppCantidadTotalBajasAprobadas.Caption      := Parameters.ParamByName('@CantidadTotalBajasAprobadas').Value;
            ppCantidadTotalMandatosAprobados.Caption   := Parameters.ParamByName('@CantidadTotalMandatosAprobados').Value;
            ppCantidadTotalAltasRechazadas.Caption     := Parameters.ParamByName('@CantidadTotalAltasRechazadas').Value;
            ppCantidadTotalBajasRechazadas.Caption     := Parameters.ParamByName('@CantidadTotalBajasRechazadas').Value;
            ppCantidadTotalMandatosRechazados.Caption  := Parameters.ParamByName('@CantidadTotalMandatosRechazados').Value;
            //Resultado de la Validación
            ppRegistrosEnviados.Caption                := Parameters.ParamByName('@CantidadTotalRegistrosEnviados').Value;
            ppRegistrosRecibidos.Caption               := CantidadTotalRegistrosRecibidos;
            ppRegistrosDiferencia.Caption              := Parameters.ParamByName('@RegistrosDiferencia').value;
            ppAltasEnviadas.Caption                    := Parameters.ParamByName('@CantidadTotalAltasEnviadas').Value;
            ppAltasRecibidas.Caption                   := Parameters.ParamByName('@CantidadTotalAltasRecibidas').Value;
            ppAltasDiferencia.Caption                  := Parameters.ParamByName('@AltasDiferencia').value;
            ppBajasEnviadas.Caption                    := Parameters.ParamByName('@CantidadTotalBajasEnviadas').Value;
            ppBajasRecibidas.Caption                   := Parameters.ParamByName('@CantidadTotalBajasRecibidas').Value;
            ppBajasDiferencia.Caption                  := Parameters.ParamByName('@BajasDiferencia').value;
        end;
        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FnombreReporte;
    except
        on E: exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.
