{-----------------------------------------------------------------------------
 File Name: frmEnvioRespuestasCaptaciones.pas
 Author:    FLamas
 Date Created: 21/06/2005
 Language: ES-AR
 Description: Modulo de la interfaz Falabella - Generaci�n Archivo de Respuestas a Captaciones
-----------------------------------------------------------------------------}
unit frmEnvioRespuestasCaptaciones;

interface

uses
  //Envio de Respuesta a Captaciones
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  PeaTypes,
  UtilDB,
  ComunesInterfaces,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, BuscaTab, DmiCtrls, DPSControls, VariantComboBox, StrUtils;

type

  TFEnvioRespuestasCaptaciones = class(TForm)
  	Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    edDestino: TEdit;
    OpenDialog: TOpenDialog;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    spObtenerRespuestasCaptacionesCMR: TADOStoredProc;
    spActualizarRespuestasCaptaciones: TADOStoredProc;
    lblFechaInterface: TLabel;
    spObtenerInterfasesEjecutadas: TADOStoredProc;
    edInterfazEntrante: TBuscaTabEdit;
    buscaInterfaces: TBuscaTabla;
    spObtenerInterfasesEjecutadasFecha: TDateTimeField;
    spObtenerInterfasesEjecutadasDescripcion: TStringField;
    spObtenerInterfasesEjecutadasCodigoOperacionInterfase: TIntegerField;
    spObtenerInterfasesEjecutadasNombreArchivo: TStringField;
    spObtenerInterfasesEjecutadasUsuario: TStringField;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    function  buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);
    procedure edInterfazEntranteChange(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  	FDetenerImportacion: Boolean;
    FRespuestasTXT	: TStringList;
    FCodigoOperacion : Integer;
    FErrorMsg : String;
  	FCodigoOperacionEntrada : Integer;
  	FCMR_Directorio_Resp_Captaciones : String;
    FRespuestasOK : Integer;
    FWarnings : Integer;
    FErrores : Integer;
  	Function  RegistrarOperacion : Boolean;
  	Function  GenerarRespuestasCMR : Boolean;
    Procedure ActualizarRespuestasCMR;
  public
  	Function  Inicializar(txtCaption : ANSIString; MDIChild : Boolean) : Boolean;
	{ Public declarations }
  end;

var
  FEnvioRespuestasCaptaciones : TFEnvioRespuestasCaptaciones;

Const
  RO_MOD_INTERFAZ_SALIENTE_CAPTACIONES_CMR = 51;
  RO_MOD_INTERFAZ_ENTRANTE_CAPTACIONES_CMR = 50; //la utiliza para saber lo enviado no recibido

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    FLamas
  Date Created: 21/06/2005
  Description:
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFEnvioRespuestasCaptaciones.Inicializar(txtCaption : ANSIString; MDIChild : Boolean) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 20/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        CMR_DIRECTORIO_RESP_CAPTACIONES = 'CMR_Directorio_Resp_Captaciones';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_DIRECTORIO_RESP_CAPTACIONES , FCMR_Directorio_Resp_Captaciones) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_DIRECTORIO_RESP_CAPTACIONES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FCMR_Directorio_Resp_Captaciones := GoodDir(FCMR_Directorio_Resp_Captaciones);
                if  not DirectoryExists(FCMR_Directorio_Resp_Captaciones) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FCMR_Directorio_Resp_Captaciones;
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
	  MSG_ERROR = 'Error';
	  MSG_INVALID_DESTINATION_DIRECTORY = 'El directorio de env�os es inv�lido';
begin
    Result := False;

    if not MDIChild then begin
        FormStyle := fsNormal;
        Visible := False;
    end;

    CenterForm(Self);
    Caption := AnsiReplaceStr(txtCaption, '&', '');

    try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and
                            VerificarParametrosGenerales;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;

    spObtenerInterfasesEjecutadas.Parameters.ParamByName( '@CodigoModulo' ).Value := RO_MOD_INTERFAZ_ENTRANTE_CAPTACIONES_CMR;
    spObtenerInterfasesEjecutadas.CommandTimeout := 500;
    spObtenerInterfasesEjecutadas.Open;

    btnCancelar.Enabled := False;
    pnlAvance.Visible := False;
    lblReferencia.Caption := '';

    Result := True;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFEnvioRespuestasCaptaciones.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_CAPTACIONES        = ' ' + CRLF +
                             'El Archivo de Respuesta a Captaciones ' + CRLF +
                             'es la respuesta que envia el ESTABLECIMIENTO a FALABELLA' + CRLF +
                             'por cada archivo de Captaciones recibido' + CRLF +
                             'y tiene por objeto dar a conocer a FALABELLA' + CRLF +
                             'el resultado del procesamiento de la informaci�n enviada' + CRLF +
                             ' ' + CRLF +
                             'Nombre del Archivo: 04_CAPTACIONCMRR_DDMMAA.SS' + CRLF +
                             ' '; 
begin
    //si esta procesando sale
    if PnlAvance.visible = True then Exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_CAPTACIONES, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TFEnvioRespuestasCaptaciones.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.Visible = True then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: edFechaChange
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: En caso de Reprocesar esta interfaz
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioRespuestasCaptaciones.edInterfazEntranteChange(Sender: TObject);
begin
    BtnProcesar.Enabled := ( edInterfazEntrante.Text <> '' );
end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfasesProcess
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: En caso de Reprocesar esta interfaz
  Parameters: Tabla: TDataSet; var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFEnvioRespuestasCaptaciones.buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
    Result := True;
	  with Tabla do
        Texto :=  FieldByName('Fecha').AsString +  ' - ' + FieldByName('NombreArchivo').AsString;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfacesSelect
  Author:    FLamas
  Date Created: 21/06/2005
  Description: 	Busca una Interfaz de Captaciones para Generar las Respuestas
  				a Dicha Interfaz
  Parameters: Sender: TObject; Tabla: TDataSet
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioRespuestasCaptaciones.buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);
begin
    EdInterfazEntrante.Text := FormatDateTime( 'dd-mm-yyyy', Tabla.FieldByName('Fecha').AsDateTime);
    FCodigoOperacionEntrada := Tabla.FieldByName('CodigoOperacionInterfase').Value;

    //Creo el nombre del archivo a enviar
    Eddestino.Text:= FCMR_Directorio_Resp_Captaciones + StringReplace( Tabla.FieldByName('NombreArchivo').Value, 'E', 'R', []);
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Registra la Operaci�n en el Log de Interfases
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFEnvioRespuestasCaptaciones.RegistrarOperacion : boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
    DescError : String;
begin
    Result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_SALIENTE_CAPTACIONES_CMR, ExtractFileName(edDestino.text), UsuarioSistema, '', False, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError );
    if not Result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarRespuestasCMR
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Genera el Archivo de Respuestas a las Captaciones
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFEnvioRespuestasCaptaciones.GenerarRespuestasCMR : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: ArmarLineaTXT
      Author:    FLamas
      Date Created: 21/06/2005
      Description: Arma la l�nea del TXT
      Parameters: None
      Return Value: string
    -----------------------------------------------------------------------------}
    Function ArmarLineaTXT : String;
    begin
        with spObtenerRespuestasCaptacionesCMR do begin

            Result :=	  PADL(Trim(FieldByName('NumeroDocumento').AsString), 10, '0') +
                        PADR(Trim(FieldByName('Apellido').AsString), 24, ' ') +
                        PADR(Trim(FieldByName('Nombre').AsString), 12, ' ') +
                        PADR(Trim(FieldByName('Patente1').AsString), 6, ' ') +
                        PADR(Trim(FieldByName('Patente2').AsString), 6, ' ') +
                        PADR(Trim(FieldByName('Patente3').AsString), 6, ' ') +
                        PADR(Trim(FieldByName('NumeroTarjeta').AsString), 16, ' ') +
                        PADL(Trim(FieldByName('MandatoCMR').AsString), 12, '0') +
                        PADR(Trim(FieldByName('Direccion').AsString), 36, ' ') +
                        IIF(FieldByName('FechaInicio' ).IsNull, PADR(' ', 8, ' '), FormatDateTime('yyyymmdd', FieldByName('FechaInicio').AsDateTime)) +
                        IIF(FieldByName('FechaTermino' ).IsNull, PADR(' ', 8, ' '), FormatDateTime('yyyymmdd', FieldByName('FechaTermino').AsDateTime)) +
                        PADL(Trim(FieldByName('CodigoRespuesta').AsString), 3, '0') +
                        PADR(Copy(FieldByName('ObservacionRespuesta').AsString,1,30), 30, ' '); // Toma s�lo el Primer Error                         //PADR(ObtenerDescripcionRespuesta(FieldByName('ObservacionRespuesta').AsString), 30, ' '); // Toma s�lo el Primer Error

            //Verifico si el registro fue aprobado
            if FieldByName('CodigoRespuesta').AsInteger = 0 then begin
                //Si no tiene ninguna observacion
                if Trim(FieldByName('ObservacionRespuesta').AsString) = '' then begin
                    //fue aprobado sin observaciones
                    Inc(FRespuestasOK);
                end else begin
                    //fue aprobado con observaciones
                    Inc(FWarnings);
                end;
            end else begin
                //el registro fue rechazado
                Inc(FErrores);
            end;
            
        end;
    end;

    {********************************** File Header ********************************
      File Name : frmEnvioRespuestasCaptaciones.pas
      Author : FLamas
      Date Created: 14/07/2005
      Language : ES-AR
      Description : Devuelve la descripci�n de la 1� respuesta en funci�n de la lista
                    de todos los c�digos de respuesta.
    *******************************************************************************}
      {Function ObtenerDescripcionRespuesta(CodigoRespuesta : string) : string;
      resourcestring
            MSG_E001 	= 'Rut err�neo';
            MSG_E002_1 	= 'Pat1 no registrada en C Norte';
            MSG_E002_2 	= 'Pat2 no registrada en C Norte';
            MSG_E002_3 	= 'Pat3 no registrada en C Norte';
            MSG_E003 	= 'Tarjeta CMR no num�rica';
            MSG_E004 	= 'Tarjeta err�nea';
            MSG_E005 	= 'Fecha Inicio err�nea';
            MSG_E006 	= 'Fecha de T�rmino err�nea';
            MSG_E007 	= 'Alta para PAT CMR existente';
            MSG_E008 	= 'Baja para PAT CMR inexistente';
            MSG_E009 	= 'Convenio con mandato m�s nuevo';
            MSG_E010 	= 'No se informan patentes';
            MSG_W001 	= 'Pats en m�s de un convenio';
            MSG_W002 	= 'Mandante no Cliente del Conv.';
      var
            nPos : integer;
      begin
          result := '';

          // Busca d�nde termina la primera respuesta
          nPos := Pos( ';', CodigoRespuesta);

          if (nPos = 0) then Exit;

          CodigoRespuesta := Copy(CodigoRespuesta, 1, nPos - 1);

          // Busca el nuevo C�odigo de Respuesta
          if		CodigoRespuesta = 'E001' 	then result := MSG_E001
          else if	CodigoRespuesta = 'E002-1' then result := MSG_E002_1
          else if	CodigoRespuesta = 'E002-2' then result := MSG_E002_2
          else if	CodigoRespuesta = 'E002-3' then result := MSG_E002_3
          else if	CodigoRespuesta = 'E003'	then result := MSG_E003
          else if	CodigoRespuesta = 'E004'	then result := MSG_E004
          else if	CodigoRespuesta = 'E005'	then result := MSG_E005
          else if	CodigoRespuesta = 'E006'	then result := MSG_E006
          else if	CodigoRespuesta = 'E007'	then result := MSG_E007
          else if	CodigoRespuesta = 'E008'	then result := MSG_E008
          else if	CodigoRespuesta = 'E009'	then result := MSG_E009
          else if	CodigoRespuesta = 'E009'	then result := MSG_E010
          else if	CodigoRespuesta = 'W001'	then result := MSG_W001
          else if	CodigoRespuesta = 'W002'	then result := MSG_W002;

      end;}

Resourcestring
  	MSG_NO_PENDING_ANSWERS 	 			    = 'No hay respuestas pendientes de env�o';
  	MSG_COULD_NOT_CREATE_FILE			    = 'No se pudo crear el archivo de Respuestas a Captaciones';
  	MSG_COULD_NOT_GET_PENDING_ANSWERS = 'No se pudieron obtener las Respuestas a Captaciones';
    MSG_GENERATING_ANSWERS_FILE		    = 'Generando archivo de Respuestas - Cantidad de Respuestas : %d ';
    MSG_ERROR = 'Error';
Const
    STR_PROCESSING = 'Procesando...';
begin
    try
        Screen.Cursor := crHourglass;
        result := False;

        try

            spObtenerRespuestasCaptacionesCMR.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value := FCodigoOperacionEntrada;
            spObtenerRespuestasCaptacionesCMR.CommandTimeout := 500;
      		  spObtenerRespuestasCaptacionesCMR.Open;

            if spObtenerRespuestasCaptacionesCMR.IsEmpty then begin
                FErrorMsg := MSG_NO_PENDING_ANSWERS;
            	  MsgBox(MSG_NO_PENDING_ANSWERS, Caption, MB_ICONWARNING);
        		    Screen.Cursor := crDefault;
                Exit;
            end;

        except
        	  on e: Exception do begin
                FErrorMsg := MSG_COULD_NOT_GET_PENDING_ANSWERS;
                MsgBoxErr(MSG_COULD_NOT_GET_PENDING_ANSWERS, E.Message, MSG_ERROR, MB_ICONERROR);
                Exit;
            end;
        end;

        lblReferencia.Caption	:= Format( MSG_GENERATING_ANSWERS_FILE, [spObtenerRespuestasCaptacionesCMR.RecordCount] );
        pbProgreso.Position := 0;
        pnlAvance.Visible := True;
        lblReferencia.Caption := STR_PROCESSING;
      	FRespuestasOK := 0;
      	FWarnings := 0;
      	FErrores := 0;

        with spObtenerRespuestasCaptacionesCMR do begin

        	  pbProgreso.Max := spObtenerRespuestasCaptacionesCMR.RecordCount;

            while ( not Eof ) and ( not FDetenerImportacion ) do begin
                //Genero cada linea del txt
              	FRespuestasTXT.Add( ArmarLineaTXT );
                pbProgreso.StepIt;
                Application.ProcessMessages;
                Next;
            end;
        end;

        if ( not FDetenerImportacion ) and ( FErrorMsg = '' ) then begin
          	try
              	FRespuestasTXT.SaveToFile( edDestino.Text );
                Result := True;
            except
              	on e: Exception do begin
                    FErrorMsg := MSG_COULD_NOT_CREATE_FILE;
                  	MsgBoxErr(MSG_COULD_NOT_CREATE_FILE, e.Message, MSG_ERROR, MB_ICONERROR);
                end;
            end;
        end;

    finally
        //asigno al cursor estado normal
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarRespuestasCMR
  Author:    flamas
  Date Created: 03/01/2005
  Description: Actualiza el Detalle de Respuestas Enviadas
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioRespuestasCaptaciones.ActualizarRespuestasCMR;
resourcestring
    MSG_COULD_NOT_UPDATE_SENT_ANSWERS = 'Las respuestas no se pudieron actualizar';
    MSG_ERROR = 'Error';
const
    STR_PROCESSING = 'Procesando...';
begin
    Screen.Cursor := crHourglass;
    pbProgreso.Position := 0;
    pnlAvance.Visible := True;
    lblReferencia.Caption := STR_PROCESSING;
    Application.ProcessMessages;

  	try
        with  spActualizarRespuestasCaptaciones do begin
            CommandTimeOut := 500;
            Parameters.ParamByName( '@CodigoInterfazRecepcion' ).Value := FCodigoOperacionEntrada;
            Parameters.ParamByName( '@CodigoInterfazRespuesta' ).Value := FCodigoOperacion;
            ExecProc;
        end;
    except
        on E: Exception do begin
            FErrorMsg := MSG_COULD_NOT_UPDATE_SENT_ANSWERS;
            MsgBoxErr(MSG_COULD_NOT_UPDATE_SENT_ANSWERS, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;

    pbProgreso.Position := pbProgreso.Max;
    Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Genera el Archivo de Respuestas a Captaciones
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioRespuestasCaptaciones.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Actualizo el log al finalizar
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ActualizarLog : Boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVACION = 'Finalizo OK!';
    var
         DescError : String;
    begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);
        except
            on E: Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

Resourcestring
	  MSG_FILE_ALREADY_EXISTS = 'El archivo ya existe.'#10#13'� Desea continuar ?';
	  MSG_PROCESS_SUCCEDED = 'El proceso finaliz� con �xito'#10#13 + 'Se gener� el Archivo: %s'#10#13#10#13 + 'Respuestas Positivas: %d'#10#13 + 'Respuestas Negativas: %d'#10#13 + 'Advertencias: %d';
	  MSG_PROCESS_COULD_NOT_BE_COMPLETED = 'El proceso no se pudo completar';
Begin
    //Verifico si existe el archivo
  	if FileExists( edDestino.text ) then begin
        //Informo que el archivo ya existe y le pregunto al operador si desea continuar
	    	if not(MsgBox(MSG_FILE_ALREADY_EXISTS, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then Exit;
    end;

   	//Crea la listas de Debitos
    FRespuestasTXT := TStringList.Create;

    //Deshabilita los botones
  	btnCancelar.Enabled := True;
    btnProcesar.Enabled := False;
    BorderIcons := [];
    FErrorMsg := '';

    FDetenerImportacion := False;

    try
        //Registro la operacion y Genero la respuesta a CMR
        if RegistrarOperacion and GenerarRespuestasCMR then begin
            ActualizarRespuestasCMR;
        end;

        if ( FErrorMsg = '' ) then begin
            //Actualizo el log al Final
            ActualizarLog;
            //Informo que el proceso finalizo con exito
            MsgBox(Format(MSG_PROCESS_SUCCEDED, [ExtractFileName(edDestino.Text), FRespuestasOK + FWarnings, FErrores, FWarnings]), Caption, MB_ICONINFORMATION);
        end else begin
            //Informo que el proceso no se pudo completar
           	MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
        end;

    finally
        //libero el stringlist
    		FreeAndNil( FRespuestasTXT );
       	// Habilita los botones
    		btnCancelar.Enabled := False;
      	btnProcesar.Enabled := True;
        Close;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Detiene el Procesamiento
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
Procedure TFEnvioRespuestasCaptaciones.btnCancelarClick(Sender: TObject);
resourcestring
  	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Cierra el Form si es que es posible
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioRespuestasCaptaciones.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Cierra el Formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioRespuestasCaptaciones.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Libera el Formulario de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioRespuestasCaptaciones.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	  Action := caFree;
end;

end.
