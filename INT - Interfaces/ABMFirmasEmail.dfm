object FormFirmasEmail: TFormFirmasEmail
  Left = 265
  Top = 150
  Caption = 'Mantenimiento de Firmas de Email'
  ClientHeight = 369
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 128
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'55'#0'C'#243'digo               '
      #0'33'#0'Firma')
    HScrollBar = True
    RefreshTime = 100
    Table = FirmasEmail
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 161
    Width = 592
    Height = 169
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 15
      Top = 38
      Width = 31
      Height = 13
      Caption = '&Firma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 15
      Top = 12
      Width = 39
      Height = 13
      Caption = '&C'#243'digo: '
      FocusControl = txt_CodigoFirma
    end
    object txt_CodigoFirma: TNumericEdit
      Left = 160
      Top = 8
      Width = 69
      Height = 21
      TabStop = False
      Color = clBtnFace
      MaxLength = 3
      ReadOnly = True
      TabOrder = 0
    end
    object txtFirma: TMemo
      Left = 160
      Top = 32
      Width = 417
      Height = 129
      Color = 16444382
      Lines.Strings = (
        'txtFirma')
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 330
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 395
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object FirmasEmail: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'EMAIL_Firmas'
    Left = 242
    Top = 110
    object FirmasEmailCodigoFirma: TAutoIncField
      FieldName = 'CodigoFirma'
      ReadOnly = True
    end
    object FirmasEmailFirma: TStringField
      FieldName = 'Firma'
      Size = 2048
    end
  end
end
