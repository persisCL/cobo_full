{------------------------------------------------------------------------------
 File Name: FrmGenerarDebitosSantander.pas
 Author:    flamas
 Date Created:
 Language: ES-AR
 Description:  Modulo de la interfaz Santander - Generaci�n de Debitos
--------------------------------------------------------------------------------
 Revision History
--------------------------------------------------------------------------------
 Author: rcastro
 Date Created: 17/12/2004
 Description: revisi�n general

  	Revision 1
	Autor: sguastoni
	Fecha: 05/01/2007
	Descripcion: se agrega parametro a sp  spActualizarComprobantesEnviadosSantander

    Revision : 2
        Author : vpaszkowicz
        Date : 16/07/2007
        Description: Cambio la forma de proceso. En lugar de usar el store
        spActualizarComprobantesEnviadosSantander uso una funci�n implementada
        en ComunesInterfaces y la llamo por cada comprobante que voy insertando
        en el archivo, dado que ya los tengo calculados.

    Revision : 3
        Author : jconcheyro
        Date : 08/08/2007
        Description : Agrego la lista de Grupos de facturacion para pasarle como
        parametro al store un lista con los grupos a tener en cuenta

  Revision : 5
    Date: 13/04/2009
    Author: mpiazza
    Description: ss750 se actualiza el log de interfase con el monto
                 y cantidad de registros totales para el reporte


Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       :   SS_1147_CQU_20150519
Descripcion :   Se agrega el TipoComprobante a la funci�n ActualizarDetalleComprobanteEnviado

  Firma       : SS_1385_NDR_20150922
  Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto
-------------------------------------------------------------------------------}
unit FrmGenerarDebitosSantander;

interface

uses
  //Generar Debitos Santander
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  ComunesInterfaces,
  FrmRptEnvioComprobantes,       //Reporte de Comprobantes Enviados
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, PeaTypes, UtilDB, DPSControls, VariantComboBox, StrUtils,
  BuscaTab, DmiCtrls, ImgList, DBClient;


type

  TFGeneracionDebitosSantander = class(TForm)
	Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    edDestino: TEdit;
    OpenDialog: TOpenDialog;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    spObtenerComprobantesSantander: TADOStoredProc;
    lblFechaInterface: TLabel;
    spObtenerInterfasesEjecutadas: TADOStoredProc;
    edFecha: TBuscaTabEdit;
    buscaInterfaces: TBuscaTabla;
    spObtenerInterfasesEjecutadasFecha: TDateTimeField;
    spObtenerInterfasesEjecutadasDescripcion: TStringField;
    spObtenerInterfasesEjecutadasCodigoOperacionInterfase: TIntegerField;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    dblGrupos: TDBListEx;
    spObtenerUltimosGruposFacturados: TADOStoredProc;
    cdGrupos: TClientDataSet;
    ilImages: TImageList;
    dsGrupos: TDataSource;
    lblUltimosGruposFacturados: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    function  buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);
    procedure edFechaChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure dblGruposDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblGruposDblClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoOperacion : integer;
    FCodigoOperacionAnterior : variant;
	FDetenerImportacion: boolean;
    FDebitosTXT	: TStringList;
    FErrorMsg : string;
    FObservaciones : string;
    FSantander_Codigo_Convenio: AnsiString;
    FSantander_Directorio_Debitos: AnsiString;
    FMontoTotal     : Int64;
    FCantidadTotal  : Int64;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  	Function  RegistrarOperacion : boolean;
  	Function  GenerarDebitosSantander : boolean;
    //Function  ActualizarDebitosSantander : boolean;
    Function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
    function CargarGrupos: Boolean;
    function GenerarListaGruposAIncluir(var Lista: string): boolean;
    function ExistenGruposSeleccionados: boolean;
  public
	function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean; Reprocesar : boolean = False ) : Boolean;
	{ Public declarations }
  end;

var
  FGeneracionDebitosSantander: TFGeneracionDebitosSantander;

Const
	RO_MOD_INTERFAZ_ORDENES_DEBITO_PAC				= 25;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 25/11/2004
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGeneracionDebitosSantander.Inicializar(txtCaption: ANSIString; MDIChild:Boolean; Reprocesar : boolean = False ) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 19/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        SANTANDER_CODIGO_CONVENIO    = 'Santander_Codigo_Convenio';
        SANTANDER_DIRECTORIO_DEBITOS = 'Santander_Directorio_Debitos';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, SANTANDER_CODIGO_CONVENIO , FSantander_Codigo_Convenio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SANTANDER_CODIGO_CONVENIO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FSantander_Codigo_Convenio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + SANTANDER_CODIGO_CONVENIO;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, SANTANDER_DIRECTORIO_DEBITOS , FSantander_Directorio_Debitos) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SANTANDER_DIRECTORIO_DEBITOS;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FSantander_Directorio_Debitos := GoodDir(FSantander_Directorio_Debitos);
                if  not DirectoryExists(FSantander_Directorio_Debitos) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FSantander_Directorio_Debitos;
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
	MSG_DEBITS_PROCESSING = 'Procesamiento de d�bitos';
	MSG_DEBITS_REPROCESSING = 'Re-procesamiento de d�bitos';
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    // Elijo el modo en que se visualizara el formulario
	if not MDIChild then begin
		FormStyle := fsNormal;
		Visible := False;
	end;

    // Centro el form
	CenterForm(Self);
	try
		DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and
                        VerificarParametrosGenerales;
        if not Result then Exit;
                               
	except
		on e: Exception do begin
			MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
			Exit;
		end;
	end;

    //Si es re-proceso
   	lblFechaInterface.Visible := Reprocesar;
	edFecha.Visible := Reprocesar;

    if not CargarGrupos then begin
        Result := False;
        Exit;
    end;


    if not Reprocesar then begin
    	lblNombreArchivo.Top := 13;
		edDestino.Top := 32;
        FObservaciones := MSG_DEBITS_PROCESSING;
        //Verifica si existe el Directorio de Pagomatico
		btnProcesar.Enabled := DirectoryExists(FSantander_Directorio_Debitos);
		edDestino.Text := GoodFileName(FSantander_Directorio_Debitos + 'Debitos_Santander_' + FormatDateTime ( 'yyyy-mm-dd', Now ), 'txt' );
	end else begin
    	FObservaciones := MSG_DEBITS_REPROCESSING;
        spObtenerInterfasesEjecutadas.Parameters.ParamByName( '@CodigoModulo' ).Value := RO_MOD_INTERFAZ_ORDENES_DEBITO_PAC;
        spObtenerInterfasesEjecutadas.Parameters.ParamByName( '@TraerTodas' ).Value := 1;
        spObtenerInterfasesEjecutadas.CommandTimeout := 5000;
        spObtenerInterfasesEjecutadas.Open;
	end;

    // Todo Ok, terminamos de inicializar
    Caption := AnsiReplaceStr(txtCaption, '&', '');
	btnCancelar.Enabled := False;
	pnlAvance.Visible := False;
	lblReferencia.Caption := '';
	result := True;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFGeneracionDebitosSantander.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_DEBITOS         = ' ' + CRLF +
                          'El Archivo de Debitos es' + CRLF +
                          'utilizado por el ESTABLECIMIENTO para informar a SANTANDER' + CRLF +
                          'el detalle de los pagos a recaudar' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: Debitos_Santander_YYYY_MM_DD.txt' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_DEBITOS, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TFGeneracionDebitosSantander.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;


{-----------------------------------------------------------------------------
  Function Name: buscaInterfacesProcess
  Author:    flamas
  Date Created: 08/07/2005
  Description: Muestro la lista de interfases procesadas
  Parameters: Tabla: TDataSet; var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGeneracionDebitosSantander.buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Result := True;
	with Tabla do
    	Texto := FieldByName('CodigoOperacionInterfase').AsString + ' - ' + FieldByName('Descripcion').AsString + ' - ' + FieldByName('Fecha').AsString;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfacesSelect
  Author:    flamas
  Date Created: 08/07/2005
  Description: Permito seleccionar una interfaz
  Parameters: Sender: TObject; Tabla: TDataSet
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosSantander.buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);

    {-----------------------------------------------------------------------------
      Function Name: InvertirFecha
      Author:    flamas
      Date Created: 09/12/2004
      Description: Invierte el formato de la fecha para generar el archivo de d�bbitos
      Parameters: sFecha : string
      Return Value: string
    -----------------------------------------------------------------------------}
    function InvertirFecha( sFecha : string ) : string;
    begin
        result := Copy( sFecha, 7, 4 ) + '-' + Copy( sFecha, 4, 2 ) + '-' + Copy( sFecha, 1, 2);
    end;

resourcestring
	DEBITS_FILENAME = 'Debitos_Santander_';
begin
	edFecha.Text := FormatDateTime( 'dd-mm-yyyy', Tabla.FieldByName('Fecha').AsDateTime);
    FCodigoOperacionAnterior := Tabla.FieldByName('CodigoOperacionInterfase').Value;
    edDestino.Text := GoodFileName( FSantander_Directorio_Debitos + DEBITS_FILENAME + InvertirFecha(edFecha.Text) , 'txt' );
end;

{-----------------------------------------------------------------------------
  Function Name: edFechaChange
  Author:    flamas
  Date Created: 08/07/2005
  Description: permito procesar si hay cargada una fecha
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosSantander.edFechaChange(Sender: TObject);
begin
   btnProcesar.Enabled := ( edFecha.Text <> '' );
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 03/12/2004
  Description: Registra la Operaci�n en el registro de Operaciones
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TFGeneracionDebitosSantander.RegistrarOperacion : boolean;
resourcestring
	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
	DescError : string;
begin
   result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_ORDENES_DEBITO_PAC, ExtractFileName( edDestino.text ), UsuarioSistema, FObservaciones, False, edFecha.Visible, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError );
   if not result then
    	MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarDebitosSantander
  Author:    flamas
  Date Created: 06/12/2004
  Description: Genera el archivo de d�bitos para el Banco Santander
  Parameters: None
  Return Value: boolean
  Revision : 2
      Author : vpaszkowicz
      Date : 16/07/2007
      Description : Subo a esta parte el registro del comprobante usado ya que
      tengo todos los comprobantes en el store y no es necesario volverlos a
      calcular.
-----------------------------------------------------------------------------
  Revision :3
  Date: 13/04/2009
  Author: mpiazza
  Description: ss750 se actualiza el log de interfase con el monto
               y cantidad de registros totales para el reporte
-----------------------------------------------------------------------------}
function TFGeneracionDebitosSantander.GenerarDebitosSantander : boolean;
resourcestring
	MSG_NO_PENDING_DEBITS 	 			= 'No hay d�bitos pendientes de env�o';
	MSG_COULD_NOT_CREATE_FILE			= 'No se pudo crear el archivo de d�bitos';
	MSG_COULD_NOT_GET_PENDING_DEBITS 	= 'No se pudieron obtener los d�bitos pendientes';
    MSG_GENERATING_DEBITS_FILE			= 'Generando archivo de d�bitos - Cantidad de d�bitos : %d ';
    MSG_PROCESSING			 			= 'Procesando...';
    MSG_OBTAINING_DATA			 		= 'Obteniendo datos a enviar al archivo...';
const
    BANK_CODE = '037';  //Banco debitador Santander
var
    ListaGrupos: string;
begin
	try
    	Screen.Cursor := crHourglass;
	  	result := False;
        lblReferencia.Caption := MSG_OBTAINING_DATA;
        Application.ProcessMessages;
        //Obtiene los comprobantes a Enviar
	    try
            spObtenerComprobantesSantander.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value := FCodigoOperacionAnterior;

            if not GenerarListaGruposAIncluir( ListaGrupos ) then Exit ;
            spObtenerComprobantesSantander.Parameters.ParamByName( '@ListaGruposAIncluir' ).Value := ListaGrupos;
            spObtenerComprobantesSantander.CommandTimeout := 5000;
    		spObtenerComprobantesSantander.Open;
            if spObtenerComprobantesSantander.IsEmpty then begin
               	FErrorMsg := MSG_NO_PENDING_DEBITS;
            	  MsgBox(MSG_NO_PENDING_DEBITS, Caption, MB_ICONWARNING);
        		    Screen.Cursor := crDefault;
                Exit;
            end;

        except
        	on e: Exception do begin
                FErrorMsg := MSG_COULD_NOT_GET_PENDING_DEBITS;
                MsgBoxErr(MSG_COULD_NOT_GET_PENDING_DEBITS, e.Message, 'Error', MB_ICONERROR);
                Exit;
            end;
        end;

        //Genera el archivo
        lblReferencia.Caption := Format( MSG_GENERATING_DEBITS_FILE, [spObtenerComprobantesSantander.RecordCount] );
        pbProgreso.Position := 0;
        pnlAvance.Visible := True;
        lblReferencia.Caption := MSG_PROCESSING;
        with spObtenerComprobantesSantander do begin
        	pbProgreso.Max := spObtenerComprobantesSantander.RecordCount;
            try
                while ( not Eof ) and ( not FDetenerImportacion ) do begin
                    FDebitosTXT.Add( PadR( trim(FSantander_Codigo_Convenio), 10, ' ' ) +
                                        PadL( trim(FieldByName( 'CodigoBancoSBEI' ).AsString), 3, '0' ) +
                                        BANK_CODE +
                                        PadL( '0', 17, '0' ) +
                                        PadL( trim(FieldByName( 'Importe' ).AsString ), 10, '0' ) +
                                        PadR( trim(FieldByName( 'NumeroDocumento' ).AsString), 15, ' ' ) +
                                        PadL( Copy( trim(FieldByName( 'NumeroConvenio' ).AsString), 4, 14 ), 20, '0' ) +
                                        PadL( trim(FieldByName( 'NumeroComprobante' ).AsString), 8, '0' ));

                    FMontoTotal     := FMontoTotal + FieldByName('Importe').AsInteger;
                    inc( FCantidadTotal)  ;

                    if not ActualizarDetalleComprobanteEnviado(DMConnections.BaseCAC,
                        FCodigoOperacion, spObtenerComprobantesSantander.FieldByName('NumeroComprobante').Value,
                        spObtenerComprobantesSantander.FieldByName('NumeroComprobante').AsString,   // SS_1147_CQU_20150519
                        FErrorMsg, spObtenerComprobantesSantander.FieldByName('Importe').Value) then
                            raise Exception.Create(FErrorMsg);
                    pbProgreso.StepIt;
                    Application.ProcessMessages;
                    Next;
                end;
            except
        	    on e: Exception do begin
                    MsgBoxErr(MSG_COULD_NOT_GET_PENDING_DEBITS, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        end;

        //Guarda el archivo
        if not FDetenerImportacion then begin
        	try
            	FDebitosTXT.SaveToFile( edDestino.Text );
                result := True;
            except
            	on e: Exception do begin
                	MsgBoxErr(MSG_COULD_NOT_CREATE_FILE, e.Message, 'Error', MB_ICONERROR);
                    FErrorMsg := MSG_COULD_NOT_CREATE_FILE;
                end;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

{-------------------------------------------------------------------------------
  Function Name: ActualizarDebitosSantander
  Author:    flamas
  Date Created: 07/12/2004
  Description: Actualiza el Codigo de Operacion de los Comprobantes Enviados
  Parameters: None
  Return Value: boolean
  Revision : 2
      Author : vpaszkowicz
      Date : 16/07/2007
      Description : Comento este c�digo porque de ahora en mas se hace por
      l�nea del archivo, ya que tengo los d�bitos dentro del primer store y
      no es necesario volver a calcularlos.
--------------------------------------------------------------------------------}
{Function TFGeneracionDebitosSantander.ActualizarDebitosSantander:boolean;
resourcestring
  	MSG_UPDATING_SENT_DEBITS = 'Actualizando comprobantes enviados';
    MSG_PROCESSING = 'Procesando...';
begin
    Result := False;
  	Screen.Cursor := crHourglass;
    lblReferencia.Caption := MSG_UPDATING_SENT_DEBITS;
    pbProgreso.Position := 0;
    pnlAvance.Visible := True;
    lblReferencia.Caption := MSG_PROCESSING;
    Application.ProcessMessages;
	try
    	spActualizarComprobantesEnviadosSantander.Parameters.ParamByName( '@CodigoOperacionAnterior' ).Value := FCodigoOperacionAnterior;
    	spActualizarComprobantesEnviadosSantander.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value := FCodigoOperacion;
        //revision 1
        spActualizarComprobantesEnviadosSantander.Parameters.ParamByName( '@Usuario' ).Value := UsuarioSistema;
      spActualizarComprobantesEnviadosSantander.CommandTimeout := 5000;
    	spActualizarComprobantesEnviadosSantander.ExecProc;
        Result := True;
    except
    	on e: Exception do begin
            FErrorMsg := e.Message;
        end;
    end;
    pbProgreso.Position := pbProgreso.Max;
    Screen.Cursor := crDefault;
end;}


{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Genero el Reporte de Finalizacion de Proceso sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFGeneracionDebitosSantander.GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
var FEnvio:TFRptEnvioComprobantes;
begin
    Result:=false;
    try
        Application.createForm(TFRptEnvioComprobantes,FEnvio);
        if not fEnvio.Inicializar('Comprobantes Enviados',CodigoOperacionInterfase) then fEnvio.Release;
        Result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    flamas
  Date Created: 03/12/2004
  Description: Busca los d�bitos y Genera un TXT con los mismos
  Parameters: Sender: TObject
  Return Value: None
  Revision :
      Author : vpaszkowicz
      Date : 16/07/2007
      Description : Subo la transacci�n porque voy a ir grabando los comproban-
      tes usados uno a uno por cada l�nea del archivo.
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosSantander.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Actualizo el log al finalizar
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ActualizarLog:boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVACION = 'Finalizo OK!';
    var
         DescError : String;
    begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);
        except
            on e: Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

resourcestring
	MSG_PROCESS_SUCCEDED = 'El proceso finaliz� con �xito';
	MSG_PROCESS_COULD_NOT_BE_COMPLETED = 'El proceso no se pudo completar';
	MSG_FILE_ALREADY_EXISTS = 'El archivo ya existe.'#10#13'� Desea continuar ?';
    MSG_COULD_NOT_UPDATE_SENT_DEBITS = 'Los comprobantes no se pudieron actualizar';
    MSG_ERROR_DELETE_FILE = 'No pudo eliminar el archivo';
    MSG_ERROR = 'Error';
    MSG_SIN_GRUPOS_SELECCIONADOS = 'No se seleccionaron grupos de facturacion a incluir';
begin
    // Verifica si el archivo existe ya
	if FileExists( edDestino.text ) then
        // En caso que exista le pregunta al operador si desea continuar
		if not(MsgBox(MSG_FILE_ALREADY_EXISTS, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then Exit;

    if not ExistenGruposSeleccionados  then begin
        MsgBoxBalloon(MSG_SIN_GRUPOS_SELECCIONADOS, MSG_ERROR, MB_ICONQUESTION , dblGrupos );
        Exit;
    end;

 	// Crea la listas de Debitos
    FDebitosTXT := TStringList.Create;

    // Deshabilita los botones
	btnCancelar.Enabled := True;
    btnProcesar.Enabled := False;
    BorderIcons := [];
    FErrorMsg := '';
    FCodigoOperacion := -1;
    FMontoTotal     := 0 ;
    FCantidadTotal  := 0 ;

    FDetenerImportacion := False;

    try
        //DMConnections.BaseCAC.BeginTrans;                                         //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('BEGIN TRAN frmGenerarDebitosSantander');     //SS_1385_NDR_20150922

        //Registra la operacion y Genera los debitos
        {if RegistrarOperacion and GenerarDebitosSantander then begin
            //Abro un Transacci�n
            DMConnections.BaseCAC.BeginTrans;
            //Actualiza el estado de los debitos
            if ActualizarDebitosSantander then begin}
            if RegistrarOperacion and GenerarDebitosSantander then begin
                //Acepto la Transaccion
              //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
              DMConnections.BaseCAC.Execute('COMMIT TRAN frmGenerarDebitosSantander');					//SS_1385_NDR_20150922

            end else begin
                FErrorMsg := 'Error Procesando comprobantes';
                //Si hubo errores hace un RollBack
              //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
              DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION frmGenerarDebitosSantander END');	//SS_1385_NDR_20150922

                try
                    //Elimino el archivo
                    DeleteFile(edDestino.Text);
                except
                    on e: Exception do begin
                       //Informo que no pudo eliminar el archivo
                       MsgBoxErr(MSG_ERROR_DELETE_FILE, e.Message, Self.caption, MB_ICONERROR);
                    end;
                end;
                //Muestro un Cartel informando que no pudo actualizar los debitos
                MsgBoxErr(MSG_COULD_NOT_UPDATE_SENT_DEBITS, FErrorMsg, MSG_ERROR, MB_ICONERROR);
            end;
        //end;

        //Verifico si hubo errores
        if ( FErrorMsg = '' ) then begin

            //Actualizo el log al Final
            ActualizarLog;
            //Muestro Cartel de Finalizaci�n
            MsgBox(MSG_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION );
            //Muestro el Reporte de Finalizacion del Proceso
			if (FCodigoOperacion > 0) then GenerarReportedeFinalizacion(FCodigoOperacion);

        end else begin

            //Informa que el proceso no se pudo completar
        	MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);

        end;

    finally
        // Libero la lista de debitos
		FreeAndNil( FDebitosTXT );
    	// Habilita los botones
		btnCancelar.Enabled := False;
    	btnProcesar.Enabled := True;
        Close;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    flamas
  Date Created: 07/12/2004
  Description: Detiene el proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosSantander.btnCancelarClick(Sender: TObject);
resourcestring
	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: permite salir si no esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosSantander.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
	CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosSantander.btnSalirClick(Sender: TObject);
begin
   Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosSantander.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	Action := caFree;
end;



procedure TFGeneracionDebitosSantander.dblGruposDblClick(Sender: TObject);
begin
    cdGrupos.Edit;
    if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
        cdGrupos.FieldByName('Seleccionado').AsBoolean := False;
    end else begin
        if ( cdGrupos.FieldByName('NoTerminados').AsInteger > 0 ) then
        begin
            ShowMessage('Este Proceso de Facturacion tiene comprobantes no terminados');
            cdGrupos.FieldByName('Seleccionado').AsBoolean := False;
        end
        else
            cdGrupos.FieldByName('Seleccionado').AsBoolean := True;
    end;
    cdGrupos.Post;
end;

procedure TFGeneracionDebitosSantander.dblGruposDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column.FieldName = 'Seleccionado' then begin
        if cdGrupos.FieldByName('Seleccionado').AsBoolean = True then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 1);
          end else if cdGrupos.FieldByName('Seleccionado').AsBoolean = False then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 0);
          end;
          DefaultDraw := False;
    end;

end;


{******************************** Function Header ******************************
Function Name: CargarGrupos
Author :
Date Created :
Description :
Parameters : none
Return Value : Boolean
-----------------------------------------------------------------------------
  Revision :1
  Date: 23/09/2009
  Author: mpiazza
  Description: ss750 se incrementa a 200 segundos  el comandTimeOut
                    de ObtenerUltimosGruposFacturados
*******************************************************************************}
function TFGeneracionDebitosSantander.CargarGrupos : Boolean;
begin
    Result := False;
    spObtenerUltimosGruposFacturados.Close;

    try
        cdGrupos.CreateDataSet;
        spObtenerUltimosGruposFacturados.CommandTimeout := 200;
        spObtenerUltimosGruposFacturados.Open;
        Result := not spObtenerUltimosGruposFacturados.IsEmpty;
        while not spObtenerUltimosGruposFacturados.Eof do begin
            // Insertamos nuestro registro
            cdGrupos.AppendRecord([False, spObtenerUltimosGruposFacturados.FieldByName('CodigoGrupoFacturacion').AsInteger,
              spObtenerUltimosGruposFacturados.FieldByName('FechaProgramadaEjecucion').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaCorte').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaEmision').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaVencimiento').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('NoTerminados').AsInteger
              ]);
            spObtenerUltimosGruposFacturados.Next;
        end;
        spObtenerUltimosGruposFacturados.Close;
        cdGrupos.First;
    except
        on e: Exception do begin
            MsgBoxErr('Error obteniendo los �ltimos grupos facturados', e.Message, Caption, MB_ICONERROR);
            Result:= False;
        end;
    end;
end;



function TFGeneracionDebitosSantander.GenerarListaGruposAIncluir( var Lista:string ): boolean;
var
    ListaTemporal :TStringList;
    I: integer;
begin
    Result := False;
    ListaTemporal := TStringList.Create;
    try
        try
            ListaTemporal.Duplicates := dupIgnore; // la lista ignora los duplicados
            cdGrupos.First;
            while not cdGrupos.Eof do begin
                if cdGrupos.FieldByName('Seleccionado').AsBoolean then
                    ListaTemporal.Add( IntToStr(cdGrupos.FieldByName('GrupoFacturacion').AsInteger));
                cdGrupos.Next;
            end;
            cdGrupos.First;
            for I := 0 to ListaTemporal.Count - 1 do begin
                Lista := Lista + ListaTemporal[I];
                if (I < (ListaTemporal.Count -1) ) then   // asi no me agrega una coma al final
                    Lista := Lista + ','
            end;
            Result := True;
        except
            on E: Exception do begin
                MsgBoxErr('Error generando par�metro con lista de grupos para el store', e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
            ListaTemporal.Free;
    end;

end;


function TFGeneracionDebitosSantander.ExistenGruposSeleccionados: boolean;
begin
    Result := False;
    try
        cdGrupos.First;
        while not cdGrupos.Eof do begin
            if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
                Result := True;
                Break;
            end;
            cdGrupos.Next;
        end;
        cdGrupos.First;
    except
        on E: Exception do begin
            MsgBoxErr('Error contando grupos para la generaci�n ', e.Message, Caption, MB_ICONERROR);
        end;
    end;

end;


end.
