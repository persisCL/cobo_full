object FormCancelarNotificacionMasivaInfractores: TFormCancelarNotificacionMasivaInfractores
  Left = 333
  Top = 122
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Anular proceso masivo de impresi'#243'n'
  ClientHeight = 113
  ClientWidth = 352
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 4
    Top = 8
    Width = 344
    Height = 63
  end
  object lblFechaInterfase: TLabel
    Left = 18
    Top = 18
    Width = 224
    Height = 13
    Caption = 'Fecha del proceso infractores a anular:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnProcesar: TButton
    Left = 184
    Top = 78
    Width = 75
    Height = 27
    Caption = '&Procesar'
    Enabled = False
    TabOrder = 1
    OnClick = btnProcesarClick
  end
  object btnSalir: TButton
    Left = 268
    Top = 78
    Width = 75
    Height = 27
    Cancel = True
    Caption = '&Salir'
    TabOrder = 2
    OnClick = btnSalirClick
  end
  object edProceso: TBuscaTabEdit
    Left = 18
    Top = 40
    Width = 311
    Height = 21
    Hint = 'B'#250'squeda de procesos de notificaci'#243'n previos'
    Enabled = True
    ParentShowHint = False
    ReadOnly = True
    ShowHint = True
    TabOrder = 0
    EditorStyle = bteTextEdit
    BuscaTabla = btOperacionesInterfaz
  end
  object spCancelarOperacionInterfazInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CancelarOperacionInterfazInfractores'
    Parameters = <>
    Left = 75
    Top = 76
  end
  object btOperacionesInterfaz: TBuscaTabla
    Caption = 'Notificaci'#243'n Masiva a Infractores'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = spObtenerOperacionesInterfazInfractores
    OnProcess = btOperacionesInterfazProcess
    OnSelect = btOperacionesInterfazSelect
    Left = 12
    Top = 76
  end
  object spObtenerOperacionesInterfazInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOperacionesInterfazInfractores'
    Parameters = <>
    Left = 43
    Top = 76
  end
end
