object GeneracionDeNominasForm: TGeneracionDeNominasForm
  Left = 0
  Top = 0
  Caption = 'GeneracionDeNominasForm'
  ClientHeight = 450
  ClientWidth = 402
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    402
    450)
  PixelsPerInch = 96
  TextHeight = 16
  object lblLider: TLabel
    Left = 272
    Top = 53
    Width = 16
    Height = 22
    Caption = 'P'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Wingdings 2'
    Font.Style = []
    ParentFont = False
  end
  object lblServipag: TLabel
    Left = 272
    Top = 85
    Width = 16
    Height = 22
    Caption = 'P'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Wingdings 2'
    Font.Style = []
    ParentFont = False
  end
  object lblSantander: TLabel
    Left = 272
    Top = 117
    Width = 16
    Height = 22
    Caption = 'P'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Wingdings 2'
    Font.Style = []
    ParentFont = False
  end
  object lblMisCuentas: TLabel
    Left = 272
    Top = 148
    Width = 16
    Height = 22
    Caption = 'P'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Wingdings 2'
    Font.Style = []
    ParentFont = False
  end
  object lblUnimarc: TLabel
    Left = 272
    Top = 180
    Width = 16
    Height = 22
    Caption = 'P'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Wingdings 2'
    Font.Style = []
    ParentFont = False
  end
  object mmoLog: TMemo
    Left = 0
    Top = 257
    Width = 402
    Height = 193
    Align = alBottom
    Lines.Strings = (
      'mmoLog')
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object chkNominasLider: TCheckBox
    Tag = 10
    Left = 112
    Top = 56
    Width = 97
    Height = 17
    Alignment = taLeftJustify
    Caption = 'L'#237'der'
    Checked = True
    State = cbChecked
    TabOrder = 1
  end
  object chkNominasMisCuentas: TCheckBox
    Tag = 40
    Left = 112
    Top = 151
    Width = 97
    Height = 17
    Alignment = taLeftJustify
    Caption = 'MisCuentas'
    Checked = True
    State = cbChecked
    TabOrder = 2
  end
  object chkNominasSantander: TCheckBox
    Tag = 30
    Left = 112
    Top = 120
    Width = 97
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Santander'
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
  object chkNominasServipag: TCheckBox
    Tag = 20
    Left = 112
    Top = 88
    Width = 97
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Servipag'
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
  object btnProcesar: TButton
    Left = 152
    Top = 209
    Width = 97
    Height = 25
    Caption = '&Procesar'
    TabOrder = 5
    OnClick = btnProcesarClick
  end
  object btnSalir: TButton
    Left = 290
    Top = 8
    Width = 97
    Height = 25
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 6
    OnClick = btnSalirClick
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 3
    Width = 73
    Height = 136
    DataSource = dsNominas
    TabOrder = 7
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    Visible = False
  end
  object chkNominasUnimarc: TCheckBox
    Tag = 10
    Left = 112
    Top = 180
    Width = 97
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Unimarc'
    Checked = True
    State = cbChecked
    TabOrder = 8
  end
  object spNominasCrear: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'NominasGenerar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 24
    Top = 216
  end
  object dsNominas: TDataSource
    DataSet = spNominasCrear
    Left = 56
    Top = 216
  end
end
