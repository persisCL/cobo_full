unit fConsultaBHTU;

interface

uses
    // utiles,
    UtilProc, DateUtils, PeaProcs, PeaTypes, Util, ConstParametrosGenerales,
    // base de datos
    UtilDB, DMConnection,
    // Edicion de BHTU
    fEdicionBHTU,
    // generales
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, ExtCtrls, StdCtrls, DmiCtrls, Validate, DateEdit, DB, ADODB;

type
  TfrmConsultaBHTU = class(TForm)
    pnlFiltros: TPanel;
    pnlBotones: TPanel;
    btnConsultar: TButton;
    btnSalir: TButton;
    btnEditar: TButton;
    neCantidad: TNumericEdit;
    deFechaVenta: TDateEdit;
    txtPatente: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    chkAsignados: TCheckBox;
    dblBHTU: TDBListEx;
    spObtenerListadoConsultaBHTU: TADOStoredProc;
    dsListado: TDataSource;
    btnLimpiar: TButton;
    lblCantidad: TLabel;
    procedure dblBHTUDblClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnLimpiarClick(Sender: TObject);
    procedure dblBHTUColumns2HeaderClick(Sender: TObject);
    procedure dblBHTUColumns0HeaderClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FEditando   : boolean;
    FFechaInicio: TDateTime;
    procedure InicializarVariables;
    procedure CargarBHTU;
  public
    { Public declarations }
    function Inicializar( Titulo: AnsiString): Boolean;
  end;

var
  frmConsultaBHTU: TfrmConsultaBHTU;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: FormClose
Author : ndonadio
Date Created : 10/10/2005
Description : Permite que el form se libere automaticamente al cerrarse.
Parameters : Sender: TObject; var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TfrmConsultaBHTU.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : ndonadio
Date Created : 10/10/2005
Description :   Si hay un registro en edicion no permite cerrar...
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmConsultaBHTU.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not FEditando;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 10/10/2005
Description :   Inicializa el formulario, seta tama�o, posicion y asigna
                valores iniciales a las variables.
Parameters : Titulo: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmConsultaBHTU.Inicializar(Titulo: AnsiString): Boolean;
resourcestring
    ERROR_CANNOT_INITIALIZE = 'No se puede inicializar';
    ERROR_CANT_LOAD_INITIAL_DATE = 'No puedo obtener el Parametro General: FECHA_INICIO_COBRANZA';
var
    S: TSize;
begin
    REsult := False;
    // Asigno el caption del form
    Caption := Titulo;
    // Fijo el tama�o inicial del form
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);

    // Obtengo la fecha de inicio de cobranza para validar la fecha
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'FECHA_INICIO_COBRANZA', FFechaInicio) then begin
        MsgBoxErr(ERROR_CANNOT_INITIALIZE, ERROR_CANT_LOAD_INITIAL_DATE, Caption, MB_ICONSTOP);
        Exit;
    end;

    // Llevo las variables de paraemtros a valores iniciales...
    InicializarVariables;

    Result := True;
end;

{******************************** Function Header ******************************
Function Name: InicializarVariables
Author : ndonadio
Date Created : 10/10/2005
Description : Blanquea todas las variables de parametros y los lleva a sus
                valores iniciales.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmConsultaBHTU.InicializarVariables;
begin
    neCantidad.ValueInt := 100;
    txtPatente.Clear;
    deFechaVenta.Clear;
    chkAsignados.Checked := False;
    lblCantidad.Caption := '';
end;

procedure TfrmConsultaBHTU.btnConsultarClick(Sender: TObject);
resourcestring
    ERROR_CANT_GET_DATA = 'No se puede obtener la lista de BHTU';
    ERR_INVALID_DATE    = 'Valor inv�lido para la fecha';
    ERR_INVALIDA_QTY    = 'La cantidad a visualizar debe ser positiva';

    function ValidarFecha: boolean;
    begin
        Result := deFechaVenta.IsEmpty;
        try
            StrToDate(deFechaVenta.Text);
            Result := True;
        except
            on exception do exit;
        end;
    end;

begin
    if not ValidateControls( [deFechaVenta, neCantidad], [(deFechaVEnta.IsEmpty) OR (deFechaVenta.Date > FFechaInicio), (neCantidad.valueInt > 0)], Caption, [ERR_INVALID_DATE, ERR_INVALIDA_QTY]) then begin
        Exit;
    end;
    try
        CargarBHTU;

    except
        on e:exception do begin
            MsgBoxErr( ERROR_CANT_GET_DATA, e.Message, Caption, MB_ICONERROR);
        end;
    end;
end;

procedure TfrmConsultaBHTU.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: dblBHTUColumns0HeaderClick
Author : ndonadio
Date Created : 10/10/2005
Description : Ordena de acuerdo a Patente...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmConsultaBHTU.dblBHTUColumns0HeaderClick(Sender: TObject);
resourcestring
    ERROR_CANT_GET_DATA = 'No se puede obtener la lista de BHTU';
begin
    if (not spObtenerListadoConsultaBHTU.Active) OR (spObtenerListadoConsultaBHTU.IsEmpty) then Exit;
    if dblBHTU.Columns[0].Sorting = csAscending then dblBHTU.Columns[0].Sorting := csNone
    else dblBHTU.Columns[0].Sorting := csAscending;

    try
        spObtenerListadoConsultaBHTU.Close;
        spObtenerListadoConsultaBHTU.Parameters.ParamByName('@OrdenFecha').Value := (dblBHTU.Columns[2].Sorting = csAscending);
        spObtenerListadoConsultaBHTU.Open;
    except
        on e:exception do begin
            MsgBoxErr( ERROR_CANT_GET_DATA, e.Message, Caption, MB_ICONERROR);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: dblBHTUColumns2HeaderClick
Author : ndonadio
Date Created : 10/10/2005
Description :  Ordena de acuerdo a Fecha...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmConsultaBHTU.dblBHTUColumns2HeaderClick(Sender: TObject);
resourcestring
    ERROR_CANT_GET_DATA = 'No se puede obtener la lista de BHTU';
begin
    if (not spObtenerListadoConsultaBHTU.Active) OR (spObtenerListadoConsultaBHTU.IsEmpty) then Exit;
    if dblBHTU.Columns[2].Sorting = csAscending then dblBHTU.Columns[2].Sorting := csNone
    else dblBHTU.Columns[2].Sorting := csAscending;

    try
        spObtenerListadoConsultaBHTU.Close;
        spObtenerListadoConsultaBHTU.Parameters.ParamByName('@OrdenFecha').Value := (dblBHTU.Columns[2].Sorting = csAscending);
        spObtenerListadoConsultaBHTU.Open;
    except
        on e:exception do begin
            MsgBoxErr( ERROR_CANT_GET_DATA, e.Message, Caption, MB_ICONERROR);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnLimpiarClick
Author : ndonadio
Date Created : 10/10/2005
Description : re Inicializar variables de parametros
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmConsultaBHTU.btnLimpiarClick(Sender: TObject);
begin
    InicializarVariables;
    spObtenerListadoConsultaBHTU.Close;
end;

procedure TfrmConsultaBHTU.btnEditarClick(Sender: TObject);
resourcestring
    ERROR_CANT_GET_DATA = 'No se puede obtener la lista de BHTU';
var
    f: TFrmEdicionBHTU;
    NumeroDaypass,
    Categoria           : integer;
    FechaVenta          : TDateTime;
    FechaUso            : TDateTime;
    Patente,
    Identificador       : AnsiString;
    Usado               : Boolean;

begin
    if (not spObtenerListadoConsultaBHTU.Active) OR (spObtenerListadoConsultaBHTU.IsEmpty) then Exit;
    Application.CreateForm(TFrmEdicionBHTU, f);
    try
        with spObtenerListadoConsultaBHTU do begin
            NumeroDaypass   := FieldByName('NumeroDaypass').AsInteger;
            Categoria       := FieldByName('Categoria').AsInteger;
            FechaVenta      := FieldByName('FechaVenta').AsDateTime;
            FechaUso        := FieldByName('FechaUso').AsDateTime;
            Patente         := FieldByName('Patente').AsString;
            Identificador   := FieldByName('Identificador').AsString;
            Usado           := FieldByName('Usado').asBoolean;
        end;
        if f.Inicializar( NumeroDaypass, Categoria, FechaVenta,
          Patente, Identificador, Usado, FechaUso) then begin
            try
                if f.ShowModal = mrOk then CargarBHTU;
            except
                on e:exception do begin
                    MsgBoxErr( ERROR_CANT_GET_DATA, e.Message, Caption, MB_ICONERROR);
                end;
            end;
        end;
    finally
        f.Release;
    end;
end;

{******************************** Function Header ******************************
Function Name: CargarBHTU
Author : ndonadio
Date Created : 11/10/2005
Description : Carga la lista de BHTU seg�n los filtro
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmConsultaBHTU.CargarBHTU;
resourcestring
    CAP_QUANTITY        = 'Mostrando %d de %d BHTU';
    CAP_BHTU_NOT_FOUND  = 'No se encontraron BHTU para los filtros seleccionados';
begin
    with spObtenerListadoConsultaBHTU, Parameters do begin
        Close;
        ParamByName('@Cantidad').Value      := neCantidad.ValueInt;
        ParamByName('@Patente').Value       := TRIM(txtPatente.Text);
        ParamByName('@FechaVenta').Value    := iif(deFechaVenta.Date = NULLDATE, NULL, deFechaVenta.Date);
        ParamByName('@Usados').Value        := chkAsignados.Checked;
        if dblBHTU.Columns[2].Sorting = csAscending then
            Parameters.ParamByName('@OrdenFecha').Value := True
        else
            Parameters.ParamByName('@OrdenFecha').Value := False;
        Open;
        if not IsEmpty then  lblCantidad.Caption := Format(CAP_QUANTITY, [RecordCount, Integer(ParamByName('@Total').Value)])
        else lblCantidad.Caption := CAP_BHTU_NOT_FOUND;
        btnEditar.Enabled := not IsEmpty;
    end;
end;

{******************************** Function Header ******************************
Function Name: dblBHTUDblClick
Author : ndonadio
Date Created : 11/10/2005
Description : Al hacer el doble click en la lista...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmConsultaBHTU.dblBHTUDblClick(Sender: TObject);
begin
    if btnEditar.Enabled and btnEditar.Visible then btnEditarClick(Sender);
end;

end.
