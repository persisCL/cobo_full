{------------------------------------------------------------------------------
 File Name: FrmGenerarUniversoSantander.pas
 Author:    flamas
 Date Created:
 Language: ES-AR
 Description:  Modulo de la interfaz Santander - Recepci�n de universo
--------------------------------------------------------------------------------
 Revision History
--------------------------------------------------------------------------------
 Author: rcastro
 Date Created: 17/12/2004
 Description: revisi�n general
-------------------------------------------------------------------------------
 Author: ndonadio
 Date Created: 05/04/2005
 Description: Dialog confirmando procesamiento.

 Revision : 3
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-------------------------------------------------------------------------------}
unit FrmRecepcionUniversoSantander;

interface

uses
  //Recepcion Universo Santander
  DMConnection,
  Util,
  UtilDB,
  UtilProc,
  PeaTypes,
  ConstParametrosGenerales,
  PeaProcs,
  ComunesInterfaces,
  frmRptRecepcionUniversoSantander,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx,DPSControls, ppDB, ppTxPipe, StrUtils, ppParameter,
  ppModule, raCodMod, ppCtrls, ppBands, ppReport, ppPrnabl, ppClass, ppStrtch,
  ppSubRpt, ppCache, ppComm, ppRelatv, ppProd, UtilRB, RStrings, RBSetup,
  DBClient, ppDBPipe;

type

  TUniversoPACRecord = record
  	NumeroConvenio : string;
    CodigoBancoSBEI : integer;
    NombreBancoSBEI : string;
    RUTSubscriptor : string;
    NroCuentaBancaria : string;
    FechaInicio : TDateTime;
    NroMandato: string;
  end;

  TFRecepcionUniversoSantander = class(TForm)
    Bevel1: TBevel;
    btnAbrirArchivo: TSpeedButton;
    Label2: TLabel;
    lblReferencia: TLabel;
    edOrigen: TEdit;
    OpenDialog: TOpenDialog;
    spCopiarDatosConveniosAUniversoPAC: TADOStoredProc;
    spObtenerConveniosPacNoVerificados: TADOStoredProc;
  	rbiListado: TRBInterface;
    ppReporteRecepUniversoSantander: TppReport;
    ppDetailBand1: TppDetailBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppHeaderBand2: TppHeaderBand;
    ppLine2: TppLine;
    ppLabel1: TppLabel;
    lbl_usuario: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppImage2: TppImage;
    ppDetailBand2: TppDetailBand;
    ppParameterList1: TppParameterList;
    spEliminarMedioPagoPAC: TADOStoredProc;
    spObtenerUltimoCodigoConvenio: TADOStoredProc;
  	spActualizarMandatosPendientes: TADOStoredProc;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    spAgregarUniversoPAC: TADOStoredProc;
    spBorrarUniversoPAC: TADOStoredProc;
    cdsErrores: TClientDataSet;
    qryBanco: TADOQuery;
    ppdbErrores: TppDBPipeline;
    dsErrores: TDataSource;
    ppDBText3: TppDBText;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    cdsUniversoPACSantander: TClientDataSet;
    spBorrarUniversoPACSantander: TADOStoredProc;
    spAgregarUniversoPACSantander: TADOStoredProc;
    spCompararUniversoPACUniversoPACSantander: TADOStoredProc;
    spAgregarErrorInterfaz: TADOStoredProc;
    spObtenerDatosProcesoUniversoSantander: TADOStoredProc;
    spActualizarLogOperacionesRegistrosProcesados: TADOStoredProc;
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure edOrigenChange(Sender: TObject);
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure rbiListadoExecute(Sender: TObject; var Cancelled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    FCodigoOperacion : integer;
  	FDetenerImportacion: boolean;
  	FUniversoTXT : TStringList;
    FErrores : TStringList;
  	FErrorMsg : string;
    FCantConvenios : integer;
    FVerReporteErrores: boolean;
  	FSantander_Directorio_Universo : AnsiString;
    FSantander_Directorio_Errores : Ansistring;
    FSTD_DirectorioUniversoProcesado: AnsiString;
    FMandatosMayoraArchivo : String;
    FArchivoMayoraMandato  : String;
    FDiferenciaPermitidaArchVSBase   : integer;
    FDiferenciaPermitidaBaseVSArch   : integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function  BorrarUniversoPAC : boolean;
    function  BorrarUniversoPACSantander : boolean;
    function  CopiarConveniosAUniversoPAC : boolean;
    function  AnalizarUniversoTxt : boolean;
    function  CargarUniversoPACSantander : boolean;
    function  CompararUniversoPACUniversoPACSantander : boolean;
    function  VerificarUniverso : boolean;
  	function  ProcesarNoExistentesUniversoPAC : boolean;
    function  ActualizarMandatosPendientes : boolean;
    function  ParseUniversoPACLine( sline : string; var UniversoRecord : TUniversoPACRecord; var sParseError : string ) : boolean;
  	procedure GenerarReportesErrores;
  	function  RegistrarOperacion : boolean;
    function  ConfirmaCantidades(Lineas:integer): Boolean;
    procedure AgregarErrorInterfaz(mDescripcionError: String);
    function ObtenerDatosProcesoUniversoSantander(CodigoOperacionInterfase: integer; var LineasArchivo: integer;
                var ConveniosDuplicados: integer; var RegistrosExito: integer; var RegistrosError: integer): boolean;
    function  MsgProcesoFinalizadoSantander(sMensajeOK, sMensajeError, sTitulo: AnsiString; RLineas, RDuplicados, RExito, RError: integer): boolean;
  public
  	function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
  	{ Public declarations }
  end;

var
  FRecepcionUniversoSantander: TFRecepcionUniversoSantander;

implementation

{$R *.dfm}

const
	  RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_PAC			= 22;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 25/11/2004
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean

  Revision 1
  Author: nefernandez
  Date: 16/04/2008
  Description: Se crea la tabla cliente para cargar los datos al archivo de universo

 Revision : 2
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 25/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        SANTANDER_DIRECTORIO_UNIVERSO    = 'Santander_Directorio_Universo';
        SANTANDER_DIRECTORIO_ERRORES     = 'Santander_Directorio_Errores';
        STD_DIRECTORIOUNIVERSOPROCESADO  = 'STD_DirectorioUniversoProcesado';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, SANTANDER_DIRECTORIO_UNIVERSO, FSantander_Directorio_Universo) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SANTANDER_DIRECTORIO_UNIVERSO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FSantander_Directorio_Universo := GoodDir(FSantander_Directorio_Universo);
                if  not DirectoryExists(FSantander_Directorio_Universo) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FSantander_Directorio_Universo;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, SANTANDER_DIRECTORIO_ERRORES , FSantander_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SANTANDER_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FSantander_Directorio_Errores := GoodDir(FSantander_Directorio_Errores);
                if  not DirectoryExists(FSantander_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FSantander_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC, STD_DIRECTORIOUNIVERSOPROCESADO, FSTD_DirectorioUniversoProcesado);

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_MAX_DIF_PAC_BASEVSARCH',FDiferenciaPermitidaBaseVSArch) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'CANT_MAX_DIF_PAC_BASEVSARCH';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_MAX_DIF_PAC_ARCHVSBASE',FDiferenciaPermitidaArchVSBase) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'CANT_MAX_DIF_PAC_ARCHVSBASE';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'MSG_DIF_ARCHIVO_MAYOR_A_MANDATOS',FArchivoMayoraMandato) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'MSG_DIF_ARCHIVO_MAYOR_A_MANDATOS';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'MSG_DIF_MANDATOS_MAYOR_A_ARCHIVO',FMandatosMayoraArchivo) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'MSG_DIF_MANDATOS_MAYOR_A_ARCHIVO';
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Elijo el modo en que si visualizara la ventana
	if not MDIChild then begin
		FormStyle := fsNormal;
		Visible := False;
	end;

  //centro el formlario
	CenterForm(Self);

	try
        DMConnections.BaseCAC.Connected := True;
        //Revision 1
        cdsUniversoPACSantander.CreateDataSet;
        cdsUniversoPACSantander.LogChanges := False;

	    	Result := DMConnections.BaseCAC.Connected and
                              VerificarParametrosGenerales;
	except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
  end;

  //Resolver ac� lo que necesita este form para inicializar correctamente
	Caption := AnsiReplaceStr(txtCaption, '&', '');
  FCantConvenios := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT Max(CodigoConvenio) From Convenio WITH (NOLOCK)');
	btnCancelar.Enabled := False;
	btnProcesar.Enabled := False;
	pnlAvance.Visible := False;
	lblReferencia.Caption := '';
  FCodigoOperacion := 0;         //inicializo codigo de operacion
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFRecepcionUniversoSantander.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_UNIVERSO        = ' ' + CRLF +
                          'El archivo de Universo' + CRLF +
                          'es enviado por SANTANDER al ESTABLECIMIENTO' + CRLF +
                          'segun la periodicidad convenida y contiene' + CRLF +
                          'el detalle de todos los mandantes que tienen' + CRLF +
                          'medio de pago PAC de SANTANDER' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: R00000000000000.ASC' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para actualizar los datos de' + CRLF +
                          'los mandantes en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_UNIVERSO , Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TFRecepcionUniversoSantander.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoClick
  Author:    flamas
  Date Created: 03/12/2004
  Description: Busca el Archivo del Universo.TXT
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoSantander.btnAbrirArchivoClick(Sender: TObject);
resourcestring
	MSG_ERROR_FILE_DOES_NOT_EXIST = 'El archivo %s no existe';
begin
    OpenDialog.InitialDir := FSantander_Directorio_Universo;
    if OpenDialog.execute then begin
        edOrigen.text:=UpperCase(opendialog.filename);

		if FileExists(edOrigen.text) then begin
            btnProcesar.Enabled := True;
            if FSTD_DirectorioUniversoProcesado <> '' then begin
                if RightStr(FSTD_DirectorioUniversoProcesado,1) = '\' then  FSTD_DirectorioUniversoProcesado := FSTD_DirectorioUniversoProcesado + ExtractFileName(edOrigen.text)
                else FSTD_DirectorioUniversoProcesado := FSTD_DirectorioUniversoProcesado + '\' + ExtractFileName(edOrigen.Text);
            end;
        end
    	else begin
        	btnProcesar.Enabled := False;
        	MsgBox(Format(MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigen.text]), Caption, MB_ICONERROR);
    	end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: edOrigenChange
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: permito procesar si el path pertenece a un archivo que existe
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoSantander.edOrigenChange(Sender: TObject);
begin
   btnProcesar.Enabled := FileExists( edOrigen.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: AnalizarUniversoTxt
  Author:    flamas
  Date Created: 21/12/2004
  Description: Hace un prean�lisis del Universo antes de procesarlo
  Parameters: None
  Return Value: boolean

  Revision 1
  Author: nefernandez
  Date: 16/04/2008
  Description: Se abre la tabla cliente para cargar los datos al archivo de universo
-----------------------------------------------------------------------------}
function  TFRecepcionUniversoSantander.AnalizarUniversoTxt : boolean;
resourcestring
  	MSG_ANALIZING_UNIVERSE_FILE	 = 'Analizando Archivo de Universo - Cantidad de lineas : %d';
    MSG_UNIVERSE_FILE_HAS_ERRORS = 'El archivo de Universo contiene errores'#10#13'L�nea : %d';
    MSG_ERROR					 = 'Error';
var
  	nNroLineaScript, nLineasScript :integer;
    FUniversoRecord : TUniversoPACRecord;
  	sParseError 	: string;
begin
	Screen.Cursor := crHourglass;

    //Revision 1
    if not cdsUniversoPACSantander.Active then cdsUniversoPACSantander.Active := True;
    cdsUniversoPACSantander.EmptyDataSet;

	nLineasScript := FUniversoTXT.Count;
	lblReferencia.Caption := Format( MSG_ANALIZING_UNIVERSE_FILE, [nLineasScript] );
	pbProgreso.Position := 0;
	pbProgreso.Max := FUniversoTXT.Count;
	pnlAvance.Visible := True;

  nNroLineaScript := 0;

  while (nNroLineaScript < nLineasScript) and
      (not FDetenerImportacion) and
        (FErrorMsg = '') do begin

          if not ParseUniversoPACLine(FUniversoTXT[nNroLineaScript], FUniversoRecord, sParseError) then begin
              // Si encuentra un error termina
              FErrorMsg := Format(MSG_UNIVERSE_FILE_HAS_ERRORS, [nNroLineaScript]) + #10#13 + sParseError;
              MsgBox(FErrorMsg, Caption, MB_ICONERROR);
              Screen.Cursor := crDefault;
              result := False;
              Exit;
          end;

        Inc( nNroLineaScript );
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
  end;

	result := not FDetenerImportacion;
	Screen.Cursor := crDefault;
end;


{-----------------------------------------------------------------------------
  Function Name: ConfirmaCantidades
  Author:    ndonadio
  Date Created: 05/04/2005
  Description: Consulta al usuario si quiere continuar o no,
                presentandole un resumen de las cantidades a procesar
                y de las cantidades existentes.
  Parameters: Lineas: integer
  Return Value: Boolean

  Revision 1
 Author : Fsandi
 Date : 24-05-2007
 Description : Se modifica las consultas SQL en delphi, se las cambia para que llamen funciones de la Base de Datos

  Revision 2
 Author : Fsandi
 Date : 12-06-2007
 Description : Ahora se ponen mensajes de advertencia o error si la diferencia es mucha entre el total mandatos y el archivo

 Revision 3
 Author : Fsandi
 Date : 26-06-2007
 Description : Por solicitud del cliente, se permite continuar aunque aparezca el mensaje de error.
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.ConfirmaCantidades(Lineas: integer): Boolean;
resourcestring
    MSG_CONFIRMATION = 'Actualmente existen %d Mandatos Confirmados '+crlf+'y %d Mandatos Pendientes, que hacen'+crlf+
                       'un total de %d Mandatos PAC Existentes.'+crlf+crlf+
                       'El Archivo seleccionado contiene %d Mandatos a procesar.';
    MSG_KEEP_PROCESSING = 'Desea continuar con el procesamiento del archivo?';
    MSG_TITLE = 'Confirme Recepci�n de Universo PAC';
var
    MandatosPendientes: integer;
    MandatosConfirmados: integer;
    MandatosExistentes: integer;
    MensajeCompleto : String;
begin
    //Obtengo la cantidad de mandatos Existentes/Pendientes y Confirmados
    MandatosExistentes  :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[7,0]));
    MandatosPendientes  :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[8,0]));
    MandatosConfirmados :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[9,0]));
    //Armo el mensaje
    MensajeCompleto := Format(MSG_CONFIRMATION,[MandatosConfirmados,MandatosPendientes,MandatosExistentes, Lineas]);

    // se comparan las cantidades en el archivo y la tabla que no se pasen de ciertas diferencias determinadas por parametro
    // si se superan los par�metros, se solicita confirmaci�n o se cancela el proceso.

    if MandatosExistentes > Lineas then begin
        if MandatosExistentes - Lineas > FDiferenciaPermitidaBaseVSArch then begin
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + FMandatosMayoraArchivo;
        end else begin
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING;
        end;
        Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
    end;

    if Lineas > MandatosExistentes then begin
        if Lineas - MandatosExistentes > FDiferenciaPermitidaArchVSBase then begin
            MensajeCompleto := MensajeCompleto +crlf+crlf+ FArchivoMayoraMandato;
        end else begin
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING ;
        end;
        Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
    end;
    
    if Lineas = MandatosExistentes then begin
        MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING;
        Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 03/12/2004
  Description: Registra la Operaci�n en el LogOperacionesInterface
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.RegistrarOperacion : boolean;
resourcestring
    MSG_UNIVERSE_IMPORT = 'Importaci�n Universo PAC - Cantidad de Errores : %d';
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
	sDescrip : string;
	DescError : string;
begin
    sDescrip := Format( MSG_UNIVERSE_IMPORT, [FErrores.Count] );

    result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_PAC, ExtractFileName(edOrigen.text), UsuarioSistema, sDescrip, True, False, NowBase(DMConnections.BaseCAC), 0,FCodigoOperacion, FUniversoTXT.Count, 0, DescError);

   if not result then
    	MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: BorrarUniversoPAC
  Author:    flamas
  Date Created: 31/01/2005
  Description: Borra los datos viejos de UniversoPAC por Bloques
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.BorrarUniversoPAC : boolean;
resourcestring
    MSG_DELETEING_OLD_UNIVERSE			= 'Eliminando Universo anterior';
  	MSG_ERROR_DELETEING_OLD_UNIVERSE	= 'Error eliminando Universo anterior.';
    MSG_ERROR				 			= 'Error';
var
    SiguienteConvenio : variant;
begin
  	Screen.Cursor := crHourglass;

    lblReferencia.Caption :=  MSG_DELETEING_OLD_UNIVERSE;
    pbProgreso.Position := 0;
    pbProgreso.Max := FCantConvenios;
    pnlAvance.Visible := True;

    SiguienteConvenio := 0;

    while (SiguienteConvenio <> null ) and
    	  (not FDetenerImportacion) and
          (FErrorMsg = '') do begin
          try
          	spBorrarUniversoPAC.Parameters.ParamByName('@CodigoConvenio' ).Value := SiguienteConvenio;
            spBorrarUniversoPAC.CommandTimeout := 5000;
            spBorrarUniversoPAC.ExecProc;
          	SiguienteConvenio := spBorrarUniversoPAC.Parameters.ParamByName('@CodigoConvenio' ).Value;

            if ( SiguienteConvenio = Null ) then Break;

          except
          	on e: exception do begin
            	MsgBoxErr(MSG_ERROR_DELETEING_OLD_UNIVERSE, e.Message, MSG_ERROR, MB_ICONERROR);
                FErrorMsg := MSG_ERROR_DELETEING_OLD_UNIVERSE;
            end;
    	end;
        pbProgreso.Position := SiguienteConvenio;
        Application.ProcessMessages;
    end;

    pbProgreso.Position := pbProgreso.Max;
  	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: TFRecepcionUniversoSantander
  Author: nefernandez
  Date Created: 16/04/2008
  Description: Borra los datos viejos de UniversoPACSantander por Bloques
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.BorrarUniversoPACSantander : boolean;
resourcestring
    MSG_DELETEING_OLD_UNIVERSE			= 'Eliminando Universo Santander anterior';
  	MSG_ERROR_DELETEING_OLD_UNIVERSE	= 'Error eliminando Universo Santander anterior.';
    MSG_ERROR				 			= 'Error';
var
    SiguienteConvenio : variant;
begin
  	Screen.Cursor := crHourglass;

    lblReferencia.Caption :=  MSG_DELETEING_OLD_UNIVERSE;
    pbProgreso.Position := 0;
    pbProgreso.Max := FCantConvenios;
    pnlAvance.Visible := True;

    SiguienteConvenio := 0;

    while (SiguienteConvenio <> null ) and
    	  (not FDetenerImportacion) and
          (FErrorMsg = '') do begin
          try
          	spBorrarUniversoPACSantander.Parameters.ParamByName('@CodigoConvenio').Value := SiguienteConvenio;
            spBorrarUniversoPACSantander.CommandTimeout := 5000;
            spBorrarUniversoPACSantander.ExecProc;
          	SiguienteConvenio := spBorrarUniversoPACSantander.Parameters.ParamByName('@CodigoConvenio' ).Value;

            if ( SiguienteConvenio = Null ) then Break;

          except
          	on e: exception do begin
            	MsgBoxErr(MSG_ERROR_DELETEING_OLD_UNIVERSE, e.Message, MSG_ERROR, MB_ICONERROR);
                FErrorMsg := MSG_ERROR_DELETEING_OLD_UNIVERSE;
            end;
    	end;
        pbProgreso.Position := SiguienteConvenio;
        Application.ProcessMessages;
    end;

    pbProgreso.Position := pbProgreso.Max;
  	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: CopiarConveniosAUniversoPAC
  Author:    flamas
  Date Created: 05/01/2005
  Description: Copia los Convenios PAC que actualmente hay en Convenios a la
  				Tabla UniversoPAC para poder Actualizarlos
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.CopiarConveniosAUniversoPAC : boolean;
resourcestring
  	MSG_ERROR_COULD_NOT_UPDATE_UNIVERSE = 'No se pudo actualizar el Universo anterior';
    MSG_UPDATING_UNIVERSE_DATA 			= 'Copiando datos del Universo';
    MSG_ERROR				 			= 'Error';
var
    SiguienteConvenio : variant;
begin
	  Screen.Cursor := crHourGlass;
    lblReferencia.Caption := MSG_UPDATING_UNIVERSE_DATA;
    pbProgreso.Position := 0;
    pbProgreso.Max := FCantConvenios;
    pnlAvance.Visible := True;

    SiguienteConvenio := 0;

    while (SiguienteConvenio <> null ) and
    	  (not FDetenerImportacion) and
          (FErrorMsg = '') do begin
          try
          	spCopiarDatosConveniosAUniversoPAC.Parameters.ParamByName('@CodigoConvenio' ).Value := SiguienteConvenio;
            spCopiarDatosConveniosAUniversoPAC.CommandTimeout := 5000;
            spCopiarDatosConveniosAUniversoPAC.ExecProc;
          	SiguienteConvenio := spCopiarDatosConveniosAUniversoPAC.Parameters.ParamByName('@CodigoConvenio' ).Value;

            if ( SiguienteConvenio = Null ) then Break;

          except
          	on e: exception do begin
            	MsgBoxErr(MSG_ERROR_COULD_NOT_UPDATE_UNIVERSE, e.Message, MSG_ERROR, MB_ICONERROR);
                FErrorMsg := MSG_ERROR_COULD_NOT_UPDATE_UNIVERSE;
            end;
    	end;
        pbProgreso.Position := SiguienteConvenio;
        Application.ProcessMessages;
    end;

    pbProgreso.Position := pbProgreso.Max;
  	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: VerificarUniverso
  Author:    flamas
  Date Created: 05/01/2005
  Description: Compara los datos del Universo con los del TXT y actualiza los convenios
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 2
  nefernandez
  16/04/2008
  Se usa la tabla cliente (datos del archivo universo) en lugar del arreglo de lista (TStringList)
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.VerificarUniverso : boolean;
resourcestring
    MSG_PROCESSING_UNIVERSE_FILE	= 'Procesando Archivo de Universo - Cantidad de Convenios : %d';
  	MSG_ERROR_PROCESSING_UNIVERSE 	= 'Error al procesar el Universo en la l�nea : %d';
  	MSG_PARSE_ERROR					= 'Error al interpretar el archivo de texto';
    MSG_ERROR						= 'Error';
var
  	nNroLineaScript, nLineasScript :integer;
    FUniversoRecord 	: TUniversoPACRecord;
    sDescripcionError 	: string;
  	sParseError 		: string;
    sDescErrorInterfaz 	: string;
begin
  	Screen.Cursor := crHourglass;

    nLineasScript := cdsUniversoPACSantander.RecordCount;
    lblReferencia.Caption := Format( MSG_PROCESSING_UNIVERSE_FILE, [nLineasScript] );
    pbProgreso.Position := 0;
    pbProgreso.Max := nLineasScript;
    pnlAvance.Visible := True;

    nNroLineaScript := 0;

    cdsUniversoPACSantander.First;
    while (not cdsUniversoPACSantander.Eof ) and
    	  (not FDetenerImportacion) and
          (FErrorMsg = '') do begin

        	with spAgregarUniversoPAC, Parameters do begin
            	try
                    ParamByName('@FechaAlta' ).Value 			:= cdsUniversoPACSantander.FieldByName('FechaInicio').Value;
                    ParamByName('@NumeroConvenio' ).Value 		:= cdsUniversoPACSantander.FieldByName('NumeroConvenio').Value;
                    ParamByName('@CodigoBancoSBEI' ).Value 		:= cdsUniversoPACSantander.FieldByName('CodigoBancoSBEI').Value;
                    ParamByName('@RUTSubscriptor' ).Value 		:= cdsUniversoPACSantander.FieldByName('RUTSubscriptor').Value;
                    ParamByName('@NroCuentaBancaria' ).Value 	:= iif(Trim(cdsUniversoPACSantander.FieldByName('NroCuentaBancaria').AsString) = '', NULL, cdsUniversoPACSantander.FieldByName('NroCuentaBancaria').Value);
                    CommandTimeOut := 5000;
                    ExecProc;
                    sDescripcionError := Trim( VarToStr( ParamByName('@DescripcionError' ).Value ));

                  	//if (sDescripcionError <> '') then FErrores.Add( FormatFloat('0000',CodigoBancoSBEI)+'Mandato: '+Padr(NroMandato,15,'0')+' - '+StringReplace(sDescripcionError, #10#13, ' - ', [rfReplaceAll]));
                  	if (sDescripcionError <> '') then begin
                        sDescErrorInterfaz := FormatFloat('0000',cdsUniversoPACSantander.FieldByName('CodigoBancoSBEI').Value)+'Mandato: '+Padr(cdsUniversoPACSantander.FieldByName('NroMandato').Value,15,'0')+' - '+StringReplace(sDescripcionError, #10#13, ' - ', [rfReplaceAll]);
                        FErrores.Add(sDescErrorInterfaz);
                        AgregarErrorInterfaz(sDescErrorInterfaz);
                    end;

                except
                	  on e: exception do begin
        				        MsgBoxErr(Format(MSG_ERROR_PROCESSING_UNIVERSE, [nNroLineaScript]), e.Message, MSG_ERROR, MB_ICONERROR);
                        FErrorMsg := MSG_ERROR_PROCESSING_UNIVERSE;
                    end;
                end;
            end;

        Inc( nNroLineaScript );
        cdsUniversoPACSantander.Next;
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

  	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: CargarUniversoPACSantander
  Author: nefernandez
  Date Created: 16/04/2008
  Description: Carga los datos de la tabla cliente (archvo de recepcion) a la
  base de datos
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.CargarUniversoPACSantander : boolean;
resourcestring
    MSG_PROCESSING_UNIVERSE_FILE	= 'Cargando Archivo de Universo Santander - Cantidad de Convenios : %d';
  	MSG_ERROR_PROCESSING_UNIVERSE 	= 'Error al cargar el Universo Santander en la l�nea ';
  	MSG_PARSE_ERROR					= 'Error al interpretar el archivo de texto';
    MSG_PROCESSING_LOG          	= 'Error al actualizar el log de operaciones con la cantidad de convenios a procesar';
    MSG_ERROR						= 'Error';
const
    STR_LINE = 'Linea: ';
var
  	nNroLineaScript, nLineasScript :integer;
    FUniversoRecord 	: TUniversoPACRecord;
    sDescripcionError 	: string;
  	sParseError 		: string;
begin
  	Screen.Cursor := crHourglass;

    nLineasScript := cdsUniversoPACSantander.RecordCount;
    lblReferencia.Caption := Format( MSG_PROCESSING_UNIVERSE_FILE, [nLineasScript] );
    pbProgreso.Position := 0;
    pbProgreso.Max := nLineasScript;
    pnlAvance.Visible := True;

    nNroLineaScript := 0;

    cdsUniversoPACSantander.First;
    while (not cdsUniversoPACSantander.Eof ) and
    	  (not FDetenerImportacion) and
          (FErrorMsg = '') do begin
        try
            spAgregarUniversoPACSantander.Parameters.ParamByName('@NumeroConvenio').Value := cdsUniversoPACSantander.FieldByName('NumeroConvenio').Value;
            spAgregarUniversoPACSantander.Parameters.ParamByName('@CodigoBancoSBEI').Value := cdsUniversoPACSantander.FieldByName('CodigoBancoSBEI').Value;
            spAgregarUniversoPACSantander.Parameters.ParamByName('@NombreBancoSBEI').Value := cdsUniversoPACSantander.FieldByName('NombreBancoSBEI').Value;
            spAgregarUniversoPACSantander.Parameters.ParamByName('@RUTSubscriptor').Value := cdsUniversoPACSantander.FieldByName('RUTSubscriptor').Value;
            spAgregarUniversoPACSantander.Parameters.ParamByName('@NroCuentaBancaria').Value := cdsUniversoPACSantander.FieldByName('NroCuentaBancaria').Value;
            spAgregarUniversoPACSantander.Parameters.ParamByName('@FechaInicio' ).Value := cdsUniversoPACSantander.FieldByName('FechaInicio').Value;
            spAgregarUniversoPACSantander.Parameters.ParamByName('@NroMandato' ).Value := cdsUniversoPACSantander.FieldByName('NroMandato').Value;
            spAgregarUniversoPACSantander.CommandTimeOut := 5000;
            spAgregarUniversoPACSantander.ExecProc;


        except
            on e: exception do begin
            	MsgBoxErr(MSG_PROCESSING_UNIVERSE_FILE, e.Message, MSG_ERROR, MB_ICONERROR);
                FErrorMsg := MSG_PROCESSING_UNIVERSE_FILE;
            end;
        end;

        Inc( nNroLineaScript );
        cdsUniversoPACSantander.Next;
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

    // Se actualiza el log de operaciones con la cantidad de convenios a procesar
    try
        spActualizarLogOperacionesRegistrosProcesados.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
        spActualizarLogOperacionesRegistrosProcesados.ExecProc;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_PROCESSING_LOG, e.Message, MSG_ERROR, MB_ICONERROR);
            FErrorMsg := MSG_PROCESSING_LOG;
        end;
    end;
    
  	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: CompararUniversoPACUniversoPACSantander
  Author: nefernandez
  Date Created: 16/04/2008
  Description: Carga los datos de la tabla cliente (archvo de recepcion) a la
  base de datos
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.CompararUniversoPACUniversoPACSantander : boolean;
resourcestring
    MSG_PROCESSING_UNIVERSE_FILE	= 'Comparando Universo PAC CAC contra Universo PAC Santander';
  	MSG_ERROR_PROCESSING_UNIVERSE 	= 'Error al comparando Universo PAC CAC contra Universo PAC Santander ';
  	MSG_PARSE_ERROR					= 'Error al comparando Universo PAC CAC contra Universo PAC Santander';
    MSG_ERROR						= 'Error';
const
    STR_LINE = 'Linea: ';
var
  	nNroLineaScript, nLineasScript :integer;
    FUniversoRecord 	: TUniversoPACRecord;
    sDescripcionError 	: string;
  	sParseError 		: string;
begin
    result := False;
  	Screen.Cursor := crHourglass;

        try
            spCompararUniversoPACUniversoPACSantander.CommandTimeOut := 5000;
            spCompararUniversoPACUniversoPACSantander.ExecProc;
            result := True;
        except
            on e: exception do begin
            	MsgBoxErr(MSG_PROCESSING_UNIVERSE_FILE, e.Message, MSG_ERROR, MB_ICONERROR);
                FErrorMsg := MSG_PROCESSING_UNIVERSE_FILE;
            end;
        end;

  	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: ParseUniversoPACLine
  Author:    flamas
  Date Created: 01/12/2004
  Description: Parsea una linea proveniente del Universo Santander
  Parameters: sline : string; var UniversoRecord : TUniversoPACRecord
  Return Value: boolean

  Revision 1
  Author: nefernandez
  Date: 16/04/2008
  Description: Se va agregando los registros del archivo de Universo PAC a la
  tabla cliente
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.ParseUniversoPACLine(sline : string; var UniversoRecord : TUniversoPACRecord; var sParseError : string) : boolean;
resourcestring
	  MSG_BANK_CODE_ERROR 		= 'El c�digo de banco es inv�lido.';
    MSG_DOCUMENT_NUMBER_ERROR 	= 'El n�mero de RUT es inv�lido.';
    MSG_STARTING_DATE_ERROR  	= 'La Fecha de Inicio es inv�lida.';
const
    CODIGO_SBEI_BANCO_SANTANDER = 37;
begin
  	Result := False;
    if ( Trim( sline ) = '' ) then Exit;
    try
    	with UniversoRecord do begin
        	sParseError := MSG_BANK_CODE_ERROR;
        	NumeroConvenio := '001' + UpperCase(Copy(sline, 92, 14));
          if ObtenerCodigoConcesionariaNativa=CODIGO_VS then                    //SS_1147Q_NDR_20141202[??]
          begin                                                                 //SS_1147Q_NDR_20141202[??]
            NumeroConvenio := Copy( NumeroConvenio, 6, 12) ;                    //SS_1147Q_NDR_20141202[??]
          end;                                                                  //SS_1147Q_NDR_20141202[??]
          CodigoBancoSBEI := StrToInt(trim(Copy(sline, 11, 3)));
        	NombreBancoSBEI := trim(Copy(sline, 14, 30));
        	RUTSubscriptor := trim(Copy(sline, 77, 9));//trim( Copy( sline, 71, 15 ) );
        	sParseError := MSG_DOCUMENT_NUMBER_ERROR;
        	StrToInt64(Copy(RUTSubscriptor, 1, Length(RUTSubscriptor) - 1));
        	NroCuentaBancaria := trim(Copy(sline, 44, 17));
    			if 	(CodigoBancoSBEI <> CODIGO_SBEI_BANCO_SANTANDER) then NroCuentaBancaria := '';
          	  sParseError := MSG_STARTING_DATE_ERROR;
              FechaInicio := EncodeDate(	StrToInt(Copy(sline,125,4)), StrToInt(Copy(sline,123,2)), StrToInt(Copy(sline,121,2)));
              NroMandato := Trim(Copy(sline,106,15));

            //Revision 1: Se busca si ya existe un NroConvenio, si es as� se compara el
            //campo FechaInicio a fin de tener registrado la �ltima Activacion del Medio Pago
            if (cdsUniversoPACSantander.FindKey([NumeroConvenio])) then begin
                if ( FechaInicio >= cdsUniversoPACSantander.FieldByName('FechaInicio').AsDateTime ) then begin
                    cdsUniversoPACSantander.Edit;
                    cdsUniversoPACSantander.FieldByName('NumeroConvenio').AsString := NumeroConvenio;
                    cdsUniversoPACSantander.FieldByName('CodigoBancoSBEI').AsInteger := CodigoBancoSBEI;
                    cdsUniversoPACSantander.FieldByName('NombreBancoSBEI').AsString := NombreBancoSBEI;
                    cdsUniversoPACSantander.FieldByName('RUTSubscriptor').AsString := RUTSubscriptor;
                    cdsUniversoPACSantander.FieldByName('NroCuentaBancaria').AsString := NroCuentaBancaria;
                    cdsUniversoPACSantander.FieldByName('FechaInicio').AsDateTime := FechaInicio;
                    cdsUniversoPACSantander.FieldByName('NroMandato').AsString := NroMandato;
                    cdsUniversoPACSantander.Post;
                end;
            end
            //Revision 1: Si no exite, se agrega la linea a la tabla cliente
            else begin
                    cdsUniversoPACSantander.Append;
                    cdsUniversoPACSantander.FieldByName('NumeroConvenio').AsString := NumeroConvenio;
                    cdsUniversoPACSantander.FieldByName('CodigoBancoSBEI').AsInteger := CodigoBancoSBEI;
                    cdsUniversoPACSantander.FieldByName('NombreBancoSBEI').AsString := NombreBancoSBEI;
                    cdsUniversoPACSantander.FieldByName('RUTSubscriptor').AsString := RUTSubscriptor;
                    cdsUniversoPACSantander.FieldByName('NroCuentaBancaria').AsString := NroCuentaBancaria;
                    cdsUniversoPACSantander.FieldByName('FechaInicio').AsDateTime := FechaInicio;
                    cdsUniversoPACSantander.FieldByName('NroMandato').AsString := NroMandato;
                    cdsUniversoPACSantander.Post;
            end;
          end;

    except
      	on exception do begin
          	Exit;
        end;
    end;
    Result 		:= True;
    sParseError := '';
end;

{-----------------------------------------------------------------------------
  Function Name: ProcesarNoExistentesUniversoPAC
  Author:    flamas
  Date Created: 14/12/2004
  Description: Procesa los Convenios que NO existen en el Universo PAC
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.ProcesarNoExistentesUniversoPAC : boolean;
resourcestring
  	MSG_ERROR_PROCESSING_UNIVERSE = 'Error al procesar el Universo';
  	MSG_ERROR_GETTING_UNEXISTING_CONTRACTS = 'Error al obtener convenios no existentes en Universo PAC';
    MSG_PROCESSING_UNEXISTING_CONTRACTS	= 'Procesando convenios no existentes en Universo PAC';
    MSG_ERROR = 'Error';
var
    sDescripcionError : string;
    sDescErrorInterfaz : string;
begin
  	Screen.Cursor := crHourglass;
    lblReferencia.Caption := MSG_PROCESSING_UNEXISTING_CONTRACTS;
    pbProgreso.Position := 0;
    pnlAvance.Visible := True;
  	Application.ProcessMessages;

  	try
        spObtenerConveniosPacNoVerificados.Open;
        spObtenerConveniosPacNoVerificados.CommandTimeout := 5000;
        pbProgreso.Max := spObtenerConveniosPacNoVerificados.RecordCount;
    except
      	on e: Exception do begin
        		MsgBoxErr(MSG_ERROR_GETTING_UNEXISTING_CONTRACTS, e.Message, MSG_ERROR, MB_ICONERROR);
          	FErrorMsg := MSG_ERROR_GETTING_UNEXISTING_CONTRACTS;
        end;
    end;

    while ( not spObtenerConveniosPacNoVerificados.EOF )  and ( not FDetenerImportacion ) and ( FErrorMsg = '' ) do begin

    		with spEliminarMedioPagoPAC, Parameters do begin

            ParamByName( '@CodigoConvenio' ).Value := spObtenerConveniosPacNoVerificados.FieldByName( 'CodigoConvenio' ).Value;
            try

                CommandTimeOut := 5000;
                ExecProc;
                sDescripcionError := Trim( VarToStr( ParamByName( '@DescripcionError' ).Value ));
                //if ( sDescripcionError <> '' ) then FErrores.Add( StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]));
                if ( sDescripcionError <> '' ) then begin
                    sDescErrorInterfaz := StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]);
                    FErrores.Add( sDescErrorInterfaz );
                    AgregarErrorInterfaz(sDescErrorInterfaz);
                end;
            except
                on e: Exception do begin
                      MsgBoxErr(MSG_ERROR_PROCESSING_UNIVERSE, e.Message, MSG_ERROR, MB_ICONERROR);
                      FErrorMsg := MSG_ERROR_PROCESSING_UNIVERSE;
                      Break;
                end;
            end;
            spObtenerConveniosPacNoVerificados.Next;
            pbProgreso.StepIt;
            Application.ProcessMessages;
        end;

    end;
    spObtenerConveniosPacNoVerificados.Close;
    pbProgreso.Position := pbProgreso.Max;
    Application.ProcessMessages;
    Screen.Cursor := crDefault;
	  result := ( FErrorMsg = '' ) and ( not FDetenerImportacion );
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarMandatosPendientes
  Author:    flamas
  Date Created: 14/12/2004
  Description:	Actualiza las respuestas de los Mandatos Pendientes
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFRecepcionUniversoSantander.ActualizarMandatosPendientes : boolean;
resourcestring
  	MSG_ERROR_COULD_NOT_UPDATE_WARRANTS = 'No se pudieron actualizar los mandatos';
    MSG_UPDATING_PENDING_WARRANTS 		= 'Actualizando los mandatos pendientes';
begin
  	result := False;
  	Screen.Cursor := crHourGlass;
    lblReferencia.Caption := MSG_UPDATING_PENDING_WARRANTS;
  	lblReferencia.Repaint;
    try
    	spActualizarMandatosPendientes.CommandTimeout := 5000;
    	spActualizarMandatosPendientes.ExecProc;
    	result := True;
    except
        on e: Exception do begin
           MsgBoxErr(MSG_ERROR_COULD_NOT_UPDATE_WARRANTS, e.Message, 'Error', MB_ICONERROR);
           FErrorMsg := MSG_ERROR_COULD_NOT_UPDATE_WARRANTS;
        end;
    end;
  	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReportesErrores
  Author:    flamas
  Date Created: 03/12/2004
  Description: Genera el Reporte de Errores
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  nefernandez
  02/06/2008
  Se llama al reporte de errores que ahora est� en otro formulario
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoSantander.GenerarReportesErrores;
resourcestring
    PROCESSING_ERRORS_FILE				= 'Universo Santander - Errores Procesamiento ';
    REPORT_DIALOG_CAPTION 				= 'Universo Santander - Errores';
    MSG_ERROR_SAVING_ERRORS_FILE 	    = 'Error generando archivo de errores';
    MSG_ERR_REPORT                      = 'El Reporte de Errores se guardo en: %s';
    TITLE_ERR_REPORT                    = 'Reporte de Errores';
    MSG_THERE_IS_NO_BANK                = 'Banco Desconocido';
    MSG_COULD_NOT_OPEN_THE_REPORT       = 'No se puede mostrar el reporte';
var
    sArchivoErroresProcesamiento : string;
  	Config: TRBConfig;
    i: integer;
    HayBanco: boolean;
    iCodSBEI : Integer;
    sNOmbreBanco: string;
    f : TRptRecepcionUniversoSantanderForm;
begin
	if ( FErrores.Count = 0 ) then Exit;

    // Generaci�n de Reportes de Errores
    sArchivoErroresProcesamiento := GoodDir( FSantander_Directorio_Errores ) + PROCESSING_ERRORS_FILE + FormatDateTime ( 'yyyy-mm-dd', Now ) + '.txt';
    try
        FErrores.SaveToFile( sArchivoErroresProcesamiento );
    except
    	on e:exception do begin
        	MsgBoxErr(MSG_ERROR_SAVING_ERRORS_FILE, e.Message, 'Error', MB_ICONERROR);
            Exit;
        end;
    end;

    if NOT FVerReporteErrores then begin
        MsgBox(Format(MSG_ERR_REPORT,[sArchivoErroresProcesamiento]),TITLE_ERR_REPORT,MB_ICONINFORMATION);
        Exit;
    end;

    Application.CreateForm(TRptRecepcionUniversoSantanderForm, f);
    try
        try
            f.MostrarReporte(FCodigoOperacion);
        except
            on E: Exception do begin
                MsgBoxErr(MSG_COULD_NOT_OPEN_THE_REPORT, E.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        f.Release;
    end;

    {
    //Manda los errores al cdsErrores
    qrybanco.Open;

    ///Mover esto a la pag�na de reporte
    cdsErrores.CreateDataSet ;

    i := 0;
    While i < FErrores.Count do begin
           HayBanco := qryBanco.Locate('CodigoBancoSBEI',iVal(Copy(FErrores.Strings[i],1,4)),[]);
           sNombreBanco := iif(HayBanco,qryBanco.FieldByName('Descripcion').asString,MSG_THERE_IS_NO_BANK);
           iCodSBEI := iif(HayBanco, iVal( Copy(FErrores.Strings[i],1,4) ), 0);
           cdsErrores.InsertRecord([iCodSBEI, sNombreBanco, Copy(FErrores.Strings[i],5,Length(FErrores.Strings[i])) ] );
           i := i + 1;
    end;


	// Asigna los Pipelines del reporte
	//ppErroresPipeline.FileName := sArchivoErroresProcesamiento;

    rbiListado.Caption := REPORT_DIALOG_CAPTION;

	// Carga la Configuraci�n de Usuario
    // la funci�n del componente RBInterface no funciona en este caso
    // porque el reporte tiene subreportes y el label est� dentro de un subreporte
    // Adem�s existen 3 labes uno en cada subreporte y no se pueden llamar igual
  	Config := rbiListado.GetConfig;
    if Config.ShowUser then begin
    	if UsuarioSistema <> '' then lbl_usuario.caption := 'Usuario: ' +
        	UsuarioSistema + ' ' else lbl_usuario.Caption := '';
    end else begin
    	lbl_usuario.Caption := '';
    end;

    if Config.ShowDateTime then begin
    	lbl_usuario.caption := lbl_usuario.caption +
        FormatShortDate(Date) + ' ' + FormatTime(Time);
    end;
    rbiListado.Execute(True);
    }
end;

procedure TFRecepcionUniversoSantander.AgregarErrorInterfaz(mDescripcionError: String);
resourcestring
  	MSG_ERROR_PROCESSING_UNIVERSE 	= 'Error al agregar en ErroresInterfases. Linea:   ';
    MSG_ERROR						= 'Error';
const
    STR_LINE = 'Linea: ';
begin
    try
        spAgregarErrorInterfaz.Close;
        spAgregarErrorInterfaz.Parameters.ParamByName('@CodigoOperacionInterfase').Value    := FCodigoOperacion;
        spAgregarErrorInterfaz.Parameters.ParamByName('@CodigoModulo').Value                := RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_PAC;
        spAgregarErrorInterfaz.Parameters.ParamByName('@CodigoError').Value                 := 0;
        spAgregarErrorInterfaz.Parameters.ParamByName('@Fecha').Value                       := NowBase(DMConnections.BaseCAC);
        spAgregarErrorInterfaz.Parameters.ParamByName('@DescripcionError').Value            := mDescripcionError;
        spAgregarErrorInterfaz.ExecProc;
    except
        on e: exception do begin
            MsgBoxErr(MSG_ERROR_PROCESSING_UNIVERSE + CRLF + STR_LINE + mDescripcionError, e.Message, MSG_ERROR, MB_ICONERROR);
            FErrorMsg := MSG_ERROR_PROCESSING_UNIVERSE;
        end;
    end;
end;

function TFRecepcionUniversoSantander.ObtenerDatosProcesoUniversoSantander(CodigoOperacionInterfase: integer; var LineasArchivo: integer;
            var ConveniosDuplicados: integer; var RegistrosExito: integer; var RegistrosError: integer): boolean;
resourcestring
    MSG_ERROR = 'No se pudo obtener los datos de resumen de proceso';
begin
    try
        spObtenerDatosProcesoUniversoSantander.Close;
        spObtenerDatosProcesoUniversoSantander.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
        spObtenerDatosProcesoUniversoSantander.ExecProc;

        LineasArchivo := spObtenerDatosProcesoUniversoSantander.Parameters.ParamByName('@LineasArchivo').Value;
        ConveniosDuplicados := spObtenerDatosProcesoUniversoSantander.Parameters.ParamByName('@ConveniosDuplicados').Value;
        RegistrosExito := spObtenerDatosProcesoUniversoSantander.Parameters.ParamByName('@RegistrosExito').Value;
        RegistrosError := spObtenerDatosProcesoUniversoSantander.Parameters.ParamByName('@RegistrosError').Value;

        Result := true;
    except
       on e: Exception do begin
           MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
           Result := False;
       end;
    end;
end;

function TFRecepcionUniversoSantander.MsgProcesoFinalizadoSantander(sMensajeOK, sMensajeError, sTitulo: AnsiString; RLineas, RDuplicados, RExito, RError: integer):boolean;
resourcestring
    MSG_PROC_INFO                       =   CrLf+'Cantidad de Lineas del Archivo: %d '+
                                            CrLf+'Cantidad de Convenios Duplicados: %d ' +
                                            CrLf+'Cantidad de Registros Procesados con �xito: %d ' +
                                            CrLf+'Cantidad de Registros Procesados con Error: %d ';
    MSG_ASK_SHOW_ERROR_REPORT           =   CrLf + 'Desea ver el Reporte de Errores?.';
var
    Mensaje     : AnsiString;
    Respuesta   : Boolean;
begin
    Mensaje :=  Format(MSG_PROC_INFO,[RLineas, RDuplicados, RExito, RError]);

    if RError > 0 then begin
        Mensaje := sMensajeError + Mensaje +  MSG_ASK_SHOW_ERROR_REPORT;
        Respuesta := (MsgBox(Mensaje, sTitulo, MB_ICONWARNING + MB_YESNO) = mrYes);
    end
    else begin
        Mensaje := sMensajeOK + Mensaje;
        Respuesta := False;
        MsgBox(Mensaje, sTitulo, MB_ICONINFORMATION);
    end;
    Result :=  Respuesta;
end;

{-----------------------------------------------------------------------------
  Function Name: rbiListadoExecute
  Author:    flamas
  Date Created: 04/07/2005
  Description: ejecuta el informe
  Parameters: Sender: TObject; var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoSantander.rbiListadoExecute(Sender: TObject; var Cancelled: Boolean);
begin
	try
		try
			Screen.Cursor := crHourGlass;
		finally
			Screen.Cursor := crDefault;
		end;
	except
		on E: Exception do begin
			Cancelled := True;
			MsgBoxErr(MSG_ERROR_DATOS_LISTADO, E.Message, Self.Caption, MB_ICONSTOP);
		end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    flamas
  Date Created: 03/12/2004
  Description: Borra el Universo PAC Anterior y Carga el nuevo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 2
  nefernandez
  16/04/2008
  SS 682: Se agregan nuevos pasos al proceso de recepci�n de Universo PAC:
  - Se carga la tabla UniversoPACSantander con los datos del archivo de recepci�n
  - Se compara el UniversoPAC del CAC contra el Universo recibido a fin de dar
    de baja los medios de pago que no figuran en el ultimo archivo
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoSantander.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/06/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 01/07/2005
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog(CantidadErrores:integer):boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : string;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_SUCCEDED 					        = 'El proceso finaliz� con �xito';
    MSG_PROCESS_FINALIZED_WITH_ERRORS		  = 'El proceso finaliz� con errores';
    MSG_PROCESS_COULD_NOT_BE_COMPLETED 		= 'El proceso no se pudo completar';
    MSG_ERROR_CANNOT_OPEN_UNIVERSE_FILE 	= 'No se puede abrir el archivo de universo %s';
    MSG_ERRROR_UNIVERSE_FILE_IS_EMPTY 		= 'El archivo de universo seleccionado est� vac�o';
var
    TotalRegistros: integer;
    CantidadErrores: integer;
    CantidadLineasArchivo: integer;
	CantidadConveniosDuplicados: integer;
	CantidadRegistrosExito: integer;
	CantidadRegistrosError: integer;
begin
	FUniversoTXT := TStringList.Create;
 	// Crea las listas de Errores
	FErrorMsg := '';
  FErrores := TStringList.Create;

	// Deshabilita los botones
	btnCancelar.Enabled := True;
	btnProcesar.Enabled := False;
	btnSalir.Enabled := False;
	btnAbrirArchivo.Enabled := False;
	edOrigen.Enabled := False;

	FDetenerImportacion := False;

	try
		try
      //Abre el archivo y lo pone en el stringlist
			FUniversoTxt.text:= FileToString(edOrigen.text);

			// Verifica si el Archivo Contiene alguna linea
			if ( FUniversoTXT.Count = 0 ) then

                //Muestra un cartel indicando que el archivo esta vacio
				MsgBox(MSG_ERRROR_UNIVERSE_FILE_IS_EMPTY, Caption, MB_ICONERROR)

			else begin

                //Confirma las cantidades
                if not ConfirmaCantidades(FUniversoTXT.Count) then Exit;

                //Obtiene la cantidad de registros que hay en el archivo
                //TotalRegistros := FUniversoTXT.Count;

                //Analiza el archivo para ver si hay errores de sintaxis
        				if  AnalizarUniversoTxt then begin

                	// Si el Archivo es v�lido Inicia la Transacci�n
      					if RegistrarOperacion and
                            BorrarUniversoPAC and
                            BorrarUniversoPACSantander and //Revision 2
                                CopiarConveniosAUniversoPAC and
                                CargarUniversoPACSantander and  //Revision 2
                                CompararUniversoPACUniversoPACSantander and  // Universo 2
                    						    VerificarUniverso and
                	          					ProcesarNoExistentesUniversoPAC and
                       			        			ActualizarMandatosPendientes then begin

                            //Obtengo la cantidad de errores
                            ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);

                            //Obtiene los datos de resumen del proceso
                            ObtenerDatosProcesoUniversoSantander(FCodigoOperacion, CantidadLineasArchivo, CantidadConveniosDuplicados,
                                                                 CantidadRegistrosExito, CantidadRegistrosError);

                            //Actualizo el log al Final
                            ActualizarLog(CantidadErrores);

                            //Obtiene la cantidad de convenios que hay en el archivo
                            TotalRegistros := cdsUniversoPACSantander.RecordCount;

                            //Muestro cartel de finalizacion
                            FVerReporteErrores := MsgProcesoFinalizadoSantander( MSG_PROCESS_SUCCEDED,
                                                                    MSG_PROCESS_FINALIZED_WITH_ERRORS, Caption,
                                                                     CantidadLineasArchivo, CantidadConveniosDuplicados,
                                                                     CantidadRegistrosExito, CantidadRegistrosError);
                            //Si hubo errores generamos el reporte de errores
                            ///if (FErrores.Count > 0) then GenerarReportesErrores;
                            if (CantidadRegistrosError > 0) then GenerarReportesErrores;
                            //Muevo el archivo procesado
                            MoverArchivoProcesado(Caption,edOrigen.Text, FSTD_DirectorioUniversoProcesado);

					end
					else begin

              // Si hubo alg�n error Deshace la Transacci�n
              MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);

					end
        end;
            end;
		except
			on e: Exception do begin
            MsgBoxErr(Format(MSG_ERROR_CANNOT_OPEN_UNIVERSE_FILE, [ExtractFilePath(edOrigen.Text)]), e.Message, 'Error', MB_ICONERROR);
            FErrorMsg := MSG_ERROR_CANNOT_OPEN_UNIVERSE_FILE;
			end;
		end;
	finally
      FreeAndNil(FUniversoTXT);
      FreeAndNil(FErrores);
      // Lo desactiva para poder Cerrar el Form
      btnCancelar.Enabled := False;
      Close;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    flamas
  Date Created: 07/12/2004
  Description: Detiene el proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoSantander.btnCancelarClick(Sender: TObject);
resourcestring
	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 04/07/2005
  Description:  permito salir del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoSantander.btnSalirClick(Sender: TObject);
begin
    close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 04/07/2005
  Description:  impide salir si esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoSantander.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 04/07/2005
  Description:  lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoSantander.FormClose(Sender: TObject;var Action: TCloseAction);
begin
   Action := caFree;
end;


end.
