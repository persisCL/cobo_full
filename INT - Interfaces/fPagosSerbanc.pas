{********************************** Unit Header ********************************
File Name : fPagosSerbanc.pas
Author : ndonadio
Date Created: 22/08/2005
Language : ES-AR
Description : Procesa un archivo de rendicion de pagos enviado por Serbanc
              y aplica dichos pagos a los comprobantes.


Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

  
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto
*******************************************************************************}
unit fPagosSerbanc;

interface          

uses
    //Utiles
    DMConnection,
    UtilDB,
    Util,
    UtilProc,
    StrUtils,
    PeaProcs,
    PeaTypes,
    ComunesInterfaces,
    ConstParametrosGenerales,
    fReportePagosSerbanc,
    //Otros
    Windows, Messages, SysUtils, Variants, Classes,  Graphics,  Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB, ppDBPipe,
    ppPrnabl, ppClass, ppCtrls, ppBands,  ppCache,  ppComm, ppRelatv, ppProd,
    ppReport, UtilRB;

type
    TRegistroPago = record
        TipoDeudor          : byte;
        RUT                 : AnsiString;
        NumeroConvenio      : AnsiString;
        CodConvenio         : Integer;
        NumeroComprobante   : int64;
        TipoComprobante     : AnsiString;
        FormaPago           : byte;
        Importe             : int64;
        FechaPago           : TDateTime; 
        NroCheque           : AnsiString;
        FechaCheque         : TDateTime;
        Banco               : Integer;
        RUTEmisor           : AnsiString;
        NroCuenta           : AnsiString;
        Error               : AnsiString;
        EsValido            : boolean;
    end;

  TfrmRecepcionArchivoPagosSerbanc = class(TForm)
    Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    txtOrigen: TPickEdit;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    OpenDialog: TOpenDialog;
    //En desuso
    //SPRegistrarPagoSerbanc: TADOStoredProc;
    spPagar: TADOStoredProc;
    spAgregarDetalle: TADOStoredProc;
    StringField3: TStringField;
    IntegerField2: TIntegerField;
    StringField4: TStringField;
    spValidarPagoSerbanc: TADOStoredProc;
    StringField7: TStringField;
    IntegerField4: TIntegerField;
    StringField8: TStringField;
    procedure btnProcesarClick(Sender: TObject);
    procedure txtOrigenButtonClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ImgAyudaClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnSalirClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MostrarReporte;
  private
    { Private declarations }
    FOrigen : AnsiString;
    FProcesando : Boolean;
    FCancelar : Boolean;
    FCodigoOperacion : Integer;
    FFechaProceso : TDateTime;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function  AbrirArchivo(var strList: TStringList): boolean;
    function  ValidarHeader(strList: TStringList; var Error: AnsiString): boolean;
    procedure HabilitarProceso;
    function  RegistrarOperacion(var Error: AnsiString): boolean;
    function  ObtenerRegistro(Linea: AnsiString; var Registro: TRegistroPago): boolean;
    function  ProcesarRegistro(var Registro: TRegistroPago; var Error: AnsiString): boolean;
    function  RegistrarError(Linea: AnsiString; Registro: TRegistroPago; var Error: AnsiString): Boolean;
    function  RegistrarFinalizacionEnLog: boolean;
    //Agregados en rev.
    function ValidarPagoSerbanc(var Registro: TRegistroPago; var DescriError: AnsiString): Boolean;
    function RegistrarPago(var Registro: TRegistroPago): Boolean;
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): boolean;
  end;

var
  frmRecepcionArchivoPagosSerbanc: TfrmRecepcionArchivoPagosSerbanc;

implementation

{$R *.dfm}


{******************************** Function Header ******************************
Function Name: Inicializar
Author : lgisuk
Date Created : 02/12/2005
Description :  Inicializaci�m de Este Formulario
Parameters :
Return Value : None
*******************************************************************************}
function TfrmRecepcionArchivoPagosSerbanc.Inicializar(Titulo: AnsiString): boolean;
resourcestring
    ERROR_CANNOT_INITIALIZE     = 'No se puede inicializar la interfaz.';
    MSG_NOTHING_TO_PROCESS      = 'No hay Activaciones Pendientes.';
    ERROR_CANNOT_GET_PARAMETER  = 'Error obteniendo el parametro general %s';
const
    DIR_ORIGEN_PAGOS_SERBANC = 'DIR_ORIGEN_PAGOS_SERBANC';
begin
    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    // Le pongo de caption lo que trae de parametro...
    Caption :=  Titulo;
    //Centro el form
  	CenterForm(Self);
    // Cargar el parametro general de directorio de origen standard
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_ORIGEN_PAGOS_SERBANC, FOrigen) then begin
        MsgBoxErr(ERROR_CANNOT_INITIALIZE, Format(ERROR_CANNOT_GET_PARAMETER, [DIR_ORIGEN_PAGOS_SERBANC]), Caption, MB_ICONSTOP);
        Exit;
    end;
    //txtOrigen.Text := GoodDir(FOrigen);
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : ndonadio
Date Created : 22/08/2005
Description : muestra un mensaje de ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoPagosSerbanc.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_ACCIONES_SERBANC  = ' ' + CRLF +
                          'El Archivo de Pagos es enviado' + CRLF +
                          'por SERBANC para informar al ESTABLECIMIENTO ' + CRLF +
                          'de las Notas de Cobro que se han cobrado.' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: PAG_REC_YYYYMMDD_SSS.txt' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if FProcesando then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_ACCIONES_SERBANC, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : ndonadio
Date Created : 22/08/2005
Description :   Le cambia el puntero al mouse a una manito cuando pasa por la ayuda
Parameters : Sender: TObject; Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoPagosSerbanc.ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
begin
    if FProcesando then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{******************************** Function Header ******************************
Function Name: txtOrigenButtonClick
Author : lgisuk
Date Created : 02/12/2005
Description : Selecci�no el Archivo a Procesar
Parameters : None
Return Value : AnsiString
*******************************************************************************}
procedure TfrmRecepcionArchivoPagosSerbanc.txtOrigenButtonClick(Sender: TObject);

    {******************************** Function Header ******************************
    Function Name: ObtenerFiltro
    Author : ndonadio
    Date Created : 10/08/2005
    Description : Filtra que solo se puedan seleccionar archivos con extension
                  TXT.
    Parameters : None
    Return Value : AnsiString
    *******************************************************************************}
    Function ObtenerFiltro : AnsiString;
    Const
        FILE_TITLE	   = 'Archivo de Pagos Serbanc|';
        FILE_NAME = 'PAG_REC_';
        FILE_EXTENSION = '.txt';
    begin
        Result:= FILE_TITLE + FILE_NAME + '*' + FILE_EXTENSION;
    end;

resourcestring
    MSG_ERROR = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST   = 'El archivo %s no existe';
  	MSG_ERROR_INVALID_FILENAME 		= 'El nombre del archivo %s es inv�lido';
  	MSG_FILE_ALREADY_PROCESSED 		= 'El archivo %s ya fue procesado';
var
    Filtro : String;
begin
    //obtengo el filtro a aplicar a este tipo de archivos
    Filtro := ObtenerFiltro;
    btnProcesar.Enabled := False;

    Opendialog.InitialDir := txtOrigen.Text;;
  	OpenDialog.FileName := '';OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if opendialog.execute then begin

        txtOrigen.text:=UpperCase( opendialog.filename );

        if not FileExists( txtOrigen.text ) then begin
          	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

      	{if  VerificarArchivoProcesado( DMConnections.BaseCAC, txtOrigen.text ) then begin
    			  MsgBox (Format(MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtOrigen.text)]),Caption, MB_ICONSTOP);
            Exit;
      	end;}

        btnProcesar.Enabled := True;
	  end;

end;

{******************************** Function Header ******************************
Function Name: AbrirArchivo
Author : ndonadio
Date Created : 23/08/2005
Description :  Valida la estructura del nombre del archivo y lo abre...
Parameters : var strList: TStringList
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoPagosSerbanc.AbrirArchivo(var strList: TStringList): boolean;
resourcestring
    ERROR_CANNOT_OPEN_FILE          = 'No se puede abrir el archivo %s.';
begin
    Result := False;
    strList := TStringList.Create;
    try
        //Cargo el archivo en un string list
        strList.LoadFromFile(txtOrigen.text);
        pbProgreso.Min := 0;
        pbProgreso.Max := strList.Count;
        Result := True;
    except
        MsgBox (Format(ERROR_CANNOT_OPEN_FILE, [txtOrigen.text]),Caption, MB_ICONSTOP);
    end;

end;

{******************************** Function Header ******************************
Function Name: ValidarHeader
Author : ndonadio
Date Created : 23/08/2005
Description :  Valida el header del Archivo...
Parameters : strList: TStringList; var Error: AnsiString
Return Value : boolean
*******************************************************************************
Revision 1
lgisuk
03/08/2007
Cambio de StrToInt a StrToInt64
*******************************************************************************}
function TfrmRecepcionArchivoPagosSerbanc.ValidarHeader(strList: TStringList;var Error: AnsiString): boolean;
resourcestring
    ERROR_FECHA_HEADER_INVALIDA = 'El valor "%s" en el header no es una fecha v�lida';
    ERROR_CANT_REG_INVALIDA     = 'El valor "%s" en el header no es una cantidad v�lida';
    ERROR_MONTO_INVALIDO        = 'El valor "%s" en el header no es un monto v�lido';
    ERROR_CANT_NO_CONCUERDA     = 'La cantidad de registros ( %d ) no coincide con la informada en el header ( %d ).';
    ERROR_MONTO_NO_CONCUERDA    = 'El monto total ( %d ) no coincide con el informado en el header ( %d )';
    ERROR_IMPORTE_INVALIDO_LINEA= 'Se ha encontrado un importe inv�lido (%s) en la l�nea %d del archivo. ';
var
    i           : integer;      // para la iteracion...
    H_Registros : integer;      // la cantidad de registros en el header
    H_Monto     : int64;        // el monto total informado en el header
    Importe     : int64;        // el importe de un registro
    Total       : int64;        // el acumulador de los importes de los registros
    sFecha      : AnsiString;
    Fecha       : TDateTime;
begin
    Result := False;
    sFecha :=  Copy(strList.Strings[0], 1, 14);
    if not FechaHoraAAAAMMDDToDateTime(DMConnections.BaseCAC, sFecha, Fecha, Error) then begin 
        Error := Format(ERROR_FECHA_HEADER_INVALIDA, [Copy(strList.Strings[0], 1, 8)]) + CRLF + Error;
        Exit;
    end;
    try
        H_Registros := StrToInt( Copy(strList.Strings[0], 15, 7) );
    except
        Error := Format(ERROR_CANT_REG_INVALIDA, [Copy(strList.Strings[0], 15, 7)]);
        Exit;
    end;
    if H_Registros <> (strList.Count - 1) then begin
        Error := Format(ERROR_CANT_NO_CONCUERDA, [ (strList.Count - 1),H_Registros]);
        Exit;
    end;
    try 
        H_Monto := StrToInt( Copy(strList.Strings[0], 22, 17));
    except
        Error := Format(ERROR_MONTO_INVALIDO, [Copy(strList.Strings[0], 22, 17)]);
        Exit;
    end;
    Total   := 0;
    Importe := 0;
    for i := 1 to strList.Count - 1 do begin
        try
            Importe := StrToInt64( Copy(strList.Strings[i], 45, 16) );
        except
            Error := Format(ERROR_IMPORTE_INVALIDO_LINEA, [(Copy(strList.Strings[i], 45, 16)), i]) + Format(ERROR_MONTO_NO_CONCUERDA, [Total, H_Monto]);
            Exit;
        end;
        Total := Total + Importe;
    end;
    if Total <> H_Monto then begin  
        Error := Format(ERROR_MONTO_NO_CONCUERDA, [Total, H_Monto]);
        Exit;
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: HabilitarProceso
Author : ndonadio
Date Created : 23/08/2005
Description :   Segun el valor de FProcesando setea diversas variables y objetos
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoPagosSerbanc.HabilitarProceso;
begin
    PnlAvance.Visible := FProcesando;
    btnProcesar.Enabled := not FProcesando;
    btnCancelar.Enabled := FProcesando;
    btnSalir.Enabled := not FProcesando;
    FCancelar := False;
    // Actualizo el cursor
    if FProcesando then Cursor := crHourglass else Cursor := crDefault;
end;

{******************************** Function Header ******************************
Function Name: RegistrarOperacion
Author : ndonadio
Date Created : 22/08/2005
Description : Registra el inicio de la operacion en el log de operaciones de
              interfases.
Parameters : None
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoPagosSerbanc.RegistrarOperacion(var Error: AnsiString): boolean;
resourcestring
    MSG_ERROR_NO_PUEDE_OBTENER_FECHAPROCESO = 'No se puede obtener la fecha del proceso';
begin
    //registrar operacion
    Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRADA_PAGOS_SERBANC, ExtractFileName(txtOrigen.Text), UsuarioSistema, '', True, False, NowBase(DMCOnnections.BaseCAC),0, FCodigoOPeracion, Error);
    try
        if Result then begin
            //obtengo la fecha del proceso
            FFechaProceso := QueryGetValueDateTime(DMCOnnections.BaseCAC, FORMAT( 'SELECT Fecha FROM LogoperacionesINterfases (NOLOCK) WHERE CodigoOperacionInterfase = %d', [FCodigoOPeracion]));
            Result := True
        end;
    except
        Error := MSG_ERROR_NO_PUEDE_OBTENER_FECHAPROCESO;
       Result := False;
    end;
end;

{******************************** Function Header ******************************
Function Name: ObtenerRegistro
Author : ndonadio
Date Created : 22/08/2005
Description :   Parsea una linea del archivo y carga los datos en un registro
                valida que sean correctos desde el punto de vista del formato.
                Los registros inv�lidos los agrega al log de errores.
                Devuelve False si es un error grave y se debe cancelar la ejecuci�n.
Parameters : Linea: AnsiString; var Registro: TRegistroPago
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoPagosSerbanc.ObtenerRegistro(Linea: AnsiString;var Registro: TRegistroPago): boolean;

    {******************************** Function Header ******************************
    Function Name: ObtenerCodigoConvenio
    Author : lgisuk
    Date Created : 02/12/2005
    Description :  Obtengo el codigo de convenio
    Parameters :
    Return Value : boolean
    *******************************************************************************}
    function ObtenerCodigoConvenio(NumeroConvenio: AnsiString): Integer;
    var
        strSQL :AnsiString;
    begin
        strSQl := 'SELECT ISNULL( (SELECT CodigoConvenio FROM Convenio (NOLOCK) WHERE NumeroConvenio = ''%s''), -1)';
        strSQL := Format(strSQL, [Trim(NumeroConvenio)]);
        Result := QueryGetValueInt(DMCOnnections.BaseCAC, strSQL);
        if Result = 0 then Raise Exception.Create('');
    end;

resourcestring
    ERROR_TIPODEUDOR        = '"%s" no es un valor v�lido para ' + 'el tipo de deudor';
    ERROR_RUT               = '"%s" no es un valor v�lido para ' + 'el numero de RUT';
    ERROR_CODCLIENTE        = '"%s" no es un valor v�lido para ' + 'el C�digo de cliente (N�mero de Convenio)';
    ERROR_NROCOMPROBANTE    = '"%s" no es un valor v�lido para ' + 'el N�mero de Comprobante';
    ERROR_TIPOCOMPROBANTE   = '"%s" no es un valor v�lido para ' + 'el Tipo de Comprobante';
    ERROR_FORMAPAGO         = '"%s" no es un valor v�lido para ' + 'la forma de Pago';
    ERROR_IMPORTE           = '"%s" no es un valor v�lido para ' + 'el importe';
    ERROR_FECHAPAGO         = '"%s" no es un valor v�lido para ' + 'la fecha de pago';
    ERROR_FECHACHEQUE       = '"%s" no es un valor v�lido para ' + 'la Fecha del Cheque';
    ERROR_BANCO             = '"%s" no es un valor v�lido para ' + 'el C�digo de Banco';
    ERROR_RUTEMISOR         = '"%s" no es un valor v�lido para ' + 'RUT del Emisor del Cheque';
    ERROR_CODCLIENTE_NOEXISTE = 'El valor "%s" no es un Numero de Convenio existente';
begin
    Result := False;
    Registro.EsValido := False;
    try
        //Tipo Deudor
        Registro.Error              := ERROR_TIPODEUDOR;
        Registro.TipoDeudor         := StrToInt(Copy( Linea, 1, 1));
        //Rut
        Registro.Error              := ERROR_RUT;
        Registro.RUT                := Copy( Linea, 2, 10 );
        //Numero Convenio
        Registro.Error              := ERROR_CODCLIENTE;
        Registro.NumeroConvenio     := Copy( Linea, 12, 17);
        if ObtenerCodigoConcesionariaNativa=CODIGO_VS then                      //SS_1147Q_NDR_20141202[??]
        begin                                                                   //SS_1147Q_NDR_20141202[??]
          Registro.NumeroConvenio     := Copy( Registro.NumeroConvenio, 6, 12); //SS_1147Q_NDR_20141202[??]
        end;                                                                    //SS_1147Q_NDR_20141202[??]
        //Codigo Convenio
        Registro.Error              := ERROR_CODCLIENTE_NOEXISTE;
        Registro.CodConvenio        := ObtenerCodigoConvenio( Registro.NumeroConvenio );
        //Numero Comprobante
        Registro.Error              := ERROR_NROCOMPROBANTE;
        Registro.NumeroComprobante  := StrToInt(Copy( Linea, 29, 12));
        //Tipo Comprobante
        Registro.Error              := ERROR_TIPOCOMPROBANTE;
        Registro.TipoComprobante    := Copy( Linea, 41, 2);
        //Forma de Pago
        Registro.Error              := ERROR_FORMAPAGO;
        Registro.FormaPago          := StrToInt(Copy( Linea, 43, 2));
        //Importe
        Registro.Error              := ERROR_IMPORTE;
        Registro.Importe            := StrToInt(Copy( Linea, 45, 16));
        //Fecha de Pago
        if not FechaHoraAAAAMMDDToDateTime( DMConnections.BaseCAC, Copy(Linea, 61, 8) + '000000', Registro.FechaPago, Registro.Error) then begin
            Registro.Error := ERROR_FECHAPAGO + '(' + Registro.Error + ')';
            Exit;
        end;
        //Numero de Cheque
        Registro.NroCheque          := Copy( Linea, 69, 20);
        if trim(Registro.NroCheque) = '00000000000000000000' then  Registro.NroCheque := '';
        //Fecha del Cheque
        if trim(Copy( Linea, 89, 8)) <> '00000000' then begin
            if not FechaHoraAAAAMMDDToDateTime( DMConnections.BaseCAC, Copy( Linea, 89, 8) + '000000', Registro.FechaCheque, Registro.Error) then begin
                Registro.Error := ERROR_FECHACHEQUE + '(' + Registro.Error + ')';
                Exit;
            end;
        end else begin
            Registro.FechaCheque   := NullDate;
        end;
        //Banco
        Registro.Error              := ERROR_BANCO;
        Registro.Banco              := StrToInt(Copy( Linea, 97, 3));
        //Rut Emisor
        Registro.Error              := ERROR_RUTEMISOR;
        Registro.RUTEmisor          := Copy( Linea, 100, 9);
        //Numero de Cuenta
        Registro.NroCuenta          := Copy( Linea, 109, 30);
        //Fin
        Registro.Error              := '';
        Registro.EsValido           := True;
        Result := True;
    except
    end;
end;

{******************************** Function Header ******************************
Function Name: RegistrarPago
Author : vpaszkowicz
Date Created : 04/02/2008
Description : Graba un pago de manera transaccional.
Parameters : var Registro: TRegistroPago
Return Value : Boolean
*******************************************************************************}
function TfrmRecepcionArchivoPagosSerbanc.RegistrarPago(var Registro: TRegistroPago): Boolean;
resourceString
    DESCRI_PAGO = 'Pago recibido de Serbanc';
    CODIGO_DOCUMENTO_RUT = 'RUT';
var
    CodigoTipoMedioPago, CodigoConceptoPago: integer;
    NumeroPago: Int64;
    DescripcionPago, CodigoRut: string;
begin
    NumeroPago := 0;
    Result := False;
    spPagar.Close;
    try
        CodigoTipoMedioPago := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CANAL_PAGO_SERBANC()');
{INICIO: TASK_005_JMA_20160418		
        CodigoConceptoPago := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_CONCEPTO_PAGO_SERBANC()');
}
		CodigoConceptoPago := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_CONCEPTO_PAGO_SERBANC(dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}		
        DescripcionPago := DESCRI_PAGO;
        CodigoRut := CODIGO_DOCUMENTO_RUT;
        //if not DMConnections.BaseCAC.InTransaction then                                     //SS_1385_NDR_20150922
        //begin                                                                               //SS_1385_NDR_20150922
            //DMConnections.BaseCAC.BeginTrans;                                               //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('BEGIN TRAN fPagosSerbanc');                        //SS_1385_NDR_20150922
            spPagar.Parameters.ParamByName('@TipoComprobante').Value := 'NK';
            spPagar.Parameters.ParamByName('@NumeroComprobante').Value := Registro.NumeroComprobante;
            spPagar.Parameters.ParamByName('@CodigoEstadoDebito').Value := NULL;
            spPagar.Parameters.ParamByName('@CodigoTipoMedioPago').Value := CodigoTipoMedioPago;
            spPagar.Parameters.ParamByName('@CodigoFormaPago').Value := Registro.FormaPago;
            spPagar.Parameters.ParamByName('@Importe').Value := (Registro.Importe * 100);
            spPagar.Parameters.ParamByName('@FechaHoraPago').Value := Registro.FechaPago;
            spPagar.Parameters.ParamByName('@ChequeMonto').Value := NULL;
            if  (spPagar.Parameters.ParamByName('@CodigoFormaPago').Value = 4 ) or(spPagar.Parameters.ParamByName('@CodigoFormaPago').Value = 11 ) then begin
                spPagar.Parameters.ParamByName('@ChequeNumero').Value := IIF(Registro.NroCheque = '', NULL, Registro.NroCheque);
                spPagar.Parameters.ParamByName('@ChequeFecha').Value := Registro.FechaCheque;
                spPagar.Parameters.ParamByName('@ChequeCodigoBanco').Value := Registro.Banco;
                spPagar.Parameters.ParamByName('@ChequeCodigoDocumento').Value := CodigoRut;
                spPagar.Parameters.ParamByName('@ChequeNumeroDocumento').Value := IIF(Registro.RUTEmisor = '', NULL, Registro.RUTEmisor);
                spPagar.Parameters.ParamByName('@ChequeTitular').Value := NULL; // no viene en el archivo
            end else begin
                spPagar.Parameters.ParamByName('@ChequeNumero').Value := NULL;
                spPagar.Parameters.ParamByName('@ChequeFecha').Value := NULL;
                spPagar.Parameters.ParamByName('@ChequeCodigoBanco').Value := NULL;
                spPagar.Parameters.ParamByName('@ChequeCodigoDocumento').Value := NULL;
                spPagar.Parameters.ParamByName('@ChequeNumeroDocumento').Value := NULL;
                spPagar.Parameters.ParamByName('@ChequeTitular').Value := NULL; // no viene en el archivo
            end;
            spPagar.Parameters.ParamByName('@NumeroPago').Value := NULL;
            spPagar.Parameters.ParamByName('@CodigoConceptoPago').Value := CodigoConceptoPago;
            spPagar.Parameters.ParamByName('@DescripcionPago').Value := DescripcionPago;
            spPagar.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
            spPagar.ExecProc;
            NumeroPago := spPagar.Parameters.ParamByName('@NumeroPago').Value;

            // Agrego tambien en la tabla DetalleComprobantesRecibidos
            spAgregarDetalle.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
            spAgregarDetalle.Parameters.ParamByName('@TipoComprobante').Value := 'NK';
            spAgregarDetalle.Parameters.ParamByName('@NumeroComprobante').Value := Registro.NumeroComprobante ;
            spAgregarDetalle.Parameters.ParamByName('@FechaHoraPago').Value := Registro.FechaPago;
            spAgregarDetalle.Parameters.ParamByName('@NumeroPago').Value := NumeroPago;
            spAgregarDetalle.Parameters.ParamByName('@CodigoTipoDeudor').Value := Registro.TipoDeudor;
            spAgregarDetalle.ExecProc;

            //DMConnections.BaseCAC.CommitTrans;                                                                //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('COMMIT TRAN fPagosSerbanc');					                                //SS_1385_NDR_20150922
            Result := (NumeroPago > 0);
        //end else begin                                                                                       //SS_1385_NDR_20150922
        //    raise Exception.Create('No se pudo iniciar una transacci�n para registrar un pago en el CAC');   //SS_1385_NDR_20150922
        //end;                                                                                                 //SS_1385_NDR_20150922
    except
        on e: Exception do begin
            //if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;                  //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fPagosSerbanc END');	    //SS_1385_NDR_20150922

            Registro.Error := e.Message;
        end;
    end;

end;

{******************************** Function Header ******************************
Function Name: ValidarPagoSerbanc
Author : vpaszkowicz
Date Created : 04/02/2008
Description : Valida que se pueda registrar un pago y sino graba el mensaje de
error en el registro.
Parameters : var Registro: TRegistroPago
Return Value : Boolean
*******************************************************************************}
function TfrmRecepcionArchivoPagosSerbanc.ValidarPagoSerbanc(var Registro: TRegistroPago; var DescriError: AnsiString): Boolean;
begin
    Result := False;
    try
        DescriError := '';
        // Cargamos los parametros
        spValidarPagoSerbanc.Parameters.Refresh;
        spValidarPagoSerbanc.Parameters.ParamByName('@CodigoOperacion').Value := FCodigoOperacion;
        spValidarPagoSerbanc.Parameters.ParamByName('@TipoComprobante').Value := Registro.TipoComprobante;
        spValidarPagoSerbanc.Parameters.ParamByName('@NumeroComprobante').Value := Registro.NumeroComprobante;
        spValidarPagoSerbanc.Parameters.ParamByName('@CodigoFormaPago').Value := Registro.FormaPago;
        spValidarPagoSerbanc.Parameters.ParamByName('@FechaHoraPago').Value := Registro.FechaPago;
        spValidarPagoSerbanc.Parameters.ParamByName('@DescripcionError').Value := '';
        spValidarPagoSerbanc.ExecProc;
        if spValidarPagoSerbanc.Parameters.ParamByName('@RETURN_VALUE').Value = 0 then begin
            Registro.Error := '';
            // si tengo algo en la descripcion del error, es porque
            // el pago es contra una NK que NO se envi� a Serbanc
            Result := True;
        end else begin
            Registro.Error := spValidarPagoSerbanc.Parameters.ParamByName('@DescripcionError').Value;
        end;
    except
        on e: exception do begin
            DescriError := e.Message;
            //Registro.Error := 'No se pudo procesar el registro ' + e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ProcesarRegistro
Author : ndonadio
Date Created : 22/08/2005
Description :   Si no pudiera procesarlo lo agrega al log de errores.
                Devuelve False si es un error grave y se debe cancelar la ejecuci�n.
Parameters : Registro: TRegistroPago
Return Value : boolean
Revision :1
    Author : vpaszkowicz
    Date : 04/02/2008
    Description : Cambio la forma de procesar, para separar la validaci�n de la
    grabaci�n en s� del pago.
*******************************************************************************}
function TfrmRecepcionArchivoPagosSerbanc.ProcesarRegistro(var Registro: TRegistroPago; var Error: AnsiString): boolean;
resourcestring
    MSG_ERROR_INVALID_DATE = 'La fecha del pago es posterior a la del proceso.';
begin
    Result := False;
    if Registro.FechaPago > FFechaProceso then begin
        Result := True;
        Registro.Error := MSG_ERROR_INVALID_DATE;
        Exit;
    end;
    if ValidarPagoSerbanc(Registro, Error) then
        Result := RegistrarPago(Registro)
    else Result := (Error = '');
end;

{******************************** Function Header ******************************
Function Name: RegistrarError
Author : ndonadio
Date Created : 22/08/2005
Description :   Crea un registro del error en ErroresInterfases
Parameters : var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmRecepcionArchivoPagosSerbanc.RegistrarError(Linea: AnsiString; Registro: TRegistroPago; var Error: AnsiString): Boolean;
begin
    Result := False;
    try
        AgregarErrorInterfase(DMCOnnections.BaseCAC , FCodigoOperacion, Registro.Error + ' , ' + Linea);
        Result := True;
    except
        on e : exception do begin
            Error := e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: RegistrarFinalizacionEnLog
Author : ndonadio
Date Created : 22/08/2005
Description :   Registra la finalizacion exitosa en el log de interfaces.
Parameters : none
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoPagosSerbanc.RegistrarFinalizacionEnLog: boolean;
resourcestring
    OBS_OK          = 'Finaliz� OK!';
    ERROR_FIN_LOG   = 'Error al cerrar la Operaci�n en el Log de Interfaces.' + CRLF + 'Se sugiere volver a ejecutar el proceso';
var
    descError: AnsiString;
begin
    Result := False;
    if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOperacion,OBS_OK, descError) then begin
        // Muestra un mensaje de error y salgo.
        MsgBoxErr( ERROR_FIN_LOG, descError, Caption, MB_ICONERROR);
        Exit;
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: MostrarReporte
Author : ndonadio
Date Created : 23/08/2005
Description :  Lanza el reporte de finalizacion en forma autom�tica
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoPagosSerbanc.MostrarReporte;
resourcestring
    ERROR_REPORT = 'No se puede mostrar el reporte';
var
    fr : TfrmReportePagosSerbanc;
    descError : AnsiString;
begin
    try
        Application.Createform(TfrmReportePagosSerbanc, fr);
        if not fr.MostrarReporte(FCodigoOperacion, Caption, descError) then begin
            MsgBoxErr(ERROR_REPORT, descError, Caption, MB_ICONERROR);
            Exit;
        end else begin
            fr.Release;
        end;
    except
        on e : exception do begin
            MsgBoxErr(ERROR_REPORT, e.Message, Caption, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author : ndonadio
Date Created : 22/08/2005
Description :   Procesa el archivo seleccionado
Parameters : Sender: TObject
Return Value : None
*******************************************************************************
  Revision 1
  lgisuk
  16/10/07
  Ahora se registra en el log que el usuario cancelo el proceso
******************************************************************************
  Revision 2
  lgisuk
  06/11/2007
  Ahora se registra en el log las excepciones y se muestra un mensaje de error mas descriptivo.
******************************************************************************}
procedure TfrmRecepcionArchivoPagosSerbanc.btnProcesarClick(Sender: TObject);
resourcestring
    ERROR_PARSEANDO     = 'Error al interpretar el archivo de pagos. El proceso es cancelado.';
    ERROR_PROCESANDO    = 'Error al procesar el archivo de pagos. El proceso es cancelado.';
    ERROR_REG_ERROR     = 'Error al registrar en el log de errores. El proceso es cancelado.';
    ERROR_REG_OPER      = 'Error intentando registrar en el Log de Interfaces. El proceso es cancelado';
    ERROR_GRAVE         = 'Error al procesar el archivo de pagos';
    ERROR_HEADER_ERROR  = 'El encabezado del archivo no concuerda con los registros contenidos. El proceso es cancelado.';
    MSG_FINISH_OK       = 'El proceso termin� con �xito!';
    MSG_PROCESS_CANCELLED_BY_USER = 'Proceso cancelado por el usuario.';
const
    STR_LINE =  'Linea: ';
var
    RegistroPago: TRegistroPago;
    strPagos    : TStringList;
    auxStr      : AnsiString;
    Error       : AnsiString;
    i           : integer;
begin
    // Valido y Abro el archivo...
    if not AbrirArchivo(strPagos) then Exit;
    FProcesando := True;
    HabilitarProceso;
    //Hago un try... finally para protejer el cursor, botones, stringlists, etc.
    try
        // Registro que inicie una operacion...
        pbProgreso.Position := pbProgreso.Min;
        if not RegistrarOperacion(Error) then begin
            MsgBoxErr(ERROR_REG_ERROR, Error, Caption, MB_ICONERROR);
            Exit;
        end;
        // Valido el header del archivo...
        if not ValidarHeader(strPagos, Error) then begin
            //Registro la excepcion en el log de operaciones
            //ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacion, ERROR_HEADER_ERROR + ' ' + Error);
            MsgBoxErr(ERROR_HEADER_ERROR, Error, Caption, MB_ICONERROR);
            Exit;
        end;
        //Ciclo por el archivo, y parseo los items y los proceso
        i := 1;
        pbProgreso.StepIt;
        while (i < strPagos.Count) and  (not FCancelar) do begin
            auxStr := strPagos.Strings[i];
            if ObtenerRegistro( auxStr, RegistroPago) then begin   //obtengo un registro
                  //proceso el pago.
                if not ProcesarRegistro(RegistroPago, Error) then begin
                        //Registro la excepcion en el log de operaciones
                        //ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacion, ERROR_GRAVE + ' - ' + STR_LINE + IntToStr(i) + ' - ' + strPagos[i] + ' - ' + RegistroPago.Error+ ' - ' + Error);
                        MsgBoxErr(ERROR_GRAVE + CRLF + STR_LINE + IntToStr(i) + CRLF + strPagos[i], RegistroPago.Error+CRLF+CRLF+Error, Caption, MB_ICONERROR);
                        Exit;
                end;
            end;
            if (trim(RegistroPago.Error) <> '') then begin
                // si en RegistroPago.Error hay algo, entonces debo registrar el error...
                 if not RegistrarError(auxStr, RegistroPago, Error) then begin
                        //Registro la excepcion en el log de operaciones
                        //ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacion, ERROR_REG_ERROR + ' ' + Error);
                        MsgBoxErr(ERROR_REG_ERROR, Error, Caption, MB_ICONERROR);
                        Exit;
                 end;
            end;
            pbProgreso.StepIt;
            Application.ProcessMessages;
            inc(i);
        end; // while
        if not FCancelar then begin
            if not RegistrarFinalizacionEnLog then exit;; //registro la finalizacion en el log...
            // Mensaje de termine OK!
            MsgBox(MSG_FINISH_OK, Caption, MB_ICONINFORMATION);
            // Lanzo reporte de finalizacion...
            MostrarReporte;
        end
        else begin

            //registro que el proceso fue cancelado
            //RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacion);

            MsgBox( MSG_PROCESS_CANCELLED_BY_USER, Caption, MB_ICONSTOP);
        end;
    finally
        // Liberar resourcestring
        strPagos.Free;
        FProcesando := False;
        HabilitarProceso;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnCancelarClick
Author : ndonadio
Date Created : 22/08/2005
Description :  Cancelar el Proceso
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoPagosSerbanc.btnCancelarClick(Sender: TObject);
begin
    FCancelar := True;
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : ndonadio
Date Created : 22/08/2005
Description :   Cierra el form
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoPagosSerbanc.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : ndonadio
Date Created : 22/08/2005
Description : inhibe el cierre del form si esta procesando...
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoPagosSerbanc.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : ndonadio
Date Created : 22/08/2005
Description :   hace que el mdi child form se cierre realmente y no que se minimice
Parameters : Sender: TObject; var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoPagosSerbanc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
