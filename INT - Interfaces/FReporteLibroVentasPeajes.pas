{********************************** Unit Header ********************************
File Name : FReporteLibroVentasPeajes.pas
Author : ggomez
Date Created: 02/09/2005
Language : ES-AR
Description : Form desde el cual se obtiene el reporte del Libro Auxilizar de
    Ventas de Peajes.

Revision 1:
    Author: ggomez
    Date:   23/10/2005
    Description:
        - En los importes ya no se muestra el signo $.
        - En el resumen, ahora se muestra la cantidad de documentos impresos.

Revision 2:
    Author: ggomez
    Date:   02/11/2005
    Description:
        - Agregu� un t�tulo al reporte.

Revision 3:
    Author: ggomez
    Date:   07/11/2005
    Description:
        - En el resumen de cada tipo de comprobante, agregu� la cantidad de
            comprobantes impresos.

Revision 4:
    Author: ggomez
    Date:   20/01/2005
    Description:
        - Agregu� la impresi�n de Notas de Cobro Infracciones.

Revision 5:
    Author: ggomez
    Date:   13/02/2006
    Description:
        - Coloqu� el T�tulo del reporte para que comience a la altura de la
        columna Desc., por un pedido hecho por Macarena Villavicencio.

Revision : 6
    Author : vpaszkowicz
    Date : 04/10/2006
    Description : Agrego el procesamiento para el tipo de comprobante Nota de
    Cr�dito con desgloce de IVA - Es el tipo NC de la base de datos. Este pasa a
    llamarse en el reporte NC/IVA, mientras que el antiguo NC pasa a ser NC/NK

Revision : 7
    Author : jconcheyro
    Date : 26/04/2007
    Description : El contador de NK manuales y masivas contaba mal y no pude
    determinar por que. Agregu� un par de campos en el store, uno que dice si
    es manual y otro si es automatica. En el reporte reemplace los labels actuales
    por un par de componentes SUM que hacen la cuenta. De este form saque las variables
    que llevaban los count y los eventos de las bandas para actualizar los contadores.

Revision : 8
    Author: nefernandez
    Date: 07/05/2007
    Description: No incluir los comprobantes 'NC' en este reporte

Revision : 9
    Author: FSandi
    Date: 19-06-2007
    Description: Se modifica el reporte de libros iva y auxiliar, ahora se hace mediante relaciones
                entre tablas y los nombres de columnas se obtienen dinamicamente.

Revision : 10
    Author: FSandi
    Date: 18-07-2007
    Description: Se modifica el reporte de libros iva y auxiliar, al momento de procesar los comprobantes
                verificamos Si al procesar un libro se encuentra que un concepto asociado al documento no tiene una columna
                asociada, el proceso arrojar� un error indicando los siguientes datos: Tipo Comprobante, n�mero comprobante,
                c�digo concepto y descripci�n concepto, suspendi�ndose el procesamiento.

Revision : 11
    Author: jconcheyro
    Date: 24/10/2007
    Description: Agrego un campo tieneDiferencia, el cual viene con un * desde el store si lo que est� en el reporte no
                 coincide con lo que est� en la base de datos. Hace pocos d�as, la columna uno del reporte mostr�
                 el doble de peajes que la base.
                 al final del proceso ejecuto el procedure ListarComprobantesQueNoBalancean que los carga en el memo de la pantalla
                 y luego pide un directorio para guardar el log como archivo.
                
Revision : 12
    Author : ggomez
    Date : 22/02/2008
    Description : Libro Aux de Ventas de Peajes:
        - Agregu� el control para asegurar que este proceso no est� siendo ejecutado.
        Esto lo hice para evitar datos err�neos debido a ejecuciones simult�neas.
        - Agregu� la lectura del valor GrabarLog desde el archivo install.ini.
        - Agregu� que deje registro en archivos de log.
        - El SP que procesa los comprobantes lanza errores, para que podamos
        indentificar aqu� que parte ha fallado.
        - Si al procesar un bloque de comprobantes, se encuentra uno de nuestros
        errores, se pregunta al operador si desea reintentar el procesamiento o si
        desea cancelar.
        - En caso de error de los nuestros, se almacena en un archivo en disco
        el contenido de los datos que el SP utiliz�. De esta manera podremos
        investigar la falla teniendo los datos que el SP us�.


Revision 13
Author: mbecerra
Date: 12-Agosto-2008
Description: Se quita la Impresi�n del reporte y en vez de eso se graba en un archivo de
             texto plano.

             * Se Cambia la funci�n ListarComprobantesQueNoBalancean para que en vez de que
               recorra nuevamente el SP, simplemente use los datos de la Grilla.

Revision 14
Author: mbecerra
Date: 27-Noviembre-2008
Description:	Se agrega la validaci�n de la correlatividad de los comprobantes

Revision 15
Date: 02/03/2009
Author: mpiazza
Description:    Ref (SRX01226) se cambia" now " por nowbase

Revision : 16
Date: 04/03/2009
Author: pdominguez
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
              los bloqueos de tablas en la lectura

Revision : 17
Date: 25-Noviembre-2009
Author: Nelson Droguett Sierra
Description:    1.- Las columnas del libro ahora son dinamicas (con un max. de 10)
                2.- Se elimina el uso de la funcion AnchoFijo y se usa PADL y PADR
                    para la justificacion de las columnas.
                3.- Los conceptos que no tienen columna asociada, se sumaran en
                    una columna "Otros Cargos" por lo que ya no es necesario abortar
                    el proceso.
				4.- Se agrega un resumen por tipocomprobante y concepto
				5.- Se calcula el valor IVA no recuperado, que es el IVA de las notas
					de credito a notas de cobro anteriores a 3 periodos contables incluyendo
					el periodo actual.
				6.- Se eliminan las columnas de AjusteSencillo.
                7.- Se genera el archivo segun TipoComprobanteFiscal y NumeroComprobanteFiscal

Revision : 18
Date: 05-Enero-2010
Author: Nelson Droguett Sierra
Description:    1.- Aumentar la capacidad de las columnas dinamicas de 10 a 15 

Firma         : SS-1015-NDR-20120517
Description   : Que separe el resumen por TipoComprobante/Concepto, ademas por
                concesionaria, con subtotal por concesionaria y un total del
                resumen.

Firma         : SS_1006_1015_20120912_CQU
Description   : Se parametriza el directorio temporal en donde se guarda el archivo.

Firma         : SS_1006_1015_NDR_20121107
Description   : Columna Parque Arauco e IVATerceros

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1147_NDR_20150506 / SS_1258_NDR_20151029
Descripcion : Poner NombreCortoConcesionaria en el nombre de archivo
*******************************************************************************}
unit FReporteLibroVentasPeajes;

interface

uses
	ComunInterfaces,
    frmVerLibroDePeajes,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilProc, StrUtils, ExtCtrls, ComCtrls, StdCtrls, Validate, DateEdit, PeaProcs,
  DB, ADODB, DMConnection, UtilRB, ppProd, ppClass, ppReport, ppComm, ppRelatv,
  ppDB, ppDBPipe, ppPrnabl, ppCtrls, ppBands, ppCache, ppModule, daDataModule,
  PeaTypes, Util, UtilDB, ppParameter, ConstParametrosGenerales, DateUtils,
  Grids, DBGrids;

type
  TfrmReporteLibroVentasPeajes = class(TForm)
    sp_LibroAuxVentasPeajes_ObtenerDatosReporte: TADOStoredProc;
    ds_ObtenerDatosReporte: TDataSource;
    pp_ObtenerDatosReporteLibroVentasPeajes: TppDBPipeline;
    pprpt_LibroVentasPeajes: TppReport;
    rbi_LibroVentasPeajes: TRBInterface;
    gr: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppl_TipoComprobante1: TppLabel;
    ppl_FechaEmision1: TppLabel;
    ppl_NumeroComprobante: TppLabel;
    ppl_Cliente: TppLabel;
    ppl_RUT: TppLabel;
    ppgr_TipoComprobante: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppl_AjsuteSencilloMesActual1: TppLabel;
    ppl_FechaEmision2: TppLabel;
    ppl_TipoComporbante2: TppLabel;
    dbt_TipoComprobante: TppDBText;
    dbt_FechaEmision: TppDBText;
    dbt_NumeroComprobante: TppDBText;
    dbt_Cliente: TppDBText;
    dbt_RUT: TppDBText;
    dbt_Intereses: TppDBText;
    dbt_GastosAdministrativos: TppDBText;
    dbt_Descuentos: TppDBText;
    dbt_OtrosCargos: TppDBText;
    ppl_AjusteSencilloMesActual2: TppLabel;
    dbt_AjusteSencilloMesActual: TppDBText;
    ppl_AjsuteSencilloMesAnterior1: TppLabel;
    ppl_AjsuteSencilloMesAnterior2: TppLabel;
    dbt_AjusteSencillomesAnterior: TppDBText;
    ppl_TotalAPagar: TppLabel;
    dbt_TotalAPagar: TppDBText;
    ppl_TotalFacturacionMasiva: TppLabel;
    ppl_TotalFacturacionManual: TppLabel;
    ppl_Total: TppLabel;
    dbc_TotalPeajesEmitidosMasivamente: TppDBCalc;
    dbc_TotalPeajesEmitidosManualmente: TppDBCalc;
    dbc_InteresesManual: TppDBCalc;
    dbc_InteresesMasivo: TppDBCalc;
    dbc_Intereses: TppDBCalc;
    dbc_RecupGastosAdminMasivo: TppDBCalc;
    dbc_RecupGastosAdminManual: TppDBCalc;
    dbc_RecupGastosAdmin: TppDBCalc;
    dbc_DescuentosMasivo: TppDBCalc;
    dbc_DescuentosManual: TppDBCalc;
    dbc_Descuentos: TppDBCalc;
    dbc_OtrosCargosMasivo: TppDBCalc;
    dbc_OtrosCargosManual: TppDBCalc;
    dbc_OtrosCargos: TppDBCalc;
    dbc_AjusteSencilloActualMasivo: TppDBCalc;
    dbc_AjusteSencilloActualManual: TppDBCalc;
    dbc_AjsuteSencilloActual: TppDBCalc;
    dbc_AjusteSencilloAnteriorMasivo: TppDBCalc;
    dbc_AjusteSencilloAnteriorManual: TppDBCalc;
    dbc_AjusteSencilloAnterior: TppDBCalc;
    dbc_TotalAPagar: TppDBCalc;
    dbc_TotalAPagarManual: TppDBCalc;
    dbc_TotalAPagarMasivo: TppDBCalc;
    ppSummaryBand1: TppSummaryBand;
    dbc_TotalPeajes2: TppDBCalc;
    ppLabel1: TppLabel;
    dbcTotalInteresesMora: TppDBCalc;
    dbc_TotalRecupGastosAdmin: TppDBCalc;
    dbc_TotalDescuentos: TppDBCalc;
    dbcTotalOtrosCargos: TppDBCalc;
    dbcTotalAjsuteSencilloActual: TppDBCalc;
    dbc_TotalAjusteSencilloAnterior: TppDBCalc;
    dbcTotalTotalAPagar: TppDBCalc;
    ppLine1: TppLine;
    sp_LibroAuxVentasPeajes_ProcesarComprobantes: TADOStoredProc;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppParameterList1: TppParameterList;
    dbcTotalDocumentos: TppDBCalc;
    lblTitLibroPeaje: TppLabel;
    lblPeriodoLibroPeaje: TppLabel;
    CantidadDocumentos: TppDBCalc;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    SP_ObtenerColumnasLibro: TADOStoredProc;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    SP_ObtenerComprobantesValidos: TADOStoredProc;
    ppDBText1: TppDBText;
    dlgGuardarArchivoLog: TSaveDialog;
    Panel1: TPanel;
    pnl_Avance: TPanel;
    lbl_ProgesoGeneral: TLabel;
    de_FechaDesde: TDateEdit;
    de_FechaHasta: TDateEdit;
    lbl_FechaHasta: TLabel;
    lbl_FechaDesde: TLabel;
    Pnl_Botones: TPanel;
    btnGenerar: TButton;
    btn_Salir: TButton;
    Panel3: TPanel;
    Pnl_Estado: TPanel;
    lbl_Mensaje: TLabel;
    Pnl_Inconsistencias: TPanel;
    meLineasError: TMemo;
    Panel6: TPanel;
    pb_ProgresoGeneral: TProgressBar;
    lbl_inconsistencias: TLabel;
	dbgDatos: TDBGrid;
    btnVisualizar: TButton;
    spLibroAuxVentasPeajes_VerificarContinuidadComprobantes: TADOStoredProc;
    dsCorrelatividad: TDataSource;
    dbgCorrelatividad: TDBGrid;
    spObtenerListadoColumnasContables: TADOStoredProc;
    spLibroAuxVentasPeajes_ResumenTipoComprobanteConcepto: TADOStoredProc;
    ds_ResumenTipoComprobanteConcepto: TDataSource;
    DBGResumenConcepto: TDBGrid;
    spLibroAuxVentasPeajes_DocumentosIvaNoRecuperado: TADOStoredProc;
    procedure ppl_TotalFacturacionMasivaPrint(Sender: TObject);
    procedure ppl_TotalPrint(Sender: TObject);
    procedure rbi_LibroVentasPeajesExecute(Sender: TObject;
      var Cancelled: Boolean);
    procedure btnGenerarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_SalirClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
  private
    { Private declarations }
    FCancelar: Boolean;
    FGrabarLog: Boolean;
    FLogFilePath, FLogFileName, FLogFileExtension, FMachineName, FUser: AnsiString;
    FAnteriorNow, FActualNow, FAnteriorNow2, FActualNow2: TDatetime;
    FDirectorioTemporal : AnsiString;                                   // SS_1006_1015_20120912_CQU
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure CrearTablasTemporales;
    procedure EliminarTablasTemporales;
    Function CargarColumnasReporte:Boolean;      //Revision 9
    function ObtenerCantidadComprobantes(Conn: TADOConnection;
        FechaDesde, FechaHasta: TDateTime; TipoCompro: AnsiString): Int64;
    procedure ProcesarComprobantes(TipoComprobante: AnsiString);
    procedure ImprimirReporte;

    function ProcesoEnEjecucion: Boolean;
    function CrearDirectorioTemporal : Boolean;                          // SS_1006_1015_20120912_CQU

  public
    { Public declarations }
    function Inicializar(txt_Caption: AnsiString): Boolean;
    procedure GrabarDatosEnArchivo;
    procedure ListarComprobantesQueNoBalancean(EsMostrar : boolean; Linea : string);
  end;

resourcestring                                                          // SS_1006_1015_20120912_CQU
    DIRECTORIO_ARCHIVO_TEMPORAL_LIBRO = 'LIBRO_AUX_PEAJE_DIR_TMP';      // SS_1006_1015_20120912_CQU

var
  frmReporteLibroVentasPeajes: TfrmReporteLibroVentasPeajes;


implementation

uses IniFiles;

{$R *.dfm}

{ TfrmReporteLibroVentasPeajes }

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ggomez
Date Created : 02/09/2005
Description : Iniciliza el form. Retorna True, si iniciliz� con exito.
Parameters : txt_Caption: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmReporteLibroVentasPeajes.Inicializar(txt_Caption: AnsiString): Boolean;
resourcestring                                                                                          // SS_1006_1015_20120912_CQU
	MSG_ERROR = 'Error';                                                                                // SS_1006_1015_20120912_CQU
    MSG_ERROR_CHECK_GENERAL   = 'Error al verificar parametro ';                                        // SS_1006_1015_20120912_CQU
    STR_NOT_EXISTS_GENERAL    = 'No existe parametro: ';                                                // SS_1006_1015_20120912_CQU
    STR_EMPTY_GENERAL         = 'Parametro vacio: ';                                                    // SS_1006_1015_20120912_CQU
var                                                                                                     // SS_1006_1015_20120912_CQU
    DescError : AnsiString;                                                                             // SS_1006_1015_20120912_CQU
begin
    try
        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

        pnl_Avance.Hide;

        lbl_Mensaje.Caption := EmptyStr;
        Pnl_Inconsistencias.Visible := False;
        dbgCorrelatividad.Visible := False;
        CenterForm(Self);
    	Self.Caption := AnsiReplaceStr(txt_Caption, '&', EmptyStr);
        Self.FormStyle:=fsNormal;
        Self.Visible := False;

        de_FechaDesde.Date := IncDay(Date, -1);
        de_FechaHasta.Date := de_FechaDesde.Date;

        MoveToMyOwnDir;
        ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_LOG_LIBROAUXILIARPEAJES', FLogFilePath);
        FLogFileName         := 'LibroAuxPeajes';
        FLogFileExtension    := 'log';
        FMachineName         := GetMachineName;
        FUser                := UsuarioSistema;

        // Inicio Bloque SS_1006_1015_20120912_CQU
        if (FDirectorioTemporal = EmptyStr) then begin
            if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIRECTORIO_ARCHIVO_TEMPORAL_LIBRO , FDirectorioTemporal) then begin
                DescError := STR_NOT_EXISTS_GENERAL + DIRECTORIO_ARCHIVO_TEMPORAL_LIBRO;
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL, DescError, MSG_ERROR, MB_ICONWARNING);
                Result := False;
                Exit;
            end else if FDirectorioTemporal = EmptyStr then begin
                DescError := STR_EMPTY_GENERAL + DIRECTORIO_ARCHIVO_TEMPORAL_LIBRO;
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL, DescError, MSG_ERROR, MB_ICONWARNING);
                Result := False;
                Exit;
            end;
        end;
        // Fin Bloque SS_1006_1015_20120912_CQU

        FGrabarLog := InstallIni.ReadBool('General', 'GrabarLog', True);

    	Result := True;
    except
    	Result := False;
    end; // except
end;

procedure TfrmReporteLibroVentasPeajes.btnVisualizarClick(Sender: TObject);
resourcestring
	MSG_CAPTION = 'Visualizar Libro de Peajes';

var
	f : TVerLibroDePeajesForm;
begin
	if FindFormOrCreate(TVerLibroDePeajesForm, f) then f.Show
    else if not f.Inicializar(MSG_CAPTION) then f.Release;
         
	{Application.CreateForm(TVerLibroDePeajesForm, f);
    f.Inicializar(MSG_CAPTION);
    f.Position := poScreenCenter;
    f.ShowModal;
    f.Free;}

end;

procedure TfrmReporteLibroVentasPeajes.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmReporteLibroVentasPeajes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if sp_LibroAuxVentasPeajes_ObtenerDatosReporte.Active then begin
        sp_LibroAuxVentasPeajes_ObtenerDatosReporte.Close;
    end;
    Action := caFree;
end;

procedure TfrmReporteLibroVentasPeajes.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

procedure TfrmReporteLibroVentasPeajes.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    // Permitir salir s�lo si no esta procesando.
    CanClose := not pnl_Avance.Visible;
end;

{******************************** Function Header ******************************
Function Name: btn_ImprimirClick
Author : ggomez
Date Created : 04/10/2006
Description :
Parameters : Sender: TObject
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 04/10/2006
    Description : Agrego el tipo NC a los comprobantes a procesar.

Revision : 2
    Date: 02/03/2009
    Author: mpiazza
    Description:    Ref (SRX01226) se cambia" now " por nowbase

*******************************************************************************}

procedure TfrmReporteLibroVentasPeajes.btnGenerarClick(Sender: TObject);
resourcestring
    MSG_PROCESO_CANCELADO   = 'Proceso cancelado.';
    MSG_FECHA_NO_VALIDA     = 'La fecha ingresada no es v�lida.';
    MSG_FECHA_DESDE_DEBE_SER_ANTERIOR_O_IGUAL_FECHA_HASTA	= 'La Fecha Desde debe ser anterior o igual a la Fecha Hasta.';
    MSG_FECHA_HASTA_DEBE_SER_ANTERIOR_FECHA_ACTUAL			= 'La Fecha Hasta debe ser anterior a la fecha actual.';
    MSG_ERROR_OBTENER_DATOS				= 'Ha ocurrido un error al obtener los datos para el reporte.';
    MSG_ELIMINANDO_TABLAS_TEMPORALES    = 'Eliminando tablas temporales ...';
    MSG_CREANDO_TABLAS_TEMPORALES       = 'Creando tablas temporales ...';
    MSG_PROCESANDO						= 'Procesando %s ...';
    MSG_IMPRIMIENDO_REPORTE				= 'Imprimiendo el reporte ...';
    MSG_PROCESO_EN_EJECUCION	= 'Existe en ejecuci�n otro proceso de Generaci�n del Libro Auxiliar de Ventas de Peajes.'
        						  + CRLF + 'No se puede continuar con este proceso.';
    MSG_PROCESO_FINALIZADO		= 'Proceso Finalizado';
    MSG_PROCESO_INIT			= '***************** Inicio Generaci�n del Libro. Desde: %s - Hasta: %s  *****************';
    MSG_CORRELATIVIDAD			= 'Verificando Correlatividad de Comprobantes';

var
    CantidadTiposComprobantes, i : Integer;
    DateBD: TDatetime;
begin
    FCancelar := False;
    meLineasError.Lines.Clear;
    // Controlar que se hayan ingresado las fechas.
    // Controlar que la Fecha Desde sea anterior o igual a la Fecha Hasta
    DateBD := Trunc(NowBase(DMConnections.BaseCAC));
    if not ValidateControls(
            [de_FechaDesde,
                de_FechaHasta,
                de_FechaDesde,
                de_FechaHasta],
            [de_FechaDesde.Date >= 0,
                de_FechaHasta.Date >= 0,
                de_FechaDesde.Date <= de_FechaHasta.Date,
                de_FechaHasta.Date <= DateBD],
            Self.Caption,
            [MSG_FECHA_NO_VALIDA,
                MSG_FECHA_NO_VALIDA,
                MSG_FECHA_DESDE_DEBE_SER_ANTERIOR_O_IGUAL_FECHA_HASTA,
                MSG_FECHA_HASTA_DEBE_SER_ANTERIOR_FECHA_ACTUAL]) then begin
        Exit;
    end;

    // Controlar que el proceso NO est� en ejecuci�n.
    if ProcesoEnEjecucion() then begin
        MsgBox(MSG_PROCESO_EN_EJECUCION, Self.Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if not CrearDirectorioTemporal() then Exit; // SS_1006_1015_20120912_CQU


    de_FechaDesde.Enabled := False;
    de_FechaHasta.Enabled := False;
    btnGenerar.Enabled    := False;
    btnVisualizar.Enabled := False;
    pnl_Avance.Show;
  	Screen.Cursor   := crHourGlass;
    KeyPreview      := True;
    //Revision 14
    meLineasError.Visible := True;
    dbgCorrelatividad.Visible := False;
    lbl_inconsistencias.Caption := 'Inconsistencias encontradas durante la generaci�n:';
    try
        if FGrabarLog then begin
            GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
            		Format(MSG_PROCESO_INIT, [de_FechaDesde.Text, de_FechaHasta.Text]), FMachineName, FUser);
        end;

        //Revision 14
        lbl_Mensaje.Caption := MSG_CORRELATIVIDAD;
        lbl_Mensaje.Refresh;
        if FGrabarLog then begin
            GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, MSG_CORRELATIVIDAD, FMachineName, FUser);
        	FAnteriorNow := NowBase(DMConnections.BaseCAC);
        end;

        spLibroAuxVentasPeajes_VerificarContinuidadComprobantes.Close;
        spLibroAuxVentasPeajes_VerificarContinuidadComprobantes.Parameters.ParamByName('@FechaDesde').Value := de_FechaDesde.Date;
        spLibroAuxVentasPeajes_VerificarContinuidadComprobantes.Parameters.ParamByName('@FechaHasta').Value := de_FechaHasta.Date;
        spLibroAuxVentasPeajes_VerificarContinuidadComprobantes.Open;
        if not spLibroAuxVentasPeajes_VerificarContinuidadComprobantes.IsEmpty then begin
        	meLineasError.Visible := False;
            dbgCorrelatividad.Visible := True;
            lbl_inconsistencias.Caption := 'Comprobantes que Faltan:';
        end;


        try

            lbl_Mensaje.Caption := MSG_ELIMINANDO_TABLAS_TEMPORALES;
            Application.ProcessMessages;

            if FGrabarLog then begin
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, MSG_ELIMINANDO_TABLAS_TEMPORALES, FMachineName, FUser);
                FAnteriorNow := NowBase(DMConnections.BaseCAC);
            end;

            EliminarTablasTemporales;

            if FGrabarLog then begin
                FActualNow := NowBase(DMConnections.BaseCAC);
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                    'Fin ' + MSG_ELIMINANDO_TABLAS_TEMPORALES
                        + '   ' + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow, FAnteriorNow)),
                    FMachineName, FUser);
            end;

            lbl_Mensaje.Caption := MSG_CREANDO_TABLAS_TEMPORALES;
            Application.ProcessMessages;
            if FGrabarLog then begin
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, MSG_CREANDO_TABLAS_TEMPORALES, FMachineName, FUser);
                FAnteriorNow := NowBase(DMConnections.BaseCAC);
            end;

            CrearTablasTemporales;

            if FGrabarLog then begin
                FActualNow := NowBase(DMConnections.BaseCAC);
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                    'Fin ' + MSG_CREANDO_TABLAS_TEMPORALES
                        + '   ' + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow, FAnteriorNow)),
                    FMachineName, FUser);
            end;

//Revision 9
            if FGrabarLog then begin
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, 'Obtener Comprobantes V�lidos.', FMachineName, FUser);
                FAnteriorNow := NowBase(DMConnections.BaseCAC);
            end;

            SP_ObtenerComprobantesValidos.Open;
            CantidadTiposComprobantes := SP_ObtenerComprobantesValidos.RecordCount;
            SP_ObtenerComprobantesValidos.First;

            if FGrabarLog then begin
                FActualNow := NowBase(DMConnections.BaseCAC);
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                    'Fin Obtener Comprobantes V�lidos.'
                        + '   ' + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow, FAnteriorNow)),
                    FMachineName, FUser);
            end;

            for i := 0 to CantidadTiposComprobantes - 1 do begin
                if FCancelar then begin
                    SP_ObtenerComprobantesValidos.Close;
                    Exit;
                end;
                lbl_Mensaje.Caption := Format(MSG_PROCESANDO, [trim(SP_ObtenerComprobantesValidos.FieldByName('Descripcion').AsString)]);
                Application.ProcessMessages;

                if FGrabarLog then begin
                    GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                        Format(MSG_PROCESANDO, [trim(SP_ObtenerComprobantesValidos.FieldByName('Descripcion').AsString)]), FMachineName, FUser);
                    FAnteriorNow := NowBase(DMConnections.BaseCAC);
                end;

                ProcesarComprobantes(trim(SP_ObtenerComprobantesValidos.FieldByName('TipoComprobante').AsString));

                if FGrabarLog then begin
                    FActualNow := NowBase(DMConnections.BaseCAC);
                    GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                        'Fin ' + Format(MSG_PROCESANDO, [trim(SP_ObtenerComprobantesValidos.FieldByName('Descripcion').AsString)])
                            + '   ' + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow, FAnteriorNow)),
                        FMachineName, FUser);
                end;

                SP_ObtenerComprobantesValidos.Next;
            end;

            if FGrabarLog then begin
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, 'Acciones previas a ImprimirReporte ...', FMachineName, FUser);
                FAnteriorNow := NowBase(DMConnections.BaseCAC);
            end;

            SP_ObtenerComprobantesValidos.Close;
            if FCancelar then Exit;
            lbl_Mensaje.Caption := MSG_IMPRIMIENDO_REPORTE;
            Application.ProcessMessages;

            if FGrabarLog then begin
                FActualNow := NowBase(DMConnections.BaseCAC);
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                    'Fin Acciones previas a ImprimirReporte.'
                        + '   ' + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow, FAnteriorNow)),
                    FMachineName, FUser);
            end;

			// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra -----------------
            With spLibroAuxVentasPeajes_ResumenTipoComprobanteConcepto do begin
              Close;
              Parameters.Refresh;
              Parameters.ParamByName('@FechaDesde').Value:=de_FechaDesde.Date;
              Parameters.ParamByName('@FechaHasta').Value:=de_FechaHasta.Date;
              Open;
            end;
			//-----------------------------------------------------------------------

            //ImprimirReporte;           //Revision 13
            GrabarDatosEnArchivo();   //Revision 13

            if FGrabarLog then begin
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, 'Fin ImprimirReporte.', FMachineName, FUser);
            end;

            if meLineasError.Lines.Count > 0 then	ListarComprobantesQueNoBalancean(False, '');

            if FCancelar then Exit;
            EliminarTablasTemporales;


            // Cerrar el form, porque al ser Dialog, queda sobre el reporte.
            //Close;   //Revision 13
            MsgBox(MSG_PROCESO_FINALIZADO, Caption, MB_ICONINFORMATION);
        except
            on E: Exception do begin
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, MSG_ERROR_OBTENER_DATOS + ' Error: ' + E.Message, FMachineName, FUser);
                MsgBoxErr(MSG_ERROR_OBTENER_DATOS, E.Message, Self.Caption, MB_ICONSTOP);
            end;
        end; // except
    finally
      	Screen.Cursor   := crDefault;
        pnl_Avance.Hide;
        pb_ProgresoGeneral.Position := 0;
        lbl_Mensaje.Caption     := EmptyStr;
        de_FechaDesde.Enabled   := True;
        de_FechaHasta.Enabled   := True;
        btnGenerar.Enabled    := True;
		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra -----------------
        btnVisualizar.Enabled := True;

        if FCancelar then begin
            if FGrabarLog then begin
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, 'Cancelaci�n de la Generaci�n del Libro.', FMachineName, FUser);
            end;

            // Eliminar las tablas temporales
            EliminarTablasTemporales;
            FCancelar := False;
            // Mostrar el mensaje de proceso cancelado (sin errores).
            MsgBox(MSG_PROCESO_CANCELADO, Self.Caption,
                MB_OK + MB_ICONINFORMATION);
        end
    end; // finally


end;

{*****************************************************************************
Function Name: GrabarDatosEnArchivo
Author : mbecerra
Date: 12-Agosto-2008
Description : Graba el resultado de un stored en un archivo plano

Revision : 1
    Date: 02/03/2009
    Author: mpiazza
    Description:    Ref (SRX01226) se cambia" now " por nowbase
*******************************************************************************}
procedure TfrmReporteLibroVentasPeajes.GrabarDatosEnArchivo;
resourcestring
	MSG_ABRIENDO		= 'Obteniendo Datos';
    MSG_GUARDANDO		= 'Generando Archivo en disco local';
    MSG_GUARDANDO_MEM   = 'Guardando en memoria';
    MSG_GUARDADO		= 'Fin generacion archivo';
    MSG_NO_HAY			= 'No se encontraron Datos para generar en el archivo';
    MSG_SIN_PARAM       = 'No se encontr� el par�metro que indica la carpeta donde dejar el archivo: %s. ';
    //MSG_SIN_PARAM_2     = 'El archivo quedar� en la carpeta local C:\';  // SS_1006_1015_20120912_CQU
    MSG_SIN_PARAM_2     = 'El archivo quedar� en la carpeta local %s';     // SS_1006_1015_20120912_CQU
    MSG_ARCHIVO         = 'Archivo:  ';
    PARAM_DIR_SALIDA    = 'LIBRO_AUX_PEAJE_DIR_SALIDA';
    MSG_PROCESADOS      = 'Generadas : %.0n';
	MSG_NO_CUADRAN		= 'Existen comprobantes cuyos montos no cuadran con el total';
var
    NombreArchivo,NombreArchivoDiferencias, Linea, DirSalida, DescripcionConcesionaria : string;      //SS-1015-NDR-20120517
    i, Cuenta, CodigoConcesionaria : integer;                                                         //SS-1015-NDR-20120517
    lstDatos, lstResumen, lstResumenConcepto : TStringList;

    {variables para el resumen}
    ResumenTipoDoc : string;
    Columna1,Columna2,Columna3,Columna4,Columna5,
    Columna6,Columna7,Columna8,Columna9,Columna10 : String;
    // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
    Columna11,Columna12,Columna13,Columna14,Columna15:String;
    //--------------------------------------------------------------------------------------
    Iva : String;
    IvaTerceros : String;                                                                              //SS_1006_1015_NDR_20121107
    CantidadDeDocumentos : integer;
    Resumen_Col1,Resumen_Col2,Resumen_Col3,Resumen_Col4,Resumen_Col5,
    Resumen_Col6,Resumen_Col7,Resumen_Col8,Resumen_Col9,Resumen_Col10,
    // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
    Resumen_Col11,Resumen_Col12,Resumen_Col13,Resumen_Col14,Resumen_Col15,
    //--------------------------------------------------------------------------------------
    ResumenOtrosCargos,ResumenTotalAPagar,ResumenIVA,ResumenIVATerceros,                                  //SS_1006_1015_NDR_20121107
    ResumenSuma_Col1, ResumenSuma_Col2, ResumenSuma_Col3, ResumenSuma_Col4, ResumenSuma_Col5,
    ResumenSuma_Col6, ResumenSuma_Col7, ResumenSuma_Col8, ResumenSuma_Col9, ResumenSuma_Col10,
    // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
    ResumenSuma_Col11,ResumenSuma_Col12,ResumenSuma_Col13,ResumenSuma_Col14,ResumenSuma_Col15,
    //--------------------------------------------------------------------------------------
    ResumenSumaOtrosCargos,ResumenSumaTotalApagar,ResumenSumaIVA,ResumenSumaIVATerceros,                   //SS_1006_1015_NDR_20121107
    ResumenMinDoc, ResumenMaxDoc  : Int64;

    TotalImporte, TotalImporteIva, TotalIVATerceros, TotalMonto : Double;                               //SS_1006_1015_NDR_20121107
    SubTotalNeto, SubTotalIVA, SubTotalIVATerceros, SubTotalTotal : Double;                             //SS_1006_1015_NDR_20121107 //SS-1015-NDR-20120517

    q:TADOQuery;

begin

    Columna1:='';
    Columna2:='';
    Columna3:='';
    Columna4:='';
    Columna5:='';
    Columna6:='';
    Columna7:='';
    Columna8:='';
    Columna9:='';
    Columna10:='';
    // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
    Columna11:='';
    Columna12:='';
    Columna13:='';
    Columna14:='';
    Columna15:='';
    //--------------------------------------------------------------------------------------

	lstDatos            := TStringList.Create;
    lstResumen          := TStringList.Create;
    lstResumenConcepto  := TStringList.Create;
    //lstNoCuadran        := TStringList.Create;


	if FGrabarLog then begin
        FAnteriorNow := NowBase(DMConnections.BaseCAC);
		GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, MSG_ABRIENDO, FMachineName, FUser);
    end;

    With spObtenerListadoColumnasContables do
    begin
      Close;
      Parameters.ParamByName('@CodigoLibroContable').Value := QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.CONST_LIBRO_AUX_VENTAS()');
      Open;
      First;
	  // Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra ----------------------------
      While Not(EOF) do
      begin
        case FieldByName('IdColumnaLibroContable').AsInteger of
         1:Columna1:=FieldByName('Descripcion').AsString;
         2:Columna2:=FieldByName('Descripcion').AsString;
         3:Columna3:=FieldByName('Descripcion').AsString;
         4:Columna4:=FieldByName('Descripcion').AsString;
         5:Columna5:=FieldByName('Descripcion').AsString;
         6:Columna6:=FieldByName('Descripcion').AsString;
         7:Columna7:=FieldByName('Descripcion').AsString;
         8:Columna8:=FieldByName('Descripcion').AsString;
         9:Columna9:=FieldByName('Descripcion').AsString;
         10:Columna10:=FieldByName('Descripcion').AsString;
         // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
         11:Columna11:=FieldByName('Descripcion').AsString;
         12:Columna12:=FieldByName('Descripcion').AsString;
         13:Columna13:=FieldByName('Descripcion').AsString;
         14:Columna14:=FieldByName('Descripcion').AsString;
         15:Columna15:=FieldByName('Descripcion').AsString;
         // -------------------------------------------------------------------------------------
        end;
        Next;
      end;
    end;

	lbl_Mensaje.Caption := MSG_ABRIENDO;
    Application.ProcessMessages;
    sp_LibroAuxVentasPeajes_ObtenerDatosReporte.Close;
    sp_LibroAuxVentasPeajes_ObtenerDatosReporte.CommandTimeout := 15000;
    sp_LibroAuxVentasPeajes_ObtenerDatosReporte.Open;
    if FGrabarLog then begin
    	FActualNow := NowBase(DMConnections.BaseCAC);
    	GrabarMensajeEnArchivoLog(	FLogFilePath, FLogFileName, FLogFileExtension,
        							MSG_GUARDANDO_MEM + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow, FAnteriorNow)),
                                    FMachineName, FUser);
        FAnteriorNow := NowBase(DMConnections.BaseCAC);
    end;

    if not sp_LibroAuxVentasPeajes_ObtenerDatosReporte.IsEmpty then begin


	  // Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra ----------------------------
        TotalImporte := 0;
        TotalImporteIva:=0;
        TotalIVATerceros:=0;                                                                        //SS_1006_1015_NDR_20121107
        TotalMonto:=0;
        With DBGResumenConcepto.DataSource.DataSet do begin
          if Not(IsEmpty) then begin
              lstResumenConcepto.Add( '');
              lstResumenConcepto.Add( 'TC Tipo  Descripci�n Concepto                                               Neto          I.V.A.  I.V.A.Terceros           Total');//SS_1006_1015_NDR_20121107
              lstResumenConcepto.Add( DupeString('=',130));                                       //SS_1006_1015_NDR_20121107//SS-1015-NDR-20120517
              First;                                                                              //SS-1015-NDR-20120517
              CodigoConcesionaria:=0;                                                             //SS-1015-NDR-20120517
              DescripcionConcesionaria:='';                                                       //SS-1015-NDR-20120517
              while Not(EOF) do begin                                                             //SS-1015-NDR-20120517
                if CodigoConcesionaria<>FieldByName('CodigoConcesionaria').AsInteger then         //SS-1015-NDR-20120517
                begin                                                                             //SS-1015-NDR-20120517
                  SubTotalNeto:=0;                                                                //SS-1015-NDR-20120517
                  SubTotalIVA:=0;                                                                 //SS-1015-NDR-20120517
                  SubTotalIVATerceros:=0;                                                         //SS_1006_1015_NDR_20121107
                  SubTotalTotal:=0;                                                               //SS-1015-NDR-20120517
                  CodigoConcesionaria:=FieldByName('CodigoConcesionaria').AsInteger;              //SS-1015-NDR-20120517
                  DescripcionConcesionaria:=FieldByName('DescripcionConcesionaria').AsString;     //SS-1015-NDR-20120517
                  lstResumenConcepto.Add( '');                                                    //SS-1015-NDR-20120517
                  lstResumenConcepto.Add( 'Concesionaria : ' + DescripcionConcesionaria);         //SS-1015-NDR-20120517
                  lstResumenConcepto.Add( DupeString('-',130));                                   //SS_1006_1015_NDR_20121107//SS-1015-NDR-20120517
                end;                                                                              //SS-1015-NDR-20120517
                lstResumenConcepto.Add( PADR(Trim(FieldByName('TipoComprobante').AsString),2,' ') + ' ' +
                                        PADR(Trim(FieldByName('TipoComprobanteFiscal').AsString),2,' ') + '    ' +
                                        PADR(Trim(FieldByName('Concepto').AsString),55,' ')  + ' ' +
                                        PADL(Format('%.0n',[FieldByName('Importe').AsFloat]),15,' ') + ' ' +
                                        PADL(Format('%.0n',[FieldByName('ImporteIva').AsFloat]),15,' ') +  ' ' +
                                        PADL(Format('%.0n',[FieldByName('IVATerceros').AsFloat]),15,' ') +  ' ' +//SS_1006_1015_NDR_20121107
                                        PADL(Format('%.0n',[FieldByName('Monto').AsFloat]),15,' ')
                            );
                TotalImporte := TotalImporte + FieldByName('Importe').AsFloat;
                TotalImporteIva := TotalImporteIva + FieldByName('ImporteIva').AsFloat;
                TotalIVATerceros := TotalIVATerceros + FieldByName('IVATerceros').AsFloat;          //SS_1006_1015_NDR_20121107
                TotalMonto := TotalMonto + FieldByName('Monto').AsFloat;
                SubTotalNeto := SubTotalNeto + FieldByName('Importe').AsFloat;                   //SS-1015-NDR-20120517
                SubTotalIva := SubTotalIva + FieldByName('ImporteIva').AsFloat;                  //SS-1015-NDR-20120517
                SubTotalIVATerceros := SubTotalIVATerceros + FieldByName('IVATerceros').AsFloat; //SS_1006_1015_NDR_20121107
                SubTotalTotal := SubTotalTotal + FieldByName('Monto').AsFloat;                   //SS-1015-NDR-20120517
                Next;
                if (CodigoConcesionaria<>FieldByName('CodigoConcesionaria').AsInteger) or Eof then                              //SS-1015-NDR-20120517
                begin                                                                                                           //SS-1015-NDR-20120517
                  lstResumenConcepto.Add( DupeString('-',130));                                                                 //SS_1006_1015_NDR_20121107//SS-1015-NDR-20120517
                  lstResumenConcepto.Add( PADR(Trim('SubTotal Concesionaria : '+DescripcionConcesionaria),64,' ')  + ' ' +      //SS-1015-NDR-20120517
                                          PADL(Format('%.0n',[SubTotalNeto]),15,' ') + ' ' +                                    //SS-1015-NDR-20120517
                                          PADL(Format('%.0n',[SubTotalIva]),15,' ') +  ' ' +                                    //SS-1015-NDR-20120517
                                          PADL(Format('%.0n',[SubTotalIVATerceros]),15,' ') +  ' ' +                            //SS_1006_1015_NDR_20121107
                                          PADL(Format('%.0n',[SubTotalTotal]),15,' ')                                           //SS-1015-NDR-20120517
                            );                                                                                                  //SS-1015-NDR-20120517
                  lstResumenConcepto.Add( '');                                                                                  //SS-1015-NDR-20120517
                end;                                                                                                            //SS-1015-NDR-20120517
              end;                                                                                                              //SS-1015-NDR-20120517
              lstResumenConcepto.Add( DupeString('=',130));                                                                     //SS_1006_1015_NDR_20121107//SS-1015-NDR-20120517
              lstResumenConcepto.Add(   PADR('Total Resumen',65,' ')+                                                           //SS-1015-NDR-20120517
                                        PADL(Format('%.0n',[TotalImporte]),15,' ') + ' ' +                                      //SS-1015-NDR-20120517
                                        PADL(Format('%.0n',[TotalImporteIva]),15,' ') +  ' ' +                                  //SS-1015-NDR-20120517
                                        PADL(Format('%.0n',[TotalIVATerceros]),15,' ') +  ' ' +                                 //SS_1006_1015_NDR_20121107
                                        PADL(Format('%.0n',[TotalMonto]),15,' ')                                                //SS-1015-NDR-20120517
                          );                                                                                                    //SS-1015-NDR-20120517



         	  // Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra ----------------------------
              lstResumenConcepto.Add('');
              lstResumenConcepto.Add( DupeString('-',130));

              lstResumenConcepto.Add('');
              lstResumenConcepto.Add('');
              lstResumenConcepto.Add('');
              lstResumenConcepto.Add(   'I.V.A. NO RECUPERADO :   '  + ' ' +
                                        PADL(IntToStr(QueryGetValueInt(DMConnections.BaseCAC ,
                                            Format('SELECT dbo.ObtenerMontoIvaNoRecuperado (''%s'',''%s'')',
                                                    [ IntToStr(YearOf(de_FechaDesde.date))+'-'+
                                                     IntToStr(MonthOf(de_FechaDesde.date))+'-'+
                                                     IntToStr(DayOf(de_FechaDesde.date)),

                                                     IntToStr(YearOf(de_FechaHasta.date))+'-'+
                                                     IntToStr(MonthOf(de_FechaHasta.date))+'-'+
                                                     IntToStr(DayOf(de_FechaHasta.date))]))),8,' ')
                                     );                
              lstResumenConcepto.Add('');
              lstResumenConcepto.Add('------ Nota de Credito------   -- Documento Referenciado --');
              lstResumenConcepto.Add('TC       Numero   Fecha        TC       Numero   Fecha     ');
              //                      xx   xxxxxxxxxx   xx-xx-xxxx   xx   xxxxxxxxxx   xx-xx-xxxx
              lstResumenConcepto.Add('-----------------------------------------------------------');
              With spLibroAuxVentasPeajes_DocumentosIvaNoRecuperado do begin
                Close;
                Parameters.Refresh;
                Parameters.ParamByName('@FechaDesde').Value := de_FechaDesde.Date;
                Parameters.ParamByName('@FechaHasta').Value := de_FechaHasta.Date;
                Open;
                while Not(Eof) do begin
                    lstResumenConcepto.Add( FieldByName('TipoComprobanteFiscal').AsString + '   ' +
                                            PADL(FieldByName('NumeroComprobanteFiscal').AsString,10,' ') + '   ' +
                                            LeftStr(FieldByName('FechaComprobanteFiscal').AsString,10) + '   ' +
                                            FieldByName('RefTipoComprobanteFiscal').AsString + '   ' +
                                            PADL(FieldByName('RefNumeroComprobanteFiscal').AsString,10,' ') + '   ' +
                                            LeftStr(FieldByName('RefFechaComprobanteFiscal').AsString,10)
                                          );
                    Next;
                end;
              end;
              lstResumenConcepto.Add('-----------------------------------------------------------');
              lstResumenConcepto.Add('');


              lstResumenConcepto.Add(SeparadorResumen);
          end
        end;




    	lbl_Mensaje.Caption := MSG_GUARDANDO_MEM;
    	Application.ProcessMessages;

        //NombreArchivo := 'C:\';               // SS_1006_1015_20120912_CQU
        NombreArchivo := FDirectorioTemporal;   // SS_1006_1015_20120912_CQU
        NombreArchivo := 	NombreArchivo + Trim(ObtenerNombreCortoConcesionariaNativa)+ '_'  +'LAP_Desde_' +                   //SS_1258_NDR_20151029-SS_1147_NDR_20150506
        					FormatDateTime('yyyy-mm-dd', de_FechaDesde.Date) +                                                          //SS_1258_NDR_20151029-SS_1147_NDR_20150506
                            '_Hasta_' + FormatDateTime('yyyy-mm-dd', de_FechaHasta.Date) + '.TXT';                            //SS_1258_NDR_20151029-SS_1147_NDR_20150506
        {primero, crear las columnas}

		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------
        //lstDatos.Clear;
        Linea := 	PADR('Tipo',5,' ') + ' ' +
                    PADR('Fecha Emisi�n', 13,' ') + ' ' +
                    PADL('N�mero', 10,' ') + ' ' +
                    PADR('Cliente', 40,' ') + ' ' +
                    PADR('RUT', 12,' ') + ' ' +
                    ifThen(Columna1<>'' , PADL(Columna1,  15,' ') + ' ','')+
                    ifThen(Columna2<>'' , PADL(Columna2,  15,' ') + ' ','')+
                    ifThen(Columna3<>'' , PADL(Columna3,  15,' ') + ' ','')+
                    ifThen(Columna4<>'' , PADL(Columna4,  15,' ') + ' ','')+
                    ifThen(Columna5<>'' , PADL(Columna5,  15,' ') + ' ','')+
                    ifThen(Columna6<>'' , PADL(Columna6,  15,' ') + ' ','')+
                    ifThen(Columna7<>'' , PADL(Columna7,  15,' ') + ' ','')+
                    ifThen(Columna8<>'' , PADL(Columna8,  15,' ') + ' ','')+
                    ifThen(Columna9<>'' , PADL(Columna9,  15,' ') + ' ','')+
                    ifThen(Columna10<>'', PADL(Columna10, 15,' ') + ' ','')+
                    // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
                    ifThen(Columna11<>'', PADL(Columna11, 15,' ') + ' ','')+
                    ifThen(Columna12<>'', PADL(Columna12, 15,' ') + ' ','')+
                    ifThen(Columna13<>'', PADL(Columna13, 15,' ') + ' ','')+
                    ifThen(Columna14<>'', PADL(Columna14, 15,' ') + ' ','')+
                    ifThen(Columna15<>'', PADL(Columna15, 15,' ') + ' ','')+
                    // -------------------------------------------------------------------------------------
                    PADL('Otros Cargos', 15,' ') + ' ' +
                    PADL('I.V.A.', 15,' ') + ' ' +
                    PADL('I.V.A. Terceros', 15,' ') + ' ' +                     //SS_1006_1015_NDR_20121107
                    PADL('Total', 15,' ');
        lstDatos.Add(Linea);
        Linea := 	DupeString('-',85)+
                    ifThen(Columna1 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna2 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna3 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna4 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna5 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna6 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna7 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna8 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna9 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna10<>'',DupeString('-',15) + '-','')+
                    // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
                    ifThen(Columna11<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna12<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna13<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna14<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna15<>'',DupeString('-',15) + '-','')+
                    // -------------------------------------------------------------------------------------
                    DupeString('-',15) + '-' +
                    DupeString('-',15) + '-' +
                    DupeString('-',15) + '-' +                                  //SS_1006_1015_NDR_20121107
                    DupeString('-',15);
        lstDatos.Add(Linea);

        {ahora la data}
        Cuenta := 0;
        dbgDatos.DataSource.DataSet.First;
        dbgDatos.DataSource.DataSet.DisableControls;
        ResumenTipoDoc := Trim(UpperCase(dbgDatos.DataSource.DataSet.FieldByName('TipoComprobanteFiscal').AsString));
        ResumenMinDoc  := dbgDatos.DataSource.DataSet.FieldByName('NumeroComprobanteFiscal').AsInteger;
        ResumenMaxDoc  := ResumenMinDoc;

		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------
        CantidadDeDocumentos	:= 0;
        ResumenTotalAPagar		:= 0;
        Resumen_Col1    		:= 0;
        Resumen_Col2   		    := 0;
        Resumen_Col3    		:= 0;
        Resumen_Col4    		:= 0;
        Resumen_Col5    		:= 0;
        Resumen_Col6    		:= 0;
        Resumen_Col7    		:= 0;
        Resumen_Col8    		:= 0;
        Resumen_Col9    		:= 0;
        Resumen_Col10   		:= 0;
        // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
        Resumen_Col11   		:= 0;
        Resumen_Col12   		:= 0;
        Resumen_Col13   		:= 0;
        Resumen_Col14   		:= 0;
        Resumen_Col15   		:= 0;
        // -------------------------------------------------------------------------------------
        ResumenOtrosCargos		:= 0;
        ResumenIVA              := 0;
        ResumenIVATerceros      := 0;                                           //SS_1006_1015_NDR_20121107

		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------
        {sumas totales}
        ResumenSumaTotalApagar		:= 0;
        ResumenSuma_Col1            := 0;
        ResumenSuma_Col2            := 0;
        ResumenSuma_Col3            := 0;
        ResumenSuma_Col4            := 0;
        ResumenSuma_Col5            := 0;
        ResumenSuma_Col6            := 0;
        ResumenSuma_Col7            := 0;
        ResumenSuma_Col8            := 0;
        ResumenSuma_Col9            := 0;
        ResumenSuma_Col10           := 0;
        // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
        ResumenSuma_Col11           := 0;
        ResumenSuma_Col12           := 0;
        ResumenSuma_Col13           := 0;
        ResumenSuma_Col14           := 0;
        ResumenSuma_Col15           := 0;
        // -------------------------------------------------------------------------------------
        ResumenSumaOtrosCargos		:= 0;
        ResumenSumaIVA              := 0;
        ResumenSumaIVATerceros      := 0;                                       //SS_1006_1015_NDR_20121107

		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------
    	while not dbgDatos.DataSource.DataSet.Eof do begin
            Linea :=    PADR(Trim(dbgDatos.DataSource.DataSet.FieldByName('TipoComprobante').AsString),2,' ') + ' ' +
                        PADR(Trim(dbgDatos.DataSource.DataSet.FieldByName('TipoComprobanteFiscal').AsString),2,' ') + ' ' +
                    	PADR(Trim(dbgDatos.DataSource.DataSet.FieldByName('FechaEmisionStr').AsString),13,' ') + ' ' +
                    	PADL(Trim(dbgDatos.DataSource.DataSet.FieldByName('NumeroComprobanteFiscal').AsString),10,' ') + ' ' +
                    	PADR(Trim(dbgDatos.DataSource.DataSet.FieldByName('Cliente').AsString),40,' ') + ' ' +
                    	PADR(Trim(dbgDatos.DataSource.DataSet.FieldByName('RUT').AsString),12,' ')+ ' ' +
                        IfThen( Columna1<>'',  PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_1').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna2<>'',  PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_2').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna3<>'',  PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_3').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna4<>'',  PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_4').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna5<>'',  PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_5').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna6<>'',  PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_6').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna7<>'',  PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_7').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna8<>'',  PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_8').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna9<>'',  PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_9').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna10<>'', PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_10').AsFloat]),15,' ') + ' ' ,'') +
                        // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
                        IfThen( Columna11<>'', PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_11').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna12<>'', PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_12').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna13<>'', PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_13').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna14<>'', PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_14').AsFloat]),15,' ') + ' ' ,'') +
                        IfThen( Columna15<>'', PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('Col_15').AsFloat]),15,' ') + ' ' ,'') +
                        // -------------------------------------------------------------------------------------
                        PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('OtrosCargos').AsFloat]),15,' ') + ' ' +
                        PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('IVA').AsFloat]),15,' ') + ' ' +
                        PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('IVATerceros').AsFloat]),15,' ') + ' ' +        //SS_1006_1015_NDR_20121107
                        PADL(Format('%.0n',[dbgDatos.DataSource.DataSet.FieldByName('TotalComprobante').AsFloat]),15,' ');
            lstDatos.Add(Linea);

            {ver comprobantes que no balancean}
            if dbgDatos.DataSource.DataSet.FieldByName('TieneDiferencia').AsString = '*' then begin
            	Linea := 	dbgDatos.DataSource.DataSet.FieldByName('TipoComprobante').AsString + ' - ' +
                            dbgDatos.DataSource.DataSet.FieldByName('NumeroComprobante').AsString;
                ListarComprobantesQueNoBalancean(True, Linea);
            end;

            {ver datos para el resumen}
            if ResumenMinDoc > dbgDatos.DataSource.DataSet.FieldByName('NumeroComprobanteFiscal').AsInteger then begin
                ResumenMinDoc := dbgDatos.DataSource.DataSet.FieldByName('NumeroComprobanteFiscal').AsInteger;
            end;

            if ResumenMaxDoc < dbgDatos.DataSource.DataSet.FieldByName('NumeroComprobanteFiscal').AsInteger then begin
                ResumenMaxDoc := dbgDatos.DataSource.DataSet.FieldByName('NumeroComprobanteFiscal').AsInteger;
            end;

		    // Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------
            Inc(CantidadDeDocumentos);
            Inc(Resumen_Col1, dbgDatos.DataSource.DataSet.FieldByName('Col_1').AsInteger);
            Inc(Resumen_Col2, dbgDatos.DataSource.DataSet.FieldByName('Col_2').AsInteger);
            Inc(Resumen_Col3, dbgDatos.DataSource.DataSet.FieldByName('Col_3').AsInteger);
            Inc(Resumen_Col4, dbgDatos.DataSource.DataSet.FieldByName('Col_4').AsInteger);
            Inc(Resumen_Col5, dbgDatos.DataSource.DataSet.FieldByName('Col_5').AsInteger);
            Inc(Resumen_Col6, dbgDatos.DataSource.DataSet.FieldByName('Col_6').AsInteger);
            Inc(Resumen_Col7, dbgDatos.DataSource.DataSet.FieldByName('Col_7').AsInteger);
            Inc(Resumen_Col8, dbgDatos.DataSource.DataSet.FieldByName('Col_8').AsInteger);
            Inc(Resumen_Col9, dbgDatos.DataSource.DataSet.FieldByName('Col_9').AsInteger);
            Inc(Resumen_Col10, dbgDatos.DataSource.DataSet.FieldByName('Col_10').AsInteger);
            // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
            Inc(Resumen_Col11, dbgDatos.DataSource.DataSet.FieldByName('Col_11').AsInteger);
            Inc(Resumen_Col12, dbgDatos.DataSource.DataSet.FieldByName('Col_12').AsInteger);
            Inc(Resumen_Col13, dbgDatos.DataSource.DataSet.FieldByName('Col_13').AsInteger);
            Inc(Resumen_Col14, dbgDatos.DataSource.DataSet.FieldByName('Col_14').AsInteger);
            Inc(Resumen_Col15, dbgDatos.DataSource.DataSet.FieldByName('Col_15').AsInteger);
            // -------------------------------------------------------------------------------------
            Inc(ResumenOtrosCargos, dbgDatos.DataSource.DataSet.FieldByName('OtrosCargos').AsInteger);
            Inc(ResumenTotalAPagar, dbgDatos.DataSource.DataSet.FieldByName('TotalComprobante').AsInteger);
            Inc(ResumenIVA, dbgDatos.DataSource.DataSet.FieldByName('IVA').AsInteger);
            Inc(ResumenIVATerceros, dbgDatos.DataSource.DataSet.FieldByName('IVATerceros').AsInteger);                              //SS_1006_1015_NDR_20121107
     		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------
            dbgDatos.DataSource.DataSet.Next;
            if (not dbgDatos.DataSource.DataSet.Eof) and
               (UpperCase(Trim(dbgDatos.DataSource.DataSet.FieldByName('TipoComprobanteFiscal').AsString)) <> ResumenTipoDoc) then begin
            	Linea := 	PADR(Trim(ResumenTipoDoc),10,' ') + ' ' +
                			PADL(IntToStr(ResumenMinDoc),10,' ')  + ' ' +
                            PADL(IntToStr(ResumenMaxDoc),10,' ')  + ' ' +
                            PADL(Format('%.0n',[CantidadDeDocumentos * 1.0]),10,' ')+' ' +
                            IfThen( Columna1<>'', PADL(Format('%.0n',[Resumen_Col1 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna2<>'', PADL(Format('%.0n',[Resumen_Col2 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna3<>'', PADL(Format('%.0n',[Resumen_Col3 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna4<>'', PADL(Format('%.0n',[Resumen_Col4 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna5<>'', PADL(Format('%.0n',[Resumen_Col5 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna6<>'', PADL(Format('%.0n',[Resumen_Col6 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna7<>'', PADL(Format('%.0n',[Resumen_Col7 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna8<>'', PADL(Format('%.0n',[Resumen_Col8 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna9<>'', PADL(Format('%.0n',[Resumen_Col9 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna10<>'', PADL(Format('%.0n',[Resumen_Col10 * 1.0]),15,' ') + ' ' ,'') +
                            // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
                            IfThen( Columna11<>'', PADL(Format('%.0n',[Resumen_Col11 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna12<>'', PADL(Format('%.0n',[Resumen_Col12 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna13<>'', PADL(Format('%.0n',[Resumen_Col13 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna14<>'', PADL(Format('%.0n',[Resumen_Col14 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen( Columna15<>'', PADL(Format('%.0n',[Resumen_Col15 * 1.0]),15,' ') + ' ' ,'') +
                            // -------------------------------------------------------------------------------------
                            PADL(Format('%.0n',[ResumenOtrosCargos * 1.0]),15,' ') + ' ' +
                            PADL(Format('%.0n',[ResumenIVA * 1.0]),15,' ') + ' ' +
                            PADL(Format('%.0n',[ResumenIVATerceros * 1.0]),15,' ') + ' ' +                                          //SS_1006_1015_NDR_20121107
                            PADL(Format('%.0n',[ResumenTotalAPagar * 1.0]),15,' ') ;
                lstResumen.Add(Linea);

     		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------
                Inc(ResumenSumaTotalApagar, ResumenTotalAPagar);
                Inc(ResumenSuma_Col1, Resumen_Col1);
                Inc(ResumenSuma_Col2, Resumen_Col2);
                Inc(ResumenSuma_Col3, Resumen_Col3);
                Inc(ResumenSuma_Col4, Resumen_Col4);
                Inc(ResumenSuma_Col5, Resumen_Col5);
                Inc(ResumenSuma_Col6, Resumen_Col6);
                Inc(ResumenSuma_Col7, Resumen_Col7);
                Inc(ResumenSuma_Col8, Resumen_Col8);
                Inc(ResumenSuma_Col9, Resumen_Col9);
                Inc(ResumenSuma_Col10, Resumen_Col10);
                // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
                Inc(ResumenSuma_Col11, Resumen_Col11);
                Inc(ResumenSuma_Col12, Resumen_Col12);
                Inc(ResumenSuma_Col13, Resumen_Col13);
                Inc(ResumenSuma_Col14, Resumen_Col14);
                Inc(ResumenSuma_Col15, Resumen_Col15);
                // -------------------------------------------------------------------------------------
                Inc(ResumenSumaOtrosCargos, ResumenOtrosCargos);
                Inc(ResumenSumaIVA, ResumenIVA);
                Inc(ResumenSumaIVATerceros, ResumenIVATerceros);                                                                    //SS_1006_1015_NDR_20121107

                ResumenTipoDoc := Trim(UpperCase(dbgDatos.DataSource.DataSet.FieldByName('TipoComprobanteFiscal').AsString));

                ResumenMinDoc  := dbgDatos.DataSource.DataSet.FieldByName('NumeroComprobanteFiscal').AsInteger;
                ResumenMaxDoc  := ResumenMinDoc;
                ResumenTotalAPagar := 0;

     		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------
            	Resumen_Col1 := 0;
            	Resumen_Col2 := 0;
            	Resumen_Col3 := 0;
            	Resumen_Col4 := 0;
            	Resumen_Col5 := 0;
            	Resumen_Col6 := 0;
            	Resumen_Col7 := 0;
            	Resumen_Col8 := 0;
            	Resumen_Col9 := 0;
            	Resumen_Col10 := 0;
                // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
            	Resumen_Col11 := 0;
            	Resumen_Col12 := 0;
            	Resumen_Col13 := 0;
            	Resumen_Col14 := 0;
            	Resumen_Col15 := 0;
                // -------------------------------------------------------------------------------------
            	ResumenIVA    := 0;
            	ResumenIVATerceros:= 0;                                                                                             //SS_1006_1015_NDR_20121107

            	ResumenOtrosCargos := 0;
                CantidadDeDocumentos := 0;

            end;

            Inc(Cuenta);
            if (Cuenta mod 100000) = 0 then begin
                lbl_Mensaje.Caption := MSG_GUARDANDO_MEM + Format(MSG_PROCESADOS, [Cuenta * 1.0]);
                Application.ProcessMessages;
            end;

    	end;

        {se acabaron los registros, pero hay data resumen del �ltimo tipo}
        {imprimir}
   		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------
         Linea := 	 PADR(Trim(ResumenTipoDoc),10,' ') + ' ' +
                     PADL(IntToStr(ResumenMinDoc),10,' ')  + ' ' +
                     PADL(IntToStr(ResumenMaxDoc),10,' ')  + ' ' +
                     PADL(Format('%.0n',[CantidadDeDocumentos * 1.0]),10,' ')+' ' +
                     IfThen( Columna1<>'', PADL(Format('%.0n',[Resumen_Col1 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna2<>'', PADL(Format('%.0n',[Resumen_Col2 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna3<>'', PADL(Format('%.0n',[Resumen_Col3 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna4<>'', PADL(Format('%.0n',[Resumen_Col4 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna5<>'', PADL(Format('%.0n',[Resumen_Col5 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna6<>'', PADL(Format('%.0n',[Resumen_Col6 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna7<>'', PADL(Format('%.0n',[Resumen_Col7 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna8<>'', PADL(Format('%.0n',[Resumen_Col8 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna9<>'', PADL(Format('%.0n',[Resumen_Col9 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna10<>'', PADL(Format('%.0n',[Resumen_Col10 * 1.0]),15,' ') + ' ' ,'') +
                     // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
                     IfThen( Columna11<>'', PADL(Format('%.0n',[Resumen_Col11 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna12<>'', PADL(Format('%.0n',[Resumen_Col12 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna13<>'', PADL(Format('%.0n',[Resumen_Col13 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna14<>'', PADL(Format('%.0n',[Resumen_Col14 * 1.0]),15,' ') + ' ' ,'') +
                     IfThen( Columna15<>'', PADL(Format('%.0n',[Resumen_Col15 * 1.0]),15,' ') + ' ' ,'') +
                     // -------------------------------------------------------------------------------------
                     PADL(Format('%.0n',[ResumenOtrosCargos * 1.0]),15,' ') + ' ' +
                     PADL(Format('%.0n',[ResumenIVA * 1.0]),15,' ') + ' ' +
                     PADL(Format('%.0n',[ResumenIVATerceros * 1.0]),15,' ') + ' ' +                                         //SS_1006_1015_NDR_20121107
                     PADL(Format('%.0n',[ResumenTotalAPagar * 1.0]),15,' ') ;

        lstResumen.Add(Linea);

        {sumar al total}
        Inc(ResumenSumaTotalApagar, ResumenTotalAPagar);

  		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------
        Inc(ResumenSuma_Col1, Resumen_Col1);
        Inc(ResumenSuma_Col2, Resumen_Col2);
        Inc(ResumenSuma_Col3, Resumen_Col3);
        Inc(ResumenSuma_Col4, Resumen_Col4);
        Inc(ResumenSuma_Col5, Resumen_Col5);
        Inc(ResumenSuma_Col6, Resumen_Col6);
        Inc(ResumenSuma_Col7, Resumen_Col7);
        Inc(ResumenSuma_Col8, Resumen_Col8);
        Inc(ResumenSuma_Col9, Resumen_Col9);
        Inc(ResumenSuma_Col10, Resumen_Col10);
        // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
        Inc(ResumenSuma_Col11, Resumen_Col11);
        Inc(ResumenSuma_Col12, Resumen_Col12);
        Inc(ResumenSuma_Col13, Resumen_Col13);
        Inc(ResumenSuma_Col14, Resumen_Col14);
        Inc(ResumenSuma_Col15, Resumen_Col15);
        // -------------------------------------------------------------------------------------

        Inc(ResumenSumaOtrosCargos, ResumenOtrosCargos);
        Inc(ResumenSumaIVA, ResumenIVA);
        Inc(ResumenSumaIVATerceros, ResumenIVATerceros);                                        //SS_1006_1015_NDR_20121107
        dbgDatos.DataSource.DataSet.EnableControls;

        if FGrabarLog then begin
        	FActualNow := NowBase(DMConnections.BaseCAC);
    		GrabarMensajeEnArchivoLog(	FLogFilePath, FLogFileName, FLogFileExtension,
        							MSG_GUARDANDO + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow, FAnteriorNow)),
                                    FMachineName, FUser);
        	FAnteriorNow := NowBase(DMConnections.BaseCAC);
        end;

        {insertar el Resumen de Concepto al Comienzo del archivo}
        for i := lstResumenConcepto.Count - 1 downto 0 do lstDatos.Insert(0, lstResumenConcepto[i]);

        {insertar el Resumen}
        lstDatos.Insert(0, SeparadorResumen);
        lstDatos.Insert(0, ' ');
        lstDatos.Insert(0, ' ');
        	{NK Manual}

   		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------
        lstDatos.Insert(0,	PADR(IndicadorDeTotal,44,' ')+
                            IfThen(Columna1  <> '' , PADL( Format('%.0n',[ResumenSuma_Col1 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna2  <> '' , PADL( Format('%.0n',[ResumenSuma_Col2 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna3  <> '' , PADL( Format('%.0n',[ResumenSuma_Col3 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna4  <> '' , PADL( Format('%.0n',[ResumenSuma_Col4 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna5  <> '' , PADL( Format('%.0n',[ResumenSuma_Col5 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna6  <> '' , PADL( Format('%.0n',[ResumenSuma_Col6 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna7  <> '' , PADL( Format('%.0n',[ResumenSuma_Col7 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna8  <> '' , PADL( Format('%.0n',[ResumenSuma_Col8 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna9  <> '' , PADL( Format('%.0n',[ResumenSuma_Col9 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna10 <> '' , PADL( Format('%.0n',[ResumenSuma_Col10 * 1.0]),15,' ') + ' ' ,'') +
                            // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
                            IfThen(Columna11 <> '' , PADL( Format('%.0n',[ResumenSuma_Col11 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna12 <> '' , PADL( Format('%.0n',[ResumenSuma_Col12 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna13 <> '' , PADL( Format('%.0n',[ResumenSuma_Col13 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna14 <> '' , PADL( Format('%.0n',[ResumenSuma_Col14 * 1.0]),15,' ') + ' ' ,'') +
                            IfThen(Columna15 <> '' , PADL( Format('%.0n',[ResumenSuma_Col15 * 1.0]),15,' ') + ' ' ,'') +
                            // -------------------------------------------------------------------------------------
                            PADL( Format('%.0n',[ResumenSumaOtrosCargos * 1.0]),15,' ') + ' ' +
                            PADL( Format('%.0n',[ResumenSumaIVA * 1.0]),15,' ') + ' '  +
                            PADL( Format('%.0n',[ResumenSumaIVATerceros * 1.0]),15,' ') + ' '  +                            //SS_1006_1015_NDR_20121107
                            PADL( Format('%.0n',[ResumenSumaTotalAPagar * 1.0]),15,' ') );

        Linea := 	DupeString('-',44)  +
                    ifThen(Columna1 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna2 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna3 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna4 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna5 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna6 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna7 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna8 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna9 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna10<>'',DupeString('-',15) + '-','')+
                    // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
                    ifThen(Columna11<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna12<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna13<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna14<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna15<>'',DupeString('-',15) + '-','')+
                    // -------------------------------------------------------------------------------------
                    DupeString('-',15) + '-' +
                    DupeString('-',15) + '-' +
                    DupeString('-',15) + '-' +                                  //SS_1006_1015_NDR_20121107
                    DupeString('-',15);
        lstDatos.Insert(0,Linea);


        {insertar el Resumen al Comienzo del archivo}
        for i := lstResumen.Count - 1 downto 0 do lstDatos.Insert(0, lstResumen[i]);

   		// Rev.17 / 29-Diciembre-2009 / Nelson Droguett Sierra---------------------------------

        Linea := 	DupeString('-',44)  +
                    ifThen(Columna1 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna2 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna3 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna4 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna5 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna6 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna7 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna8 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna9 <>'',DupeString('-',15) + '-','')+
                    ifThen(Columna10<>'',DupeString('-',15) + '-','')+
                    // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
                    ifThen(Columna11<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna12<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna13<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna14<>'',DupeString('-',15) + '-','')+
                    ifThen(Columna15<>'',DupeString('-',15) + '-','')+
                    // -------------------------------------------------------------------------------------
                    DupeString('-',15) + '-' +
                    DupeString('-',15) + '-' +
                    DupeString('-',15) + '-' +                                  //SS_1006_1015_NDR_20121107
                    DupeString('-',15);
        lstDatos.Insert(0,Linea);

        Linea := 	PADR('Tipo          Inicial      Final   Cantidad',44,' ')+
                    ifThen(Columna1 <>'',PADL(Columna1, 15,' ') + ' ','')+
                    ifThen(Columna2 <>'',PADL(Columna2, 15,' ') + ' ','')+
                    ifThen(Columna3 <>'',PADL(Columna3, 15,' ') + ' ','')+
                    ifThen(Columna4 <>'',PADL(Columna4, 15,' ') + ' ','')+
                    ifThen(Columna5 <>'',PADL(Columna5, 15,' ') + ' ','')+
                    ifThen(Columna6 <>'',PADL(Columna6, 15,' ') + ' ','')+
                    ifThen(Columna7 <>'',PADL(Columna7, 15,' ') + ' ','')+
                    ifThen(Columna8 <>'',PADL(Columna8, 15,' ') + ' ','')+
                    ifThen(Columna9 <>'',PADL(Columna9, 15,' ') + ' ','')+
                    ifThen(Columna10<>'',PADL(Columna10, 15,' ') + ' ','')+
                    // Rev.18 / 05-Enero-2010 / Nelson Droguett Sierra -------------------------------------
                    ifThen(Columna11<>'',PADL(Columna11, 15,' ') + ' ','')+
                    ifThen(Columna12<>'',PADL(Columna12, 15,' ') + ' ','')+
                    ifThen(Columna13<>'',PADL(Columna13, 15,' ') + ' ','')+
                    ifThen(Columna14<>'',PADL(Columna14, 15,' ') + ' ','')+
                    ifThen(Columna15<>'',PADL(Columna15, 15,' ') + ' ','')+
                    // -------------------------------------------------------------------------------------
                    PADL('Otros Cargos', 15,' ') + ' ' +
                    PADL('I.V.A.', 15,' ') + ' ' +
                    PADL('I.V.A.Terceros', 15,' ') + ' ' +                      //SS_1006_1015_NDR_20121107
                    PADL('Total', 15,' ');
        lstDatos.Insert(0,Linea);


        {insertar la fecha desde-hasta del proceso}
        lstDatos.Insert(0, FormatDateTime('dd-mm-yyyy', de_FechaDesde.Date) + ' - ' + FormatDateTime('dd-mm-yyyy', de_FechaHasta.Date));


        {grabar a Disco}
        lstDatos.SaveToFile(NombreArchivo);

        if FGrabarLog then begin
        	FActualNow := NowBase(DMConnections.BaseCAC);
    		GrabarMensajeEnArchivoLog(	FLogFilePath, FLogFileName, FLogFileExtension,
        							MSG_GUARDADO + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow, FAnteriorNow)),
                                    FMachineName, FUser);
        	FAnteriorNow := NowBase(DMConnections.BaseCAC);
        end;

        lbl_Mensaje.Caption := MSG_GUARDADO;
        Application.ProcessMessages;

        {ver si est� ingresado el directorio para mover el archivo}
        if ObtenerParametroGeneral(DMConnections.BaseCAC, PARAM_DIR_SALIDA, DirSalida) then begin

            Linea := GoodDir(DirSalida) + ExtractFileName(NombreArchivo);
        	MoverArchivoProcesado(Caption, NombreArchivo, Linea);

        end
        else
        	MsgBox(	Format(MSG_SIN_PARAM, [PARAM_DIR_SALIDA]) + CRLF +  CRLF +
            		//MSG_SIN_PARAM_2 +  CRLF + CRLF +                              // SS_1006_1015_20120912_CQU
                    Format(MSG_SIN_PARAM_2, [FDirectorioTemporal]) +  CRLF + CRLF + // SS_1006_1015_20120912_CQU
                    MSG_ARCHIVO + NombreArchivo, Caption,MB_ICONEXCLAMATION);
    end
    else begin
        lbl_Mensaje.Caption := MSG_NO_HAY;
        MsgBox(MSG_NO_HAY, Caption, MB_ICONEXCLAMATION);
    end;

    lstDatos.Free;
    lstResumen.Free;
    lstResumenConcepto.Free;
    //lstNoCuadran.Free;
end;
{******************************** Function Header ******************************
Function Name: ImprimirReporte
Author : ggomez
Date Created : 02/09/2005
Description : Muestra el reporte con el Libro Auxiliar de Ventas de Peajes.
Parameters : None
Return Value : None

Revision : 1
    Date: 02/03/2009
    Author: mpiazza
    Description:    Ref (SRX01226) se cambia" now " por nowbase

*******************************************************************************}
procedure TfrmReporteLibroVentasPeajes.ImprimirReporte;
var
    Res: Boolean;
begin
    if FGrabarLog then begin
        GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, 'ImprimirReporte.CargarColumnasReporte ...', FMachineName, FUser);
        FAnteriorNow := NowBase(DMConnections.BaseCAC);
    end;

    Res := CargarColumnasReporte;

    if FGrabarLog then begin
        FActualNow := NowBase(DMConnections.BaseCAC);
        GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
            'Fin ImprimirReporte.CargarColumnasReporte.'
                + '   ' + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow, FAnteriorNow)),
            FMachineName, FUser);
    end;

    if Res then begin
        rbi_LibroVentasPeajes.Execute;

        if FGrabarLog then begin
            FActualNow2 := NowBase(DMConnections.BaseCAC);
            GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                'Fin Generando Reporte.'
                    + '   ' + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow2, FAnteriorNow2)),
                FMachineName, FUser);
        end;

    end else begin
        Exit;
    end;


end;

{-----------------------------------------------------------------------------
  Function Name: rbi_LibroVentasPeajesExecute
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject; var Cancelled: Boolean
  Return Value: N/A

  Revision : 1
    Date: 02/03/2009
    Author: mpiazza
    Description:  Ref (SRX01226) se cambia" now " por nowbase

-----------------------------------------------------------------------------}
procedure TfrmReporteLibroVentasPeajes.rbi_LibroVentasPeajesExecute(
  Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_NO_HAY_DATOS                = 'No hay datos para generar el reporte.';
    MSG_ERROR_AL_GENERAR_REPORTE    = 'Ha ocurrido un error al generar el reporte.';
begin
    // Almaceno el valor para poder saber cuanto demora desde que se presiona OK en
    // la ventana que permite seleccionar el modo de impresion del reporte.
    if FGrabarLog then begin
        GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, 'Generando Reporte ...', FMachineName, FUser);
        FAnteriorNow2 := NowBase(DMConnections.BaseCAC);
    end;

    try
        with sp_LibroAuxVentasPeajes_ObtenerDatosReporte do begin
            Close;
            CommandTimeout := 15000;

            if FGrabarLog then begin
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension, '      Obteniendo datos del reporte.', FMachineName, FUser);
                FAnteriorNow := NowBase(DMConnections.BaseCAC);
            end;

            Open;

            if FGrabarLog then begin
                FActualNow := NowBase(DMConnections.BaseCAC);
                GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                    '      Fin Obteniendo datos del reporte.'
                        + '   ' + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow, FAnteriorNow)),
                    FMachineName, FUser);
            end;

            if IsEmpty then begin
                Cancelled := True;
                MsgBox(MSG_NO_HAY_DATOS, Self.Caption, MB_ICONINFORMATION);
            end else begin
                // Setear el per�odo en el reporte.
                lblPeriodoLibroPeaje.Caption := FormatDateTime('dd/mm/yyyy', de_FechaDesde.Date)
                                                + ' - ' + FormatDateTime('dd/mm/yyyy', de_FechaHasta.Date);
            end; // else if
        end;
    except
        on E: Exception do begin
            Cancelled := True;
            raise Exception.Create(MSG_ERROR_AL_GENERAR_REPORTE
                + CRLF + E.Message);
        end;
    end;

end;

{******************************** Function Header ******************************
Function Name: ppl_TotalPrint
Author : ggomez
Date Created : 04/10/2006
Description :
Parameters : Sender: TObject
Return Value : None
Revision : 1
    Author : vpaszkowicz
    Date : 04/10/2006
    Description : La cambie los captions para diferenciar lo que es nota de
    cr�dito para notas de cobro de las notas de cr�dito con desgloce de IVA. Le
    agregu� el caption para esta �ltima.
*******************************************************************************}

procedure TfrmReporteLibroVentasPeajes.ppl_TotalPrint(Sender: TObject);

resourcestring
    CAPTION_TOTAL_COMPROBANTES          = 'Total de %s';
var
    TipoComprobante, Descripcion : String;
begin
    //Revision 9
    TipoComprobante := trim (sp_LibroAuxVentasPeajes_ObtenerDatosReporte.FieldByName('TipoComprobante').AsString);
    Descripcion :=  QueryGetValue (DMConnections.BaseCAC,
                    format('Select dbo.ObtenerDescripcionComprobante(''%s'')',[TipoComprobante]));

    // Setear el label Total del Group Footer de Total de Comprobante
    ppl_Total.Caption := Format(CAPTION_TOTAL_COMPROBANTES, [Descripcion]);

end;

procedure TfrmReporteLibroVentasPeajes.ppl_TotalFacturacionMasivaPrint(
  Sender: TObject);
begin
    ppl_TotalFacturacionMasiva.Visible  := sp_LibroAuxVentasPeajes_ObtenerDatosReporte.FieldByName('TipoComprobante').AsString = TC_NOTA_COBRO;
    ppl_TotalFacturacionManual.Visible  := ppl_TotalFacturacionMasiva.Visible;

    dbc_TotalPeajesEmitidosMasivamente.Visible := ppl_TotalFacturacionMasiva.Visible;
    dbc_TotalPeajesEmitidosManualmente.Visible := ppl_TotalFacturacionMasiva.Visible;

    ppDBCalc1.Visible := ppl_TotalFacturacionMasiva.Visible;
    ppDBCalc2.Visible := ppl_TotalFacturacionMasiva.Visible;
    dbc_InteresesMasivo.Visible         := ppl_TotalFacturacionMasiva.Visible;
    dbc_InteresesManual.Visible         := ppl_TotalFacturacionMasiva.Visible;
    dbc_RecupGastosAdminMasivo.Visible  := ppl_TotalFacturacionMasiva.Visible;
    dbc_RecupGastosAdminManual.Visible  := ppl_TotalFacturacionMasiva.Visible;
    dbc_DescuentosMasivo.Visible        := ppl_TotalFacturacionMasiva.Visible;
    dbc_DescuentosManual.Visible        := ppl_TotalFacturacionMasiva.Visible;
    dbc_OtrosCargosMasivo.Visible       := ppl_TotalFacturacionMasiva.Visible;
    dbc_OtrosCargosManual.Visible       := ppl_TotalFacturacionMasiva.Visible;
    dbc_AjusteSencilloActualMasivo.Visible      := ppl_TotalFacturacionMasiva.Visible;
    dbc_AjusteSencilloActualManual.Visible      := ppl_TotalFacturacionMasiva.Visible;
    dbc_AjusteSencilloAnteriorMasivo.Visible    := ppl_TotalFacturacionMasiva.Visible;
    dbc_AjusteSencilloAnteriorManual.Visible    := ppl_TotalFacturacionMasiva.Visible;
    dbc_TotalAPagarManual.Visible := ppl_TotalFacturacionMasiva.Visible;
    dbc_TotalAPagarMasivo.Visible := ppl_TotalFacturacionMasiva.Visible;

end;


//REVISION 9
{******************************** Function Header ******************************
Function Name: CargarColumnasReporte
Author : FSandi
Date Created : 19-06-2007
Description : Carga los nombres de columnas al reporte
            Se fija un total de 5 columnas para el reporte
Parameters : None
Return Value : None
*******************************************************************************}
Function TfrmReporteLibroVentasPeajes.CargarColumnasReporte : Boolean;
var
    Columnas : integer;
begin
    SP_ObtenerColumnasLibro.Parameters.ParamByName('@IdLibro').Value := 1;
    SP_ObtenerColumnasLibro.Open;
    Columnas := SP_ObtenerColumnasLibro.RecordCount;
    if Columnas <> 5 then begin
        Result := False;
    end else begin
        SP_ObtenerColumnasLibro.First;
        ppLabel6.Caption := SP_ObtenerColumnasLibro.FieldByName('Descripcion').AsString;
        SP_ObtenerColumnasLibro.Next;
        ppLabel7.Caption := SP_ObtenerColumnasLibro.FieldByName('Descripcion').AsString;
        SP_ObtenerColumnasLibro.Next;
        ppLabel8.Caption := SP_ObtenerColumnasLibro.FieldByName('Descripcion').AsString;
        SP_ObtenerColumnasLibro.Next;
        ppLabel9.Caption := SP_ObtenerColumnasLibro.FieldByName('Descripcion').AsString;
        SP_ObtenerColumnasLibro.Next;
        ppLabel10.Caption := SP_ObtenerColumnasLibro.FieldByName('Descripcion').AsString;
        SP_ObtenerColumnasLibro.Close;
        Result := True;
    end;
end;
//FIN DE REVISION 9

{******************************** Function Header ******************************
Function Name: CrearTablasTemporales
Author : ggomez
Date Created : 05/09/2005
Description : Crea las tablas temporales que se utilizan en el Libro Auxiliar
    de Ventas de Peajes.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmReporteLibroVentasPeajes.CrearTablasTemporales;
var
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection       := DMConnections.BaseCAC;
        sp.ProcedureName    := 'LibroAuxVentasPeajes_CrearTablasTemporales';
        sp.ExecProc;
    finally
        sp.Free;
    end; // finally
end;

{******************************** Function Header ******************************
Function Name: ProcesarComprobantes
Author : ggomez
Date Created : 05/09/2005
Description : Procesa los comprobantes del tipo que se pasa como par�metro.
Parameters : TipoComprobante: AnsiString
Return Value : None

Revision : 1
    Date: 02/03/2009
    Author: mpiazza
    Description:    Ref (SRX01226) se cambia" now " por nowbase

*******************************************************************************}
procedure TfrmReporteLibroVentasPeajes.ProcesarComprobantes(
  TipoComprobante: AnsiString);
    {******************************** Function Header ******************************
    Function Name: GuardarDatos
    Author : ggomez
    Date Created : 21/02/2008
    Description : Para guardar en archivos en disco con los datos que se acaban de procesar.
    Parameters : None
    Return Value : None
    *******************************************************************************}
    procedure GuardarDatos;
    var
        q: TADOQuery;
        NombreArchivo: AnsiString;
    begin
        q := TADOQuery.Create(Self);
        try
            q.Connection        := DMConnections.BaseCAC;
            q.CommandTimeout    := 500;
            q.SQL.Text          := 'SELECT * FROM ##ComprobantesATratar WITH (NOLOCK) ORDER BY TipoComprobanteFiscal, NumeroComprobanteFiscal';

                q.Open;
                NombreArchivo := IncludeTrailingPathDelimiter(FLogFilePath) + 'LibroAuxPeajesCompTratar_D_'
                    + FormatDateTime('yyyymmdd', de_FechaDesde.Date)
                    + '_H_' + FormatDateTime('yyyymmdd', de_FechaHasta.Date)
                    + '_' + FormatDateTime('yyyymmdd_hhnnsszzz', NowBase(DMConnections.BaseCAC)) + '.xml';

                q.SaveToFile(NombreArchivo, pfXML);

                //q.Close;                                                                                                                      //SS-1015-NDR-20120517
                //q.SQL.Text := 'SELECT * FROM ##TEMPMovCuentas WITH (NOLOCK) ORDER BY TipoComprobanteFiscal, NumeroComprobanteFiscal';         //SS-1015-NDR-20120517
                //q.Open;                                                                                                                       //SS-1015-NDR-20120517
                //NombreArchivo := IncludeTrailingPathDelimiter(FLogFilePath) + 'LibroAuxPeajesMovsCuentas_D_'                                  //SS-1015-NDR-20120517
                //    + FormatDateTime('yyyymmdd', de_FechaDesde.Date)                                                                          //SS-1015-NDR-20120517
                //    + '_H_' + FormatDateTime('yyyymmdd', de_FechaHasta.Date)                                                                  //SS-1015-NDR-20120517
                //    + '_' + FormatDateTime('yyyymmdd_hhnnsszzz', NowBase(DMConnections.BaseCAC)) + '.xml';                                    //SS-1015-NDR-20120517
                //q.SaveToFile(NombreArchivo, pfXML);                                                                                           //SS-1015-NDR-20120517

                q.Close;
                q.SQL.Text := 'SELECT * FROM ConceptosMovimientoLibrosContables WITH (NOLOCK) ORDER BY CodigoConcepto, IdLibroContable, IdColumnaLibroContable';
                q.Open;
                NombreArchivo := IncludeTrailingPathDelimiter(FLogFilePath) + 'LibroAuxPeajesConcMovLibCont_D_'
                    + FormatDateTime('yyyymmdd', de_FechaDesde.Date)
                    + '_H_' + FormatDateTime('yyyymmdd', de_FechaHasta.Date)
                    + '_' + FormatDateTime('yyyymmdd_hhnnsszzz', NowBase(DMConnections.BaseCAC)) + '.xml';

                q.SaveToFile(NombreArchivo, pfXML);

        finally
            q.Close;
            q.Free;
        end;
    end;

resourcestring
    MSG_ERROR_PROCESAR_COMPROBANTES = 'Ha ocurrido un error al procesar los comprobantes "%s", desde el N�: %d.';
    MSG_ERROR_ASIGNACION_CONCEPTOS = 'Se encontr� un concepto asociado al documento que no tiene una columna asociada';
    MSG_DESEA_REINTENTAR = '�Desea reintentar el procesamiento de los comprobantes?';
var
    CantidadComprobantes: Int64;
    NumeroComprobanteDesde,
    NumeroComprobanteHasta: Int64;
    DescriError : String;
    Reintentar: Boolean;
begin
    // Inicializar las variables para que entre al menos una vez al ciclo.
    NumeroComprobanteDesde := -2;
    NumeroComprobanteHasta := -1;
    DescriError := EmptyStr;         //Revision 10

    // Inicializar la barra de progreso
    CantidadComprobantes := ObtenerCantidadComprobantes(DMConnections.BaseCAC,
                             de_FechaDesde.Date, de_FechaHasta.Date, TipoComprobante);
    pb_ProgresoGeneral.Position := 1;
    pb_ProgresoGeneral.Max      := CantidadComprobantes + 2;

    while (NumeroComprobanteDesde <> NumeroComprobanteHasta) and (not FCancelar) do begin

        NumeroComprobanteDesde := NumeroComprobanteHasta + 1;

        Reintentar := True;
        while (Reintentar) do begin
            try
                with sp_LibroAuxVentasPeajes_ProcesarComprobantes do begin
                    Close;
                    CommandTimeout := 15000;

                    Parameters.Refresh;
                    Parameters.ParamByName('@FechaDesde').Value := de_FechaDesde.Date;
                    Parameters.ParamByName('@FechaHasta').Value := de_FechaHasta.Date;
                    Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
                    Parameters.ParamByName('@NumeroComprobanteDesde').Value := NumeroComprobanteDesde;
                    Parameters.ParamByName('@NumeroComprobanteHasta').Value := 0;
                    Parameters.ParamByName('@DescriError').Value := EmptyStr; //Revision 10
                    ExecProc;

                    // Obtener el �ltimo N� de comprobante procesado.
                    NumeroComprobanteHasta := Parameters.ParamByName('@NumeroComprobanteHasta').Value;
                    //Revision 10
                    //Verificamos si hubo algun concepto sin asociaci�n de columna en el libro
                    DescriError := Parameters.ParamByName('@DescriError').Value;
                    if Trim(DescriError) <> EmptyStr then begin
                        GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                            Format(MSG_ERROR_PROCESAR_COMPROBANTES, [TipoComprobante, NumeroComprobanteDesde]) + '. Error: ' + Trim(DescriError),
                            FMachineName, FUser);
                        MsgBoxErr(MSG_ERROR_ASIGNACION_CONCEPTOS, DescriError, Self.Caption, MB_ICONSTOP);
                        FCancelar := True;
                        Exit;
                    end;
                    //Fin de Revision 10
                end; // with
                Reintentar := False;
            except
                on E: Exception do begin
                    // Si la excepci�n es por la PK de la tablas temporal ##ImportesConceptos que usa
                    // este proceso, entonces preguntar al operador si desea reintentar o si desea cancelar.
                    // Esto lo hice para dar la posibilidad de dejar registro del contexto en el que ocurre
                    // el problema reportado en la SS 621. Y hacemos el reintento para ver si se puede
                    // obtener el reporte.


                    // Dejar registro en el log del mensaje de error .
                    GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                        Format(MSG_ERROR_PROCESAR_COMPROBANTES, [TipoComprobante, NumeroComprobanteDesde]) + '. Error: ' + E.Message,
                        FMachineName, FUser);

                    // Dejar registro de los datos procesados, asi podemos investigarlos para descubrir que query obtuvo mal los datos.
                    GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                        'Guardando datos procesados ...',
                        FMachineName, FUser);
                    FAnteriorNow := NowBase(DMConnections.BaseCAC);
                    GuardarDatos;
                    FActualNow := NowBase(DMConnections.BaseCAC);
                    GrabarMensajeEnArchivoLog(FLogFilePath, FLogFileName, FLogFileExtension,
                        'Fin Guardando datos procesados ...'
                            + '   ' + ' .Tiempo ms: ' + IntToStr(MilliSecondsBetween(FActualNow, FAnteriorNow)),
                        FMachineName, FUser);

                    // los textos que se buscan son los nombres de las PK de las tablas temporales que usa este proceso.
                    // Ver Stored Procedure: LibroAuxVentasPeajes_CrearTablasTemporales.
                    // "LibroAuxPeajes_AmountsNotBalance" es el texto de la excepci�n que se lanza cuando hay diferencias en los montos
                    // los comprobantes procesados. Ver Stored Procedure: LibroAuxVentasPeajes_ProcesarComprobantes.
                    if (Pos('PK_LibroAuxPeajesTEMPImportesConceptos', E.Message) > 0)
                            or (Pos('PK_LibroAuxPeajesTEMPComprobantesATratar', E.Message) > 0)
                            or (Pos('PK_LibroAuxPeajesTEMPCompsReporte', E.Message) > 0)
                            or (Pos('LibroAuxPeajes_AmountsNotBalance', E.Message) > 0)
                            or (Pos('PK_LibroAuxPeajesImportesConceptosTEMP_MC', E.Message) > 0)
                            or (Pos('PK_LibroAuxPeajesImportesConceptosTEMP_LC', E.Message) > 0) then begin
                        Reintentar := MsgBox(Format(MSG_ERROR_PROCESAR_COMPROBANTES, [TipoComprobante, NumeroComprobanteDesde])
                            + CRLF + 'Error: ' + E.Message
                            + CRLF + CRLF + MSG_DESEA_REINTENTAR, Self.Caption, MB_ICONSTOP + MB_YESNO + MB_DEFBUTTON1) = mrYes;
                        // S�lo actualizar FCancelar si el operador NO desea reintentar.
                        if not Reintentar then begin
                            FCancelar := True;
                        end;
                    end else begin
                        raise Exception.Create(Format(MSG_ERROR_PROCESAR_COMPROBANTES, [TipoComprobante, NumeroComprobanteDesde])
                            + CRLF + 'Error: ' + E.Message);
                    end;
                end;
            end; // except
        end; // while (Reintentar)

        // Incrementar la barra de progreso
        pb_ProgresoGeneral.StepIt;
        if pb_ProgresoGeneral.Position >= pb_ProgresoGeneral.Max then begin
            // Inicializar la barra de progreso
            pb_ProgresoGeneral.Position := 1;
        end;

        // Refrescar la pantalla
        Application.ProcessMessages;
    end; // while

    // Finalizar la barra de progreso.
    pb_ProgresoGeneral.Position := pb_ProgresoGeneral.Max;
    Application.ProcessMessages;

end;

{******************************** Function Header ******************************
Function Name: ProcesoEnEjecucion
Author : ggomez
Date Created : 22/02/2008
Description : Retorna True si el proceso de generaci�n del Libro Auxiliar de
    Venta de Peajes, ya est� en ejecuci�n.
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TfrmReporteLibroVentasPeajes.ProcesoEnEjecucion: Boolean;
var
    q: TADOQuery;
begin
    q := TADOQuery.Create(Self);
    try
        q.Connection    := DMConnections.BaseCAC;
        q.SQL.Text      := 	'SELECT 1 FROM Tempdb.dbo.Sysobjects WITH (NOLOCK) WHERE Name = ''##CompsReporte''';
        q.Open;

        Result  := not q.IsEmpty;
    finally
        q.Close;
        q.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: EliminarTablasTemporales
Author : ggomez
Date Created : 05/09/2005
Description : Eliminar las tablas temporales creadas.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmReporteLibroVentasPeajes.EliminarTablasTemporales;
var
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection       := DMConnections.BaseCAC;
        sp.ProcedureName    := 'LibroAuxVentasPeajes_EliminarTablasTemporales';
        sp.ExecProc;
    finally
        sp.Free;
    end; // finally
end;

{******************************** Function Header ******************************
Function Name: ObtenerCantidadComprobantes
Author : ggomez
Date Created : 06/09/2005
Description : Retorna la cantidad de comprobantes de un Tipo que han sido
    emitidos entre dos fechas.
Parameters : FechaDesde, FechaHasta: TDateTime; TipoCompro: AnsiString
Return Value : Int64
*******************************************************************************}
function TfrmReporteLibroVentasPeajes.ObtenerCantidadComprobantes(Conn: TADOConnection;
    FechaDesde, FechaHasta: TDateTime; TipoCompro: AnsiString): Int64;
var
    codigolibro : integer;
begin
    CodigoLibro := QueryGetValueInt(Conn, 'SELECT DBO.CONST_LIBRO_AUX_VENTAS()');
    Result := QueryGetValueInt(Conn, 'SELECT dbo.LibroAuxVentasPeajes_CantidadComprobantes(' + QuotedStr(FormatDateTime('YYYYMMDD', FechaDesde)) + ', ' + QuotedStr(FormatDateTime('YYYYMMDD', FechaHasta)) + ', '+ InttoStr(CodigoLibro) +')');
end;


{-----------------------------------------------------------------------------
  Function Name: ListarComprobantesQueNoBalancean
  Author:
  Date Created:  /  /
  Description:
  Parameters:
  Return Value: N/A

  Revision : 1
    Date: 02/03/2009
    Author: mpiazza
    Description:  Ref (SRX01226) se cambia" now " por nowbase

-----------------------------------------------------------------------------}
procedure TfrmReporteLibroVentasPeajes.ListarComprobantesQueNoBalancean;
resourcestring
		MSG_LINEAS_CON_ERROR_ENCONTRADAS = 'Se han encontrado l�neas con errores de balanceo, se dejan en el archivo ';
        MSG_LISTADO = 'Listado de comprobantes que no balancean - ';
var
    Mensaje : string;
    DirectorioPredeterminado : String;
begin
	{************* Revision 13
	if sp_LibroAuxVentasPeajes_ObtenerDatosReporte.Active then
        sp_LibroAuxVentasPeajes_ObtenerDatosReporte.Close;

	CantidadLineasError := 0;
	sp_LibroAuxVentasPeajes_ObtenerDatosReporte.Open;
	while not sp_LibroAuxVentasPeajes_ObtenerDatosReporte.eof do begin
		if sp_LibroAuxVentasPeajes_ObtenerDatosReporte.FieldByName('TieneDiferencia').AsString = '*' then begin
			if CantidadLineasError = 0 then begin // solo creo el archivo si hay alguna linea
                Pnl_Inconsistencias.Visible := True;
				Mensaje :=  'Listado de comprobantes que no balancean - ' + FormatDateTime('yyyymmddhhnnss', now);
                meLineasError.Lines.Add(Mensaje);
			end;
			Mensaje := sp_LibroAuxVentasPeajes_ObtenerDatosReporte.FieldByName('TipoComprobante').AsString + ' - ' + sp_LibroAuxVentasPeajes_ObtenerDatosReporte.FieldByName('NumeroComprobante').AsString ;
            meLineasError.Lines.Add(Mensaje);
			CantidadLineasError := CantidadLineasError + 1 ;
		end;
		sp_LibroAuxVentasPeajes_ObtenerDatosReporte.Next;
	end;
	sp_LibroAuxVentasPeajes_ObtenerDatosReporte.Close;
	if CantidadLineasError > 0 then begin
        MsgBox(MSG_LINEAS_CON_ERROR_ENCONTRADAS, Self.Caption, MB_OK + MB_ICONINFORMATION);
        ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_LOG_LIBROAUXILIARPEAJES', DirectorioPredeterminado);
        DirectorioPredeterminado := GoodDir(DirectorioPredeterminado);
        dlgGuardarArchivoLog.InitialDir := DirectorioPredeterminado;
        dlgGuardarArchivoLog.FileName := 'LogLibroAuxiliarVentaPeajes' + FormatDateTime('yyyymmddhhnnss', now) + '.txt';
        if dlgGuardarArchivoLog.Execute then meLineasError.Lines.SaveToFile( dlgGuardarArchivoLog.FileName);
    end;

    Fin Revision 13 ******************
	}

    if EsMostrar then begin  {es agregar}
        if meLineasError.Lines.Count = 0 then begin
        	Pnl_Inconsistencias.Visible := True;
            Mensaje :=  MSG_LISTADO + FormatDateTime('yyyymmddhhnnss', NowBase(DMConnections.BaseCAC));
            meLineasError.Lines.Add(Mensaje);
        end;

        meLineasError.Lines.Add(Linea);
        meLineasError.Refresh;
    end
    else begin                 {hay que grabar el archivo}
    	MsgBox(MSG_LINEAS_CON_ERROR_ENCONTRADAS, Caption, MB_OK + MB_ICONINFORMATION);
        ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_LOG_LIBROAUXILIARPEAJES', DirectorioPredeterminado);
        DirectorioPredeterminado := GoodDir(DirectorioPredeterminado);
        dlgGuardarArchivoLog.InitialDir := DirectorioPredeterminado;
        dlgGuardarArchivoLog.FileName := 'LogLibroAuxiliarVentaPeajes' + FormatDateTime('yyyymmddhhnnss', NowBase(DMConnections.BaseCAC)) + '.txt';
        if dlgGuardarArchivoLog.Execute then begin
        	meLineasError.Lines.SaveToFile( dlgGuardarArchivoLog.FileName);
        end;
    end;


end;

{-----------------------------------------------------------------------------
  Function Name : CrearDirectorioTemporal
  Author        : CQuezada
  Date Created  : 12-Sep�tiembre-2012
  Description   : Intenta crear un directorio en la carpeta indicada en un oar�metro general

  Parameters    : None
  Return Value  : True or False

  Firma: SS_1006_1015_20120912_CQU
-----------------------------------------------------------------------------}
function TfrmReporteLibroVentasPeajes.CrearDirectorioTemporal;
resourcestring
    STR_DIRECTORY_NOT_EXISTS    = 'No existe el directorio: %s' + CRLF + 'Se intent� crear pero no fue posible'+ CRLF;
    MSG_ERROR_CHECK_GENERAL     = 'Error al validar el directorio temporal';
    MSG_ERROR_GENERAL           = 'Ha ocurrido un error al validar el directorio: %s ' + CRLF + 'Mensaje del sistema: %s'+ CRLF;
    MSG_EROR_TITULO             = 'Error';
var
    DescError : string;
begin
    FDirectorioTemporal := GoodDir(FDirectorioTemporal);
    if not DirectoryExists(FDirectorioTemporal) then begin
        try
            if ForceDirectories(FDirectorioTemporal) then
                Result := True
            else begin
                DescError := Format(STR_DIRECTORY_NOT_EXISTS, [FDirectorioTemporal]);
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL, DescError, MSG_EROR_TITULO, MB_ICONWARNING);
                Result := False;
            end
        except
            on e: Exception do begin
                DescError := Format(MSG_ERROR_GENERAL, [FDirectorioTemporal, e.Message]);
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL, DescError, MSG_EROR_TITULO, MB_ICONWARNING);
                Result := False;
            end;
        end;
    end else Result := True;
end;
end.
