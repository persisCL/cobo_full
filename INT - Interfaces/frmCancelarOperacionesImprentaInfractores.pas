{-----------------------------------------------------------------------------
 Unit Name: frmCancelarOperacionesImprentaInfractores.pas
 Author:
 Date Created: 
 Language: ES-AR
 Description: M�dulo para la anulacion de procesos masivos de impresion

 Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
  
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

-----------------------------------------------------------------------------}
unit frmCancelarOperacionesImprentaInfractores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BuscaTab, DB, ADODB, DmiCtrls, StdCtrls, ComCtrls, ExtCtrls, UtilProc,
  PeaProcs, PeaTypes, RStrings, DMConnection, ComunesInterfaces, Util, StrUtils;

type
  TFormCancelarNotificacionMasivaInfractores = class(TForm)
	Bevel1: TBevel;
    btnProcesar: TButton;
    btnSalir: TButton;
    spCancelarOperacionInterfazInfractores: TADOStoredProc;
	edProceso: TBuscaTabEdit;
    btOperacionesInterfaz: TBuscaTabla;
    spObtenerOperacionesInterfazInfractores: TADOStoredProc;
    lblFechaInterfase: TLabel;
    procedure btnProcesarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btOperacionesInterfazSelect(Sender: TObject; Tabla: TDataSet);
    function btOperacionesInterfazProcess(Tabla: TDataSet;var Texto: String): Boolean;
  private
    FCodigoOperacionInterfaseACancelar: LongInt;
    FEnProceso: Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	{ Private declarations }
  public
    function Inicializar: Boolean;
    { Public declarations }
  end;

var
  FormCancelarNotificacionMasivaInfractores: TFormCancelarNotificacionMasivaInfractores;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: Inicializar
Author :
Date Created : 15/07/2005
Description : inicializacion de este formulario
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TFormCancelarNotificacionMasivaInfractores.Inicializar: Boolean;
resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR      = 'Error';
begin
    CenterForm(Self);

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    btnProcesar.Enabled := False;
    FEnProceso := False;
    FCodigoOperacionInterfaseACancelar := -1;

	Result := False;
	try
        // Obtiene la lista de operaciones de interfaz no canceladas
        with spObtenerOperacionesInterfazInfractores, Parameters do begin
            Close;
            Parameters.Refresh;
			ParamByName('@OperacionesCanceladas').Value := 0;
			Open
		end;
		Result := True
	except
		on e: Exception do begin
			MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
		end;
	end;
end;


{******************************** Function Header ******************************
Function Name: btOperacionesInterfazSelect
Author :
Date Created : 15/07/2005
Description : muestro los procesos de impresion
Parameters : Sender: TObject; Tabla: TDataSet
Return Value : None
*******************************************************************************}
procedure TFormCancelarNotificacionMasivaInfractores.btOperacionesInterfazSelect(Sender: TObject; Tabla: TDataSet);
begin
	// Obtengo el C�digo de Operaci�n de Interfaz a cancelar
	with Tabla do begin
		FCodigoOperacionInterfaseACancelar := FieldByName('CodigoOperacionInterfase').AsInteger;
		edProceso.Text := Format('Cartas generadas el %s',
						  [FormatDateTime('dd/mm/yyyy hh:mm:ss', FieldByName('FechaGeneracion').AsDateTime)]);
	end;

	btnProcesar.Enabled := True;
end;

{******************************** Function Header ******************************
Function Name: btOperacionesInterfazProcess
Author :
Date Created : 15/07/2005
Description : selecciono un proceso de impresion
Parameters : Tabla: TDataSet; var Texto: String
Return Value : Boolean
*******************************************************************************}
function TFormCancelarNotificacionMasivaInfractores.btOperacionesInterfazProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Result := True;
	with Tabla do
		Texto := Format('Cartas generadas el %s -  Usuario: %s',
						[FormatDateTime('dd/mm/yyyy hh:mm:ss', FieldByName('FechaGeneracion').AsDateTime),
						FieldByName('Usuario').AsString]);
end;

{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author :
Date Created : 15/07/2005
Description : anulo el proceso de impresion
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormCancelarNotificacionMasivaInfractores.btnProcesarClick(Sender: TObject);
resourcestring
	MSG_PROCESS_OK          = 'El proceso finaliz� con �xito';
	MSG_PROCESS_INCOMPLETE  = 'El proceso no se pudo completar';
	MSG_OPERATION_LOG       = 'Anulaci�n Interfaz: %s';
	MSG_ERROR               = 'Error';
var
	FCodigoOperacionInterfaseNuevo: Integer;
	DescError: String;
begin
	btnProcesar.Enabled := False;
	FEnProceso := True;
	cursor := crHourGlass;

    try
        try
            //DMConnections.BaseCAC.BeginTrans;                                             //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('BEGIN TRAN frmCancelarOperaciones');     //SS_1385_NDR_20150922

            // Anulo la impresi�n de las cartas asociadas al C�digo de Operaci�n de Interfaz
            with spCancelarOperacionInterfazInfractores do begin
                Parameters.Refresh;
				Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfaseACancelar;
				ExecProc;
			end;

			// Registro la anulaci�n en Log de Operaciones de Interfaz
			if not RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_CANCELAR_INTERFAZ_SALIENTE_CARTAS_INFRACTORES, EmptyStr, UsuarioSistema, Format(MSG_OPERATION_LOG, [Trim(edProceso.Text)]), False, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacionInterfaseNuevo, DescError) then Raise(Exception.Create(DescError));

            //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('COMMIT TRAN frmCancelarOperaciones');					//SS_1385_NDR_20150922

			// Obtiene la lista de operaciones de interfaz no canceladas
			with spObtenerOperacionesInterfazInfractores do begin
                Close;
                Parameters.Refresh;
				Parameters.ParamByName('@OperacionesCanceladas').Value := 0;
                Open
            end;

            MsgBox(MSG_PROCESS_OK, Self.Caption, MB_OK + MB_ICONINFORMATION);

            edProceso.Clear;
        except
            on E: Exception do begin
                cursor := crDefault;
                //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION frmCancelarOperaciones END');	//SS_1385_NDR_20150922

    			MsgBoxErr(MSG_PROCESS_INCOMPLETE, e.Message, MSG_ERROR, MB_ICONERROR);
                btnProcesar.Enabled := True;
			end;
        end;
    finally
        cursor := crDefault;
        FEnProceso := False;
    end;
end;


{******************************** Function Header ******************************
Function Name: btnSalirClick
Author :
Date Created : 15/07/2005
Description : permito salir del formulario
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormCancelarNotificacionMasivaInfractores.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author :
Date Created : 15/07/2005
Description : Impido salir si esta procesando
Parameters : Sender: TObject;var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TFormCancelarNotificacionMasivaInfractores.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
	CanClose := not FEnProceso;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author :
Date Created : 15/07/2005
Description : lo libero de memoria
Parameters : Sender: TObject;var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TFormCancelarNotificacionMasivaInfractores.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    spObtenerOperacionesInterfazInfractores.Close;
    Action := caFree;
end;

end.
