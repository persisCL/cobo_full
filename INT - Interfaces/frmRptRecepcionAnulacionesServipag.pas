{-------------------------------------------------------------------------------
 File Name: frmRptRecepcionAnulacionesServipag.pas
 Author: nefernandez
 Date Created: 13/07/2007
 Language: ES-AR
 Description: Gestion de Interfases - Reporte de finalizacion del proceso
              Informa la cantidad de anulaciones recibidos, el importe total y
              otra informacion util para verificar si el proceso se realizo
              segun parametros normales.



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
unit frmRptRecepcionAnulacionesServipag;

interface

uses
  DMConnection,
  Util,
  UtilProc,
  RBSetup,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppProd, ppClass, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe,
  DB, ADODB, ppCtrls, ppBands, ppPrnabl, ppCache, ppStrtch, ppSubRpt, ppMemo, ConstParametrosGenerales, //SS_1147_NDR_20140710
  ppParameter;

type
  TRptRecepcionAnulacionesServipagForm = class(TForm)
    spObtenerReporteRecepcionAnulacionesServipag: TADOStoredProc;
    dsObtenerReporteRecepcionAnulacionesServipag: TDataSource;
    dbplspObtenerReporteRecepcionAnulacionesServipag: TppDBPipeline;
    ppReporte: TppReport;
    RBIListado: TRBInterface;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppLTitulo: TppLabel;
    ppImage1: TppImage;
    ppLine1: TppLine;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    pptFechaProcesamiento: TppLabel;
    pptNumeroProceso: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppFechaProcesamiento: TppLabel;
    ppNumeroProceso: TppLabel;
    ppModulo: TppLabel;
    ppSubReportResumen: TppSubReport;
    ChilReportResumen: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    lblResumen: TppLabel;
    ppLine8: TppLine;
    lblResumenCodigoServicio: TppLabel;
    lblResumenCantidadAceptados: TppLabel;
    lblResumenMontoAceptados: TppLabel;
    lblCantidadRechazados: TppLabel;
    lblResumenMontoRechazados: TppLabel;
    lblResumenCantidadTotal: TppLabel;
    lblMontoTotal: TppLabel;
    ppLine9: TppLine;
    dtxtResumenCodigoServicio: TppDBText;
    dtxtResumenCantidadAceptados: TppDBText;
    dtxtMontoAceptadosAMostrar: TppDBText;
    dtxtResumenCantidadRechazados: TppDBText;
    dtxtMontoRechazadosAMostrar: TppDBText;
    dtxtResumenCantidadTotal: TppDBText;
    dtxtMontoTotalAMostrar: TppDBText;
    ppLabel2: TppLabel;
    dbcTotalCantidadAceptados: TppDBCalc;
    lblTotalMontoAceptados: TppLabel;
    dbcTotalCantidadRechazados: TppDBCalc;
    lblTotalMontoRechazados: TppLabel;
    dbcTotalCantidadTotal: TppDBCalc;
    lblTotalMontoTotal: TppLabel;
    lblTotalResumen: TppLine;
    ppLine11: TppLine;
    ppLine2: TppLine;
    dbplspObtenerErroresInterfase: TppDBPipeline;
    dsObtenerErroresInterfase: TDataSource;
    spObtenerErroresInterfase: TADOStoredProc;
    ppSubReportErrores: TppSubReport;
    ChildReporteErrores: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailBand3: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    ldlDetalleErrores: TppLabel;
    ppLine3: TppLine;
    ppDBMemo1: TppDBMemo;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FNombreReporte: String;
    FCodigoOperacionInterfase: Integer;
  public
    { Public declarations }
    function Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase: Integer): Boolean;
  end;

var
  RptRecepcionAnulacionesServipagForm: TRptRecepcionAnulacionesServipagForm;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
Function Name: Inicializar
Author: nefernandez
Date Created: 13/07/2007
Description: Inicializo este formulario
Parameters: NombreReporte, CodigoOperacionInterfase
Return Value: Boolean
-----------------------------------------------------------------------------}
function TRptRecepcionAnulacionesServipagForm.Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase: Integer): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    FNombreReporte              := NombreReporte;
    FCodigoOperacionInterfase   := CodigoOperacionInterfase;
    RBIListado.Execute;
    Result := True;
end;

{-----------------------------------------------------------------------------
Function Name: RBIListadoExecute
Author: nefernandez
Date Created: 13/07/2007
Description: Ejecuto el informe
Parameters: Sender, Cancelled
Return Value: None
-----------------------------------------------------------------------------}
procedure TRptRecepcionAnulacionesServipagForm.RBIListadoExecute(
  Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_ERROR_TO_GENERATE_REPORT = 'Error al generar el reporte.';
    MSG_ERROR = 'Error';
var
    Config: TRBConfig;
begin
    // Configuración del reporte
    Config := rbiListado.GetConfig;
    if Config.BinName <> '' then begin
        rbiListado.Report.PrinterSetup.BinName := Config.BinName;
    end;

    try
        spObtenerReporteRecepcionAnulacionesServipag.Close;
        spObtenerReporteRecepcionAnulacionesServipag.Parameters.Refresh;
        spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@FechaProcesamiento').Value := NULL;
        spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@NombreArchivo').Value := NULL;
        spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@Modulo').Value := NULL;
        spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@Usuario').Value := NULL;
        spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@MontoTotalAceptadosAMostrar').Value := Null;
        spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@MontoTotalRechazadosAMostrar').Value := Null;
        spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@MontoTotalAMostrar').Value := Null;

        spObtenerReporteRecepcionAnulacionesServipag.CommandTimeOut := 500;
        spObtenerReporteRecepcionAnulacionesServipag.Open;

        // Asigno los valores a los campos del reporte
        ppmodulo.Caption                := spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@Modulo').Value;
        ppFechaProcesamiento.Caption    := DateToStr(spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@FechaProcesamiento').Value);
        ppNombreArchivo.Caption         := Copy(spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@NombreArchivo').Value, 1, 40);
        ppUsuario.Caption               := spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@Usuario').Value;
        ppNumeroProceso.Caption         := IntToStr(FCodigoOperacionInterfase);

        lblTotalMontoAceptados.Caption  := spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@MontoTotalAceptadosAMostrar').Value;
        lblTotalMontoRechazados.Caption := spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@MontoTotalRechazadosAMostrar').Value;
        lblTotalMontoTotal.Caption      := spObtenerReporteRecepcionAnulacionesServipag.Parameters.ParamByName('@MontoTotalAMostrar').Value;

        spObtenerErroresInterfase.Close;
        spObtenerErroresInterfase.Parameters.Refresh;
        spObtenerErroresInterfase.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerErroresInterfase.CommandTimeOut := 500;
        spObtenerErroresInterfase.Open;

        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FNombreReporte;

    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_TO_GENERATE_REPORT, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

end.
