{-----------------------------------------------------------------------------
 File Name: MOPRespuestasDenunciosBind.pas
 Author:    dcalani
 Date Created: 15/06/2005
 Language: ES-AR
 Description:
-----------------------------------------------------------------------------}
unit MOPRespuestasDenunciosBind;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRespuestasDenunciosType = interface;
  IXMLArchivoType = interface;
  IXMLDenuncioType = interface;
  IXMLDenuncioTypeList = interface;

{ IXMLRespuestasDenunciosType }

  IXMLRespuestasDenunciosType = interface(IXMLNode)
    ['{406A1405-8FE1-4FB4-BE10-1C9A9810B40D}']
    { Property Accessors }
    function Get_Archivo: IXMLArchivoType;
    { Methods & Properties }
    property Archivo: IXMLArchivoType read Get_Archivo;
  end;

{ IXMLArchivoType }

  IXMLArchivoType = interface(IXMLNode)
    ['{982890A2-69D7-4BBE-8851-B1C97CDE7C89}']
    { Property Accessors }
    function Get_CodigoEstado: Integer;
    function Get_Nombre: WideString;
    function Get_Concesionaria: Integer;
    function Get_Observaciones: WideString;
    function Get_FechaProceso: WideString;
    function Get_Denuncio: IXMLDenuncioTypeList;
    procedure Set_CodigoEstado(Value: Integer);
    procedure Set_Nombre(Value: WideString);
    procedure Set_Concesionaria(Value: Integer);
    procedure Set_Observaciones(Value: WideString);
    procedure Set_FechaProceso(Value: WideString);
    { Methods & Properties }
    property CodigoEstado: Integer read Get_CodigoEstado write Set_CodigoEstado;
    property Nombre: WideString read Get_Nombre write Set_Nombre;
    property Concesionaria: Integer read Get_Concesionaria write Set_Concesionaria;
    property Observaciones: WideString read Get_Observaciones write Set_Observaciones;
    property FechaProceso: WideString read Get_FechaProceso write Set_FechaProceso;
    property Denuncio: IXMLDenuncioTypeList read Get_Denuncio;
  end;

{ IXMLDenuncioType }

  IXMLDenuncioType = interface(IXMLNode)
    ['{06F28096-6CCA-4F40-BB93-0C7705487064}']
    { Property Accessors }
    function Get_NumInfraccion: Integer;
    function Get_CodigoEstado: Integer;
    function Get_Observaciones: WideString;
    procedure Set_NumInfraccion(Value: Integer);
    procedure Set_CodigoEstado(Value: Integer);
    procedure Set_Observaciones(Value: WideString);
    { Methods & Properties }
    property NumInfraccion: Integer read Get_NumInfraccion write Set_NumInfraccion;
    property CodigoEstado: Integer read Get_CodigoEstado write Set_CodigoEstado;
    property Observaciones: WideString read Get_Observaciones write Set_Observaciones;
  end;

{ IXMLDenuncioTypeList }

  IXMLDenuncioTypeList = interface(IXMLNodeCollection)
    ['{E7CD7B8C-4F66-4B12-BA98-0CB4D2335F64}']
    { Methods & Properties }
    function Add: IXMLDenuncioType;
    function Insert(const Index: Integer): IXMLDenuncioType;
    function Get_Item(Index: Integer): IXMLDenuncioType;
    property Items[Index: Integer]: IXMLDenuncioType read Get_Item; default;
  end;

{ Forward Decls }

  TXMLSalidaDenunciosDTDType = class;
  TXMLArchivoType = class;
  TXMLDenuncioType = class;
  TXMLDenuncioTypeList = class;

{ TXMLSalidaDenunciosDTDType }

  TXMLSalidaDenunciosDTDType = class(TXMLNode, IXMLRespuestasDenunciosType)
  protected
    { IXMLRespuestasDenunciosType }
    function Get_Archivo: IXMLArchivoType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLArchivoType }

  TXMLArchivoType = class(TXMLNode, IXMLArchivoType)
  private
    FDenuncio: IXMLDenuncioTypeList;
  protected
    { IXMLArchivoType }
    function Get_CodigoEstado: Integer;
    function Get_Nombre: WideString;
    function Get_Concesionaria: Integer;
    function Get_Observaciones: WideString;
    function Get_FechaProceso: WideString;
    function Get_Denuncio: IXMLDenuncioTypeList;
    procedure Set_CodigoEstado(Value: Integer);
    procedure Set_Nombre(Value: WideString);
    procedure Set_Concesionaria(Value: Integer);
    procedure Set_Observaciones(Value: WideString);
    procedure Set_FechaProceso(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDenuncioType }

  TXMLDenuncioType = class(TXMLNode, IXMLDenuncioType)
  protected
    { IXMLDenuncioType }
    function Get_NumInfraccion: Integer;
    function Get_CodigoEstado: Integer;
    function Get_Observaciones: WideString;
    procedure Set_NumInfraccion(Value: Integer);
    procedure Set_CodigoEstado(Value: Integer);
    procedure Set_Observaciones(Value: WideString);
  end;

{ TXMLDenuncioTypeList }

  TXMLDenuncioTypeList = class(TXMLNodeCollection, IXMLDenuncioTypeList)
  protected
    { IXMLDenuncioTypeList }
    function Add: IXMLDenuncioType;
    function Insert(const Index: Integer): IXMLDenuncioType;
    function Get_Item(Index: Integer): IXMLDenuncioType;
  end;

{ Global Functions }

function GetRespuestasDenuncios(Doc: IXMLDocument): IXMLRespuestasDenunciosType;
function LoadRespuestasDenuncios(const FileName: WideString): IXMLRespuestasDenunciosType;
function NewRespuestasDenuncios: IXMLRespuestasDenunciosType;

implementation

{ Global Functions }

function GetRespuestasDenuncios(Doc: IXMLDocument): IXMLRespuestasDenunciosType;
begin
//  Result := Doc.GetDocBinding('RespuestasDenuncios', TXMLSalidaDenunciosDTDType) as IXMLRespuestasDenunciosType;
      Result := Doc.GetDocBinding('SalidaDenuniciosDTD', TXMLSalidaDenunciosDTDType) as IXMLRespuestasDenunciosType;
end;

function LoadRespuestasDenuncios(const FileName: WideString): IXMLRespuestasDenunciosType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('SalidaDenuniciosDTD', TXMLSalidaDenunciosDTDType) as IXMLRespuestasDenunciosType;
end;

function NewRespuestasDenuncios: IXMLRespuestasDenunciosType;
begin
  Result := NewXMLDocument.GetDocBinding('SalidaDenuniciosDTD', TXMLSalidaDenunciosDTDType) as IXMLRespuestasDenunciosType;
end;

{ TXMLSalidaDenunciosDTDType }

procedure TXMLSalidaDenunciosDTDType.AfterConstruction;
begin
  RegisterChildNode('Archivo', TXMLArchivoType);
  inherited;
end;

function TXMLSalidaDenunciosDTDType.Get_Archivo: IXMLArchivoType;
begin
  Result := ChildNodes['Archivo'] as IXMLArchivoType;
end;

{ TXMLArchivoType }

procedure TXMLArchivoType.AfterConstruction;
begin
  RegisterChildNode('Denuncio', TXMLDenuncioType);
  FDenuncio := CreateCollection(TXMLDenuncioTypeList, IXMLDenuncioType, 'Denuncio') as IXMLDenuncioTypeList;
  inherited;
end;

function TXMLArchivoType.Get_CodigoEstado: Integer;
begin
  Result := ChildNodes['CodigoEstado'].NodeValue;
end;

procedure TXMLArchivoType.Set_CodigoEstado(Value: Integer);
begin
  ChildNodes['CodigoEstado'].NodeValue := Value;
end;

function TXMLArchivoType.Get_Nombre: WideString;
begin
  Result := ChildNodes['Nombre'].Text;
end;

procedure TXMLArchivoType.Set_Nombre(Value: WideString);
begin
  ChildNodes['Nombre'].NodeValue := Value;
end;

function TXMLArchivoType.Get_Concesionaria: Integer;
begin
  Result := ChildNodes['Concesionaria'].NodeValue;
end;

procedure TXMLArchivoType.Set_Concesionaria(Value: Integer);
begin
  ChildNodes['Concesionaria'].NodeValue := Value;
end;

function TXMLArchivoType.Get_Observaciones: WideString;
begin
  Result := ChildNodes['Observaciones'].Text;
end;

procedure TXMLArchivoType.Set_Observaciones(Value: WideString);
begin
  ChildNodes['Observaciones'].NodeValue := Value;
end;

function TXMLArchivoType.Get_FechaProceso: WideString;
begin
  Result := ChildNodes['FechaProceso'].Text;
end;

procedure TXMLArchivoType.Set_FechaProceso(Value: WideString);
begin
  ChildNodes['FechaProceso'].NodeValue := Value;
end;

function TXMLArchivoType.Get_Denuncio: IXMLDenuncioTypeList;
begin
  Result := FDenuncio;
end;

{ TXMLDenuncioType }

function TXMLDenuncioType.Get_NumInfraccion: Integer;
begin
  Result := ChildNodes['NumInfraccion'].NodeValue;
end;

procedure TXMLDenuncioType.Set_NumInfraccion(Value: Integer);
begin
  ChildNodes['NumInfraccion'].NodeValue := Value;
end;

function TXMLDenuncioType.Get_CodigoEstado: Integer;
begin
  Result := ChildNodes['CodigoEstado'].NodeValue;
end;

procedure TXMLDenuncioType.Set_CodigoEstado(Value: Integer);
begin
  ChildNodes['CodigoEstado'].NodeValue := Value;
end;

function TXMLDenuncioType.Get_Observaciones: WideString;
begin
  Result := ChildNodes['Observaciones'].Text;
end;

procedure TXMLDenuncioType.Set_Observaciones(Value: WideString);
begin
  ChildNodes['Observaciones'].NodeValue := Value;
end;

{ TXMLDenuncioTypeList }

function TXMLDenuncioTypeList.Add: IXMLDenuncioType;
begin
  Result := AddItem(-1) as IXMLDenuncioType;
end;

function TXMLDenuncioTypeList.Insert(const Index: Integer): IXMLDenuncioType;
begin
  Result := AddItem(Index) as IXMLDenuncioType;
end;

function TXMLDenuncioTypeList.Get_Item(Index: Integer): IXMLDenuncioType;
begin
  Result := List[Index] as IXMLDenuncioType;
end;

end.
