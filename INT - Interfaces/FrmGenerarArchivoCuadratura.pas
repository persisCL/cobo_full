{-------------------------------------------------------------------------------
 File Name: FrmGenerarArchivoCudratura.pas
 Author:    lgisuk
 Date Created: 04/10/2005
 Language: ES-AR
 Description: M�dulo de la interface Falabella - Generar Archivo de Cuadratura

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
-------------------------------------------------------------------------------}
unit FrmGenerarArchivoCuadratura;

interface

uses
  //Envio de Archivo de Cuadratura
  DMConnection,              //Coneccion a base de datos OP_CAC
  UtilProc,                  //Mensajes
  Util,                      //Stringtofile,padl..
  UtilDB,                    //Rutinas para base de datos
  ComunesInterfaces,         //Procedimientos y Funciones Comunes a todos los formularios
  ConstParametrosGenerales,  //Obtengo Valores de la Tabla Parametros Generales
  Peatypes,                  //Constantes
  PeaProcs,                  //NowBase
  FrmRptEnvioCuadratura,     //Reporte del Proceso
  //General
  DB, ADODB, ComCtrls, StdCtrls, Controls,ExtCtrls, Classes, Windows, Messages, SysUtils,
  Variants,  Graphics,  Forms, Dialogs, Grids, DBGrids,  DPSControls, ListBoxEx,
  DBListEx, StrUtils, UtilRB, ppProd, ppClass, ppReport, ppComm, ppRelatv,
  ppDB, ppTxPipe, ppPrnabl, ppStrtch, ppSubRpt, ppCache, ppBands, ppCtrls,
  Validate, DateEdit;

type
  TFGenerarArchivoCuadratura = class(TForm)
    SPObtenerMandatosVigentesCMR: TADOStoredProc;
    pnlAvance: TPanel;
    pbProgreso: TProgressBar;
    Bevel1: TBevel;
    btnProcesar: TButton;
    SalirBTN: TButton;
    Lprocesogeneral: TLabel;
    lblMensaje: TLabel;
    Ltitulo: TMemo;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    lbl_Archivo: TLabel;
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SalirBTNClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoOperacion : Integer;
    FCancelar : Boolean;
    FErrorGrave : Boolean;
    FNombreArchivoCuadratura : AnsiString;
    FNumeroSecuencia : Integer;
    FSumatoriaDeCuentas : Real;
    FCMR_CodigodeAutopista : AnsiString;
    FCMR_Directorio_Destino_Cuadratura : AnsiString;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    Function CrearNombreArchivoCuadratura(var NombreArchivo : AnsiString; Fecha : TDateTime) : Boolean;
    Function RegistrarOperacion : Boolean;
    Function ObtenerMandatos : Boolean;
    function GuardarArchivo : Boolean;
    Function GenerarReporteFinalizacionProceso(CodigoOperacionInterfase : Integer; CantidadRegistros, SumatoriaCuentas : AnsiString) : Boolean;
  public
    Function Inicializar : Boolean;
    { Public declarations }
  end;

var
  FGenerarArchivoCuadratura : TFGenerarArchivoCuadratura;
  FLista : TStringList;

const
  RO_MOD_INTERFAZ_SALIENTE_CUADRATURA_CMR = 65;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    Lgisuk
  Date Created: 04/10/2005
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoCuadratura.Inicializar : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 04/10/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        CMR_CODIGODEAUTOPISTA             = 'CMR_CodigodeAutopista';
        CMR_DIRECTORIO_DESTINO_CUADRATURA = 'CMR_Directorio_Destino_Cuadratura';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_CODIGODEAUTOPISTA , FCMR_CodigodeAutopista) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_CODIGODEAUTOPISTA;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FCMR_CodigodeAutopista <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + CMR_CODIGODEAUTOPISTA;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_DIRECTORIO_DESTINO_CUADRATURA, FCMR_Directorio_Destino_Cuadratura) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_DIRECTORIO_DESTINO_CUADRATURA;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FCMR_Directorio_Destino_Cuadratura := GoodDir(FCMR_Directorio_Destino_Cuadratura);
                if  not DirectoryExists(FCMR_Directorio_Destino_Cuadratura) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FCMR_Directorio_Destino_Cuadratura;
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;


resourcestring
	  MSG_INIT_ERROR = 'Error al Inicializar';
	  MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Centro el formulario
  	CenterForm(Self);
  	try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and
                        VerificarParametrosGenerales and
                                CrearNombreArchivoCuadratura(FNombreArchivoCuadratura, Now);
    except
        on e : Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
    // Todo Ok, terminamos de inicializar
    lTitulo.text := FNombreArchivoCuadratura;      //indico que archivo voy a generar
    lblMensaje.Caption := '';                     //no muestro ningun mensaje
    PnlAvance.visible := False;                   //Oculto el Panel de Avance
    FCancelar := False;                           //inicio cancelar en false
    FCodigoOperacion := 0;                        //Inicializo el codigo de operacion
    FErrorGrave := False;                         //Inicio sin errores
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 03/06/2005
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoCuadratura.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_CUADRATURA       = ' ' + CRLF +
                          'El Archivo de Cuadratura es' + CRLF +
                          'utilizado por el ESTABLECIMIENTO para informar a FALABELLA' + CRLF +
                          'todos los mandantes con medio de Pago CMR - FALABELLA Vigentes' + CRLF +
                          'en la base de datos del ESTABLECIMIENTO' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: 04_CCCUADRATURA_DDMMAA.xx' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.Visible = True then Exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_CUADRATURA, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: permito hacer click en la ayuda solo si no esta procesando
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoCuadratura.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.Visible = True then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: CrearNombreArchivoCuadratura
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: Crea el nombre del archivo de Cuadratura
  Parameters: var NombreArchivo: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoCuadratura.CrearNombreArchivoCuadratura(var NombreArchivo: AnsiString; Fecha: TDateTime): Boolean;
resourcestring
    MSG_FILE_NAME_ERROR = 'Error al crear el nombre del archivo';
const
    FILE_NAME = 'CCCUADRATURA';
var
    Year, Month, Day : Word;
    Mes,Dia : AnsiString;
    NumeroSecuencia : AnsiString;
begin
    try
        //Obtengo a�o, mes y dia actual
        DecodeDate(Fecha, Year, Month, Day);
        if Length(IntToStr(Month)) = 1 then Mes := '0'+ IntToStr(Month) else Mes := IntToStr(Month);
        if Length(IntToStr(Day)) = 1 then Dia := '0'+ IntToStr(Day) else Dia := IntToStr(Day);
        //Asigo Numero de Secuencia es fijo
        NumeroSecuencia := '01';
        //Genero el Nombre del Archivo de Cuadratura
        FCMR_CodigodeAutopista := IStr0(StrtoInt(FCMR_CodigodeAutopista),2);
        NombreArchivo := FCMR_CodigodeAutopista + '_' + FILE_NAME + '_';
        NombreArchivo := NombreArchivo + Dia + Mes + Copy(IntToStr(Year),3,2);
        NombreArchivo := FCMR_Directorio_Destino_Cuadratura + Trim(NombreArchivo) + '.' + NumeroSecuencia;
        //guardo el numero de secuencia
        FNumeroSecuencia := StrToInt(NumeroSecuencia);
        Result := True;
    except
        raise Exception.Create(MSG_FILE_NAME_ERROR);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: Registro la operacion en el log
  Parameters: var DescError:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFGenerarArchivoCuadratura.RegistrarOperacion : Boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
const
    STR_OBSERVACION = 'Generar Archivo Cuadratura';
var
    DescError : String;
begin
    //Registro la operacion en el Log
    Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_INTERFAZ_SALIENTE_CUADRATURA_CMR, Trim(ExtractFileName(FNombreArchivoCuadratura)), UsuarioSistema, STR_OBSERVACION, False, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);
    if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerMandatos
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: Abro la Consulta
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFGenerarArchivoCuadratura.ObtenerMandatos : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: QueryToTxt
      Author:    lgisuk
      Date Created: 04/10/2005
      Description: convierte el bloque recibido a txt
      Parameters: Query:TCustomAdoDataSet
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    function QueryToTxt(Query : TCustomAdoDataSet) : Boolean;

        {-----------------------------------------------------------------------------
          Function Name: ReemplazarKporNueve
          Author:    lgisuk
          Date Created: 22/06/2005
          Description: Reemplaza la K que viene en algunos numeros de convenio
                       por un Nueve, para conseguir que cada numero de convenio
                       sea un valor numerico y se pueda sumar
          Parameters: valor:string
          Return Value: string
        -----------------------------------------------------------------------------}
        Function ReemplazarKporNueve(Valor : String) : String;
        begin
            Result := AnsiReplaceStr(Uppercase(Valor), 'K', '9');
        end;

    resourcestring
        MSG_PROCESS_ERROR = 'Error procesando los datos de los clientes vigentes';
        MSG_ERROR = 'Error';
    var
        I : Integer;
        Linea : AnsiString;
        CodigoConvenio : Integer;
        DescError : AnsiString;
    begin

        pbProgreso.Position := 0;
        pbprogreso.Max := Query.RecordCount;

        Result := False;
        try
            with Query do begin
                First;
                while not Eof and (not FCancelar) do begin
                    i := 1;
                    Linea := '';
                    //mientras halla registros y no sea cancelado
                    while (i <= Query.FieldCount) and (not FCancelar) do begin
                        //genero las lineas del archivo
                        Linea := Linea + Fields.FieldByNumber(i).AsString;
                        Inc(i);
                    end;
                    //inserto la linea en el txt
                    FLista.Add(linea);
                    //sumatoria de los numeros de convenio
                    FSumatoriadeCuentas := FSumatoriadeCuentas + StrToFloat(ReemplazarKporNueve(Fields.FieldByNumber(3).AsString));

                    //Registro el mandato recibido
                    CodigoConvenio := QueryGetValueINT(DMConnections.BaseCAC,'SELECT dbo.ObtenerCodigoConvenio('''+'00100'+Fields.FieldByNumber(3).asstring+''')');
                    AgregarDetalleMandatoEnviado(DMconnections.BaseCAC,FcodigoOperacion,CodigoConvenio,copy(Fields.FieldByNumber(1).asstring,1,1),DescError);

                    //Actualizo la barra de progreso
                    pbprogreso.StepIt;
                    Application.ProcessMessages;

                    Next;
                end;

                //Refresco la pantalla
                pbprogreso.Position := pbprogreso.Max;
                Application.ProcessMessages;
                
            end;

            Result := (not FCancelar) and (FLista.Count > 0);
        except
            on e: Exception do begin
                FErrorGrave := True;
                MsgBoxErr(MSG_PROCESS_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                Exit;
            end;
        end;
    end;

Resourcestring
    MSG_COULT_NOT_INSERT_LOG = 'No se puedo registrar la operacion en el log';
    MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS = 'No se pudieron obtener los mandatos pendientes';
    MSG_EMPTY_QUERY_ERROR = 'No hay Mandatos vigentes para informar!';
    MSG_ERROR = 'Error';
var
    FErrorMsg : String;
    Year, Month, Day : Word;
    Mes,Dia : AnsiString;
    CantidadTotal : Integer;
begin
    Result := False;

    //Inicializo variables
    FLista.Clear;
    FerrorMsg := '';
    CantidadTotal := 0;
    FSumatoriaDeCuentas := 0;

    With SPObtenerMandatosVigentesCMR do begin
        try
            Close;
            CommandTimeout := 500;
            Parameters.Refresh;
            Open;
        except
            on e: Exception do begin
                FErrorMsg := MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS;
                FErrorGrave := True;
                MsgBoxErr(FErrorMsg, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
        //Acumulo la cantidad total de registros
        CantidadTotal := CantidadTotal + RecordCount;
        //Creo el bloque en el archivo
        QueryToTxt(SPObtenerMandatosVigentesCMR);
    end;


    //Genero en Encabezado del archivo y lo inserto al principio del archivo
    DecodeDate(Now, Year, Month, Day);
    if Length(IntToStr(Month)) = 1 then Mes := '0'+ IntToStr(Month) else Mes := IntToStr(Month);
    if Length(IntToStr(Day)) = 1 then Dia := '0'+ IntToStr(Day) else Dia := IntToStr(Day);
    FLista.Insert(0, Dia + Mes + IntToStr(Year) + PADL(IntToStr(FLista.count),12,'0') + Space(2) + PADL(FloatToStr(FSumatoriadeCuentas),18,'0') + PADL('',109,' '));


    //Verifico que halla mandatos CMR-Falbella vigentes para informar
    if CantidadTotal = 0 then begin
        pbProgreso.Position := 0;
        lblmensaje.Caption := '';
        FErrorGrave := True;
        Msgbox(MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS + CRLF + MSG_EMPTY_QUERY_ERROR, Self.caption , 0);
        exit;
    end;

    Result := True;
    
end;

{-----------------------------------------------------------------------------
  Function Name: GuardarArchivo
  Author:    lgisuk
  Date Created: 16/12/2004
  Description: Guardar Archivo
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TFGenerarArchivoCuadratura.GuardarArchivo : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: GuardarArchivoCuadratura
      Author:    lgisuk
      Date Created: 04/10/2005
      Description:  Guarda el archivo de Cuadratura
      Parameters: var DescError:AnsiString
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function GuardarArchivoCuadratura (var DescError : AnsiString) : Boolean;
    Resourcestring
        MSG_ERROR  = 'No se pudo guardar archivo de cuadratura';
        MSG_CANCEL = 'Se ha cancelado el proceso debido a la existencia de un ' + CRLF +
                     'archivo id�ntico en el directorio de destino';
    Const
        FILE_TITLE   = ' El archivo:  ';
        FILE_EXISTS  = ' ya existe en el directorio de destino. �Desea reemplazarlo?';
        FILE_WARNING = ' Atenci�n';
    begin
        //Verificamos que no exista uno con el mismo nombre
        if FileExists(FNombreArchivoCuadratura) then begin
            if (MsgBox( FILE_TITLE + FNombreArchivoCuadratura + FILE_EXISTS,FILE_WARNING , MB_YESNO + MB_ICONQUESTION) = IDNO) then begin
                DescError := MSG_CANCEL; //informo que fue cancelado
                Result := False; //fallo
                Exit; //y salgo
            end;
        end;
        //creo el archivo
        try
            Result := StringToFile(FLista.text, FNombreArchivoCuadratura);
            if not Result then DescError := MSG_ERROR;
        except
            raise Exception.Create(MSG_ERROR);
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: GuardarNumeroSecuencia
      Author:    lgisuk
      Date Created: 04/10/2005
      Description:  Guarda el numero de secuencia
      Parameters: CodigoOperacionInterfase:integer;NumeroSecuencia:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function GuardarNumeroSecuencia(CodigoOperacionInterfase:integer;NumeroSecuencia:integer):boolean;
    resourcestring
        MSG_ERROR = 'Error al guardar el numero de secuencia: ';
    var
        DescError : AnsiString;
    begin
        try
            //Actualizo el numero de secuencia
            ActualizarNumeroSecuencia(DMConnections.BaseCAC,
                                         CodigoOperacionInterfase,
                                         NumeroSecuencia,
                                         DescError
                                        );
            Result := True;
        except
            raise Exception.Create(MSG_ERROR + descerror );
        end;
    end;

Resourcestring
    MSG_SAVE_FILE_ERROR = 'No se pudo Guardar el Archivo: ';
    MSG_ERROR = 'Error';
var
    DescError : Ansistring;
begin
    Result := False;
    if not Assigned(FLista) or (FLista.Count = 0) then Exit;
    try
        Result :=  (//Grabo el archivo,
                    GuardarArchivoCuadratura(DescError) and
                    //Y Guardo el numero de secuencia
                    GuardarNumeroSecuencia(FCodigoOperacion,FnumeroSecuencia)
                   );
        if not Result then begin
            DeleteFile(FNombreArchivoCuadratura);
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR, DescError, MSG_ERROR, MB_ICONERROR);
        end;
    except
        on e : Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarInformedelProceso
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: Muestro un informe de finalizaci�n de proceso
  Parameters: CodigoOperacionInterfase:Integer; NombreReporte, CantidadRegistros, SumatoriaCuentas: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
Function TFGenerarArchivoCuadratura.GenerarReporteFinalizacionProceso(CodigoOperacionInterfase:Integer;CantidadRegistros, SumatoriaCuentas: AnsiString):boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de finalizaci�n de proceso';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Envio de Cuadratura';
var
    F : TFRptEnvioCuadratura;
begin
    Result := False;
    try
        //Muestro el reporte
        Application.CreateForm(TFRptEnvioCuadratura, F);
        if not F.Inicializar(CodigoOperacionInterfase, REPORT_TITLE, CantidadRegistros, SumatoriaCuentas) then f.Release;
        Result := True;
    except
       on e: Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: Genera el Archivo de Cuadratura
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoCuadratura.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 04/10/2005
      Description: Actualizo el log al finalizar
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ActualizarLog : Boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVACION = 'Finalizo OK!';
    var
         DescError : String;
    begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);
        except
            on e: Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

Resourcestring
    MSG_WARNING = 'Existen Mandatos Vigentes anteriores para la fecha que se ha especificado. Desea generar igual el archivo de "Cuadratura"?';
    MSG_PROCESS_CANCEL = 'Proceso cancelado';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
	  MSG_PROCESS_FINALLY_OK = 'El proceso finaliz� con �xito';
Const
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
    STR_OBTAINING_DATA = 'Obteniendo los Datos...';
    STR_SAVE_FILE  = 'Guardando el Archivo...';
    STR_CAPTION = 'Atenci�n';
begin
  	BtnProcesar.Enabled := False;
    PnlAvance.visible := True;
  	Screen.Cursor := crHourGlass;
    KeyPreview := True;

    try

        //Registro la operacion en el log
        Lblmensaje.Caption := STR_REGISTER_OPERATION;          //Informo Tarea
        Application.ProcessMessages;                           //Refresco la pantalla
        if not RegistrarOperacion then begin
             FErrorGrave := True;
             exit;
        end;

        //Obtengo los datos y genero el archivo
        Lblmensaje.Caption := STR_OBTAINING_DATA;               //Informo Tarea
        Application.ProcessMessages;                            //Refresco la pantalla
  	    if not ObtenerMandatos then begin
            FErrorGrave := True;
            Exit;
        end;

        //Guardo el archivo
        Lblmensaje.Caption := STR_SAVE_FILE;                    //Informo Tarea
        Application.ProcessMessages;                            //Refresco la pantalla
  	    if not GuardarArchivo then begin
            FErrorGrave := True;
            Exit;
        end;

        //Actualizo el log al Final
        ActualizarLog;

    finally
       	Screen.Cursor := crDefault;
        pbProgreso.Position := 0;
        PnlAvance.visible := False;
    		lblMensaje.Caption := '';
        if FCancelar then begin
            //Muestro mensaje que el proceso fue cancelado  (sin errores)
            MsgBox(MSG_PROCESS_CANCEL, Self.Caption, MB_OK + MB_ICONINFORMATION);
        end else if FErrorGrave then begin
            //Muestro mensaje que finalizo por un error
            MsgBox(MSG_PROCESS_FINALLY_WITH_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
        end else begin
            //Muestro mensaje que el proceso finalizo exitosamente
            MsgBox(MSG_PROCESS_FINALLY_OK, Self.Caption, MB_OK + MB_ICONINFORMATION);
            //Genero el reporte de finalizacion de proceso
            GenerarReporteFinalizacionProceso(FCodigoOperacion, IntToStr(FLista.Count-1), FloatToStr(FSumatoriaDeCuentas));
        end;
        KeyPreview := False;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: Permito Cancelar
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoCuadratura.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: Permite salir solo si no esta procesando.
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoCuadratura.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando.
    CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoCuadratura.SalirBTNClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 04/10/2005
  Description: Lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoCuadratura.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

initialization
    FLista   := TStringList.Create;
finalization
    FreeAndNil(Flista);
end.
