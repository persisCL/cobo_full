﻿{-----------------------------------------------------------------------------
  File Name         : FrmGenerarArchivosTransaccionesRechazadas.pas
  Fecha Creacion    : 30/03/2012
  Firma             : SS-1015-CQU-20120330
  Descripcion       : Genera los archivos de salida de otras concesionarias para informar
                    acerca de tránsitos, movimientos y estacionamientos.

  Fecha             : 20/04/2012
  Firma             : SS-1015-CQU-20120420
  Description       : Se realizan modificaciones de acuerdo a lo indicado por QA

  Fecha             : 26/04/2012
  Firma             : SS-1015-CQU-20120426
  Description       : Se realizan modificaciones de acuerdo a lo indicado por TL

  Fecha             : 11/05/2012
  Firma             : SS-1015-CQU-20120510
  Description       : Se cambia el nombre del parámetro que define el nombre de la carpeta de rechazos,
                      se agregan los parametros para el prefijo en el nombre de archivo.

  Fecha             : 17/05/2012
  Firma             : SS_1015_CQU_20120517
  Descripction      : Se agrega la interface Cuotas y Saldo Inicial, adicionalmente se realiza una modificación
                      de la pantalla, ahora primero se obtienen los datos y luego, si el usuario lo desea,
                      se genera el archivo.
                      También se realizan mejoras a los métodos para minimizar las consultas a BD.
                      NOTA: se agrega un DBGrid el cual se sobre pone a un panel de progreso,
                      el despliegue de ambos debe ser alternado, mientras se muestra el DBGrid
                      NO SE DEBE desplegar el Panel.

    Fecha           : 28/05/2012
    Firma           : SS_1015_CQU_20120528
    Description     : Se modifica el objeto grilla y se coloca un TDBListEx en su lugar.

    Fecha           : 29/05/2012
    Firma           : SS_1015_CQU_20120530
    Descripcion     : Corrige errores detectados por QA, en el cambio de nombre del archivo

    Fecha           : 11/07/2012
    Firma           : SS_1015_CQU_20120711
    Descripcion     : A petición del cliente, se eliminan todos los sufijos creados para cada archivo rechazado
                      y se crea un único Sufijo para Todos reduciendo así la cantidad de parámetros generales.
                      No se eliminarán las variables sino que se modificará el nombre del parámetro, esto es debido
                      a que en un futuro se necesite nuevamente volver a definir un parámetro por tipo de archivo.

    Fecha           : 29/08/2012
    Firma           : SS_1006_1015_CQU_20120829
    Description     : Se modifica la obtención del código de del Modulo, ahora se usa una función y no es necesaria
                      la consulta SQL.

    Fecha           : 29/09/2012
    Firma           : SS_1006_MCO_20120926
    Descripcion     : Se modifica funcion GuardarHistorico para que almacene solo el nombre del archivo (sin ruta).
                      Ademas se modifican nombres de parametros @IDRechazo, @IdentificadorTransaccion

    Fecha           : 28/09/2012
    Firma           : SS_1006_1015_CQU_20120928
    Descripcion     : Se modifica el comportamiento cuando no encuentra registros, ahora se muestra sólo una advertencia
                      y no un mensaje de error.
                      Se cambia el mensaje que se despliega y se deja el que se utiliza en IVA, Folios y Pagos dejándolo estandar.
                      Se modifica la alineación de las columnas al desplegar los datos.

    Fecha           : 05/10/2012
    Firma           : SS_1006_1015_CQU_20121005
    Descripcion     : Se corrige el nombre de unos campos que fueron cambiados con anterioridad.

    Firma       : SS_1147_NDR_20141216
    Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

}
unit FrmGenerarArchivoTransaccionesRechazadas;

interface

uses
    StrUtils, Util, UtilProc, PeaProcs, DMConnection, ConstParametrosGenerales,
    DmiCtrls, ComunesInterfaces, UtilDB, PeaTypes,
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, ExtCtrls, StdCtrls, Validate, DateEdit, VariantComboBox, DB, ADODB,
    ComCtrls, Grids, DBGrids    //SS_1015_CQU_20120517
    , ListBoxEx, DBListEx  //;  //SS_1015_CQU_20120528 //SS_1015_CQU_20120530
    , ExtDlgs //;               //SS_1015_CQU_20120530 //SS_1006_1015_CQU_20120829
    , PeaProcsCN;               //SS_1006_1015_CQU_20120829

type
  TFGenerarArchivoTransaccionesRechazadas = class(TForm)
    Bevel1: TBevel;
    edFechaDesde: TDateEdit;
    edFechaHasta: TDateEdit;
    lblFechaDesde: TLabel;
    lblFechaHasta: TLabel;
    btnSalir: TButton;
    btnGenerar: TButton;          // SS_1015_CQU_20120517
    lblConcesionaria: TLabel;
    vcbConcesionarias: TVariantComboBox;
    lblNombreArchivo: TLabel;
    //dlgSelArchivo: TOpenDialog;      // SS_1015_CQU_20120530
    txtArchivo: TPickEdit;
    PnlAvance: TPanel;
    qryConsulta: TADOQuery;
    pbProgreso: TProgressBar;
    lblMensaje: TLabel;
    lblProgreso: TLabel;
    spObtenerEstacionamientosRechazados: TADOStoredProc;
    spAgregaHistoricoEstacionamientosRechazadosEnviados: TADOStoredProc;
    spObtenerTransaccionesMovimientosRechazados: TADOStoredProc;
    //dbgRechazados: TDBGrid;             // SS_1015_CQU_20120517 // SS_1015_CQU_20120528
    rdgTipoTransaccion: TRadioGroup;    // SS_1015_CQU_20120517
    btnObtener: TButton;                // SS_1015_CQU_20120517
    dsFuenteDatosGrilla: TDataSource;   // SS_1015_CQU_20120517
    spRechazosGenerico: TADOStoredProc; // SS_1015_CQU_20120517
    dblRechazados: TDBListEx;           // SS_1015_CQU_20120517
    dlgSelArchivo: TSaveDialog;         // SS_1015_CQU_20120530
    procedure btnSalirClick(Sender: TObject);
    procedure txtArchivoButtonClick(Sender: TObject);
    procedure btnGenerarClick(Sender: TObject);        // SS_1015_CQU_20120517
    procedure vcbConcesionariasChange(Sender: TObject);
    procedure edFechaDesdeChange(Sender: TObject);
    procedure edFechaHastaChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
//    procedure rdbMovimientoClick(Sender: TObject); // SS_1015_CQU_20120517
//    procedure rdbVariableClick(Sender: TObject);   // SS_1015_CQU_20120517
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnObtenerClick(Sender: TObject);       // SS_1015_CQU_20120517
    procedure rdgTipoTransaccionClick(Sender: TObject); // SS_1015_CQU_20120517
  private
    { Private declarations }
    RutaTransaccion: AnsiString;
    NombreArchivo: AnsiString;
    FCodigoConsecionaria: AnsiString;
    // Inicio Bloque SS_1015_CQU_20120510
    //FDirectorioRechazo : AnsiString;
    FDirectorioRechazoMov : AnsiString;
    FDirectorioRechazoEst : AnsiString;
    FDirectorioRechazoTra : AnsiString;
    FPrefRechArchivoMovimiento : AnsiString;
    FPrefRechArchivoTransito : AnsiString;
    FPrefRechArchivoEstacionamiento : AnsiString;
    // Fin Bloque SS_1015_CQU_20120510
    // Inicio Bloque SS_1015_CQU_20120517
    FDirectorioRechazoSald : AnsiString;
    FDirectorioRechazoCuot : AnsiString;
    FPrefRechArchivoSaldo : AnsiString;
    FPrefRechArchivoCuot  : AnsiString;
    // Fin Bloque SS_1015_CQU_20120517
    FCancelar : Boolean;
    FCodigoOperacion : Integer;
    FNombreArchivo : AnsiString;
    FFechaDesde : Variant;
    FFechaHasta : Variant;
    FNumeroSecuencia : Integer;
    TipoInterface : string;
    FErrorGrave : Boolean;
    //CodigoModulo : string; // Obtengo el Codigo del Modulo a Ejecutar //SS_1006_1015_CQU_20120829
    CodigoModulo : Integer;                                             //SS_1006_1015_CQU_20120829
    DirectorioSeleccionado : string;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    Function GenerarNombreArchivo(FechaInicio, FechaTermino : TDateEdit) : string;
    Function RegistrarOperacion : Boolean;
    Function ObtenerEstacionamientosRechazados: Boolean;
    Function GuardarArchivo : Boolean;
    Function ObtenerNumeroSecuencia(var NumeroSecuencia : AnsiString) : Boolean;
    Function ActualizarLog : Boolean;
    function GuardarHistorico : Boolean;
    function ObtenerRechazos: Boolean;
    Procedure CambiarTipoTransaccion();
    Procedure CambiarNombreArchivo();
    Function VerificarParametrosGenerales : boolean;
    function ObtenerRechazosGenerico(sTipoInterface : String) : Boolean; // SS_1015_CQU_20120517
    function DesplegarDatos(dsFuenteDatos : TDataSource): Boolean;       // SS_1015_CQU_20120528
  public
    { Public declarations }

    function Inicializar(txtCaption: ANSIString; MDIChild:Boolean; TipoInterfaceIndicada : string) : Boolean;

  end;

const
    // Inicio Bloque SS_1015_CQU_20120510
    //DIRECTORIO_DESTINO = 'CARPETA_SALIDA_TRASAC_RECHAZADAS';
    DIRECTORIO_ARCHIVOS_MOV_RECHAZADOS  = 'DIR_ARCHIVOS_MOV_CTAS_RECHAZADOS';
    DIRECTORIO_ARCHIVOS_TTOS_RECHAZADOS = 'DIR_ARCHIVOS_TTOS_RECHAZADOS';
    //PREFIJO_ARCHIVO_MOV_RECHAZADOS      = 'NOMBRE_ARCHIVO_MOV_CTAS_RECHAZADOS';   // SS_1015_CQU_20120711
    PREFIJO_ARCHIVO_MOV_RECHAZADOS      = 'NOMBRE_ARCHIVOS_RECHAZADOS';             // SS_1015_CQU_20120711
    //PREFIJO_ARCHIVO_TTOS_RECHAZADOS     = 'NOMBRE_ARCHIVO_TRX_TTOS_RECHAZADOS';   // SS_1015_CQU_20120711
    PREFIJO_ARCHIVO_TTOS_RECHAZADOS     = 'NOMBRE_ARCHIVOS_RECHAZADOS';             // SS_1015_CQU_20120711

    SEPARADOR = '_';
    TIPO_MLFF = '001';          //tipo MultiLine Fre Flow
    TIPO_PEAJ = '002';          //tipo Peaje Tradicional
    TIPO_ESTA = '003';          //tipo Estacionamiento
    TIPO_MOV  = '004';          //tipo Movimiento Cuenta
    TIPO_SALD = '005';          //tipo Saldo Inicial
    //TIPO_CRED = '006';          //tipo Créditos       // SS_1015_CQU_20120517
    TIPO_CUOT = '006';          //tipo Cuotas           // SS_1015_CQU_20120517
    // Fin Bloque SS_1015_CQU_20120510
    // Inicio Bloque SS_1015_CQU_20120517
    DIRECTORIO_ARCHIVOS_SALD_RECHAZADOS = 'DIR_ARCHIVOS_SALDO_INI_RECHAZADOS';
    DIRECTORIO_ARCHIVOS_CUOT_RECHAZADOS = 'DIR_ARCHIVOS_CUOTAS_OC_RECHAZADOS';
    DIRECTORIO_ARCHIVOS_ESTA_RECHAZADOS = 'DIR_ARCHIVOS_ESTAC_RECHAZADOS';
    //PREFIJO_ARCHIVO_SALD_RECHAZADOS     = 'NOMBRE_ARCHIVO_SALDO_INI_RECHAZADOS';  // SS_1015_CQU_20120711
    PREFIJO_ARCHIVO_SALD_RECHAZADOS     = 'NOMBRE_ARCHIVOS_RECHAZADOS';             // SS_1015_CQU_20120711
    //PREFIJO_ARCHIVO_CUOT_RECHAZADOS     = 'NOMBRE_ARCHIVO_CUOTAS_OC_RECHAZADAS';  // SS_1015_CQU_20120711
    PREFIJO_ARCHIVO_CUOT_RECHAZADOS     = 'NOMBRE_ARCHIVOS_RECHAZADOS';             // SS_1015_CQU_20120711
    //PREFIJO_ARCHIVO_ESTA_RECHAZADOS     = 'NOMBRE_ARCHIVO_ESTAC_RECHAZADOS';      // SS_1015_CQU_20120711
    PREFIJO_ARCHIVO_ESTA_RECHAZADOS     = 'NOMBRE_ARCHIVOS_RECHAZADOS';             // SS_1015_CQU_20120711
    CODIGO_PARQUEARAUCO                 = '8';
    MSG_ERROR_CONCESIONARIA_ESTACIONA   = 'La concesionaria seleccionada no puede procesar este tipo de transacción';
    // Fin Bloque SS_1015_CQU_20120517
    MSG_ERROR_CONCESION = 'Debe seleccionar una concesionaria';
    MSG_ERROR_DIRECTORIO = 'El directorio indicado en los parámetros no existe, verifique Parametros Generales';
    MSG_ERROR_TITULO = 'Atención';
    MSG_EMPTY_QUERY_ERROR = 'No se han encontrado registros para los criterios de búsqueda seleccionados.';    // SS_1006_1015_CQU_20120928
var
    FGenerarArchivoTransaccionesRechazadas: TFGenerarArchivoTransaccionesRechazadas;
    bCargando : boolean; // Con esta variable le indico al sistema si estoy cargando el formulario
    FLista : TStringList;
    FBorrarArchivo : Boolean;   // SS_1015_CQU_20120530

implementation

{$R *.dfm}
function TFGenerarArchivoTransaccionesRechazadas.Inicializar(txtCaption: ANSIString; MDIChild:Boolean; TipoInterfaceIndicada: string): Boolean;

    {-----------------------------------------------------------------------------
      Function Name: CargarCombo
      Author:    CQuezada
      Date Created: 04-04-2012
      Description: Elimina la concesionaria Costanera Norte del Combo.
      Parameters: None
      Return Value: None
    -----------------------------------------------------------------------------}
    procedure CargarCombo;
    var indice : Integer;
    begin
        CargarComboOtrasConcesionarias(DMConnections.BaseCAC, vcbConcesionarias, True, True);
        vcbConcesionarias.ItemIndex := 0;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';

begin
    Result := false;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    TipoInterface := TipoInterfaceIndicada;
    // Cargando la Aplicación
    bCargando := true;
    // Elijo el modo en que se visualizara el formulario
    if not MDIChild then begin
		  FormStyle := fsNormal;
		  Visible := False;
    end;
    // Centro el form
	CenterForm(Self);
    try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected And VerificarParametrosGenerales;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
    FCancelar := False;                             //inicio cancelar en false
    FCodigoOperacion := 0;                          //Inicializo el codigo de operacion
    CargarCombo;                                    // Cargo los datos en el Combo Concesionarias
    //btnProcesar.Enabled := Result;                  // Si está todo bien (Result) habilito el botón       // SS_1015_CQU_20120517
    // Obtengo el Codigo del Módulo
    //CodigoModulo := QueryGetValue(DMConnections.BaseCAC, 'SELECT CodigoModulo FROM Modulos WITH (NOLOCK) WHERE Descripcion = ''Interfaz Saliente Generar Archivo Transacciones Rechazadas''');    //SS_1006_1015_CQU_20120829
    CodigoModulo := ObtenerCodigoModuloTransaccionesRechazadasOC(DMConnections.BaseCAC);                    // SS_1006_1015_CQU_20120829
    //FNombreArchivo := GenerarNombreArchivo(edFechaDesde, edFechaHasta); // Genero el nombre del archivo   // SS_1015_CQU_20120510
    //txtArchivo.Text := FNombreArchivo;              // Coloco el nombre del archivo en el texto           // SS_1015_CQU_20120510
    //grpTipoTransaccion.Enabled := False;            // Deshabilito el gripo de RadioButons                // SS_1015_CQU_20120517
    rdgTipoTransaccion.Enabled := False;                                                                    // SS_1015_CQU_20120517
    PnlAvance.Visible := False;
    Caption := AnsiReplaceStr (txtCaption, '&', '');
    // Terminó de Cargar
    bCargando := False;
end;
{-----------------------------------------------------------------------------
  Function Name: VerificarParametrosGenerales
  Author:    CQuezada
  Date Created: 04-04-2012
  Description: Verifica que el parametro de directorio exista en la BD y físicamente.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFGenerarArchivoTransaccionesRechazadas.VerificarParametrosGenerales : boolean;
resourcestring
    MSG_ERROR_CHECK_GENERAL   = 'Error al verificar parametro ';
    MSG_ERROR                 = 'Error';
    STR_NOT_EXISTS_GENERAL    = 'No existe parametro: ';
    STR_EMPTY_GENERAL         = 'Parametro vacio: ';
    STR_DIRECTORY_NOT_EXISTS  = 'No existe el directorio indicado en parametros generales: ' + CRLF;
var
    DescError : AnsiString;
    // sParametroDirectorio : string; // SS_1015_CQU_20120510
begin
    Result    := True;
    DescError := '';
    try
        //if (TipoInterface = 'EST') then
        //    sParametroDirectorio := DIRECTORIO_DESTINO + TipoInterface + 'AC_RECHAZ'
        //else // Para las otras transacciones sólo cambia el tipo (MOV y TRA, etc...)
        //    sParametroDirectorio := DIRECTORIO_DESTINO; // SS_1015_CQU_20120510

        try
            //Obtengo el Parametro General
            // Inicio Bloque SS_1015_CQU_20120510
            {
            if not ObtenerParametroGeneral(DMConnections.BaseCAC, sParametroDirectorio , FDirectorioRechazo) then begin
                DescError := STR_DIRECTORY_NOT_EXISTS + sParametroDirectorio;
                Result := False;
                Exit;
            end;
            //Verifico que sea válido
            FDirectorioRechazo := GoodDir(FDirectorioRechazo);
            if  not DirectoryExists(FDirectorioRechazo) then begin
                DescError := STR_DIRECTORY_NOT_EXISTS + FDirectorioRechazo;
                Result := False;
                Exit;
            end;
            }
            // Obtengo el Parámetro General para Movimientos
            if (FDirectorioRechazoMov = '') then begin
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIRECTORIO_ARCHIVOS_MOV_RECHAZADOS , FDirectorioRechazoMov) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + DIRECTORIO_ARCHIVOS_MOV_RECHAZADOS;
                    Result := False;
                    Exit;
                end;
            end;
            FDirectorioRechazoMov := GoodDir(FDirectorioRechazoMov); //Verifico que sea válido
            if  not DirectoryExists(FDirectorioRechazoMov) then begin
                DescError := STR_DIRECTORY_NOT_EXISTS + FDirectorioRechazoMov;
                Result := False;
                Exit;
            end;
            if (FPrefRechArchivoMovimiento = '') then begin
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PREFIJO_ARCHIVO_MOV_RECHAZADOS , FPrefRechArchivoMovimiento) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + PREFIJO_ARCHIVO_MOV_RECHAZADOS;
                    Result := False;
                    Exit;
                end;
            end;
            // Obtengo el Parámetro General para Transitos
            if (FDirectorioRechazoTra = '') then begin
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIRECTORIO_ARCHIVOS_TTOS_RECHAZADOS , FDirectorioRechazoTra) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + DIRECTORIO_ARCHIVOS_TTOS_RECHAZADOS;
                    Result := False;
                    Exit;
                end;
            end;
            FDirectorioRechazoTra := GoodDir(FDirectorioRechazoTra); //Verifico que sea válido
            if  not DirectoryExists(FDirectorioRechazoTra) then begin
                DescError := STR_DIRECTORY_NOT_EXISTS + FDirectorioRechazoTra;
                Result := False;
                Exit;
            end;
            if (FPrefRechArchivoTransito = '') then begin
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PREFIJO_ARCHIVO_TTOS_RECHAZADOS , FPrefRechArchivoTransito) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + PREFIJO_ARCHIVO_TTOS_RECHAZADOS;
                    Result := False;
                    Exit;
                end;
            end;
            // Obtengo el Parámetro General para Estacionamientos
            //FDirectorioRechazoEst := FDirectorioRechazoTra;                   // SS_1015_CQU_20120517
            //FPrefRechArchivoEstacionamiento := FPrefRechArchivoTransito;      // ss_1015_cqu_20120517
            // Fin Bloque SS_1015_CQU_20120510

            // Inicio Bloque SS_1015_CQU_20120517
            // Obtengo el Parámetro General para Estacionamientos
            if (FDirectorioRechazoEst = '') then begin
				if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIRECTORIO_ARCHIVOS_ESTA_RECHAZADOS , FDirectorioRechazoEst) then begin
					DescError := STR_DIRECTORY_NOT_EXISTS + DIRECTORIO_ARCHIVOS_ESTA_RECHAZADOS;
					Result := False;
					Exit;
				end;
			end;
			FDirectorioRechazoEst := GoodDir(FDirectorioRechazoEst); //Verifico que sea válido
			if  not DirectoryExists(FDirectorioRechazoEst) then begin
				DescError := STR_DIRECTORY_NOT_EXISTS + FDirectorioRechazoEst;
				Result := False;
				Exit;
			end;
			if (FPrefRechArchivoEstacionamiento = '') then begin
				if not ObtenerParametroGeneral(DMConnections.BaseCAC, PREFIJO_ARCHIVO_ESTA_RECHAZADOS , FPrefRechArchivoEstacionamiento) then begin
					DescError := STR_DIRECTORY_NOT_EXISTS + PREFIJO_ARCHIVO_ESTA_RECHAZADOS;
					Result := False;
					Exit;
				end;
			end;
            // Obtengo el Parámetro General para Saldos
			if (FDirectorioRechazoSald = '') then begin
				if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIRECTORIO_ARCHIVOS_SALD_RECHAZADOS , FDirectorioRechazoSald) then begin
					DescError := STR_DIRECTORY_NOT_EXISTS + DIRECTORIO_ARCHIVOS_SALD_RECHAZADOS;
					Result := False;
					Exit;
				end;
			end;
			FDirectorioRechazoSald := GoodDir(FDirectorioRechazoSald); //Verifico que sea válido
			if  not DirectoryExists(FDirectorioRechazoSald) then begin
				DescError := STR_DIRECTORY_NOT_EXISTS + FDirectorioRechazoSald;
				Result := False;
				Exit;
			end;
			if (FPrefRechArchivoSaldo = '') then begin
				if not ObtenerParametroGeneral(DMConnections.BaseCAC, PREFIJO_ARCHIVO_SALD_RECHAZADOS , FPrefRechArchivoSaldo) then begin
					DescError := STR_DIRECTORY_NOT_EXISTS + PREFIJO_ARCHIVO_SALD_RECHAZADOS;
					Result := False;
					Exit;
				end;
			end;
            // Obtengo el Parámetro General para Cuotas
			if (FDirectorioRechazoCuot = '') then begin
				if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIRECTORIO_ARCHIVOS_CUOT_RECHAZADOS , FDirectorioRechazoCuot) then begin
					DescError := STR_DIRECTORY_NOT_EXISTS + DIRECTORIO_ARCHIVOS_CUOT_RECHAZADOS;
					Result := False;
					Exit;
				end;
			end;
			FDirectorioRechazoCuot := GoodDir(FDirectorioRechazoCuot); //Verifico que sea válido
			if  not DirectoryExists(FDirectorioRechazoCuot) then begin
				DescError := STR_DIRECTORY_NOT_EXISTS + FDirectorioRechazoCuot;
				Result := False;
				Exit;
			end;
			if (FPrefRechArchivoCuot = '') then begin
				if not ObtenerParametroGeneral(DMConnections.BaseCAC, PREFIJO_ARCHIVO_CUOT_RECHAZADOS , FPrefRechArchivoCuot) then begin
					DescError := STR_DIRECTORY_NOT_EXISTS + PREFIJO_ARCHIVO_CUOT_RECHAZADOS;
					Result := False;
					Exit;
				end;
			end;
            // Fin Bloque SS_1015_CQU_20120517
        except
            on e: Exception do begin
                Result := False;
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL, e.Message, MSG_ERROR, MB_ICONWARNING);
            end;
        end;

    finally
        //si no pasó la verificación de parametros generales
        if not (Result) then begin
            //informo la situación
            MsgBoxErr(MSG_ERROR_CHECK_GENERAL, DescError, MSG_ERROR, MB_ICONWARNING);
        end;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: CambiarNombreArchivo
  Author:    CQuezada
  Date Created: 03-04-2012
  Description: Crea un nuevo nombre de archivo en base a los datos del formulario
                y actualiza el campo de texto que lo despliega.
  Parameters: FechaInicio, FechaTermino: TDateEdit
  Return Value: string

  Firma         : SS_1015_CQU_20120510
  Descripcition : se modifica el formato en el nombre de archivo y la carpeta donde quedará
-----------------------------------------------------------------------------}
function TFGenerarArchivoTransaccionesRechazadas.GenerarNombreArchivo(FechaInicio, FechaTermino : TDateEdit) : string;
resourcestring
    MSG_FILE_NAME_ERROR = 'Error al crear el nombre del archivo';
    MSG_DIRECTORY_ERROR = 'El directorio de salida %s no existe.' + CRLF +
                          'Verifique los parámetros generales.';
var
    NombreArchivo : string;
    NumeroSecuencia : string;
    FDirectorioRechazo : AnsiString;    // SS_1015_CQU_20120510
    TipoTransaccion : string;           // SS_1015_CQU_20120510
    CodigoConceionariaTemp : string;    // SS_1015_CQU_20120510
begin
    try
        // Inicio Bloque SS_1015_CQU_20120510
        //Obtengo Numero de Secuencia
        {
        ObtenerNumeroSecuencia(NumeroSecuencia);
        if NumeroSecuencia = null then NumeroSecuencia := '000';
        FNumeroSecuencia := StrToInt(NumeroSecuencia);
        // Por defecto el nombre de archivo será con la fecha actual
        NombreArchivo := FormatDateTime ('yyyymmdd', FechaInicio.Date)
        + '_' + FormatDateTime ('yyyymmdd', FechaTermino.Date)
        + '_' + FCodigoConsecionaria
        + '_' + TipoInterface + 'RECHAZ'
        + '.' + NumeroSecuencia
        + '.txt';
        }
        if (TipoInterface = 'TRA') then begin
            NombreArchivo := FPrefRechArchivoTransito;
            TipoTransaccion := TIPO_MLFF;
            FDirectorioRechazo := FDirectorioRechazoTra;
        end
        else if (TipoInterface = 'EST') then begin
            NombreArchivo := FPrefRechArchivoEstacionamiento;
            //TipoTransaccion := TIPO_PEAJ; // SS_1015_CQU_20120517
            TipoTransaccion := TIPO_ESTA;   // SS_1015_CQU_20120517
            FDirectorioRechazo := FDirectorioRechazoEst;
        end
        else if (TipoInterface = 'MOV') then begin
            NombreArchivo := FPrefRechArchivoMovimiento;
            TipoTransaccion := TIPO_MOV;
            FDirectorioRechazo := FDirectorioRechazoMov;
        // Inicio Bloque SS_1015_CQU_20120517
        //end;
        end
        else if (TipoInterface = 'SALD') then begin
            NombreArchivo := FPrefRechArchivoSaldo;
            TipoTransaccion := TIPO_SALD;
            FDirectorioRechazo := FDirectorioRechazoSald;
        end
        else if (TipoInterface = 'CUOT') then begin
            NombreArchivo := FPrefRechArchivoCuot;
            TipoTransaccion := TIPO_CUOT;
            FDirectorioRechazo := FDirectorioRechazoCuot;
        end;
        // fin Bloque SS_1015_CQU_20120517
        // Coloco los ceros para mantener el estandar del importador
        if (Length(FCodigoConsecionaria) = 1) then
            CodigoConceionariaTemp  := '0' + FCodigoConsecionaria;
             
        // Genero el Nombre de Archivo
        NombreArchivo := NombreArchivo
        + SEPARADOR
        + '01'
        + SEPARADOR
        + CodigoConceionariaTemp
        + SEPARADOR
        + TipoTransaccion
        + SEPARADOR
        + FormatDateTime ('yyyymmddhhnnss', Date + Time)
        + '.txt';
        // Fin Bloque SS_1015_CQU_20120510

        // Verifico que el directorio seleccionado no haya cambiado
        // por el directorio de los parámetros
        if (NOT (DirectorioSeleccionado = '') AND (DirectorioSeleccionado <> FDirectorioRechazo)) then
            FDirectorioRechazo := DirectorioSeleccionado;
        Result := GoodFileName(FDirectorioRechazo + NombreArchivo, 'txt');
    except
        raise Exception.Create(MSG_FILE_NAME_ERROR);
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: CambiarTipoTransaccion
  Author:    CQuezada
  Date Created: 03-04-2012
  Description: Cambia el TipoTransaccion en base a la Concesionaria
            seleccionada en el ComboBox y su valor en la tabla TipoConcesionaria
  Parameters: None
  Return Value: None

  Firma         : SS_1015_CQU_20120517
  Description   : Realiza una mejora la cual permite hacer la consulta por única vez en la BD
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.CambiarTipoTransaccion();
var
    sConsulta : string;
begin
    // Inicio Bloque SS_1015_CQU_20120517
    {
    sConsulta := 'SELECT TipoConcesionaria.Descripcion'
    + ' FROM TipoConcesionaria WITH (NOLOCK)'
    + ' INNER JOIN Concesionarias WITH (NOLOCK INDEX = [PK_Concesionarias])'
    + ' ON Concesionarias.CodigoTipoConcesionaria = TipoConcesionaria.CodigoTipoConcesionaria'
    + ' WHERE Concesionarias.CodigoConcesionaria = ' + FCodigoConsecionaria;

    try
        qryConsulta.SQL.Text := sConsulta;
        qryConsulta.ExecSQL;
        qryConsulta.Open;
        if (qryConsulta.RecordCount > 0) then
        begin
            if (qryConsulta.FieldByName('Descripcion').AsString = 'Estacionamiento') then
            begin
                rdbVariable.Caption := 'Estacionamientos';
                if not rdbMovimiento.Checked then TipoInterface := 'EST';
            end
            else
            begin
                rdbVariable.Caption := 'Tránsitos'; // SS-1015-CQU-20120420
                if not rdbMovimiento.Checked then TipoInterface := 'TRA';
            end;
            rdbVariable.Refresh;
        end;
        // Si no existe el parámetro o el directorio, lanzo una excepción
        if not VerificarParametrosGenerales then raise Exception.Create(MSG_ERROR_DIRECTORIO);
    finally
        qryConsulta.Close;
    end;
    }
    sConsulta := 'SELECT TipoConcesionaria.CodigoTipoConcesionaria'
    + ', Concesionarias.CodigoConcesionaria'
    + ' FROM TipoConcesionaria WITH (NOLOCK)'
    + ' INNER JOIN Concesionarias WITH (NOLOCK INDEX = [PK_Concesionarias])'
    + ' ON Concesionarias.CodigoTipoConcesionaria = TipoConcesionaria.CodigoTipoConcesionaria';
    try
        // Verifico que no se haya cargado con anterioridad
        if (qryConsulta.Connection = nil) then begin
            qryConsulta.Create(nil);
            qryConsulta.Connection := DMConnections.BaseCAC;
            qryConsulta.SQL.Text := sConsulta;
            qryConsulta.ExecSQL;
        end;
        qryConsulta.Open;
        if not qryConsulta.Eof then begin
            if qryConsulta.Locate('CodigoConcesionaria', FCodigoConsecionaria, [loCaseInsensitive]) then begin
                if (qryConsulta.FieldByName('CodigoTipoConcesionaria').AsInteger = 2) then
                    rdgTipoTransaccion.ItemIndex := 1;
            end;
        end;
    finally
        qryConsulta.Close;
    end;
    // Fin Bloque SS_1015_CQU_20120517
end;
{-----------------------------------------------------------------------------
  Function Name: CambiarNombreArchivo
  Author:    CQuezada
  Date Created: 03-04-2012
  Description: Cambia el nombre de archivo y actualiza el campo de texto que lo despliega.
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.CambiarNombreArchivo();
begin
    FNombreArchivo := GenerarNombreArchivo(edFechaDesde, edFechaHasta);
    txtArchivo.Text := FNombreArchivo;
    //if (NOT btnProcesar.Enabled) then btnProcesar.Enabled := True; // SS_1015_CQU_20120517
    //if (dbgRechazados.Visible) then btnGenerar.Enabled := True;      // SS_1015_CQU_20120517 // SS_1015_CQU_20120528
    if (dblRechazados.Visible) then btnGenerar.Enabled := True;         // SS_1015_CQU_20120528
    txtArchivo.Refresh;
end;
{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    CQuezada
  Date Created: 03-04-2012
  Description: Comienza el proceso de generación del archivo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.btnGenerarClick(Sender: TObject);
// Inicio Bloque SS_1015_CQU_20120517
{
function ValidarEntrada: Boolean;
resourcestring
    MSG_ERROR                 = 'Validación';
var
    DescError :string;
begin
    DescError := '';
    Result := True;

    if (Integer(vcbConcesionarias.Value) <= 0) then
        DescError := DescError + 'Debe seleccionar una concesionaria' + #13 + #10;

    if (edFechaDesde.IsEmpty OR edFechaHasta.IsEmpty) then
        DescError := DescError + 'Debe ingresar una fecha válida' + #13 + #10
    else if edFechaDesde.Date > edFechaHasta.Date then
        DescError := DescError + 'Fecha Desde debe ser menor o igual a Fecha Hasta' + #13 + #10;

    //if (NOT rdbMovimiento.Checked AND NOT rdbVariable.Checked) then   // SS_1015_CQU_20120517
    if (rdgTipoTransaccion.ItemIndex <= 0) then                         // SS_1015_CQU_20120517
        DescError := DescError + 'Debe seleccionar un tipo de transacción' + #13 + #10;

    if DescError <> '' then
    begin
        MsgBox(DescError, MSG_ERROR, MB_ICONWARNING);
        Result := False;
    end;
end;
}
// Fin Bloque SS_1015_CQU_20120517
resourcestring
    MSG_PROCESS_CANCEL = 'Proceso cancelado';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finalizó con Errores';
  	MSG_PROCESS_FINALLY_OK = 'El proceso finalizó con éxito';
Const
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
    STR_OBTAINING_DATA     = 'Obteniendo los Datos...';
    STR_SAVE_FILE          = 'Guardando el Archivo...';
    STR_SAVE_HISTORIC      = 'Guardando en Historico...';

begin
    // Inicio Bloque SS_1015_CQU_20120517
    {
    if (ValidarEntrada) then
    begin
        btnProcesar.Enabled := False; // SS_1015_CQU_20120517
        PnlAvance.visible := True;
  	    Screen.Cursor := crHourGlass;
        KeyPreview := True;
        FErrorGrave := False;

        FFechaDesde := edFechaDesde.Date;
        FFechaHasta := edFechaHasta.Date;
        FNombreArchivo := txtArchivo.Text;

        try
            //Registro la operación en el Log
            lblMensaje.Caption := STR_REGISTER_OPERATION;          //Informo Tarea
            Application.ProcessMessages;                           //Refresco la pantalla
            if not RegistrarOperacion then begin
                FErrorGrave := True;
                Exit;
            end;

            // Obtengo los datos y genero el archivo
            lblMensaje.Caption := STR_OBTAINING_DATA;               //Informo Tarea
            Application.ProcessMessages;                            //Refresco la pantalla
            // Es Estacionamiento
            if TipoInterface = 'EST' then begin
  	            if not ObtenerEstacionamientosRechazados then begin
                    FErrorGrave := True;
                    Exit;
                end;
            end;
            // Es Transaccion o Movimiento
            if (TipoInterface = 'MOV') OR (TipoInterface = 'TRA') then begin
                if not ObtenerRechazos then begin
                    FErrorGrave := True;
                    Exit;
                end;
            end;
            {
            else if (TipoInterface = 'TRA') then
                if not ObtenerEstacionamientosRechazados then begin
                    FErrorGrave := True;
                    Exit;
                end;
            }

        btnGenerar.Enabled := False;
        btnObtener.Enabled := False;
        //dbgRechazados.Visible := False;   // SS_1015_CQU_20120528
        dblRechazados.Visible := False;     // SS_1015_CQU_20120528
        PnlAvance.visible := True;
        dblRechazados.SendToBack;           // SS_1015_CQU_20120530
        PnlAvance.BringToFront;             // SS_1015_CQU_20120530
  	    Screen.Cursor := crHourGlass;
        KeyPreview := True;
        FErrorGrave := False;
    // Fin Bloque SS_1015_CQU_20120517
        try
            // Guardo el archivo
            lblMensaje.Caption := STR_SAVE_FILE;                    //Informo Tarea
            Application.ProcessMessages;                            //Refresco la pantalla
  	        if not GuardarArchivo then begin
                FErrorGrave := True;
                Exit;
            end;

            if TipoInterface = 'EST' then begin
                //Guardo los registros que se agregaron al archivo
                lblmensaje.Caption := STR_SAVE_HISTORIC;                 //Informo Tarea
                Application.ProcessMessages;
                if not GuardarHistorico then begin
                    FErrorGrave := true;
                    Exit;
                end;
            end;

            //Actualizo el log al Final
            ActualizarLog;

        finally
            Screen.Cursor := crDefault;
            pbProgreso.Position := 0;
            PnlAvance.visible := False;
            dblRechazados.BringToFront;       // SS_1015_CQU_20120530
            PnlAvance.SendToBack;             // SS_1015_CQU_20120530
            lblMensaje.Caption := '';
            btnObtener.Enabled := True; // SS_1015_CQU_20120517
            if FCancelar then begin
                //Muestro mensaje que el proceso fue cancelado  (sin errores)
                MsgBox(MSG_PROCESS_CANCEL, Self.Caption, MB_OK + MB_ICONINFORMATION);
            end else if FErrorGrave then begin
                //Muestro mensaje que finalizo por un error
                MsgBox(MSG_PROCESS_FINALLY_WITH_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
            end else begin
                //Muestro mensaje que el proceso finalizo exitosamente
                MsgBox(MSG_PROCESS_FINALLY_OK, Self.Caption, MB_OK + MB_ICONINFORMATION);
                // Inicio SS_1015_CQU_20120517
                //btnProcesar.Enabled := True;
                //dbgRechazados.Visible := True;    // SS_1015_CQU_20120528
                dblRechazados.Visible := True;      // SS_1015_CQU_20120528
                btnGenerar.Enabled := True
                // Fin SS_1015_CQU_20120517
            end;
            KeyPreview := False;
            // Dejo el Botón Habilitado
            //btnProcesar.Enabled := True;
        end;
    //end; // SS_1015_CQU_20120517
end;
{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    CQuezada
  Date Created: 03-Abril-2012
  Description: Cierra el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.btnSalirClick(Sender: TObject);
begin
    Close;
end;
{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    CQuezada
  Date Created: 03-Abril-2012
  Description: Libera el formulario de memoria.
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;
{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    edbaez
  Date Created: 28-Febrero-2012
  Description: Permite salir solo si no esta procesando.
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando.
  	CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    edbaez
  Date Created: 28-Febrero-2012
  Description: Permito Cancelar
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE:
            begin
                FCancelar := True;
                PnlAvance.Visible := False;
            end;
    else
        FCancelar := False;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: edFechaDesdeChange
  Author:    CQuezada
  Date Created: 4-Abril-2012
  Description: Cambia la fecha y también cambia el nombre del archivo a generar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.edFechaDesdeChange(Sender: TObject);
begin
    if not bCargando then begin
        CambiarNombreArchivo;
        FFechaDesde := edFechaDesde.Date;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: edFechaHastaChange
  Author:    CQuezada
  Date Created: 4-Abril-2012
  Description: Cambia la fecha y también cambia el nombre del archivo a generar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.edFechaHastaChange(Sender: TObject);
begin
    if not bCargando then begin
        CambiarNombreArchivo;
        FFechaHasta := edFechaHasta.Date;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: txtArchivoButtonClick
  Author:    CQuezada
  Date Created: 4-Abril-2012
  Description: Permite al usuario colocar su propia ruta y nombre de archivo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.txtArchivoButtonClick(Sender: TObject);

resourcestring
    MSG_ERROR           = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST = 'El archivo %s ya existe';
var
    CanceloDialogo : boolean;                          // SS_1015_CQU_20120530
begin
    CanceloDialogo := False;                           // SS_1015_CQU_20120530
//    dlgSelArchivo.Filter := '*.txt';
    //dlgSelArchivo.InitialDir := FDirectorioRechazo; // SS_1015_CQU_20120510
  	dlgSelArchivo.FileName := FNombreArchivo;
    if dlgSelArchivo.Execute then begin
        txtArchivo.Text := UpperCase( dlgSelArchivo.Filename );
        if FileExists( txtArchivo.text ) then begin
          	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtArchivo.Text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;
    end else
        CanceloDialogo := True; // SS_1015_CQU_20120530
    // Si existe el Archivo se habilita el botón Procesar
    if not (FileExists(txtArchivo.Text)) then
    begin
        // Inicio Bloque SS_1015_CQU_20120517
        //btnProcesar.Enabled := true;
        //if not (dbgRechazados = nil) then begin                           // SS_1015_CQU_20120528
            //if (dbgRechazados.DataSource.DataSet.RecordCount > 0) then    // SS_1015_CQU_20120528
        btnGenerar.Enabled := False;                                        // SS_1015_CQU_20120530
        if not (dblRechazados = nil) then                                   // SS_1015_CQU_20120530
            if not (dblRechazados.DataSource = nil) then                    // SS_1015_CQU_20120530
                if not (dblRechazados.DataSource.DataSet = nil) then        // SS_1015_CQU_20120530
                    if (dblRechazados.DataSource.DataSet.RecordCount > 0) then      // SS_1015_CQU_20120528
                        btnGenerar.Enabled := True;
        // Inicio Bloque SS_1015_CQU_20120517
        FNombreArchivo         := ExtractFileName(txtArchivo.Text);
        //FDirectorioRechazo     := ExtractFilePath(txtArchivo.Text); // SS_1015_CQU_20120510
        DirectorioSeleccionado := ExtractFilePath(txtArchivo.Text);
    end
    else begin
        if not (CanceloDialogo) then    // SS_1015_CQU_20120530
            MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtArchivo.Text]), MSG_ERROR, MB_ICONERROR );
        Exit;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: vcbConcesionariasChange
  Author:    CQuezada
  Date Created: 4-Abril-2012
  Description: Cambia el nombre de archivo, el tipo de transacción y oculta o cambia los RadioButton
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.vcbConcesionariasChange(Sender: TObject);
begin
    if not bCargando then
    begin
        if (vcbConcesionarias.ItemIndex = 0) then
        begin
            // Inicio Bloque SS_1015_CQU_20120517
            {
            grpTipoTransaccion.Enabled := False;
            rdbMovimiento.Checked := False;
            rdbVariable.Checked := False;
            rdbVariable.Caption := 'Transacciones'; // SS-1015-CQU-20120420
            }
            rdgTipoTransaccion.Enabled := False;
            rdgTipoTransaccion.ItemIndex := -1;
            // Inicio Bloque SS_1015_CQU_20120528
            //dbgRechazados.DataSource := nil;
            //dbgRechazados.Visible := false;
            dblRechazados.DataSource := nil;
            dblRechazados.Visible := False;
            // Fin Bloque SS_1015_CQU_20120528
            btnGenerar.Enabled := false;
            // Fin Bloque SS_1015_CQU_20120517
            MsgBox(MSG_ERROR_CONCESION, MSG_ERROR_TITULO);
        end
        else
        begin
            FCodigoConsecionaria := vcbConcesionarias.Items[vcbConcesionarias.ItemIndex].Value;
            // Inicio Bloque SS_1015_CQU_20120517
            //dbgRechazados.DataSource := nil;  // SS_1015_CQU_20120528
            //dbgRechazados.Visible := false;   // SS_1015_CQU_20120528
            dblRechazados.DataSource := nil;    // SS_1015_CQU_20120528
            dblRechazados.Visible := False;     // SS_1015_CQU_20120528
            btnGenerar.Enabled := false;
            //CambiarTipoTransaccion();
            //grpTipoTransaccion.Enabled := True;
            if not rdgTipoTransaccion.Enabled then
                rdgTipoTransaccion.Enabled:= True;
            if (FCodigoConsecionaria <> CODIGO_PARQUEARAUCO) AND (rdgTipoTransaccion.ItemIndex = 1) then begin
                rdgTipoTransaccion.ItemIndex := -1;
            end
            else
                CambiarTipoTransaccion();
            // Fin Bloque SS_1015_CQU_20120517
            CambiarNombreArchivo;
            if TipoInterface = 'EST' then begin
                // Inicio Bloque SS_1015_CQU_20120517
                //if (not rdbMovimiento.Checked) AND (not rdbVariable.Checked) then
                //    rdbVariable.Checked := True;
                if (rdgTipoTransaccion.ItemIndex <= 0) AND (FCodigoConsecionaria = CODIGO_PARQUEARAUCO) then rdgTipoTransaccion.ItemIndex := 2;
                // Fin Bloque SS_1015_CQU_20120517
            end;
        end;
    end;
end;
// Inicio Bloque SS_1015_CQU_20120517
{-----------------------------------------------------------------------------
  Function Name: rdbMovimientoClick
  Author:    CQuezada
  Date Created: 4-Abril-2012
  Description: Al pinchar un RadioButton, cambia el tipo de transacción y el nombre de archivo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
{
procedure TFGenerarArchivoTransaccionesRechazadas.rdbMovimientoClick(Sender: TObject);
begin
    TipoInterface := 'MOV';
    if VerificarParametrosGenerales then
        CambiarNombreArchivo
    else
        raise Exception.Create(MSG_ERROR_DIRECTORIO);
end;
// Fin Bloque SS_1015_CQU_20120517
{-----------------------------------------------------------------------------
  Function Name: ObtenerRechazos
  Author:    CQuezada
  Date Created: 05-Abril-2012
  Description: Obtiene los Movimientos o Transacciones rechazadas desde las tablas
                .- RegistrosRechazadosOtrasConcesionarias
                .- TransitosRechazadosOtrasConcesionarias
                .- MovimientosRechazadosOtrasConcesionarias
  Parameters: None
  Return Value: True or False
-----------------------------------------------------------------------------}
function TFGenerarArchivoTransaccionesRechazadas.ObtenerRechazos: Boolean;
Resourcestring
    MSG_COULD_NOT_OBTAIN_ESTACIONAMIENTOS_RECHAZADOS = 'No se pudieron obtener rechazos';
    //MSG_EMPTY_QUERY_ERROR = 'No hay registros rechazados para informar!'; // SS_1006_1015_CQU_20120928
    MSG_ERROR = 'Error';
var
    i : Integer;
    Linea : AnsiString;
    ErrorMsg : String;
    nProgreso : Integer;
begin
    Result := False;
    //Inicializo variables
    FLista.Clear;
    FCancelar := False;
    //inicializo la barra de progreso
    pbProgreso.Position := 0;
    pbProgreso.Max := 0; //debo colocar la cantidad de registros para el final del progreso

    //Obtengo los estacionamientos rechazados
    try
        spObtenerTransaccionesMovimientosRechazados.Close;
        spObtenerTransaccionesMovimientosRechazados.Connection := DMConnections.BaseCAC;
        spObtenerTransaccionesMovimientosRechazados.Parameters.Refresh;
        spObtenerTransaccionesMovimientosRechazados.Parameters.ParamByName('@FechaDesde').Value := FFechaDesde;
        //spObtenerTransaccionesMovimientosRechazados.Parameters.ParamByName('@FechaHata').Value := FFechaHasta;//SS-1015-CQU-20120426
        spObtenerTransaccionesMovimientosRechazados.Parameters.ParamByName('@FechaHasta').Value := FFechaHasta; //SS-1015-CQU-20120426
        spObtenerTransaccionesMovimientosRechazados.Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConsecionaria;
        spObtenerTransaccionesMovimientosRechazados.Parameters.ParamByName('@TipoTransaccion').Value := TipoInterface;
        spObtenerTransaccionesMovimientosRechazados.CommandTimeout := 500;
        spObtenerTransaccionesMovimientosRechazados.Open;
        // Inicio Bloque SS_1015_CQU_20120517

        if not spObtenerTransaccionesMovimientosRechazados.IsEmpty then
        begin
        {
            //pbProgreso.Max := spObtenerTransaccionesMovimientosRechazados.FieldCount; //SS-1015-CQU-20120426
            pbProgreso.Max := spObtenerTransaccionesMovimientosRechazados.RecordCount;  //SS-1015-CQU-20120426
            nProgreso := 0;
            while not spObtenerTransaccionesMovimientosRechazados.EOF and (not FCancelar) do begin
                nProgreso := nProgreso + 1;
                i := 1;
                Linea := '';
                //mientras halla registros y no sea cancelado
                while (i <= spObtenerTransaccionesMovimientosRechazados.FieldCount) and (not FCancelar) do begin
                    //genero las lineas del archivo
                    Linea := Linea + spObtenerTransaccionesMovimientosRechazados.Fields.FieldByNumber(i).AsString + ';';
                    Inc(i);
                end;
                FLista.Add(Linea);
                spObtenerTransaccionesMovimientosRechazados.Next;

                //Refresco la pantalla
                //if nProgreso <= spObtenerTransaccionesMovimientosRechazados.FieldCount then pbProgreso.Position:= nProgreso; //SS-1015-CQU-20120426
                if nProgreso <= spObtenerTransaccionesMovimientosRechazados.RecordCount then pbProgreso.Position:= nProgreso;  //Muestro el progreso //SS-1015-CQU-20120426
                Application.ProcessMessages;

            end; }
            Result := (not FCancelar); 
        end
        else
            //MsgBoxErr(MSG_COULD_NOT_OBTAIN_ESTACIONAMIENTOS_RECHAZADOS                    // SS_1006_1015_CQU_20120928
            //, MSG_EMPTY_QUERY_ERROR, MSG_ERROR, MB_ICONERROR);                            // SS_1006_1015_CQU_20120928
            MsgBox(MSG_EMPTY_QUERY_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);        // SS_1006_1015_CQU_20120928
        // Fin Bloque SS_1015_CQU_20120517
    except
        on e: Exception do begin
            ErrorMsg := MSG_COULD_NOT_OBTAIN_ESTACIONAMIENTOS_RECHAZADOS;
            MsgBoxErr(ErrorMsg , e.Message, MSG_ERROR, MB_ICONERROR);
            //Break;
        end;
    end;      
end;
// Inicio Bloque SS_1015_CQU_20120517
{-----------------------------------------------------------------------------
  Function Name: rdbVariableClick
  Author:    CQuezada
  Date Created: 4-Abril-2012
  Description: Al pinchar un RadioButton, cambia el tipo de transacción y el nombre de archivo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
{
procedure TFGenerarArchivoTransaccionesRechazadas.rdbVariableClick(Sender: TObject);
begin
    // SS-1015-CQU-20120420
    {if (rdbVariable.Caption = 'Estacionamientos') then
        TipoInterface := 'EST'
    else
        TipoInterface := 'TRA';}
{    if (rdbVariable.Caption = 'Transacciones') then
        MsgBox(MSG_ERROR_CONCESION, MSG_ERROR_TITULO)
    else
    begin
        if (rdbVariable.Caption = 'Estacionamientos') then
            TipoInterface := 'EST'
        else
            TipoInterface := 'TRA';
    end;
    // SS-1015-CQU-20120420
    if VerificarParametrosGenerales then
        CambiarNombreArchivo
    else
        raise Exception.Create(MSG_ERROR_DIRECTORIO);    
end;
}
// Fin Bloque SS_1015_CQU_20120517
{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    edbaez
  Date Created: 29-Febrero-2012
  Description: Registra la Operación en el Log de Operaciones
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoTransaccionesRechazadas.RegistrarOperacion : Boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operación';
    MSG_ERROR = 'Error';
var
	DescError : String;
    STR_OBSERVACION : string;
    // CodigoDelModulo : integer; //SS_1006_1015_CQU_20120829
begin
    if (TipoInterface = 'EST') then
        STR_OBSERVACION := 'Generar Archivo Estacionamientos Rechazados'
    else if (TipoInterface = 'TRA') then
        STR_OBSERVACION := 'Generar Archivo Transacciones Rechazadas'
    else if (TipoInterface = 'MOV') then
        STR_OBSERVACION := 'Generar Archivo Movimientos Rechazados' //;     // Inicio Bloque SS_1015_CQU_20120517
    else if (TipoInterface = 'SALD') then
        STR_OBSERVACION := 'Generar Archivo Saldo Inicial Rechazados'
    else if (TipoInterface = 'CUOT') then
        STR_OBSERVACION := 'Generar Archivo Cuotas Rechazadas';
    // Fin Bloque SS_1015_CQU_20120517
    // Convierto a Entero el Código del Módulo
    //CodigoDelModulo := StrToInt(CodigoModulo);    // SS_1006_1015_CQU_20120829

    //Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, CodigoDelModulo, ExtractFileName(FNombreArchivo), UsuarioSistema, STR_OBSERVACION, False, False , NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);  // SS_1006_1015_CQU_20120829
    Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, CodigoModulo, ExtractFileName(FNombreArchivo), UsuarioSistema, STR_OBSERVACION, False, False , NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);       // SS_1006_1015_CQU_20120829
    if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;
{-----------------------------------------------------------------------------
  Function Name: ObtenerEstacionamientosRechazados
  Author:    edbaez
  Date Created: 29-Febrero-2012
  Description: Obtiene los estacionamientos rechazados desde las tablas
                .- RegistrosRechazadosOtrasConcesionarias
                .- EstacionamientosRechazadosOtrasConcesionarias
                .- Estacionamientos (este último se informaran los con estado = 9 (NO FACTURABLR))
  Parameters: None
  Return Value: True or False
-----------------------------------------------------------------------------}
function TFGenerarArchivoTransaccionesRechazadas.ObtenerEstacionamientosRechazados: Boolean;
Resourcestring
    MSG_COULD_NOT_OBTAIN_ESTACIONAMIENTOS_RECHAZADOS = 'No se pudieron obtener estacionamientos rechazados';
    //MSG_EMPTY_QUERY_ERROR = 'No hay estacionamientos rechazados para informar!';                             // SS_1006_1015_CQU_20120928
    MSG_EMPTY_QUERY_ERROR = 'No se han encontrado registros para los criterios de búsqueda seleccionados.';    // SS_1006_1015_CQU_20120928
    MSG_ERROR = 'Error';
var
    i : Integer;
    Linea : AnsiString;
    ErrorMsg : String;
    nProgreso : Integer;
begin
    Result := False;
    //Inicializo variables
    FLista.Clear;
    FCancelar := False;
    //inicializo la barra de progreso
    pbProgreso.Position := 0;
    pbProgreso.Max := 0; //debo colocar la cantidad de registros para el final del progreso

    //Obtengo los estacionamientos rechazados
    try
        spObtenerEstacionamientosRechazados.Close;
        spObtenerEstacionamientosRechazados.Connection := DMConnections.BaseCAC;
        spObtenerEstacionamientosRechazados.Parameters.Refresh;
        spObtenerEstacionamientosRechazados.Parameters.ParamByName('@FechaDesde').Value := FFechaDesde;
        //spObtenerEstacionamientosRechazados.Parameters.ParamByName('@FechaHata').Value := FFechaHasta;//SS-1015-CQU-20120426
        spObtenerEstacionamientosRechazados.Parameters.ParamByName('@FechaHasta').Value := FFechaHasta; //SS-1015-CQU-20120426
        spObtenerEstacionamientosRechazados.CommandTimeout := 500;
        spObtenerEstacionamientosRechazados.Open;
        if not spObtenerEstacionamientosRechazados.IsEmpty then
        begin
            //pbProgreso.Max := spObtenerEstacionamientosRechazados.FieldCount; //SS-1015-CQU-20120426
            pbProgreso.Max := spObtenerEstacionamientosRechazados.RecordCount; //SS-1015-CQU-20120426
            nProgreso := 0;
            while not spObtenerEstacionamientosRechazados.EOF and (not FCancelar) do begin
                nProgreso := nProgreso + 1;
                i := 1;
                Linea := '';
                //mientras halla registros y no sea cancelado
                while (i <= spObtenerEstacionamientosRechazados.FieldCount) and (not FCancelar) do begin
                    //genero las lineas del archivo
                    Linea := Linea + spObtenerEstacionamientosRechazados.Fields.FieldByNumber(i).AsString + ';';
                    Inc(i);
                end;
                FLista.Add(Linea);
                spObtenerEstacionamientosRechazados.Next;

                //Refresco la pantalla
                //if nProgreso <= spObtenerEstacionamientosRechazados.FieldCount then pbProgreso.Position:= nProgreso;//SS-1015-CQU-20120426
                if nProgreso <= spObtenerEstacionamientosRechazados.RecordCount then pbProgreso.Position:= nProgreso;  //Muestro el progreso //SS-1015-CQU-20120426
                Application.ProcessMessages;

            end;
            Result := (not FCancelar);
        end
        else
            // MsgBoxErr(ErrorMsg, MSG_EMPTY_QUERY_ERROR, MSG_ERROR, MB_ICONERROR);     // SS-1015-CQU-20120420
            //MsgBoxErr(MSG_COULD_NOT_OBTAIN_ESTACIONAMIENTOS_RECHAZADOS                                            // SS_1006_1015_CQU_20120928
            //, MSG_EMPTY_QUERY_ERROR, MSG_ERROR, MB_ICONERROR);                          // SS-1015-CQU-20120420   // SS_1006_1015_CQU_20120928
            MsgBox(MSG_EMPTY_QUERY_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);                                // SS_1006_1015_CQU_20120928
        
    except
        on e: Exception do begin
            ErrorMsg := MSG_COULD_NOT_OBTAIN_ESTACIONAMIENTOS_RECHAZADOS;
            MsgBoxErr(ErrorMsg , e.Message, MSG_ERROR, MB_ICONERROR);
            //Break;
        end;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: ObtenerNumeroSecuencia
  Author:    edbaez
  Date Created: 01-Marzo-2012
  Description: Obtengo el numero de secuencia
  Parameters: var NumeroSecuencia:AnsiString
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFGenerarArchivoTransaccionesRechazadas.ObtenerNumeroSecuencia(var NumeroSecuencia : AnsiString) : Boolean;
resourcestring
    MSG_ERROR = 'Error al crear el numero de secuencia';
Var
    SQLNumeroSecuenciaTBK : String;
    Valor : Integer;
begin
    try
        //Obtengo el numero de secuencia
        //SQLNumeroSecuenciaTBK := 'Select dbo.ObtenerNumeroSecuenciaTBK (' + CodigoModulo + ')';           // SS_1006_1015_CQU_20120829
        SQLNumeroSecuenciaTBK := 'Select dbo.ObtenerNumeroSecuenciaTBK (' + IntToStr(CodigoModulo) + ')';   // SS_1006_1015_CQU_20120829
        Valor := QueryGetValueInt(DMConnections.BaseCAC, SQLNumeroSecuenciaTBK);
        //el numero de secuencia debe ocupar 3 posiciones
        NumeroSecuencia := IStr0(Valor, 3);
        Result := True;
    except
        raise Exception.Create(MSG_ERROR);
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: GuardarArchivo
  Author:    edbaez
  Date Created: 06-Marzo-2012
  Description: Proceso que guarda el archivo que se generó
  Parameters: None
  Return Value: True or False
-----------------------------------------------------------------------------}
function TFGenerarArchivoTransaccionesRechazadas.GuardarArchivo : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: GuardarArchivoRechazados
      Author:    edbaez
      Date Created: 06-Marzo-2012
      Description: Guarda el archivo que se generó
      Parameters: var DescError:AnsiString
      Return Value: True or False
    -----------------------------------------------------------------------------}
    Function GuardarArchivoRechazado(var DescError : AnsiString) : Boolean;
    Resourcestring
        MSG_ERROR  = 'No se pudo guardar archivo';
        MSG_CANCEL = 'Se ha cancelado el proceso debido a la existencia de un ' + CRLF +
                     'archivo idéntico en el directorio de destino';
    Const
        FILE_TITLE   = ' El archivo:  ';
        FILE_EXISTS  = ' ya existe en el directorio de destino. ¿Desea reemplazarlo?';
        FILE_WARNING = ' Atención';
        SEPARADOR_CAMPO = ';';              // SS_1015_CQU_20120517
    var                                     // SS_1015_CQU_20120517
        sLinea : string;                    // SS_1015_CQU_20120517
        I, J, nProgreso : Integer;          // SS_1015_CQU_20120517
    begin
        //Verificamos que no exista uno con el mismo nombre
        if FileExists(FNombreArchivo) then begin
            if (MsgBox( FILE_TITLE + FNombreArchivo + FILE_EXISTS, FILE_WARNING, MB_YESNO + MB_ICONQUESTION) = IDNO) then begin
                DescError := MSG_CANCEL; //informo que fue cancelado
                Result := False; //fallo
                FBorrarArchivo := False;    // SS_1015_CQU_20120530
                Exit; //y salgo
            end;
        end;

        try
            // Inicio Bloque SS_1015_CQU_20120517
            FLista.Clear;
            sLinea := '';
            //if (dbgRechazados.DataSource.DataSet.RecordCount > 0) then begin  // SS_1015_CQU_20120528
            if (dblRechazados.DataSource.DataSet.RecordCount > 0) then begin    // SS_1015_CQU_20120528
                PnlAvance.Visible := True;
                pbProgreso.Position := 0;
                //pbProgreso.Max := dbgRechazados.DataSource.DataSet.RecordCount; // SS_1015_CQU_20120528
                pbProgreso.Max := dblRechazados.DataSource.DataSet.RecordCount;   // SS_1015_CQU_20120528
                nProgreso := 0;
                //dbgRechazados.DataSource.DataSet.First;   // SS_1015_CQU_20120528
                dblRechazados.DataSource.DataSet.First;     // SS_1015_CQU_20120528
                //for I := 0 to dbgRechazados.DataSource.DataSet.RecordCount - 1 do begin   // SS_1015_CQU_20120528
                for I := 0 to dblRechazados.DataSource.DataSet.RecordCount - 1 do begin     // SS_1015_CQU_20120528
                    nProgreso := nProgreso + 1;
                    sLinea := '';
                    //for J := 0 to dbgRechazados.DataSource.DataSet.FieldCount - 1 do begin    // SS_1015_CQU_20120528
                    for J := 0 to dblRechazados.DataSource.DataSet.FieldCount - 1 do begin      // SS_1015_CQU_20120528
//                        sLinea := sLinea + dbgRechazados.Columns[J].Field.AsString + SEPARADOR_CAMPO;
                        //sLinea := sLinea + dbgRechazados.DataSource.DataSet.Fields[J].AsString + SEPARADOR_CAMPO; // SS_1015_CQU_20120528
                        sLinea := sLinea + dblRechazados.DataSource.DataSet.Fields[J].AsString + SEPARADOR_CAMPO;   // SS_1015_CQU_20120528
                    end;
                    FLista.Add(sLinea);
                    // dbgRechazados.DataSource.DataSet.Next;   // SS_1015_CQU_20120528
                    dblRechazados.DataSource.DataSet.Next;      // SS_1015_CQU_20120528
                    //if nProgreso <= dbgRechazados.DataSource.DataSet.RecordCount then pbProgreso.Position:= nProgreso;    // SS_1015_CQU_20120528
                    if nProgreso <= dblRechazados.DataSource.DataSet.RecordCount then pbProgreso.Position:= nProgreso;      // SS_1015_CQU_20120528
                    Application.ProcessMessages;
                end;
                Result := True;
            end;
            // Fin Bloque SS_1015_CQU_20120517
            //creo el archivo
            FLista.SaveToFile(FNombreArchivo);
            if not Result then
                DescError := MSG_ERROR
        except
            raise Exception.Create(MSG_ERROR);
        end;
    end;
    {-----------------------------------------------------------------------------
	Function Name: GuardarNumeroSecuencia
	Author:    edbaez
	Date Created: 06-Marzo-2012
	Description: Guardo el numero de secuencia
	Parameters: CodigoOperacionInterfase: integer; NumeroSecuencia: integer
	Return Value: true or false
	-----------------------------------------------------------------------------}
	Function GuardarNumeroSecuencia(CodigoOperacionInterfase: integer; NumeroSecuencia: integer): boolean;
	resourcestring
		MSG_ERROR = 'Error al guardar el numero de secuencia: ';
	var
		DescError : AnsiString;
	begin
		try
			//Actualizo el numero de secuencia
			ActualizarNumeroSecuencia(DMConnections.BaseCAC,
										 CodigoOperacionInterfase,
										 NumeroSecuencia,
										 DescError
										);
			Result := True;
		except
			raise Exception.Create(MSG_ERROR + descerror );
		end;
	end;

Resourcestring
    MSG_SAVE_FILE_ERROR = 'No se pudo Guardar el Archivo: ';
    MSG_ERROR = 'Error';
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
var
    DescError : Ansistring;
begin
    Result := False;
    //    if not Assigned(FLista) or (FLista.Count = 0) then Exit; // SS_1015_CQU_20120517
    try

        //Registro la operación en el Log                                              // SS_1006_1015_CQU_20120829
        lblMensaje.Caption := STR_REGISTER_OPERATION;          //Informo Tarea         // SS_1006_1015_CQU_20120829
        Application.ProcessMessages;                           //Refresco la pantalla  // SS_1006_1015_CQU_20120829
        if not RegistrarOperacion then begin                                           // SS_1006_1015_CQU_20120829
            FErrorGrave := True;                                                       // SS_1006_1015_CQU_20120829
            Exit;                                                                      // SS_1006_1015_CQU_20120829
        end;                                                                           // SS_1006_1015_CQU_20120829

        Result :=  (//Grabo el archivo,
                    GuardarArchivoRechazado(DescError) and
                    //Y Guardo el numero de secuencia
                    GuardarNumeroSecuencia(FCodigoOperacion,FNumeroSecuencia)
                   );
        if not Result then begin
            //DeleteFile(FNombreArchivo);   // SS_1015_CQU_20120530
            if (FBorrarArchivo) then        // SS_1015_CQU_20120530
                DeleteFile(FNombreArchivo); // SS_1015_CQU_20120530
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR , DescError, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    except
        on e: Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR , e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: ActualizarLog
  Author:    edbaez
  Date Created: 29-Febrero-2012
  Description: Obtengo la fecha del ultimo proceso de esta interfaz
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFGenerarArchivoTransaccionesRechazadas.ActualizarLog : Boolean;
Resourcestring
     MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
Const
     STR_OBSERVACION = 'Finalizo OK!';
var
     DescError : String;
begin
    try
        Result := ActualizarLogOperacionesInterfaseAlFinal
                  (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);
    except
        on e : Exception do begin
           Result := False;
           MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
        end;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: GuardarHistorico
  Author:    edbaez
  Date Created: 07-Marzo-2012
  Description: Guarda los registros que se guardaron en el archivo que se genero
                con los estacionamientos rechazados
  Parameters: None
  Return Value: True or False
-----------------------------------------------------------------------------}
function TFGenerarArchivoTransaccionesRechazadas.GuardarHistorico : Boolean;
Resourcestring
    MSG_SAVE_FILE_ERROR = 'No se pudo Guardar el Historico: ';
    MSG_ERROR = 'Error';
var
    DescError : Ansistring;
    i : Integer;
    Linea : AnsiString;
    ErrorMsg : String;
    nProgreso : Integer;

    ValorRetorno : smallint;
    Mensaje : string;
begin
    Result := False;
    try
        //inserto cada registro en el historico
        if not spObtenerEstacionamientosRechazados.IsEmpty then
        begin
            //pbProgreso.Max := spObtenerEstacionamientosRechazados.FieldCount; //SS-1015-CQU-20120426
            pbProgreso.Max := spObtenerEstacionamientosRechazados.RecordCount; //SS-1015-CQU-20120426
            pbProgreso.Visible := true; // SS_1015_CQU_20120530
            pnlAvance.BringToFront;    // SS_1015_CQU_20120530
            nProgreso := 0;
            Application.ProcessMessages;
            spObtenerEstacionamientosRechazados.First;
            while not spObtenerEstacionamientosRechazados.EOF and (not FCancelar) do begin
                nProgreso := nProgreso + 1;
                i := 1;

                //aqui llamo al sp que insertara los registro
                spAgregaHistoricoEstacionamientosRechazadosEnviados.Close;
                spAgregaHistoricoEstacionamientosRechazadosEnviados.Connection := DMConnections.BaseCAC;
                spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.Refresh;
                //spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@IDRechazo').Value := spObtenerEstacionamientosRechazados.FieldByName('NumCorrCA').AsInteger; //SS_1006_MCO_20120926                              // SS_1006_1015_CQU_20121005
//                spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@NumCorrCA').Value := spObtenerEstacionamientosRechazados.FieldByName('NumCorrCA').AsInteger; //SS_1006_MCO_20120926
                spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@IDRechazo').Value := spObtenerEstacionamientosRechazados.FieldByName('ID Rechazo').AsInteger;                                                      // SS_1006_1015_CQU_20121005
                // Inicio Bloque SS_1015_CQU_20120528
                //spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@NumCorrOtrasConsecionaria').Value := spObtenerEstacionamientosRechazados.FieldByName('NumCorrOtrasConcesionaria').AsInteger;
                //spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@CodigoConcesionaria').Value := spObtenerEstacionamientosRechazados.FieldByName('CodigoConcesionaria').AsInteger;
                //spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@FechaHoraRechazo').Value := spObtenerEstacionamientosRechazados.FieldByName('FechaHoraRechazo').AsDateTime;
                //spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@DescripcionRechazo').Value := spObtenerEstacionamientosRechazados.FieldByName('DescripcionRechazo').AsString;
                //spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@NombreTabla').Value := spObtenerEstacionamientosRechazados.FieldByName('NombreTabla').AsString;
//                spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@NumCorrOtrasConsecionaria').Value := spObtenerEstacionamientosRechazados.FieldByName('NumCorr Estacionamiento').AsInteger; SS_1006_MCO_20120926
                //spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@IdentificadorTransaccion').Value := spObtenerEstacionamientosRechazados.FieldByName('NumCorr Estacionamiento').AsInteger; //SS_1006_MCO_20120926 // SS_1006_1015_CQU_20121005
                spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@IdentificadorTransaccion').Value := spObtenerEstacionamientosRechazados.FieldByName('Identificador Transaccion').AsInteger;                        // SS_1006_1015_CQU_20121005
                spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@CodigoConcesionaria').Value := spObtenerEstacionamientosRechazados.FieldByName('Código Concesionaria').AsInteger;
                spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@FechaHoraRechazo').Value := spObtenerEstacionamientosRechazados.FieldByName('Fecha del Rechazo').AsDateTime;
                spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@DescripcionRechazo').Value := spObtenerEstacionamientosRechazados.FieldByName('Motivo del Rechazo').AsString;
                spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@NombreTabla').Value := spObtenerEstacionamientosRechazados.FieldByName('Nombre de la Tabla de Registro').AsString;
                // Fin Bloque SS_1015_CQU_20120528
                spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@NombreArchivo').Value := ExtractFileName(FNombreArchivo); //FNombreArchivo; //SS_1006_MCO_20120926
                spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@MensajeError').Value := null;
                spAgregaHistoricoEstacionamientosRechazadosEnviados.ExecProc;
                ValorRetorno := spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@RETURN_VALUE').Value;
                Mensaje := IIF(spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@MensajeError').Value = null, '', spAgregaHistoricoEstacionamientosRechazadosEnviados.Parameters.ParamByName('@MensajeError').Value);

                if ValorRetorno < 0 then
                begin
                    IIf( Mensaje = '', 'Error al Agregar registro a la tabla HistoricoEnvioEstacionamientosRechazados', Mensaje);
                    raise EConvertError.Create(Mensaje);
                end;

                spObtenerEstacionamientosRechazados.Next;

                //Refresco la pantalla
                //if nProgreso <= spObtenerEstacionamientosRechazados.FieldCount then pbProgreso.Position:= nProgreso;//SS-1015-CQU-20120426
                if nProgreso <= spObtenerEstacionamientosRechazados.RecordCount then pbProgreso.Position:= nProgreso;  //Muestro el progreso //SS-1015-CQU-20120426
                Application.ProcessMessages;

            end;
            Result := (not FCancelar);
        end;
    except
        on e: Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR , e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;

    end;

end;

{-----------------------------------------------------------------------------
  Procedure Name    : rdgTipoTransaccionClick
  Author            : Claudio Quezada Ibáñez
  Date Created      : 17-Mayo-2012
  Description       : Obtiene el tipo de transacción ne base al radiobutton seleccionado
  Parameters        : Sender: TObject
  Return Value      : none
  Firma: SS_1015_CQU_20120517
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.rdgTipoTransaccionClick(
  Sender: TObject);
begin
    case rdgTipoTransaccion.ItemIndex of
        0: begin TipoInterface := 'TRA'; end;
        1: begin
                //if (FCodigoConsecionaria > '0') AND (FCodigoConsecionaria = '8') then  // SS_1006_CQU_20120806
                    TipoInterface := 'EST'
                //else begin                                                             // SS_1006_CQU_20120806
                //    MsgBox(MSG_ERROR_CONCESIONARIA_ESTACIONA);                         // SS_1006_CQU_20120806
                //    rdgTipoTransaccion.ItemIndex := -1;                                // SS_1006_CQU_20120806
                //end;                                                                   // SS_1006_CQU_20120806
            end;
        2: begin TipoInterface := 'MOV' end;
        3: begin TipoInterface := 'SALD'; end;
        4: begin TipoInterface := 'CUOT'; end;
    end;

    // Inicio Bloque SS_1006_CQU_20120806
    if ((TipoInterface = 'EST') and not (FCodigoConsecionaria = '8')) OR
        ((FCodigoConsecionaria = '8') and not ((TipoInterface = 'EST') OR (TipoInterface = 'MOV')))
    then begin
        MsgBox(MSG_ERROR_CONCESIONARIA_ESTACIONA);
        rdgTipoTransaccion.ItemIndex := -1;
    end;
    // Fin Bloque SS_1006_CQU_20120806

    if VerificarParametrosGenerales then begin
        CambiarNombreArchivo;
        //if (dbgRechazados.Visible) then begin // SS_1015_CQU_20120528
        //    dbgRechazados.Visible := False;   // SS_1015_CQU_20120528
        if (dblRechazados.Visible) then begin // SS_1015_CQU_20120528
            dblRechazados.Visible := False;   // SS_1015_CQU_20120528
            btnGenerar.Enabled := False;
        end;
    end
    else
        raise Exception.Create(MSG_ERROR_DIRECTORIO);
end;

{-----------------------------------------------------------------------------
  Procedure Name    : btnObtenerClick
  Author            : Claudio Quezada Ibáñez
  Date Created      : 17-Mayo-2012
  Description       : Obtiene los registros de rechazos a ser desplegados
  Parameters        : None
  Return Value      : none
  Firma: SS_1015_CQU_20120517
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoTransaccionesRechazadas.btnObtenerClick(Sender: TObject);

    function ValidarEntrada: Boolean;
    resourcestring
        MSG_ERROR = 'Validación';
    var
        DescError :string;
    begin
        DescError := '';
        Result := True;

        if (Integer(vcbConcesionarias.Value) <= 0) then
            DescError := DescError + 'Debe seleccionar una concesionaria' + CRLF;

        if (edFechaDesde.IsEmpty OR edFechaHasta.IsEmpty) then
            DescError := DescError + 'Debe ingresar una fecha válida' + CRLF
        else if edFechaDesde.Date > edFechaHasta.Date then
            DescError := DescError + 'Fecha Desde debe ser menor o igual a Fecha Hasta' + CRLF;

        if (rdgTipoTransaccion.ItemIndex < 0) then
            DescError := DescError + 'Debe seleccionar un tipo de transacción' + CRLF;

        if DescError <> '' then begin
            MsgBox(DescError, MSG_ERROR, MB_ICONWARNING);
            Result := False;
        end;
    end;

var
    MostrarGrilla : Boolean;
begin
    if (ValidarEntrada) then begin                  
        MostrarGrilla := False;                     // SS_1015_CQU_20120530
        FFechaDesde := edFechaDesde.Date;
        FFechaHasta := edFechaHasta.Date;
        FNombreArchivo := txtArchivo.Text;
        dsFuenteDatosGrilla.DataSet := nil;
        PnlAvance.Visible := MostrarGrilla;         // SS_1015_CQU_20120530
        //dbgRechazados.Visible := MostrarGrilla;   // SS_1015_CQU_20120528
        dblRechazados.Visible := MostrarGrilla;
        try
            // Es Estacionamiento
            if (TipoInterface = 'EST') then begin
  	            if not ObtenerEstacionamientosRechazados then begin
                    FErrorGrave := True;
                    Exit;
                end
                else
                    dsFuenteDatosGrilla.DataSet := spObtenerEstacionamientosRechazados;
            end
            // Es Transaccion o Movimiento
            else if ((TipoInterface = 'MOV') OR (TipoInterface = 'TRA')) then begin
                if not ObtenerRechazos then begin
                    FErrorGrave := True;
                    Exit;
                end
                else
                    dsFuenteDatosGrilla.DataSet := spObtenerTransaccionesMovimientosRechazados;
            end
            // Es Saldo Inicial  o Cuotas
            else if (TipoInterface = 'SALD') or (TipoInterface = 'CUOT') then begin
                if not ObtenerRechazosGenerico(TipoInterface) then begin
                    FErrorGrave := True;
                    Exit;
                end
                else
                    dsFuenteDatosGrilla.DataSet := spRechazosGenerico;
            end;
//            dbgRechazados.DataSource := dsFuenteDatosGrilla; // SS_1015_CQU_20120528
        finally
            // Inicio Bloque SS_1015_CQU_20120528
            {
            if (dbgRechazados.DataSource = nil) then
                MostrarGrilla := False
            else if (dbgRechazados.DataSource.DataSet = nil) then
                MostrarGrilla := False
            else if (dbgRechazados.DataSource.DataSet.RecordCount <= 0) then
                MostrarGrilla := False;
            }
            MostrarGrilla := DesplegarDatos(dsFuenteDatosGrilla);
            //PnlAvance.Visible := not MostrarGrilla;
            //PnlAvance.Visible := not FErrorGrave;   // SS_1015_CQU_20120530
            // Fin Bloque SS_1015_CQU_20120528
            btnGenerar.Enabled := MostrarGrilla;
            //dbgRechazados.Visible := MostrarGrilla; // SS_1015_CQU_20120528
            dblRechazados.Visible := MostrarGrilla;   // SS_1015_CQU_20120528
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : ObtenerRechazosGenerico
  Author        : CQuezada
  Date Created  : 17-Mayo-2012
  Description   : Obtiene los registros rechazados de Otras Concesionarias
                    - Saldo Inicial
                    - Cuotas
                    -
  Parameters    : None
  Return Value  : True or False

  Firma: SS_1015_CQU_20120517
-----------------------------------------------------------------------------}
function TFGenerarArchivoTransaccionesRechazadas.ObtenerRechazosGenerico(sTipoInterface : String): Boolean;
Resourcestring
    MSG_COULD_NOT_OBTAIN_ESTACIONAMIENTOS_RECHAZADOS = 'No se pudieron obtener rechazos';
    //MSG_EMPTY_QUERY_ERROR = 'No hay registros rechazados para informar!'; // SS_1006_1015_CQU_20120928
    MSG_ERROR = 'Error';
var
    ErrorMsg : String;
begin
    Result := False;
    FCancelar := False;
    //Obtengo los estacionamientos rechazados
    try
        spRechazosGenerico.Close;
        spRechazosGenerico.Connection := DMConnections.BaseCAC;
        if (sTipoInterface = 'TRA') OR (sTipoInterface = 'MOV') then begin
            spRechazosGenerico.ProcedureName := 'ObtenerTransaccionesMovimientosRechazados';
            spRechazosGenerico.Parameters.Refresh;
            spRechazosGenerico.Parameters.ParamByName('@FechaDesde').Value := FFechaDesde;
            spRechazosGenerico.Parameters.ParamByName('@FechaHasta').Value := FFechaHasta;
            spRechazosGenerico.Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConsecionaria;
            spRechazosGenerico.Parameters.ParamByName('@TipoTransaccion').Value := sTipoInterface;
        end
        else if sTipoInterface = 'EST' then begin
            spRechazosGenerico.ProcedureName := 'ObtenerEstacionamientosRechazados';
            spRechazosGenerico.Parameters.Refresh;
            spRechazosGenerico.Parameters.ParamByName('@FechaDesde').Value := FFechaDesde;
            spRechazosGenerico.Parameters.ParamByName('@FechaHasta').Value := FFechaHasta;
        end
        else if (sTipoInterface = 'SALD') OR (sTipoInterface = 'CUOT') then begin
            spRechazosGenerico.ProcedureName := 'ObtenerRechazadosGenerico';
            spRechazosGenerico.Parameters.Refresh;
            spRechazosGenerico.Parameters.ParamByName('@FechaDesde').Value := FFechaDesde;
            spRechazosGenerico.Parameters.ParamByName('@FechaHasta').Value := FFechaHasta;
            spRechazosGenerico.Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConsecionaria;
            spRechazosGenerico.Parameters.ParamByName('@TipoTransaccion').Value := sTipoInterface;
        end;

        spRechazosGenerico.CommandTimeout := 500;
        spRechazosGenerico.Open;

        if not spRechazosGenerico.IsEmpty then
            Result := (not FCancelar)
        else
            //MsgBoxErr(MSG_COULD_NOT_OBTAIN_ESTACIONAMIENTOS_RECHAZADOS                // SS_1006_1015_CQU_20120928
            //, MSG_EMPTY_QUERY_ERROR, MSG_ERROR, MB_ICONERROR);                        // SS_1006_1015_CQU_20120928
            MsgBox(MSG_EMPTY_QUERY_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);    // SS_1006_1015_CQU_20120928
    except
        on e: Exception do begin
            ErrorMsg := MSG_COULD_NOT_OBTAIN_ESTACIONAMIENTOS_RECHAZADOS;
            MsgBoxErr(ErrorMsg , e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;      
end;
{-----------------------------------------------------------------------------
  Function Name : DesplegarDatos
  Author        : CQuezada
  Date Created  : 28-Mayo-2012
  Description   : Carga en el nuevo objeto TDBListEx los datos recuperados

  Parameters    : dsFuenteDatos : TDataSource
  Return Value  : True or False

  Firma: SS_1015_CQU_20120528
-----------------------------------------------------------------------------}
function TFGenerarArchivoTransaccionesRechazadas.DesplegarDatos(dsFuenteDatos : TDataSource) : Boolean;
    {-----------------------------------------------------------------------------
      Function Name : ColocarCabeceras
      Author        : CQuezada
      Date Created  : 28-Mayo-2012
      Description   : Coloca las Cabeceras en el TDBListEx
                      de acuerdo a lo obtenido en el DataSource

      Parameters    : None
      Return Value  : True or False

      Firma: SS_1015_CQU_20120528
    -----------------------------------------------------------------------------}
    function ColocarCabeceras : Boolean;
    var
        Campo : Integer;
        Alineacion : TAlignment; // SS_1006_1015_CQU_20120928
    begin
        try
            // Limpio la grilla (TDBListEx), lo hago por cabeceras ya que estas varían según los datos solicitados
            if dblRechazados.Columns.Count > 0 then begin
                dblRechazados.Columns.Clear;
                dblRechazados.DataSource := nil;
            end;
            // Cargo las Cabeceras en la grilla (TDBListEx)
            with dsFuenteDatos do begin
                for Campo := 0 to DataSet.FieldCount - 1 do begin
                    dblRechazados.Columns.Add;
                    dblRechazados.Columns.Items[Campo].FieldName := DataSet.Fields[Campo].FieldName;
                    dblRechazados.Columns.Items[Campo].Header.Caption := DataSet.Fields[Campo].FieldName;

                    dblRechazados.Columns.Items[Campo].Header.Alignment := taCenter;                        // SS_1006_1015_CQU_20120928
                    dblRechazados.Columns.Items[Campo].MinWidth := 80;                                      // SS_1006_1015_CQU_20120928
                    dblRechazados.Columns.Items[Campo].Width := 100;                                        // SS_1006_1015_CQU_20120928

                    //if DataSet.Fields[Campo] is TIntegerField then begin                                  // SS_1006_1015_CQU_20120928
                    //    dblRechazados.Columns.Items[Campo].Alignment := taRightJustify;                   // SS_1006_1015_CQU_20120928
                    //    dblRechazados.Columns.Items[Campo].Header.Alignment := taRightJustify;            // SS_1006_1015_CQU_20120928
                    //end                                                                                   // SS_1006_1015_CQU_20120928
                    //else begin                                                                            // SS_1006_1015_CQU_20120928
                    //    dblRechazados.Columns.Items[Campo].Alignment := taLeftJustify;                    // SS_1006_1015_CQU_20120928
                    //    dblRechazados.Columns.Items[Campo].Header.Alignment := taLeftJustify;             // SS_1006_1015_CQU_20120928
                    //end;                                                                                  // SS_1006_1015_CQU_20120928

                    if      DataSet.Fields[Campo] is TNumericField  then Alineacion := taRightJustify       // SS_1006_1015_CQU_20120928
                    else if DataSet.Fields[Campo] is TDateTimeField then Alineacion := taCenter             // SS_1006_1015_CQU_20120928
                    else Alineacion := taLeftJustify;                                                       // SS_1006_1015_CQU_20120928

                    dblRechazados.Columns.Items[Campo].Alignment := Alineacion;                             // SS_1006_1015_CQU_20120928
                end;
                Result := True;
            end;
        except
            Result := False;
        end;
    end;

begin
    try
        Result := False;
        if not (dsFuenteDatos = nil) then begin
            if not (dsFuenteDatos.DataSet = nil) then begin
                if (dsFuenteDatos.DataSet.RecordCount > 0) then begin
                    if ColocarCabeceras then begin
                        dblRechazados.DataSource := dsFuenteDatos;
                        Result := True;
                    end;
                end;
            end;
        end;
    except
        Result := False;
    end;
end;

initialization
    FLista   := TStringList.Create;
finalization
    FreeAndNil(Flista);

end.
