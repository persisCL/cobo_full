{-------------------------------------------------------------------------------
File Name  : RendicionPAC.pas
Author   : ndonadio
Date Created:  03/12/2004
Language  : ES-AR
Description : M�dulo de la interface Santanter - Recepci�n de Rendiciones
--------------------------------------------------------------------------------
Revision History
--------------------------------------------------------------------------------
Author: rcastro
Date Created: 17/12/2004
Description: revisi�n general



Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-------------------------------------------------------------------------------}
unit RendicionPAC;

interface

uses
 //Recepcion de rendiciones del Santander
 DMConnection,
 Util,
 Utildb,
 UtilProc,
 StrUtils,
 ConstParametrosGenerales,
 PeaProcs,
 ComunesInterfaces,
 FrmRptRecepcionRendiciones,
 //General
 Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
 Dialogs, StdCtrls, DB, ADODB, ComCtrls, ExtCtrls, Buttons, PeaTypes,
 {RptErroresRendicion,} DPSControls,  ppParameter,
 ppCtrls, ppBands, ppReport, ppPrnabl, ppClass, ppStrtch, ppSubRpt,
 ppCache, ppProd, UtilRB, ppDB, ppComm, ppRelatv, ppTxPipe, RBSetup;


type

  //Registro de Rendici�n
  RecordRendicionPAC = record
    NumeroComprobante: int64;
    EstadoDebito: smallint;             //CodigoEstadoDebito despues de Validar
    TotalAPagar: int64;
    CodigoTipoMedioPago: string;
    CodigoBancoSBEI: smallint;
    PAC_CodigoBanco: integer;           //Solo despues de validar;
    NroCuentaBancaria: string;
    PAC_CodigoTipoCuentaBancaria: word; //Solo despues de validar;
    MontoFijo: int64;                   //TotalAPagar
    RutSubscriptor: string;             //NumeroDocumento NO validado!
    PAC_Sucursal: string;               //Sucursal tomada del Comprobante!
    NumeroConvenio: string;             // Ya Validado
    FechaDebito: TDateTime;
    FechaDebitoFormateada: TDateTime;   //Solo despues de validar;
    DescripcionErrorValidacion: string; //Solo despues de validar;
    CodigoConceptoPago: integer;
  end;


  TfrmRendicionPAC = class(TForm)
    btnAbrirArchivo: TSpeedButton;
    Label2: TLabel;
    edOrigen: TEdit;
    OpenDialog: TOpenDialog;
    spProcesarRendicion: TADOStoredProc;
    spRegistrarOperacion: TADOStoredProc;
    spActualizaObservacionesLOG: TADOStoredProc;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    Bevel1: TBevel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    SpObtenerResumenComprobantesRecibidos: TADOStoredProc;
    SpObtenerResumenComprobantesRecibidosMotivo: TStringField;
    SpObtenerResumenComprobantesRecibidosCantidad: TIntegerField;
    SpObtenerResumenComprobantesRecibidosMonto: TStringField;
    SPAgregarRendicionPAC: TADOStoredProc;
    procedure btnSalirClick(Sender: TObject);
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    private
    { Private declarations }
      FCodigoOperacion: integer;
      FCancelar: Boolean;
      iTotalLines: Integer;   //Total de lineas en el archivo
      iInvalidLines: Integer; //Lineas Con Debitos Invalidos
      FErrores: TStringList;
      txtRendicionesPAC: TStringList;
      FSantander_Directorio_Rendiciones: AnsiString;
      FSantander_Directorio_Errores : AnsiString;
      FSTD_DirectorioRendicionesProcesadas: AnsiString;
      FLineasArchivo: integer;
      FMontoArchivo: int64;
      FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
      function  ParseLine(sLine: string; var RecRendiPAC: recordRendicionPAC; NroLinea:integer): String;
      function  AnalizarRendicionesTXT : boolean;
      function  RegistrarOperacion(var CodigoOperacion: integer): boolean;
      Function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
    public
    { Public declarations }
      function Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
  end;

var
  frmRendicionPAC: TfrmRendicionPAC;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
 Function Name: Inicializar
 Author:  gcasais
 Date Created: 03/12/2004
 Description: Inicializaci�n de este formulario
 Parameters: None
 Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmRendicionPAC.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;


    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 25/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        SANTANDER_DIRECTORIO_RENDICIONES    = 'Santander_Directorio_Rendiciones';
        SANTANDER_DIRECTORIO_ERRORES        = 'Santander_Directorio_Errores';
        STD_DIRECTORIORENDICIONESPROCESADAS = 'STD_DirectorioRendicionesProcesadas';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, SANTANDER_DIRECTORIO_RENDICIONES , FSantander_Directorio_Rendiciones) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SANTANDER_DIRECTORIO_RENDICIONES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FSantander_Directorio_Rendiciones := GoodDir(FSantander_Directorio_Rendiciones);
                if  not DirectoryExists(FSantander_Directorio_Rendiciones) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FSantander_Directorio_Rendiciones;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, SANTANDER_DIRECTORIO_ERRORES , FSantander_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SANTANDER_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FSantander_Directorio_Errores := GoodDir(FSantander_Directorio_Errores);
                if  not DirectoryExists(FSantander_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FSantander_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC, STD_DIRECTORIORENDICIONESPROCESADAS, FSTD_DirectorioRendicionesProcesadas);

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_ERROR_INIT = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Elijo el modo en que si visualizara la ventana
  	if not MDIChild then begin
  		FormStyle := fsNormal;
  		Visible := False;
  	end;
    //centro el formulario
    CenterForm(Self);
    try
        Result := DMConnections.BaseCAC.Connected and
                          VerificarParametrosGenerales;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Caption := AnsiReplaceStr(txtCaption, '&', '');
   	pnlAvance.Visible := False;
	  lblReferencia.Caption := '';
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRendicionPAC.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_RENDICION          = ' ' + CRLF +
                          'El Archivo de Respuesta a Debitos' + CRLF +
                          'es enviado por SANTANDER al ESTABLECIMIENTO' + CRLF +
                          'por cada Archivo de Debitos recibido' + CRLF +
                          'y tiene por objeto dar a conocer al Establecimiento, ' + CRLF +
                          'el resultado del procesamiento de la informaci�n enviada.' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: Rendiciones Santander YYYY-MM-DD.asc' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para registrar los pagos de los' + CRLF +
                          'comprobantes aceptados en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.Visible = True then Exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_RENDICION, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TfrmRendicionPAC.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
 Function Name: btnAbrirArchivoClick
 Author:  ndonadio
 Date Created: 07/12/2004
 Description: Busca el Archivo del Universo.TXT
 Parameters: Sender: TObject
 Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmRendicionPAC.btnAbrirArchivoClick(Sender: TObject);
resourcestring
  	MSG_FILE_ALREADY_PROCESSED 			  = 'El archivo %s ya fue procesado';
    MSG_FILE_WISH_TO_REPROCESS			  = '�Desea volver a procesarlo?';
  	ERROR_RENDERINGS_FILE_NOT_EXISTS 	= 'El archivo de rendiciones %s no existe';
    MSG_ERROR = 'Error';
begin
    //Define un directorio de origen
    opendialog.InitialDir:=FSantander_Directorio_Rendiciones;
    //abro el dialgo de busqueda
  	if opendialog.execute then begin
        //obtengo el archivo elegido
    		edOrigen.text:= opendialog.filename;
        //si existe el arhivo
        if FileExists( edOrigen.text ) then begin
            //permito procesar
            btnProcesar.Enabled := True;
        end else begin
            //no permito procesar
            btnProcesar.Enabled := False;
            //Muestro Cartel de Errores
            Exception.Create( Format ( ERROR_RENDERINGS_FILE_NOT_EXISTS, [edOrigen.text]));
        end;
        //Verifica que el nombre del archivo procesado sea correcto
        if  VerificarArchivoProcesado( DMConnections.BaseCAC, edOrigen.text ) then begin
			  if (MsgBox (Format (MSG_FILE_ALREADY_PROCESSED, [ExtractFileName (edOrigen.text) ]) + #13#10 + MSG_FILE_WISH_TO_REPROCESS, MSG_ERROR, MB_ICONQUESTION or MB_YESNO) <> IDYES) then
            Exit;
        end;
        if FSTD_DirectorioRendicionesProcesadas <> '' then begin
            if RightStr(FSTD_DirectorioRendicionesProcesadas,1) = '\' then  FSTD_DirectorioRendicionesProcesadas := FSTD_DirectorioRendicionesProcesadas + ExtractFileName(edOrigen.text)
            else FSTD_DirectorioRendicionesProcesadas := FSTD_DirectorioRendicionesProcesadas + '\' + ExtractFileName(edOrigen.text);
        end;
    end;
end;

{-----------------------------------------------------------------------------
 Function Name: ParseLine
 Author:  ndonadio
 Date Created: 09/12/2004
 Description: Parsea la linea de entrada y almacena los datos en un registro
 Parameters:
 Return Value:
-----------------------------------------------------------------------------}
Function TfrmRendicionPAC.ParseLine(sLine: string; var RecRendiPAC: recordRendicionPAC; NroLinea:Integer): String;
resourcestring
    ERROR_PARSING          =  'Error de Parseo...';
    ERROR_SAVING_LINE      =  'Error al Asentar la Operaci�n en Linea: %d. ';
    ERROR_CRITICAL         =  'Valor inv�lido: ';
    STR_BANK_CODE          =  'C�digo de Banco';
    STR_ACCOUNT_NUMBER     =  'Nro. de Cuenta';
    STR_AMOUNT             =  'Importe del D�bito';
    STR_SUBSCRIPTOR_RUT    =  'RUT del Subscriptor';
    STR_AGREEMENT_NUMBER   =  'N�mero de Convenio';
    STR_DEBIT_STATE        =  'Estado del D�bito';
    STR_INVOICE_NUMBER     =  'Numero de Comprobante';
    STR_DEBIT_DATE         =  'Fecha de D�bito';
Var
  	Rta    : string;
    sFecha : string;
    Flag : byte;
Begin
    Rta := ERROR_PARSING;
    try
    	With RecRendiPAC do begin
            CodigoBancoSBEI := 0;
            NroCuentaBancaria := '';
            MontoFijo := 0;
            RUTSubscriptor := '';
            NumeroConvenio := '';
            EstadoDebito := 0;
            NumeroComprobante := 0;
            FechaDebito := nulldate;
            CodigoTipoMedioPago := '';
            PAC_CodigoBanco:=0;
            PAC_CodigoTipoCuentaBancaria := 0;
            PAC_Sucursal := '';
            FechaDebitoFormateada := Now;
            DescripcionErrorValidacion := '';
            TotalAPagar := 0;
        end;
        Flag := 0;
        try
            With RecRendiPAC  do begin
                inc(Flag);
                CodigoBancoSBEI := strToInt(trim(copy(sLine, 1, 3)));
                inc(Flag);
                NroCuentaBancaria := trim(copy(sLine, 34, 17));
                inc(Flag);
                MontoFijo := strtoint64(trim(copy(sLine, 51, 8)));
                inc(Flag);
                RUTSubscriptor := trim(copy(sLine, 61, 15));
                inc(Flag);
                NumeroConvenio := '001' + trim(copy(sLine, 82, 14));
                //if ObtenerCodigoConcesionariaNativa=CODIGO_VS then              //SS_1147Q_NDR_20141202[??]
                //begin                                                           //SS_1147Q_NDR_20141202[??]
                //  NumeroConvenio:=Copy(NumeroConvenio, 6, 12);                  //SS_1147Q_NDR_20141202[??]
                //end;                                                            //SS_1147Q_NDR_20141202[??]
                inc(Flag);
                EstadoDebito := strtoint(trim(copy(sLine, 96, 2)));
                inc(Flag);
                NumeroComprobante := strtoint(trim(copy(sLine, 118, 8)));
                inc(Flag);
    		      	sFecha := trim(copy(sLine, 126, 8));
			        	if ( sFecha = '00000000' ) then FechaDebito := nulldate else FechaDebito := EncodeDate(	StrToInt(Copy(sFecha, 5, 4)), StrToInt(Copy(sFecha, 3, 2)), StrToInt(Copy(sFecha, 1, 2)));
            end;
            Rta := '';
        except
            on e : Exception do begin
                Rta := ERROR_CRITICAL;
                Case Flag of
                    1: Rta := Rta + STR_BANK_CODE  ;
                    2: Rta := Rta + STR_ACCOUNT_NUMBER  ;
                    3: Rta := Rta + STR_AMOUNT  ;
                    4: Rta := Rta + STR_SUBSCRIPTOR_RUT  ;
                    5: Rta := Rta + STR_AGREEMENT_NUMBER  ;
                    6: Rta := Rta + STR_DEBIT_STATE  ;
                    7: Rta := Rta + STR_INVOICE_NUMBER  ;
                    8: Rta := Rta + STR_DEBIT_DATE  ;
                    else
                        Rta := e.Message;
                end;
		    end;
        end;
    finally
	    ParseLine := Rta;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: AnalizarRendicionesTXT
  Author:    flamas
  Date Created: 14/01/2005
  Description: Hace el Parseo para verificar que el archivo sea v�lido
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfrmRendicionPAC.AnalizarRendicionesTXT : boolean;
resourcestring
	  MSG_REF_ANALYZING_LINE = 'Analizando linea %d';
	  MSG_ERROR_LINE         = 'Error de formato del Archivo - L�nea: %d '#10#13;
var
    rRPAC: recordrendicionPAC;
    ParseResult : string;
	  NroLineaProc: integer;
begin
	result := True;
    FMontoArchivo := 0;
    pbProgreso.Max := txtRendicionesPAC.Count;
	pbProgreso.Position := 0;
	NroLineaProc := 0;
	While ( NroLineaProc < txtRendicionesPAC.Count) and not FCancelar do Begin
      Application.ProcessMessages;
      pbProgreso.StepIt;
      lblReferencia.Caption := Format(MSG_REF_ANALYZING_LINE, [NroLineaProc+1]);
      ParseResult := ParseLine(txtRendicionesPAC.strings[NroLineaProc], rRPAC, NroLineaProc+1);
      if not (ParseResult = '') then begin
          ShowMessage( Format( MSG_ERROR_LINE + ParseResult,[NroLineaProc] ));
          result := False;
          Break;
      end
      else FMontoArchivo := FMontoArchivo + rRPAC.MontoFijo;
      Inc(NroLineaProc);
  end;
  FLineasArchivo := NroLineaProc;
end;

{-----------------------------------------------------------------------------
 Function Name: RegistrarOperacion
 Author:  ndonadio
 Date Created: 13/12/2004
 Description: Registra la operacion en el log de interfaces
 Parameters: CodigoOperacion: Integer
 Return Value: integer
-----------------------------------------------------------------------------}
function TfrmRendicionPAC.RegistrarOperacion(var CodigoOperacion: integer):boolean;
resourcestring
	MSG_RESULT = '( %d / %d )';
  MSG_INIT = 'Proceso Iniciado ( %s ) - ';
const
  RO_MOD_INTERFAZ_RENDICIONES_PAC = 26;
var
    sDescrip : string;
    rta: boolean;
    CodOperacion: integer;
    DescError: string;
begin
    rta := False;
    if CodigoOperacion = 0 then begin
        sDescrip := Format( MSG_INIT, [DateToStr(now)]);
        rta := RegistrarOperacionEnLogInterface(dmconnections.BaseCAC, RO_MOD_INTERFAZ_RENDICIONES_PAC, ExtractFileName(edOrigen.text), UsuarioSistema, sDescrip, True, false, NowBase(DMConnections.BaseCAC), 0, CodOperacion, FLineasArchivo, FMontoArchivo, DescError);
        CodigoOperacion := CodOperacion;
    end
    else begin
        sDescrip := Format( MSG_RESULT, [iTotalLines, iInvalidLines]);
        ActualizarObservacionesEnLogInterface(DMCOnnections.BaseCAC , spActualizaObservacionesLOG , CodigoOperacion, sDescrip, True);
    end;
    RegistrarOperacion := rta;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Genero el Reporte de Finalizacion de Proceso. sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  TfrmRendicionPAC.GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
Const
    STR_TITLE = 'Comprobantes Recibidos';
var
    FRecepcion : TFRptRecepcionRendiciones;
begin
    Result:=false;
    try
        //muestro el reporte
        Application.CreateForm(TFRptRecepcionRendiciones, FRecepcion);
        if not fRecepcion.Inicializar(STR_TITLE, CodigoOperacionInterfase) then fRecepcion.Release;
        Result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
 Function Name: btnProcesarClick
 Author:  ndonadio
 Date Created: 09/12/2004
 Description: Bucle central del proceso de Importacion
 Parameters: Sender: TObject
 Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  14/03/07
  ahora al llamar a registrar el pago informo si se trata de un reintento
-----------------------------------------------------------------------------}
procedure TfrmRendicionPAC.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/06/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name:  ObtenerResumen
      Author:    lgisuk
      Date Created: 19/12/2005
      Description:  Obtengo el resumen del proceso realizado
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    procedure ObtenerResumen (CodigoOperacionInterfase : Integer;Var CantRegs, Aprobados, Rechazados : Integer);
    resourcestring
      MSG_ERROR = 'Error';
      MSG_ERROR_QUERY = 'Error Obteniendo Resumen';
    begin
      try
          With spObtenerResumenComprobantesRecibidos, Parameters do begin
              Close;
              ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
              CommandTimeout := 500;
              Open;
              CantRegs := Parameters.ParamByName('@CantidadComprobantesRecibidos').Value;
              Aprobados := Parameters.ParamByName('@CantidadComprobantesAceptados').Value;
              Rechazados := Parameters.ParamByName('@CantidadComprobantesRechazados').Value;
              Close;
          end;
      except
          on E : Exception do begin
              MsgBoxErr(MSG_ERROR_QUERY, E.Message, MSG_ERROR, MB_ICONERROR);
          end;
      end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 01/07/2005
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog(CantidadErrores:integer):boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : string;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on e : Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;


resourcestring
    MSG_ERROR_FILE_NOT_EXISTS	                 = 'El archivo %s no existe';
    MSG_ERROR_CANNOT_OPEN_FILE 	               = 'No se puede abrir el archivo %s';
    MSG_ERROR_LINE                             = 'Error al Importar L�nea: %d - ';
    MSG_ERROR_IMPORTING_LINE                   = 'Error al Importar L�nea: %d - Comprobante %d , ';
    MSG_ERROR_INVALID_DATE                     = 'Error al Importar L�nea: %d - Fecha Invalida. Reemplazada por fecha actual ';
    MSG_ERROR_VALIDATION                       = 'Error de Validaci�n L�nea: %d - Comprobante %d , ';
    MSG_ERROR_SAVING_INVOICE                   = 'Error al Asentar la Operaci�n Comprobante: %d  ';
    MSG_PROCESSING_FILE 		                   = 'Procesando Archivo de Rendiciones - Cantidad de lineas : %d';
    MSG_PROCESSING				                     = 'Procesando...';
    MSG_PROCESS_ENDS_OK                        = 'El proceso finaliz� con �xito';
    MSG_PROCESS_ENDS_WITH_ERRORS               = 'El proceso finaliz� con errores';
    MSG_PROCESS_CANNOT_COMPLETE                = 'El proceso no se pudo completar';
    MSG_PROCESS_USER_CANCELED                  = 'El Proceso ha sido Cancelado por el Usuario';
    MSG_REF_STARTING                           = 'Iniciando el proceso de importaci�n del archivo %s';
    MSG_REF_ANALYZING_LINE                     = 'Analizando linea %d';
    MSG_REF_VALIDATING                         = 'Validando valores importados';
    MSG_REF_REGISTERING_PAYMENT                = 'Registrando Informaci�n del Pago...';
    MSG_REF_REGISTERING_ERROR                  = 'Registrando Informaci�n de Error ';
const
	  CONST_TIPO_COMPROBANTE_DEBITO = 'NK';
var
    rRPAC: recordrendicionPAC;
    NroLineaProc: integer;
  	sDescripcionError: String;
    FErrorGrave : boolean;
    CantidadErrores : integer;
    CantRegs, Aprobados, Rechazados : Integer;
    //
    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
begin
    FErrores := TStringList.Create;

    FErrores.Clear;

    btnProcesar.Enabled := False;
    btnCancelar.Enabled := True;
    pnlAvance.Visible := True;
    try
        lblReferencia.Caption := Format(MSG_REF_STARTING, [edOrigen.text]);
        Screen.Cursor := crHourglass;

		//Lee el archivo de Rendiciones ...
		try
        txtRendicionesPAC := TStringList.Create;
        txtRendicionesPAC.LoadFromFile(edOrigen.text);
		except
        on E : Exception do begin
            MsgBoxErr(Format(MSG_ERROR_CANNOT_OPEN_FILE, [edOrigen.text]), e.Message, 'Error', MB_ICONERROR);
            Screen.Cursor := crDefault;
            Exit;
        end;
		end;
    
		NroLineaProc := 0;
		FCancelar := False;
    FCodigoOperacion := 0;
		pbProgreso.Max := txtRendicionesPAC.Count;
		pbProgreso.Position := 0;
		iTotalLines:=txtRendicionesPAC.Count;
		iInvalidLines := 0;
		pbProgreso.Step := 1;
    FErrorGrave := False;

    // Hace el Parseo y si el Parseo funciona ok, lo procesa
		if AnalizarRendicionesTXT then begin

            //Registro la operaci�n en el log
            RegistrarOperacion(FCodigoOperacion);

            //Realiza la operacion de registrar los pagos
            While ( NroLineaProc < txtRendicionesPAC.Count) and not FCancelar do Begin
                Application.ProcessMessages;
                pbProgreso.StepIt;
                lblReferencia.Caption := Format(MSG_REF_ANALYZING_LINE, [NroLineaProc+1]);

                ParseLine(txtRendicionesPAC.strings[NroLineaProc], rRPAC, NroLineaProc+1);

                lblReferencia.Caption := MSG_REF_REGISTERING_PAYMENT;

                // Intentamos registrar el pago de un comprobante.
                Reintentos  := 0;
                while Reintentos < 3 do begin

                    // Registra el pago del comprobante
                    with spAgregarRendicionPAC, Parameters do begin

                      ParamByName('@CodigoOperacionInterfase').Value	:= FCodigoOperacion;
                      ParamByName('@CodigoBancoSBEI').Value  			:= rrpac.CodigoBancoSBEI;
                      ParamByName('@NroCuentaBancaria').Value 		:= trim(rrpac.NroCuentaBancaria);
                      ParamByName('@Monto').Value     				:= rrpac.MontoFijo;
                      ParamByName('@RUTSubscriptor').Value   			:= trim(rrpac.RutSubscriptor);
                      ParamByName('@NumeroConvenio').Value   			:= trim(rrpac.NumeroConvenio);
                      ParamByName('@EstadoDebito').Value    			:= rrpac.EstadoDebito;
                      ParamByName('@NumeroComprobante').Value 		:= rrpac.NumeroComprobante;
                      ParamByName('@FechaDebito').Value    			:= rrpac.FechaDebito;
                      ParamByName('@Usuario').Value    				:= UsuarioSistema;
                      ParamByName('@Reintento').Value    				:= Reintentos;

                      try
                          CommandTimeOut := 500;
                          ExecProc;

                          sDescripcionError := Trim( VarToStr( ParamByName( '@DescripcionError' ).Value ));
                          if ( sDescripcionError <> '' ) then FErrores.Add( StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]));
                          //1/4 de segundo entre registracion de pago y otro
                          sleep(25);
                          //Salgo del ciclo por Ok
                          Break;

                      except
                          on E : Exception do begin
                                if (pos('deadlock', e.Message) > 0) then begin
                                    mensajeDeadlock := e.message;
                                    inc(Reintentos);
                                    sleep(2000);
                                end else begin
                                    MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [rrpac.NumeroComprobante]), e.Message, caption, MB_ICONSTOP);
                                    FErrorGrave := True;
                                    Break;
                                end;
                          end;
                      end;
                    end;

                end;

                //en caso de excepcion de no continuo con proximo registro
                if FErrorGrave then Break;

                //una vez que se reintento 3 veces x deadlock lo informo
                if Reintentos = 3 then begin
                    MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [rrpac.NumeroComprobante]), mensajeDeadLock, caption, MB_ICONSTOP);
                    FErrorGrave := True;
                    Break;
                end;

                INC(NroLineaProc);
            end;
            Screen.Cursor := crDefault;

            lblReferencia.Caption := '';
            pbProgreso.Position := 0;

            //si no se cancelo ni hubo errores graves
            if ( not FCancelar ) and ( not FErrorGrave ) then begin
                    //Obtengo la cantidad de errores
                    ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);
                    //Actualizo el log al Final
                    ActualizarLog(CantidadErrores);
                    //Obtengo resumen
                    ObtenerResumen (FCodigoOperacion, CantRegs, Aprobados, Rechazados);
                    //Muestro el cartel de finalizacion
                    if MsgProcesoFinalizado( MSG_PROCESS_ENDS_OK, MSG_PROCESS_ENDS_WITH_ERRORS, Caption, CantRegs, Aprobados, Rechazados) then begin
                        //Muestro el Reporte de Finalizacion del Proceso
                        GenerarReportedeFinalizacion(FCodigoOperacion);
                    end;
                    //Muevo el archivo procesado a otro directorio
                    MoverArchivoProcesado(Caption,edOrigen.Text, FSTD_DirectorioRendicionesProcesadas);
            end
            else if ( FCancelar ) then begin
                    //Muestro el cartel que fue cancelado por el usuario
                    MsgBox(MSG_PROCESS_USER_CANCELED);
            end else begin
                    //Muestro un cartel diciendo que la operacion no se pudo completar porque
                    //hubo errores
                    MsgBox(MSG_PROCESS_CANNOT_COMPLETE);
            end;
        end;
	finally
      pnlAvance.Visible := False;
      pbProgreso.Position := 0;
      lblReferencia.Caption := '';
      Screen.Cursor := crDefault;
      FreeAndNil( txtRendicionesPAC );
      FreeAndNil(FErrores);
      btnProcesar.Enabled := False;
      btnCancelar.Enabled := False;
      FCancelar := False;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Permite cancelar el proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmRendicionPAC.btnCancelarClick(Sender: TObject);
begin
    FCancelar := True;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmRendicionPAC.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Lo libero de Memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmRendicionPAC.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;


end.
