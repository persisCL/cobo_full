object FormABMTiposCorrespondencia: TFormABMTiposCorrespondencia
  Left = 0
  Top = 0
  Caption = 'Mantenimiento de Tipos de Correspondencia'
  ClientHeight = 392
  ClientWidth = 572
  Color = clBtnFace
  Constraints.MinHeight = 390
  Constraints.MinWidth = 500
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 572
    Height = 33
    Habilitados = [btAlta, btModi, btSalir, btBuscar]
    OnClose = AbmToolbarClose
  end
  object dblTipoCorrespondencia: TAbmList
    Left = 0
    Top = 33
    Width = 572
    Height = 199
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'47'#0'C'#243'digo  '
      
        #0'325'#0'Descripci'#243'n                                                ' +
        '                                       '
      #0'56'#0'Plantilla    '
      #0'45'#0'Firma    ')
    HScrollBar = True
    RefreshTime = 100
    Table = EMAIL_TipoCorrespondencia
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dblTipoCorrespondenciaClick
    OnDrawItem = dblTipoCorrespondenciaDrawItem
    OnRefresh = dblTipoCorrespondenciaRefresh
    OnInsert = dblTipoCorrespondenciaInsert
    OnDelete = dblTipoCorrespondenciaDelete
    OnEdit = dblTipoCorrespondenciaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object GroupB: TPanel
    Left = 0
    Top = 232
    Width = 572
    Height = 121
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    BiDiMode = bdLeftToRight
    Enabled = False
    ParentBiDiMode = False
    TabOrder = 2
    object lblDescripcion: TLabel
      Left = 31
      Top = 38
      Width = 59
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txtDescripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblCodigoDocumentacionRespaldo: TLabel
      Left = 54
      Top = 11
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = txtCodigoTipoCorrespondencia
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblFirma: TLabel
      Left = 62
      Top = 92
      Width = 28
      Height = 13
      Caption = '&Firma:'
      FocusControl = vcbFirma
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblPlantilla: TLabel
      Left = 51
      Top = 65
      Width = 39
      Height = 13
      Caption = '&Plantilla:'
      FocusControl = vcbPlantilla
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtDescripcion: TEdit
      Left = 98
      Top = 35
      Width = 289
      Height = 21
      MaxLength = 40
      TabOrder = 1
    end
    object txtCodigoTipoCorrespondencia: TNumericEdit
      Left = 98
      Top = 8
      Width = 69
      Height = 21
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
    end
    object vcbFirma: TVariantComboBox
      Left = 98
      Top = 89
      Width = 423
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      Items = <
        item
          Caption = 'Persona'
          Value = 'S'
        end
        item
          Caption = 'Convenio'
          Value = 'C'
        end
        item
          Caption = 'Veh'#237'culo'
          Value = 'V'
        end>
    end
    object vcbPlantilla: TVariantComboBox
      Left = 98
      Top = 62
      Width = 423
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 3
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 353
    Width = 572
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 375
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 112
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object EMAIL_TipoCorrespondencia: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'EMAIL_TipoCorrespondencia'
    Left = 258
    Top = 118
  end
end
