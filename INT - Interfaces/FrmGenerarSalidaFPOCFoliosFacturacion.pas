{-----------------------------------------------------------------------------
  File Name: FrmGenerarSalidaFPOCFoliosFacturacion.pas
  Fecha Creacion  : 09/04/2012
  Firma       : SS_1015_HUR_20120409
  Documentos  :
      * PAR0014 - DD - SRV CU 115 Interfaz Saliente Transitos Folios y Pagos de
        Otras Concesionarias.doc
  Descripcion : Genera los archivos de salida de otras concesionarias para informar
                del proceso de facturacion de CN.

  Firma         : SS-1006-NDR-20120812
  Description   : Formato de fecha y registro de log operaciones interfaces

  Firma         : SS_1006_CQU_20120827
  Descripctrion : Se corrige el nombre del par�metro FOLIOS_SALIDA_FOLIOS_Y_PAGOS por NOMBRE_ARCHIVO_SALIDA_FOLIOS_OC

  Fecha         : 29/08/2012
  Firma         : SS_1006_1015_CQU_20120829
  Description   : Se modifica la obtenci�n del c�digo de del Modulo, ahora se usa una funci�n y no es necesaria
                  la consulta SQL.
                  Se modifica la funci�n que graba el Log al final de la operaci�n para que cuando no encuentre datos lo indique.

  Firma       : SS_1147_NDR_20141216
  Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

  }

unit FrmGenerarSalidaFPOCFoliosFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilProc, DMConnection, StrUtils, StdCtrls, ExtCtrls, Validate,
  DateEdit, ComCtrls,UtilDB, PeaProcs, VariantComboBox, ConstParametrosGenerales, Util,            //SS-1006-NDR-20120812
  DB, DBTables, ADODB,ComunesInterfaces,                                                           //SS-1006-NDR-20120812
  PeaProcsCN;                                                                                      //SS_1006_1015_CQU_20120829

type
  TGenerarArchivoFPOCFoliosFacturacion = class(TForm)
    tbu: TBevel;
    btnProcesar: TButton;
    btnSalir: TButton;
    pnlAvance: TPanel;
    Label4: TLabel;
    pbProgreso: TProgressBar;
    lblMensaje: TLabel;
    pnlFiltros: TPanel;
    Label1: TLabel;
    deDesde: TDateEdit;
    Label3: TLabel;
    Label5: TLabel;
    deHasta: TDateEdit;
    Label6: TLabel;
    lblNombreArchivo: TLabel;
    vcbConcesionarias: TVariantComboBox;
    spGenerarArchivoFolioFacturacion: TADOStoredProc;
    spObtenerArchivoSalidaFoliosYPagos: TADOStoredProc;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure vcbConcesionariasChange(Sender: TObject);
    procedure deDesdeChange(Sender: TObject);
    procedure deHastaChange(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    FCarpetaFoliosyPagosOtrasConcesionarias: String;
    FPrefijoFolioFacturacion:String;
    FFechaArchivo: TDateTime;

    FCodigoOperacion : Integer;                                                 //SS-1006-NDR-20120812
    //CodigoModulo : string; // Obtengo el Codigo del Modulo a Ejecutar         //SS-1006-NDR-20120812
    CodigoModulo : Integer;                                                     //SS_1006_1015_CQU_20120829
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216

    procedure HabilitaProcesar();
    Function RegistrarOperacion : Boolean;                                      //SS-1006-NDR-20120812
    Function ActualizarLog : Boolean;                                           //SS-1006-NDR-20120812
  public
    function Inicializar (txtCaption: ANSIString; MDIChild:Boolean): Boolean;
  end;

const
  CONST_TIPOARCHIVO_FOLIOSFACTURACION = 1;

resourcestring                                                                                             //SS_1006_1015_CQU_20120829
  MSG_PROCESS_SUCCESS = 'Archivo de salida generado exitosamente.';                                        //SS_1006_1015_CQU_20120829
  MSG_PROCESS_FAIL = 'Ha ocurrido un error al intentar generar el archivo de salida';                      //SS_1006_1015_CQU_20120829
  MSG_NO_RECORDS_FOUND = 'No se han encontrado registros para los criterios de b�squeda seleccionados.';   //SS_1006_1015_CQU_20120829

var
  GenerarArchivoFPOCFoliosFacturacion: TGenerarArchivoFPOCFoliosFacturacion;

implementation

{$R *.dfm}



procedure TGenerarArchivoFPOCFoliosFacturacion.btnProcesarClick(
  Sender: TObject);
  function ValidarEntrada: Boolean;
  resourcestring
    MSG_ERROR = 'Validaci�n';
  var
    DescError :string;
  begin
      DescError := '';
      Result := True;

      if deDesde.Date > deHasta.Date then
        DescError := DescError + 'Fecha Desde debe ser menor o igual a Fecha Hasta';

      if DescError <> '' then
      begin
        MsgBox(DescError, MSG_ERROR, MB_ICONWARNING);
        Result := False;
      end;

  end;
//resourcestring                                                                                             //SS_1006_1015_CQU_20120829
//  MSG_PROCESS_SUCCESS = 'Archivo de salida generado exitosamente.';                                        //SS_1006_1015_CQU_20120829
//  MSG_PROCESS_FAIL = 'Ha ocurrido un error al intentar generar el archivo de salida';                      //SS_1006_1015_CQU_20120829
//  MSG_NO_RECORDS_FOUND = 'No se han encontrado registros para los criterios de b�squeda seleccionados.';   //SS_1006_1015_CQU_20120829
Const                                                                                                                    //SS-1006-NDR-20120812
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';                                        //SS-1006-NDR-20120812

var
  ReturnValue: Integer;
  DescError: string;
  FArchivoSalida: TStringList;
begin
   if ValidarEntrada then
   begin
      pnlFiltros.Enabled := False;
      pnlAvance.Visible := True;
      btnProcesar.Enabled := False;
      btnSalir.Enabled := False;
      pbProgreso.Position := 0;

      lblMensaje.Caption := 'Generando datos para el archivo de salida.';
      Application.ProcessMessages;

      try
       //Registro la operaci�n en el Log                                                                //SS-1006-NDR-20120812
        lblMensaje.Caption := STR_REGISTER_OPERATION;          //Informo Tarea                          //SS-1006-NDR-20120812
        Application.ProcessMessages;                           //Refresco la pantalla                   //SS-1006-NDR-20120812
        if not RegistrarOperacion then begin                                                            //SS-1006-NDR-20120812
            Exit;                                                                                       //SS-1006-NDR-20120812
        end;                                                                                            //SS-1006-NDR-20120812

        with spGenerarArchivoFolioFacturacion  do begin                                                 //SS-1006-NDR-20120812
          Close;                                                                                        //SS-1006-NDR-20120812
          Parameters.Refresh;                                                                           //SS-1006-NDR-20120812

          Parameters.ParamByName('@CodigoConcesionaria').Value := vcbConcesionarias.Value;              //SS-1006-NDR-20120812
          Parameters.ParamByName('@NombreArchivo').Value := lblNombreArchivo.Caption;                   //SS-1006-NDR-20120812
          Parameters.ParamByName('@FechaDesde').Value := deDesde.Date;                                  //SS-1006-NDR-20120812
          Parameters.ParamByName('@FechaHasta').Value := deHasta.Date;                                  //SS-1006-NDR-20120812
          Parameters.ParamByName('@DescError').Value := NULL;                                           //SS-1006-NDR-20120812

          ExecProc;

          DescError := VarToStr(Parameters.ParamByName('@DescError').Value);                            //SS-1006-NDR-20120812
          ReturnValue := Integer(Parameters.ParamByName('@RETURN_VALUE').Value);                        //SS-1006-NDR-20120812
        end;
      finally

      end;

      if ReturnValue = 0 then
      begin
        FArchivoSalida := TStringList.Create;

        lblMensaje.Caption := 'Obteniendo datos para el archivo de salida.';
        Application.ProcessMessages;

        with spObtenerArchivoSalidaFoliosYPagos, Parameters do begin
          Close;
          Refresh;

          ParamByName('@CodigoTipoArchivo').Value := CONST_TIPOARCHIVO_FOLIOSFACTURACION;

          Open;

          pbProgreso.Max := RecordCount;
          lblMensaje.Caption := 'Escribiendo archivo...';
          Application.ProcessMessages;

          while not Eof do begin
            FArchivoSalida.Add(FieldByName('Linea').AsString);

            pbProgreso.StepIt;
            Application.ProcessMessages;
            
            Next;
          end;
        end;

        lblMensaje.Caption := 'Guardando archivo de salida.';
        Application.ProcessMessages;

        FArchivoSalida.SaveToFile(Format('%s%s001_%s_%s.TXT',
          [
            FCarpetaFoliosyPagosOtrasConcesionarias,
            FPrefijoFolioFacturacion,
            PadL(VarToStr(vcbConcesionarias.Value), 3, '0'),
            FormatDateTime('yyyymmddhhnnss', FFechaArchivo)
          ]));

        FArchivoSalida.Destroy;
        lblMensaje.Caption := '';
        Application.ProcessMessages;

        MsgBox(MSG_PROCESS_SUCCESS, 'Proceso', MB_ICONINFORMATION);
      end
      else
      begin
         if ReturnValue = -1 then
         begin
            MsgBoxErr(MSG_PROCESS_FAIL, DescError, 'Error', MB_ICONERROR);
            lblMensaje.Caption := MSG_PROCESS_FAIL;
            Application.ProcessMessages;
         end
         else
         begin
            MsgBox(MSG_NO_RECORDS_FOUND, 'Proceso', MB_ICONINFORMATION);
            lblMensaje.Caption := MSG_NO_RECORDS_FOUND;
            Application.ProcessMessages;
         end;

         pnlFiltros.Enabled := True;
         pnlAvance.Visible := False;
      end;
      //Actualizo el log al Final                                               //SS-1006-NDR-20120812
      ActualizarLog;                                                            //SS-1006-NDR-20120812

      btnSalir.Enabled := True;
   end;
end;

procedure TGenerarArchivoFPOCFoliosFacturacion.btnSalirClick(Sender: TObject);
begin
  Close;
end;

procedure TGenerarArchivoFPOCFoliosFacturacion.deDesdeChange(Sender: TObject);
begin
  HabilitaProcesar;
end;

procedure TGenerarArchivoFPOCFoliosFacturacion.deHastaChange(Sender: TObject);
begin
  HabilitaProcesar;
end;

procedure TGenerarArchivoFPOCFoliosFacturacion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TGenerarArchivoFPOCFoliosFacturacion.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_FOLIOSFACTURACION         = ' ' + CRLF +
                          'Los archivos de folios de facturaci�n son ' +
                          'utilizados por el ESTABLECIMIENTO para informar a otras ' +
                          'concesionarias de los tr�nsitos y/o estacionamientos ' +
                          'que han sido facturados.' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: %s(C�digo Concesionaria Origen)_(C�digo Concesionaria Destino)_AAAAMMDDHHNNSS.txt' + CRLF +
                          ' ';
begin
    if pnlAvance.Visible = True then exit;
    MsgBoxBalloon(Format(MSG_FOLIOSFACTURACION, [ FPrefijoFolioFacturacion ]), Caption, MB_ICONQUESTION, IMGAYUDA);
end;

function TGenerarArchivoFPOCFoliosFacturacion.Inicializar;
    procedure CargarCombo;
    begin
        CargarComboOtrasConcesionarias(DMConnections.BaseCAC, vcbConcesionarias, True, True);
        vcbConcesionarias.ItemIndex := 0;
    end;

    function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL   = 'Error al verificar par�metro ';
        MSG_ERROR                 = 'Error';
        STR_NOT_EXISTS_GENERAL    = 'No existe par�metro: ';
        STR_EMPTY_GENERAL         = 'Parametro vac�o: ';
        STR_DIRECTORY_NOT_EXISTS  = 'No existe el directorio indicado en par�metros generales: ' + CRLF;
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'CARPETA_FOLIOS_Y_PAGOS_OC' , FCarpetaFoliosyPagosOtrasConcesionarias) then begin
                    DescError := STR_NOT_EXISTS_GENERAL + 'CARPETA_FOLIOS_Y_PAGOS_OC';
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FCarpetaFoliosyPagosOtrasConcesionarias := GoodDir(FCarpetaFoliosyPagosOtrasConcesionarias);
                if  not DirectoryExists(FCarpetaFoliosyPagosOtrasConcesionarias) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FCarpetaFoliosyPagosOtrasConcesionarias;
                    Result := False;
                    Exit;
                end;

                //if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'FOLIOS_SALIDA_FOLIOS_Y_PAGOS' , FPrefijoFolioFacturacion) then begin   // SS_1006_CQU_20120827
                //    DescError := STR_NOT_EXISTS_GENERAL + 'FOLIOS_SALIDA_FOLIOS_Y_PAGOS';                                                     // SS_1006_CQU_20120827
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'NOMBRE_ARCHIVO_SALIDA_FOLIOS_OC' , FPrefijoFolioFacturacion) then begin  // SS_1006_CQU_20120827
                    DescError := STR_NOT_EXISTS_GENERAL + 'NOMBRE_ARCHIVO_SALIDA_FOLIOS_OC';                                                    // SS_1006_CQU_20120827
                    Result := False;
                    Exit;
                end;
            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
begin
    Result := True;
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    FCodigoOperacion := 0;                          //Inicializo el codigo de operacion                                                                                           //SS-1006-NDR-20120812
   //CodigoModulo := QueryGetValue(DMConnections.BaseCAC,                                                                                                                          //SS-1006-NDR-20120812   //SS_1006_1015_CQU_20120829
   //                              'SELECT CodigoModulo FROM Modulos WITH (NOLOCK) WHERE Descripcion = ''Interfaz Saliente Generar Archivo Folio Otras Concesionarias''');         //SS-1006-NDR-20120812   //SS_1006_1015_CQU_20120829
   CodigoModulo := ObtenerCodigoModuloSalidaFoliosFacturacionOC(DMConnections.BaseCAC); //SS_1006_1015_CQU_20120829

  if not MDIChild then begin
		FormStyle := fsNormal;
		Visible := False;
	end;

  CenterForm (Self);

  try
		DMConnections.BaseCAC.Connected := True;
		Result := DMConnections.BaseCAC.Connected and VerificarParametrosGenerales;
	except
		on e: Exception do begin
			MsgBoxErr (MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
      Exit;
		end;
	end;

  CargarCombo;

  spGenerarArchivoFolioFacturacion.Connection := DMConnections.BaseCAC;
  spObtenerArchivoSalidaFoliosYPagos.Connection := DMConnections.BaseCAC;

  Caption := AnsiReplaceStr (txtCaption, '&', '');
  btnProcesar.Enabled := False;
  pnlFiltros.Enabled := True;
	pnlAvance.Visible := False;
	lblMensaje.Caption := '';
  pbProgreso.Position := 0;

  FFechaArchivo := NowBase(DMConnections.BaseCAC);

  lblNombreArchivo.Caption :=  Format('%s001_999_%s.TXT',
  [
    FPrefijoFolioFacturacion,
    FormatDateTime('yyyymmddhhnnss', FFechaArchivo)
  ]);
end;


procedure TGenerarArchivoFPOCFoliosFacturacion.vcbConcesionariasChange(
  Sender: TObject);
begin
  if Integer(vcbConcesionarias.Value) <= 0 then
    lblNombreArchivo.Caption :=  Format('%s001_999_%s.TXT',
    [
      FPrefijoFolioFacturacion,
      FormatDateTime('yyyymmddhhnnss', FFechaArchivo)
    ])
  else
    lblNombreArchivo.Caption :=  Format('%s001_%s_%s.TXT',
    [
      FPrefijoFolioFacturacion,
      PadL(VarToStr(vcbConcesionarias.Value), 3, '0'),
      FormatDateTime('yyyymmddhhnnss', FFechaArchivo)
    ]);

  HabilitaProcesar;
end;

procedure TGenerarArchivoFPOCFoliosFacturacion.HabilitaProcesar;
begin
  if (Integer(vcbConcesionarias.Value) > 0) and
     (not deDesde.IsEmpty) and
     (not deHasta.IsEmpty) then
    btnProcesar.Enabled := True
  else
    btnProcesar.Enabled := False;
end;

procedure TGenerarArchivoFPOCFoliosFacturacion.ImgAyudaMouseMove(
  Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if pnlAvance.Visible then
    ImgAyuda.Cursor := crdefault
  else
    ImgAyuda.Cursor := crHandPoint;
end;


//BEGIN SS-1006-NDR-20120812----------------------------------------------------------------------------------------------
function TGenerarArchivoFPOCFoliosFacturacion.RegistrarOperacion: Boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
	DescError : String;
    STR_OBSERVACION : string;
    //CodigoDelModulo : integer;    //SS_1006_1015_CQU_20120829
begin
    STR_OBSERVACION := 'Generar Archivo Folios Otras Concesionarias';
    //CodigoDelModulo := StrToInt(CodigoModulo);    //SS_1006_1015_CQU_20120829
    //Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, CodigoDelModulo, ExtractFileName(lblNombreArchivo.Caption), UsuarioSistema, STR_OBSERVACION, False, False , NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);    //SS_1006_1015_CQU_20120829
    Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, CodigoModulo, ExtractFileName(lblNombreArchivo.Caption), UsuarioSistema, STR_OBSERVACION, False, False , NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);         //SS_1006_1015_CQU_20120829
    if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;


function TGenerarArchivoFPOCFoliosFacturacion.ActualizarLog: Boolean;
Resourcestring
     MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
Const
     STR_OBSERVACION = 'Finalizo OK!';
var
     DescError : String;                                                                                             
     Observacion : String;                                                                                          //SS_1006_1015_CQU_20120829
begin
    try
        if lblMensaje.Caption = MSG_NO_RECORDS_FOUND then Observacion := STR_OBSERVACION + ' - No Existen Datos'    //SS_1006_1015_CQU_20120829
        else Observacion := STR_OBSERVACION;                                                                        //SS_1006_1015_CQU_20120829

        //Result := ActualizarLogOperacionesInterfaseAlFinal                                                        //SS_1006_1015_CQU_20120829
        //          (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);                         //SS_1006_1015_CQU_20120829

        Result := ActualizarLogOperacionesInterfaseAlFinal                                                          //SS_1006_1015_CQU_20120829
                  (DMConnections.BaseCAC, FCodigoOperacion, Observacion , DescError);                               //SS_1006_1015_CQU_20120829


    except
        on e : Exception do begin
           Result := False;
           MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
        end;
    end;
end;
//END SS-1006-NDR-20120812----------------------------------------------------------------------------------------------

end.
