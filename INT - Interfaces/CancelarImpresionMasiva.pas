{-----------------------------------------------------------------------------
 Unit Name: CancelarImpresionMasiva.pas
 Author:    mtraversa
 Date Created: 29/12/2004
 Language: ES-AR
 Description: M�dulo para la anulacion de procesos masivos de impresion

 Revision 1
 Author: nefernandez
 Date Created: 08/03/2007
 Language: ES-AR
 Description: Se pasa el parametro CodigoUsuario al SP AnularProcesoImpresionMasivo
              (para Auditoria)

 Firma       : SS_1147_NDR_20141216
 Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
 
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

-----------------------------------------------------------------------------}
unit CancelarImpresionMasiva;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BuscaTab, DB, ADODB, DmiCtrls, StdCtrls, ComCtrls, ExtCtrls, UtilProc,
  PeaProcs, PeaTypes, RStrings, DMConnection, ComunesInterfaces, Util;

type
  TFrmCancelarImpresionMasiva = class(TForm)
	Bevel1: TBevel;
    btnProcesar: TButton;
    btnSalir: TButton;
    spObtenerOperacionesImpresionMasiva: TADOStoredProc;
    spAnularProcesoImpresionMasivo: TADOStoredProc;
    lblFechaInterfase: TLabel;
    edProceso: TBuscaTabEdit;
    buscaOperaciones: TBuscaTabla;
    spObtenerOperacionesImpresionMasivaCodigoOperacionInterfase: TIntegerField;
    spObtenerOperacionesImpresionMasivaFecha: TDateTimeField;
    spObtenerOperacionesImpresionMasivaUsuario: TStringField;
    spObtenerOperacionesImpresionMasivaCantidad: TIntegerField;
    procedure btnProcesarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    function  buscaOperacionesProcess(Tabla: TDataSet;var Texto: String): Boolean;
    procedure buscaOperacionesSelect(Sender: TObject; Tabla: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FCodigoOperacionInterfase: LongInt;
    FProcesando: Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    { Private declarations }
  public
    function Inicializar: Boolean;
    { Public declarations }
  end;

var
  FrmCancelarImpresionMasiva: TFrmCancelarImpresionMasiva;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: Inicializar
Author : lgisuk
Date Created : 15/07/2005
Description : inicializacion de este formulario
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TFrmCancelarImpresionMasiva.Inicializar: Boolean;
begin
    FProcesando := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    FCodigoOperacionInterfase := -1;
    with spObtenerOperacionesImpresionMasiva do begin
    	Open;
    end;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: TFrmCancelarImpresionMasiva.buscaOperacionesProcess
  Author:    mtraversa
  Date Created: 29/12/2004
  Description: Para cada proceso se muetra Fecha de realzacion y usuario que lo
               realizo
  Parsmeters: Tabla: TDataSet; var Texto: String
  Return Value:Boolean
-----------------------------------------------------------------------------}
function TFrmCancelarImpresionMasiva.buscaOperacionesProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Result := True;
	with Tabla do
    	Texto := Format('Fecha Realizaci�n: %s  -  Usuario: %s - %d Comprobantes Procesados',
                        [FieldByName('Fecha').AsString, FieldByName('Usuario').AsString, FieldByName('Cantidad').asInteger]);

end;

{******************************** Function Header ******************************
Function Name: buscaOperacionesSelect
Author : lgisuk
Date Created : 15/07/2005
Description :  muestro la lista de procesos de impresion
Parameters : Sender: TObject; Tabla: TDataSet
Return Value : None
*******************************************************************************}
procedure TFrmCancelarImpresionMasiva.buscaOperacionesSelect(Sender: TObject; Tabla: TDataSet);
begin
    with Tabla do begin
        FCodigoOperacionInterfase := FieldByName('CodigoOperacionInterfase').AsInteger;
        edProceso.Text := FieldByName('Fecha').AsString;
    end;
    btnProcesar.Enabled := True;
end;

{-----------------------------------------------------------------------------
  Function Name: TFrmCancelarImpresionMasiva.btnProcesarClick
  Author:    mtraversa
  Date Created: 29/12/2004
  Description: Se pone en Null la FechaHoraImpreso de los comprabantes involucrados
               en el proc. de interfase con codigo FCodigoOperacionInterfase
  Parsmeters: Sender: TObject
  Return Value:None
-----------------------------------------------------------------------------}
procedure TFrmCancelarImpresionMasiva.btnProcesarClick(Sender: TObject);
resourcestring
	MSG_PROCESO_FINALIZO_CON_EXITO   = 'El proceso finaliz� con �xito';
	MSG_PROCESO_NO_SE_PUDO_COMPLETAR = 'El proceso no se pudo completar';
    MSG_ANULACION_FACTURACION        = 'Anulaci�n Impresi�n Masiva - Cod. Op.: %d';
var
    NuevoCodigoOperacionInterfase: Integer;
    DescError: String;
begin
    btnProcesar.Enabled := False;
    FProcesando := True;
    try
        try
            //DMConnections.BaseCAC.BeginTrans;                                       //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('BEGIN TRAN CancelarImpresionMasiva');     //SS_1385_NDR_20150922


            with spAnularProcesoImpresionMasivo do begin
                Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
                // Revision 1
                Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                ExecProc;
            end;

            if not RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_CANCELAR_INTERFAZ_SALIENTE_IMPRESION, EmptyStr, UsuarioSistema, Format(MSG_ANULACION_FACTURACION, [FCodigoOperacionInterfase]), False, False, NowBase(DMConnections.BaseCAC), 0, NuevoCodigoOperacionInterfase, DescError) then Raise(Exception.Create(DescError));

            //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('COMMIT TRAN CancelarImpresionMasiva');					//SS_1385_NDR_20150922


            spObtenerOperacionesImpresionMasiva.Close;
            spObtenerOperacionesImpresionMasiva.Open;

            MsgBox(MSG_PROCESO_FINALIZO_CON_EXITO, Self.Caption, MB_OK + MB_ICONINFORMATION);

            edProceso.Clear;
        except
            on E: Exception do begin
                //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION CancelarImpresionMasiva END');	//SS_1385_NDR_20150922

                MsgBoxErr(MSG_PROCESO_NO_SE_PUDO_COMPLETAR, MSG_PROCESO_NO_SE_PUDO_COMPLETAR, MSG_ERROR_ACTUALIZAR, MB_ICONERROR);
                btnProcesar.Enabled := True;
            end;
        end;
    finally
        FProcesando := False;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : lgisuk
Date Created : 15/07/2005
Description :  permito salir del formulario
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFrmCancelarImpresionMasiva.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : lgisuk
Date Created : 15/07/2005
Description : impido salir si esta procesando
Parameters : Sender: TObject;var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TFrmCancelarImpresionMasiva.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : lgisuk
Date Created : 15/07/2005
Description : lo libero de memoria
Parameters : Sender: TObject;var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TFrmCancelarImpresionMasiva.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    spObtenerOperacionesImpresionMasiva.Close;
    Action := caFree;
end;


end.
