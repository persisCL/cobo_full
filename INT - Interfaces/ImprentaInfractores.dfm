object frmImprentaInfractores: TfrmImprentaInfractores
  Left = 142
  Top = 153
  Width = 765
  Height = 505
  Caption = 'Proceso masivo de notificaci'#243'n a infractores'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  DesignSize = (
    757
    471)
  PixelsPerInch = 96
  TextHeight = 13
  object nb: TNotebook
    Left = 0
    Top = 0
    Width = 757
    Height = 428
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    PageIndex = 1
    TabOrder = 1
    object TPage
      Left = 0
      Top = 0
      Caption = 'Paso1'
      DesignSize = (
        757
        428)
      object Bevel1: TBevel
        Left = 0
        Top = 8
        Width = 741
        Height = 412
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 521
        Height = 13
        Caption = 
          'Paso 1 - Seleccionar los infractores existentes para generar las' +
          ' Notificaciones a Infractores'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 20
        Top = 39
        Width = 70
        Height = 13
        Hint = 'Fecha inicial de las infracciones a procesar'
        Caption = 'Fecha  &Desde:'
        FocusControl = txt_FechaDesde
        ParentShowHint = False
        ShowHint = True
      end
      object Label5: TLabel
        Left = 264
        Top = 39
        Width = 64
        Height = 13
        Hint = 'Fecha final de las infracciones a procesar'
        Caption = '&Fecha Hasta:'
        FocusControl = txt_FechaHasta
        ParentShowHint = False
        ShowHint = True
      end
      object Label2: TLabel
        Left = 21
        Top = 66
        Width = 81
        Height = 13
        Hint = 'Fecha de impresi'#243'n  que figura en la carta'
        Caption = '&Fecha Impresi'#243'n:'
        FocusControl = txt_FechaImpresion
        ParentShowHint = False
        ShowHint = True
      end
      object Label4: TLabel
        Left = 571
        Top = 371
        Width = 169
        Height = 13
        Anchors = [akRight, akBottom]
        Caption = 'Presione "Siguiente" para continuar'
      end
      object dblInfractores: TDBListEx
        Left = 20
        Top = 87
        Width = 721
        Height = 274
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 110
            Header.Caption = 'Fecha Infracci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaInfraccion'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 70
            Header.Caption = 'Patente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Patente'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 80
            Header.Caption = 'Infracciones'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'CantidadInfracciones'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 90
            Header.Caption = 'RUT'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroDocumento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 250
            Header.Caption = 'Nombre'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Nombre'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 250
            Header.Caption = 'Domicilio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Domicilio'
          end>
        DataSource = dsObtenerListaInfractores
        DragReorder = True
        ParentColor = False
        TabOrder = 5
        TabStop = True
      end
      object btnSiguiente: TButton
        Left = 661
        Top = 389
        Width = 78
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Siguiente'
        Enabled = False
        TabOrder = 4
        OnClick = btnSiguienteClick
      end
      object btnObtenerInfractores: TButton
        Left = 626
        Top = 57
        Width = 112
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Obtener Infractores'
        TabOrder = 3
        OnClick = btnObtenerInfractoresClick
      end
      object txt_FechaDesde: TDateEdit
        Left = 105
        Top = 35
        Width = 124
        Height = 21
        Hint = 'Fecha inicial de las infracciones a procesar'
        AutoSelect = False
        TabOrder = 0
        Date = -693594.000000000000000000
      end
      object txt_FechaHasta: TDateEdit
        Left = 336
        Top = 35
        Width = 124
        Height = 21
        Hint = 'Fecha final de las infracciones a procesar'
        AutoSelect = False
        TabOrder = 1
        Date = -693594.000000000000000000
      end
      object txt_FechaImpresion: TDateEdit
        Left = 105
        Top = 62
        Width = 124
        Height = 21
        Hint = 'Fecha de impresi'#243'n  que figura en la carta'
        AutoSelect = False
        TabOrder = 2
        Date = -693594.000000000000000000
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Paso2'
      DesignSize = (
        757
        428)
      object Bevel2: TBevel
        Left = 8
        Top = 8
        Width = 742
        Height = 412
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 404
        Height = 13
        Caption = 
          'Paso 2 - Creaci'#243'n del archivo de interface de infractores para i' +
          'mprenta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object btnBrowseForFolder: TSpeedButton
        Left = 431
        Top = 71
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = btnBrowseForFolderClick
      end
      object Label7: TLabel
        Left = 23
        Top = 54
        Width = 397
        Height = 13
        Caption = 
          'Seleccione la ubicaci'#243'n en d'#243'nde desea crear el archivo de inter' +
          'face'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object btnAnterior: TButton
        Left = 589
        Top = 387
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Anterior'
        TabOrder = 0
        OnClick = btnAnteriorClick
      end
      object btnFinalizar: TButton
        Left = 669
        Top = 387
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Finalizar'
        Enabled = False
        TabOrder = 1
        OnClick = btnFinalizarClick
      end
      object txtUbicacion: TEdit
        Left = 24
        Top = 72
        Width = 401
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object pnlprogreso: TPanel
        Left = 136
        Top = 259
        Width = 424
        Height = 137
        Anchors = [akLeft, akRight, akBottom]
        BevelOuter = bvNone
        Caption = 'Presione Escape para cancelar '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        Visible = False
        DesignSize = (
          424
          137)
        object Label8: TLabel
          Left = 16
          Top = 20
          Width = 80
          Height = 13
          Anchors = [akLeft, akBottom]
          Caption = 'Progreso general'
        end
        object lblDescri: TLabel
          Left = 160
          Top = 0
          Width = 3
          Height = 13
        end
        object labelProgreso: TLabel
          Left = 16
          Top = 80
          Width = 386
          Height = 13
          Alignment = taCenter
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'labelProgreso'
        end
        object pbProgreso: TProgressBar
          Left = 16
          Top = 40
          Width = 384
          Height = 17
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 0
        end
      end
    end
  end
  object btnSalir: TButton
    Left = 674
    Top = 440
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 0
    OnClick = btnSalirClick
  end
  object dsObtenerListaInfractores: TDataSource
    DataSet = spObtenerListaInfractores
    Left = 58
    Top = 424
  end
  object spObtenerListaInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerListaInfractores'
    Parameters = <>
    Left = 88
    Top = 424
  end
  object qryEliminarTemporales: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'DROP INDEX ##ListaInfractores.IX_ListaInfractores'
      'DROP TABLE ##ListaInfractores')
    Left = 148
    Top = 424
  end
  object spActualizarDetalleInfractoresEnviados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDetalleInfractoresEnviados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 181
    Top = 424
  end
  object spAgruparInfraccionesCarta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgruparInfraccionesCarta'
    Parameters = <>
    Left = 118
    Top = 424
  end
end
