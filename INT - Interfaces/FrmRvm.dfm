object FRvm: TFRvm
  Left = 66
  Top = 113
  Caption = 'Consultas al Registro Nacional de Veh'#237'culos Motorizados'
  ClientHeight = 478
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 443
  Constraints.MinWidth = 693
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Memo: TMemo
    Left = 0
    Top = 57
    Width = 784
    Height = 380
    Align = alClient
    Lines.Strings = (
      '')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object pnlAbajo: TPanel
    Left = 0
    Top = 437
    Width = 784
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      784
      41)
    object lblFechaConsulta: TLabel
      Left = 471
      Top = 17
      Width = 5
      Height = 13
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnCerrar: TButton
      Left = 672
      Top = 8
      Width = 101
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = 'C&errar'
      TabOrder = 0
      OnClick = btnCerrarClick
    end
    object cbUpdate: TCheckBox
      Left = 8
      Top = 16
      Width = 233
      Height = 17
      Caption = 'Actualizar Datos de Infractores al Consultar'
      TabOrder = 1
    end
    object chbForzarConsultaRNVM: TCheckBox
      Left = 257
      Top = 16
      Width = 148
      Height = 17
      Caption = 'Forzar Consulta al RNVM'
      TabOrder = 2
    end
  end
  object pnlConsulta: TGroupBox
    Left = 0
    Top = 0
    Width = 784
    Height = 57
    Align = alTop
    Caption = 'Consulta'
    TabOrder = 2
    DesignSize = (
      784
      57)
    object lblpatente: TLabel
      Left = 14
      Top = 24
      Width = 187
      Height = 13
      Caption = 'Ingrese la patente que desea consultar:'
    end
    object ImgAyuda: TImage
      Left = 749
      Top = 21
      Width = 21
      Height = 19
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Picture.Data = {
        07544269746D617036100000424D361000000000000036000000280000002000
        0000200000000100200000000000001000000000000000000000000000000000
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6
        C600B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
        B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
        B500B5B5B500B5B5B500B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636B63006363
        6300636363006363630063636300636363006363630063636300636363006363
        6300636363006363630063636300636363006363630063636300636363006363
        63006363630063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006363630094E7
        FF008CE7FF009CE7FF009CE7FF009CE7FF009CE7FF00A5EFFF00ADEFFF00ADEF
        FF00ADEFFF00ADEFFF00B5EFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00C6F7FF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        630063636300EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        630063636300EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF009CD6
        FF00B5DEFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF009CD6
        FF00B5DEFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        63004AADFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        63000084FF00DEEFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00B5DE
        FF0063636300108CFF00DEEFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF009CD6FF0063636300108CFF00DEF7FF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00DEEFFF0063636300108CFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00C6EFFF00ADDEFF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF0084C6FF0063636300A5D6FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF006363630063636300DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00A5D6FF0063636300A5D6FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF0042A5FF006363630094D6FF00E7FF
        FF00E7FFFF00DEEFFF00108CFF0063636300DEF7FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00C6EFFF00108CFF0063636300319C
        FF004AADFF000084FF00636363006BBDFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00CEEFFF0042A5FF006363
        6300636363006363630084C6FF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0063636300BDEF
        FF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636B63006363
        6300636363006363630063636300636363006363630063636300636363006363
        6300636363006363630063636300636363006363630063636300636363006363
        63006363630063636300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00}
      Stretch = True
      Transparent = True
      OnClick = ImgAyudaClick
      ExplicitLeft = 642
    end
    object txtPatente: TEdit
      Left = 207
      Top = 20
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 7
      TabOrder = 0
      OnChange = txtPatenteChange
    end
    object btnConsultar: TButton
      Left = 620
      Top = 18
      Width = 107
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'C&onsultar al RNVM'
      Default = True
      TabOrder = 2
      OnClick = btnConsultarClick
    end
    object BTNUltimaModificacion: TButton
      Left = 453
      Top = 18
      Width = 161
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'C&onsultar Ultima Modificaci'#243'n'
      Default = True
      TabOrder = 1
      OnClick = BTNUltimaModificacionClick
    end
    object btnAbrirXML: TButton
      Left = 370
      Top = 18
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Abrir XML'
      Default = True
      TabOrder = 3
      OnClick = btnAbrirXMLClick
    end
  end
  object wbXML: TWebBrowser
    Left = 32
    Top = 59
    Width = 824
    Height = 380
    TabOrder = 3
    ControlData = {
      4C0000002A550000462700000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126208000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object SPObtenerUltimaConsultaRNVM: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerUltimaConsultaRNVM;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = ' '
      end>
    Left = 48
    Top = 184
  end
end
