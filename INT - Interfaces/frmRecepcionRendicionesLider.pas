{-------------------------------------------------------------------------------
 File Name: frmRecepcionRendicionesLider.pas
 Author:    flamas
 Date Created: 11/03/2005
 Language: ES-AR
 Description: M�dulo de la interface Lider - Recepcion de Rendiciones
--------------------------------------------------------------------------------
 Author: lgisuk
 Date Created: 03/04/2006
 Description: Reemplaze el reporte de finalizacion por uno nuevo, elimine el que
              existia.


 Revision 5
 Author: mbecerra
 Date: 18-Dic-2008
 Description:	(Ref SS 766) Se hacen las siguientes modificaciones:
                	* Se agrega la validaci�n del largo de la l�nea, que debe ser igual a 263,
                      en la funci�n ParseLiderLine.

                    * ParseLiderLine: En los campos Monto y Nota de Cobro, se valida que sean num�ricos completos,
                      es decir, se cambia la instrucci�n:	Valor := StrToInt(Trim(campo));
                      por 		Valor := StrToInt(campo);

                    * Se modifica la funci�n AnalizarRendicionesTXT para que no retorne
                      error en caso de que la l�nea est� mal parseada, sino que lo deje en la
                      variable FErrores.

                    * Con el punto anterior se corrige un bug, y es que la funci�n
                      RegistrarOperacion usa la variable FErrores para indicar
                      si hay errrores en el LogOperaciones. Pero esta variable se llena
                      despu�s, en la funci�n CargarRendicionesLider.

                    * Tambien se modifica el mensaje de finalizaci�n, para que no depsliegue
                      la cantidad de errores.
                      
                    * Finalmente se modifica la funci�n CargarRendicionesLider para que
                      agregue a ErroresInterfaces los errores de Parseo.

                    * 06-Enero-2009
                    	Se pidi� el siguiente cambio: que el archivo completo no sea rechazado
                    	porque las sumas de totales no coinciden, cuando hay problemas
                        de parseo de alguna l�nea.
                    	Es decir, si el campo "monto" es num�rico que lo sume al
                    	total, para cuadratura, a�n cuando el resto de la l�nea no sea
                    	parseable.

Autor       :   Claudio Quezada
Fecha       :   06-02-2015
Firma       :   SS_1147_CQU_20150316
Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato
<<<<<<< .mine


Firma       :   SS_1147_MBE_20150413
Description :   Se agrega el tipo de comprobante, ya que la deuda puede originarse por NK, SD o TD impago.
                Adem�s, se corrigen errores de programaci�n

    2015-04-14
                Se agrega el tipo y numero Fiscal, para el registro de errores
=======

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

>>>>>>> .r849
-------------------------------------------------------------------------------}
unit frmRecepcionRendicionesLider;

interface

uses
  //Recepcion Rendiciones Lider
  DMConnection,
  ComunesInterfaces,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  PeaTypes,
  UtilDB,
  FrmRptRecepcionRendiciones,       //Reporte de Comprobantes Recibidos
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, DPSControls, ppDB, ppTxPipe, StrUtils,
  ppParameter, ppModule, raCodMod, ppCtrls, ppBands, ppReport, ppPrnabl,
  ppClass, ppStrtch, ppSubRpt, ppCache, ppComm, ppRelatv, ppProd, UtilRB,
  RStrings, RBSetup, ppDBPipe, HashingMD5, DBClient;

type
  TRendicionLiderRecord = record
  	Fecha : TDateTime;
    EPS : string;
    Sucursal : integer;
    Terminal : string;
    Correlativo : integer;
    FechaContable : TDateTime;
    CodigoServicio : string;
    TipoOperacion : string;
    IndicadorContable : string;
    Monto : INT64;
    MedioPago : string;
    CodigoBancoCheque : integer;
    CuentaCheque : string;
    SerieCheque : integer;
    PlazaCheque : integer;
    TipoTarjeta : string;
    MarcaTarjeta : string;
    NumeroTarjeta : string;
    ExpiracionTarjeta : string;
    TipoCuotas : string;
    NumeroCuotas : integer;
    Autorizacion : string;
    //NotaCobro : integer;                                                      //SS_1147_MCA_20150420
    TipoComprobante: string;                                                    //SS_1147_MCA_20150420
    NumeroComprobante: Int64;                                                   //SS_1147_MCA_20150420
    Identificador : string;
  end;

  TfRecepcionRendicionesLider = class(TForm)
    Bevel1: TBevel;
    btnAbrirArchivo: TSpeedButton;
    Label2: TLabel;
    edOrigen: TEdit;
    OpenDialog: TOpenDialog;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    cdsComprobantesCancelados: TClientDataSet;
    cdsComprobantesCanceladosNumeroComprobante: TIntegerField;
    cdsComprobantesCanceladosMonto: TIntegerField;
    chb_ControlCodigoMD5: TCheckBox;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    SpObtenerResumenComprobantesRecibidos: TADOStoredProc;
    SpObtenerResumenComprobantesRecibidosMotivo: TStringField;
    SpObtenerResumenComprobantesRecibidosCantidad: TIntegerField;
    SpObtenerResumenComprobantesRecibidosMonto: TStringField;
    SPAgregarRendicionLider: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
    { Private declarations }
    FCodigoOperacion : integer;
    FDetenerImportacion: boolean;
    FRendicionesTXT : TStringList;
    FErrores : TStringList;
    FErrorMsg : string;
    //FVerReporteErrores: boolean;
   	FLider_Directorio_Origen_Rendiciones : AnsiString;
    FLider_Directorio_Errores : AnsiString;
    FLider_DirectorioRendicionProcesada: AnsiString;
    FLineasArchivo: integer;
    FMontoArchivo: int64;
    FCodigoNativa : Integer;  // SS_1147_CQU_20150316
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function  ValidarHeaderFooterLider(var sArchivo : TStringList; sNombreArchivo, sEPS, sGlosa : string) : boolean;
    function  VerificarCodigoSeguridad : boolean;
    function  ParseLiderLine(sline : string; var LiderRecord : TRendicionLiderRecord; var sParseError : string) : boolean;
    function  AnalizarRendicionesTXT : boolean;
   	function  RegistrarOperacion : boolean;
    function  CargarRendicionesLider : boolean;
    function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
    procedure GrabarErrores;                                                    //SS_1147_MCA_20150420
   	//procedure GenerarReportesErrores;
  public
  	function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
	{ Public declarations }
  end;

var
  fRecepcionRendicionesLider: TfRecepcionRendicionesLider;

implementation


{$R *.dfm}


{-----------------------------------------------------------------------------
                	EsNumero
  Author: mbecerra
  Date: 06-Enero-2009
  Description:	Devuelve True si el texto es num�rico, False en caso contrario.
-----------------------------------------------------------------------------}
function EsNumero(Texto : string) : boolean;
var
    i : integer;
begin
    i := 1;
    Result := True;
    while Result and (i <= Length(Texto)) do begin
    	Result := (Texto[i] in ['0'..'9']);
    	Inc(i);
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Inicializacion de este formulario
  Parameters: txtCaption: ANSIString; MDIChild:Boolean
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesLider.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 25/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        LIDER_DIRECTORIO_ORIGEN_RENDICIONES = 'Lider_Directorio_Origen_Rendiciones';
        LIDER_DIRECTORIO_ERRORES            = 'Lider_Directorio_Errores';
        LIDER_DIRECTORIORENDICIONPROCESADA  = 'Lider_DirectorioRendicionProcesada';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la Concesionaria Nativa                  // SS_1147_CQU_20150316
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150316

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, LIDER_DIRECTORIO_ORIGEN_RENDICIONES , FLider_Directorio_Origen_Rendiciones) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + LIDER_DIRECTORIO_ORIGEN_RENDICIONES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FLider_Directorio_Origen_Rendiciones := GoodDir(FLider_Directorio_Origen_Rendiciones);
                if  not DirectoryExists(FLider_Directorio_Origen_Rendiciones) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FLider_Directorio_Origen_Rendiciones;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, LIDER_DIRECTORIO_ERRORES , FLider_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + LIDER_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FLider_Directorio_Errores := GoodDir(FLider_Directorio_Errores);
                if  not DirectoryExists(FLider_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FLider_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,LIDER_DIRECTORIORENDICIONPROCESADA,FLider_DirectorioRendicionProcesada);

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;



resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Elijo el modo en que si visualizara la ventana
    if not MDIChild then begin
      FormStyle := fsNormal;
      Visible := False;
    end;

    //Centro el form
    CenterForm(Self);
    try
          DMConnections.BaseCAC.Connected := True;
          Result := DMConnections.BaseCAC.Connected and
                                VerificarParametrosGenerales;
    except
          on e: Exception do begin
              MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
          end;
    end;

    //Resolver ac� lo que necesita este form para inicializar correctamente
   	Caption := AnsiReplaceStr(txtCaption, '&', '');
    chb_ControlCodigoMD5.Enabled := ExisteAcceso('configurar_control_codigo_MD5');
    btnCancelar.Enabled := False;
    btnProcesar.Enabled := False;
    pnlAvance.Visible := False;
    lblReferencia.Caption := '';
    if FCodigoNativa = CODIGO_VS then begin                 // SS_1147_CQU_20150316
        OpenDialog.Filter := 'Rendiciones|LDR_CAJA*.GEN';   // SS_1147_CQU_20150316
        chb_ControlCodigoMD5.Visible := False;              // SS_1147_CQU_20150316
        chb_ControlCodigoMD5.Checked := False;              // SS_1147_CQU_20150316
    end
    else                                                    // SS_1147_CQU_20150316
        OpenDialog.Filter := 'Rendiciones|001REND*.GEN'    // SS_1147_CQU_20150316

end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description :  Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfRecepcionRendicionesLider.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_RENDICION          = ' ' + CRLF +
                          'El Archivo de Rendiciones' + CRLF +
                          'es enviado por LIDER al ESTABLECIMIENTO' + CRLF +
                          'y tiene por objeto dar a conocer al Establecimiento, ' + CRLF +
                          'los comprobantes que fueron cobrados en las cajas de LIDER.' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: 001RENDYYYYMMDD.GEN' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para registrar los pagos de los' + CRLF +
                          'comprobantes aceptados en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.Visible = True then Exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_RENDICION, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TfRecepcionRendicionesLider.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoClick
  Author:    flamas
  Date Created: 23/12/2004
  Description: Abre el Archivo de Rendiciones, y verifica si no ha sido procesado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesLider.btnAbrirArchivoClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ValidarNombreArchivoLider
      Author:    flamas
      Date Created: 06/07/2005
      Description:
      Parameters: sFileName, sTipoArchivo, sExtension : string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    function ValidarNombreArchivoLider( sFileName, sTipoArchivo, sExtension : string ) : boolean;
    resourcestring
        MSG_INVALIDA_FILE_NAME = 'El nombre del archivo %s es inv�lido';
        MSG_ERROR = 'Error';
    begin
        if FCodigoNativa = CODIGO_VS then begin                                             // SS_1147_CQU_20150316
            // Valido que tenga Extensi�n                                                   // SS_1147_CQU_20150316
            if ( Pos( '.', sExtension ) = 0 ) then sExtension := '.' + sExtension;          // SS_1147_CQU_20150316
            // Valido que cumpla con el formato establecido                                 // SS_1147_CQU_20150316
            result := (Copy( ExtractFileName(sFileName ), 1, 8) = 'LDR_CAJA') and           // SS_1147_CQU_20150316
                       ValidarFechaAAAAMMDD(Copy( ExtractFileName(sFileName ), 10, 8 )) and // SS_1147_CQU_20150316
                       (ExtractFileExt(sFileName) = sExtension);                            // SS_1147_CQU_20150316
        end else begin                                                                      // SS_1147_CQU_20150316
        if ( Pos( '.', sExtension ) = 0 ) then sExtension := '.' + sExtension;

        result := ( Copy( ExtractFileName( sFileName ), 1, 7 ) = '001' + sTipoArchivo ) and
                    ValidarFechaAAAAMMDD( Copy( ExtractFileName( sFileName ), 8, 8 )) and
                    ( ExtractFileExt(sFileName) = sExtension );
        end;                                                                                // SS_1147_CQU_20150316
        if not result then MsgBox (Format (MSG_INVALIDA_FILE_NAME, [ExtractFileName (sFileName)]), MSG_ERROR, MB_ICONERROR);
    end;

resourcestring
    MSG_ERROR = 'Error';
    MSG_ERROR_FILE_DOES_NOT_EXIST 	= 'El archivo %s no existe';
    MSG_ERROR_INVALID_FILENAME 		= 'El nombre del archivo %s es inv�lido';
    MSG_FILE_ALREADY_PROCESSED 		= 'El archivo %s ya fue procesado';
    MSG_FILE_WISH_TO_REPROCESS		= '�Desea volver a procesarlo?';
begin
      opendialog.InitialDir := FLider_Directorio_Origen_Rendiciones;
      if opendialog.execute then begin
          edOrigen.text:=UpperCase( opendialog.filename );

      if  not ValidarNombreArchivoLider( edOrigen.text, 'REND', '.GEN' ) then begin
          MsgBox( Format ( MSG_ERROR_INVALID_FILENAME, [ExtractFileName(edOrigen.text) ]), MSG_ERROR, MB_ICONERROR );
          Exit;
      end;

      if not FileExists( edOrigen.text ) then begin
        MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigen.text]), MSG_ERROR, MB_ICONERROR );
          Exit;
      end;

    	if  VerificarArchivoProcesado( DMConnections.BaseCAC, edOrigen.text ) then begin
			if (MsgBox (Format (MSG_FILE_ALREADY_PROCESSED, [ExtractFileName (edOrigen.text) ]) + #13#10 +
            		MSG_FILE_WISH_TO_REPROCESS, MSG_ERROR, MB_ICONQUESTION or MB_YESNO) <> IDYES) then
        			Exit;
    	end;

        if FLider_DirectorioRendicionProcesada <> '' then begin
            if RightStr(FLider_DirectorioRendicionProcesada,1) = '\' then  FLider_DirectorioRendicionProcesada := FLider_DirectorioRendicionProcesada + ExtractFileName(edOrigen.text)
            else FLider_DirectorioRendicionProcesada := FLider_DirectorioRendicionProcesada + '\' + ExtractFileName(edOrigen.text);
        end;

        btnProcesar.Enabled := True
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarHeaderFooterLider
  Author:    flamas
  Date Created: 23/12/2004
  Description: Valida el encabezado, pie de p�gina y cantidad de registros del Archivo
  Parameters: var sArchivo : TStringList
  Return Value: boolean
-----------------------------------------------------------------------------}
function  TfRecepcionRendicionesLider.ValidarHeaderFooterLider(var sArchivo : TStringList; sNombreArchivo, sEPS, sGlosa : string) : boolean;
resourcestring
	MSG_ERROR					= 'Error';
	MSG_INVALID_HEADER 			= 'El encabezado del archivo es inv�lido';
	MSG_INVALID_DESCRIPTION 	= 'La glosa es incorrecta';
	MSG_INVALID_DATE 			= 'La fecha de glosa es incorrecta';
	MSG_INVALID_COMPANY 		= 'El c�digo de empresa prestadora de servicios es inv�lido';
	MSG_INVALID_FOOTER 			= 'El pie de p�gina del archivo es inv�lido';
	MSG_INVALID_REGISTER_COUNT 	= 'La cantidad de registros del archivo'#10#13 + 'no coincide con la cantidad reportada';
var
	nLineas : integer;
    nRegs	: integer;
begin
	nLineas := sArchivo.Count - 1;

    result := False;

    // Valida el Header
	if ( Pos( 'HEADER', sArchivo[0] ) <> 1 ) then begin
    	MsgBox( MSG_INVALID_HEADER, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

    // Valida la EPS
    if FCodigoNativa = CODIGO_VS then sEPS := 'VESSUR';    // SS_1147_CQU_20150316
    if ( Pos( sEPS , sArchivo[0] ) <> 8 ) then begin
    	MsgBox( MSG_INVALID_COMPANY, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

   { // Valida la Glosa
	if  (Pos(UpperCase(Trim(sGlosa)), UpperCase(sArchivo[0])) <> 15)  then begin
    	MsgBox( MSG_INVALID_DESCRIPTION, MSG_ERROR, MB_ICONERROR );
     	Exit;
    end;}

    // Valida la Fecha
    if FCodigoNativa = CODIGO_VS then begin                                                 // SS_1147_CQU_20150316
        if (Pos(Copy(ExtractFileName(edOrigen.Text), 12,6), sArchivo[0]) <> 58) then begin  // SS_1147_CQU_20150316
            MsgBox( MSG_INVALID_DATE, MSG_ERROR, MB_ICONERROR );                            // SS_1147_CQU_20150316
            Exit;                                                                           // SS_1147_CQU_20150316
        end;                                                                                // SS_1147_CQU_20150316
    end else begin                                                                          // SS_1147_CQU_20150316
	if  (Pos(Copy(ExtractFileName(edOrigen.Text), 10,6), sArchivo[0]) <> 54)  then begin
    	MsgBox( MSG_INVALID_DATE, MSG_ERROR, MB_ICONERROR );
     	Exit;
    end;
    end;                                                                                    // SS_1147_CQU_20150316

    // Valida el Footer
    if ( Pos( 'FOOTER', sArchivo[nLineas] ) <> 1 ) then begin
    	MsgBox( MSG_INVALID_FOOTER, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

	try
    	nRegs := StrToInt( trim( Copy( sArchivo[nLineas], 8, 6 )));
    except
    	on exception do begin
    		MsgBox( MSG_INVALID_FOOTER, MSG_ERROR, MB_ICONERROR );
    		Exit;
		end;
    end;

    if ( nRegs <> nLineas - 1 ) then begin
    	MsgBox( MSG_INVALID_REGISTER_COUNT, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

    result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: VerificarCodigoSeguridad
  Author:    flamas
  Date Created: 11/01/2005
  Description: Verifica el c�digo de Seguridad (HashMD5)
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function  TfRecepcionRendicionesLider.VerificarCodigoSeguridad : boolean;
resourcestring
  	MSG_VERIFYING_HASH_CODE	= 'Verificando el c�digo de seguridad - Cantidad de lineas : %d';
    MSG_INVALID_HASH_CODE	= 'El c�digo de seguridad es inv�lido.';
    MSG_ERROR				= 'Error';
var
  	nNroLineaScript, nLineasScript :integer;
    sHashString : string;
    sHashCode : string;
begin
  	Screen.Cursor := crHourglass;
    result := False;
  	nLineasScript := FRendicionesTXT.Count - 1;
  	lblReferencia.Caption := Format( MSG_VERIFYING_HASH_CODE, [nLineasScript] );
  	pbProgreso.Position := 1;
  	pbProgreso.Max := FRendicionesTXT.Count - 1;
  	pnlAvance.Visible := True;
    nNroLineaScript := 1;
    sHashString := '';
    while ( nNroLineaScript < nLineasScript ) and ( not FDetenerImportacion ) and (	FErrorMsg = '' ) do begin

        sHashString := sHashString + Copy( FRendicionesTXT[nNroLineaScript], 15, 249);
        Inc( nNroLineaScript );
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;

    end;

    if ( not FDetenerImportacion ) then begin
    	sHashCode := MD5String( sHashString );
		if ( UpperCase( Trim( Copy( FRendicionesTXT[nLineasScript], 232, 32 ))) <> sHashCode ) then MsgBox(MSG_INVALID_HASH_CODE, Caption, MB_ICONERROR)
        else result := True;
    end;

  	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: ParseLiderLine
  Author:    flamas
  Date Created: 23/12/2004
  Description: Parsea la linea de Rendicion
  Parameters: sline : string; var LiderRecord : TRendicionLiderRecord
  				var sParseError : string
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  02/08/2007
  Ahora el monto se hace StrToInt64 en lugar de STRtoINT

  Revision 2
  Author: mbecerra
  Date: 16-Dic-2008
  Description:	(Ref: SS 766) Se pide que no se validen los campos Tipo, Marca y N�mero de Tarjeta.

            	Adicionalmente se cambian los StrToInt por StrToIntDef para el caso de CHEQUE,
                porque tampoco deben validarse.

        17-Dic-2008
                Adicionalmente se agrega que la funci�n ParseLiderLine valide el largo de la l�nea.
                Y en la funci�n CargarRendicionesLider, que se agreguen a ErroresInterfaces

-----------------------------------------------------------------------------}
function TfRecepcionRendicionesLider.ParseLiderLine( sline : string; var LiderRecord : TRendicionLiderRecord; var sParseError : string) : boolean;

    {-----------------------------------------------------------------------------
      Function Name: InicializarRegistroRendicionLider
      Author:    flamas
      Date Created: 23/12/2004
      Description: Borra los campos del registro que no se reasignan en
                    forma obligatoria
      Parameters: FRendicionRecord : TRendicionLiderRecord
      Return Value: None
    -----------------------------------------------------------------------------}
    procedure InicializarRegistroRendicionLider( var LiderRecord : TRendicionLiderRecord );
    begin
        with LiderRecord do begin
            CodigoBancoCheque := 0;
            CuentaCheque := '';
            SerieCheque := 0;
            PlazaCheque := 0;
            TipoTarjeta := '';
            MarcaTarjeta := '';
            NumeroTarjeta := '';
            ExpiracionTarjeta := '';
            TipoCuotas := '';
            NumeroCuotas := 0;
            Autorizacion := '';
            //Rev 5
            Monto := 0;
        end;
    end;

resourcestring
  	MSG_DATE_ERROR				= 'La fecha es inv�lida.';
    MSG_COMPANY_ERROR			= 'La empresa prestadora de servicios es inv�lida.';
    MSG_BRANCH_ERROR			= 'La sucursal es inv�lida.';
    MSG_CONTROL_NUMBER_ERROR	= 'El correlativo es inv�lido.';
    MSG_INVOICE_DATE_ERROR		= 'La fecha contable es inv�lida.';
    MSG_OPERATION_TYPE_ERROR	= 'El tipo de operaci�n es inv�lido.';
    MSG_ACCOUNT_INDICATOR_ERROR = 'El indicador contable es inv�lido.';
    MSG_AMMOUNT_ERROR			= 'El monto es inv�lido.';
    MSG_PAYMENT_METHOD_ERROR	= 'El tipo de pago es inv�lido.';
    MSG_BANK_CODE_ERROR			= 'El c�digo de banco es inv�lido.';
    MSG_CHECK_SERIAL_ERROR		= 'La serie del cheque es inv�lida';
    MSG_CREDIT_CARD_TYPE_ERROR	= 'El tipo de tarjeta de cr�dito es inv�lido.';
    MSG_INVOICE_NUMBER_ERROR	= 'El n�mero de la nota de cobro es inv�lido.';
    MSG_NO_CUMPLE_LARGO			= 'El largo %d de la l�nea no cumple el largo esperado: %d';

const
	LARGO_LINEA_LIDER = 263;

var
    Campo : string;
    sTipoComprobanteFiscal, TipoComprobante     : string;  // SS_1147_CQU_20150316
    NumeroComprobanteFiscal, NumeroComprobante   : Int64;  // SS_1147_CQU_20150316
begin
  	Result := False;

    //Revision 5
    InicializarRegistroRendicionLider( LiderRecord );
    if Length(sline) <> LARGO_LINEA_LIDER then begin
        sParseError := Format(MSG_NO_CUMPLE_LARGO, [Length(sline), LARGO_LINEA_LIDER]);
        Exit;
    end;


    with LiderRecord do begin
    	//REV 5  InicializarRegistroRendicionLider( LiderRecord );
		try
            if FCodigoNativa = CODIGO_VS then begin	                                                                                    // SS_1147_CQU_20150316
                sParseError 		:= MSG_DATE_ERROR;																					// SS_1147_CQU_20150316
                Fecha 				:= EncodeDate(	StrToInt(Copy(sline, 1, 4)),														// SS_1147_CQU_20150316
                                                    StrToInt(Copy(sline, 5, 2)),														// SS_1147_CQU_20150316
                                                    StrToInt(Copy( sline, 7, 2)));														// SS_1147_CQU_20150316
                sParseError 		:= MSG_COMPANY_ERROR;																				// SS_1147_CQU_20150316
                EPS 				:= Trim(Copy( sline, 15, 6)); if (EPS <> 'VESSUR') then Exit;										// SS_1147_CQU_20150316
                sParseError 		:= MSG_BRANCH_ERROR;																				// SS_1147_CQU_20150316
                Sucursal 			:= StrToInt(Trim(Copy(sline, 21, 6)));																// SS_1147_CQU_20150316
//                Terminal 			:= Copy(sline, 27, 15);																				// SS_1147_MBE_20150415     // SS_1147_CQU_20150316
                Terminal 			:= Copy(sline, 27, 8);																				// SS_1147_MBE_20150415
                sParseError 		:= MSG_CONTROL_NUMBER_ERROR;																		// SS_1147_CQU_20150316
                Correlativo 		:= StrToInt(Trim( Copy( sline, 42, 10 )));															// SS_1147_CQU_20150316
                sParseError 		:= MSG_INVOICE_DATE_ERROR;																			// SS_1147_CQU_20150316
                FechaContable 		:= EncodeDate( 	StrToInt(Copy(sline, 52, 4)),														// SS_1147_CQU_20150316
                                                    StrToInt(Copy( sline, 56, 2)),														// SS_1147_CQU_20150316
                                                    StrToInt(Copy(sline, 58, 2)));														// SS_1147_CQU_20150316
                CodigoServicio		:= Copy(sline, 60, 6);																				// SS_1147_CQU_20150316
                sParseError 		:= MSG_OPERATION_TYPE_ERROR;																		// SS_1147_CQU_20150316
                TipoOperacion 		:= Copy(sline, 66, 6); if (TipoOperacion <> 'INGRES') and  (TipoOperacion <> 'EGRESO') then Exit;	// SS_1147_CQU_20150316
                sParseError 		:= MSG_ACCOUNT_INDICATOR_ERROR;																		// SS_1147_CQU_20150316
                IndicadorContable 	:= Copy(sline, 72, 1); if ((IndicadorContable <> 'D') and (IndicadorContable <> 'H')) then Exit;	// SS_1147_CQU_20150316
                sParseError 		:= MSG_AMMOUNT_ERROR;																				// SS_1147_CQU_20150316
                Campo 				:= Copy(sline, 73, 10);                                                                             // SS_1147_CQU_20150316
                if EsNumero(Campo) then Monto := StrToInt64(Campo)																		// SS_1147_CQU_20150316
                else Exit;																												// SS_1147_CQU_20150316
                MedioPago 			:= Trim(Copy(sline, 83, 6));																		// SS_1147_CQU_20150316
                sParseError 		:= MSG_PAYMENT_METHOD_ERROR;																		// SS_1147_CQU_20150316
                if (MedioPago = 'CHEQUE') then begin																					// SS_1147_CQU_20150316
                    sParseError 	  	:= MSG_BANK_CODE_ERROR;																			// SS_1147_CQU_20150316
                    CodigoBancoCheque 	:= StrToIntDef(Trim(Copy(sline, 89, 3)),0);														// SS_1147_CQU_20150316
                    CuentaCheque 		:= Copy(sline, 92, 11 );																		// SS_1147_CQU_20150316
                    sParseError 	  	:= MSG_CHECK_SERIAL_ERROR;																		// SS_1147_CQU_20150316
                    SerieCheque 		:= StrToIntDef(Trim(Copy(sline, 103, 7)),0);													// SS_1147_CQU_20150316
                    PlazaCheque 		:= StrToIntDef(Trim(Copy(sline, 110, 3)),0);													// SS_1147_CQU_20150316
                end else if (MedioPago = 'TARJET') then begin																			// SS_1147_CQU_20150316
                    sParseError 	  	:= MSG_CREDIT_CARD_TYPE_ERROR;																	// SS_1147_CQU_20150316
                    TipoTarjeta := Copy(sline, 113, 6);                                                                                 // SS_1147_CQU_20150316
                    MarcaTarjeta := Copy(sline, 119, 6);                                                                                // SS_1147_CQU_20150316
                    NumeroTarjeta := Copy(sline, 125, 19);																				// SS_1147_CQU_20150316
                    ExpiracionTarjeta := Copy(sline, 144, 4);																			// SS_1147_CQU_20150316
                    TipoCuotas := Copy(sline, 148, 6);																					// SS_1147_CQU_20150316
                    NumeroCuotas := StrToIntDef( Trim( Copy( sline, 154, 2 )), 0);														// SS_1147_CQU_20150316
                    Autorizacion := Copy( sline, 156, 8 );																				// SS_1147_CQU_20150316
                end else if ( MedioPago <> 'CASH' ) then Exit;																			// SS_1147_CQU_20150316
																																		// SS_1147_CQU_20150316
                sParseError := MSG_INVOICE_NUMBER_ERROR;																				// SS_1147_CQU_20150316
//                Campo := Copy( sline, 164, 10 );                                                                                      // SS_1147_MBE_20150415          // SS_1147_CQU_20150316
                Campo := Copy( sline, 166, 10 );                                                                                        // SS_1147_MBE_20150415
                if EsNumero(Campo) then begin                                                                                           // SS_1147_CQU_20150316
                    sTipoComprobanteFiscal  := Trim(Copy(sline, 164, 2));                                                               // SS_1147_CQU_20150316
                    case StrToInt(sTipoComprobanteFiscal) of                                                                            // SS_1147_MCA_20150420
                          33 : sTipoComprobanteFiscal := TC_FACTURA_AFECTA;                                                             // SS_1147_MCA_20150420
                          34 : sTipoComprobanteFiscal := TC_FACTURA_EXENTA;                                                             // SS_1147_MCA_20150420
                          39 : sTipoComprobanteFiscal := TC_BOLETA_AFECTA;                                                              // SS_1147_MCA_20150420
                          41 : sTipoComprobanteFiscal := TC_BOLETA_EXENTA;                                                              // SS_1147_MCA_20150420
                    end;
                    NumeroComprobanteFiscal := StrToInt64(Campo);  													                    // SS_1147_CQU_20150316
                    //ObtenerDocumentoInterno(sTipoComprobanteFiscal, NumeroComprobanteFiscal, TipoComprobante, NumeroComprobante);     // SS_1147_CQU_20150316
                    TipoComprobante := sTipoComprobanteFiscal;                                                                          // SS_1147_MCA_20150420
                    //NotaCobro := NumeroComprobante;                                                                                   // SS_1147_MCA_20150420  // SS_1147_CQU_20150316
                    NumeroComprobante := NumeroComprobanteFiscal                                                                        // SS_1147_MCA_20150420
                end else Exit;																											// SS_1147_CQU_20150316
																																		// SS_1147_CQU_20150316
                Identificador := Copy( sline, 173, 91 );																				// SS_1147_CQU_20150316
                sParseError := '';																										// SS_1147_CQU_20150316
                result := True;																											// SS_1147_CQU_20150316
            end else begin																												// SS_1147_CQU_20150316
                sParseError 		:= MSG_DATE_ERROR;
                Fecha 				:= EncodeDate(	StrToInt(Copy(sline, 1, 4)),
                                                    StrToInt(Copy(sline, 5, 2)),
                                                    StrToInt(Copy( sline, 7, 2)));
                sParseError 		:= MSG_COMPANY_ERROR;
                EPS 				:= Trim(Copy( sline, 15, 6)); if (EPS <> 'COSNOR') then Exit;
                sParseError 		:= MSG_BRANCH_ERROR;
                Sucursal 			:= StrToInt(Trim(Copy(sline, 21, 6)));
                Terminal 			:= Copy(sline, 27, 15);
                sParseError 		:= MSG_CONTROL_NUMBER_ERROR;
                Correlativo 		:= StrToInt(Trim( Copy( sline, 42, 10 )));
                sParseError 		:= MSG_INVOICE_DATE_ERROR;
                FechaContable 		:= EncodeDate( 	StrToInt(Copy(sline, 52, 4)),
                                                    StrToInt(Copy( sline, 56, 2)),
                                                    StrToInt(Copy(sline, 58, 2)));
                CodigoServicio		:= Copy(sline, 60, 6);
                sParseError 		:= MSG_OPERATION_TYPE_ERROR;
                TipoOperacion 		:= Copy(sline, 66, 6); if (TipoOperacion <> 'INGRES') and  (TipoOperacion <> 'EGRESO') then Exit;
                sParseError 		:= MSG_ACCOUNT_INDICATOR_ERROR;
                IndicadorContable 	:= Copy(sline, 72, 1); if ((IndicadorContable <> 'D') and (IndicadorContable <> 'H')) then Exit;
                sParseError 		:= MSG_AMMOUNT_ERROR;
                Campo 				:= Copy(sline, 73, 10); //Revision 5		 Monto := StrToInt64(Trim(Copy(sline, 73, 10)));
                if EsNumero(Campo) then Monto := StrToInt64(Campo)
                else Exit;
                MedioPago 			:= Trim(Copy(sline, 83, 6));
                sParseError 		:= MSG_PAYMENT_METHOD_ERROR;
                if (MedioPago = 'CHEQUE') then begin
                    sParseError 	  	:= MSG_BANK_CODE_ERROR;
                    CodigoBancoCheque 	:= StrToIntDef(Trim(Copy(sline, 89, 3)),0);
                    CuentaCheque 		:= Copy(sline, 92, 11 );
                    sParseError 	  	:= MSG_CHECK_SERIAL_ERROR;
                    SerieCheque 		:= StrToIntDef(Trim(Copy(sline, 103, 7)),0);
                    PlazaCheque 		:= StrToIntDef(Trim(Copy(sline, 110, 3)),0);
                end else if (MedioPago = 'TARJET') then begin
                    sParseError 	  	:= MSG_CREDIT_CARD_TYPE_ERROR;
                    TipoTarjeta := Copy(sline, 113, 6); // Revision 2	if (TipoTarjeta <> 'PRESTO') then Exit;
                    MarcaTarjeta := Copy(sline, 119, 6); // Revision 2	if (MarcaTarjeta <> 'PRESTO') then Exit;
                    NumeroTarjeta := Copy(sline, 125, 19);
                    ExpiracionTarjeta := Copy(sline, 144, 4);
                    TipoCuotas := Copy(sline, 148, 6);
                    NumeroCuotas := StrToIntDef( Trim( Copy( sline, 154, 2 )), 0);
                    Autorizacion := Copy( sline, 156, 8 );
                end else if ( MedioPago <> 'CASH' ) then Exit;

                sParseError := MSG_INVOICE_NUMBER_ERROR;
                Campo := Copy( sline, 164, 9 );		//Revision 5		NotaCobro := StrToInt( Trim( Copy( sline, 164, 9 )));
                //if EsNumero(Campo) then NotaCobro := StrToInt(Campo)
                if EsNumero(Campo) then NumeroComprobante := StrToInt(Campo)
                else Exit;

                Identificador := Copy( sline, 173, 91 );
                sParseError := '';
                result := True;
            end;																														// SS_1147_CQU_20150316
        except
        	on exception do Exit;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: AnalizarRendicionesTXT
  Author:    flamas
  Date Created: 06/01/2005
  Description: Antes de hacer nada Parsea todo el Archivo para verificar
  				que el parseo sea v�lido
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesLider.AnalizarRendicionesTXT : boolean;
resourcestring
  	MSG_ANALIZING_RENDERINGS_FILE		= 'Analizando Archivo de Rendiciones - Cantidad de lineas : %d';
    MSG_RENDERINGS_FILE_HAS_ERRORS 		= 'El archivo de Rendiciones contiene errores'#10#13'L�nea : %d';
  	MSG_ERROR_PROCESSING_PAYMENT_CANCEL = 'Error procesando la cancelaci�n de un pago.';
    MSG_TOTALS_ERROR					= 'Los totales del debe y el haber no coinciden'#10#13 + 'con el total de los montos del archivo';
    MSG_ERROR					 		= 'Error';
var
  	nNroLineaScript, nLineasScript :integer;
    FRendicionRecord	: TRendicionLiderRecord;
  	sParseError         : string;
    nDebe			 	: int64;//integer;
    nHaber			 	: int64;//integer;
    Monto				: int64;
begin
	Screen.Cursor := crHourglass;

	nLineasScript := FRendicionesTXT.Count - 1;
    lblReferencia.Caption := Format( MSG_ANALIZING_RENDERINGS_FILE, [nLineasScript - 1] );
	pbProgreso.Position := 1;
	pbProgreso.Max := FRendicionesTXT.Count - 2;
	pnlAvance.Visible := True;

    nNroLineaScript := 1;
    nDebe			:= 0;
    nHaber			:= 0;
    FMontoArchivo := 0;

    FErrores.Clear;    //Rev 5
    while ( nNroLineaScript < nLineasScript ) and
    	  ( not FDetenerImportacion ) and
          (	FErrorMsg = '' ) do begin

        	if ParseLiderLine( FRendicionesTXT[nNroLineaScript], FRendicionRecord, sParseError ) then begin
                // Suma los monteos del Debe y el haber
            	if ( FRendicionRecord.IndicadorContable = 'D' ) then Inc( nDebe, FRendicionRecord.Monto )
                else Inc( nHaber, FRendicionRecord.Monto );

                // Si es un Egreso, lo agrega a la tabla de Egresos
				if ( FRendicionRecord.TipoOperacion = 'EGRESO' ) then
                    with cdsComprobantesCancelados do begin
                    	try
                    		Insert;
                        	//FieldByName( 'NumeroComprobante' ).AsInteger := FRendicionRecord.NotaCobro;           //SS_1147_MCA_20150420
                            FieldByName( 'NumeroComprobante' ).AsInteger := FRendicionRecord.NumeroComprobante;     //SS_1147_MCA_20150420
                        	FieldByName( 'Monto' ).AsInteger := FRendicionRecord.Monto;
                        	Post;
        				      except
              					  on E : exception do begin
                  					  MsgBoxErr(MSG_ERROR_PROCESSING_PAYMENT_CANCEL, e.Message, MSG_ERROR, MB_ICONERROR);
                      				FErrorMsg := MSG_ERROR_PROCESSING_PAYMENT_CANCEL;
                  				end;
              				end;
                	end;
            end else begin	// Si encuentra un error termina
                //Revision 5
                	//FErrorMsg := Format( MSG_RENDERINGS_FILE_HAS_ERRORS, [nNroLineaScript] ) + #10#13 + sParseError;
                	//MsgBox(FErrorMsg, Caption, MB_ICONERROR);
                	//Screen.Cursor := crDefault;
            		//  result := False;
                	//Exit;
                FErrores.Add(sParseError);

                //Revision 5
                //si hay error de parseo, tratar de ver que el campo "monto" sea num�rico
                //para incluirlo en los totales
                sParseError := Copy(FRendicionesTXT[nNroLineaScript], 73, 10);
                if EsNumero(sParseError) then begin
					Monto := StrToInt64(sParseError);
                    if Copy(FRendicionesTXT[nNroLineaScript], 72, 1) = 'D' then Inc( nDebe, Monto );
                    if Copy(FRendicionesTXT[nNroLineaScript], 72, 1) = 'H' then Inc( nHaber, Monto );
                    {si no es D � H debe dar error, por eso no se us� un else}

                    {para que el total me cuadre en el reporte:}
                    FRendicionRecord.Monto := Monto;
                end;
            end;
            FMontoArchivo := FMontoArchivo + FRendicionRecord.Monto;
			Inc( nNroLineaScript );
        	pbProgreso.Position := nNroLineaScript - 1;
          	Application.ProcessMessages;
    end;

    if FCodigoNativa = CODIGO_VS then begin																    // SS_1147_CQU_20150316
// DEBEN DEFINIR SI ESTA CORRECTO QUE NO VENHA HABER													    // SS_1147_CQU_20150316
        if  not FDetenerImportacion and																	    // SS_1147_CQU_20150316
            ((StrToIntDef(Trim(Copy(FRendicionesTXT[nLineasScript], 15, 14)), 0) <> nHaber)  or			    // SS_1147_CQU_20150316
            //(StrToIntDef(Trim(Copy(FRendicionesTXT[nLineasScript], 30, 14)), 0) <> nHaber)) then begin	// SS_1147_MCA_20150420 // SS_1147_CQU_20150316
             (StrToIntDef(Trim(Copy(FRendicionesTXT[nLineasScript], 30, 14)), 0) <> nDebe)) then begin	    // SS_1147_MCA_20150420
            MsgBox(MSG_TOTALS_ERROR, Caption, MB_ICONERROR);											    // SS_1147_CQU_20150316
            FErrorMsg := MSG_TOTALS_ERROR;																    // SS_1147_CQU_20150316
        end;																							    // SS_1147_CQU_20150316
    end else begin																						    // SS_1147_CQU_20150316
    if  not FDetenerImportacion and
    	((StrToIntDef(Trim(Copy(FRendicionesTXT[nLineasScript], 15, 14)), 0) <> nDebe)  or
    	 (StrToIntDef(Trim(Copy(FRendicionesTXT[nLineasScript], 30, 14)), 0) <> nHaber)) then begin
        MsgBox(MSG_TOTALS_ERROR, Caption, MB_ICONERROR);
		FErrorMsg := MSG_TOTALS_ERROR;
    end;
    end;
    FLineasArchivo := nNroLineaScript - 1;
	Result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 23/12/2004
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesLider.RegistrarOperacion : boolean;
resourcestring
  	MSG_ERRORS = 'Errores : ';
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
Const
    RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_LIDER		= 27;
var
  	sDescrip : string;
    DescError : string;
begin
    sDescrip := iif( not FErrores.Count > 0, '', MSG_ERRORS + IntToStr( FErrores.Count ));

   result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_LIDER, ExtractFileName(edOrigen.text), UsuarioSistema, sDescrip, True, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, FLineasArchivo, FMontoArchivo, DescError);
   if not result then
    	MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: CargarRendicionesLider
  Author:    flamas
  Date Created: 23/12/2004
  Description: Carga
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesLider.CargarRendicionesLider : boolean;

    {-----------------------------------------------------------------------------
      Function Name: ComprobanteCancelado
      Author:    flamas
      Date Created: 13/01/2005
      Description: Verifica si el Comprobante est� en la lista de Cancelados
      Parameters: nNumeroComprobante, nMonto : integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    function ComprobanteCancelado( sTipoOperacion : string; nNumeroComprobante, nMonto : integer ) : boolean;
    resourcestring
        MSG_ERROR_PROCESSING_PAYMENT_CANCEL = 'Error procesando la cancelaci�n de un pago.';
        MSG_ERROR = 'Error';
    begin
        result := False;
        if 	( cdsComprobantesCancelados.IsEmpty ) or
            ( sTipoOperacion = 'EGRESO' ) then Exit;

        if ( cdsComprobantesCancelados.Locate( 'NumeroComprobante;Monto', VarArrayOf( [nNumeroComprobante, nMonto] ), [] ) ) then begin
            result := True;
            try
                cdsComprobantesCancelados.Delete;
            except
                on E:exception do begin
                    MsgBoxErr(MSG_ERROR_PROCESSING_PAYMENT_CANCEL, e.Message, MSG_ERROR, MB_ICONERROR);
                    FErrorMsg := MSG_ERROR_PROCESSING_PAYMENT_CANCEL;
                end;
            end;
        end;
    end;

resourcestring
    MSG_PROCESSING_RENDERINGS_FILE = 'Procesando archivo de Rendiciones - Cantidad de lineas : %d';
    MSG_ERROR_PROCESSING_RENDERINGS_FILE = 'Error procesando archivo de Rendiciones';
    MSG_ERROR_SAVING_INVOICE            = 'Error al Asentar la Operaci�n Comprobante: %d ';
    MSG_ERROR_IN_PAYMENT_CANCELS = 'Existen cancelaciones de comprobantes que no se pagaron.';
  	MSG_ERROR	= 'Error';
    MSG_LINEA = 'Linea %d: ';		//revision 5
var
  	nNroLineaScript, nLineasScript : integer;
    FRendicionRecord : TRendicionLiderRecord;
  	sParseError : string;
    sDescripcionError : string;
    //
    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
begin
  	Screen.Cursor := crHourglass;

  	nLineasScript := FRendicionesTXT.Count - 1;
    lblReferencia.Caption := Format( MSG_PROCESSING_RENDERINGS_FILE, [nLineasScript - 1] );
    pbProgreso.Position := 0;
    pbProgreso.Max := FRendicionesTXT.Count - 2;
    pnlAvance.Visible := True;

    FErrores.Clear;
    nNroLineaScript := 1;

    while ( nNroLineaScript < nLineasScript ) and ( not FDetenerImportacion ) and (	FErrorMsg = '' ) do begin

        //Intentamos registrar el pago de un comprobante.
        Reintentos  := 0;

        //Obtengo los datos de la rendicion
        if 	( ParseLiderLine(FRendicionesTXT[nNroLineaScript], FRendicionRecord, sParseError)) then begin

            while Reintentos < 3 do begin
                try
                      spAgregarRendicionLider.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value	:= FCodigoOperacion;
                      spAgregarRendicionLider.Parameters.ParamByName( '@Fecha' ).Value						:= FRendicionRecord.Fecha;
                      spAgregarRendicionLider.Parameters.ParamByName( '@EPS' ).Value						:= FRendicionRecord.EPS;
                      spAgregarRendicionLider.Parameters.ParamByName( '@Sucursal' ).Value					:= FRendicionRecord.Sucursal;
                      spAgregarRendicionLider.Parameters.ParamByName( '@Terminal' ).Value					:= FRendicionRecord.Terminal;
                      spAgregarRendicionLider.Parameters.ParamByName( '@Correlativo' ).Value				:= FRendicionRecord.Correlativo;
                      spAgregarRendicionLider.Parameters.ParamByName( '@FechaContable' ).Value				:= FRendicionRecord.FechaContable;
                      spAgregarRendicionLider.Parameters.ParamByName( '@CodigoServicio' ).Value				:= FRendicionRecord.CodigoServicio;
                      spAgregarRendicionLider.Parameters.ParamByName( '@TipoOperacion' ).Value				:= FRendicionRecord.TipoOperacion;
                      spAgregarRendicionLider.Parameters.ParamByName( '@IndicadorContable' ).Value			:= FRendicionRecord.IndicadorContable;
                      spAgregarRendicionLider.Parameters.ParamByName( '@Monto' ).Value						:= FRendicionRecord.Monto;
                      spAgregarRendicionLider.Parameters.ParamByName( '@MedioPago' ).Value					:= FRendicionRecord.MedioPago;
                      spAgregarRendicionLider.Parameters.ParamByName( '@CodigoBancoCheque' ).Value			:= Iif( FRendicionRecord.CodigoBancoCheque <> 0, FRendicionRecord.CodigoBancoCheque, NULL );
                      spAgregarRendicionLider.Parameters.ParamByName( '@CuentaCheque' ).Value				:= Iif( FRendicionRecord.CuentaCheque <> '', FRendicionRecord.CuentaCheque, NULL );
                      spAgregarRendicionLider.Parameters.ParamByName( '@SerieCheque' ).Value				:= Iif( FRendicionRecord.SerieCheque <> 0, FRendicionRecord.SerieCheque, NULL );
                      spAgregarRendicionLider.Parameters.ParamByName( '@PlazaCheque' ).Value				:= Iif( FRendicionRecord.PlazaCheque <> 0, FRendicionRecord.PlazaCheque, NULL );
                      spAgregarRendicionLider.Parameters.ParamByName( '@TipoTarjeta' ).Value				:= Iif( FRendicionRecord.TipoTarjeta <> '', FRendicionRecord.TipoTarjeta, NULL );
                      spAgregarRendicionLider.Parameters.ParamByName( '@MarcaTarjeta' ).Value				:= Iif( FRendicionRecord.MarcaTarjeta <> '', FRendicionRecord.MarcaTarjeta, NULL );
                      spAgregarRendicionLider.Parameters.ParamByName( '@NumeroTarjeta' ).Value				:= iif( FRendicionRecord.NumeroTarjeta <> '', FRendicionRecord.NumeroTarjeta, NULL );
                      spAgregarRendicionLider.Parameters.ParamByName( '@ExpiracionTarjeta' ).Value			:= Iif( FRendicionRecord.ExpiracionTarjeta <> '', FRendicionRecord.ExpiracionTarjeta, NULL );
                      spAgregarRendicionLider.Parameters.ParamByName( '@TipoCuotas' ).Value					:= Iif( FRendicionRecord.TipoCuotas <> '', FRendicionRecord.TipoCuotas, NULL );
                      spAgregarRendicionLider.Parameters.ParamByName( '@NumeroCuotas' ).Value				:= Iif( FRendicionRecord.NumeroCuotas <> 0, FRendicionRecord.NumeroCuotas, NULL );
                      spAgregarRendicionLider.Parameters.ParamByName( '@Autorizacion' ).Value				:= Iif( FRendicionRecord.Autorizacion <> '', FRendicionRecord.Autorizacion, NULL );
                      //spAgregarRendicionLider.Parameters.ParamByName( '@NotaCobro' ).Value				      := FRendicionRecord.NotaCobro;          //SS_1147_MCA_20150420
                      spAgregarRendicionLider.Parameters.ParamByName( '@TipoComprobante' ).Value			:= FRendicionRecord.TipoComprobante;          //SS_1147_MCA_20150420
                      spAgregarRendicionLider.Parameters.ParamByName( '@NumeroComprobante' ).Value			:= FRendicionRecord.NumeroComprobante;        //SS_1147_MCA_20150420
                      spAgregarRendicionLider.Parameters.ParamByName( '@Identificador' ).Value				:= Iif( FRendicionRecord.Identificador <> '', FRendicionRecord.Identificador, NULL );
                      //spAgregarRendicionLider.Parameters.ParamByName( '@Cancelado' ).Value				:= ComprobanteCancelado( FRendicionRecord.TipoOperacion, FRendicionRecord.NotaCobro, FRendicionRecord.Monto );  //SS_1147_MCA_20150420
                      spAgregarRendicionLider.Parameters.ParamByName( '@Cancelado' ).Value					:= ComprobanteCancelado( FRendicionRecord.TipoOperacion, FRendicionRecord.NumeroComprobante, FRendicionRecord.Monto );  //SS_1147_MCA_20150420
                      spAgregarRendicionLider.Parameters.ParamByName( '@Usuario' ).Value					:= UsuarioSistema;
                      spAgregarRendicionLider.Parameters.ParamByName( '@Reintento' ).Value					:= Reintentos;
                      spAgregarRendicionLider.CommandTimeout := 500;
                      spAgregarRendicionLider.ExecProc;

                      sDescripcionError := Trim( VarToStr( spAgregarRendicionLider.Parameters.ParamByName( '@DescripcionError' ).Value ));
                      if ( sDescripcionError <> '' ) then FErrores.Add( StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]));

                      //1/4 de segundo entre registracion de pago y otro
                      Sleep(25);
                      //Salgo del ciclo por Ok
                      Break;

                  except
                      on e: exception do begin
                          if (pos('deadlock', e.Message) > 0) then begin
                              mensajeDeadlock := e.message;
                              inc(Reintentos);
                              sleep(2000);
                          end else begin
                              //FErrorMsg := Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]);                                       //SS_1147_MCA_20150420
                              FErrorMsg := Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NumeroComprobante]);                                 //SS_1147_MCA_20150420
                              //MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]), e.Message, MSG_ERROR, MB_ICONERROR);     //SS_1147_MCA_20150420
                              MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NumeroComprobante]), e.Message, MSG_ERROR, MB_ICONERROR);     //SS_1147_MCA_20150420
                              Break;
                          end;
                      end;
                  end;

            end;

        end
        else begin	 //Revision 5
            AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion, Format(MSG_LINEA, [nNroLineaScript]) + sParseError);
        end;

        //una vez que se reintento 3 veces x deadlock lo informo
        if Reintentos = 3 then begin
            //FErrorMsg := Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]);                                    //SS_1147_MCA_20150420
            FErrorMsg := Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NumeroComprobante]);                                    //SS_1147_MCA_20150420
            //MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]), mensajeDeadLock, MSG_ERROR, MB_ICONERROR);  //SS_1147_MCA_20150420
            MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NumeroComprobante]), mensajeDeadLock, MSG_ERROR, MB_ICONERROR);  //SS_1147_MCA_20150420
        end;

        Inc( nNroLineaScript );
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

  	//Si qued� alg�n comprobante cancelado sin su correspondiente
    //comprobante pago, da un error
    if ( not cdsComprobantesCancelados.IsEmpty ) then begin
		    MsgBox(MSG_ERROR_IN_PAYMENT_CANCELS, Caption, MB_ICONERROR);
        FErrorMsg := MSG_ERROR_IN_PAYMENT_CANCELS;
    end;

    Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;

{------------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Genero el Reporte de Finalizacion de Proceso sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TfRecepcionRendicionesLider.GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
Const
    STR_TITLE = 'Comprobantes Recibidos';
var
    FRecepcion : TFRptRecepcionRendiciones;
begin
    Result:=false;
    try
        //muestro el reporte
        Application.createForm(TFRptRecepcionRendiciones, FRecepcion);
        if not fRecepcion.Inicializar(STR_TITLE, CodigoOperacionInterfase) then fRecepcion.Release;
        Result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    flamas
  Date Created: 23/12/2004
  Description: Procesa el archivo de d�bitos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesLider.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/06/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name:  ObtenerResumen
      Author:    lgisuk
      Date Created: 03/04/2006
      Description:  Obtengo el resumen del proceso realizado
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    procedure ObtenerResumen (CodigoOperacionInterfase : Integer;Var CantRegs, Aprobados, Rechazados : Integer);
    resourcestring
      MSG_ERROR = 'Error';
      MSG_ERROR_QUERY = 'Error Obteniendo Resumen';
    begin
      try
          With spObtenerResumenComprobantesRecibidos, Parameters do begin
              Close;
              ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
              CommandTimeout := 500;
              ExecProc;
              CantRegs := Parameters.ParamByName('@CantidadComprobantesRecibidos').Value;
              Aprobados := Parameters.ParamByName('@CantidadComprobantesAceptados').Value;
              Rechazados := Parameters.ParamByName('@CantidadComprobantesRechazados').Value;
              Close;
          end;
      except
          on E : Exception do begin
              MsgBoxErr(MSG_ERROR_QUERY, E.Message, MSG_ERROR, MB_ICONERROR);
          end;
      end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 01/07/2005
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog(CantidadErrores:integer):boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : string;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
  	MSG_PROCESS_SUCCEDED = 'El proceso finaliz� con �xito';
    MSG_PROCESS_FINISH_WITH_ERRORS = 'El proceso finaliz� con errores.';
  	MSG_PROCESS_COULD_NOT_BE_COMPLETED = 'El proceso no se pudo completar';
  	MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE = 'No se puede abrir el archivo de rendiciones';
    MSG_ERRROR_RENDERINGS_FILE_IS_EMPTY = 'El archivo seleccionado est� vac�o';
    MSG_ERROR = 'Error';
    MSG_FIN = 'El proceso ha finalizado';
    
const
    LIDER_RENDERING_FILE_DESCRIPTION = 'Pagos realizados en las cajas de L�der-';
    LIDER_COMPANY_CODE = 'COSNOR';
var
    //TotalRegistros: integer;
    CantidadErrores: integer;
    CantRegs, Aprobados, Rechazados : Integer;
begin
  //Crea las listas
	FRendicionesTXT := TStringList.Create;
  FErrores := TStringList.Create;
	FErrorMsg := '';

	//Deshabilita y Habilita los botones
	btnCancelar.Enabled := True;
	btnProcesar.Enabled := False;
	btnSalir.Enabled := False;
	btnAbrirArchivo.Enabled := False;
	edOrigen.Enabled := False;

	FDetenerImportacion := False;
	try
		try
      //Lee el archivo del Rendicones
			FRendicionesTXT.text:= FileToString(edOrigen.text);

			//Verifica si el Archivo Contiene alguna linea
			if (FRendicionesTXT.Count = 0) then

        //Informo que el archivo esta Vacio
				MsgBox(MSG_ERRROR_RENDERINGS_FILE_IS_EMPTY, Caption, MB_ICONWARNING)

			else begin
                //TotalRegistros := FRendicionesTXT.Count - 2;
            	if ValidarHeaderFooterLider(FRendicionesTXT, edOrigen.Text, LIDER_COMPANY_CODE, LIDER_RENDERING_FILE_DESCRIPTION) and
                	((not chb_ControlCodigoMD5.Checked) or (VerificarCodigoSeguridad)) and
                	AnalizarRendicionesTXT then begin
                    	if RegistrarOperacion and
                        	CargarRendicionesLider then begin

                                //Obtengo la cantidad de errores
                                ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);
                                //Actualizo el log al Final
                                ActualizarLog(CantidadErrores);
                                //Obtengo resumen
                                ObtenerResumen (FCodigoOperacion, CantRegs, Aprobados, Rechazados);
                                //Revision 5
                                	//Muestro Cartel de Finalizacion
                          			//MsgProcesoFinalizado(MSG_PROCESS_SUCCEDED, MSG_PROCESS_FINISH_WITH_ERRORS, Caption, CantRegs, Aprobados, Rechazados);
                                MsgBox(MSG_FIN, Caption, MB_ICONINFORMATION);

                                // grabar los errores encontrados                                           // SS_1147_MBE_20150413
                                GrabarErrores();                                                            // SS_1147_MBE_20150413


                                //Muestro el Reporte de Finalizacion del Proceso
                                GenerarReportedeFinalizacion(FCodigoOperacion);
                                //Muevo el archivo procesado a otro directorio
                                MoverArchivoProcesado(Caption,edOrigen.Text, FLider_DirectorioRendicionProcesada);

                        end else begin
                            //Muestro Cartel de que la operaci�n no se pudo completar
                    		  	MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
                        end;
                end;
			end;
		except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE, e.Message, MSG_ERROR, MB_ICONERROR);
            FErrorMsg := MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE;
        end;
		end;
	finally
        //Libero las listas
    		FreeAndNil(FRendicionesTXT);
    		FreeAndNil(FErrores);
    		// Lo desactiva para poder Cerrar el Form
    		btnCancelar.Enabled := False;
        Close;
	end;
end;

{----------------------------------------------------------------------------- Inicio    SS_1147_MBE_20150413
            GrabarErrores

Author      :   mcabello
Date        :   20-Abril-2015
Description :   Grabar los errores encontrados en el procesmiento de la interfaz

-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesLider.GrabarErrores;
var
    i : Integer;
begin
    for i := 0 to FErrores.Count - 1 do begin
        AgregarErrorInterfase( DMConnections.BaseCAC, FCodigoOperacion, FErrores[i], -1);
    end;

end;                                                                            // Fin      SS_1147_MBE_20150413


{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    flamas
  Date Created: 07/12/2004
  Description: Detiene el proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesLider.btnCancelarClick(Sender: TObject);
resourcestring
	  MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 06/07/2005
  Description:  impido salir si esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesLider.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
  	CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 06/07/2005
  Description: Permito cerrar el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesLider.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 06/07/2005
  Description: lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesLider.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;



end.

