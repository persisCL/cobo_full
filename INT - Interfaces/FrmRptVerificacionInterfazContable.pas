{-------------------------------------------------------------------------------
 File Name: FrmRptVerificacionInterfazContable.pas
 Author:    lgisuk
 Date Created: 27/07/2005
 Language: ES-AR
 Description: Módulo de la interface Contable - Reporte de Verificacion de Finalización de Proceso



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
Unit FrmRptVerificacionInterfazContable;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  Utildb,                    //querygetvalue
  UtilProc,                  //Mensajes
  //general
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup, ppStrtch, ppSubRpt, ppModule, raCodMod, ConstParametrosGenerales;                  //SS_1147_NDR_20140710

type
  TFRptVerificacionInterfazContable = class(TForm)
    rptVerificacionInterfazContable: TppReport;
    ppParameterList1: TppParameterList;
	  RBIListado: TRBInterface;
    SPObtenerAsientos: TADOStoredProc;
    ppObtenerAsientos: TppDBPipeline;
    dsObtenerAsientos: TDataSource;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppNombre: TppLabel;
    ppFechaJornada: TppLabel;
    ppLabel1: TppLabel;
    ppIdentificacionArchivoGenerado: TppLabel;
    ppNumeroProceso: TppLabel;
    ppfechahora: TppLabel;
    ppusuario: TppLabel;
    LtFechaHora: TppLabel;
    Ltusuario: TppLabel;
    ppLine5: TppLine;
    ppLine1: TppLine;
    ppidentificacion: TppLabel;
    ppMaxFechaEmision: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppSubReportAsientos: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLabel26: TppLabel;
    ppLabel28: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    raCodeModule1: TraCodeModule;
    ppSummaryBand1: TppSummaryBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLine2: TppLine;
    pp_TitDescripcionAsiento: TppLabel;
    ppdbt_DescripcionAsiento: TppDBText;
    SPVerificarParametrosdeControlContable: TADOStoredProc;
    dsVerificarParametrosdeControlContable: TDataSource;
    ppVerificarParametrosdeControlContable: TppDBPipeline;
    ppSubReportVerificarParametrosdeControlContable: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailBand3: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    ppDBText8: TppDBText;
    ppLine3: TppLine;
    ppLabel7: TppLabel;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FNumeroProceso : Integer;
    FNombreReporte : AnsiString;
    FNombreArchivo : AnsiString;
    { Private declarations }
  public
    Function Inicializar(NumeroProceso :Integer; NombreReporte: AnsiString; NombreArchivo : AnsiString = ''): Boolean;
    { Public declarations }
  end;

var
  FRptVerificacionInterfazContable: TFRptVerificacionInterfazContable;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 01/08/2005
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRptVerificacionInterfazContable.Inicializar(NumeroProceso :Integer; NombreReporte: AnsiString; NombreArchivo : AnsiString = ''): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    FNumeroProceso:= NumeroProceso;
    FNombreReporte:= NombreReporte;
    FNombreArchivo:= NombreArchivo;
    Result := RBIListado.Execute;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 01/08/2005
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
Procedure TFRptVerificacionInterfazContable.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
Resourcestring
    MSG_ERROR_AL_GENERAR_REPORTE = 'Error al generar el reporte';
    MSG_ERROR = 'Error';
Const
    REPORT = 'Reporte';
begin

    //Detalle de Asientos Generados
    try
        with SpObtenerAsientos do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@NumeroProcesoContabilizacion').value:= FNumeroProceso;
            Open;
        end;

        with SpVerificarParametrosdeControlContable do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@NumeroProcesoContabilizacion').value:= FNumeroProceso;
            Open;
        end;

    except
        on e: Exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
        end;
    end;

    try
        //Asigno los valores a los campos del reporte
        ppnombre.Caption                           := FnombreReporte;
        ppFechaHora.Caption                        := DateTimeToStr(now);
        ppUsuario.Caption                          := UsuarioSistema;
        ppNumeroProceso.Caption                    := InttoStr(FNumeroProceso);
        ppFechaJornada.Caption                     := QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.FechaJornadaProcesoContabilizacion ('+InttoStr(FNumeroProceso)+')');
        ppidentificacion.Visible                   := (FNombreArchivo <> '');
        ppidentificacionArchivoGenerado.Visible    := (FNombreArchivo <> '');
        ppidentificacionArchivoGenerado.Caption    :=  FNombreArchivo;

        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FnombreReporte;
    except
        on E: exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.
