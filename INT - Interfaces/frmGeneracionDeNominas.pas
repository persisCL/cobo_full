{-------------------------------------------------------------
            	frmGeneracionDeNominas

Author: mbecerra
Date: 23-Julio-2008
Description:
                Se genera una nueva interfaz de N�minas Homologada

Revision 1
Author: mbecerra
Date: 27-Mayo-2009
Description:
                Se mejora el stored NominasGenerar para evitar posibles
                registros duplicados que muy rara vez aparecen.
                
                Ahora el stored devuelve menos campos. Por ende se sincronizan
                los fuentes con el stored.

Autor		:	CQuezadaI
Fecha		:	06-01-2015
Firma		:	SS_1147_CQU_20150106
Descripcion	:	Se agrega la l�gica para Vespucio Sur

Autor		:	CQuezadaI
Fecha		:	05-03-2015
Firma		:	SS_1147_CQU_20150305
Descripcion	:	Se realizan correcciones de la primera versi�n, seg�n solicitud de PVergara de VS

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Autor		:	CQuezadaI
Fecha		:	20-04-2015
Firma		:	SS_1147_CQU_20150420
Descripcion	:	Para Servipag, Sencillito, Unimarc y Lider se reemplaza la K del Numero de Convenio por un 1
---------------------------------------------------------------------------------}

unit frmGeneracionDeNominas;

interface

uses
	DMConnection,
    Util,
	UtilProc,
    UtilDB,
    PeaTypes,
    PeaProcs,
    ConstParametrosGenerales,
    ComunInterfaces,
    DateUtils,  // SS_1147_CQU_20150106
    frmRptNominas,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, DB, ADODB, FileCtrl;

type
  TGeneracionDeNominasForm = class(TForm)
    mmoLog: TMemo;
    chkNominasLider: TCheckBox;
    lblLider: TLabel;
    lblServipag: TLabel;
    lblSantander: TLabel;
    lblMisCuentas: TLabel;
    chkNominasMisCuentas: TCheckBox;
    chkNominasSantander: TCheckBox;
    chkNominasServipag: TCheckBox;
    btnProcesar: TButton;
    btnSalir: TButton;
    spNominasCrear: TADOStoredProc;
    dsNominas: TDataSource;
    DBGrid1: TDBGrid;
    chkNominasUnimarc: TCheckBox;	// SS_1147_CQU_20150106
    lblUnimarc: TLabel;				// SS_1147_CQU_20150106
    procedure btnProcesarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

    {directorios de salida}
    FLider_Directorio,
    FServipag_Directorio,
    FMisCuentas_Directorio,
    FSantander_Directorio,
    FUnimarc_Directorio,    // SS_1147_CQU_20150106
    FServiPag_CodigoComercio : AnsiString;

    {Listas para archivos}
    FListaLider,
    FListaServipag,
    FListaMisCuentas,
    FListaUnimarc,          // SS_1147_CQU_20150106
    FListaSantander : TStringList;

    FNombreArchivoLider,
    FNombreArchivoServipag,
    FNombreArchivoMisCuentas,
    FNombreArchivoUnimarc,  // SS_1147_CQU_20150106
    FNombreArchivoSantander : AnsiString;
    
    {Montos y Cantidades}
    FCodigoOperacion : integer;
    FMonto,
    FCantidad : extended;
    FCodigoNativa : Integer;        // SS_1147_CQU_20150106
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    FDiasTope : integer;             // SS_1147_CQU_20150420
  public
    { Public declarations }
    function Inicializar(Titulo : string) : boolean;
    procedure MensajeLog(Mensaje : AnsiString);
    //procedure MostrarLabels(lLider, lServiPag, lSantander, lMisCuentas : boolean);            // SS_1147_CQU_20150106
    procedure MostrarLabels(lLider, lServiPag, lSantander, lMisCuentas, lUnimarc : boolean);    // SS_1147_CQU_20150106
    procedure Procesar;
    function ValidarParametroGeneral(Tipo, Parametro : string; var Variable : string) : string;
    function Validaciones : boolean;
    function ObtenerNominas : boolean;
    function GenerarNominas : boolean;
    function RegistrarOperacion : boolean;
    function RegistrarOperacionAlFinal(MsgError : string) : boolean;
    function GenerarNombreArchivo : boolean;
    procedure Habilitar(EsHabilitar : boolean);
    procedure MostrarReporteDeFinalizacion;
  end;

var
  GeneracionDeNominasForm: TGeneracionDeNominasForm;

implementation

{$R *.dfm}

var
	lvFechaHoy : TDateTime;
{-------------------------------------------------------------------------------
 Function GetItem
 Revision 1:
    Author : mbecerra
    Date : 08-04-2008
    Description : Function que recibe un string de la forma <campo1>sep<campo2>sep<campo3>
                	y devuelve el <campo i> indicado en el par�metro, o un string vac�o
                    si no lo encuentra
-------------------------------------------------------------------------------}
function GetItem(Texto : string; NumeroItem : integer; Separador : Char) : string;
var
    Elemento : string;
    i, Cual : integer;
begin
    i := 1;
    Elemento := '';
    Cual := 0;
    Texto := Texto + Separador;  {para asegurarnos de encontrar el separador en el �ltimo elemento}
    while i <= Length(Texto) do begin
        if Texto[i] = Separador then begin
            Inc(Cual);
            if Cual = NumeroItem then   {es el elemento que se debe retornar}
            	i := Length(Texto) + 1
            else
                Elemento := '';         {no es el elemento que se debe retornar}

        end
        else
          Elemento := Elemento + Texto[i];

    	Inc(i);
    end;

    Result := Elemento;
end;

{----------------------------------------------------------
  					RellenarCaracter
  Author: mbecerra
  Date: 14-Julio-2008
  Description:
                Agrega un caracter tantas veces a la derecha o a la izquierda
------------------------------------------------------------}
function RellenarCaracter(Texto, Caracter : string; EsIzquierda : boolean; LargoTotal : Integer) : string;
begin
	Result := Texto;
    while Length(Result) < LargoTotal do begin
        if EsIzquierda then Result := Caracter + Result
        else Result := Result + Caracter;
    end;

    Result := Copy(Result, 1, LargoTotal);
end;

{----------------------------------------------------------
  					QuitaCaracteres
  Author: mbecerra
  Date: 14-Julio-2008
  Description:
                Quita los caracteres que pueden provocar problemas en la lectura
                del archivo y los reemplaza por un blanco
------------------------------------------------------------}
function QuitaCaracteres(Texto : string ) : string;
const
    CaracteresValidos = ['A'..'Z', 'a'..'z', '0'..'9', '�', '�', ',', '.', ';', '�','�','�','�','�'];

var
    i : integer;
    
begin
	Result := Texto;
    for i := 1 to Length(Texto) do begin
        if not (Result[i] in CaracteresValidos) then Result[i] := ' ';
    end;

end;

{----------------------------------------------------------
  					Habilitar
  Author: mbecerra
  Date: 15-Julio-2008
  Description:
                Habilita o no los botones y los checkbox
------------------------------------------------------------}
procedure TGeneracionDeNominasForm.Habilitar;
begin
    btnProcesar.Enabled          := EsHabilitar;
    btnSalir.Enabled             := EsHabilitar;
    chkNominasLider.Enabled      := EsHabilitar;
    chkNominasServipag.Enabled   := EsHabilitar;
    chkNominasMisCuentas.Enabled := EsHabilitar;
    chkNominasSantander.Enabled  := EsHabilitar;
    chkNominasUnimarc.Enabled    := EsHabilitar;    // SS_1147_CQU_20150106
end;
{----------------------------------------------------------
  					MensajeLog
  Author: mbecerra
  Date: 11-Julio-2008
  Description:
                Agrega un mensaje en el Log en pantalla
------------------------------------------------------------}
procedure TGeneracionDeNominasForm.MensajeLog;
begin
	Mensaje := FormatDateTime('dd/mm/yyyy hh:nn:ss', NowBase(DMConnections.BaseCAC)) + #9 + Mensaje;
    mmoLog.Lines.Add(Mensaje);
    mmoLog.Refresh;
end;

{----------------------------------------------------------
					ValidarParametroGeneral
  Author: mbecerra
  Date: 11-Julio-2008
  Description:
                Valida que exista el par�metro y sea un directorio v�lido
------------------------------------------------------------}
function TGeneracionDeNominasForm.ValidarParametroGeneral;
resourcestring
	MSG_NO_EXISTE_PARAMETRO = 'No se encontr� el par�metro general:  %s';
    MSG_NO_EXISTE_DIR       = 'No existe el directorio indicado en parametros generales: ';
    MSG_ERROR_VERIFICAR     = 'Error al verificar parametro general %s';
begin
	Result := '';
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, Parametro , Variable) then begin
    	Result := Format(MSG_NO_EXISTE_PARAMETRO, [Parametro]);
    end
    else if Tipo = 'D' then begin
        Variable := GoodDir(Variable);
        if not DirectoryExists(Variable) then Result := MSG_NO_EXISTE_DIR + Variable;

    end
    else if Variable = '' then begin
    	Result := Format(MSG_ERROR_VERIFICAR, [Parametro]);
    end;
end;

{----------------------------------------------------------
					Validaciones
  Author: mbecerra
  Date: 11-Julio-2008
  Description:
                Realiza las validaciones antes de generar las n�minas
------------------------------------------------------------}
function TGeneracionDeNominasForm.Validaciones;
resourcestring
    MSG_DEBE_INDICAR        = 'No ha seleccionado ninguna n�mina a generar';
    MSG_VALIDANDO           = 'Validando...';

const
	LIDER_DIRECTORIO_DESTINO_NOMINAS      = 'Lider_Destino_Nominas';
    SERVIPAG_CODIGODECOMERCIO             = 'Servipag_CodigodeComercio';
    SERVIPAG_DIRECTORIO_DESTINO_DEBITOS   = 'Servipag_Directorio_Destino_Debitos';
    MISCUENTAS_DIRECTORIO_DESTINO_NOMINAS = 'MisCuentas_Destino_Nominas';
    SANTANDER_DIRECTORIO_DESTINO_DEBITOS  = 'Santander_Direct_Destino_Debitos';
    SENCILLITO_DIRECTORIO_DESTINO_NOMINAS = 'Sencillito_Destino_Nominas';   // SS_1147_CQU_20150106
    UNIMARC_DIRECTORIO_DESTINO_NOMINAS    = 'Unimarc_Destino_Nominas';      // SS_1147_CQU_20150106
    PARAM_TOPE_DIAS                       = 'PARAM_M_FECHA_TOPE';           // SS_1147_CQU_20150420
var
	Mensaje : string;
begin

    {  mbecerra: c�digo para pruebas locales
    Result := True;
    FLider_Directorio := 'C:\';
    FServipag_Directorio := 'C:\';
    FServiPag_CodigoComercio := '67700';
    exit;                                    }

    MensajeLog(MSG_VALIDANDO);
	Mensaje := '';
    if (Mensaje = '') and
    	not( chkNominasLider.Checked or chkNominasServipag.Checked or
        ((FCodigoNativa = CODIGO_VS) and chkNominasUnimarc.Checked) or                                              // SS_1147_CQU_20150106
    	chkNominasMisCuentas.Checked or chkNominasSantander.Checked) then Mensaje := MSG_DEBE_INDICAR;

	if (Mensaje = '') and chkNominasLider.Checked then begin  	{ver par�metros generales}
        Mensaje := ValidarParametroGeneral('D', LIDER_DIRECTORIO_DESTINO_NOMINAS, FLider_Directorio);
    end;

    if (Mensaje = '') and chkNominasServipag.Checked then begin {ver par�metros generales}
    	Mensaje := ValidarParametroGeneral('O', SERVIPAG_CODIGODECOMERCIO, FServiPag_CodigoComercio);
        if (Mensaje = '') then begin
			Mensaje := ValidarParametroGeneral('D', SERVIPAG_DIRECTORIO_DESTINO_DEBITOS, FServipag_Directorio);
        end;

    end;

    if (Mensaje = '') and chkNominasMisCuentas.Checked then begin {ver par�metros generales}
        if FCodigoNativa = CODIGO_VS                                                                                // SS_1147_CQU_20150106
        then Mensaje := ValidarParametroGeneral('D', SENCILLITO_DIRECTORIO_DESTINO_NOMINAS, FMisCuentas_Directorio) // SS_1147_CQU_20150106
        else                                                                                                        // SS_1147_CQU_20150106
        Mensaje := ValidarParametroGeneral('D', MISCUENTAS_DIRECTORIO_DESTINO_NOMINAS, FMisCuentas_Directorio);
    end;

    if (Mensaje = '') and chkNominasSantander.Checked then begin  {ver par�metros generales}
    	Mensaje := ValidarParametroGeneral('D', SANTANDER_DIRECTORIO_DESTINO_DEBITOS, FSantander_Directorio);
    end;

    if (Mensaje = '') and (FCodigoNativa = CODIGO_VS) and chkNominasUnimarc.Checked then begin                      // SS_1147_CQU_20150106
    	Mensaje := ValidarParametroGeneral('D', UNIMARC_DIRECTORIO_DESTINO_NOMINAS, FUnimarc_Directorio);           // SS_1147_CQU_20150106
    end;                                                                                                            // SS_1147_CQU_20150106

    if not ObtenerParametroGeneral(DMConnections.BaseCAC, PARAM_TOPE_DIAS , FDiasTope) then begin
        Mensaje := 'No existe parametro general: ' + PARAM_TOPE_DIAS;
    end;

    if Mensaje <> '' then MensajeLog(Mensaje);

    Result := (Mensaje = '');

end;

function TGeneracionDeNominasForm.GenerarNombreArchivo;
resourcestring
	MSG_ERROR = 'Ocurri� un error al generar los nombres de las n�minas';
    NOMINA_VS_PEAJE = 'NOM96400%s%s'; // %s Debe ser la Fecha Formato AAAAMMDD              // SS_1147_CQU_20150106
var
    FechaAAAAMMDD : string;
begin
	Result := True;
    try
    	lvFechaHoy := QueryGetValueDateTime(DMConnections.BaseCAC,'SELECT GETDATE()');
    	FechaAAAAMMDD := FormatDateTime('yyyymmdd', lvFechaHoy);
        if FCodigoNativa <> CODIGO_VS then begin                                            // SS_1147_CQU_20150106
		FNombreArchivoLider      := '001' + 'STND' + FechaAAAAMMDD + '.GEN';
    	FNombreArchivoServipag   := 'NOM' + FServiPag_CodigoComercio + FechaAAAAMMDD + '.TXT';
    	FNombreArchivoMisCuentas := 'NOM' + 'CN' + FechaAAAAMMDD + '.TXT';
    	FNombreArchivoSantander  := 'NOM' + 'CNE' + FechaAAAAMMDD + '.TXT';
        end else begin                                                                      // SS_1147_CQU_20150106
            FNombreArchivoLider      := Format(NOMINA_VS_PEAJE, [FechaAAAAMMDD, '.TXT']);   // SS_1147_CQU_20150106
            FNombreArchivoServipag   := FNombreArchivoLider;                                // SS_1147_CQU_20150106
            FNombreArchivoMisCuentas := FNombreArchivoLider;                                // SS_1147_CQU_20150106
            FNombreArchivoSantander  := FNombreArchivoLider;                                // SS_1147_CQU_20150106
            FNombreArchivoUnimarc    := FNombreArchivoLider;                                // SS_1147_CQU_20150106
        end;                                                                                // SS_1147_CQU_20150106
    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
        	Result := False;
    	end;
    end;
end;
{----------------------------------------------------------
  					ObtenerNominas
  Author: mbecerra
  Date: 11-Julio-2008
  Description:
            	Ejecuta el Stored y obtiene las n�minas
------------------------------------------------------------}
function TGeneracionDeNominasForm.ObtenerNominas;
resourcestring
	MSG_EJECUTANDO   = 'Ejecutando el stored...';
    MSG_YA_EJECUTADO = 'Ya fueron obtenidos los datos de las n�minas.';
    MSG_DESEA        = '�Desea ejecutar el proceso completo nuevamente?';
    MSG_SI           = 'S� = Ejecutar proceso completo';
    MSG_NO           = 'No = S�lo generar los archivos';
    MSG_CANCELADO    = 'Proceso Cancelado por el usuario';
const
	TimeOut_Stored = 5000;

var
	Mensaje : AnsiString;
    Respuesta : byte;
begin
	Mensaje := MSG_YA_EJECUTADO + MSG_DESEA + #10#10 + MSG_SI + #10 + MSG_NO;
	Result := True;
	FListaLider.Clear;
    FListaServipag.Clear;
    FListaMisCuentas.Clear;
    FListaSantander.Clear;
    FListaUnimarc.Clear;    // SS_1147_CQU_20150106
    if spNominasCrear.Active then begin
    	Respuesta := MsgBox(Mensaje, Caption, MB_ICONQUESTION + MB_YESNOCANCEL);
        if Respuesta = IDYES then spNominasCrear.Close
        else if Respuesta = IDCANCEL then begin
        	Result := False;
            MensajeLog(MSG_CANCELADO);
        end;
    end;

    if Result and (not spNominasCrear.Active) then begin
    	MensajeLog(MSG_EJECUTANDO);
    	spNominasCrear.CommandTimeout := TimeOut_Stored;
        spNominasCrear.Open;
    end;

end;


{----------------------------------------------------------
  					GenerarNominas
  Author: mbecerra
  Date: 11-Julio-2008
  Description:
            	Procesa los resultados del Stored y graba los archivos.
------------------------------------------------------------}
function TGeneracionDeNominasForm.GenerarNominas;

	{----------------------------------------------------------
  					GrabarArchivo
	  Author: mbecerra
	  Date: 16-Octubre-2008
	  Description:
                    Intenta grabar la lista al archivo definido en los par�metros.
                    Si falla, entonces pregunta al usuario si desea grabarlo en otra
                    carpeta.
	------------------------------------------------------------}
	function GrabarArchivo( Lista : TStringList; Ruta, NombreArchivo : string) : boolean;
	resourcestring
    	MSG_CAPTION = 'Grabar archivo N�minas';
    	MSG_DESEA	= 'No se pudo grabar el archivo %s en su ubicaci�n original. �Desea grabarlo en otra carpeta?';
    	MSG_ERROR	= 'Se produjo un error al grabar el archivo en la nueva carpeta';
        MSG_GRABADO = 'Se ha grabado el archivo en: %s';

	var
		Carpeta : string;
	begin
    	Result := True;
	    try
	        Lista.SaveToFile(Ruta + NombreArchivo);
	    except
	    	Result := False;
	    end;

	    if not Result then begin
	    	if MsgBox(Format(MSG_DESEA, [NombreArchivo]), MSG_CAPTION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
	            if SelectDirectory(MSG_CAPTION, 'C:\', Carpeta, [sdNewFolder, sdValidateDir]) then begin
	                try
	                	Carpeta := GoodDir(Carpeta);
	                    Lista.SaveToFile(Carpeta + NombreArchivo);
                        MensajeLog(Format(MSG_GRABADO, [Carpeta + NombreArchivo]));
	                    Result := True;
	                except on e:exception do begin
	                    	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
	                	end;
	                end;
	            end;
	        end;
	    end;

	end;

    function HeaderVespucio(var Lista : TStringList) : string;                                                          // SS_1147_CQU_20150106
    var                                                                                                                 // SS_1147_CQU_20150106
        HeaderVespucuio : AnsiString;                                                                                   // SS_1147_CQU_20150106
    begin                                                                                                               // SS_1147_CQU_20150106
            FCantidad       := Lista.Count;                                                                             // SS_1147_CQU_20150106
            HeaderVespucuio := FormatDateTime('yyyymmdd', lvFechaHoy)                   // Fecha del Archivo            // SS_1147_CQU_20150106
                            +  RellenarCaracter(IntToStr(Trunc(FCantidad)),'0',True,7)  // Total Registros en Archivo   // SS_1147_CQU_20150106
                            //+  RellenarCaracter(IntToStr(Trunc(FMonto)),'0',True,7);    // Deuda Total de Registros   // SS_1147_CQU_20150305  // SS_1147_CQU_20150106
                            +  RellenarCaracter(IntToStr(Trunc(FMonto)),'0',True,17);    // Deuda Total de Registros    // SS_1147_CQU_20150305
            Result := HeaderVespucuio;                                                                                  // SS_1147_CQU_20150106
    end;                                                                                                                // SS_1147_CQU_20150106

    function ArmarLineaVespucio(NumeroConvenio, NumeroComprobante, Monto, RazonSocial,                                  // SS_1147_CQU_20150420
                        Rut, TipoDocumento, NumeroFiscal : string;                                                      // SS_1147_CQU_20150420
                        FechaEmision, FechaVencimiento, FechaAux : TDateTime;                                           // SS_1147_CQU_20150420
                        ReemplazarK : Boolean) : string;                                                                // SS_1147_CQU_20150420
    var                                                                                                                 // SS_1147_CQU_20150420
        Resultado : string;                                                                                             // SS_1147_CQU_20150420
    begin                                                                                                               // SS_1147_CQU_20150420
        if ReemplazarK then begin                                                                                       // SS_1147_CQU_20150420
            if (Pos('K', UpperCase(NumeroConvenio)) > 0 )then                                                           // SS_1147_CQU_20150420
                NumeroConvenio := StringReplace(NumeroConvenio, 'K', '1', [rfReplaceAll, rfIgnoreCase]);                // SS_1147_CQU_20150420
        end;                                                                                                            // SS_1147_CQU_20150420
        Resultado := RellenarCaracter(NumeroFiscal, '0', True, 10)                                                      // SS_1147_CQU_20150420
                        + FormatDateTime('yyyymmdd', FechaEmision)                                                      // SS_1147_CQU_20150420
                        + FormatDateTime('yyyymmdd', FechaVencimiento)                                                  // SS_1147_CQU_20150420
                        + FormatDateTime('yyyymmdd', FechaAux)                                                          // SS_1147_CQU_20150420
                        + RellenarCaracter(Monto, '0', True, 8)                                                         // SS_1147_CQU_20150420
                        + TipoDocumento                                                                                 // SS_1147_CQU_20150420
                        + RellenarCaracter(StrRight(Trim(NumeroConvenio), 12), '0', True, 12)                           // SS_1147_CQU_20150420
                        + RellenarCaracter(Trim(Rut), '0', True, 10);                                                   // SS_1147_CQU_20150420
        Result := Resultado;                                                                                            // SS_1147_CQU_20150420
    end;                                                                                                                // SS_1147_CQU_20150420

resourcestring
	MSG_DATOS_MEMORIA = 'Generando Datos Memoria...';
    MSG_GRABANDO      = 'Grabando Archivo ';
    MSG_LIDER         = 'LIDER: ';
    MSG_SERVIPAG      = 'SERVIPAG: ';
    MSG_MISCUENTAS    = 'MIS CUENTAS: ';
    MSG_SANTANDER     = 'SANTANDER: ';
    MSG_REGISTROS     = 'Total Registros: %.0n';
    MSG_MONTO         = 'Total Monto : %.0n';
    MSG_SENCILLITO    = 'SENCILLITO: ';                            // SS_1147_CQU_20150106
    MSG_UNIMARC       = 'UNIMARC: ';                               // SS_1147_CQU_20150106
var
	Monto, Linea : AnsiString;
//REV.1
    FechaEmision, FechaVencimiento : TDateTime;
    NumeroComprobante, RazonSocial, NumeroConvenio, Rut : string;
    FechaAux : TDateTime;                                          	// SS_1147_CQU_20150106
    LineaVespucio, TipoDocumento : AnsiString;    					// SS_1147_CQU_20150106
    NumeroFiscal : AnsiString;                                      // SS_1147_CQU_20150305
    LineaVespucioSinK : AnsiString;                                 // SS_1147_CQU_20150420
begin
	Result := True;
	MensajeLog(MSG_DATOS_MEMORIA);
    FMonto := 0;
    DBGrid1.DataSource.DataSet.DisableControls;
    //FechaAux   := IncDay(lvFechaHoy, 10);                         // SS_1147_CQU_20150420  // SS_1147_CQU_20150106
    while not DBGrid1.DataSource.DataSet.Eof do begin
    	//REV.1
        NumeroConvenio		:= DBGrid1.Fields[0].AsString;
		NumeroComprobante	:= DBGrid1.Fields[1].AsString;
		Monto				:= DBGrid1.Fields[2].AsString;
		RazonSocial			:= DBGrid1.Fields[3].AsString;
		FechaEmision		:= DBGrid1.Fields[4].AsDateTime;
		FechaVencimiento	:= DBGrid1.Fields[5].AsDateTime;
		Rut					:= DBGrid1.Fields[6].AsString;
        TipoDocumento       := DBGrid1.Fields[7].AsString;          // SS_1147_CQU_20150106
        NumeroFiscal        := DBGrid1.Fields[8].AsString;          // SS_1147_CQU_20150305
        FechaAux            := IncDay(FechaEmision, FDiasTope);     // SS_1147_CQU_20150420
        //FIN REV.1

        FMonto := FMonto + StrToInt(Monto);
        //if FCodigoNativa = CODIGO_VS then begin                                                       // SS_1147_CQU_20150420 // SS_1147_CQU_20150106
        //    //LineaVespucio := RellenarCaracter(NumeroComprobante, '0', True, 10)                     // SS_1147_CQU_20150420 // SS_1147_CQU_20150305  // SS_1147_CQU_20150106
        //    LineaVespucio := RellenarCaracter(NumeroFiscal, '0', True, 10)                            // SS_1147_CQU_20150420 // SS_1147_CQU_20150305
        //                    + FormatDateTime('yyyymmdd', FechaEmision)                                // SS_1147_CQU_20150420 // SS_1147_CQU_20150106
        //                    + FormatDateTime('yyyymmdd', FechaVencimiento)                            // SS_1147_CQU_20150420 // SS_1147_CQU_20150106
        //                    + FormatDateTime('yyyymmdd', FechaAux)                                    // SS_1147_CQU_20150420 // SS_1147_CQU_20150106
        //                    + RellenarCaracter(Monto, '0', True, 8)                                   // SS_1147_CQU_20150420 // SS_1147_CQU_20150106
        //                    + TipoDocumento                                                           // SS_1147_CQU_20150420 // SS_1147_CQU_20150106
        //                    + RellenarCaracter(StrRight(Trim(NumeroConvenio), 12), '0', True, 12)     // SS_1147_CQU_20150420 // SS_1147_CQU_20150106
        //                    //+ RellenarCaracter(Rut, '0', True, 10);                                 // SS_1147_CQU_20150420 // SS_1147_CQU_20150305 // SS_1147_CQU_20150106
        //                    + RellenarCaracter(Trim(Rut), '0', True, 10);                             // SS_1147_CQU_20150420 // SS_1147_CQU_20150305
        //end;                                                                                          // SS_1147_CQU_20150420 // SS_1147_CQU_20150106
        LineaVespucioSinK := ArmarLineaVespucio(NumeroConvenio, NumeroComprobante, Monto,               // SS_1147_CQU_20150420
                                                RazonSocial, Rut, TipoDocumento, NumeroFiscal,          // SS_1147_CQU_20150420
                                                FechaEmision, FechaVencimiento, FechaAux, True);        // SS_1147_CQU_20150420
        LineaVespucio     := ArmarLineaVespucio(NumeroConvenio, NumeroComprobante, Monto,               // SS_1147_CQU_20150420
                                                RazonSocial, Rut, TipoDocumento, NumeroFiscal,          // SS_1147_CQU_20150420
                                                FechaEmision, FechaVencimiento, FechaAux, False);       // SS_1147_CQU_20150420

        if chkNominasLider.Checked then begin        {Lider}
            if FCodigoNativa = CODIGO_VS then                                                       // SS_1147_CQU_20150106
                //Linea := LineaVespucio                                                            // SS_1147_CQU_20150420  // SS_1147_CQU_20150106
                Linea := LineaVespucioSinK                                                          // SS_1147_CQU_20150420
            else                                                                                    // SS_1147_CQU_20150106
            Linea := 	RellenarCaracter(NumeroComprobante,'0',True,9) +	{comprobante}
                        RellenarCaracter(Monto,'0',True,8);					{Monto}
            FListaLider.Add(Linea);
        end;                                                                                        // SS_1147_CQU_20150106
        if chkNominasServipag.Checked or chkNominasMisCuentas.Checked or
            ((FCodigoNativa = CODIGO_VS) and chkNominasUnimarc.Checked) or                          // SS_1147_CQU_20150106
           chkNominasSantander.Checked then begin   {Servipag,  MisCuentas, Santander}
            if FCodigoNativa = CODIGO_VS then                                                       // SS_1147_CQU_20150106
                Linea := LineaVespucio                                                              // SS_1147_CQU_20150106
            else                                                                                    // SS_1147_CQU_20150106
        	Linea :=	RellenarCaracter(NumeroComprobante,'0',True,9) +				{comprobante}
                    	RellenarCaracter(QuitaCaracteres(RazonSocial),' ',False,40) +	{Raz�n Social}
                        FormatDateTime('yyyymmdd', FechaEmision) +      				{Fecha Emision}
                        FormatDateTime('yyyymmdd', FechaVencimiento) +      			{Fecha Vencimiento}
                        RellenarCaracter(' ',' ',True,8) +                              {Fecha Limite Pago}
                        RellenarCaracter(Monto,'0',True,8) +                            {Monto}
                        Copy(NumeroConvenio,1,17) +                         			{Numero Convenio}
                        RellenarCaracter(Trim(Rut),'0',True,10); 						{Rut}

            //if chkNominasServipag.Checked then FListaServipag.Add(Linea);                                 // SS_1147_CQU_20150420
            if chkNominasServipag.Checked then begin                                                        // SS_1147_CQU_20150420
                if FCodigoNativa = CODIGO_VS then                                                           // SS_1147_CQU_20150420
                    FListaServipag.Add(LineaVespucioSinK)                                                   // SS_1147_CQU_20150420
                else                                                                                        // SS_1147_CQU_20150420
                    FListaServipag.Add(Linea);                                                              // SS_1147_CQU_20150420
            end;                                                                                            // SS_1147_CQU_20150420
            //if chkNominasMisCuentas.Checked then FListaMisCuentas.Add(Linea);                             // SS_1147_CQU_20150420
            if chkNominasMisCuentas.Checked then begin                                                      // SS_1147_CQU_20150420
                if FCodigoNativa = CODIGO_VS then                                                           // SS_1147_CQU_20150420
                    FListaMisCuentas.Add(LineaVespucioSinK)                                                 // SS_1147_CQU_20150420
                else                                                                                        // SS_1147_CQU_20150420
                    FListaMisCuentas.Add(Linea);                                                            // SS_1147_CQU_20150420
            end;                                                                                            // SS_1147_CQU_20150420
            if chkNominasSantander.Checked then FListaSantander.Add(Linea);
            //if chkNominasUnimarc.Checked then FListaUnimarc.Add(Linea);                                   // SS_1147_CQU_20150420  // SS_1147_CQU_20150106
            if chkNominasUnimarc.Checked then FListaUnimarc.Add(LineaVespucioSinK);                         // SS_1147_CQU_20150420

        end;

        DBGrid1.DataSource.DataSet.Next;
    end; {while}

    DBGrid1.DataSource.DataSet.First;
    DBGrid1.DataSource.DataSet.EnableControls;

    {grabar en archivo}
    if chkNominasLider.Checked then begin
    	FCantidad := FListaLider.Count;
        MensajeLog(MSG_GRABANDO + MSG_LIDER + FLider_Directorio + FNombreArchivoLider);
        //FListaLider.SaveToFile(FLider_Directorio + FNombreArchivoLider);
        if FCodigoNativa = CODIGO_VS then FListaLider.Insert(0, HeaderVespucio(FListaLider));               // SS_1147_CQU_20150106
        GrabarArchivo(FListaLider, FLider_Directorio, FNombreArchivoLider);
    end;

    if chkNominasServipag.Checked then begin
    	FCantidad := FListaServipag.Count;
        {insertar Header}
        Linea := 	FormatDateTime('yyyymmdd', lvFechaHoy) +                   {fecha}
        			RellenarCaracter(IntToStr(Trunc(FCantidad)),'0',True,7) +  {cantidad}
                    RellenarCaracter(IntToStr(Trunc(FMonto)),'0',True,17)   +  {Monto}
                    RellenarCaracter(' ', ' ', True, 76);                      {Blancos Relleno}
        if FCodigoNativa = CODIGO_VS then FListaServipag.Insert(0, HeaderVespucio(FListaServipag))          // SS_1147_CQU_20150106
        else                                                                                                // SS_1147_CQU_20150106
        FListaServipag.Insert(0, Linea);
        MensajeLog(MSG_GRABANDO + MSG_SERVIPAG + FServipag_Directorio + FNombreArchivoServipag);
        //FListaServipag.SaveToFile(FServipag_Directorio + FNombreArchivoServipag);
        GrabarArchivo(FListaServipag, FServipag_Directorio, FNombreArchivoServipag);
    end;

    if chkNominasMisCuentas.Checked then begin
    	FCantidad := FListaMisCuentas.Count;
        {insertar Header}
        Linea := 	FormatDateTime('yyyymmdd', lvFechaHoy) +                   {fecha}
        			RellenarCaracter(IntToStr(Trunc(FCantidad)),'0',True,7) +  {cantidad}
                    RellenarCaracter(IntToStr(Trunc(FMonto)),'0',True,17)   +  {Monto}
                    RellenarCaracter(' ', ' ', True, 76);                      {Blancos Relleno}
        if FCodigoNativa = CODIGO_VS then FListaMisCuentas.Insert(0, HeaderVespucio(FListaMisCuentas))      // SS_1147_CQU_20150106
        else                                                                                                // SS_1147_CQU_20150106
        FListaMisCuentas.Insert(0, Linea);
        if FCodigoNativa = CODIGO_VS then                                                                   // SS_1147_CQU_20150106
            MensajeLog(MSG_GRABANDO + MSG_SENCILLITO + FMisCuentas_Directorio + FNombreArchivoMisCuentas)   // SS_1147_CQU_20150106
        else                                                                                                // SS_1147_CQU_20150106
        MensajeLog(MSG_GRABANDO + MSG_MISCUENTAS + FMisCuentas_Directorio + FNombreArchivoMisCuentas);
        //FListaMisCuentas.SaveToFile(FMisCuentas_Directorio + FNombreArchivoMisCuentas);
        GrabarArchivo(FListaMisCuentas, FMisCuentas_Directorio, FNombreArchivoMisCuentas);
    end;

    if chkNominasSantander.Checked then begin
    	FCantidad := FListaSantander.Count;
        {insertar Header}
        Linea := 	FormatDateTime('yyyymmdd', lvFechaHoy) +                   {fecha}
        			RellenarCaracter(IntToStr(Trunc(FCantidad)),'0',True,7) +  {cantidad}
                    RellenarCaracter(IntToStr(Trunc(FMonto)),'0',True,17)   +  {Monto}
                    RellenarCaracter(' ', ' ', True, 76);                      {Blancos Relleno}
        if FCodigoNativa = CODIGO_VS then FListaSantander.Insert(0, HeaderVespucio(FListaSantander))        // SS_1147_CQU_20150106
        else                                                                                                // SS_1147_CQU_20150106
        FListaSantander.Insert(0, Linea);
        MensajeLog(MSG_GRABANDO + MSG_SANTANDER + FSantander_Directorio + FNombreArchivoSantander);
        //FListaSantander.SaveToFile(FSantander_Directorio + FNombreArchivoSantander);
        GrabarArchivo(FListaSantander, FSantander_Directorio, FNombreArchivoSantander);
    end;

    if (FCodigoNativa = CODIGO_VS) and chkNominasUnimarc.Checked then begin                                 // SS_1147_CQU_20150106
    	FCantidad := FListaUnimarc.Count;                                                                   // SS_1147_CQU_20150106
        FListaUnimarc.Insert(0, HeaderVespucio(FListaUnimarc));                                             // SS_1147_CQU_20150106
        MensajeLog(MSG_GRABANDO + MSG_UNIMARC + FUnimarc_Directorio + FNombreArchivoUnimarc);               // SS_1147_CQU_20150106
        GrabarArchivo(FListaUnimarc, FUnimarc_Directorio, FNombreArchivoUnimarc);                           // SS_1147_CQU_20150106
    end;                                                                                                    // SS_1147_CQU_20150106

    MensajeLog(Format(MSG_REGISTROS, [FCantidad]));
    MensajeLog(Format(MSG_MONTO, [FMonto]));
    //MostrarLabels(chkNominasLider.Checked, chkNominasServipag.Checked, chkNominasMisCuentas.Checked, chkNominasSantander.Checked);    // SS_1147_CQU_20150106
    MostrarLabels(chkNominasLider.Checked, chkNominasServipag.Checked, chkNominasSantander.Checked, chkNominasMisCuentas.Checked,       // SS_1147_CQU_20150106
                    (chkNominasUnimarc.Checked and (FCodigoNativa = CODIGO_VS)));                                                       // SS_1147_CQU_20150106
end;



{----------------------------------------------------------
  					MostrarReporteDeFinalizacion
  Author: mbecerra
  Date: 18-Julio-2008
  Description:
            	Muestra el reporte de Finalizaci�n
------------------------------------------------------------}
procedure TGeneracionDeNominasForm.MostrarReporteDeFinalizacion;
begin
	try
    	Application.CreateForm(TRptNominasForm, RptNominasForm);
    	RptNominasForm.Inicializar(FCodigoOperacion);
    finally
        RptNominasForm.Release;
    end;
    
end;

{----------------------------------------------------------
  					MuestraLabels
  Author: mbecerra
  Date: 11-Julio-2008
  Description:
            	Muestra u Oculta los Labels.
------------------------------------------------------------}
procedure TGeneracionDeNominasForm.MostrarLabels;
begin
    lblLider.Visible      := lLider;
    lblServipag.Visible   := lServiPag;
    lblSantander.Visible  := lSantander;
    lblMisCuentas.Visible := lMisCuentas;
    lblUnimarc.Visible    := lUnimarc;		// SS_1147_CQU_20150106
end;

procedure TGeneracionDeNominasForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;


{----------------------------------------------------------
  					btnProcesar
  Author: mbecerra
  Date: 11-Julio-2008
  Description:
            	Generar las N�minas.
------------------------------------------------------------}
procedure TGeneracionDeNominasForm.btnProcesarClick(Sender: TObject);
resourcestring
	MSG_ERROR = 'Error en el proceso de generaci�n de N�minas';
    MSG_FIN   = 'Fin del proceso';
begin

    Screen.Cursor := crHourGlass;
    Habilitar(False);
	try
    	Procesar();
	except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
            MensajeLog(MSG_ERROR);
            if FCodigoOperacion > 0 then RegistrarOperacionAlFinal(e.Message); 

    	end;
	end;

    MensajeLog(MSG_FIN);
    Habilitar(True);
	Screen.Cursor := crDefault;

end;

{----------------------------------------------------------
  					RegistrarOperacion
  Author: mbecerra
  Date: 11-Julio-2008
  Description:
            	Registra la Operacion de la interface.
------------------------------------------------------------}
function TGeneracionDeNominasForm.RegistrarOperacion;
resourcestring
  	MSG_ERROR       = 'No se pudo registrar la operaci�n: ';
    MSG_OBSERVACION = 'Generar Archivo Nominas.';

var
    DescError, Archivo : String;
begin
	Archivo := '';
	if chkNominasLider.Checked then Archivo := Archivo + FNombreArchivoLider + ', ';
    if chkNominasServipag.Checked then Archivo := Archivo + FNombreArchivoServipag + ', ';
    if chkNominasMisCuentas.Checked then Archivo := Archivo + FNombreArchivoMisCuentas + ', ';
    if ((FCodigoNativa = CODIGO_VS) and chkNominasUnimarc.Checked) then Archivo:= Archivo + FNombreArchivoUnimarc + ', ';   // SS_1147_CQU_20150106
    if chkNominasSantander.Checked then Archivo := Archivo + FNombreArchivoSantander;

    Result := RegistrarOperacionEnLogInterface(	DMConnections.BaseCAC,
    											RO_MOD_INTERFAZ_SALIENTE_GENERACION_NOMINAS,
                                                Archivo, UsuarioSistema,
                                                MSG_OBSERVACION, False, False,
                                                lvFechaHoy, 0, FCodigoOperacion,
                                                DescError);
    if (not Result) then begin
        MensajeLog(MSG_ERROR + DescError);
    end;

end;

{----------------------------------------------------------
  					RegistrarOperacionAlFinal
  Author: mbecerra
  Date: 11-Julio-2008
  Description:
            	Registra los datos finales del resultado de la operaci�n.
------------------------------------------------------------}
function TGeneracionDeNominasForm.RegistrarOperacionAlFinal;
resourcestring
	MSG_ERROR        = 'No se pudo Actualizar el log de operaciones';
    MSG_PROCESO_NOOK = 'El proceso finaliz� con errores: ';
    MSG_PROCESO_OK   = 'Finaliz� OK';
var
	DescError, Observacion : String;
begin
	try
        if MsgError <> '' then Observacion := MSG_PROCESO_NOOK + MsgError
        else Observacion := MSG_PROCESO_OK;
        
    	Result := ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,
        							FCodigoOperacion,
                                    Trunc(FCantidad),
                                	Trunc(FMonto),
                                    Observacion,
                                    DescError);
        except on e: exception do begin
        		Result := False;
        		MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
            end;
        end;

end;
{----------------------------------------------------------
  					Procesar
  Author: mbecerra
  Date: 11-Julio-2008
  Description:
            	Efect�a el proceso Central de Generaci�n de N�minas
------------------------------------------------------------}
procedure TGeneracionDeNominasForm.Procesar;
begin

    mmoLog.Clear;
    mmoLog.Refresh;
    if not Validaciones() then Exit;
    if not GenerarNombreArchivo() then Exit;
    if not RegistrarOperacion() then Exit;
    if not ObtenerNominas() then Exit;
    if not GenerarNominas() then Exit;
    if not RegistrarOperacionAlFinal('') then Exit;
    MostrarReporteDeFinalizacion();

end;

procedure TGeneracionDeNominasForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	FListaLider.Free;
    FListaServipag.Free;
    FListaMisCuentas.Free;
    FListaSantander.Free;
    FListaUnimarc.Free;                                 // SS_1147_CQU_20150106
    Action := caFree;
end;

procedure TGeneracionDeNominasForm.FormCreate(Sender: TObject);
begin
	FListaLider      := TStringList.Create;
    FListaServipag   := TStringList.Create;
    FListaMisCuentas := TStringList.Create;
    FListaSantander  := TStringList.Create;
    FListaUnimarc    := TStringList.Create;             // SS_1147_CQU_20150106
end;

{----------------------------------------------------------
  Function Inicializar
  Author: mbecerra
  Date: 11-Julio-2008
  Description:
            	Inicializa la interfaz de generaci�n de N�minas.
------------------------------------------------------------}
function TGeneracionDeNominasForm.Inicializar;
begin
	//MostrarLabels(False,False,False,False);           // SS_1147_CQU_20150106
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

	MostrarLabels(False,False,False,False,False);       // SS_1147_CQU_20150106
    mmoLog.Clear;
    FCodigoOperacion := 0;
	Result := True;
    Caption := Titulo;
    FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150106
    if FCodigoNativa = CODIGO_VS then begin             // SS_1147_CQU_20150106
        chkNominasMisCuentas.Caption := 'Sencillito';   // SS_1147_CQU_20150106
        chkNominasUnimarc.Visible := True;              // SS_1147_CQU_20150106
        chkNominasUnimarc.Enabled := True;              // SS_1147_CQU_20150106
    end else begin                                      // SS_1147_CQU_20150106
        chkNominasMisCuentas.Caption := 'MisCuentas';   // SS_1147_CQU_20150106
        chkNominasUnimarc.Visible := False;             // SS_1147_CQU_20150106
        chkNominasUnimarc.Enabled := False;             // SS_1147_CQU_20150106
    end;                                                // SS_1147_CQU_20150106
end;


end.
