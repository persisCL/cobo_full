{
    Revision: 1
    Author: pdominguez
    Date: 16/05/2010

    Description: SS 846 - Interfaces - Canbio Persona RNVM
        -  Se modifica la Query qryPersonasRNVM para que devuelva el CodigoPersonaRNVM
        m�s actual, asociado al RUT solicitado.
        - Se modifican los procedimientos y/o funciones:
            btnActualizaRNVMClick,
            dbl_InfraccionesDrawText,
            btnBuscarClick

    Revision: 2
    Author: pdominguez
    Date: 20/10/2010 - 22/10/2010

    Description: SS 929 - Interfaces - Error Cambio Persona RNVM
        - Se crea el procedimiento:
            txt_PatenteChange
        - Se modifica el procedimiento:
            btnBuscarClick
            btnActualizaRNVMClick

    Firma       : SS_1147_NDR_20141216
    Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

}


unit frmCambioPersonaRNVM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, DB, ADODB, ListBoxEx, DBListEx,DateUtils,
  UtilProc, StrUtils,
  util,                                    //Unit de Login de las Aplicaciones del Proyecto Oriente-Poniente.
  PeaProcs,
  DMConnection, PeaProcsCN, SysUtilsCN, VariantComboBox;

type
  TFCambioPersonaRNVM = class(TForm)
    gbRangoFechas: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    deFDesde: TDateEdit;
    deFHasta: TDateEdit;
    lblPatente: TLabel;
    txt_Patente: TEdit;
    txt_rut: TEdit;
    lblRUT: TLabel;
    txt_nombre: TEdit;
    lbl_nombre: TLabel;
    btnBuscar: TButton;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    btnActualizaRNVM: TButton;
    dbl_Infracciones: TDBListEx;
    spObtenerInfraccionesAFacturar: TADOStoredProc;
    dsObtenerInfraccionesAFacturar: TDataSource;
    spCambioPersonaRNVMa: TADOStoredProc;
    spObtenerDatosCambioPersonaRNVMa: TADOStoredProc;
    txt_Direccion: TEdit;
    Label1: TLabel;
    Label4: TLabel;
    cbConcesionaria: TVariantComboBox;
    procedure btnBuscarClick(Sender: TObject);
    procedure btnActualizaRNVMClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbl_InfraccionesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure txt_PatenteChange(Sender: TObject);
    procedure cbConcesionariaChange(Sender: TObject);
  private
    { Private declarations }
     FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    Function Inicializar : Boolean;
    { Public declarations }
  end;

var
  FCambioPersonaRNVM: TFCambioPersonaRNVM;

implementation

{$R *.dfm}

{ TFCambioPersonaRNVM }

{
    Revision: 1
    Author: pdominguez
    Date: 16/05/2010
    Description: SS 846 - Interfaces - Cambio Persona RNVM
        - Se a�ade un mensaje de finalizaci�n existosa del proceso.

    Revision: 2
    Author: pdominguez
    Date: 22/10/2010
    Description: SS 929 - Interfaces - Error Cambio Persona RNVM
        - Se valida de nuevo las infracciones a procesar antes de lanzar el proceso,
        para verificar que la selecci�n inicial no vari�.

    Firma : SS_929_PDO_20110125
    Author : pdominguez
    Date   : 25/01/2011
    Description : SS 929 - Interfaces - Error Cambio Persona RNVM
        - Se valida la existencia de la Comuna, para el registro de Consulta RNVM seleccionado,
        seg�n la Persona RNVM a cambiar.
}
procedure TFCambioPersonaRNVM.btnActualizaRNVMClick(Sender: TObject);
resourcestring
    DESEA_CAMBIAR_PERSONA_RNVM = '� Desea actualizar la consulta al RNVM de las infracciones con la persona seleccionada ?';
    CAPTION_CAMBIAR_PERSONA_RNVM = 'Cambio de Persona RNVM';
    MSG_PROCESO_CON_EXITO = 'Se Realiz� el Cambio de Persona RNVM solicitado sin Errores.'; // Rev. 1 (SS 846)
    MSG_PROCESO_CON_EXITO_CAPTION = 'Cambio Persona RNVM'; // Rev. 1 (SS 846)
begin
    btnBuscarClick(Nil); // Rev. 2 SS_929_20101022

    if btnActualizaRNVM.Enabled then // Rev. 2 SS_929_20101022
        if ShowMsgBoxCN(CAPTION_CAMBIAR_PERSONA_RNVM, DESEA_CAMBIAR_PERSONA_RNVM, MB_ICONQUESTION, Self) = mrOk then try
            Screen.Cursor:=crHourGlass;
            Application.ProcessMessages; // Rev. 1 (SS 846)

            try
                With spCambioPersonaRNVMa do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@Patente').Value             := txt_Patente.Text;
                    Parameters.ParamByName('@FechaDesde').Value          := deFDesde.Date;
                    Parameters.ParamByName('@FechaHasta').Value          := deFHasta.Date;
//                    Parameters.ParamByName('@CodigoConsultaRNVM').Value := qryPersonasRNVM.FieldByName('CodigoConsultaRNVM').AsInteger; // Rev. 1 (SS 846)
                    Parameters.ParamByName('@CodigoConsultaRNVM').Value  := spObtenerDatosCambioPersonaRNVMa.FieldByName('CodigoConsultaRNVM').AsInteger; // SS_929_PDO_20110125
                    Parameters.ParamByName('@DescripcionComuna').Value   := iif(spObtenerDatosCambioPersonaRNVMa.FieldByName('DescripcionComuna').AsString = EmptyStr, Null, spObtenerDatosCambioPersonaRNVMa.FieldByName('DescripcionComuna').AsString);  // SS_929_PDO_20110125
                    Parameters.ParamByName('@Usuario').Value             := UsuarioSistema;
                    Parameters.ParamByName('@CodigoConcesionaria').Value := cbConcesionaria.Value;
                    ExecProc;
                    btnBuscar.Click;
                end;

                ShowMsgBoxCN(MSG_PROCESO_CON_EXITO_CAPTION, MSG_PROCESO_CON_EXITO, MB_ICONINFORMATION, Self); // Rev. 1 (SS 846)
            except
                on e: Exception do ShowMsgBoxCN(e, Self);
            end;
        finally
            Screen.Cursor := crDefault;
            Application.ProcessMessages; // Rev. 1 (SS 846)
        end;
end;

{
    Revision: 1
    Author: pdominguez
    Date: 14/06/2010
    Description: SS 846 - Interfaces - Cambio Persona RNVM
        - Se cambian los Exits por raises de excepciones propietarias.
        - Se implementa la validaci�n y b�squeda del RUT.
        - Se cambian los MsgBox y MsgBoxErr por ShowMsgBoxCN.

    Revision: 2
    Author: pdominguez
    Date: 21/10/2010
    Description: SS 929 - Interfaces - Error Cambio Persona RNVM 
        - Se controla que al menos haya una infracci�n facturable dentro
        de la selecci�n para poder procesar el cambio.      
}
procedure TFCambioPersonaRNVM.btnBuscarClick(Sender: TObject);
    resourcestring
        rsTituloValidaciones        = 'Validaci�n Datos B�squeda';
        rsErrorControl_deFDesde     = 'La Fecha Desde debe ser menor o igual que la Fecha Hasta.';
        rsErrorControl_txt_Patente1 = 'La Patente es un campo obligatorio.';
        rsErrorControl_txt_Patente2 = 'El Formato de la Patente no es v�lido.';
        rsErrorControl_txt_rut1     = 'El RUT es un campo obligatorio.';
        rsErrorControl_txt_rut2     = 'El RUT introducido NO es correcto.';

    var
        TodoFacturado: Boolean; // SS_929_20101021

    function ValidarCondiciones: boolean;
        var
            vControles  : Array [1..5] of TControl;
            vCondiciones: Array [1..5] of Boolean;
            vMensajes   : Array [1..5] of String;
    begin

        vControles[1]   := txt_Patente;
        vControles[2]   := txt_Patente;
        vControles[3]   := deFDesde;
        vControles[4]   := txt_rut;
        vControles[5]   := txt_rut;

        vCondiciones[1] := (Trim(txt_Patente.Text) <> '');
        vCondiciones[2] := (Trim(txt_Patente.Text) = '') or ((Trim(txt_Patente.Text) <> '') and (EsFormatoPatenteValido(Trim(txt_Patente.Text))));
        vCondiciones[3] := (deFDesde.Date < deFHasta.Date);
        vCondiciones[4] := Trim(txt_rut.Text) <> '';
        vCondiciones[5] := (Trim(txt_rut.Text) = '') or ((Trim(txt_rut.Text) <> '') and (ValidarRUT(DMConnections.BaseCAC, RightStr('000000000'+Trim(txt_rut.Text),9))));

        vMensajes[1]    := rsErrorControl_txt_Patente1;
        vMensajes[2]    := rsErrorControl_txt_Patente2;
        vMensajes[3]    := rsErrorControl_deFDesde;
        vMensajes[4]    := rsErrorControl_txt_rut1;
        vMensajes[5]    := rsErrorControl_txt_rut2;

        Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);

    end;
begin
    try
        try
            Screen.Cursor:=crHourGlass;
            Application.ProcessMessages;

            if  ValidarCondiciones then begin

                With spObtenerDatosCambioPersonaRNVMa do begin                                                          // SS_929_PDO_20110125
                    if Active then Close;                                                                               // SS_929_PDO_20110125
                    Parameters.Refresh;                                                                                 // SS_929_PDO_20110125
                    Parameters.ParamByName('@NumeroDocumento').Value := RightStr('000000000'+Trim(txt_rut.Text),9);     // SS_929_PDO_20110125
                    Open;                                                                                               // SS_929_PDO_20110125

                    if IsEmpty then begin                                                                               // SS_929_PDO_20110125
                        txt_nombre.Text    := 'Rut No Encontrado';
                        txt_Direccion.Text := EmptyStr;                                                                 // SS_929_PDO_20110125
                    end                                                                                                 // SS_929_PDO_20110125
                    else begin                                                                                          // SS_929_PDO_20110125
                        txt_nombre.Text    := FieldByName('Nombre').AsString ;
                        txt_Direccion.Text := FieldByName('Direccion').AsString;                                        // SS_929_PDO_20110125
                    end;                                                                                                // SS_929_PDO_20110125
                end;

                With spObtenerInfraccionesAFacturar do begin
                    if Active then Close;
                    try // SS_929_20101021
                        dsObtenerInfraccionesAFacturar.DataSet := Nil;

                        Parameters.Refresh;
                        Parameters.ParamByName('@Patente').Value                    := txt_Patente.Text;
                        Parameters.ParamByName('@FechaInicial').Value               := deFDesde.Date;
                        Parameters.ParamByName('@FechaFinal').Value                 := deFHasta.Date;
                        Parameters.ParamByName('@SePuedeEmitirCertificado').Value   := Null;
                        Parameters.ParamByName('@ValidarInfraccionesImpagas').Value := Null;
                        Parameters.ParamByName('@SePuedeEmitirCertificado').Value   := Null;
                        Parameters.ParamByName('@CodigoConcesionaria').Value        := cbConcesionaria.Value;
                        Parameters.ParamByName('@CambioPersonaRNVM').Value          := 1;
                        Open;

                        TodoFacturado := True;

                        while (not Eof) and TodoFacturado do begin
                            TodoFacturado := TodoFacturado and (FieldByName('EsFacturable').AsInteger = 0);
                            Next;
                        end;

                        First;
                    finally
                        dsObtenerInfraccionesAFacturar.DataSet := spObtenerInfraccionesAFacturar;
                    end; // Fin Rev. SS_929_20101021
                end;

                Screen.Cursor := crDefault;
                Application.ProcessMessages;

                if spObtenerInfraccionesAFacturar.IsEmpty AND spObtenerDatosCambioPersonaRNVMa.IsEmpty then
                    ShowMsgBoxCN(' Resultado B�squeda', 'No se encontraron infracciones para esa patente en ese periodo.' + CRLF + 'El RUT ingresado no est� registrado en el RNVM', MB_ICONINFORMATION, Self)
                else if spObtenerInfraccionesAFacturar.IsEmpty then
                    ShowMsgBoxCN(' Resultado B�squeda', 'No se encontraron infracciones para esa patente en ese periodo.', MB_ICONINFORMATION, Self)
                else if spObtenerInfraccionesAFacturar.IsEmpty then
                    ShowMsgBoxCN(' Resultado B�squeda', 'El RUT ingresado no est� registrado en el RNVM.', MB_ICONINFORMATION, Self)
                else if TodoFacturado then // SS_929_20101021
                    ShowMsgBoxCN(' Resultado B�squeda', 'No es posible realizar ning�n cambio, pues ninguna Infracci�n de la b�squeda es Facturable.', MB_ICONINFORMATION, Self) // SS_929_20101021
                else if (spObtenerDatosCambioPersonaRNVMa.FieldByName('DescripcionComuna').IsNull) and (spObtenerDatosCambioPersonaRNVMa.FieldByName('ComunaDomicilio').AsString = 'SIN DETERMINAR') then // SS_929_20110125
                    ShowMsgBoxCN(' Resultado B�squeda', 'No es posible realizar ning�n cambio, pues la Direcci�n NO tiene Comuna.', MB_ICONINFORMATION, Self); // SS_929_20110125
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, self);
            end;
        end;
    finally
      Screen.Cursor:=crDefault;
      Application.ProcessMessages;

      btnActualizaRNVM.Enabled :=
        Not
            (
                spObtenerInfraccionesAFacturar.IsEmpty or
                spObtenerDatosCambioPersonaRNVMa.IsEmpty or
                TodoFacturado or
                (spObtenerDatosCambioPersonaRNVMa.FieldByName('DescripcionComuna').IsNull and // SS_929_20110125
                (spObtenerDatosCambioPersonaRNVMa.FieldByName('ComunaDomicilio').AsString = 'SIN DETERMINAR')) // SS_929_20110125
            ); // SS_929_20101021
    end;
end;

{
    Revision: 1
    Author: pdominguez
    Date: 08/06/2010
    Description: SS 846 - Interfaces - Cambio Persona RNVM
        - Se incorpora la visualizaci�n por colores para diferenciar las infracciones facturadas.
}
procedure TFCambioPersonaRNVM.dbl_InfraccionesDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    with spObtenerInfraccionesAFacturar do
        if Active then begin
    		if FieldByName('EsFacturable').AsInteger = 0 then
               	Sender.Canvas.Font.Color := clRed
        end;
end;

procedure TFCambioPersonaRNVM.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

function TFCambioPersonaRNVM.Inicializar: Boolean;
begin
    try
        Result := False;

        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216


        txt_Patente.Text   :='';
        txt_nombre.Text    :='';
        txt_rut.Text       :='';
        txt_Direccion.Text := EmptyStr; // SS_929_20110125

        deFDesde.Date := StartOfTheMonth(Now());
        deFHasta.Date := NowBaseCN(DMConnections.BaseCAC);

        btnActualizaRNVM.Enabled := False;
        CargarComboConcesionarias(DMConnections.BaseCAC, cbConcesionaria, False);
        Result := True;
    except
        on e:Exception do begin
            ShowMsgBoxCN(e, Self);
        end;
    end;
end;
{
    Revision: 2
    Author: pdominguez
    Date: 20/10/2010

    Description: SS 929 - Interfaces - Error Cambio Persona RNVM
        - Se asocia el evento de cambio a los objetos txt_Patente, deFDesde,
        deFHasta y txtRut, para borrar los resultados de b�squeda en caso
        de que se modifique alguno de los par�metros de entrada.
}
procedure TFCambioPersonaRNVM.txt_PatenteChange(Sender: TObject);
begin
    if spObtenerInfraccionesAFacturar.Active then spObtenerInfraccionesAFacturar.Close;
    if spObtenerDatosCambioPersonaRNVMa.Active then spObtenerDatosCambioPersonaRNVMa.Close;
    txt_nombre.Text := EmptyStr;
    txt_Direccion.Text := EmptyStr; // SS_929_20110125
    btnActualizaRNVM.Enabled := False;
end;

procedure TFCambioPersonaRNVM.cbConcesionariaChange(Sender: TObject);
begin
    if spObtenerInfraccionesAFacturar.Active then spObtenerInfraccionesAFacturar.Close;
    btnActualizaRNVM.Enabled := False;
end;

end.
