{-------------------------------------------------------------------------------
 Revision : 1
 Author : Nelson Droguett Sierra
 Date : 11-Diciembre-2009
 Description : Agregar resumen por concepto
-------------------------------------------------------------------------------}
unit frmVerLibroDePeajes;

interface

uses
	UtilProc, Util,
    ComunInterfaces,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, DB, DBClient, ExtCtrls, ComCtrls, DbList,
  Abm_obj, ppDB, ppTxPipe, ppParameter, ppCtrls, ppBands, ppClass, ppPrnabl,
  ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB, ppStrtch, ppSubRpt, ppVar;

type
  TVerLibroDePeajesForm = class(TForm)
    Panel1: TPanel;
    btnLeer: TButton;
    Panel2: TPanel;
    btnSalir: TButton;
    btnImprimir: TButton;
    odlgArchivo: TOpenDialog;
    pcArchivoResumen: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    lstvResumen: TListView;
    lblArchivo: TLabel;
    mmoData: TMemo;
    lblCargando: TLabel;
    rbiLibroPeajes: TRBInterface;
    rptLibroPeajes: TppReport;
    gr: TppHeaderBand;
    lblTitLibroPeaje: TppLabel;
    ppNombreArchivo: TppLabel;
    ppDetailBand1: TppDetailBand;
    dbt_TipoComprobante: TppDBText;
    dbt_FechaEmision: TppDBText;
    dbt_NumeroComprobante: TppDBText;
    dbt_Cliente: TppDBText;
    dbt_RUT: TppDBText;
    dbt_TotalPeajes: TppDBText;
    dbt_Intereses: TppDBText;
    dbt_GastosAdministrativos: TppDBText;
    dbt_Descuentos: TppDBText;
    dbt_OtrosCargos: TppDBText;
    dbt_AjusteSencilloMesActual: TppDBText;
    dbt_AjusteSencillomesAnterior: TppDBText;
    dbt_TotalAPagar: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppParameterList1: TppParameterList;
    ppTextPipeline: TppTextPipeline;
    ppTextPipelineppField1: TppField;
    ppTextPipelineppField2: TppField;
    ppTextPipelineppField3: TppField;
    ppTextPipelineppField4: TppField;
    ppTextPipelineppField5: TppField;
    ppTextPipelineppField6: TppField;
    ppTextPipelineppField7: TppField;
    ppTextPipelineppField8: TppField;
    ppTextPipelineppField9: TppField;
    ppTextPipelineppField10: TppField;
    ppTextPipelineppField11: TppField;
    ppTextPipelineppField12: TppField;
    ppTextPipelineppField13: TppField;
    ppGroup1: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppTitleBand1: TppTitleBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppLabel1: TppLabel;
    ppArchivoTitulo: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel16: TppLabel;
    ppLabel18: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppTextResumen: TppTextPipeline;
    ppTextResumenppField1: TppField;
    ppTextResumenppField2: TppField;
    ppTextResumenppField3: TppField;
    ppTextResumenppField4: TppField;
    ppTextResumenppField5: TppField;
    ppTextResumenppField6: TppField;
    ppTextResumenppField7: TppField;
    ppTextResumenppField8: TppField;
    ppDBText11: TppDBText;
    ppTextResumenppField9: TppField;
    ppTextResumenppField10: TppField;
    ppTextResumenppField11: TppField;
    ppTitleBand2: TppTitleBand;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    ppLine2: TppLine;
    ppLabel32: TppLabel;
    ppDBText12: TppDBText;
    ppTextResumenppField12: TppField;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel8: TppLabel;
    ppLabel11: TppLabel;
    ppLabel15: TppLabel;
    ppLabel17: TppLabel;
    lblFecha: TLabel;
    ppLine3: TppLine;
    ppGPeajes: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppGInter: TppLabel;
    ppGGastos: TppLabel;
    ppGDesc: TppLabel;
    ppGCargos: TppLabel;
    ppGActual: TppLabel;
    ppGAnte: TppLabel;
    ppGTotal: TppLabel;
    ppDBText13: TppDBText;
    ppTPeajes: TppLabel;
    ppTInter: TppLabel;
    ppTGastos: TppLabel;
    ppTDesc: TppLabel;
    ppTCargos: TppLabel;
    ppTActual: TppLabel;
    ppTAnte: TppLabel;
    ppTTotal: TppLabel;
    ppLabel33: TppLabel;
    ppLabel34: TppLabel;
    ppManualGlosa: TppLabel;
    ppManualCount: TppLabel;
    ppManualPeajes: TppLabel;
    ppManualInter: TppLabel;
    ppManualGastos: TppLabel;
    ppManualDesc: TppLabel;
    ppManualCargos: TppLabel;
    ppManualActual: TppLabel;
    ppManualAnte: TppLabel;
    ppManualTotal: TppLabel;
    ppMasivaGlosa: TppLabel;
    ppMasivaCount: TppLabel;
    ppMasivaPeajes: TppLabel;
    ppMasivaInter: TppLabel;
    ppMasivaGastos: TppLabel;
    ppMasivaDesc: TppLabel;
    ppMasivaCargos: TppLabel;
    ppMasivaActual: TppLabel;
    ppMasivaAnte: TppLabel;
    ppMasivaTotal: TppLabel;
    TabSheet3: TTabSheet;
    lstvResumenConcepto: TListView;
    ppTextResumenConcepto: TppTextPipeline;
    ppTextPipeline1ppField1: TppField;
    ppTextPipeline1ppField2: TppField;
    ppTextPipeline1ppField3: TppField;
    ppTextPipeline1ppField4: TppField;
    ppTextPipeline1ppField5: TppField;
    memoResumen: TMemo;
    memoResumenConcepto: TMemo;
    procedure btnSalirClick(Sender: TObject);
    procedure btnLeerClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure rbiLibroPeajesExecute(Sender: TObject; var Cancelled: Boolean);
    procedure ppGroupFooterBand2BeforePrint(Sender: TObject);
    procedure ppSummaryBand1BeforePrint(Sender: TObject);
  private
    { Private declarations }
    FArchivo, FResumen, FResumenConcepto : TStringList;
  public
    { Public declarations }
    function Inicializar(sCaption : string) : boolean;
    procedure CargarLibroDePeaje(Archivo : string);
    function MostrarResumen : integer;
  end;

const
	SeparadorCampo = #9;
    SeparadorResumen = #15;
    SeparadorMasivoManual = '-----';
    IndicadorMasivo = 'NK MASIVA';
    IndicadorManual = 'NK MANUAL';
    IndicadorDeTotal = 'TOTALES';
    NumeroDeCamposEnArchivo = 13;
    cArchivoParaImprimir = 'C:\_LAP_Completo.txt';
    cResumenParaImprimir = 'C:\_RES_LAP.txt';

var
  VerLibroDePeajesForm: TVerLibroDePeajesForm;

implementation

{$R *.dfm}

function BuscaPosicion(Tipo : string; Lista : TStringList) : string;
const
	cPosTipo = 1;

var
	Posicion, i : Integer;
begin
	{buscar los totales del tipo}
    Posicion := -1;
    i := 0;
    while (Posicion < 0) and (i < Lista.Count) do begin
        if Tipo = GetItem(Lista[i], cPosTipo, SeparadorCampo) then Posicion := i;

    	Inc(i);
    end;

    if Posicion >= 0 then Result := Lista[Posicion]
    else Result := '';

end;
{-------------------------------------------------------------------------------
 					MostrarResumen
 Author : mbecerra
 Date : 19-Agosto-2008
 Description : Muestra el Resumen que est� contenido en el archivo de Libro de Peajes
            	Retorna la fila + 1 en que se encontr� el Separador de Total
                -1 si no lo encontr�
-------------------------------------------------------------------------------}
function TVerLibroDePeajesForm.MostrarResumen;
var
	i, j : integer;
    Item : TListItem;
    Linea, Campo : string;
    bResumen,bResumenConcepto:Boolean;
begin
    bResumen:=True;
    bResumenConcepto:=False;

	FResumen.Clear;
	lstvResumen.Clear;
    // Rev.1 / 11-Diciembre-2009 / Nelson Droguett Sierra -----------------------------------
    FResumenConcepto.Clear;
    lstvResumenConcepto.Clear;
    // --------------------------------------------------------------------------------------

    Result := -1;
    {la primera l�nea del archivo debe ser la fecha}
    if mmoData.Lines.Count > 0 then lblFecha.Caption := mmoData.Lines[0];

    {las siguientes, hasta el separador de resumen, deben contener los totales}
    i := 1;
    while i < mmoData.Lines.Count do begin
    	Linea := mmoData.Lines[i];
        if bResumen then begin
          if (Linea <> SeparadorResumen) then begin
              Linea := Trim(Linea);
              if Linea <> '' then begin
                  memoResumen.Lines.Add(Linea);
                  FResumen.Add(Linea);
                  Campo := GetItem(Linea,1,SeparadorCampo);  	   {Tipo comprobante}
                  Item := lstvResumen.Items.Add;
                  Item.Caption := Campo;
                  for j := 2 to NumeroDeCamposEnArchivo do begin {Resto de los campos}
                      Campo := GetItem(Linea,j,SeparadorCampo);
                      Item.SubItems.Add(GetItem(Linea,j,SeparadorCampo));
                  end;
              end;
          end
          else begin
              bResumen:=False;
              bResumenConcepto:=True;
              Result := i + 1;
              Inc(i);
              Linea := '';
          end;
        end;
        if bResumenConcepto then begin
          if (Linea <> SeparadorResumen) then begin
              Linea := Trim(Linea);
              if Linea <> '' then begin
                  FResumenConcepto.Add(Linea);
                  memoResumenConcepto.Lines.Add(Linea);
                  Campo := GetItem(Linea,1,SeparadorCampo);  	   {Tipo comprobante}
                  Item := lstvResumenConcepto.Items.Add;
                  Item.Caption := Campo;
                  for j := 2 to 7 do begin {Resto de los campos}
                      Campo := GetItem(Linea,j,SeparadorCampo);
                      Item.SubItems.Add(GetItem(Linea,j,SeparadorCampo));
                  end;
              end;
          end
          else begin
              bResumen:=False;
              bResumenConcepto:=True;
              Result := i + 1;
              i := mmoData.Lines.Count; 	{salir}
          end;
        end;
        Inc(i);
    end;

end;

{-------------------------------------------------------------------------------
 					ppGroupFooterBand2BeforePrint
 Author : mbecerra
 Date : 06-Noviembre-2008
 Description:	Busca los totales para el tipo en el resumen.
            	As� no se retrasa la ejecuci�n del reporte al tener que calcular.
-------------------------------------------------------------------------------}
procedure TVerLibroDePeajesForm.ppGroupFooterBand2BeforePrint(Sender: TObject);
const
	cPosPeaje = 5;
var
	Linea, Tipo : string;
begin
	Tipo := Trim(ppTextPipeline.GetFieldValue('Tipo'));
    Linea := BuscaPosicion(Tipo, FResumen);
    if Linea <> '' then begin
    	{coloca los totales del resumen, en la banda}
    	ppGPeajes.Caption	:= GetItem(Linea, cPosPeaje, SeparadorCampo);
    	ppGInter.Caption    := GetItem(Linea, cPosPeaje + 1, SeparadorCampo);
    	ppGGastos.Caption	:= GetItem(Linea, cPosPeaje + 2, SeparadorCampo);
    	ppGDesc.Caption		:= GetItem(Linea, cPosPeaje + 3, SeparadorCampo);
    	ppGCargos.Caption	:= GetItem(Linea, cPosPeaje + 4, SeparadorCampo);
    	ppGActual.Caption	:= GetItem(Linea, cPosPeaje + 5, SeparadorCampo);
    	ppGAnte.Caption		:= GetItem(Linea, cPosPeaje + 6, SeparadorCampo);
    	ppGTotal.Caption	:= GetItem(Linea, cPosPeaje + 7, SeparadorCampo);
    end;

    {si el Tipo es NK, entonces colocar las NK Masivas y Manuales, de lo contrario NO}
    if UpperCase(Tipo) = 'NK' then begin
        Linea := BuscaPosicion(IndicadorMasivo, FResumen);
        if Linea <> '' then begin
            ppMasivaCount.Caption	:= GetItem(Linea, cPosPeaje - 1, SeparadorCampo);
            ppMasivaPeajes.Caption	:= GetItem(Linea, cPosPeaje, SeparadorCampo);
    		ppMasivaInter.Caption	:= GetItem(Linea, cPosPeaje + 1, SeparadorCampo);
    		ppMasivaGastos.Caption	:= GetItem(Linea, cPosPeaje + 2, SeparadorCampo);
    		ppMasivaDesc.Caption	:= GetItem(Linea, cPosPeaje + 3, SeparadorCampo);
    		ppMasivaCargos.Caption	:= GetItem(Linea, cPosPeaje + 4, SeparadorCampo);
    		ppMasivaActual.Caption	:= GetItem(Linea, cPosPeaje + 5, SeparadorCampo);
    		ppMasivaAnte.Caption	:= GetItem(Linea, cPosPeaje + 6, SeparadorCampo);
    		ppMasivaTotal.Caption	:= GetItem(Linea, cPosPeaje + 7, SeparadorCampo);
        end;

        Linea := BuscaPosicion(IndicadorManual, FResumen);
        if Linea <> '' then begin
            ppManualCount.Caption	:= GetItem(Linea, cPosPeaje - 1, SeparadorCampo);
            ppManualPeajes.Caption	:= GetItem(Linea, cPosPeaje, SeparadorCampo);
    		ppManualInter.Caption	:= GetItem(Linea, cPosPeaje + 1, SeparadorCampo);
    		ppManualGastos.Caption	:= GetItem(Linea, cPosPeaje + 2, SeparadorCampo);
    		ppManualDesc.Caption	:= GetItem(Linea, cPosPeaje + 3, SeparadorCampo);
    		ppManualCargos.Caption	:= GetItem(Linea, cPosPeaje + 4, SeparadorCampo);
    		ppManualActual.Caption	:= GetItem(Linea, cPosPeaje + 5, SeparadorCampo);
    		ppManualAnte.Caption	:= GetItem(Linea, cPosPeaje + 6, SeparadorCampo);
    		ppManualTotal.Caption	:= GetItem(Linea, cPosPeaje + 7, SeparadorCampo);
        end;

    end
    else begin
        ppMasivaGlosa.Visible  := False;
        ppMasivaCount.Visible  := False;
        ppMasivaPeajes.Visible := False;
        ppMasivaInter.Visible  := False;
        ppMasivaGastos.Visible := False;
        ppMasivaDesc.Visible   := False;
        ppMasivaCargos.Visible := False;
        ppMasivaActual.Visible := False;
        ppMasivaAnte.Visible   := False;
        ppMasivaTotal.Visible  := False;

        ppManualGlosa.Visible  := False;
        ppManualCount.Visible  := False;
        ppManualPeajes.Visible := False;
        ppManualInter.Visible  := False;
        ppManualGastos.Visible := False;
        ppManualDesc.Visible   := False;
        ppManualCargos.Visible := False;
        ppManualActual.Visible := False;
        ppManualAnte.Visible   := False;
        ppManualTotal.Visible  := False;
    end;

end;

{-------------------------------------------------------------------------------
 					ppSummaryBand1BeforePrint
 Author : mbecerra
 Date : 06-Noviembre-2008
 Description:	Imprime el Grand Total seg�n lo almacenado en el Resumen.
            	La �ltima l�nea debe contener la sumatoria de los totales
-------------------------------------------------------------------------------}
procedure TVerLibroDePeajesForm.ppSummaryBand1BeforePrint(Sender: TObject);
const
	cPosPeaje = 5;
var
	Linea : string;

begin
	Linea := FResumen[Fresumen.Count - 1];
    {coloca los totales generales en la banda}
    ppTPeajes.Caption	:= GetItem(Linea, cPosPeaje, SeparadorCampo);
    ppTInter.Caption    := GetItem(Linea, cPosPeaje + 1, SeparadorCampo);
    ppTGastos.Caption	:= GetItem(Linea, cPosPeaje + 2, SeparadorCampo);
    ppTDesc.Caption		:= GetItem(Linea, cPosPeaje + 3, SeparadorCampo);
    ppTCargos.Caption	:= GetItem(Linea, cPosPeaje + 4, SeparadorCampo);
    ppTActual.Caption	:= GetItem(Linea, cPosPeaje + 5, SeparadorCampo);
    ppTAnte.Caption		:= GetItem(Linea, cPosPeaje + 6, SeparadorCampo);
    ppTTotal.Caption	:= GetItem(Linea, cPosPeaje + 7, SeparadorCampo);
end;

{-------------------------------------------------------------------------------
 					rbiLibroPeajesExecute
 Author : mbecerra
 Date : 19-Agosto-2008
 Description : Imprime el Reporte.
-------------------------------------------------------------------------------}
procedure TVerLibroDePeajesForm.rbiLibroPeajesExecute(Sender: TObject;
  var Cancelled: Boolean);
var
	ListaAux : TStringList;
    i : integer;
begin
	{quitarle al resumen las NK masivas y Manuales}
    ListaAux := TStringList.Create;
    i := 0;
    while (i < FResumen.Count) and (FResumen[i] <> SeparadorMasivoManual) do begin
        ListaAux.Add(FResumen[i]);
    	Inc(i);
    end;

    {Grabar los archivos}
    FArchivo.SaveToFile(cArchivoParaImprimir);
    ListaAux.SaveToFile(cResumenParaImprimir);

    {asignarlos a los Pipelines}
    ppTextPipeline.FileName := cArchivoParaImprimir;
    ppTextResumen.FileName  := cResumenParaImprimir;
    {indicar los t�tulos}
    ppNombreArchivo.Caption := lblFecha.Caption; //lblArchivo.Caption;
    ppArchivoTitulo.Caption := lblFecha.Caption; //lblArchivo.Caption;

    ListaAux.Free;
end;

{-------------------------------------------------------------------------------
 					CargarLibroDePeaje
 Author : mbecerra
 Date : 19-Agosto-2008
 Description : Carga el archivo del libro de peajes en pantalla
-------------------------------------------------------------------------------}
procedure TVerLibroDePeajesForm.CargarLibroDePeaje;
resourcestring
    MSG_ERROR		= 'Ocurri� un error durante la carga del archivo del Libro de Peajes';
    MSG_NO_RESUMEN 	= 'No se encontr� el resumen en el archivo';

var
    Indice : integer;
begin
	try
        mmoData.Lines.LoadFromFile(Archivo);
        Indice := MostrarResumen();
        if Indice >= 0 then begin
        	while Indice > 0 do begin
            	mmoData.Lines.Delete(0);
                Dec(Indice);
            end;

        end
        else MsgBox(MSG_NO_RESUMEN, Caption, MB_ICONEXCLAMATION);

 //       FArchivo.Clear;
 //       FArchivo.AddStrings(mmoData.Lines);			{cargar el archivo}
 //       FArchivo.Delete(0);                     	{borrar los t�tulos}

    except on e:exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;


{-------------------------------------------------------------------------------
 					Inicializar
 Author : mbecerra
 Date : 19-Agosto-2008
 Description : Inicializa el formulario del libro de peajes en pantalla
-------------------------------------------------------------------------------}
function TVerLibroDePeajesForm.Inicializar;
begin
    Caption := sCaption;
    lblArchivo.Caption := '';
    mmoData.Clear;
    lblCargando.Visible := False;
    CenterForm(Self);
end;

procedure TVerLibroDePeajesForm.btnImprimirClick(Sender: TObject);
begin
	if mmoData.Lines.Count > 0 then begin
		Screen.Cursor := crHourGlass;
	    rbiLibroPeajes.Execute();
        ppTextPipeline.Close;
        ppTextResumen.Close;
        DeleteFile(cArchivoParaImprimir);
    	DeleteFile(cResumenParaImprimir);
	    Screen.Cursor := crDefault;
    end;
end;

procedure TVerLibroDePeajesForm.btnLeerClick(Sender: TObject);
resourcestring
	MSG_NO_ARCHIVO = 'El archivo %s  no existe';
begin
	if odlgArchivo.Execute then begin
    	if FileExists(odlgArchivo.FileName) then begin
            try
            	Screen.Cursor := crHourGlass;
            	pcArchivoResumen.TabIndex := 0;
                mmoData.Visible := False;
                lblCargando.Visible := True;
                Self.Refresh;
                Application.ProcessMessages;
        		CargarLibroDePeaje(odlgArchivo.FileName);
            	lblArchivo.Caption := odlgArchivo.FileName;
            finally
            	mmoData.Visible := True;
                lblCargando.Visible := False;
                Screen.Cursor := crDefault;
            end;

        end
        else
        	MsgBox(MSG_NO_ARCHIVO, Caption, MB_ICONEXCLAMATION);
    end;
    
end;

procedure TVerLibroDePeajesForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TVerLibroDePeajesForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	{borrar los archivos que se usaron para la impresi�n}
    DeleteFile(cArchivoParaImprimir);
    DeleteFile(cResumenParaImprimir);
	FArchivo.Free;
    FResumen.Free;
    Action := caFree;
end;

procedure TVerLibroDePeajesForm.FormCreate(Sender: TObject);
begin
	FArchivo := TStringList.Create;
    FResumen := TStringList.Create;
    // Rev.1 / 11-Diciembre-2009 / Nelson Droguett Sierra -----------------------------------
    FResumenConcepto := TStringList.Create;
    // --------------------------------------------------------------------------------------
end;


end.
