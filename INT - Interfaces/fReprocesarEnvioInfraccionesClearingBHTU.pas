{********************************** Unit Header ********************************
File Name : fReprocesarEnvioInfraccionesClearingBHTU.pas
Author : ndonadio
Date Created: 26/09/2005
Language : ES-AR
Description : Reprocesamiento de los archivos de infracciones  y transitos
 
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

*******************************************************************************}
unit fReprocesarEnvioInfraccionesClearingBHTU;

interface

uses
    //Reprocesar Envio Infracciones
    DMConnection,
    UtilDB,
    PeaProcs,
    PeaTypes,
    ComunesInterfaces,
    Util,
    UtilProc,
    ConstParametrosGenerales,
    fReporteEnvioInfraccionesBHTU,
    //General
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, DateUtils, ppDB,
    ppDBPipe, UtilRB, ppCtrls, ppBands, ppClass, ppPrnabl, ppCache, ppComm,
    ppRelatv, ppProd, ppReport, ppModule, raCodMod, ppParameter, Validate,
    DateEdit, BuscaTab;

type
    TEstadoPRoceso = (NORMAL, PROCESANDO);

  TfrmReprocesarEnvioInfraccionesClearingBHTU = class(TForm)
    Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    Label1: TLabel;
    lblDetalleProgreso: TLabel;
    pb_Progreso: TProgressBar;
    txtDirectorioDestino: TPickEdit;
    btnProcesar: TButton;
    btnSalir: TButton;
    Label2: TLabel;
    spObtenerEnviosInfraccionesBHTU: TADOStoredProc;
    spObtenerInfracciones: TADOStoredProc;
    spObtenerTransitos: TADOStoredProc;
    spActualizarInfracciones: TADOStoredProc;
    spRegistrarArchivo: TADOStoredProc;
    deDesde: TDateEdit;
    deHasta: TDateEdit;
    Label3: TLabel;
    Label4: TLabel;
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ImgAyudaClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnProcesarClick(Sender: TObject);
    procedure txtDirectorioDestinoButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoOperacionAReprocesar : Integer;
    FArchivoInfracciones : AnsiString;
    FArchivoTransitos : AnsiString;
    FDestino : AnsiString;
    FNroReproceso : Integer;
    FCantInfracciones : Integer;
    FCantTransitos : Integer;
    FFechaArchivo : TDateTime;
    FProcesando : Boolean;
    FCancelar : Boolean;
    function  RegistrarOperacion(var CodigoOperacionInterfase : Integer) : Boolean;
    function  GenerarArchivoInfracciones(CodigoOperacionInterfase : Integer) : Boolean;
    function  GenerarArchivoTransitos(CodigoOperacionInterfase : Integer) : Boolean;
    function  RegistrarArchivo(CodigoOperacionInterfase : Integer) : Boolean;
    function  ActualizarInfracciones(CodigoOperacionInterfase : Integer) : Boolean;
    function  CerrarLog(CodigoOperacionInterfase : Integer) : Boolean;
    function  MostrarReporte(Primera, Ultima : Integer) : Boolean;
    procedure PrepararBarraProgreso(Blanquear : Boolean = False);
    procedure ActualizarBarraProgreso(Descripcion : AnsiString);
    procedure HabilitarBotones(Estado : TEstadoProceso);
  public
    { Public declarations }
    function Inicializar(Titulo : AnsiString) : Boolean;
  end;

var
  frmReprocesarEnvioInfraccionesClearingBHTU: TfrmReprocesarEnvioInfraccionesClearingBHTU;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created :  26/09/2005
Description :  Inicializacion de este formulario
Parameters : Titulo: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmReprocesarEnvioInfraccionesClearingBHTU.Inicializar(Titulo : AnsiString) : Boolean;
resourcestring
    MSG_ERROR_CANT_OPEN_PROCESS_LIST = 'No se puede obtener la lista de procesos realizados.';
    MSG_ERROR_CANNOT_INITIALIZE = 'No se puede inicializar la interfaz.';
    MSG_ERROR_CANNOT_GET_PARAMETER = 'Error obteniendo el parametro general %s';
    MSG_ERROR_NOTHING_TO_REPROCESS = 'No hay Env�os para Reprocesar';
const
    DIR_DESTINO_INFRAC_CLEARING_BHTU = 'DIR_DESTINO_INFRAC_CLEARING_BHTU';
begin
    Result := False;
    Caption := Trim(Titulo);
    CenterForm(Self);
    FCodigoOperacionAReprocesar := 0;

    //Cargo los parametros generales de directorios de destino para el par de archivos
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DESTINO_INFRAC_CLEARING_BHTU, FDestino) then begin
        MsgBoxErr(MSG_ERROR_CANNOT_INITIALIZE, Format(MSG_ERROR_CANNOT_GET_PARAMETER, [DIR_DESTINO_INFRAC_CLEARING_BHTU]), Caption, MB_ICONSTOP);
        Exit;
    end;
    
    txtDirectorioDestino.Text := GoodDir(FDestino);
    Result := (DirectoryExists(GoodDir(FDestino)));
    if not Result then MsgBox(MSG_ERROR_NOTHING_TO_REPROCESS, Caption, MB_ICONINFORMATION);
    btnProcesar.Enabled := Result;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : ndonadio
Date Created : 27/09/2005
Description :  Muestra ayuda sobre la interfaz
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReprocesarEnvioInfraccionesClearingBHTU.ImgAyudaClick(Sender: TObject);
Resourcestring
  MSG_AYUDA   = ' ' + CRLF +
                'El Archivo de Infracciones es ' + CRLF +
                'utilizado por el ESTABLECIMIENTO para informar '+ CRLF +
                'las infracciones detectadas a las CONCESIONARIAS ADHERIDAS' + CRLF +
                ' ' + CRLF +
                'Nombre del Archivo: INFRACTORES_N_YYYYMMDD.TRX' + CRLF +
                ' ';
begin
    //si esta procesando sale
    if PnlAvance.Visible = True then Exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_AYUDA, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : ndonadio
Date Created : 27/09/2005
Description : cambia el cursor al pasar sobre el boton de ayuda
Parameters : Sender: TObject; Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TfrmReprocesarEnvioInfraccionesClearingBHTU.ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.Visible = True then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{******************************** Function Header ******************************
Function Name: txtDirectorioDestinoButtonClick
Author : ndonadio
Date Created : 26/09/2005
Description : Busca un nuevo directorio de destino....
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReprocesarEnvioInfraccionesClearingBHTU.txtDirectorioDestinoButtonClick(Sender: TObject);
resourcestring
    MSG_SELECT_LOCATION = 'Seleccione ubicaci�n donde se generar�n los archivos.';
var
    Location : String;
begin
    btnProcesar.Enabled := False;
    //Abre el form de seleccion de directorio
    Location := Trim(BrowseForFolder(MSG_SELECT_LOCATION));
    //Valida el directio elegido
    if Location = '' then Location := GoodDir(txtDirectorioDestino.Text);
    FDestino := Location;
    //Lo carga en las variables, edits, etc.
    txtDirectorioDestino.Text := GoodDir(Location);
    //Habilita el boton procesar si puede crear nombres de archivos...
    btnProcesar.Enabled := DirectoryExists(GoodDir(Location));
end;

{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 27/09/2005
Description : Habilita o deshabilita los botones
Parameters : Estado: TEstadoPRoceso
Return Value : None
*******************************************************************************}
procedure TfrmReprocesarEnvioInfraccionesClearingBHTU.HabilitarBotones(Estado: TEstadoPRoceso);
const
    STR_QUIT    = 'Salir';
    STR_CANCEL  = 'Cancelar';
begin
    if (Estado = PROCESANDO) then begin
        btnSalir.Caption := STR_CANCEL;
    end else begin
        btnSalir.Caption := STR_QUIT;
    end;
    FProcesando := (Estado = PROCESANDO);
    btnProcesar.Enabled := (not (Estado = PROCESANDO)) and DirectoryExists(FDestino);
    txtDirectorioDestino.Enabled := not (Estado = PROCESANDO);
    deDesde.Enabled := not (Estado = PROCESANDO);
    deHasta.Enabled := not (Estado = PROCESANDO);
    FCancelar := False;
    pnlAvance.Visible   := (Estado = PROCESANDO);
    PrepararBarraProgreso(not (Estado = PROCESANDO));
end;

{******************************** Function Header ******************************
Function Name: PrepararBarraProgreso
Author : ndonadio
Date Created : 26/09/2005
Description :   Setea los valores de la barra de progreso antes de iniciar un proceso
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmReprocesarEnvioInfraccionesClearingBHTU.PrepararBarraProgreso(Blanquear: Boolean = False);
begin
    pb_Progreso.Min := 0;
    if not Blanquear then begin
        pb_Progreso.Max := 6 + FCantInfracciones + FCantTransitos;
    end else begin
        pb_Progreso.Max := 1;
    end;
    pb_Progreso.Position := 0;
    lblDetalleProgreso.Caption := '';
    pnlAvance.Visible := not Blanquear;
end;

{******************************** Function Header ******************************
Function Name: ActualizarBarraProgreso
Author : ndonadio
Date Created :  26/09/2005
Description  :  Actualiza la barra de progreso acrecentandola en 1 y agregando
                el label que llega como paraemtro.
Parameters   : None
Return Value : None
*******************************************************************************}
procedure TfrmReprocesarEnvioInfraccionesClearingBHTU.ActualizarBarraProgreso(Descripcion: AnsiString);
begin
    lblDetalleProgreso.Caption := TRIM(Descripcion);
    pb_Progreso.StepIt;
    lblDetalleProgreso.refresh;
    pb_Progreso.Refresh;
    Application.ProcessMessages;
end;

{******************************** Function Header ******************************
Function Name: RegistrarOperacion
Author : ndonadio
Date Created : 26/09/2005
Description : Registra la operacion en el log de operaciones. Obtiene el Codigo
Parameters : var CodigoOperacionInterfase: Integer
Return Value : Boolean
*******************************************************************************}
function TfrmReprocesarEnvioInfraccionesClearingBHTU.RegistrarOperacion(var CodigoOperacionInterfase : Integer) : Boolean;
resourcestring
    MSG_ERROR_REGISTERING_LOG = 'No se puede registrar la operaci�n en el log.';
Const
    STR_PROGRESS_REGISTERING_LOG = 'Registrando Operaci�n en el Log de Interfaces';
var
    DescError : AnsiString;
begin
    Result := False;
    ActualizarBarraProgreso(STR_PROGRESS_REGISTERING_LOG);
    if not RegistrarOperacionEnLogInterface(DMCOnnections.BaseCAC,RO_MOD_INTERFAZ_SALIDA_INFRACCIONES_CLEARING_BHTU,FDestino, UsuarioSistema,'',False, TRUE,NowBase(DMConnections.BaseCAC),0,CodigoOperacionInterfase, descError) then begin
        MsgBoxErr(MSG_ERROR_REGISTERING_LOG, descError, Caption, MB_ICONERROR);
        Exit;
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: GenerarArchivoInfracciones
Author : ndonadio
Date Created : 26/09/2005
Description :   Obtiene los datos y genera el archivo de infracciones.
Parameters : CodigoOperacionInterfase: Integer
Return Value : Boolean
*******************************************************************************}
function TfrmReprocesarEnvioInfraccionesClearingBHTU.GenerarArchivoInfracciones(CodigoOperacionInterfase : Integer) : Boolean;
resourcestring
    MSG_ERROR_CANT_CREATE_FILE  = 'No se puede crear el archivo de infracciones.';
const
    STR_PROGRESS_CREATING_FILE  = 'Obteniendo listado de infracciones...';
    STR_PROGRESS_PROCESSING_X   = 'Procesando Infracci�n %d de %d';
var
    Lista : TStringList;
begin
    ActualizarBarraProgreso(STR_PROGRESS_CREATING_FILE);
    Result := False;
    Lista := TStringList.Create;
    try
        try //Obtengo la lista de infraccion
            spObtenerInfracciones.Close;
            spObtenerInfracciones.Parameters.ParamByName('@CodigoOperacion').Value := FCodigoOperacionAReprocesar;
            spObtenerInfracciones.Open;
            //La meto en un string list
            while NOT spObtenerInfracciones.Eof do begin
                ActualizarBarraProgreso(Format(STR_PROGRESS_PROCESSING_X, [Lista.Count+1, FCantInfracciones])) ;
                Lista.Add(spObtenerInfracciones.FieldByName('Campo').asString);
                spObtenerInfracciones.Next ;
            end;
            //Grabo el string list como el archivo que corresponde
            if Lista.Count > 0 then Lista.SaveToFile( GoodDir(FDestino) + FArchivoInfracciones );
            Result := True;
        except
            on E : Exception do begin
                MsgBoxErr(MSG_ERROR_CANT_CREATE_FILE, e.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
       Lista.Free;
       spObtenerInfracciones.Close;
    end;
end;

{******************************** Function Header ******************************
Function Name: GenerarArchivoTransitos
Author : ndonadio
Date Created : 26/09/2005
Description :  Obtiene los datos y genera el archivo de transitos
Parameters : CodigoOperacionInterfase: Integer
Return Value : Boolean
*******************************************************************************}
function TfrmReprocesarEnvioInfraccionesClearingBHTU.GenerarArchivoTransitos(CodigoOperacionInterfase : Integer) : Boolean;
resourcestring
    MSG_ERROR_CANT_CREATE_FILE  = 'No se puede crear el archivo de tr�nsitos.';
const
    STR_PROGRESS_CREATING_FILE  = 'Obteniendo listado de tr�nsitos...';
    STR_PROGRESS_PROCESSING_X   = 'Procesando Tr�nsito %d de %d';
var
    Lista : TStringList;
begin
    ActualizarBarraProgreso(STR_PROGRESS_CREATING_FILE);
    Result := False;
    Lista := TStringList.Create;
    try
        try
            //obtengo la lista de infraccion
            spObtenerTransitos.Close;
            spObtenerTransitos.Parameters.ParamByName('@CodigoOperacion').Value := FCodigoOperacionAReprocesar;
            spObtenerTransitos.Open;
            //la meto en un string list
            while not spObtenerTransitos.Eof do begin
                ActualizarBarraProgreso(Format(STR_PROGRESS_PROCESSING_X, [Lista.Count + 1, FCantTransitos])) ;
                Lista.Add(spObtenerTransitos.FieldByName('Campo').asString);
                spObtenerTransitos.Next ;
            end;
            //grabo el string list como el archivo que corresponde
            if Lista.Count > 0 then Lista.SaveToFile( GoodDir(FDestino) + FArchivoTransitos );
            Result := True;
        except
            on E : Exception do begin
                MsgBoxErr( MSG_ERROR_CANT_CREATE_FILE, E.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
       Lista.Free;
       spObtenerTransitos.Close;
    end;
end;

{******************************** Function Header ******************************
Function Name: RegistrarArchivo
Author : ndonadio
Date Created : 26/09/2005
Description : Registra una entrada en la tabla de Archivos Enviados
Parameters : CodigoOperacionInterfase: Integer
Return Value : Boolean
*******************************************************************************}
function TfrmReprocesarEnvioInfraccionesClearingBHTU.RegistrarArchivo(CodigoOperacionInterfase : Integer) : Boolean;
resourcestring
    MSG_ERROR_INSERT_FAIL  = 'Fall� la operaci�n de insertar el registro del archivo enviado.';
    MSG_ERROR_PROCESS = 'No se puede registrar el Archivo a Enviar.';
begin
    Result := False;
    try
        with spRegistrarArchivo do begin
            Parameters.ParamByName('@CodigoOperacion').Value := CodigoOperacionInterfase;
            Parameters.ParamByName('@FechaArchivo').Value := FFechaArchivo;
            Parameters.ParamByName('@ArchivoInfraccion').Value := ExtractFileName(FArchivoInfracciones);
            Parameters.ParamByName('@ArchivoTransitos').Value := ExtractFileName(FArchivoTransitos);
            Parameters.ParamByName('@NumeroReproceso').Value := FNroReproceso + 1;
            ExecProc;
            if  Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                MsgBoxErr(MSG_ERROR_INSERT_FAIL, MSG_ERROR_PROCESS, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
        Result := True;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_INSERT_FAIL, E.Message, Caption, MB_ICONERROR);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ActualizarInfracciones
Author : ndonadio
Date Created : 26/09/2005
Description :   Actualiza las tablas de infracciones y transitos enviados
                y la tabla de Infracciones con el nuevo codigo
Parameters : CodigoOperacionInterfase: Integer
Return Value : Boolean
*******************************************************************************}
function TfrmReprocesarEnvioInfraccionesClearingBHTU.ActualizarInfracciones(CodigoOperacionInterfase : Integer) : Boolean;
resourcestring
    MSG_ERROR_PROCESSING = 'Error al ejecutar la actualizaci�n de interfaces';
    MSG_ERROR_TO_UPDATE_RECORDS = 'No se pueden actualizar las infracciones y transitos enviados.';
const
    STR_PROGRESS_UPDATING = 'Actualizando Infracciones y Tr�nsitos Enviados';
begin
    Result := False;
    ActualizarBarraProgreso(STR_PROGRESS_UPDATING);
    try
        spActualizarInfracciones.Parameters.ParamByName('@CodigoOperacionOriginal').Value := FCodigoOperacionAReprocesar;
        spActualizarInfracciones.Parameters.ParamByName('@CodigoOperacionReenvio').Value := CodigoOperacionInterfase;
        spActualizarInfracciones.ExecProc;
        if spActualizarInfracciones.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then raise Exception.Create(MSG_ERROR_PROCESSING);
        Result := True;
    except
        on E : Exception do begin
             MsgBoxErr(MSG_ERROR_TO_UPDATE_RECORDS, e.Message, Caption, MB_ICONERROR);
             Exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: CerrarLog
Author : ndonadio
Date Created : 26/09/2005
Description : Asienta el fin de la operacion en el Log de Interfases
Parameters : CodigoOperacionInterfase: Integer
Return Value : Boolean
*******************************************************************************}
function TfrmReprocesarEnvioInfraccionesClearingBHTU.CerrarLog(CodigoOperacionInterfase : Integer) : Boolean;
resourcestring
    MSG_ERROR_ENDING_OPERATION = 'Se ha producido un error al actualizar el Log de Operaciones.';
const
    STR_PROGRESS_CLOSING_LOG = 'Registrando finalizaci�n del proceso.';
    STR_FINALLY_OK = 'Finaliz� OK!.';
var
    DescError : AnsiString;
begin
    Result := False;
    ActualizarBarraProgreso(STR_PROGRESS_CLOSING_LOG);
    if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, CodigoOperacionInterfase, STR_FINALLY_OK, DescError) then begin
        //DMConnections.BaseCAC.RollbackTrans;                                                                              //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fInfraccionesClearingBHTU END');	    //SS_1385_NDR_20150922
        MsgBoxErr(MSG_ERROR_ENDING_OPERATION, DescError, Caption, MB_ICONERROR);
        Exit;
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: MostrarReporte
Author : ndonadio
Date Created : 26/09/2005
Description : Muestra el reporte
Parameters : CodigoOperacionInterfase: Integer
Return Value : Boolean
*******************************************************************************}
function TfrmReprocesarEnvioInfraccionesClearingBHTU.MostrarReporte(Primera, Ultima : Integer) : Boolean;
resourcestring
    MSG_ERROR_CANT_SHOW_REPORT  = 'No se puede mostrar el reporte.';
var
    F : TfrmReporteEnvioInfraccionesBHTU;
    DescError : AnsiString;
begin
    Result := False;
    //Creo el reporte
    try
        Application.CreateForm(TfrmReporteEnvioInfraccionesBHTU, F);
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_CANT_SHOW_REPORT, e.Message, caption, MB_ICONERROR);
            Exit;
        end;
    end;
    //Ejecuto el reporte
    if not f.MostrarReporte(Primera, Ultima, Caption, descError) then begin
        if trim(descError) <> '' then begin
            //Informo que no pudo abrir el reporte
            MsgBoxErr(MSG_ERROR_CANT_SHOW_REPORT, descError, Caption, MB_ICONERROR);
        end;
    end;
    f.Release;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author : ndonadio
Date Created : 26/09/2005
Description : Genera los archivos y actualiza la base de datos
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReprocesarEnvioInfraccionesClearingBHTU.btnProcesarClick(Sender: TObject);

    {******************************** Function Header ******************************
    Function Name: ValidarFecha
    Author : lgisuk
    Date Created : 08/02/2006
    Description : Valido que sea una fecha valida
    Parameters : Sender: TObject
    Return Value : None
    *******************************************************************************}
    function ValidarFecha(Control : TDateEdit): Boolean;
    begin
        Result := False;
        if Control.IsEmpty then Exit;
        try
            StrToDate(Control.Text);
            Result := True;
        except
            on exception do exit;
        end;
    end;

    {******************************** Function Header ******************************
    Function Name: ValidarDirectorio
    Author : lgisuk
    Date Created : 08/02/2006
    Description : Valido que exista el directorio
    Parameters : Sender: TObject
    Return Value : None
    *******************************************************************************}
    function ValidarDirectorio : Boolean;
    begin
        Result := DirectoryExists(GoodDir(TRIM(txtDirectorioDestino.Text)));
    end;

resourcestring
    MSG_ERROR_CANT_LOAD_SENDED = 'No se pueden obtener los envios.';
    MSG_ERROR_VALUE_PATH = 'El directorio seleccionado no es v�lido';
    MSG_ERROR_VALUE_FROM = 'La Fecha Desde no es una fecha v�lida.';
    MSG_ERROR_VALUE_TO = 'La Fecha Hasta no es una fecha v�lida.';
    MSG_ERROR_VALUE_RANGE = 'La Fecha Hasta debe ser POSTERIOR (o igual) a la Fecha Desde';
    MSG_ERROR_NOTHING_SEND_ON_DATES_SELECTED = 'No hay env�os registrados entre las fechas seleccionadas';
var
    CodigoOperacionReenvio : Integer;
    PrimeraOperacion : Integer;
    UltimaOperacion : Integer;
begin
    //Validar los datos seleccionados
    if not ValidateControls( [txtDirectorioDestino, deDesde, deHasta, deHasta], [(ValidarDirectorio), (ValidarFecha(deDesde)), (ValidarFecha(deHasta)), (deDesde.Date <= deHasta.Date)], Caption, [MSG_ERROR_VALUE_PATH, MSG_ERROR_VALUE_FROM, MSG_ERROR_VALUE_TO,  MSG_ERROR_VALUE_RANGE]) then Exit;

    //Preparar todo
    PrimeraOperacion := 0;
    UltimaOperacion := 0;
    FDestino := GoodDir(TRIM(txtDirectorioDestino.Text));
    HabilitarBotones(PROCESANDO);
    try

        //Obtengo las infracciones a enviar
        try
            spObtenerEnviosInfraccionesBHTU.Parameters.ParamByName('@FechaDesde').Value := deDesde.Date;
            spObtenerEnviosInfraccionesBHTU.Parameters.ParamByName('@FechaHasta').Value := deHasta.Date;
            spObtenerEnviosInfraccionesBHTU.Open;
        except
            on E : Exception do begin
                //Informo que hubo un error al obtener las infracciones a enviar
                MsgBoxErr( MSG_ERROR_CANT_LOAD_SENDED, E.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;

        //Verifico si hay infracciones para enviar
        if spObtenerEnviosInfraccionesBHTU.IsEmpty then begin
            //Informo que no hay infracciones para enviar y salgo
            MsgBox(MSG_ERROR_NOTHING_SEND_ON_DATES_SELECTED, Caption, MB_ICONINFORMATION);
            Exit;
        end;

        //Mientras halla infracciones para enviar
        while (not spObtenerEnviosInfraccionesBHTU.Eof) AND (not FCancelar) do begin

            //Cargo los datos
            with spObtenerEnviosInfraccionesBHTU do begin
                FCodigoOperacionAReprocesar := FieldByName('CodigoOperacionInterfase').AsInteger;
                FArchivoInfracciones := FieldByName('ArchivoInfracciones').AsString;
                FArchivoTransitos := FieldByName('ArchivoTransitos').AsString;
                FNroReproceso := FieldByName('NumeroReproceso').AsInteger;;
                FCantInfracciones := FieldByName('CantidadInfracciones').AsInteger;
                FCantTransitos := FieldByName('CantidadTransitos').AsInteger;
                FFechaArchivo := FieldByName('FechaArchivo').AsDateTime;
            end;

            //Registro la operacion en el log
            if not RegistrarOperacion(CodigoOperacionReenvio) then Exit;
            if PrimeraOperacion = 0 then PrimeraOperacion := CodigoOperacionReenvio;
            UltimaOperacion :=  CodigoOperacionReenvio;

            //Generaro el Archivo Infracciones
            if not GenerarArchivoInfracciones(CodigoOperacionReenvio) then Exit;

            //Genero el Archivo transitos
            if not GenerarArchivoTransitos(CodigoOperacionReenvio) then Exit;

            //Inicio una transaccion para actualizar la base de datos
            //DMConnections.BaseCAC.BeginTrans;                                               //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('BEGIN TRAN fInfraccionesClearingBHTU');         //SS_1385_NDR_20150922

            //Registro el Archivo
            if not RegistrarArchivo(CodigoOperacionReenvio) then begin
                //DMConnections.BaseCAC.RollbackTrans;                                                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fInfraccionesClearingBHTU END');	    //SS_1385_NDR_20150922
                Exit;
            end;

            //Actualizo las Infracciones
            if not ActualizarInfracciones(CodigoOperacionReenvio) then begin
                //DMConnections.BaseCAC.RollbackTrans;                                                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fInfraccionesClearingBHTU END');	    //SS_1385_NDR_20150922
                Exit;
            end;

            //Cierro el log
            if not CerrarLog(CodigoOperacionReenvio) then begin
                //DMConnections.BaseCAC.RollbackTrans;                                                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fInfraccionesClearingBHTU END');	    //SS_1385_NDR_20150922
                Exit;
            end;

            //Acepto la transaccion
            //DMConnections.BaseCAC.CommitTrans;                                                                //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('COMMIT TRAN fInfraccionesClearingBHTU');					              //SS_1385_NDR_20150922
            spObtenerEnviosInfraccionesBHTU.Next;
            Application.ProcessMessages;
        end;

        // Mostrar Reporte
        if (PrimeraOperacion > 0) and (UltimaOperacion > 0) then begin
            //Muesto el reporte de finalizacion
            MostrarReporte(PrimeraOperacion, UltimaOperacion);
        end;

    finally
        //Dejar todo en cero y recargar lista de procesos anteriores
        //para actualizar nro de reenvio
        FCodigoOperacionAReprocesar := 0;
        FArchivoTransitos := '';
        FArchivoInfracciones := '';
        FNroReproceso := 0;
        HabilitarBotones(NORMAL);
        deDesde.Clear;
        deHasta.Clear;
        spObtenerEnviosInfraccionesBHTU.Close;
        spObtenerEnviosInfraccionesBHTU.OPen;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : lgisuk
Date Created : 02/08/06
Description : Permito cancelar la operacion o cerrar el formulario
Parameters :
Return Value : None
*******************************************************************************}
procedure TfrmReprocesarEnvioInfraccionesClearingBHTU.btnSalirClick(Sender: TObject);
begin
    if FProcesando then FCancelar := True else Close;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : ndonadio
Date Created : 26/09/2005
Description : Si est� procesando no se puede cerrar...
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmReprocesarEnvioInfraccionesClearingBHTU.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : lgisuk
Date Created : 02/08/06
Description : Libero el formulario de memoria
Parameters :
Return Value : None
*******************************************************************************}
procedure TfrmReprocesarEnvioInfraccionesClearingBHTU.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
