{********************************** File Header ********************************
File Name : FrmMain.pas
Author: lgisuk
Date Created: 25/11/2004
Language : ES-AR
Description : Gesti�n de Interfaces

Revision 1
Author: mbecerra
Date: 23-Julio-2008
Description:
            	Se agrega la nueva interfaz de Generaci�n de N�minas:
                	* Servipag,
                    * Lider,
                    * Mis Cuentas
                    * Santander.

                se quitan estas opciones del men� correspondiente:
                	* mnu_Interfaces --> Lder1 --> GeneracindeNominas1
                    * mnu_Interfaces --> mnu_Santander --> mnuGeneracionDeDebitosNuevo
                	* mnu_Interfaces --> mnu_Servipag --> GeneracindeDebitos1
                    * mnu_Interfaces --> mnu_Servipag --> Mnu_GeneracionNominasConMedioPago
                    * mnu_Interfaces --> MisCuentas1 --> Mnu_GeneracindeNominasMisCuentas

                Estas interfaces dejan de ser desatendidas.
Revision 4
    Author      :   Nelson Droguett Sierra
    Date        :   13-Noviembre-2009
    Description :   SS-846 : Cambio de Persona RNVM

Revision 5
    Author      :   Jose Jofre Aranguiz
    Date        :   17 de Mayo de 2010
    Description :   SS-884 : Interfaz Banco de Chile
            Se agregan los siguientes Objetos, funciones.
            
    procedure mnu_GeneracionDeDebitosBcoChileClick(Sender: TObject);
    procedure mnu_RecepcionUniversoBancoChileClick(Sender: TObject);
    procedure mnu_RecepcionRendicionesBcoChileClick(Sender: TObject);

Revision 6
    Author      : pdominguez
    Date        : 03/08/2010
    Description : SS 912 - Gestion Interfaces - Nombre menu - Modificaci�n
        - Se modifica el nombre de los men�s de Mis Cuentas /CMR a�adiendole Unimarc.

Revision 7
    Author      :   pdominguez
    Date        :   12/08/2010
    Description : SS 909 - BHTU Copec Rechazos
        - Se modifican los siguientes Procedimientos:
            mnuProcesarPDUClick,
            mnuProcesarPDUTClick,
            RecepcindeArchivodeVentasdeBHTU1Click

Revision 8
    Author      :   alabra
    Date        :   05/04/2011
    Description : SS 961 - Interfaces - Cuenta Movil
        - Se modificaron los nombres de los siguientes menus:
            * mnu_RecepcionUniversoBancoChile
            * mnu_GeneracionDeDebitosBcoChile
            * mnu_RecepcionRendicionesBcoChile
        - Se agrego un nuevo subMenu dentro de BancoChile llamado mnu_PagoCtaMovilBcoChile,
            el cual ejecuta la misma funcionalidad de mnu_RecepcionRendicionesMisCuentasClick
        - Se creo un nuevo procedure llamado RecepcionRendicionesMisCuentas,
            el cual ejecuta la funcionalidad Recepcion de Rendiciones de Mis Cuentas.

Description : Se elimina del men� mnu_RecepcionRendicionesMisCuentas, y se cambia por
              mnu_RendicionesMisCuentas, mnu_RendicionesCMR, mnu_RendicionesUnimarc,
              mnu_RendicionesMulticaja, las cuales llaman al mismo m�todo
              RecepcionRendicionesMisCuentas
Firma       : SS_969_ALA_20110811

Revision 9
    Author      :   cquezada
    Date        :   29/03/2012
    Description :   SS 1015 - Listado de tr�nsitos/movimientos cuentas rechazados
        -   Se agregan los siguientes Objetos, funciones.
            N31
            mnu_InformesOtrasConcesionarias
            mnu_TransaccionesRechazadas
            mnu_TransaccionesRechazadasClick

Firma           :   SS_1015_20120329_CQU

Firma           :   SS_1015_HUR_20120409
Descripcion     :   Se agregan los menues Folios Facturacion y Folios Pagos en el
                    menu Informes Otras Concesionarias y el llamado a los respectivos
                    formularios.

Firma           :   SS-1015-NDR-20120425
Descripcion     :   Se agregan los siguientes objetos y funciones
                      N32
                      mnu_IVAOtrasConcesionarias
                      mnu_IVAOtrasConcesionariasClick

Firma       :   SS_925_NDR_20131122
Description :   Genera nuevos menu segun campo GenerarMenues de tabla SistemasVersiones (no del .ini)
Firma			:	SS_1143_MCA_20131108
Descripcion		:   Se agregan Permisos para el formulario frmRvm.pas para los botones y check que contiene
                        seguridad_RNVM
                        seguridad_botones
                        Boton_Consulta_Ultima_modificacion
                        Boton_Consulta_RNVM
                        Boton_AbrirXML
                        Check_Actualizar_Datos_Infractores
                        Check_Forzar_Consulta_RNVM

Firma			:	SS_1140_MCA_20131118
Descripcion		:	se elimina opcion de menu de respuesta de captaciones ya que estan seran informadas en las novedades.

Firma       :   SS_925_MBE_20131223
Description :   Se cambia la ventana de Acerca de.

Firma       :   SS_1147_CQU_20150316
Descripcion :   Se agrea una evaluaci�n al tipo ConcesionariaNativa para cambiar el nombre del men� mnu_RendicionesMisCuentas
                si la Nativa es VespucioSur se cambia el nombre por "Sencillito" sino "Mis Cuentas"

Firma       :   SS_1147_CQU_20150324
Descripcion :   Se agrega un nuevo formulario llamado frmRecepcionCaptacionesCMR_VS para CMR de Vespucio Sur
                Se corrige la obtencion de la concesionaria nativa para hacerla global al formulario

Firma       : SS_1147_NDR_20141216
Descripcion : Color de menues segun concesionaria nativa

Autor       :   Claudio Quezada
Fecha       :   16-06-2015
Firma       :   SS_1314_CQU_20150908
Descripcion :   Homologar CMR de VS al de CN
				Se cambia la llamada a frmRecepcionCaptacionesCMR_VS por frmRecepcionCaptacionesCMR

Autor       :   Claudio Quezada
Fecha       :   03-09-2015
Firma       :   SS_1375_CQU_20150903
Descripcion :   FCavada pide separar, en VS, los men� de servipag para poder "reconocer" por donde lleg� tal o cual pago.
                El nuevo orden ser�a:
                    Entidad                 Nombre Archivo              C�digoServicio     Men� en CAC
                    SERVIPAG CAJA  	        SPG_CAJA_AAAAMMDD.TXT	    CAJA	        INTERFACES; interfaces- Servipag - Recepcion de Rendiciones Caja
                    SERVIPAG EXPRESS   	    SPG_EXP_AAAAMMDD.TXT	    EXPRES	        INTERFACES; Interfaces- Servipag - Recepcion de Rendiciones Express
                    SERVIPAG PORTAL 	    SPG_PORTAL_AAAAMMDD.TXT	    PORTAL	        INTERFACES; Interfaces- Servipag - Recepcion de Rendiciones Portal
                    SERVIPAG RUA CAJA    	SPG_RUA_AAAAMMDD.TXT	    UNCAJA	        INTERFACES; interfaces- Servipag - Recepcion de Rendiciones Caja Unificado
                    SERVIPAG RUA EXPRESS    SPG_RUAEXP_AAAAMMDD.TXT     UNEXPR	        INTERFACES; interfaces- Servipag - Recepcion de Rendiciones Express Unificado
                    SERVIPAG RUA PORTAL 	SPG_RUAPORTAL_AAAAMMDD.TXT  UNPORT	        INTERFACES; interfaces- Servipag - Recepcion de Rendiciones Portal Unificado
                Se modifica el MenuItem y en la propiedad TAG se deja el valor que corresponde.

Firma		: SS_1378_MCA_20150915
Descripcion	: se agrega codigo modulo para las rendiciones unificadas.

Firma       :   SS_1400_NDR_20151007
Descripcion :   Interfaz Entrante Respuestas RNVM

Firma       :   SS_1406_NDR_20151022
Descripcion :   Asignacion de ConsultasRNVM a Infracciones


Firma       : SS_1436_NDR_20151230
Descripcion : Dejar un registro en RegistroOperaciones cuando el usuario sale del sistema  o cuando trata de ingresar con password erronea

Firma       : SS_1437_NDR_20151230
Descripcion : Que los menus de los informes se carguen segun los permisos del usuario, y que dependan del GenerarMenues del Sistema

Firma		: CAC-CLI-066_MGO_20160218
Descripcion	: Se agrega opci�n "Tipos de Correspondencia"   

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

Etiqueta    :   20160513 MGO
Descripci�n :   Se llama a TformEjecutarInterfaz para los submen�es de interfaces
*******************************************************************************}
unit FrmMain;

interface

uses
    //Gesti�n de Interfaces
    DMConnection,                             //Conexion a Base de datos OP_CAC
    util,                                     //rutinas utiles
    // INICIO : 20160513 MGO
    UtilDB,
    // FIN : 20160513 MGO
    UtilProc,                                 //Mensajes
    PeaTypes,                                 //Constantes
    PeaProcs,                                 //Permisos
    ConstParametrosGenerales,
    Login,                                    //Unit de Login de las Aplicaciones del Proyecto Oriente-Poniente.
    CambioPassword,                           //Permite cambiar password del usuario
    DPSAbout,                                 //Acerca de..
    CustomAssistance,                         //Informacion de Soporte tecnico
    frmAcercaDe,                                          //SS_925_MBE_20131223

    frmGeneracionDeNominas,                   //Interfaz de Generaci�n de las n�minas Lider, Servipag, MisCuentas y Santander
    FrmLogOperaciones,                        //Log de Operaciones Realizadas
    FrmGenerarArchivoMovimientos,             //Modulo de la interfaz TransBank - Generar Archivo de Movimientos
    FrmGenerarArchivoNomina,                  //Modulo de la interfaz TransBank - Generar Archivo de Nomina
    FrmConvertirMovimientosATabla,            //Modulo de la interfaz TransBank - Procesar Archivos de Movimientos
    FrmConvertirNominaATabla,                 //Modulo de la interfaz TransBank - Recepci�n de Rendiciones
    FrmRecepcionUniversoSantander,            //Modulo de la interfaz Santander - Recepci�n de universo
    FrmGenerarDebitosSantander,               //Modulo de la interfaz Santander - Generaci�n de Debitos
    RendicionPAC,                             //M�dulo de la interfaz Santanter - Recepci�n de Rendiciones
    FrmRecepcionRendicionesSantander,         //M�dulo de la interfaz Santanter - Recepci�n de Rendiciones PET Y POC
    FrmRecepcionRendicionesBDP,               //M�dulo de la interfaz Santanter - Recepci�n de Rendiciones BDP
//    Imprenta,                                 //M�dulo de la interfaz Imprenta - Impresi�n masiva para imprenta
    ImprentaJORDAN,                           //M�dulo de la interfaz Imprenta - Impresi�n masiva para imprenta
    CancelarImpresionMasiva,                  //M�dulo de la interfaz Imprenta - Anulacion de procesos masivos de impresion
    CatalogoImagenes,                         //M�dulo de la interfaz Imprenta - ABM de im�genes para impresi�n masiva
    frmRecepcionRendicionesLider,             //Modulo de la interfaz Lider - Recepcion de Rendiciones
    EnvioMandatosPresto,                      //Modulo de la interfaz Presto - Envio de Mandatos
    frmRecepcionMandatosPresto,               //Modulo de la interfaz Presto - Permite procesar el estado en que se encuentran los mandatos en PRESTO.
    frmGenerarUniversoPresto,                 //Modulo de la interfaz Presto - Recepci�n del Universo de Presto
  	FrmGenerarDebitosPresto,                  //Modulo de la interfaz Presto - Generaci�n de Debitos
    frmRecepcionRendicionesPresto,            //Modulo de la interfaz Presto - Procesa la rendiciones recibidas de Presto
    CargaDevolucionesCorreo,                  //Modulo de la interfaz Correo - Formulario para la carga de los comprobantes devueltos por el correo
    DayPass,                                  //Modulo de la interfaz Servipag -	DayPass
    FrmRvm,                                   //Modulo de la interfaz RNVM - Consulta al RNVM
    RNVMConfig,                               //Modulo de la interfaz RNVM - Configuraci�n del servicio RNVM
    FrmConsultaRNVMTransitosValidados,        //Modulo de la interfaz RNVM -  Consulta al RNVM por Transitos Validados
    RNVMSeguridad,                              //Modulo de la interfaz RNVM -  Seguridad
    frmInterfazImprentaInfractores,           //M�dulo de la interfaz Imprenta/Infractores - Notificaciones a infractores: interfaz para imprenta
    frmCancelarOperacionesImprentaInfractores,//M�dulo de la interfaz Imprenta/Infractores - M�dulo para la anulacion de procesos masivos de impresion
    FrmGenerarArchivoNovedades,               //Modulo de la interfaz Falabella - Generar Archivo de Novedades
    FrmRecepcionNovedades,                    //Modulo de la interfaz Falabella - Procesar Respuesta recibida del archivo de Novedades Enviado
    frmRecepcionCaptacionesCMR,               //Modulo de la interfaz Falabella - Recepci�n del Archivo de Captaciones de Falabella
    //frmRecepcionCaptacionesCMR_VS,            //Modulo de la interfaz Falabella - Recepci�n del Archivo de Captaciones de Falabella Vespucio Sur   // SS_1314_CQU_20150908  // SS_1147_CQU_20150324
    frmEnvioRespuestasCaptaciones,            //Modulo de la interfaz Falabella - Gerneraci�n del Archivo de Respuestas a Captaciones CMR-Falabella
    FrmGenerarArchivoCuadratura,              //M�dulo de la interfaz Falabella - Generar Archivo de Cuadratura
    frmGeneracionDebitosCMR,                  //Modulo de la interfaz Falabella - Generaci�n de D�bitos a Tarjetas CMR-Falabella
    frmRecepcionRendicionesCMR,               //Modulo de la interfaz Falabella - Recepci�n de Rendiciones CMR-Falabella
    frmGeneracionXMLInfracciones,             //Modulo de la interfaz Infractores XML - Genera los archivos XML de infractores para enviar al MOPTT
    fRecepcionXMLRespuestaInfraccion,         //Modulo de la interfaz Infractores XML - Procesa los archivos XML de respuestas de infracciones del MOPTT
    ExplorarEMails,                           //Servicio de Mails - Explorador de Mails
    ABMFirmasEmail,                           //Servicio de Mails - ABM de Mantenimiento de Firmas para servicio de mails
    ABMPlantillasEmail,                       //Servicio de Mails - ABM de Mantenimiento de Plantillas para servicio de mails
    frmConfiguracionEnviosAutomaticos,        //Servicio de Mails - Modulo de configuracion de emails
    frmContabilizaciondeJornada,              //Modulo de la interfaz Contable - Contabilizaci�n de Jornada
    frmCierredeJornada,                       //Modulo de la interfaz Contable - Cierre de Jornada
    FrmProcesosContabilizados,                //Modulo de la interfaz Contable - Ver Procesos Contabilizados
    ABMVentasPasesDiarios,                    //Modulo de la interfaz Contable - ABM de Ventas de Pases Diarios
    ABMPorcentajeVentasPDU,                   //Modulo de la interfaz Contable - ABM Porcentaje por Ventas de PDU
    ABMPorcentajeVentasBHT,                   //Modulo de la interfaz Contable - ABM Porcentaje por Ventas de BHT
    //ABMDepositoEmpresasDeCobranza,            //Modulo de la interfaz Contable - ABM Deposito Empresas de Cobranza
    FReporteLibroVentasPeajes,                //Modulo de la interfaz Contable - Libro de Ventas de Peaje
    FrmRptLibroVentas,                        //Modulo de la interfaz Contable - Libro de Ventas
    fActivacionesSerbanc,                     //Modulo de la interfaz Serbanc - Generacion de Archivos de Activaciones Serbanc
    fDesactivacionesSerbanc,                  //Modulo de la interfaz Serbanc - Generacion de Archivos de Desactivaciones Serbanc
    fAccionesSerbanc,                         //Modulo de la interfaz Serbanc - Recepcion de Acciones de Cobranzas por parte de Serbanc
    fPagosSerbanc,                            //Modulo de la interfaz Serbanc - Recepcion de Archivo de Pagos Serbanc
    fCambiosTipoCobranzaSerbanc,              //Modulo de la interfaz Serbanc - Cambios tipo de cobranza
    fRecepcionDireccionesSerbanc,             //Modulo de la interfaz Serbanc - Recepcion de Archivo de Propuestas de Cambios de Tipo de Cobranza de Serbanc
    fEnvioInfraccionesClearingBHTU,           //Modulo de la interfas de Clearing BHTU - Envio de Archivo de Infracciones
    fReprocesarEnvioInfraccionesClearingBHTU, //Modulo de Reprocesamiento de Envio de Infracciones para Clearing BHTU
    fRececpcionArchivoInfraccionesClearingBHTU,//Modulo de Recepcion de Infracciones para el Clearing de BHTU
    fRecepcionArchVentasBHTU,                 //Recepcion de Archivo de Ventas de BHTu
    fProcesarAsignacionClearingBHTU,          //Proceso de Asignacion de BHTU
    fProcesoAsignacionBHTUaInfraccion,          //Proceso de Asignacion de BHTU
    fGenerarArchivoAsignacionesBHTU,          //Proceso de Generacion del Archivo de Asignaciones
    FConsultaBHTU,                            //Consulta y modificacion de BHTU
    FrmRecepcionRendicionesServipag,          //Modulo de la interfaz Servipag - Recepci�n de Rendiciones
    FrmRecepcionRendicionesMisCuentas,        //Modulo de la interfaz MisCuentas - Recepci�n de Rendiciones
    FrmWebAuditoria,                            //Modulo de la Web - Ver registros de auditoria
    //ConsultaRNUT,
    //RNUTDBImport,
    frmRecepcionAnulacionesServipag,            //Modulo de la interfaz Servipag - Recepci�n de Anulaciones
    frmCopecPDU,                                //Modulo COPEC PDU
    // Rev.4 / 13-Noviembre-2009 / Nelson Droguett Sierra ----------------------------------
    frmCambioPersonaRNVM,                       //Cambio de Persona RNVM
    frmReEmisionAnulacionDiscoXMLInfracciones,
    //--------------------------------------------------------------------------------------
    //General
    //Rev. 5
    FrmGenerarDebitosBcoChile,
    RendicionPACBancoChile,
    FrmRecepcionUniversoBcoChile,
    FrmGenerarArchivoTransaccionesRechazadas,   // Modulo de la interfaz Informes - Informes Otras Concesionarias SS_1015_20120329_CQU
    //frmGenerarArchivoEstacionamientosRechazados, //SS-1006-EBA-20120227
  	Forms, jpeg, Controls, Classes, Menus, ComCtrls, ExtCtrls,Windows,  Dialogs,
    ExtDlgs, sysUtils, Graphics, DB, ADODB, ImgList, RStrings, Mensajes, XPMan, ShellAPI,
    FrmGenerarSalidaFPOCFoliosFacturacion,          // SS_1015_HUR_20120409
    FrmGenerarSalidaFPOCFoliosPagos,                // SS_1015_HUR_20120409
    frmGenerarNominaCMRRNUT,                         //SS_1140_MCA_20131118
    FrmGenerarArchivoIVAConcesionarias,              //SS-1015-NDR-20120425
    frmRespuestasAConsultasRNVM,                     //SS_1400_NDR_20151007
    frmAsignarConsultasRNVMInfracciones,             //CAC-CLI-066_MGO_20160218 //SS_1406_NDR_20151007
    // INICIO : 20160513 MGO
    frmEjecutarInterfaz,
    // FIN : 20160513 MGO
    frmABMTiposCorrespondencia,                      //CAC-CLI-066_MGO_20160218
    FormDynacTollRates;                                   //TASK_110_JMA_20170224
type
  TMainForm = class(TForm)
    StatusBar: TStatusBar;
    Imagenes: TImageList;
    ImagenFondo: TImage;
    MenuImagenes: TImageList;
    MainMenu1: TMainMenu;
  	mnu_Archivo: TMenuItem;
    mnu_salir: TMenuItem;
    mnu_Interfaces: TMenuItem;
    mnu_Mantenimiento: TMenuItem;
    mnu_CambiarPassword: TMenuItem;
    mnu_ventana: TMenuItem;
    mnu_Cascada: TMenuItem;
    mnu_Mosaico: TMenuItem;
    MenuItem5: TMenuItem;
    mnu_Siguiente: TMenuItem;
    mnu_Anterior: TMenuItem;
    mnu_Ayuda: TMenuItem;
    mnu_AcercaDe: TMenuItem;
    TransBank1: TMenuItem;
    mnu_GenerarArchivodeNomina: TMenuItem;
    mnu_GenerarArchivoMovimientos1: TMenuItem;
    mnu_ConvertirMovimientosaTabla: TMenuItem;
    mnu_ConvertirNominaATabla: TMenuItem;
    mnu_VerLogdeOperacionesRealizadas: TMenuItem;
    mnu_Santander: TMenuItem;
    mnu_GeneracionDeDebitosSantander: TMenuItem;
    Imprenta1: TMenuItem;
    mnu_ProcesarArchivodeRendiciondeDebitos: TMenuItem;
    mnu_RecepciondeUniverso: TMenuItem;
    mnu_Reprocesar: TMenuItem;
    mnu_Presto: TMenuItem;
    mnu_RecepciondeUniversoPresto: TMenuItem;
    mnu_EnviodeMandatosPresto: TMenuItem;
    mnu_RecepciondeMandatosPresto: TMenuItem;
    mnu_GeneracindeDebitosPresto: TMenuItem;
    mnu_RecepciondeRendicionesPresto: TMenuItem;
    Lder1: TMenuItem;
    mnu_RecepcionRendicionesLider: TMenuItem;
    mnu_Administracindeimgenesdecorativas: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    mnu_ReprocesarEnviodeMandatos: TMenuItem;
    mnu_Anular_Proceso_Impresion: TMenuItem;
    N4: TMenuItem;
    mnu_ReprocesarGeneraciondeDebitos: TMenuItem;
    N3: TMenuItem;
    Correos1: TMenuItem;
    mnu_IngresarCorrespondenciaRechazada: TMenuItem;
    mnu_ProcesarBaseEntera: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N9: TMenuItem;
    ServiPag1: TMenuItem;
    mnu_GestiondePases: TMenuItem;
    RVM1: TMenuItem;
    mnu_ConsultasalRvm: TMenuItem;
    ConfigurarProxy1: TMenuItem;
    mnu_ImprentaInfractores: TMenuItem;
    Infractores1: TMenuItem;
    mnu_Informes: TMenuItem;
    Comprobantes1: TMenuItem;
    mnu_CancelacionImprentaInfractores: TMenuItem;
    mnu_RecepcionNovedades: TMenuItem;
    mnu_Infracciones: TMenuItem;
    mnu_GenerarXMLInfracciones1: TMenuItem;
    mnu_ReprocesarDiscoXML: TMenuItem;
    Mnu_CMRFalabella: TMenuItem;
    mnu_RecepcionNovedadesCMR: TMenuItem;
    //mnu_EnvioRespuestasCaptacionesCMR: TMenuItem;	//SS_1140_MCA_20131118
    mnu_RecepcionCaptacionesCMR: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    mnu_GeneracionDebitosCMR: TMenuItem;
    mnu_RecepcionRendicionesCMR: TMenuItem;
    mnu_RepcocesarGeneracionDebitosCMR: TMenuItem;
    mnu_ReprocesarGeneracionDebitosTBK: TMenuItem;
    N5: TMenuItem;
    mnu_ReprocerEnvioMandatosTBK: TMenuItem;
    mnu_ReprocesarEnvioNovedadesCMR: TMenuItem;
    ServiciodeEmails1: TMenuItem;
    mnu_CargarArchivo: TMenuItem;
    dlgAbrir: TOpenDialog;
    N8: TMenuItem;
    mnu_MantenimientodeFirmas: TMenuItem;
    mnu_MantenimientodePlantillas: TMenuItem;
    mnu_RecepciondeRespaDiscoXMLInf: TMenuItem;
    XPManifest1: TXPManifest;
    mnu_Contable: TMenuItem;
    mnu_LibroAuxiliarVentasPeajes: TMenuItem;
    Mante1: TMenuItem;
    Configuraciondeenvodeemails1: TMenuItem;
    Serbanc1: TMenuItem;
    EnviodeActivacionesSerbanc: TMenuItem;
    EnvodeArchivodeDesactivacionesyCambios1: TMenuItem;
    N14: TMenuItem;
    RecepcindeAccionesdeCobranza1: TMenuItem;
    RecepindePagosRecibidos1: TMenuItem;
    RececpcindeSugerenciasdeCambios1: TMenuItem;
    RececpcindeCambiosdeDomicilio1: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N24: TMenuItem;
    ClearingBHTU1: TMenuItem;
    ProcesarAsignacionBHTU1: TMenuItem;
    N27: TMenuItem;
    mnu_EnvioCuadratura: TMenuItem;
    RecepcindeArchivodeVentasdeBHTU1: TMenuItem;
    mnu_Servipag: TMenuItem;
    mnu_RecepcionRendicionesServipag: TMenuItem;
    N13: TMenuItem;
    mnu_RecepcionRendicionesSantanderPECyPOC: TMenuItem;
    mnu_LibrodeVentas: TMenuItem;
    RecepcindeRendicionesExpress1: TMenuItem;
    Mnu_RecepciondeRendicionesBDP: TMenuItem;
    Web1: TMenuItem;
    N22: TMenuItem;
    mnu_VerLogAuditoria: TMenuItem;
    mnu_RecepcionRendicionesUnCajaPortal: TMenuItem;
    N30: TMenuItem;
    mnu_RecepcionAnulacionesServipag: TMenuItem;
    mnu_RecepcionRendicionesUnExpress: TMenuItem;
    mnuCopec: TMenuItem;
    N23: TMenuItem;
    mnuProcesarPDU: TMenuItem;
    mnuprocesarPDUT: TMenuItem;
    mnuGeneracionDeNominas: TMenuItem;
    N25: TMenuItem;
    mnuRecepcionRendicionesPorRUT: TMenuItem;
    mnu_Proceso_Masivo_JORDAN: TMenuItem;
    Mnu_BancoChile: TMenuItem;
    mnu_RecepcionUniversoBancoChile: TMenuItem;
    N26: TMenuItem;
    mnu_GeneracionDeDebitosBcoChile: TMenuItem;
    N28: TMenuItem;
    mnu_RecepcionRendicionesBcoChile: TMenuItem;
    mnu_PagoCtaMovilBcoChile: TMenuItem;
    RendicionesUnificado: TMenuItem;
    mnu_RendicionesMisCuentas: TMenuItem;
    mnu_RendicionesCMR: TMenuItem;
    mnu_RendicionesUnimarc: TMenuItem;
    mnu_RendicionesMulticaja: TMenuItem;
    N31: TMenuItem;                                                             // SS_1015_20120329_CQU
    mnu_InformesOtrasConcesionarias: TMenuItem;                                 // SS_1015_20120329_CQU
    mnu_TransaccionesRechazadas: TMenuItem;										// SS_1015_20120329_CQU	
    N32: TMenuItem;																// SS_1015_20120329_CQU
    mnu_FPOCFoliosFacturacion: TMenuItem;                                       // SS_1015_HUR_20120409
    mnu_FPOCFoliosPagos: TMenuItem;                                             // SS_1015_HUR_20120409
    mnu_IVAOtrasConcesionarias: TMenuItem;                                      //SS-1015-NDR-20120425
    mnu_GenerarContratosCMR_RNUT: TMenuItem;                                    //SS_1140_MCA_20131118
    mnu_RecepcionRendicionesServipagPortal: TMenuItem;                          // SS_1375_CQU_20150903
    mnu_RecepcionRendicionesUnPortal: TMenuItem;                                // SS_1375_CQU_20150903
    mnuInterfazEntranteRespuestasRNVM: TMenuItem;                          //SS_1406_NDR_20151022
    mnuTiposCorrespondencia: TMenuItem;
    N33: TMenuItem;
    mnu_Dynac: TMenuItem;
    mnu_TollRates: TMenuItem;
    mnu_InterfacesNotificaciones: TMenuItem;
    N29: TMenuItem;
    mnu_InterfacesEnviarNotifCasosaRespAreas: TMenuItem;
    mnu_InterfacesImportarCartolaBancos: TMenuItem;
    N34: TMenuItem;
    mnu_InterfacesConciliarCartBancRen: TMenuItem;
    mnuEnvioDeCartasDeConvenios: TMenuItem;
    // INICIO : TASK_175_MGO_20170420
    mnu_PACGenrico: TMenuItem;
    mnu_DebitosPACGenrico: TMenuItem;
    mnu_RendicionesPACGenerico: TMenuItem;
    mnu_EnvioUniversoPACGenerico: TMenuItem;
    mnu_RecepcionUniversoPACGenerico: TMenuItem;                          				//CAC-CLI-066_MGO_20160218
    // FIN : TASK_175_MGO_20170420
    procedure LibrodeVentas1Click(Sender: TObject);
    //procedure mnu_RecepcionRendicionesMisCuentasClick(Sender: TObject);       //SS_969_ALA_20110811
    procedure mnu_RecepcionRendicionesSantanderPECyPOCClick(Sender: TObject);
    procedure mnu_PorcentajeVentasBHTClick(Sender: TObject);
    procedure mnu_PorcentajeVentasPDUClick(Sender: TObject);
    procedure mnu_LibrodeVentasClick(Sender: TObject);
    procedure ConsultadeBHTU1Click(Sender: TObject);
    procedure Seguridad1Click(Sender: TObject);
    procedure ProcesarAsignacionBHTU1Click(Sender: TObject);
    procedure RecepcindeArchivodeVentasdeBHTU1Click(Sender: TObject);
    procedure mnu_EnvioCuadraturaClick(Sender: TObject);
    procedure EnvodeInfracciones1Click(Sender: TObject);
    procedure EnviodeInfracciones1Click(Sender: TObject);
//    procedure mnu_VerProcesosContabilizadosClick(Sender: TObject);
    procedure mnu_LibroAuxiliarVentasPeajesClick(Sender: TObject);
	  procedure RececpcindeCambiosdeDomicilio1Click(Sender: TObject);
    procedure RececpcindeSugerenciasdeCambios1Click(Sender: TObject);
    procedure RecepindePagosRecibidos1Click(Sender: TObject);
    procedure mnu_VentasPasesDiariosClick(Sender: TObject);
    procedure RecepcindeAccionesdeCobranza1Click(Sender: TObject);
    procedure EnvodeArchivodeDesactivacionesyCambios1Click(Sender: TObject);
    procedure EnviodeActivacionesSerbancClick(Sender: TObject);
    procedure Configuraciondeenvodeemails1Click(Sender: TObject);
    procedure mnu_CierredeJornadaContableClick(Sender: TObject);
    procedure mnu_ContabilizaciondeJornadaClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure mnu_siguienteClick(Sender: TObject);
    procedure mnu_anteriorClick(Sender: TObject);
    procedure mnu_cascadaClick(Sender: TObject);
    procedure mnu_mosaicoClick(Sender: TObject);
    procedure mnu_AcercaDeClick(Sender: TObject);
    procedure mnu_salirClick(Sender: TObject);
    procedure mnu_CambiarPasswordClick(Sender: TObject);
    procedure mnu_ventanaClick(Sender: TObject);
    procedure mnu_GenerarArchivoMovimientos1Click(Sender: TObject);
    procedure mnu_GenerarArchivodeNominaClick(Sender: TObject);
    procedure mnu_ConvertirMovimientosaTablaClick(Sender: TObject);
    procedure mnu_ConvertirNominaATablaClick(Sender: TObject);
    procedure mnu_VerLogdeOperacionesRealizadasClick(Sender: TObject);
    procedure mnu_GeneracionDeDebitosSantanderClick(Sender: TObject);
    procedure mnu_ProcesoMasivodeImpresionClick(Sender: TObject);
    procedure mnu_ProcesarArchivodeRendiciondeDebitosClick(Sender: TObject);
  	procedure mnu_ReprocesarClick(Sender: TObject);
    procedure mnu_RecepciondeUniversoClick(Sender: TObject);
    procedure mnu_RecepcionRendicionesLiderClick(Sender: TObject);
    procedure mnu_AdministracindeimgenesdecorativasClick(Sender: TObject);
    procedure mnu_EnviodeMandatosPrestoClick(Sender: TObject);
    procedure mnu_ReprocesarEnviodeMandatosClick(Sender: TObject);
    procedure mnu_RecepciondeMandatosPrestoClick(Sender: TObject);
    procedure mnu_Anular_Proceso_ImpresionClick(Sender: TObject);
    procedure mnu_GeneracindeDebitosPrestoClick(Sender: TObject);
    procedure mnu_RecepciondeUniversoPrestoClick(Sender: TObject);
    procedure mnu_ReprocesarGeneraciondeDebitosClick(Sender: TObject);
    procedure mnu_RecepciondeRendicionesPrestoClick(Sender: TObject);
    procedure mnu_IngresarCorrespondenciaRechazadaClick(Sender: TObject);
    procedure mnu_ProcesarBaseEnteraClick(Sender: TObject);
    procedure mnu_GestiondePasesClick(Sender: TObject);
    procedure mnu_ConsultasalRvmClick(Sender: TObject);
    procedure mnu_ConsultasRNVMTransitosValidadosClick(Sender: TObject);
    procedure ConfigurarProxy1Click(Sender: TObject);
    procedure mnu_ImprentaInfractoresClick(Sender: TObject);
    procedure mnu_CancelacionImprentaInfractoresClick(Sender: TObject);
    procedure mnu_RecepcionNovedadesClick(Sender: TObject);
    procedure mnu_InformesClick(Sender: TObject);
    procedure mnu_GenerarXMLInfracciones1Click(Sender: TObject);
    procedure mnu_ReprocesarDiscoXMLClick(Sender: TObject);
    procedure mnu_EnvioNovedadesCMRClick(Sender: TObject);
    procedure mnu_RecepcionCaptacionesCMRClick(Sender: TObject);
    procedure mnu_RecepcionNovedadesCMRClick(Sender: TObject);
    //procedure mnu_EnvioRespuestasCaptacionesCMRClick(Sender: TObject);		SS_1140_MCA_20131118
    procedure mnu_GeneracionDebitosCMRClick(Sender: TObject);
    procedure mnu_RepcocesarGeneracionDebitosCMRClick(Sender: TObject);
    procedure mnu_RecepcionRendicionesCMRClick(Sender: TObject);
    procedure mnu_ReprocesarGeneracionDebitosTBKClick(Sender: TObject);
    procedure mnu_ReprocerEnvioMandatosTBKClick(Sender: TObject);
    procedure mnu_ReprocesarEnvioNovedadesCMRClick(Sender: TObject);
    procedure mnu_CargarArchivoClick(Sender: TObject);
    procedure mnu_MantenimientodeFirmasClick(Sender: TObject);
    procedure mnu_MantenimientodePlantillasClick(Sender: TObject);
    procedure mnu_RecepciondeRespaDiscoXMLInfClick(Sender: TObject);
    procedure Mnu_RecepciondeRendicionesBDPClick(Sender: TObject);
    procedure mnu_VerLogAuditoriaClick(Sender: TObject);
	procedure ExploradordeConsultasRNVM1Click(Sender: TObject);
    procedure mnu_RecepcionAnulacionesServipagClick(
      Sender: TObject);
    procedure mnuProcesarPDUClick(Sender: TObject);
    procedure mnuprocesarPDUTClick(Sender: TObject);
    procedure mnuGeneracionDeNominasClick(Sender: TObject);
    procedure RecepcionRendicionesServipag(Sender: TObject);
    procedure mnu_Proceso_Masivo_JORDANClick(Sender: TObject);
    procedure mnuCambioPersonaRNVMClick(Sender: TObject);
    procedure mnu_GeneracionDeDebitosBcoChileClick(Sender: TObject);
    procedure mnu_RecepcionUniversoBancoChileClick(Sender: TObject);
    procedure mnu_RecepcionRendicionesBcoChileClick(Sender: TObject);
    procedure mnu_PagoCtaMovilBcoChileClick(Sender: TObject);                   //SS 961 05-04-2011 ALABRA
    //procedure RecepcionRendicionesMisCuentas(NombreMenu : String);            //SS_1378_MCA_20150915  //SS 961 05-04-2011 ALABRA
    procedure RecepcionRendicionesMisCuentas(NombreMenu : String; CodigoModulo: integer);  //SS_1378_MCA_20150915 //SS 961 05-04-2011 ALABRA
    procedure FormCreate(Sender: TObject);
    procedure mnu_RendicionesMisCuentasClick(Sender: TObject);                  //SS_969_ALA_20110811
    procedure mnu_RendicionesCMRClick(Sender: TObject);                         //SS_969_ALA_20110811
    procedure mnu_RendicionesUnimarcClick(Sender: TObject);                     //SS_969_ALA_20110811
    procedure mnu_RendicionesMulticajaClick(Sender: TObject);                   //SS_969_ALA_20110811
    procedure mnu_TransaccionesRechazadasClick(Sender: TObject);                // SS_1015_20120329_CQU
    procedure mnu_InformesOtrasConcesionariasClick(Sender: TObject);			// SS_1015_20120329_CQU
    procedure mnu_FPOCFoliosFacturacionClick(Sender: TObject);                  // SS_1015_HUR_20120409
    procedure mnu_FPOCFoliosPagosClick(Sender: TObject);                        // SS_1015_HUR_20120409
    procedure mnu_IVAOtrasConcesionariasClick(Sender: TObject);                 //SS-1015-NDR-20120425
    procedure mnu_GenerarContratosCMR_RNUTClick(Sender: TObject);				//SS_1140_MCA_20131118
    function CambiarEventoMenu(Menu: TMenu): Boolean;
    procedure ServiPag1Click(Sender: TObject);                           //SS_1147_NDR_20141216
    procedure mnuInterfazEntranteRespuestasRNVMClick(Sender: TObject);			//SS_1400_NDR_20151007
    procedure mnuAsignacionConsultasRNVMInfraccionesClick(Sender: TObject);		//SS_1406_NDR_20151022
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnuTiposCorrespondenciaClick(Sender: TObject);                  //CAC-CLI-066_MGO_20160218
    procedure mnu_TollRatesClick(Sender: TObject);
    procedure mnu_InterfacesEnviarNotifCasosaRespAreasClick(
      Sender: TObject);
    procedure mnu_InterfacesImportarCartolaBancosClick(Sender: TObject);
    procedure mnu_InterfacesConciliarCartBancRenClick(
      Sender: TObject);
    procedure mnuEnvioDeCartasDeConveniosClick(Sender: TObject);
    procedure mnu_DebitosPACGenricoClick(Sender: TObject);
    procedure mnu_RendicionesPACGenericoClick(Sender: TObject);
    procedure mnu_EnvioUniversoPACGenericoClick(Sender: TObject);
    procedure mnu_RecepcionUniversoPACGenericoClick(Sender: TObject);				              //TASK_110_JMA_20170224

  private
    FUsarProxy: Boolean;
    FProxyServer: String;
    FProxyPort: Integer;
    FCodigoNativa : Integer; // SS_1147_CQU_20150324
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                //SS_1147_NDR_20141216
    procedure RefrescarBarra;
    procedure DrawMenuItem(Sender: TObject; ACanvas: TCanvas;ARect: TRect; Selected: Boolean);             //SS_1147_NDR_20141216
    { Private declarations }
  public
	{ Public declarations }
	function Inicializar: Boolean;
  end;

var
  MainForm: TMainForm;

Const																				//SS_1378_MCA_20150915
  RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_UNIMARC = 73;								//SS_1378_MCA_20150915
  RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_CMR = 76;									//SS_1378_MCA_20150915
  RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_SENCILLITO = 79;								//SS_1378_MCA_20150915

implementation

uses frmExploradorConsultaRNVM, FrmStartup;

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}   

{******************************** Function Header ******************************
Function Name: Inicializar
Author : lgisuk
Date Created : 25/11/2004
Description :  inicializacion de este formulario
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TMainForm.Inicializar: Boolean;
resourcestring
    CAPTION_USER = 'Usuario: %s';
    MSG_CONFIGURAR_CONTROL_CODIGO_MD5   = 'Configurar control del c�digo MD5 (Interfaz L�der)';
    MSG_IMPRIMIR_NK_ENVIO_ELECTRONICO   = 'Permite el env�o a imprenta de Notas de Cobro marcadas para envio electr�nico (Interfaz Imprenta)';
    MSG_FORZAR_REIMPRESION_NK           = 'Fuerza el env�o a imprenta de Notas de Cobro ya impresas (Interfaz Imprenta)';
    MSG_SEGURIDAD_RNVM					= 'Formulario de Consultas al RNVM';										//SS_1143_MCA_20131108
    MSG_SEGURIDAD_BOTONES_RNVM			= 'Botones';                                            					//SS_1143_MCA_20131108
    MSG_CONSULTA_ULTIMA_MODIFICACION	= 'Consulta ultima modificaci�n';                     	  				    //SS_1143_MCA_20131108
    MSG_CONSULTA_RNVM					= 'Consultar al RNVM';                                  					//SS_1143_MCA_20131108
    MSG_ABRIR_XML_RNVM					= 'Permite abrir el XML la �ltima consulta al RNVM';    					//SS_1143_MCA_20131108
    MSG_ACTUALIZAR_DATOS_INFRACTORES	= 'actualizar datos de infractores al consultar';       					//SS_1143_MCA_20131108
    MSG_FORZAR_CONSULTA_RNVM			= 'forzar consulta al RNVM';                            					//SS_1143_MCA_20131108
const
    //FuncionesAdicionales: Array[1..3] of TPermisoAdicional = (													//SS_1143_MCA_20131108
	FuncionesAdicionales: Array[1..10] of TPermisoAdicional = (                										//SS_1143_MCA_20131108
        (Funcion: 'configurar_control_codigo_MD5'; Descripcion: MSG_CONFIGURAR_CONTROL_CODIGO_MD5; Nivel: 1),
        (Funcion: 'imprimir_nk_envio_electronico'; Descripcion: MSG_IMPRIMIR_NK_ENVIO_ELECTRONICO; Nivel: 1),
        //(Funcion: 'forzar_reimpresion_nk';Descripcion: MSG_FORZAR_REIMPRESION_NK; Nivel: 1));						//SS_1143_MCA_20131108
        (Funcion: 'forzar_reimpresion_nk';Descripcion: MSG_FORZAR_REIMPRESION_NK; Nivel: 1),						//SS_1143_MCA_20131108
		(Funcion: 'seguridad_RNVM'; Descripcion: MSG_SEGURIDAD_RNVM; Nivel:1),                                      //SS_1143_MCA_20131108
        (Funcion: 'seguridad_botones'; Descripcion: MSG_SEGURIDAD_BOTONES_RNVM; Nivel:2),                           //SS_1143_MCA_20131108
        (Funcion: 'Boton_Consulta_Ultima_modificacion'; Descripcion: MSG_CONSULTA_ULTIMA_MODIFICACION; Nivel:3),    //SS_1143_MCA_20131108
        (Funcion: 'Boton_Consulta_RNVM'; Descripcion: MSG_CONSULTA_RNVM; Nivel:3),                                  //SS_1143_MCA_20131108
        (Funcion: 'Boton_AbrirXML'; Descripcion: MSG_ABRIR_XML_RNVM; Nivel:3),                                      //SS_1143_MCA_20131108
        (Funcion: 'Check_Actualizar_Datos_Infractores'; Descripcion: MSG_ACTUALIZAR_DATOS_INFRACTORES; Nivel:3),    //SS_1143_MCA_20131108
        (Funcion: 'Check_Forzar_Consulta_RNVM'; Descripcion: MSG_FORZAR_CONSULTA_RNVM; Nivel:3));                   //SS_1143_MCA_20131108
Var
	f: TLoginForm;
begin
	Result := False;
    SistemaActual:=SYS_GESTION_INTERFACES;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    try
        Application.CreateForm(TLoginForm, f);
        if f.Inicializar(DMConnections.BaseCAC, SYS_GESTION_INTERFACES) and (f.ShowModal = mrOk) then begin
            UsuarioSistema := f.CodigoUsuario;

            //BEGIN : SS_925_NDR_20131122-----------------------------------------------------------------------
            //if not ValidarVersion(DMConnections.BaseCAC, SistemaActual) then begin
            //    MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
            //    Exit;
            //end;
            //
            //// Generamos el men� en la base de datos
            //if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) then begin
            //    GenerateMenuFile(Menu, SYS_GESTION_INTERFACES, DMConnections.BaseCAC, FuncionesAdicionales);
            //end;
            {INICIO:	20160831 CFU
            CargarInformes(DMConnections.BaseCAC, SYS_GESTION_INTERFACES, MainMenu1);      //SS_1437_NDR_20151230
            TERMINO:	20160831 CFU}
            if not ValidarVersion(DMConnections.BaseCAC, SistemaActual, Menu, FuncionesAdicionales) then begin
                MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
                Exit;
            end;
            //END : SS_925_NDR_20131122-----------------------------------------------------------------------

            { INICIO : 20160315 MGO
            FUsarProxy := ApplicationIni.ReadBool('RNVM', 'USEPROXY', False);
            if FUsarProxy then begin
                FProxyServer := ApplicationIni.ReadString('RNVM', 'PROXYSERVER', '');
                FProxyPort := ApplicationIni.ReadInteger('RNVM', 'SERVERPORT', 0);
            end;
            }
            FUsarProxy := InstallIni.ReadBool('RNVM', 'USEPROXY', False);
            if FUsarProxy then begin
                FProxyServer := InstallIni.ReadString('RNVM', 'PROXYSERVER', '');
                FProxyPort := InstallIni.ReadInteger('RNVM', 'SERVERPORT', 0);
            end;
            // FIN : 20160315 MGO

            StatusBar.Panels[0].text := Format(CAPTION_USER, [UsuarioSistema]);
            StatusBar.Panels[1].Text := Trim(Format(MSG_CONECTADO_A, [InstallIni.ReadString('Database CAC', 'Server', ''), InstallIni.ReadString('Database CAC', 'DatabaseName', '')]));
            StatusBar.Panels[1].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[1].text));
            RefrescarBarra;

            CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_GESTION_INTERFACES);
            HabilitarPermisosMenu(Menu);
            //CargarInformes(DMConnections.BaseCAC, SYS_GESTION_INTERFACES, MainMenu1,DMConnections.cnInformes);      //SS_1437_NDR_20151230
            //INICIO:	20160831 CFU
            CargarInformes(DMConnections.BaseCAC, SYS_GESTION_INTERFACES, MainMenu1);      //SS_1437_NDR_20151230
            //TERMINO:	20160831 CFU


            CambiarEventoMenu(MainMenu1);                                    //SS_1147_NDR_20141216
            FCodigoNativa := ObtenerCodigoConcesionariaNativa;          // SS_1147_CQU_20150324
            //if ObtenerCodigoConcesionariaNativa = CODIGO_VS           // SS_1147_CQU_20150324  // SS_1147_CQU_20150316
            if (FCodigoNativa = CODIGO_VS)                              // SS_1147_CQU_20150324
            then mnu_RendicionesMisCuentas.Caption := 'Sencillito'      // SS_1147_CQU_20150316
            else mnu_RendicionesMisCuentas.Caption := 'Mis Cuentas';    // SS_1147_CQU_20150316

            //INICIO: TASK_017_ECA_20160528
            mnu_Presto.Visible:=False;
            Lder1.Visible:=False;
            mnu_Santander.Visible:=False;
            Mnu_CMRFalabella.Visible:=False;
            RendicionesUnificado.Visible:=False;
            ServiPag1.Visible:=False;
            ClearingBHTU1.Visible:=False;
            mnuCopec.Visible:=False;
            Serbanc1.Visible:=False; 
            //FIN: TASK_017_ECA_20160528

            Result := True;
        end else begin
            UsuarioSistema := f.CodigoUsuario;                                  //SS_1436_NDR_20151230
            Result := False;
        end;
    finally
        f.Release;
        if Assigned(FormStartup) then FreeAndNil(FormStartup);
    end;
end;

{******************************** Function Header ******************************
Function Name: FormPaint
Author : lgisuk
Date Created : 25/11/2004
Description : Dibuja la imagen de fondo
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}


procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
resourcestring                                                                                    //SS_1436_NDR_20151230
	MSG_REGISTRO_CAPTION	= 'Error al Grabar Registro de Operaciones';                              //SS_1436_NDR_20151230
var                                                                                               //SS_1436_NDR_20151230
  DescriError: array[0..255] of char;                                                             //SS_1436_NDR_20151230
begin
    if ( CrearRegistroOperaciones(                                                                //SS_1436_NDR_20151230
            DMCOnnections.BaseCAC,                                                                //SS_1436_NDR_20151230
            SYS_GESTION_INTERFACES,                                                               //SS_1436_NDR_20151230
            RO_MOD_LOGOUT,                                                                        //SS_1436_NDR_20151230
            RO_AC_LOGOUT,                                                                         //SS_1436_NDR_20151230
            0,                                                                                    //SS_1436_NDR_20151230
            GetMachineName,                                                                       //SS_1436_NDR_20151230
            UsuarioSistema,                                                                       //SS_1436_NDR_20151230
            'LOGOUT DEL SISTEMA',                                                                 //SS_1436_NDR_20151230
            0,0,0,0,                                                                              //SS_1436_NDR_20151230
            DescriError) = -1 ) then                                                              //SS_1436_NDR_20151230
    begin                                                                                         //SS_1436_NDR_20151230
      MsgBoxErr(MSG_ERROR_ACCESO, StrPas(DescriError), MSG_REGISTRO_CAPTION, MB_ICONSTOP);        //SS_1436_NDR_20151230
    end;                                                                                          //SS_1436_NDR_20151230
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
	{$IFNDEF PRODUCCION}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
    {$ELSE}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
        //Caption := Caption + Format(' - Versi�n: %s', [GetFileVersionString(GetExeName, True)]);
    {$ENDIF}
end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
     DrawMDIBackground(imagenFondo.Picture.Graphic);
end;

{******************************** Function Header ******************************
Function Name: mnu_SiguienteClick
Author : lgisuk
Date Created : 25/11/2004
Description : Ventana --> Siguiente
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_SiguienteClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Siguiente') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Next;
end;

{******************************** Function Header ******************************
Function Name: mnu_AnteriorClick
Author : lgisuk
Date Created : 25/11/2004
Description : Ventana --> Anterior
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_AnteriorClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Anterior') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Previous;
end;

{******************************** Function Header ******************************
Function Name: mnu_CascadaClick
Author : lgisuk
Date Created : 25/11/2004
Description : Ventana --> Cascada
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_CascadaClick(Sender: TObject);
begin
	if not ExisteAcceso('mnu_Cascada') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Cascade;
end;

{******************************** Function Header ******************************
Function Name: mnu_MosaicoClick
Author : lgisuk
Date Created : 25/11/2004
Description : Ventana --> Mosaico
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_MosaicoClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Mosaico') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Tile;
end;



{******************************** Function Header ******************************
Function Name: mnu_VentanaClick
Author : lgisuk
Date Created : 25/11/2004
Description : Ventana
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_VentanaClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Ventana') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
	mnu_cascada.Enabled := GetMenuItemCount(mnu_ventana.handle) > 5;
	mnu_mosaico.Enabled := mnu_cascada.Enabled;
	mnu_anterior.Enabled := GetMenuItemCount(mnu_ventana.handle) > 7;
	mnu_siguiente.Enabled := mnu_anterior.Enabled;
end;

{******************************** Function Header ******************************
Function Name: mnu_AcercaDeClick
Author : lgisuk
Date Created : 25/11/2004
Description : Muestra ventana de Acerca de..
Parameters : Sender: TObject
Return Value : None

Revision : 1
    Author : ggomez
    Date : 29/01/2007
    Description : En la llamada f.Inicializa, agregu� el segundo par�metro ''
        debido a que cambi� el perfil de la funci�n en las Utiles.
*******************************************************************************}
procedure TMainForm.mnu_AcercaDeClick(Sender: TObject);
{                                                   SS_925_MBE_20131223
Var
	f: TAboutfrm;
    a: TfrmLocalAssistance;
}
begin
{                                                   SS_925_MBE_20131223
	if not ExisteAcceso('mnu_AcercaDe') then begin
		MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
		Exit;
	end;
    Application.CreateForm(TfrmLocalAssistance, a);
    try
        if a.Inicializar then begin
            Application.CreateForm(TAboutFrm, f);
            if f.Inicializa(Caption, '', '', a) then f.ShowModal;
            f.Release;
        end;
    finally
        a.Release;
    end; // finally
}
    Application.CreateForm(TAcercaDeForm, AcercaDeForm);
    AcercaDeForm.ShowModal;
    AcercaDeForm.Release;
end;

{******************************** Function Header ******************************
Function Name: mnu_SalirClick
Author : lgisuk
Date Created : 25/11/2004
Description : Permite salir de la aplicaci�n
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_SalirClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Salir') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Close;
end;

{******************************** Function Header ******************************
Function Name: mnu_CambiarPasswordClick
Author : lgisuk
Date Created : 25/11/2004
Description : Permite cambiar password
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_CambiarPasswordClick(Sender: TObject);
Var
	f: TFormCambioPassword;
begin
 	if not ExisteAcceso('mnu_CambiarPassword') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
	Application.CreateForm(TFormCambioPassword, f);
	if f.Inicializa(DMConnections.BaseCAC, UsuarioSistema) then f.ShowModal;
	f.Release;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarArchivoMovimientos1Click
  Author:    lgisuk
  Date Created: 26/11/2004
  Description:  Generar Archivo de Movimientos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_GenerarArchivoMovimientos1Click(Sender: TObject);
{ INICIO : 20160513 MGO
var
    f: TfGenerarArchivoMovimientos;
begin
	if FindFormOrCreate(TfGenerarArchivoMovimientos, f) then begin
		f.Show
    end
	else begin
		if not f.Inicializar then f.Release;
	end;
}
resourcestring
    QRY_CODIGO_MODULO = 'SELECT dbo.CONST_CODIGO_MODULO_ENVIO_MOVIMIENTOS_TRANSBANK()';
var
    f: TFormEjecutarInterfaz;
    CodigoModulo: Integer;
begin
    CodigoModulo := QueryGetValueInt(DMConnections.BaseCAC, QRY_CODIGO_MODULO);

    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(CodigoModulo) then f.Release;
	end;
// FIN : 20160513 MGO
end;

procedure TMainForm.mnu_GenerarContratosCMR_RNUTClick(Sender: TObject);         //SS_1140_MCA_20131118
var                                                                             //SS_1140_MCA_20131118
    formGenerarNominaCMRRNUT: TFGenerarNominaCMRRNUT;                             //SS_1140_MCA_20131118
begin                                                                           //SS_1140_MCA_20131118

	Application.CreateForm(TFGenerarNominaCMRRNUT, formGenerarNominaCMRRNUT);		//SS_1140_MCA_20131118
    if formGenerarNominaCMRRNUT.Inicializar(mnu_GenerarContratosCMR_RNUT.Caption, True)  then begin	  //SS_1140_MCA_20131118
        formGenerarNominaCMRRNUT.ShowModal;										//SS_1140_MCA_20131118
        formGenerarNominaCMRRNUT.Release;										//SS_1140_MCA_20131118
    end else begin																//SS_1140_MCA_20131118
        formGenerarNominaCMRRNUT.Release;										//SS_1140_MCA_20131118
    end;																		//SS_1140_MCA_20131118
end;                                                                            //SS_1140_MCA_20131118

{******************************** Function Header ******************************
Function Name: mnu_ReprocerEnvioMandatosTBKClick
Author : lgisuk
Date Created : 11/07/2005
Description :  Permito Re-Generar el Archivo de Movimientos
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_ReprocerEnvioMandatosTBKClick(Sender: TObject);
var
    f: TfGenerarArchivoMovimientos;
begin
	if FindFormOrCreate(TfGenerarArchivoMovimientos, f) then begin
		f.Show
    end
	else begin
		if not f.Inicializar(True) then f.Release;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: GenerarArchivodeNmina1Click
  Author:    lgisuk
  Date Created: 26/11/2004
  Description:  Generar Archivo de Nomina
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_GenerarArchivodeNominaClick(Sender: TObject);
{ INICIO : 20160513 MGO
var
    f: TfGenerarArchivoNomina;
begin
	if FindFormOrCreate(TfGenerarArchivoNomina, f) then begin
		f.Show
    end
	else begin
		if not f.Inicializar(mnu_GenerarArchivodeNomina.Caption, True) then f.Release;
	end;
}
resourcestring
    QRY_CODIGO_MODULO = 'SELECT dbo.CONST_CODIGO_MODULO_GENERACION_NOMINA_TRANSBANK()';
var
    f: TFormEjecutarInterfaz;
    CodigoModulo: Integer;
begin
    CodigoModulo := QueryGetValueInt(DMConnections.BaseCAC, QRY_CODIGO_MODULO);

    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(CodigoModulo) then f.Release;
	end;
// FIN : 20160513 MGO
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_ReprocesarGeneracionDebitosTBKClick
  Author:    lgisuk
  Date Created: 05/07/2005
  Description:  Re - Generar Archivo de Nomina
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ReprocesarGeneracionDebitosTBKClick(Sender: TObject);
var
    f: TfGenerarArchivoNomina;
begin
	if FindFormOrCreate(TfGenerarArchivoNomina, f) then begin
		f.Show
    end
	else begin
		if not f.Inicializar(mnu_GenerarArchivodeNomina.Caption, True, True) then f.Release;
	end;
end;



{-----------------------------------------------------------------------------
  Function Name: ConvertirMovimientosaTabla1Click
  Author:    lgisuk
  Date Created: 26/11/2004
  Description: Procesar Archivo de Respuesta a Movimientos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ConvertirMovimientosaTablaClick(Sender: TObject);
{ INICIO : 20160513 MGO
var
    f: TfConvertirMovimientosATabla;
begin
	if FindFormOrCreate(TfConvertirMovimientosATabla, f) then begin
		f.Show
    end
	else begin
		if not f.Inicializar(mnu_ConvertirMovimientosaTabla.Caption,'RESPUESTAMOVIC', True) then f.Release;
	end;
}
resourcestring
    QRY_CODIGO_MODULO = 'SELECT dbo.CONST_CODIGO_MODULO_RESPUESTA_MOVIMIENTOS_TRANSBANK()';
var
    f: TFormEjecutarInterfaz;
    CodigoModulo: Integer;
begin
    CodigoModulo := QueryGetValueInt(DMConnections.BaseCAC, QRY_CODIGO_MODULO);

    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(CodigoModulo) then f.Release;
	end;
// FIN : 20160513 MGO
end;

{-----------------------------------------------------------------------------
  Function Name: ProcesarBaseEntera1Click
  Author:    lgisuk
  Date Created: 26/01/2005
  Description: Procesar Base Entera
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ProcesarBaseEnteraClick(Sender: TObject);
var
    f: TfConvertirMovimientosATabla;
begin
	if FindFormOrCreate(TfConvertirMovimientosATabla, f) then begin
		f.Show
    end
	else begin
		if not f.Inicializar(mnu_ProcesarBaseEntera.Caption,'BASEINSTDECARGO', True) then f.Release;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: ConvertirNominaaTabla1Click
  Author:    lgisuk
  Date Created: 26/11/2004
  Description:  Procesar Archivo de Nomina
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ConvertirNominaATablaClick(Sender: TObject);
{ INICIO : 20160513 MGO
var
    f: TfConvertirNominaATabla;
begin
	if FindFormOrCreate(TfConvertirNominaATabla, f) then begin
		f.Show
    end
	else begin
		if not f.Inicializar(mnu_ConvertirNominaaTabla.Caption, True) then f.Release;
	end;
}
resourcestring
    QRY_CODIGO_MODULO = 'SELECT dbo.CONST_CODIGO_MODULO_RECEPCION_RENDICIONES_TRANSBANK()';
var
    f: TFormEjecutarInterfaz;
    CodigoModulo: Integer;
begin
    CodigoModulo := QueryGetValueInt(DMConnections.BaseCAC, QRY_CODIGO_MODULO);

    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(CodigoModulo) then f.Release;
	end;
// FIN : 20160513 MGO
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_VerLogAuditoriaClick
  Author:    lgisuk
  Date Created: 25/10/2006
  Description: Log de Auditoria de la WEB
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_VerLogAuditoriaClick(Sender: TObject);
var
    f: TFWebAuditoria;
begin
    if FindFormOrCreate(TFWebAuditoria, f) then  f.Show
    else if not f.Inicializar(mnu_VerLogAuditoria.caption, True) then f.Release;
end;


procedure TMainForm.mnu_VerLogdeOperacionesRealizadasClick(Sender: TObject);
var
	f: TfLogOperaciones;
begin
	if FindFormOrCreate(TFLogOperaciones, f) then begin
		f.Show
    end
	else begin
		if not f.Inicializar(mnu_VerLogdeOperacionesRealizadas.Caption, True) then f.Release;
	end;
end;

//Rev. 5
procedure TMainForm.mnu_GeneracionDeDebitosBcoChileClick(Sender: TObject);
{ INICIO : 20160513 MGO
var
    f: TFGeneracionDebitosBcoChile;
begin
   	if FindFormOrCreate(TFGeneracionDebitosBcoChile, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_GeneracionDeDebitosBcoChile.Caption, True, False) then f.Release;
	end;
}
resourcestring
    QRY_CODIGO_MODULO = 'SELECT dbo.CONST_CODIGO_MODULO_GENERACION_NOMINA_BANCOCHILE()';
var
    f: TFormEjecutarInterfaz;
    CodigoModulo: Integer;
begin
    CodigoModulo := QueryGetValueInt(DMConnections.BaseCAC, QRY_CODIGO_MODULO);

    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(CodigoModulo) then f.Release;
	end;
// FIN : 20160513 MGO
end;


{-----------------------------------------------------------------------------
  Function Name: mnu_ConvertirUniversoSantanderATablaClick
  Author:    flamas
  Date Created: 01/12/2004
  Description: Convierte el Universo Santander a la Tabla UniversoPAC
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_GeneracionDeDebitosSantanderClick(Sender: TObject);
var
	f: TFGeneracionDebitosSantander;
begin
	if FindFormOrCreate(TFGeneracionDebitosSantander, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_GeneracionDeDebitosSantander.Caption, True, False) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_ReprocesarInterfaceClick
  Author:    flamas
  Date Created: 09/12/2004
  Description:  Reprocesar Interface generacion de debitos santander
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ReprocesarClick(Sender: TObject);
var
    f: TFGeneracionDebitosSantander;
begin
	if FindFormOrCreate(TFGeneracionDebitosSantander, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_Reprocesar.Caption, True, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ProcesoMasivodeImpresin1Click
  Author:    gcasais
  Date Created: 06/12/2004
  Description: Llamada al m�dulo de imprenta
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ProcesoMasivodeImpresionClick(Sender: TObject);
//var
//	f: TfrmImprenta;
begin
//	if FindFormorCreate(TfrmImprenta, f) then begin
//		f.Show;
//	end else begin
//		if not f.Inicializar then f.Release;
//	end;
end;

procedure TMainForm.mnu_Proceso_Masivo_JORDANClick(Sender: TObject);
var
	f: TfrmImprentaJORDAN;
begin
	if FindFormorCreate(TfrmImprentaJORDAN, f) then begin
		f.Show;
	end else begin
		if not f.Inicializar then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ProcesarArchivodeRendicindeDbitos1Click
  Author:    ndonadio
  Date Created: 09/12/2004
  Description: Importa Archivo de Rendiciones PAC y actualiza DB
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ProcesarArchivodeRendiciondeDebitosClick(Sender: TObject);
var
	f: TFrmRendicionPAC;
begin
	if FindFormOrCreate(TFrmRendicionPAC, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_ProcesarArchivodeRendiciondeDebitos.Caption, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_GeneracindeUniversoClick
  Author:    flamas
  Date Created: 13/12/2004
  Description:   Recepcion Universo Santander
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_RecepciondeUniversoClick(Sender: TObject);
var
	f: TFRecepcionUniversoSantander;
begin
	if FindFormOrCreate(TFRecepcionUniversoSantander, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_RecepciondeUniverso.Caption, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_RecepcionRendicionesLiderClick
  Author:    flamas
  Date Created: 23/12/2004
  Description: Procesa el Archivo de Rendiciones de Lider
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_RecepcionRendicionesLiderClick(Sender: TObject);
var
	f: TfRecepcionRendicionesLider;
begin
	if FindFormOrCreate(TfRecepcionRendicionesLider, f) then begin
		f.Show
	end	else begin
		if not f.Inicializar(mnu_RecepcionRendicionesLider.Caption, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: Administracindeimgenesdecorativas1Click
  Author:    gcasais
  Date Created: 11/03/2005
  Description:   Catalogo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_AdministracindeimgenesdecorativasClick( Sender: TObject);
var
    f: TfrmCatalogo;
begin
    if FindFormorCreate(TfrmCatalogo, f) then begin
        f.Show;
    end else begin
        if not f.Inicializar then f.Release;
    end;
end;

{-------------------------------------------------- ---------------------------
  Function Name: mnu_EnviodeMandatosPrestoClick
  Author:    flamas
  Date Created: 27/12/2004
  Description: Genera el Archivo del Env�o de Mandatos de Presto
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_EnviodeMandatosPrestoClick(Sender: TObject);
var
	f: TfEnvioMandatosPresto;
begin
	if FindFormOrCreate(TfEnvioMandatosPresto, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_EnviodeMandatosPresto.Caption, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_ReprocesarEnviodeMandatosClick
  Author:    flamas
  Date Created: 28/12/2004
  Description: Reprocesa el Env�o de Mandatos de Presto
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ReprocesarEnviodeMandatosClick(Sender: TObject);
var
	f: TfEnvioMandatosPresto;
begin
	if FindFormOrCreate(TfEnvioMandatosPresto, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_EnviodeMandatosPresto.Caption, True, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_RecepciondeMandatosPrestoClick
  Author:    flamas
  Date Created: 28/12/2004
  Description: Procesa el Archivo de Mandatos de Presto
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_RecepciondeMandatosPrestoClick(Sender: TObject);
var
	f: TfRecepcionMandatosPresto;
begin
	if FindFormOrCreate(TfRecepcionMandatosPresto, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_RecepciondeMandatosPresto.Caption, True ) then f.Release;
	end;
end;

{******************************** Function Header ******************************
Function Name: Mnu_RecepciondeRendicionesBDP
Author : lgisuk
Date Created : 14/08/2006
Description : Recepcion de Rendiciones de BDP
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.Mnu_RecepciondeRendicionesBDPClick(Sender: TObject);
var
    f: TfRecepcionRendicionesBDP;
begin
	if FindFormOrCreate(TfRecepcionRendicionesBDP, f) then begin
		f.Show
  end	else begin
		if not f.Inicializar(Mnu_RecepciondeRendicionesBDP.Caption) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_Anular_Proceso_ImpresionClick
  Author:    MRodriguez
  Date Created: 11/03/2005
  Description: Anular Proceso de Impresion
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_Anular_Proceso_ImpresionClick(Sender: TObject);
var
	f: TFrmCancelarImpresionMasiva;
begin
	if FindFormOrCreate(TFrmCancelarImpresionMasiva, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_GeneracindeDebitosPrestoClick
  Author:    flamas
  Date Created: 03/01/2005
  Description: Genera el Archivo de D�bitos de Presto
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_GeneracindeDebitosPrestoClick(Sender: TObject);
var
	f: TFGeneracionDebitosPresto;
begin
	if FindFormOrCreate(TFGeneracionDebitosPresto, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_GeneracindeDebitosPresto.Caption, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_ReprocesarGeneraciondeDebitosClick
  Author:    flamas
  Date Created: 04/01/2005
  Description: Reprocesa la Generaci�n de D�bitos de Presto
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ReprocesarGeneraciondeDebitosClick( Sender: TObject);
var
	f: TFGeneracionDebitosPresto;
begin
	if FindFormOrCreate(TFGeneracionDebitosPresto, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_GeneracindeDebitosPresto.Caption, True, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_RecepciondeUniversoPrestoClick
  Author:    Rcastro
  Date Created: 11/03/2005
  Description: Recepcion del Universo Presto
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_RecepciondeUniversoPrestoClick(Sender: TObject);
var
	f: TFUniversoPresto;
begin
	if FindFormOrCreate(TFUniversoPresto, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_RecepciondeUniversoPresto.Caption, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_RecepciondeRendicionesPrestoClick
  Author:    flamas
  Date Created: 04/01/2005
  Description: Recepci�n del Archivo de Rendiciones de Presto
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_RecepciondeRendicionesPrestoClick(Sender: TObject);
var
	f: TfRecepcionRendicionesPresto;
begin
	if FindFormOrCreate(TfRecepcionRendicionesPresto, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_RecepciondeRendicionesPresto.Caption, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: IngresarCorrespondenciaRechazada1Click
  Author:    Mrodriguez
  Date Created: 11/03/2005
  Description: Ingresar Correspondencia Rechazada
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_IngresarCorrespondenciaRechazadaClick(Sender: TObject);
var
    f: TfrmCargaDevolucionesCorreo;
begin
    if FindFormOrCreate(TfrmCargaDevolucionesCorreo, f) then begin
        f.Show;
    end else begin
        if f.Inicializar then f.Show else f.Release;
    end;
end;

procedure TMainForm.mnu_IVAOtrasConcesionariasClick(Sender: TObject);                                   //SS-1015-NDR-20120425
var                                                                                                     //SS-1015-NDR-20120425
    f: TFGenerarArchivoIVAConcesionarias;                                                               //SS-1015-NDR-20120425
begin                                                                                                   //SS-1015-NDR-20120425
	if FindFormOrCreate(TFGenerarArchivoIVAConcesionarias, f, True) then begin                            //SS-1015-NDR-20120425
		f.Show                                                                                              //SS-1015-NDR-20120425
    end                                                                                                 //SS-1015-NDR-20120425
	else begin                                                                                            //SS-1015-NDR-20120425
		if not f.Inicializar(mnu_IVAOtrasConcesionarias.Caption, True, 'TRA') then f.Release;               //SS-1015-NDR-20120425
	end;                                                                                                  //SS-1015-NDR-20120425
end;                                                                                                    //SS-1015-NDR-20120425

{-----------------------------------------------------------------------------
  Function Name: GestindePases1Click
  Author:    Flamas
  Date Created: 11/03/2005
  Description:  Gestion de Day Pass
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_GestiondePasesClick(Sender: TObject);
Resourcestring
    MSG_ERROR = 'Error';
    MSG_INIT_ERROR = 'No se ha podido inicializar';
Const
    STR_TITLE = 'Gesti�n de Pases Diarios';
var
    f: TfrmDayPass;
begin
   if FindFormOrCreate(TFrmDayPass, f) then begin
        f.Show;
   end else begin
        if f.Inicializar then f.Show else begin
            f.Free;
            MsgBoxErr(MSG_ERROR, MSG_INIT_ERROR , STR_TITLE , MB_ICONERROR);
        end;
   end;
end;

{-----------------------------------------------------------------------------
  Function Name: ConsultasalRvm1Click
  Author:    lgisuk
  Date Created: 23/02/2005
  Description: Consultas al RNVM
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ConsultasalRvmClick(Sender: TObject);
{ INICIO : TASK_159_MGO_20170323
var
    f: TfRvm;
begin
	if FindFormOrCreate(TfRvm, f) then begin
		f.Show
    end	else begin
		if not f.Inicializar then f.Release;
	end;
}
resourcestring
    QRY_CODIGO_MODULO = 'SELECT dbo.CONST_CODIGO_MODULO_ENVIO_CONSULTAS_RNVM()';
var
    f: TFormEjecutarInterfaz;
    CodigoModulo: Integer;
begin
    CodigoModulo := QueryGetValueInt(DMConnections.BaseCAC, QRY_CODIGO_MODULO);

    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(CodigoModulo) then f.Release;
	end;
end;
// FIN : TASK_159_MGO_20170323

{******************************** Function Header ******************************
Function Name: ConsultasRNVMTransitosValidadosClick
Author : gcasais
Date Created : 18/07/2005
Description :  Consultas al RNVM por Transitos Validados
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_ConsultasRNVMTransitosValidadosClick(Sender: TObject);
var
    f: TFormConsultaRNVMTransitosValidados;
begin
	if FindFormOrCreate(TFormConsultaRNVMTransitosValidados, f) then begin
		f.Show;
    end else begin
		if not f.Inicializar(True) then f.Release;
	end;
end;

{******************************** Function Header ******************************
Function Name: ConfigurarProxy1Click
Author : gcasais
Date Created : 18/07/2005
Description : Configuraci�n del Proxy para comunicarse con el RNVM
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.ConfigurarProxy1Click(Sender: TObject);
var
    f: TfrmConfig;
begin
    Application.CreateForm(Tfrmconfig, f);
    if f.inicializar then begin
        f.ShowModal;
        if f.ModalResult = mrOk then begin
            { INICIO : 20160315 MGO
            FUsarProxy := ApplicationIni.ReadBool('RNVM', 'USEPROXY', False);
            if FUsarProxy then begin
                FProxyServer := ApplicationIni.ReadString('RNVM', 'PROXYSERVER', '');
                FProxyPort := ApplicationIni.ReadInteger('RNVM', 'SERVERPORT', 0);
            end else begin
                FProxyServer := '';
                FProxyPort := 0;
            end;
            }
            FUsarProxy := InstallIni.ReadBool('RNVM', 'USEPROXY', False);
            if FUsarProxy then begin
                FProxyServer := InstallIni.ReadString('RNVM', 'PROXYSERVER', '');
                FProxyPort := InstallIni.ReadInteger('RNVM', 'SERVERPORT', 0);
            end else begin
                FProxyServer := '';
                FProxyPort := 0;
            end;
            // FIN : 20160315 MGO
            f.Release;
            RefrescarBarra;
        end;
    end else begin
        f.Release;
    end;
end;

{******************************** Function Header ******************************
Function Name: RefrescarBarra
Author : gcasais
Date Created : 18/07/2005
Description : Actualiza la barra para indicar si usa o no proxy
Parameters : None
Return Value : None
*******************************************************************************}
procedure TMainForm.RefrescarBarra;
Const
    STR_PROXY = 'PROXY: ';
    STR_PORT =  ' Port: ';
    STR_NOT_USE_PROXY = 'NO USA PROXY';
begin
    if FUsarProxy then
        StatusBar.Panels[2].Text := STR_PROXY + FProxyServer + STR_PORT + IntToStr(FProxyPort)
    else
        StatusBar.Panels[2].Text := STR_NOT_USE_PROXY;
end;

{******************************** Function Header ******************************
Function Name: mnuImprentaInfractoresClick
Author : rcastro
Date Created : 18/07/2005
Description : Notificaciones a infractores: interfaz para imprenta
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_ImprentaInfractoresClick(Sender: TObject);
var
	f: TFormInterfazImprentaInfractores;
begin
	if FindFormorCreate(TFormInterfazImprentaInfractores, f) then begin
		f.Show;
	end else begin
		if not f.Inicializar then f.Release;
	end;
end;

{******************************** Function Header ******************************
Function Name: mnuCancelacionImprentaInfractoresClick
Author : rcastro
Date Created : 18/07/2005
Description : M�dulo para la anulacion de procesos masivos de impresion
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_CancelacionImprentaInfractoresClick(Sender: TObject);
var
	f: TFormCancelarNotificacionMasivaInfractores;
begin
	if FindFormOrCreate(TFormCancelarNotificacionMasivaInfractores, f) then
		f.Show
	else
        if not f.Inicializar () then f.Release;
end;

{-----------------------------------------------------------------------------
  Procedure: TMainForm.RecepcindeNovedades1Click
  Author:    lgisuk
  Date:      13-May-2005
  Arguments: Recepcion de Novedades
  Result:    None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_RecepcionNovedadesClick(Sender: TObject);
var
    f: TfConvertirMovimientosATabla;
begin
	if FindFormOrCreate(TfConvertirMovimientosATabla, f) then begin
		f.Show
    end
	else begin
		if not f.Inicializar(mnu_RecepcionNovedades.Caption,'NOVEDADESIC', True) then f.Release;
	end;
end;

{******************************** Function Header ******************************
Function Name: mnu_InformesClick
Author :
Date Created : 18/07/2005
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_InformesClick(Sender: TObject);
begin
    //
end;

procedure TMainForm.mnu_InformesOtrasConcesionariasClick(Sender: TObject);
begin

end;

{-----------------------------------------------------------------------------
  Function Name: mnu_GenerarXMLInfracciones1Click
  Author:    ndonadio
  Date Created: 15/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_GenerarXMLInfracciones1Click(Sender: TObject);
var
	f: TfGeneracionXMLInfracciones;
begin
	if FindFormOrCreate(TfGeneracionXMLInfracciones, f) then begin
        f.Show
	end	else begin
		if not f.Inicializar then f.Release
 	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_ReprocesarDiscoXMLClick
  Author:    ndonadio
  Date Created: 15/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ReprocesarDiscoXMLClick(Sender: TObject);
var
	f: TReEmisionAnulacionDiscoXMLInfraccionesForm;
begin
	if FindFormOrCreate(TReEmisionAnulacionDiscoXMLInfraccionesForm, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar then f.Release
	end;
end;

procedure TMainForm.mnu_EnvioNovedadesCMRClick(Sender: TObject);
begin

end;

{-----------------------------------------------------------------------------
  Function Name: mnu_RecepcionCaptacionesCMRClick
  Author:    flamas
  Date Created: 15/06/2005
  Description: Procesa el Archivo de Captaciones de CMR
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_RecepcionCaptacionesCMRClick(Sender: TObject);
var
	f: TfRecepcionCaptacionesCMR;
    //f_vs : TfRecepcionCaptacionesCMR_VS;                                      // SS_1314_CQU_20150908  // SS_1147_CQU_20150324
begin
    //if (FCodigoNativa = CODIGO_VS) then begin                                 // SS_1314_CQU_20150908  // SS_1147_CQU_20150324
    //    if FindFormOrCreate(TfRecepcionCaptacionesCMR_VS, f_vs) then begin    // SS_1314_CQU_20150908  // SS_1147_CQU_20150324
    //        f_vs.Show                                                         // SS_1314_CQU_20150908  // SS_1147_CQU_20150324
    //    end	else begin                                                      // SS_1314_CQU_20150908  // SS_1147_CQU_20150324
    //        if not f_vs.Inicializar(mnu_RecepcionCaptacionesCMR.Caption, True)// SS_1314_CQU_20150908  // SS_1147_CQU_20150324
    //        then f_vs.Release;                                                // SS_1314_CQU_20150908  // SS_1147_CQU_20150324
    //    end;                                                                  // SS_1314_CQU_20150908  // SS_1147_CQU_20150324
    //end else begin                                                            // SS_1314_CQU_20150908  // SS_1147_CQU_20150324
	if FindFormOrCreate(TfRecepcionCaptacionesCMR, f) then begin
		f.Show
	end	else begin
		if not f.Inicializar(mnu_RecepcionCaptacionesCMR.Caption, True ) then f.Release;
	end;
    //end;                                                                      // SS_1314_CQU_20150908  // SS_1147_CQU_20150324
end;

{-----------------------------------------------------------------------------
  Function Name: Mnu_RecepcionNovedadesCMRClick
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: Recepci�n de Novedades Falabella
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_RecepcionNovedadesCMRClick(Sender: TObject);
var
    f: TfRecepcionNovedades;
begin
	if FindFormOrCreate(TfRecepcionNovedades, f) then begin
		f.Show
    end	else begin
		if not f.Inicializar(Mnu_RecepcionNovedadesCMR.Caption,True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_EnvioRespuestasCaptacionesCMRClick
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Genera el Archivo de Respuesta a Captaciones
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
//procedure TMainForm.mnu_EnvioRespuestasCaptacionesCMRClick(Sender: TObject);  SS_1140_MCA_20131118
//var                                                                           SS_1140_MCA_20131118
//	f: TfEnvioRespuestasCaptaciones;                                            SS_1140_MCA_20131118
//begin                                                                         SS_1140_MCA_20131118
//	if FindFormOrCreate(TfEnvioRespuestasCaptaciones, f) then begin             SS_1140_MCA_20131118
//		f.Show                                                                  SS_1140_MCA_20131118
//	end	else begin                                                              SS_1140_MCA_20131118
//		if not f.Inicializar(mnu_EnvioRespuestasCaptacionesCMR.Caption, True ) then f.Release; SS_1140_MCA_20131118
//	end;                                                                        SS_1140_MCA_20131118
//end;                                                                          SS_1140_MCA_20131118

{-----------------------------------------------------------------------------
  Function Name: mnu_GeneracionDebitosCMRClick
  Author: FLamas
  Date Created: 21/06/2005
  Description: Genera los D�bitos de CMR-Falabella
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_GeneracionDebitosCMRClick(Sender: TObject);
var
	f: TFGeneracionDebitosCMR;
begin
	if FindFormOrCreate(TFGeneracionDebitosCMR, f) then begin
		f.Show;
	end	else begin
		if not f.Inicializar(mnu_GeneracionDebitosCMR.Caption, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_RepcocesarGeneracionDebitosCMRClick
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Reprocesa los D�bitos de CMR-Falabella
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_RepcocesarGeneracionDebitosCMRClick(Sender: TObject);
var
	f: TFGeneracionDebitosCMR;
begin
	if FindFormOrCreate(TFGeneracionDebitosCMR, f) then begin
		f.Show
	end	else begin
		if not f.Inicializar(mnu_GeneracionDebitosCMR.Caption, True, True) then f.Release;
	end;
end;

//Rev. 5
procedure TMainForm.mnu_RecepcionRendicionesBcoChileClick(Sender: TObject);
{ INICIO : 20160513 MGO
var
	f: TfrmRendicionPACBancoChile;
begin
	if FindFormOrCreate(TfrmRendicionPACBancoChile, f) then begin
		f.Show;
	end	else begin
		if not f.Inicializar(mnu_RecepcionRendicionesBcoChile.Caption, True) then f.Release;
	end;
}
resourcestring
    QRY_CODIGO_MODULO = 'SELECT dbo.CONST_CODIGO_MODULO_RECEPCION_RENDICIONES_PAC_BANCOCHILE()';
var
    f: TFormEjecutarInterfaz;
    CodigoModulo: Integer;
begin
    CodigoModulo := QueryGetValueInt(DMConnections.BaseCAC, QRY_CODIGO_MODULO);

    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(CodigoModulo) then f.Release;
	end;
// FIN : 20160513 MGO
end;


{-----------------------------------------------------------------------------
  Function Name: mnu_RecepcionRendicionesCMRClick
  Author:    FLamas
  Date Created: 22/06/2005
  Description: Recepci�n de Rendiciones CMR-Falabella
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_RecepcionRendicionesCMRClick(Sender: TObject);
var
	f: TfRecepcionRendicionesCMR;
begin
	if FindFormOrCreate(TfRecepcionRendicionesCMR, f) then begin
		f.Show;
	end	else begin
		if not f.Inicializar(mnu_RecepcionRendicionesCMR.Caption, True) then f.Release;
	end;
end;

{******************************** Function Header ******************************
Function Name: mnu_ReprocesarEnvioNovedadesCMRClick
Author : lgisuk
Date Created : 11/07/2005
Description : Permite Reprocesar el envio de Novedades
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_ReprocesarEnvioNovedadesCMRClick(Sender: TObject);
var
    f: TfGenerarArchivoNovedades;
begin
	if FindFormOrCreate(TfGenerarArchivoNovedades, f) then begin
		f.Show
    end	else begin
		if not f.Inicializar then f.Release;
	end;
end;

{******************************** Function Header ******************************
Function Name: CargarArchivo1Click
Author : drodriguez
Date Created : 15/07/2005
Description :  Recepcion de Mensajes para servicio de Mail desde archivo
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_CargarArchivoClick(Sender: TObject);
var
    f: TfrmExplorarMails;
begin
    if FindFormOrCreate(TfrmExplorarMails, f) then begin
        f.Show;
    end else begin
        if not f.Inicializar then f.Release;
    end;
end;

{******************************** Function Header ******************************
Function Name: MantenimientodeFirmas1Click
Author : drodriguez
Date Created : 15/07/2005
Description :   A/B/M de Mantenimiento de Firmas para Servicio de Mails
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_MantenimientodeFirmasClick(Sender: TObject);
var
    f: TFormFirmasEmail;
begin
    f := TFormFirmasEmail.create(nil);
    try
        if f.Inicializa then f.Show else f.Release;
    except
        f.release;
    end;
end;

{******************************** Function Header ******************************
Function Name: MantenimientodePlantillas1Click
Author :  drodriguez
Date Created : 15/07/2005
Description :  A/B/M de Mantenimiento de Planillas para Servicio de Mails
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_MantenimientodePlantillasClick(Sender: TObject);
var
    f: TFormPlantillasEmail;
begin
    f := TFormPlantillasEmail.create(nil);
    try
        if f.Inicializa then f.Show else f.Release;
    except
        f.release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_RecepcionXMLRespuestasMOP1Click
  Author:    ndonadio
  Date Created: 15/06/2005
  Description: Recepci�n de Respuesta a Disco XML de Infracciones
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_RecepciondeRespaDiscoXMLInfClick(Sender: TObject);
var
	f: TfrmRecepcionXMLRespuestaInfraccion;
begin
 	if FindFormOrCreate(TfrmRecepcionXMLRespuestaInfraccion, f) then begin
 		f.Show;
 	end	else begin
 		if not f.Inicializar then f.Release
 	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_ContabilizaciondeJornadaClick
  Author:    lgisuk
  Date Created: 27/07/2005
  Description: Contabilizaci�n de Jornada
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ContabilizaciondeJornadaClick(Sender: TObject);
var
    f: TfContabilizaciondeJornada;
begin
	if FindFormOrCreate(TfContabilizaciondeJornada, f) then begin
  		f.Show
  end	else begin
  		if not f.Inicializar then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_CierredeJornadaContableClick
  Author:    lgisuk
  Date Created: 27/07/2005
  Description: Cierre de Jornada
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_CierredeJornadaContableClick(Sender: TObject);
var
    f: TfCierredeJornada;
begin
	if FindFormOrCreate(TfCierredeJornada, f) then begin
  		f.Show
  end	else begin
  		if not f.Inicializar then f.Release;
	end;
end;

procedure TMainForm.Configuraciondeenvodeemails1Click(Sender: TObject);
var
    f: TformConfiguracionEnviosAutomaticos;
begin
	Application.CreateForm(TformConfiguracionEnviosAutomaticos, f);
    if f.Inicializar then begin
        f.ShowModal;
        f.Release;
    end else begin
        f.Release;
    end;

end;

{******************************** Function Header ******************************
Function Name: EnviodeActivacionesSerbancClick
Author : ndonadio
Date Created : 09/08/2005
Description :  Lanza el form de Generacion de Archivos de Activaciones
                para enviar a Serbanc
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.EnviodeActivacionesSerbancClick(Sender: TObject);
var
    f: TfrmActivacionesSerbanc;
begin
    if FindFormOrCreate(TfrmActivacionesSerbanc, f) then  f.Show
    else if not f.Inicializar(EnviodeActivacionesSerbanc.Caption) then f.Release;
end;


{******************************** Function Header ******************************
Function Name: EnvodeArchivodeDesactivacionesyCambios1Click
Author : ndonadio
Date Created : 17/08/2005
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.EnvodeArchivodeDesactivacionesyCambios1Click(
  Sender: TObject);
var
    f: TfrmDesactivacionesSerbanc;
begin
    if FindFormOrCreate(TfrmDesactivacionesSerbanc, f) then  f.Show
    else if not f.Inicializar(EnvodeArchivodeDesactivacionesyCambios1.Caption) then f.Release;
end;

procedure TMainForm.ExploradordeConsultasRNVM1Click(Sender: TObject);
var
    f: TExploradorConsultaRNVMForm;
begin
	if FindFormOrCreate(TExploradorConsultaRNVMForm, f) then begin
		f.Show
    end	else begin
		if not f.Inicializar then f.Release;
	end;
end;

{******************************** Function Header ******************************
Function Name: RecepcindeAccionesdeCobranza1Click
Author : ndonadio
Date Created : 10/08/2005
Description :   Abre el form de recepcion de acciones de cobranzas
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.RecepcindeAccionesdeCobranza1Click(Sender: TObject);
var
    f: TfrmAccionesSerbanc;
begin
    if FindFormOrCreate(TfrmAccionesSerbanc, f) then  f.Show
    else if not f.Inicializar(RecepcindeAccionesdeCobranza1.Caption) then f.Release;
end;

{******************************** Function Header ******************************
Function Name: mnu_VentasPasesDiariosClick
Author : lgisuk
Date Created :
Description :   ABM Venta de Pases Diarios
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_VentasPasesDiariosClick(Sender: TObject);
var
    f: TfrmABMVentasPasesDiarios;
begin
    if FindFormOrCreate(TfrmABMVentasPasesDiarios, f) then  f.Show
    else if not f.Inicializar(f.Caption, True) then f.Release;
end;

{******************************** Function Header ******************************
Function Name: RecepindePagosRecibidos1Click
Author : ndonadio
Date Created : 24/08/2005
Description : Abre la interfaz de recepcion de pagos de serbanc
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.RecepindePagosRecibidos1Click(Sender: TObject);
var
    f: TfrmRecepcionArchivoPagosSerbanc;
begin
    if FindFormOrCreate(TfrmRecepcionArchivoPagosSerbanc, f) then  f.Show
    else if not f.Inicializar(RecepindePagosRecibidos1.Caption) then f.Release;
end;


{******************************** Function Header ******************************
Function Name: RececpcindeSugerenciasdeCambios1Click
Author : ndonadio
Date Created : 30/08/2005
Description :   abre la interfaz de recepcion de sugerencia de cambios de tipo de cobranza
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.RececpcindeSugerenciasdeCambios1Click(Sender: TObject);
var
    f: TfrmRecepcionCambioTipoCobranzaSerbanc;
begin
    if FindFormOrCreate(TfrmRecepcionCambioTipoCobranzaSerbanc, f) then  f.Show
    else if not f.Inicializar(RececpcindeSugerenciasdeCambios1.Caption) then f.Release;
end;



{******************************** Function Header ******************************
Function Name: RececpcindeCambiosdeDomicilio1Click
Author : ndonadio
Date Created : 30/08/2005
Description : Abre la interfaz de recepcion de nuevas direcciones de Serbanc
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.RececpcindeCambiosdeDomicilio1Click(Sender: TObject);
var
    f: TfrmRecpcionNuevasDireccionesSerbanc;
begin
    if FindFormOrCreate(TfrmRecpcionNuevasDireccionesSerbanc, f) then  f.Show
    else if not f.Inicializar(RececpcindeCambiosdeDomicilio1.Caption) then f.Release;
end;

{******************************** Function Header ******************************
Function Name: mnu_LibroAuxiliarVentasPeajesClick
Author : ggomez
Date Created :
Description : Libro Auxiliar de Ventas de Peaje
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_LibroAuxiliarVentasPeajesClick(Sender: TObject);
var
    f: TfrmReporteLibroVentasPeajes;
begin
    try
        f := TfrmReporteLibroVentasPeajes.Create(nil);
    if f.Inicializar(mnu_LibroAuxiliarVentasPeajes.Caption) then f.ShowModal;
    finally
    	if Assigned(f) then FreeAndNil(f);
        
    end;
end;

{******************************** Function Header ******************************
Function Name: mnu_VerProcesosContabilizados
Author : lgisuk
Date Created :  12/09/2005
Description : Permite ver los procesos contabilizados
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
(*
procedure TMainForm.mnu_VerProcesosContabilizadosClick(Sender: TObject);
var
    f: TFProcesosContabilizados;
begin
    if FindFormOrCreate(TFProcesosContabilizados, f) then  f.Show
    else if not f.Inicializar(mnu_VerProcesosContabilizados.caption,True) then f.Release;
end;         *)

{******************************** Function Header ******************************
Function Name: EnviodeInfracciones1Click
Author : ndonadio
Date Created : 16/09/2005
Description : Abro el form de la interfaz de envio de archivo de infracciones
                del clearing de BHTU
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.EnviodeInfracciones1Click(Sender: TObject);

begin

end;

{******************************** Function Header ******************************
Function Name: EnvodeInfracciones1Click
Author : ndonadio
Date Created : 26/09/2005
Description : REPROCESAMIENTO - Generaci�n de Archivo de Infracciones
                                para el Clearing de BHTU
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.EnvodeInfracciones1Click(Sender: TObject);
begin

end;
{******************************** Function Header ******************************
				RecepcionRendicionesServipag
Author : mbecerra
Date Created : 19-Agosto-2008
Description : Procesa las rendiciones Servipag
*******************************************************************************}
procedure TMainForm.RecepcionRendicionesServipag(Sender: TObject);
{ INICIO : 20160513 MGO
var
    f: TfRecepcionRendicionesServipag;
    Parametro : string;
begin
	if FindFormOrCreate(TfRecepcionRendicionesServipag, f) then begin
		f.Show
  end	else begin
        // Revision 1
		//if not f.Inicializar(mnu_RecepcionRendicionesServipag.Caption, False) then f.Release;
		// Revision 2
        //if not f.Inicializar(mnu_RecepcionRendicionesServipag.Caption, '02') then f.Release;
        Parametro := IntToStr(TMenuItem(Sender).Tag);
        if TMenuItem(Sender).Tag < 10 then  Parametro := '0' + Parametro;
        if not f.Inicializar(TMenuItem(Sender).Caption, Parametro) then f.Release;

	end;
}
resourcestring
    QRY_CODIGO_MODULO = 'SELECT dbo.CONST_CODIGO_MODULO_RECEPCION_RENDICIONES_SERVIPAG()';
var
    f: TFormEjecutarInterfaz;
    CodigoModulo: Integer;
begin
    CodigoModulo := QueryGetValueInt(DMConnections.BaseCAC, QRY_CODIGO_MODULO);

    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(CodigoModulo) then f.Release;
	end;
// FIN : 20160513 MGO
end;

{******************************** Function Header ******************************
Function Name: mnu_RecepcionAnulacionesServipag
Author: nefernandez
Date Created: 06/07/2007
Description: Recepci�n Archivo de Anulaciones de Servipag (Caja y Portal Unificados)
Parameters: Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_RecepcionAnulacionesServipagClick(
  Sender: TObject);
var
    f: TRecepcionAnulacionesServipagForm;
begin
    if FindFormOrCreate(TRecepcionAnulacionesServipagForm, f) then begin
        f.Show
    end else begin
        if not f.Inicializar(mnu_RecepcionAnulacionesServipag.Caption) then f.Release;
    end;
end;

{******************************** Function Header ******************************
Function Name: mnu_EnvioCuadraturaClick
Author : lgisuk
Date Created : 04/10/2005
Description : Envio de Archivo de Cuadratura a Falabella
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_EnvioCuadraturaClick(Sender: TObject);
var
    f: TFGenerarArchivoCuadratura;
begin
    if  FindFormOrCreate(TFGenerarArchivoCuadratura, f) then f.Show
    else if not f.Inicializar then f.Release;
end;


{******************************** Function Header ******************************
Function Name: RecepcindeArchivodeVentasdeBHTU1Click
Author : ndonadio
Date Created : 05/10/2005
Description : Abre el form de carga del archivo de ventas de BHTU
Parameters : Sender: TObject
Return Value : None

    Revision 1
        Author: pdominguez
        Date  : 13/08/2010
        Description: SS 909 - COPEC BHTU - Rechazos
            - Se cambia la forma de creaci�n del formulario.
*******************************************************************************}
procedure TMainForm.RecepcindeArchivodeVentasdeBHTU1Click(Sender: TObject);
var
    f: TfrmRecepcionArchVentasBHTU;
begin
    try // SS 909
        f := TfrmRecepcionArchVentasBHTU.Create(Self);
        if f.Inicializar(RecepcindeArchivodeVentasdeBHTU1.Caption) then
            f.ShowModal;
    finally
        if Assigned(f) then FreeAndNil(f);
    end;
end;

{******************************** Function Header ******************************
Function Name: ProcesarAsignacionBHTU1Click
Author : ndonadio
Date Created : 06/10/2005
Description : Abre el form para hacer el proceso de asignacion
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.ProcesarAsignacionBHTU1Click(Sender: TObject);
var
    f: TfrmProcesoAsignacionBHTUaInfraccion;
begin
    if  FindFormOrCreate(TfrmProcesoAsignacionBHTUaInfraccion, f) then f.Show
    else if not f.Inicializar(ProcesarAsignacionBHTU1.Caption) then f.Release;
end;

{******************************** Function Header ******************************
Function Name: EnviodeAsignaciones1Click
Author : ndonadio
Date Created : 06/10/2005
Description : Proceos de Generacion del Envio de Asginaciones de BHTU
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.Seguridad1Click(Sender: TObject);
var
    f:TfrmSeguridadRNVM;
begin
    Application.CreateForm(TfrmSeguridadRNVM, f);
    if f.Inicializar then f.ShowModal else f.Release;
end;

procedure TMainForm.ServiPag1Click(Sender: TObject);
begin

end;

{******************************** Function Header ******************************
Function Name: ConsultadeBHTU1Click
Author : ndonadio
Date Created : 10/10/2005
Description : Abre el formulario que permite consultar BHTU por Fecha
              y/o Patente y editar sus valores.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.ConsultadeBHTU1Click(Sender: TObject);
{var
    f : TfrmConsultaBHTU;}
begin
{    if  FindFormOrCreate(TfrmConsultaBHTU, f) then f.Show
    else if not f.Inicializar(ConsultadeBHTU1.Caption) then f.Release;}
end;

{******************************** Function Header ******************************
Function Name: mnu_LibrodeVentasClick
Author : lgisuk
Date Created : 02/11/2005
Description : Libro de Ventas
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_LibrodeVentasClick(Sender: TObject);
var
    f: TFRptLibroVentas;
begin
    Application.CreateForm(TFRptLibroVentas, f);
    if f.Inicializar then f.ShowModal;
end;

{******************************** Function Header ******************************
Function Name: mnu_PorcentajeVentasPDUClick
Author : lgisuk
Date Created : 21/11/2005
Description : A/B/M Porcentaje por Ventas de PDU
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_PorcentajeVentasPDUClick(Sender: TObject);
var
    f: TFAbmPorcentajeVentasPDU;
begin
    if FindFormOrCreate(TFAbmPorcentajeVentasPDU, f) then  f.Show
    else if not f.Inicializar then f.Release;
end;

{******************************** Function Header ******************************
Function Name: mnu_PorcentajeVentasPDUClick
Author : lgisuk
Date Created : 21/11/2005
Description : A/B/M Porcentaje por Ventas de BHT
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_PorcentajeVentasBHTClick(Sender: TObject);
var
    f: TFAbmPorcentajeVentasBHT;
begin
    if FindFormOrCreate(TFAbmPorcentajeVentasBHT, f) then  f.Show
    else if not f.Inicializar then f.Release;
end;


{******************************** Function Header ******************************
Function Name: mnu_RecepcionRendicionesSantander
Author : lgisuk
Date Created : 12/07/2006
Description : Recepci�n Archivo de Rendiciones de Santander (PEC Y POC)
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_RecepcionRendicionesSantanderPECyPOCClick(Sender: TObject);
var
    f: TfRecepcionRendicionesSantander;
begin
	if FindFormOrCreate(TfRecepcionRendicionesSantander, f) then begin
		f.Show
  end	else begin
		if not f.Inicializar(mnu_RecepcionRendicionesSantanderPECyPOC.Caption) then f.Release;
	end;
end;

//Rev. 5
procedure TMainForm.mnu_RecepcionUniversoBancoChileClick(Sender: TObject);
{ INICIO : 20160513 MGO
var
    f: TFRecepcionUniversoBcoChile;
begin
   	if FindFormOrCreate(TFRecepcionUniversoBcoChile, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnu_RecepcionUniversoBancoChile.Caption, True) then f.Release;
	end;
}
resourcestring
    QRY_CODIGO_MODULO = 'SELECT dbo.CONST_CODIGO_MODULO_UNIVERSO_BANCOCHILE()';
var
    f: TFormEjecutarInterfaz;
    CodigoModulo: Integer;
begin
    CodigoModulo := QueryGetValueInt(DMConnections.BaseCAC, QRY_CODIGO_MODULO);

    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(CodigoModulo) then f.Release;
	end;
// FIN : 20160513 MGO
end;

{******************************** Function Header ******************************
Function Name   : mnu_RecepcionRendicionesMisCuentasClick
Author          : lgisuk
Date Created    : 21/07/2006
Description     : Recepcion de Rendiciones de Mis Cuentas
Parameters      : Sender: TObject
Return Value    : None
*******************************************************************************}
{INICIO BLOQUE SS_696_ALA_20110811
procedure TMainForm.mnu_RecepcionRendicionesMisCuentasClick(Sender: TObject);
//var                                                                               //SS 961 05-04-2011 ALABRA
//    f: TfRecepcionRendicionesMisCuentas;                                          //SS 961 05-04-2011 ALABRA
begin
    RecepcionRendicionesMisCuentas(mnu_RecepcionRendicionesMisCuentas.Caption);
//	if FindFormOrCreate(TfRecepcionRendicionesMisCuentas, f) then begin             //SS 961 05-04-2011 ALABRA
//		f.Show                                                                      //SS 961 05-04-2011 ALABRA
//    end	else begin                                                              //SS 961 05-04-2011 ALABRA
//		if not f.Inicializar() then f.Release;                                      //SS 961 05-04-2011 ALABRA
//	end;                                                                            //SS 961 05-04-2011 ALABRA
end;
FIN BLOQUE SS_696_ALA_20110811}


{******************************** Function Header ******************************
Function Name: mnu_LibrodeVentasClick
Author : lgisuk
Date Created : 02/11/2005
Description : Libro de Ventas
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.LibrodeVentas1Click(Sender: TObject);
var
    f: TFRptLibroVentas;
begin
    Application.CreateForm(TFRptLibroVentas, f);
    if f.Inicializar then f.ShowModal;
end;

{****************************************************************************
mnuCambioPersonaRNVMClick
Author: Nelson Droguett Sierra
Date: 13-Noviembre-2009
Description:  Men� de invocaci�n del formulario de Cambio de Persona RNVM.
*****************************************************************************}
procedure TMainForm.mnuCambioPersonaRNVMClick(Sender: TObject);
var
	f : TFCambioPersonaRNVM;
begin
    if FindFormOrCreate(TFCambioPersonaRNVM, f) then begin
        f.Show;
    end
    else begin
        if not f.Inicializar() then f.Release;
    end;
end;

{****************************************************************************
      mnuGeneracionDeNominasClick
Author: mbecerra
Date: 23-Julio-2008
Description:  Men� de invocaci�n del formulario de generaci�n de N�minas.
*****************************************************************************}
procedure TMainForm.mnuGeneracionDeNominasClick(Sender: TObject);
{ INICIO : 20160513 MGO
resourcestring
    MSG_CAPTION = 'Generaci�n de N�minas';
     
var
	f : TGeneracionDeNominasForm;
begin
    if FindFormOrCreate(TGeneracionDeNominasForm, f) then begin
        f.Show;
    end
    else begin
        if not f.Inicializar(MSG_CAPTION) then f.Release;

    end;
}
resourcestring
    QRY_CODIGO_MODULO = 'SELECT dbo.CONST_CODIGO_MODULO_GENERACION_NOMINA_SERVIPAG()';
var
    f: TFormEjecutarInterfaz;
    CodigoModulo: Integer;
begin
    CodigoModulo := QueryGetValueInt(DMConnections.BaseCAC, QRY_CODIGO_MODULO);

    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(CodigoModulo) then f.Release;
	end;
// FIN : 20160513 MGO
end;

{
    Revision 7
        Author      :   pdominguez
        Date        :   12/08/2010
        Description : SS 909 - BHTU Copec Rechazos
            - Se crea y libera el formulario de manera expl�cita.
}
procedure TMainForm.mnuProcesarPDUClick(Sender: TObject);
var
	f : TCopecPDUForm;
begin

    try
        f := TCopecPDUForm.Create(Self);

        if f.Inicializar(True, 'COPEC PDU') then
            f.ShowModal;
    finally
        if Assigned(f) then FreeAndNil(f);
    end;
end;

{
    Revision 7
        Author      :   pdominguez
        Date        :   12/08/2010
        Description : SS 909 - BHTU Copec Rechazos
            - Se crea y libera el formulario de manera expl�cita.
}
procedure TMainForm.mnuProcesarPDUTClick(Sender: TObject);
var
	f : TCopecPDUForm;
begin
    try
        f := TCopecPDUForm.Create(Self);

        if f.Inicializar(False, 'COPEC PDUT') then
            f.ShowModal;
    finally
        if Assigned(f) then FreeAndNil(f);
    end;
end;

{********************************************
*   BEGIN   :   CAC-CLI-066_MGO_20160218    *
*********************************************}
procedure TMainForm.mnuTiposCorrespondenciaClick(Sender: TObject);
var
    f: TFormABMTiposCorrespondencia;
begin
    if FindFormOrCreate(TFormABMTiposCorrespondencia, f) then
        f.Show
    else begin
        if not f.Inicializar then f.Release;
    end;
end;                 
{********************************************
*   END     :   CAC-CLI-066_MGO_20160218    *
*********************************************}

{-----------------------------------------------------------------------------
Function Name   :   mnu_PagoCtaMovilBcoChileClick
Author          :   Alejandro Labra
Date Created    :   05-04-2011
Description     :   Recepcion de Rendiciones de Mis Cuentas
Parameters      :   Sender: TObject
Firma           :   SS 961 05-04-2011 ALABRA
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_PagoCtaMovilBcoChileClick(Sender: TObject);
begin
    //RecepcionRendicionesMisCuentas(mnu_PagoCtaMovilBcoChile.Caption);															//SS_1378_MCA_20150915
	RecepcionRendicionesMisCuentas(mnu_PagoCtaMovilBcoChile.Caption, RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_MISCUENTAS);			//SS_1378_MCA_20150915
end;

{-----------------------------------------------------------------------------
Function Name   :   RecepcionRendicionesMisCuentas
Author          :   Alejandro Labra
Date Created    :   05-04-2011
Description     :   Recepcion de Rendiciones de Mis Cuentas
Parameters      :   NombreMenu:String, el cual es el nombre del men� que nos
                    servir� para inicializar TfRecepcionRendicionesMisCuentas.
Firma           :   SS 961 05-04-2011 ALABRA
-----------------------------------------------------------------------------}
//procedure TMainForm.RecepcionRendicionesMisCuentas(NombreMenu : String);			//SS_1378_MCA_20150915
procedure TMainForm.RecepcionRendicionesMisCuentas(NombreMenu : String; CodigoModulo: Integer);			//SS_1378_MCA_20150915
var
    f: TfRecepcionRendicionesMisCuentas;
begin
	if FindFormOrCreate(TfRecepcionRendicionesMisCuentas, f) then begin
		f.Show
    end	else begin
		//if not f.Inicializar(NombreMenu) then f.Release;							//SS_1378_MCA_20150915
		if not f.Inicializar(NombreMenu, CodigoModulo) then f.Release;				//SS_1378_MCA_20150915
	end;
end;

//INICIO BLOQUE SS_969_ALA_20110811
procedure TMainForm.mnu_RendicionesCMRClick(Sender: TObject);
begin
    //RecepcionRendicionesMisCuentas(mnu_RendicionesCMR.Caption);											//SS_1378_MCA_20150915
	RecepcionRendicionesMisCuentas(mnu_RendicionesCMR.Caption, RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_CMR);	//SS_1378_MCA_20150915
end;

procedure TMainForm.mnu_RendicionesMisCuentasClick(Sender: TObject);
begin
    //RecepcionRendicionesMisCuentas(mnu_RendicionesMisCuentas.Caption);												//SS_1378_MCA_20150915
	RecepcionRendicionesMisCuentas(mnu_RendicionesMisCuentas.Caption, RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_SENCILLITO);	//SS_1378_MCA_20150915
end;

procedure TMainForm.mnu_RendicionesMulticajaClick(Sender: TObject);
begin
    //RecepcionRendicionesMisCuentas(mnu_RendicionesMulticaja.Caption);													//SS_1378_MCA_20150915
	RecepcionRendicionesMisCuentas(mnu_RendicionesMulticaja.Caption, RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_MISCUENTAS);	//SS_1378_MCA_20150915
end;

procedure TMainForm.mnu_RendicionesUnimarcClick(Sender: TObject);
begin
    //RecepcionRendicionesMisCuentas(mnu_RendicionesUnimarc.Caption);												//SS_1378_MCA_20150915
	RecepcionRendicionesMisCuentas(mnu_RendicionesUnimarc.Caption, RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_UNIMARC);	//SS_1378_MCA_20150915
end;
//TERMINO BLOQUE SS_969_ALA_20110811

{INICIO: TASK_110_JMA_20170224}
procedure TMainForm.mnu_TollRatesClick(Sender: TObject);
var
    f: TfrmDynacTollRates;
begin
	if FindFormOrCreate(TfrmDynacTollRates, f, True) then begin
		f.Show
    end
	else begin
		if f.Inicializar then
            f.Show
        else
            f.Release;
	end;
end;
{TERMINO: TASK_110_JMA_20170224}

{-----------------------------------------------------------------------------
Function Name   :   mnu_TransaccionesRechazadasClick
Author          :   Claudio Quezada
Date Created    :   29-03-2012
Description     :   Interfaces --> Informes Otras Concesionarias --> TransaccionesRechazadas
Parameters      :   Sender: TObject
Firma           :   SS_1015_20120329_CQU
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_TransaccionesRechazadasClick(Sender: TObject);
var
    f: TFGenerarArchivoTransaccionesRechazadas;
begin
	if FindFormOrCreate(TFGenerarArchivoTransaccionesRechazadas, f, True) then begin
		f.Show
    end
	else begin
		if not f.Inicializar(mnu_TransaccionesRechazadas.Caption, True, 'TRA') then f.Release;
	end;
end;

procedure TMainForm.mnu_FPOCFoliosFacturacionClick(Sender: TObject);
var
  f: TGenerarArchivoFPOCFoliosFacturacion;
begin
  if FindFormOrCreate(TGenerarArchivoFPOCFoliosFacturacion, f) then begin
    f.Show
  end else begin
    if not f.Inicializar(mnu_FPOCFoliosFacturacion.Caption, True) then
      f.Release
    else
      f.Show;
  end;
end;

procedure TMainForm.mnu_FPOCFoliosPagosClick(Sender: TObject);
var
  f: TGenerarArchivoFPOCFoliosPagos;
begin
  if FindFormOrCreate(TGenerarArchivoFPOCFoliosPagos, f) then begin
    f.Show
  end else begin
    if not f.Inicializar(mnu_FPOCFoliosPagos.Caption, True) then
      f.Release
    else
      f.Show;
  end;
end;
//TERMINO BLOQUE SS_1015_HUR_20120409
//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
procedure TMainForm.DrawMenuItem(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
var
  S: String;
begin
  with ACanvas do
  begin
    S := TMenuItem(Sender).Caption;
    if Selected then
    begin
      Brush.Color := FColorMenuSel;
      Font.Color := FColorFontSel;
    end
    else
    begin
      Brush.Color := FColorMenu;
      Font.Color := FColorFont;
    end;

    if (Parent = nil) and (TMenuItem(Sender).MenuIndex = 5 ) and not Selected then
        ARect.Right := Width;

    FillRect(ARect);
    DrawText(ACanvas.Handle, PChar(S), Length(S), ARect, DT_SINGLELINE or DT_VCENTER);

    if S='-' then
    begin
      with ACanvas do
      begin
        ACanvas.MoveTo(ARect.Left,ARect.top + 4);
        ACanvas.Pen.Color := FColorFont;
        ACanvas.LineTo(ARect.Right, ARect.top + 4 );
      end;
    end;

  end;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
function TMainForm.CambiarEventoMenu(Menu: TMenu): Boolean;

	Procedure CambiarEventoItem(Nivel: Integer; Item: TmenuItem);
	Var
		i: Integer;
	begin                                                                     
    if (Nivel <> 0) then
       Item.OnDrawItem := DrawMenuItem;

		for i := 0 to Item.Count - 1 do begin
			CambiarEventoItem(Nivel + 1, Item.Items[i]);
		end;
	end;

begin
	CambiarEventoItem(0, Menu.Items);
	Result := True;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------

//BEGIN : SS_1400_NDR_20151007 --------------------------------------------------------------------
{****************************************************************************
mnuInterfazEntranteRespuestasRNVMClick
Author: Nelson Droguett Sierra
Date: 07-Octubre-2015
Description:  Men� de invocaci�n del formulario de Interfaz Entrante de respuestas a consultas rnvm
*****************************************************************************}
procedure TMainForm.mnuInterfazEntranteRespuestasRNVMClick(Sender: TObject);
{ INICIO : TASK_159_MGO_20170323
var
	f: TfRespuestasAConsultasRNVM;
begin
	if FindFormOrCreate(TfRespuestasAConsultasRNVM, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar(mnuInterfazEntranteRespuestasRNVM.Caption, True) then f.Release;
	end;
}
resourcestring
    QRY_CODIGO_MODULO = 'SELECT dbo.CONST_CODIGO_MODULO_RECEPCION_RESPUESTA_RNVM()';
var
    f: TFormEjecutarInterfaz;
    CodigoModulo: Integer;
begin
    CodigoModulo := QueryGetValueInt(DMConnections.BaseCAC, QRY_CODIGO_MODULO);

    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(CodigoModulo) then f.Release;
	end;
end;
// FIN : TASK_159_MGO_20170323


//BEGIN : SS_1406_NDR_20151022 --------------------------------------------------------------------
{****************************************************************************
mnuAsignaciondeConsultasRNVMaInfraccionesClick
Author: Nelson Droguett Sierra
Date: 22-Octubre-2015
Description:  Men� de invocaci�n del formulario de Asignacion de Consultas RNVM a Infracciones
*****************************************************************************}
procedure TMainForm.mnuAsignacionConsultasRNVMInfraccionesClick(
  Sender: TObject);
var
	f: TfrmAsignarConsultasRNVMInfraccionesForm;
begin
	if FindFormOrCreate(TfrmAsignarConsultasRNVMInfraccionesForm, f) then begin
		f.Show
	end
	else begin
		if not f.Inicializar() then f.Release;
	end;
end;
//END : SS_1406_NDR_20151022 --------------------------------------------------------------------

{****************************************************************************
Name: mnu_InterfacesEnviarNotifCasosaRespAreasClick
Author: aunanue
Date: 13/04/2017
Firma: TASK_008_AUN_20170413-CU.COBO.CRM.JOB.004
Description:  Invoca al env�o de notificaciones de casos a responsables de �reas.
*****************************************************************************}
procedure TMainForm.mnu_InterfacesEnviarNotifCasosaRespAreasClick(
  Sender: TObject);
var
    f: TFormEjecutarInterfaz;
begin
    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_MODULO_NOTIF_CASOS_RESP_AREA()')) then f.Release;
	end;
end;

{****************************************************************************
Name: mnu_InterfacesImportarCartolaBancosClick
Author: aunanue
Date: 13/04/2017
Firma: TASK_009_AUN_20170413-CU.COBO.REC.JOB.601
Description: CU.COBO.REC.JOB.601 Cargar Cartolas Bancarias.
*****************************************************************************}
procedure TMainForm.mnu_InterfacesImportarCartolaBancosClick(Sender: TObject);
var
    f: TFormEjecutarInterfaz;
begin
    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_MODULO_PROC_CARTOLAS_BANCO()')) then f.Release;
	end;
end;

{****************************************************************************
Name: mnu_InterfacesImportarCartolaBancosClick
Author: aunanue
Date: 13/04/2017
Firma: TASK_009_AUN_20170413-CU.COBO.REC.JOB.601
Description: CU.COBO.REC.JOB.601 Cargar Cartolas Bancarias.
*****************************************************************************}
procedure TMainForm.mnu_InterfacesConciliarCartBancRenClick(
  Sender: TObject);
var
    f: TFormEjecutarInterfaz;
begin
    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_MODULO_CONCI_CARTOLAS_BANCO()')) then f.Release;
	end;
end;

{****************************************************************************
Name: mnuEnviodecartasdeinhabilitacionClick
Author: aunanue
Date: 13/04/2017
Firma: TASK_011_AUN_20170418-CU.COBO.INT.660
Description: CU.COBO.INT.660 Enviar a Imprenta Cartas de inhabilitaci�n
*****************************************************************************}
procedure TMainForm.mnuEnvioDeCartasDeConveniosClick(
  Sender: TObject);
var
    f: TFormEjecutarInterfaz;
begin
    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_MODULO_ENVIO_CARTAS_A_SERV_IMPRESION()')) then f.Release;
	end;
end;

// INICIO : TASK_175_MGO_20170420
procedure TMainForm.mnu_DebitosPACGenricoClick(Sender: TObject);
var
    f: TFormEjecutarInterfaz;
begin
    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_MODULO_GENERACION_NOMINA_PAC_GRAL()')) then f.Release;
	end;
end;   

procedure TMainForm.mnu_RendicionesPACGenericoClick(Sender: TObject);   
var
    f: TFormEjecutarInterfaz;
begin
    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_MODULO_RECEPCION_RENDICIONES_PAC_GRAL()')) then f.Release;
	end;
end;       

procedure TMainForm.mnu_EnvioUniversoPACGenericoClick(Sender: TObject); 
var
    f: TFormEjecutarInterfaz;
begin 
    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_MODULO_ENVIO_UNIVERSO_PAC_GRAL()')) then f.Release;
	end;
end;

procedure TMainForm.mnu_RecepcionUniversoPACGenericoClick(Sender: TObject);
var
    f: TFormEjecutarInterfaz;
begin        
    if FindFormOrCreate(TFormEjecutarInterfaz, f) then begin
    	f.Show
    end
	else begin
		if not f.Inicializar(QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_MODULO_RECEPCION_UNIVERSO_PAC_GRAL()')) then f.Release;
	end;
end;
// FIN : TASK_175_MGO_20170420

end.


