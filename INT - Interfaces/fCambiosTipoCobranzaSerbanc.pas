{

Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

}

unit fCambiosTipoCobranzaSerbanc;

interface

uses
    // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils,
    // Parametros Generales
    ConstParametrosGenerales,
    // DB
    UtilDB, DMConnection,
    // Reporte finalizacion
    fReporteCTCSerbanc,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes,  Graphics,  Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB,     ppDBPipe, ppPrnabl, ppClass,
    ppCtrls, ppBands,  ppCache,  ppComm,   ppRelatv, ppProd,    ppReport, UtilRB;


type
  TfrmRecepcionCambioTipoCobranzaSerbanc = class(TForm)
    Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    txtOrigen: TPickEdit;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    OpenDialog: TOpenDialog;
    spActualizarObservaciones: TADOStoredProc;
    spAgregaSugerenciaCTCSerbanc: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnSalirClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure ImgAyudaClick(Sender: TObject);
    procedure txtOrigenButtonClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    FProcesando,
    FCancelar   : Boolean;
    FCodigoOPeracion,
    FCantidad   : integer;
    FFechaArchivo: TDateTime;
    FOrigen     : AnsiString;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function  ValidarArchivo(var strFile: TStringList;var Error: AnsiString): Boolean;
    function  ProcesarLinea( Linea: AnsiString; var Error: AnsiString ): boolean;
    procedure ActualizarOperacion;
    procedure HabilitarBotones;
    procedure MostrarReporte;
    procedure UpdateProgress(Msg: AnsiString);
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): Boolean;
  end;

var
  frmRecepcionCambioTipoCobranzaSerbanc: TfrmRecepcionCambioTipoCobranzaSerbanc;

implementation

{$R *.dfm}

procedure TfrmRecepcionCambioTipoCobranzaSerbanc.ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
    if FProcesando then ImgAyuda.Cursor := crDefault else ImgAyuda.Cursor := crHandPoint;
end;

function TfrmRecepcionCambioTipoCobranzaSerbanc.Inicializar(Titulo: AnsiString): Boolean;
resourcestring
    ERROR_CANNOT_INITIALIZE     = 'No se puede inicializar la interfaz.';
    MSG_NOTHING_TO_PROCESS      = 'No hay Activaciones Pendientes.';
    ERROR_CANNOT_GET_PARAMETER  = 'Error obteniendo el parametro general %s';
const
    DIR_ORIGEN_CTC_SERBANC = 'DIR_ORIGEN_CTC_SERBANC';
begin
    // Obtiene el directorio "default" donde deber�a estar el archivo
    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    // Le pongo de caption lo que trae de parametro...
    Caption :=  Titulo;
    //Centro el form
  	CenterForm(Self);
    // Cargar el parametro general de directorio de origen standard
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_ORIGEN_CTC_SERBANC, FOrigen) then begin
        MsgBoxErr(ERROR_CANNOT_INITIALIZE, Format(ERROR_CANNOT_GET_PARAMETER, [DIR_ORIGEN_CTC_SERBANC]), Caption, MB_ICONSTOP);
        Exit;
    end;
    txtOrigen.Text := GoodDir(FOrigen);
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : ndonadio
Date Created : 26/08/2005
Description :  muestra la ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionCambioTipoCobranzaSerbanc.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_ACCIONES_SERBANC  = ' ' + CRLF +
                          'El Archivo de cambios de Tipo de Cobranza es enviado' +
                          'por SERBANC  para sugerir al ESTABLECIMIENTO ' + CRLF +
                          'el cambio de Tipo de  Gestion de Cobranza ' +
                          'de la Notas de Cobro informadas' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if FProcesando then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_ACCIONES_SERBANC, Caption, MB_ICONQUESTION, IMGAYUDA);
end;


procedure TfrmRecepcionCambioTipoCobranzaSerbanc.btnCancelarClick(Sender: TObject);
begin
    FCancelar := True;
end;


procedure TfrmRecepcionCambioTipoCobranzaSerbanc.txtOrigenButtonClick(Sender: TObject);
    {******************************** Function Header ******************************
    Function Name: ObtenerFiltro
    Author : ndonadio
    Date Created : 10/08/2005
    Description :
    Parameters : None
    Return Value : AnsiString
    *******************************************************************************}
    Function ObtenerFiltro: AnsiString;
    Const
        FILE_TITLE	   = 'Archivo de Cambios de tipo de Cobranza|';
        FILE_NAME 	   = 'PRO_CTC_';
        FILE_EXTENSION = '.txt';
    begin
            Result:= FILE_TITLE + FILE_NAME + '*' + FILE_EXTENSION;
    end;

resourcestring
    MSG_ERROR = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST   = 'El archivo %s no existe';
  	MSG_ERROR_INVALID_FILENAME 		= 'El nombre del archivo %s es inv�lido';
  	MSG_FILE_ALREADY_PROCESSED 		= 'El archivo %s ya fue procesado';
var
    Filtro: string;
begin
    //obtengo el filtro a aplicar a este tipo de archivos
    Filtro := ObtenerFiltro;

    Opendialog.InitialDir := txtOrigen.Text;;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if opendialog.execute then begin
        txtOrigen.text:=UpperCase( opendialog.filename );
        // si el archivo no existe...
        if not FileExists( txtOrigen.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;
        // si el archivo ya se proceso...
    	if  VerificarArchivoProcesado( DMConnections.BaseCAC, txtOrigen.text ) then begin
    			MsgBox (Format(MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtOrigen.text)]),Caption, MB_ICONSTOP);
                Exit;
    	end;
        btnProcesar.Enabled := True;
	end;
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : ndonadio
Date Created : 26/08/2005
Description :   Sale...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionCambioTipoCobranzaSerbanc.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmRecepcionCambioTipoCobranzaSerbanc.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 26/08/2005
Description :   Habilita/Deshabilita botones, cambia el cursor, etc
                dependiendo del valor de la Constante FProcesando.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionCambioTipoCobranzaSerbanc.HabilitarBotones;
begin

    btnProcesar.Enabled := not FProcesando; // boton Procesar
    btnCancelar.Enabled := FProcesando;     // boton Cancelar
    btnSalir.Enabled := not FProcesando;    // boton Salir
    txtOrigen.Enabled := not FProcesando;   // Selector de Archivo
    Cursor := iif( FProcesando, crHourglass, crDefault); // Cursor
    FCancelar := False;
    pnlAvance.Visible := FProcesando;
    lblReferencia.Caption := '';
    lblReferencia.Visible := FProcesando;

end;

{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author : ndonadio
Date Created : 26/08/2005
Description :   Parsea el archivo y carga las propuestas en las tablas correspondientes...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionCambioTipoCobranzaSerbanc.btnProcesarClick(Sender: TObject);
resourcestring
    ERROR_CANT_OPEN_FILE        = 'No se puede abrir el archivo.';
    ERROR_FILE_HEADER_NOT_VALID = 'Los datos del encabezado del archivo no concuerdan con los registros.';
    ERROR_PROCESSING            = 'Ha ocurrido un error insalvable. El proceso es cancelado.';
    ERROR_CANT_REGISTER_OP      = 'No se puede registrar la operaci�n';
    ERROR_CANT_END_OP           = 'El proceso ha concluido, pero no se puede registrar el cierre de la operacion.';
    PROCESS_CANCELED            = 'Proceso cancelado por el usuario';
    PROCESS_ENDS_OK             = 'El proceso finaliz� con �xito';
    // for Progress Bar
    CAP_OPENING_FILE        = 'Abriendo el archivo...';
    CAP_REGISTERING_OP      = 'Registrando la operaci�n';
    CAP_PROCESSING_LINE     = 'Procesando la l�nea %d de %d';
    CAP_ENDING_OP           = 'Registrando Fin de la Operacion';
const
    OBS_LOG         = 'Finalizado OK';
    OBS_LOG_FAIL    = 'Proceso cancelado o terminado anormalmente';
var
    descError   : AnsiString;
    strCTC      : TStringList;
    i           : integer;
    FinAnormal  : boolean;
begin
    FinAnormal := False;
    strCTC := TStringList.Create;
    FProcesando := True;
    HabilitarBotones;
    try
        // Carga el archivo a un string list
        try
            strCTC.LoadFromFile(txtOrigen.Text);
        except
            on e:Exception do begin
                MsgBoxErr(ERROR_CANT_OPEN_FILE, e.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
        pbProgreso.Max := strCTC.Count + 3;
        // Registra inicio de operacion en log de interfases
        UpdateProgress(CAP_REGISTERING_OP);
        if not RegistrarOperacionEnLogInterface(DMCOnnections.BaseCAC,RO_MOD_INTERFAZ_ENTRADA_CAMBIOS_SERBANC,ExtractFileName(txtOrigen.Text),
          UsuarioSistema,'',True,False,FFechaArchivo,0,FCodigoOperacion,descError) then begin
            MsgBoxErr(ERROR_CANT_REGISTER_OP, descError, Caption, MB_ICONERROR);
            Exit;
        end;
        // Valida el archivo...
        if not ValidarArchivo(strCTC, descError) then begin
                MsgBoxErr(ERROR_CANT_OPEN_FILE, descError, Caption, MB_ICONERROR);
                Exit;
        end;
        // Parsea el string list y carga procesa de a una sugerencia...
        i := 1;
        While (i < FCantidad+1) and (not FCancelar) do begin
            UpdateProgress(Format(CAP_PROCESSING_LINE, [i, FCantidad]));
            if not ProcesarLinea( strCTC.Strings[i], descError ) then begin
                MsgBoxErr(ERROR_CANT_OPEN_FILE, descError, Caption, MB_ICONERROR);
                Exit;
            end;
            inc(i);
        end;
        if FCancelar then begin
            MsgBox(PROCESS_CANCELED, Caption, MB_ICONSTOP);
            Exit;
        end;
        UpdateProgress(CAP_ENDING_OP);
        if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOPeracion,OBS_LOG,descError) then begin
              MsgBoxErr(ERROR_CANT_END_OP, descError, Caption, MB_ICONERROR);
              Exit;
        end;
        UpdateProgress('');
        pbProgreso.Position := pbProgreso.Max;
        MsgBOx(PROCESS_ENDS_OK, Caption, MB_ICONINFORMATION);
        MostrarReporte;
    finally
        if FinAnormal then ActualizarOperacion;
        FProcesando := False;
        strCTC.Free;
        HabilitarBotones;
        txtOrigen.Text := ExtractFilePath(txtOrigen.Text);
    end;

end;

{******************************** Function Header ******************************
Function Name: ValidarArchivo
Author : ndonadio
Date Created : 26/08/2005
Description :   Valida el header del archivo
Parameters : var strFile: TStringList; var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmRecepcionCambioTipoCobranzaSerbanc.ValidarArchivo(var strFile: TStringList;
  var Error: AnsiString): Boolean;
resourcestring
    ERROR_QTTY_NOT_MATCH    = 'La cantidad de registros informada en el header ( %d ) ' + CRLF +
                                'no concuerda con la cantidad de lineas en el archivo ( %d ) ';
var
    regHeader,
    sCant,
    sFecha          : AnsiString;
    FechaHeader     : TDateTime;
    CantHeader      : integer;
begin
    Result := False;

    regHeader   := strFile.Strings[0];
    sFecha      := copy(regHeader, 1,14);
    sCant       := copy(regHeader, 15, 7 );
    // Valido la fecha
    if not FechaHoraAAAAMMDDToDateTime(DMConnections.BaseCAC, sFEcha, FechaHEader,Error ) then Exit;
    // Valido la cantidad
    try
        CantHeader := StrToInt(sCant);
    except
        on e: Exception do begin
            Error := e.Message;
            Exit;
        end;
    end;
    // Verifico la cantidad informada con la real
    if ( CantHeader <> (strFile.Count-1) ) then begin
        Error := ERROR_QTTY_NOT_MATCH;
        Exit;
    end;
    // Termino Ok!
    FCantidad   := CantHeader;
    FFechaArchivo := FechaHeader;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: ActualizarOperacion
Author : ndonadio
Date Created : 26/08/2005
Description :   Si la operacion fall� o fue cancelada trata de registrar esto en el log
                de operaciones. Si esto falla, no hace nada...
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionCambioTipoCobranzaSerbanc.ActualizarOperacion;
resourcestring
    OBS_CANCELADO   = 'Proceso Cancelado por el usuario (%s)';
    OBS_ERROR       = 'Proceso Termino Anormalmente (%s)';
var
    sFecha : AnsiString;
    Observacion: AnsiString;
begin
    try
        sFEcha := TRIM( DateToStr(NowBase(DMCOnnections.BaseCAC)));
        if FCancelar then
                Observacion := Format( OBS_CANCELADO, [sFEcha])
        else    Observacion := Format( OBS_ERROR, [sFEcha]);
        ActualizarObservacionesEnLogInterface(DMConnections.BaseCAC,
            spActualizarObservaciones,FCodigoOPeracion,Observacion,False)
    except
    end;
end;

{******************************** Function Header ******************************
Function Name: ProcesarLinea
Author : ndonadio
Date Created : 26/08/2005
Description :  Procesa una linea. Si los datos no son validos, realiza el asiento en el
                los de errores y sale.
                Si son validos, realiza los asientos necesarios en el detalle de operaciones.
                Para cada registro crea una transaccion.
                De evitar los reprocesos se encarga el SP de Registro.
                Solo sala con False si hubo un error grave que requeire abortar el proceso.
Parameters : Linea: AnsiString; var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionCambioTipoCobranzaSerbanc.ProcesarLinea(Linea: AnsiString;
  var Error: AnsiString): boolean;
resourcestring
    ERROR_INSERTING_RECORD  = 'No se puede insertar el registro para los valores: %s %d - Tipo de Cambio: %d '+CRLF+'en linea "%s" ';
    ERROR_TIPOCAMBIO_INVALIDO   = 'Comprobante %s %d : %d no es un tipo de cambio de cobranza v�lido';
    ERROR_COMPROBANTE_INVALIDO  = 'Comprobante %s %d : No es un comprobante v�lido o no est� activo en Serbanc';
    ERROR_ERROR         = 'Error producido al intentar registrar el error de validaci�n: ';
    ERROR_NUMCOMPRO     = 'El valor %s no se puede interpretar como un Numero de Comprobante v�lido. Linea: "%s"';
    ERROR_TIPOCAMBIO    = 'El valor %s no se puede interpretar como un tipo de Cambio de Cobranza v�lido para el Comp. %s %d. Linea: "%s"';
    ERROR_CONVENIO      = 'El valor %s no se corrsponde con nin�n convenio v�lido';
    ERROR_RUTCONVENIO   = 'El N�mero de Documento (RUT) %s no se corresponde con el convenio %s';
var
    NumeroConvenio,
    NumeroDocumento,    // RUT
    TipoComprobante: AnsiString;
    NumComprobante: Int64;
    TipoCambio: Integer;
    Error2: AnsiString;
begin
    Result := False;
    NumeroConvenio  := Copy(Linea,11,17);                                       //SS_1147Q_NDR_20141202[??]
    if ObtenerCodigoConcesionariaNativa=CODIGO_VS then                          //SS_1147Q_NDR_20141202[??]
    begin                                                                       //SS_1147Q_NDR_20141202[??]
      NumeroConvenio  := Copy(NumeroConvenio,6,12);                             //SS_1147Q_NDR_20141202[??]
    end;                                                                        //SS_1147Q_NDR_20141202[??]
    NumeroDocumento := Copy(Linea,1,10);
    TipoComprobante := Copy(Linea,28,2);
    try
        NumComprobante  := StrToInt(Copy(Linea,30,12));
    except
        on e:exception do begin
            Error := Format(ERROR_NUMCOMPRO, [Copy(Linea,30,12), Linea]);
            if not AgregarErrorInterfase(DMConnections.BaseCAC,FCodigoOperacion, Error) then begin
                Error := ERROR_ERROR+CRLF+e.Message;
                Exit;
            end;
            Result := True;
            Exit;
        end;
    end;
    try
        TipoCambio      := StrToInt(Copy(Linea,42,3));
    except
        on e:exception do begin
            Error := Format(ERROR_TIPOCAMBIO, [Copy(Linea,42,3), TipoComprobante ,NumComprobante, Linea]);
            if not AgregarErrorInterfase(DMConnections.BaseCAC,FCodigoOperacion, Error) then begin
                Error := ERROR_ERROR+CRLF+e.Message;
                Exit;
            end;
            Result := True;
            Exit;
        end;
    end;

    try
        with spAgregaSugerenciaCTCSerbanc do begin
            Close;
            Parameters.ParamByName('@CodigoOperacion').Value    := FCodigoOperacion;
            Parameters.ParamByName('@NumeroConvenio').Value     := NumeroConvenio;
            Parameters.ParamByName('@NumeroRUT').Value          := NumeroDocumento;
            Parameters.ParamByName('@TipoComprobante').Value    := TipoComprobante;
            Parameters.ParamByName('@NumeroComprobante').Value  := NumComprobante;
            Parameters.ParamByName('@TipoCambioCobranza').Value := TipoCambio;
            ExecProc;
            Case Parameters.ParamByName('@RETURN_VALUE').Value of
                  0: Error := '';
                  1: Error := Format( ERROR_TIPOCAMBIO_INVALIDO, [TipoComprobante, NumComprobante, TipoCambio]);
                  2: Error := Format( ERROR_COMPROBANTE_INVALIDO, [TipoComprobante, NumComprobante]);
                  3: begin
                            Error :=  Format(ERROR_INSERTING_RECORD, [TipoComprobante, NumComprobante, TipoCambio, Linea]);
                            if not AgregarErrorInterfase(DMConnections.BaseCAC,FCodigoOperacion, Error2) then   Error := Error + CRLF+CRLF+Error2;
                            Exit;
                     end;
                  4: Error := Format(ERROR_CONVENIO, [NumeroConvenio]);
                  5: Error := Format(ERROR_RUTCONVENIO, [NumeroDocumento, NumeroConvenio]);
            end;
        end;
        if trim(Error) <> '' then begin
            if not AgregarErrorInterfase(DMConnections.BaseCAC,FCodigoOperacion, Error) then begin
                Error := ERROR_ERROR+CRLF+Error;
                Exit;
            end;
        end;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
        end;
    end;

end;

procedure TfrmRecepcionCambioTipoCobranzaSerbanc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmRecepcionCambioTipoCobranzaSerbanc.UpdateProgress(Msg: AnsiString);
begin
    lblReferencia.Caption := Msg;
    pbProgreso.StepIt;
    Application.ProcessMessages;
end;

{******************************** Function Header ******************************
Function Name: MostrarReporte
Author : ndonadio
Date Created : 26/08/2005
Description :  Lanza el reporte...
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionCambioTipoCobranzaSerbanc.MostrarReporte;
resourcestring
    ERROR_REPORT    = 'No se puede mostrar el reporte';
var
    fr: TfrmReporteCTCSerbanc;
    descError: AnsiString;
begin
    try
        Application.Createform(TfrmReporteCTCSerbanc, fr);
        if not fr.MostrarReporte(FCodigoOperacion, Caption, descError) then begin
            MsgBoxErr(ERROR_REPORT, descError, Caption, MB_ICONERROR);
            Exit;
        end else begin
            fr.Release;
        end;
    except
        on e:exception do begin
            MsgBoxErr(ERROR_REPORT, e.Message, Caption, MB_ICONERROR);
            Exit;
        end;
    end;
end;

end.
