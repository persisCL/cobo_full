{********************************** Unit Header  *********************************
File Name : fDesactivacionesSerbanc.pas
Author : mlopez
Date Created: 05/08/2005
Language : ES-AR
Description : Gestion de Cobranza a Morosos - Envio de Desactivaciones a Serbanc

    Revision 1
    mpiazza
    29/04/2009
    se cambia el commantimeout elevandolo a 2000 segundos (33 minutos)

Revision 2
Author: mbecerra
Date: 30-Abril-2009
Description:	Se agrega una DbGrid para apurar el proceso de generaci�n del archivo

Revision 2
Author: mpiazza
Date: 12/05/2009
Description:	ss-803 se introduce un combo para agregar empresas

Revision 2
  Author: mpiazza
  Date: 04/06/2009
  Description:	ss-803 Se agrega un parametro para el filtro del combo de empresa


Revision 5
Author: mpiazza
Date: 22/06/2009
Description:	ss-803 se cambia a la funcion CargarEmpresaRecaudacionPreJudicial
            por una que filtre directamente prejudicial

Revision : 7
    Author : vpaszkowicz
    Date : 07/07/2009
    Description : Cambio la consulta embebida que permite habilitar las componentes
    para iniciar el proceso si existen registros para procesar. Cambio los count
    por IF EXISTS ya que se demoraba mucho.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

**********************************************************************************}
unit fDesactivacionesSerbanc;

interface

uses
    //Envio de Desactivaciones
    DMConnection,
    PeaProcs,
    PeaTypes,
    ComunesInterfaces,
    Util,
    UtilProc,
    ConstParametrosGenerales,
    UtilDB,
    fReporteDesactivacionesSerbanc,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, DateUtils, ppDB,
    ppDBPipe, UtilRB, ppCtrls, ppBands, ppClass, ppPrnabl, ppCache, ppComm,
    ppRelatv, ppProd, ppReport, ppModule, raCodMod, ppParameter, Grids, DBGrids,
  VariantComboBox;

type
  TfrmDesactivacionesSerbanc = class(TForm)
    Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    lblReferencia: TLabel;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pb_Progreso: TProgressBar;
    txtArchivoDestino: TPickEdit;
    sp_ObtenerDesactivacionesSerbanc: TADOStoredProc;
    sp_EliminarDesactivacionesSerbanc: TADOStoredProc;
    spEliminarTemporalDesactivacionSerbanc: TADOStoredProc;
    spRegistrarErroresDesactivacionSerbanc: TADOStoredProc;
    dsDatos: TDataSource;
    dbgDatos: TDBGrid;
    cbEmpresasRecaudadoras: TVariantComboBox;
    Label1: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnSalirClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtArchivoDestinoButtonClick(Sender: TObject);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    FProcesando: Boolean;
    FSobreescribir: Boolean;
    FFechaProceso: TDateTime;
    FArchivoDestino: AnsiString;
    FDirectorioDestino: AnsiString;
    FCodigoOperacion: Integer;
  	FCancelar: Boolean;
    FNombreArchivoNK : string;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function  GenerarLog(var Error: AnsiString): Boolean;
    function  VaciarTabla(CodigoEmpresasRecaudadoras: integer;var Error: AnsiString): Boolean;
    function  ActualizarLog(var Error: AnsiString; Observaciones : String): Boolean;
    function  GenerarArchivoDesactivaciones(var Error: AnsiString; CodigoEmpresasRecaudadoras : integer): Boolean;
    function  CargarRegistros(Destino: TStringList; CodigoEmpresasRecaudadoras : integer ; var Error: AnsiString): Boolean;
    function  ObtenerNombreArchivo(var Destino: AnsiString): Boolean;
    procedure DropearTemporal;
    procedure HabilitarBotones;
    procedure ResetProgreso(Max: Integer);
    Procedure MostrarReporteFinalizacion( CodigoOperacion : integer);
    function  DevuelveNombreArchivo(MascaraArchivo: String; CodigoEmpresa: Integer): String;
    function ObtenerDiferenciasActivacionDesactivacion(CodigoEmpresasRecaudadoras:Integer):integer;
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): Boolean;
  end;

var
  frmDesactivacionesSerbanc: TfrmDesactivacionesSerbanc;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
Function Name: Inicializar
Author : mlopez
Date Created : 05/08/2005
Description :   Valida que la tabla de origen de datos tenga algo para procesar
                e intenta obtener el destino de los archivos.
Parameters : None
Return Value : boolean

Revision 1
Author: mpiazza
Date: 12/05/2009
Description:	ss-803 se introduce un combo para agregar empresas

Revision 2
Author: mpiazza
Date: 04/06/2009
Description:	ss-803 Se agrega un parametro para el filtro del combo de empresa

Revision 3
Author: mpiazza
Date: 22/06/2009
Description:	ss-803 se cambia a la funcion CargarEmpresaRecaudacionPreJudicial
            por una que filtre directamente prejudicial

-----------------------------------------------------------------------------}
function TfrmDesactivacionesSerbanc.Inicializar(Titulo: AnsiString): Boolean;
resourcestring
    MSG_ERROR_CANNOT_INITIALIZE     = 'No se puede inicializar la interfaz.';
    MSG_ERROR_CANNOT_GET_PARAMETER  = 'Error obteniendo el parametro general %s';
    MSG_NOTHING_TO_PROCESS          = 'No hay Desactivaciones Pendientes.';
const
    DIR_DEST_DESACTIVACIONES_SERBANC = 'DIR_DEST_DESACTIVACIONES_SERBANC' ;
var
    Archivo: AnsiString;
begin
    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    FSobreescribir := False;
    // Le pongo de caption lo que trae de parametro...
    Caption :=  Titulo;
    //Centro el form
  	CenterForm(Self);
    // Cargar el parametro general de directorio de destino
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DEST_DESACTIVACIONES_SERBANC, FDirectorioDestino) then begin
        MsgBoxErr(MSG_ERROR_CANNOT_INITIALIZE, Format(MSG_ERROR_CANNOT_GET_PARAMETER, [DIR_DEST_DESACTIVACIONES_SERBANC]), Caption, MB_ICONSTOP);
        Exit;
    end else begin
        Archivo := txtArchivoDestino.Text;
        if not ObtenerNombreArchivo(Archivo) then begin
            Close;
            Exit;
        end;
        txtArchivoDestino.Text := Archivo;
        btnProcesar.Enabled := DirectoryExists(FDirectorioDestino);
    end;
    // Verificar que la tabla de pendientes tenga datos...
    if (QueryGetValueInt(DMConnections.BaseCAC, 'IF EXISTS(SELECT 1 FROM DesactivacionesSerbancPendientes (NOLOCK)) SELECT 1 ELSE SELECT 0')= 0 ) and (QueryGetValueInt(DMConnections.BaseCAC,'IF EXISTS(SELECT 1 FROM Comprobantes C (NOLOCK) '+ ' INNER JOIN ComprobantesEnviadosSerbanc S (NOLOCK) ON S.TipoComprobante = C.TipoComprobante AND s.NumeroComprobante = C.NumeroComprobante '+ ' WHERE EstadoPago = ''P'' AND EstadoCobranza = ''A'') SELECT 1 ELSE SELECT 0')= 0 ) then begin
        MsgBox(MSG_NOTHING_TO_PROCESS, Caption, MB_ICONINFORMATION);
        Exit;
    end;
    // Revisi�n 1/2 (SS 803)
    CargarEmpresaRecaudacionPreJudicial( DMConnections.BaseCAC,cbEmpresasRecaudadoras,True,0);
    //cbEmpresasRecaudadoras.Items[0].Caption := 'Todas las Empresas';
    cbEmpresasRecaudadoras.ItemIndex := 0;

    Result := True;
end;

{-----------------------------------------------------------------------------
Function Name: ImgAyudaClick
Author : mlopez
Date Created : 05/08/2005
Description :   Muestra un hint con info acerca de la interfaz.
Parameters : Sender: TObject
Return Value : None
-----------------------------------------------------------------------------}
procedure TfrmDesactivacionesSerbanc.ImgAyudaClick(Sender: TObject);
resourcestring
    MSG_ACTIVACIONES_SERBANC  = ' ' + CRLF +
      'El Archivo de Desactivaciones es' + CRLF +
      'utilizado por el ESTABLECIMIENTO para informar a SERBANC' + CRLF +
      'las Notas de Cobro para las cuales ya no debe tratar la' + CRLF +
      'Gesti�n de Cobranza. ' + CRLF +
      ' ' + CRLF +
      'Nombre del Archivo de Desactivaciones: %s' + CRLF + ' ';
var
    Archivo: AnsiString;
begin
    if FProcesando then Exit;
    ObtenerNombreArchivo(Archivo);
    MsgBoxBalloon(Format(MSG_ACTIVACIONES_SERBANC,[ExtractFileNAme(Archivo)]), Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
Function Name: ImgAyudaMouseMove
Author : mlopez
Date Created : 05/08/2005
Description :   habilita la ayuda si no esta procesando
Parameters : Sender: TObject
Return Value : None
-----------------------------------------------------------------------------}
procedure TfrmDesactivacionesSerbanc.ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
Function Name: txtDestinoArchivoButtonClick
Author : mlopez
Date Created : 05/08/2005
Description :  Permite seleccionar un directorio de destino para los archivos.
Parameters : Sender: TObject
Return Value : None
-----------------------------------------------------------------------------}
procedure TfrmDesactivacionesSerbanc.txtArchivoDestinoButtonClick(Sender: TObject);
resourcestring
    MSG_SELECT_LOCATION = 'Seleccione ubicaci�n donde se generar�n los archvos.';
var
    Location: String;
    Archivo: AnsiString;
begin
    Archivo := '';
    Location := Trim(BrowseForFolder(MSG_SELECT_LOCATION));
    if Location = '' then Exit;
    Archivo := Location;
    FDirectorioDestino := GoodDir(Location);
    while not ObtenerNombreArchivo(Archivo) do begin
        Archivo := '';
        Location := Trim(BrowseForFolder(MSG_SELECT_LOCATION));
        if Location = '' then Exit;
        Archivo := Location;
        FDirectorioDestino := GoodDir(Location);
    end;;
    txtArchivoDestino.Text  := Archivo;
    btnProcesar.Enabled := DirectoryExists(Location);
end;

{******************************** Function Header ******************************
Function Name: ResetProgreso
Author : ndonadio
Date Created : 17/08/2005
Description :   Inicializa la barra de progreso...
Parameters : Max: Integer
Return Value : None
*******************************************************************************}
procedure TfrmDesactivacionesSerbanc.ResetProgreso(Max: Integer);
begin
    pb_Progreso.Min := 0;
    pb_progreso.Max := Max;
    pb_Progreso.Position := 0;
end;

{******************************** Function Header ******************************
Function Name: CargarRegistros
Author : ndonadio
Date Created : 17/08/2005
Description :  Carga los registros en el string list
Parameters : Destino: TStringList
Return Value : Boolean
*******************************************************************************
  Revision 1
  lgisuk
  16/10/07
  Ahora se registra en el log que el usuario cancelo el proceso

  Revision 2
  Author: mpiazza
  Date: 12/05/2009
  Description:	ss-803 se introduce un combo para agregar empresas

******************************************************************************}
function TfrmDesactivacionesSerbanc.CargarRegistros(Destino: TStringList; CodigoEmpresasRecaudadoras : integer; var Error: AnsiString): Boolean;

    {******************************** Function Header ******************************
    Function Name: ObtenerRegistroControl
    Author : ndonadio
    Date Created :
    Description :  permite obtener el registro de control
    Parameters :
    Return Value : Boolean
  *******************************************************************************
    Revision 1
    lgisuk
    29/01/2008
    Se cambia el tipo de dato de la variable TotalCobranza de Integer a Int64
    porque si la funcion recibe un valor mayor a int, lo convierte mal
    incluyendo un registro de control incorrecto.
  *******************************************************************************
    Revision 2
    mpiazza
    29/04/2009
    se cambia el commantimeout elevandolo a 2000 segundos (33 minutos)
  *******************************************************************************
    Revision 3
    Author: vpaszkowicz
    Date: 29/05/2009
    Description: Homologo el header con el ancho de columna de activaciones, o sea
    7 caracteres para el total de registros.
  *******************************************************************************}
    function ObtenerRegistroControl(TotalRegistros: Integer; TotalCobranza: Int64): String;
    begin
        Result := '';
        Result := Result + FormatDateTime('YYYYMMDDHHNNSS', NowBase(DMConnections.BaseCAC));
        //Result := Result + PadL(IntToStr(TotalRegistros), 4, '0');
        Result := Result + PadL(IntToStr(TotalRegistros), 7, '0');
        Result := Result + PadL(IntToStr(TotalCobranza), 18, '0');
    end;

    //permite obtener el registro de detalle
    function ObtenerRegistroDetalle(TipoDeudor, RUT, CodigoCliente, NumeroDocumento,Monto, TipoDocumento, TipoDesactivacion: String; FechaDesactivacion: TDateTime): String;
    begin
        Result := '';
        Result := Result + TipoDeudor;
        Result := Result + PadL(Trim(RUT), 10, '0');
        Result := Result + PadL(Trim(CodigoCliente), 17, '0');
        Result := Result + PadL(Trim(NumeroDocumento), 12, '0');
        Result := Result + PadL(Trim(Monto), 16, '0');
        Result := Result + TipoDocumento;
        Result := Result + TipoDesactivacion;
        Result := Result + FormatDateTime('YYYYMMDD', FechaDesactivacion);
    end;

resourcestring
    MSG_CANCEL = 'Proceso cancelado por el usuario';
var
    RegistroControl: String;
    RegistroDetalle: String;
    TotCob: Int64;
begin
    Result := False;
    with sp_ObtenerDesactivacionesSerbanc do begin

         try
            CommandTimeOut := 2000;
            lblReferencia.Caption := 'Obteniendo datos del stored...';
            lblReferencia.Refresh;
            close;
            parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value := CodigoEmpresasRecaudadoras;
            Open;
            lblReferencia.Caption := 'Generando datos en memoria...';                                                                   
            lblReferencia.Refresh;                                                                                                      
            dbgDatos.DataSource.DataSet.DisableControls;                                                                                
            ResetProgreso(RecordCount);                                                                                                 
            First;                                                                                                                      
            TotCob := 0;                                                                                                                
           // while (not Eof) and (not FCancelar) do begin                                                                              
            while (not dbgDatos.DataSource.DataSet.Eof) and (not FCancelar) do begin                                                    
                                                                                                                                        
            {    RegistroDetalle := ObtenerRegistroDetalle(FieldByName('CodigoTipoDeudor').AsString,                                    
                                                          FieldByName('NumeroDocumento').AsString,                                      
                                                          FieldByName('NumeroConvenio').AsString,                                       
                                                          FieldByName('NumeroComprobante').AsString,                                    
                                                          FieldByName('TotalAPagar').AsString,                                          
                                                          FieldByName('TipoComprobante').AsString,                                      
                                                          FieldByName('CodigoMotivoDesactivacion').AsString,                            
                                                          FieldByName('FechaDesactivacion').AsDateTime);                                
            }                                                                                                                           
             RegistroDetalle := ObtenerRegistroDetalle(dbgDatos.DataSource.DataSet.FieldByName('CodigoTipoDeudor').AsString,            
                                                          dbgDatos.DataSource.DataSet.FieldByName('NumeroDocumento').AsString,          
                                                          dbgDatos.DataSource.DataSet.FieldByName('NumeroConvenio').AsString,           
                                                          dbgDatos.DataSource.DataSet.FieldByName('NumeroComprobante').AsString,        
                                                          dbgDatos.DataSource.DataSet.FieldByName('TotalAPagar').AsString,              
                                                          dbgDatos.DataSource.DataSet.FieldByName('TipoComprobante').AsString,          
                                                          dbgDatos.DataSource.DataSet.FieldByName('CodigoMotivoDesactivacion').AsString,
                                                          dbgDatos.DataSource.DataSet.FieldByName('FechaDesactivacion').AsDateTime);    
                Destino.Add(RegistroDetalle);
                totCob := totCob + dbgDatos.DataSource.DataSet.FieldByName('TotalAPagar').AsInteger;
                dbgDatos.DataSource.DataSet.Next;                                                                                       
                //pb_Progreso.Position := (RecNo * 100) div Parameters.ParamByName('@TotalRegistros').Value                             
                pb_Progreso.StepIt;                                                                                                     
                Application.ProcessMessages;                                                                                            
            end;                                                                                                                        
            RegistroControl := ObtenerRegistroControl(Destino.Count, totCob);                                                           
            Destino.Insert(0,RegistroControl);                                                                                          
                                                                                                                                        
            dbgDatos.DataSource.DataSet.EnableControls;                                                                                 
            // Close; // sp...no se cierra porque se usa luego para registrar en el log...                                              
            if FCancelar then begin                                                                                                     
                                                                                                                                        
                //registro que el proceso fue cancelado                                                                                 
                //RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacion);                                                
                                                                                                                                        
                                                                                                                                        
                MsgBox(MSG_CANCEL, caption, MB_ICONSTOP);                                                                               
                FProcesando := False;                                                                                                   
                Exit;                                                                                                                   
                //Self.Close;                                                                                                           
            end;                                                                                                                        
            Result := True;                                                                                                             
        except                                                                                                                          
            on e: Exception do begin                                                                                                    
                Error :=  e.Message;                                                                                                    
            end;                                                                                                                        
        end;                                                                                                                            
    end;                                                                                                                                

end;

{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 17/08/2005
Description :  Habilita, y dehabilita / Muestra u Oculta botones, panels,
                cursores, etc. segun el valor de la var. global FProcesando;
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmDesactivacionesSerbanc.HabilitarBotones;
begin
    // el panel de avance...
    pnlAvance.Visible := FProcesando;
    // los botones
    btnProcesar.Enabled := not FProcesando;
    btnCancelar.Enabled := FProcesando;
    btnSalir.Enabled := not FProcesando;
    // el cursor...
    Cursor := iif(FProcesando, crHourGlass, crDefault);
end;


{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author: mlopez
  Date Created: 08/08/2005
  Description:
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmDesactivacionesSerbanc.GenerarLog(var Error: AnsiString): Boolean;
begin
    Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_INTERFAZ_SALIENTE_DESACTIVACION_SERBANC, ExtractFileName(FNombreArchivoNK), UsuarioSistema, '', False, False, FFechaProceso, 0, FCodigoOperacion, Error);
end;

{-----------------------------------------------------------------------------
Function Name: ObtenerDiferenciasActivacionDesactivacion
Author : mpiazza
Date Created : 16/05/2009
Description :   valida contra la tabla DetalleactivacionesEnviadasSerbanc y
            DesactivacionesSerbancPendientes las diferencias informadas en los
            codigo de empresa
Parameters : None
Return Value : integer
-----------------------------------------------------------------------------}
function TfrmDesactivacionesSerbanc.ObtenerDiferenciasActivacionDesactivacion(CodigoEmpresasRecaudadoras:Integer): integer;
resourcestring
    MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
begin
  Result := 0;
    try
        Result := QueryGetValueInt(DMconnections.BaseCAC, format('SELECT dbo.ObtenerDiferenciasActivacionDesactivacionXEmpRecaudadora(%d)', [CodigoEmpresasRecaudadoras]));
    except
       on e: Exception do begin
           MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
           Result := 0;
       end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name: ObtenerNombreArchivo
Author : mlopez
Date Created : 08/08/2005
Description :  Se encarga de generar el Nombre del Archivo en el que se guardaran
               los datos de salida de la Interfaz
Parameters : None
Return Value : string
-----------------------------------------------------------------------------}
function TfrmDesactivacionesSerbanc.ObtenerNombreArchivo(var Destino: AnsiString):boolean;
resourcestring
    MSG_WARNING_FILE_ALREADY_EXISTS = 'IMPORTANTE: '+CRLF+
        ' '+CRLF+
        'El archivo ya existe en el directorio seleccionado.'+CRLF+
        'Si continua se sobreescribir� el archivo existente.'+CRLF+
        'Seleccione otro directorio de destino si no desea sobreescribirlo.';
const
    FILENAME_PREFIX = 'DTS_COB_%s_';
    FILENAME_EXT = '.txt';
    FILENAME_DATE_FORMAT = 'YYYYMMDD';
var
    Fecha: AnsiString;
begin
    FFechaProceso := NowBase(DMConnections.BaseCAC );
    Fecha := FormatDateTime(FILENAME_DATE_FORMAT, FFechaProceso);
    Destino := GoodDir(FDirectorioDestino) +FILENAME_PREFIX + Fecha + FILENAME_EXT;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: GenerarArchivoDesactivaciones
Author : lgisuk
Date Created : 17/02/2006
Description :   Genera el archivo de desactivaciones
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None

Revision 1
Author: mpiazza
Date: 12/05/2009
Description:	ss-803 se introduce un combo para agregar empresas
*******************************************************************************}
function TfrmDesactivacionesSerbanc.GenerarArchivoDesactivaciones(var Error: AnsiString; CodigoEmpresasRecaudadoras : integer): Boolean;
resourcestring
    MSG_CAPTION = 'Desactivaciones Serbanc';
    MSG_GENERANDO_ARCHIVO = 'Generando archivo para exportar...';
    MSG_NO_EXISTEN_REGISTROS = 'No existen registros en la tabla DesactivacionesSerbancPendientes para la generaci�n del archivo';
    MSG_FINALIZACION_EXITOSA = 'El archivo "%s" se ha generado exitosamente';
    MSG_ERROR_OBTENIENDO_DATOS = 'Ha ocurrido un error al obtener los datos desde la tabla DesactivacionesSerbancPendientes.'#10#13 +
                                 'Verifique la consistencia de los mismos.';
var
    ListaDeshabilitaciones: TStringList;
begin
    {Nota: Es importante tener en cuenta que el form no se crea en caso de no tener Desactivaciones pendientes}
    Result := False;
    ListaDeshabilitaciones := TStringList.Create;
    try
        lblReferencia.Caption := MSG_GENERANDO_ARCHIVO;
        if not CargarRegistros(ListaDeshabilitaciones, CodigoEmpresasRecaudadoras, Error) then Exit;

        try
            lblReferencia.Caption := 'Guardando datos a disco';
            lblReferencia.Refresh;
            ListaDeshabilitaciones.SaveToFile(FNombreArchivoNK);
        except
            on E : Exception do begin
                Error := e.Message;
                Exit;
            end;
        end;
        Result := True;
    finally
        ListaDeshabilitaciones.Free;
        lblReferencia.Caption := '';
    end;
end;


{******************************** Function Header ******************************
Function Name: MostrarReporteFinalizacion
Author : ndonadio
Date Created : 18/08/2005
Description : Lanza el reporte de finalizacion
Parameters : None
Return Value : None

  Revision 2
  mpiazza
  15/05/2009
  ss-803 Se modifico la operatoria para que acepten multiples codigos de operacion
*******************************************************************************}
procedure TfrmDesactivacionesSerbanc.MostrarReporteFinalizacion( CodigoOperacion : integer);
resourcestring
    MSG_ERROR_REPORT    = 'No se puede mostrar el reporte';
var
    fr: TfrmReporteDesactivacionesSerbanc;
    descError: AnsiString;
begin
    try
        Application.Createform(TfrmReporteDesactivacionesSerbanc, fr);
        if not fr.MostrarReporte( CodigoOperacion, Caption, descError) then begin
            MsgBoxErr(MSG_ERROR_REPORT, descError, Caption, MB_ICONERROR);
            Exit;
        end else begin
            fr.Release;
        end;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_REPORT, e.Message, Caption, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: DevuelveNombreArchivo
Author : ndonadio
Date Created : 12/05/2009
Description :
Parameters :
Return Value : None
*******************************************************************************}
function TfrmDesactivacionesSerbanc.DevuelveNombreArchivo(
  MascaraArchivo: String; CodigoEmpresa: Integer): String;
begin
   Result :=
       Format(MascaraArchivo, [StringOfChar('0', 2 - Length(IntToStr(CodigoEmpresa))) + IntToStr(CodigoEmpresa)]);
end;
{******************************** Function Header ******************************
Function Name: DropearTemporal
Author : lgisuk
Date Created : 17/02/2006
Description :   Elimino la tabla temporal
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmDesactivacionesSerbanc.DropearTemporal;
resourcestring
    COULDNT_DROP_TEMP_TABLE     =   'No se pudo remover una de las tablas '+ CRLF +
                                    'temporales usadas para el proceso. '+ CRLF +
                                    'Por favor, salga de la aplicacion para que se remueva.';
begin
    try
        spEliminarTemporalDesactivacionSerbanc.CommandTimeout := 500;
        spEliminarTemporalDesactivacionSerbanc.ExecProc;
    except
        MsgBox(COULDNT_DROP_TEMP_TABLE,caption, MB_ICONWARNING);
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarLog
  Author: mlopez
  Date Created: 08/08/2005
  Description: Actualizo el log al finalizar
  Parameters:
  Return Value: Boolean

  Revision 1
  mpiazza
  18/05/2009
  ss-803 se agrego el parametro Observaciones
-----------------------------------------------------------------------------}
function TfrmDesactivacionesSerbanc.ActualizarLog(var Error: AnsiString; Observaciones : String): Boolean;
const
     STR_OBSERVACION_OK = 'Finaliz� OK! ';
     STR_OBSERVACION_CANCEL = 'Cancelado por usuario';
begin
    Observaciones := STR_OBSERVACION_OK + Observaciones ;
    // cierro el log de operaciones
    Result := ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacion, iif(FCancelar, STR_OBSERVACION_CANCEL, Observaciones) , Error);
end;


{******************************** Function Header ******************************
Function Name: VaciarTabla
Author : ndonadio 
Date Created : 17/08/2005
Description :  Vacia la tabla de desactivaciones pendientes
Parameters : var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmDesactivacionesSerbanc.VaciarTabla(CodigoEmpresasRecaudadoras: integer; var Error: AnsiString): Boolean;
var
    Cantidad    : integer;
begin
  Result := False;
  try
    // registro los errores de interfase
    spRegistrarErroresDesactivacionSerbanc.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
    spRegistrarErroresDesactivacionSerbanc.Parameters.ParamByName('@CodigoEmpresaRecaudadora').Value := CodigoEmpresasRecaudadoras;
    spRegistrarErroresDesactivacionSerbanc.CommandTimeout := 500;
    spRegistrarErroresDesactivacionSerbanc.ExecProc;
    // Elimino las desactivaciones actualizando todo...
    with sp_EliminarDesactivacionesSerbanc do begin
        Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
        //Parameters.ParamByName('@Cantidad').Value := NULL;
        Cantidad := -1;
        While Cantidad <> 0 do begin
            CommandTimeOut := 500;
            ExecProc;
            Cantidad := Parameters.ParamByName('@Cantidad').Value;
            Result := Parameters.ParamByName('@RETURN_VALUE').Value = 0;
            if not Result then begin
                    Error := 'Error Nro. ' + IntToStr(Parameters.ParamByName('@RETURN_VALUE').Value);
                    Break;
            end;
        end;
    end;
  except
      on e : exception do begin
            Error := e.Message;
      end;
  end;
end;

{-----------------------------------------------------------------------------
  Procedure: btnProcesarClick
  Author:    ndonadio
  Date:      05-Ago-2005
  Description : Se agregaron controles y validacioens. Los procedimientos que
                eran llamados ahora son funciones que devuelven un mensaje de error
                si fallan.
  Result:    None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  06/11/2007
  Ahora se registra en el log las excepciones y se quita la transaccion.

  Revision 2
  mpiazza
  15/05/2009
  ss-803 Se modifico la operatoria para que acepten multiples empresas
-----------------------------------------------------------------------------}
procedure TfrmDesactivacionesSerbanc.btnProcesarClick(Sender: TObject);
resourcestring
    MSG_ERROR_START_LOG             = 'Error al intentar registrar en el Log de Operaciones';
    MSG_ERROR_END_LOG               = 'Error al intentar cerrar la operaci�n.';
    MSG_ERROR_PROC                  = 'No se pueden procesar las desactivacioens.';
    MSG_ERROR_UPDATING              = 'No se pueden actualizar los comprobantes enviados.';
    MSG_FINALIZACION_EXITOSA        = 'El proceso a finalizado exitosamente';
    MSG_WARNING_FILE_ALREADY_EXISTS = 'IMPORTANTE: '+CRLF+' '+CRLF+ 'Los archivos ya existen en el directorio seleccionado.'+CRLF+ 'Si continua se sobreescribir�n. '+CRLF+ 'Seleccione otro directorio de destino si no desea sobreescribirlos.'; // por si existen los archivos
    MSG_WARNING_DIFERENCIAS_EMPRESAS = 'IMPORTANTE: '+CRLF+' '+CRLF+ 'Las empresas informadas para desactivaci�n difieren de las enviadas a activar.'+CRLF+ '�Procede de todas formas?.'; // por si existen los archivos
    MSG_MENSAJE_DIFERENCIAS_EMPRESAS = 'Existen %S discrepancias en las empresas informadas, proceso aceptado por el usuario.'; // por si existen los archivos
    MSG_SELECCIONE_EMPRESA          = 'Selecione una Empresa para lanzar el Proceso.';
var
    descError                   : AnsiString;
    TemporalCreada              : Boolean;
    IndiceEmpresa               : integer;
    IndiceEmpresaInicio         : integer;
    IndiceEmpresaFin            : integer;
    CodigoEmpresasRecaudadoras  : integer;
    ListaCodigoOperacion        : tstringlist;
    Indice                      : integer;
    Diferencias                 : integer;
    Observaciones               : string;
function ExistenArchivos: Boolean;
   Var
       Indice: Integer;
begin
   Result := False;
   case cbEmpresasRecaudadoras.ItemIndex of
       0: begin
           for Indice := 1 to cbEmpresasRecaudadoras.Items.Count - 1 do
               Result :=
                   Result or FileExists(DevuelveNombreArchivo(FArchivoDestino,cbEmpresasRecaudadoras.Items[Indice].Value));
       end;
   else
       Result := FileExists(DevuelveNombreArchivo(FNombreArchivoNK,cbEmpresasRecaudadoras.Items[cbEmpresasRecaudadoras.ItemIndex].Value));
  end;
end;
begin
    // Si la tabla existe la Dropeo...
    DropearTemporal;
    Observaciones := '';

    Diferencias := ObtenerDiferenciasActivacionDesactivacion(-1);
    if Diferencias > 0 then begin
     if  (MsgBox(MSG_WARNING_DIFERENCIAS_EMPRESAS, Caption, MB_ICONWARNING+MB_YESNO) <> mrYes) then begin
        Exit;
     end else begin

     end;
    end;

    // Genero el nombre del archivo tal cual lo voy a utilizar
    if not ObtenerNombreArchivo(FArchivoDestino) then begin
        Exit;
    end;

   if ExistenArchivos then begin
     if  (MsgBox(MSG_WARNING_FILE_ALREADY_EXISTS, Caption, MB_ICONWARNING+MB_YESNO) <> mrYes) then begin
        Exit;
     end else begin
        FSobreescribir := true;
     end;
   end;

    TemporalCreada := False;

    // Inidico que empece a procesar y actualizo botones
    FProcesando := True;
    HabilitarBotones;
    IndiceEmpresaInicio := cbEmpresasRecaudadoras.ItemIndex;
    if IndiceEmpresaInicio = 0 then begin
      IndiceEmpresaInicio := 1;
      IndiceEmpresaFin := cbEmpresasRecaudadoras.Items.Count -1;
    end else begin
      IndiceEmpresaFin := IndiceEmpresaInicio;
    end;
    ListaCodigoOperacion := tstringlist.Create;
    try
        for IndiceEmpresa := IndiceEmpresaInicio to IndiceEmpresaFin do begin //ss803
          // registro en el log de interfaces
          FNombreArchivoNK := DevuelveNombreArchivo(FArchivoDestino,cbEmpresasRecaudadoras.Items[IndiceEmpresa].Value);
          CodigoEmpresasRecaudadoras := cbEmpresasRecaudadoras.Items[IndiceEmpresa].Value;

          if  not GenerarLog(descError) then begin
              MsgBoxErr(MSG_ERROR_START_LOG, descError, Caption, MB_ICONERROR);
              Exit;
          end;
          ListaCodigoOperacion.Add(inttostr(FCodigoOperacion));

          //verifico cuantas diferencias existen, ENVIO -1 POR QUE AL EXISTIR COHERENCIA SE INFORMA A TODAS LAS EMPRESAS
          //Diferencias := ObtenerDiferenciasActivacionDesactivacion(-1);
          if Diferencias = 0 then begin
              Observaciones := '';
          end else begin
              Observaciones := ' (' + Format(MSG_MENSAJE_DIFERENCIAS_EMPRESAS, [inttostr(Diferencias)])  + ')' ;
          end;

          // Genero el archivo
          if  not GenerarArchivoDesactivaciones(descError, CodigoEmpresasRecaudadoras)  then begin
              if not FCancelar then begin
                  //Registro la excepcion en el log de operaciones

                  MsgBoxErr(MSG_ERROR_PROC, descError, Caption, MB_ICONERROR);
              end else begin
                  ActualizarLog(descError, Observaciones);
              end;
              Exit;
          end;
          TemporalCreada := True;

          // Vacio la tabla de pendientes y a la vez actualizo los comprobantes enviados
          // e inserto en el detalle del log las desactivaciones enviadas
          if  not VaciarTabla(CodigoEmpresasRecaudadoras, descError)  then begin
              DeleteFile(FArchivoDestino);
              //Registro la excepcion en el log de operaciones

              MsgBoxErr(MSG_ERROR_UPDATING, descError, Caption, MB_ICONERROR);
              Exit;
          end;
          ///TemporalCreada := False;
          // Cierro el log de operaciones
          if FCancelar = False  then begin
            if  not ActualizarLog(descError, Observaciones)  then begin
                DeleteFile(FNombreArchivoNK);
                //Registro la excepcion en el log de operaciones

                MsgBoxErr(MSG_ERROR_END_LOG, descError, Caption, MB_ICONERROR);
                Exit;
            end;
          end;
          Close;
        end;
        // Termino y cierro el form...
        FProcesando := False;
        MsgBox(Format(MSG_FINALIZACION_EXITOSA, [FArchivoDestino]), Caption, MB_ICONINFORMATION);
        // muestro el reporte
        for Indice := 0 to ListaCodigoOperacion.Count - 1 do   begin
            MostrarReporteFinalizacion(strtoint(ListaCodigoOperacion.Strings[Indice]));
        end;
        TemporalCreada := false;

    finally
        if TemporalCreada then DropearTemporal;
        ListaCodigoOperacion.Free;
        FProcesando := False;
        HabilitarBotones;
    end;
end;

{******************************** Function Header ******************************
Function Name: FormKeyPress
Author : lgisuk
Date Created : 17/02/2006
Description :   Permito cancelar el proceso tocando la tecla escape
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmDesactivacionesSerbanc.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Ord(Key) = VK_ESCAPE then FCancelar := True;
end;

{******************************** Function Header ******************************
Function Name: btnCancelar
Author : lgisuk
Date Created : 17/02/2006
Description :   Permito cancelar el proceso
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmDesactivacionesSerbanc.btnCancelarClick(Sender: TObject);
begin
    FCancelar := True;
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : lgisuk
Date Created : 17/02/2006
Description :   Permito cerrar el formulario
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmDesactivacionesSerbanc.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : ndonadio
Date Created : 17/08/2005
Description :   Si est� procesando NO puede cerrarse...
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmDesactivacionesSerbanc.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : lgisuk
Date Created : 17/02/2006
Description :   Libero el formulario de memoria
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmDesactivacionesSerbanc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;


end.
