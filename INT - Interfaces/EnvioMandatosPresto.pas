{-----------------------------------------------------------------------------
 File Name: EnvioMandatosPresto.pas
 Author:    flamas
 Date Created: 28/12/2004
 Language: ES-AR
 Description: Modulo de la interfaz Presto - Envio de Mandatos

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-----------------------------------------------------------------------------}
unit EnvioMandatosPresto;

interface

uses
  //Envio Mandatos Presto
  DMConnection,
  Util,
  UtilProc,
  UtilDB,
  PeaTypes,
  ConstParametrosGenerales,
  PeaProcs,
  ComunesInterfaces,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx,  DPSControls, VariantComboBox, StrUtils,
  BuscaTab, DmiCtrls, Validate, DateEdit;

type

  TFEnvioMandatosPresto = class(TForm)
	Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    edDestino: TEdit;
    OpenDialog: TOpenDialog;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    spObtenerMandatosPresto: TADOStoredProc;
    spActualizarEnvioPresto: TADOStoredProc;
    lblFechaInterfase: TLabel;
    edFecha: TDateEdit;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure edFechaChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
    { Private declarations }
    FCodigoOperacion : integer;
	FDetenerImportacion: boolean;
    FMandatosTXT	: TStringList;
    FNumeroEnvio : integer;
    FFechaDesde : variant;
    FErrorMsg : string;
    FObservaciones : string;
    sFechaGlosa : string;
    FPresto_CodigodeComercio: AnsiString;
    FPresto_Directorio_Destino_Mandatos : AnsiString;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	function  RegistrarOperacion : boolean;
	function  GenerarMandatosPresto : boolean;
	function  CrearNombreArchivo(var NombreArchivo: AnsiString; dFecha : TDateTime): Boolean;
  public
	function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean; bReprocesar : boolean = False ) : Boolean;
	{ Public declarations }
  end;

var
  FEnvioMandatosPresto: TFEnvioMandatosPresto;

implementation
{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 28/12/2004
  Description: M�dulo de Inicializaci�n del Formulario
  Parameters: txtCaption: ANSIString; MDIChild:Boolean; bReprocesar : boolean = False
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFEnvioMandatosPresto.Inicializar(txtCaption: ANSIString; MDIChild:Boolean; bReprocesar : boolean = False ) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 19/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        PRESTO_CODIGODECOMERCIO            = 'Presto_CodigodeComercio';
        PRESTO_DIRECTORIO_DESTINO_MANDATOS = 'Presto_Directorio_Destino_Mandatos';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_CODIGODECOMERCIO  , FPresto_CodigodeComercio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_CODIGODECOMERCIO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FPresto_CodigodeComercio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + PRESTO_CODIGODECOMERCIO;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_DIRECTORIO_DESTINO_MANDATOS , FPresto_Directorio_Destino_Mandatos) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_DIRECTORIO_DESTINO_MANDATOS;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FPresto_Directorio_Destino_Mandatos := GoodDir(FPresto_Directorio_Destino_Mandatos);
                if  not DirectoryExists(FPresto_Directorio_Destino_Mandatos) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FPresto_Directorio_Destino_Mandatos;
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
	MSG_INVALID_DESTINATION_DIRECTORY = 'El directorio de env�os es inv�lido';
	MSG_WARRANTS_FILE_SEND = 'Env�o de mandatos';
	MSG_WARRANTS_FILE_RESEND = 'Re-env�o de mandatos';
	MSG_ERROR_FILE_ALREADY_PROCESSED = 'El archivo %s ya fue procesado';
var
    NombreArchivo: AnsiString;
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

	if not MDIChild then begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

	try
		DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and
                        VerificarParametrosGenerales;
	except
		on e: Exception do begin
			MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
			Exit;
		end;
	end;

	lblFechaInterfase.Visible := bReprocesar;
	edFecha.Visible := bReprocesar;

    FFechaDesde := QueryGetValueDateTime(DMConnections.BaseCAC,format('select dbo.UltimoEnvioInterfaz (%d)', [RO_MOD_INTERFAZ_SALIENTE_MANDATOS_PRESTO]));
    edFecha.Date := FFechaDesde;

    if not bReprocesar then begin
    	lblNombreArchivo.Top := 12;
		edDestino.Top := 27;
        FObservaciones := MSG_WARRANTS_FILE_SEND;
		btnProcesar.Enabled := DirectoryExists( FPresto_Directorio_Destino_Mandatos ); // Verifica si existe el Directorio de Pagomatico

        //Creo el nombre del archivo a enviar
        CrearNombreArchivo(NombreArchivo, Now);
        eddestino.Text:= NombreArchivo;

	end else begin
    	FObservaciones := MSG_WARRANTS_FILE_RESEND;
	end;

	btnCancelar.Enabled := False;
	pnlAvance.Visible := False;
	lblReferencia.Caption := '';

	result := True;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFEnvioMandatosPresto.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_MANDATOS        = ' ' + CRLF +
                          'El Archivo de Mandatos es' + CRLF +
                          'utilizado por el ESTABLECIMIENTO para informar a PRESTO' + CRLF +
                          'las altas, bajas y modificaciones de mandantes' + CRLF +
                          'efectuadas por el ESTABLECIMIENTO' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: MND1DDMM1D' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = True then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_MANDATOS, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TFEnvioMandatosPresto.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: CrearNombreArchivo
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: Creo el nombre del archivo a enviar
  Parameters: var NombreArchivo: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFEnvioMandatosPresto.CrearNombreArchivo(var NombreArchivo: AnsiString; dFecha : TDateTime): Boolean;
Const
    FILE_NAME = 'MND1';
    FILE_EXTENSION = '';
    FILE_DATE_FORMAT = 'ddmm';
    FILE_APPEND = '1D';
begin
    Result:= False;
    try
        //Creo el nombre del archivo a enviar
        NombreArchivo := GoodFileName( FPresto_Directorio_Destino_Mandatos + FILE_NAME + FormatDateTime ( FILE_DATE_FORMAT , Now ) + FILE_APPEND, FILE_EXTENSION );
        sFechaGlosa := FormatDateTime('yymmdd', dFecha);
        Result:= True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: edFechaChange
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: En caso de Reprocesar esta interfaz
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioMandatosPresto.edFechaChange(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: InvertirFecha
      Author:    flamas
      Date Created: 09/12/2004
      Description: Invierte el formato de la fecha para generar el archivo de d�bitos
      Parameters: sFecha : string
      Return Value: string
    -----------------------------------------------------------------------------}
    function InvertirFecha( sFecha : string ) : string;
    begin
    	result := Copy( sFecha, 7, 4 ) + Copy( sFecha, 4, 2 ) + Copy( sFecha, 1, 2);
    end;
var
    NombreArchivo:Ansistring;
begin
	if ( edFecha.Date <> nulldate ) then begin
   		btnProcesar.Enabled := True;
   		FFechaDesde := edFecha.Date;

        //Creo el nombre del archivo a enviar
        CrearNombreArchivo(NombreArchivo, edFecha.Date);
        eddestino.Text:= NombreArchivo;

    end else begin
   		btnProcesar.Enabled := False;
   		edDestino.Text := '';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 28/12/2004
  Description:
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFEnvioMandatosPresto.RegistrarOperacion : boolean;
resourcestring
	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
	DescError : string;
begin
   result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_SALIENTE_MANDATOS_PRESTO, ExtractFileName(edDestino.text), UsuarioSistema, FObservaciones, False, edFecha.Visible, NowBase(DMConnections.BaseCAC), FNumeroEnvio, FCodigoOperacion, DescError );
   if not result then
    	MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarMandatosPresto
  Author:    flamas
  Date Created: 28/12/2004
  Description: Genera el TXT con los Mandatos Pendientes
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFEnvioMandatosPresto.GenerarMandatosPresto : boolean;

     {-----------------------------------------------------------------------------
       Function Name: HeaderMandato
       Author:    flamas
       Date Created: 28/12/2004
       Description: Devuelve el Encabezado del Archivo
       Parameters: None
       Return Value: string
     -----------------------------------------------------------------------------}
     function  HeaderMandato : string;
     resourcestring
         //PRESTO_SENT_WARRANTS_DESCRIPTION = 'Mandatos enviados por Costanera Norte - '; //SS_1147_MCA_20140408 //OK  Revisado: 15/03/05
         PRESTO_SENT_WARRANTS_DESCRIPTION = 'Mandatos enviados por %s - '; 		//SS_1147_MCA_20140408
	 var
     	NombreConcesionaria: string;
     begin
     	NombreConcesionaria := QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM Concesionarias WITH (NOLOCK) WHERE CodigoConcesionaria = dbo.ObtenerConcesionariaNativa()');		//SS_1147_MCA_20140408
        result := 'HEADER ' + PADL(FPresto_CodigodeComercio, 6, '0') + ' ' + //Codigo Empresa Prestadora
                    //PRESTO_SENT_WARRANTS_DESCRIPTION + sFechaGlosa;           //SS_1147_MCA_20140408
                    Format(PRESTO_SENT_WARRANTS_DESCRIPTION, [NombreConcesionaria]) + sFechaGlosa;	//SS_1147_MCA_20140408
        result := PADR(result, 267, ' ');
     end;

      {-----------------------------------------------------------------------------
        Function Name: ArmarLineaTXT
        Author:    flamas
        Date Created: 28/12/2004
        Description: Arma una Linea del TXT y la Agrega al HashString
        Parameters: None
        Return Value: string
      -----------------------------------------------------------------------------}
      function ArmarLineaTXT : string;
      begin
          with spObtenerMandatosPresto do begin
              result := PADR( IntToStr( FNumeroEnvio ), 10, ' ' ) +
                          PADL(FPresto_CodigodeComercio, 6, '0') + //Codigo Empresa Prestadora
                          FieldByName( 'TipoOperacion' ).AsString +
                          PADR( Trim(FieldByName( 'NumeroDocumento' ).AsString), 9, ' ' ) +
                          PADR( Trim(FieldByName( 'Nombre' ).AsString), 20, ' ' ) +
                          PADR( Trim(FieldByName( 'Apellido' ).AsString), 30, ' ' ) +
                          PADR( Trim(FieldByName( 'ApellidoMaterno' ).AsString), 20, ' ' ) +
                          PADR( Trim(FieldByName( 'NumeroTarjetaCredito' ).AsString), 19, ' ' ) +
                          FormatDateTime( 'yymmdd', FieldByName( 'FechaInicio' ).AsDateTime ) +
                          iif( FieldByName( 'FechaBaja' ).IsNull, PADR(' ', 6, ' '), FormatDateTime( 'yymmdd', FieldByName( 'FechaBaja' ).AsDateTime )) +
                          'PE' +
                          PADR( Trim(FieldByName( 'NumeroConvenio' ).AsString), 17, ' ' );
                          //'000000' +                           //prueba
                          //PADR('000000: Aprobado', 94, ' ');   //prueba
              result := PADR(result, 267, ' ');
          end;
      end;

    {-----------------------------------------------------------------------------
      Function Name: FooterMandato
      Author:    flamas
      Date Created: 28/12/2004
      Description:  Devuelve el Footer correspondiente al archivo de mandatos
      Parameters: None
      Return Value: string
    -----------------------------------------------------------------------------}
    function FooterMandato : string;
    begin
        result := 'FOOTER ' + PadL( IntToStr( FMandatosTXT.Count - 1 ), 6, '0' );
        result := PADR(result, 267, ' ');
    end;

resourcestring
	MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS 	= 'No se pudieron obtener los mandatos pendientes';
	MSG_NO_PENDING_WARRANTS 				= 'No hay mandatos pendientes de env�o';
	MSG_COULD_NOT_CREATE_WARRANTS_FILE 		= 'No se pudo crear el archivo de mandatos';
	MSG_COULD_NOT_REGISTER_SENT_WARRANT		= 'No se pudo registrar el mandato enviado';
    MSG_GENERATING_WARRANTS_FILE 			= 'Generando archivo de mandatos';
    MSG_PROCESSING							= 'Procesando...';
    MSG_ERROR								= 'Error';
var
	nNroConvenio : variant;
begin
    Screen.Cursor := crHourglass;
    result := False;
	try
		// Obtiene el N�mero Consecutivo de D�bitos - Para Procesamiento o Re-Procesamiento
        FNumeroEnvio := QueryGetValueInt(DMConnections.BaseCAC,format('select dbo.ObtenerUltimoRegistroEnviadoInterfase (NULL, %d)',[RO_MOD_INTERFAZ_SALIENTE_MANDATOS_PRESTO])) + 1;

		FMandatosTXT.Add( HeaderMandato );
        lblReferencia.Caption := MSG_GENERATING_WARRANTS_FILE;
        pbProgreso.Position := 0;
        pnlAvance.Visible := True;
        lblReferencia.Caption := MSG_PROCESSING;
        pbProgreso.Max := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT Max(CodigoConvenio) FROM Convenio WITH(NOLOCK)');
        nNroConvenio := 0;
		Application.ProcessMessages;
        
        while   (nNroConvenio <> null) and
        		(FErrorMsg = '') and
                (not FDetenerImportacion) do begin
			try
                spObtenerMandatosPresto.Close;
                spObtenerMandatosPresto.CommandTimeout := 600;
    			spObtenerMandatosPresto.Parameters.ParamByName( '@FechaInterfase' ).Value := FFechaDesde;
    			spObtenerMandatosPresto.Parameters.ParamByName( '@CodigoConvenio' ).Value := nNroConvenio;
    			spObtenerMandatosPresto.Open;
            except
                on e: Exception do begin
                    MsgBoxErr(MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS, e.Message, MSG_ERROR, MB_ICONERROR);
                    FErrorMsg := MSG_COULD_NOT_OBTAIN_PENDING_WARRANTS;
                    Break;
                end;
            end;

            with spObtenerMandatosPresto do begin
                while ( not Eof ) and ( not FDetenerImportacion ) do begin
                    //Genero cada linea del txt
                    FMandatosTXT.Add( ArmarLineaTXT );
                    try
                        spActualizarEnvioPresto.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value := FCodigoOperacion;
                        spActualizarEnvioPresto.Parameters.ParamByName( '@CodigoConvenio' ).Value := FieldByName( 'CodigoConvenio' ).Value;
                        spActualizarEnvioPresto.Parameters.ParamByName( '@TipoOperacion' ).Value := FieldByName( 'TipoOperacion' ).Value;
                        spActualizarEnvioPresto.ExecProc;
                    except
                        on e: Exception do begin
                            MsgBoxErr(MSG_COULD_NOT_REGISTER_SENT_WARRANT, e.Message, 'Error', MB_ICONERROR);
                            FErrorMsg := MSG_COULD_NOT_REGISTER_SENT_WARRANT;
                        end;
                    end;
                    Application.ProcessMessages;
                    Next;
                end;

            end;

            nNroConvenio := spObtenerMandatosPresto.Parameters.ParamByName( '@CodigoConvenio' ).Value;
            if (nNroConvenio <> null) then pbProgreso.Position := nNroConvenio;
            Application.ProcessMessages;
		end;

        pbProgreso.Position := 0;
        spObtenerMandatosPresto.Close;

        if ( not FDetenerImportacion ) and ( FErrorMsg = '' ) then begin
            try
            	if FMandatosTXT.Count > 1 then begin
                	FMandatosTXT.Add( FooterMandato );
                	FMandatosTXT.SaveToFile( edDestino.Text );
                end;
                result := True;
            except
                on e: Exception do begin
                    MsgBoxErr(MSG_COULD_NOT_CREATE_WARRANTS_FILE, e.Message, 'Error', MB_ICONERROR);
                    FErrorMsg := MSG_COULD_NOT_CREATE_WARRANTS_FILE;
                end;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    flamas
  Date Created: 28/12/2004
  Description: Busca los mandatos Pendientes y Genera el env�o
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioMandatosPresto.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Actualizo el log al finalizar
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ActualizarLog:boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVACION = 'Finalizo OK!';
    var
         DescError : String;
    begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);
        except
            on e: Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

resourcestring
	MSG_PROCESS_SUCCEDED 				= 'El proceso finaliz� con �xito.' + crlf +
    										'Se procesaron %d novedades.';
	MSG_NO_PENDING_WARRANTS				= 'El proceso finaliz� con �xito.' + crlf +
    										'No se gener� el archivo por no' + crlf +
                                            'existir mandatos pendientes.';
	MSG_PROCESS_COULD_NOT_BE_COMPLETED 	= 'El proceso no se pudo completar';
	MSG_FILE_ALREADY_EXISTS 			= 'El archivo ya existe.'#10#13'� Desea continuar ?';
begin
    //Verifico si ya existe el archivo
	if FileExists( edDestino.text ) then
        //informo al operador que ya existe y le pregunto si desea continuar
		if not(MsgBox(MSG_FILE_ALREADY_EXISTS, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then Exit;

 	// Crea la listas de Debitos
    FMandatosTXT := TStringList.Create;

    // Deshabilita los botones
	btnCancelar.Enabled := True;
    btnProcesar.Enabled := False;
    FErrorMsg := '';

    FDetenerImportacion := False;

    try
        //Registra la operacion y Genera el archivo de Mandatos
        if RegistrarOperacion and GenerarMandatosPresto then begin
            if (FMandatosTXT.Count > 1) then begin
                //Actualizo el log al Final
                ActualizarLog;
                //Informo que la operacion finalizo con exito
                MsgBox(Format(MSG_PROCESS_SUCCEDED, [FMandatosTXT.Count - 2]), Caption, MB_ICONINFORMATION);
            end else begin
                //Informo no habia mandatos pendientes de envio
                MsgBox(MSG_NO_PENDING_WARRANTS, Caption, MB_ICONINFORMATION);
            end;
        end else begin
            //Informo que el proceso no se pudo completar
        	MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
        end;

    finally
        // Libero el StringList
		FreeAndNil( FMandatosTXT );
    	// Habilita los botones
		btnCancelar.Enabled := False;
    	btnProcesar.Enabled := True;
        Close;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    flamas
  Date Created: 07/12/2004
  Description: Detiene el proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioMandatosPresto.btnCancelarClick(Sender: TObject);
resourcestring
	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: permite salir si no esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioMandatosPresto.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
	CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioMandatosPresto.btnSalirClick(Sender: TObject);
begin
   Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFEnvioMandatosPresto.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	spObtenerMandatosPresto.Close; //cierro el sp
	Action := caFree;
end;




end.
