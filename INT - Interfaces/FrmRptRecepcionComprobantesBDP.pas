{-------------------------------------------------------------------------------
 File Name: FrmRptRecepcionComprobantesBDP.pas
 Author:    lgisuk
 Date Created: 14/08/2006
 Language: ES-AR
 Description: Gestion de Interfases - Reporte de finalizacion del proceso
              Informa la cantidad de comprobantes recibidos , el importe total y
              otra informacion util para verificar si el proceso se realizo
              seg�n par�metros normales.



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
unit FrmRptRecepcionComprobantesBDP;

interface

uses
  //Reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  UtilProc,                  //Mensajes
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd,
  ppReport, ppDB, ppComm, ppRelatv, ppTxPipe, DB, ppDBPipe, ADODB,
  ppParameter, ppModule, raCodMod, ppStrtch, ppSubRpt,RBSetup, daDataModule, ConstParametrosGenerales; //SS_1147_NDR_20140710

type
  TFrmRptRecepcionComprobantesBDP = class(TForm)
    ppReporte: TppReport;
    ppParameterList1: TppParameterList;
    RBIListado: TRBInterface;
    spObtenerReporteRecepcionComprobantesBDP: TADOStoredProc;
    dbplspObtenerReporteRecepcionComprobantesSantander: TppDBPipeline;
    dsspObtenerReporteRecepcionComprobantesSantander: TDataSource;
    spObtenerReportePagosRechazadosBDP: TADOStoredProc;
    dsspObtenerReportePagosRechazadosSantander: TDataSource;
    dbplspObtenerReportePagosRechazadosSantander: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppModulo: TppLabel;
    pptFechaProcesamiento: TppLabel;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    ppFechaProcesamiento: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppLine3: TppLine;
    ppLabel5: TppLabel;
    ppNumeroProceso: TppLabel;
    ppLine8: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSubReportDetalleComprobantesRechazados: TppSubReport;
    ChildReporteDetalleRechazos: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppLine9: TppLine;
    lblDetalleDeRechazos: TppLabel;
    ppLine10: TppLine;
    lblNumeroComprobante: TppLabel;
    lblMotivoRechazo: TppLabel;
    lblMonto: TppLabel;
    ppLine11: TppLine;
    lblConvenio: TppLabel;
    ppDetailBand4: TppDetailBand;
    dtxtNumeroComprobante: TppDBText;
    dtxtMotivoRechazo: TppDBText;
    dtxtMontoAMostrar: TppDBText;
    dtxtNumeroConvenio: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    raCodeModule4: TraCodeModule;
    spObtenerReporteRecepcionComprobantesBDPCodigoServicio: TIntegerField;
    spObtenerReporteRecepcionComprobantesBDPCantidadAceptados: TIntegerField;
    spObtenerReporteRecepcionComprobantesBDPMontoAceptados: TLargeIntField;
    spObtenerReporteRecepcionComprobantesBDPMontoAceptadosAMostrar: TStringField;
    spObtenerReporteRecepcionComprobantesBDPCantidadRechazados: TIntegerField;
    spObtenerReporteRecepcionComprobantesBDPMontoRechazados: TLargeIntField;
    spObtenerReporteRecepcionComprobantesBDPMontoRechazadosAMostrar: TStringField;
    spObtenerReporteRecepcionComprobantesBDPCantidadTotal: TIntegerField;
    spObtenerReporteRecepcionComprobantesBDPMontoTotal: TLargeIntField;
    spObtenerReporteRecepcionComprobantesBDPMontoTotalAMostrar: TStringField;
    spObtenerReportePagosRechazadosBDPNumeroComprobante: TLargeintField;
    spObtenerReportePagosRechazadosBDPNumeroConvenio: TStringField;
    spObtenerReportePagosRechazadosBDPCodigoServicio: TIntegerField;
    spObtenerReportePagosRechazadosBDPMonto: TLargeIntField;
    spObtenerReportePagosRechazadosBDPMontoAMostrar: TStringField;
    spObtenerReportePagosRechazadosBDPMotivoRechazo: TStringField;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel6: TppLabel;
    ppCantidadRatificados: TppLabel;
    ppCantidadAceptados: TppLabel;
    ppCantidadRechazados: TppLabel;
    ppMontoRatificados: TppLabel;
    ppMontoAceptados: TppLabel;
    ppMontoRechazados: TppLabel;
    ppSubReportDetalleAceptados: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLine2: TppLine;
    ppLabel7: TppLabel;
    ppLine4: TppLine;
    ppLabel8: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    raCodeModule1: TraCodeModule;
    ppLine1: TppLine;
    ppSubReportPagosNoRatificados: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppLine5: TppLine;
    ppLabel9: TppLabel;
    ppLine6: TppLine;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLine7: TppLine;
    ppLabel15: TppLabel;
    ppDetailBand3: TppDetailBand;
    ppDBText2: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppSummaryBand3: TppSummaryBand;
    raCodeModule2: TraCodeModule;
    SPObtenerReportePagosAceptados: TADOStoredProc;
    DSObtenerReportePagosAceptados: TDataSource;
    ppDBplObtenerReportePagosAceptados: TppDBPipeline;
    SPObtenerReportePagosNoRatificadosBDP: TADOStoredProc;
    DSObtenerPagosNoRatificadosBDP: TDataSource;
    ppDBPlObtenerReportePagosNoRatificadosBDP: TppDBPipeline;
    ppLabel16: TppLabel;
    ppLineasArchivo: TppLabel;
    ppLabel18: TppLabel;
    ppMontoArchivo: TppLabel;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FNombreReporte: String;
    // Para mantener el c�digo de operaci�n interfase para el cual generar el reporte.
    FCodigoOperacionInterfase: Integer;
    { Private declarations }
  public
    function Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase: Integer): Boolean;
    { Public declarations }
  end;

var
  FRptRecepcionComprobantesSantander: TFrmRptRecepcionComprobantesBDP;

implementation

{$R *.dfm}

const
 CONST_SIN_DATOS = 'Sin datos.';

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 14/08/2006
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
    CodigoOperacionInterfase: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrmRptRecepcionComprobantesBDP.Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase: Integer): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
      ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
      try                                                                                                 //SS_1147_NDR_20140710
        ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
      except                                                                                              //SS_1147_NDR_20140710
        On E: Exception do begin                                                                          //SS_1147_NDR_20140710
          Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
          MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
          Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
        end;                                                                                              //SS_1147_NDR_20140710
      end;                                                                                                //SS_1147_NDR_20140710

    FNombreReporte              := NombreReporte;
    FCodigoOperacionInterfase   := CodigoOperacionInterfase;
    // Ejecutar el reporte.
    RBIListado.Execute;
    Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 14/08/2006
  Description:  Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrmRptRecepcionComprobantesBDP.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
resourcestring
    MSG_ERROR_TO_GENERATE_REPORT = 'Error al generar el reporte.';
    MSG_ERROR = 'Error';
var
    Config: TRBConfig;
begin
    // Configuraci�n del reporte
    Config := rbiListado.GetConfig;
    if Config.BinName <> '' then begin
        rbiListado.Report.PrinterSetup.BinName := Config.BinName;
    end;

    try
        // Abrir la consulta de Comprobantes Pagos
        spObtenerReporteRecepcionComprobantesBDP.Close;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.Refresh;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@FechaProcesamiento').Value := NULL;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@NombreArchivo').Value := NULL;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@Modulo').Value := NULL;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@Usuario').Value := NULL;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@CantidadRatificados').Value := 0;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@CantidadAceptados').Value := 0;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@CantidadRechazados').Value := 0;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@MontoRatificados').Value := 0;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@MontoTotalAceptados').Value := 0;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@MontoTotalRechazados').Value := 0;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@LineasArchivo').Value := 0;
        spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@MontoArchivo').Value := 0;
        spObtenerReporteRecepcionComprobantesBDP.CommandTimeOut := 5000;
        spObtenerReporteRecepcionComprobantesBDP.ExecProc;

        // Asigno los valores a los campos del reporte
        ppmodulo.Caption                := spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@Modulo').Value;
        ppFechaProcesamiento.Caption    := DateToStr(spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@FechaProcesamiento').Value);
        ppNombreArchivo.Caption         := Copy(spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@NombreArchivo').Value, 1, 40);
        ppUsuario.Caption               := spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@Usuario').Value;
        ppNumeroProceso.Caption         := IntToStr(FCodigoOperacionInterfase);
        ppLineasArchivo.Caption                := iif(spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@LineasArchivo').Value = 0, CONST_SIN_DATOS, IntToStr(spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@LineasArchivo').Value));
        ppMontoArchivo.Caption          := iif(spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@MontoArchivo').Value = '0', CONST_SIN_DATOS, spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@MontoArchivo').Value);

        ppCantidadRatificados.Caption   :=  spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@CantidadRatificados').Value;
        ppCantidadAceptados.Caption     :=  spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@CantidadAceptados').Value;
        ppCantidadRechazados.Caption    :=  spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@CantidadRechazados').Value;

        ppMontoRatificados.Caption      :=  spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@MontoRatificados').Value;
        ppMontoAceptados.Caption        :=  spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@MontoTotalAceptados').Value;
        ppMontoRechazados.Caption       :=  spObtenerReporteRecepcionComprobantesBDP.Parameters.ParamByName('@MontoTotalRechazados').Value;

        // Obtener los comprobantes aceptados.
        spObtenerReportePagosAceptados.Close;
        spObtenerReportePagosAceptados.Parameters.Refresh;
        spObtenerReportePagosAceptados.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerReportePagosAceptados.CommandTimeOut := 5000;
        spObtenerReportePagosAceptados.Open;

        // Obtener los comprobantes rechazados.
        spObtenerReportePagosRechazadosBDP.Close;
        spObtenerReportePagosRechazadosBDP.Parameters.Refresh;
        spObtenerReportePagosRechazadosBDP.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerReportePagosRechazadosBDP.CommandTimeOut := 5000;
        spObtenerReportePagosRechazadosBDP.Open;

        // Obtener los comprobantes pagos no ratificados
        spObtenerReportePagosNoRatificadosBDP.Close;
        spObtenerReportePagosNoRatificadosBDP.CommandTimeOut := 5000;
        spObtenerReportePagosNoRatificadosBDP.Open;

        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FNombreReporte;

    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_TO_GENERATE_REPORT, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

end.


