{-------------------------------------------------------------------------------
 File Name: FrmRptRecepcionComprobantesMisCuentas.pas
 Author:    lgisuk
 Date Created: 21/07/2006
 Language: ES-AR
 Description: Gestion de Interfases - Reporte de finalizacion del proceso
              Informa la cantidad de comprobantes recibidos , el importe total y
              otra informacion util para verificar si el proceso se realizo
              segun parametros normales.



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Firma       : SS_1147_MCA_20150505
Descripcion : se agrega al reporte los errores de validacion.
-------------------------------------------------------------------------------}
unit FrmRptRecepcionComprobantesMisCuentas;

interface

uses
  //Reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  UtilProc,                  //Mensajes
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd,
  ppReport, ppDB, ppComm, ppRelatv, ppTxPipe, DB, ppDBPipe, ADODB,
  ppParameter, ppModule, raCodMod, ppStrtch, ppSubRpt,RBSetup, daDataModule, ConstParametrosGenerales; //SS_1147_NDR_20140710

type
  TFrmRptRecepcionComprobantesMisCuentas = class(TForm)
    ppReporte: TppReport;
    ppParameterList1: TppParameterList;
    RBIListado: TRBInterface;
    spObtenerReporteRecepcionComprobantesMisCuentas: TADOStoredProc;
    dbplspObtenerReporteRecepcionComprobantesMisCuentas: TppDBPipeline;
    dsspObtenerReporteRecepcionComprobantesMisCuentas: TDataSource;
    spObtenerReportePagosRechazadosMisCuentas: TADOStoredProc;
    dsspObtenerReportePagosRechazadosMisCuentas: TDataSource;
    dbplspObtenerReportePagosRechazadosMisCuentas: TppDBPipeline;
    spObtenerReporteRecepcionComprobantesMisCuentasCodigoServicio: TStringField;
    spObtenerReporteRecepcionComprobantesMisCuentasCantidadAceptados: TIntegerField;
    spObtenerReporteRecepcionComprobantesMisCuentasMontoAceptados: TLargeIntField;
    spObtenerReporteRecepcionComprobantesMisCuentasMontoAceptadosAMostrar: TStringField;
    spObtenerReporteRecepcionComprobantesMisCuentasCantidadRechazados: TIntegerField;
    spObtenerReporteRecepcionComprobantesMisCuentasMontoRechazados: TLargeIntField;
    spObtenerReporteRecepcionComprobantesMisCuentasMontoRechazadosAMostrar: TStringField;
    spObtenerReporteRecepcionComprobantesMisCuentasCantidadTotal: TIntegerField;
    spObtenerReporteRecepcionComprobantesMisCuentasMontoTotal: TLargeIntField;
    spObtenerReporteRecepcionComprobantesMisCuentasMontoTotalAMostrar: TStringField;
    spObtenerReportePagosRechazadosMisCuentasNumeroComprobante: TLargeintField;
    spObtenerReportePagosRechazadosMisCuentasNumeroConvenio: TStringField;
    spObtenerReportePagosRechazadosMisCuentasCodigoServicio: TStringField;
    spObtenerReportePagosRechazadosMisCuentasMonto: TLargeIntField;
    spObtenerReportePagosRechazadosMisCuentasMontoAMostrar: TStringField;
    spObtenerReportePagosRechazadosMisCuentasMotivoRechazo: TStringField;
    ppDBPipelineErrores: TppDBPipeline;                                         //SS_1147_MCA_20150505
    dsErrores: TDataSource;                                                     //SS_1147_MCA_20150505
    qryErroresInterfases: TADOQuery;                                            //SS_1147_MCA_20150505
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppModulo: TppLabel;
    pptFechaProcesamiento: TppLabel;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    ppFechaProcesamiento: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppLine3: TppLine;
    ppLabel5: TppLabel;
    ppNumeroProceso: TppLabel;
    ppLabel1: TppLabel;
    ppLineas: TppLabel;
    ppLabel3: TppLabel;
    ppMontoArchivo: TppLabel;
    ppLine8: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSubReportDetalleComprobantesRechazados: TppSubReport;
    ChildReporteDetalleRechazos: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppLine9: TppLine;
    lblDetalleDeRechazos: TppLabel;
    ppLine10: TppLine;
    lblNumeroComprobante: TppLabel;
    lblMotivoRechazo: TppLabel;
    lblMonto: TppLabel;
    ppLine11: TppLine;
    lblConvenio: TppLabel;
    lblCodigoServicio: TppLabel;
    ppDetailBand4: TppDetailBand;
    dtxtNumeroComprobante: TppDBText;
    dtxtMotivoRechazo: TppDBText;
    dtxtMontoAMostrar: TppDBText;
    dtxtNumeroConvenio: TppDBText;
    dtxtCodigoServicio: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    raCodeModule4: TraCodeModule;
    ppSubReportResumenRechazos: TppSubReport;
    ChilReportResumen: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppLine5: TppLine;
    lblResumen: TppLabel;
    ppLine6: TppLine;
    lblResumenCodigoServicio: TppLabel;
    lblResumenCantidadTotal: TppLabel;
    lblMontoTotal: TppLabel;
    ppLine7: TppLine;
    lblResumenCantidadAceptados: TppLabel;
    lblResumenMontoAceptados: TppLabel;
    lblCantidadRecahazados: TppLabel;
    lblResumenMontoRechazados: TppLabel;
    ppDetailBand3: TppDetailBand;
    dtxtResumenCodigoServicio: TppDBText;
    dtxtMontoRechazadosAMostrar: TppDBText;
    dtxtMontoTotalAMostrar: TppDBText;
    dtxtResumenCantidadAceptados: TppDBText;
    dtxtMontoAceptadosAMostrar: TppDBText;
    dtxtResumenCantidadRechazados: TppDBText;
    dtxtResumenCantidadTotal: TppDBText;
    ppSummaryBand3: TppSummaryBand;
    ppLabel9: TppLabel;
    ppLine13: TppLine;
    ppLine14: TppLine;
    dbcTotalCantidadAceptados: TppDBCalc;
    dbcTotalCantidadRechazados: TppDBCalc;
    dbcTotalCantidadTotal: TppDBCalc;
    lblTotalMontoAceptados: TppLabel;
    lblTotalMontoRechazados: TppLabel;
    lblTotalMontoTotal: TppLabel;
    raCodeModule1: TraCodeModule;
    raCodeModule2: TraCodeModule;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppDBText1: TppDBText;
    ppLine1: TppLine;
    ppLabel2: TppLabel;
    ppLine2: TppLine;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FNombreReporte: String;
    // Para mantener el c�digo de operaci�n interfase para el cual generar el reporte.
    FCodigoOperacionInterfase: Integer;
    { Private declarations }
  public
    function Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase: Integer): Boolean;
    { Public declarations }
  end;

var
  FRptRecepcionComprobantesMisCuentas: TFrmRptRecepcionComprobantesMisCuentas;

implementation

{$R *.dfm}

const
 CONST_SIN_DATOS = 'Sin datos.';

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
    CodigoOperacionInterfase: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrmRptRecepcionComprobantesMisCuentas.Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase: Integer): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
      ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
      try                                                                                                 //SS_1147_NDR_20140710
        ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
      except                                                                                              //SS_1147_NDR_20140710
        On E: Exception do begin                                                                          //SS_1147_NDR_20140710
          Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
          MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
          Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
        end;                                                                                              //SS_1147_NDR_20140710
      end;                                                                                                //SS_1147_NDR_20140710

    FNombreReporte              := NombreReporte;
    FCodigoOperacionInterfase   := CodigoOperacionInterfase;
    // Ejecutar el reporte.
    RBIListado.Execute;
    Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 21/07/2006
  Description:  Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrmRptRecepcionComprobantesMisCuentas.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
resourcestring
    MSG_ERROR_TO_GENERATE_REPORT = 'Error al generar el reporte.';
    MSG_ERROR = 'Error';
var
    Config: TRBConfig;
begin
    // Configuraci�n del reporte
    Config := rbiListado.GetConfig;
    if Config.BinName <> '' then begin
        rbiListado.Report.PrinterSetup.BinName := Config.BinName;
    end;

    try
        // Abrir la consulta de Comprobantes Pagos
        spObtenerReporteRecepcionComprobantesMisCuentas.Close;
        spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.Refresh;
        spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@FechaProcesamiento').Value := NULL;
        spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@NombreArchivo').Value := NULL;
        spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@Modulo').Value := NULL;
        spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@Usuario').Value := NULL;
        spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@MontoTotalAceptadosAMostrar').Value := Null;
        spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@MontoTotalRechazadosAMostrar').Value := Null;
        spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@MontoTotalAMostrar').Value := Null;
        spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@LineasArchivo').Value := Null;
        spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@MontoArchivo').Value := Null;

        spObtenerReporteRecepcionComprobantesMisCuentas.CommandTimeOut := 5000;
        spObtenerReporteRecepcionComprobantesMisCuentas.Open;

        // Asigno los valores a los campos del reporte
        ppmodulo.Caption                := spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@Modulo').Value;
        ppFechaProcesamiento.Caption    := DateToStr(spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@FechaProcesamiento').Value);
        ppNombreArchivo.Caption         := Copy(spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@NombreArchivo').Value, 1, 40);
        ppUsuario.Caption               := spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@Usuario').Value;
        ppNumeroProceso.Caption         := IntToStr(FCodigoOperacionInterfase);
        ppLineas.Caption                := iif(spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@LineasArchivo').Value = 0, CONST_SIN_DATOS, IntToStr(spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@LineasArchivo').Value));
        ppMontoArchivo.Caption          := iif(spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@MontoArchivo').Value = '0', CONST_SIN_DATOS, spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@MontoArchivo').Value);


        lblTotalMontoAceptados.Caption  := spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@MontoTotalAceptadosAMostrar').Value;
        lblTotalMontoRechazados.Caption := spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@MontoTotalRechazadosAMostrar').Value;
        lblTotalMontoTotal.Caption      := spObtenerReporteRecepcionComprobantesMisCuentas.Parameters.ParamByName('@MontoTotalAMostrar').Value;

        // Obtener los comprobantes rechazados.
        spObtenerReportePagosRechazadosMisCuentas.Close;
        spObtenerReportePagosRechazadosMisCuentas.Parameters.Refresh;
        spObtenerReportePagosRechazadosMisCuentas.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerReportePagosRechazadosMisCuentas.CommandTimeOut := 5000;
        spObtenerReportePagosRechazadosMisCuentas.Open;

        qryErroresInterfases.Close;                                             //SS_1147_MCA_20150505
        qryErroresInterfases.SQL.Text := 'SELECT * FROM ErroresInterfases WHERE CodigoOperacionInterfase = ' + IntToStr(FCodigoOperacionInterfase); //SS_1147_MCA_20150505
        qryErroresInterfases.Open;                                              //SS_1147_MCA_20150505

        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FNombreReporte;

    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_TO_GENERATE_REPORT, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

end.


