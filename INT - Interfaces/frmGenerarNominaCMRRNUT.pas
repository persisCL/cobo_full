unit frmGenerarNominaCMRRNUT;

{
	Author		: mcabello
    Date		: 02/05/2014
    Descripcion	: permite generar el archivo de contratos CMR RNUT

    Firma       : SS_1147_NDR_20141216
    Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

    Autor       :   CQuezadaI
    Fecha       :   22/09/2015
    Firma       :   SS_1388_CQU_20150922
    Descripcion :   Se debe agregar registro en el Log Operaciones Interfaces
                    para ello se agregan las funciones RegistrarOperacion y ActualizarLog
                    para registrar el inicio y fin, respectivamente.
}

interface

uses
	DMConnection,
    ConstParametrosGenerales,
    StrUtils,
    Util,
    UtilProc,
    DB, ADODB,
    ComunesInterfaces, SysUtilsCN,          // SS_1388_CQU_20150922
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls;

type
  TFGenerarNominaCMRRNUT = class(TForm)
    bvl1: TBevel;
    btnSalir: TButton;
    btnGenerar: TButton;
    lbl1: TLabel;
    spGenerarContratosCMR_RNUT: TADOStoredProc;
    txtDestino: TEdit;
    lblReferencia: TLabel;
    procedure btnSalirClick(Sender: TObject);
    procedure btnGenerarClick(Sender: TObject);
    function RegistrarOperacion : Boolean;                          // SS_1388_CQU_20150922
    function ActualizarLogAlFinal(pError : string) : boolean;       // SS_1388_CQU_20150922
  private
    { Private declarations }
    FCMR_Directorio_Contratos_RNUT: AnsiString;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	FCodigoOperacion : Integer;                                     					// SS_1388_CQU_20150922
  public
    { Public declarations }
    function  Inicializar(txtCaption : ANSIString; MDIChild : Boolean) : Boolean;
  end;

var
  FGenerarNominaCMRRNUT: TFGenerarNominaCMRRNUT;
const																// SS_1388_CQU_20150922
    RO_MOD_INTERFAZ_SALIENTE_CMR_RNUT = 110;						// SS_1388_CQU_20150922

implementation

{$R *.dfm}

function TFGenerarNominaCMRRNUT.Inicializar(txtCaption : ANSIString; MDIChild : Boolean) : Boolean;

    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        CMR_DIRECTORIO_DESTINO_CONTRATO_RNUT = 'CMR_Directorio_ContratosRNUT';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_DIRECTORIO_DESTINO_CONTRATO_RNUT , FCMR_Directorio_Contratos_RNUT) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_DIRECTORIO_DESTINO_CONTRATO_RNUT;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FCMR_Directorio_Contratos_RNUT := GoodDir(FCMR_Directorio_Contratos_RNUT);
                if  not DirectoryExists(FCMR_Directorio_Contratos_RNUT) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FCMR_Directorio_Contratos_RNUT;
                    Result := False;
                    Exit;
                end;
            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Defino el modo en que se visualizara la ventana
    if not MDIChild then begin
        FormStyle := fsNormal;
        Visible := False;
    end;
    //Centro el form
    CenterForm(Self);
  	try
        DMConnections.BaseCAC.Connected := True;
    		Result := DMConnections.BaseCAC.Connected and
                              VerificarParametrosGenerales;
  	except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
    Caption := AnsiReplaceStr(txtCaption, '&', '');
    txtDestino.Text := FCMR_Directorio_Contratos_RNUT;
end;

function TFGenerarNominaCMRRNUT.RegistrarOperacion : boolean;                                               // SS_1388_CQU_20150922
resourcestring                                                                                              // SS_1388_CQU_20150922
	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';                                 // SS_1388_CQU_20150922
    MSG_ERROR = 'Error';                                                                                    // SS_1388_CQU_20150922
var                                                                                                         // SS_1388_CQU_20150922
	DescError : string;                                                                                     // SS_1388_CQU_20150922
begin                                                                                                       // SS_1388_CQU_20150922
    Result := RegistrarOperacionEnLogInterface(                                                             // SS_1388_CQU_20150922
                                                DMConnections.BaseCAC,          // ConexionBD               // SS_1388_CQU_20150922
                                                RO_MOD_INTERFAZ_SALIENTE_CMR_RNUT,// CodigoModulo           // SS_1388_CQU_20150922
                                                '',                             // NombreArchivo            // SS_1388_CQU_20150922
                                                UsuarioSistema,                 // UsuarioSistema           // SS_1388_CQU_20150922
                                                '',                             // Observaciones            // SS_1388_CQU_20150922
                                                False,                          // Recibido                 // SS_1388_CQU_20150922
                                                False,                          // Reprocesado              // SS_1388_CQU_20150922
                                                NowBaseCN(DMConnections.BaseCAC),// FechaOperacion          // SS_1388_CQU_20150922
                                                0,                              // UltimoRegistroEnviado    // SS_1388_CQU_20150922
                                                FCodigoOperacion,               // CodigoOperacion          // SS_1388_CQU_20150922
                                                DescError);                     // Error                    // SS_1388_CQU_20150922
    if not Result then                                                                                      // SS_1388_CQU_20150922
        MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);                    // SS_1388_CQU_20150922
end;                                                                                                        // SS_1388_CQU_20150922

Function TFGenerarNominaCMRRNUT.ActualizarLogAlFinal(pError : string) : boolean;                            // SS_1388_CQU_20150922
Resourcestring                                                                                              // SS_1388_CQU_20150922
     MSG_ERROR = 'No se pudo Actualizar el log de operaciones';                                             // SS_1388_CQU_20150922
Const                                                                                                       // SS_1388_CQU_20150922
     STR_OBSERVACION        = 'Finalizo OK!';                                                               // SS_1388_CQU_20150922
     STR_OBSERVACION_ERROR  = 'Finalizo Con Error: %s';                                                     // SS_1388_CQU_20150922
var                                                                                                         // SS_1388_CQU_20150922
     DescError : String;                                                                                    // SS_1388_CQU_20150922
begin                                                                                                       // SS_1388_CQU_20150922
    Result := ActualizarLogOperacionesInterfaseAlFinal (                                                    // SS_1388_CQU_20150922
                                                DMConnections.BaseCAC,                                      // SS_1388_CQU_20150922
                                                FCodigoOperacion,                                           // SS_1388_CQU_20150922
                                                IIf(pError = '', STR_OBSERVACION,                           // SS_1388_CQU_20150922
                                                    Format(STR_OBSERVACION_ERROR, [pError])),               // SS_1388_CQU_20150922
                                                DescError);                                                 // SS_1388_CQU_20150922
    if not Result then                                                                                      // SS_1388_CQU_20150922
        MsgBoxErr(MSG_ERROR, DescError, Self.caption, MB_ICONERROR);                                        // SS_1388_CQU_20150922
end;                                                                                                        // SS_1388_CQU_20150922

procedure TFGenerarNominaCMRRNUT.btnGenerarClick(Sender: TObject);
resourcestring
	MSG_ERROR     = 'Error al generar el archivo de contratos CMR RNUT';
begin
	lblReferencia.Visible := True;
    btnGenerar.Enabled := False;
    KeyPreview := True;
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages; 
  	try
    	try
            RegistrarOperacion;                                                                             // SS_1388_CQU_20150922
        	with spGenerarContratosCMR_RNUT do begin
                Parameters.Refresh;																			// SS_1388_CQU_20150922
                Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;              // SS_1388_CQU_20150922
                CommandTimeout := 5000;
                ExecProc;
        	end;
            ActualizarLogAlFinal('');                                                                       // SS_1388_CQU_20150922
		except
            on  E : Exception do begin
                MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
                ActualizarLogAlFinal(e.Message);                                                            // SS_1388_CQU_20150922
                Exit;
            end;
        end;
    finally
    	btnGenerar.Enabled := True;
        Screen.Cursor := crDefault;
        lblReferencia.Visible := False;
        MsgBox('La generaci�n del Archivo de Contratos CMR RNUT ha finalizado con �xito', Self.Caption, MB_OK + MB_ICONINFORMATION);
    end;
end;

procedure TFGenerarNominaCMRRNUT.btnSalirClick(Sender: TObject);
begin
	Close;
end;

end.
