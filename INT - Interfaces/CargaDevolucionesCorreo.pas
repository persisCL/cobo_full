{-----------------------------------------------------------------------------
 Unit Name: CargaDevolucionesCorreo.pas
 Author:    mtraversa
 Date Created: 25/01/2005
 Language: ES-AR
 Description: Formulario para la carga de los comprobantes devueltos por el correo

 Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

  
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

 -----------------------------------------------------------------------------}
unit CargaDevolucionesCorreo;

interface

uses
  //Devoluciones del Correo
  DMConnection,
  ComunesInterfaces,
  PeaProcs,
  UtilProc,
  UtilDB,
  Util,
  PeaTypes,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, BuscaTab, Buttons, ListBoxEx, DBListEx,
  Validate, DateEdit, DB, ADODB, DBClient, ExtCtrls, ComCtrls, Menus;

type
  TfrmCargaDevolucionesCorreo = class(TForm)
    btMotivoDev: TBuscaTabla;
    tblMotivosDevolucion: TADOTable;
    tblMotivosDevolucionCodigoMotivoDevolucion: TIntegerField;
    tblMotivosDevolucionDetalle: TStringField;
    cdsComprobantes: TClientDataSet;
    cdsComprobantesNumeroComprobante: TIntegerField;
    cdsComprobantesCliente: TStringField;
    cdsComprobantesFechaVencimiento: TDateTimeField;
    cdsComprobantesTotal: TCurrencyField;
    cdsComprobantesCodigoBarras: TStringField;
    cdsComprobantesCodMotivoDevolucion: TIntegerField;
    cdsComprobantesDescMotivoDevolucion: TStringField;
    dsComprobantes: TDataSource;
    spObtenerComprobanteDevuelto: TADOStoredProc;
    spActualizarComprobantesDevueltosCorreo: TADOStoredProc;
    spComprobanteDevueltoCorreoRepetido: TADOStoredProc;
    Label2: TLabel;
    lblCodBarras: TLabel;
    edCodBarras: TEdit;
    edNumNotaCobro: TNumericEdit;
    lblNunNotaCobro: TLabel;
    bteMotivoDev: TBuscaTabEdit;
    dblComprobantes: TDBListEx;
    Panel1: TPanel;
    btnFinalizar: TButton;
    btnCancelar: TButton;
    Label3: TLabel;
    edIdLote: TEdit;
    Label1: TLabel;
    deFechaRecepcion: TDateEdit;
    popCartas: TPopupMenu;
    Eliminar1: TMenuItem;
    miEliminarUnaCarta: TMenuItem;
    miEliminarTodasLasCartas: TMenuItem;
    Bevel1: TBevel;
    function btMotivoDevProcess(Tabla: TDataSet;var Texto: String): Boolean;
    procedure btMotivoDevSelect(Sender: TObject; Tabla: TDataSet);
    procedure edCodBarrasChange(Sender: TObject);
    procedure btnFinalizarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure edNumNotaCobroKeyPress(Sender: TObject; var Key: Char);
    procedure edCodBarrasExit(Sender: TObject);
    procedure LoteControlsKeyPress(Sender: TObject; var Key: Char);
    procedure miEliminarUnaCartaClick(Sender: TObject);
    procedure miEliminarTodasLasCartasClick(Sender: TObject);
  private
    FCodigoMotivoDevolucion: Integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    { Private declarations }
  public
    function Inicializar: Boolean;
    { Public declarations }
  end;

const
    RO_MOD_INTERFAZ_ENTRANTE_DEVOLUCION_CORREO = 37;

resourcestring
    MSG_ERROR = 'Error';

var
  frmCargaDevolucionesCorreo: TfrmCargaDevolucionesCorreo;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: Inicializar
Author : lgisuk
Date Created : 15/07/2005
Description : Inicializacion de este formulario
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TfrmCargaDevolucionesCorreo.Inicializar: Boolean;
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    if not OpenTables([tblMotivosDevolucion]) then Exit;
    if cdsComprobantes.Active then
        cdsComprobantes.EmptyDataSet
    else begin
        cdsComprobantes.CreateDataSet;
        if not OpenTables([cdsComprobantes]) then Exit;
    end;
    edIdLote.Clear;
    deFechaRecepcion.Clear;
    bteMotivoDev.Clear;
    FCodigoMotivoDevolucion := -1;
    edCodBarras.Clear;
    edNumNotaCobro.Clear;
    if lblCodBarras.Enabled then begin
        lblCodBarras.Enabled    := False;
        edCodBarras.Enabled     := False;
        lblNunNotaCobro.Enabled := False;
        edNumNotaCobro.Enabled  := False;
    end;
    edIdLote.SetFocus;

    Result := True;
end;


{******************************** Function Header ******************************
Function Name: btMotivoDevProcess
Author : lgisuk
Date Created : 15/07/2005
Description : Muestro motivos de devolucion
Parameters : Tabla: TDataSet;var Texto: String
Return Value : Boolean
*******************************************************************************}
function TfrmCargaDevolucionesCorreo.btMotivoDevProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
	Result := True;
    Texto  := Tabla.FieldByName('Detalle').AsString;
end;

{******************************** Function Header ******************************
Function Name: btMotivoDevSelect
Author : lgisuk
Date Created : 15/07/2005
Description : Permito elegir motivo de devoluci�n
Parameters : Sender: TObject;Tabla: TDataSet
Return Value : None
*******************************************************************************}
procedure TfrmCargaDevolucionesCorreo.btMotivoDevSelect(Sender: TObject;Tabla: TDataSet);
begin
    with Tabla do begin
        FCodigoMotivoDevolucion := FieldByName('CodigoMotivoDevolucion').AsInteger;
        bteMotivoDev.Text       := FieldByName('Detalle').AsString;

        if not lblCodBarras.Enabled then begin
            lblCodBarras.Enabled    := True;
            edCodBarras.Enabled     := True;
            lblNunNotaCobro.Enabled := True;
            edNumNotaCobro.Enabled  := True;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: TfrmCargaDevolucionesCorreo.edCodBarrasChange
  Author:    mtraversa
  Date Created: 25/01/2005
  Description: Aca se carga el comprobante a la grilla cuando la long del codigo
               de barras es de 29
  Parsmeters: Sender: TObject
  Return Value:None
-----------------------------------------------------------------------------}
procedure TfrmCargaDevolucionesCorreo.edCodBarrasChange(Sender: TObject);

    {******************************** Function Header ******************************
    Function Name: CargaComprobante
    Author : lgisuk
    Date Created : 15/07/2005
    Description : carga un comprobante al dataset
    Parameters : const NumeroComprobante: Int64
    Return Value : Boolean
    *******************************************************************************}
    function CargaComprobante(const NumeroComprobante: Int64): Boolean;
    begin
        with spObtenerComprobanteDevuelto do begin
            Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
            ExecProc;
            if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then begin
                //Comprobante OK
                cdsComprobantes.Append;
                cdsComprobantes.FieldByName('NumeroComprobante').Value    := NumeroComprobante;
                cdsComprobantes.FieldByName('Cliente').Value              := Parameters.ParamByName('@Cliente').Value;
                cdsComprobantes.FieldByName('FechaVencimiento').Value     := Parameters.ParamByName('@FechaVencimiento').Value;
                cdsComprobantes.FieldByName('Total').Value                := Parameters.ParamByName('@Total').Value;
                cdsComprobantes.FieldByName('CodigoBarras').Value         := edCodBarras.Text;
                cdsComprobantes.FieldByName('CodMotivoDevolucion').Value  := FCodigoMotivoDevolucion;
                cdsComprobantes.FieldByName('DescMotivoDevolucion').Value := bteMotivoDev.Text;
                cdsComprobantes.Post;

                Result := True;
            end
            else
                //Comprobante inexistente
                Result := False;
        end;
    end;

    {******************************** Function Header ******************************
    Function Name: ComprobanteInsertado
    Author : lgisuk
    Date Created : 15/07/2005
    Description : Me fijo primero si esta en los ingresados actualmente
    Parameters : const NumeroComprobante: Int64
    Return Value : Boolean
    *******************************************************************************}
    function ComprobanteInsertado(const NumeroComprobante: Int64): Boolean;
    begin
        //Me fijo primero si esta en los ingresados actualmente
        Result := cdsComprobantes.Locate('NumeroComprobante', NumeroComprobante, []);
    end;

resourcestring
    MSG_NOT_EXISTS    = 'Comprobante inexistente';
    MSG_INVALID_EPS   = 'Empresa prestadora de servicios inv�lida';
    MSG_COMP_INSERTED = 'Comprobante ya insertado dentro del lote';
    MSG_IMPUT_ERROR   = 'Error al cargar el comprobante';
    COD_EPS           = '001';
var
    sComprobante: ShortString;
begin
    if Length(edCodBarras.Text) = 29 then begin

        //El numero de comprobante son los ultimos nueve caracteres del codigo de barras
        sComprobante := Trim(Copy(edCodBarras.Text, 7, 9));

        if (sComprobante <> '') then begin
            if (ValidateControls([edCodBarras, ActiveControl],
                                 [Copy(edCodBarras.Text, 1, 3) = COD_EPS,
                                  not ComprobanteInsertado(StrToInt(sComprobante))],
                                 Self.Caption,
                                 [MSG_INVALID_EPS,
                                  MSG_COMP_INSERTED])) then
            begin
                if (ValidateControls([ActiveControl], [CargaComprobante(StrToInt(sComprobante))],
                                     Self.Caption, [MSG_NOT_EXISTS])) then begin
                    edCodBarras.Clear;
                    edNumNotaCobro.Clear;
                end
                else
                    if ActiveControl is TCustomEdit then
                        TCustomEdit(ActiveControl).SelectAll;
            end
            else
                if ActiveControl is TCustomEdit then
                    TCustomEdit(ActiveControl).SelectAll;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: TfrmCargaDevolucionesCorreo.edNumNotaCobroKeyPress
  Author:    mtraversa
  Date Created: 25/01/2005
  Description: Cuando se ingresa el numero de comprobante a manos completo el
               codigo de barras con ceros
  Parsmeters: Sender: TObject; var Key: Char
  Return Value:None
-----------------------------------------------------------------------------}
procedure TfrmCargaDevolucionesCorreo.edNumNotaCobroKeyPress(Sender: TObject;var Key: Char);
begin
	if not (Key in [#8, #9, #13, '0'..'9']) then
        Key := #0;

    if (Key = #13) then begin
        Key := #0;
        edCodBarras.Text := StringReplace(Format('001%12d',[IVal(edNumNotaCobro.Text)]), ' ', '0', [rfReplaceAll]) + '00000000000000';
    end else if (edCodBarras.Text <> EmptyStr) then edCodBarras.Clear;
end;

{-----------------------------------------------------------------------------
  Function Name: TfrmCargaDevolucionesCorreo.btnFinalizarClick
  Author:    mtraversa
  Date Created: 25/01/2005
  Description: Al finalizar los cambios se hacen efectivos en la BD y se
               reinicilizan todos los controles
  Parsmeters: Sender: TObject
  Return Value:None
-----------------------------------------------------------------------------}
procedure TfrmCargaDevolucionesCorreo.btnFinalizarClick(Sender: TObject);

    {******************************** Function Header ******************************
    Function Name: ExisteComprobanteRepetido
    Author : lgisuk
    Date Created : 15/07/2005
    Description :  Me fijo si el lote ya fue procesado y si alguno de los comprobantes ya fueron registrados
    Parameters : None
    Return Value : Boolean
    *******************************************************************************}
    function ExisteComprobanteRepetido: Boolean;
    begin
        //Me fijo si el lote ya fue procesado y si alguno de los comprobantes ya fueron registrados
        Result := False;

        cdsComprobantes.DisableControls;
        try
            cdsComprobantes.First;
            while not cdsComprobantes.Eof do begin
                with spComprobanteDevueltoCorreoRepetido, Parameters do begin
                    ParamByName('@CodigoLoteDevolucion').Value := Trim(edIdLote.Text);
                    ParamByName('@TipoComprobante').Value      := 'NK';
                    ParamByName('@NumeroComprobante').Value    := cdsComprobantes.FieldByName('NumeroComprobante').Value;
                    ExecProc;
                    if ParamByName('@RETURN_VALUE').Value = 1 then begin
                        Result := True;
                        Exit;
                    end;
                end;
                cdsComprobantes.Next;
            end;
            cdsComprobantes.First;
        finally
            cdsComprobantes.EnableControls;
        end;
    end;

    {******************************** Function Header ******************************
    Function Name: RegistrarOperacion
    Author : lgisuk
    Date Created : 15/07/2005
    Description : Registro la operacion en el log de operaciones
    Parameters : None
    Return Value : Boolean
    *******************************************************************************}
    function RegistrarOperacion: Boolean;
    resourcestring
        MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
        MSG_ERROR = 'Error';
    var
        DescError : string;
        Cod: Integer;
    begin
        Cod := 0;
        Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRANTE_DEVOLUCION_CORREO, EmptyStr, UsuarioSistema, EmptyStr, True, False, deFechaRecepcion.Date, 0, Cod, DescError);
       if not result then
            MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
    end;

resourcestring
    MSG_NO_RECORDS              = 'Ingrese los comprobantes devueltos';
	MSG_PROCESS_ENDS_OK         = 'El proceso finaliz� con �xito';
    MSG_PROCESS_CANNOT_COMPLETE = 'El proceso no se pudo completar';
    MSG_INPUT_ID_LOTE           = 'Ingrese la identificaci�n del lote';
    MSG_INPUT_VALID_DATE        = 'Ingrese la fecha de recepci�n del lote';
    MSG_CONFIRM                 = 'Lote: %s'#13'Cantidad de cartas le�das: %d'#13#13'�Desea continuar con la operaci�n?';
    MSG_CAPTION_CONFIRM         = 'Confirme la operaci�n';
    MSG_COMP_INSERTED           = 'El comprobante fue previamente registrado en el lote';
begin
    if ValidateControls([edIdLote, deFechaRecepcion, dblComprobantes],
                        [Trim(edIdLote.Text) <> EmptyStr, not deFechaRecepcion.IsEmpty, not cdsComprobantes.IsEmpty],
                        Self.Caption,
                        [MSG_INPUT_ID_LOTE, MSG_INPUT_VALID_DATE, MSG_NO_RECORDS]) then

        if ValidateControls([dblComprobantes], [not ExisteComprobanteRepetido], Self.Caption, [MSG_COMP_INSERTED]) then begin

            if MsgBox(Format(MSG_CONFIRM, [edIdLote.Text, cdsComprobantes.RecordCount]), MSG_CAPTION_CONFIRM, MB_YESNO + MB_ICONQUESTION) = IDNO then
                Exit;

            btnFinalizar.Enabled := False;
            btnCancelar.Enabled  := False;
            cdsComprobantes.DisableControls;
            try
                //DMConnections.BaseCAC.BeginTrans;                                       //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('BEGIN TRAN CargaDevolucionesCorreo');     //SS_1385_NDR_20150922
                try
                    cdsComprobantes.First;
                    while not cdsComprobantes.Eof do begin
                        with spActualizarComprobantesDevueltosCorreo, Parameters do begin
                            ParamByName('@CodigoLoteDevolucion').Value    := Trim(edIdLote.Text);
                            ParamByName('@TipoComprobante').Value         := 'NK';
                            ParamByName('@NumeroComprobante').Value       := cdsComprobantes.FieldByName('NumeroComprobante').Value;
                            ParamByName('@FechaRecepcion').Value          := deFechaRecepcion.Date;
                            ParamByName('@CodigoMotivoDevolucion').Value := cdsComprobantes.FieldByName('CodMotivoDevolucion').Value;
                            ExecProc;
                        end;
                        cdsComprobantes.Next;
                    end;

                    RegistrarOperacion;  //Creo el log de la operacion

                    //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
                    DMConnections.BaseCAC.Execute('COMMIT TRAN CargaDevolucionesCorreo');					//SS_1385_NDR_20150922

                    MsgBox(MSG_PROCESS_ENDS_OK);

                    Inicializar;
                except
                    on E: Exception do begin
                        //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                        DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION CargaDevolucionesCorreo END');	//SS_1385_NDR_20150922
                        MsgBoxErr(MSG_PROCESS_CANNOT_COMPLETE, E.Message, MSG_ERROR, MB_ICONERROR);
                    end;
                end;
            finally
                cdsComprobantes.EnableControls;
                btnFinalizar.Enabled := True;
                btnCancelar.Enabled  := True;
            end;
        end;
end;


{******************************** Function Header ******************************
Function Name: edCodBarrasExit
Author : lgisuk
Date Created : 15/07/2005
Description : Borra el codigo de barras
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmCargaDevolucionesCorreo.edCodBarrasExit(Sender: TObject);
begin
    edCodBarras.Clear;
end;

{-----------------------------------------------------------------------------
  Function Name: TfrmCargaDevolucionesCorreo.LoteControlsKeyPress
  Author:    mtraversa
  Date Created: 25/01/2005
  Description: Se usa en el OnKeyPress de los controles del Lote y en el motivo
               de devolucion
  Parsmeters: Sender: TObject; var Key: Char
  Return Value:None
-----------------------------------------------------------------------------}
procedure TfrmCargaDevolucionesCorreo.LoteControlsKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (Key = #13) then begin
        if (edNumNotaCobro.Text <> EmptyStr) then
            edNumNotaCobro.OnKeyPress(edNumNotaCobro, Key)
        else
            edCodBarras.OnChange(Sender);
        Key := #0;
    end;
end;

{******************************** Function Header ******************************
Function Name: miEliminarUnaCartaClick
Author : lgisuk
Date Created : 15/07/2005
Description : si el dataset no esta vacio elimina
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmCargaDevolucionesCorreo.miEliminarUnaCartaClick(Sender: TObject);
begin
    if not cdsComprobantes.IsEmpty then cdsComprobantes.Delete;
end;

{******************************** Function Header ******************************
Function Name: miEliminarTodasLasCartasClick
Author : lgisuk
Date Created : 15/07/2005
Description : deja el dataset vacio
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmCargaDevolucionesCorreo.miEliminarTodasLasCartasClick(Sender: TObject);
begin
    if not cdsComprobantes.IsEmpty then cdsComprobantes.EmptyDataSet;
end;

{******************************** Function Header ******************************
Function Name: btnCancelarClick
Author : lgisuk
Date Created : 15/07/2005
Description :  permito cerrar el formulario
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmCargaDevolucionesCorreo.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : lgisuk
Date Created : 15/07/2005
Description : lo libero de memoria
Parameters : Sender: TObject;var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TfrmCargaDevolucionesCorreo.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    cdsComprobantes.Close;
    tblMotivosDevolucion.Close;
    Action := caFree;
end;


end.
