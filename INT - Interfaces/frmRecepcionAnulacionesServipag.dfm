object RecepcionAnulacionesServipagForm: TRecepcionAnulacionesServipagForm
  Left = 0
  Top = 0
  Caption = 'Recepci'#243'n de Anulaciones'
  ClientHeight = 275
  ClientWidth = 542
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 16
    Top = 24
    Width = 505
    Height = 185
  end
  object lblMensaje: TLabel
    Left = 48
    Top = 160
    Width = 50
    Height = 13
    Caption = 'lblMensaje'
  end
  object btnProcesar: TButton
    Left = 365
    Top = 232
    Width = 75
    Height = 25
    Caption = 'Procesar'
    TabOrder = 0
    OnClick = btnProcesarClick
  end
  object btnSalir: TButton
    Left = 446
    Top = 232
    Width = 75
    Height = 25
    Caption = 'Salir'
    TabOrder = 1
    OnClick = btnSalirClick
  end
  object pnlOrigen: TPanel
    Left = 32
    Top = 32
    Width = 417
    Height = 41
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 0
      Top = 11
      Width = 36
      Height = 13
      Caption = 'Origen:'
    end
    object btnAbrirArchivo: TSpeedButton
      Left = 384
      Top = 8
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = btnAbrirArchivoClick
    end
    object txtOrigen: TEdit
      Left = 42
      Top = 8
      Width = 336
      Height = 21
      TabOrder = 0
      OnChange = txtOrigenChange
    end
  end
  object pnlAyuda: TPanel
    Left = 475
    Top = 40
    Width = 25
    Height = 23
    BevelInner = bvRaised
    TabOrder = 3
    object imgAyuda: TImage
      Left = 2
      Top = 2
      Width = 21
      Height = 19
      Cursor = crHandPoint
      Align = alClient
      Picture.Data = {
        07544269746D617036100000424D361000000000000036000000280000002000
        0000200000000100200000000000001000000000000000000000000000000000
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6
        C600B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
        B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
        B500B5B5B500B5B5B500B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636B63006363
        6300636363006363630063636300636363006363630063636300636363006363
        6300636363006363630063636300636363006363630063636300636363006363
        63006363630063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006363630094E7
        FF008CE7FF009CE7FF009CE7FF009CE7FF009CE7FF00A5EFFF00ADEFFF00ADEF
        FF00ADEFFF00ADEFFF00B5EFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00C6F7FF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        630063636300EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        630063636300EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF009CD6
        FF00B5DEFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF009CD6
        FF00B5DEFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        63004AADFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        63000084FF00DEEFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00B5DE
        FF0063636300108CFF00DEEFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF009CD6FF0063636300108CFF00DEF7FF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00DEEFFF0063636300108CFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00C6EFFF00ADDEFF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF0084C6FF0063636300A5D6FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF006363630063636300DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00A5D6FF0063636300A5D6FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF0042A5FF006363630094D6FF00E7FF
        FF00E7FFFF00DEEFFF00108CFF0063636300DEF7FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00C6EFFF00108CFF0063636300319C
        FF004AADFF000084FF00636363006BBDFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00CEEFFF0042A5FF006363
        6300636363006363630084C6FF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0063636300BDEF
        FF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636B63006363
        6300636363006363630063636300636363006363630063636300636363006363
        6300636363006363630063636300636363006363630063636300636363006363
        63006363630063636300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00}
      Stretch = True
      Transparent = True
      OnClick = imgAyudaClick
      OnMouseMove = imgAyudaMouseMove
      ExplicitLeft = 8
      ExplicitTop = 8
      ExplicitWidth = 105
      ExplicitHeight = 105
    end
  end
  object pnlAvance: TPanel
    Left = 48
    Top = 79
    Width = 401
    Height = 65
    BevelOuter = bvNone
    TabOrder = 4
    object Label2: TLabel
      Left = 0
      Top = 32
      Width = 83
      Height = 13
      Caption = 'Progreso General'
    end
    object pbProgreso: TProgressBar
      Left = 96
      Top = 32
      Width = 297
      Height = 17
      TabOrder = 0
    end
  end
  object OpenDialog: TOpenDialog
    Left = 24
    Top = 224
  end
  object spProcesarAnulacionesServipag: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ProcesarAnulacionesServipag;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Fecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EPS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@Sucursal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Terminal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@Correlativo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaContable'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoServicio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@TipoOperacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@IndicadorContable'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Monto'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@MedioPago'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@CodigoBancoCheque'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CuentaCheque'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@SerieCheque'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PlazaCheque'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@TipoTarjeta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@MarcaTarjeta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@NumeroTarjeta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ExpiracionTarjeta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@TipoCuotas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@NumeroCuotas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Autorizacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@NotaCobro'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Identificador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 91
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@TipoComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@DescripcionError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 64
    Top = 224
  end
  object spObtenerResumenAnulacionesServipag: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerResumenAnulacionesServipag;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaProcesamiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = 36526d
      end
      item
        Name = '@NombreArchivo'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = #39#39
      end
      item
        Name = '@Modulo'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = #39#39
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = #39#39
      end
      item
        Name = '@CantidadAnulacionesRecibidas'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end
      item
        Name = '@ImporteTotalRecibido'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = '0'
      end
      item
        Name = '@CantidadAnulacionesAceptadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end
      item
        Name = '@ImporteTotalAceptado'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = '0'
      end
      item
        Name = '@CantidadAnulacionesRechazadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end
      item
        Name = '@ImporteTotalRechazado'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = '0'
      end>
    Left = 104
    Top = 224
  end
end
