object frmCargaDevolucionesCorreo: TfrmCargaDevolucionesCorreo
  Left = 151
  Top = 145
  Caption = 'Registraci'#243'n de devoluciones del Correo'
  ClientHeight = 437
  ClientWidth = 648
  Color = clBtnFace
  Constraints.MinHeight = 475
  Constraints.MinWidth = 664
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    648
    437)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 641
    Height = 153
    Anchors = [akLeft, akTop, akRight]
  end
  object Label2: TLabel
    Left = 14
    Top = 72
    Width = 90
    Height = 13
    Caption = '&Motivo devoluci'#243'n:'
    FocusControl = bteMotivoDev
  end
  object lblCodBarras: TLabel
    Left = 14
    Top = 96
    Width = 133
    Height = 13
    Caption = 'C'#243'd. &Barras  Nota de Cobro:'
    Enabled = False
    FocusControl = edCodBarras
  end
  object lblNunNotaCobro: TLabel
    Left = 14
    Top = 120
    Width = 112
    Height = 13
    Caption = 'N'#250'mero No&ta de Cobro:'
    Enabled = False
    FocusControl = edNumNotaCobro
  end
  object Label3: TLabel
    Left = 14
    Top = 22
    Width = 90
    Height = 13
    Caption = 'Identificacion Lote:'
  end
  object Label1: TLabel
    Left = 14
    Top = 47
    Width = 118
    Height = 13
    Caption = 'F&echa de recepci'#243'n:'
    FocusControl = deFechaRecepcion
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edCodBarras: TEdit
    Left = 151
    Top = 92
    Width = 250
    Height = 21
    Enabled = False
    MaxLength = 29
    TabOrder = 3
    OnChange = edCodBarrasChange
    OnExit = edCodBarrasExit
  end
  object edNumNotaCobro: TNumericEdit
    Left = 151
    Top = 116
    Width = 145
    Height = 21
    Enabled = False
    TabOrder = 4
    OnKeyPress = edNumNotaCobroKeyPress
  end
  object bteMotivoDev: TBuscaTabEdit
    Left = 151
    Top = 68
    Width = 433
    Height = 21
    Enabled = True
    ReadOnly = True
    TabOrder = 2
    OnKeyPress = LoteControlsKeyPress
    EditorStyle = bteTextEdit
    BuscaTabla = btMotivoDev
  end
  object dblComprobantes: TDBListEx
    Left = 0
    Top = 171
    Width = 656
    Height = 229
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'N'#250'mero'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroComprobante'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 400
        Header.Caption = 'Motivo devoluci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescMotivoDevolucion'
      end>
    DataSource = dsComprobantes
    DragReorder = True
    ParentColor = False
    PopupMenu = popCartas
    TabOrder = 5
    TabStop = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 396
    Width = 648
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 6
    ExplicitTop = 400
    ExplicitWidth = 656
    DesignSize = (
      648
      41)
    object btnFinalizar: TButton
      Left = 486
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btnFinalizarClick
    end
    object btnCancelar: TButton
      Left = 566
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
  object edIdLote: TEdit
    Left = 151
    Top = 18
    Width = 145
    Height = 21
    MaxLength = 15
    TabOrder = 0
    OnKeyPress = LoteControlsKeyPress
  end
  object deFechaRecepcion: TDateEdit
    Left = 151
    Top = 43
    Width = 102
    Height = 21
    AutoSelect = False
    TabOrder = 1
    OnKeyPress = LoteControlsKeyPress
    Date = -693594.000000000000000000
  end
  object btMotivoDev: TBuscaTabla
    Caption = 'Seleccionar Motivo Devoluci'#243'n'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = tblMotivosDevolucion
    OnProcess = btMotivoDevProcess
    OnSelect = btMotivoDevSelect
    Left = 143
    Top = 360
  end
  object tblMotivosDevolucion: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'MotivosDevolucionComprobantesCorreo'
    Left = 175
    Top = 360
    object tblMotivosDevolucionCodigoMotivoDevolucion: TIntegerField
      FieldName = 'CodigoMotivoDevolucion'
    end
    object tblMotivosDevolucionDetalle: TStringField
      FieldName = 'Detalle'
      Size = 100
    end
  end
  object cdsComprobantes: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'NumeroComprobante'
        DataType = ftInteger
      end
      item
        Name = 'Cliente'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'FechaVencimiento'
        DataType = ftDateTime
      end
      item
        Name = 'Total'
        DataType = ftCurrency
      end
      item
        Name = 'CodigoBarras'
        DataType = ftString
        Size = 29
      end
      item
        Name = 'CodMotivoDevolucion'
        DataType = ftInteger
      end
      item
        Name = 'DescMotivoDevolucion'
        DataType = ftString
        Size = 100
      end>
    IndexDefs = <>
    IndexFieldNames = 'NumeroComprobante'
    Params = <>
    StoreDefs = True
    Left = 208
    Top = 360
    object cdsComprobantesNumeroComprobante: TIntegerField
      FieldName = 'NumeroComprobante'
      DisplayFormat = '000000000'
    end
    object cdsComprobantesCliente: TStringField
      FieldName = 'Cliente'
      Size = 200
    end
    object cdsComprobantesFechaVencimiento: TDateTimeField
      FieldName = 'FechaVencimiento'
    end
    object cdsComprobantesTotal: TCurrencyField
      FieldName = 'Total'
    end
    object cdsComprobantesCodigoBarras: TStringField
      FieldName = 'CodigoBarras'
      Size = 29
    end
    object cdsComprobantesCodMotivoDevolucion: TIntegerField
      FieldName = 'CodMotivoDevolucion'
    end
    object cdsComprobantesDescMotivoDevolucion: TStringField
      FieldName = 'DescMotivoDevolucion'
      Size = 100
    end
  end
  object dsComprobantes: TDataSource
    DataSet = cdsComprobantes
    Left = 112
    Top = 360
  end
  object spObtenerComprobanteDevuelto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerComprobanteDevuelto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Cliente'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 210
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@Total'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 16
    Top = 360
  end
  object spActualizarComprobantesDevueltosCorreo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarComprobantesDevueltosCorreo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLoteDevolucion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaRecepcion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoMotivoDevolucion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 80
    Top = 360
  end
  object spComprobanteDevueltoCorreoRepetido: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ComprobanteDevueltoCorreoRepetido;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLoteDevolucion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 48
    Top = 360
  end
  object popCartas: TPopupMenu
    Left = 448
    Top = 280
    object Eliminar1: TMenuItem
      Caption = 'Eliminar'
      object miEliminarUnaCarta: TMenuItem
        Caption = 'Esta carta'
        OnClick = miEliminarUnaCartaClick
      end
      object miEliminarTodasLasCartas: TMenuItem
        Caption = 'Todas todas las cartas'
        OnClick = miEliminarTodasLasCartasClick
      end
    end
  end
end
