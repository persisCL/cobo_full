
{********************************** Unit Header ********************************
File Name : fRecepcionArchivoVentasBHTU.pas
Author : ndonadio
Date Created: 30/09/2005
Language : ES-AR
Description : Carga el archivo de ventas de BHTU seg�n la especificaci�n
              "Clearing BHTU" ver. 3
            y basado principalmente en la interfaz existente para carga de
            Pases Diarios Tardios (DayPass.pas, flamas, 11/03/2005)
*******************************************************************************}
unit fRecepcionArchivoVentasBHTU;

interface

uses
  // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils, DateUtils,
    // Parametros Generales
    ConstParametrosGenerales,
    // DB
    UtilDB, DMConnection,
    // Reporte finalizacion
    FrmRptDayPass,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes,  Graphics,  Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB,     ppDBPipe, ppPrnabl, ppClass,
    ppCtrls, ppBands,  ppCache,  ppComm,   ppRelatv, ppProd,    ppReport, UtilRB;

type
 TLateDayPassRecord = record
    Patente         : AnsiString;
    Categoria       : byte;
    Monto           : integer;
    FechaVenta      : TDateTime;
    FechaRendicion  : TDateTime;
    Identificador   : AnsiString;
//  CodigoMaterial  : string;
  end;


  TfrmRecepcionArchivoVentasBHTU = class(TForm)
    Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    lblEsc: TLabel;
    lblDetalle: TLabel;
    pbProgreso: TProgressBar;
    txtOrigen: TPickEdit;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    OpenDialog: TOpenDialog;
    spAgregarDayPassTardio: TADOStoredProc;
    procedure txtOrigenButtonClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
  private
    { Private declarations }
    FErrorLog,
    FPasesDiariosTardios        : TStringList;
    FErrors,
    FCancelar,
    FProcesando                 : Boolean;
    FBHTU_Directorio_Procesados,
    FBHTU_Directorio_Entrada    : AnsiString;
    FCodigoOperacion,
    FCantidadErrores            : integer;
    procedure HabilitarBotones;
    procedure EliminarLineasEnBlanco( var PaseDiario : TStringList);
    function VerificarCantidadDeRegistros(FPasesDiarios : TStringList; sArchivo : string) : boolean;
    function AnalizarArchivoPasesDiariosTardios: boolean;
    function RegistrarOperacion(CodigoModulo: integer; sFileName: string): boolean;
    function CargarPasesDiariosTardios: boolean;
    function ObtenerCantidadErroresInterfaz(CodigoOperacion: integer;
      var Cantidad: integer): boolean;
    function ActualizarLog(CantidadErrores: integer): boolean;
    function GenerarReporteDeFinalizacion(CodigoOperacionInterfase: integer): boolean;
    function ParseLateDayPassLine(sline: string; var LateDayPassRecord: TLateDayPassRecord;
      var sParseError: string): boolean;
    procedure AddParsingErrors(CodigoOperacion: Integer);
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): boolean;
  end;


Const
    FILE_NAME_PREFIX            = 'MC';//'CN';
    FILE_NAME_DATEFORMAT        = 'YYMMDD';
    FILE_EXTENSION              = '.OK';  // '.MOK'
  	RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO_TARDIO		= 40;


var
  frmRecepcionArchivoVentasBHTU: TfrmRecepcionArchivoVentasBHTU;

implementation

{$R *.dfm}

procedure TfrmRecepcionArchivoVentasBHTU.btnProcesarClick(Sender: TObject);
resourcestring
    MSG_ERROR 								= 'Error';
  	MSG_FILE_ALREADY_PROCESSED 				= 'El archivo %s ya fue procesado' + crlf + 'Desea reprocesarlo ?';
    MSG_ERRROR_LATE_DAYPASS_FILE_IS_EMPTY	= 'El archivo de pases diarios est� vac�o';
  	MSG_ERROR_CANNOT_OPEN_LATE_DAYPASS_FILE	= 'No se puede abrir el archivo de pases diarios tard�os';
  	MSG_PROCESS_SUCCEDED 					= 'El proceso finaliz� con �xito';
  	MSG_PROCESS_ENDED_WITH_ERRORS			= 'El proceso finaliz� con errores';
  	MSG_PROCESS_COULD_NOT_BE_COMPLETED 		= 'El proceso no se pudo completar';
    MSG_LATE_DAYPASS_SUCCEDED				= 'La importaci�n de Pases Diarios Tard�os finaliz� con �xito';
    MSG_LATE_DAYPASS_FILE					= 'Archivo de Pase Diario Tard�o';
begin
	// Verifica que el Archivo no haya sido procesado
	if  VerificarArchivoProcesado(DMConnections.BaseCAC, txtOrigen.Text) then begin
        if MsgBox (Format (MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtOrigen.Text)]),
        	Caption, MB_ICONWARNING or MB_YESNO) <> IDYES then Exit;
    end;

    // Creo el stringlist
    FPasesDiariosTardios := TStringList.Create;
    FErrorLog := TStringList.Create;
	// Deshabilita y Habilita los botones
    FProcesando := True;
    HabilitarBotones;

    try
		try
            //Leo el archivo de pases diarios tardios
			FPasesDiariosTardios.LoadFromFile(txtOrigen.Text);

            //Elimino las lineas en blanco
            EliminarLineasEnBlanco(FPasesDiariosTardios);

			//Verifica si el Archivo Contiene alguna linea
			if (FPasesDiariosTardios.Count = 0) then begin

                //Informo que el archivo esta vacio
				MsgBox(MSG_ERRROR_LATE_DAYPASS_FILE_IS_EMPTY, Caption, MB_ICONERROR)

			end else begin
            	if VerificarCantidadDeRegistros(FPasesDiariosTardios, MSG_LATE_DAYPASS_FILE) and
                	AnalizarArchivoPasesDiariosTardios then begin
                    	if RegistrarOperacion(RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO_TARDIO, ExtractFileName(txtOrigen.Text))
                        	and CargarPasesDiariosTardios then begin

                                //Verifico si hubo errores
                                if not FErrors then begin
                                    //Informo que el proceso finalizo con exito
                                    MsgBox(MSG_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION);
                                end else begin
                                    //Informo que el proceso finalizo con errores
                                    MsgBox(MSG_PROCESS_ENDED_WITH_ERRORS, Caption, MB_ICONINFORMATION);
                                end;
                                AddParsingErrors(FCodigoOperacion);
                                //Obtengo la cantidad de errores
                                ObtenerCantidadErroresInterfaz(FCodigoOperacion, FCantidadErrores);
                                //Actualizo el log al Final
                                ActualizarLog(FCantidadErrores);
                                //Muestro el Reporte de Finalizacion del Proceso
                                GenerarReportedeFinalizacion(FCodigoOperacion);
                                //Muevo el archivo procesado a otro directorio
                                MoverArchivoProcesado(Caption, txtOrigen.Text, FBHTU_Directorio_Procesados + ExtractFileName(txtOrigen.Text));

                        end else begin

                                //Informo que el proceso no se pudo completar
                    			MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);

                        end;
                end;
			end;
		except
			on e: Exception do begin
				MsgBoxErr(MSG_ERROR_CANNOT_OPEN_LATE_DAYPASS_FILE, e.Message, 'Error', MB_ICONERROR);
			end;
		end;
    finally
        //Libero el stringlist
    	FPasesDiariosTardios.Free;
		//Deshabilita y Habilita los botones
    	FProcesando := False;
        HabilitarBotones;
    end;

end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 30/09/2005
Description :   Inicializa el form
Parameters : Titulo: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmRecepcionArchivoVentasBHTU.Inicializar(Titulo: AnsiString): boolean;
resourcestring
  	MSG_INIT_ERROR  = 'No se pudo inicializar la interfaz de Recepcion de Archivod e Ventas de BHTU';
    ERROR_P_GRAL    = 'Error al cargar el Par�metro General "%s".' ;
Const
    SERVIPAG_DIRECTORIO_PROCESADOS  = 'SERVIPAG_DIRECTORIO_PROCESADOS';
    SERVIPAG_DIRECTORIO_ENTRADA     = 'SERVIPAG_DIRECTORIO_ENTRADA';
begin
    Result := False;
    Caption := Titulo;
    CenterForm(Self);
    FErrors 	:= False;
    FProcesando := False;
    lblDetalle.Caption 	:= '';
  	// Obtiene el Directorio de Entrada de la Inter e
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, SERVIPAG_DIRECTORIO_ENTRADA, FBHTU_Directorio_Entrada)   then begin
        MsgBoxErr(MSG_INIT_ERROR, Format( ERROR_P_GRAL, [SERVIPAG_DIRECTORIO_ENTRADA]), Caption, MB_ICONSTOP);
        Exit;
    end;

    if ObtenerParametroGeneral(DMConnections.BaseCAC, SERVIPAG_DIRECTORIO_PROCESADOS, FBHTU_Directorio_Procesados) then begin
        MsgBoxErr(MSG_INIT_ERROR, Format( ERROR_P_GRAL, [SERVIPAG_DIRECTORIO_PROCESADOS]), Caption, MB_ICONSTOP);
        Exit;
    end;

    txtOrigen.Text := FBHTU_Directorio_Entrada;

    HabilitarBotones;

    Result := True;

end;



{******************************** Function Header ******************************
Function Name: txtOrigenButtonClick
Author : ndonadio
Date Created : 30/09/2005
Description : Abre y valida nombre del archivo de BHTU Vendidos
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoVentasBHTU.txtOrigenButtonClick(Sender: TObject);
resourcestring
    MSG_ERROR 						= 'Error';
	MSG_ERROR_FILE_DOES_NOT_EXIST 	= 'El archivo %s no existe';
    DAYPASS_FILES_FILTER			= 'Pases Diarios Tard�os|' + FILE_NAME_PREFIX + '*.MOK';
begin
    OpenDialog.InitialDir := FBHTU_Directorio_Entrada;
	OpenDialog.FileName := '';
    OpenDialog.Filter := DAYPASS_FILES_FILTER;
    if OpenDialog.Execute then begin
        txtOrigen.text:=UpperCase( OpenDialog.filename );

        if not FileExists( txtOrigen.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtOrigen.Text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;
    end;

    btnProcesar.Enabled := True
end;


{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 30/09/2005
Description :   Habilita O No Los Botones Y Otros Controles Dependiendo De
                El Valor De La Variable Fprocesando
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmRecepcionArchivoVentasBHTU.HabilitarBotones;
begin
    btnCancelar.Enabled := FProcesando;
    btnProcesar.Enabled := (not FProcesando) AND FileExists(txtOrigen.Text);
    btnSalir.Enabled := not FProcesando;
    txtOrigen.Enabled := not FProcesando;
    FErrors:= False;
    pnlAvance.Visible := FProcesando;
    lblDetalle.Caption := '';
    pbProgreso.Position := pbProgreso.Min;
end;


{-----------------------------------------------------------------------------
  Function Name: EliminarLineasEnBlanco
  Author:    flamas
  Date Created: 28/03/2005
  Description: Elimina las l�neas en Blanco que haya al final
  Parameters: var PaseDiario : TStringList
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmRecepcionArchivoVentasBHTU.EliminarLineasEnBlanco( var PaseDiario : TStringList);
var
	i : integer;
begin
	i := PaseDiario.Count -1;
    while (PaseDiario[i] = '') do begin
    	PaseDiario.Delete(i);
        Dec(i);
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: VerificarCantidadDeRegistrosPD
  Author:    flamas
  Date Created: 18/02/2005
  Description: Verifica que la cantidad de registros coincida con el
  				registro de control
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmRecepcionArchivoVentasBHTU.VerificarCantidadDeRegistros(FPasesDiarios : TStringList; sArchivo : string) : boolean;
resourcestring
	MSG_INVALID_RECORD_NUMBER 		= 'El n�mero de registros del %s ' +CRLF+
    									'no coincide con el registro de control';
    MSG_VERIFYING_CONTROL_REGISTER 	= 'Verficando registro de control del ';
var
	nLen : integer;
begin
	Result  := True;
    Exit;
    // Esto lo dejo asi porque hay diferencias entre las especificaciones
    // es decir, en la esp. habla del registro de control
    // pero no esta especificado y en los archivos de ejemplo no aparecen.
	nLen    := FPasesDiarios.Count;
	lblDetalle.Caption := MSG_VERIFYING_CONTROL_REGISTER + sArchivo;
	if (StrToIntDef(Copy(FPasesDiarios[nLen-1], 1, 10),0) <> nLen) then begin
    	MsgBox(Format(MSG_INVALID_RECORD_NUMBER, [sArchivo]), Caption, MB_ICONERROR);
		Result := False;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: AnalizarArchivoPasesDiariosTardios
  Author:    flamas
  Date Created: 17/03/2005
  Description:	Analiza el Archivo de Pases Diarios Tard�os
  Parameters: None
  Return Value: boolean
  Revision:  ndonadio, 30/09/2005 - cambie detalles. agregu� un try..finally
                                    para no repetir codigo
-----------------------------------------------------------------------------}
function TfrmRecepcionArchivoVentasBHTU.AnalizarArchivoPasesDiariosTardios: boolean;
resourcestring
    MSG_ANALIZING_LATE_DAYPASS_FILE		= 'Analizando Archivo Pases Diarios Tard�os - Cantidad de lineas : %d';
    MSG_LATE_DAYPASS_FILE_HAS_ERRORS 	= 'Error de Parseo: (L�nea : %d): ';
var
    nNroLineaScript, nLineasScript :integer;
    FDayPassRecord 	: TLateDayPassRecord;
    sParseError		: string;
    sErrorMsg   	: string;
begin
    Result := False;
	Screen.Cursor := crHourglass;
  	nLineasScript := FPasesDiariosTardios.Count - 1;
    lblDetalle.Caption := Format(MSG_ANALIZING_LATE_DAYPASS_FILE, [nLineasScript]);
    pbProgreso.Position := 0;
	pbProgreso.Max := FPasesDiariosTardios.Count - 1;
    try
        nNroLineaScript := 0;
        while ( nNroLineaScript < nLineasScript ) and
              ( not FCancelar ) and
              (	sErrorMsg = '' ) do begin

            if not ParseLateDayPassLine( FPasesDiariosTardios[nNroLineaScript], FDayPassRecord, sParseError ) then begin
                sErrorMsg := Format( MSG_LATE_DAYPASS_FILE_HAS_ERRORS, [nNroLineaScript] ) + sParseError;
                //MsgBox(sErrorMsg, Caption, MB_ICONERROR);
                FErrorLog.Add(sErrorMsg);
            end;

            Inc( nNroLineaScript );
            pbProgreso.Position := nNroLineaScript;
            Application.ProcessMessages;
        end;
    finally
    	result := ( not FCancelar ) and ( sErrorMsg = '' );
	    Screen.Cursor := crDefault;
        pbProgreso.Position := 0;
        Application.ProcessMessages;
    end;
end;



{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 23/12/2004
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TfrmRecepcionArchivoVentasBHTU.RegistrarOperacion(CodigoModulo: integer; sFileName: string) : boolean;
resourcestring
	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
var
    DescError : string;
begin
	result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, CodigoModulo, sFileName, UsuarioSistema, '', True, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);
   if not Result then
    	MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, Caption, MB_ICONERROR);
end;


{-----------------------------------------------------------------------------
  Function Name: CargarPasesDiariosTardios
  Author:    flamas
  Date Created: 17/03/2005
  Description: Carga los Pases Diarios Tard�os
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfrmRecepcionArchivoVentasBHTU.CargarPasesDiariosTardios : boolean;
resourcestring
    MSG_PROCESSING_LATE_DAYPASS_FILE		= 'Procesando archivo de Pases Diarios Tard�os - Cantidad de lineas : %d';
    MSG_ERROR_PROCESSING_LATE_DAYPASS_FILE	= 'Error procesando archivo de Pases Diarios Tard�os - Linea: %d';
	MSG_PROCESS_SUCCEDED 					= 'El proceso finaliz� con �xito';
	MSG_PROCESS_ENDED_WITH_ERRORS			= 'El proceso finaliz� con errores';
    MSG_ERROR                               = 'Error';
var
	nNroLineaScript, nLineasScript : integer;
    FDayPassRecord 		: TLateDayPassRecord;
	sParseError			: string;
    sErrorMsg   		: string;
    sDescripcionError 	: string;
begin
	Screen.Cursor := crHourglass;

	nLineasScript := FPasesDiariosTardios.Count - 1;
    lblDetalle.Caption  := Format(MSG_PROCESSING_LATE_DAYPASS_FILE, [nLineasScript]);
    pbProgreso.Position := 0;
    pbProgreso.Max      := FPasesDiariosTardios.Count - 1;


	FErrors:= False;
    nNroLineaScript := 0;

    while ( nNroLineaScript < nLineasScript ) and
    	  ( not FCancelar ) and
          (	sErrorMsg = '' ) do begin

    	if ParseLateDayPassLine( FPasesDiariosTardios[nNroLineaScript], FDayPassRecord, sParseError ) then begin
        	with FDayPassRecord, spAgregarDayPassTardio, Parameters do begin
				try
                    DMConnections.BaseCAC.BeginTrans;
                    ParamByName( '@Patente' ).Value						:= Patente;
                    ParamByName( '@Importe' ).Value						:= Monto * 100;
                    ParamByName( '@FechaVenta' ).Value					:= FechaVenta;
                    ParamByName( '@FechaRendicion' ).Value  			:= FechaRendicion;
                    ParamByName( '@Identificador' ).Value               := Identificador;
                    ParamByName( '@Categoria').Value                    := Categoria;
                    ParamByName( '@CodigoOperacion' ).Value	            := FCodigoOperacion;


                    spAgregarDayPassTardio.ExecProc;

                	sDescripcionError := Trim( VarToStr( ParamByName( '@DescripcionError' ).Value ));
                	if ( sDescripcionError <> '' ) then FErrors:= True;

                    DMConnections.BaseCAC.CommitTrans;

				except
                	on e: exception do begin
                        DMConnections.BaseCAC.RollbackTrans;
        				MsgBoxErr(Format(MSG_ERROR_PROCESSING_LATE_DAYPASS_FILE, [nNroLineaScript]), e.Message, MSG_ERROR, MB_ICONERROR);
                        sErrorMsg := MSG_ERROR_PROCESSING_LATE_DAYPASS_FILE;
                        FErrors:= True;
					end;
              	end;
            end;
        end;

        Inc( nNroLineaScript );
		pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

    lblDetalle.Caption := MSG_PROCESS_SUCCEDED;
    pbProgreso.Position := 0;

	Screen.Cursor := crDefault;
    result := ( not FCancelar ) and ( sErrorMsg = '' );
end;


{-----------------------------------------------------------------------------
  Function Name: ObtenerCantidadErroresInterfaz
  Author:    lgisuk
  Date Created: 21/06/2005
  Description: Obtengo la cantidad de errores contemplados que se produjeron
               al procesar el archivo
  Parameters: CodigoOperacion:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TfrmRecepcionArchivoVentasBHTU.ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
resourcestring
    MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
begin
    Cantidad := 0;
    try
        Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
        Result := true;
    except
       on e: Exception do begin
           MsgBoxErr(MSG_ERROR,e.Message, Caption, MB_ICONERROR);
           Result := False;
       end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarLog
  Author:    lgisuk
  Date Created: 01/07/2005
  Description: Actualizo el log al finalizar
  Parameters: CantidadErrores:integer
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TfrmRecepcionArchivoVentasBHTU.ActualizarLog(CantidadErrores:integer):boolean;
Resourcestring
    MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
Const
    STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
var
    DescError : string;
begin
    try
        Result := ActualizarLogOperacionesInterfaseAlFinal
                   (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
    except
        on e: Exception do begin
           MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
           Result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Genero el Reporte de Finalizacion de Proceso sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TfrmRecepcionArchivoVentasBHTU.GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
var
    F:TFRptDaypass;
begin
    Result:=false;
    try
        //muestro el reporte
        Application.createForm(TFRptDayPass,F);
        if not F.Inicializar('Day Pass',CodigoOperacionInterfase) then F.Release;
        Result:=true;
    except
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: ParseLateDayPassLine
  Author:    flamas
  Date Created: 17/03/2005
  Description: Parsea un al�nea del Pase diario Tard�o
  Parameters: sline : string; var LateDayPassRecord : TLateDayPassRecord;
                var sParseError : string
  Return Value: boolean
    Revision:   ndonadio, 30/09/2005
                Se mdifico para tomar en cuenta la estructura del archivo
                segun la especificacion "Clearing BHTU v.3"
-----------------------------------------------------------------------------}
function  TfrmRecepcionArchivoVentasBHTU.ParseLateDayPassLine(sline : string; var LateDayPassRecord : TLateDayPassRecord; var sParseError : string): boolean;
resourcestring
    MSG_PLATE_ERROR 		    = 'La patente es inv�lida.';
	MSG_SALES_DATE_ERROR 	    = 'La fecha de venta es inv�lida.';
	MSG_RENDITION_DATE_ERROR    = 'La fecha de rendici�n es inv�lida.';
	MSG_AMMOUNT_ERROR 		    = 'El monto es inv�lido.';
    MSG_FILE_TYPE_ERROR		    = 'El tipo de archivo a procesar no corresponde con el archivo seleccionado';
    MSG_CATEGORY_ERROR          = 'La categoria del BHTU es inv�lida';
begin
	result := False;
    with LateDayPassRecord do begin
		try
            // Patente
			sParseError 		:= MSG_PLATE_ERROR;
			Patente				:= Trim(Copy( sline, 1, 10)); if (Patente = '') then Exit;
            // Categoria
    		sParseError 		:= MSG_CATEGORY_ERROR;
			Categoria   		:= StrToIntDef(Trim(Copy( sline,  11, 1)), 0);
            // Monto
    		sParseError 		:= MSG_AMMOUNT_ERROR;
			Monto   			:= StrToIntDef(Trim(Copy( sline,  12, 10)), 0);
            // Fecha de Venta
			sParseError 		:= MSG_SALES_DATE_ERROR;
            FechaVenta 			:= EncodeDate(	StrToInt(Copy(sline, 22, 4)),
            									StrToInt(Copy(sline, 26, 2)),
                                                StrToInt(Copy(sline, 38, 2)));
            // Fecha de Rendicion
			sParseError 		:= MSG_RENDITION_DATE_ERROR;
            FechaVenta 			:= EncodeDate(	StrToInt(Copy(sline, 30, 4)),
            									StrToInt(Copy(sline, 34, 2)),
                                                StrToInt(Copy(sline, 36, 2)));

            // Identificador
			sParseError 		:= MSG_PLATE_ERROR;
			Identificador		:= Trim(Copy( sline, 38, 31)); if (Identificador = '') then Exit;

            sParseError := '';
            result := True;
        except
        	on exception do Exit;
        end;
    end;
end;




{******************************** Function Header ******************************
Function Name: ObtenerCantidadErroresInterfaz
Author : ndonadio
Date Created : 03/10/2005
Description : Pasa los errores de parseo al log de errores de interfases
Parameters : CodigoOperacion:integer;var Cantidad:integer
Return Value : boolean
*******************************************************************************}
procedure TfrmRecepcionArchivoVentasBHTU.AddParsingErrors(CodigoOperacion: Integer);
var
    i: integer;
begin
    if FErrors AND (FErrorLog.Count > 0) then begin
        i := 0;
        While i < FErrorLog.Count do begin
            if not AgregarErrorInterfase(DMConnections.BaseCAC, CodigoOperacion, FErrorLog[i]) then exit;
            inc(i)
        end;
    end;
end;

end.
