unit frmConfiguracionEnviosAutomaticos;

{
Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, dmConnection, DB, ADODB, utilproc, util, utildb,
  ExtCtrls;

type
  TformConfiguracionEnviosAutomaticos = class(TForm)
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    txtPlantillaNK: TNumericEdit;
    txtFirmaNK: TNumericEdit;
    Label5: TLabel;
    Label6: TLabel;
    txtPlantillaDetalle: TNumericEdit;
    Label7: TLabel;
    txtFirmaDetalle: TNumericEdit;
    btnGrabar: TButton;
    btnCancelar: TButton;
    spObtenerPlantillasActivas: TADOStoredProc;
    spGrabarPlantillasActivas: TADOStoredProc;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Label1: TLabel;
    Bevel3: TBevel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    txtPlantillaCliente: TNumericEdit;
    txtFirmaCliente: TNumericEdit;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnGrabarClick(Sender: TObject);
  private
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    { Private declarations }
  public
    function Inicializar: boolean;
  end;

var
  formConfiguracionEnviosAutomaticos: TformConfiguracionEnviosAutomaticos;

implementation

{$R *.dfm}

function TformConfiguracionEnviosAutomaticos.Inicializar: boolean;
begin
    result := true;
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
    
    try
        with spObtenerPlantillasActivas do begin
            Open;
            txtPlantillaNK.Value := iif(not eof and not fieldbyname('CodigoPlantillaFactura').isnull, fieldbyname('CodigoPlantillaFactura').AsInteger, 0);
            txtFirmaNK.Value := iif(not eof and not fieldbyname('CodigoFirmaFactura').isnull, fieldbyname('CodigoFirmaFactura').AsInteger, 0);
            txtPlantillaDetalle.Value := iif(not eof and not fieldbyname('CodigoPlantillaDetalleFacturacion').isnull, fieldbyname('CodigoPlantillaDetalleFacturacion').AsInteger, 0);
            txtFirmaDetalle.Value := iif(not eof and not fieldbyname('CodigoFirmaDetalleFacturacion').isnull, fieldbyname('CodigoFirmaDetalleFacturacion').AsInteger, 0);
            txtPlantillaCliente.Value := iif(not eof and not fieldbyname('CodigoPlantillaComprobante').isnull, fieldbyname('CodigoPlantillaComprobante').AsInteger, 0);
            txtFirmaCliente.Value := iif(not eof and not fieldbyname('CodigoFirmaComprobante').isnull, fieldbyname('CodigoFirmaComprobante').AsInteger, 0);
            Close;
        end;
    except
        on e: exception do begin
            MsgBoxErr('Error al inicializar el formulario', e.Message, 'Error', MB_ICONERROR);
        end;
    end;
end;

procedure TformConfiguracionEnviosAutomaticos.btnGrabarClick(Sender: TObject);
begin
    // verifico los datos...
    if txtPlantillaNK.Value = 0 then begin
        msgboxballoon('Error: debe especificar una plantilla.', 'Error', MB_ICONERROR, txtPlantillaNK);
        txtPlantillaNK.SetFocus;
        exit;
    end;
    if QueryGetValueInt(dmconnections.BaseCAC, 'SELECT dbo.EMAIL_VerificarCodigoPlantilla(' + inttostr(txtPlantillaNK.ValueInt) + ')') = 0 then begin
        msgboxballoon('Error: la plantilla especificada no existe.', 'Error', MB_ICONERROR, txtPlantillaNK);
        txtPlantillaNK.SetFocus;
        exit;
    end;
    if txtFirmaNK.Value = 0 then begin
        msgboxballoon('Error: debe especificar una firma.', 'Error', MB_ICONERROR, txtFirmaNK);
        txtFirmaNK.SetFocus;
        exit;
    end;
    if QueryGetValueInt(dmconnections.BaseCAC, 'SELECT dbo.EMAIL_VerificarCodigoFirma(' + inttostr(txtFirmaNK.ValueInt) + ')') = 0 then begin
        msgboxballoon('Error: la firma especificada no existe.', 'Error', MB_ICONERROR, txtFirmaNK);
        txtFirmaNK.SetFocus;
        exit;
    end;
    if txtPlantillaDetalle.Value = 0 then begin
        msgboxballoon('Error: debe especificar una plantilla.', 'Error', MB_ICONERROR, txtPlantillaDetalle);
        txtPlantillaDetalle.SetFocus;
        exit;
    end;
    if QueryGetValueInt(dmconnections.BaseCAC, 'SELECT dbo.EMAIL_VerificarCodigoPlantilla(' + inttostr(txtPlantillaDetalle.ValueInt) + ')') = 0 then begin
        msgboxballoon('Error: la plantilla especificada no existe.', 'Error', MB_ICONERROR, txtPlantillaDetalle);
        txtPlantillaDetalle.SetFocus;
        exit;
    end;
    if txtFirmaDetalle.Value = 0 then begin
        msgboxballoon('Error: debe especificar una firma.', 'Error', MB_ICONERROR, txtFirmaDetalle);
        txtFirmaDetalle.SetFocus;
        exit;
    end;
    if QueryGetValueInt(dmconnections.BaseCAC, 'SELECT dbo.EMAIL_VerificarCodigoFirma(' + inttostr(txtFirmaDetalle.ValueInt) + ')') = 0 then begin
        msgboxballoon('Error: la firma especificada no existe.', 'Error', MB_ICONERROR, txtFirmaDetalle);
        txtFirmaDetalle.SetFocus;
        exit;
    end;
    if QueryGetValueInt(dmconnections.BaseCAC, 'SELECT dbo.EMAIL_VerificarCodigoFirma(' + inttostr(txtFirmaCliente.ValueInt) + ')') = 0 then begin
        msgboxballoon('Error: la firma especificada no existe.', 'Error', MB_ICONERROR, txtFirmaCliente);
        txtFirmaCliente.SetFocus;
        exit;
    end;
    if QueryGetValueInt(dmconnections.BaseCAC, 'SELECT dbo.EMAIL_VerificarCodigoPlantilla(' + inttostr(txtPlantillaCliente.ValueInt) + ')') = 0 then begin
        msgboxballoon('Error: la firma especificada no existe.', 'Error', MB_ICONERROR, txtPlantillaCliente);
        txtPlantillaCliente.SetFocus;
        exit;
    end;
    // grabo las plantillas
    try
        with spGrabarPlantillasActivas do begin
            Parameters.Refresh;
            parameters.ParamByName('@CodigoPlantillaFactura').Value := txtPlantillaNK.ValueInt;
            parameters.ParamByName('@CodigoPlantillaDetalleFacturacion').Value := txtPlantillaDetalle.ValueInt;
            parameters.ParamByName('@CodigoFirmaFactura').Value := txtFirmaNK.ValueInt;
            parameters.ParamByName('@CodigoFirmaDetalleFacturacion').Value := txtFirmaDetalle.ValueInt;
            parameters.ParamByName('@CodigoPlantillaResumenTrimestral').Value := txtPlantillaNK.ValueInt;
            parameters.ParamByName('@CodigoFirmaResumenTrimestral').Value := txtPlantillaDetalle.ValueInt;
            parameters.ParamByName('@CodigoFirmaComprobante').Value := txtFirmaCliente.ValueInt;
            parameters.ParamByName('@CodigoPlantillaComprobante').Value := txtPlantillaCliente.ValueInt;
            Execproc;
        end;
        Close;
    except
        on e: exception do begin
            msgboxErr('Error al actualizar las plantillas.', e.message, 'Error', MB_ICONERROR);
        end;
    end;
end;

procedure TformConfiguracionEnviosAutomaticos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TformConfiguracionEnviosAutomaticos.btnCancelarClick(Sender: TObject);
begin
    close;
end;

end.
