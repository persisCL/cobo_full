{------------------------------------------------------------------------------
 File Name: FrmGenerarDebitosBcoChile.pas
 Author:    jjofre
 Date Created: 17 de Mayo de 2010
 Description:  Modulo de la interfaz Banco Chile - Generaci�n de Debitos

 Revision 1
 Author:    jjofre
 Date Created: 02 de Junio de 2010
 Description:  Se incorpora el Parametro General BANCODECHILE_CODIGO_EMPRESA
                al nombre del archivo, se modifican las siguientes funciones
                -Inicializar
                -ImgAyudaClick
                -buscaInterfacesSelect
 Revision 1
 Author:    jjofre
 Date Created: 16 de Junio de 2010
 Descripcion    : Se reemplaza en la funcion GenerarDebitosBancoChile los espcios
                 por el caracter '.'

 Firma        : SS-1028-NDR-20120418
 Description  : Quitar transaccionalidad del proceso completo y dejarlo solo cuando
                se actualiza el detalle de envio de comprobantes.
                Hacer la actualizacion en una tabla de paso y al final de proceso
                vaciar la tabla

Firma       :   SS_1110_CQU_20130522
Descripcion :   Corrige la conversi�n cuando los valores son demasiado altos.

Firma       :   SS_1147_CQU_20150106
Descripcion :   Se agrega la l�gica para VespucioSur

Firma       :   SS_1147_CQU_20150305
Descripcion :   Se relizan correcciones segun lo indicado por PVergara de VespucioSur

Firma       :   SS_1147_CQU_20150324
Descripcion :   La modificaci�n 8 del documento VS gatillo modificaciones.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       :   SS_1147_CQU_20150515
Descripcion :   El Documento de VS con el que se desarroll� estaba malo,
                el importe del registro es de largo 9 + 2 decimales por lo tanto
                el ev�o debe ser IGUAL que en CN, es decir, el importe tal cual
                viene de la BD. Se comentan las lineas agregadas para VS y se vuelve a lo anterior.

Firma       :   SS_1147_CQU_20150520
Descripcion :   Deja el Footer igual tanto para VS como CN.

Firma       :   SS_1310_CQU_20150617
Descripcion :   Se quita la constante TC_NOTA_COBRO y se cambia por el TipoComprobante
                que corersponde seg�n los datos obtenidos.

Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

-------------------------------------------------------------------------------}
unit FrmGenerarDebitosBcoChile;

interface

uses
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  ComunesInterfaces,
  FrmRptEnvioComprobantes,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, PeaTypes, UtilDB, DPSControls, VariantComboBox, StrUtils,
  BuscaTab, DmiCtrls, ImgList, DBClient;

type
  TFGeneracionDebitosBcoChile = class(TForm)
	Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    edDestino: TEdit;
    OpenDialog: TOpenDialog;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    lblFechaInterface: TLabel;
    spObtenerInterfasesEjecutadas: TADOStoredProc;
    edFecha: TBuscaTabEdit;
    buscaInterfaces: TBuscaTabla;
    spObtenerInterfasesEjecutadasFecha: TDateTimeField;
    spObtenerInterfasesEjecutadasDescripcion: TStringField;
    spObtenerInterfasesEjecutadasCodigoOperacionInterfase: TIntegerField;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    dblGrupos: TDBListEx;
    spObtenerUltimosGruposFacturados: TADOStoredProc;
    cdGrupos: TClientDataSet;
    ilImages: TImageList;
    dsGrupos: TDataSource;
    lblUltimosGruposFacturados: TLabel;
    spObtenerComprobantesBancoChile: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    function  buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);
    procedure edFechaChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure dblGruposDrawText(Sender: TCustomDBListEx;
    Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
    var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
    var DefaultDraw: Boolean);
    procedure dblGruposDblClick(Sender: TObject);
private
    { Private declarations }
    FCodigoOperacion : integer;
    FCodigoOperacionAnterior : variant;
	FDetenerImportacion: boolean;
    FDebitosTXT	: TStringList;
    FErrorMsg : string;
    FObservaciones : string;
    FBancoChile_Codigo_Convenio: AnsiString;
    FBancoChile_Codigo_Banco: AnsiString;
    FBancoChile_Directorio_Debitos: AnsiString;
    FMontoTotal     : Int64;
    FCantidadTotal  : Int64;
    FCodigoNativa : Integer;        // SS_1147_CQU_20150106
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  	Function RegistrarOperacion : boolean;
  	Function GenerarDebitosBancoChile : boolean;
    Function GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
    function CargarGrupos: Boolean;
    function GenerarListaGruposAIncluir(var Lista: string ; var Listaprocesos:string ): boolean;
    function ExistenGruposSeleccionados: boolean;
  public
	function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean; Reprocesar : boolean = False ) : Boolean;
	{ Public declarations }
  end;

var
   FGeneracionDebitosBcoChile: TFGeneracionDebitosBcoChile;

Const
	RO_MOD_INTERFAZ_ORDENES_DEBITO_BANCOCHILE				= 102;

resourcestring																				// SS_1147_CQU_20150106
    NOMINA_VS_PAC_BCO_CHILE = 'Cargos_%s_Chile';  // %s Debe ser la Fecha Formato DDMMAAAA	// SS_1147_CQU_20150106

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    jjofre
  Date Created: 17/05/2010
  Description: M�dulo de inicializaci�n de este formulario
-----------------------------------------------------------------------------}
function TFGeneracionDebitosBcoChile.Inicializar(txtCaption: ANSIString; MDIChild:Boolean; Reprocesar : boolean = False ) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : jjofre
    Date Created : 17/05/2010
    Description : Se obtienen los Parametros Generales que se utilizaran en el formulario
                  al inicializar y se verifica que los valores obtenidos sean validos.
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        BANCODECHILE_CODIGO_CONVENIO    = 'BancoChile_Codigo_Convenio';
        BANCODECHILE_DIRECTORIO_DEBITOS = 'BancoChile_Directorio_Debitos';
        BANCODECHILE_CODIGO_EMPRESA     = 'BancoChile_Codigo_Empresa';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Se obtiene el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BANCODECHILE_CODIGO_CONVENIO , FBancoChile_Codigo_Convenio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BANCODECHILE_CODIGO_CONVENIO;
                    Result := False;
                    Exit;
                end;
                //Se Verifica que sea v�lido
                if not (FBancoChile_Codigo_Convenio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + BANCODECHILE_CODIGO_CONVENIO;
                   Result := False;
                   Exit;
                end;

                //Se obtiene el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BANCODECHILE_CODIGO_EMPRESA , FBancoChile_Codigo_Banco) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BANCODECHILE_CODIGO_EMPRESA;
                    Result := False;
                    Exit;
                end;
                //Se Verifica que sea v�lido
                if not (FBancoChile_Codigo_Banco <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + BANCODECHILE_CODIGO_EMPRESA;
                   Result := False;
                   Exit;
                end;
             
                //Se obtiene el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BANCODECHILE_DIRECTORIO_DEBITOS , FBancoChile_Directorio_Debitos) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BANCODECHILE_DIRECTORIO_DEBITOS;
                    Result := False;
                    Exit;
                end;
                //Se Verifica que sea v�lido
                FBancoChile_Directorio_Debitos := GoodDir(FBancoChile_Directorio_Debitos);
                if  not DirectoryExists(FBancoChile_Directorio_Debitos) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FBancoChile_Directorio_Debitos;
                    Result := False;
                    Exit;
                end;

                // Obtengo la Concesionaria                         // SS_1147_CQU_20150106
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150106
            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
	MSG_DEBITS_PROCESSING = 'Procesamiento de d�bitos';
	MSG_DEBITS_REPROCESSING = 'Re-procesamiento de d�bitos';
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

 	if not MDIChild then begin
		FormStyle := fsNormal;
		Visible := False;
	end;

    CenterForm(Self);
	try
		DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and VerificarParametrosGenerales;
        if not Result then Exit;
	except
		on e: Exception do begin
			MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
			Exit;
		end;
	end;

   	lblFechaInterface.Visible := Reprocesar;
	edFecha.Visible := Reprocesar;

    if not CargarGrupos then begin
        Result := False;
        Exit;
    end;


    if not Reprocesar then begin
    	lblNombreArchivo.Top := 13;
		edDestino.Top := 32;
        FObservaciones := MSG_DEBITS_PROCESSING;
        btnProcesar.Enabled := DirectoryExists(FBancoChile_Directorio_Debitos);
        if FCodigoNativa = CODIGO_VS then                                                                           // SS_1147_CQU_20150106
            edDestino.Text := GoodFileName(                                                                         // SS_1147_CQU_20150106
                                            FBancoChile_Directorio_Debitos                                          // SS_1147_CQU_20150106
                                        +   Format(NOMINA_VS_PAC_BCO_CHILE, [FormatDateTime ( 'ddmmyyyy', Now )])   // SS_1147_CQU_20150106
                                        , 'txt')                                                                    // SS_1147_CQU_20150106
        else                                                                                                        // SS_1147_CQU_20150106
		edDestino.Text := GoodFileName(FBancoChile_Directorio_Debitos + 'PACC_' + PadL(FBancoChile_Codigo_Banco,3,'0') + '_' + FormatDateTime ( 'yyyy-mm-dd', Now ), 'txt' ); //REV.1
	end else begin
    	FObservaciones := MSG_DEBITS_REPROCESSING;
        spObtenerInterfasesEjecutadas.Parameters.ParamByName( '@CodigoModulo' ).Value := RO_MOD_INTERFAZ_ORDENES_DEBITO_BANCOCHILE;
        spObtenerInterfasesEjecutadas.Parameters.ParamByName( '@TraerTodas' ).Value := 1;
        spObtenerInterfasesEjecutadas.CommandTimeout := 5000;
        spObtenerInterfasesEjecutadas.Open;
	end;

    Caption := AnsiReplaceStr(txtCaption, '&', '');
	btnCancelar.Enabled := False;
	pnlAvance.Visible := False;
	lblReferencia.Caption := '';
	result := True;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : jjofre
Date Created : 20/05/2010
Description : Mensaje de Ayuda
*******************************************************************************}
procedure TFGeneracionDebitosBcoChile.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_DEBITOS         = ' ' + CRLF +
                          'El Archivo de Debitos es' + CRLF +
                          'utilizado por el ESTABLECIMIENTO para informar a BANCO DE CHILE' + CRLF +
                          'el detalle de los pagos a recaudar' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: PACC_(Codigo CEM)_YYYY_MM_DD.txt' + CRLF +
                          ' ';
begin
    if PnlAvance.visible = true then exit;
    MsgBoxBalloon(MSG_DEBITOS, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : jjofre
Date Created : 17/05/2010
Description :
*******************************************************************************}
procedure TFGeneracionDebitosBcoChile.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfacesProcess
  Author:    flamas
  Date Created: 08/07/2005
  Description: Muestro la lista de interfases procesadas
-----------------------------------------------------------------------------}
function TFGeneracionDebitosBcoChile.buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Result := True;
	with Tabla do
    	Texto := FieldByName('CodigoOperacionInterfase').AsString + ' - ' + FieldByName('Descripcion').AsString + ' - ' + FieldByName('Fecha').AsString;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfacesSelect
  Author:    flamas
  Date Created: 08/07/2005
  Description: Permito seleccionar una interfaz
  Parameters: Sender: TObject; Tabla: TDataSet
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosBcoChile.buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);

    {-----------------------------------------------------------------------------
      Function Name: InvertirFecha
      Author:    flamas
      Date Created: 09/12/2004
      Description: Invierte el formato de la fecha para generar el archivo de d�bbitos
      Parameters: sFecha : string
      Return Value: string
    -----------------------------------------------------------------------------}
    function InvertirFecha( sFecha : string ) : string;
    begin
        result := Copy( sFecha, 7, 4 ) + '-' + Copy( sFecha, 4, 2 ) + '-' + Copy( sFecha, 1, 2);
    end;

resourcestring
	DEBITS_FILENAME = 'PACC_' ;      //REV. 1
begin
	edFecha.Text := FormatDateTime( 'dd-mm-yyyy', Tabla.FieldByName('Fecha').AsDateTime);
    FCodigoOperacionAnterior := Tabla.FieldByName('CodigoOperacionInterfase').Value;
    if FCodigoNativa = CODIGO_VS then                                                                                                                   // SS_1147_CQU_20150106
        edDestino.Text := GoodFileName(Format(NOMINA_VS_PAC_BCO_CHILE, [FormatDateTime( 'ddmmyyyy', Tabla.FieldByName('Fecha').AsDateTime)]), 'txt')    // SS_1147_CQU_20150106
    else                                                                                                                                                // SS_1147_CQU_20150106
    edDestino.Text := GoodFileName( FBancoChile_Directorio_Debitos + DEBITS_FILENAME +  PadL(FBancoChile_Codigo_Banco,3,'0') + '_'  +InvertirFecha(edFecha.Text) , 'txt' );
end;

{-----------------------------------------------------------------------------
  Function Name: edFechaChange
  Author:    flamas
  Date Created: 08/07/2005
  Description: permito procesar si hay cargada una fecha
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosBcoChile.edFechaChange(Sender: TObject);
begin
   btnProcesar.Enabled := ( edFecha.Text <> '' );
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    jjofre
  Date Created: 17/05/2010
  Description: Registra la Operaci�n en el registro de Operaciones
-----------------------------------------------------------------------------}
function TFGeneracionDebitosBcoChile.RegistrarOperacion : boolean;
resourcestring
	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
	DescError : string;
begin
   result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_ORDENES_DEBITO_BANCOCHILE, ExtractFileName( edDestino.text ), UsuarioSistema, FObservaciones, False, edFecha.Visible, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError );
   if not result then
    	MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarDebitosBancoChile
  Author:    jjofre
  Date Created: 17/05/2010
  Description: Genera el archivo de d�bitos para el Banco de Chile
              -Se actualiza el log de interfase con el monto
               y cantidad de registros totales para el reporte
-----------------------------------------------------------------------------}
function TFGeneracionDebitosBcoChile.GenerarDebitosBancoChile : boolean;
resourcestring
	MSG_NO_PENDING_DEBITS 	 			= 'No hay d�bitos pendientes de env�o';
	MSG_COULD_NOT_CREATE_FILE			= 'No se pudo crear el archivo de d�bitos';
	MSG_COULD_NOT_GET_PENDING_DEBITS 	= 'No se pudieron obtener los d�bitos pendientes';
    MSG_GENERATING_DEBITS_FILE			= 'Generando archivo de d�bitos - Cantidad de d�bitos : %d ';
    MSG_PROCESSING			 			= 'Procesando...';
    MSG_OBTAINING_DATA			 		= 'Obteniendo datos a enviar al archivo...';
const
    DETALLE_CARGO = 'D';
    SQL_INSERT = 'INSERT INTO #DetalleComprobantesEnviados (CodigoOperacionInterfase,TipoComprobante,NumeroComprobante,ComprobanteMonto) VALUES (%d,''%s'',%d,%d)';  //SS-1028-NDR-20121418 //SS_1028_PDO_20120420
var
    ListaGrupos,Listaprocesos: string;
    Importe : Double;   // SS_1147_CQU_20150305
begin
	try
    	Screen.Cursor := crHourglass;
	  	result := False;
        lblReferencia.Caption := MSG_OBTAINING_DATA;
        Application.ProcessMessages;
        //Obtiene los comprobantes a Enviar
	    try
            spObtenerComprobantesBancoChile.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value := FCodigoOperacionAnterior;

            if not GenerarListaGruposAIncluir( ListaGrupos, Listaprocesos) then Exit ;

            spObtenerComprobantesBancoChile.Parameters.ParamByName( '@ListaGruposAIncluir' ).Value := ListaGrupos;
            spObtenerComprobantesBancoChile.Parameters.ParamByName( '@ListaProcesosAIncluir' ).Value := Listaprocesos;
            spObtenerComprobantesBancoChile.CommandTimeout := 5000;
    		spObtenerComprobantesBancoChile.Open;
            if spObtenerComprobantesBancoChile.IsEmpty then begin
               	FErrorMsg := MSG_NO_PENDING_DEBITS;
                MsgBox(MSG_NO_PENDING_DEBITS, Caption, MB_ICONWARNING);
        		Screen.Cursor := crDefault;
                Result := True;     //SS_1028_PDO_20120420
                Exit;
            end;

        except
        	on e: Exception do begin
                FErrorMsg := MSG_COULD_NOT_GET_PENDING_DEBITS;
                MsgBoxErr(MSG_COULD_NOT_GET_PENDING_DEBITS, e.Message, 'Error', MB_ICONERROR);
                Exit;
            end;
        end;

        //Genera el archivo
        lblReferencia.Caption := Format( MSG_GENERATING_DEBITS_FILE, [spObtenerComprobantesBancoChile.RecordCount] );
        pbProgreso.Position := 0;
        pnlAvance.Visible := True;
        lblReferencia.Caption := MSG_PROCESSING;

        with spObtenerComprobantesBancoChile do begin
            pbProgreso.Max := spObtenerComprobantesBancoChile.RecordCount;
            try
                while ( not Eof ) and ( not FDetenerImportacion ) do begin
                    if FCodigoNativa <> CODIGO_VS then begin                                                                // SS_1147_CQU_20150106

                    FDebitosTXT.Add(    PadL( trim(FieldByName( 'CodigoBancoSBEI' ).AsString), 3, '0' ) +                   {Codigo de Banco Asignado por la superintendencia}
                                        PadL( trim(FBancoChile_Codigo_Banco), 3, '0' ) +                                    {Codigo de la empresa, asignado por el banco}
                                        PadL( trim(FBancoChile_Codigo_Convenio), 3, '0' ) +                                 {Codigo de convenio Asignado por el Banco}
                                        DETALLE_CARGO +
                                        '0' +                                                                               {Detalle de Cargo 'D'}
                                        PadR( Copy( trim(FieldByName( 'NumeroConvenio' ).AsString), 4, 14 ), 22, ' ' ) +    {Identificador Servicio de la Empresa}
                                        PadL( trim(FieldByName( 'NumeroComprobante' ).AsString), 10, '0' ) +                {NumeroComprobante}
                                        PadL( trim(FieldByName( 'Importe' ).AsString ), 11, '0' ) +                         {Monto de Cargo a efectuar}
                                        FormatDateTime( 'yyyymmdd', FieldByName( 'FechaEmision' ).AsDateTime ) +
                                        FormatDateTime( 'yyyymmdd', FieldByName( 'FechaVencimiento' ).AsDateTime ) +
                                        PadL('.',10,'.'));
                        //FMontoTotal     := FMontoTotal + Trunc(FieldByName('Importe').AsFloat);                                                          // SS_1147_CQU_20150515  // SS_1147_CQU_20150305
                    end else begin                                                                                                                         // SS_1147_CQU_20150106
                        //Importe := Trunc(FieldByName( 'Importe' ).AsFloat) div 100;                                                                      // SS_1147_CQU_20150515  // SS_1147_CQU_20150305
                        FDebitosTXT.Add(                                                                                                                   // SS_1147_CQU_20150106
                                    PadL( trim(FieldByName( 'CodigoBancoSBEI' ).AsString), 3, '0' )                     // C�digo del Banco                // SS_1147_CQU_20150106
                                    + PadL( trim(FBancoChile_Codigo_Banco), 3, '0' )                                    // C�digo Empresa                  // SS_1147_CQU_20150106
                                    //+ PadL( trim(FBancoChile_Codigo_Convenio), 4, '0' )                                 // C�digo del Convenio (VS-BCO)  // SS_1147_CQU_20150305  // SS_1147_CQU_20150106
                                    + PadL( trim(FBancoChile_Codigo_Convenio), 3, '0' )                                 // C�digo del Convenio (VS-BCO)    // SS_1147_CQU_20150305
                                    + DETALLE_CARGO                                                                                                        // SS_1147_CQU_20150305
                                    //+ PadL( StrRight(trim(FieldByName( 'NumeroConvenio' ).AsString), 14 ), 15, '0' )    // N�mero del Convenio CN        // SS_1147_CQU_20150324  // SS_1147_CQU_20150305 // SS_1147_CQU_20150106
                                    + PadL( trim(FieldByName( 'NumeroConvenio' ).AsString), 15, '0' )                    // N�mero del Convenio CN         // SS_1147_CQU_20150106
                                    + PadL(' ',  18, ' ')                                                                                                  // SS_1147_CQU_20150106
                                    //+ PadL( trim(FieldByName( 'Importe' ).AsString ), 9, '0')                         // Monto Facturado                 // SS_1147_CQU_20150305  // SS_1147_CQU_20150106
                                    //+ PadL( FloatToStr(Importe), 9, '0')                                                // Monto Facturado               // SS_1147_CQU_20150515  // SS_1147_CQU_20150305
                                    //+ PadL('0',  2, '0')                                                                                                 // SS_1147_CQU_20150515  // SS_1147_CQU_20150106
                                    + PadL( trim(FieldByName( 'Importe' ).AsString ), 11, '0' )                                                            // SS_1147_CQU_20150515
                                    + FormatDateTime( 'yyyymmdd', FieldByName( 'FechaEmision' ).AsDateTime )            // Fecha de Emisi�n                // SS_1147_CQU_20150106
                                    + FormatDateTime( 'yyyymmdd', FieldByName( 'FechaVencimiento' ).AsDateTime )        // Fecha de Vencimiento            // SS_1147_CQU_20150106
                                    + PadL('.', 10, '.'));                                                              // Relleno de .                    // SS_1147_CQU_20150106
                        //FMontoTotal := FMontoTotal + Trunc(Importe);                                                                                     // SS_1147_CQU_20150515  // SS_1147_CQU_20150305
                    end;                                                                                                                                   // SS_1147_CQU_20150106
                    //FMontoTotal     := FMontoTotal + FieldByName('Importe').AsInteger;    // SS_1110_CQU_20130522
                    FMontoTotal     := FMontoTotal + Trunc(FieldByName('Importe').AsFloat); // SS_1147_CQU_20150515 // SS_1147_CQU_20150305   // SS_1110_CQU_20130522
                    inc( FCantidadTotal)  ;

                    //if not ActualizarDetalleComprobanteEnviado( DMConnections.BaseCAC,                                                        //SS-1028-NDR-20120418
                    //                                            FCodigoOperacion,                                                             //SS-1028-NDR-20120418
                    //                                            spObtenerComprobantesBancoChile.FieldByName('NumeroComprobante').Value,       //SS-1028-NDR-20120418
                    //                                            FErrorMsg,                                                                    //SS-1028-NDR-20120418
                    //                                            (spObtenerComprobantesBancoChile.FieldByName('Importe').Value div 100)        //SS-1028-NDR-20120418
                    //                                          ) then                                                                          //SS-1028-NDR-20120418
                    //    raise Exception.Create(FErrorMsg);                                                                                    //SS-1028-NDR-20120418

                    QueryExecute(DMConnections.BaseCAC,                                                                                         //SS-1028-NDR-20120418
                                  Format(SQL_INSERT,[ FCodigoOperacion,                                                                         //SS-1028-NDR-20120418
                                                      //TC_NOTA_COBRO,                                                                          // SS_1310_CQU_20150617  //SS-1028-NDR-20120418
                                                      spObtenerComprobantesBancoChile.FieldByName('TipoComprobante').AsString,                  // SS_1310_CQU_20150617
                                                      spObtenerComprobantesBancoChile.FieldByName('NumeroComprobante').AsInteger,               //SS_1028_PDO_20120420
                                                      //(spObtenerComprobantesBancoChile.FieldByName('Importe').AsInteger div 100)                              // SS_1110_CQU_20130522  //SS_1028_PDO_20120420
                                                      StrToInt64(FloatToStr((Trunc(spObtenerComprobantesBancoChile.FieldByName('Importe').AsFloat) div 100)))   // SS_1110_CQU_20130522
                                                    ]));                                                                                        //SS-1028-NDR-20120418


                    pbProgreso.StepIt;
                    Application.ProcessMessages;
                    Next;
                end;

                //if FCodigoNativa <> CODIGO_VS then begin                                                                                          // SS_1147_CQU_20150305  // SS_1147_CQU_20150106
                //Agrega la Linea de Totales al final del archivo de debitos
                FDebitosTXT.Add( '001' +                                              {C�digo del banco asignado; por la superintendencia}
                                 PadL( trim(FBancoChile_Codigo_Banco), 3, '0' ) +     {C�digo de la empresa, asignado por el banco (CEM)}
                                 PadL( trim(FBancoChile_Codigo_Convenio), 3, '0' ) +  {C�digo de convenio Asignado por el Banco}
                                 'T' +                                                {Tipo de registro 'T' : registro de totales}
                                 PadL(' ',33,' ')+                                    {Espacios}
                                 PadL(Trim(IntToStr(FMontoTotal)), 11,'0') +          {Monto total de cargos (moneda origen con dos decimales)}     // SS_1147_CQU_20150515  // SS_1147_CQU_20150305
                                 //IIf(FCodigoNativa = CODIGO_CN,                                                                                   // SS_1147_CQU_20150515  // SS_1147_CQU_20150305
                                 //   PadL(Trim(IntToStr(FMontoTotal)), 11,'0'),                                                                    // SS_1147_CQU_20150515  // SS_1147_CQU_20150305
                                 //   PadL(Trim(IntToStr(FMontoTotal)), 9,'0')) +                                                                   // SS_1147_CQU_20150515  // SS_1147_CQU_20150305
                                 //PadL(Trim(IntToStr(FCantidadTotal)),6,'0') +         {N�mero de registros de detalle}                            // SS_1147_CQU_20150305
                                 //IIf(FCodigoNativa = CODIGO_CN,                                                                                   // SS_1147_CQU_20150520  // SS_1147_CQU_20150305
                                 //   PadL(Trim(IntToStr(FCantidadTotal)),6,'0'),                                                                   // SS_1147_CQU_20150520  // SS_1147_CQU_20150305
                                 //   PadL(Trim(IntToStr(FCantidadTotal)),8,'0')) +                                                                 // SS_1147_CQU_20150520  // SS_1147_CQU_20150305
                                 PadL(Trim(IntToStr(FCantidadTotal)),6,'0') +         {N�mero de registros de detalle}                              // SS_1147_CQU_20150520
                                 'N' +                                                {Indica si la nomina es de reintentos S : n�mina con reintentos N : n�mina sin reintentos}
                                 '0' +                                                {N�mero de reintentos definidos por la empresa. Rango v�lido, entre 0 y 5. Este campo esta relacionado con el anterior}
                                 PadL('.',18,'.')                                     {Campo debe ser llenado con car�cter punto '.'}

                ) ;
                //end;                                                                                                                              // SS_1147_CQU_20150305  // SS_1147_CQU_20150106
            except
        	    on e: Exception do begin
                    MsgBoxErr(MSG_COULD_NOT_GET_PENDING_DEBITS, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        end;

        //Guarda el archivo
        if not FDetenerImportacion then begin
        	try
            	FDebitosTXT.SaveToFile( edDestino.Text );
                result := True;
            except
            	on e: Exception do begin
                	MsgBoxErr(MSG_COULD_NOT_CREATE_FILE, e.Message, 'Error', MB_ICONERROR);
                    FErrorMsg := MSG_COULD_NOT_CREATE_FILE;
                end;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    jjofre
  Date Created: 17/05/2010
  Description: Se Genera el Reporte de Finalizacion de Proceso
-----------------------------------------------------------------------------}
Function TFGeneracionDebitosBcoChile.GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
var FEnvio:TFRptEnvioComprobantes;
begin
    Result:=false;
    try
        Application.createForm(TFRptEnvioComprobantes,FEnvio);
        if not fEnvio.Inicializar('Comprobantes Enviados',CodigoOperacionInterfase) then fEnvio.Release;
        Result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    jjofre
  Date Created: 17/05/2010
  Description: Busca los d�bitos y Genera un TXT con los mismos
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosBcoChile.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    jjofre
      Date Created: 17/05/2010
      Description: Se Actualiza el log al finalizar
    -----------------------------------------------------------------------------}
    Function ActualizarLog:boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVACION = 'Finalizo OK!';
    var
         DescError : String;
    begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);
        except
            on e: Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

resourcestring
	MSG_PROCESS_SUCCEDED = 'El proceso finaliz� con �xito';
	MSG_PROCESS_COULD_NOT_BE_COMPLETED = 'El proceso no se pudo completar';
	MSG_FILE_ALREADY_EXISTS = 'El archivo ya existe.'#10#13'� Desea continuar ?';
    MSG_COULD_NOT_UPDATE_SENT_DEBITS = 'Los comprobantes no se pudieron actualizar';
    MSG_ERROR_DELETE_FILE = 'No pudo eliminar el archivo';
    MSG_ERROR = 'Error';
    MSG_SIN_GRUPOS_SELECCIONADOS = 'No se seleccionaron grupos de facturacion a incluir';
const
    SQL_BORRA_TEMPORAL  = 'IF OBJECT_ID(''tempdb..#DetalleComprobantesEnviados'') IS NOT NULL DROP TABLE #DetalleComprobantesEnviados'; //SS_1028_PDO_20120420
    SQL_CREA_TEMPORAL   = 'SELECT TOP 0 * INTO #DetalleComprobantesEnviados FROM DetalleComprobantesEnviados WITH (NOLOCK)';            //SS_1028_PDO_20120420
    SQL_CONSOLIDA_DATOS = 'INSERT INTO DetalleComprobantesEnviados SELECT * FROM #DetalleComprobantesEnviados';                         //SS_1028_PDO_20120420
begin
    // Verifica si el archivo existe ya
	if FileExists( edDestino.text ) then
        // En caso que exista le pregunta al operador si desea continuar
		if not(MsgBox(MSG_FILE_ALREADY_EXISTS, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then Exit;

    if not ExistenGruposSeleccionados  then begin
        MsgBoxBalloon(MSG_SIN_GRUPOS_SELECCIONADOS, MSG_ERROR, MB_ICONQUESTION , dblGrupos );
        Exit;
    end;

 	// Crea la listas de Debitos
    FDebitosTXT := TStringList.Create;

    btnCancelar.Enabled := True;
    btnProcesar.Enabled := False;
    BorderIcons := [];
    FErrorMsg := '';
    FCodigoOperacion := -1;
    FMontoTotal     := 0 ;
    FCantidadTotal  := 0 ;

    FDetenerImportacion := False;

    try
//        DMConnections.BaseCAC.BeginTrans;                                                     //SS-1028-NDR-20120418
//                                                                                              //SS-1028-NDR-20120418
//        if RegistrarOperacion and GenerarDebitosBancoChile then begin                         //SS-1028-NDR-20120418
//            //Acepto la Transaccion                                                           //SS-1028-NDR-20120418
//        	DMConnections.BaseCAC.CommitTrans;                                                  //SS-1028-NDR-20120418
//        end else begin                                                                        //SS-1028-NDR-20120418
//            FErrorMsg := 'Error Procesando comprobantes';                                     //SS-1028-NDR-20120418
//            //Si hubo errores hace un RollBack                                                //SS-1028-NDR-20120418
//            DMConnections.BaseCAC.RollbackTrans;                                              //SS-1028-NDR-20120418
//            try                                                                               //SS-1028-NDR-20120418
//                //Elimino el archivo                                                          //SS-1028-NDR-20120418
//                DeleteFile(edDestino.Text);                                                   //SS-1028-NDR-20120418
//            except                                                                            //SS-1028-NDR-20120418
//                on e: Exception do begin                                                      //SS-1028-NDR-20120418
//                    //Informo que no pudo eliminar el archivo                                 //SS-1028-NDR-20120418
//                    MsgBoxErr(MSG_ERROR_DELETE_FILE, e.Message, Self.caption, MB_ICONERROR);  //SS-1028-NDR-20120418
//                end;                                                                          //SS-1028-NDR-20120418
//            end;                                                                              //SS-1028-NDR-20120418
//            //Muestro un Cartel informando que no pudo actualizar los debitos                 //SS-1028-NDR-20120418
//            MsgBoxErr(MSG_COULD_NOT_UPDATE_SENT_DEBITS, FErrorMsg, MSG_ERROR, MB_ICONERROR);  //SS-1028-NDR-20120418
//        end;                                                                                  //SS-1028-NDR-20120418
//        //end;                                                                                //SS-1028-NDR-20120418


        if RegistrarOperacion then                                                              //SS-1028-NDR-20120418
        try                                                                                   //SS-1028-NDR-20120418

            QueryExecute(DMConnections.BaseCAC, SQL_BORRA_TEMPORAL);                            //SS_1028_PDO_20120420
            QueryExecute(DMConnections.BaseCAC, SQL_CREA_TEMPORAL);                             //SS_1028_PDO_20120420

            if GenerarDebitosBancoChile then                                                    //SS-1028-NDR-20120418
            try                                                                                 //SS-1028-NDR-20120418
                //DMConnections.BaseCAC.BeginTrans;                                             //SS_1385_NDR_20150922//SS-1028-NDR-20120418
                DMConnections.BaseCAC.Execute('BEGIN TRAN FrmGenerarDebitosBcoChile');          //SS_1385_NDR_20150922

                QueryExecute(DMConnections.BaseCAC, SQL_CONSOLIDA_DATOS);                       //SS_1028_PDO_20120420
                //DMConnections.BaseCAC.CommitTrans;                                            //SS_1385_NDR_20150922//SS-1028-NDR-20120418
                DMConnections.BaseCAC.Execute('COMMIT TRAN FrmGenerarDebitosBcoChile');					//SS_1385_NDR_20150922

            except
                on e: Exception do begin
                    //if DMConnections.BaseCAC.InTransaction then begin                           //SS_1385_NDR_20150922//SS_1028_PDO_20120420
                    //    DMConnections.BaseCAC.RollbackTrans;                                    //SS_1385_NDR_20150922//SS-1028-NDR-20120418
                    //end;                                                                        //SS_1385_NDR_20150922//SS_1028_PDO_20120420
                    DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION FrmGenerarDebitosBcoChile END');	    //SS_1385_NDR_20150922



                    FErrorMsg := 'Error al Actualizar la tabla DetalleComprobantesEnviados.';
                    MsgBoxErr(FErrorMsg, e.Message, MSG_ERROR, MB_ICONERROR);
                end;
            end                                                                                 //SS-1028-NDR-20120418
            else                                                                                //SS-1028-NDR-20120418
            begin                                                                               //SS-1028-NDR-20120418
              FErrorMsg := 'Error Procesando comprobantes';                                     //SS-1028-NDR-20120418
              try                                                                               //SS-1028-NDR-20120418
                DeleteFile(edDestino.Text);                                                     //SS-1028-NDR-20120418
              except                                                                            //SS-1028-NDR-20120418
                on e: Exception do begin                                                        //SS-1028-NDR-20120418
                  MsgBoxErr(MSG_ERROR_DELETE_FILE, e.Message, Self.caption, MB_ICONERROR);      //SS-1028-NDR-20120418
                end;                                                                            //SS-1028-NDR-20120418
              end;                                                                              //SS-1028-NDR-20120418
              MsgBoxErr(MSG_COULD_NOT_UPDATE_SENT_DEBITS, FErrorMsg, MSG_ERROR, MB_ICONERROR);  //SS-1028-NDR-20120418
            end;                                                                                //SS-1028-NDR-20120418
        finally
            QueryExecute(DMConnections.BaseCAC, SQL_BORRA_TEMPORAL);                            //SS_1028_PDO_20120420
        end;                                                                                   //SS-1028-NDR-20120418

        ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacion, FCantidadTotal , (FMontoTotal div 100), '', FErrorMsg);
        //Verifico si hubo errores
        if ( FErrorMsg = '' ) then begin

            ActualizarLog;
            MsgBox(MSG_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION );

            //Muestro el Reporte de Finalizacion del Proceso
			if (FCodigoOperacion > 0) and (not spObtenerComprobantesBancoChile.IsEmpty) then GenerarReportedeFinalizacion(FCodigoOperacion);    //SS_1028_PDO_20120420

        end else begin

            //Informa que el proceso no se pudo completar
        	MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);

        end;

    finally
		FreeAndNil( FDebitosTXT );
    	btnCancelar.Enabled := False;
    	btnProcesar.Enabled := True;
        Close;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    jjofre
  Date Created: 17/05/2010
  Description: Detiene el proceso
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosBcoChile.btnCancelarClick(Sender: TObject);
resourcestring
	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    jjofre
  Date Created: 17/05/2010
  Description: permite salir si no esta procesando
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosBcoChile.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
	CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    jjofre
  Date Created: 17/05/2010
  Description: Cierro el formulario
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosBcoChile.btnSalirClick(Sender: TObject);
begin
   Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    jjofre
  Date Created: 17/05/2010
  Description: Se libera de memoria
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosBcoChile.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFGeneracionDebitosBcoChile.dblGruposDblClick(Sender: TObject);
begin
    cdGrupos.Edit;
    if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
        cdGrupos.FieldByName('Seleccionado').AsBoolean := False;
    end else begin
        if ( cdGrupos.FieldByName('NoTerminados').AsInteger > 0 ) then begin
            ShowMessage('Este Proceso de Facturacion tiene comprobantes no terminados');
            cdGrupos.FieldByName('Seleccionado').AsBoolean := False;
        end else cdGrupos.FieldByName('Seleccionado').AsBoolean := True;
    end;
    cdGrupos.Post;
end;

procedure TFGeneracionDebitosBcoChile.dblGruposDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column.FieldName = 'Seleccionado' then begin
        if cdGrupos.FieldByName('Seleccionado').AsBoolean = True then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 1);
        end else if cdGrupos.FieldByName('Seleccionado').AsBoolean = False then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 0);
          end;
          DefaultDraw := False;
    end;

end;


{******************************** Function Header ******************************
Function Name: CargarGrupos
Author : jjofre
Date Created : 17/05/2010
Description: Obtiene los ultimos grupos facturados
*******************************************************************************}
function TFGeneracionDebitosBcoChile.CargarGrupos : Boolean;
begin
    Result := False;
    spObtenerUltimosGruposFacturados.Close;

    try
        cdGrupos.CreateDataSet;
        spObtenerUltimosGruposFacturados.CommandTimeout := 200;
        spObtenerUltimosGruposFacturados.Open;
        Result := not spObtenerUltimosGruposFacturados.IsEmpty;
        while not spObtenerUltimosGruposFacturados.Eof do begin
            // Insertamos nuestro registro
            cdGrupos.AppendRecord([False, spObtenerUltimosGruposFacturados.FieldByName('CodigoGrupoFacturacion').AsInteger,
              spObtenerUltimosGruposFacturados.FieldByName('FechaProgramadaEjecucion').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaCorte').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaEmision').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaVencimiento').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('NoTerminados').AsInteger,
              spObtenerUltimosGruposFacturados.FieldByName('NumeroProcesoFacturacion').AsInteger
              ]);
            spObtenerUltimosGruposFacturados.Next;
        end;
        spObtenerUltimosGruposFacturados.Close;
        cdGrupos.First;
    except
        on e: Exception do begin
            MsgBoxErr('Error obteniendo los �ltimos grupos facturados', e.Message, Caption, MB_ICONERROR);
            Result:= False;
        end;
    end;
end;



function TFGeneracionDebitosBcoChile.GenerarListaGruposAIncluir( var Lista:string ; var Listaprocesos:string ): boolean;
var
    ListaTemporal,ListaTemporal2 :TStringList;
    I: integer;

begin
    Result := False;
    ListaTemporal := TStringList.Create;
    ListaTemporal2 :=  TStringList.Create;


    try
        try
            ListaTemporal.Duplicates := dupIgnore; // la lista ignora los duplicados
            cdGrupos.First;
            while not cdGrupos.Eof do begin
                if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
                    ListaTemporal.Add( IntToStr(cdGrupos.FieldByName('GrupoFacturacion').AsInteger));
                    ListaTemporal2.Add( IntToStr(cdGrupos.FieldByName('NumeroProcesoFacturacion').AsInteger));
                end;
                cdGrupos.Next;
            end;
            cdGrupos.First;
            for I := 0 to ListaTemporal.Count - 1 do begin
                Lista := Lista + ListaTemporal[I];
                Listaprocesos := Listaprocesos + ListaTemporal2[I];
                if (I < (ListaTemporal.Count -1) ) then  begin // asi no me agrega una coma al final
                    Lista := Lista + ',';
                    Listaprocesos := Listaprocesos + ',';
                end;
            end;
            Result := True;
        except
            on E: Exception do begin
                MsgBoxErr('Error generando par�metro con lista de grupos para el store', e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
            ListaTemporal.Free;
            ListaTemporal2.Free;
    end;

end;


function TFGeneracionDebitosBcoChile.ExistenGruposSeleccionados: boolean;
begin
    Result := False;
    try
        cdGrupos.First;
        while not cdGrupos.Eof do begin
            if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
                Result := True;
                Break;
            end;
            cdGrupos.Next;
        end;
        cdGrupos.First;
    except
        on E: Exception do begin
            MsgBoxErr('Error contando grupos para la generaci�n ', e.Message, Caption, MB_ICONERROR);
        end;
    end;

end;


end.
