object FLogOperaciones: TFLogOperaciones
  Left = 44
  Top = 123
  Caption = 'Log de Operaciones Realizadas'
  ClientHeight = 457
  ClientWidth = 973
  Color = clBtnFace
  Constraints.MinHeight = 491
  Constraints.MinWidth = 981
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PAbajo: TPanel
    Left = 0
    Top = 416
    Width = 973
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object PDerecha: TPanel
      Left = 872
      Top = 0
      Width = 101
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object SalirBTN: TButton
        Left = 17
        Top = 8
        Width = 75
        Height = 25
        Caption = '&Salir'
        TabOrder = 0
        OnClick = SalirBTNClick
      end
    end
  end
  object Parriba: TPanel
    Left = 0
    Top = 0
    Width = 973
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object LDesde: TLabel
      Left = 31
      Top = 5
      Width = 64
      Height = 13
      Caption = 'Desde Fecha'
    end
    object LHasta: TLabel
      Left = 135
      Top = 5
      Width = 61
      Height = 13
      Caption = 'Hasta Fecha'
    end
    object Label1: TLabel
      Left = 239
      Top = 5
      Width = 35
      Height = 13
      Caption = 'M'#243'dulo'
    end
    object PAderecha: TPanel
      Left = 776
      Top = 0
      Width = 197
      Height = 49
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object AbrirBTN: TButton
        Left = 102
        Top = 16
        Width = 78
        Height = 25
        Caption = 'Consultar'
        TabOrder = 0
        OnClick = AbrirBTNClick
      end
    end
    object Edesde: TDateEdit
      Left = 31
      Top = 21
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = 37987.000000000000000000
    end
    object EHasta: TDateEdit
      Left = 135
      Top = 21
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 2
      Date = 37987.000000000000000000
    end
    object vcbModulo: TVariantComboBox
      Left = 239
      Top = 21
      Width = 259
      Height = 21
      Hint = 'Tipo de Interfaz'
      Style = vcsDropDownList
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Items = <>
    end
  end
  object DBListEx: TDBListEx
    Left = 0
    Top = 49
    Width = 973
    Height = 367
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 75
        Header.Caption = 'C'#243'digo Op.'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'CodigoOperacionInterfase'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'M'#243'dulo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'descripcion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Archivo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'NombreArchivo'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Usuario'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'Usuario'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'Observaciones'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Observaciones'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 115
        Header.Caption = 'Inicio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListExColumns0HeaderClick
        FieldName = 'FechaHoraRecepcion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 115
        Header.Caption = 'Finalizaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaHoraFinalizacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 65
        Header.Caption = 'Duraci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Diferencia'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        Width = 60
        Header.Caption = 'Informe'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clBlack
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = True
        FieldName = 'Reporte'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        Width = 70
        Header.Caption = 'Detalle'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = True
        FieldName = 'ReporteXML'
      end>
    DataSource = DataSource
    DragReorder = True
    ParentColor = False
    TabOrder = 2
    TabStop = True
    OnDrawText = DBListExDrawText
    OnLinkClick = DBListExLinkClick
  end
  object DataSource: TDataSource
    DataSet = SPObtenerLogOperacionesTBK
    Left = 192
    Top = 79
  end
  object SPObtenerLogOperacionesTBK: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    OnCalcFields = SPObtenerLogOperacionesTBKCalcFields
    ProcedureName = 'ObtenerLogOperacionesInterfase;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@DesdeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HastaFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ErrorDescription'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Size = 4000
        Value = Null
      end>
    Left = 216
    Top = 128
    object SPObtenerLogOperacionesTBKCodigoOperacionInterfase: TIntegerField
      FieldName = 'CodigoOperacionInterfase'
    end
    object SPObtenerLogOperacionesTBKCodigoModulo: TWordField
      FieldName = 'CodigoModulo'
    end
    object SPObtenerLogOperacionesTBKFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object SPObtenerLogOperacionesTBKNombreArchivo: TStringField
      FieldName = 'NombreArchivo'
      Size = 100
    end
    object SPObtenerLogOperacionesTBKUsuario: TStringField
      FieldName = 'Usuario'
      Size = 50
    end
    object SPObtenerLogOperacionesTBKObservaciones: TMemoField
      FieldName = 'Observaciones'
      BlobType = ftMemo
    end
    object SPObtenerLogOperacionesTBKReprocesamiento: TBooleanField
      FieldName = 'Reprocesamiento'
    end
    object SPObtenerLogOperacionesTBKFechaHoraRecepcion: TDateTimeField
      FieldName = 'FechaHoraRecepcion'
    end
    object SPObtenerLogOperacionesTBKUltimoRegistroEnviado: TIntegerField
      FieldName = 'UltimoRegistroEnviado'
    end
    object SPObtenerLogOperacionesTBKFechaHoraFinalizacion: TDateTimeField
      FieldName = 'FechaHoraFinalizacion'
    end
    object SPObtenerLogOperacionesTBKFinalizoOK: TBooleanField
      FieldName = 'FinalizoOK'
    end
    object SPObtenerLogOperacionesTBKDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 60
    end
    object SPObtenerLogOperacionesTBKTieneComprobantesEnviados: TBooleanField
      FieldName = 'TieneComprobantesEnviados'
      ReadOnly = True
    end
    object SPObtenerLogOperacionesTBKTieneComprobantesRecibidos: TBooleanField
      FieldName = 'TieneComprobantesRecibidos'
      ReadOnly = True
    end
    object SPObtenerLogOperacionesTBKTieneDayPassRecibidos: TBooleanField
      FieldName = 'TieneDayPassRecibidos'
      ReadOnly = True
    end
    object SPObtenerLogOperacionesTBKTieneReporteSerbanc: TBooleanField
      FieldName = 'TieneReporteSerbanc'
      ReadOnly = True
    end
    object SPObtenerLogOperacionesTBKTieneReporteBHTU: TBooleanField
      FieldName = 'TieneReporteBHTU'
      ReadOnly = True
    end
    object SPObtenerLogOperacionesTBKTieneReporteDebitos: TBooleanField
      FieldName = 'TieneReporteDebitos'
      ReadOnly = True
    end
    object SPObtenerLogOperacionesTBKTieneReporteCaptaciones: TBooleanField
      FieldName = 'TieneReporteCaptaciones'
      ReadOnly = True
    end
    object SPObtenerLogOperacionesTBKTieneReporteUnivSantander: TBooleanField
      FieldName = 'TieneReporteUnivSantander'
      ReadOnly = True
    end
    object SPObtenerLogOperacionesTBKDiferencia: TDateTimeField
      FieldName = 'Diferencia'
      ReadOnly = True
    end
    object SPObtenerLogOperacionesTBKReporte: TStringField
      FieldKind = fkCalculated
      FieldName = 'Reporte'
      Calculated = True
    end
    object SPObtenerLogOperacionesTBKReporteXML: TStringField
      FieldKind = fkCalculated
      FieldName = 'ReporteXML'
      Calculated = True
    end
    object SPObtenerLogOperacionesTBKCodigoReporte: TIntegerField
      FieldName = 'CodigoReporte'
    end
    object SPObtenerLogOperacionesTBKTieneReporteUnivBancoChile: TBooleanField
      FieldName = 'TieneReporteUnivBancoChile'
      ReadOnly = True
    end
    object blnfldSPObtenerLogOperacionesTBKTieneReporteRespuestaNovedadesCMR: TBooleanField
      FieldName = 'TieneReporteRespuestaNovedadesCMR'
      ReadOnly = True
    end
    object SPObtenerLogOperacionesTBKTieneRespuestasConsultasRNVM: TBooleanField
      FieldName = 'TieneRespuestasConsultasRNVM'
    end
    object SPObtenerLogOperacionesTBKTieneReporteMovimientosTBK: TBooleanField
      FieldName = 'TieneReporteMovimientosTBK'
    end
  end
end
