{-----------------------------------------------------------------------------
 File Name: RVMTypes.pas
 Author:    gcasais
 Date Created: 22/02/2005
 Language: ES-AR
 Description: Tipos de datos usados por el cliente RVM
-----------------------------------------------------------------------------}
unit RVMTypes;
interface
    uses Classes;

Type
    TLicensePlate = String;
    TVehicleNotes = String;

    TVehicleInformation = record
        Category: String;
        Manufacturer: String;
        Model: String;
        Color: String;
        EngineID: String;
        ChassisID: String;
        Serial: String;
        VINNumber: String;
        Year: String;
    end;

    TInsuranceInformation = record
        Entity: String;
        ContractID: String;
        ExpirationDate: String;
    end;

    TOwnerInformation = record
        OwnerID1: String;
        OwnerName1: String;
    end;

    TAddressInformation = record
        DoorNumber,
        Street,
        Letter,
        OtherInfo,
        Commune,
        Field6: String;
    end;

    TDomainConstraints = record
        Notas: Array[1..11] of string;
    end;

    TDomainRequests = record
        Nota1: String;
        Nota2: String;
    end;

    TOthersOwnersInformation = TOwnerInformation;

    TRVMInformation = record
        RequestedLicensePlate: TLicensePlate;
        VehicleInformation: TVehicleInformation;
        InsuranceInformation: TInsuranceInformation;
        OwnerInformation: TOwnerInformation;
        AddressInformation: TAddressInformation;
        VehicleNotes: TVehicleNotes;
        DomainConstraints: TDomainConstraints;
        DomainRequests: TDomainRequests;
        OthersOwnersInformation: TOthersOwnersInformation;
    end;

implementation
end.
