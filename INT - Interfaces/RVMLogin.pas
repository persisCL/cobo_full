{-----------------------------------------------------------------------------
  Function Name: GetUserName
  Author:    gcasais
  Date Created: 23/02/2005
  Description: M�dulo de Login para el servicio del RVM
  Parameters: None
  Return Value: String
-----------------------------------------------------------------------------}
unit RVMLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TfrmLogin = class(TForm)
    btnAceptar: TButton;
    btnCancelar: TButton;
    Bevel1: TBevel;
    txtUserName: TEdit;
    txtClave: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Image1: TImage;
    procedure FormShow(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    FUserName, FPassWord: String;
    function GetUserName: String;
    procedure SetUserName(const Value: String);
    function GetPassword: String;
    procedure SetPassword(const Value: String);
  public
    { Public declarations }
  published
    property UserName: String read GetUserName write SetUserName;
    property Password: String read GetPassword write SetPassword;
  end;



var
  frmLogin: TfrmLogin;

implementation

{$R *.dfm}

{ TfrmLogin }

function TfrmLogin.GetPassword: String;
begin
    Result := FPassword;
end;

function TfrmLogin.GetUserName: String;
begin
    Result := FUserName;
end;

procedure TfrmLogin.SetPassword(const Value: String);
begin
    if Value = FPassword then Exit;
    FPassword := Value;
end;

procedure TfrmLogin.SetUserName(const Value: String);
begin
    if Value = FUserName then Exit;
    FUserName := Value;
end;

procedure TfrmLogin.FormShow(Sender: TObject);
begin
    ActiveControl := txtUserName;
end;

procedure TfrmLogin.btnAceptarClick(Sender: TObject);
begin
    UserName := trim(txtUserName.Text);
    Password := trim(txtClave.Text);
end;

procedure TfrmLogin.btnCancelarClick(Sender: TObject);
begin
    close;
end;

end.
