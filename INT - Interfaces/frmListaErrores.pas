unit frmListaErrores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TformListaErrores = class(TForm)
    mem_ListaErrores: TMemo;
    btnCerrar: TButton;
    Label1: TLabel;
    lblaca: TLabel;
    Save: TSaveDialog;
    procedure lblacaClick(Sender: TObject);
    procedure btnCerrarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formListaErrores: TformListaErrores;

implementation

{$R *.dfm}

procedure TformListaErrores.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TformListaErrores.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

procedure TformListaErrores.lblacaClick(Sender: TObject);
begin
    if Save.Execute then begin
        mem_ListaErrores.Lines.SaveToFile(Save.FileName);
    end;
end;

end.
