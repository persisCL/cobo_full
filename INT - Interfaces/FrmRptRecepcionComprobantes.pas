{-------------------------------------------------------------------------------
 File Name: FrmRptRecepcionComprobantes.pas
 Author:    Flamas, lgisuk
 Date Created: 09/03/2005
 Language: ES-AR
 Description: Gestion de Interfases - Reporte de finalizacion del proceso
              Informa la cantidad de comprobantes recibidos , el importe total y
              otra informacion util para verificar si el proceso se realizo
              segun parametros normales.



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
unit FrmRptRecepcionComprobantes;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  UtilProc,                  //Mensajes
  //general
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd,
  ppReport, ppDB, ppComm, ppRelatv, ppTxPipe, DB, ppDBPipe, ADODB,
  ppParameter, ppModule, raCodMod, ppStrtch, ppSubRpt,RBSetup, ConstParametrosGenerales; //SS_1147_NDR_20140710

type
  TFRptRecepcionComprobantes = class(TForm)
    ppReporte: TppReport;
    ppParameterList1: TppParameterList;
    RBIListado: TRBInterface;
    SpObtenerReporterecepcionComprobantes: TADOStoredProc;
    ppDBPipelineComprobantesPagos: TppDBPipeline;
    DataSource: TDataSource;
    SpObtenerReportePagosRechazados: TADOStoredProc;
    DataSource1: TDataSource;
    ppDBPipelineComprobantesRechazados: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppModulo: TppLabel;
    pptFechaProcesamiento: TppLabel;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    ppFechaProcesamiento: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppLine3: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSubReportComprobantesPagos: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLine4: TppLine;
    ppLabel2: TppLabel;
    pptCantidadComprobantes: TppLabel;
    ppCantidadComprobantes: TppLabel;
    ppLine1: TppLine;
    ppTipoComprobante: TppLabel;
    ppMinComprobante: TppLabel;
    ppMaxComprobante: TppLabel;
    ppMinFechaEmision: TppLabel;
    ppMaxFechaEmision: TppLabel;
    ppLabel1: TppLabel;
    ppLine2: TppLine;
    ppDetailBand2: TppDetailBand;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText1: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppImporteTotal: TppLabel;
    pptImporteTotal: TppLabel;
    raCodeModule2: TraCodeModule;
    ppSubReportComprobantesRechazados: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppLine5: TppLine;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppCantidadRechazada: TppLabel;
    ppLine6: TppLine;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel11: TppLabel;
    ppLine7: TppLine;
    ppDetailBand3: TppDetailBand;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppSummaryBand3: TppSummaryBand;
    ppLabel9: TppLabel;
    ppImporteTotalRechazado: TppLabel;
    raCodeModule1: TraCodeModule;
    raCodeModule3: TraCodeModule;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FNombreReporte:string;
    FCodigoOperacionInterfase:integer;
    { Private declarations }
  public
    Function Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase:integer): Boolean;
    { Public declarations }
  end;

var
  FRptRecepcionComprobantes: TFRptRecepcionComprobantes;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 13/04/2005
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRptRecepcionComprobantes.Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase:integer): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
      ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
      try                                                                                                 //SS_1147_NDR_20140710
        ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
      except                                                                                              //SS_1147_NDR_20140710
        On E: Exception do begin                                                                          //SS_1147_NDR_20140710
          Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
          MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
          Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
        end;                                                                                              //SS_1147_NDR_20140710
      end;                                                                                                //SS_1147_NDR_20140710

    FNombreReporte:= NombreReporte;
    FCodigoOperacionInterfase:= CodigoOperacionInterfase;
    RBIListado.Execute;
    Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 13/04/2005
  Description:  Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptRecepcionComprobantes.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
resourcestring
	MSG_ERROR_AL_GENERAR_REPORTE = 'Error Al Generar Reporte';
    MSG_ERROR = 'Error';
var
    Config: TRBConfig;
begin
    //configuración del reporte
    Config := rbiListado.GetConfig;
    if Config.BinName <> '' then rbiListado.Report.PrinterSetup.BinName := Config.BinName;
    try

        //Abro la consulta de Comprobantes Pagos
        with SpObtenerReporteRecepcionComprobantes do begin

            Close;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := fcodigooperacioninterfase;
    		Open;

            //Asigno los valores a los campos del reporte
            ppmodulo.Caption:= Parameters.ParamByName('@Modulo').Value;
            ppFechaProcesamiento.Caption:= DateToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
            ppNombreArchivo.Caption:= copy(Parameters.ParamByName('@NombreArchivo').Value,1,40);
            ppUsuario.Caption:= Parameters.ParamByName('@Usuario').Value;
            ppCantidadComprobantes.caption:= inttostr(Parameters.ParamByName('@CantidadComprobantes').Value);
            ppImporteTotal.Caption:= Parameters.ParamByName('@ImporteTotal').Value;

        end;

        //Abro la consulta de Pagos Rechazados
        with SpObtenerReportePagosRechazados do begin

            Close;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := fcodigooperacioninterfase;
    		Open;

            //Asigno los valores a los campos del reporte
            ppCantidadRechazada.caption:= inttostr(Parameters.ParamByName('@CantidadComprobantes').Value);
            ppImporteTotalRechazado.Caption:= Parameters.ParamByName('@ImporteTotal').Value;
        end;


        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FNombreReporte;

        {if (not SpObtenerReporteRecepcionComprobantes.IsEmpty) or
        	(not SpObtenerReportePagosRechazados.IsEmpty) then  }

    except
        on E: exception do begin
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.


