object FormParametros: TFormParametros
  Left = 265
  Top = 273
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Par'#225'metros generales del Sistema'
  ClientHeight = 323
  ClientWidth = 585
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 6
    Top = 5
    Width = 574
    Height = 285
    ActivePage = Interfaces
    TabIndex = 0
    TabOrder = 0
    object Interfaces: TTabSheet
      Caption = 'Interfaces'
      ImageIndex = 2
      object Label1: TLabel
        Left = 17
        Top = 23
        Width = 179
        Height = 13
        Caption = 'Dir. pred. para Archivos de Interfaces:'
      end
      object btnDirArchivosImagenes: TSpeedButton
        Left = 517
        Top = 20
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = btnDirArchivosImagenesClick
      end
      object txtDirArchivosInterfaces: TEdit
        Left = 210
        Top = 20
        Width = 301
        Height = 21
        MaxLength = 255
        TabOrder = 0
        OnChange = ActualizarBotones
      end
    end
  end
  object btnCancelar: TDPSButton
    Left = 434
    Top = 295
    Width = 71
    Height = 24
    Cancel = True
    Caption = '&Cancelar'
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object btnAceptar: TDPSButton
    Left = 360
    Top = 295
    Width = 71
    Height = 24
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnAplicar: TDPSButton
    Left = 508
    Top = 295
    Width = 71
    Height = 24
    Cancel = True
    Caption = 'A&plicar'
    Enabled = False
    TabOrder = 3
    OnClick = btnAplicarClick
  end
  object Parametros: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'Parametros'
    Left = 2
    Top = 292
  end
end
