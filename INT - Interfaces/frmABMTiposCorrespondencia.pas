unit frmABMTiposCorrespondencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, VariantComboBox, DmiCtrls, DbList, Abm_obj, DB,
  ADODB, UtilDB, Util, DMConnection, UtilProc, RStrings, PeaProcs;

type
  TFormABMTiposCorrespondencia = class(TForm)
    AbmToolbar: TAbmToolbar;
    dblTipoCorrespondencia: TAbmList;
    GroupB: TPanel;
    lblDescripcion: TLabel;
    lblCodigoDocumentacionRespaldo: TLabel;
    lblFirma: TLabel;
    lblPlantilla: TLabel;
    txtDescripcion: TEdit;
    txtCodigoTipoCorrespondencia: TNumericEdit;
    vcbFirma: TVariantComboBox;
    vcbPlantilla: TVariantComboBox;
    Panel2: TPanel;
    EMAIL_TipoCorrespondencia: TADOTable;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure AbmToolbarClose(Sender: TObject);
    procedure dblTipoCorrespondenciaClick(Sender: TObject);
    procedure dblTipoCorrespondenciaDelete(Sender: TObject);
    procedure dblTipoCorrespondenciaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dblTipoCorrespondenciaEdit(Sender: TObject);
    procedure dblTipoCorrespondenciaInsert(Sender: TObject);
    procedure dblTipoCorrespondenciaRefresh(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
  private
    { Private declarations }      
    procedure CargarFirmas;
    procedure CargarPlantillas;
    procedure LimpiarCampos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializar: boolean;
  end;

var
  FormABMTiposCorrespondencia: TFormABMTiposCorrespondencia;

implementation

{$R *.dfm}       

procedure TFormABMTiposCorrespondencia.CargarFirmas;
var
    vTable: TADOTable;
begin
    vTable := TADOTable.Create(nil);
    try
        vTable.Connection := DMConnections.BaseCAC;
        vTable.TableName := 'EMAIL_Firmas';
        vTable.Open;

        vcbFirma.Clear;
        while not vTable.Eof do begin
            vcbFirma.Items.Add(vTable.FieldByName('Firma').AsString, vTable.FieldByName('CodigoFirma').AsInteger);
            vTable.Next;
        end;
    finally
        vTable.Close;
        vTable.Free;
    end;
end;

procedure TFormABMTiposCorrespondencia.CargarPlantillas;
var
    vTable: TADOTable;
begin
    vTable := TADOTable.Create(nil);
    try
        vTable.Connection := DMConnections.BaseCAC;
        vTable.TableName := 'EMAIL_Plantillas';
        vTable.Open;

        vcbPlantilla.Clear;
        while not vTable.Eof do begin
            vcbPlantilla.Items.Add(vTable.FieldByName('Asunto').AsString, vTable.FieldByName('CodigoPlantilla').AsInteger);
            vTable.Next;
        end;
    finally
        vTable.Close;
        vTable.Free;
    end;
end;

procedure TFormABMTiposCorrespondencia.HabilitarCampos;
var
    NuevoCodigo: Integer;
begin
    dblTipoCorrespondencia.Enabled := False;
    Notebook.PageIndex := 1;
    groupb.Enabled := True;
    if dblTipoCorrespondencia.Estado = Alta then begin
        NuevoCodigo := QueryGetValueInt(DMConnections.BaseCAC,
                        'SELECT ISNULL(MAX(CodigoTipoCorrespondencia),0) + 1' +
                        'FROM EMAIL_TipoCorrespondencia  WITH(NOLOCK)');
        txtCodigoTipoCorrespondencia.Value := NuevoCodigo;
    end;
    txtDescripcion.SetFocus;
end;        

procedure TFormABMTiposCorrespondencia.LimpiarCampos;
begin
    txtCodigoTipoCorrespondencia.Clear;
	txtDescripcion.Clear;
    vcbPlantilla.ItemIndex := -1;
    vcbFirma.ItemIndex := -1;
end;

procedure TFormABMTiposCorrespondencia.VolverCampos;
begin
    dblTipoCorrespondencia.Estado := Normal;
	dblTipoCorrespondencia.Enabled := True;
	dblTipoCorrespondencia.SetFocus;
    txtCodigoTipoCorrespondencia.Enabled := False;
    vcbPlantilla.ItemIndex := 0;
    vcbFirma.ItemIndex := 0;
	Notebook.PageIndex := 0;
    groupb.Enabled := False;

    dblTipoCorrespondenciaClick(dblTipoCorrespondencia);
end;

function TFormABMTiposCorrespondencia.Inicializar: Boolean;
begin
    try
        CargarFirmas;
        CargarPlantillas;
        if not OpenTables([EMAIL_TipoCorrespondencia]) then
            Result := False
        else begin
            Result := True;
            dblTipoCorrespondencia.Reload;
        end;
        Notebook.PageIndex := 0;
    except
        Result := False;
    end;
end;

procedure TFormABMTiposCorrespondencia.AbmToolbarClose(Sender: TObject);
begin
    Close;
end;

procedure TFormABMTiposCorrespondencia.BtnAceptarClick(Sender: TObject);
var
    ExisteCampo : Boolean;
begin
    if not ValidateControls([txtDescripcion, vcbPlantilla, vcbFirma],
      [trim(txtDescripcion.text) <> '', vcbPlantilla.ItemIndex > 0, vcbFirma.ItemIndex > 0],
      format(MSG_CAPTION_ACTUALIZAR,[FLD_TIPO_CORRESPONDENCIA]),
      [format(MSG_VALIDAR_DEBE_EL,[FLD_DESCRIPCION]),
        format(MSG_VALIDAR_DEBE_LA,['Plantilla']),
        format(MSG_VALIDAR_DEBE_LA,['Firma'])]) then Exit;

    ExisteCampo := (QueryGetValue(DMConnections.BaseCAC,
                    'SELECT TOP 1 Descripcion FROM EMAIL_TipoCorrespondencia  WITH (NOLOCK) ' +
                    'WHERE Descripcion = ''' + Trim(txtDescripcion.Text) + '''') <> '') and ((dblTipoCorrespondencia.Estado = Alta) or
                    ((dblTipoCorrespondencia.Estado = Modi) and (EMAIL_TipoCorrespondencia.FieldByName('Descripcion').AsString <> txtDescripcion.Text)));

    if ExisteCampo then begin
        MsgBoxBalloon(format(MSG_ERROR_EXISTE_EL, ['Tipo de Correspondencia ' + txtDescripcion.text]), format(MSG_CAPTION_ACTUALIZAR, [FLD_TIPO_CORRESPONDENCIA]), MB_ICONSTOP, txtDescripcion);
        Exit;
    end;

    if dblTipoCorrespondencia.Estado = Alta then
        txtCodigoTipoCorrespondencia.Value := QueryGetValueInt(DMConnections.BaseCAC,
                        'SELECT ISNULL(MAX(CodigoTipoCorrespondencia),0) + 1' +
                        'FROM EMAIL_TipoCorrespondencia  WITH(NOLOCK)');

    Screen.Cursor := crHourGlass;
	with EMAIL_TipoCorrespondencia do begin
        try
            if dblTipoCorrespondencia.Estado = Alta then Append
            else Edit;

            FieldByName('Descripcion').AsString	:= Trim(txtDescripcion.Text);
            FieldByName('CodigoPlantilla').AsInteger := vcbPlantilla.Value;
            FieldByName('CodigoFirma').AsInteger := vcbFirma.Value;
            FieldByName('FechaModificacion').AsDateTime := NowBase(DMConnections.BaseCAC);
            FieldByName('CodigoUsuarioModificacion').AsString := UsuarioSistema;
            if dblTipoCorrespondencia.Estado = Alta then begin
                FieldByName('CodigoTipoCorrespondencia').AsInteger := txtCodigoTipoCorrespondencia.ValueInt;
                FieldByName('FechaCreacion').AsDateTime := NowBase(DMConnections.BaseCAC);
                FieldByName('CodigoUsuarioAlta').AsString := UsuarioSistema;
            end;

            Post;
        except
            On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(format(MSG_ERROR_ACTUALIZAR, [FLD_TIPO_CORRESPONDENCIA]), e.message, format(MSG_CAPTION_ACTUALIZAR, [FLD_TIPO_CORRESPONDENCIA]), MB_ICONSTOP);
                Screen.Cursor := crDefault;
                Exit;
			end;
        end;
    end;
    VolverCampos;
    Screen.Cursor := crDefault;
end;

procedure TFormABMTiposCorrespondencia.BtnCancelarClick(Sender: TObject);
begin
    VolverCampos;
end;

procedure TFormABMTiposCorrespondencia.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormABMTiposCorrespondencia.dblTipoCorrespondenciaClick(
  Sender: TObject);
begin
    with EMAIL_TipoCorrespondencia do begin
        txtCodigoTipoCorrespondencia.Value := FieldByName('CodigoTipoCorrespondencia').AsInteger;
        txtDescripcion.Text	:= Trim(FieldByName('Descripcion').AsString);
        vcbPlantilla.ItemIndex := vcbPlantilla.Items.IndexOfValue(FieldByName('CodigoPlantilla').AsInteger);
        vcbFirma.ItemIndex := vcbFirma.Items.IndexOfValue(FieldByName('CodigoFirma').AsInteger);
    end;
end;

procedure TFormABMTiposCorrespondencia.dblTipoCorrespondenciaDelete(
  Sender: TObject);
resourcestring
    MSG_ERROR_RELATED_RECORDS = 'No se puede eliminar el Tipo de Correspondencia porque existen datos relacionados al mismo';
begin
    Screen.Cursor := crHourGlass;
    try
        If MsgBox(format(MSG_QUESTION_BAJA,[FLD_TIPO_CORRESPONDENCIA]), format(MSG_CAPTION_GESTION,[FLD_TIPO_CORRESPONDENCIA]), MB_YESNO + MB_ICONQUESTION) =
            IDYES then begin
            try
                EMAIL_TipoCorrespondencia.Delete;
            Except
                On E: Exception do begin
                    EMAIL_TipoCorrespondencia.Cancel;
                    MsgBoxErr(MSG_ERROR_RELATED_RECORDS, e.message, format(MSG_CAPTION_GESTION,[FLD_TIPO_CORRESPONDENCIA]), MB_ICONSTOP);
                end;
            end;
            dblTipoCorrespondencia.Reload;
        end;
        VolverCampos;
    finally
	    Screen.Cursor := crDefault;
    end;
end;             

procedure TFormABMTiposCorrespondencia.dblTipoCorrespondenciaDrawItem(
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
    with Sender.Canvas, Tabla do begin
        FillRect(Rect);
        TextOut(Cols[0], Rect.Top, Tabla.FieldByName('CodigoTipoCorrespondencia').AsString);
        TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldByName('Descripcion').AsString));
        TextOut(Cols[2], Rect.Top, Tabla.FieldByName('CodigoPlantilla').AsString);
        TextOut(Cols[3], Rect.Top, Tabla.FieldByName('CodigoFirma').AsString);
    end;
end;

procedure TFormABMTiposCorrespondencia.dblTipoCorrespondenciaEdit(
  Sender: TObject);
begin
    dblTipoCorrespondencia.Estado := Modi;
    HabilitarCampos;
end;

procedure TFormABMTiposCorrespondencia.dblTipoCorrespondenciaInsert(
  Sender: TObject);
begin
    LimpiarCampos;
    dblTipoCorrespondencia.Estado := Alta;
    HabilitarCampos;
end;

procedure TFormABMTiposCorrespondencia.dblTipoCorrespondenciaRefresh(
  Sender: TObject);
begin
	if dblTipoCorrespondencia.Empty then LimpiarCampos;
end;

procedure TFormABMTiposCorrespondencia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFormABMTiposCorrespondencia.FormShow(Sender: TObject);
begin
   	dblTipoCorrespondencia.Reload;
end;

end.
