{-----------------------------------------------------------------------------
 File Name: frmRecepcionRendicionesCMR.pas
 Author:    flamas
 Date Created: 23/06/2005
 Language: ES-AR
 Description: Recepci�n de Rendiciones CMR-Falabella
 Revision :1
     Author : vpaszkowicz
     Date : 20/03/2008
     Description :Agrego la cantidad de lineas del archivo y la suma de los
     montos recibidos en el log de operaciones.


Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

Autor       :   Claudio Quezada
Fecha       :   06-02-2015
Firma       :   SS_1147_CQU_20150316
Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato

Firma       :   SS_1147_CQU_20150305
Descripcion :   Se relizan correcciones segun lo indicado por PVergara de VespucioSur

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Autor       :   Claudio Quezada
Fecha       :   29-04-2015
Firma       :   SS_1147_CQU_20150428
Descripcion :   La unica diferencia entre CN y VS es el numero de convenio
                por lo que se comenta el codigo desarrollado con anterioridad
                y se cambia esa parte.
                Tambien se incrementa el numero de linea inicial ya que en VS
                la linea de control viene al principio y en CN al final.
                Se envia el TipoDocumento

Autor       :   Claudio Quezada
Fecha       :   18-05-2015
Firma       :   SS_1147_CQU_20150518
Descripcion :   Se corrige el nombre de la ventana (Caption) agregandole el " - CMR Falabella"

Autor       :   Claudio Quezada
Fecha       :   16-06-2015
Firma       :   SS_1314_CQU_20150908
Descripcion :   Homologar CMR de VS al de CN
				Se modifica el NombreArchivo en el bot�n Ayuda, ahora toma el CodigoAutopista que corresponde, se hace lo mismo para el filtro utilizado al querer cargar un archivo.
------------------------------------------------------------------------------}
unit frmRecepcionRendicionesCMR;

interface

uses
  //Recepcion de Rendiciones Falabella
  ComunesInterfaces,
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  FrmRptRecepcionRendiciones,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, PeaTypes, UtilDB, DPSControls, ppDB, ppTxPipe, StrUtils,
  ppParameter, ppModule, raCodMod, ppCtrls, ppBands, ppReport, ppPrnabl,
  ppClass, ppStrtch, ppSubRpt, ppCache, ppComm, ppRelatv, ppProd, UtilRB,
  RStrings, RBSetup,ppDBPipe,Math;

type
  TRendicionCMRRecord = record
    Monto : INT64;
    NumeroConvenio : string;
    NotaCobro : Int64;
  	FechaProceso : TDateTime;
    NumeroTarjeta : string;
    CodigoTransaccion : string;
    CodigoRespuesta : integer;
    GlosaRespuesta : string;
    TipoComprobante : string;   // SS_1147_CQU_20150428
  end;

  TfRecepcionRendicionesCMR = class(TForm)
    Bevel1: TBevel;
    btnAbrirArchivo: TSpeedButton;
    Label2: TLabel;
    edOrigen: TEdit;
    OpenDialog: TOpenDialog;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    SpObtenerResumenComprobantesRecibidos: TADOStoredProc;
    SpObtenerResumenComprobantesRecibidosMotivo: TStringField;
    SpObtenerResumenComprobantesRecibidosCantidad: TIntegerField;
    SpObtenerResumenComprobantesRecibidosMonto: TStringField;
    spActualizarRendicionCMR: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
    { Private declarations }
    FCodigoOperacion: integer;
  	FDetenerImportacion: boolean;
  	FRendicionesTXT: TStringList;
  	FErrorMsg: string;
    FMonto: Int64;
   	FCMR_Directorio_Origen_Rendiciones : Ansistring;
    FCMR_Directorio_Procesados: AnsiString;
    FLineas: integer;
    FCodigoNativa : Integer;  // SS_1147_CQU_20150316
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    FLineaDeInicio : Integer; // SS_1147_CQU_20150428
    FCMR_CodigodeAutopista : Ansistring;                                        // SS_1314_CQU_20150908
  	function  ParseCMRLine( sline : string; var CMRRecord : TRendicionCMRRecord; var sParseError : string ) : boolean;
  	function  AnalizarRendicionesTXT : boolean;
  	function  RegistrarOperacion : boolean;
    function  CargarRendicionesCMR : boolean;
    Function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
  public
	  { Public declarations }
  	function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
  end;

var
  fRecepcionRendicionesCMR: TfRecepcionRendicionesCMR;

Const
  RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_CMR = 53;

implementation


{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    FLamas
  Date Created: 22/06/2005
  Description: Inicializa el Formulario
  Parameters: txtCaption: ANSIString; MDIChild:Boolean
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesCMR.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;

{******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 05/08/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        CMR_DIRECTORIO_ORIGEN_RENDICIONES = 'CMR_Directorio_Origen_Rendiciones';
        CMR_DIRECTORIO_PROCESADOS         = 'CMR_Directorio_Procesados';
        CMR_CODIGODEAUTOPISTA             = 'CMR_CodigodeAutopista';            // SS_1314_CQU_20150908
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la Concesionaria Nativa                  // SS_1147_CQU_20150316
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150316

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_DIRECTORIO_ORIGEN_RENDICIONES , FCMR_Directorio_Origen_Rendiciones) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_DIRECTORIO_ORIGEN_RENDICIONES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FCMR_Directorio_Origen_Rendiciones := GoodDir(FCMR_Directorio_Origen_Rendiciones);
                if  not DirectoryExists(FCMR_Directorio_Origen_Rendiciones) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FCMR_Directorio_Origen_Rendiciones;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,CMR_DIRECTORIO_PROCESADOS, FCMR_Directorio_Procesados);

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_CODIGODEAUTOPISTA , FCMR_CodigodeAutopista) then begin    // SS_1314_CQU_20150908
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_CODIGODEAUTOPISTA;                                          // SS_1314_CQU_20150908
                    Result := False;                                                                                                // SS_1314_CQU_20150908
                    Exit;                                                                                                           // SS_1314_CQU_20150908
                end;                                                                                                                // SS_1314_CQU_20150908
            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR 	= 'Error al Inicializar';
    MSG_ERROR 		= 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Defino el modo en que se visualizara la ventana
  	if not MDIChild then begin
    		FormStyle := fsNormal;
    		Visible := False;
  	end;
    //Centro el form
  	CenterForm(Self);
  	try
        DMConnections.BaseCAC.Connected := True;
    		Result := DMConnections.BaseCAC.Connected and
                            VerificarParametrosGenerales;
  	except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
    //Resolver ac� lo que necesita este form para inicializar correctamente
   	Caption := AnsiReplaceStr(txtCaption, '&', '');
    if Pos('CMR', Caption) = 0 then Caption := Caption + ' - CMR Falabella';    // SS_1147_CQU_20150518
    btnCancelar.Enabled := False;
    btnProcesar.Enabled := False;
    pnlAvance.Visible := False;
    lblReferencia.Caption := '';
    if FCodigoNativa = CODIGO_VS    // SS_1147_CQU_20150428
    then FLineaDeInicio := 1        // SS_1147_CQU_20150428
    else FLineaDeInicio := 0;       // SS_1147_CQU_20150428
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfRecepcionRendicionesCMR.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_RENDICION          = ' ' + CRLF +
                          'El Archivo de Respuesta a Debitos' + CRLF +
                          'es enviado por FALABELLA al ESTABLECIMIENTO' + CRLF +
                          'por cada Archivo de Debitos recibido' + CRLF +
                          'y tiene por objeto dar a conocer al Establecimiento, ' + CRLF +
                          'el resultado del procesamiento de la informaci�n enviada.' + CRLF +
                          ' ' + CRLF +
                          //'Nombre del Archivo: CCNOMI04MMDDR.SSS' + CRLF +    // SS_1314_CQU_20150908
                          'Nombre del Archivo: CCNOMI%sMMDDR.SSS' + CRLF +      // SS_1314_CQU_20150908
                          ' ' + CRLF +
                          'Se utiliza para registrar los pagos de los' + CRLF +
                          'comprobantes aceptados en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    //MsgBoxBalloon(MSG_RENDICION, Caption, MB_ICONQUESTION, IMGAYUDA);         // SS_1314_CQU_20150908
    MsgBoxBalloon(Format(MSG_RENDICION, [FCMR_CodigodeAutopista]),              // SS_1314_CQU_20150908
                Caption, MB_ICONQUESTION, IMGAYUDA);                            // SS_1314_CQU_20150908
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TfRecepcionRendicionesCMR.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoClick
  Author:    FLamas
  Date Created: 22/06/2005
  Description: Abre el Archivo de Rendiciones, y verifica si no ha sido procesado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesCMR.btnAbrirArchivoClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerFiltro
      Author:    FLamas
      Date Created: 22/06/2005
      Description:	Arma el Filtro para la b�squeda de Archivos
      Parameters: var Filtro: AnsiString
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function ObtenerFiltro(var Filtro: AnsiString): Boolean;
    Const
        FILE_TITLE	   = 'Rendiciones CMR-Falabella|';
        //FILE_NAME 	   = 'CCNOMI04';    // SS_1314_CQU_20150908
        FILE_NAME      = 'CCNOMI%s';        // SS_1314_CQU_20150908
        FILE_APPEND	   = 'R';
        FILE_EXTENSION = '.001';
        //FILE_NAME_VS   = 'CCNOMI03';		// SS_1314_CQU_20150908 // SS_1147_CQU_20150316
    var                                     // SS_1314_CQU_20150908
        FiltroNombreArchivio : AnsiString;  // SS_1314_CQU_20150908
    begin
        Result:= False;
        try
            if FCodigoNativa = CODIGO_VS then begin                                         // SS_1147_CQU_20150316
                //Filtro := FILE_TITLE + FILE_NAME_VS + '*' + FILE_APPEND + FILE_EXTENSION;   // SS_1314_CQU_20150908   // SS_1147_CQU_20150316
                 FiltroNombreArchivio := Format(FILE_NAME, [FCMR_CodigodeAutopista]);         // SS_1314_CQU_20150908                                                                                       // SS_1314_CQU_20150908
            //end else                                                                        // SS_1314_CQU_20150908   // SS_1147_CQU_20150316
            end;                                                                              // SS_1314_CQU_20150908
    		//Filtro := FILE_TITLE + FILE_NAME + '*' + FILE_APPEND + FILE_EXTENSION;          // SS_1314_CQU_20150908
            Filtro := FILE_TITLE + FiltroNombreArchivio + '*' + FILE_APPEND + FILE_EXTENSION; // SS_1314_CQU_20150908
            
            Result:= True;
        except
        end;
    end;

resourcestring
    MSG_ERROR = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST = 'El archivo %s no existe';
  	MSG_ERROR_INVALID_FILENAME 		= 'El nombre del archivo %s es inv�lido';
  	MSG_FILE_ALREADY_PROCESSED 		= 'El archivo %s ya fue procesado'#10#13 + 'Desea reprocesarlo ?';
var
    Filtro: string;
begin
    //obtengo el filtro a aplicar a este tipo de archivos
    ObtenerFiltro(Filtro);

    Opendialog.InitialDir := FCMR_Directorio_Origen_Rendiciones;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if opendialog.execute then begin
        edOrigen.text:=UpperCase( opendialog.filename );

        if not FileExists( edOrigen.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

    	if  VerificarArchivoProcesado( DMConnections.BaseCAC, edOrigen.text ) then begin
    			if (MsgBox (Format(MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(edOrigen.text)]),Caption, MB_YESNO + MB_ICONQUESTION) = IDNO) then Exit;
    	end;

        if FCMR_Directorio_Procesados <> '' then begin
            if RightStr(FCMR_Directorio_Procesados,1) = '\' then  FCMR_Directorio_Procesados := FCMR_Directorio_Procesados+ ExtractFileName(edOrigen.text)
            else FCMR_Directorio_Procesados := FCMR_Directorio_Procesados + '\' + ExtractFileName(edOrigen.text);
        end;

        btnProcesar.Enabled := True;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ParseCMRLine
  Author:    FLamas
  Date Created: 22/06/2005
  Description: Parsea la linea de Rendicion de CMR
  Parameters: sline : string; var CMRRecord : TRendicionCMRRecord; var sParseError : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesCMR.ParseCMRLine( sline : string; var CMRRecord : TRendicionCMRRecord; var sParseError : string ) : boolean;
resourcestring
    MSG_ERROR_INVALID_AMMOUNT   		  = 'El monto del d�bito es inv�lido';
    MSG_ERROR_INVALID_INVOICE_NUMBER	= 'El n�mero de la nota de cobro es inv�lido';
    MSG_ERROR_DATE              		  = 'La fecha de proceso es inv�lida';
    MSG_ERROR_ANSWER					        = 'El c�digo de respuesta es inv�lido';
var
    sTipoComprobanteFiscal, TipoComprobante : string;  // SS_1147_CQU_20150316
    //NumeroComprobanteFiscal : Integer;               // SS_1147_CQU_20150428  // SS_1147_CQU_20150316
    NumeroComprobanteFiscal : Int64;                   // SS_1147_CQU_20150428
begin
  	result := False;
    with CMRRecord do begin
        try
            sParseError			:= MSG_ERROR_INVALID_AMMOUNT;
            Monto       		:= StrToInt64(Copy(sline, 1, 11));
            sParseError			:= MSG_ERROR_INVALID_AMMOUNT;
            //NumeroConvenio	:= '00100' + Copy(sline, 14, 12);               // SS_1147_CQU_20150428
            if FCodigoNativa = CODIGO_VS                                        // SS_1147_CQU_20150428
            then NumeroConvenio := Copy(sline, 14, 12)                          // SS_1147_CQU_20150428
            else NumeroConvenio	:= '00100' + Copy(sline, 14, 12);               // SS_1147_CQU_20150428
            //if ObtenerCodigoConcesionariaNativa=CODIGO_VS then                // SS_1147_CQU_20150428// SS_1147_CQU_20150316  //SS_1147Q_NDR_20141202[??]
            //if FCodigoNativa = CODIGO_VS then                                 // SS_1147_CQU_20150428  // SS_1147_CQU_20150316
            //begin                                                             // SS_1147_CQU_20150428  //SS_1147Q_NDR_20141202[??]
            //  NumeroConvenio:=Copy(NumeroConvenio,6,12);                      // SS_1147_CQU_20150428  //SS_1147Q_NDR_20141202[??]
            //end;                                                              // SS_1147_CQU_20150428  //SS_1147Q_NDR_20141202[??]
            sParseError			:= MSG_ERROR_INVALID_INVOICE_NUMBER;
            //NotaCobro			  := StrToInt64(Copy(sline, 34, 12));                                                   // SS_1147_CQU_20150316
            if FCodigoNativa = CODIGO_VS then begin                                                                     // SS_1147_CQU_20150316
                sTipoComprobanteFiscal  := Trim(Copy(sline, 34, 2));                                                    // SS_1147_CQU_20150316
                NumeroComprobanteFiscal := StrToInt64(Copy(sline, 36, 10));                                             // SS_1147_CQU_20150316
                //ObtenerDocumentoInterno(sTipoComprobanteFiscal, NumeroComprobanteFiscal, TipoComprobante, NotaCobro); // SS_1147_CQU_20150428 // SS_1147_CQU_20150316
                case StrToInt(sTipoComprobanteFiscal) of                                                                // SS_1147_CQU_20150428
                    33 : TipoComprobante := TC_FACTURA_AFECTA;                                                          // SS_1147_CQU_20150428
                    34 : TipoComprobante := TC_FACTURA_EXENTA;                                                          // SS_1147_CQU_20150428
                    39 : TipoComprobante := TC_BOLETA_AFECTA;                                                           // SS_1147_CQU_20150428
                    41 : TipoComprobante := TC_BOLETA_EXENTA;                                                           // SS_1147_CQU_20150428
                end;                                                                                                    // SS_1147_CQU_20150428
                NotaCobro   := NumeroComprobanteFiscal;                                                                 // SS_1147_CQU_20150428
            //end else NotaCobro			  := StrToInt64(Copy(sline, 34, 12));                                       // SS_1147_CQU_20150428 // SS_1147_CQU_20150316
            end else begin                                                                                              // SS_1147_CQU_20150428
                NotaCobro       := StrToInt64(Copy(sline, 34, 12));                                                     // SS_1147_CQU_20150428
                TipoComprobante := TC_NOTA_COBRO;                                                                       // SS_1147_CQU_20150428
            end;                                                                                                        // SS_1147_CQU_20150428

            sParseError			:= MSG_ERROR_ANSWER;
            CodigoRespuesta	:= StrToInt(Trim(Copy(sline, 57, 3)));
            sParseError			:= MSG_ERROR_DATE;
            FechaProceso		:= EncodeDate(	StrToInt(Copy(sline, 80, 4)), StrToInt(Copy(sline, 78, 2)), StrToInt(Copy(sline, 76, 2)));
            NumeroTarjeta		:= Trim(Copy(sline, 98,16));
            sParseError			:= '';
            result := True;
        except
        	  on exception do Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AnalizarRendicionesTXT
  Author:    FLamas
  Date Created: 22/06/2005
  Description: Antes de hacer nada Parsea todo el Archivo para verificar
  				que el parseo sea v�lido
  Parameters: None
  Return Value: boolean
  Revision : 1
      Author : vpaszkowicz
      Date : 11/03/2008
      Description : Agrego 2 caracteres mas al parseo del monto en el footer
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesCMR.AnalizarRendicionesTXT : boolean;
resourcestring
  	MSG_ANALIZING_RENDERINGS_FILE		    = 'Analizando Archivo de Rendiciones - Cantidad de lineas : %d';
    MSG_RENDERINGS_FILE_HAS_ERRORS 		  = 'El archivo de Rendiciones contiene errores'#10#13'L�nea : %d';
    MSG_ERROR_FOOTER_INVALID_AMMOUNT 	  = 'El monto en el registro de control es inv�lido';
    MSG_ERROR_FOOTER_INVALID_LINES 		  = 'El n�mero de l�neas en el registro de control es inv�lido';
    MSG_ERROR_FOOTER_AMMOUNT_DIFFERENCE	= 'El monto en el registro de control'#10#13 + 'no coincide con el calculado'#10#13#10#13 + 'Monto del Registro de Control: $ %d'#10#13 + 'Monto Calculado: $ %d';
	  MSG_ERROR_FOOTER_LINES_DIFFERENCE	  = 'La cantidad de l�neas en el registro de control'#10#13 + 'no coincide con la calculada'#10#13#10#13 + 'L�neas del Registro de Control: $ %d'#10#13 + 'L�neas Calculadas: $ %d';
    MSG_ERROR					 		              = 'Error';
var
  	//nNroLineaScript, nLineasScript :integer;
    nLineasScript :integer;
    FRendicionRecord : TRendicionCMRRecord;
    sParseError: string;
    nMontoCtrl: Int64;
    nLineasCtrl: integer;
    diferencia: int64;
begin
    Screen.Cursor := crHourglass;

    nLineasScript := FRendicionesTXT.Count - 1;
    nLineasScript := nLineasScript + FLineaDeInicio;    // SS_1147_CQU_20150428
    lblReferencia.Caption := Format( MSG_ANALIZING_RENDERINGS_FILE, [nLineasScript] );
    pbProgreso.Position := 0;
    pbProgreso.Max := FRendicionesTXT.Count - 1;
    pnlAvance.Visible := True;
    //FLineas := 0;                 // SS_1147_CQU_20150316
    //if FCodigoNativa = CODIGO_VS          // SS_1147_CQU_20150428 // SS_1147_CQU_20150316
    //then FLineas := 1                     // SS_1147_CQU_20150428 // SS_1147_CQU_20150316
    //else FLineas := 0;                    // SS_1147_CQU_20150428 // SS_1147_CQU_20150316
    FLineas := FLineas + FLineaDeInicio;    // SS_1147_CQU_20150428
    FMonto := 0;

    while ( FLineas < nLineasScript ) and
    	  ( not FDetenerImportacion ) and
          (	FErrorMsg = '' ) 	do begin

        	if ParseCMRLine( FRendicionesTXT[FLineas], FRendicionRecord, sParseError ) then
    	    			FMonto := FMonto + FRendicionRecord.Monto
            else begin
          			// Si encuentra un error termina
                FErrorMsg := Format( MSG_RENDERINGS_FILE_HAS_ERRORS, [FLineas] ) + #10#13 + sParseError;
                MsgBox(FErrorMsg, Caption, MB_ICONERROR);
		        		Screen.Cursor := crDefault;
              	result := False;
                Exit;
            end;

			Inc( FLineas );
        	pbProgreso.Position := FLineas;
          	Application.ProcessMessages;
    end;



  	//Valida el Monto en el Registro de Control
    try
    	  //nMontoCtrl:= StrToInt(Copy(FRendicionesTXT[nNroLineaScript], 9, 16));
        if FCodigoNativa = CODIGO_VS                                // SS_1147_CQU_20150316
        //then nMontoCtrl:= StrToInt(Copy(FRendicionesTXT[0], 9, 17))   // SS_1147_CQU_20150428 // SS_1147_CQU_20150316
        then nMontoCtrl:= StrToInt(Copy(FRendicionesTXT[0], 9, 18))     // SS_1147_CQU_20150428
        else                                                        // SS_1147_CQU_20150316
          nMontoCtrl:= StrToInt(Copy(FRendicionesTXT[FLineas], 9, 18));            

    except
      	on exception do begin
        		FErrorMsg := MSG_ERROR_FOOTER_INVALID_AMMOUNT;
          	MsgBox(FErrorMsg, Caption, MB_ICONERROR);
      			Screen.Cursor := crDefault;
          	result := False;
          	Exit;
        end;
    end;

  	// Valida la Cantidad de l�neas en el Registro de Control
    try
        if FCodigoNativa = CODIGO_VS                                    // SS_1147_CQU_20150316
        //then nLineasCtrl:= StrToInt(Copy(FRendicionesTXT[0], 26, 9))  // SS_1147_CQU_20150428  // SS_1147_CQU_20150316
        then nLineasCtrl:= StrToInt(Copy(FRendicionesTXT[0], 27, 8))    // SS_1147_CQU_20150428
        else                                                            // SS_1147_CQU_20150316
      	nLineasCtrl:= StrToInt(Copy(FRendicionesTXT[FLineas], 27, 8));
    except
      	on exception do begin
        		FErrorMsg := MSG_ERROR_FOOTER_INVALID_AMMOUNT;
          	MsgBox(FErrorMsg, Caption, MB_ICONERROR);
      			Screen.Cursor := crDefault;
          	result := False;
          	Exit;
        end;
    end;

  	// Compara el Monto Calculado con el de Control
    if (FMonto <> nMontoCtrl) then begin
        //Obtengo la diferencia
        Diferencia := 0;
        if FMonto > nMontoCtrl then Diferencia := Fmonto - nMontoCtrl;
        if nMontoCtrl > FMonto then Diferencia := nMontoCtrl - Fmonto;
        //Si la diferencia es relevante
        if Diferencia > 100 then
        begin
            //Informo que el monto calculado no coincide con el de control
            FErrorMsg := Format(MSG_ERROR_FOOTER_AMMOUNT_DIFFERENCE, [nMontoCtrl, FMonto]);
            MsgBox(FErrorMsg, Caption, MB_ICONERROR);
            Screen.Cursor := crDefault;
            result := False;
            Exit;
        end;
    end;

  	// Compara el N�mero de L�neas Calculadas con las de Control
    if (FRendicionesTXT.Count - 1 <> nLineasCtrl) then begin
        FErrorMsg := Format(MSG_ERROR_FOOTER_LINES_DIFFERENCE, [nLineasCtrl, FRendicionesTXT.Count - 1]);
        MsgBox(FErrorMsg, Caption, MB_ICONERROR);
        Screen.Cursor := crDefault;
        result := False;
        Exit;
    end;

  	result := not FDetenerImportacion;
  	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    FLamas
  Date Created: 22/06/2005
  Description: Registra la Operaci�n
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesCMR.RegistrarOperacion : boolean;
resourcestring
  	MSG_ERRORS 							          = 'Errores : ';
  	MSG_COULD_NOT_REGISTER_OPERATION 	= 'No se pudo registrar la operaci�n';
    MSG_DESCRIPTION						        = 'Procesamiento de D�bitos CMR';
    MSG_ERROR 							          = 'Error';
var
    DescError : string;
begin
  result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_CMR, ExtractFileName(edOrigen.text), UsuarioSistema, MSG_DESCRIPTION, True, False,  NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, FLineas-1, FMonto, DescError);
  if not result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: CargarRendicionesCMR
  Author:    FLamas
  Date Created: 22/06/2005
  Description: Carga las Rendiciones en la Base
  Parameters: None
  Return Value: boolean

	 Revision 1
	 Author: lgisuk
	 Date Created: 29/03/2006
	 Description: al store se le pasa como parametro si es reintento o no para 
	 que diferencie un reintento de un registro duplicado

  
-----------------------------------------------------------------------------}
function TfRecepcionRendicionesCMR.CargarRendicionesCMR : boolean;
resourcestring
    MSG_PROCESSING_RENDERINGS_FILE			  = 'Procesando archivo de Rendiciones - Cantidad de lineas : %d';
    MSG_ERROR_SAVING_INVOICE              = 'Error al Asentar la Operaci�n Comprobante: %d  ';
  	MSG_ERROR								              = 'Error';
var
  	nNroLineaScript, nLineasScript : integer;
    FRendicionRecord : TRendicionCMRRecord;
    sParseError : string;
    //
    Reintentos : Integer;
    mensajeDeadlock : AnsiString;
begin
  	Screen.Cursor := crHourglass;

  	nLineasScript := FRendicionesTXT.Count - 1;
    lblReferencia.Caption := Format( MSG_PROCESSING_RENDERINGS_FILE, [nLineasScript] );
    pbProgreso.Position := 0;
    pbProgreso.Max := FRendicionesTXT.Count - 1;
    pnlAvance.Visible := True;

    nNroLineaScript := 0;
    nLineasScript   := nLineasScript + FLineaDeInicio;      // SS_1147_CQU_20150428
    nNroLineaScript := nNroLineaScript + FLineaDeInicio;    // SS_1147_CQU_20150428

    while ( nNroLineaScript < nLineasScript ) and ( not FDetenerImportacion ) and (	FErrorMsg = '' ) do begin

        // Intentamos registrar el pago de un comprobante.
        Reintentos  := 0;

        //Obtengo los datos del a rendicion
        if ParseCMRLine( FRendicionesTXT[nNroLineaScript], FRendicionRecord, sParseError ) then begin

            while Reintentos < 3 do begin

                with FRendicionRecord, spActualizarRendicionCMR, Parameters do begin
                    try
                        Refresh;
                        ParamByName( '@CodigoOperacionInterfase' ).Value 	:= FCodigoOperacion;
                        ParamByName( '@FechaCargo' ).Value 					      := FechaProceso;
                        ParamByName( '@NumeroTarjeta' ).Value 				    := NumeroTarjeta;
                        ParamByName( '@NumeroConvenio' ).Value 				    := NumeroConvenio;
                        ParamByName( '@MontoPagado' ).Value					      := Monto;
                        ParamByName( '@CodigoRespuesta' ).Value 			    := CodigoRespuesta;
                        ParamByName( '@DescripcionRespuesta' ).Value 		  := GlosaRespuesta;
                        ParamByName( '@NumeroComprobante' ).Value 			  := NotaCobro;
                        ParamByName( '@Usuario' ).Value 					        := UsuarioSistema;
                        ParamByName( '@Reintento' ).Value 					      := Reintentos;
                        ParamByName( '@TipoComprobante' ).Value                 := TipoComprobante; // SS_1147_CQU_20150428
                        CommandTimeOut := 500;
                        ExecProc;
                        //1/4 de segundo entre registracion de pago y otro
                        sleep(25);
                        //Salgo del ciclo por Ok
                        Break;
                    except
                        on e: exception do begin
                            if (pos('deadlock', e.Message) > 0) then begin
                                mensajeDeadlock := e.message;
                                inc(Reintentos);
                                sleep(2000);
                            end else begin
                                FErrorMsg := Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]);
                                MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]), e.Message, MSG_ERROR, MB_ICONERROR);
                                Break;
                            end;
                        end;
                    end;
                end;

            end;

        end;

        //una vez que se reintento 3 veces x deadlock lo informo
        if Reintentos = 3 then begin
            FErrorMsg := Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]);
            MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [FRendicionRecord.NotaCobro]), mensajeDeadLock, MSG_ERROR, MB_ICONERROR);
        end;

        Inc( nNroLineaScript );
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

  	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;


{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 31/03/2006
  Description: Genero el Reporte de Finalizacion de Proceso. sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  TfRecepcionRendicionesCMR.GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
Const
    STR_TITLE = 'Comprobantes Recibidos';
var
    FRecepcion : TFRptRecepcionRendiciones;
begin
    Result:=false;
    try
        //muestro el reporte
        Application.CreateForm(TFRptRecepcionRendiciones, FRecepcion);
        if not fRecepcion.Inicializar(STR_TITLE, CodigoOperacionInterfase) then fRecepcion.Release;
        Result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    FLamas
  Date Created: 23/06/2005
  Description: Procesa el archivo de d�bitos de CMR
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesCMR.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/06/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name:  ObtenerResumen
      Author:    lgisuk
      Date Created: 31/03/2006
      Description:  Obtengo el resumen del proceso realizado
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    procedure ObtenerResumen (CodigoOperacionInterfase : Integer;Var CantRegs, Aprobados, Rechazados : Integer);
    resourcestring
      MSG_ERROR = 'Error';
      MSG_ERROR_QUERY = 'Error Obteniendo Resumen';
    begin
      try
          With spObtenerResumenComprobantesRecibidos, Parameters do begin
              Close;
              ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
              CommandTimeout := 500;
              Open;
              CantRegs := Parameters.ParamByName('@CantidadComprobantesRecibidos').Value;
              Aprobados := Parameters.ParamByName('@CantidadComprobantesAceptados').Value;
              Rechazados := Parameters.ParamByName('@CantidadComprobantesRechazados').Value;
              Close;
          end;
      except
          on E : Exception do begin
              MsgBoxErr(MSG_ERROR_QUERY, E.Message, MSG_ERROR, MB_ICONERROR);
          end;
      end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 01/07/2005
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog(CantidadErrores:integer):boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : string;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_SUCCEDED 					          = 'El proceso finaliz� con �xito';
    MSG_PROCESS_FINISH_WITH_ERRORS	        = 'El proceso finaliz� con errores.';
    MSG_PROCESS_COULD_NOT_BE_COMPLETED 		  = 'El proceso no se pudo completar';
    MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE 	= 'No se puede abrir el archivo de rendiciones';
    MSG_ERRROR_RENDERINGS_FILE_IS_EMPTY 	  = 'El archivo seleccionado est� vac�o';
    MSG_ERROR = 'Error';
Const
    PRESTO_RENDERINGS_DESCRIPTION 			    = 'Rendici�n Presto sobre Estado de D�bitos - ';  //Ok Revisado 16/03/05
Var
    CantRegs, Aprobados, Rechazados : integer;
    CantidadErrores: integer;
begin
 	// Crea las listas
	FRendicionesTXT := TStringList.Create;
	FErrorMsg := '';

	// Deshabilita los botones
	btnCancelar.Enabled := True;
	btnProcesar.Enabled := False;
	btnSalir.Enabled := False;
	btnAbrirArchivo.Enabled := False;
	edOrigen.Enabled := False;

	FDetenerImportacion := False;
	try
		try
     	//Lee el archivo del Rendiciones
			FRendicionesTXT.text:= FileToString(edOrigen.text);

			//Verifica si el Archivo Contiene alguna linea
			if ( FRendicionesTXT.Count = 0 ) then begin

                //Informo que el arhivo esta vacio
        				MsgBox(MSG_ERRROR_RENDERINGS_FILE_IS_EMPTY, Caption, MB_ICONERROR);

			end else begin

            	if AnalizarRendicionesTXT then begin
                    if RegistrarOperacion and
                    	CargarRendicionesCMR then begin

                            //Obtengo la cantidad de errores
                            ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);
                            //Actualizo el log al Final
                            ActualizarLog(CantidadErrores);
                            //Obtengo resumen del proceso
                            ObtenerResumen (FCodigoOperacion, CantRegs, Aprobados, Rechazados);
                            //Muestro Cartel de Finalzaci�n
                            if MsgProcesoFinalizado(MSG_PROCESS_SUCCEDED , MSG_PROCESS_FINISH_WITH_ERRORS , Caption , CantRegs , Aprobados , Rechazados) then begin
                                //Genero el reporte de finalizacion
                              	GenerarReportedeFinalizacion(FCodigoOperacion);
                            end;
                            //Muevo el archivo Procesado a otro directorio
                            MoverArchivoProcesado(Caption,edOrigen.Text, FCMR_Directorio_Procesados);

                    end else begin

                        //Informo que la operacion no se pudo completar
                        MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);

                    end;
                end;
			end;
		except
  			on e: Exception do begin
    				MsgBoxErr(MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE, e.Message, MSG_ERROR, MB_ICONERROR);
    				FErrorMsg := MSG_ERROR_CANNOT_OPEN_RENDERINGS_FILE;
  			end;
		end;
	finally
        //Libero el stringlist
    		FreeAndNil(FRendicionesTXT);
     		//Lo desactiva para poder Cerrar el Form
     		btnCancelar.Enabled := False;
        Close;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    FLamas
  Date Created: 23/06/2005
  Description: Cancela el Proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesCMR.btnCancelarClick(Sender: TObject);
resourcestring
	  MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    FLamas
  Date Created: 23/06/2005
  Description: Permito salir si no esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesCMR.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
  	CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    FLamas
  Date Created: 23/06/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesCMR.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    FLamas
  Date Created: 23/06/2005
  Description: Libera el formulario de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionRendicionesCMR.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
