object frmAsignarConsultasRNVMInfraccionesForm: TfrmAsignarConsultasRNVMInfraccionesForm
  Left = 0
  Top = 0
  Caption = 'Asignar Consultas RNVM a Infracciones'
  ClientHeight = 475
  ClientWidth = 711
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 16
  object lblTituloGrilla: TLabel
    Left = 0
    Top = 125
    Width = 711
    Height = 16
    Align = alTop
    Caption = 'Infracciones sin Consulta RNVM Asignada'
    ExplicitWidth = 244
  end
  object dblInfraccionesSinConsulta: TDBListEx
    Left = 0
    Top = 141
    Width = 711
    Height = 293
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Patente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Patente'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha Infraccion'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaInfraccion'
      end>
    DataSource = dsAsignarConsultasRNVMaInfracciones
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 434
    Width = 711
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      711
      41)
    object btnProcesar: TButton
      Left = 503
      Top = 6
      Width = 97
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Procesar'
      TabOrder = 0
      OnClick = btnProcesarClick
    end
    object btnSalir: TButton
      Left = 606
      Top = 8
      Width = 97
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Salir'
      TabOrder = 1
      OnClick = btnSalirClick
    end
  end
  object pnlParametros: TPanel
    Left = 0
    Top = 0
    Width = 711
    Height = 125
    Align = alTop
    TabOrder = 2
    DesignSize = (
      711
      125)
    object lblFechaMaxima: TLabel
      Left = 11
      Top = 15
      Width = 117
      Height = 13
      Caption = 'Fecha m'#225'xima infracci'#243'n'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblDirectorio: TLabel
      Left = 11
      Top = 39
      Width = 102
      Height = 13
      Caption = 'Directorio de Destino:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblNombreArchivo: TLabel
      Left = 11
      Top = 80
      Width = 92
      Height = 13
      Caption = 'Archivo a Generar :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object deFechaMaxima: TDateEdit
      Left = 134
      Top = 12
      Width = 97
      Height = 21
      AutoSelect = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Date = -693594.000000000000000000
    end
    object txtDirectorioDestino: TPickEdit
      Left = 9
      Top = 56
      Width = 540
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      Enabled = True
      MaxLength = 255
      TabOrder = 1
      EditorStyle = bteTextEdit
    end
    object edNombreArchivo: TEdit
      Left = 9
      Top = 96
      Width = 540
      Height = 24
      TabStop = False
      Anchors = [akLeft, akTop, akRight]
      ReadOnly = True
      TabOrder = 2
    end
  end
  object spAsignarConsultasRNVMaInfracciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AsignarConsultasRNVMaInfracciones;1'
    Parameters = <>
    Left = 624
    Top = 160
  end
  object dsAsignarConsultasRNVMaInfracciones: TDataSource
    DataSet = spAsignarConsultasRNVMaInfracciones
    Left = 656
    Top = 160
  end
end
