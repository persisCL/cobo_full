{-------------------------------------------------------------------------------
 File Name: FrmRptRecepcionCaptacionesCMR.pas
 Author:    Lgisuk
 Date Created: 17/09/2007
 Language: ES-AR
 Description: M�dulo de la interface Falabella - Reporte de Recepcion de Captaciones


Author		: Mcabello
 Firma		: SS_1140_MCA_20131118
 Descripcion: se elimina del reporte las bajas, debido a que solo seran solicitudes de altas.
 
Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
Unit FrmRptRecepcionCaptacionesCMR;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  Utildb,                    //querygetvalue
  UtilProc,                  //Mensajes
  //general
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup, ppModule, raCodMod, ppStrtch, ppSubRpt, ConstParametrosGenerales; //SS_1147_NDR_20140710

type
  TFRptRecepcionCaptacionesCMR = class(TForm)
	  RBIListado: TRBInterface;
    dsReporteResultadoCaptacionesCMR: TDataSource;
    ppDBCaptaciones: TppDBPipeline;
    //ppDBCaptacionesppField1: TppField;			//SS_1140_MCA_20131118
    //ppDBCaptacionesppField2: TppField;			//SS_1140_MCA_20131118
    //ppDBCaptacionesppField3: TppField;			//SS_1140_MCA_20131118
    //ppDBCaptacionesppField4: TppField;			//SS_1140_MCA_20131118
    //ppDBCaptacionesppField5: TppField;			//SS_1140_MCA_20131118
    ppReporteCaptaciones: TppReport;
    ppTitleBand1: TppTitleBand;
    ppLine7: TppLine;
    ppLabel32: TppLabel;
    ppImage3: TppImage;
    ppLabel33: TppLabel;
    ppLabel39: TppLabel;
    ppLabel40: TppLabel;
    ppLabel41: TppLabel;
    ppLine8: TppLine;
    ppLabel44: TppLabel;
    ppLabel45: TppLabel;
    ppLabel47: TppLabel;
    ppLabel49: TppLabel;
    //ppLabel54: TppLabel;			//SS_1140_MCA_20131118
    //ppLabel55: TppLabel;			//SS_1140_MCA_20131118
    //ppLabel56: TppLabel;				//SS_1140_MCA_20131118
    ppLine9: TppLine;
    ppLabel57: TppLabel;
    ppLine10: TppLine;
    ppLabel59: TppLabel;
    ppLabel60: TppLabel;
    ppLabel61: TppLabel;
    pplblFechaHora: TppLabel;
    pplblusuario: TppLabel;
    pplblArchivo: TppLabel;
    pplblNumeroProceso: TppLabel;
    pplblCantRegs: TppLabel;
    pplblTotalAltasBase: TppLabel;
    pplblTotalNoConfirmados: TppLabel;
    //pplblBajasValidas: TppLabel;			//SS_1140_MCA_20131118
    //pplblTotalBajasInvalidas: TppLabel;			//SS_1140_MCA_20131118
    //lblTotalBajasRecibidas: TppLabel;			//SS_1140_MCA_20131118
    pplblAltasValidas: TppLabel;
    pplblAltasInvalidas: TppLabel;
    lblTotalAltasRecibidas: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppsrResultados: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppLine4: TppLine;
    ppLabel15: TppLabel;
    ppLabel22: TppLabel;
    ppLabel24: TppLabel;
    ppLine6: TppLine;
    ppLabel8: TppLabel;
    RUT: TppLabel;
    ppLabel2: TppLabel;
    ppHeaderBand2: TppHeaderBand;
    ppDetailBand3: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText4: TppDBText;
    ppDBText7: TppDBText;
    ppDBText9: TppDBText;
    ppDBText2: TppDBText;
    raCodeModule3: TraCodeModule;
    ppParameterList2: TppParameterList;
    SPReporteResultadoCaptacionesCMR: TADOStoredProc;
    ppDBText3: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppLabel1: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FCodigoOperacionInterfase : Integer;
    { Private declarations }
  public
    Function Inicializar(CodigoOperacionInterfase : Integer) : Boolean;
    { Public declarations }
  end;

var
  FRptRecepcionCaptacionesCMR: TFRptRecepcionCaptacionesCMR;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 17/09/2007
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRptRecepcionCaptacionesCMR.Inicializar(CodigoOperacionInterfase : Integer) : Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage3.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    FCodigoOperacionInterfase := CodigoOperacionInterfase;
    Result := RBIListado.Execute;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 17/09/2007
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  17/09/2007
  Ya no obtiene los totales por diferencia
-----------------------------------------------------------------------------}
Procedure TFRptRecepcionCaptacionesCMR.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
resourcestring
    MSG_ERROR_GENERATING_REPORT = 'Error generando Reporte Final';
    MSG_ERROR = 'Error';
const
    TITLE_REPORT = 'Reporte de Captaciones';
begin
    try
        //Obtego el resultado del proceso
        with spReporteResultadoCaptacionesCMR, Parameters do begin
            Close;
            CommandTimeOut := 500;
            Refresh;
            ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
            Open;
            //Encabezado Pagina
            pplblFechaHora.Caption				  := FormatDateTime('dd-mm-yyyy HH:NN', ParamByName('@FechaProcesamiento').Value);
            pplblUsuario.Caption            := ParamByName('@Usuario').Value;
            pplblNumeroProceso.Caption			:= IntToStr(FCodigoOperacionInterfase);
            pplblArchivo.Caption				    := ParamByName('@ArchivoProcesado').Value;
            pplblCantRegs.Caption				    := IntToStr(ParamByName('@TotalRecibido').Value);
            //Datos del Proceso
            pplblTotalAltasBase.Caption 		:= IntToStr(ParamByName('@TotalMandatosConfirmados').Value);
            pplblTotalNoConfirmados.Caption := IntToStr(ParamByName('@TotalMandatosPendientes').Value);
            lblTotalAltasRecibidas.Caption 	:= IntToStr(ParamByName('@TotalAltasRecibidas').Value);
            pplblAltasValidas.Caption		:= IntToStr(ParamByName('@TotalAltasCorrectas').Value);
            pplblAltasInvalidas.Caption 		:= IntToStr(ParamByName('@TotalAltasErroneas').Value);
            //lblTotalBajasRecibidas.Caption	:= IntToStr(ParamByName('@TotalBajasRecibidas').Value);		//SS_1140_MCA_20131118
            //pplblBajasValidas.Caption		:= IntToStr(ParamByName('@TotalBajasCorrectas').Value);         //SS_1140_MCA_20131118
            //pplblTotalBajasInvalidas.Caption:= IntToStr(ParamByName('@TotalBajasErroneas').Value);        //SS_1140_MCA_20131118
        end;

        //Asigno el titulo
        rbiListado.Caption := TITLE_REPORT;
    except
        on E : Exception do begin
            Cancelled := True;
            //Informo que hubo un error al generar el reporte
            MsgBoxErr(MSG_ERROR_GENERATING_REPORT, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

end.
