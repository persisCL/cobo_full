{********************************** File Header ********************************
File Name   : frmInterfazImprentaInfractores.pas
Author      : rcastro
Date Created: 13/04/2005
Language    : ES-AR
Description : Notificaciones a infractores: interfaz para imprenta
********************************************************************************
 Revision History
********************************************************************************
 Author: rcastro
 Date Created: 04/05/2005
 Description: Cambio de estructura en infracciones, reimpresi�n de interfaz


 Revision		: 1
 Author			: Nelson Droguett Sierra
 Date			: 16-Junio-2010
 Description	: Se genera un archivo separado por concesionaria, las concesionarias
                	se pueden generar seleccionandolas de una lista de CheckBoxs.

 Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

  
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto
 *******************************************************************************}


unit frmInterfazImprentaInfractores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DMConnection, Util, UtilProc, UtilDB, StdCtrls, ExtCtrls, DB,
  ADODB, ListBoxEx, DBListEx, Buttons, DmiCtrls, DPSControls, ComCtrls,
  ConstParametrosGenerales, DateUtils, DBTables, BuscaTab, ComunesInterfaces,
  PeaTypes, Grids, DBGrids, PeaProcs, Mask, DBCtrls, Validate, DateEdit,
  CheckLst, VariantComboBox,Math;

type
  TFormInterfazImprentaInfractores = class(TForm)
	Bevel1: TBevel;
	Bevel2: TBevel;
	btnAnterior: TButton;
	btnBrowseForFolder: TSpeedButton;
	btnCancelar: TButton;
	btnFinalizar: TButton;
	btnLimpiar: TButton;
	btnObtenerInfractores: TButton;
	btnSalir: TButton;
	btnSiguiente: TButton;
	btOperacionesInterfaz: TBuscaTabla;
	dblInfractores: TDBListEx;
	dsListaInfractores: TDataSource;
	edProceso: TBuscaTabEdit;
	Label1: TLabel;
	Label2: TLabel;
	Label3: TLabel;
	Label4: TLabel;
	Label5: TLabel;
	Label6: TLabel;
	Label7: TLabel;
	Label8: TLabel;
	Label9: TLabel;
	labelProgreso: TLabel;
	lblDescri: TLabel;
	nb: TNotebook;
	pbProgreso: TProgressBar;
	pnlprogreso: TPanel;
	qryEliminarTemporales: TADOQuery;
	spActualizarOperacionEnCartasAInfractores: TADOStoredProc;
	spAgruparListaInterfazInfractores: TADOStoredProc;
	spGenerarListaInterfazInfractores: TADOStoredProc;
	spObtenerOperacionesInterfazInfractores: TADOStoredProc;
	txt_FechaDesde: TDateEdit;
	txt_FechaEnvio: TDateEdit;
	txt_FechaHasta: TDateEdit;
	txtUbicacion: TEdit;
    CLB_Concesionarias: TVariantCheckListBox;
    Label10: TLabel;
    spResumirListaInterfazInfractores: TADOStoredProc;
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnSalirClick(Sender: TObject);
	procedure btnSiguienteClick(Sender: TObject);
	procedure btnAnteriorClick(Sender: TObject);
	procedure btnObtenerInfractoresClick(Sender: TObject);
	procedure btnBrowseForFolderClick(Sender: TObject);
	procedure btnFinalizarClick(Sender: TObject);
	procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure btnLimpiarClick(Sender: TObject);
    function btOperacionesInterfazProcess(Tabla: TDataSet;var Texto: String): Boolean;
    procedure btOperacionesInterfazSelect(Sender: TObject;Tabla: TDataSet);
    procedure btnCancelarClick(Sender: TObject);
    procedure dblInfractoresColumns0HeaderClick(Sender: TObject);
	procedure CargarCheckListBoxConcesionarias(Conn: TADOConnection; CheckListBox: TVariantCheckListBox );
  private
	FDestino, FNombreArchivo, FNombreArchivoControl: AnsiString;
	FOutFile: TextFile;
	FCancel, FEnProceso: Boolean;
	FCantidadCartas,
    FCodigoOperacionInterfaseOriginal,
	FCodOperacionesInterfase: Integer;
    FechaGeneracionPrevia: TDateTime;
	FFileBuffer: TStringList;
    FReimpresion: Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	function GenerarArchivo:Boolean;
	function ActualizarCartasAInfractores(CodigoConcesionaria:Integer): boolean;
	function CrearArchivo(Archivo: String; var DescriError: AnsiString):Boolean;
	function  RegistrarOperacion : boolean;
    //Rev.1 / 16-Junio-2010 / Nelson Droguett Sierra-----------------------------------
	procedure CrearNombresArchivos(Path: String; CodigoConcesionaria: Integer);
	procedure HabilitarControles(Habilitado: Boolean);
	function EscribirArchivoControl: Boolean;
  public
	function Inicializar: Boolean;
  end;

resourcestring
	MSG_SELECT_LOCATION     = 'Seleccione una ubicaci�n para el archivo';
	MSG_CANNOT_CREATEFILE   = 'Error al crear el archivo';
	MSG_ERROR               = 'Error';

Const
	SEPARADOR               = '|';
	FILE_EXTENSION          = '.txt';
    //Rev.1 / 16-Junio-2010 / Nelson Droguett Sierra
	FILE_PREFIX				= 'INFR-%s-';
	CONTROL_FILE_PREFIX     = 'INFR-%s-CONTROL-';

var
  FormInterfazImprentaInfractores: TFormInterfazImprentaInfractores;


implementation

uses FrmMain;

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: Inicializar
Author       : rcastro
Date Created : 13/04/2005
Description  : Inicializa el formulario
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TFormInterfazImprentaInfractores.Inicializar: Boolean;
resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR_CARGANDO_PARAMETROS = 'Error cargando los par�metros generales.';
	MSG_PARAMETER_NOT_EXISTS = 'El par�metro %s no existe en la base de datos.';
    OUTPUT_DIRECTORY = 'DIR_SALIDA_IMPRENTA_INFRACTORES';
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

	try
		Result := DMConnections.BaseCAC.Connected;
		if Result then begin
			nb.PageIndex := 0;
			result := ObtenerParametroGeneral(DMConnections.BaseCAC, OUTPUT_DIRECTORY, FDestino);
			if not result then begin
				msgBoxErr(MSG_ERROR_CARGANDO_PARAMETROS, Format(MSG_PARAMETER_NOT_EXISTS, [OUTPUT_DIRECTORY]), caption, MB_ICONERROR);
				exit;
			end;
			FEnProceso := False;

			FFileBuffer:= TStringList.Create;
			txt_FechaEnvio.Date := NowBase(DMConnections.BaseCAC);

            with spObtenerOperacionesInterfazInfractores, Parameters do begin
                Close;
                Parameters.Refresh;
                ParamByName('@OperacionesCanceladas').Value := 1;
			    Open
            end;

            btnLimpiarClick(nil);

            //Rev.1 / 16-Junio-2010 / Nelson Droguett Sierra
            CargarCheckListBoxConcesionarias(DMConnections.BaseCAC,
                                             CLB_Concesionarias);

		end;
	except
		on e: Exception do begin
			MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
		end;
	end;
end;

{******************************** Function Header ******************************
Function Name: btOperacionesInterfazProcess
Author : lgisuk
Date Created : 15/07/2005
Description : Muestro los procesos de infractores
Parameters : Tabla: TDataSet; var Texto: String
Return Value : Boolean
*******************************************************************************}
function TFormInterfazImprentaInfractores.btOperacionesInterfazProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Result := True;
	with Tabla do
    	Texto := Format('Cartas generadas el %s -  Usuario: %s',
                        [FormatDateTime('dd/mm/yyyy hh:mm:ss', FieldByName('FechaGeneracion').AsDateTime),
                        FieldByName('Usuario').AsString]);
end;

{******************************** Function Header ******************************
Function Name: btOperacionesInterfazSelect
Author : lgisuk
Date Created : 15/07/2005
Description : selecciono un proceso
Parameters : Sender: TObject; Tabla: TDataSet
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.btOperacionesInterfazSelect(Sender: TObject; Tabla: TDataSet);
begin
    // Obtengo el C�digo de Operaci�n de Interfaz original
    with Tabla do begin
        try
            FCodigoOperacionInterfaseOriginal := FieldByName('CodigoOperacionInterfase').AsInteger;
            FechaGeneracionPrevia:= FieldByName('FechaGeneracion').AsDateTime;
            edProceso.Text := Format('Cartas generadas el %s',
                              [FormatDateTime('dd/mm/yyyy hh:mm:ss', FieldByName('FechaGeneracion').AsDateTime)]);
            FReimpresion := True;

			spGenerarListaInterfazInfractores.Close
        except
            on e: Exception do MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;

        btnObtenerInfractoresClick(Sender)
    end;
end;


{******************************** Function Header ******************************
Function Name: btnSiguienteClick
Author : lgisuk
Date Created : 15/07/2005
Description : permito ir a la siguiente pagina
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.btnSiguienteClick(Sender: TObject);
resourcestring
	MSG_ERROR_DATE= 'La fecha de env�o de la carta es inv�lida.';
begin
	// Valida que la fecha de Imprenta es v�lida
	if txt_FechaEnvio.Date = NullDate then begin
		MsgBoxBalloon(MSG_ERROR_DATE,Caption,MB_ICONSTOP,txt_FechaDesde);
		exit;
	end;

	nb.PageIndex := nb.PageIndex + 1;
	btnFinalizar.Enabled := True;
    btnCancelar.Enabled := False;
end;

{******************************** Function Header ******************************
Function Name: btnAnteriorClick
Author : lgisuk
Date Created : 15/07/2005
Description : permito ir a la pagina anterior
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.btnAnteriorClick(Sender: TObject);
begin
	nb.PageIndex := nb.PageIndex - 1;
end;


{******************************** Function Header ******************************
Function Name: CrearNombresArchivos
Author       : rcastro
Date Created : 13/04/2005
Description  : Define el nombre del archivo de salida y de control
Parameters   : Path: String
Return Value : None
*******************************************************************************}
//Rev.1 / 16-Junio-2010 / Nelson Droguett Sierra--------------------------------------
procedure TFormInterfazImprentaInfractores.CrearNombresArchivos(Path: String; CodigoConcesionaria: Integer);
var
  sFecha : string;
  NombreCorto:string;
begin
  if FReimpresion then
  	sFecha := FormatDateTime('ddmmyyyy', FechaGeneracionPrevia)
  else
  	sFecha := FormatDateTime('ddmmyyyy', NowBase(DMConnections.BaseCAC));

  NombreCorto             := trim(QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerNombreCortoConcesionaria (''%s'')',[IntToStr(CodigoConcesionaria)])));
  FNombreArchivoControl   := Format(CONTROL_FILE_PREFIX, [NombreCorto]) + sFecha + FILE_EXTENSION;
  FNombreArchivo          := Format(FILE_PREFIX, [NombreCorto]) + sFecha + FILE_EXTENSION;
  txtUbicacion.Text       := GoodDir(Path);
end;
//FinRev.1-----------------------------------------------------------------------------


{******************************** Function Header ******************************
Function Name: HabilitarControles
Author : lgisuk
Date Created : 15/07/2005
Description : habilito los controles
Parameters : Habilitado: Boolean
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.HabilitarControles(Habilitado: Boolean);
begin
	txtUbicacion.Enabled        := Habilitado;
	btnBrowseForFolder.Enabled  := Habilitado;
end;

{******************************** Function Header ******************************
Function Name: RegistrarOperacion
Author       : rcastro
Date Created : 13/04/2005
Description  : Registra la operaci�n de interfaz
Parameters   : None
Return Value : boolean
*******************************************************************************}
function TFormInterfazImprentaInfractores.RegistrarOperacion : boolean;
resourcestring
	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
	MSG_ERROR = 'Error';
var
	DescError : string;
  sDetalle  : string;
begin
  if FReimpresion then sDetalle := 'Reimpresi�n archivo interfaz'
  else sDetalle := 'Creaci�n archivo interfaz';

  // al registrar la operaci�n de interfaz tengo en cuenta si estoy reimprimiendo o generando un nuevo archivo para imprenta
	result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_INTERFAZ_SALIENTE_CARTAS_INFRACTORES,  ExtractFileName(FNombreArchivo), UsuarioSistema, sDetalle, True, False, NowBase(DMConnections.BaseCAC),0, FCodOperacionesInterfase, DescError);
	if not result then
		MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{******************************** Function Header ******************************
Function Name: btnObtenerInfractoresClick
Author       : rcastro
Date Created : 13/04/2005
Description  : Obtiene la lista de infractores seg�n filtros ingresados
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.btnObtenerInfractoresClick(Sender: TObject);
resourcestring
	MSG_ERROR_GETTING_INFRACTORS = 'Error al obtener los infractores';
	MSG_ERROR_DATE_ORDER= 'La fecha desde debe ser menor que la fecha hasta.';
    MSG_NO_DATA = 'No hay datos a procesar.';
//Rev.1 / 15-Junio-2010 / Nelson Droguett Sierra------------------------------
var
	iLoop : integer;
    bDebeBorrar: boolean;
//FinRev.1
begin
	// Valida que la fecha Desde no sea mayor que la fecha Hasta
	if (txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate) and (txt_FechaDesde.Date>txt_FechaHasta.Date) then begin
		MsgBoxBalloon(MSG_ERROR_DATE_ORDER,Caption,MB_ICONSTOP,txt_FechaDesde);
		exit;
	end;

    // Determino si estoy reimprimendo el archivo de interfaz
    FReimpresion := Trim(edProceso.Text) <> '';

    cursor := crHourGlass;

   	//Rev.1 / 15-Junio-2010 / Nelson Droguett Sierra------------------------------
    {
    try
		qryEliminarTemporales.ExecSQL
	except
		on e: Exception do //
	end;
    }
    //FinRev.1

	try
    	//Rev.1 / 15-Junio-2010 / Nelson Droguett Sierra------------------------------
    	bDebeBorrar:=True;
        if Not FReimpresion then begin
          for iLoop := 0 to CLB_Concesionarias.Items.Count - 1 do
          begin
            if CLB_Concesionarias.Checked[iLoop] then begin
              try
                  spGenerarListaInterfazInfractores.Close;
                  with spGenerarListaInterfazInfractores, Parameters do begin
                      Parameters.Refresh;
                      if (txt_FechaDesde.Date=nulldate) then
                          Parameters.ParamByName('@FechaDesde').Value := null
                      else
                          Parameters.ParamByName('@FechaDesde').Value	:= FormatDateTime('yyyy-mm-dd',txt_FechaDesde.Date);

                      if (txt_FechaHasta.Date=nulldate) then
                          Parameters.ParamByName('@FechaHasta').Value := null
                      else
                          Parameters.ParamByName('@FechaHasta').Value	:= FormatDateTime('yyyy-mm-dd',txt_FechaHasta.Date);

                      if (FReimpresion) then
                          Parameters.ParamByName('@CodigoOperacionInterfase').Value:= FCodigoOperacionInterfaseOriginal
                      else
                          Parameters.ParamByName('@CodigoOperacionInterfase').Value:= null;

                      Parameters.ParamByName('@DebeBorrarTabla').Value:= IfThen(bDebeBorrar,1,0);
                      Parameters.ParamByName('@CodigoConcesionaria').Value:= CLB_Concesionarias.Items[iLoop].Value;
                      ExecProc;
                  end;
                  bDebeBorrar:=False;
              except
                  on e: Exception do begin
                      cursor := crDefault;
                      MsgBoxErr(MSG_ERROR_GETTING_INFRACTORS, e.Message, MSG_ERROR, MB_ICONERROR);
                  end;
              end
            end;
        end;
        end
        else begin
          try
              spGenerarListaInterfazInfractores.Close;
              with spGenerarListaInterfazInfractores, Parameters do begin
                  Parameters.Refresh;
                  Parameters.ParamByName('@FechaDesde').Value := null;
                  Parameters.ParamByName('@FechaHasta').Value := null;
                  Parameters.ParamByName('@CodigoConcesionaria').Value := null;
                  Parameters.ParamByName('@CodigoOperacionInterfase').Value:= FCodigoOperacionInterfaseOriginal;
                  Parameters.ParamByName('@DebeBorrarTabla').Value:= 1;
                  ExecProc;
              end;
          except
              on e: Exception do begin
                  cursor := crDefault;
                  MsgBoxErr(MSG_ERROR_GETTING_INFRACTORS, e.Message, MSG_ERROR, MB_ICONERROR);
              end;
          end
        end;
        spResumirListaInterfazInfractores.Close;
        spResumirListaInterfazInfractores.Open;
        //FinRev.1
	finally
        cursor := crDefault;
		btnSiguiente.Enabled := (not spResumirListaInterfazInfractores.IsEmpty);
		if not btnSiguiente.Enabled then MsgBox(MSG_NO_DATA);
	end
end;


{******************************** Function Header ******************************
Function Name: dblInfractoresColumns0HeaderClick
Author : lgisuk
Date Created : 15/07/2005
Description : permito ordener las columnas de la grilla
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.dblInfractoresColumns0HeaderClick(Sender: TObject);
var
  sOrder : AnsiString;
begin
    if (TDBListExColumn(sender).Sorting=csAscending) then
      TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;

    if (TDBListExColumn(sender).Sorting=csAscending) then sOrder := 'ASC'
    else sOrder := 'DESC';

    if spGenerarListaInterfazInfractores.Active then
        spGenerarListaInterfazInfractores.Sort := TDBListExColumn(sender).FieldName  + ' ' + sOrder;
end;

{******************************** Function Header ******************************
Function Name: btnBrowseForFolderClick
Author       : rcastro
Date Created : 13/04/2005
Description  : Define ubicaci�n del archivo a generar
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.btnBrowseForFolderClick(Sender: TObject);
var
	Location: String;
begin
	Location := Trim(BrowseForFolder(MSG_SELECT_LOCATION));
	if Location = '' then exit;
	FNombreArchivo := '';
	txtUbicacion.Text := Location;

	FDestino := GoodDir(txtUbicacion.Text);

	btnFinalizar.Enabled := True;
end;

{******************************** Function Header ******************************
Function Name: CrearArchivo
Author       : rcastro
Date Created : 13/04/2005
Description  : Crea el archivo para imprenta
Parameters   : Archivo: String; var DescriError: AnsiString
Return Value : Boolean
*******************************************************************************}
//Rev.1 / 16-Junio-2010 / Nelson Droguett Sierra--------------------------------------
procedure TFormInterfazImprentaInfractores.CargarCheckListBoxConcesionarias(
  Conn: TADOConnection; CheckListBox: TVariantCheckListBox);
    resourcestring
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';
    var
        ADOStoredProc: TADOStoredProc;
begin
    try
        ADOStoredProc := TADOStoredProc.Create(nil);
        with ADOStoredProc, CheckListBox do try
            Connection := Conn;
            ProcedureName := 'ObtenerConcesionarias';
            Open;
            Clear;
            while not Eof do begin
                Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoConcesionaria').AsInteger);
                Next;
            end;
            ItemIndex := 0;
        except
            on e: Exception do begin
                MsgBoxErr(
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),
                    MSG_ERROR_CAPTION,
                    MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;
end;
//FinRev.1---------------------------------------------------------------------------



function TFormInterfazImprentaInfractores.CrearArchivo(Archivo: String; var DescriError: AnsiString):Boolean;
resourcestring
	MSG_FILE_EXISTS = 'El archivo %s ya existe en el directorio de destino. �Desea reemplazarlo?';
	MSG_ERROR_DESCRIPTION = 'Se ha cancelado el proceso debido a la existencia de un archivo id�ntico en el directorio de destino';
	MSG_WARNING = 'Atenci�n';
begin
	Result := False;
	// Verifica existencia archivo, verificando si quiero sobreescribirlo
	if FileExists(Archivo) then begin
		if (MsgBox(Format (MSG_FILE_EXISTS, [Archivo]), MSG_WARNING, MB_YESNO + MB_ICONQUESTION) = IDNO) then begin
			DescriError := MSG_ERROR_DESCRIPTION;
			Exit;
		end;
	end;

	try
		AssignFile(FOutFile, Trim(Archivo));
		Rewrite(FOutFile);
		Result := True;
		CloseFile(FOutFile);
	except
		on e:Exception do begin
			DescriError := e.Message;
		end;
	end;
end;

{******************************** Function Header ******************************
Function Name: btnFinalizarClick
Author       : rcastro
Date Created : 14/04/2005
Description  : Registro de notificaciones y generacion de archivo de interfaz
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.btnFinalizarClick(Sender: TObject);
    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    fsandi
      Date Created: 29/11/2007
      Description: Actualizo el log al finalizar
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ActualizarLog:boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVACION = 'Finalizo OK!';
    var
         DescError : String;
    begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC, FCodOperacionesInterfase, STR_OBSERVACION , DescError);
        except
            on e: Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;


resourcestring
	MSG_PROCESS_FAIL            = 'El proceso finaliz� con errores';
	MSG_DELETE_ERROR            = 'Error al eliminar el archivo de interface';
	MSG_PROCESS_FINISHED        = 'El proceso finaliz� correctamente';
	MSG_PROCESS_CANCELLED       = 'Proceso cancelado';
var
	Error		: String;
    RegOpOK		: Boolean;
    ProcesoOK	:Boolean;
    //Rev.1 / 16-Junio-2010 / Nelson Droguett Sierra ----------------------------------
    iLoop:Integer;
//Rev.1 / 16-Junio-2010 / Nelson Droguett Sierra---------------------------------------
begin
    HabilitarControles(False);
    btnFinalizar.Enabled := False;
    btnSalir.Enabled    := False;
    FCancel             := False;
    KeyPreview          := True;
    pnlProgreso.Visible := True;
    btnAnterior.Enabled := False;
    btnCancelar.Enabled := True;
    FEnProceso          := True;
    pbProgreso.Step     := 1;
    pbProgreso.Min      := 0;
    pbProgreso.Smooth   := True;
    Screen.Cursor := crHourGlass;
    // Comienzo transacci�n
    //DMConnections.BaseCAC.BeginTrans;                                             //SS_1385_NDR_20150922
    DMConnections.BaseCAC.Execute('BEGIN TRAN frmImprentaInfractores');     //SS_1385_NDR_20150922

    RegOpOk := RegistrarOperacion;
    ProcesoOk:=False;

    try
      for iLoop := 0 to CLB_Concesionarias.Items.Count - 1 do
      begin
      	if (CLB_Concesionarias.Checked[iLoop] or FReimpresion) then begin
          // Agrupo las infracciones
          spAgruparListaInterfazInfractores.Close;
          spAgruparListaInterfazInfractores.Parameters.Refresh;
          spAgruparListaInterfazInfractores.Parameters.ParamByName('@CodigoConcesionaria').Value := CLB_Concesionarias.Items[iLoop].Value;
          spAgruparListaInterfazInfractores.Open;
          if Not(spAgruparListaInterfazInfractores.IsEmpty) then
          begin
            CrearNombresArchivos(FDestino,CLB_Concesionarias.Items[iLoop].Value);
            if not CrearArchivo(txtUbicacion.Text+FNombreArchivo, Error) then begin
                MsgBoxErr(MSG_CANNOT_CREATEFILE, Error, MSG_ERROR, MB_ICONERROR);
                Exit;
            end;
            pbprogreso.Max 		:= spAgruparListaInterfazInfractores.RecordCount;
            pbProgreso.Position := 0;
            try
                if ActualizarCartasAInfractores(CLB_Concesionarias.Items[iLoop].Value) then
                   if GenerarArchivo then
                      if EscribirArchivoControl then
                         ProcesoOK:=True;
                if Not ProcesoOK then
                    exit;
            except
                on e:Exception do begin
                    Screen.Cursor := crDefault;
                    MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                end;
            end;
          end;
        end;
      end;
    finally
        if (RegOpOk And ProcesoOk) then begin
            ActualizarLog;
            //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('COMMIT TRAN frmImprentaInfractores');					//SS_1385_NDR_20150922

            Screen.Cursor := crDefault;

            MsgBox(MSG_PROCESS_FINISHED);
        end
        else begin
            // Hay errores � se cancel� el proceso
            //if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;  //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION frmImprentaInfractores END');	//SS_1385_NDR_20150922



            // Elimino el archivo de infractores creado
            for iLoop := 0 to CLB_Concesionarias.Items.Count - 1 do
            begin
              if (CLB_Concesionarias.Checked[iLoop] OR FReimpresion) then begin
                CrearNombresArchivos(FDestino,CLB_Concesionarias.Items[iLoop].Value);
                if FileExists(txtUbicacion.Text+FNombreArchivo) then begin
                    try
                        DeleteFile(txtUbicacion.Text+FNombreArchivo);
                    except
                        on e: Exception do begin
                            MsgBoxErr(MSG_DELETE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                        end;
                    end
                end;
                if FileExists(txtUbicacion.Text+FNombreArchivoControl) then begin
                    try
                        DeleteFile(txtUbicacion.Text+FNombreArchivoControl);
                    except
                        on e: Exception do begin
                            MsgBoxErr(MSG_DELETE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                        end;
                    end
                end;
              end;
            end;
            Screen.Cursor := crDefault;
            if FCancel then begin
                labelProgreso.Caption := MSG_PROCESS_CANCELLED;
                MsgBox(MSG_PROCESS_CANCELLED);
            end
            else MsgBox(MSG_PROCESS_FAIL);
        end;
        spAgruparListaInterfazInfractores.Close;
        spResumirListaInterfazInfractores.Close;
        qryEliminarTemporales.ExecSQL;
        FEnProceso := False;
        KeyPreview := False;
        pbProgreso.Position := 0;
        btnAnterior.Enabled := True;
        btnSalir.Enabled := True;
        btnCancelar.Enabled := False;
        txtUbicacion.Clear;
        lblDescri.Caption := '';
        HabilitarControles(True);
        btnLimpiarClick(Sender);
        btnAnteriorClick(Sender)
    end;
end;
//FinRev.1----------------------------------------------------------------------------

{******************************** Function Header ******************************
Function Name: ActualizarCartasAInfractores
Author       : rcastro
Date Created : 14/04/2005
Description  : Registra los datos de la operacion de interfaz en las Cartas a
				Infractores
Parameters   : None
Return Value : boolean
*******************************************************************************}
function TFormInterfazImprentaInfractores.ActualizarCartasAInfractores(CodigoConcesionaria:Integer) : boolean;
resourcestring
	MSG_ERROR_UPDATING_INFRACTORS_DETAIL = 'Error actualizando Cartas a Infractores';
	MSG_PROCESSING_INFRACTORS = 'Procesando %d infracciones...';
begin
	labelProgreso.Caption := FORMAT(MSG_PROCESSING_INFRACTORS, [pbprogreso.Max]);
	labelProgreso.Update;
	pbProgreso.Position := pbprogreso.Max;
	self.Repaint;

	try
		result := false;
		try
			with spActualizarOperacionEnCartasAInfractores, Parameters do begin
				Parameters.Refresh;
				ParamByname ('@CodigoOperacionInterfase').Value := FCodOperacionesInterfase;//iif (FReimpresion, FCodigoOperacionInterfaseOriginal, FCodOperacionesInterfase);
				ParamByname ('@FechaDeEnvio').Value := txt_FechaEnvio.Date;
                //Rev.1 / 16-Junio-2010 / Nelson Droguett Sierra ---------------------------------------------------
				ParamByname ('@CodigoConcesionaria').Value := CodigoConcesionaria;
				ExecProc
			end;

			result := True;
		except
			on e:Exception do begin
				raise Exception.Create(MSG_ERROR_UPDATING_INFRACTORS_DETAIL + Space(1) + e.message );
			end;
		end;
	finally
	end;
end;

{******************************** Function Header ******************************
Function Name: GenerarArchivo
Author       : rcastro
Date Created : 14/04/2005
Description  : Genera el archivo de interfaz
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TFormInterfazImprentaInfractores.GenerarArchivo: Boolean;
resourcestring
	MSG_ERROR_WRITING_FILE = 'Error generando el archivo para imprenta.';
	MSG_WRITING_FILE = 'Escribiendo los datos al disco.';
	LETTER_TYPE	  = 'INFR18';
	CLIENT_PREFIX = '5000';
	LETTER_PREFIX = '6000';
	DETAIL_PREFIX = '7000';
	RESUME_PREFIX = '9000';
	DETAIL_FIELDS = '3';
	RESUME_FIELDS = '2';
	LETTER_SIGNATURE_1 = 'F001';
	LETTER_SIGNATURE_2 = 'F002';
var
	FDocumento : String;
begin
	labelProgreso.Caption := MSG_WRITING_FILE;
	pbProgreso.Position := 0;
	self.Repaint;

	try
		result := false;
		try
			AssignFile(FOutFile, Trim(txtUbicacion.Text+FNombreArchivo));
			Rewrite(FOutFile);

			with spAgruparListaInterfazInfractores do begin
				DisableControls;
				FDocumento := '';
				FCantidadCartas := 0;

				First;
				while not EoF do begin
					Application.ProcessMessages;

					if FDocumento <> Trim(FieldByName('NumeroDocumento').AsString) then begin
						FCantidadCartas := FCantidadCartas + 1;
						FDocumento := Trim(FieldByName('NumeroDocumento').AsString);

						// Datos Cliente
						WriteLn (FOutFile,
							CLIENT_PREFIX, SEPARADOR,
							Trim(FieldByName('Nombre').AsString), SEPARADOR,
							(* APELLIDO_MATERNO *) SEPARADOR,
							(* NOMBRES *) SEPARADOR,
							(* SEXO *) SEPARADOR,
							(* PERSONERIA_JURIDICA *) SEPARADOR,
							Trim(FieldByName('Region').AsString), SEPARADOR,
							Trim(FieldByName('Comuna').AsString), SEPARADOR,
							Trim(FieldByName('Calle').AsString), SEPARADOR,
							Trim(FieldByName('Numero').AsString), SEPARADOR,
							Trim(FieldByName('Detalle').AsString), SEPARADOR,
							Trim(FieldByName('CodigoPostal').AsString), SEPARADOR);

						// Datos Carta
						WriteLn (FOutFile,
							LETTER_PREFIX, SEPARADOR,
							LETTER_TYPE, SEPARADOR,
							FormatDateTime ('dd/mm/yyyy', txt_FechaEnvio.Date), SEPARADOR,
							LETTER_SIGNATURE_1, SEPARADOR);
					end;

					// Item Carta
					WriteLn (FOutFile,
						DETAIL_PREFIX, SEPARADOR,
						LETTER_TYPE, SEPARADOR,
						DETAIL_FIELDS, SEPARADOR,
						Trim(FieldByName('Patente').AsString), SEPARADOR,
						Trim(FieldByName('Categoria').AsString), SEPARADOR,
						FormatDateTime ('dd/mm/yyyy', FieldByName('FechaHora').AsDateTime), SEPARADOR);
					pbProgreso.Position := pbProgreso.Position + 1;

					Next;
				end;

				EnableControls;
			end;

			CloseFile(FOutFile);
			Result := True;
		except
			on e: Exception do begin
				raise Exception.Create(MSG_ERROR_WRITING_FILE + Space(1) + e.message );
			end;
		end;
	finally
	end;

	labelProgreso.Caption := '';
end;

{******************************** Function Header ******************************
Function Name: EscribirArchivoControl
Author       : rcastro
Date Created : 14/04/2005
Description  : Genera el archivo de control de interfaz
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TFormInterfazImprentaInfractores.EscribirArchivoControl: Boolean;
resourcestring
	MSG_CONTROL_WRITE_FAIL = 'Error generando el archivo de control.';
const
	CONTROL_PREFIX = '8000';
var
	Linea: AnsiString;
begin
	try
		result := false;
		try
			Linea := CONTROL_PREFIX + SEPARADOR +
						IntToStr(spAgruparListaInterfazInfractores.RecordCount + 2*FCantidadCartas) + SEPARADOR +
						IntToStr(FCantidadCartas) + SEPARADOR;
            //Rev.1 / 16-Junio-2010 / Nelson Droguett Sierra--------------------------
			StringToFile(Linea, txtUbicacion.Text+FNombreArchivoControl);

			Result := True;
		except
			on e: Exception do begin
				result := false;
				raise Exception.Create(MSG_CONTROL_WRITE_FAIL + Space(1) + e.message );
			end;
		end
	finally
	end;
end;


{******************************** Function Header ******************************
Function Name: btnLimpiarClick
Author : lgisuk
Date Created : 15/07/2005
Description : limpio el form
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.btnLimpiarClick(Sender: TObject);
begin
    try
		spGenerarListaInterfazInfractores.Close
    except
        on e: Exception do MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
    end;

	nb.PageIndex := 0;
    btnSiguiente.Enabled := false;
    txt_FechaDesde.Clear;
    txt_Fechahasta.Clear;
    edProceso.Text := '';
    txt_FechaDesde.SetFocus;
    FReimpresion := False;
    //Rev.1 / 16-Junio-2010 / Nelson Droguett Sierra
    //CrearNombresArchivos(FDestino);
end;

{******************************** Function Header ******************************
Function Name: FormKeyDown
Author : lgisuk
Date Created : 15/07/2005
Description : permito cancelar apretando tecla escape
Parameters : Sender: TObject; var Key: Word;Shift: TShiftState
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
	case Key of
		VK_ESCAPE: begin
		    FCancel := True;
        end;
	end;
end;


{******************************** Function Header ******************************
Function Name: btnCancelarClick
Author : lgisuk
Date Created : 15/07/2005
Description :  permito cancelar haciendo click en boton
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.btnCancelarClick(Sender: TObject);
begin
    FCancel := True;
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : lgisuk
Date Created : 15/07/2005
Description : Permito cerrar el form
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.btnSalirClick(Sender: TObject);
begin
	Close;
end;


{******************************** Function Header ******************************
Function Name: FormClose
Author : lgisuk
Date Created : 15/07/2005
Description : lo libero de memoria
Parameters : Sender: TObject;var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TFormInterfazImprentaInfractores.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	if FEnProceso then begin
		Action := caNone;
		Exit;
	end;

    spObtenerOperacionesInterfazInfractores.Close;
	Action := caFree;
end;



end.


