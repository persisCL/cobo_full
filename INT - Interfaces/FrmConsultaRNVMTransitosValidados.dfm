object FormConsultaRNVMTransitosValidados: TFormConsultaRNVMTransitosValidados
  Left = 60
  Top = 120
  Caption = 'Consultas al RNVM por Tr'#225'nsitos Validados'
  ClientHeight = 433
  ClientWidth = 860
  Color = clBtnFace
  Constraints.MinHeight = 471
  Constraints.MinWidth = 876
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 860
    Height = 73
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 868
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 868
      Height = 73
      Align = alClient
      Caption = 'Filtros'
      TabOrder = 0
      DesignSize = (
        860
        73)
      object Label8: TLabel
        Left = 11
        Top = 23
        Width = 40
        Height = 13
        Caption = '&Patente:'
        FocusControl = txt_Patente
      end
      object Label2: TLabel
        Left = 132
        Top = 23
        Width = 136
        Height = 13
        Caption = 'Fecha de Infracci'#243'n:  &desde:'
        FocusControl = txt_FechaDesde
      end
      object Label3: TLabel
        Left = 394
        Top = 23
        Width = 29
        Height = 13
        Caption = '&hasta:'
        FocusControl = txt_FechaHasta
      end
      object Label4: TLabel
        Left = 548
        Top = 23
        Width = 31
        Height = 13
        Caption = 'Hasta:'
        FocusControl = txt_CantInfracciones
      end
      object Label1: TLabel
        Left = 636
        Top = 22
        Width = 58
        Height = 13
        Caption = 'Infracciones'
      end
      object btn_Filtrar: TButton
        Left = 697
        Top = 16
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Anchors = [akTop, akRight]
        Caption = '&Filtrar'
        Default = True
        TabOrder = 1
        OnClick = btn_FiltrarClick
        ExplicitLeft = 705
      end
      object btn_Limpiar: TButton
        Left = 777
        Top = 16
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Anchors = [akTop, akRight]
        Caption = '&Limpiar'
        TabOrder = 2
        OnClick = btn_LimpiarClick
        ExplicitLeft = 785
      end
      object txt_Patente: TEdit
        Left = 56
        Top = 19
        Width = 65
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 0
      end
      object txt_FechaDesde: TDateEdit
        Left = 272
        Top = 19
        Width = 114
        Height = 21
        AutoSelect = False
        TabOrder = 3
        OnExit = txt_FechaDesdeExit
        Date = -693594.000000000000000000
      end
      object txt_FechaHasta: TDateEdit
        Left = 427
        Top = 19
        Width = 114
        Height = 21
        AutoSelect = False
        TabOrder = 4
        Date = -693594.000000000000000000
      end
      object txt_CantInfracciones: TNumericEdit
        Left = 582
        Top = 19
        Width = 53
        Height = 21
        MaxLength = 7
        TabOrder = 5
        Value = 999.000000000000000000
      end
      object chkSinConsultar: TCheckBox
        Left = 11
        Top = 44
        Width = 358
        Height = 17
        Caption = 
          'Realizar la(s) consulta(s) sin verificar cuando se consult'#243' por ' +
          #250'ltima vez'
        TabOrder = 6
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 392
    Width = 860
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 403
    ExplicitWidth = 868
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 868
      Height = 41
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        860
        41)
      object lbl_Mostar: TLabel
        Left = 8
        Top = 16
        Width = 292
        Height = 13
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'lbl_Mostar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 300
      end
      object BtnSalir: TButton
        Left = 717
        Top = 7
        Width = 130
        Height = 26
        Hint = 'Salir del visualizador de Solicitudes'
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Salir'
        TabOrder = 0
        OnClick = BtnSalirClick
        ExplicitLeft = 725
      end
      object btnConsultarRNVM: TButton
        Left = 579
        Top = 7
        Width = 130
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Cons&ultar RNVM'
        Enabled = False
        TabOrder = 1
        OnClick = btnConsultarRNVMClick
        ExplicitLeft = 587
      end
    end
  end
  object dblInfracciones: TDBListEx
    Left = 0
    Top = 73
    Width = 712
    Height = 319
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 46
        Header.Caption = 'RNVM'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'EnviarRNVM'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Patente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'Patente'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'FechaInfraccion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Cantidad Tr'#225'nsitos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'CantidadTransitos'
      end>
    DataSource = dsInfValidadas
    DragReorder = True
    ParentColor = False
    PopupMenu = popSeleccion
    TabOrder = 2
    TabStop = True
    OnDblClick = dblInfraccionesDblClick
    OnDrawText = dblInfraccionesDrawText
    ExplicitWidth = 720
    ExplicitHeight = 330
  end
  object Panel2: TPanel
    Left = 712
    Top = 73
    Width = 148
    Height = 319
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitLeft = 720
    ExplicitHeight = 330
    object btn_MarcarDesmarcar: TButton
      Left = 8
      Top = 16
      Width = 129
      Height = 25
      Caption = 'Marcar / Desmarcar'
      Enabled = False
      TabOrder = 0
      OnClick = btn_MarcarDesmarcarClick
    end
    object btn_MarcarTodos: TButton
      Left = 8
      Top = 48
      Width = 129
      Height = 25
      Caption = 'Marcar Todos'
      Enabled = False
      TabOrder = 1
      OnClick = btn_MarcarTodosClick
    end
    object btn_InverirSeleccion: TButton
      Left = 8
      Top = 80
      Width = 129
      Height = 25
      Caption = 'Invertir Selecci'#243'n Actual'
      Enabled = False
      TabOrder = 2
      OnClick = btn_InverirSeleccionClick
    end
  end
  object spInfraccionesRVM: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerInfraccionesConfirmadas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@CantidadInfracciones'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SinConsultar'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 72
    Top = 352
  end
  object dsInfValidadas: TDataSource
    DataSet = cdsInfValidadas
    Left = 8
    Top = 352
  end
  object Imagenes: TImageList
    Left = 104
    Top = 352
    Bitmap = {
      494C010102000400140010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0080808000C0C0C000F0FBFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00F0FB
      FF000000800000000000A56E3A00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C0C0
      C000000000000000000000000000F0FBFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000080
      8000000000000080800000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00808080000000
      000000000000F0FBFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C0000000
      8000FFFFFF00FFFFFF00A56E3A000000000080808000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00808080000000000080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080800000000000C0C0C000F0FB
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F0FBFF008080800000000000C0DC
      C000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00808080000000
      0000A56E3A00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A56E
      3A0000000000A56E3A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F0FBFF0080808000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object cdsInfValidadas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Patente'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'FechaInfraccion'
        DataType = ftDateTime
      end
      item
        Name = 'CantidadTransitos'
        DataType = ftInteger
      end
      item
        Name = 'EnviarRNVM'
        DataType = ftBoolean
      end
      item
        Name = 'CodigosInfraccionAsociados'
        DataType = ftString
        Size = 200
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterOpen = cdsInfValidadasAfterOpen
    AfterClose = cdsInfValidadasAfterOpen
    AfterPost = cdsInfValidadasAfterPost
    AfterScroll = cdsInfValidadasAfterScroll
    Left = 40
    Top = 352
  end
  object popSeleccion: TPopupMenu
    Left = 136
    Top = 352
    object Seleccionar1: TMenuItem
      Caption = 'Marcar / Desmarcar'
      OnClick = Seleccionar1Click
    end
    object SeleccionarTodos1: TMenuItem
      Caption = 'Marcar Todos'
      OnClick = SeleccionarTodos1Click
    end
    object InvertirSeleccin1: TMenuItem
      Caption = 'Invertir Selecci'#243'n Actual'
      OnClick = InvertirSeleccin1Click
    end
  end
end
