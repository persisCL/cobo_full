object FormPlantillasEmail: TFormPlantillasEmail
  Left = 246
  Top = 168
  Caption = 'Mantenimiento de Plantillas de Email'
  ClientHeight = 628
  ClientWidth = 874
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 874
    Height = 33
    Habilitados = [btAlta, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 874
    Height = 181
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'55'#0'C'#243'digo               '
      #0'41'#0'Asunto')
    HScrollBar = True
    RefreshTime = 100
    Table = Plantillas
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 214
    Width = 874
    Height = 375
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    DesignSize = (
      874
      375)
    object Label15: TLabel
      Left = 15
      Top = 11
      Width = 39
      Height = 13
      Caption = '&C'#243'digo: '
      FocusControl = txt_CodigoFirma
    end
    object Label2: TLabel
      Left = 15
      Top = 37
      Width = 44
      Height = 13
      Caption = '&Asunto:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 15
      Top = 61
      Width = 50
      Height = 13
      Caption = '&Plantilla:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 380
      Top = 271
      Width = 140
      Height = 13
      Caption = '&Incluye Archivo Adjunto:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 632
      Top = 271
      Width = 62
      Height = 13
      Caption = '&Contenido:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_Vendedor: TLabel
      Left = 16
      Top = 351
      Width = 35
      Height = 13
      Caption = 'Firma:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblEmail: TLabel
      Left = 16
      Top = 271
      Width = 31
      Height = 13
      Caption = 'Email'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblClaveSender: TLabel
      Left = 16
      Top = 298
      Width = 33
      Height = 13
      Caption = 'Clave'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRepetirClave: TLabel
      Left = 16
      Top = 323
      Width = 90
      Height = 13
      Caption = 'Confirmar Clave'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_CodigoFirma: TNumericEdit
      Left = 115
      Top = 8
      Width = 69
      Height = 21
      TabStop = False
      Color = clBtnFace
      MaxLength = 3
      ReadOnly = True
      TabOrder = 0
    end
    object txtPlantilla: TMemo
      Left = 115
      Top = 58
      Width = 752
      Height = 204
      Anchors = [akLeft, akTop, akRight, akBottom]
      Color = 16444382
      ScrollBars = ssVertical
      TabOrder = 2
    end
    object txtAsunto: TEdit
      Left = 115
      Top = 34
      Width = 752
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = 16444382
      TabOrder = 1
      Text = 'txtAsunto'
    end
    object rgIncluyeAdjunto: TRadioGroup
      Left = 526
      Top = 260
      Width = 89
      Height = 30
      Columns = 2
      Ctl3D = True
      ItemIndex = 0
      Items.Strings = (
        'S'#237
        'No')
      ParentCtl3D = False
      TabOrder = 3
    end
    object rgContentType: TRadioGroup
      Left = 700
      Top = 260
      Width = 167
      Height = 30
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Texto plano'
        'HTML')
      TabOrder = 4
    end
    object cbFirma: TVariantComboBox
      Left = 115
      Top = 348
      Width = 259
      Height = 21
      Hint = 'Tipo de pase diario'
      Style = vcsDropDownList
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnChange = cbFirmaChange
      Items = <>
    end
    object txtFirma: TMemo
      Left = 380
      Top = 295
      Width = 487
      Height = 75
      Anchors = [akLeft, akTop, akRight, akBottom]
      Color = 16444382
      Enabled = False
      ScrollBars = ssVertical
      TabOrder = 9
    end
    object Txt_Email: TEdit
      Left = 115
      Top = 268
      Width = 259
      Height = 21
      TabOrder = 5
    end
    object txtClaveSender: TEdit
      Left = 115
      Top = 295
      Width = 259
      Height = 21
      PasswordChar = '*'
      TabOrder = 6
    end
    object txtConfirmarClave: TEdit
      Left = 115
      Top = 320
      Width = 259
      Height = 21
      PasswordChar = '*'
      TabOrder = 7
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 589
    Width = 874
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 677
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Plantillas: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'EMAIL_Plantillas'
    Left = 210
    Top = 78
  end
end
