{********************************** Unit Header ********************************
File Name : FrmConsultaRNVMTransitosValidados.pas
Author : gcasais
Date Created: 28/07/2005
Language : ES-AR
Description :

Firma       :   SS_1133_MVI_20131011
Descripcion :   Al iniciar la pantalla el valor del m�ximo de infracciones debe ser de 999.
                Se permite dejar la cantidad de infracciones vac�as, siempre y cuando las fechas
                no sean nulas. Para este caso se env�a al SP la cantidad de 9.999.999.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

*******************************************************************************}
unit FrmConsultaRNVMTransitosValidados;

interface

uses
  //Consulta al RNVM por Transitos Validados
  Dmconnection,
  PeaProcs,
  PeaTypes,
  Util,
  UtilProc,
  UtilDB,
  RVMLogin,
  ActualizarDatosInfractor,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ListBoxEx, DBListEx, StdCtrls, DPSControls, Validate,
  DateEdit, ExtCtrls, DmiCtrls, RStrings, DateUtils, ImgList, DBClient,
  Menus, ComCtrls, frmListaErrores, RNVMProgress, RVMTypes, ConstParametrosGenerales;

type
  TFormConsultaRNVMTransitosValidados = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    btn_Filtrar: TButton;
    btn_Limpiar: TButton;
    txt_Patente: TEdit;
    Panel3: TPanel;
    Panel4: TPanel;
    lbl_Mostar: TLabel;
    BtnSalir: TButton;
    spInfraccionesRVM: TADOStoredProc;
    dsInfValidadas: TDataSource;
    Label2: TLabel;
    txt_FechaDesde: TDateEdit;
    txt_FechaHasta: TDateEdit;
    Label3: TLabel;
    btnConsultarRNVM: TButton;
    Imagenes: TImageList;
    Label4: TLabel;
    txt_CantInfracciones: TNumericEdit;
    cdsInfValidadas: TClientDataSet;
    dblInfracciones: TDBListEx;
    popSeleccion: TPopupMenu;
    Seleccionar1: TMenuItem;
    SeleccionarTodos1: TMenuItem;
    InvertirSeleccin1: TMenuItem;
    Label1: TLabel;
    Panel2: TPanel;
    btn_MarcarDesmarcar: TButton;
    btn_MarcarTodos: TButton;
    btn_InverirSeleccion: TButton;
    chkSinConsultar: TCheckBox;
    procedure btn_LimpiarClick(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure txt_FechaDesdeExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure dblInfraccionesColumnHeaderClick(Sender: TObject);
    procedure dblInfraccionesDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure btnConsultarRNVMClick(Sender: TObject);
    procedure InvertirSeleccin1Click(Sender: TObject);
    procedure cdsInfValidadasAfterPost(DataSet: TDataSet);
    procedure cdsInfValidadasAfterScroll(DataSet: TDataSet);
    procedure cdsInfValidadasAfterOpen(DataSet: TDataSet);
    procedure dblInfraccionesDblClick(Sender: TObject);
    procedure btn_MarcarDesmarcarClick(Sender: TObject);
    procedure Seleccionar1Click(Sender: TObject);
    procedure btn_MarcarTodosClick(Sender: TObject);
    procedure SeleccionarTodos1Click(Sender: TObject);
    procedure btn_InverirSeleccionClick(Sender: TObject);
  private
    FUsername: String;
    FPassWord: String;
    FDM: TDMActualizarDatosInfractor;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure ActualizarLabelCantRegistros;
    procedure Limpiar;
    function ObtenerCantidadConsultasEfectivas: Integer;
    { Private declarations }
  public
    function Inicializar(MDIChild: Boolean): Boolean;
    { Public declarations }
  end;

var
  FormConsultaRNVMTransitosValidados: TFormConsultaRNVMTransitosValidados;

implementation

{$R *.dfm}

resourcestring
    MSG_ERROR_INICIALIZACION = 'Error al inicializar Infracciones Validadas';


function TFormConsultaRNVMTransitosValidados.Inicializar(MDIChild: Boolean): Boolean;
resourcestring
    MSG_ERROR_OBTENER_PARAMETRO_GENERAL = 'Error al obtener informaci�n de par�metros generales';
    MSG_INIT_ERROR = 'Error de Inicializaci�n';
    MSG_PARAMS_ERROR = 'Error de Par�metros Generales del RNVM';
    MSG_ERROR = 'Error';
var
    S: TSize;
    TextoError: String;
begin
    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    if MDIChild then begin
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
    end else begin
        FormStyle := fsNormal;
        Visible := False;
    end;
    Limpiar;
    txt_FechaHasta.Date := Now;
//    txt_CantInfracciones.ValueInt := 100;                                         //SS_1133_MVI_20131011
    txt_CantInfracciones.ValueInt := 999;                                           //SS_1133_MVI_20131011
    FDM := TDMActualizarDatosInfractor.Create(Self);
    if not FDM.ConfigurarParametrosGenerales(TextoError) then begin
        MsgBoxErr(MSG_INIT_ERROR, MSG_PARAMS_ERROR + CRLF + TextoError, MSG_ERROR, MB_ICONERROR);
        Exit;
    end else begin
        Result := True;
    end;
end;

procedure TFormConsultaRNVMTransitosValidados.btn_LimpiarClick(Sender: TObject);
begin
    Limpiar;
end;

procedure TFormConsultaRNVMTransitosValidados.ActualizarLabelCantRegistros;
resourcestring
    STR_CANT_INFRACCIONES = 'Cantidad de Infracciones: %d.';
var
    CantRegistros: Integer;
begin
    CantRegistros := 0;
    if cdsInfValidadas.Active then begin
        CantRegistros := cdsInfValidadas.RecordCount;
    end;

    lbl_Mostar.Caption           := Format(STR_CANT_INFRACCIONES, [CantRegistros]);
end;

procedure TFormConsultaRNVMTransitosValidados.btn_FiltrarClick(Sender: TObject);
resourcestring                                                                                             //SS_1133_MVI_20131011
    MSG_CANTIDAD_VACIA = 'No puede dejar los campos de fecha y cantidad de infracciones vac�os.';          //SS_1133_MVI_20131011
    procedure CargarTablaTemp;
    begin
        if cdsInfValidadas.Active then
            cdsInfValidadas.EmptyDataSet
        else begin
            cdsInfValidadas.CreateDataSet;
            cdsInfValidadas.Open;
        end;
        spInfraccionesRVM.First;

        //Paso los registros del store al CDS
        spInfraccionesRVM.RecordCount;
        while not spInfraccionesRVM.Eof do begin
            with cdsInfValidadas do begin
                Append;
                FieldByName('Patente').AsString                    := spInfraccionesRVM.FieldByName('Patente').AsString;
                FieldByName('FechaInfraccion').AsDateTime          := spInfraccionesRVM.FieldByName('FechaHora').AsDateTime;
                FieldByName('CantidadTransitos').AsInteger         := spInfraccionesRVM.FieldByName('CantidadTransitos').AsInteger;
                FieldByName('CodigosInfraccionAsociados').AsString := spInfraccionesRVM.FieldByName('CodigoInfraccion').AsString;
                Post;
            end;
            spInfraccionesRVM.Next;
        end;
        cdsInfValidadas.First;
    end;

begin
    if (txt_FechaDesde.Date <> NullDate) and (txt_FechaHasta.Date <> NullDate) and (txt_FechaDesde.Date > txt_FechaHasta.Date) then begin
        MsgBoxBalloon(MSG_VALIDAR_ORDEN_FECHA, Caption, MB_ICONSTOP, txt_FechaDesde);
        txt_FechaDesde.SetFocus;
        Exit;
    end;

  if (txt_CantInfracciones.Text <> '') then begin                                                        //SS_1133_MVI_20131011
                                                                                                         //SS_1133_MVI_20131011
    if (txt_CantInfracciones.Value < 1) then begin
        MsgBoxBalloon(MSG_VALIDAR_TOP_LISTA_CONVENIO_MIN, Caption, MB_ICONSTOP, txt_CantInfracciones);
        txt_CantInfracciones.SetFocus;
        Exit;
    end;
  end                                                                                                           	//SS_1133_MVI_20131011
  else begin                                                                                                    	//SS_1133_MVI_20131011
                                                                                                                	//SS_1133_MVI_20131011
    if (txt_FechaDesde.Date = NullDate) and (txt_FechaHasta.Date = NullDate) and (txt_Patente.Text = '')then begin	//SS_1133_MVI_20131011
        MsgBoxBalloon(MSG_CANTIDAD_VACIA, Caption, MB_ICONSTOP, txt_CantInfracciones);                     			//SS_1133_MVI_20131011
        txt_CantInfracciones.SetFocus;                                                                          	//SS_1133_MVI_20131011
        Exit;                                                                                                   	//SS_1133_MVI_20131011
    end;                                                                                                        	//SS_1133_MVI_20131011
                                                                                                                	//SS_1133_MVI_20131011
  end;                                                                                                          	//SS_1133_MVI_20131011

    try
        try
            cdsInfValidadas.DisableControls;
            with spInfraccionesRVM do begin
                Parameters.Refresh;
                Parameters.ParamByName('@FechaDesde').Value           := iif(txt_FechaDesde.Date = NullDate, Null, txt_FechaDesde.Date); //AcomodarFecha(txt_FechaDesde.Date, True));
                Parameters.ParamByName('@FechaHasta').Value           := iif(txt_FechaHasta.Date = NullDate, Null, txt_FechaHasta.Date); //AcomodarFecha(txt_FechaHasta.Date, False));
                Parameters.ParamByName('@Patente').Value              := iif(Trim(txt_Patente.Text) = '', Null, Trim(txt_Patente.Text));
                Parameters.ParamByName('@CantidadInfracciones').Value := iif(trim(txt_CantInfracciones.Text) = '', 9999999, txt_CantInfracciones.ValueInt);    //SS_1133_MVI_20131011
                Parameters.ParamByName('@SinConsultar').Value := not chkSinConsultar.Checked;
            end;

            if not(OpenTables([spInfraccionesRVM])) then
                MsgBox(MSG_ERROR_INICIALIZACION, Caption, MB_ICONSTOP)
            else begin
                CargarTablaTemp;
                ActualizarLabelCantRegistros;
                spInfraccionesRVM.Close;
            end;
        except
            on e: Exception do begin
                MsgBoxErr('Error', e.Message, 'Error', MB_ICONERROR);
            end;
        end;
    finally
        cdsInfValidadas.EnableControls;
    end;

    dblInfracciones.SetFocus;
end;

procedure TFormConsultaRNVMTransitosValidados.txt_FechaDesdeExit(Sender: TObject);
begin
    if (txt_FechaHasta.Date = NullDate) then
        txt_FechaHasta.Date := txt_FechaDesde.Date;
end;


procedure TFormConsultaRNVMTransitosValidados.dblInfraccionesColumnHeaderClick(Sender: TObject);
var
    i: Integer;
begin
    if not cdsInfValidadas.Active then
        Exit;

    for i := 0 to dblInfracciones.Columns.Count-1 do
        if dblInfracciones.Columns[i] <> Sender then
            dblInfracciones.Columns[i].Sorting := csNone;

    if TDBListExColumn(Sender).Sorting = csAscending then
        TDBListExColumn(Sender).Sorting := csDescending
    else
        TDBListExColumn(Sender).Sorting := csAscending;

    with cdsInfValidadas do begin
        DisableControls;
        try
            if IndexName <> EmptyStr then
                DeleteIndex('xOrder');
            AddIndex('xOrder', TDBListExColumn(Sender).FieldName, [],
                     iif(TDBListExColumn(Sender).Sorting = csDescending, TDBListExColumn(Sender).FieldName, EmptyStr));
            IndexName := 'xOrder';
        finally
            EnableControls;
        end;
    end;
end;

procedure TFormConsultaRNVMTransitosValidados.dblInfraccionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp, Blanco: TBitMap;
begin
    with Sender do
        if (Column.FieldName = 'EnviarRNVM') then begin
            Text := '';
			DefaultDraw := False;
			canvas.FillRect(rect);
            bmp := TBitMap.Create;
            Blanco := TBitMap.Create;
            if (cdsInfValidadas.FieldByName('EnviarRNVM').AsBoolean) then
          	    Imagenes.GetBitmap(0, Bmp)
            else
                Imagenes.GetBitmap(1, Bmp);

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width := Rect.Right - Rect.Left;
            Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp); // Agrego un fondo balnco y centro el led en la grilla
            Bmp.Free;
            Blanco.Free;
        end;
end;

procedure TFormConsultaRNVMTransitosValidados.btnConsultarRNVMClick(Sender: TObject);
resourcestring
    MSG_OK = 'Los tr�nsitos se actualizaron correctamente';
var
    f: TfrmLogin;
    Patente: String[10];
    CodigoInfractor: Integer;
    ListaErrores: TStringList;
    ReportarErores: Boolean;
    p: TfrmRNVMProgress;
    Actual: Integer;
    DescriError: String;
    Respuesta: TRVMInformation;
begin
    // Solicitamos el login al servicio
    Application.CreateForm(TfrmLogin, f);
    f.UserName := 'cnorte';
    if f.ShowModal = mrOK then begin
        FUsername := f.UserName;
        FPassword := f.Password;
    end else begin
        FUsername := EmptyStr;
        FPassWord := EmptyStr;
        Exit;
    end;
    f.Release;
    ReportarErores  := False;
    cdsInfValidadas.DisableControls;
    ListaErrores    := TStringList.Create;
    Actual := 1;
    Application.CreateForm(TfrmRNVMProgress, p);
    p.CantidadConsultas := ObtenerCantidadConsultasEfectivas;
    if p.Inicializar then p.Show;
    Update;
    try
        cdsInfValidadas.First;
        while not cdsInfValidadas.Eof do begin
            if p.Cancelar then Break;
            if cdsInfValidadas.FieldByName('EnviarRNVM').AsBoolean then begin
                p.ConsultaActual := Actual;
                Inc(Actual);
                Patente := cdsInfValidadas.FieldByName('Patente').AsString;
                CodigoInfractor := cdsInfValidadas.FieldByName('CodigosInfraccionAsociados').AsInteger;
                // Hacemos la consulta y si corresponde informamos los errores de las mismas
                // para que se muestren todos juntos al final
                DescriError := '';
                if FDM.Actualizar(False, Patente, CodigoInfractor, FUsername, FPassWord, SistemaActual, Respuesta, DescriError, not chkSinConsultar.Checked) then begin
                    if DescriError <> '' then begin
                        ListaErrores.Add(DescriError);
                        ReportarErores := True;
                    end;
                end else begin
                    ListaErrores.Add(DescriError);
                    ReportarErores := True;
                end;
            end;
            cdsInfValidadas.Edit;
            cdsInfValidadas.FieldByName('EnviarRNVM').AsBoolean := False;
            cdsInfValidadas.Next;
        end;
        if not ReportarErores then begin
            MsgBox(MSG_OK);
        end else begin
            Application.CreateForm(TformListaErrores, formListaErrores);
            formListaErrores.mem_ListaErrores.Lines := ListaErrores;
            formListaErrores.ShowModal;
            formListaErrores.Release;
        end;
    finally
        p.Release;
        cdsInfValidadas.EnableControls;
        FreeAndNil(ListaErrores);
    end;
end;

procedure TFormConsultaRNVMTransitosValidados.InvertirSeleccin1Click(Sender: TObject);
begin
    if btn_InverirSeleccion.Enabled then btn_InverirSeleccion.Click;
end;
procedure TFormConsultaRNVMTransitosValidados.cdsInfValidadasAfterPost(DataSet: TDataSet);
var
    MyBookmark: TBookmark;
    EnableButton: Boolean;
begin
    EnableButton := False;
    with cdsInfValidadas do begin
        MyBookmark := DataSet.GetBookmark;
        First;
        DisableControls;
        while not Eof do begin
            EnableButton := FieldByName('EnviarRNVM').AsBoolean or EnableButton;
            if EnableButton then Break;
            Next;
        end;
        GotoBookmark(MyBookmark);
        EnableControls;
    end;
    btnConsultarRNVM.Enabled := EnableButton;
end;

procedure TFormConsultaRNVMTransitosValidados.cdsInfValidadasAfterScroll(
  DataSet: TDataSet);
begin
    popSeleccion.Items.Enabled := not cdsInfValidadas.IsEmpty;
    btn_MarcarDesmarcar.Enabled := not cdsInfValidadas.IsEmpty;
    btn_MarcarTodos.Enabled := not cdsInfValidadas.IsEmpty;
    btn_InverirSeleccion.Enabled := not cdsInfValidadas.IsEmpty;
end;

procedure TFormConsultaRNVMTransitosValidados.cdsInfValidadasAfterOpen(DataSet: TDataSet);
begin
    btn_MarcarDesmarcar.Enabled := cdsInfValidadas.Active;
    btn_MarcarTodos.Enabled := cdsInfValidadas.Active;
    btn_InverirSeleccion.Enabled := cdsInfValidadas.Active;
end;

procedure TFormConsultaRNVMTransitosValidados.dblInfraccionesDblClick(Sender: TObject);
begin
    if btn_MarcarDesmarcar.Enabled then btn_MarcarDesmarcar.Click;
end;

procedure TFormConsultaRNVMTransitosValidados.btn_MarcarDesmarcarClick(Sender: TObject);
begin
    cdsInfValidadas.Edit;
    cdsInfValidadas.FieldByName('EnviarRNVM').AsBoolean := not cdsInfValidadas.FieldByName('EnviarRNVM').AsBoolean;
    cdsInfValidadas.Post;
end;

procedure TFormConsultaRNVMTransitosValidados.Seleccionar1Click(Sender: TObject);
begin
    if btn_MarcarDesmarcar.Enabled then btn_MarcarDesmarcar.Click;
end;

procedure TFormConsultaRNVMTransitosValidados.btn_MarcarTodosClick(Sender: TObject);
var
    i, PosAct: Integer;
begin
    with cdsInfValidadas do begin
        PosAct := RecNo;
        DisableControls;
        try
            First;
            for i := 1 to RecordCount do begin
                Edit;
                FieldByName('EnviarRNVM').AsBoolean := True;
                Post;
                Next;
            end;
            RecNo := PosAct;
        finally
            EnableControls;
        end;
    end;
end;

procedure TFormConsultaRNVMTransitosValidados.SeleccionarTodos1Click(Sender: TObject);
begin
    if btn_MarcarTodos.Enabled then btn_MarcarTodos.Click;
end;

procedure TFormConsultaRNVMTransitosValidados.btn_InverirSeleccionClick(Sender: TObject);
var
    i, PosAct: Integer;
begin
    with cdsInfValidadas do begin
        PosAct := RecNo;
        DisableControls;
        try
            First;
            for i := 1 to RecordCount do begin
                Edit;
                FieldByName('EnviarRNVM').AsBoolean := not FieldByName('EnviarRNVM').AsBoolean;
                Post;
                Next;
            end;
            RecNo := PosAct;
        finally
            EnableControls;
            dblInfracciones.Repaint;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: BtnSalirClick
Author : gcasais
Date Created :
Description :  Permito cerrar el formulario
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormConsultaRNVMTransitosValidados.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : gcasais
Date Created :
Description : lo libero de memoria  
Parameters : Sender: TObject;var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TFormConsultaRNVMTransitosValidados.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    if Assigned(FDM) then FreeAndNil(FDM);
    cdsInfValidadas.Close;
    Action := caFree;
end;


procedure TFormConsultaRNVMTransitosValidados.Limpiar;
begin
    txt_Patente.Clear;
    txt_FechaDesde.Clear;
    txt_FechaHasta.Date := Now;
    cdsInfValidadas.Close;
    ActualizarLabelCantRegistros;
end;

function TFormConsultaRNVMTransitosValidados.ObtenerCantidadConsultasEfectivas: Integer;
var
    i: Integer;
begin
    Result := 0;
    cdsInfValidadas.First;
    cdsInfValidadas.DisableControls;
    for i:= 1 to cdsInfValidadas.RecordCount do begin
        if cdsInfValidadas.FieldByName('EnviarRNVM').AsBoolean = True then begin
            Inc(Result);
        end;
        cdsInfValidadas.Next;
    end;
    cdsInfValidadas.EnableControls;
end;

end.
