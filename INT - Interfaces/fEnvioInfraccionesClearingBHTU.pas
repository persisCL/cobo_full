
{********************************** Unit Header ********************************
File Name : fEnvioInfraccionesClearingBHTU.pas
Author : ndonadio
Date Created: 27/09/2005
Language : ES-AR
Description : Genera los archivos de infracciones y transitos infractores para
                enviar en el contexto del Clearing de BHTU
 
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

Autor       :   Claudio Quezada Ib��ez
Firma       :   SS_1147_CQU_20140408
Fecha       :   23-04-2014
Descripcion :   Se agrega variable y llamada a funci�n para obtener la Concesionaria Nativa
*******************************************************************************}
unit fEnvioInfraccionesClearingBHTU;

interface

uses
   // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc,
    // Parametros GEnerales
    ConstParametrosGenerales,
   // DB
    UtilDB, DMConnection,
    // Reporte
    fReporteEnvioInfraccionesBHTU,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, DateUtils, ppDB, ppDBPipe, UtilRB,
  ppCtrls, ppBands, ppClass, ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport, ppModule,
  raCodMod, ppParameter, Validate, DateEdit;

type
  TfrmEnvioInfraccionesClearingBHTU = class(TForm)
    Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    lblReferencia: TLabel;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pb_Progreso: TProgressBar;
    txtDirectorioDestino: TPickEdit;
    Label1: TLabel;
    lblDetalleProgreso: TLabel;
    deFechaDesde: TDateEdit;
    deFechaHasta: TDateEdit;
    lbl: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    spPrepararProcesoEnvioInfraccionesClearingBHTU: TADOStoredProc;
    spObtenerInfraccionesAEnviarClearingBHTU: TADOStoredProc;
    spSincronizarInfraccionesEnviadasBHTU: TADOStoredProc;
    spMarcarInfraccionesEnvioClearingBHTU: TADOStoredProc;
    spObtenerListaEnvioInfraccionesClearingBHTU: TADOStoredProc;
    spEliminarTablasTemporales: TADOStoredProc;
    spRegistrarArchivoEnvioInfraccionesBHTU: TADOStoredProc;
    spObtenerListaEnvioTransitosClearingBHTU: TADOStoredProc;
    spObtenerTransitosAEnviarClearingBHTU: TADOStoredProc;
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ImgAyudaClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure txtDirectorioDestinoButtonClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FGuardeArchivo,
    FProcesando,
    FCancelar               : boolean;
    FHoy,
    FFechaUltimoProceso     : TDateTime;
    FDestino,
    FErrorHeader            : AnsiString;
    FPrimerPRoceso,
    FUltimoProceso          : Integer;
    FCodigoConcesionariaNativa : Integer;   // SS_1147_CQU_20140408
    function ObtenerFechasIniciales(var Error: AnsiString): boolean;
    function ObtenerInfraccionesAEnviar(CodigoOperacion: Integer; FechaInfraccion: TDateTime; var Error: AnsiString): boolean;
    function ObtenerTransitosAEnviar(CodigoOperacion: Integer; var Error: AnsiString): boolean;
    function GuardarArchivoInfracciones(NombreArchivo: AnsiString; var Error: AnsiString): Boolean;
    function GuardarArchivoTransitos(NombreArchivo: AnsiString; var Error: AnsiString): Boolean;
    function CalcularFechaInfraccion(Fecha: TDateTime): TDateTime;
    function CrearNombreArchivoInfraccion(FechaArchivo: TDateTime): AnsiString;
    function CrearNombreArchivoTransitos(FechaArchivo: TDateTime): AnsiString;
    function GuardarRegistroArchivo(Operacion: integer; ArchivoInfraccion, ArchivoTransitos: AnsiString; FechaArchivo: TDateTime; NumReprocesos: Integer; var Error: AnsiString): Boolean;
    procedure HabilitarBotones;
    procedure MostrarReporte(Primera, Ultima: Integer);
    procedure LiberarTablasTemporales;
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): Boolean;
  end;

const
    FORMATO_FECHA   = 'dd/mm/yyyy';

var
  frmEnvioInfraccionesClearingBHTU: TfrmEnvioInfraccionesClearingBHTU;

implementation

{$R *.dfm}

{ TfrmEnvioInfraccionesClearingBHTU }

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 16/09/2005
Description : Carga parametros, inicializa y seta variables, etc.
Parameters : Titulo: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmEnvioInfraccionesClearingBHTU.Inicializar(Titulo: AnsiString): Boolean;
resourcestring
    ERROR_CANNOT_INITIALIZE     = 'No se puede inicializar la interfaz.';
    ERROR_CANNOT_GET_PARAMETER  = 'Error obteniendo el parametro general %s';
    ERROR_CANNOT_OBTAIN_LAST_FILE_DATE = 'No se puede obtener la fecha del �ltimo env�o.';
const
    DIR_DESTINO_INFRAC_CLEARING_BHTU = 'DIR_DESTINO_INFRAC_CLEARING_BHTU';
var
    descError: AnsiString;
begin
    Result := False;
    Caption := Titulo;
    // Seteo la fecha actual
    try
        FHoy := QueryGetValueDateTime(DMConnections.BaseCAC, 'SELECT dbo.PrincipioDia(GetDate())');
        FCodigoConcesionariaNativa := ObtenerCodigoConcesionariaNativa; // SS_1147_CQU_20140408
    except
        on e:exception do begin
            MsgBoxErr(ERROR_CANNOT_INITIALIZE, e.Message, Caption, MB_ICONERROR);
            Exit;
        end;
    end;
    // Cargo los parametros generales de directorios de destino para el par de archivos
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DESTINO_INFRAC_CLEARING_BHTU, FDestino) then begin
        MsgBoxErr(ERROR_CANNOT_INITIALIZE, Format(ERROR_CANNOT_GET_PARAMETER, [DIR_DESTINO_INFRAC_CLEARING_BHTU]), Caption, MB_ICONSTOP);
        Exit;
    end;
    txtDirectorioDestino.Text := GoodDir(FDestino);
    // Calculo las fechas por default...
    if not ObtenerFechasIniciales(descError)then begin
        // cagamos
       MsgBoxErr(ERROR_CANNOT_INITIALIZE, descError, Caption, MB_ICONSTOP);
       Exit;

    end;
    // Habilito el bot�n de procesar si es que el directorio existe
    btnProcesar.Enabled := DirectoryExists(GoodDir(FDestino));
    // Esta todo OK!, devuelvo TRUE!
    Result := True;
end;

procedure TfrmEnvioInfraccionesClearingBHTU.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmEnvioInfraccionesClearingBHTU.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := NOT FProcesando;
end;

procedure TfrmEnvioInfraccionesClearingBHTU.btnSalirClick(Sender: TObject);
begin
    // no hace falta validar nada, si est� procesando el boton no se puede clickear...
    Close;
end;

{******************************** Function Header ******************************
Function Name: txtDirectorioDestinoButtonClick
Author : ndonadio
Date Created : 16/09/2005
Description :  Selecciona un nuevo directorio
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmEnvioInfraccionesClearingBHTU.txtDirectorioDestinoButtonClick(Sender: TObject);
resourcestring
    MSG_SELECT_LOCATION = 'Seleccione ubicaci�n donde se generar�n los archivos.';
var
    Location: String;
begin
    btnProcesar.Enabled := False;
    // abre el form de seleccion de directorio
    Location := Trim(BrowseForFolder(MSG_SELECT_LOCATION));
    // valida el directorio elegido
    if Location = '' then Location := GoodDir(txtDirectorioDestino.Text);
   // lo carga en las variables, edits, etc.
    txtDirectorioDestino.Text := GoodDir(Location);
    // Habilita el boton procesar si puede crear nombres de archivos...
    btnProcesar.Enabled := DirectoryExists(GoodDir(Location));
end;


{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author : ndonadio
Date Created : 16/09/2005
Description : PROCESO PRINCIPAL
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmEnvioInfraccionesClearingBHTU.btnProcesarClick(Sender: TObject);
resourcestring
    // Errores
    ERROR_INVALID_DATE              = 'La fecha final debe estar en el rango %s - %s ';
    ERROR_INVALID_PATH              = 'El directorio de destino no es v�lido';
    ERROR_DATE_IS_WEEKEND           = 'La fecha hasta debe ser d�a habil.';
    ERROR_HEADER_FOR_DATE           = 'Se ha producido el siguiente error, procesando la fecha %s'+CRLF+CRLF;
    ERROR_REGISTER_OPERATION_LOG    = 'Error al registrar la operaci�n en el log.';
    ERROR_STARTING_OPERATION        = 'Error al preparar los datos para procesar.' + CRLF + 'Puede ser que el proceso ya est� en ejecuci�n.';
    ERROR_RETRIEVING_DATA           = 'Error al recuperar las infracciones.';
    ERROR_SINCRONIZING_DATA         = 'Se ha producido un error al transferir las infracciones a las tablas de registro.';
    ERROR_MARKING_DATA              = 'Se ha producido un error al intentar ' + CRLF + ' marcar las infracciones como enviadas para el Clearing';
    ERROR_SAVING_INFR_FILE          = 'Ha ocurrido un error en el proceso de creaci�n del archivo de infracciones.';
    ERROR_SAVING_TRX_FILE           = 'Ha ocurrido un error en el proceso de creaci�n del archivo de transitos.';
    ERROR_SAVING_FILE_DATA          = 'Se ha producido un error al guardar la informaci�n del archivo generado.';
    ERROR_ENDING_OPERATION          = 'Se ha producido un error al actualizar el Log de Operaciones.';
    ERROR_REFRESHING_DATES          = 'No se pueden recalcular las fechas. La interfaz se cerrar�.' + CRLF + 'Por favor, reinicie la aplicaci�n antes de continuar.';
    ERROR_INFRACTIONS_LOST          = 'Ha fallado la recoleccion de infracciones a enviar.';

    MSG_DATE_INTERVAL_TOO_SHORT     = 'El intervalo seleccionado no cubre la totalidad de d�as sin procesar. Desea continuar?';
    MSG_PROCESS_CANCELLED           = 'Proceso cancelado por el usuario.';
    MSG_PROCESS_ERROR               = 'El proceso finaliz� con errores.';
    MSG_PROCESS_OK                  = 'El proceso finaliz� con �xito';

    function ValidarDestino: boolean;
    begin
        Result :=   (TRIM(txtDirectorioDestino.Text) <> '')
                AND (DirectoryExists(GoodDir(TRIM(txtDirectorioDestino.Text))));
    end;

var
    FechaArchivoEnProceso   : TDateTime;
    FechaInfraccionEnProceso: TDateTime;
    CodigoOperacion         : Integer;
    descError               : AnsiString;
    OperacionesList         : TStringList;
    ArchivoInfraccion ,
    ArchivoTransitos        : AnsiString;
begin
    // Valido la fecha...
    if not ValidateControls([txtDirectorioDestino, deFechaHasta, deFechaHasta],[(ValidarDestino),(deFechaHasta.Date <= FHoy) AND (deFechaHasta.Date >= deFechaDesde.Date), (NOT(DayofTheWeek(deFechaHasta.Date) IN [6,7]))],Caption,
        [ERROR_INVALID_PATH,Format(ERROR_INVALID_DATE,[FormatDateTime(FORMATO_FECHA,deFechaDesde.Date), FormatDateTime(FORMATO_FECHA, FHoy)]), ERROR_DATE_IS_WEEKEND ]) then Exit;;
    // Si el operador selecciono una fecha pasada como fechahasta
    if deFechaHasta.Date < FHoy then begin
        // Consulto al operador si el intervalo seleccionado es correcto
        if MsgBox(MSG_DATE_INTERVAL_TOO_SHORT, caption, MB_ICONQUESTION + MB_YESNO) = mrNo then Exit;
    end;
    FGuardeArchivo  := False;
    FPrimerPRoceso := 0;
    FUltimoProceso := 0;
    deFechaHasta.Enabled := False;
    FCancelar := False;
    FProcesando := True;
    HabilitarBotones;
    OperacionesList     := TStringList.Create;
    pb_Progreso.Max := (DaysBetween(deFechaHasta.Date, deFechaDesde.Date) + 1) * 12;
    FechaArchivoEnProceso := deFechaDesde.Date;
    Application.ProcessMessages;
    try
        // Por cada FECHA entre Lunes y Viernes
        While  (FechaArchivoEnProceso  <=  deFechaHasta.Date) and (not FCancelar) do begin
        // no me preocupo por los feriados porque los va a procesar como debe
        // ya que se procesan al otro dia como si no hubiera sido feriado
            pb_Progreso.StepIt;
            // Armo el encabezado para los errores...
            FErrorHeader := Format( ERROR_HEADER_FOR_DATE, [FormatDateTime(FORMATO_FECHA,FechaArchivoEnProceso)]);
            // Creo nombres de archivos y limpio las string list...
            ArchivoInfraccion := CrearNombreArchivoInfraccion(FechaArchivoEnProceso);
            ArchivoTransitos := CrearNombreArchivoTransitos(FechaArchivoEnProceso);
            //0. Abro el Log de Operaciones
            if not RegistrarOperacionEnLogInterface(DMCOnnections.BaseCAC, RO_MOD_INTERFAZ_SALIDA_INFRACCIONES_CLEARING_BHTU,
              '', UsuarioSistema, '', False,False, FechaArchivoEnProceso, 0, CodigoOperacion, descError) then begin
                MsgBoxErr(ERROR_REGISTER_OPERATION_LOG, FErrorHeader + descError, Caption, MB_ICONERROR);
                Exit;
            end;
            if FPrimerPRoceso = 0 then FPrimerProceso := CodigoOperacion;
            FUltimoProceso := CodigoOperacion;
            pb_Progreso.StepIt;

            //1. Genero la temporal ( PrepararProcesoEnvioInfraccionesClearingBHTU )
            try
                spPrepararProcesoEnvioInfraccionesClearingBHTU.ExecProc;
            except
                on e:exception do begin
                    MsgBoxErr(ERROR_STARTING_OPERATION, FErrorHeader + e.Message, Caption, MB_ICONERROR);
                    Exit;
                end;
            end;
            pb_Progreso.StepIt;

            // Primero calcula la fecha de las infracciones a tomar, de acuerdo a la tabla de relaciones de fechas especificada.
            FechaInfraccionEnProceso := CalcularFechaInfraccion(FechaArchivoEnProceso);

            //2. Cargo los datos en la temporal  ( ObtenerInfraccionesAEnviarClearingBHTU )
            if not ObtenerInfraccionesAEnviar(CodigoOperacion, FechaInfraccionEnProceso, descError) then begin
                MsgBoxErr(ERROR_RETRIEVING_DATA, FErrorHeader + descError, Caption, MB_ICONERROR);
                Exit;
            end;
            if FCancelar then Continue; // como en el paso anterior pude haber cancelado por la mitas, verifico esto antes de seguir...
            pb_Progreso.StepIt;
            //3. Levanto los transitos de esas infracciones (joineando a la temporal) a otra temporal
            if not ObtenerTransitosAEnviar(CodigoOperacion, descError) then begin
                MsgBoxErr(ERROR_RETRIEVING_DATA, FErrorHeader + descError, Caption, MB_ICONERROR);
                Exit;
            end;
            if FCancelar then Continue; // como en el paso anterior pude haber cancelado por la mitas, verifico esto antes de seguir...
            pb_Progreso.StepIt;

            //4. Genero el archivo de infracciones
            if not GuardarArchivoInfracciones(ArchivoInfraccion, descError) then begin
                if trim(descError) = '' then  MsgBoxErr(ERROR_SAVING_INFR_FILE, FErrorHeader + ERROR_INFRACTIONS_LOST, Caption, MB_ICONERROR)
                else  MsgBoxErr(ERROR_SAVING_INFR_FILE, FErrorHeader + descError, Caption, MB_ICONERROR);
                Exit;
            end;
            pb_Progreso.StepIt;

            //5. Genero el Archivo de Transitos
            if not GuardarArchivoTransitos(ArchivoTransitos , descError) then begin
                MsgBoxErr(ERROR_SAVING_TRX_FILE, FErrorHeader + descError, Caption, MB_ICONERROR);
                Exit;
            end;
            pb_Progreso.StepIt;

            //   INICIO TRANSACCION
            //DMConnections.BaseCAC.BeginTrans;                                               //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('BEGIN TRAN fEnvioInfraccionesBHTU');         //SS_1385_NDR_20150922

            //6. Guardo las infracciones en la tabla ( SincronizarInfraccionesEnviadasBHTU    )
            //y   7. Guardo los Transitos en la tabla
            try
                spSincronizarInfraccionesEnviadasBHTU.ExecProc;
            except
                on e:exception do begin
                    //DMConnections.BaseCAC.RollbackTrans;                                                                              //SS_1385_NDR_20150922
                    DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fEnvioInfraccionesBHTU END');	    //SS_1385_NDR_20150922
                    MsgBoxErr(ERROR_SINCRONIZING_DATA, FErrorHeader + e.Message, Caption, MB_ICONERROR);
                    Exit;
                end;
            end;
            pb_Progreso.StepIt;

            //8. Marco las Infracciones con el Codigo de Proceso y Marco las Infracciones como Enviadas  y Elimino la temporal( MarcarInfraccionesEnvioClearingBHTU) (debo eliminarla porque estoy dentro de un ciclo...=)
            try
                spMarcarInfraccionesEnvioClearingBHTU.ExecProc;
            except
                on e:exception do begin
                    //DMConnections.BaseCAC.RollbackTrans;                                                                              //SS_1385_NDR_20150922
                    DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fEnvioInfraccionesBHTU END');	    //SS_1385_NDR_20150922
                    MsgBoxErr(ERROR_MARKING_DATA, FErrorHeader + e.Message, Caption, MB_ICONERROR);
                    Exit;
                end;
            end;
            pb_Progreso.StepIt;

            //9. Guardo los datos del Archivo generado.
            if not GuardarRegistroArchivo(CodigoOperacion, ArchivoInfraccion, ArchivoTransitos, FechaArchivoEnProceso, 0, descError) then begin
                //DMConnections.BaseCAC.RollbackTrans;                                                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fEnvioInfraccionesBHTU END');	    //SS_1385_NDR_20150922
                MsgBoxErr(ERROR_SAVING_FILE_DATA, FErrorHeader + descError, Caption, MB_ICONERROR);
                Exit;
            end;
            pb_Progreso.StepIt;

            //10. Cierro el Log de Operaciones
            if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, CodigoOperacion, 'Finaliz� OK!.', descError) then begin
                //DMConnections.BaseCAC.RollbackTrans;                                                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fEnvioInfraccionesBHTU END');	    //SS_1385_NDR_20150922
                MsgBoxErr(ERROR_ENDING_OPERATION, FErrorHeader + descError, Caption, MB_ICONERROR);
                Exit;
            end;
            pb_Progreso.StepIt;

            //   COMMIT DE LA TRANSACCION
            //DMConnections.BaseCAC.CommitTrans;                                                                //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('COMMIT TRAN fEnvioInfraccionesBHTU');					              //SS_1385_NDR_20150922
            // Cierro el ciclo...
            OperacionesList.Add( IntToStr(CodigoOperacion) ); // Agro la operacion completada en el stringlist de OPeraciones
            pb_Progreso.StepIt;

            FechaArchivoEnProceso := IncDay(FechaArchivoEnProceso,1); // Incremento la fecha de archivo en proceso
            if DayOfTheWeek(FechaArchivoEnProceso) = 6 then begin
                // si es s�bado, voy al proximo l�nes
                FechaArchivoEnProceso := IncDay(FechaArchivoEnProceso,2);
            end;
            Application.ProcessMessages;
        end; // while...
        pb_Progreso.Position := pb_Progreso.Max; // llevo la barra al final... por si le pifi� en el conteo...
    finally
        FProcesando := False;
        HabilitarBotones;
        LiberarTablasTemporales; //elimino las temporales. En condiciones normales ya no deberian existir, pero si se cancel� o se sali� con error, pueden a�n estar...
        if FCancelar then begin  // Si se cancel� aviso...
            MsgBox(MSG_PROCESS_CANCELLED, Caption, MB_ICONINFORMATION);
        end
        else begin
            // Si no se cancelo puede haber terminado bien o haber fallado... esto lo puedo saber consultando la fecha
            // si en fecha a procesar tengo un valor mayor que fecha hasta, salio normalmente, sino, hubo un fallo
            if FechaArchivoEnProceso > deFechaHasta.Date then begin
                // Todo Ok!
                MsgBox(MSG_PROCESS_OK, Caption, MB_ICONINFORMATION);
            end
            else begin
                // Hubo error!
                MsgBox(MSG_PROCESS_ERROR, Caption, MB_ICONWARNING);
            end;

        end;
        // Mostrar reporte si corresponde (aunque se haya cancelado, pueden haberse generado algunos archivos...)
        if (OperacionesList.Count > 0) and (FGuardeArchivo) then begin // si guard� algo en la liusta de operaicones completadas
            // Muestro el Reporte
            MostrarReporte(FPrimerProceso, FUltimoProceso);
        end;

        //OperacionesList.Free;
        if not ObtenerFechasIniciales(descError) then begin
            MsgBoxErr(ERROR_REFRESHING_DATES, descError, Caption, MB_ICONSTOP);
            Close;
        end;
    end;

end;

procedure TfrmEnvioInfraccionesClearingBHTU.btnCancelarClick(Sender: TObject);
begin
    FCancelar := True;
end;

{******************************** Function Header ******************************
Function Name: CalcularFechaInfraccion
Author : ndonadio
Date Created : 16/09/2005
Description : Calcula hasta que fecha tomar las infracciones dada la fecha de
                generacion del archivo, de acuerdo a lo especificado en el
                punto 2.3.1 Relacion de Fechas en el documento de especificaci�n
                Clearing de BHTU, rev. 3
Parameters : Fecha: TDateTime
Return Value : TDateTime
*******************************************************************************}
function TfrmEnvioInfraccionesClearingBHTU.CalcularFechaInfraccion(Fecha: TDateTime): TDateTime;
var
    FechaRespuesta: TDateTime;
begin
    CASE DayOfTheWeek(Fecha) OF
        1: begin // LUNES -> Lunes y Martes anteriores paso la fecha del martes.
                FechaRespuesta := IncDay(Fecha, -6)
           end;
        2: begin // MARTES -> Miercoles anterior
                FechaRespuesta := IncDay(Fecha, -6)
           end;
        3: begin // MIERCOLES -> Jueves anterior
                FechaRespuesta := IncDay(Fecha, -6)
           end;
        4: begin // JUEVES -> Viernes anterior
                FechaRespuesta := IncDay(Fecha, -6)
           end;
        5: begin // VIERNES -> Sabado y Domingo anteriores. Paso la fecha del Domingo.
                FechaRespuesta := IncDay(Fecha, -5)
           end;
    else
        FechaRespuesta := NULLDATE;
    end;
    Result := FechaRespuesta;
end;

{******************************** Function Header ******************************
Function Name: CrearNombreArchivoInfraccion
Author : ndonadio
Date Created : 16/09/2005
Description : Crea el nombre del arhcivo de infracciones para le fecha dada
Parameters : FechaArchivo: TDateTime
Return Value : AnsiString
*******************************************************************************}
function TfrmEnvioInfraccionesClearingBHTU.CrearNombreArchivoInfraccion(
  FechaArchivo: TDateTime): AnsiString;
const
    EXT             = '.dat';
    PREFIX          = 'infractores_%d_';
    FILEDATEFORMAT  = 'yyyymmdd';
begin
    //Result := GoodDir(txtDirectorioDestino.Text) + Format(PREFIX, [CODIGO_CN]) + FormatDateTime( FILEDATEFORMAT, FechaArchivo) + EXT;                 // SS_1147_CQU_20140408
    Result := GoodDir(txtDirectorioDestino.Text) + Format(PREFIX, [FCodigoConcesionariaNativa]) + FormatDateTime( FILEDATEFORMAT, FechaArchivo) + EXT;  // SS_1147_CQU_20140408
end;

{******************************** Function Header ******************************
Function Name: GuardarRegistroArchivo
Author : ndonadio
Date Created : 16/09/2005
Description : Genera na entrada en la tabla
                ArchivosInfraccionEnviadosClearingBHTU
Parameters : Operacion: integer; ArchivoInfraccion, ArchivoTransitos: AnsiString; var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmEnvioInfraccionesClearingBHTU.GuardarRegistroArchivo(Operacion: integer;
  ArchivoInfraccion, ArchivoTransitos: AnsiString; FechaArchivo: TDateTime; NumReprocesos: Integer; var Error: AnsiString): Boolean;
resourcestring
    ERROR_FALLO_INSERT  = 'No se pudo insertar el registro del archivo enviado.';
begin
    Result := False;
    try
        with spRegistrarArchivoEnvioInfraccionesBHTU do begin
            Parameters.ParamByName('@CodigoOperacion').Value := Operacion;
            Parameters.ParamByName('@FechaArchivo').Value := FechaArchivo;
            Parameters.ParamByName('@ArchivoInfraccion').Value := ExtractFileName(ArchivoInfraccion);
            Parameters.ParamByName('@ArchivoTransitos').Value := ExtractFileName(ArchivoTransitos);
            Parameters.ParamByName('@NumeroReproceso').Value := NumReprocesos;
            ExecProc;
            if  Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                Error := ERROR_FALLO_INSERT;
                Exit;
            end;
        end;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: CrearNombreArchivoTransitos
Author : ndonadio
Date Created : 16/09/2005
Description :   Genera el nombre del arhcivo de transitos...
Parameters : FechaArchivo: TDateTime
Return Value : AnsiString
*******************************************************************************}
function TfrmEnvioInfraccionesClearingBHTU.CrearNombreArchivoTransitos(FechaArchivo: TDateTime): AnsiString;
const
    EXT             = '.trx';
    PREFIX          = 'infractores_%d_';
    FILEDATEFORMAT  = 'yyyymmdd';
begin
    //Result := GoodDir(txtDirectorioDestino.Text) + Format(PREFIX, [CODIGO_CN]) + FormatDateTime( FILEDATEFORMAT, FechaArchivo) + EXT;                 // SS_1147_CQU_20140408
    Result := GoodDir(txtDirectorioDestino.Text) + Format(PREFIX, [FCodigoConcesionariaNativa]) + FormatDateTime( FILEDATEFORMAT, FechaArchivo) + EXT;  // SS_1147_CQU_20140408
end;

{******************************** Function Header ******************************
Function Name: GuardarArchivoInfracciones
Author : ndonadio
Date Created : 16/09/2005
Description :  Obtiene la lista de infracciones y la guarda en un archivo.
Parameters : NombreArchivo: AnsiString; Lista: TStringList; var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmEnvioInfraccionesClearingBHTU.GuardarArchivoInfracciones(NombreArchivo: AnsiString;
  var Error: AnsiString): Boolean;
var
    Lista: TStringList;
begin
    Result := False;
    Lista := TStringList.Create;
    Error := '';
    try
        try // obtengo la lista de infraccion
            spObtenerListaEnvioInfraccionesClearingBHTU.Close;
            spObtenerListaEnvioInfraccionesClearingBHTU.Open;
            // la meto en un string list
            while NOT spObtenerListaEnvioInfraccionesClearingBHTU.Eof do begin
                Lista.Add(spObtenerListaEnvioInfraccionesClearingBHTU.FieldByName('Campo').asString);
                spObtenerListaEnvioInfraccionesClearingBHTU.Next ;
            end;
            // grabo el string list como el archivo que corresponde
            if Lista.Count > 0 then begin
                Lista.SaveToFile( NombreArchivo );
                FGuardeArchivo := True;
            end;
            Result := True;
        except
            on e:exception do begin
                Error := e.Message;
                Exit;
            end;
        end;
    finally
       Lista.Free;
       spObtenerListaEnvioInfraccionesClearingBHTU.Close;
    end;
end;


{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 16/09/2005
Description : Habilita o deshabilita botones, paneles, etc. de acuerdo al valor
                de la variable FProcesando.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmEnvioInfraccionesClearingBHTU.HabilitarBotones;
begin

    btnProcesar.Enabled := NOT FProcesando;
    btnCancelar.Enabled := FProcesando;
    btnSalir.Enabled    := NOT FProcesando;

    btnProcesar.Enabled := NOT FProcesando;
    btnCancelar.Visible := FProcesando;
    btnSalir.Visible    := NOT FProcesando;

    deFechaHasta.Enabled := NOT FProcesando;
    pnlAvance.Visible := FProcesando;

    lblDetalleProgreso.Visible := FALSE; 
end;

{******************************** Function Header ******************************
Function Name: LiberarTablasTemporales
Author : ndonadio
Date Created : 19/09/2005
Description : Libero las tablas temporales
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmEnvioInfraccionesClearingBHTU.LiberarTablasTemporales;
resourcestring
    ERROR_DELETING_TEMP_TABLES = 'No se pueden eliminar las tablas temporales.' + CRLF + 'Por favor salga de la aplicaci�n para completar el proceso de eliminaci�n.';
begin
    try
        spEliminarTablasTemporales.ExecProc;
    except
        on e:exception do begin
            MsgBoxErr(ERROR_DELETING_TEMP_TABLES, e.Message, Caption, MB_ICONWARNING);
            // cierro la aplicacion de prepo...
            Application.Terminate;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ObtenerInfraccionesAEnviar
Author : ndonadio
Date Created : 19/09/2005
Description :   Obtiene las infracciones que se deben enviar y las inserta en la
                tabla temporal que corresponda....
Parameters : CodigoOperacion: Integer; FechaInfraccion: TDateTime; var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmEnvioInfraccionesClearingBHTU.ObtenerInfraccionesAEnviar(CodigoOperacion: Integer;
  FechaInfraccion: TDateTime; var Error: AnsiString): boolean;
resourcestring
    ERROR_SP    = 'Error al ejecutar el proceso de obtenci�n de datos de Infracciones';
begin
    Result := False;
    // Los Parametros no cambian por eso los asigno fuera del loop
    spObtenerInfraccionesAEnviarClearingBHTU.Parameters.ParamByName('@FechaEnProceso').Value := FechaInfraccion;
    spObtenerInfraccionesAEnviarClearingBHTU.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacion;
    spObtenerInfraccionesAEnviarClearingBHTU.Parameters.ParamByName('@CantidadProcesada').Value := 0;
    while (not FCancelar) do begin // cargo de a 500...
        try
            // Ejecuto el SP
            spObtenerInfraccionesAEnviarClearingBHTU.ExecProc;
            // Si no hay error
            if spObtenerInfraccionesAEnviarClearingBHTU.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                // Error al procesar
                Error := ERROR_SP;
                Exit;
            end;
            // Evaluo la cantidad procesada...
            if spObtenerInfraccionesAEnviarClearingBHTU.Parameters.ParamByName('@CantidadProcesada').Value = 0 then begin
                // SI la cantidad es CERO Terminamos...
                Break;
            end;
        except
            on e:exception do begin
                Error := e.Message;
                Exit;
            end;
        end;
    end;
    Result := True;

end;

{******************************** Function Header ******************************
Function Name: GuardarArchivoTransitos
Author : ndonadio
Date Created : 19/09/2005
Description :   Guarda el archivo de transitos, almacenando primero los trx en
                un stringlist y luego grabandolo....
Parameters : NombreArchivo: AnsiString; var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmEnvioInfraccionesClearingBHTU.GuardarArchivoTransitos(NombreArchivo: AnsiString;
  var Error: AnsiString): Boolean;
var
    Lista: TStringList;
begin
    Result := False;
    Lista := TStringList.Create;
    try
        try // obtengo la lista de infraccion
            spObtenerListaEnvioTransitosClearingBHTU.Close;
            spObtenerListaEnvioTransitosClearingBHTU.Open;
            // la meto en un string list
            while NOT spObtenerListaEnvioTransitosClearingBHTU.Eof do begin
                Lista.Add(spObtenerListaEnvioTransitosClearingBHTU.FieldByName('Campo').asString);
                spObtenerListaEnvioTransitosClearingBHTU.Next ;
            end;
            // grabo el string list como el archivo que corresponde
            if Lista.Count > 0 then Lista.SaveToFile( NombreArchivo );
            Result := True;
        except
            on e:exception do begin
                Error := e.Message;
                Exit;
            end;
        end;
    finally
       Lista.Free;
       spObtenerListaEnvioTransitosClearingBHTU.Close;
    end;
end;

{******************************** Function Header ******************************
Function Name: ObtenerTransitosAEnviar
Author : ndonadio
Date Created : 19/09/2005
Description : Obtiene los transitos a enviar basandose en la temporal de infracciones
Parameters : CodigoOperacion: Integer; var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmEnvioInfraccionesClearingBHTU.ObtenerTransitosAEnviar(CodigoOperacion: Integer;
  var Error: AnsiString): boolean;
resourcestring
    ERROR_SP    = 'Error al ejecutar el proceso de obtenci�n de datos de Transitos';
begin
    Result := False;
    spObtenerTransitosAEnviarClearingBHTU.Parameters.ParamByName('@CantidadProcesada').Value := 0;
    while (not FCancelar) do begin // cargo de a 500...
        try
            // Ejecuto el SP
            spObtenerTransitosAEnviarClearingBHTU.ExecProc;
            // Evaluo la cantidad procesada...
            if spObtenerTransitosAEnviarClearingBHTU.Parameters.ParamByName('@CantidadProcesada').Value = 0 then begin
                // SI la cantidad es CERO Terminamos...
                Break;
            end;
        except
            on e:exception do begin
                Error := ERROR_SP + CRLF + CRLF + e.Message;
                Exit;
            end;
        end;
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: ObtenerFechasIniciales
Author : ndonadio
Date Created : 27/09/2005
Description :  Obtiene las fechas iniciales actualizadas para rellenar los DateEdit
Parameters : var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmEnvioInfraccionesClearingBHTU.ObtenerFechasIniciales(var Error: AnsiString): boolean;
resourcestring
    ERROR_CANNOT_INITIALIZE     = 'No se puede inicializar la interfaz.';
    ERROR_CANNOT_OBTAIN_LAST_FILE_DATE = 'No se puede obtener la fecha del �ltimo env�o.';
begin
    Result := False;
    try
        // Recupero la ultima operacion para ver que dia se proceso...
        FFechaUltimoProceso := QueryGetValueDateTime(DMConnections.BaseCAC, 'SELECT dbo.ObtenerUltimoEnvioInfraccionesClearingBHTU()');
    except
        on e:exception do begin
            Error := ERROR_CANNOT_OBTAIN_LAST_FILE_DATE + CRLF  + e.Message;
            Exit;
        end;
    end;
    // Seteo el d�a minimo de procesamiento.
    // si el ultimo fue un viernes, paso al lunes
    if DayOfTheWeek(FFechaUltimoProceso+1) >= 6 then deFechaDesde.Date := FFechaUltimoProceso + 3
    else deFechaDesde.Date := FFechaUltimoProceso + 1; // sino al dia siguiente
    deFechaHasta.Date := FHoy;
    deFechaHasta.Enabled := FFechaUltimoProceso < FHoy;
    btnProcesar.Enabled := FFechaUltimoProceso < FHoy;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: MostrarReporte
Author : ndonadio
Date Created : 27/09/2005
Description : Muestra ek reporte
Parameters : Primera, Ultima: Integer
Return Value : None
*******************************************************************************}
procedure TfrmEnvioInfraccionesClearingBHTU.MostrarReporte(Primera, Ultima: Integer);
resourcestring
    ERROR_CANT_SHOW_REPORT  = 'No se puede mostrar el reporte.';
var
    f: TfrmReporteEnvioInfraccionesBHTU;
    descError: AnsiString;
begin
    try
        Application.CreateForm(TfrmReporteEnvioInfraccionesBHTU, f);
    except
        on e:exception do begin
            MsgBoxErr(ERROR_CANT_SHOW_REPORT, e.Message, caption, MB_ICONERROR);
            Exit;
        end;
    end;
    if not f.MostrarReporte(Primera, Ultima, Caption, descError) then begin
        if trim(descError) <> '' then
            MsgBoxErr(ERROR_CANT_SHOW_REPORT, descError, Caption, MB_ICONERROR)
    end;
    f.Release;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : ndonadio
Date Created : 27/09/2005
Description :   Muestra la ayuda sobre la interfaz
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmEnvioInfraccionesClearingBHTU.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_AYUDA   = ' ' + CRLF +
                'El Archivo de Infracciones es ' + CRLF +
                'utilizado por el ESTABLECIMIENTO para informar '+ CRLF +
                'a las Concesionarias adheridas acerca de sus infractores' + CRLF +
                'de forma que se puedan asignar correctamente los BHTU.' + CRLF +
                ' ' + CRLF +
                'Este proceso forma parte del Clearing de BHTU ' + CRLF +
                ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_AYUDA, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : ndonadio
Date Created : 27/09/2005
Description : Cambia el cursor al pasar sobre el pnl ayuda
Parameters : Sender: TObject; Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TfrmEnvioInfraccionesClearingBHTU.ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

end.

