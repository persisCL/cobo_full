{------------------------------------------------------------------------------
 File Name: FrmRecepcionUniversoBcoChile.pas
 Author:    jjofre
 Date Created:   20 de Mayo de 2010
 Description:  Modulo de la interfaz Banco de Chile - Recepci�n de universo

 Revision : 1
     Author : jjofre
     Date : 28 de mayo de 2010
     Description : se modifica la funcion ParseUniversoPACLine para adaptarlo a
                nuevo requerimiento de banco de chile

 Revision : 2

    Author : jjofre
    Date : 02 de Junio de 2010
    Description : se cambia el reporte Universo antiguo por formulario
                   frmRptRecepcionUniversoBancoChile

  Revision : 3
    Author : jjofre
    Date : 09 de Junio de 2010
    Description : Se modifican las siguientes funciones para procesar el el log
                de errores y reporte de finalizacion, los convenios duplicados
                -BtnProcesarClick
                -RegistrarOperacion
                -ParseUniversoPACLine

  Revision : 4
    Author : jjofre
    Date : 14 de Junio de 2010
    Descripcion :  se modifican las siguientes funciones para para validar los
                registros de control, nombres de archivos.
                -Inicializar
                -ParseUniversoPACLine
                -BtnProcesarClick
                -AnalizarUniversoTXT
    Revision : 5
    Author : jjofre
    Date : 29 de Junio de 2010
    Descripcion :  se modifican las siguientes funciones por cambios en requerimientos
                banco de chile
                -ParseUniversoPACLine

    Firma       : SS-884-NDR-20120621
    Description : Corrige error al comparar PAC con Universo, daba por validos
                entradas invalidas.
                CN Siempre envia los convenios al banco de la forma 00012345678KNNN
                donde 000 es un relleno 12.345.678-K es el rut y NNN es el ident del convenio 001,002,003 etc
                El primer caracter SIEMPRE es un '0'. Luego cuando el archivo vuelve se cambia el primer caracter '0'
                por un '001' quedando 0010012345678KNNN los 17 caracteres que se usan en CN como numeroconvenio
                EL problema es que a veces desde el banco el primer caracter NO ES UN '0'. Como antes se cambiaba
                arbitrariamente el primer caracter por '001' (sin discriminar si era el '0' enviado por CN) encontraba
                convenios que en realidad no eran los que el banco mandaba
                Si desde el banco, el dato que llega, en el primer caracter (de los 15) no contiene un '0', entonces
                esa linea es rechazada por "convenio enviado por error desde el banco "


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

Autor       :   Claudio Quezada
Fecha       :   24-03-2015
Firma       :   SS_1147_CQU_20150324
Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1147_NDR_20150520
Descripcion : Corregir proceso de Recepcion Universo Banco de Chile
-------------------------------------------------------------------------------}
unit FrmRecepcionUniversoBcoChile;

interface

uses
  DMConnection,
  Util,
  UtilDB,
  UtilProc,
  PeaTypes,
  ConstParametrosGenerales,
  PeaProcs,
  ComunesInterfaces,
  frmRptRecepcionUniversoBancoChile,   //REV.2
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx,DPSControls, ppDB, ppTxPipe, StrUtils, ppParameter,
  ppModule, raCodMod, ppCtrls, ppBands, ppReport, ppPrnabl, ppClass, ppStrtch,
  ppSubRpt, ppCache, ppComm, ppRelatv, ppProd, UtilRB, RStrings, RBSetup,
  DBClient, ppDBPipe, Math;

type

  TUniversoPACBancoChileRecord = record
  	NumeroConvenio  : string;
    CodigoBancoSBEI : integer;
    FechaInicio     : TDateTime;
   end;


  TFRecepcionUniversoBcoChile = class(TForm)
    Bevel1: TBevel;
    btnAbrirArchivo: TSpeedButton;
    Label2: TLabel;
    lblReferencia: TLabel;
    edOrigen: TEdit;
    OpenDialog: TOpenDialog;
    spCopiarDatosConveniosAUniversoPAC: TADOStoredProc;
    spObtenerConveniosPacNoVerificados: TADOStoredProc;
  	rbiListado: TRBInterface;
    ppReporteRecepUniversoBancoChile: TppReport;
    ppDetailBand1: TppDetailBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppHeaderBand2: TppHeaderBand;
    ppLine2: TppLine;
    ppLabel1: TppLabel;
    lbl_usuario: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppImage2: TppImage;
    ppDetailBand2: TppDetailBand;
    ppParameterList1: TppParameterList;
    spObtenerUltimoCodigoConvenio: TADOStoredProc;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    cdsErrores: TClientDataSet;
    qryBanco: TADOQuery;
    ppdbErrores: TppDBPipeline;
    dsErrores: TDataSource;
    ppDBText3: TppDBText;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    spAgregarErrorInterfaz: TADOStoredProc;
    spObtenerDatosProcesoUniversoBancoChile: TADOStoredProc;
    spActualizarLogOperacionesRegistrosProcesados: TADOStoredProc;
    spBorrarUniversoPACBancoChile: TADOStoredProc;
    cdsUniversoPACBancoChile: TClientDataSet;
    spAgregarUniversoPACBancoChile: TADOStoredProc;
    spCompararUniversoPACUniversoPACBancoChile: TADOStoredProc;
    spAgragarUniversoPACBC: TADOStoredProc;
    spEliminarMediopagoPACBC: TADOStoredProc;
    spBorrarUniversoPAC: TADOStoredProc;
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure edOrigenChange(Sender: TObject);
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure rbiListadoExecute(Sender: TObject; var Cancelled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    FCodigoOperacion : integer;
  	FDetenerImportacion: boolean;
  	FUniversoTXT : TStringList;
    FErrores : TStringList;
  	FErrorMsg : string;
    FCantConvenios : integer;
    FVerReporteErrores: boolean;
  	FBancoChile_Directorio_Universo : AnsiString;
    FBancoChile_Directorio_Errores : Ansistring;
    FSTD_DirectorioUniversoProcesado: AnsiString;
    //REV.4
    FBancoChile_Codigo_Convenio: AnsiString;
    FBancoChile_Codigo_Empresa: AnsiString;
    FBancoChile_Codigo_Convenio_aux: AnsiString;
    FBancoChile_Codigo_Empresa_aux: AnsiString;
    FMandatosMayoraArchivo : String;
    FArchivoMayoraMandato  : String;
    FDiferenciaPermitidaArchVSBase   : integer;
    FDiferenciaPermitidaBaseVSArch   : integer;
    FCodigoNativa : Integer;  // SS_1147_CQU_20150324
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function  BorrarUniversoPAC : boolean;
    function  BorrarUniversoPACBancoChile : boolean;
    function  CopiarConveniosAUniversoPAC : boolean;
    function  AnalizarUniversoTxt : boolean;
    function  CargarUniversoPACBancoChile : boolean;
    function  CompararUniversoPACUniversoPACBancoChile : boolean;
    function  VerificarUniverso : boolean;
  	function  ProcesarNoExistentesUniversoPAC : boolean;
    function  ParseUniversoPACLine( sline : string; var UniversoRecord : TUniversoPACBancoChileRecord; var sParseError : string ) : boolean;
  	procedure GenerarReportesErrores;
  	function  RegistrarOperacion : boolean;
    function  ConfirmaCantidades(Lineas:integer): Boolean;
    procedure AgregarErrorInterfaz(mDescripcionError: String);
    function ObtenerDatosProcesoUniversoBancoChile(CodigoOperacionInterfase: integer; var LineasArchivo: integer;
                var ConveniosDuplicados: integer; var RegistrosExito: integer; var RegistrosError: integer): boolean;
    function  MsgProcesoFinalizadoBancoChile(sMensajeOK, sMensajeError, sTitulo: AnsiString; RLineas, RDuplicados, RExito, RError: integer): boolean;
  public
  	function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
  	{ Public declarations }
  end;

var
  FRecepcionUniversoBcoChile: TFRecepcionUniversoBcoChile;

implementation

{$R *.dfm}

const
	  RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_PAC_BCHILE			= 103;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    jjofre
  Date Created: 20/05/2010
  Description: M�dulo de inicializaci�n de este formulario
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRecepcionUniversoBcoChile.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : jjofre
    Date Created : 20/05/2010
    Description : Se Obtienen los Parametros Generales que se utilizaran en el formulario
                  al inicializar y se verifica que los valores obtenidos sean validos.
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        BANCOCHILE_DIRECTORIO_UNIVERSO    = 'BancoChile_Directorio_Universo';
        BANCOCHILE_DIRECTORIO_ERRORES     = 'BancoChile_Directorio_Errores';
        BCO_DIRECTORIOUNIVERSOPROCESADO   = 'BCO_DirectorioUniversoProcesado';
        BANCODECHILE_CODIGO_CONVENIO    = 'BancoChile_Codigo_Convenio';
        BANCODECHILE_CODIGO_EMPRESA     = 'BancoChile_Codigo_Empresa';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la Concesionaria Nativa                  // SS_1147_CQU_20150324
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150324

                //Se Obtiene el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BANCOCHILE_DIRECTORIO_UNIVERSO, FBancoChile_Directorio_Universo) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BANCOCHILE_DIRECTORIO_UNIVERSO;
                    Result := False;
                    Exit;
                end;

                //Se Verifica que sea v�lido
                FBancoChile_Directorio_Universo := GoodDir(FBancoChile_Directorio_Universo);
                if  not DirectoryExists(FBancoChile_Directorio_Universo) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FBancoChile_Directorio_Universo;
                    Result := False;
                    Exit;
                end;

                //Se Obtiene el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BANCOCHILE_DIRECTORIO_ERRORES , FBancoChile_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BANCOCHILE_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;

                //Se Verifica que sea v�lido
                FBancoChile_Directorio_Errores := GoodDir(FBancoChile_Directorio_Errores);
                if  not DirectoryExists(FBancoChile_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FBancoChile_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Se Obtiene el Parametro General
                ObtenerParametroGeneral(DMConnections.BaseCAC, BCO_DIRECTORIOUNIVERSOPROCESADO, FSTD_DirectorioUniversoProcesado);

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_MAX_DIF_PAC_BASEVSARCH',FDiferenciaPermitidaBaseVSArch) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'CANT_MAX_DIF_PAC_BASEVSARCH';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_MAX_DIF_PAC_ARCHVSBASE',FDiferenciaPermitidaArchVSBase) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'CANT_MAX_DIF_PAC_ARCHVSBASE';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'MSG_DIF_ARCHIVO_MAYOR_A_MANDATOS',FArchivoMayoraMandato) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'MSG_DIF_ARCHIVO_MAYOR_A_MANDATOS';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'MSG_DIF_MANDATOS_MAYOR_A_ARCHIVO',FMandatosMayoraArchivo) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'MSG_DIF_MANDATOS_MAYOR_A_ARCHIVO';
                    Result := False;
                    Exit;
                end;

                     //Se obtiene el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BANCODECHILE_CODIGO_CONVENIO , FBancoChile_Codigo_Convenio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BANCODECHILE_CODIGO_CONVENIO;
                    Result := False;
                    Exit;
                end;
                //Se Verifica que sea v�lido
                if not (FBancoChile_Codigo_Convenio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + BANCODECHILE_CODIGO_CONVENIO;
                   Result := False;
                   Exit;
                end;

                //Se obtiene el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BANCODECHILE_CODIGO_EMPRESA , FBancoChile_Codigo_Empresa) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BANCODECHILE_CODIGO_EMPRESA;
                    Result := False;
                    Exit;
                end;
                //Se Verifica que sea v�lido
                if not (FBancoChile_Codigo_Empresa <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + BANCODECHILE_CODIGO_EMPRESA;
                   Result := False;
                   Exit;
                end;

                FBancoChile_Codigo_Convenio_aux := PadL(FBancoChile_Codigo_Convenio,3,'0');
                FBancoChile_Codigo_Empresa_aux := PadL(FBancoChile_Codigo_Empresa,3,'0');
            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si la verificacion de parametros generales no es exitosa
            if (Result = False) then begin
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    //Se elije el modo en que si visualizara la ventana
	if not MDIChild then begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	CenterForm(Self);

	try
        DMConnections.BaseCAC.Connected := True;
        cdsUniversoPACBancoChile.CreateDataSet;
        cdsUniversoPACBancoChile.LogChanges := False;
       	Result := DMConnections.BaseCAC.Connected and VerificarParametrosGenerales;
	except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;

    Caption := AnsiReplaceStr(txtCaption, '&', '');
    FCantConvenios := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT Max(CodigoConvenio) From Convenio WITH (NOLOCK)');
	btnCancelar.Enabled := False;
	btnProcesar.Enabled := False;
	pnlAvance.Visible := False;
	lblReferencia.Caption := '';
    FCodigoOperacion := 0;

    if FCodigoNativa = CODIGO_VS                                // SS_1147_CQU_20150324
    then OpenDialog.Filter := 'Archivos de Texto|bajapc_*.*'    // SS_1147_CQU_20150324
    else OpenDialog.Filter := 'Archivos de Texto|*.TXT';        // SS_1147_CQU_20150324
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : jjofre
Date Created : 20/05/2010
Description : Mensaje de Ayuda
*******************************************************************************}
procedure TFRecepcionUniversoBcoChile.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_UNIVERSO        = ' ' + CRLF +
                          'El archivo de Universo' + CRLF +
                          'es enviado por BANCO DE CHILE al ESTABLECIMIENTO' + CRLF +
                          'segun la periodicidad convenida y contiene' + CRLF +
                          'el detalle de todos los mandantes que tienen' + CRLF +
                          'medio de pago PAC de BANCO DE CHILE' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: PACU_CEM_DDMMAAAA.TXT' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para actualizar los datos de' + CRLF +
                          'los mandantes en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    MsgBoxBalloon(MSG_UNIVERSO , Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : jjofre
Date Created : 20/05/2010
*******************************************************************************}
procedure TFRecepcionUniversoBcoChile.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoClick
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Busca el Archivo del PACU_CEM.TXT
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoBcoChile.btnAbrirArchivoClick(Sender: TObject);
resourcestring
	MSG_ERROR_FILE_DOES_NOT_EXIST = 'El archivo %s no existe';
begin
    OpenDialog.InitialDir := FBancoChile_Directorio_Universo;
    if OpenDialog.execute then begin
        edOrigen.text:=UpperCase(opendialog.filename);

		if FileExists(edOrigen.text) then begin
            btnProcesar.Enabled := True;
            if FSTD_DirectorioUniversoProcesado <> '' then begin
                if RightStr(FSTD_DirectorioUniversoProcesado,1) = '\' then  FSTD_DirectorioUniversoProcesado := FSTD_DirectorioUniversoProcesado + ExtractFileName(edOrigen.text)
                else FSTD_DirectorioUniversoProcesado := FSTD_DirectorioUniversoProcesado + '\' + ExtractFileName(edOrigen.Text);
            end;
        end
    	else begin
        	btnProcesar.Enabled := False;
        	MsgBox(Format(MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigen.text]), Caption, MB_ICONERROR);
    	end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: edOrigenChange
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Se permite procesar si el path pertenece a un archivo que existe
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoBcoChile.edOrigenChange(Sender: TObject);
begin
   btnProcesar.Enabled := FileExists( edOrigen.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: AnalizarUniversoTxt
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Hace un prean�lisis del Universo antes de procesarlo
               Se abre la tabla cliente para cargar los datos al archivo de universo
-----------------------------------------------------------------------------}
function  TFRecepcionUniversoBcoChile.AnalizarUniversoTxt : boolean;
resourcestring
  	MSG_ANALIZING_UNIVERSE_FILE	 = 'Analizando Archivo de Universo - Cantidad de lineas : %d';
    MSG_UNIVERSE_FILE_HAS_ERRORS = 'El archivo de Universo contiene errores'#10#13'L�nea : %d';
    MSG_UNIVERSE_LOG_ERRORS      = 'El archivo de Universo contiene errores, L�nea : %d ,';
    MSG_ERROR					 = 'Error';
    MSG_ERROR_FINALIZACION       = 'Proceso Finalizado';
var
  	nNroLineaScript, nLineasScript :integer;
    FUniversoRecord : TUniversoPACBancoChileRecord;
  	sParseError , FerrorLog	: string;
begin
	Screen.Cursor := crHourglass;

    if not cdsUniversoPACBancoChile.Active then cdsUniversoPACBancoChile.Active := True;
    cdsUniversoPACBancoChile.EmptyDataSet;

	nLineasScript := FUniversoTXT.Count-1;
	lblReferencia.Caption := Format( MSG_ANALIZING_UNIVERSE_FILE, [nLineasScript] );
	pbProgreso.Position := 0;
	pbProgreso.Max := FUniversoTXT.Count-1;
	pnlAvance.Visible := True;
    nNroLineaScript := 0;

    while (nNroLineaScript < nLineasScript) and
      (not FDetenerImportacion) and
        (FErrorMsg = '') do begin

        if not ParseUniversoPACLine(FUniversoTXT[nNroLineaScript], FUniversoRecord, sParseError) then begin
            // Si encuentra un error termina
            FErrorMsg := Format(MSG_UNIVERSE_FILE_HAS_ERRORS, [(nNroLineaScript + 1)]) + #10#13 + sParseError;
            MsgBox(FErrorMsg, Caption, MB_ICONERROR);
            Screen.Cursor := crDefault;
            FerrorLog := Format(MSG_UNIVERSE_LOG_ERRORS, [(nNroLineaScript + 1)]) +  sParseError ;
            ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,FerrorLog,sParseError);
            result := False;
            Exit;
        end;

        Inc( nNroLineaScript );
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

	result := not FDetenerImportacion;
	Screen.Cursor := crDefault;
end;


{-----------------------------------------------------------------------------
  Function Name: ConfirmaCantidades
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Consulta al usuario si quiere continuar o no,
                presentandole un resumen de las cantidades a procesar
                y de las cantidades existentes.
              -Se ponen mensajes de advertencia o error si la diferencia es mucha entre el total mandatos y el archivo
              -Por solicitud del cliente, se permite continuar aunque aparezca el mensaje de error.
-----------------------------------------------------------------------------}
function TFRecepcionUniversoBcoChile.ConfirmaCantidades(Lineas: integer): Boolean;
resourcestring
    MSG_CONFIRMATION = 'Actualmente existen %d Mandatos Confirmados '+crlf+'y %d Mandatos Pendientes, que hacen'+crlf+
                       'un total de %d Mandatos PAC Existentes.'+crlf+crlf+
                       'El Archivo seleccionado contiene %d Mandatos a procesar.';
    MSG_KEEP_PROCESSING = 'Desea continuar con el procesamiento del archivo?';
    MSG_TITLE = 'Confirme Recepci�n de Universo PAC';
var
    MandatosPendientes: integer;
    MandatosConfirmados: integer;
    MandatosExistentes: integer;
    MensajeCompleto : String;
begin
    Dec(Lineas);
    //Se Obtienen las cantidad de mandatos Existentes/Pendientes y Confirmados
    MandatosExistentes  :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[7,0]));
    MandatosPendientes  :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[8,0]));
    MandatosConfirmados :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[9,0]));

    //Se Arma el mensaje completo
    MensajeCompleto := Format(MSG_CONFIRMATION,[MandatosConfirmados,MandatosPendientes,MandatosExistentes, Lineas]);

    // se comparan las cantidades del archivo y la tabla que no se pasen de ciertas diferencias determinadas por parametro
    // si se superan los par�metros, se solicita confirmaci�n o se cancela el proceso.

    if MandatosExistentes > Lineas then begin
        if MandatosExistentes - Lineas > FDiferenciaPermitidaBaseVSArch then begin
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + FMandatosMayoraArchivo;
        end else begin
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING;
        end;
        Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
    end;

    if Lineas > MandatosExistentes then begin
        if Lineas - MandatosExistentes > FDiferenciaPermitidaArchVSBase then begin
            MensajeCompleto := MensajeCompleto +crlf+crlf+ FArchivoMayoraMandato;
        end else begin
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING ;
        end;
        Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
    end;
    
    if Lineas = MandatosExistentes then begin
        MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING;
        Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Registra la Operaci�n en el LogOperacionesInterface
-----------------------------------------------------------------------------}
function TFRecepcionUniversoBcoChile.RegistrarOperacion : boolean;
resourcestring
    MSG_UNIVERSE_IMPORT = 'Importaci�n Universo PAC - Cantidad de Errores : %d';
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
	sDescrip : string;
	DescError : string;
begin

    sDescrip := Format( MSG_UNIVERSE_IMPORT, [FErrores.Count] );
    result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_PAC_BCHILE, ExtractFileName(edOrigen.text), UsuarioSistema, sDescrip, True, False, NowBase(DMConnections.BaseCAC), 0,FCodigoOperacion, (FUniversoTXT.Count-1), 0, DescError);
    if not result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
    
end;

{-----------------------------------------------------------------------------
  Function Name: BorrarUniversoPAC
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Borra los datos viejos de UniversoPACCN por Bloques
-----------------------------------------------------------------------------}
function TFRecepcionUniversoBcoChile.BorrarUniversoPAC : boolean;
resourcestring
    MSG_DELETEING_OLD_UNIVERSE			= 'Eliminando Universo anterior';
  	MSG_ERROR_DELETEING_OLD_UNIVERSE	= 'Error eliminando Universo anterior.';
    MSG_ERROR				 			= 'Error';
var
    SiguienteConvenio : variant;
begin
  	Screen.Cursor := crHourglass;
    lblReferencia.Caption :=  MSG_DELETEING_OLD_UNIVERSE;
    // BEGIN : SS_1147_NDR_20150520 -------------------------------------------------------------------------------
    //pbProgreso.Position := 0;
    //pbProgreso.Max := FCantConvenios;
    pnlAvance.Visible := True;
    //SiguienteConvenio := 0;
    //while (SiguienteConvenio <> null ) and
    //	  (not FDetenerImportacion) and
    //      (FErrorMsg = '') do begin
    //    try
    //        spBorrarUniversoPAC.Parameters.ParamByName('@CodigoConvenio' ).Value := SiguienteConvenio;
    //        spBorrarUniversoPAC.CommandTimeout := 5000;
    //        spBorrarUniversoPAC.ExecProc;
    //      	SiguienteConvenio := spBorrarUniversoPAC.Parameters.ParamByName('@CodigoConvenio' ).Value;
    //
    //        if ( SiguienteConvenio = Null ) then Break;
    //
    //    except
    //      	on e: exception do begin
    //        	MsgBoxErr(MSG_ERROR_DELETEING_OLD_UNIVERSE, e.Message, MSG_ERROR, MB_ICONERROR);
    //            FErrorMsg := MSG_ERROR_DELETEING_OLD_UNIVERSE;
    //        end;
    //	end;
    //    pbProgreso.Position := SiguienteConvenio;
    //    Application.ProcessMessages;
    //end;
    try
      spBorrarUniversoPAC.CommandTimeout := 5000;
      spBorrarUniversoPAC.ExecProc;
    except
      on e: exception do begin
      	MsgBoxErr(MSG_ERROR_DELETEING_OLD_UNIVERSE, e.Message, MSG_ERROR, MB_ICONERROR);
          FErrorMsg := MSG_ERROR_DELETEING_OLD_UNIVERSE;
      end;
    end;
    //pbProgreso.Position := pbProgreso.Max;
  	//Screen.Cursor := crDefault;
    //result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
    result := ( FErrorMsg = '' );
    // END : SS_1147_NDR_20150520 -------------------------------------------------------------------------------
end;

{-----------------------------------------------------------------------------
  Function Name: BorrarUniversoPACBancoChile
  Author: jjofre
  Date Created: 20/05/2010
  Description: Borra los datos viejos de Universo Banco Chile por Bloques
-----------------------------------------------------------------------------}
function TFRecepcionUniversoBcoChile.BorrarUniversoPACBancoChile : boolean;
resourcestring
    MSG_DELETEING_OLD_UNIVERSE			= 'Eliminando Universo Banco Chile anterior';
  	MSG_ERROR_DELETEING_OLD_UNIVERSE	= 'Error eliminando Universo Banco Chile anterior.';
    MSG_ERROR				 			= 'Error';
var
    SiguienteConvenio : variant;
begin
  	Screen.Cursor := crHourglass;
    lblReferencia.Caption :=  MSG_DELETEING_OLD_UNIVERSE;
    // BEGIN : SS_1147_NDR_20150520 -------------------------------------------------------------------------------
    //pbProgreso.Position := 0;
    //pbProgreso.Max := FCantConvenios;
    pnlAvance.Visible := True;
    //SiguienteConvenio := 0;
    //while (SiguienteConvenio <> null ) and
    //	  (not FDetenerImportacion) and
    //      (FErrorMsg = '') do begin
    //    try
    //      	spBorrarUniversoPACBancoChile.Parameters.ParamByName('@CodigoConvenio').Value := SiguienteConvenio;
    //        spBorrarUniversoPACBancoChile.CommandTimeout := 5000;
    //        spBorrarUniversoPACBancoChile.ExecProc;
    //      	SiguienteConvenio := spBorrarUniversoPACBancoChile.Parameters.ParamByName('@CodigoConvenio' ).Value;
    //
    //        if ( SiguienteConvenio = Null ) then Break;
    //
    //    except
    //      	on e: exception do begin
    //        	MsgBoxErr(MSG_ERROR_DELETEING_OLD_UNIVERSE, e.Message, MSG_ERROR, MB_ICONERROR);
    //            FErrorMsg := MSG_ERROR_DELETEING_OLD_UNIVERSE;
    //        end;
    //	end;
    //    pbProgreso.Position := SiguienteConvenio;
    //    Application.ProcessMessages;
    //end;
    try
      spBorrarUniversoPACBancoChile.CommandTimeout := 5000;
      spBorrarUniversoPACBancoChile.ExecProc;
    except
      on e: exception do begin
      	MsgBoxErr(MSG_ERROR_DELETEING_OLD_UNIVERSE, e.Message, MSG_ERROR, MB_ICONERROR);
          FErrorMsg := MSG_ERROR_DELETEING_OLD_UNIVERSE;
      end;
    end;
    //pbProgreso.Position := pbProgreso.Max;
  	//Screen.Cursor := crDefault;
    //result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
    result := ( FErrorMsg = '' );
    // END : SS_1147_NDR_20150520 -------------------------------------------------------------------------------
end;

{-----------------------------------------------------------------------------
  Function Name: CopiarConveniosAUniversoPAC
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Copia los Convenios PAC que actualmente hay en Convenios a la
  				Tabla UniversoPACCN para poder Actualizarlos
-----------------------------------------------------------------------------}
function TFRecepcionUniversoBcoChile.CopiarConveniosAUniversoPAC : boolean;
resourcestring
  	MSG_ERROR_COULD_NOT_UPDATE_UNIVERSE = 'No se pudo actualizar el Universo anterior';
    MSG_UPDATING_UNIVERSE_DATA 			= 'Copiando datos del Universo';
    MSG_ERROR				 			= 'Error';
var
    SiguienteConvenio : variant;
begin
   	Screen.Cursor := crHourGlass;
    lblReferencia.Caption := MSG_UPDATING_UNIVERSE_DATA;
    // BEGIN : SS_1147_NDR_20150520 -------------------------------------------------------------------------------
    //pbProgreso.Position := 0;
    //pbProgreso.Max := FCantConvenios;
    pnlAvance.Visible := True;
    //SiguienteConvenio := 0;

    //while (SiguienteConvenio <> null ) and
    //	  (not FDetenerImportacion) and
    //      (FErrorMsg = '') do begin
    //    try
    //      	spCopiarDatosConveniosAUniversoPAC.Parameters.ParamByName('@CodigoConvenio' ).Value := SiguienteConvenio;
    //        spCopiarDatosConveniosAUniversoPAC.CommandTimeout := 5000;
    //        spCopiarDatosConveniosAUniversoPAC.ExecProc;
    //      	SiguienteConvenio := spCopiarDatosConveniosAUniversoPAC.Parameters.ParamByName('@CodigoConvenio' ).Value;
    //
    //        if ( SiguienteConvenio = Null ) then Break;
    //
    //    except
    //      	on e: exception do begin
    //        	MsgBoxErr(MSG_ERROR_COULD_NOT_UPDATE_UNIVERSE, e.Message, MSG_ERROR, MB_ICONERROR);
    //            FErrorMsg := MSG_ERROR_COULD_NOT_UPDATE_UNIVERSE;
    //        end;
    //	end;
    //    pbProgreso.Position := SiguienteConvenio;
    //    Application.ProcessMessages;
    //end;
    try
      spCopiarDatosConveniosAUniversoPAC.CommandTimeout := 5000;
      spCopiarDatosConveniosAUniversoPAC.ExecProc;
    except
      on e: exception do begin
      	MsgBoxErr(MSG_ERROR_COULD_NOT_UPDATE_UNIVERSE, e.Message, MSG_ERROR, MB_ICONERROR);
          FErrorMsg := MSG_ERROR_COULD_NOT_UPDATE_UNIVERSE;
      end;
    end;
    //pbProgreso.Position := pbProgreso.Max;
  	//Screen.Cursor := crDefault;
    //result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
    result := ( FErrorMsg = '' );
    // END : SS_1147_NDR_20150520 -------------------------------------------------------------------------------
end;

{-----------------------------------------------------------------------------
  Function Name: VerificarUniverso
  Author:    jjofre
  Date Created: 05/01/2005
  Description: Compara los datos del UniversoPACCN con los de UniversoPACBAncoChile
               y actualiza los convenios
 -----------------------------------------------------------------------------}
function TFRecepcionUniversoBcoChile.VerificarUniverso : boolean;
resourcestring
    MSG_PROCESSING_UNIVERSE_FILE	= 'Procesando Archivo de Universo - Cantidad de Convenios : %d / %d';
  	MSG_ERROR_PROCESSING_UNIVERSE 	= 'Error al procesar el Universo en la l�nea : %d';
  	MSG_PARSE_ERROR					= 'Error al interpretar el archivo de texto';
    MSG_ERROR						= 'Error';
var
  	nNroLineaScript, nLineasScript :integer;
    FUniversoRecord 	: TUniversoPACBancoChileRecord;
    sDescripcionError 	: string;
  	sParseError 		: string;
    sDescErrorInterfaz 	: string;
begin
  	Screen.Cursor := crHourglass;

    nNroLineaScript := 0;
    nLineasScript := cdsUniversoPACBancoChile.RecordCount;
    lblReferencia.Caption := Format( MSG_PROCESSING_UNIVERSE_FILE, [nNroLineaScript,nLineasScript] );
    pbProgreso.Position := 0;
    pbProgreso.Max := nLineasScript;
    pnlAvance.Visible := True;

    cdsUniversoPACBancoChile.First;
    while (not cdsUniversoPACBancoChile.Eof ) and
    	  (not FDetenerImportacion) and
          (FErrorMsg = '') do begin

        with spAgragarUniversoPACBC, Parameters do begin
            try
                ParamByName('@FechaAlta' ).Value 			:= cdsUniversoPACBancoChile.FieldByName('FechaInicio').Value;
                ParamByName('@NumeroConvenio' ).Value 		:= cdsUniversoPACBancoChile.FieldByName('NumeroConvenio').Value;
                ParamByName('@CodigoBancoSBEI' ).Value 		:= cdsUniversoPACBancoChile.FieldByName('CodigoBancoSBEI').Value;
                CommandTimeOut := 5000;
                ExecProc;
                sDescripcionError := Trim( VarToStr( ParamByName('@DescripcionError' ).Value ));

                if (sDescripcionError <> '') then begin
                    sDescErrorInterfaz := FormatFloat('0000',cdsUniversoPACBancoChile.FieldByName('CodigoBancoSBEI').Value)+' - '+StringReplace(sDescripcionError, #10#13, ' - ', [rfReplaceAll]);
                    FErrores.Add(sDescErrorInterfaz);
                    AgregarErrorInterfaz(sDescErrorInterfaz);
                end;

            except
                on e: exception do begin
        		    MsgBoxErr(Format(MSG_ERROR_PROCESSING_UNIVERSE, [nNroLineaScript]), e.Message, MSG_ERROR, MB_ICONERROR);
                    FErrorMsg := MSG_ERROR_PROCESSING_UNIVERSE;
                end;
            end;
        end;

        Inc( nNroLineaScript );
        lblReferencia.Caption := Format( MSG_PROCESSING_UNIVERSE_FILE, [nNroLineaScript,nLineasScript] );
        cdsUniversoPACBancoChile.Next;
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

  	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: CargarUniversoPACBancoChile
  Author: jjofre
  Date Created: 20/05/2010
  Description: Carga los datos de la tabla cliente (archvo de recepcion Universo)
            a la base de datos tabla UniversoPACBancoChile
-----------------------------------------------------------------------------}
function TFRecepcionUniversoBcoChile.CargarUniversoPACBancoChile : boolean;
resourcestring
    MSG_PROCESSING_UNIVERSE_FILE	= 'Cargando Archivo de Universo Banco Chile - Convenio : %d / %d';
  	MSG_ERROR_PROCESSING_UNIVERSE 	= 'Error al cargar el Universo Banco Chile en la l�nea ';
  	MSG_PARSE_ERROR					= 'Error al interpretar el archivo de texto';
    MSG_PROCESSING_LOG          	= 'Error al actualizar el log de operaciones con la cantidad de convenios a procesar';
    MSG_ERROR						= 'Error';
const
    STR_LINE = 'Linea: ';
var
  	nNroLineaScript, nLineasScript :integer;
    FUniversoRecord 	: TUniversoPACBancoChileRecord;
    sDescripcionError 	: string;
  	sParseError 		: string;
begin
  	Screen.Cursor := crHourglass;

    nNroLineaScript := 0;

    nLineasScript := cdsUniversoPACBancoChile.RecordCount;
    lblReferencia.Caption := Format( MSG_PROCESSING_UNIVERSE_FILE, [nNroLineaScript,nLineasScript] );
    pbProgreso.Position := 0;
    pbProgreso.Max := nLineasScript;
    pnlAvance.Visible := True;

    cdsUniversoPACBancoChile.First;
    while (not cdsUniversoPACBancoChile.Eof ) and
    	  (not FDetenerImportacion) and
          (FErrorMsg = '') do begin
        try
            spAgregarUniversoPACBancoChile.Parameters.ParamByName('@NumeroConvenio').Value := cdsUniversoPACBancoChile.FieldByName('NumeroConvenio').Value;
            spAgregarUniversoPACBancoChile.Parameters.ParamByName('@CodigoBancoSBEI').Value := cdsUniversoPACBancoChile.FieldByName('CodigoBancoSBEI').Value;
            spAgregarUniversoPACBancoChile.Parameters.ParamByName('@FechaInicio' ).Value := cdsUniversoPACBancoChile.FieldByName('FechaInicio').Value;
            spAgregarUniversoPACBancoChile.CommandTimeOut := 5000;
            spAgregarUniversoPACBancoChile.ExecProc;
        except
            on e: exception do begin
            	MsgBoxErr(MSG_PROCESSING_UNIVERSE_FILE, e.Message, MSG_ERROR, MB_ICONERROR);
                FErrorMsg := MSG_PROCESSING_UNIVERSE_FILE;
            end;
        end;

        Inc( nNroLineaScript );
        lblReferencia.Caption := Format( MSG_PROCESSING_UNIVERSE_FILE, [nNroLineaScript,nLineasScript] );
        cdsUniversoPACBancoChile.Next;
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

    // Se actualiza el log de operaciones con la cantidad de convenios a procesar
    try
        spActualizarLogOperacionesRegistrosProcesados.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
        spActualizarLogOperacionesRegistrosProcesados.ExecProc;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_PROCESSING_LOG, e.Message, MSG_ERROR, MB_ICONERROR);
            FErrorMsg := MSG_PROCESSING_LOG;
        end;
    end;

  	Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: CompararUniversoPACUniversoPACBancoChile
  Author: jjofre
  Date Created: 20/05/2010
  Description: Compara UniversoPACCN contra Universo PAC Banco de Chile marcando
            los registros en UniversoPACCN con el Mandato 'B' siempre cuando
            existan en Universo PAC Banco de Chile, para poder confirmarlos.
-----------------------------------------------------------------------------}
function TFRecepcionUniversoBcoChile.CompararUniversoPACUniversoPACBancoChile : boolean;
resourcestring
    MSG_PROCESSING_UNIVERSE_FILE	= 'Comparando Universo PAC CAC contra Universo PAC Banco de Chile';
  	MSG_ERROR_PROCESSING_UNIVERSE 	= 'Error al comparando Universo PAC CAC contra Universo PAC Banco de Chile ';
  	MSG_PARSE_ERROR					= 'Error al comparando Universo PAC CAC contra Universo PAC Banco de Chile';
    MSG_ERROR						= 'Error';
const
    STR_LINE = 'Linea: ';
var
  	nNroLineaScript, nLineasScript :integer;
    FUniversoRecord 	: TUniversoPACBancoChileRecord;
    sDescripcionError 	: string;
  	sParseError 		: string;
begin
    result := False;
  	Screen.Cursor := crHourglass;
    try
        spCompararUniversoPACUniversoPACBancoChile.CommandTimeOut := 5000;
        spCompararUniversoPACUniversoPACBancoChile.ExecProc;
        result := True;
    except
        on e: exception do begin
            MsgBoxErr(MSG_PROCESSING_UNIVERSE_FILE, e.Message, MSG_ERROR, MB_ICONERROR);
            FErrorMsg := MSG_PROCESSING_UNIVERSE_FILE;
        end;
    end;

  	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: ParseUniversoPACLine
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Parsea una linea proveniente del Universo Banco de Chile
              -Se va agregando los registros del archivo de Universo PAC Banco chile
              a la tabla UniversoPACBancoChile
-----------------------------------------------------------------------------}
function TFRecepcionUniversoBcoChile.ParseUniversoPACLine(sline : string; var UniversoRecord : TUniversoPACBancoChileRecord; var sParseError : string) : boolean;
resourcestring
    MSG_EMPRESA_ERROR 		            = 'El c�digo de La Empresa es inv�lido.';
    MSG_CONVENIO_ERROR                = 'El c�digo de Convenio es inv�lido.';
    MSG_STARTING_DATE_ERROR  	        = 'La Fecha de Inicio es inv�lida.';
    MSG_NO_TIENE_CANTIDAD_CARACTERES  = 'La linea no tiene la cantidad de caracteres que corresponde';
    MSG_CONVENIO_DUPLICADO            = 'El n�mero de convenio est� duplicado. - Convenio :'   ;
    MSG_BANK_CODE_ERROR               = 'El c�digo de banco SBEI es inv�lido.';
    MSG_CONVENIO_ERROR_BANCO          = 'El convenio fue enviado por error desde el Banco : ';        //SS-884-NDR-20120621
const
    CODIGO_SBEI_BANCO_CHILE = 1;
    CANTIDAD_CARACTERES_VALIDACION = 49; //REV.5

var
    sDescErrorInterfaz : string;
    sPrimerCaracterConvenio:string;                                             //SS-884-NDR-20120621

begin

  	Result := False;
    if ( Trim( sline ) = '' ) then Exit;

    if  Length ( Trim ( sline ) ) <> CANTIDAD_CARACTERES_VALIDACION then begin
        sParseError := MSG_NO_TIENE_CANTIDAD_CARACTERES;
        Exit;
    end;

    if ( (Trim(Copy(sline,4,3))) <> FBancoChile_Codigo_Empresa_aux ) then begin
        sParseError := MSG_EMPRESA_ERROR;
        Exit;
    end;

    if ( (Trim(Copy(sline,7,3))) <> FBancoChile_Codigo_Convenio_aux) then begin
        sParseError := MSG_CONVENIO_ERROR;
        Exit;
    end;

    try
        with UniversoRecord do begin
            sPrimerCaracterConvenio := UpperCase(Copy(sline, 18, 1));                                                            //SS-884-NDR-20120621
            if IfThen(FCodigoNativa <> CODIGO_VS, IfThen(sPrimerCaracterConvenio='0',1,0),1)=1 then                              //SS_1147_NDR_20150520 SS-884-NDR-20120621
            begin                                                                                                                //SS-884-NDR-20120621
              NumeroConvenio := '001' + UpperCase(Copy(sline, 19, 14));                                                          //SS-884-NDR-20120621
              //if ObtenerCodigoConcesionariaNativa=CODIGO_VS then              // SS_1147_CQU_20150324  //SS_1147Q_NDR_20141202[??]
              if FCodigoNativa = CODIGO_VS then                                 // SS_1147_CQU_20150324
              begin                                                             //SS_1147Q_NDR_20141202[??]
                NumeroConvenio := Copy( NumeroConvenio, 6, 12);                 //SS_1147Q_NDR_20141202[??]
              end;                                                              //SS_1147Q_NDR_20141202[??]
              sParseError := MSG_BANK_CODE_ERROR;                                                                                //SS-884-NDR-20120621
              CodigoBancoSBEI := StrToInt(trim(Copy(sline, 1, 3)));                                                              //SS-884-NDR-20120621
              sParseError := MSG_STARTING_DATE_ERROR;                                                                            //SS-884-NDR-20120621
              FechaInicio := EncodeDate(	StrToInt(Copy(sline,42,4)), StrToInt(Copy(sline,46,2)), StrToInt(Copy(sline,48,2)));   //SS-884-NDR-20120621
                                                                                                                                 //SS-884-NDR-20120621
              //Se busca si ya existe un NroConvenio, si es as� se compara el                                                    //SS-884-NDR-20120621
              //campo FechaInicio a fin de tener registrado la �ltima Activacion del Medio Pago                                  //SS-884-NDR-20120621
              if (cdsUniversoPACBancoChile.FindKey([NumeroConvenio])) then begin                                                 //SS-884-NDR-20120621
                   sDescErrorInterfaz := FormatFloat('0000',cdsUniversoPACBancoChile.FieldByName('CodigoBancoSBEI').Value)+      //SS-884-NDR-20120621
                                                            ' - '+                                                               //SS-884-NDR-20120621
                                                            MSG_CONVENIO_DUPLICADO +                                             //SS-884-NDR-20120621
                                                            NumeroConvenio;                                                      //SS-884-NDR-20120621
                  FErrores.Add(sDescErrorInterfaz);                                                                              //SS-884-NDR-20120621
                  AgregarErrorInterfaz(sDescErrorInterfaz);                                                                      //SS-884-NDR-20120621
                                                                                                                                 //SS-884-NDR-20120621
                  if ( FechaInicio >= cdsUniversoPACBancoChile.FieldByName('FechaInicio').AsDateTime ) then begin                //SS-884-NDR-20120621
                      cdsUniversoPACBancoChile.Edit;                                                                             //SS-884-NDR-20120621
                      cdsUniversoPACBancoChile.FieldByName('NumeroConvenio').AsString := NumeroConvenio;                         //SS-884-NDR-20120621
                      cdsUniversoPACBancoChile.FieldByName('CodigoBancoSBEI').AsInteger := CodigoBancoSBEI;                      //SS-884-NDR-20120621
                      cdsUniversoPACBancoChile.FieldByName('FechaInicio').AsDateTime := FechaInicio;                             //SS-884-NDR-20120621
                      cdsUniversoPACBancoChile.Post;                                                                             //SS-884-NDR-20120621
                  end;                                                                                                           //SS-884-NDR-20120621
              end else begin                                                                                                     //SS-884-NDR-20120621
                  //Si no exite, se agrega la linea a la tabla cliente                                                           //SS-884-NDR-20120621
                  cdsUniversoPACBancoChile.Append;                                                                               //SS-884-NDR-20120621
                  cdsUniversoPACBancoChile.FieldByName('NumeroConvenio').AsString := NumeroConvenio;                             //SS-884-NDR-20120621
                  cdsUniversoPACBancoChile.FieldByName('CodigoBancoSBEI').AsInteger := CodigoBancoSBEI;                          //SS-884-NDR-20120621
                  cdsUniversoPACBancoChile.FieldByName('FechaInicio').AsDateTime := FechaInicio;                                 //SS-884-NDR-20120621
                  cdsUniversoPACBancoChile.Post;                                                                                 //SS-884-NDR-20120621
              end;                                                                                                               //SS-884-NDR-20120621
            end                                                                                                                  //SS-884-NDR-20120621
            else                                                                                                                 //SS-884-NDR-20120621
            begin                                                                                                                //SS-884-NDR-20120621
              sDescErrorInterfaz := FormatFloat('0000',                                                                          //SS-884-NDR-20120621
                                                cdsUniversoPACBancoChile.FieldByName('CodigoBancoSBEI').Value)+                  //SS-884-NDR-20120621
                                                ' - '+                                                                           //SS-884-NDR-20120621
                                                MSG_CONVENIO_ERROR_BANCO +                                                       //SS-884-NDR-20120621
                                                UpperCase(Copy(sline, 18, 15));                                                  //SS-884-NDR-20120621
              FErrores.Add(sDescErrorInterfaz);                                                                                  //SS-884-NDR-20120621
              AgregarErrorInterfaz(sDescErrorInterfaz);                                                                          //SS-884-NDR-20120621
            end;                                                                                                                 //SS-884-NDR-20120621
        end;                                                                                                                     //SS-884-NDR-20120621
    except
      	on exception do begin
          	Exit;
        end;
    end;
    Result 		:= True;
    sParseError := '';
end;

{-----------------------------------------------------------------------------
  Function Name: ProcesarNoExistentesUniversoPAC
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Procesa los Convenios que NO existen en el Universo PAC BancoCHile
-----------------------------------------------------------------------------}
function TFRecepcionUniversoBcoChile.ProcesarNoExistentesUniversoPAC : boolean;
resourcestring
  	MSG_ERROR_PROCESSING_UNIVERSE = 'Error al procesar el Universo';
  	MSG_ERROR_GETTING_UNEXISTING_CONTRACTS = 'Error al obtener convenios no existentes en Universo PAC';
    MSG_PROCESSING_UNEXISTING_CONTRACTS	= 'Procesando convenios no existentes en Universo PAC';
    MSG_ERROR = 'Error';
var
    sDescripcionError : string;
    sDescErrorInterfaz : string;
begin
  	Screen.Cursor := crHourglass;
    lblReferencia.Caption := MSG_PROCESSING_UNEXISTING_CONTRACTS;
    pbProgreso.Position := 0;
    pnlAvance.Visible := True;
  	Application.ProcessMessages;

    //Obtiene los Convenios de la tabla UniversoPACCN que no fueron Verificados
  	try
        spObtenerConveniosPacNoVerificados.Open;
        spObtenerConveniosPacNoVerificados.CommandTimeout := 5000;
        pbProgreso.Max := spObtenerConveniosPacNoVerificados.RecordCount;
    except
      	on e: Exception do begin
            MsgBoxErr(MSG_ERROR_GETTING_UNEXISTING_CONTRACTS, e.Message, MSG_ERROR, MB_ICONERROR);
          	FErrorMsg := MSG_ERROR_GETTING_UNEXISTING_CONTRACTS;
        end;
    end;

    while ( not spObtenerConveniosPacNoVerificados.EOF )  and ( not FDetenerImportacion ) and ( FErrorMsg = '' ) do begin

        with spEliminarMediopagoPACBC, Parameters do begin

            ParamByName( '@CodigoConvenio' ).Value := spObtenerConveniosPacNoVerificados.FieldByName( 'CodigoConvenio' ).Value;
            try

                CommandTimeOut := 5000;
                ExecProc;
                sDescripcionError := Trim( VarToStr( ParamByName( '@DescripcionError' ).Value ));

                if ( sDescripcionError <> '' ) then begin
                    sDescErrorInterfaz := StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]);
                    FErrores.Add( sDescErrorInterfaz );
                    AgregarErrorInterfaz(sDescErrorInterfaz);
                end;
            except
                on e: Exception do begin
                      MsgBoxErr(MSG_ERROR_PROCESSING_UNIVERSE, e.Message, MSG_ERROR, MB_ICONERROR);
                      FErrorMsg := MSG_ERROR_PROCESSING_UNIVERSE;
                      Break;
                end;
            end;
            spObtenerConveniosPacNoVerificados.Next;
            pbProgreso.StepIt;
            Application.ProcessMessages;
        end;
    end;
    spObtenerConveniosPacNoVerificados.Close;
    pbProgreso.Position := pbProgreso.Max;
    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    result := ( FErrorMsg = '' ) and ( not FDetenerImportacion );
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReportesErrores
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Genera el Reporte de Errores
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoBcoChile.GenerarReportesErrores;
resourcestring
    PROCESSING_ERRORS_FILE				= 'Universo Banco de Chile - Errores Procesamiento ';
    REPORT_DIALOG_CAPTION 				= 'Universo Banco de Chile - Errores';
    MSG_ERROR_SAVING_ERRORS_FILE 	    = 'Error generando archivo de errores';
    MSG_ERR_REPORT                      = 'El Reporte de Errores se guardo en: %s';
    TITLE_ERR_REPORT                    = 'Reporte de Errores';
    MSG_THERE_IS_NO_BANK                = 'Banco Desconocido';
    MSG_COULD_NOT_OPEN_THE_REPORT       = 'No se puede mostrar el reporte';
var
    sArchivoErroresProcesamiento : string;
  	Config: TRBConfig;
    i: integer;
    HayBanco: boolean;
    iCodSBEI : Integer;
    sNOmbreBanco: string;
    f : TRptRecepcionUniversoBancoChileForm;   //REV. 2
    RutaLogo: AnsiString;                                                                               //SS_1147_NDR_20140710
begin
	if ( FErrores.Count = 0 ) then Exit;



    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710




    // Generaci�n de Reportes de Errores
    sArchivoErroresProcesamiento := GoodDir( FBancoChile_Directorio_Errores ) + PROCESSING_ERRORS_FILE + FormatDateTime ( 'yyyy-mm-dd', Now ) + '.txt';
    try
        FErrores.SaveToFile( sArchivoErroresProcesamiento );
    except
    	on e:exception do begin
        	MsgBoxErr(MSG_ERROR_SAVING_ERRORS_FILE, e.Message, 'Error', MB_ICONERROR);
            Exit;
        end;
    end;

    if NOT FVerReporteErrores then begin
        MsgBox(Format(MSG_ERR_REPORT,[sArchivoErroresProcesamiento]),TITLE_ERR_REPORT,MB_ICONINFORMATION);
        Exit;
    end;

    Application.CreateForm(TRptRecepcionUniversoBancoChileForm, f);
    try
        try
            f.MostrarReporte(FCodigoOperacion);
        except
            on E: Exception do begin
                MsgBoxErr(MSG_COULD_NOT_OPEN_THE_REPORT, E.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        f.Release;
    end;
end;

procedure TFRecepcionUniversoBcoChile.AgregarErrorInterfaz(mDescripcionError: String);
resourcestring
  	MSG_ERROR_PROCESSING_UNIVERSE 	= 'Error al agregar en ErroresInterfases. Linea:   ';
    MSG_ERROR						= 'Error';
const
    STR_LINE = 'Linea: ';
begin
    try
        spAgregarErrorInterfaz.Close;
        spAgregarErrorInterfaz.Parameters.ParamByName('@CodigoOperacionInterfase').Value    := FCodigoOperacion;
        spAgregarErrorInterfaz.Parameters.ParamByName('@CodigoModulo').Value                := RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_PAC_BCHILE;
        spAgregarErrorInterfaz.Parameters.ParamByName('@CodigoError').Value                 := 0;
        spAgregarErrorInterfaz.Parameters.ParamByName('@Fecha').Value                       := NowBase(DMConnections.BaseCAC);
        spAgregarErrorInterfaz.Parameters.ParamByName('@DescripcionError').Value            := mDescripcionError;
        spAgregarErrorInterfaz.ExecProc;
    except
        on e: exception do begin
            MsgBoxErr(MSG_ERROR_PROCESSING_UNIVERSE + CRLF + STR_LINE + mDescripcionError, e.Message, MSG_ERROR, MB_ICONERROR);
            FErrorMsg := MSG_ERROR_PROCESSING_UNIVERSE;
        end;
    end;
end;

function TFRecepcionUniversoBcoChile.ObtenerDatosProcesoUniversoBancoChile(CodigoOperacionInterfase: integer; var LineasArchivo: integer;
            var ConveniosDuplicados: integer; var RegistrosExito: integer; var RegistrosError: integer): boolean;
resourcestring
    MSG_ERROR = 'No se pudo obtener los datos de resumen de proceso';
begin
    try
        spObtenerDatosProcesoUniversoBancoChile.Close;
        spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
        spObtenerDatosProcesoUniversoBancoChile.ExecProc;

        LineasArchivo := spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@LineasArchivo').Value;
        ConveniosDuplicados := spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@ConveniosDuplicados').Value;
        RegistrosExito := spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@RegistrosExito').Value;
        RegistrosError := spObtenerDatosProcesoUniversoBancoChile.Parameters.ParamByName('@RegistrosError').Value;

        Result := true;
    except
       on e: Exception do begin
           MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
           Result := False;
       end;
    end;
end;

function TFRecepcionUniversoBcoChile.MsgProcesoFinalizadoBancoChile(sMensajeOK, sMensajeError, sTitulo: AnsiString; RLineas, RDuplicados, RExito, RError: integer):boolean;
resourcestring
    MSG_PROC_INFO                       =   CrLf+'Cantidad de Lineas del Archivo: %d '+
                                            CrLf+'Cantidad de Convenios Duplicados: %d ' +
                                            CrLf+'Cantidad de Registros Procesados con �xito: %d ' +
                                            CrLf+'Cantidad de Registros Procesados con Error: %d ';
    MSG_ASK_SHOW_ERROR_REPORT           =   CrLf + 'Desea ver el Reporte de Errores?.';
var
    Mensaje     : AnsiString;
    Respuesta   : Boolean;
begin
    Mensaje :=  Format(MSG_PROC_INFO,[RLineas, RDuplicados, RExito, RError]);

    if RError > 0 then begin
        Mensaje := sMensajeError + Mensaje +  MSG_ASK_SHOW_ERROR_REPORT;
        Respuesta := (MsgBox(Mensaje, sTitulo, MB_ICONWARNING + MB_YESNO) = mrYes);
    end
    else begin
        Mensaje := sMensajeOK + Mensaje;
        Respuesta := False;
        MsgBox(Mensaje, sTitulo, MB_ICONINFORMATION);
    end;
    Result :=  Respuesta;
end;

{-----------------------------------------------------------------------------
  Function Name: rbiListadoExecute
  Author:    jjofre
  Date Created: 20/05/2010
  Description: ejecuta el informe
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoBcoChile.rbiListadoExecute(Sender: TObject; var Cancelled: Boolean);
begin
	try
		try
			Screen.Cursor := crHourGlass;
		finally
			Screen.Cursor := crDefault;
		end;
	except
		on E: Exception do begin
			Cancelled := True;
			MsgBoxErr(MSG_ERROR_DATOS_LISTADO, E.Message, Self.Caption, MB_ICONSTOP);
		end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    jjofre
  Date Created: 20/05/2010
  Description:  Borra el Universo PAC CN
                Borra el Universo PAC Banco de Chile
                Carga el Universo PAC CN y Universo PAC Banco de Chile
                Compara el Universo Universo PAC CN contra el Universo PAC Banco de Chile
                Confirma los registros de Universo PAC CN que se encuentran en Universo PAC Banco de Chile
                Da el alta a los registros Nuevos de Universo PAC Banco de Chile en el CAC
                Da la baja de los registro que no fueron confirmados de Universo PAC CN
                pero que tienen encendido el bit confirmado.
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoBcoChile.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    jjofre
      Date Created: 20/05/2010
      Description: Se obtienen la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    jjofre
      Date Created: 20/05/2010
      Description: Se Actualiza el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog(CantidadErrores:integer):boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : string;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_SUCCEDED 					= 'El proceso finaliz� con �xito';
    MSG_PROCESS_FINALIZED_WITH_ERRORS		= 'El proceso finaliz� con errores';
    MSG_PROCESS_COULD_NOT_BE_COMPLETED 		= 'El proceso no se pudo completar';
    MSG_ERROR_CANNOT_OPEN_UNIVERSE_FILE 	= 'No se puede abrir el archivo de universo %s';
    MSG_ERRROR_UNIVERSE_FILE_IS_EMPTY 		= 'El archivo de universo seleccionado est� vac�o';
    MSG_ERROR_UNIVERSE_FILE_ROW             = 'El archivo No contiene la cantidad de filas que indica el registro de Control';
    MSG_ERROR_UNIVERSE_NAME_FILE            = 'El nombre del archivo contiene un c�digo de Empresa inv�lido ';
    MSG_ERROR_CODIGO_EMPRESA                = 'Codigo de Empresa Inv�lido en Registro de Control';
    MSG_ERROR_CODIGO_CONVENIO               = 'Codigo de Convenio Inv�lido en Registro de Control';
var
    TotalRegistros: integer;
    CantidadErrores: integer;
    CantidadLineasArchivo: integer;
	CantidadConveniosDuplicados: integer;
	CantidadRegistrosExito: integer;
	CantidadRegistrosError: integer;
    DescError:string;

begin
	FUniversoTXT := TStringList.Create;
 	// Crea las listas de Errores
	FErrorMsg := '';
    FErrores := TStringList.Create;

	// Deshabilita los botones
	btnCancelar.Enabled := True;
	btnProcesar.Enabled := False;
	btnSalir.Enabled := False;
	btnAbrirArchivo.Enabled := False;
	edOrigen.Enabled := False;

	FDetenerImportacion := False;

	try
		try
            //Abre el archivo y lo pone en el stringlist
            FUniversoTxt.text:= FileToString(edOrigen.text);
            
            if RegistrarOperacion then begin

                if FCodigoNativa <> CODIGO_VS then
                begin    // SS_1147_CQU_20150324
                  //Valida el Codigo de Empresa del nombre del archivo
                  if (Trim(Copy(ExtractFileName(edOrigen.text),6,3)) <> FBancoChile_Codigo_Empresa_aux ) then begin
                      //Muestra un cartel indicando que el nombre del archivo no contiene el codigo de convenio Valido
                      MsgBox(MSG_ERROR_UNIVERSE_NAME_FILE, Caption, MB_ICONERROR);
                      ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOperacion,MSG_ERROR_UNIVERSE_NAME_FILE,DescError);
                      Exit;
                  end;
                end;                                        // SS_1147_CQU_20150324


                // Verifica si el Archivo Contiene alguna linea
                if ( FUniversoTXT.Count = 0 ) then begin
                    //Muestra un cartel indicando que el archivo esta vacio
                    MsgBox(MSG_ERRROR_UNIVERSE_FILE_IS_EMPTY, Caption, MB_ICONERROR);
                    ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOperacion,MSG_ERRROR_UNIVERSE_FILE_IS_EMPTY,DescError);
                    Exit;
                end
                else begin
                    //Valida la cantidad de registros en el archivo, indicada el registro de control.
                    if ((FUniversoTXT.Count-1) <> StrToInt(Copy(FUniversoTXT[FUniversoTXT.Count-1],34,6))) then begin
                        MsgBox(MSG_ERROR_UNIVERSE_FILE_ROW, Caption, MB_ICONERROR);
                        ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOperacion,MSG_ERROR_UNIVERSE_FILE_ROW,DescError);
                        Exit;
                    end;

                    //Valida el Codigo de Empresa del registro de control
                    if (Trim(Copy(FUniversoTXT[FUniversoTXT.Count-1],4,3)) <> FBancoChile_Codigo_Empresa_aux ) then begin
                        MsgBox(MSG_ERROR_CODIGO_EMPRESA, Caption, MB_ICONERROR);
                        ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOperacion,MSG_ERROR_CODIGO_EMPRESA,DescError);
                        Exit;
                    end;

                    //Valida el Codigo de Convenio del registro de control
                    if (Trim(Copy(FUniversoTXT[FUniversoTXT.Count-1],7,3)) <> FBancoChile_Codigo_Convenio_aux ) then begin
                        MsgBox(MSG_ERROR_CODIGO_CONVENIO, Caption, MB_ICONERROR);
                        ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOperacion,MSG_ERROR_CODIGO_CONVENIO,DescError);
                        Exit;
                    end;

                    //Confirma las cantidades
                    if not ConfirmaCantidades(FUniversoTXT.Count) then Exit;

                    //Analiza el archivo para ver si hay errores de sintaxis
                    if  AnalizarUniversoTxt then begin

                        // Si el Archivo es v�lido Inicia la Transacci�n
                        if BorrarUniversoPAC and
                                BorrarUniversoPACBancoChile and
                                    CopiarConveniosAUniversoPAC and
                                    CargarUniversoPACBancoChile and
                                    CompararUniversoPACUniversoPACBancoChile and
                                                    VerificarUniverso and
                                                    ProcesarNoExistentesUniversoPAC then begin

                            //Se obtienen la cantidad de errores
                            ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);

                            //Se obtienen los datos de resumen del proceso
                            ObtenerDatosProcesoUniversoBancoChile(FCodigoOperacion, CantidadLineasArchivo, CantidadConveniosDuplicados,
                                                                     CantidadRegistrosExito, CantidadRegistrosError);

                            //Se Actualiza el log al Final
                            ActualizarLog(CantidadErrores);

                            //Obtiene la cantidad de convenios que hay en el archivo
                            TotalRegistros := cdsUniversoPACBancoChile.RecordCount;

                            //Muestro cartel de finalizacion
                            FVerReporteErrores := MsgProcesoFinalizadoBancoChile( MSG_PROCESS_SUCCEDED,
                                                                        MSG_PROCESS_FINALIZED_WITH_ERRORS, Caption,
                                                                         CantidadLineasArchivo, CantidadConveniosDuplicados,
                                                                         CantidadRegistrosExito, CantidadRegistrosError);

                            //Si hubo errores generamos el reporte de errores
                            //if (CantidadRegistrosError > 0) then GenerarReportesErrores;

                            GenerarReportesErrores;

                            //Se mueve el archivo procesado
                            MoverArchivoProcesado(Caption,edOrigen.Text, FSTD_DirectorioUniversoProcesado);

                        end
                        else begin
                            // Si hubo alg�n error Deshace la Transacci�n
                            MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);
                        end
                    end;
                end;
            end;
		except
			on e: Exception do begin
                MsgBoxErr(Format(MSG_ERROR_CANNOT_OPEN_UNIVERSE_FILE, [ExtractFilePath(edOrigen.Text)]), e.Message, 'Error', MB_ICONERROR);
                FErrorMsg := MSG_ERROR_CANNOT_OPEN_UNIVERSE_FILE;
			end;
		end;
	finally
        FreeAndNil(FUniversoTXT);
        FreeAndNil(FErrores);
        btnCancelar.Enabled := False;
        Close;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    jjofre
  Date Created: 20/05/2010
  Description: Detiene el proceso
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoBcoChile.btnCancelarClick(Sender: TObject);
resourcestring
	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    jjofre
  Date Created: 20/05/2010
  Description:  Permite salir del formulario
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoBcoChile.btnSalirClick(Sender: TObject);
begin
    close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    jjofre
  Date Created: 20/05/2010
  Description:  Impide salir si esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoBcoChile.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    jjofre
  Date Created: 20/05/2010
  Description:  se libera de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
-----------------------------------------------------------------------------}
procedure TFRecepcionUniversoBcoChile.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
