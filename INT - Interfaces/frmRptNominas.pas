{-------------------------------------------------------------------------------
 File Name: frmRptNominas.pas
 Author:    mbecerra
 Date: 18-Julio-2008
 Description:
            	Genera el reporte de finalización para la generación de las nóminas.
                Este archivo queda disponible en el LogOperaciones y ya
                no es necesaria la generación en PDF.


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
Unit frmRptNominas;

interface

uses
  //Reporte
  DMConnection,                //Coneccion a Base de Datos OP_CAC
  UtilProc,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup, ppStrtch, ppSubRpt, TXComp, TXRB, ConstParametrosGenerales;          //SS_1147_NDR_20140710

type
  TRptNominasForm = class(TForm)
    rptEnvioDebitos: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppMaxFechaEmision: TppLabel;
    ppLabel1: TppLabel;
    ppLine1: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    ppParameterList1: TppParameterList;
   	RBIListado: TRBInterface;
    spObtenerRegistroLogOperaciones: TADOStoredProc;
    ppLine5: TppLine;
    Ltusuario: TppLabel;
    lblFecha: TppLabel;
    ppUsuario: TppLabel;
    ppFechaHora: TppLabel;
    ppProceso: TppLabel;
    ppArchivo: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppTotal: TppLabel;
    ppCantidad: TppLabel;
    ppLabel3: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppModulo: TppLabel;
    ppLine2: TppLine;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FCodigoOperacionInterfase : Integer;
    { Private declarations }
  public
    Function  Inicializar(CodigoOperacionInterfase : Integer) : Boolean;
    { Public declarations }
  end;

var
  RptNominasForm : TRptNominasForm;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    mbecerra
  Date : 18-Julio-2008
  Description:  Inicialización del reporte.
-----------------------------------------------------------------------------}
function TRptNominasForm.Inicializar(CodigoOperacionInterfase : Integer): Boolean;
resourcestring
	MSG_CAPTION = 'Reporte de Nóminas generadas';
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

  	Self.Caption := MSG_CAPTION;
    FCodigoOperacionInterfase := CodigoOperacionInterfase;
    Result := RBIListado.Execute;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    mbecerra
  Date: 18-Julio-2008
  Description:   Ejecución del Informe.
-----------------------------------------------------------------------------}
procedure TRptNominasForm.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
resourcestring
    MSG_ERROR = 'Error al generar el reporte %s';
begin
	try
    	spObtenerRegistroLogOperaciones.Close;
    	with spObtenerRegistroLogOperaciones do begin
        	Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
            Open;
            ppArchivo.Caption   := FieldByName('NombreArchivo').AsString;
            ppFechaHora.Caption := FormatDateTime('dd/mm/yyyy hh:nn:ss', FieldByName('Fecha').AsDateTime);
            ppUsuario.Caption   := FieldByName('Usuario').AsString;
            ppProceso.Caption   := IntToStr(FCodigoOperacionInterfase);
            ppCantidad.Caption  := Format('%.0n', [FieldByName('LineasArchivo').AsFloat]);
            ppTotal.Caption     := Format('%.0n', [FieldByName('MontoArchivo').AsFloat]);
        end;
        
    	spObtenerRegistroLogOperaciones.Close;
    except on e:exception do begin
           MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;

    end;
    
end;



end.
