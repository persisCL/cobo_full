object frmABMDepositoEmprasasDeCobranza: TfrmABMDepositoEmprasasDeCobranza
  Left = 0
  Top = 0
  Width = 703
  Height = 520
  Caption = 'Deposito por parte de las Empresas de Cobranza'
  Color = clBtnFace
  Constraints.MinHeight = 520
  Constraints.MinWidth = 703
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object abmtb_Depositos: TAbmToolbar
    Left = 0
    Top = 0
    Width = 695
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = abmtb_DepositosClose
  end
  object dbl_Depositos: TAbmList
    Left = 0
    Top = 33
    Width = 695
    Height = 327
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'89'#0'Fecha Deposito  '
      #0'103'#0'Empresa Cobranza  '
      #0'85'#0'Importe              '
      #0'95'#0'Usuario                 '
      #0'116'#0'Fecha Modificaci'#243'n     '
      #0'100'#0'N'#186' Contabilizaci'#243'n  '
      #0'11'#0' ')
    HScrollBar = True
    RefreshTime = 100
    Table = tbl_DepositoEmpresasCobranza
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dbl_DepositosClick
    OnDrawItem = dbl_DepositosDrawItem
    OnRefresh = dbl_DepositosRefresh
    OnInsert = dbl_DepositosInsert
    OnDelete = dbl_DepositosDelete
    OnEdit = dbl_DepositosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = abmtb_Depositos
  end
  object pnl_Datos: TPanel
    Left = 0
    Top = 360
    Width = 695
    Height = 87
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    object lbl_FechaDeposito: TLabel
      Left = 15
      Top = 36
      Width = 109
      Height = 13
      Caption = 'Fecha de Deposito: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_Importe: TLabel
      Left = 15
      Top = 63
      Width = 47
      Height = 13
      Caption = 'Importe:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_Vendedor: TLabel
      Left = 15
      Top = 10
      Width = 128
      Height = 13
      Caption = 'Empresa de Cobranza:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object de_FechaDeposito: TDateEdit
      Left = 156
      Top = 32
      Width = 110
      Height = 21
      Hint = 'Fecha de Deposito'
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
    object ne_Importe: TNumericEdit
      Left = 156
      Top = 59
      Width = 109
      Height = 21
      Hint = 'Importe del Deposito'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Decimals = 2
    end
    object cb_EmpresaDeCobranza: TVariantComboBox
      Left = 156
      Top = 6
      Width = 110
      Height = 21
      Hint = 'Empresa de Cobranza'
      Style = vcsDropDownList
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Items = <>
    end
  end
  object pnl_botonera: TPanel
    Left = 0
    Top = 447
    Width = 695
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 498
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btn_Salir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btn_SalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object btn_Aceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btn_AceptarClick
        end
        object btn_Cancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = btn_CancelarClick
        end
      end
    end
  end
  object tbl_DepositoEmpresasCobranza: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'DepositoEmpresasCobranza'
    Left = 14
    Top = 450
  end
  object sp_InsertarDepositoEmpresaCobranza: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarDepositoEmpresaCobranza;1'
    Parameters = <>
    Left = 48
    Top = 452
  end
  object sp_ActualizarDepositoEmpresaCobranza: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDepositoEmpresaCobranza;1'
    Parameters = <>
    Left = 84
    Top = 452
  end
  object sp_EliminarDepositoEmpresaCobranza: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarDepositoEmpresaCobranza;1'
    Parameters = <>
    Left = 120
    Top = 452
  end
end
