{-----------------------------------------------------------------------------
 File Name: CatalogoImagenes.pas
 Author:    gcasais
 Date Created: 23/12/2004
 Language: ES-AR
 Description: ABM de imágenes para impresión masiva

 Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-----------------------------------------------------------------------------}
unit CatalogoImagenes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb, PeaTypes,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls, ListBoxEx, DBListEx, Variants,
  RStrings;

type
  TfrmCatalogo = class(TForm)
	GroupB: TPanel;
	Panel2: TPanel;
	Notebook: TNotebook;
    tblCatalogo: TADOTable;
	Panel1: TPanel;
    AbmToolbar1: TAbmToolbar;
	Label2: TLabel;
    Label3: TLabel;
    spActualizarCatalogoImprenta: TADOStoredProc;
	Label7: TLabel;
	Label10: TLabel;
    ListaImagenes: TAbmList;
    txtNombre: TEdit;
    txtDescripcion: TEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
	procedure BtnCancelarClick(Sender: TObject);
	procedure ListaImagenesClick(Sender: TObject);
	procedure ListaImagenesDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaImagenesEdit(Sender: TObject);
	procedure ListaImagenesRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
	procedure ListaImagenesDelete(Sender: TObject);
	procedure ListaImagenesInsert(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
	function ListaImagenesProcess(Tabla: TDataSet; var Texto: String): Boolean;
  private
	{ Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializar: Boolean;
  end;

var
  frmCatalogo: TfrmCatalogo;

implementation

{$R *.DFM}

function TfrmCatalogo.Inicializar:Boolean;
Var
	S: TSize;
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblCatalogo]) then exit;
	Notebook.PageIndex := 0;
	ListaImagenes.Reload;
	Result := True;
end;

procedure TfrmCatalogo.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TfrmCatalogo.ListaImagenesClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   txtNombre.Text	:= FieldByName('Nombre').AsString;
	   txtDescripcion.Text	:= FieldByname('Descripcion').AsString;
	end;
end;

procedure TfrmCatalogo.ListaImagenesDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Trim(Tabla.FieldbyName('Nombre').AsString));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TfrmCatalogo.ListaImagenesEdit(Sender: TObject);
begin
	ListaImagenes.Enabled:= False;
	ListaImagenes.Estado := modi;
    Notebook.PageIndex := 1;
	groupb.Enabled     := True;
    ActiveControl:= txtDescripcion;
end;

procedure TfrmCatalogo.ListaImagenesRefresh(Sender: TObject);
begin
	 if ListaImagenes.Empty then LimpiarCampos;
end;

procedure TfrmCatalogo.LimpiarCampos;
begin
	txtNombre.Clear;
	txtDescripcion.Clear;
end;

procedure TfrmCatalogo.AbmToolbar1Close(Sender: TObject);
begin
	 close;
end;

procedure TfrmCatalogo.ListaImagenesDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;

	If MsgBox('Desea Eliminar Esta Imagen', 'Eliminar', MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            QueryExecute(DMConnections.BaseCAC, 'DELETE FROM CATALOGOIMAGENESIMPRENTA WHERE CODIGO = ' +
              IntToStr(tblCatalogo.FieldByName('Codigo').AsInteger));
		Except
			On E: Exception do begin
				MsgBoxErr('Error', e.message, 'Error', MB_ICONSTOP);
			end
		end
	end;
	ListaImagenes.Reload;
	ListaImagenes.Estado := Normal;
	ListaImagenes.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TfrmCatalogo.ListaImagenesInsert(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled          	:= True;
	ListaImagenes.Enabled := False;
	Notebook.PageIndex      	:= 1;
	ActiveControl           	:= txtNombre;
end;

procedure TfrmCatalogo.BtnAceptarClick(Sender: TObject);

begin
	with ListaImagenes do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarCatalogoImprenta, Parameters do begin
                    if ListaImagenes.Estado = Alta then ParamByName('@Accion').Value := 0
                      else ParamByName('@Accion').Value := 1;
                    ParamByName('@Codigo').Value := tblCatalogo.FieldByName('Codigo').AsInteger;
                    ParamByName('@Nombre').Value := Trim(txtNombre.Text);
					ParamByName('@Descripcion').Value := Trim(txtDescripcion.Text);
					ExecProc;
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr('Error', e.message, 'Error', MB_ICONERROR);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

procedure TfrmCatalogo.FormShow(Sender: TObject);
begin
	ListaImagenes.Reload;
end;

procedure TfrmCatalogo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;

procedure TfrmCatalogo.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TfrmCatalogo.Volver_Campos;
begin
	ListaImagenes.Estado := Normal;
	ListaImagenes.Enabled:= True;
	ActiveControl       := ListaImagenes;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;
	ListaImagenes.Reload;
end;

function TfrmCatalogo.ListaImagenesProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Result := True;
end;


end.

