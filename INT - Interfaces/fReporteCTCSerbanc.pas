unit fReporteCTCSerbanc;

interface

uses
 // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils,
    // Parametros GEnerales
    ConstParametrosGenerales,
   // DB
    UtilDB, DMConnection,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB, ppDBPipe, ppPrnabl, ppClass,
  ppCtrls, ppBands, ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB;


type
  TfrmReporteCTCSerbanc = class(TForm)
    spObtenerDatosReporteCambiosSerbanc: TADOStoredProc;
    dsObtenerDatosReporte: TDataSource;
    RBI: TRBInterface;
    ppDBReporte: TppDBPipeline;
    ppDBReporteppField1: TppField;
    ppReporte: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLblFechaProceso: TppLabel;
    ppLblNombreArchivo: TppLabel;
    ppLblUsuario: TppLabel;
    ppLblNumeroProceso: TppLabel;
    ppLblRecibidos: TppLabel;
    ppLblRechazados: TppLabel;
    ppLblValidos: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLine3: TppLine;
    lbl_usuario: TppLabel;
    ppLabel12: TppLabel;
    pplblCargados: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppDBCalc1: TppDBCalc;
    ppLabel11: TppLabel;
    procedure RBIExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigoOperacionInterfase: Integer;
    FError: AnsiString;
    function MostrarReporte(CodigoOperacionInterfase: Integer; Titulo: AnsiString; var Error: AnsiString): boolean;
  end;

var
  frmReporteCTCSerbanc: TfrmReporteCTCSerbanc;

implementation

{$R *.dfm}
function TfrmReporteCTCSerbanc.MostrarReporte(CodigoOperacionInterfase: Integer; Titulo: AnsiString; var Error: AnsiString): boolean;
resourcestring
    MSG_CANCELED = 'Ejecuci�n del reporte cancelada por el usuario';
begin
    Result := False;
    FCodigoOperacionInterfase := CodigoOperacionInterfase;
    try
        RBI.Caption := 'Reporte de ' + Titulo;
        if not RBI.Execute(True) then begin
            Error := FError;
            Exit;
        end;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
            Exit;
        end;
    end;
end;

procedure TfrmReporteCTCSerbanc.RBIExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_CANT_GATHER_DATA_FOR_REPORT = 'No se pueden obtener los datos para el reporte';
    MSG_THERE_IS_NOTHING_TO_REPORT  = 'No hay datos para generar el reporte';
const
    FORMATO_CANTIDAD     = '#,##0' ;
    FORMATO_FECHA        = 'dd-mm-yyyy';
var
    FechaProceso: TDateTime;
    UsuarioProceso, NombreArchivo: AnsiString;
    RegValidos, RegRecibidos, RegRechazados: Integer;
begin
    // intento abrir el ssp de datos del reporte

    try
        spObtenerDatosReporteCambiosSerbanc.Close;
        spObtenerDatosReporteCambiosSerbanc.Parameters.ParamByName('@CodigoOperacion').Value :=  FCodigoOperacionInterfase;
        spObtenerDatosReporteCambiosSerbanc.Open;

        FechaProceso    :=  spObtenerDatosReporteCambiosSerbanc.Parameters.ParamByName('@FechaProceso').Value;
        UsuarioProceso  :=  spObtenerDatosReporteCambiosSerbanc.Parameters.ParamByName('@UsuarioResponsable').Value;
        NombreArchivo   :=  spObtenerDatosReporteCambiosSerbanc.Parameters.ParamByName('@NombreArchivo').Value;
        RegRecibidos    :=  spObtenerDatosReporteCambiosSerbanc.Parameters.ParamByName('@CantidadErrores').Value +
                            spObtenerDatosReporteCambiosSerbanc.Parameters.ParamByName('@CantidadInsertados').Value;
        RegValidos      :=  spObtenerDatosReporteCambiosSerbanc.Parameters.ParamByName('@CantidadInsertados').Value;
        RegRechazados   :=  spObtenerDatosReporteCambiosSerbanc.Parameters.ParamByName('@CantidadErrores').Value;
    except
        on e:exception do begin
            FError :=  e.Message;
            Cancelled := True;
            Exit;
        end;
    end;

    pplblNumeroProceso.Caption  := Format( 'Proceso N� %d', [FCodigoOperacionInterfase]);
    pplblFechaProceso.Caption   := Format( 'Fecha de Proceso: %s; Hora: %s',[FormatDateTime(FORMATO_FECHA, FechaProceso),FormatDateTime('HH:nn', FechaProceso)]);
    ppLblUsuario.Caption        := TRIM(UsuarioProceso);
    ppLblRecibidos.Caption      := FormatFloat(FORMATO_CANTIDAD,RegRecibidos);
    ppLblRechazados.Caption     := FormatFloat(FORMATO_CANTIDAD,RegRechazados);
    ppLblValidos.Caption        := FormatFloat(FORMATO_CANTIDAD,RegValidos);
    pplblCargados.Caption       := FormatFloat(FORMATO_CANTIDAD,RegValidos);
    ppLblNombreArchivo.Caption  := TRIM(NombreArchivo);
end;

end.


