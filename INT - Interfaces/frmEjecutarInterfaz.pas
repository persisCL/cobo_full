unit frmEjecutarInterfaz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ADODB,
  DMConnection, UtilProc, Util, UtilDB, PeaProcs, PeaTypes, ComunesInterfaces,
  ConstParametrosGenerales;

type
  TFormEjecutarInterfaz = class(TForm)
    Bevel: TBevel;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lblFechaHoraProceso: TLabel;
    lblFechaHoraFinalizacion: TLabel;
    lblNombreArchivo: TLabel;
    lblLineasArchivo: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lblFinalizoOK: TLabel;
    lblObservaciones: TLabel;
    lbl8: TLabel;
    lblUsuario: TLabel;
    Label1: TLabel;
    lblMontoTotal: TLabel;
    btnProcesar: TButton;
    btnSalir: TButton;
    procedure btnProcesarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations } 
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;
    FCodigoModulo: Integer;
  public
    { Public declarations }   
    function Inicializar(CodigoModulo: Integer): Boolean;
  end;

var
  FormEjecutarInterfaz: TFormEjecutarInterfaz;

implementation

{$R *.dfm}

procedure TFormEjecutarInterfaz.btnProcesarClick(Sender: TObject);
resourcestring
	MSG_INIT_ERROR = 'Error al ejecutar el proceso';
	MSG_ERROR = 'Error';
    MSG_EXITO = 'El proceso ser� ejecutado. Recibir� un email cuando el mismo finalice.';
var
    spInterfacesPendientes_INSERT: TADOStoredProc;
begin
    spInterfacesPendientes_INSERT := TADOStoredProc.Create(nil);
    try
        try
            with spInterfacesPendientes_INSERT do begin
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'InterfacesPendientes_INSERT';

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoModulo').Value := FCodigoModulo;
                Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                Parameters.ParamByName('@ErrorDescription').Value := '';

                ExecProc;

                if Parameters.ParamByName('@RETURN_VALUE').Value = 0 then begin
                    MsgBox(MSG_EXITO);
                    Close;
                end else begin
                    MsgBox(Parameters.ParamByName('@ErrorDescription').Value);
                end;
            end;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        spInterfacesPendientes_INSERT.Free;
    end;
end;

procedure TFormEjecutarInterfaz.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormEjecutarInterfaz.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

function TFormEjecutarInterfaz.Inicializar(CodigoModulo: Integer): Boolean;
resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
    QRY_DESCRIPCION_MODULO = 'SELECT dbo.Modulos_GetDescription(%d)';
var
    spLogOperacionesInterfases_SELECT: TADOStoredProc;
begin
    Result := True;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));
    Color := FColorMenu;

    FCodigoModulo := CodigoModulo;
            
    //Centro el formulario
	CenterForm(Self);
    
	spLogOperacionesInterfases_SELECT := TADOStoredProc.Create(nil);
    try
        try
            // Se obtiene c�digo de m�dulo para la operaci�n
            Self.Caption := 'Ejecutar ' + QueryGetValue(DMConnections.BaseCAC, Format(QRY_DESCRIPCION_MODULO, [CodigoModulo]));

            // Se obtiene la �ltima ejecuci�n del m�dulo
            with spLogOperacionesInterfases_SELECT do begin
                ProcedureName := 'LogOperacionesInterfases_SELECT';
                Connection := DMConnections.BaseCAC;

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoModulo').Value := FCodigoModulo;
                Parameters.ParamByName('@ErrorDescription').Value := '';
                Open;

                if Parameters.ParamByName('@RETURN_VALUE').Value = 0 then begin
                    // Si la consulta fue exitosa, se cargan los Label
                    lblFechaHoraProceso.Caption := FieldByName('FechaHoraRecepcion').AsString;
                    lblFechaHoraFinalizacion.Caption := FieldByName('FechaHoraFinalizacion').AsString;
                    lblNombreArchivo.Caption := FieldByName('NombreArchivo').AsString;
                    lblLineasArchivo.Caption := FieldByName('LineasArchivo').AsString; 
                    lblMontoTotal.Caption := FieldByName('MontoArchivo').AsString;
                    lblFinalizoOK.Caption := IIf(FieldByName('FinalizoOK').AsBoolean, 'SI', 'NO');
                    lblObservaciones.Caption := FieldByName('Observaciones').AsString;
                    lblUsuario.Caption := FieldByName('Usuario').AsString;

                    if lblFechaHoraFinalizacion.Caption = EmptyStr then
                        lblFechaHoraFinalizacion.Caption := 'SIN FINALIZAR';
                        
                end else begin
                    // Si hubo error, se muestra mensaje
                    MsgBox(Parameters.ParamByName('@ErrorDescription').Value);
                    Result := False;
                    Exit;
                end;
            end;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                Result := False;
                Exit;
            end;
        end; 
    finally
        spLogOperacionesInterfases_SELECT.Free;
    end;

    if Result then Self.Show;

end;

end.
