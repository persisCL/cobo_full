{-----------------------------------------------------------------------------
 File Name: frmGeneracionDebitosCMR.pas
 Author:    FLamas
 Date Created: 21/06/2005
 Language: ES-AR
 Description: Generaci�n de D�bitos a Tarjetas CMR-Falabella
	Revision 1
	Autor: sguastoni
	Fecha: 05/01/2007
	Descripcion: se agrega parametro a sp  spActualizarComprobantesEnviadosCMR
    Revision : 2
        Author : vpaszkowicz
        Date : 16/07/2007
        Description : Cambio la forma de proceso. En lugar de usar el store
        spActualizarComprobantesEnviadosSantander uso una funci�n implementada
        en ComunesInterfaces y la llamo por cada comprobante que voy insertando
        en el archivo, dado que ya los tengo calculados

    Revision : 3
        Author : jconcheyro
        Date : 08/08/2007
        Description : Agrego la lista de Grupos de facturacion para pasarle como
        parametro al store un lista con los grupos a tener en cuenta



  Revision :2
    Date: 13/04/2009
    Author: mpiazza
    Description: ss750 se actualiza el log de interfase con el monto
                 y cantidad de registros totales para el reporte


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

    Autor           :   Claudio Quezada
    Fecha           :   06-01-2015
    Firma           :   SS_1147_CQU_20150106
    Descripci�n     :   Se agrega la l�gica para que determine si usa formato VS o CN

    Autor           :   Claudio Quezada
    Fecha           :   31-03-2015
    Firma           :   SS_1147_CQU_20150324
    Descripcion     :   Se corrige el Header, a pesar de que el documento de Falabella
                        indica vacios los ultimos 99 caractareres, el documento
                        de VS indica rellenar con 0

    Firma       : SS_1147_NDR_20141216
    Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

    Firma       : SS_1147_MCA_20150417
    Descripcion : se aplica solo header para los debitos de CN y VS

    Firma       :   SS_1147_CQU_20150519
    Descripcion :   Se agrega el TipoComprobante a la funci�n ActualizarDetalleComprobanteEnviado

    Autor       :   Claudio Quezada
    Fecha       :   16-06-2015
    Firma       :   SS_1314_CQU_20150908
    Descripcion :   Homologar CMR de VS al de CN
                    El NumeroConvenio viene listo desde la BD ac� se rellena con 0
					Se agrega el tipo de comprobante fiscal en el caso de VS
-  
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

-----------------------------------------------------------------------------}
unit frmGeneracionDebitosCMR;

interface

uses
  //Debitos CMR-Falabella
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  PeaTypes,
  UtilDB,
  ComunesInterfaces,
  FrmRptEnvioComprobantes,       //Reporte de Comprobantes Enviados
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, BuscaTab, DmiCtrls, DPSControls, VariantComboBox, StrUtils,
  ppCtrls, ppPrnabl, ppClass, ppBands, ppDB, ppDBPipe, UtilRB, ppParameter,
  ppModule, raCodMod, ppCache, ppComm, ppRelatv, ppProd, ppReport, DBClient,
  ImgList;


type

  TFGeneracionDebitosCMR = class(TForm)
	Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    OpenDialog: TOpenDialog;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    spObtenerComprobantesCMR: TADOStoredProc;
    lblFechaInterface: TLabel;
    spObtenerInterfasesEjecutadas: TADOStoredProc;
    edFecha: TBuscaTabEdit;
    buscaInterfaces: TBuscaTabla;
    spObtenerInterfasesEjecutadasFecha: TDateTimeField;
    spObtenerInterfasesEjecutadasDescripcion: TStringField;
    spObtenerInterfasesEjecutadasCodigoOperacionInterfase: TIntegerField;
    ppReporteCargosCMR: TppReport;
    ppDetailBand1: TppDetailBand;
    raCodeModule2: TraCodeModule;
    ppParameterList1: TppParameterList;
    rbiListado: TRBInterface;
    ppDBReporteDebitos: TppDBPipeline;
    ppHeaderBand3: TppHeaderBand;
    ppLabel30: TppLabel;
    ppLabel32: TppLabel;
    ppLabel33: TppLabel;
    ppImage3: TppImage;
    ppLabel34: TppLabel;
    ppLabel36: TppLabel;
    ppLabel38: TppLabel;
    ppLabel42: TppLabel;
    ppLine5: TppLine;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    pplblMontos: TppLabel;
    pplblTransacciones: TppLabel;
    spReporteDebitosCMR: TADOStoredProc;
    dsReporteDebitosCMR: TDataSource;
    pplblMontosBase: TppLabel;
    pplblTransaccionesBase: TppLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    edDestino: TEdit;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    lblUltimosGruposFacturados: TLabel;
    dblGrupos: TDBListEx;
    dsGrupos: TDataSource;
    ilImages: TImageList;
    cdGrupos: TClientDataSet;
    spObtenerUltimosGruposFacturados: TADOStoredProc;
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    function  buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);
    procedure edFechaChange(Sender: TObject);
    procedure rbiListadoExecute(Sender: TObject; var Cancelled: Boolean);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnSalirClick(Sender: TObject);
    procedure dblGruposDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblGruposDblClick(Sender: TObject);

  private
    { Private declarations }
    FCodigoOperacion : integer;
	  FCodigoOperacionAnterior : integer;
  	FDetenerImportacion: boolean;
    FErrorMsg : string;
    FDebitosTXT	: TStringList;
    FObservaciones : string;
  	FCMR_Directorio_Destino_Debitos : string;
    FMonto : Int64;
    FMontoTotal     : Int64;
    FCantidadTotal  : Int64;
    FCodigoNativa : Integer;        // SS_1147_CQU_20150106
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    FCMR_CodigodeAutopista : Ansistring;                                        // SS_1314_CQU_20150908
  	Function  CrearNombreArchivo(var NombreArchivo: AnsiString; dFecha : TDateTime): Boolean;
  	Function  RegistrarOperacion : boolean;
	  Function  GenerarDebitosCMR : boolean;
    //Function  ActualizarDebitosCMR:boolean;
    Function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
    function CargarGrupos: Boolean;
    function GenerarListaGruposAIncluir(var Lista: string): boolean;
    function ExistenGruposSeleccionados: boolean;
    public
	function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean; bReprocesar : boolean = False ) : Boolean;
	{ Public declarations }
  end;

var
  FGeneracionDebitosCMR: TFGeneracionDebitosCMR;

const
  RO_MOD_INTERFAZ_SALIENTE_DEBITOS_CMR = 52;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    FLamas
  Date Created: 21/06/2005
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: txtCaption: ANSIString; MDIChild:Boolean; bReprocesar : boolean = False
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGeneracionDebitosCMR.Inicializar(txtCaption: ANSIString; MDIChild:Boolean; bReprocesar : boolean = False ) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 19/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        CMR_DIRECTORIO_DESTINO_DEBITOS = 'CMR_Directorio_Destino_Debitos';
        CMR_CODIGODEAUTOPISTA             = 'CMR_CodigodeAutopista';            // SS_1314_CQU_20150908
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_DIRECTORIO_DESTINO_DEBITOS , FCMR_Directorio_Destino_Debitos) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_DIRECTORIO_DESTINO_DEBITOS;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FCMR_Directorio_Destino_Debitos := GoodDir(FCMR_Directorio_Destino_Debitos);
                if  not DirectoryExists(FCMR_Directorio_Destino_Debitos) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FCMR_Directorio_Destino_Debitos;
                    Result := False;
                    Exit;
                end;
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_CODIGODEAUTOPISTA , FCMR_CodigodeAutopista) then begin    // SS_1314_CQU_20150908
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_CODIGODEAUTOPISTA;                                          // SS_1314_CQU_20150908
                    Result := False;                                                                                                // SS_1314_CQU_20150908
                    Exit;                                                                                                           // SS_1314_CQU_20150908
                end;                                                                                                                // SS_1314_CQU_20150908
            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
	MSG_DEBITS_PROCESSING = 'Procesamiento de d�bitos';
	MSG_DEBITS_REPROCESSING = 'Re-procesamiento de d�bitos';
	MSG_ERROR_FILE_ALREADY_CREATED = 'El archivo %s ya fue generado.'#10#13 + 'Desea volver a generarlo ?';
var
    NombreArchivo: AnsiString;

begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

  	if not MDIChild then begin
    		FormStyle := fsNormal;
    		Visible := False;
  	end;
  	CenterForm(Self);
  	Caption := AnsiReplaceStr(txtCaption, '&', '');

  	try
  		DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and
                            VerificarParametrosGenerales;
        if not Result  then Exit;
        FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150106
  	except
    		on e: Exception do begin
		    	MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
    			Exit;
    		end;
	  end;

    if not CargarGrupos then begin
        Result := False;
        Exit;
    end;

  	lblFechaInterface.Visible := bReprocesar;
  	edFecha.Visible := bReprocesar;

    if not bReprocesar then begin

      	lblNombreArchivo.Top := 13;
        edDestino.Top := 32;
        FObservaciones := MSG_DEBITS_PROCESSING;
  		  btnProcesar.Enabled := DirectoryExists( FCMR_Directorio_Destino_Debitos ); // Verifica si existe el Directorio de Pagomatico

        //Creo el nombre del archivo a enviar
        CrearNombreArchivo(NombreArchivo, Now);
        eddestino.Text:= NombreArchivo;

        if  VerificarArchivoProcesado( DMConnections.BaseCAC, edDestino.text ) then begin
            lblReferencia.Caption := '';
    		    if (MsgBox(Format(MSG_ERROR_FILE_ALREADY_CREATED, [ExtractFileName(edDestino.text)]), Caption, MB_YESNO + MB_ICONQUESTION) <> IDYES) then begin
                Exit;
            end;
        end;

	end else begin

    	  FObservaciones := MSG_DEBITS_REPROCESSING;
        spObtenerInterfasesEjecutadas.Parameters.ParamByName( '@CodigoModulo' ).Value := RO_MOD_INTERFAZ_SALIENTE_DEBITOS_CMR;
        spObtenerInterfasesEjecutadas.CommandTimeout := 500;
        spObtenerInterfasesEjecutadas.Open;
        
	end;

	btnCancelar.Enabled := False;
	pnlAvance.Visible := False;
	lblReferencia.Caption := '';
	result := True;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFGeneracionDebitosCMR.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_DEBITOS         = ' ' + CRLF +
                          'El Archivo de Debitos es' + CRLF +
                          'utilizado por el ESTABLECIMIENTO para informar a FALABELLA' + CRLF +
                          'el detalle de los pagos a recaudar' + CRLF +
                          ' ' + CRLF +
                          //'Nombre del Archivo: CCNOMI04MMDDN.SSS' + CRLF +            // SS_1314_CQU_20150908
                          'Nombre del Archivo: CCNOMI%sMMDDN.SSS' + CRLF +              // SS_1314_CQU_20150908
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    //MsgBoxBalloon(MSG_DEBITOS, Caption, MB_ICONQUESTION, IMGAYUDA);                   // SS_1314_CQU_20150908
    MsgBoxBalloon(Format(MSG_DEBITOS, [FCMR_CodigodeAutopista]),                        // SS_1314_CQU_20150908
                Caption, MB_ICONQUESTION, IMGAYUDA);                                    // SS_1314_CQU_20150908
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TFGeneracionDebitosCMR.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: CrearNombreArchivo
  Author:    FLamas
  Date Created: 21/06/2005
  Description:	Crea el Nombre del Archivo a Enviar
  Parameters: var NombreArchivo: AnsiString; dFecha : TDateTime
  Return Value: Boolean
-----------------------------------------------------------------------------
  Revision :1
  Date: 23/09/2009
  Author: mpiazza
  Description: ss750 se incrementa a 200 segundos  el comandTimeOut
                    de ObtenerUltimosGruposFacturados
-----------------------------------------------------------------------------}
function TFGeneracionDebitosCMR.CargarGrupos: Boolean;
begin
    Result := False;
    spObtenerUltimosGruposFacturados.Close;

    try
        cdGrupos.CreateDataSet;
        spObtenerUltimosGruposFacturados.CommandTimeout := 200;
        spObtenerUltimosGruposFacturados.Open;
        Result := not spObtenerUltimosGruposFacturados.IsEmpty;
        while not spObtenerUltimosGruposFacturados.Eof do begin
            // Insertamos nuestro registro
            cdGrupos.AppendRecord([False, spObtenerUltimosGruposFacturados.FieldByName('CodigoGrupoFacturacion').AsInteger,
              spObtenerUltimosGruposFacturados.FieldByName('FechaProgramadaEjecucion').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaCorte').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaEmision').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaVencimiento').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('NoTerminados').AsInteger
              ]);
            spObtenerUltimosGruposFacturados.Next;
        end;
        spObtenerUltimosGruposFacturados.Close;
        cdGrupos.First;
    except
        on e: Exception do begin
            MsgBoxErr('Error obteniendo los �ltimos grupos facturados', e.Message, Caption, MB_ICONERROR);
            Result:= False;
        end;
    end;

end;

Function TFGeneracionDebitosCMR.CrearNombreArchivo(var NombreArchivo: AnsiString; dFecha : TDateTime): Boolean;
Const
    //FILE_NAME = 'CCNOMI04';           // SS_1314_CQU_20150908
    FILE_NAME = 'CCNOMI%s';             // SS_1314_CQU_20150908
    FILE_DATE_FORMAT = 'mmdd';
    FILE_APPEND = 'N';
    FILE_EXTENSION = '.001';
    //DEBITO_FALABELLA = 'CCNOMI03';    // SS_1314_CQU_20150908   // SS_1147_CQU_20150106
var                                     // SS_1314_CQU_20150908
    FiltroNombreArchivio : AnsiString;  // SS_1314_CQU_20150908
begin
    Result:= False;
    try
        //Obtengo el codigo de comercio
        if FCodigoNativa = CODIGO_VS then                                                                                                                                       // SS_1147_CQU_20150106
            //NombreArchivo := GoodFileName( FCMR_Directorio_Destino_Debitos + DEBITO_FALABELLA + FormatDateTime ( FILE_DATE_FORMAT , dFecha ) + FILE_APPEND, FILE_EXTENSION )  // SS_1314_CQU_20150908  // SS_1147_CQU_20150106
            FiltroNombreArchivio := Format(FILE_NAME, [FCMR_CodigodeAutopista]);                                                                                                // SS_1314_CQU_20150908
        //else                                                                                                                                                                  // SS_1314_CQU_20150908  // SS_1147_CQU_20150106
        //NombreArchivo := GoodFileName( FCMR_Directorio_Destino_Debitos + FILE_NAME + FormatDateTime ( FILE_DATE_FORMAT , dFecha ) + FILE_APPEND, FILE_EXTENSION );            // SS_1314_CQU_20150908
        NombreArchivo := GoodFileName( FCMR_Directorio_Destino_Debitos + FiltroNombreArchivio + FormatDateTime ( FILE_DATE_FORMAT , dFecha ) + FILE_APPEND, FILE_EXTENSION );   // SS_1314_CQU_20150908
        Result:= True;
    except
    end;
end;

procedure TFGeneracionDebitosCMR.dblGruposDblClick(Sender: TObject);
begin
    cdGrupos.Edit;
    if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
        cdGrupos.FieldByName('Seleccionado').AsBoolean := False;
    end else begin
        if ( cdGrupos.FieldByName('NoTerminados').AsInteger > 0 ) then
        begin
            ShowMessage('Este Proceso de Facturacion tiene comprobantes no terminados');
            cdGrupos.FieldByName('Seleccionado').AsBoolean := False;
        end
        else
            cdGrupos.FieldByName('Seleccionado').AsBoolean := True;
    end;
    cdGrupos.Post;

end;

procedure TFGeneracionDebitosCMR.dblGruposDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    if Column.FieldName = 'Seleccionado' then begin
        if cdGrupos.FieldByName('Seleccionado').AsBoolean = True then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 1);
          end else if cdGrupos.FieldByName('Seleccionado').AsBoolean = False then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 0);
          end;
          DefaultDraw := False;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: edFechaChange
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Activa el Bot�n de Procesar si se seleccion� una Interfaz
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosCMR.edFechaChange(Sender: TObject);
begin
    btnProcesar.Enabled := ( edFecha.Text <> '' );
end;

function TFGeneracionDebitosCMR.ExistenGruposSeleccionados: boolean;
begin
    Result := False;
    try
        cdGrupos.First;
        while not cdGrupos.Eof do begin
            if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
                Result := True;
                Break;
            end;
            cdGrupos.Next;
        end;
        cdGrupos.First;
    except
        on E: Exception do begin
            MsgBoxErr('Error contando grupos para la generaci�n ', e.Message, Caption, MB_ICONERROR);
        end;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfacesProcess
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Busca las Interfaces a Procesar 
  Parameters: Tabla: TDataSet; var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGeneracionDebitosCMR.buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Result := True;
	with Tabla do
    	Texto := FieldByName('Descripcion').AsString + ' - ' + FieldByName('Fecha').AsString;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfacesSelect
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Selecciona una interfaz a Procesar
  Parameters: Sender: TObject; Tabla: TDataSet
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosCMR.buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);
var
    NombreArchivo:Ansistring;
begin
	edFecha.Text := FormatDateTime( 'dd-mm-yyyy', Tabla.FieldByName('Fecha').AsDateTime);
    FCodigoOperacionAnterior := Tabla.FieldByName('CodigoOperacionInterfase').Value;
    //Creo el nombre del archivo a enviar
    CrearNombreArchivo(NombreArchivo, Tabla.FieldByName('Fecha').AsDateTime);
    edDestino.Text:= NombreArchivo;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Registra la Operaci�n de Interfase
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFGeneracionDebitosCMR.RegistrarOperacion : boolean;
resourcestring
	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
	DescError : string;
begin
   result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_SALIENTE_DEBITOS_CMR, ExtractFileName(edDestino.text), UsuarioSistema, FObservaciones, False, edFecha.Visible,  NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError );
   if not result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarDebitosCMR
  Author:    flamas
  Date Created: 03/01/2005
  Description: Genera el archivo de d�bitos de Presto
  Parameters: None
  Return Value: boolean
  Revision : 2
      Author : vpaszkowicz
      Date : 16/07/2007
      Description : Subo a esta parte el registro del comprobante usado ya que
      tengo todos los comprobantes en el store y no es necesario volverlos a
      calcular.
-----------------------------------------------------------------------------
  Revision 2
  mpiazza
  15/04/2009
  Ref: SS-750 Se agrega Monto a ActualizarDetalleComprobanteEnviado
-----------------------------------------------------------------------------}
function TFGeneracionDebitosCMR.GenerarDebitosCMR : boolean;

      {-----------------------------------------------------------------------------
        Function Name: ArmarLineaTXT
        Author:    flamas
        Date Created: 04/01/2005
        Description:	Arma la l�nea del TXT
        Parameters: None
        Return Value: string
      -----------------------------------------------------------------------------
        Revision :2
        Date: 13/04/2009
        Author: mpiazza
        Description: ss750 se actualiza el log de interfase con el monto
                     y cantidad de registros totales para el reporte
      -----------------------------------------------------------------------------}
      function ArmarLineaTXT : string;
      begin

          with spObtenerComprobantesCMR do begin
            //if FCodigoNativa = CODIGO_VS then begin                                                     // SS_1314_CQU_20150908   // SS_1147_CQU_20150106
            //    result :=   PADL(Trim(FieldByName('Importe').AsString) + '00', 13, '0') +               // SS_1314_CQU_20150908   // SS_1147_CQU_20150106
            //                PADL(Trim(FieldByName('NumeroConvenio').AsString), 12, '0') +               // SS_1314_CQU_20150908   // SS_1147_CQU_20150106
            //                PADL(' ', 8, ' ') +                                                         // SS_1314_CQU_20150908   // SS_1147_CQU_20150106
            //                FieldByName('TipoDocumento').AsString +                                     // SS_1314_CQU_20150908   // SS_1147_CQU_20150106
            //                //PADL(Trim(FieldByName('NumeroComprobante').AsString), 10, '0') +          // SS_1314_CQU_20150908   // SS_1147_CQU_20150305 // SS_1147_CQU_20150106
            //                PADL(Trim(FieldByName('NumeroComprobanteFiscal').AsString), 10, '0') +      // SS_1314_CQU_20150908   // SS_1147_CQU_20150305
            //                PADR(' ',  3, ' ') + // Blancos                                             // SS_1314_CQU_20150908
            //                PADR('0',  8, '0') + // C�d. Autoriz.                                       // SS_1314_CQU_20150908
            //                PADR(' ',  3, ' ') + // Blancos                                             // SS_1314_CQU_20150908
            //                PADR(' ', 16, ' ') + // Glosa Resp                                          // SS_1314_CQU_20150908
            //                PADR('0',  8, '0') + // Fecha Proc.                                         // SS_1314_CQU_20150908
            //                '0200' + // C�d Trans. 200 Cargar                                           // SS_1314_CQU_20150908
            //                PADR('0',  8, '0') + // Fecha Liqu.                                         // SS_1314_CQU_20150908
            //                PADR('0',  2, '0') + // D�a Venc.                                           // SS_1314_CQU_20150908
            //                PADL(Trim(FieldByName('NumeroTarjeta').AsString), 16, '0') +                // SS_1314_CQU_20150908
            //                PADR(' ', 20, ' ');// Observ.                                               // SS_1314_CQU_20150908
            //end else begin                                                                              // SS_1314_CQU_20150908   // SS_1147_CQU_20150106
              result :=  PADL(Trim(FieldByName('Importe').AsString + '00'), 13, '0') +
                          //Copy(FieldByName('NumeroConvenio').AsString, 6, 12) +                         // SS_1314_CQU_20150908
                          PadL(Trim(FieldByName('NumeroConvenio').AsString), 12, '0') +                   // SS_1314_CQU_20150908
                          PADR(' ', 8, ' ') +
                          //PADL(Trim(FieldByName('NumeroComprobante').AsString), 12, '0') +              // SS_1314_CQU_20150908
                          IIf((FCodigoNativa <> CODIGO_VS),                                               // SS_1314_CQU_20150908
                                PADL(Trim(FieldByName('NumeroComprobante').AsString), 12, '0'),           // SS_1314_CQU_20150908
                                (FieldByName('TipoDocumento').AsString +                                  // SS_1314_CQU_20150908
                                PADL(Trim(FieldByName('NumeroComprobanteFiscal').AsString), 10, '0'))     // SS_1314_CQU_20150908
                          ) +                                                                             // SS_1314_CQU_20150908
                          PADR(' ',  3, ' ') + // Blancos
                          PADR('0',  8, '0') + // C�d. Autoriz.
                          PADR(' ',  3, ' ') + // Blancos
                          PADR(' ', 16, ' ') + // Glosa Resp
                          PADR('0',  8, '0') + // Fecha Proc.
                          '0200' + // C�d Trans. 200 Cargar
                          PADR('0',  8, '0') + // Fecha Liqu.
                          PADR('0',  2, '0') + // D�a Venc.
                          PADL(Trim(FieldByName('NumeroTarjeta').AsString), 16, '0') +
                          PADR(' ', 20, ' ');// Observ.
            //end;                                                                                        // SS_1314_CQU_20150908   // SS_1147_CQU_20150106
              FMonto := FMonto + FieldByName('Importe').AsInteger;
              FMontoTotal     := FMontoTotal + FieldByName('Importe').AsInteger;
              inc( FCantidadTotal)  ;
          end;                                                                                      
      end;

    {-----------------------------------------------------------------------------
      Function Name: ArmarFooterCMR
      Author:    FLamas
      Date Created: 22/06/2005
      Description:	Arma el Footer de CMR
      Parameters: None
      Return Value: string
      Revision : 1
          Author : vpaszkowicz
          Date : 11/03/2008
          Description : Le quito los ceros de decimales al footer
    -----------------------------------------------------------------------------}
    function  ArmarFooterCMR : string;
    begin
        result := 	FormatDateTime('ddmmyyyy', NowBase(DMConnections.BaseCAC)) +
                    //PADL(IntToStr(FMonto) + '00', 18, '0') +
                    PADL(IntToStr(FMonto), 18, '0') +
                    PADL(IntToStr(FDebitosTXT.Count), 8, '0') +
                    PADR(' ', 99, ' ');
    end;

    {---------------------------------------------------------------------------    // SS_1147_CQU_20150106
      Function Name :   ArmarHeaderCMR                                              // SS_1147_CQU_20150106
      Author        :   CQuezadaI                                                   // SS_1147_CQU_20150106
      Date Created  :   23/01/2015                                                  // SS_1147_CQU_20150106
      Description   :	Arma el Header de CMR para Vespucio Sur,                    // SS_1147_CQU_20150106
                        se cre� nueva funci�n para diferenciar CN de VS             // SS_1147_CQU_20150106
                        adem�s que el Footer de CN podr�a cambiar.                  // SS_1147_CQU_20150106
      Firma         :   SS_1147_CQU_20150106                                        // SS_1147_CQU_20150106
      Parameters    :   None                                                        // SS_1147_CQU_20150106
      Return Value  :   string                                                      // SS_1147_CQU_20150106
    ----------------------------------------------------------------------------}   // SS_1147_CQU_20150106
    function  ArmarHeaderCMR : string;                                              // SS_1147_CQU_20150106
    begin                                                                           // SS_1147_CQU_20150106
        result := 	FormatDateTime('ddmmyyyy', NowBase(DMConnections.BaseCAC)) +    // SS_1147_CQU_20150106
                    PADL(IntToStr(FMonto), 18, '0') +                               // SS_1147_CQU_20150106
                    PADL(IntToStr(FDebitosTXT.Count), 8, '0') +                     // SS_1147_CQU_20150106
                    //PADR(' ', 99, ' ');                                           // SS_1147_CQU_20150324  // SS_1147_CQU_20150106
                    PADR('', 99, ' ');                                              // SS_1147_CQU_20150324
    end;                                                                            // SS_1147_CQU_20150106

resourcestring
	MSG_NO_PENDING_DEBITS 	 			= 'No hay d�bitos pendientes de env�o';
	MSG_COULD_NOT_CREATE_FILE			= 'No se pudo crear el archivo de d�bitos';
	MSG_COULD_NOT_GET_PENDING_DEBITS 	= 'No se pudieron obtener los d�bitos pendientes';
    MSG_GENERATING_DEBITS_FILE			= 'Generando archivo de d�bitos - Cantidad de d�bitos : %d ';
    MSG_PROCESSING			 			= 'Procesando...';
    MSG_OBTAINING_DATA			 		= 'Obteniendo datos a enviar al archivo...';
var
    ListaGrupos: string;
begin
	try
    	Screen.Cursor := crHourglass;
		result := False;
    	FMonto := 0;
        lblReferencia.Caption := MSG_OBTAINING_DATA;
        Application.ProcessMessages;
	    try
            if not GenerarListaGruposAIncluir( ListaGrupos ) then Exit ;
            spObtenerComprobantesCMR.Parameters.ParamByName( '@ListaGruposAIncluir' ).Value := ListaGrupos;
	        	spObtenerComprobantesCMR.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value := Iif (FCodigoOperacionAnterior = 0, NULL, FCodigoOperacionAnterior );
            spObtenerComprobantesCMR.CommandTimeout := 5000;
            spObtenerComprobantesCMR.Open;

            if spObtenerComprobantesCMR.IsEmpty then begin
            	  MsgBox(MSG_NO_PENDING_DEBITS, Caption, MB_ICONWARNING);
                FErrorMsg := MSG_NO_PENDING_DEBITS;
            		Screen.Cursor := crDefault;
                Exit;
            end;

        except
          	on e: Exception do begin
	            		MsgBoxErr(MSG_COULD_NOT_GET_PENDING_DEBITS, e.Message, 'Error', MB_ICONERROR);
                	FErrorMsg := MSG_COULD_NOT_GET_PENDING_DEBITS;
                  Exit;
            end;
        end;

        lblReferencia.Caption := Format( MSG_GENERATING_DEBITS_FILE, [spObtenerComprobantesCMR.RecordCount] );
        pbProgreso.Position := 0;
        pnlAvance.Visible := True;
        lblReferencia.Caption := MSG_PROCESSING;
        with spObtenerComprobantesCMR do begin
          	pbProgreso.Max := spObtenerComprobantesCMR.RecordCount;
            try
                while ( not Eof ) and ( not FDetenerImportacion ) do begin
                    //Genero cada linea del txt
                    FDebitosTXT.Add( ArmarLineaTXT );
                     if not ActualizarDetalleComprobanteEnviado(DMConnections.BaseCAC,
                            FCodigoOperacion, spObtenerComprobantesCMR.FieldByName('NumeroComprobante').Value,
                            spObtenerComprobantesCMR.FieldByName('TipoComprobante').AsString,   // SS_1147_CQU_20150519
                            FErrorMsg, spObtenerComprobantesCMR.FieldByName('Importe').Value) then
                                raise Exception.Create(FErrorMsg);
                        pbProgreso.StepIt;
                        Application.ProcessMessages;
                    pbProgreso.StepIt;
                    Application.ProcessMessages;
                    Next;
                end;
            except
        	    on e: Exception do begin
                    MsgBoxErr(MSG_COULD_NOT_GET_PENDING_DEBITS, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        end;

        if ( not FDetenerImportacion ) and ( FErrorMsg = '' ) then begin
          	try
                    //if FCodigoNativa = CODIGO_VS then FDebitosTXT.Insert(0, ArmarHeaderCMR) // SS_1147_CQU_20150106
                    //else                                                                    // SS_1147_CQU_20150106
        			//	FDebitosTXT.Add(ArmarFooterCMR);
                FDebitosTXT.Insert(0, ArmarHeaderCMR); //SS_1147_MCA_20150417
              	FDebitosTXT.SaveToFile(edDestino.Text);
                result := True;
            except
              	on e: Exception do begin
                  	MsgBoxErr(MSG_COULD_NOT_CREATE_FILE, e.Message, 'Error', MB_ICONERROR);
                    FErrorMsg := MSG_COULD_NOT_CREATE_FILE;
                end;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

function TFGeneracionDebitosCMR.GenerarListaGruposAIncluir(
  var Lista: string): boolean;
var
    ListaTemporal :TStringList;
    I: integer;
begin
    Result := False;
    ListaTemporal := TStringList.Create;
    try
        try
            ListaTemporal.Duplicates := dupIgnore; // la lista ignora los duplicados
            cdGrupos.First;
            while not cdGrupos.Eof do begin
                if cdGrupos.FieldByName('Seleccionado').AsBoolean then
                    ListaTemporal.Add( IntToStr(cdGrupos.FieldByName('GrupoFacturacion').AsInteger));
                cdGrupos.Next;
            end;
            cdGrupos.First;
            for I := 0 to ListaTemporal.Count - 1 do begin
                Lista := Lista + ListaTemporal[I];
                if (I < (ListaTemporal.Count -1) ) then   // asi no me agrega una coma al final
                    Lista := Lista + ','
            end;
            Result := True;
        except
            on E: Exception do begin
                MsgBoxErr('Error generando par�metro con lista de grupos para el store', e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
            ListaTemporal.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarDebitosCMR
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Actualiza los Comprobantes Enviados
  Parameters: None
  Return Value: None
  Revision : 2
      Author : vpaszkowicz
      Date : 16/07/2007
      Description : Comento este c�digo porque de ahora en mas se hace por
      l�nea del archivo, ya que tengo los d�bitos dentro del primer store y
      no es necesario volver a calcularlos.
-----------------------------------------------------------------------------}
{Function TFGeneracionDebitosCMR.ActualizarDebitosCMR : Boolean;
resourcestring
  	MSG_UPDATING_SENT_DEBITS = 'Actualizando comprobantes enviados';
    MSG_COULD_NOT_UPDATE_SENT_DEBITS = 'Los comprobantes no se pudieron actualizar';
    MSG_ERROR = 'Error';
    MSG_PROCESSING = 'Procesando...';
begin
    Result := False;
  	Screen.Cursor := crHourglass;
    lblReferencia.Caption := MSG_UPDATING_SENT_DEBITS;
    pbProgreso.Position := 0;
    pnlAvance.Visible := True;
    lblReferencia.Caption := MSG_PROCESSING;
    Application.ProcessMessages;

	  try
    	spActualizarComprobantesEnviadosCMR.Parameters.ParamByName( '@CodigoOperacionAnterior' ).Value := iif(FCodigoOperacionAnterior=0, Null, FCodigoOperacionAnterior);
    	spActualizarComprobantesEnviadosCMR.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value := FCodigoOperacion;
        //revision 1
        spActualizarComprobantesEnviadosCMR.Parameters.ParamByName( '@Usuario' ).Value := UsuarioSistema;
        spActualizarComprobantesEnviadosCMR.CommandTimeout := 5000;
    	  spActualizarComprobantesEnviadosCMR.ExecProc;
        Result := True;
    except
    	  on e: Exception do begin
        	  MsgBoxErr(MSG_COULD_NOT_UPDATE_SENT_DEBITS, e.Message, MSG_ERROR, MB_ICONERROR);
            FErrorMsg := MSG_COULD_NOT_UPDATE_SENT_DEBITS;
        end;
    end;

    pbProgreso.Position := pbProgreso.Max;
    Screen.Cursor := crDefault;
end;}

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Genera un Reporte de Finalizaci�n
  Parameters: CodigoOperacionInterfase:integer
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFGeneracionDebitosCMR.GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer):boolean;
resourcestring
    TITLE_REPORT = 'Reporte de Generaci�n de Cargos CMR';
var                                                                                                     //SS_1147_NDR_20140710
    RutaLogo: AnsiString;                                                                               //SS_1147_NDR_20140710

begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage3.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

	  rbiListado.Caption := TITLE_REPORT;
    result := rbiListado.Execute(True);
end;


{-----------------------------------------------------------------------------
  Function Name: rbiListadoExecute
  Author:    FLamas
  Date Created: 22/06/2005
  Description: Ejecuta el Reporte de Finalizaci�n
  Parameters: Sender: TObject; var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosCMR.rbiListadoExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
  	MSG_ERROR_GENERATING_REPORT = 'Error generando reporte de finalizaci�n';
    MSG_ERROR = 'Error';
begin
	try
		pplblMontos.Caption	:= FormatFloat(FORMATO_IMPORTE, FMonto);
		pplblTransacciones.Caption 	:= IntToStr(FDebitosTXT.Count - 1);

		spReporteDebitosCMR.Close;
    spReporteDebitosCMR.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
    spReporteDebitosCMR.CommandTimeout := 5000;
		spReporteDebitosCMR.Open;

		pplblMontosBase.Caption	:= FormatFloat(FORMATO_IMPORTE, spReporteDebitosCMR.FieldByName('MontoTotal').AsInteger);
		pplblTransaccionesBase.Caption := IntToStr(spReporteDebitosCMR.FieldByName('CantidadTotal').AsInteger);

    except
      	on e: exception do begin
          	MsgBoxErr(MSG_ERROR_GENERATING_REPORT, e.Message, MSG_ERROR, MB_ICONERROR);
            Cancelled := True;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    flamas
  Date Created: 03/12/2004
  Description: Busca los d�bitos y Genera un TXT con los mismos
  Parameters: Sender: TObject
  Return Value: None
  Revision : 2
      Author : vpaszkowicz
      Date : 16/07/2007
      Description : Subo la transacci�n porque voy a ir grabando los comproban-
      tes usados uno a uno por cada l�nea del archivo.
-----------------------------------------------------------------------------
  Revision 2
    Author: mpiazza
    Date: 13/04/2009
    Description: ss750 se actualiza el log de interfase con el monto
                 y cantidad de registros totales
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosCMR.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Actualizo el log al finalizar
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ActualizarLog:boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVACION = 'Finalizo OK!';
    var
         DescError : String;
    begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);
        except
            on e: Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

resourcestring
	  MSG_PROCESS_SUCCEDED 				= 'El proceso finaliz� con �xito';
  	MSG_PROCESS_COULD_NOT_BE_COMPLETED 	= 'El proceso no se pudo completar';
  	MSG_FILE_ALREADY_EXISTS 			= 'El archivo ya existe.'#10#13'� Desea continuar ?';
    MSG_COULD_NOT_UPDATE_SENT_DEBITS    = 'Los comprobantes no se pudieron actualizar';
    MSG_ERROR_DELETE_FILE               = 'No pudo eliminar el archivo';
    MSG_ERROR = 'Error';
    MSG_SIN_GRUPOS_SELECCIONADOS = 'No se seleccionaron grupos de facturacion a incluir';
   
begin
	if FileExists( edDestino.text ) then
		if not(MsgBox(MSG_FILE_ALREADY_EXISTS, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then Exit;


    if not ExistenGruposSeleccionados  then begin
        MsgBoxBalloon(MSG_SIN_GRUPOS_SELECCIONADOS, MSG_ERROR, MB_ICONQUESTION , dblGrupos );
        Exit;
    end;


 	  //Crea la listas de Debitos
    FDebitosTXT := TStringList.Create;

    //Deshabilita los botones
  	btnCancelar.Enabled := True;
    btnProcesar.Enabled := False;
    BorderIcons := [];
    FErrorMsg := '';

    FDetenerImportacion := False;

    try

        //Registro la operacion y Genero el archivo de debitos
        {if RegistrarOperacion and GenerarDebitosCMR then begin
            //Abro un Transacci�n
            DMConnections.BaseCAC.BeginTrans;
            //Actualiza el estado de los debitos
        	if ActualizarDebitosCMR then begin}
                //Acepto la Transaccion
            //DMConnections.BaseCAC.BeginTrans;                                             //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('BEGIN TRAN frmGeneracionDebitosCMR');     //SS_1385_NDR_20150922

            if RegistrarOperacion and GenerarDebitosCMR then begin
                //Acepto la Transaccion
                //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('COMMIT TRAN frmGeneracionDebitosCMR');					//SS_1385_NDR_20150922
            end else begin
                //Si hubo errores hace un RollBack
                //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION frmGeneracionDebitosCMR END');	//SS_1385_NDR_20150922
                try
                    //Elimino el archivo
                    DeleteFile(edDestino.Text);
                except
                    on e: Exception do begin
                       //Informo que no pudo eliminar el archivo
                       MsgBoxErr(MSG_ERROR_DELETE_FILE, e.Message, Self.caption, MB_ICONERROR);
                    end;
                end;
                //Muestro un Cartel informando que no pudo actualizar los debitos
                MsgBoxErr(MSG_COULD_NOT_UPDATE_SENT_DEBITS, FErrorMsg, MSG_ERROR, MB_ICONERROR);
            end;
        //end;
        //ss750
        ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacion, FCantidadTotal, FMontoTotal, '', FErrorMsg);
        if ( FErrorMsg = '' ) then begin

            //Actualizo el log al Final
            ActualizarLog;
            //Informo que la operacion Finalizo con exito
            MsgBox(MSG_PROCESS_SUCCEDED, Caption, MB_ICONINFORMATION);
            //Muestro el Reporte de Finalizacion del Proceso
            GenerarReportedeFinalizacion(FCodigoOperacion);

        end else begin

            //Informo que el proceso no se pudo completar
        	MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);

        end;

    finally
        //Libero el StringList
		    FreeAndNil( FDebitosTXT );
    	  //Habilita los botones
    		btnCancelar.Enabled := False;
       	btnProcesar.Enabled := True;
        Close;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Detiene el proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosCMR.btnCancelarClick(Sender: TObject);
resourcestring
	  MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Permito salir si no esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosCMR.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
	  CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosCMR.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    FLamas
  Date Created: 21/06/2005
  Description: Lo libero de Memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGeneracionDebitosCMR.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	  Action := caFree;
end;

end.
