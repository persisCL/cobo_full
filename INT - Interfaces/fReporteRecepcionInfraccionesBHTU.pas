unit fReporteRecepcionInfraccionesBHTU;

interface

uses
    // Db
    DMConnection,
    // comunes
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppProd, ppClass, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, DB, ADODB, ppCtrls,
  ppPrnabl, ppBands, ppCache, UtilProc;


type
  TfrmReporteRecepcionInfraccionesBHTU = class(TForm)
    spObtenerDatosReporteRecepcionInfraccionesBHTU: TADOStoredProc;
    ppReporte: TppReport;
    RBI: TRBInterface;
    dsReporte: TDataSource;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppDBText1: TppDBText;
    ppDBReporte: TppDBPipeline;
    lbl_usuario: TppLabel;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLine: TppLine;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLine1: TppLine;
    lblArchivoInfracciones: TppLabel;
    lblArchivoTransitos: TppLabel;
    lblFechaArchivo: TppLabel;
    lblReproceso: TppLabel;
    lblFechaProceso: TppLabel;
    lblUsuarioProceso: TppLabel;
    lblNumeroProceso: TppLabel;
    ppLabel16: TppLabel;
    lblCantInfracciones: TppLabel;
    lblCantInfraccionesCargadas: TppLabel;
    lblCantInfraccionesNoCargadas: TppLabel;
    lblCantTransitos: TppLabel;
    lblTrxC: TppLabel;
    lblTrxNC: TppLabel;
    ppShape1: TppShape;
    ppLabel17: TppLabel;
    lblConcesionaria: TppLabel;
    procedure RBIExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FCodigoOperacion: Integer;
    function PrepararReporte(CodigoOperacion: Integer; var Error: AnsiString): boolean;
  public
    { Public declarations }
    function MostrarReporte( CodigoOperacion: Integer; Titulo: AnsiString): boolean;
  end;

var
  frmReporteRecepcionInfraccionesBHTU: TfrmReporteRecepcionInfraccionesBHTU;

implementation

{$R *.dfm}

{ TfrmReporteRecepcionInfraccionesBHTU }

function TfrmReporteRecepcionInfraccionesBHTU.MostrarReporte(CodigoOperacion: Integer;
  Titulo: AnsiString): boolean;
begin
    FCodigoOperacion := CodigoOperacion;
    RBI.Caption := Titulo;
    Result := RBI.Execute(True);
end;

function TfrmReporteRecepcionInfraccionesBHTU.PrepararReporte(CodigoOperacion: Integer;
  var Error: AnsiString): boolean;
begin
    result := False;
    try
        with spObtenerDatosReporteRecepcionInfraccionesBHTU do begin
            Parameters.ParamByName('@CodigoOperacion').Value := CodigoOperacion;
            Open;

            lblArchivoInfracciones.Caption := Parameters.ParamByName('@ArchivoInfracciones').Value;
            lblArchivoTransitos.Caption := Parameters.ParamByName('@ArchivoTransitos').Value;
            lblFechaArchivo.Caption := Parameters.ParamByName('@FechaArchivo').Value;
            lblReproceso.Caption := Parameters.ParamByName('@Reproceso').Value;
            lblFechaProceso.Caption := Parameters.ParamByName('@FechaProceso').Value;
            lblUsuarioProceso.Caption := Parameters.ParamByName('@UsuarioResponsable').Value;
            lblNumeroProceso.Caption := Parameters.ParamByName('@CodigoOperacion').Value;
            lblCantInfracciones.Caption := Parameters.ParamByName('@CantidadInfracciones').Value;
            lblCantInfraccionesCargadas.Caption := Parameters.ParamByName('@InfraccionesCargadas').Value;
            lblCantInfraccionesNoCargadas.Caption := Parameters.ParamByName('@InfraccionesNoCargadas').Value;
            lblCantTransitos.Caption := Parameters.ParamByName('@CantidadTransitos').Value;
            lblTrxC.Caption := Parameters.ParamByName('@TransitosCargados').Value;
            lblTrxNC.Caption := Parameters.ParamByName('@TransitosNoCargados').Value;
            lblConcesionaria.Caption := Parameters.ParamByName('@Concesionaria').Value;
          end;
          Result := True;
    except
        on e:exception do begin
            Error := e.Message;
        end;
    end;  
end;

procedure TfrmReporteRecepcionInfraccionesBHTU.RBIExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    ERROR_REPORTE = 'Error al intentar mostrar el reporte';
var
    descError: AnsiString;
begin
    if not PrepararReporte(FCodigoOperacion, descError) then begin
        MsgBoxErr( ERROR_REPORTE, descError, RBI.Caption, MB_ICONERROR);
        Cancelled := True;
    end;
end;

end.
